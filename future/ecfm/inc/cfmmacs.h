/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmmacs.h,v 1.91 2016/02/18 08:32:27 siva Exp $
 * 
 * Description: This file contains the macro definition for the ECFM
 *              Module.
 *********************************************************************/

#ifndef _CFM_MACS_H
#define _CFM_MACS_H

#ifdef ECFM_ARRAY_TO_RBTREE_WANTED
#define MIN(a,b) (a < b) ? a : b
#define ECFM_CC_MAX_PORT_INFO (MIN(FsECFMSizingParams[MAX_ECFM_CC_PORT_INFO_SIZING_ID].u4PreAllocatedUnits , ECFM_MAX_PORTS_PER_CONTEXT))
#define ECFM_LBLT_MAX_PORT_INFO (MIN(FsECFMSizingParams[MAX_ECFM_LBLT_PORT_INFO_SIZING_ID].u4PreAllocatedUnits , ECFM_MAX_PORTS_PER_CONTEXT))
#define ECFM_CC_MAX_MEP_INFO (MIN(FsECFMSizingParams[MAX_ECFM_CC_MEP_INFO_SIZING_ID].u4PreAllocatedUnits , ECFM_MAX_PORTS_PER_CONTEXT))
#define ECFM_LBLT_MAX_MEP_INFO (MIN(FsECFMSizingParams[MAX_ECFM_LBLT_MEP_INFO_SIZING_ID].u4PreAllocatedUnits , ECFM_MAX_PORTS_PER_CONTEXT))
#define ECFM_CC_MAX_MIP_INFO (MIN(FsECFMSizingParams[MAX_ECFM_CC_MIP_INFO_SIZING_ID].u4PreAllocatedUnits , ECFM_MAX_PORTS_PER_CONTEXT))
#else
#define ECFM_CC_MAX_PORT_INFO   ECFM_PORTS_PER_CONTEXT_MAX 
#define ECFM_LBLT_MAX_PORT_INFO ECFM_PORTS_PER_CONTEXT_MAX
#define ECFM_CC_MAX_MEP_INFO    ECFM_PORTS_PER_CONTEXT_MAX
#define ECFM_LBLT_MAX_MEP_INFO  ECFM_PORTS_PER_CONTEXT_MAX
#define ECFM_CC_MAX_MIP_INFO    ECFM_PORTS_PER_CONTEXT_MAX 
#endif

/*Macros used to assign values to buffer */
#define ECFM_PUT_1BYTE(pu1PktBuf,u1Val)\
do{\
    *pu1PktBuf = u1Val;\
    pu1PktBuf += 1;\
}while(0)
#define ECFM_PUT_2BYTE(pu1PktBuf, u2Val)\
do{\
    UINT2 u2Value = ECFM_HTONS(u2Val);\
    MEMCPY (pu1PktBuf, &u2Value, sizeof (UINT2));\
     pu1PktBuf += sizeof (UINT2);\
}while(0)

#define ECFM_PUT_4BYTE(pu1PktBuf, u4Val)\
do{\
    UINT4 u4Value = ECFM_HTONL(u4Val);\
    MEMCPY (pu1PktBuf, &u4Value, sizeof (UINT4));\
    pu1PktBuf += sizeof (UINT4);\
}while(0)

#define ECFM_PUT_NBYTE(pu1PktBuf, pu1Src, u4Len)\
do{\
    MEMCPY ((UINT1 *) pu1PktBuf, (UINT1 *) pu1Src, u4Len);\
    pu1PktBuf += u4Len;\
}while(0)

/* Macros used to extract values from buffer */

#define ECFM_GET_1BYTE(u1Val, pu1Buf)\
do{\
    u1Val = *pu1Buf;\
    pu1Buf += 1;\
}while(0)

#define ECFM_GET_2BYTE(u2Val, pu1Buf)\
do{\
    MEMCPY (&u2Val, pu1Buf, 2);\
    pu1Buf += 2;\
    u2Val = (UINT2) (ECFM_NTOHS(u2Val));\
}while(0)
#define ECFM_GET_4BYTE(u4Val, pu1Buf)\
do{\
    MEMCPY (&u4Val, pu1Buf, 4);\
    pu1Buf += 4;\
    u4Val = (UINT4) (ECFM_NTOHL(u4Val));\
}while(0)
#define ECFM_GET_NBYTE(pu1Dest, pu1Buf, u4Len)\
do{\
    MEMCPY ((UINT1 *) pu1Dest, (UINT1 *) pu1Buf, u4Len);\
    pu1Buf += u4Len;\
}while(0)

#define ECFM_Y1564_GET_4BYTE(u4Val, pu1Buf) \
{\
        MEMCPY (&u4Val, pu1Buf, 4);\
        u4Val = (OSIX_NTOHL(u4Val));\
}
#define ECFM_R2544_GET_4BYTE(u4Val, pu1Buf) \
{\
        MEMCPY (&u4Val, pu1Buf, 4);\
        u4Val = (OSIX_NTOHL(u4Val));\
}

/****************************************************************************/
/* To get the Timer value in millisecond on the bases of the CCMInterval 
  value present in the Ma Info*/
#define ECFM_GET_CCM_INTERVAL(u1CcmIntervalCode,u4CcmInterval)\
do \
{ \
  if ((u1CcmIntervalCode > 0) && (u1CcmIntervalCode < 8)) \
  {\
     u4CcmInterval = au4CCTimer[u1CcmIntervalCode]; \
  }\
  else \
  {\
     u4CcmInterval = 0; \
  }\
} while (0)
/****************************************************************************/
/* To get the Timer value in millisecond on the bases of the Ais/Lck Interval 
  value present in the Mep Info*/
#define ECFM_GET_AIS_LCK_INTERVAL(u1AisLckIntervalCode,u4AisLckInterval)\
do \
{\
 if ((u1AisLckIntervalCode > 0 ) && (u1AisLckIntervalCode < 3)) \
 {\
     u4AisLckInterval = au4AisLckTimer[u1AisLckIntervalCode];\
 }\
 else\
 {\
    u4AisLckInterval = ECFM_INIT_VAL;\
 }\
}while (0)
/****************************************************************************/
/* CRU related Macros */
#define ECFM_CRU_GET_1_BYTE(pMsg, u4Offset, u1Value)\
   CRU_BUF_Copy_FromBufChain(pMsg,((UINT1 *) &u1Value), u4Offset, 1)

#define ECFM_CRU_GET_2_BYTE(pMsg, u4Offset, u2Value) \
do{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = ECFM_NTOHS (u2Value);\
}while(0)
#define ECFM_CRU_GET_4_BYTE(pMsg, u4Offset, u4Value) \
do{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = ECFM_NTOHL (u4Value);\
}while(0)

#define ECFM_CRU_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,\
                            u4_StringLength) \
do{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1_StringMemArea, u4Offset,\
                            u4_StringLength);\
}while(0)

#define ECFM_BUF_SET_INTERFACEID(pBuf,IfaceId) \
        CRU_BUF_Set_InterfaceId(pBuf,IfaceId)

#define ECFM_BUF_GET_INTERFACEID(pBuf) \
        CRU_BUF_Get_InterfaceId(pBuf)

#define ECFM_LINK_BUFFERS(pBuf1, pBuf2) \
        CRU_BUF_Link_BufChains (pBuf1, pBuf2) 

#define ECFM_UNLINK_BUFFERS(pBuf) \
        CRU_BUF_UnLink_BufChain (pBuf)

#define ECFM_ALLOC_CRU_BUF                    CRU_BUF_Allocate_MsgBufChain
#define ECFM_RELEASE_CRU_BUF                  CRU_BUF_Release_MsgBufChain
#define ECFM_COPY_OVER_CRU_BUF                CRU_BUF_Copy_OverBufChain
#define ECFM_COPY_FROM_CRU_BUF                CRU_BUF_Copy_FromBufChain
#define ECFM_GET_CRU_VALID_BYTE_COUNT         CRU_BUF_Get_ChainValidByteCount
#define ECFM_CRU_BUF_MOVE_VALID_OFFSET        CRU_BUF_Move_ValidOffset
#define ECFM_GET_DATA_PTR_IF_LINEAR           CRU_BUF_Get_DataPtr_IfLinear
#define ECFM_DUPLICATE_CRU_BUF                CRU_BUF_Duplicate_BufChain
#define ECFM_PREPEND_CRU_BUF                  CRU_BUF_Prepend_BufChain 
#define ECFM_DEL_CRU_BUF_AT_END               CRU_BUF_Delete_BufChainAtEnd 
#define ECFM_FRAGMENT_CRU_BUF                 CRU_BUF_Fragment_BufChain
#define ECFM_CONCAT_CRU_BUF                   CRU_BUF_Concat_MsgBufChains

#define ECFM_VCM_SUCCESS                      VCM_SUCCESS
#define ECFM_VCM_FAILURE                      VCM_FAILURE
#define ECFM_CRU_SUCCESS                      CRU_SUCCESS
#define ECFM_CRU_FAILURE                      CRU_FAILURE
#define ECFM_MEM_SUCCESS                      MEM_SUCCESS
#define ECFM_MEM_FAILURE                      MEM_FAILURE
#define ECFM_TMR_SUCCESS                      TMR_SUCCESS
#define ECFM_TMR_FAILURE                      TMR_FAILURE
#define ECFM_RB_SUCCESS                       RB_SUCCESS
#define ECFM_RB_FAILURE                       RB_FAILURE
#define ECFM_HTONS                            OSIX_HTONS
#define ECFM_NTOHS                            OSIX_NTOHS
#define ECFM_HTONL                            OSIX_HTONL
#define ECFM_NTOHL                            OSIX_NTOHL
#define ECFM_PSEUDO_WIRE                 CFA_PSEUDO_WIRE
/*****************************************************************************/
/* Macro for creating a Memory Pool */
#define ECFM_CREATE_MEM_POOL(x,y,MemType,EcfmMemPoolId)\
    MemCreateMemPool(x,y,MemType,(tMemPoolId *)EcfmMemPoolId)

/* Macro for deleting a Memory Pool */
#define ECFM_DELETE_MEM_POOL( EcfmMemPoolId)\
    MemDeleteMemPool (EcfmMemPoolId)

/* Macro for getting the number of free Units in the MemPool */
#define ECFM_GET_FREE_MEM_UNITS(EcfmPoolId) \
        MemGetFreeUnits(EcfmPoolId)

/* Macro for freeing memory block to the Memory Pools */
#define ECFM_FREE_MEM_BLOCK(EcfmPoolId, pu1Msg)\
    MemReleaseMemBlock (EcfmPoolId, pu1Msg)

/****************************************************************************/
/* Macro for allocating Memory Blocks from the Memory Pools */
#define ECFM_ALLOC_MEM_BLOCK_ACTIVE_MD_LEVEL(pu1ActiveMdLevels) \
    (pu1ActiveMdLevels = (UINT1 *) MemAllocMemBlk(ECFM_CC_ACTIVE_MD_LEVEL_POOL))

#define ECFM_ALLOC_MEM_BLOCK_ACTIVE_MA_LEVEL(ppActiveMaNodes) \
    (ppActiveMaNodes = (tEcfmCcMaInfo **) MemAllocMemBlk(ECFM_CC_ACTIVE_MA_LEVEL_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_MSGQ(pMsg)\
          (pMsg = (tEcfmLbLtMsg *) MemAllocMemBlk(ECFM_LBLT_MSGQ_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_MSGQ(pMsg)\
          (pMsg = (tEcfmCcMsg *) MemAllocMemBlk(ECFM_CC_MSGQ_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_CONTEXT(pContextInfo) \
    (pContextInfo = (tEcfmCcContextInfo *) MemAllocMemBlk(ECFM_CC_CONTEXT_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_CONTEXT(pContextInfo) \
    (pContextInfo = (tEcfmLbLtContextInfo *) MemAllocMemBlk(ECFM_LBLT_CONTEXT_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_PORT_INFO(pPortEntry) \
    (pPortEntry = (tEcfmCcPortInfo *) MemAllocMemBlk(ECFM_CC_PORT_INFO_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_PORT_INFO(pPortEntry) \
    (pPortEntry = (tEcfmLbLtPortInfo *) MemAllocMemBlk(ECFM_LBLT_PORT_INFO_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_TABLE(pLbLtMepNewNode) \
    (pLbLtMepNewNode = (tEcfmLbLtMepInfo *) MemAllocMemBlk(ECFM_LBLT_MEP_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_LBM_PDU(pu1Octets) \
    (pu1Octets = MemAllocMemBlk(ECFM_LBLT_MEP_LBM_PDU_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_MEP_LBM_DATA_TLV(pu1Octets) \
    (pu1Octets = MemAllocMemBlk(ECFM_LBLT_MEP_LBM_DATA_TLV_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_STACK_TABLE(pStackNewNode) \
    (pStackNewNode = (tEcfmCcStackInfo *) MemAllocMemBlk(ECFM_CC_STACK_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_REG_APP(pEcfmAppInfo) \
    (pEcfmAppInfo = (tEcfmRegParams *) MemAllocMemBlk(ECFM_CC_APP_REG_MEM_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_ECFM_HW_ARRAY(pEcfmHwArray) \
    (pEcfmHwArray = (UINT4 *) MemAllocMemBlk(ECFM_CC_HW_PORTLIST_ARRAY))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_STACK_TABLE(pStackNewNode) \
    (pStackNewNode = (tEcfmLbLtStackInfo *) MemAllocMemBlk(ECFM_LBLT_STACK_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_MIP_TABLE(pMipNewNode) \
    (pMipNewNode = (tEcfmLbLtMipInfo *) MemAllocMemBlk(ECFM_LBLT_MIP_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTM_REPLY_LIST_TABLE(pLtmNode) \
    (pLtmNode = (tEcfmLbLtLtmReplyListInfo *) MemAllocMemBlk(ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_TABLE(pLtrNode) \
    (pLtrNode = (tEcfmLbLtLtrInfo *) MemAllocMemBlk(ECFM_LBLT_LTR_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_SENDER_TLV_CHASSIS_ID(pu1Octets)\
    (pu1Octets = MemAllocMemBlk (ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN(pu1Octets)\
    (pu1Octets = MemAllocMemBlk (ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_SENDER_TLV_MGMT_ADDR(pu1Octets)\
    (pu1Octets = MemAllocMemBlk (ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_ORG_TLV_VALUE(pu1Octets)\
    (pu1Octets = MemAllocMemBlk (ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_EGRESS_TLV_EPORT_ID(pu1Octets)\
    (pu1Octets = MemAllocMemBlk (ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LTR_INRESS_TLV_EPORT_ID(pu1Octets)\
    (pu1Octets = MemAllocMemBlk (ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LBR_TABLE(pLbrNode) \
    (pLbrNode = (tEcfmLbLtLbrInfo *) MemAllocMemBlk(ECFM_LBLT_LBR_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_LBM_TABLE(pLbmNode) \
    (pLbmNode = (tEcfmLbLtLbmInfo *) MemAllocMemBlk(ECFM_LBLT_LBM_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_REG_APP(pEcfmAppInfo) \
     (pEcfmAppInfo = (tEcfmRegParams *) MemAllocMemBlk(ECFM_LBLT_APP_REG_MEM_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_MEP_DB_TABLE(pRMepNewNode) \
    (pRMepNewNode = (tEcfmCcRMepDbInfo *) MemAllocMemBlk(ECFM_CC_MEP_DB_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_RMEP_CHASSIS_ID(pu1Octets) \
    (pu1Octets = MemAllocMemBlk(ECFM_CC_RMEP_CHASSIS_ID_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_RMEP_MGMT_ADDR_DOMAIN(pu1Octets) \
    (pu1Octets =  MemAllocMemBlk(ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_RMEP_MGMT_ADDR(pu1Octets) \
    (pu1Octets =  MemAllocMemBlk(ECFM_CC_RMEP_MGMT_ADDR_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_MIP_DB_TABLE(pMipCcmNode) \
    (pMipCcmNode = (tEcfmCcMipCcmDbInfo *) MemAllocMemBlk(ECFM_CC_MIP_DB_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_MIP_TABLE(pMipNode) \
    (pMipNode = (tEcfmCcMipInfo *) MemAllocMemBlk(ECFM_CC_MIP_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_MIP_PREVENT_TABLE(pMipNode) \
    (pMipNode = \
(tEcfmCcMipPreventInfo *) MemAllocMemBlk(ECFM_CC_MIP_PREVENT_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_VLAN_TABLE(pVlanNewNode) \
    (pVlanNewNode = (tEcfmCcVlanInfo *) MemAllocMemBlk(ECFM_CC_VLAN_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_CONF_ERR_LIST_TABLE(pConfigErrNode) \
    (pConfigErrNode = (tEcfmCcConfigErrInfo *) MemAllocMemBlk(ECFM_CC_CONF_ERR_LIST_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_MD_TABLE(pMdNode) \
    (pMdNode = (tEcfmCcMdInfo *) MemAllocMemBlk(ECFM_CC_MD_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_MD_TABLE(pMdNode) \
    (pMdNode = (tEcfmLbLtMdInfo *) MemAllocMemBlk(ECFM_LBLT_MD_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_DEF_MD_TABLE(pDefMdNode) \
    (pDefMdNode = (tEcfmCcDefaultMdTableInfo *) MemAllocMemBlk(ECFM_CC_DEF_MD_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_MA_TABLE(pMaNode) \
    (pMaNode = (tEcfmCcMaInfo *) MemAllocMemBlk(ECFM_CC_MA_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_MA_TABLE(pMaNode) \
    (pMaNode = (tEcfmLbLtMaInfo *) MemAllocMemBlk(ECFM_LBLT_MA_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_MA_MEP_LIST_TABLE(pMepListNode) \
    (pMepListNode = (tEcfmCcMepListInfo *) MemAllocMemBlk(ECFM_CC_MA_MEP_LIST_TABLE_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_MEP_TABLE(pMepNode) \
    (pMepNode = (tEcfmCcMepInfo *) MemAllocMemBlk(ECFM_CC_MEP_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_MEP_MPTP_PARAMS_BLK(pEcfmMplsParams) \
        (pEcfmMplsParams = (tEcfmMplsParams *) MemAllocMemBlk(ECFM_CC_MEP_MPTP_PARAMS_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_CC_ERR_LOG_TABLE(pCcErrorNode) \
    (pCcErrorNode = (tEcfmCcErrLogInfo *) MemAllocMemBlk(ECFM_CC_ERR_LOG_POOL))
    
#define ECFM_ALLOC_MEM_BLOCK_CC_FL_BUFF_TABLE(pFrmLossBuffNode) \
    (pFrmLossBuffNode = (tEcfmCcFrmLossBuff *) MemAllocMemBlk(ECFM_CC_FRM_LOSS_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_FD_BUFF_TABLE(pFrmDelayBuffNode) \
    (pFrmDelayBuffNode = (tEcfmLbLtFrmDelayBuff *) MemAllocMemBlk(ECFM_LBLT_FD_BUFFER_POOL))
 
#define ECFM_ALLOC_MEM_BLOCK_LBLT_MIP_DB_TABLE(pMipCcmDbNode) \
    (pMipCcmDbNode = (tEcfmLbLtMipCcmDbInfo *) MemAllocMemBlk(ECFM_LBLT_MIP_CCM_DB_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_RMEP_DB_TABLE(pRMepDbNode) \
    (pRMepDbNode = (tEcfmLbLtRMepDbInfo *) MemAllocMemBlk(ECFM_LBLT_RMEP_DB_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_DELAY_QUEUE(pNode) \
    (pNode = (tEcfmLbLtDelayQueueNode *) MemAllocMemBlk(ECFM_LBLT_DELAY_QUEUE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBLT_DEF_MD_TABLE(pDefMdNode) \
    (pDefMdNode = (tEcfmLbLtDefaultMdTableInfo *) MemAllocMemBlk(ECFM_LBLT_DEF_MD_TABLE_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_MEP_AVLBLTY_TABLE(pAvlbltyNode) \
    (pAvlbltyNode = (tEcfmCcAvlbltyInfo *) MemAllocMemBlk(ECFM_AVLBLTY_INFO_POOLID))

#define ECFM_ALLOC_MEM_BLOCK_CC_MEP_ERR_CCM(pu1Octets) \
    (pu1Octets = MemAllocMemBlk(ECFM_CC_MEP_ERR_CCM_POOL))

#define ECFM_ALLOC_MEM_BLOCK_CC_MEP_XCON_CCM(pu1Octets) \
    (pu1Octets = MemAllocMemBlk(ECFM_CC_MEP_XCON_CCM_POOL))

#define ECFM_ALLOC_MEM_BLOCK_LBR_RCVD_INFO(pLbrRcvdInfoNode)\
     (pLbrRcvdInfoNode = (tEcfmLbrRcvdInfo *) MemAllocMemBlk (ECFM_LBR_RCVD_INFO_POOL))

/****************************************************************************/
/* Macro for getting task id */
#define ECFM_SELF_TASK_ID(TaskId) \
        OsixTskIdSelf((tOsixTaskId *) TaskId)

/* Macro for creating a Message Queue */
#define ECFM_CREATE_MSG_QUEUE(au1QName, u4MsgLen, u4MaxMsg, pQueId) \
        OsixQueCrt(au1QName, u4MsgLen, u4MaxMsg, (tOsixQId *)pQueId)

/* Macro for Deleting a Message Queue */
#define ECFM_DELETE_MSG_QUEUE(QueId) \
        OsixQueDel(QueId)

/* Macro for Posting message to a Queue */
#define ECFM_ENQUE_MSG(EcfmMsgQId, pu1Msg, u4MsgLen )\
        OsixQueSend (EcfmMsgQId,pu1Msg, u4MsgLen)

/* Macro for extracting message from a queue */
#define ECFM_DEQUE_MSG(EcfmQId, pu1Msg, u4MsgLen )\
        OsixQueRecv (EcfmQId, pu1Msg, u4MsgLen, 0)

/* Macro for receivng Event received on the Task */
#define ECFM_RECEIVE_EVENT(TaskId, u4Event, u4Flags, pu4RcvEvent) \
        OsixEvtRecv (TaskId,u4Event,u4Flags,pu4RcvEvent)

/* Macro for sending an event to task */
#define ECFM_SEND_EVENT(EcfmTaskId, u4Event)\
        OsixEvtSend (EcfmTaskId, u4Event)

/* Semaphore Createtion */
#define ECFM_CREATE_SEMAPHORE(au1SemName, SemId)\
        OsixSemCrt (au1SemName,(tOsixSemId *) SemId)

/* Semaphore Deletion */
#define ECFM_DELETE_SEMAPHORE(SemId)\
        OsixSemDel(SemId)

/* Mutual Exclusion Lock On */
#define ECFM_TAKE_SEMAPHORE(SemId)\
        OsixSemTake (SemId)

/* Mutual Exclusion Lock Off */
#define ECFM_GIVE_SEMAPHORE(SemId)\
        OsixSemGive(SemId)


#define ECFM_MPLSTP_DEFAULT_MDLEVEL     7

#define ECFM_MPLSTP_MAX_GACH_TLV_HEADER_LEN 4
#define ECFM_MPLSTP_MAX_GACH_TLV_LEN           255
#define ECFM_MPLSTP_MAX_IP_HDR_LEN             24
#define ECFM_MPLSTP_MAX_UDP_HDR_LEN            8
#define ECFM_MPLSTP_CTRL_PKT_OFFSET     (MPLS_MAX_L2HEADER_LEN +\
                                        (MPLS_MAX_LABEL_STACK  * 4)+\
                                        MPLS_MAX_GAL_GACH_HEADER_LEN +\
                                        ECFM_MPLSTP_MAX_GACH_TLV_HEADER_LEN +\
                                        ECFM_MPLSTP_MAX_GACH_TLV_LEN +\
                                        34) 

/* MPLS_TP PORT TYPE */
#define ECFM_MPLSTP_DEFAULT_PORT_TYPE     7 
#define ECFM_MPLSTP_PATH_UP    1
#define ECFM_MPLSTP_PATH_DOWN  2
/* Defines of Library functions used*/
#define ECFM_MEMCPY              MEMCPY
#define ECFM_MEMCMP              MEMCMP
#define ECFM_MEMSET              MEMSET
#define ECFM_STRCMP              STRCMP
#define ECFM_STRCPY              STRCPY
#define ECFM_STRLEN              STRLEN
#define ECFM_MALLOC              MEM_MALLOC
#define ECFM_MEM_FREE            MEM_FREE
/* Timer related macros */

#define ECFM_CREATE_TMR_LIST         TmrCreateTimerList
#define ECFM_DELETE_TMR_LIST         TmrDeleteTimerList
#define ECFM_START_TIMER             TmrStartTimer
#define ECFM_STOP_TIMER              TmrStopTimer
#define ECFM_GET_NEXT_EXPIRED_TMR    TmrGetNextExpiredTimer
#define ECFM_GET_SYS_TIME            OsixGetSysTime
#define ECFM_GET_REMAINING_TIME      TmrGetRemainingTime

/* Trace Options */
#define ECFM_GLB_TRC_OPTION           gu4EcfmGlobalTrace
#define ECFM_TRACE_OPTION(u4ContextId)gau4EcfmTraceOption[u4ContextId]
#define ECFM_CC_TRACE_OPTION          gau4EcfmTraceOption[gpEcfmCcContextInfo->u4ContextId]
#define ECFM_LBLT_TRACE_OPTION        gau4EcfmTraceOption[gpEcfmLbLtContextInfo->u4ContextId]

#define ECFM_TRACE_OPTION_FOR_CONTEXT(u4ContextId)\
        gau4EcfmTraceOption[u4ContextId]

#define ECFM_Y1731_STATUS(u4ContextId) \
    ((u4ContextId >= \
    ECFM_MAX_CONTEXTS)?(ECFM_DISABLE):(gau1EcfmY1731Status[u4ContextId]))

#define ECFM_SYSTEM_STATUS(u4ContextId) \
    ((u4ContextId >= \
    ECFM_MAX_CONTEXTS)?(ECFM_SHUTDOWN):(gau1EcfmSystemStatus[u4ContextId]))

#define ECFM_MODULE_STATUS(u4ContextId) \
    ((u4ContextId >= \
        ECFM_MAX_CONTEXTS)?(ECFM_DISABLE):(gau1EcfmModuleStatus[u4ContextId]))

#define ECFM_SET_Y1731_STATUS(u4ContextId, u1Status) \
do {\
    if (u4ContextId < ECFM_MAX_CONTEXTS) \
    {\
        gau1EcfmY1731Status[u4ContextId] = u1Status;\
    }\
}while (0)

#define ECFM_REVERSE_MEP_DIR(u1RxDirection) \
((u1RxDirection == ECFM_MP_DIR_DOWN)?ECFM_MP_DIR_UP:ECFM_MP_DIR_DOWN)

#define ECFM_SET_SYSTEM_STATUS(u4ContextId, u1Status) \
do {\
    if (u4ContextId < ECFM_MAX_CONTEXTS) \
    {\
       gau1EcfmSystemStatus[u4ContextId] = u1Status;\
    }\
}while (0)


#define ECFM_SET_MODULE_STATUS(u4ContextId, u1Status) \
do {\
   if (u4ContextId < ECFM_MAX_CONTEXTS) \
   {\
      gau1EcfmModuleStatus[u4ContextId] = u1Status;\
   }\
}while (0)

#define ECFM_Y1731_PORT_OPER_STATUS(pPortInfo)\
        (pPortInfo->u1PortY1731Status)

#define ECFM_IS_SYSTEM_STARTED(u4ContextId)\
        (ECFM_SYSTEM_STATUS(u4ContextId)==ECFM_START)
#define ECFM_IS_SYSTEM_SHUTDOWN(u4ContextId)\
        (!ECFM_IS_SYSTEM_STARTED(u4ContextId))

#define ECFM_IS_MODULE_ENABLED(u4ContextId)\
        (ECFM_MODULE_STATUS(u4ContextId)==ECFM_ENABLE)
#define ECFM_IS_MODULE_DISABLED(u4ContextId)\
        (!ECFM_IS_MODULE_ENABLED(u4ContextId))

#define ECFM_IS_Y1731_ENABLED(u4ContextId)\
        (ECFM_Y1731_STATUS(u4ContextId)==ECFM_ENABLE)
#define ECFM_IS_Y1731_DISABLED(u4ContextId)\
        (!ECFM_IS_Y1731_ENABLED(u4ContextId))

#define ECFM_CC_INITIALISED() gu1EcfmCcInitialised        
#define ECFM_LBLT_INITIALISED() gu1EcfmLbLtInitialised        

#define ECFM_IS_SYSTEM_INITIALISED()\
        ((ECFM_CC_INITIALISED()==ECFM_TRUE)&&(ECFM_LBLT_INITIALISED()==ECFM_TRUE))
#define ECFM_GLOBAL_CCM_OFFLOAD_STATUS   \
        ECFM_CC_CURR_CONTEXT_INFO ()->b1GlobalCcmOffload

#define ECFM_IS_GLOBAL_CCM_OFFLOAD_ENABLED()\
        ((ECFM_CC_CURR_CONTEXT_INFO ()->b1GlobalCcmOffload) == ECFM_CCM_OFFLOAD_ENABLE)

#define ECFM_SET_MEMBER_PORT(au1PortArray, u2Port) \
do\
{\
 UINT2 u2PortBytePos=0;\
 UINT2 u2PortBitPos=0;\
 UINT1 au1PortBitMap[8] = { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
 u2PortBytePos = (UINT2)(u2Port / 8);\
 u2PortBitPos  = (UINT2)(u2Port % 8);\
 if (u2PortBitPos  == 0) {u2PortBytePos --;} \
 if (u2PortBytePos < sizeof(au1PortArray)) {\
 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
               | au1PortBitMap[u2PortBitPos]);}\
}while(0)

#define ECFM_GROUP_MAC_ADDRESS      gu1CfmMacAddr
#define ECFM_TUNNEL_MAC_ADDRESS     gu1CfmTunnelMacAddr

#define ECFM_SYSLOG_ID              gEcfmCcGlobalInfo.u4SysLogId
#define ECFM_NAME                   (CONST UINT1 *)"ECFM"
#define ECFM_GET_TRANSPORT_DOMAIN(u1SubType)\
gaau4TransportDomain[((u1SubType-1)<ECFM_MAX_TRANSPORT_DOMAIN)?(u1SubType-1):0]
/* chassis-Id*/
#define ECFM_CHASSISID               gEcfmCcGlobalInfo.au1ChassisId
#define ECFM_CHASSISID_SUBTYPE       gEcfmCcGlobalInfo.u1ChassisIdSubType
#define ECFM_CHASSISID_LEN           gEcfmCcGlobalInfo.u1ChassisIdLen
/* management address domain*/
#define ECFM_MGMT_ADDR_DOMAIN_CODE   gEcfmCcGlobalInfo.u1MgmtAddressDomain
#define ECFM_MGMT_ADDR_DOMAIN        ECFM_GET_TRANSPORT_DOMAIN(ECFM_MGMT_ADDR_DOMAIN_CODE)   
/* management address domain length = transport domain length + 2 bytes of ans1
 * BER encoding*/
#define ECFM_MGMT_ADDR_DOMAIN_LEN    ECFM_MAX_TRANSPORT_DOMAIN_LEN + 2
/* management address*/
#define ECFM_MGMT_ADDRESS            gEcfmCcGlobalInfo.au1MgmtAddress
#define ECFM_MGMT_ADDRESS_LEN        gEcfmCcGlobalInfo.u4MgmtAddressLen
#define ECFM_IPv4_L3IFINDEX          gEcfmCcGlobalInfo.i4Ipv4L3IfIndex
#define ECFM_IPv6_L3IFINDEX          gEcfmCcGlobalInfo.i4Ipv6L3IfIndex
#define ECFM_HW_CAPABILITY           gu4HwCapability
#define ECFM_BITLIST_SCAN_MASK       gaau1EcfmBitListScanMask
#define ECFM_HW_LBM_SUPPORT()        ((ECFM_HW_CAPABILITY&0x00000001)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_HW_DMM_SUPPORT()        (((ECFM_HW_CAPABILITY&0x00000002)>>1)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_HW_1DM_SUPPORT()        (((ECFM_HW_CAPABILITY&0x00000004)>>2)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_HW_LMM_SUPPORT()        (((ECFM_HW_CAPABILITY&0x00000008)>>3)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_HW_TST_SUPPORT()        (((ECFM_HW_CAPABILITY&0x00000010)>>4)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_HW_CCMOFFLOAD_SUPPORT() (((ECFM_HW_CAPABILITY&0x00000020)>>5)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_HW_UPMEP_OFFLOAD_SUPPORT() (((ECFM_HW_CAPABILITY&0x00000040)>>6)?ECFM_TRUE:ECFM_FALSE)
#define ECFM_BITLIST_SCAN_TABLE(value,pos) ECFM_BITLIST_SCAN_MASK[value][pos]
/******************************************************************************/
/*                         MACROS FOR CC TASK                                 */
/******************************************************************************/
#define ECFM_CC_RMEP_CCM_DB_SIZE    (sizeof(tEcfmCcRMepDbInfo))
#define ECFM_CC_MIP_CCM_DB_INFO_SIZE (sizeof(tEcfmCcMipCcmDbInfo))
#define ECFM_CC_MEP_INFO_SIZE       (sizeof(tEcfmCcMepInfo))
#define ECFM_CC_MEP_MPTP_PARAMS_SIZE  (sizeof(tEcfmMplsParams))
#define ECFM_CC_MIP_INFO_SIZE       (sizeof(tEcfmCcMipInfo))
#define ECFM_CC_MIP_PREVENT_INFO_SIZE (sizeof(tEcfmCcMipPreventInfo))
#define ECFM_CC_MA_INFO_SIZE        (sizeof(tEcfmCcMaInfo))
#define ECFM_CC_MD_INFO_SIZE        (sizeof(tEcfmCcMdInfo))
#define ECFM_CC_VLAN_INFO_SIZE      (sizeof(tEcfmCcVlanInfo))
#define ECFM_CC_PORT_INFO_SIZE      (sizeof(tEcfmCcPortInfo))
#define ECFM_CC_STACK_INFO_SIZE     (sizeof(tEcfmCcStackInfo))
#define ECFM_CC_MEP_LIST_SIZE       (sizeof(tEcfmCcMepListInfo))
#define ECFM_CC_CONTEXT_INFO_SIZE   (sizeof(tEcfmCcContextInfo))
#define ECFM_CC_MSG_INFO_SIZE       (sizeof(tEcfmCcMsg))
#define ECFM_CC_GLOBAL_INFO_SIZE    (sizeof(tEcfmCcGlobalInfo))
#define ECFM_CC_PDUSM_INFO_SIZE     (sizeof(tEcfmCcPduSmInfo))
#define ECFM_CC_CONFIG_ERR_INFO_SIZE     (sizeof(tEcfmCcConfigErrInfo))
#define ECFM_CC_DEF_MD_INFO_SIZE    (sizeof(tEcfmCcDefaultMdTableInfo))
#define ECFM_CC_ERR_LOG_INFO_SIZE   (sizeof(tEcfmCcErrLogInfo))
#define ECFM_CC_FRM_LOSS_INFO_SIZE  (sizeof(tEcfmCcFrmLossBuff)) 
#define ECFM_CC_ACTIVE_MA_LEVEL_SIZE ((sizeof(tEcfmCcMaInfo*))*(ECFM_MAX_MA))
#define ECFM_CC_ACTIVE_MD_LEVEL_SIZE  ((sizeof(UINT1))*(ECFM_MAX_MA))

/*Organization Unique Identifier for CC Task*/
#define ECFM_CC_ORG_UNIT_ID         gEcfmCcGlobalInfo.au1OUI

#define ECFM_CC_PDU        gEcfmCcGlobalInfo.pu1CCPdu

#define ECFM_CC_SELECT_CONTEXT(u4ContextId)\
        EcfmCcSelectContext (u4ContextId)
#define ECFM_CC_RELEASE_CONTEXT()\
        EcfmCcReleaseContext()

#define ECFM_CC_GET_CONTEXT_INFO(u4ContextId)\
        gEcfmCcGlobalInfo.apContextInfo[u4ContextId]

#define ECFM_CC_CURR_CONTEXT_INFO() gpEcfmCcContextInfo
#define ECFM_CC_CURR_CONTEXT_ID()   (ECFM_CC_CURR_CONTEXT_INFO())->u4ContextId
#define ECFM_CC_BRIDGE_MODE() (ECFM_CC_CURR_CONTEXT_INFO())->u4BridgeMode
#define ECFM_CC_REG_MOD_BMP() (ECFM_CC_CURR_CONTEXT_INFO())->u4RegModBmp

/*macros for System control*/
#define ECFM_CC_CFG_QUEUE_ID        gEcfmCcGlobalInfo.CfgQId
#define ECFM_CC_PKT_QUEUE_ID        gEcfmCcGlobalInfo.PktQId
#define ECFM_CC_INT_QUEUE_ID        gEcfmCcGlobalInfo.IntQId
#define ECFM_CC_SEM_ID              gEcfmCcGlobalInfo.SemId
#define ECFM_CC_TASK_ID             gEcfmCcGlobalInfo.TaskId
#define ECFM_CC_TMRLIST_ID          gEcfmCcGlobalInfo.TimerListId
/* Semaphore Name for CC Task */
#define ECFM_CC_SEM_NAME            "CcSem"

/*macros for  Memory control*/
#define ECFM_CC_TMR_MEMPOOL                 gEcfmCcGlobalInfo.TmrMemPool
#define ECFM_CC_MSGQ_POOL                   gEcfmCcGlobalInfo.MsgQMemPool
#define ECFM_CC_PORT_INFO_POOL              gEcfmCcGlobalInfo.PortInfoMemPool
#define ECFM_CC_MD_TABLE_POOL               gEcfmCcGlobalInfo.MdTableMemPool
#define ECFM_CC_MA_TABLE_POOL               gEcfmCcGlobalInfo.MaTableMemPool
#define ECFM_CC_MEP_TABLE_POOL              gEcfmCcGlobalInfo.MepTableMemPool
#define ECFM_CC_MEP_MPTP_PARAMS_POOL        gEcfmCcGlobalInfo.MepMpTpParamsMemPool
#define ECFM_CC_MEP_DB_TABLE_POOL           gEcfmCcGlobalInfo.MepDbTableMemPool
#define ECFM_CC_RMEP_CHASSIS_ID_POOL        gEcfmCcGlobalInfo.RmepChassiIdPool
#define ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_POOL  gEcfmCcGlobalInfo.RmepMgmtAddrDomPool
#define ECFM_CC_RMEP_MGMT_ADDR_POOL         gEcfmCcGlobalInfo.RmepMgmtAddrPool
#define ECFM_CC_DEF_MD_TABLE_POOL           gEcfmCcGlobalInfo.DefMdTableMemPool
#define ECFM_CC_APP_REG_MEM_POOL            gEcfmCcGlobalInfo.AppRegMemPoolId
#define ECFM_CC_ACTIVE_MA_LEVEL_POOL        gEcfmCcGlobalInfo.ActiveMaLevelMemPool
#define ECFM_CC_ACTIVE_MD_LEVEL_POOL        gEcfmCcGlobalInfo.ActiveMdLevelMemPool
#define ECFM_CC_MEP_ERR_CCM_POOL            gEcfmCcGlobalInfo.MepErrCcmMemPool
#define ECFM_CC_MEP_XCON_CCM_POOL           gEcfmCcGlobalInfo.MepXconCcmMemPool
#define ECFM_LBR_RCVD_INFO_POOL             gEcfmCcGlobalInfo.LbrRcvdInfoPool

/* Global RB Trees*/
#define ECFM_CC_GLOBAL_OFFLOAD_RMEP_TABLE   gEcfmCcGlobalInfo.GlobalOffloadRMepDbTbl
#define ECFM_CC_GLOBAL_OFFLOAD_MEP_TABLE    gEcfmCcGlobalInfo.GlobalOffloadMepTbl
#define ECFM_CC_GLOBAL_PORT_TABLE           gEcfmCcGlobalInfo.GlobalPortTable
#define ECFM_CC_GLOBAL_LOCAL_PORT_TABLE     gEcfmCcGlobalInfo.GlobalCcLocalPortTable
#define ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE   gEcfmCcGlobalInfo.GlobalLbltLocalPortTable
#define ECFM_CC_GLOBAL_STACK_TABLE          gEcfmCcGlobalInfo.GlobalStackTable
#define ECFM_CC_GLOBAL_CONFIG_ERR_TABLE     gEcfmCcGlobalInfo.GlobalConfigErrTable
#define ECFM_CC_GLOBAL_MIP_TABLE            gEcfmCcGlobalInfo.GlobalMipTable
#define ECFM_CC_GLOBAL_MIP_PREVENT_TABLE    gEcfmCcGlobalInfo.GlobalMipPreventTbl
#define ECFM_CC_GLOBAL_HW_MEP_TABLE         gEcfmCcGlobalInfo.GlobalHwMepTbl
#define ECFM_CC_GLOBAL_HW_RMEP_TABLE        gEcfmCcGlobalInfo.GlobalHwRMepTbl

/* ECFM registration CC Specific Information */
                                                                             
#define ECFM_APP_REG_INFO_SIZE              (sizeof(tEcfmRegParams))
#define ECFM_CC_GET_APP_INFO(u4ModId)       gEcfmCcGlobalInfo.apAppRegParams[u4ModId]


#define ECFM_CC_MA_MEP_LIST_TABLE_POOL\
        gEcfmCcGlobalInfo.MaMepListTableMemPool
#define ECFM_CC_STACK_TABLE_POOL\
        gEcfmCcGlobalInfo.StackTableMemPool
#define ECFM_CC_MIP_TABLE_POOL              gEcfmCcGlobalInfo.MipTableMemPool
#define ECFM_CC_MIP_PREVENT_TABLE_POOL    gEcfmCcGlobalInfo.MipPreventTblMemPool
#define ECFM_CC_CONF_ERR_LIST_TABLE_POOL\
        gEcfmCcGlobalInfo.ConfigErrListTableMemPool
#define ECFM_CC_VLAN_TABLE_POOL\
        gEcfmCcGlobalInfo.VlanTableMemPool
#define ECFM_CC_CONTEXT_POOL                gEcfmCcGlobalInfo.ContextMemPool
#define ECFM_CC_HW_PORTLIST_ARRAY           gEcfmCcGlobalInfo.HwPortListPool
#define ECFM_CC_AIS_PDU_POOL                gEcfmCcGlobalInfo.AisPduPool
#define ECFM_CC_LCK_PDU_POOL                gEcfmCcGlobalInfo.LckPduPool
#define ECFM_AVLBLTY_INFO_POOLID            gEcfmCcGlobalInfo.AvlbltyPool

/*Macros for Context Specific Information*/
#define ECFM_CC_PORT_MEP_TABLE\
    (ECFM_CC_CURR_CONTEXT_INFO())->MepPortTable

#define ECFM_CC_ERR_LOG_POOL\
        (ECFM_CC_CURR_CONTEXT_INFO())->ErrLogPool

#define ECFM_CC_FRM_LOSS_POOL\
        (ECFM_CC_CURR_CONTEXT_INFO())->FrmLossPool


#define ECFM_CC_DEF_MD_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->DefaultMdTable

#define ECFM_CC_MD_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->MdTableIndex

#define ECFM_CC_MPLSMEP_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->MplsPathTree

#define ECFM_CC_MA_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->MaTableIndex

#define ECFM_CC_MEP_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->MepTableIndex

#define ECFM_CC_RMEP_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->RMepDbTable

#define ECFM_CC_MA_MEP_LIST_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->MaMepListTable

#define ECFM_CC_VLAN_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->VlanTable

#define ECFM_CC_PRIMARY_VLAN_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->PrimaryVlanTable

#define ECFM_CC_MIP_CCM_DB_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->MipCcmDbTable

#define ECFM_CC_ERR_LOG_TABLE\
        (ECFM_CC_CURR_CONTEXT_INFO())->ErrLogTable

#define ECFM_CC_ERR_LOG_STATUS\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1ErrLogStatus
#define ECFM_CC_ERR_LOG_SIZE\
        (ECFM_CC_CURR_CONTEXT_INFO())->u2ErrLogSize


#define ECFM_CC_MIP_CCM_DB_STATUS\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1MipCcmDbStatus
#define ECFM_CC_MIP_CCM_DB_HOLD_TIME\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1MipCcmDbHoldTime
#define ECFM_CC_MIP_CCM_DB_SIZE\
        (ECFM_CC_CURR_CONTEXT_INFO())->u2MipCcmDbHoldSize
        
#define ECFM_CC_ERR_LOG_SEQ_NUM\
        (ECFM_CC_CURR_CONTEXT_INFO())->u4ErrorLogSeqNum

#define ECFM_CC_GET_PORT_INFO(u2PortNum)\
        (((u2PortNum<=0)||((UINT2)u2PortNum>ECFM_CC_MAX_PORT_INFO))?NULL:ECFM_CC_PORT_INFO((UINT2)u2PortNum))

#define ECFM_CC_TMR_MIP_DB_HOLD_TMR\
        (ECFM_CC_CURR_CONTEXT_INFO())->MipDbHoldTimer
        
#define ECFM_CC_DEF_MD_DEFAULT_LEVEL\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1DefaultMdDefLevel

#define ECFM_CC_DEF_MD_DEFAULT_MHF_CREATION\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1DefaultMdDefMhfCreation

#define ECFM_CC_DEF_MD_DEFAULT_SENDER_ID_PERMISSION\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1DefaultMdDefIdPermission

#define ECFM_CC_MIP_DB_TABLE_POOL  (ECFM_CC_CURR_CONTEXT_INFO())->MipCcmDbInfoPool
#define ECFM_CC_IS_PORT_MODULE_STS_ENABLED(u2PortNum)\
        ((ECFM_CC_GET_PORT_INFO(u2PortNum)!=NULL)?(ECFM_CC_PORT_INFO(u2PortNum)->u1PortEcfmStatus==ECFM_ENABLE):0) 
#define ECFM_CC_IS_PORT_MODULE_STS_DISABLED(u2PortNum)\
        (!(ECFM_CC_IS_PORT_MODULE_STS_ENABLED(u2PortNum)))
#define ECFM_CC_IS_Y1731_ENABLED_ON_PORT(u2PortNum)\
        (((ECFM_CC_GET_PORT_INFO(u2PortNum))!=NULL)?(ECFM_CC_PORT_INFO(u2PortNum)->u1PortY1731Status==ECFM_ENABLE):0)
#define ECFM_CC_IS_Y1731_DISABLED_ON_PORT(u2PortNum)\
        (!(ECFM_CC_IS_Y1731_ENABLED_ON_PORT(u2PortNum)))

/* Loss Measurement related Macros */ 
#define ECFM_CC_LM_BUFFER_POOL \
 (( ECFM_CC_CURR_CONTEXT_INFO())->FrmLossBuffMemPool)

#define ECFM_CC_FL_BUFFER_TABLE \
 (( ECFM_CC_CURR_CONTEXT_INFO())->FrmLossBuffer)

#define ECFM_CC_FL_BUFFER_SIZE \
    ((ECFM_CC_CURR_CONTEXT_INFO ())->u2FrmLossBuffSize)/* To get the Timer value in millisecond on the bases of the LMM Interval 
  value present in the Mep Info*/

#define ECFM_GET_LMM_INTERVAL(u1LmmIntervalCode,u4LmmInterval)\
do \
{\
 if ((u1LmmIntervalCode >= ECFM_MIN_LMM_INTERVALS) && \
     (u1LmmIntervalCode < ECFM_MAX_LMM_INTERVALS)) \
 {\
     u4LmmInterval = au4LmmTimer [u1LmmIntervalCode];\
 }\
 else\
 {\
    u4LmmInterval = ECFM_INIT_VAL;\
 }\
}while (0)

/* To get the Availability Measurement interval */
#define ECFM_GET_AVLBLTY_INTERVAL(u1AvlbltyIntervalCode,u4Interval)\
do \
{\
 if ((u1AvlbltyIntervalCode >= ECFM_MIN_AVLBLTY_INTERVALS) && \
     (u1AvlbltyIntervalCode < ECFM_MAX_AVLBLTY_INTERVALS)) \
 {\
     u4Interval = au4AvlbltyTimer[u1AvlbltyIntervalCode];\
 }\
 else\
 {\
    u4Interval = ECFM_INIT_VAL;\
 }\
}while (0)

#define ECFM_CC_SET_PORTID_SUBTYPE(u2PortNum, u1SubType)\
do {\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum)!=NULL)\
   {\
      (ECFM_CC_PORT_INFO(u2PortNum)->u1PortIdSubType)=u1SubType;\
   }\
}while (0)

#define ECFM_CC_PORTID(u2PortNum)\
    (ECFM_CC_GET_PORT_INFO(u2PortNum)!=NULL?(ECFM_CC_PORT_INFO(u2PortNum)->au1PortId):NULL)
/*CC Error Log related macros */
#define ECFM_CC_IS_ERR_LOG_ENABLED()\
        ((ECFM_CC_ERR_LOG_STATUS == ECFM_ENABLE)?(ECFM_TRUE):(ECFM_FALSE))
#define ECFM_CC_IS_ERR_LOG_DISABLED()\
        (!ECFM_CC_IS_ERR_LOG_ENABLED())
#define ECFM_IS_CC_ERR_LOG_EMPTY()\
       ((RBTreeGetFirst (ECFM_CC_ERR_LOG_TABLE))?(ECFM_FALSE):(ECFM_TRUE))



/* MipCcmDb related macros */
#define ECFM_IS_MIP_CCM_DB_ENABLED()\
        ((ECFM_CC_MIP_CCM_DB_STATUS == ECFM_ENABLE)?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_MIP_CCM_DB_DISABLED()\
        ((ECFM_CC_MIP_CCM_DB_STATUS) != ECFM_ENABLE)

#define ECFM_IS_MIP_CCM_DB_EMPTY()\
       ((RBTreeGetFirst (ECFM_CC_MIP_CCM_DB_TABLE))?(ECFM_FALSE):(ECFM_TRUE))

#define ECFM_CC_MIP_DYNAMIC_EVALUATION_STATUS\
        (ECFM_CC_CURR_CONTEXT_INFO())->b1MipDynamicEvaluationStatus


/* CC Specific Get and Set Macros*/
#define ECFM_CC_IS_RDI_ENABLED(pMepInfo)\
        (pMepInfo->CcInfo.u1RdiCapability==ECFM_ENABLE)
#define ECFM_CC_IS_RDI_DISABLED(pMepInfo)\
        (!ECFM_CC_IS_RDI_ENABLED(pMepInfo))

/*macros to set RDI Capability enable/disable*/        
#define ECFM_CC_SET_RDI_ENABLE(pMepInfo)\
        (pMepInfo->CcInfo.u1RdiCapability=ECFM_ENABLE)
        
#define ECFM_CC_SET_RDI_DISABLE(pMepInfo)\
        (pMepInfo->CcInfo.u1RdiCapability=ECFM_DISABLE)

/*Macros for back pointer */
#define ECFM_CC_GET_MDINFO_FROM_MA(pCcMaInfo)\
        (pCcMaInfo->pMdInfo)

#define ECFM_CC_GET_MDINFO_FROM_MEP(pCcMepInfo)\
        (pCcMepInfo->pMaInfo->pMdInfo)

#define ECFM_CC_GET_MAINFO_FROM_MEP(pCcMepInfo)\
        (pCcMepInfo->pMaInfo)

#define ECFM_CC_GET_MAINFO_FROM_MEPLIST(pCcMepListInfo)\
        (pCcMepListInfo->pMaInfo)

#define ECFM_CC_GET_PORTINFO_FROM_MEP(pCcMepInfo)\
        (pCcMepInfo->pPortInfo)

#define ECFM_CC_GET_LMINFO_FROM_MEP(pCcMepInfo)\
       (&pCcMepInfo->LmInfo)        

#define ECFM_CC_GET_MEP_FROM_PDUSM(pCcPduSmInfo)\
        (pCcPduSmInfo->pMepInfo)

#define ECFM_CC_GET_MIP_FROM_PDUSM(pCcPduSmInfo)\
        (pCcPduSmInfo->pMipInfo)

#define ECFM_CC_GET_MHF_FROM_PDUSM(pCcPduSmInfo)\
        (pCcPduSmInfo->pMhfInfo)

#define ECFM_CC_GET_RMEP_FROM_PDUSM(pCcPduSmInfo)\
        (pCcPduSmInfo->pRMepInfo)

#define ECFM_CC_GET_CCINFO_FROM_MEP(pCcMepInfo)\
        (&pCcMepInfo->CcInfo)

#define ECFM_CC_GET_AISINFO_FROM_MEP(pCcMepInfo)\
        (&pCcMepInfo->AisInfo)
#define ECFM_CC_GET_LCKINFO_FROM_MEP(pCcMepInfo)\
        (&pCcMepInfo->LckInfo)

#define ECFM_CC_GET_FNGINFO_FROM_MEP(pCcMepInfo)\
        (&pCcMepInfo->FngInfo)

#define ECFM_CC_GET_AVLBLTYINFO_FROM_MEP(pCcMepInfo)\
       (pCcMepInfo->pAvlbltyInfo)        

/*Macro to get the parased information in the received CCM */
#define ECFM_CC_GET_CCM_FROM_PDUSM(pCcPduSmInfo)\
        (&pCcPduSmInfo->uPduInfo.Ccm)

/*Macor to set the OUI in the octet stream provided*/
#define ECFM_CC_SET_OUI(pu1OctetStream)\
do\
{\
  pu1OctetStream[0]=(ECFM_CC_ORG_UNIT_ID[0]);\
  pu1OctetStream[1]=(ECFM_CC_ORG_UNIT_ID[1]);\
  pu1OctetStream[2]=(ECFM_CC_ORG_UNIT_ID[2]);\
  pu1OctetStream+=(ECFM_OUI_FIELD_SIZE);\
}\
while (0)

/*Folowing macro is used to set the LLC SNAP header in the CFM frame*/
#define ECFM_CC_SET_LLC_SNAP_HDR(pu1Pdu, u2Length)\
do\
{\
 ECFM_PUT_2BYTE(pu1Pdu,u2Length);\
 ECFM_PUT_1BYTE(pu1Pdu,ECFM_DST_LSAP_IN_LLC_SNAP_HDR);\
 ECFM_PUT_1BYTE(pu1Pdu,ECFM_SRC_LSAP_IN_LLC_SNAP_HDR);\
 ECFM_PUT_1BYTE(pu1Pdu,ECFM_CONTROL_BYTE_IN_LLC_SNAP_HDR);\
 *(pu1Pdu++)=(ECFM_CC_ORG_UNIT_ID[0]);\
 *(pu1Pdu++)=(ECFM_CC_ORG_UNIT_ID[1]);\
 *(pu1Pdu++)=(ECFM_CC_ORG_UNIT_ID[2]);\
 ECFM_PUT_2BYTE(pu1Pdu,ECFM_LLC_CFM_TYPE);\
}\
while(0)

/* EcfmCcUtilGetMepSenderIdPerm - used to get the sender-id permission from
 * MD/MA.
 * EcfmUtilSetSenderIdTlv - Sets the sender-id according to the sender-id
 * permission received from EcfmCcUtilGetMepSenderIdPerm.
 * */
#define ECFM_CC_SET_SENDER_ID_TLV(pCcMepInfo,pu1OctetStream)\
   EcfmUtilSetSenderIdTlv(EcfmCcUtilGetMepSenderIdPerm(pCcMepInfo),&pu1OctetStream)

/*Macro to Set CFM LEAK error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_SET_CONFIG_ERROR_CFM_LEAK(pConfigErrInfo)\
   ECFM_SET_U1BIT(pConfigErrInfo->u1ErrorType,7)

/*Macro to Set CONFLICTING VIDS error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_SET_CONFIG_ERROR_CONFLICT_VIDS(pConfigErrInfo)\
   ECFM_SET_U1BIT(pConfigErrInfo->u1ErrorType,6)

/*Macro to Set EXCESSIVE LEVELS error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_SET_CONFIG_ERROR_EXCESS_LEVELS(pConfigErrInfo)\
   ECFM_SET_U1BIT(pConfigErrInfo->u1ErrorType,5)

/*Macro to Set OVERLAPPED LEVELS error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_SET_CONFIG_ERROR_OVERLAPPED_LEVELS(pConfigErrInfo)\
   ECFM_SET_U1BIT(pConfigErrInfo->u1ErrorType,4)


/*Macro to Clear CFM LEAK error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_CLEAR_CONFIG_ERROR_CFM_LEAK(pConfigErrInfo)\
   ECFM_CLEAR_U1BIT(pConfigErrInfo->u1ErrorType,7)

/*Macro to clear CONFLICTING VIDS error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_CLEAR_CONFIG_ERROR_CONFLICT_VIDS(pConfigErrInfo)\
   ECFM_CLEAR_U1BIT(pConfigErrInfo->u1ErrorType,6)

/*Macro to clear EXCESSIVE LEVELS error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_CLEAR_CONFIG_ERROR_EXCESS_LEVELS(pConfigErrInfo)\
   ECFM_CLEAR_U1BIT(pConfigErrInfo->u1ErrorType,5)

/*Macro to clear OVERLAPPED LEVELS error in in dot1agCfmConfigErrorListType*/
#define ECFM_CC_CLEAR_CONFIG_ERROR_OVERLAPPED_LEVELS(pConfigErrInfo)\
   ECFM_CLEAR_U1BIT(pConfigErrInfo->u1ErrorType,4)



/*Macro to Set CCM Interval bits in the Flags of CFM-HDR*/
#define ECFM_CC_SET_CCM_INTERVAL(u1Flags,u1CcmInterval)\
        (u1Flags=u1Flags|(u1CcmInterval&0x07))

/*Macro to Get RDI from the Flags of CFM-HDR*/
#define ECFM_CC_GET_RDI(u1Flags) (u1Flags>>7)

/*Macro to Get CCM Interval  from the Flags of CFM-HDR*/
#define ECFM_CC_GET_CCM_INTERVAL(u1Flags) (u1Flags&0x07)

/* Macro to get the Defect Priority according to the defect*/
#define ECFM_CC_GET_DEFECT_PRIORITY(u1Defect) (u1Defect)
/*Macro to Set Check if MEP is in Active*/
#define ECFM_CC_IS_MEP_ACTIVE(pCcMepInfo)\
        ((pCcMepInfo->b1Active == ECFM_TRUE) && (pCcMepInfo->u1RowStatus == ECFM_ROW_STATUS_ACTIVE))
/*Macro to increment dot1agCfmMepCciSentCcms*/

#define ECFM_CC_INCR_CCM_SEQ_NUMBER(pCcInfo)\
        (ECFM_INCR((pCcInfo->u4CciSentCcms),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/*Macro to increment dot1agCfmMepCcmSequenceErrors*/
#define ECFM_CC_INCR_CCM_SEQ_ERRORS(pCcInfo)\
        (ECFM_INCR((pCcInfo->u4CcmSeqErrors),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/* Macros for statemachines running in context of CC Task*/

/* Continuity Check Inititator State MAchine*/
#define ECFM_CC_CCI_STATE_MACHINE(u1Event,u1State,pParam)\
        (((u1Event<ECFM_SM_CCI_MAX_EVENTS)&& (u1State <ECFM_CCI_MAX_STATES))?\
         (*gaEcfmCCActionProc[gau1EcfmCCmSem[u1Event][u1State]])(pParam):ECFM_FAILURE)

/* MHF Continuity Check Receiver State MAchine*/
#define ECFM_CC_MHF_CCR_STATE_MACHINE(u1Event,u1State,pParam)

/* Remote MEP State MAchine*/
#define ECFM_CC_RMEP_STATE_MACHINE(u1Event,u1State,pParam)\
        (((u1Event<ECFM_SM_RMEP_MAX_EVENTS)&& (u1State <ECFM_RMEP_MAX_STATES))?\
        (*gaEcfmRmepActionProc[gau1EcfmRmepSem[u1Event][u1State]])(pParam):ECFM_FAILURE)

/* Remote MEP Error State MAchine*/
#define ECFM_CC_RMEP_ERR_STATE_MACHINE(u1Event,u1State,pParam)\
        (((u1Event<ECFM_SM_RMEP_ERR_MAX_EVENTS)&& (u1State <ECFM_RMEP_ERR_MAX_STATES))?\
        (*gaEcfmRmepErrActionProc[gau1EcfmRmepErrSem[u1Event][u1State]])(pParam):ECFM_FAILURE)

/* MEP Cross Connect State MAchine*/
#define ECFM_CC_MEP_XCON_STATE_MACHINE(u1Event,u1State,pParam)\
        (((u1Event<ECFM_SM_XCON_MAX_EVENTS)&& (u1State <ECFM_MEP_XCON_MAX_STATES))?\
        (*gaEcfmLmepXconActionProc[gau1EcfmLmepXconSem[u1Event][u1State]])(pParam):ECFM_FAILURE)

/* Fault Notificatio State MAchine*/
#define ECFM_CC_FNG_STATE_MACHINE(u1Event,u1State,pParam)\
        if((u1Event<ECFM_SM_FNG_MAX_EVENTS)&& (u1State <ECFM_MEP_FNG_MAX_STATES))\
        (*gaEcfmFngActionProc[gau1EcfmFngSem[u1Event][u1State]])(pParam)

/* Macros for setting for states for various statemachines running in context of 
 * CC Task
 */
#define ECFM_CC_CCI_SET_STATE(pMepInfo,u1State)\
        (pMepInfo->CcInfo.u1CciState = u1State)
#define ECFM_CC_RMEP_SET_STATE(pRMepInfo,u1RMepState)\
        (pRMepInfo->u1State = u1RMepState)

#define ECFM_CC_RMEP_ERR_SET_STATE(pMepInfo,u1State)\
        (pMepInfo->CcInfo.u1RMepErrState = u1State)

#define ECFM_CC_MEP_XCON_SET_STATE(pMepInfo,u1State)\
        (pMepInfo->CcInfo.u1MepXconState = u1State)

#define ECFM_CC_FNG_SET_STATE(pMepInfo,u1State)\
        (pMepInfo->FngInfo.u1FngState = u1State)

/* Macros for getting for states for various statemachines running in context of 
 * CC Task
 */
#define ECFM_CC_CCI_GET_STATE(pMepInfo)\
        (pMepInfo->CcInfo.u1CciState)
#define ECFM_CC_RMEP_GET_STATE(pRMepInfo)\
        (pRMepInfo->u1State)

#define ECFM_CC_RMEP_ERR_GET_STATE(pMepInfo)\
        (pMepInfo->CcInfo.u1RMepErrState)

#define ECFM_CC_MEP_XCON_GET_STATE(pMepInfo)\
        (pMepInfo->CcInfo.u1MepXconState)

#define ECFM_CC_FNG_GET_STATE(pMepInfo)\
        (pMepInfo->FngInfo.u1FngState)
/*Macro to Check if MIP is Active*/
#define ECFM_CC_IS_MIP_ACTIVE(pMipInfo)\
        ((pMipInfo->b1Active == ECFM_TRUE)&&(pMipInfo->u1RowStatus == ECFM_ROW_STATUS_ACTIVE))
#define ECFM_CC_IS_MEP(pStackInfo) (pStackInfo->pMepInfo!=NULL)
#define ECFM_CC_IS_MHF(pStackInfo) (pStackInfo->pMipInfo!=NULL)

/* Macro to convert FNG While timer from 10ms Ticks to Msec*/
#define ECFM_CC_CONVERT_FNG_TIMER_TO_MSEC(u2FngTimer)\
 (u2FngTimer*10)

/* Currently component Id internally taken as ContextId and also these table entries
 * Can be created only for DEFAULT context ONLY. So always component id value will be ONE */
#define ECFM_CONVERT_CTXT_ID_TO_COMP_ID(u4ContextId) \
  u4ContextId = (u4ContextId + 1)

/* Global Statistics */
#define ECFM_CC_INCR_MEMORY_FAILURE_COUNT()gEcfmCcGlobalInfo.u4MemoryFailureCount++
#define ECFM_CC_INCR_BUFFER_FAILURE_COUNT()gEcfmCcGlobalInfo.u4BufferFailureCount++

/* Per Context Statistics */
#define ECFM_CC_INCR_CFM_UP_COUNT()gpEcfmCcContextInfo->u4EcfmUpCount++
#define ECFM_CC_INCR_CFM_DOWN_COUNT()gpEcfmCcContextInfo->u4EcfmDownCount++
#define ECFM_CC_INCR_NO_DEFECT_COUNT()gpEcfmCcContextInfo->u4NoDefectCount++
#define ECFM_CC_INCR_RDI_DEFECT_COUNT()gpEcfmCcContextInfo->u4RdiDefectCount++
#define ECFM_CC_INCR_MAC_STATUS_DEFECT_COUNT()gpEcfmCcContextInfo->u4MacStatusDefectCount++
#define ECFM_CC_INCR_REMOTE_CCM_DEFECT_COUNT()gpEcfmCcContextInfo->u4RemoteCcmDefectCount++
#define ECFM_CC_INCR_ERROR_CCM_DEFECT_COUNT()gpEcfmCcContextInfo->u4ErrorCcmDefectCount++
#define ECFM_CC_INCR_XCONN_DEFECT_COUNT()gpEcfmCcContextInfo->u4XconnDefectCount++

/* Per Port Statistics */
#define ECFM_CC_INCR_TX_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_CC_PORT_INFO(u2PortNum)->u4TxCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_TX_COUNT(u2PortNum,u1OpCode)\
do\
{\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_CCM:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4TxCcmCount++;\
           break;\
       case ECFM_OPCODE_AIS:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4TxAisCount++;\
           break;\
       case ECFM_OPCODE_LCK:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4TxLckCount++;\
           break;\
       case ECFM_OPCODE_LMM:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4TxLmmPduCount++;\
           break;\
       case ECFM_OPCODE_LMR:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4TxLmrPduCount++;\
           break;\
       default:\
           break;\
   }\
  }\
}while(0)
           
#define ECFM_CC_INCR_TX_FAILED_COUNT(u2PortNum)\
do {\
      if (ECFM_CC_GET_PORT_INFO(u2PortNum)!=NULL)\
      {\
         ECFM_CC_PORT_INFO(u2PortNum)->u4TxFailedCount++;\
      }\
}while (0)

#define ECFM_CC_INCR_RX_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_CC_PORT_INFO(u2PortNum)->u4RxCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_RX_COUNT(u2PortNum,u1OpCode)\
do\
{\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_CCM:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4RxCcmCount++;\
       break;\
       case ECFM_OPCODE_AIS:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4RxAisCount++;\
           break;\
       case ECFM_OPCODE_LCK:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4RxLckCount++;\
           break;\
       case ECFM_OPCODE_LMM:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4RxLmmPduCount++;\
           break;\
       case ECFM_OPCODE_LMR:\
           ECFM_CC_PORT_INFO(u2PortNum)->u4RxLmrPduCount++;\
           break;\
       default:\
       break;\
   }\
   }\
}while(0)
#define ECFM_CC_INCR_RX_BAD_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_CC_PORT_INFO(u2PortNum)->u4RxBadCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_FRWD_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_CC_PORT_INFO(u2PortNum)->u4FrwdCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_DSRD_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_CC_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_CC_PORT_INFO(u2PortNum)->u4DsrdCfmPduCount++;\
   }\
}while (0)

/* Per Context Statistics */
#define ECFM_CC_INCR_CTX_TX_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_CTX_TX_COUNT(u4CtxId,u1OpCode)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_CCM:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxCcmCount++;\
           break;\
       case ECFM_OPCODE_AIS:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxAisCount++;\
           break;\
       case ECFM_OPCODE_LCK:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxLckCount++;\
           break;\
       case ECFM_OPCODE_LMM:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxLmmPduCount++;\
           break;\
       case ECFM_OPCODE_LMR:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxLmrPduCount++;\
           break;\
       default:\
           break;\
   }\
   }\
}while(0)
           
#define ECFM_CC_INCR_CTX_TX_FAILED_COUNT(u4CtxId)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
         ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4TxFailedCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_CTX_RX_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_CTX_RX_COUNT(u4CtxId,u1OpCode)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_CCM:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxCcmCount++;\
       break;\
       case ECFM_OPCODE_AIS:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxAisCount++;\
           break;\
       case ECFM_OPCODE_LCK:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxLckCount++;\
           break;\
       case ECFM_OPCODE_LMM:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxLmmPduCount++;\
           break;\
       case ECFM_OPCODE_LMR:\
           ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxLmrPduCount++;\
           break;\
       default:\
           break;\
   }\
   }\
}while(0)
#define ECFM_CC_INCR_CTX_RX_BAD_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4RxBadCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_CTX_FRWD_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4FrwdCfmPduCount++;\
   }\
}while (0)

#define ECFM_CC_INCR_CTX_DSRD_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_CC_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_CC_GET_CONTEXT_INFO(u4CtxId)->CcStats.u4DsrdCfmPduCount++;\
   }\
}while (0)
           
/* Loss Measurement Related Macros*/
#define ECFM_CC_INCR_PM_TRANS_ID(pMeNode) (pMeNode->u4TransId++)
#define ECFM_CC_RESET_PM_SEQ_NUM(pMeNode) (pMeNode->u4SeqNum=0)
#define ECFM_CC_INCR_LM_TRANS_ID(pMepInfo) (pMepInfo->pMaInfo->u4TransId++)
#define ECFM_CC_RESET_LM_SEQ_NUM(pMepInfo)(pMepInfo->pMaInfo->u4SeqNum=0)
#define ECFM_CC_INCR_LM_SEQ_NUM(pMepInfo)pMepInfo->pMaInfo->u4SeqNum++
#define ECFM_CC_INCR_LMR_RCVD_COUNT(pMepInfo) (pMepInfo->LmInfo.u2NoOfLmrIn++)

/* Extra Deadline status*/
#define ECFM_SET_EXTRA_DEADLINE_STATUS(u4Val)ECFM_SET_U4BIT(u4Val,31)
#define ECFM_GET_EXTRA_DEADLINE_STATUS(u4Val)ECFM_GET_U4BIT(u4Val,31)
#define ECFM_CLEAR_EXTRA_DEADLINE_STATUS(u4Val)ECFM_CLEAR_U4BIT(u4Val,31)
#define ECFM_MASK_EXTRA_DEADLINE_STATUS(u4Val) u4Val&~0x80000000

/* Set Infinite Status */
#define ECFM_SET_INFINITE_TX_STATUS(u2Val)ECFM_SET_U2BIT(u2Val,15)
#define ECFM_GET_INFINITE_TX_STATUS(u2Val)ECFM_GET_U2BIT(u2Val,15)
#define ECFM_CLEAR_INFINITE_TX_STATUS(u2Val)ECFM_CLEAR_U2BIT(u2Val,15)
#define ECFM_MASK_INFINITE_TX_STATUS(u2Val) u2Val&~0x8000
#define ECFM_CC_DECR_TRANSMIT_LM_MSG(pMepInfo)\
            ((pMepInfo->LmInfo.u2TxLmmMessages)? \
             (pMepInfo->LmInfo.u2TxLmmMessages--):(0))
#define ECFM_CC_MAX_VAL(a,b) (a>b?a:b)
/* AIS Capability*/
#define ECFM_CC_IS_AIS_ENABLED(pCcMepInfo)\
        ((pCcMepInfo->AisInfo.u1AisCapability == ECFM_ENABLE)? (ECFM_TRUE):(ECFM_FALSE))
#define ECFM_CC_IS_AIS_DISABLED(pCcMepInfo)\
        (!ECFM_CC_IS_AIS_ENABLED(pCcMepInfo))

/* Macro to be called when AIS Frames to be initiated on occurence 
 * of Defect */ 
/*Note: AIS is specific funtionality of Y.1731*/
#define ECFM_CC_AIS_TRIGGER_START(pMepInfo)\
do{\
    if(ECFM_CC_IS_Y1731_ENABLED_ON_PORT(pMepInfo->u2PortNum))\
    {\
        EcfmCcClntAisInitiator(pMepInfo,ECFM_AIS_START_TRANSACTION);\
    }\
}while(0)

#define ECFM_CC_AIS_TRIGGER_STOP(pMepInfo)\
do{\
    if(ECFM_CC_IS_Y1731_ENABLED_ON_PORT(pMepInfo->u2PortNum))\
    {\
        EcfmCcClntAisInitiator(pMepInfo,ECFM_AIS_STOP_TRANSACTION);\
    }\
}while(0)

#define ECFM_CC_IS_AIS_CONDITION_TRUE(pMepInfo)(pMepInfo->AisInfo.b1AisCondition==ECFM_TRUE)
 
/* Macro to be called when LCK Frames to be initiated on occurence of 
 * Out of Service */
/* LCK specific macros*/
#define ECFM_CC_LCK_IS_CONFIGURED(pMepInfo) \
 ((pMepInfo->LckInfo.b1LckDelayExp)? ECFM_TRUE:ECFM_FALSE)
#define ECFM_CC_IS_LCK_CONDITION_TRUE(pMepInfo)(pMepInfo->LckInfo.b1LckCondition==ECFM_TRUE)

/* Macro for generating trap*/
#define ECFM_CC_GENERATE_TRAP(pPduSmInfo,u1TrapId)\
do\
{\
   if (EcfmVcmGetSystemModeExt (ECFM_PROTOCOL_ID) == VCM_SI_MODE)\
   {\
       EcfmSnmpIfSendTrap(pPduSmInfo,u1TrapId);\
   }\
   else\
   {\
       EcfmMISnmpIfSendTrap(pPduSmInfo,u1TrapId);\
   }\
}while(0)
#define Y1731_CC_GENERATE_TRAP(pCcErrLog,u1TrapId)\
        Y1731CcSnmpIfSendTrap(pCcErrLog,u1TrapId)
#define ECFM_CC_HANDLE_TRANSMIT_FAILURE(u2PortNum,u1OpCode)\
        EcfmLbLtSetTransmitFailOpcode (ECFM_CC_CURR_CONTEXT_ID(),u2PortNum,u1OpCode);

#define ECFM_CC_UNAWARE_MEP_PRESENT(u2PortNum)\
   ((ECFM_CC_GET_PORT_INFO(u2PortNum))!=NULL?ECFM_CC_PORT_INFO(u2PortNum)->b1UnawareMepPresent:0)

#define ECFM_CC_802_1AH_BRIDGE()\
   (((ECFM_CC_GET_BRIDGE_MODE()==ECFM_PBB_ICOMPONENT_BRIDGE_MODE)||\
     (ECFM_CC_GET_BRIDGE_MODE()==ECFM_PBB_BCOMPONENT_BRIDGE_MODE))?ECFM_TRUE:ECFM_FALSE)

#define ECFM_CC_802_1AD_BRIDGE() \
   (((ECFM_CC_GET_BRIDGE_MODE()==ECFM_PROVIDER_CORE_BRIDGE_MODE)||\
     (ECFM_CC_GET_BRIDGE_MODE()==ECFM_PROVIDER_EDGE_BRIDGE_MODE ))?ECFM_TRUE:ECFM_FALSE)

#define ECFM_CC_GET_PORT_TYPE(u2port) \
        ((ECFM_CC_GET_PORT_INFO(u2port))!=NULL?ECFM_CC_PORT_INFO(u2port)->u1PortType:ECFM_INVALID_PROVIDER_PORT)
#define ECFM_CC_GET_BRIDGE_MODE()     (ECFM_CC_CURR_CONTEXT_INFO ())->u4BridgeMode
   
#define ECFM_BRIDGE_MODE EcfmL2IwfGetBridgeMode

#define ECFM_CC_ISPORT_SISP_LOG(u2Port,pTempPortInfo) \
  (((pTempPortInfo = (ECFM_CC_GET_PORT_INFO(u2Port))) == NULL)?ECFM_FALSE:\
   ((ECFM_MIN_SISP_INDEX <= pTempPortInfo->u4IfIndex)&&\
    (pTempPortInfo->u4IfIndex <= ECFM_MAX_SISP_INDEX)?\
     ECFM_TRUE:ECFM_FALSE))

#define ECFM_CC_GET_PHY_PORT(u2Port,pTempPortInfo) \
   (((pTempPortInfo = (ECFM_CC_GET_PORT_INFO(u2Port))) == NULL)?0:\
    (((ECFM_MIN_SISP_INDEX <= pTempPortInfo->u4IfIndex)&& \
      (ECFM_MAX_SISP_INDEX >= pTempPortInfo->u4IfIndex))?\
       pTempPortInfo->u4PhyPortNum:\
       pTempPortInfo->u4IfIndex))\

  
/*****************************************************************************/
/*                         MACROS FOR LBLT TASK                              */
/*****************************************************************************/
#define ECFM_LBLT_MSG_INFO_SIZE             (sizeof(tEcfmLbLtMsg))
#define ECFM_LBLT_PORT_INFO_SIZE            (sizeof(tEcfmLbLtPortInfo))
#define ECFM_LBLT_MEP_INFO_SIZE             (sizeof(tEcfmLbLtMepInfo))
#define ECFM_LBLT_MIP_INFO_SIZE             (sizeof(tEcfmLbLtMipInfo))
#define ECFM_LBLT_STACK_INFO_SIZE           (sizeof (tEcfmLbLtStackInfo))
#define ECFM_LBLT_LTR_INFO_SIZE             (sizeof(tEcfmLbLtLtrInfo))
#define ECFM_LBLT_LTM_REPLY_LIST_INFO_SIZE  (sizeof(tEcfmLbLtLtmReplyListInfo))
#define ECFM_LBLT_GLOBAL_INFO_SIZE          (sizeof(tEcfmLbLtGlobalInfo))
#define ECFM_LBLT_CONTEXT_INFO_SIZE         (sizeof(tEcfmLbLtContextInfo))
#define ECFM_LBLT_PDUSM_INFO_SIZE           (sizeof(tEcfmLbLtPduSmInfo))
#define ECFM_LBLT_LBM_INFO_SIZE             (sizeof(tEcfmLbLtLbmInfo))
#define ECFM_LBLT_LBR_INFO_SIZE             (sizeof(tEcfmLbLtLbrInfo))
#define ECFM_LBLT_FRM_DELAY_INFO_SIZE       (sizeof(tEcfmLbLtFrmDelayBuff))
#define ECFM_LBLT_RMEP_DB_INFO_SIZE         (sizeof(tEcfmLbLtRMepDbInfo))
#define ECFM_LBLT_MIP_CCM_DB_INFO_SIZE      (sizeof(tEcfmLbLtMipCcmDbInfo))
#define ECFM_LBLT_MA_INFO_SIZE              (sizeof(tEcfmLbLtMaInfo))
#define ECFM_LBLT_MD_INFO_SIZE              (sizeof(tEcfmLbLtMdInfo))
#define ECFM_LBLT_DELAY_Q_NODE_SIZE         (sizeof(tEcfmLbLtDelayQueueNode))
#define ECFM_LBLT_DEF_MD_INFO_SIZE          (sizeof(tEcfmLbLtDefaultMdTableInfo))

/*Organization Unique Identifier for LBLT Task*/
#define ECFM_LBLT_ORG_UNIT_ID         gEcfmLbLtGlobalInfo.au1OUI
#define ECFM_LBLT_PDU      gEcfmLbLtGlobalInfo.pu1LbLtPdu

 /* ECFM registration LBLT Specific Information */
#define ECFM_LBLT_GET_APP_INFO(u4ModId)     gEcfmLbLtGlobalInfo.apAppRegParams[u4ModId]

#define ECFM_LBLT_SELECT_CONTEXT(u4ContextId)\
        EcfmLbLtSelectContext (u4ContextId)
#define ECFM_LBLT_RELEASE_CONTEXT()\
        EcfmLbLtReleaseContext()

#define ECFM_LBLT_GET_CONTEXT_INFO(u4ContextId)\
        gEcfmLbLtGlobalInfo.apContextInfo[u4ContextId]
#define ECFM_LBLT_CURR_CONTEXT_INFO() gpEcfmLbLtContextInfo
#define ECFM_LBLT_CURR_CONTEXT_ID() (ECFM_LBLT_CURR_CONTEXT_INFO())->u4ContextId
#define ECFM_LBLT_CLI_EVENT_INFO(u4CliContext) gapEcfmLbLtCliEventInfo[u4CliContext]
#define ECFM_LBLT_BRIDGE_MODE() (ECFM_LBLT_CURR_CONTEXT_INFO())->u4BridgeMode
#define ECFM_LBLT_REG_MOD_BMP() (ECFM_LBLT_CURR_CONTEXT_INFO())->u4RegModBmp
/* macros for System control */
#define ECFM_LBLT_CFG_QUEUE_ID           gEcfmLbLtGlobalInfo.CfgQId
#define ECFM_LBLT_PKT_QUEUE_ID           gEcfmLbLtGlobalInfo.PktQId
#define ECFM_LBLT_SEM_ID                 gEcfmLbLtGlobalInfo.SemId
#define ECFM_LBLT_TASK_ID                gEcfmLbLtGlobalInfo.TaskId
#define ECFM_LBLT_TMRLIST_ID             gEcfmLbLtGlobalInfo.TimerListId
#define ECFM_MAX_LB_SESSIONS             10
/* Semaphore Name for LBLT Task */
#define ECFM_LBLT_SEM_NAME               "LbLtSem"

/* macros for memory control */
#define ECFM_LBLT_TMR_MEMPOOL                 gEcfmLbLtGlobalInfo.TmrMemPool
#define ECFM_LBLT_MSGQ_POOL                   gEcfmLbLtGlobalInfo.MsgQMemPool
#define ECFM_LBLT_PORT_INFO_POOL              gEcfmLbLtGlobalInfo.PortInfoMemPool
#define ECFM_LBLT_MEP_TABLE_POOL              gEcfmLbLtGlobalInfo.MepTableMemPool
#define ECFM_LBLT_MEP_LBM_PDU_POOL            gEcfmLbLtGlobalInfo.MepLbmPduMemPool
#define ECFM_LBLT_MEP_LBM_DATA_TLV_POOL       gEcfmLbLtGlobalInfo.MepLbmDataTlvMemPool
#define ECFM_LBLT_MIP_TABLE_POOL              gEcfmLbLtGlobalInfo.MipTableMemPool
#define ECFM_LBLT_STACK_TABLE_POOL            gEcfmLbLtGlobalInfo.StackTableMemPool
#define ECFM_LBLT_LTM_REPLY_LIST_TABLE_POOL   gEcfmLbLtGlobalInfo.LtmReplyListMemPool
#define ECFM_LBLT_CONTEXT_POOL                gEcfmLbLtGlobalInfo.ContextMemPool
#define ECFM_LBLT_LBM_TABLE_POOL              gEcfmLbLtGlobalInfo.LbmTableMemPool
#define ECFM_LBLT_RMEP_DB_POOL                gEcfmLbLtGlobalInfo.RMepDbMemPool
#define ECFM_LBLT_MA_POOL                     gEcfmLbLtGlobalInfo.MaMemPool
#define ECFM_LBLT_MD_POOL                     gEcfmLbLtGlobalInfo.MdMemPool
#define ECFM_LBLT_DELAY_QUEUE_POOL            gEcfmLbLtGlobalInfo.DelayQueueMemPool
#define ECFM_LBLT_DEF_MD_TABLE_POOL           gEcfmLbLtGlobalInfo.DefMdTableMemPool
#define ECFM_LBLT_APP_REG_MEM_POOL            gEcfmLbLtGlobalInfo.AppRegMemPoolId
#define ECFM_LBLT_VLAN_MEM_POOL               gEcfmLbLtGlobalInfo.VlanMemPool 
#define ECFM_LTR_CACHE_INDICES_POOLID         gEcfmLbLtGlobalInfo.LtrCacheIndicesPool

/*Macros for getting context specific information*/
#define ECFM_LBLT_PORT_MEP_TABLE\
    (ECFM_LBLT_CURR_CONTEXT_INFO())->MepPortTable

#define ECFM_LBLT_DEF_MD_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->DefaultMdTable

#define ECFM_LBLT_GET_DEFAULT_MD_ENTRY(u4VidIsid,pDefaultMdNode)\
    EcfmLbLtUtilGetDefaultMdNode(u4VidIsid,&pDefaultMdNode)

#define ECFM_LBLT_DEF_MD_DEFAULT_SENDER_ID_PERMISSION\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u1DefaultMdDefIdPermission

#define ECFM_LBLT_MIP_CCM_DB_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->MipCcmDbMemPool

#define ECFM_LBLT_MA_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->MaTable

#define ECFM_LBLT_MD_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->MdTable

#define ECFM_LBLT_MPLSMEP_TABLE\
            (ECFM_LBLT_CURR_CONTEXT_INFO())->MplsPathTree

#define ECFM_LBLT_MIP_CCM_DB_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->MipCcmDbTable

#define ECFM_LBLT_RMEP_DB_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->RMepDbTable

#define ECFM_LBLT_FD_BUFFER_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->FrmDelayBuffer

#define ECFM_LBLT_LBM_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LbmTable

#define ECFM_LBLT_LTM_REPLY_LIST\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtmReplyList

#define ECFM_LBLT_MEP_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->MepTableIndex

#define ECFM_LBLT_MIP_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->MipTable

#define ECFM_LBLT_LTR_TABLE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrTable

#define ECFM_LBLT_GET_PORT_INFO(u2PortNum)\
        (((u2PortNum == 0) || (u2PortNum > ECFM_LBLT_MAX_PORT_INFO)) ? NULL : ECFM_LBLT_PORT_INFO((UINT2)u2PortNum))

#define ECFM_LBLT_GET_MEP_TRANSMIT_LBM_STATUS(pLbInfo)\
        pLbInfo->u1TxLbmStatus


#define ECFM_LBLT_DELAY_QUEUE()\
        (&(ECFM_LBLT_CURR_CONTEXT_INFO())->DelayQueueInfo)

#define ECFM_LBLT_LBR_CACHE_SIZE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u2LbrCacheSize
#define ECFM_LBLT_LBR_CACHE_STATUS\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u1LbrCacheStatus
#define ECFM_LBLT_CURR_LBR_CACHE_STATUS\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u1LbrCacheStatusBckup
#define ECFM_LBLT_LBR_CACHE_HOLD_TIME\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u2LbrCacheHoldTime
#define ECFM_LBLT_LBR_HOLD_TIMER\
    (ECFM_LBLT_CURR_CONTEXT_INFO())->LbrHoldTimer
#define ECFM_IS_LBR_CACHE_EMPTY()\
       ((RBTreeGetFirst (ECFM_LBLT_LBM_TABLE))?(ECFM_FALSE):(ECFM_TRUE))

#define ECFM_LBLT_FD_BUFFER_SIZE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u2FrmDelayBuffSize


#define ECFM_LBLT_LTR_CACHE_SIZE\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u2LtrCacheSize
#define ECFM_LBLT_LTR_CACHE_STATUS\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u1LtrCacheStatus
#define ECFM_LBLT_LTR_CACHE_HOLD_TIME\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u2LtrCacheHoldTime
#define ECFM_LBLT_LTR_HOLD_TIMER\
    (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrHoldTimer
 
#define ECFM_LBLT_LTR_TABLE_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrTableMemPool
        
#define ECFM_LBLT_LBR_TABLE_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LbrTableMemPool

#define ECFM_LBLT_FD_BUFFER_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->FrmDelayBuffMemPool

#define ECFM_LBLT_LTR_SENDER_TLV_CHASSIS_ID_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrSenderTlvChassisIdPool

#define ECFM_LBLT_LTR_SENDER_TLV_MGMT_DOMAIN_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrSenderTlvMgmtDomainPool

#define ECFM_LBLT_LTR_SENDER_TLV_MGMT_ADDR_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrSenderTlvMgmtAddrPool

#define ECFM_LBLT_LTR_ORG_TLV_VALUE_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrOrgTlvValuePool

#define ECFM_LBLT_LTR_EGRESS_TLV_EPORT_ID_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrEgressTlvEgressPortIdPool

#define ECFM_LBLT_LTR_INGRESS_TLV_IPORT_ID_POOL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->LtrIngressTlvIngressPortIdPool

#define ECFM_LBLT_IS_PORT_MODULE_STS_ENABLED(u2PortNum)\
        ((ECFM_LBLT_GET_PORT_INFO(u2PortNum)!=NULL)?(ECFM_LBLT_PORT_INFO(u2PortNum)->u1PortEcfmStatus==ECFM_ENABLE):0)
#define ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT(u2PortNum)\
        ((ECFM_LBLT_GET_PORT_INFO(u2PortNum)!=NULL)?(ECFM_LBLT_PORT_INFO(u2PortNum)->u1PortY1731Status==ECFM_ENABLE):0) 
#define ECFM_LBLT_IS_Y1731_DISABLED_ON_PORT(u2PortNum)\
        (!(ECFM_LBLT_IS_Y1731_ENABLED_ON_PORT(u2PortNum)))
#define ECFM_LBLT_SET_PORTID_SUBTYPE(u2PortNum, u1SubType)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      (ECFM_LBLT_PORT_INFO(u2PortNum)->u1PortIdSubType) = u1SubType;\
   }\
}while (0)
#define ECFM_LBLT_PORTID(u2PortNum)\
      (ECFM_LBLT_GET_PORT_INFO(u2PortNum)!=NULL?(ECFM_LBLT_PORT_INFO(u2PortNum)->au1PortId):NULL)
/* FrameDelay Buffer related macros */
#define ECFM_IS_FD_BUFFER_EMPTY()\
       ((RBTreeGetFirst (ECFM_LBLT_FD_BUFFER_TABLE))?(ECFM_FALSE):(ECFM_TRUE))
        
/* LTR Cache related macros */
#define ECFM_IS_LTR_CACHE_ENABLED()\
        ((ECFM_LBLT_LTR_CACHE_STATUS == ECFM_ENABLE)?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_LTR_CACHE_DISABLED()\
        ((ECFM_LBLT_LTR_CACHE_STATUS) != ECFM_ENABLE)

/* LBR Cache related macros */
#define ECFM_IS_LBR_CACHE_ENABLED()\
        ((ECFM_LBLT_LBR_CACHE_STATUS == ECFM_ENABLE)?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_LBR_CACHE_DISABLED()\
        ((ECFM_LBLT_LBR_CACHE_STATUS) != ECFM_ENABLE)
/* Macro to Set LTM Ttl most significant bit */
#define ECFM_LBLT_SET_LTM_TTL_MSB(u2Flags)\
        (u2Flags=u2Flags|0x8000)

/* Macro to Get LTM Ttl most significant bit */
#define ECFM_LBLT_GET_LTM_TTL_MSB(u2Flags)\
        ((u2Flags&0x8000)>>15)

/* Macro to clear LTM Ttl most significant bit */
#define ECFM_LBLT_MASK_LTM_TTL_MSB(u2Flags)\
        ((u2Flags=u2Flags&0xff))

/* Macro to Set LTM flags least significant bit */
#define ECFM_LBLT_SET_LTM_FLAGS_LSB(u1Flags)\
        (u1Flags=u1Flags|0x01)

/* Macro to Get LTM flags least significant bit */
#define ECFM_LBLT_GET_LTM_FLAGS_LSB(u1Flags)\
        ((u1Flags&0x01)<<7)

/* Macro to clear LTM flags least significant bit */
#define ECFM_LBLT_MASK_LTM_FLAGS_LSB(u1Flags)\
        ((u1Flags=u1Flags&0xfe))

/*LBLT Specific Get and Set Macros */
#define ECFM_LBLT_SET_OUI(pu1OctetStream)\
do\
{\
  pu1OctetStream[0]=(ECFM_LBLT_ORG_UNIT_ID[0]);\
  pu1OctetStream[1]=(ECFM_LBLT_ORG_UNIT_ID[1]);\
  pu1OctetStream[2]=(ECFM_LBLT_ORG_UNIT_ID[2]);\
  pu1OctetStream+=(ECFM_OUI_FIELD_SIZE);\
}\
while (0)

/*Folowing macro is used to set the LLC SNAP header in the CFM frame*/
#define ECFM_LBLT_SET_LLC_SNAP_HDR(pu1Pdu,u2Length)\
do\
{\
 ECFM_PUT_2BYTE(pu1Pdu,u2Length);\
 ECFM_PUT_1BYTE(pu1Pdu,ECFM_DST_LSAP_IN_LLC_SNAP_HDR);\
 ECFM_PUT_1BYTE(pu1Pdu,ECFM_SRC_LSAP_IN_LLC_SNAP_HDR);\
 ECFM_PUT_1BYTE(pu1Pdu,ECFM_CONTROL_BYTE_IN_LLC_SNAP_HDR);\
 *(pu1Pdu++)=(ECFM_LBLT_ORG_UNIT_ID[0]);\
 *(pu1Pdu++)=(ECFM_LBLT_ORG_UNIT_ID[1]);\
 *(pu1Pdu++)=(ECFM_LBLT_ORG_UNIT_ID[2]);\
 ECFM_PUT_2BYTE(pu1Pdu,ECFM_LLC_CFM_TYPE);\
}\
while(0)
/* EcfmCcGetMepSenderIdPerm - used to get the sender-id permission from
 * MD/MA of CC task.
 * EcfmUtilSetSenderIdTlv - Sets the sender-id according to the sender-id
 * permission received from EcfmCcGetMepSenderIdPerm.
 * */
#define ECFM_LBLT_SET_SENDER_ID_TLV(pLbltMepInfo,pu1OctetStream)\
   EcfmUtilSetSenderIdTlv(EcfmLbLtUtilGetMepSenderIdPerm(pLbltMepInfo),&pu1OctetStream)

/* EcfmCcGetMhfSenderIdPerm - used to get the sender-id permission from
 * MD/MA of CC task.
 * EcfmUtilSetSenderIdTlv - Sets the sender-id according to the sender-id
 * permission received from EcfmCcGetMhfSenderIdPerm.
 * */
#define ECFM_LBLT_SET_SENDER_ID_TLV_FOR_MHF(pStackInfo,pu1OctetStream)\
   EcfmUtilSetSenderIdTlv(EcfmLbLtUtilGetMhfSenderIdPerm(pStackInfo),&pu1OctetStream)

#define ECFM_SET_LTM_EGRESS_ID(pStackInfo, au1EgressId)\
do\
{\
   if (ECFM_LBLT_GET_PORT_INFO(pStackInfo->u2PortNum)!=NULL)\
   {\
   UINT1 *pu1Ptr = au1EgressId;\
   ECFM_PUT_2BYTE(pu1Ptr,ECFM_LBLT_PORT_INFO(pStackInfo->u2PortNum)->u4IfIndex);\
   EcfmCfaGetContextMacAddr(ECFM_LBLT_CURR_CONTEXT_ID(),pu1Ptr);\
   pu1Ptr = pu1Ptr + ECFM_MAC_ADDR_LENGTH;\
   }\
}\
while(0)

/* Loopback specific macros*/

/* Loopback Capability*/
#define ECFM_LBLT_IS_LB_ENABLED(pLbLtMepInfo)\
        ((pLbLtMepInfo->LbInfo.u1LbCapability == ECFM_ENABLE)? (ECFM_TRUE):(ECFM_FALSE))
#define ECFM_LBLT_IS_LB_DISABLED(pLbLtMepInfo)\
        (!ECFM_LBLT_IS_LB_ENABLED(pLbLtMepInfo))

/* Multicast Loopback Reception Capability*/
#define ECFM_LBLT_IS_MCAST_LBM_RX_ENABLED(pLbLtMepInfo)\
        ((pLbLtMepInfo->LbInfo.u1RxMCastLbmCap == ECFM_ENABLE)? (ECFM_TRUE):(ECFM_FALSE))
#define ECFM_LBLT_IS_MCAST_LBM_RX_DISABLED(pLbLtMepInfo)\
        (!ECFM_LBLT_IS_MCAST_LBM_RX_ENABLED(pLbLtMepInfo))

/* Multicast TST Reception Capability*/
#define ECFM_LBLT_IS_MCAST_TST_RX_ENABLED(pLbLtMepInfo)\
        ((pLbLtMepInfo->TstInfo.u1MulticastTstRxCap == ECFM_ENABLE)? (ECFM_TRUE):(ECFM_FALSE))
#define ECFM_LBLT_IS_MCAST_TST_RX_DISABLED(pLbLtMepInfo)\
        (!ECFM_LBLT_IS_MCAST_LBM_RX_ENABLED(pLbLtMepInfo))

/* TST Capability*/
#define ECFM_LBLT_IS_TST_ENABLED(pLbLtMepInfo)\
        ((pLbLtMepInfo->TstInfo.u1TstCapability == ECFM_ENABLE)? (ECFM_TRUE):(ECFM_FALSE))
#define ECFM_LBLT_IS_TST_DISABLED(pLbLtMepInfo)\
        (!ECFM_LBLT_IS_TST_ENABLED(pLbLtMepInfo))
/*Macro to decrement the number to be transmitted TST frame*/
#define ECFM_LBLT_DECR_TRANSMIT_TST_MSG(pMepInfo)\
        ((pMepInfo->TstInfo.u4TstMessages)?(pMepInfo->TstInfo.u4TstMessages--):(0))
/* Macro to increment the TST sequence number */
#define ECFM_LBLT_INCR_TST_SEQ_NUM(pTstInfo)pTstInfo->u4TstSeqNumber++

/*Macro to Increment Bit Error in TstInfo*/
#define ECFM_TST_INCR_BIT_ERROR(pTstInfo)\
        (ECFM_INCR((pTstInfo->u4BitErroredTstIn),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/*Macro to Increment Valid Tst Pdu received in TstInfo*/
#define ECFM_LBLT_INCR_TST_IN(pTstInfo)\
        (ECFM_INCR((pTstInfo->u4ValidTstIn),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/*Macro to Increment Tst Pdu Transmitted in TstInfo*/
#define ECFM_LBLT_INCR_TST_OUT(pTstInfo)\
        (ECFM_INCR((pTstInfo->u4TstsSent),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/* Delay Mesaurement related macros*/
#define ECFM_LBLT_INCR_DM_TRANS_ID(pLbLtMepInfo)pLbLtMepInfo->DmInfo.u4CurrTransId ++
#define ECFM_LBLT_RESET_DM_SEQ_NUM(pLbLtMepInfo)pLbLtMepInfo->DmInfo.u4TxDmSeqNum=0
#define ECFM_LBLT_INCR_DM_SEQ_NUM(pLbLtMepInfo)pLbLtMepInfo->DmInfo.u4TxDmSeqNum++
#define ECFM_LBLT_DECR_DM_SEQ_NUM(u4SeqNum) \
                     (u4SeqNum?(u4SeqNum--):0) 
#define ECFM_LBLT_INCR_1DM_RCVD_COUNT(pLbLtMepInfo)pLbLtMepInfo->DmInfo.u2NoOf1DmIn++
#define ECFM_LBLT_INCR_DMR_RCVD_COUNT(pLbLtMepInfo)pLbLtMepInfo->DmInfo.u2NoOfDmrIn++

#define ECFM_LBLT_GET_THINFO_FROM_MEP(pLbLtMepInfo)\
        (&pLbLtMepInfo->ThInfo)

#define ECFM_LBLT_GET_DMINFO_FROM_MEP(pLbLtMepInfo)\
        (&pLbLtMepInfo->DmInfo)
        
#define ECFM_LBLT_GET_TSTINFO_FROM_MEP(pLbLtMepInfo)\
        (&pLbLtMepInfo->TstInfo)
        
#define ECFM_LBLT_GET_LBINFO_FROM_MEP(pLbLtMepInfo)\
        (&pLbLtMepInfo->LbInfo)

#define ECFM_LBLT_GET_LTINFO_FROM_MEP(pLbLtMepInfo)\
        (&pLbLtMepInfo->LtInfo)

#define ECFM_LBLT_GET_PORTINFO_FROM_MEP(pLbLtMepInfo)\
        (pLbLtMepInfo->pPortInfo)

#define ECFM_LBLT_GET_MEP_FROM_PDUSM(pLbltPduSmInfo)\
        (pLbltPduSmInfo->pMepInfo)

#define ECFM_LBLT_GET_MIP_FROM_PDUSM(pLbltPduSmInfo)\
        (pLbltPduSmInfo->pMipInfo)

#define ECFM_LBLT_GET_MHF_FROM_PDUSM(pLbltPduSmInfo)\
        (pLbltPduSmInfo->pMhfInfo)

#define ECFM_LBLT_GET_LBR_FROM_PDUSM(pLbltPduSmInfo)\
        (&pLbltPduSmInfo->uPduInfo.Lbr)

#define ECFM_LBLT_GET_LTR_FROM_PDUSM(pLbltPduSmInfo)\
        (&pLbltPduSmInfo->uPduInfo.Ltr)

#define ECFM_LBLT_GET_LTM_FROM_PDUSM(pLbltPduSmInfo)\
        (&pLbltPduSmInfo->uPduInfo.Ltm)

/*Macro to Get UseFDBonly bit from the Flags of CFM-HDR*/
#define ECFM_LBLT_GET_USE_FDB_ONLY(u1Flags)\
        ((u1Flags&0x80)>>7)
        
/*Macro to Get FwdYes bit from the Flags of CFM-HDR*/
#define ECFM_LBLT_GET_FWD_YES(u1Flags)\
        ((u1Flags&0x40)>>6)
        
/*Macro to Get TerminalMEP bit from the Flags of CFM-HDR*/
#define ECFM_LBLT_GET_TERM_MEP(u1Flags)\
        ((u1Flags&0x20)>>5)
        
/*Macro to Set UseFDBonly bit in the Flags of CFM-HDR*/
#define ECFM_LBLT_SET_USE_FDB_ONLY(u1Flags)\
        (u1Flags=u1Flags|0x80)
        
/*Macro to Set FwdYes bit in the Flags of CFM-HDR*/
#define ECFM_LBLT_SET_FWD_YES(u1Flags)\
        (u1Flags=u1Flags|0x40)
        
/*Macro to Set TerminalMEP bit in the Flags of CFM-HDR*/
#define ECFM_LBLT_SET_TERM_MEP(u1Flags)\
        (u1Flags=u1Flags|0x20)

/*Macro to Set Bad Msdu BIT in LbrStatus*/
#define ECFM_LBLT_SET_BAD_MSDU_ERR(pLbrInfo)\
        (pLbrInfo->u1LbrErrorStatus=ECFM_LBLT_LBR_WITH_BAD_MSDU)

/*Macro to Set CRC-Err BIT in LbrStatus*/
#define ECFM_LBLT_SET_BIT_ERR(pLbrInfo)\
        (pLbrInfo->u1LbrErrorStatus=ECFM_LBLT_LBR_WITH_BIT_ERR)

/*Macro to decrement the dot1agCfmMepTransmitLbmMessages*/
#define ECFM_LBLT_DECR_TRANSMIT_LBM_MSG(pLbInfo)\
        ((pLbInfo->u2TxLbmMessages)?(pLbInfo->u2TxLbmMessages--):(0))
        
/*Macro to decrement the number to be transmitted DM frame*/
#define ECFM_LBLT_DECR_TRANSMIT_DM_MSG(pMepInfo)\
        ((pMepInfo->DmInfo.u2TxNoOfMessages)?(pMepInfo->DmInfo.u2TxNoOfMessages--):(0))
        
/*Macro to increment the dot1agCfmMepNextLbmTransId*/
#define ECFM_LBLT_INCR_NEXT_LBM_TRANS_ID(pLbInfo)pLbInfo->u4NextTransId++
/*Macro to increment the expectedLbrTransId*/
#define ECFM_LBLT_INCR_EXPECT_LBR_TRANS_ID(pLbInfo)pLbInfo->u4ExpectedLbrTransId++

#define ECFM_LBLT_RESET_LBM_SEQ_NUM(pLbInfo)pLbInfo->u4NextLbmSeqNo=0
/* Macro to increment the LBM sequence number */
#define ECFM_LBLT_INCR_LBM_NEXT_SEQ_NUM(pLbInfo)pLbInfo->u4NextLbmSeqNo++
/* Macro to increment the number of LBR received */
#define ECFM_LBLT_INCR_LBR_RCVD_COUNT(pLbInfo)pLbInfo->u2NoOfLbrIn++

/*Macro to Increment Bit Error in LBInfo*/
#define ECFM_LBLT_INCR_BIT_ERROR(pLbInfo)\
        (ECFM_INCR((pLbInfo->u4LbrBitError),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
        
/*Macro to increment the dot1agCfmMepBadMsdu*/
#define ECFM_LBLT_INCR_BAD_MSDU(pLbInfo)\
        (ECFM_INCR((pLbInfo->u4LbrBadMsdu),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
/*Macro to increment the Unexpected LBR received */
#define ECFM_LBLT_INCR_UNEXP_LBR_IN(pLbmNode)\
        (ECFM_INCR((pLbmNode->u4UnexpLbrIn),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
        
/*Macro to increment the Duplicate LBR received */
#define ECFM_LBLT_INCR_DUP_LBR_IN(pLbmNode)\
        (ECFM_INCR((pLbmNode->u4DupLbrIn),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
/*Macro to increment the No. of LB Responders */
#define ECFM_LBLT_INCR_NUM_OF_RESPONDERS(pLbNode)\
        (ECFM_INCR((pLbNode->u4NumOfResponders),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
/*Macro to increment the dot1agCfmMepLbrInOutOfOrder*/
#define ECFM_LBLT_INCR_LBR_IN_OUT_OF_ORDER(pLbInfo)\
        (ECFM_INCR((pLbInfo->u4LbrInOutOfOrder),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
        
/*Macro to increment the LbmOut*/
#define ECFM_LBLT_INCR_LBM_OUT(pLbInfo)\
        (ECFM_INCR((pLbInfo->u4LbmOut),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
/*Macro to increment the dot1agCfmMepLbrIn*/
#define ECFM_LBLT_INCR_LBR_IN_ORDER(pLbInfo)\
        (ECFM_INCR((pLbInfo->u4LbrIn),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))
        
/*Macro to increment the dot1agCfmMepLbrOut*/
#define ECFM_LBLT_INCR_LBR_OUT(pLbInfo)\
        (ECFM_INCR((pLbInfo->u4LbrOut),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/*Macro to increment the dot1agCfmMepLtmSeqNumber*/
#define ECFM_LBLT_INCR_LTM_SEQ_NUM(pLtiSmInfo)pLtiSmInfo->u4LtmNextSeqNum++

/*Macro to increment the dot1agCfmMepUnexpLtrIn*/
#define ECFM_LBLT_INCR_UNEXP_LTR_IN(pLtInfo)\
        (ECFM_INCR((pLtInfo->u4UnexpLtrIn),\
        (ECFM_INCR_VAL),(ECFM_UINT4_MAX),1))

/*ECFM SAP*/
/*Macros for LTI SAP*/
/* 0x1A3CA24D is selected so as to have a unique value*/
#define ECFM_LBLT_IS_LTI_SAP_IND_PRESENT(pBuf)\
        (pBuf->ModuleData.u4Reserved2==0x1A3CA24D)
        
#define ECFM_LBLT_SET_LTI_SAP_INDICATION(pBuf)\
        (pBuf->ModuleData.u4Reserved2=(0x1A3CA24D))

#define ECFM_LBLT_RESET_LTI_SAP_INDICATION(pBuf)\
        (pBuf->ModuleData.u4Reserved2=0)

#define ECFM_LBLT_GET_LTM_PRI_FRM_PDUSM(pPduSmInfo)\
        pPduSmInfo->VlanClassificationInfo.OuterVlanTag.u1Priority        
/* Macros for statemachines running in context of LBLT Task*/

/*LT Initiator State Machine*/
#define ECFM_LBLT_LTINIT_STATE_MACHINE(u1Event,u1State,pParam)\
        (((u1Event<ECFM_SM_LTI_TX_MAX_EVENTS)&& (u1State <ECFM_LTI_MAX_STATES))?\
        (*gaEcfmLtInitActionProc[gau1EcfmLtInitSm[u1Event][u1State]])(pParam):ECFM_FAILURE)

/* Macros for setting for states for various statemachines running in context of 
 * LBLT Task
 */
#define ECFM_LBLT_LBITX_SET_STATE(pLbLtMepInfo,u1State)\
        pLbLtMepInfo->LbInfo.u1LbiTxSmState = u1State

#define ECFM_LBLT_LTM_INIT_SET_STATE(pLbLtMepInfo, u1State)\
        pLbLtMepInfo->LtInfo.u1LtInitSmState = u1State
        
/* Macros for getting for states for various statemachines running in context of 
 * LBLT Task
 */
#define ECFM_LBLT_LBITX_GET_STATE(pLbLtMepInfo)\
        (pLbLtMepInfo->LbInfo.u1LbiTxSmState)

#define ECFM_LBLT_LTM_INIT_GET_STATE(pLbLtMepInfo)\
        (pLbLtMepInfo->LtInfo.u1LtInitSmState)
        
/*Macro to get the Mac Address of the MEP*/
#define ECFM_LBLT_GET_MAC_ADDR_OF_MEP(pMepInfo,MacAddr)\
do\
{\
   if (ECFM_LBLT_GET_PORT_INFO(pMepInfo->u2PortNum) != NULL)\
   {\
    ECFM_GET_MAC_ADDR_OF_PORT\
    ((ECFM_LBLT_PORT_INFO(pMepInfo->u2PortNum)->u4IfIndex),MacAddr);\
   }\
}while(0)

/*Macro to Set MEP to Active State*/
#define ECFM_LBLT_SET_MEP_ACTIVE(pLbLtMepInfo)\
        (pLbLtMepInfo->b1Active = ECFM_TRUE)
/*Macro to Set MEP to Not Active State*/
#define ECFM_LBLT_SET_MEP_NOT_ACTIVE(pLbLtMepInfo)\
        (pLbLtMepInfo->b1Active = ECFM_FALSE)
/*Macro to Check if MEP is Active*/
#define ECFM_LBLT_IS_MEP_ACTIVE(pLbLtMepInfo)\
        ((pLbLtMepInfo->b1Active == ECFM_TRUE) && (pLbLtMepInfo->u1RowStatus == ECFM_ROW_STATUS_ACTIVE))
/*Macro to Check if the Stack Info corresponds to MEP/MIP */
#define ECFM_LBLT_IS_MEP(pStackInfo) (pStackInfo->pLbLtMepInfo!=NULL)
#define ECFM_LBLT_IS_MHF(pStackInfo) (pStackInfo->pLbLtMipInfo!=NULL)
/*Macro to Check if MIP is Active*/
#define ECFM_LBLT_IS_MIP_ACTIVE(pMipInfo)\
        ((pMipInfo->b1Active == ECFM_TRUE) && (pMipInfo->u1RowStatus == ECFM_ROW_STATUS_ACTIVE))

/* Global Statistics */
#define ECFM_LBLT_INCR_MEMORY_FAILURE_COUNT()gEcfmLbLtGlobalInfo.u4MemoryFailureCount++
#define ECFM_LBLT_INCR_BUFFER_FAILURE_COUNT() gEcfmLbLtGlobalInfo.u4BufferFailureCount++

/* Per Port Statistics */
#define ECFM_LBLT_INCR_TX_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxCfmPduCount++;\
   }\
}while (0)

#define ECFM_LBLT_INCR_TX_COUNT(u2PortNum,u1OpCode)\
do\
{\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_LBM:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxLbmCount++;\
           break;\
       case ECFM_OPCODE_LBR:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxLbrCount++;\
           break;\
       case ECFM_OPCODE_LTM:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxLtmCount++;\
           break;\
       case ECFM_OPCODE_LTR:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxLtrCount++;\
           break;\
       case ECFM_OPCODE_DMM:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxDmmCount++;\
           break;\
       case ECFM_OPCODE_DMR:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxDmrCount++;\
            break;\
       case ECFM_OPCODE_TST:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxTstCount++;\
            break;\
       case ECFM_OPCODE_1DM:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4Tx1DmCount++;\
            break;\
       case ECFM_OPCODE_APS:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxApsCount++;\
            break;\
       case ECFM_OPCODE_MCC:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxMccCount++;\
            break;\
       case ECFM_OPCODE_EXM:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxExmCount++;\
            break;\
       case ECFM_OPCODE_EXR:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxExrCount++;\
            break;\
       case ECFM_OPCODE_VSM:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxVsmCount++;\
            break;\
       case ECFM_OPCODE_VSR:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxVsrCount++;\
            break;\
       default:\
       break;\
   }\
   }\
}while(0)
#define ECFM_LBLT_INCR_TX_FAILED_COUNT(u2PortNum)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
      ECFM_LBLT_PORT_INFO(u2PortNum)->u4TxFailedCount++;\
   }\
}while (0)
 
#define ECFM_LBLT_INCR_RX_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
       ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxCfmPduCount++;\
   }\
}while(0)
#define ECFM_LBLT_INCR_RX_COUNT(u2PortNum,u1OpCode)\
do\
{\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_LBM:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxLbmCount++;\
           break;\
       case ECFM_OPCODE_LBR:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxLbrCount++;\
           break;\
       case ECFM_OPCODE_LTM:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxLtmCount++;\
           break;\
       case ECFM_OPCODE_LTR:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxLtrCount++;\
           break;\
       case ECFM_OPCODE_DMM:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxDmmCount++;\
           break;\
       case ECFM_OPCODE_DMR:\
           ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxDmrCount++;\
          break;\
       case ECFM_OPCODE_TST:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxTstCount++;\
            break;\
       case ECFM_OPCODE_1DM:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4Rx1DmCount++;\
            break;\
       case ECFM_OPCODE_APS:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxApsCount++;\
            break;\
       case ECFM_OPCODE_MCC:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxMccCount++;\
            break;\
       case ECFM_OPCODE_EXM:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxExmCount++;\
            break;\
       case ECFM_OPCODE_EXR:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxExrCount++;\
            break;\
       case ECFM_OPCODE_VSM:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxVsmCount++;\
            break;\
       case ECFM_OPCODE_VSR:\
            ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxVsrCount++;\
            break;\
       default:\
       break;\
   }\
   }\
}while(0)
#define ECFM_LBLT_INCR_RX_BAD_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
       ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxBadCfmPduCount++;\
   }\
}while(0)

#define ECFM_LBLT_INCR_FRWD_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum)!=NULL)\
   {\
      ECFM_LBLT_PORT_INFO(u2PortNum)->u4FrwdCfmPduCount++;\
   }\
}while (0)
#define ECFM_LBLT_INCR_DSRD_CFM_PDU_COUNT(u2PortNum)\
do {\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
       ECFM_LBLT_PORT_INFO(u2PortNum)->u4DsrdCfmPduCount++;\
   }\
}while (0)

/* Per Context Statistics */
#define ECFM_LBLT_INCR_CTX_TX_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxCfmPduCount++;\
   }\
}while (0)

#define ECFM_LBLT_INCR_CTX_TX_COUNT(u4CtxId,u1OpCode)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_LBM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxLbmCount++;\
           break;\
       case ECFM_OPCODE_LBR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxLbrCount++;\
           break;\
       case ECFM_OPCODE_LTM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxLtmCount++;\
           break;\
       case ECFM_OPCODE_LTR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxLtrCount++;\
           break;\
       case ECFM_OPCODE_DMM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxDmmCount++;\
           break;\
       case ECFM_OPCODE_DMR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxDmrCount++;\
           break;\
       case ECFM_OPCODE_TST:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxTstCount++;\
           break;\
       case ECFM_OPCODE_1DM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4Tx1DmCount++;\
           break;\
       case ECFM_OPCODE_APS:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxApsCount++;\
           break;\
       case ECFM_OPCODE_MCC:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxMccCount++;\
           break;\
       case ECFM_OPCODE_EXM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxExmCount++;\
           break;\
       case ECFM_OPCODE_EXR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxExrCount++;\
           break;\
       case ECFM_OPCODE_VSM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxVsmCount++;\
           break;\
       case ECFM_OPCODE_VSR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxVsrCount++;\
           break;\
       default:\
       break;\
   }\
   }\
}while(0)
#define ECFM_LBLT_INCR_CTX_TX_FAILED_COUNT(u4CtxId)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4TxFailedCount++;\
   }\
}while (0)
 
#define ECFM_LBLT_INCR_CTX_RX_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
       ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxCfmPduCount++;\
   }\
}while(0)
#define ECFM_LBLT_INCR_CTX_RX_COUNT(u4CtxId,u1OpCode)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
   switch(u1OpCode)\
   {\
       case ECFM_OPCODE_LBM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxLbmCount++;\
           break;\
       case ECFM_OPCODE_LBR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxLbrCount++;\
           break;\
       case ECFM_OPCODE_LTM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxLtmCount++;\
           break;\
       case ECFM_OPCODE_LTR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxLtrCount++;\
           break;\
       case ECFM_OPCODE_DMM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxDmmCount++;\
           break;\
       case ECFM_OPCODE_DMR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxDmrCount++;\
          break;\
       case ECFM_OPCODE_TST:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxTstCount++;\
           break;\
       case ECFM_OPCODE_1DM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4Rx1DmCount++;\
           break;\
       case ECFM_OPCODE_APS:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxApsCount++;\
           break;\
       case ECFM_OPCODE_MCC:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxMccCount++;\
           break;\
       case ECFM_OPCODE_EXM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxExmCount++;\
           break;\
       case ECFM_OPCODE_EXR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxExrCount++;\
           break;\
       case ECFM_OPCODE_VSM:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxVsmCount++;\
           break;\
       case ECFM_OPCODE_VSR:\
           ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxVsrCount++;\
           break;\
       default:\
       break;\
   }\
   }\
}while(0)
#define ECFM_LBLT_INCR_CTX_RX_BAD_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
       ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxBadCfmPduCount++;\
   }\
}while(0)

#define ECFM_LBLT_INCR_CTX_FRWD_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
      ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4FrwdCfmPduCount++;\
   }\
}while (0)
#define ECFM_LBLT_INCR_CTX_DSRD_CFM_PDU_COUNT(u4CtxId)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
       ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4DsrdCfmPduCount++;\
   }\
}while (0)
 
#define ECFM_LBLT_DECR_RX_COUNT(u2PortNum,u1OpCode)\
do\
{\
   if (ECFM_LBLT_GET_PORT_INFO(u2PortNum) != NULL)\
   {\
     if (ECFM_OPCODE_LBR == u1OpCode)\
     {\
 ECFM_LBLT_PORT_INFO(u2PortNum)->u4RxLbrCount--;\
     }\
   }\
}while(0) 
 
#define ECFM_LBLT_DECR_CTX_RX_COUNT(u4CtxId,u1OpCode)\
do {\
   if (ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId) != NULL)\
   {\
    if (ECFM_OPCODE_LBR == u1OpCode)\
    {\
 ECFM_LBLT_GET_CONTEXT_INFO(u4CtxId)->LbLtStats.u4RxLbrCount--;\
    }\
   }\
}while(0)

/* Macro to be called to notify the registered modules about an event */
#define ECFM_NOTIFY_PROTOCOLS(u4EventId, pMepInfo, pMacAddr, u4Var, \
                              u1Flag,u1Offset,pu1DataPtr, \
                              u1CfmPduOffset, pu1OuiPtr,u1SubOpcodeVal,u1TaskId )\
do\
{\
   EcfmUtilPrepareNotification (u4EventId,pMepInfo, pMacAddr, \
                                u4Var,u1Flag,u1Offset, pu1DataPtr, \
                                u1CfmPduOffset, pu1OuiPtr, u1SubOpcodeVal,\
                                u1TaskId);\
}\
while (0)
/* Macro to get no. of bytes to be send in the LBM PDU by providing size of the
 * last LBM PDU 
 * Following is the calculation for the maximum size of data in the data-Tlv or
 * Test-Tlv:
 *  18 bytes - Ethernet Header
 *  4  bytes - CFM Header
 *  4  bytes - Sequence-Number
 *  1  byte  - End Tlv
 *  3  bytes - Over-Head in case of Data-Tlv and  4 bytes - in case of
 *  Test-Tlv */
#define ECFM_LBLT_GET_DATA_TO_SEND_SIZE(u4LastLbmSize, u2DataSize)\
do\
{\
   if (u4LastLbmSize == 0)\
   {\
       u2DataSize = 2;\
   }\
   else\
   {\
       u2DataSize = ((2 * (u4LastLbmSize + 18)) - 30);\
       if (u2DataSize > (ECFM_MAX_PDU_SIZE - 30))\
       {\
          u2DataSize = 2;\
       }\
   }\
}\
while (0)

#define ECFM_LBLT_GET_PATTERN_TO_SEND_SIZE(u4LastLbmSize, u2DataSize)\
do\
{\
   if (u4LastLbmSize == 0)\
   {\
       u2DataSize = 33;\
   }\
   else\
   {\
       u2DataSize = ((2 * (u4LastLbmSize + 18)) - 31);\
       if (u2DataSize > (ECFM_MAX_JUMBO_PDU_SIZE - 31))\
       {\
          u2DataSize = 33;\
       }\
   }\
}\
while (0)
/* TST PDU related */
#define ECFM_LBLT_GET_TST_PATTERN_TO_SEND_SIZE(u4LastTstSize, u2DataSize)\
do\
{\
   if (u4LastTstSize == 0)\
   {\
       u2DataSize = 33;\
   }\
   else\
   {\
       u2DataSize = ((2 * (u4LastTstSize + 31)) - 31);\
       if (u2DataSize > (ECFM_MAX_JUMBO_PDU_SIZE - 31))\
       {\
          u2DataSize = 33;\
       }\
   }\
}\
while (0)

/* Macro for generating LBLT trap*/
#define Y1731_LBLT_GENERATE_TRAP(pTrapInfo,u1TrapId)\
        Y1731LbLtSnmpIfSendTrap(pTrapInfo,u1TrapId)
/* Macros for configuration of LBLT task*/ 
#define ECFM_LBLT_HANDLE_CREATE_PORT(u4ContextId,u4IfIndex,u2PortNum)\
do\
{\
   INT4 i4RetVal = ECFM_INIT_VAL;\
   ECFM_LBLT_LOCK();\
   i4RetVal = ECFM_LBLT_SELECT_CONTEXT(ECFM_CC_CURR_CONTEXT_ID());\
   EcfmLbLtIfHandleCreatePort(u4ContextId,u4IfIndex,u2PortNum);\
   ECFM_LBLT_RELEASE_CONTEXT();\
   ECFM_LBLT_UNLOCK();\
   UNUSED_PARAM(i4RetVal);\
}while(0)
#define ECFM_LBLT_HANDLE_MAP_PORT(u4ContextId,u4IfIndex,u2PortNum)\
        ECFM_LBLT_HANDLE_CREATE_PORT(u4ContextId,u4IfIndex,u2PortNum)
#define ECFM_LBLT_HANDLE_DELETE_PORT(u2PortNum)\
do\
{\
   INT4 i4RetVal = ECFM_INIT_VAL;\
   ECFM_LBLT_LOCK();\
   i4RetVal = ECFM_LBLT_SELECT_CONTEXT(ECFM_CC_CURR_CONTEXT_ID());\
   EcfmLbLtIfHandleDeletePort(u2PortNum);\
   ECFM_LBLT_RELEASE_CONTEXT();\
   ECFM_LBLT_UNLOCK();\
   UNUSED_PARAM(i4RetVal);\
}while(0)
#define ECFM_LBLT_HANDLE_UNMAP_PORT(u2PortNum)\
        ECFM_LBLT_HANDLE_DELETE_PORT(u2PortNum)
#define ECFM_LBLT_HANDLE_PORT_OPER_CHG(u2PortNum,u1PortOperState)\
do\
{\
   INT4 i4RetVal = ECFM_INIT_VAL;\
   ECFM_LBLT_LOCK();\
   i4RetVal = ECFM_LBLT_SELECT_CONTEXT(ECFM_CC_CURR_CONTEXT_ID());\
   EcfmLbLtIfHandlePortOperChg(u2PortNum,u1PortOperState);\
   ECFM_LBLT_RELEASE_CONTEXT();\
   ECFM_LBLT_UNLOCK();\
   UNUSED_PARAM(i4RetVal);\
}while(0)
#define ECFM_LBLT_HANDLE_CREATE_CONTEXT(u4ContextId)\
do\
{\
   ECFM_LBLT_LOCK();\
   EcfmLbLtHandleCreateContext(u4ContextId);\
   ECFM_LBLT_RELEASE_CONTEXT();\
   ECFM_LBLT_UNLOCK();\
}while(0)
#define ECFM_LBLT_HANDLE_DELETE_CONTEXT(u4ContextId)\
do\
{\
   ECFM_LBLT_LOCK();\
   EcfmLbLtHandleDeleteContext(u4ContextId);\
   ECFM_LBLT_UNLOCK();\
}while(0)

#define ECFM_LBLT_UPDATE_BRIDGE_MODE(u2BMode)\
do\
{\
   INT4 i4RetVal = ECFM_INIT_VAL;\
   ECFM_LBLT_LOCK();\
   i4RetVal = ECFM_LBLT_SELECT_CONTEXT(ECFM_CC_CURR_CONTEXT_ID());\
   ECFM_LBLT_CURR_CONTEXT_INFO()->u4BridgeMode=u2BMode;\
   ECFM_LBLT_RELEASE_CONTEXT();\
   ECFM_LBLT_UNLOCK();\
   UNUSED_PARAM(i4RetVal);\
}while(0)

#define ECFM_LBLT_UNAWARE_MEP_PRESENT(u2PortNum)\
   ((ECFM_LBLT_GET_PORT_INFO(u2PortNum))!=NULL?ECFM_LBLT_PORT_INFO(u2PortNum)->b1UnawareMepPresent:0)

/* Macro to be called when Frames to be blocked on occurence of 
 * Out of Service */
#define ECFM_LBLT_LCK_IS_CONFIGURED(pMepInfo) \
 ((pMepInfo->b1LckDelayExp)? ECFM_TRUE:ECFM_FALSE)

#define ECFM_LBLT_HANDLE_TRANSMIT_FAILURE(u2PortNum,u1OpCode)\
        Y1731LbLtSnmpIfSendTxFailTrap(u2PortNum, u1OpCode);

#define ECFM_LBLT_802_1AH_BRIDGE()\
   (((ECFM_LBLT_GET_BRIDGE_MODE () == ECFM_PBB_ICOMPONENT_BRIDGE_MODE) || \
   (ECFM_LBLT_GET_BRIDGE_MODE () == ECFM_PBB_BCOMPONENT_BRIDGE_MODE)) ? ECFM_TRUE : ECFM_FALSE)
#define ECFM_LBLT_GET_PORT_TYPE(u2port) \
        ((ECFM_LBLT_GET_PORT_INFO(u2port))!=NULL?ECFM_LBLT_PORT_INFO(u2port)->u1PortType:ECFM_INVALID_PROVIDER_PORT)

#define ECFM_LBLT_GET_BRIDGE_MODE()     (ECFM_LBLT_CURR_CONTEXT_INFO ())->u4BridgeMode
#define ECFM_LBLT_HANDLE_INTF_TYPE_CHANGE(u4ContextId,u1IntfType)\
do\
{\
   INT4 i4RetVal = ECFM_INIT_VAL;\
   ECFM_LBLT_LOCK();\
   i4RetVal = ECFM_LBLT_SELECT_CONTEXT(ECFM_CC_CURR_CONTEXT_ID());\
   EcfmLbLtIfHandleIntfTypeChg(u1IntfType);\
   ECFM_LBLT_RELEASE_CONTEXT();\
   ECFM_LBLT_UNLOCK();\
   UNUSED_PARAM(i4RetVal);\
}while(0)

#define ECFM_LBLT_ISPORT_SISP_LOG(u2Port) \
    ((ECFM_MIN_SISP_INDEX <= (ECFM_LBLT_GET_PORT_INFO(u2Port)->u4IfIndex))&& \
     ((ECFM_LBLT_GET_PORT_INFO(u2Port)->u4IfIndex) <= ECFM_MAX_SISP_INDEX)?\
     ECFM_TRUE:ECFM_FALSE)

#define ECFM_LBLT_GET_PHY_PORT(u2Port,pTempPortInfo) \
   (((pTempPortInfo = (ECFM_LBLT_GET_PORT_INFO(u2Port))) == NULL)?0:\
    (((ECFM_MIN_SISP_INDEX <= pTempPortInfo->u4IfIndex)&& \
      (ECFM_MAX_SISP_INDEX >= pTempPortInfo->u4IfIndex))?\
       pTempPortInfo->u4PhyPortNum:\
       pTempPortInfo->u4IfIndex))\

/*****************************************************************************/
/*                       COMMON FOR CC AND LBLT TASK                         */
/*****************************************************************************/
#define ECFM_GET_CONTEXT_INFO_FROM_IFINDEX(u4IfIndex, pu4ContextId,\
                                           pu2LocalPortId)\
        EcfmVcmGetContextInfoFromIfIndex(u4IfIndex, pu4ContextId, pu2LocalPortId)

#define ECFM_GET_IFINDEX_FROM_LOCAL_PORT(u4ContextId, u2LocalPort,\
                                         pu4IfIndex)\
        EcfmVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPort, pu4IfIndex)


#define ECFM_VALIDATE_MEP_TABLE_INDICES(Index1,Index2,Index3)\
        ((Index1 < ECFM_MD_INDEX_MIN)||\
        (Index2  < ECFM_MA_INDEX_MIN)||\
        (Index3  < ECFM_MEPID_MIN) ||\
        (Index3  > ECFM_MEPID_MAX)) 

#define ECFM_VALIDATE_MIP_TABLE_INDICES(Index1,Index2,Index3)\
        ((Index1 < ECFM_PORTS_PER_CONTEXT_MIN)||\
        ((UINT2)Index1 > ECFM_CC_MAX_MIP_INFO)||\
        (Index2 < ECFM_MD_LEVEL_MIN)||\
        (Index2 > ECFM_MD_LEVEL_MAX)||\
        (Index3 < ECFM_VLANID_MIN)||\
        (ECFM_CC_802_1AH_BRIDGE()?(Index3 > (ECFM_ISID_MAX + ECFM_VLANID_MAX)):(Index3 > ECFM_VLANID_MAX)))
 


#define ECFM_VALIDATE_STACK_TABLE_INDICES(Index1,Index2,Index3,Index4)\
        ((Index1 < ECFM_PORTS_PER_CONTEXT_MIN)||\
        (Index1 > ECFM_PORTS_PER_CONTEXT_MAX)||\
        (Index2 > ECFM_VLANID_MAX)||\
        (Index3 < ECFM_MD_LEVEL_MIN)||\
        (Index3 > ECFM_MD_LEVEL_MAX)||\
        (Index4 < ECFM_MP_DIR_DOWN)||\
        (Index4 > ECFM_MP_DIR_UP))

#define ECFM_VALIDATE_MA_MEPLIST_TABLE_INDICES(Index1,Index2,Index3)\
        ((Index1 < ECFM_MD_INDEX_MIN) ||\
        (Index2 < ECFM_MA_INDEX_MIN) ||\
        (Index3 < ECFM_MEPID_MIN) ||\
        (Index3 > ECFM_MEPID_MAX))

#define ECFM_VALIDATE_RMEP_TABLE_INDICES(Index1,Index2,Index3,Index4)\
        ((Index1 < ECFM_MD_INDEX_MIN) ||\
        (Index2 < ECFM_MA_INDEX_MIN) ||\
        (Index3 < ECFM_MEPID_MIN) ||\
        (Index3 > ECFM_MEPID_MAX) ||\
        (Index4 < ECFM_MEPID_MIN) ||\
        (Index4 > ECFM_MEPID_MAX))

#define ECFM_VALIDATE_LTR_TABLE_INDICES(Index1,Index2,Index3,Index4,Index5)\
       ((Index1 < ECFM_MD_INDEX_MIN) ||\
       (Index2 < ECFM_MA_INDEX_MIN) ||\
       (Index3 < ECFM_MEPID_MIN) ||\
       (Index3 > ECFM_MEPID_MAX) ||\
       (Index4 < ECFM_LTR_SEQ_NUM_MIN) ||\
       (Index5 < ECFM_LTR_RECV_ORDER_MIN))
       
#define ECFM_ANY_MA_CONFIGURED_IN_MD(pMdNode)\
       ((RBTreeGetFirst (pMdNode->MaTable))?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_ANY_MEP_CONFIGURED_IN_MA(pMaNode)\
        ((TMO_DLL_First(&(pMaNode->MepTable)))?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_CC_GET_DEFAULT_MD_ENTRY(u4VidIsid,pDefaultMdNode)\
    EcfmCcUtilGetDefaultMdNode(u4VidIsid,&pDefaultMdNode)

/*Macro to Get MD Level from CFM-HDR*/
#define ECFM_GET_MDLEVEL(u1FirstByte)\
        ((u1FirstByte&0xE0)>>5)

/*Macro to Get Version from CFM-HDR*/
#define ECFM_GET_VERSION(u1FirstByte)\
        (u1FirstByte&0x1F)

/*Macro to Set MD Level in CFM-HDR*/
#define ECFM_SET_MDLEVEL(u1FirstByte,u1MdLevel)\
        (u1FirstByte=u1FirstByte|((u1MdLevel&0x07)<<5))

/*Macro to Set Verson in CFM-HDR*/
#define ECFM_SET_VERSION(u1FirstByte)\
        (u1FirstByte=u1FirstByte|((ECFM_PROTOCOL_VERSION)&0x1F))

#define ECFM_INCR(uValue,uIncrVal,uRoundOff,uInitVal)\
        (uValue=(((uValue+uIncrVal)>=uRoundOff)?(uInitVal):(uValue+uIncrVal)))\

#define ECFM_COMPARE_MAC_ADDR(MacAddr1,MacAddr2)\
        ((!ECFM_MEMCMP(MacAddr1,MacAddr2,(ECFM_MAC_ADDR_LENGTH)))?(ECFM_SUCCESS):(ECFM_FAILURE))

/*Macro to generate and check cfm group address*/
/*Multicast class 1*/
#define ECFM_GEN_MULTICAST_CLASS1_ADDR(MacAddr,u1MdLevel)\
do\
{\
    ECFM_MEMCPY(MacAddr,(ECFM_GROUP_MAC_ADDRESS),(ECFM_MAC_ADDR_LENGTH));\
    MacAddr[5]=(MacAddr[5]&0x30)|(u1MdLevel);\
}\
while (0)

/*Multicast class 2*/
#define ECFM_GEN_MULTICAST_CLASS2_ADDR(MacAddr,u1MdLevel) \
do\
{\
    ECFM_MEMCPY(MacAddr,(ECFM_GROUP_MAC_ADDRESS),(ECFM_MAC_ADDR_LENGTH));\
    MacAddr[5]=(MacAddr[5]&0x30)|(u1MdLevel+8);\
}\
while (0)

#define ECFM_IS_MULTICAST_CLASS1_ADDR(MacAddr,u1MdLevel)\
        ((MacAddr[0]==(ECFM_GROUP_MAC_ADDRESS[0])&&\
          MacAddr[1]==(ECFM_GROUP_MAC_ADDRESS[1])&&\
          MacAddr[2]==(ECFM_GROUP_MAC_ADDRESS[2])&&\
          MacAddr[3]==(ECFM_GROUP_MAC_ADDRESS[3])&&\
          MacAddr[4]==(ECFM_GROUP_MAC_ADDRESS[4])&&\
          MacAddr[5]==((ECFM_GROUP_MAC_ADDRESS[5]&0x30)|u1MdLevel))\
          ?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_MULTICAST_CLASS2_ADDR(MacAddr,u1MdLevel)\
        ((MacAddr[0]==(ECFM_GROUP_MAC_ADDRESS[0])&&\
          MacAddr[1]==(ECFM_GROUP_MAC_ADDRESS[1])&&\
          MacAddr[2]==(ECFM_GROUP_MAC_ADDRESS[2])&&\
          MacAddr[3]==(ECFM_GROUP_MAC_ADDRESS[3])&&\
          MacAddr[4]==(ECFM_GROUP_MAC_ADDRESS[4])&&\
          MacAddr[5]==((ECFM_GROUP_MAC_ADDRESS[5]&0x30)|(u1MdLevel+8)))?\
          (ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_MULTICAST_ADDR(MacAddr)\
        ((MacAddr[0]&0x01)?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_GROUP_DMAC_ADDR(MacAddr)\
        ((MacAddr[0]==(ECFM_GROUP_MAC_ADDRESS[0])&&\
          MacAddr[1]==(ECFM_GROUP_MAC_ADDRESS[1])&&\
          MacAddr[2]==(ECFM_GROUP_MAC_ADDRESS[2])&&\
          MacAddr[3]==(ECFM_GROUP_MAC_ADDRESS[3])&&\
          MacAddr[4]==(ECFM_GROUP_MAC_ADDRESS[4]))\
          ?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_IS_TUNNEL_MAC_ADDR(MacAddr)\
     ((MacAddr[0]==(ECFM_TUNNEL_MAC_ADDRESS[0])&&\
          MacAddr[1]==(ECFM_TUNNEL_MAC_ADDRESS[1])&&\
          MacAddr[2]==(ECFM_TUNNEL_MAC_ADDRESS[2])&&\
          MacAddr[3]==(ECFM_TUNNEL_MAC_ADDRESS[3])&&\
          MacAddr[4]==(ECFM_TUNNEL_MAC_ADDRESS[4])&&\
          MacAddr[5]==(ECFM_TUNNEL_MAC_ADDRESS[5]))\
          ?(ECFM_TRUE):(ECFM_FALSE))

#define ECFM_CONVERT_HRS_TO_MSEC(u4Hrs)\
        (((u4Hrs*60)*60)*1000)
        
#define ECFM_CONVERT_SEC_TO_TIME_TICKS(u4Seconds)\
        (u4Seconds*(ECFM_NUM_OF_TIME_UNITS_IN_A_SEC))

#define ECFM_CONVERT_MSEC_TO_TIME_TICKS(u4MSeconds)\
        (u4MSeconds/(ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT))
        
#define ECFM_CONVERT_TIME_TICKS_TO_MSEC(u4TimeTicks)\
        (u4TimeTicks*(ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT))

#define ECFM_CONVERT_TIME_TICKS_TO_SEC(u4TimeTicks)\
        ((u4TimeTicks*(ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT))/1000)

#define ECFM_CONVERT_SNMP_TIME_TICKS_TO_MSEC(u4SnmpTimeTicks)\
        (u4SnmpTimeTicks*10)

#define ECFM_CONVERT_MSEC_TO_SNMP_TIME_TICKS(u4MSeconds)\
        (u4MSeconds/10)
#define ECFM_CONVERT_SEC_TO_SNMP_TIME_TICKS(u4Seconds)\
        (u4Seconds*100)
#define ECFM_CONVERT_TIME_TICKS_TO_SNMP_TIME_TICKS(u4TimeTicks)\
        ((u4TimeTicks*ECFM_NUM_OF_MSEC_IN_A_TIME_UNIT)/10)

#define ECFM_GET_TIMESTAMP(pu4Time) \
        *pu4Time = OsixGetSysUpTime () * SYS_TIME_TICKS_IN_A_SEC

#define ECFM_USEC_TIMER_SUPPORT()          ECFM_TRUE 

/* Macro to get the MAC Address of the Port */
#define ECFM_GET_MAC_ADDR_OF_PORT(IfIndex,MacAddr) \
 EcfmGetMacAddrOfPort((UINT4) IfIndex,(UINT1 *) MacAddr)

#define ECFM_GET_MAC_ADDR_OF_PORTID(IfIndex,MacAddr,CfaInfo)\
do\
{\
    MEMSET (&CfaInfo, 0, sizeof (tCfaIfInfo));\
    if ((EcfmUtilGetIfInfo(IfIndex,&CfaInfo) != CFA_FAILURE))\
    {\
        ECFM_MEMCPY(MacAddr,CfaInfo.au1MacAddr,(ECFM_MAC_ADDR_LENGTH));\
    }\
}\
while (0)
#define ECFM_IS_MEP_VLAN_AWARE(u2VlanId)\
        (u2VlanId != ECFM_INIT_VAL)

/* Macro to reset the ports in the portarray
   DO NOT Call with u2Port as ZERO
*/
#define ECFM_RESET_MEMBER_PORT(au1PortArray, u2Port) \
do\
{\
 UINT2 u2PortBytePos=0;\
 UINT2 u2PortBitPos=0;\
 UINT1 au1PortBitMap[8] = { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
 u2PortBytePos = (UINT2)(u2Port / 8);\
 u2PortBitPos  = (UINT2)(u2Port % 8);\
 if (u2PortBitPos  == 0) {(u2PortBytePos!=0)?(u2PortBytePos--):(u2PortBytePos);} \
 \
    if(u2PortBytePos >= ECFM_PORT_LIST_SIZE) { break; }\
 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
               & ~au1PortBitMap[u2PortBitPos]);\
}while(0)

/* Macro to set the ports in the portarray
   DO NOT Call with u2NoOfPorts as ZERO
*/
#define ECFM_SET_PORTS_IN_LIST(au1PortArray, u2NoOfPorts)\
do\
{\
        UINT1 au1PortBitMap[9]={0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF};\
        UINT2 u2NoOfBytes=0;\
        UINT2 u2NoOfBits=0;\
        u2NoOfBytes = u2NoOfPorts/8;\
        u2NoOfBits  = u2NoOfPorts%8;\
        ECFM_MEMSET(au1PortArray,0xFF,u2NoOfBytes);\
        if (u2NoOfBits != 0) \
        {\
                au1PortArray[u2NoOfBytes] = u2NoOfBytes | au1PortBitMap[u2NoOfBits];\
        }\
}\
while(0)
/* Macro to reset the ports in the portarray
   DO NOT Call with u2Vlan as ZERO
*/
#define ECFM_RESET_MEMBER_VLAN(au1VlanArray, u2Vlan) \
do\
{\
 UINT2 u2PortBytePos;\
 UINT2 u2PortBitPos;\
 UINT1 au1PortBitMap[8] = { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
 u2PortBytePos = (UINT2)(u2Vlan / 8);\
 u2PortBitPos  = (UINT2)(u2Vlan % 8);\
 if (u2PortBitPos  == 0) {(u2PortBytePos!=0)?(u2PortBytePos--):(u2PortBytePos);} \
 \
    if(u2PortBytePos >= ECFM_VLAN_LIST_SIZE) { break; }\
 au1VlanArray[u2PortBytePos] = (UINT1)(au1VlanArray[u2PortBytePos] \
               & ~au1PortBitMap[u2PortBitPos]);\
}while(0)
#define ECFM_IS_HEX_DIGIT(MaValue)\
    (((MaValue>='0') && (MaValue<='9')) || (((MaValue>='a')) && (MaValue<='f')) ||\
   (((MaValue>='A')) && (MaValue<='F')))

#define ECFM_IS_LIST_MEMBER(au1Array, u2Member, u1Result) \
do \
        {\
           UINT2 u2BytePos;\
           UINT2 u2BitPos;\
           UINT1 au1BitMaskMap[8] =\
              { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
           u2BytePos = (UINT2)(u2Member / 8);\
           u2BitPos  = (UINT2)(u2Member % 8);\
    if (u2BitPos  == 0) {u2BytePos -= 1;} \
           if ((u2BytePos < sizeof(au1Array))&&(au1Array[u2BytePos] \
                & au1BitMaskMap[u2BitPos]) != 0) {\
           \
              u1Result = ECFM_TRUE;\
           }\
           else {\
           \
              u1Result = ECFM_FALSE; \
           } \
        }\
while(0)

    /* This macro adds u2Member as a member port in the list pointed by
     * au1Array  */
 /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
 
#define ECFM_SET_LIST_MEMBER(au1Array, u2Member) \
do \
           {\
              UINT2 u2BytePos;\
              UINT2 u2BitPos;\
              UINT1 au1BitMaskMap[8] =\
              { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
               u2BytePos = (UINT2)(u2Member / 8);\
              u2BitPos  = (UINT2)(u2Member % 8);\
       if (u2BitPos  == 0) {u2BytePos -= 1;} \
           \
              if (u2BytePos < sizeof(au1Array))\
              {\
              au1Array[u2BytePos] = (UINT1)(au1Array[u2BytePos] \
                             | au1BitMaskMap[u2BitPos]);\
              }\
           }\
while(0)

#define ECFM_RESET_LIST_MEMBER(au1Array, u2Member) \
do \
           {\
              UINT2 u2BytePos;\
              UINT2 u2BitPos;\
              u2BytePos = (UINT2)(u2Member / 8);\
              u2BitPos  = (UINT2)(u2Member % 8);\
              if (u2BitPos  == 0) {(u2BytePos!=0)?(u2BytePos--):(u2BytePos);} \
              if (u2BytePos < sizeof(tVlanListExt))\
              {\
                  au1Array[u2BytePos] = (UINT1)(au1Array[u2BytePos] \
                             & 0);\
              }\
           }\
while(0)

/* au1List1 = au1List1 & au1lisy2
 * */
#define ECFM_AND_PORT_LIST(au1List1, au1List2) \
{\
       UINT2 u2ByteIndex;\
       for (u2ByteIndex = 0; u2ByteIndex  < ECFM_PORT_LIST_SIZE;\
            u2ByteIndex++)\
       {\
           au1List1[u2ByteIndex] &= au1List2[u2ByteIndex];\
       }\
}

/* Macro to CLEAR a bit, bit position starts from 0*/
#define ECFM_CLEAR_U1BIT(u1Var,u1BitPos)\
   u1Var=u1Var&~(0x01<<u1BitPos)
   
#define ECFM_CLEAR_U2BIT(u2Var,u1BitPos)\
   u2Var=u2Var&~(0x0001<<u1BitPos)
   
#define ECFM_CLEAR_U4BIT(u4Var,u1BitPos)\
   u4Var=u4Var&~(0x00000001<<u1BitPos)


/* Macro to SET a bit, bit position starts from 0*/
#define ECFM_SET_U1BIT(u1Var,u1BitPos)\
   u1Var=u1Var|(0x01<<u1BitPos)
   
#define ECFM_SET_U2BIT(u2Var,u1BitPos)\
   u2Var=u2Var|(0x0001<<u1BitPos)
   
#define ECFM_SET_U4BIT(u4Var,u1BitPos)\
   u4Var=u4Var|(0x00000001<<u1BitPos)

/* Macro to GET a bit, bit position starts from 0*/
#define ECFM_GET_U1BIT(u1Var,u1BitPos)\
   ((u1Var&(0x01<<u1BitPos))>>u1BitPos)
   
#define ECFM_GET_U2BIT(u2Var,u1BitPos)\
   ((u2Var&(0x0001<<u1BitPos))>>u1BitPos)
   
#define ECFM_GET_U4BIT(u4Var,u1BitPos)\
   ((u4Var&((UINT4)(0x00000001<<u1BitPos)))>>u1BitPos)
   
#define ECFM_CONVERT_TO_SNMP_BOOL(EcfmBool)\
((EcfmBool == ECFM_FALSE)?ECFM_SNMP_FALSE:ECFM_SNMP_TRUE)

#define ECFM_CONVERT_TO_ECFM_BOOL(SnmpBool)\
((SnmpBool == ECFM_SNMP_FALSE)?ECFM_FALSE:ECFM_TRUE)

#define ECFM_COPY_TIME_REPRESENTATION(pDstTimRep,pSrcTimeRep)\
    ECFM_MEMCPY(pDstTimRep,pSrcTimeRep,sizeof(tEcfmTimeRepresentation))
/*To check of the given byte is numeric*/ 
#define ECFM_IS_NUMBERIC(u1Val)\
        (((u1Val>=48)&&(u1Val<=57))?ECFM_TRUE:ECFM_FALSE)
#define ECFM_IS_ALPHABET(u1Val)\
        (((u1Val>=65)&&(u1Val<=90)) ||((u1Val>=97)&&(u1Val<=122))?ECFM_TRUE:ECFM_FALSE)

/* Macro to Set & GET the OpCode in the Reserved byte of Buffer Module data
 * Last 1 byte of the Reserved field is utilized for that*/
#define ECFM_BUF_SET_OPCODE(pBuf,u1OpCode)\
do \
   {\
    UINT4 u4Byte=0; \
    u4Byte = pBuf->ModuleData.u4Reserved3; \
    u4Byte = u4Byte&0xffffff00; \
    u4Byte = u4Byte|u1OpCode; \
    pBuf->ModuleData.u4Reserved3 = u4Byte;\
    }\
while(0)    
#define ECFM_BUF_GET_OPCODE(pBuf) \
    (UINT1) (pBuf->ModuleData.u4Reserved3 & 0x000000FF)
#define ECFM_BUF_SET_LEVEL(pBuf,u1Level)\
do \
   {\
    UINT4 u4Byte=0; \
    u4Byte = pBuf->ModuleData.u4Reserved2; \
    u4Byte = u4Byte&0xffffff00; \
    u4Byte = u4Byte|u1Level; \
    pBuf->ModuleData.u4Reserved2 = u4Byte;\
    }\
while(0)    
#define ECFM_BUF_GET_LEVEL(pBuf) \
    (UINT1) (pBuf->ModuleData.u4Reserved2 & 0x000000FF)
/*Macros for sender-Id TLV*/
#define ECFM_SET_CHASSIS_ID(pu1Pdu)\
do\
{\
   ECFM_PUT_1BYTE(pu1Pdu,ECFM_CHASSISID_LEN);\
   ECFM_PUT_1BYTE(pu1Pdu,ECFM_CHASSISID_SUBTYPE);\
   ECFM_MEMCPY(pu1Pdu,ECFM_CHASSISID,ECFM_CHASSISID_LEN);\
   pu1Pdu+=ECFM_CHASSISID_LEN;\
}while(0)
#define ECFM_SET_MGMT_ADDR_DOMAIN(pu1Pdu)\
do\
{\
   UINT1 u1EncodedLen=0;\
   UINT1 *pu1Len=pu1Pdu;\
   pu1Pdu++;\
   EcfmUtilEncodeAsn1BER (ECFM_MGMT_ADDR_DOMAIN,ECFM_MAX_TRANSPORT_DOMAIN_LEN,pu1Pdu,&u1EncodedLen);\
   ECFM_PUT_1BYTE(pu1Len,u1EncodedLen);\
   pu1Pdu+=u1EncodedLen;\
}while(0)

#define ECFM_SET_MGMT_ADDRESS(pu1Pdu)\
do\
{\
   ECFM_PUT_1BYTE(pu1Pdu,ECFM_MGMT_ADDRESS_LEN);\
   if (ECFM_MGMT_ADDRESS_LEN <= ECFM_MAX_MAN_ADDR_LEN) \
   { \
   ECFM_MEMCPY(pu1Pdu,ECFM_MGMT_ADDRESS,ECFM_MGMT_ADDRESS_LEN);\
   pu1Pdu+=ECFM_MGMT_ADDRESS_LEN;\
   }\
}while(0)

/* ISID conversson Macros */
#define ECFM_IS_MEP_ISID_AWARE(u4Isid)\
        (u4Isid > ECFM_INTERNAL_ISID_MIN)

#define ECFM_ISID_TO_ISID_INTERNAL(u4Isid)\
        (u4Isid + (ECFM_INTERNAL_ISID_MIN))

#define ECFM_ISID_INTERNAL_TO_ISID(u4Isid)\
        (u4Isid - (ECFM_INTERNAL_ISID_MIN))

/* Validation macro for interface index*/
#define ECFM_IS_VALID_INTERFACE(u4IfIndex)\
((((u4IfIndex>=ECFM_BRG_PLUS_LOG_PORTS_MIN)&&(u4IfIndex<=ECFM_BRG_PLUS_LOG_PORTS_MAX))||\
((u4IfIndex>=ECFM_INTERNAL_IFACES_MIN)&&((u4IfIndex<=ECFM_INTERNAL_IFACES_MAX))))?ECFM_TRUE:ECFM_FALSE)

/* For Handling of Multiple Cards */
/* NOTE: Update the values of this array if slot count can be more than 4*/
#define ECFM_SET_LC_STATUS_ON_PDU_RX(pu1LCStatus, i4SlotId) \
do\
{\
 UINT1 u1SetBit = 0x80;\
 u1SetBit >>= i4SlotId;\
 *pu1LCStatus = *pu1LCStatus | u1SetBit;\
}while(0)

#define ECFM_RESET_LC_STATUS_ON_NOTIFICATION_RX(pu1LCStatus, i4SlotId) \
do\
{\
 UINT1 u1ResetBit = 0x80;\
 u1ResetBit >>= i4SlotId;\
 u1ResetBit = ~u1ResetBit;\
 *pu1LCStatus = *pu1LCStatus & u1ResetBit;\
}while(0)
#define ECFM_CALCULATE_FRAME_LOSS(u4FirstVal,u4SecVal,u4ThirdVal,u4FourthVal,u4Res) \
do \
{ \
  UINT4 u4Temp1 = 0; \
  UINT4 u4Temp2 = 0; \
  if ( u4FirstVal > u4SecVal) \
  { \
     u4Temp1 = u4FirstVal - u4SecVal;\
  }\
  else\
  {\
     u4Temp1 = u4SecVal - u4FirstVal;\
  }\
  if (u4ThirdVal > u4FourthVal)\
  {\
     u4Temp2 = u4ThirdVal - u4FourthVal;\
  }\
  else\
  {\
     u4Temp2 = u4FourthVal - u4ThirdVal;\
  }\
  if( u4Temp1 > u4Temp2)\
  {\
     u4Res = u4Temp1 - u4Temp2;\
  }\
  else\
  {\
     u4Res = u4Temp2 - u4Temp1;\
  }\
}while(0)
#define ECFM_CC_RESET_STAGGERING_DELTA()\
do{\
    tOsixSysTime     u4CurrentTime = 0;\
    OsixGetSysTime (&u4CurrentTime);\
    gEcfmCcGlobalInfo.u4StaggeringDelta = (u4CurrentTime + ECFM_CC_STAGGERING_INTERVAL);\
}while(0)

#define ECFM_CC_RELINQUISH_QUEUE(i4StartTime, u4RelinquishThresh, u4Event, EcfmQId)\
{\
    INT4                i4CurTime = 0;\
    UINT4               u4MsgNum = 0;\
    ECFM_GET_SYS_TIME ((UINT4 *) &i4CurTime);\
    if ((i4CurTime - i4StartTime) >= u4RelinquishThresh)\
    {\
        OsixQueNumMsg (EcfmQId, &u4MsgNum);\
        if (u4MsgNum > 0)\
        {\
            ECFM_SEND_EVENT (ECFM_CC_TASK_ID, u4Event);\
        }\
        break;\
    }\
}

#define ECFM_OFFSET(x,y)  ((FS_ULONG)(&(((x *)0)->y)))

/************* OCTET_STRING conversion Macros ********************/

#define ECFM_OCTETSTRING_TO_FLOAT(pOctetString, f4Val) \
    SSCANF( (CHR1 *)pOctetString->pu1_OctetList, "%f", &f4Val)

#define ECFM_FLOAT_TO_OCTETSTRING(f4Value,pOctetString) \
    SPRINTF( (CHR1 *)pOctetString->pu1_OctetList, "%f", f4Value)


/***************************************************************************/
/* Macro's needed for Show Running Config */

#define ECFM_SRC_MODULE_NAME_LEN             18 /* show running-config module
                                                 * name */
#define ECFM_ETH_SHOW_RUNNING_CONFIG         0
#define ECFM_MPLSTP_SHOW_RUNNING_CONFIG      1
#define ECFM_MAX_SRC_MODULE                  2
#define ECFM_MAX_OID_LEN                     20
#define ECFM_RBTREE_KEY_GREATER             1
#define ECFM_RBTREE_KEY_LESSER             -1
#define ECFM_RBTREE_KEY_EQUAL               0

#define ECFM_PORT_PROPERTY_DEMUX            1

#ifndef ECFM_ARRAY_TO_RBTREE_WANTED
/*Macros for accessing PortInfo in CC and LBLT*/
/*CC*/
#define ECFM_CC_CREATE_LOCAL_PORT_TABLE(RetVal)         \
{                                                       \
    /*This RBTREE wont be present in this case */       \
    RetVal = ECFM_SUCCESS;                              \
}   

#define ECFM_CC_DELETE_LOCAL_PORT_TABLE()               \
{                                                       \
    /*This RBTREE wont be present in this case */       \
}   

#define ECFM_CC_GET_IFINDEX(u4ContextId, u2PortNum) \
    ECFM_CC_GET_CONTEXT_INFO (u4ContextId)->   \
                apPortInfo[u2PortNum - 1]->u4IfIndex

#define ECFM_CC_PORT_INFO(u2PortNum)\
        ((ECFM_CC_CURR_CONTEXT_INFO())->apPortInfo[u2PortNum -1])
 
#define ECFM_CC_SET_PORT_INFO(u2PortNum, pPortEntry)\
do \
   {\
    if ((u2PortNum > 0) && (u2PortNum <= ECFM_CC_MAX_PORT_INFO))\
    {\
        ECFM_CC_PORT_INFO(u2PortNum) = pPortEntry; \
    }\
    else if(pPortEntry != NULL)\
    {\
        ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);\
    }\
}while(0)

/*LBLT*/
#define ECFM_LBLT_CREATE_LOCAL_PORT_TABLE(RetVal)       \
{                                                       \
    /*This RBTREE wont be present in this case */       \
    RetVal = ECFM_SUCCESS;                              \
}   

#define ECFM_LBLT_DELETE_LOCAL_PORT_TABLE()             \
{                                                       \
    /*This RBTREE wont be present in this case */       \
}   

#define ECFM_LBLT_PORT_INFO(u2PortNum)\
        ((ECFM_LBLT_CURR_CONTEXT_INFO())->apPortInfo[u2PortNum-1])

#define ECFM_LBLT_SET_PORT_INFO(u2PortNum,pPortEntry)\
do \
   {\
    if ((u2PortNum > 0) && (u2PortNum <= ECFM_LBLT_MAX_PORT_INFO))\
    {\
        ECFM_LBLT_PORT_INFO(u2PortNum) = pPortEntry; \
    }\
    else if (pPortEntry != NULL)\
    {\
        ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortEntry);\
    }\
} while(0)

/*DEMUX PORTLIST*/
#define ECFM_CC_DEMUX_PORTLIST    (ECFM_CC_CURR_CONTEXT_INFO())->DeMuxPortList

#define ECFM_SET_DEMUX_PORT(u2PortNum) \
        ECFM_SET_MEMBER_PORT (ECFM_CC_DEMUX_PORTLIST, u2PortNum);

#define ECFM_RESET_DEMUX_PORT(u2PortNum)\
        ECFM_RESET_MEMBER_PORT (ECFM_CC_DEMUX_PORTLIST, u2PortNum);

#define ECFM_GET_DEMUX_PORTS(pEntry)    \
        MEMCPY (pEntry, ECFM_CC_DEMUX_PORTLIST,                     \
            sizeof(tLocalPortListExt));
#else
/*Macros added for removal of PortInfo Array and LocalPortList*/
/*CC*/
#define ECFM_CC_CREATE_LOCAL_PORT_TABLE(RetVal)         \
{                                                       \
    RetVal = ECFM_SUCCESS;                              \
    ECFM_CC_GLOBAL_LOCAL_PORT_TABLE =                   \
        EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF         \
                (tEcfmCcPortInfo, LocalPortTableNode),  \
                EcfmCcLocalPortTableGlobalCmp);         \
    if (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE == NULL)        \
    {                                                   \
        RetVal = ECFM_FAILURE;                          \
    }                                                   \
}   

#define ECFM_CC_DELETE_LOCAL_PORT_TABLE()               \
{                                                       \
    if (ECFM_CC_GLOBAL_LOCAL_PORT_TABLE != NULL)        \
    {                                                   \
        RBTreeDelete(ECFM_CC_GLOBAL_LOCAL_PORT_TABLE);  \
        ECFM_CC_GLOBAL_LOCAL_PORT_TABLE = NULL;         \
    }                                                   \
}   

#define ECFM_CC_GET_IFINDEX(u4ContextId, u2PortNum) \
    (EcfmGetCcLocalPortEntry(u4ContextId, u2PortNum))->u4IfIndex

#define ECFM_CC_PORT_INFO(u2PortNum)\
       EcfmGetCcLocalPortEntry(ECFM_CC_CURR_CONTEXT_ID(), u2PortNum)
 
#define ECFM_CC_SET_PORT_INFO(u2PortNum, pPortEntry)\
if ((u2PortNum > 0) && (u2PortNum <= ECFM_PORTS_PER_CONTEXT_MAX))\
{\
    if (pPortEntry != NULL)\
    {\
        EcfmAddCcLocalPortEntry (pPortEntry); \
    }\
    else  \
    {\
        EcfmDelCcLocalPortEntry (ECFM_CC_CURR_CONTEXT_ID(), u2PortNum); \
    }\
}\
else if(pPortEntry != NULL)\
{\
    ECFM_FREE_MEM_BLOCK (ECFM_CC_PORT_INFO_POOL, (UINT1 *) pPortEntry);\
}

/*LBLT*/
#define ECFM_LBLT_CREATE_LOCAL_PORT_TABLE(RetVal)       \
{                                                       \
    RetVal = ECFM_SUCCESS;                              \
    ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE =                 \
    EcfmRBTreeCreateEmbedded (FSAP_OFFSETOF             \
            (tEcfmLbLtPortInfo, LocalPortTableNode),    \
            EcfmLbLtLocalPortTableGlobalCmp);           \
    if (ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE == NULL)      \
    {                                                   \
        RetVal = ECFM_FAILURE;                          \
    }                                                   \
}   

#define ECFM_LBLT_DELETE_LOCAL_PORT_TABLE()             \
{                                                       \
    if (ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE != NULL)      \
    {                                                   \
        RBTreeDelete(ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE);\
        ECFM_LBLT_GLOBAL_LOCAL_PORT_TABLE = NULL;       \
    }                                                   \
}   

#define ECFM_LBLT_PORT_INFO(u2PortNum)\
       EcfmGetLbLtLocalPortEntry(ECFM_LBLT_CURR_CONTEXT_ID(), u2PortNum)

#define ECFM_LBLT_SET_PORT_INFO(u2PortNum,pPortEntry)\
if ((u2PortNum > 0) && (u2PortNum <= ECFM_PORTS_PER_CONTEXT_MAX))\
{\
    if (pPortEntry != NULL)\
    {\
        EcfmAddLbLtLocalPortEntry (pPortEntry); \
    }\
    else  \
    {\
        EcfmDelLbLtLocalPortEntry (ECFM_LBLT_CURR_CONTEXT_ID(), u2PortNum); \
    }\
}\
else if (pPortEntry != NULL)\
{\
    ECFM_FREE_MEM_BLOCK (ECFM_LBLT_PORT_INFO_POOL, (UINT1 *) pPortEntry);\
}

/*DEMUX PORTLIST*/
#define ECFM_SET_DEMUX_PORT(u2PortNum)      \
        EcfmAddSinglePortInDB(ECFM_CC_CURR_CONTEXT_ID(),    \
            u2PortNum, ECFM_PORT_PROPERTY_DEMUX)

#define ECFM_RESET_DEMUX_PORT(u2PortNum)    \
        EcfmResetSinglePortInDB(ECFM_CC_CURR_CONTEXT_ID(),    \
            u2PortNum, ECFM_PORT_PROPERTY_DEMUX)

#define ECFM_GET_DEMUX_PORTS(pEntry)        \
        EcfmGetPortsFromDB(ECFM_CC_CURR_CONTEXT_ID(),   \
            ECFM_PORT_PROPERTY_DEMUX, pEntry)
#endif

#endif /* CFM_MACS_H */
/****************************************************************************
                            End of File cfmmacs.h
 ****************************************************************************/
