/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: cfmtrap.h,v 1.8 2012/05/15 13:41:02 siva Exp $
 *
 * Description: This file contains macros for snmp traps.
 *************************************************************************/

#ifndef _ECFM_TRAP_H_
#define _ECFM_TRAP_H_


#define ECFM_CC_TRAP_CONTROL\
        (ECFM_CC_CURR_CONTEXT_INFO())->u1TrapControl

#define Y1731_CC_TRAP_CONTROL\
        (ECFM_CC_CURR_CONTEXT_INFO())->u2Y1731TrapControl

#define Y1731_LBLT_TRAP_CONTROL\
        (ECFM_LBLT_CURR_CONTEXT_INFO())->u2Y1731TrapControl

#define ECFM_TRAP_CONTROL_MAX_VAL  0xfc
#define Y1731_TRAP_CONTROL_MAX_VAL  0x7ffe

#define ECFM_RDI_CCM_TRAP_DISABLED()\
           ((ECFM_CC_TRAP_CONTROL & 0x40)?(ECFM_FALSE):(ECFM_TRUE))
#define ECFM_MAC_STATUS_TRAP_DISABLED()\
           ((ECFM_CC_TRAP_CONTROL & 0x20)?(ECFM_FALSE):(ECFM_TRUE))
#define ECFM_REMOTE_CCM_TRAP_DISABLED()\
           ((ECFM_CC_TRAP_CONTROL & 0x10)?(ECFM_FALSE):(ECFM_TRUE))
#define ECFM_ERR_CCM_TRAP_DISABLED()\
           ((ECFM_CC_TRAP_CONTROL & 0x08)?(ECFM_FALSE):(ECFM_TRUE))
#define ECFM_XCONN_CCM_TRAP_DISABLED()\
           ((ECFM_CC_TRAP_CONTROL & 0x04)?(ECFM_FALSE):(ECFM_TRUE))

#define Y1731_TST_RCVD_ERR_DISABLED()\
        ((Y1731_LBLT_TRAP_CONTROL & 0x4000)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_FRM_LOSS_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x2000)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_FRM_DELAY_TRAP_DISABLED()\
        ((Y1731_LBLT_TRAP_CONTROL & 0x1000)?(ECFM_FALSE):(ECFM_TRUE))

#define Y1731_RDI_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0800)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_LOC_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0400)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_UNEXP_PERIOD_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0200)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_UNEXP_MEP_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0100)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_MISMERGE_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0080)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_UNEXP_LEVEL_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0040)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_LLF_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0020)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_INT_HW_FAIL_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0010)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_INT_SW_FAIL_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0008)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_AIS_COND_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0004)?(ECFM_FALSE):(ECFM_TRUE))
#define Y1731_LCK_COND_TRAP_DISABLED()\
        ((Y1731_CC_TRAP_CONTROL & 0x0002)?(ECFM_FALSE):(ECFM_TRUE))

/* Type code for TRAP */
#define ECFM_FAULT_ALARM_TRAP_VAL 1 
#define ECFM_RDI_CCM_TRAP_VAL     2
#define ECFM_MAC_STATUS_TRAP_VAL  3
#define ECFM_REMOTE_CCM_TRAP_VAL  4
#define ECFM_ERROR_CCM_TRAP_VAL   5
#define ECFM_XCONN_CCM_TRAP_VAL   6

/* Type code for Y.1731 Traps entry*/
#define Y1731_FRM_LOSS_TRAP_VAL                1
#define Y1731_DEFECT_CONDN_TRAP_VAL            2
#define Y1731_TST_RCVD_ERR_TRAP_VAL            3
#define Y1731_FRM_DELAY_TRAP_VAL               4
#define Y1731_LAST_CFM_TX_FAIL_TRAP_VAL        5
#define Y1731_UNEXP_PERIOD_EN_TRAP_VAL         6
#define Y1731_UNEXP_MEP_EN_TRAP_VAL            7
#define Y1731_MISMERGE_EN_TRAP_VAL             8
#define Y1731_UNEXP_LEVEL_EN_TRAP_VAL          9
#define Y1731_LLF_EN_TRAP_VAL                 10
#define Y1731_INT_HW_FAIL_EN_TRAP_VAL         11
#define Y1731_INT_SW_FAIL_EN_TRAP_VAL         12
#define Y1731_AIS_COND_EN_TRAP_VAL            13
#define Y1731_LCK_COND_EN_TRAP_VAL            14
#define Y1731_RDI_CCM_EN_TRAP_VAL             15
#define Y1731_LOC_TRAP_EN_VAL                 16
/* Type code for Y.1731 Traps exit*/
#define Y1731_EX_TRAP_BASE_VAL                40
#define Y1731_EX_TRAP_INCR_VAL                10
#define Y1731_RDI_CCM_EX_TRAP_VAL             40
#define Y1731_LOC_TRAP_EX_VAL                 50
#define Y1731_UNEXP_PERIOD_EX_TRAP_VAL        60
#define Y1731_UNEXP_MEP_EX_TRAP_VAL           70
#define Y1731_MISMERGE_EX_TRAP_VAL            80
#define Y1731_UNEXP_LEVEL_EX_TRAP_VAL         90
#define Y1731_LLF_EX_TRAP_VAL                 100
#define Y1731_INT_HW_FAIL_EX_TRAP_VAL         110
#define Y1731_INT_SW_FAIL_EX_TRAP_VAL         120
#define Y1731_AIS_COND_EX_TRAP_VAL            130
#define Y1731_LCK_COND_EX_TRAP_VAL            140

#define ECFM_MIB_OBJ_CONTEXT_NAME              "fsMIEcfmContextName" 
#define Y1731_MIB_OBJ_CONTEXT_NAME             "fsMIY1731ContextName" 
#define Y1731_MIB_OBJ_LAST_CFM_FAIL_OPCODE     "fsMIY1731LastTxFailOpcode" 
#define Y1731_MIB_OBJ_ERROR_LOG_TYPE           "fsMIY1731ErrorLogType"
#define Y1731_MIB_OBJ_FD_MEASUR_TIMESTAMP      "fsMIY1731FdMeasurementTimeStamp"
#define Y1731_MIB_OBJ_FD_MEASUR_TYPE           "fsMIY1731FdMeasurementType"
#define Y1731_MIB_OBJ_BIT_ERR_TST_IN           "fsMIY1731MepBitErroredTstIn"
#define Y1731_MIB_OBJ_FD_MEASUR_DELAY_VAL      "fsMIY1731FdDelayValue"
#define Y1731_MIB_OBJ_FL_MEASUR_TIMESTAMP      "fsMIY1731FlMeasurementTimeStamp"
#define Y1731_MIB_OBJ_FL_MEASUR_TYPE           "fsMIY1731FlStatsMeasurementType"
#define Y1731_MIB_OBJ_FL_FAR_END_LOSS          "fsMIY1731FlFarEndLoss"
#define Y1731_MIB_OBJ_FL_NEAR_END_LOSS         "fsMIY1731FlNearEndLoss"
#ifdef MI_WANTED 
#define ECFM_MIB_OBJ_TRAP_TYPE                 "fsMIEcfmTrapType"
#define ECFM_MIB_OBJ_MEP_HIGHEST_PRIORITY_DEF  "fsMIEcfmMepHighestPrDefect"
#else
#define ECFM_MIB_OBJ_TRAP_TYPE                 "fsEcfmTrapType"
#define ECFM_MIB_OBJ_MEP_HIGHEST_PRIORITY_DEF  "dot1agCfmMepHighestPrDefect"
#endif

PUBLIC VOID EcfmMISnmpIfSendTrap PROTO ((tEcfmCcPduSmInfo *, UINT1));
PUBLIC VOID EcfmSnmpIfSendTrap PROTO ((tEcfmCcPduSmInfo *, UINT1 ));
PUBLIC VOID Y1731CcSnmpIfSendTrap PROTO ((VOID *, UINT1));
PUBLIC VOID Y1731LbLtSnmpIfSendTrap PROTO ((VOID *, UINT1));
PUBLIC VOID Y1731LbLtSnmpIfSendTxFailTrap PROTO ((UINT2, UINT1));
/******************************************************************************/
/*                           End  of file cfmtrap.h                           */
/******************************************************************************/
#endif /*_ECFM_TRAP_H_*/


