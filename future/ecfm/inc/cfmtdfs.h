/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmtdfs.h,v 1.27 2012/03/27 12:36:42 siva Exp $
 * 
 * Description: This file contains data structures defined for 
 *              CC Task
 *********************************************************************/

#ifndef _CFM_TDFS_H
#define _CFM_TDFS_H
/****************************************************************************/
typedef tCRU_INTERFACE            tEcfmInterface;
typedef tCfaIfInfo                tEcfmCfaIfInfo;
typedef tTMO_SLL                  tEcfmSll;
typedef tTMO_SLL_NODE             tEcfmSllNode;
typedef tTMO_DLL                  tEcfmDll;
typedef tTMO_DLL_NODE             tEcfmDllNode;
typedef tRBTree                   tEcfmRBTree;
typedef tRBNodeEmbd               tEcfmRBNodeEmbd;
typedef UINT4                     tEcfmMsgType;
typedef tMacAddr                  tEcfmMacAddr;
typedef tTmrAppTimer              tEcfmAppTimer;
typedef tVlanTagInfo              tEcfmVlanTagInfo;
typedef tFilterEntry              tEcfmFilterEntry;
typedef enum {
   ECFM_VLAN_TAG_INFO=1,
   ECFM_ISID_TAG_INFO
}tEcfmTagType;
typedef tPbbTag           tEcfmPbbTag;
typedef tIsidTagInfo          tEcfmIsidTagInfo; 
typedef tVlanTag           tEcfmVlanTag;
typedef UINT4 tEcfmNodeStatus;
#ifdef L2RED_WANTED

/* To handle message/events given by redundancy manager. */
typedef struct {
    tRmMsg           *pFrame;     /* Message given by RM module. */
    UINT2            u2Length;    /* Length of message given by RM module.*/
    UINT1            u1Event;     /* Event given by RM module. */
    UINT1            u1Reserved;
}tEcfmRedRmFrame;
#endif

/****************************************************************************/
/*ECFM String*/
typedef struct EcfmString
{
    UINT1   *pu1Octets;
    UINT4   u4OctLen;
}tEcfmString; 
/****************************************************************************/
/*SenderIdTlv Address */
typedef struct EcfmSenderId
{
   tEcfmString  ChassisId;           /* Chassis Id*/
   tEcfmString  MgmtAddressDomain;   /* Management Address Domain*/
   tEcfmString  MgmtAddress;         /* Management Address */
   UINT1        u1ChassisIdSubType;  /* Chassis Id Subtype*/
   UINT1        au1Pad[3];
} tEcfmSenderId;
/****************************************************************************/
/*Organization Specific Value */
typedef struct EcfmOrgSpecific
{
   UINT1        au1Oui[ECFM_OUI_LENGTH]; /* Organization Unique Identifier*/
   UINT1        u1SubType;                   /* Sub-Type*/
   tEcfmString  Value;                       /* Additional Organizational 
                                              * Specific Information Maximum 
                                              * length as 252
                                              */
}tEcfmOrgSpecific;
/**************************************************************************/
/*For External Pdu*/
typedef struct ExPduMsg
{
   UINT1      *pu1Data;
   UINT4       u4DataLen;
   UINT4       u4VidIsid;      /* VLAN ID of the MEP*/
   UINT2       u2PortNum;
   UINT1       u1TlvOffset;
   UINT1       u1SubOpCode;
   UINT1       u1Direction;/*  Direction of the MEP*/
   UINT1       u1MdLevel;  /*  MD Level of the MEP*/
   UINT1       u1PduFlag;
   tMacAddr    TxDestMacAddr;
   UINT1       u1Version; /* Version is added here for filling Version Field in
                           * ECFM header for R-APS packets based on the version
                           * passed by ERPS to support G.8032 Versions: 1 and 2.
                           */
   UINT1       au1Pad[2];
}tExternalPduMsg;
/****************************************************************************/
/* Time Representation*/
typedef struct
{
   UINT4        u4Seconds;         /* Time units in seconds*/
   UINT4        u4NanoSeconds;     /* Time units in nano seconds*/
}tEcfmTimeRepresentation;
/****************************************************************************/
/* Structure used to store the current MEP prompt info*/
typedef struct EcfmMepPromptInfo
{
   UINT4    u4MdIndex;
   UINT4    u4MaIndex;
   UINT4    u4MepIndex;
}tEcfmMepPromptInfo;

/* Sizing Params enum */
enum
{
   ECFM_CC_MSG_SIZING_ID,
   ECFM_CC_CONTEXT_INFO_SIZING_ID,
   ECFM_CC_VLAN_INFO_SIZING_ID,
   ECFM_CC_PORT_INFO_SIZING_ID,
   ECFM_CC_STACK_INFO_SIZING_ID,
   ECFM_CC_MD_INFO_SIZING_ID,
   ECFM_CC_DEF_MD_INFO_SIZING_ID,
   ECFM_CC_MA_INFO_SIZING_ID, 
   ECFM_CC_MEP_LIST_SIZING_ID,
   ECFM_CC_MEP_INFO_SIZING_ID,
   ECFM_CC_MEP_MPTP_PARAMS_SIZING_ID,
   ECFM_CC_MIP_INFO_SIZING_ID,
   ECFM_CC_MIP_PREVENT_SIZING_ID,
   ECFM_CC_RMEP_CCM_DB_SIZING_ID,
   ECFM_CC_CFG_ERR_INFO_SIZING_ID,
   ECFM_CC_REG_INFO_SIZING_ID,
   ECFM_CC_HW_PORTARRAY_SIZING_ID,
   ECFM_LBLT_MSG_INFO_SIZING_ID,
   ECFM_LBLT_CONTEXT_INFO_SIZING_ID,
   ECFM_LBLT_PORT_INFO_SIZING_ID,
   ECFM_LBLT_MEP_INFO_SIZING_ID,
   ECFM_LBLT_MIP_INFO_SIZING_ID,
   ECFM_LBLT_STACK_INFO_SIZING_ID,
   ECFM_LBLT_LTM_RPY_INFO_SIZING_ID, 
   ECFM_LBLT_LBM_INFO_SIZING_ID, 
   ECFM_LBLT_RMEP_DB_INFO_SIZING_ID, 
   ECFM_LBLT_MA_INFO_SIZING_ID, 
   ECFM_LBLT_MD_INFO_SIZING_ID, 
   ECFM_LBLT_DELAY_Q_NODE_SIZING_ID, 
   ECFM_LBLT_DEF_MD_INFO_SIZING_ID,
   ECFM_LBLT_REG_INFO_SIZING_ID,
   ECFM_CC_AIS_PDU_ID,
   ECFM_CC_LCK_PDU_ID,
   ECFM_MPLSTP_OUTPARAMS_SIZING_ID,
   ECFM_MPLSTP_INPARAMS_SIZING_ID,
   ECFM_MPLSTP_PATHINFO_SIZING_ID,
   ECFM_MPLSTP_SERVICE_PTR_SIZING_ID,
   ECFM_MPTP_LBLT_PATHINFO_SIZING_ID,
   ECFM_MPTP_LBLT_OUTPARAMS_SIZING_ID,
   ECFM_MPTP_LBLT_INPARAMS_SIZING_ID,
   ECFM_CC_RMEP_CHASSIS_SIZING_ID,
   ECFM_CC_RMEP_MGMT_ADDR_DOMAIN_SIZING_ID,
   ECFM_CC_RMEP_MGMT_ADDR_SIZING_ID,
   ECFM_LBLT_MEP_LBM_PDU_SIZING_ID,
   ECFM_LBLT_MEP_LBM_DATA_TLV_SIZING_ID,
   ECFM_LTR_CACHE_INDICES_SIZING_ID,
   ECFM_AVLBLTY_INFO_SIZING_ID,
   ECFM_CC_MEP_ERR_CCM_SIZING_ID,
   ECFM_CC_MEP_XCON_CCM_SIZING_ID,
   ECFM_LBR_RCVD_INFO_SIZING_ID,
   ECFM_NO_SIZING_ID
};

/*****************************************************************************/
/* Various Timer Types  */
enum
{
   ECFM_CC_TMR_CCI_WHILE = 0,       /* CCI while timer */
   ECFM_CC_TMR_ERR_CCM_WHILE = 1,   /* Timer for the RMEP Error  
                                     * SEM 
                                     */
   ECFM_CC_TMR_XCON_CCM_WHILE = 2,  /* Timer for XCON MEP */
   ECFM_CC_TMR_FNG_WHILE =      3,  /* Timer for FNG SEM */
   ECFM_CC_TMR_FNG_RESET_WHILE =4,  /* Timer for FNG SEM*/
   ECFM_CC_TMR_MIP_DB_HOLD = 5,     /* MIP CCM DB Hold Timer */ 

   ECFM_CC_TMR_MEP_ARCHIVE_HOLD = 6,            

   ECFM_CC_TMR_RDI_PERIOD = 7,      /* Timer for RDI Period */ 
   ECFM_CC_TMR_LM_WHILE = 8,        /* Timer for LM while */
   ECFM_CC_TMR_LM_DEADLINE = 9,     /* Timer for LM Deadline */

   ECFM_CC_TMR_AIS_PERIOD = 10,     /* Timer for AIS Period */ 

   ECFM_CC_TMR_AIS_INTERVAL = 11,   /* Timer for AIS Interval */ 

   ECFM_CC_TMR_AIS_RXWHILE = 12,    /* AIS RxWhile Timer */ 

   ECFM_CC_TMR_LCK_PERIOD = 13,     /* Timer for LCK Period */ 

   ECFM_CC_TMR_LCK_INTERVAL = 14,   /* Timer for LCK Interval */ 

   ECFM_CC_TMR_LCK_RXWHILE = 15,    /* LCK RxWhile Timer */ 

   ECFM_CC_TMR_LCK_DELAY = 16,      /* LCK Delay Timer */ 
   ECFM_CC_TMR_RMEP_WHILE = 17,      /* Timer for RMEP */ 

   ECFM_CC_TMR_MAX_TYPES = 18       /* Max Timers */ 
};

/*****************************************************************************/
/* Timer Types */
enum
{

    ECFM_LBLT_TMR_DELAY_QUEUE  = 0,    /* A timer variable used by the LTR 
                                         * Transmitter state machine to time out 
                                         * the  expected transmission of LTRs.
                                         */
    ECFM_LBLT_TMR_LTM_TX  =     1,    /* Timer maintained for the transmission 
                                         * of LTMs. When it get times out 
                                         * TransactionId of LTM is incremented.
                                         */
    ECFM_LBLT_TMR_LBI_INTERVAL  = 2,    /* A timer variable used by the MEP 
                                         * Loopback Initiator transmit state 
                                         * machine to transmit next LBM and to
                                         * time out the expected 
                                         * reception of LBRs. 
                                         */
    ECFM_LBLT_TMR_LTR_HOLD   =  3,    /* Timer maintained for which the LTR entries 
                                         * are maintained in the LTR Cache */
    ECFM_LBLT_TMR_LBI_DEADLINE = 4,    /* Timer maintained for LBM transmission timeout*/
    ECFM_LBLT_TMR_LBR_HOLD     = 5,    /* Timer maintained for which the LBR
                                           entries are maintained in the LBR
                                           Table */
    ECFM_LBLT_TMR_DM_DEADLINE  = 6,   /* Timer for DM Deadline*/
    ECFM_LBLT_TMR_DM_INTERVAL  = 7,   /* Timer for DM Interval*/
    ECFM_LBLT_TMR_1DM_TRANS_INTERVAL = 8,  /* Timer for 1DM Transaction Interval */
    ECFM_LBLT_TMR_TST_INTERVAL = 9,    /* A timer variable used by the MEP 
                                         * TST Initiator to transmit next TST
                                         */
    ECFM_LBLT_TMR_TST_DEADLINE = 10,    /* Timer maintained for TST transmission timeout*/
    ECFM_LBLT_TMR_LBR_TIMEOUT  = 11,    /* Timer maintained for LBR Recption Timeout*/

    ECFM_LBLT_TMR_TH_DEADLINE  = 12,   /* Timer for TH Deadline*/
    ECFM_LBLT_TMR_VSR_TIMEOUT  = 13,   /* Timer for VSR Timeout*/
    ECFM_LBLT_TMR_TH_GET_TST_RX_COUNT = 14,   /* Timer to get TST Rx Count*/
    ECFM_LBLT_TMR_MAX_TYPES = 15   /* Timer to get TST Rx Count*/
};

/* BELOW STRUCTURE IS USED FOR MEMPOOL CREATION ONLY*/
/* START */
typedef struct CfmCcPortArray{
    UINT4  au4Ports[ECFM_MAX_PORTS_PER_CONTEXT];
}tCfmCcPortArray;

typedef enum {
    ECFM_CTX_AIS_IN,
    ECFM_CTX_AIS_OUT,
    ECFM_CTX_LCK_IN,
    ECFM_CTX_LCK_OUT,
    ECFM_CTX_TST_IN,
    ECFM_CTX_TST_OUT,
    ECFM_CTX_LMM_IN,
    ECFM_CTX_LMM_OUT,
    ECFM_CTX_LMR_IN,
    ECFM_CTX_LMR_OUT,
    ECFM_CTX_CCM_IN,
    ECFM_CTX_CCM_OUT,
    ECFM_CTX_DM_IN,
    ECFM_CTX_DM_OUT,
    ECFM_CTX_DMM_IN,
    ECFM_CTX_DMM_OUT,
    ECFM_CTX_DMR_IN,
    ECFM_CTX_DMR_OUT,
    ECFM_CTX_APS_IN,
    ECFM_CTX_APS_OUT,
    ECFM_CTX_MCC_IN,
    ECFM_CTX_MCC_OUT,
    ECFM_CTX_VSM_IN,
    ECFM_CTX_VSM_OUT,
    ECFM_CTX_VSR_IN,
    ECFM_CTX_VSR_OUT,
    ECFM_CTX_EXM_IN,
    ECFM_CTX_EXM_OUT,
    ECFM_CTX_EXR_IN,
    ECFM_CTX_EXR_OUT,
    ECFM_CTX_FAILEDOPCODE_OUT,
    ECFM_CTX_LBM_IN,
    ECFM_CTX_LBM_OUT,
    ECFM_CTX_LBR_IN,
    ECFM_CTX_LBR_OUT,
    ECFM_CTX_LTM_IN,
    ECFM_CTX_LTM_OUT,
    ECFM_CTX_LTR_IN,
    ECFM_CTX_LTR_OUT,
    ECFM_CTX_CFM_OUT,
    ECFM_CTX_CFM_IN,
    ECFM_CTX_FAILED_COUNT,
    ECFM_CTX_BAD_PDU,
    ECFM_CTX_FWD_PDU,
    ECFM_CTX_DSRD_PDU
}tEcfmctxStats;
/*END */
#endif /* _CFM_TDFS_H */
/****************************************************************************
  End of File cfmtdfs.h
 ****************************************************************************/
