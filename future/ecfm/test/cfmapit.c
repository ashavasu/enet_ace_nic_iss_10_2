/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmapit.c,v 1.3 2008/12/02 15:10:10 prabuc-iss Exp $
 *
 * Description: This file contains the Stubs for testing APIs 
 *******************************************************************/
#include "cfminc.h"
#include "cfmapit.h"

/*PUBLIC INT4 EcfmInitiateStubsForApiTest();*/
/*******************************************************************************
 * Function           : EcfmInitiateStubsForApiTest
 *
 * Description        : This routine initiates testing of APIs
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the 
 *                      information regarding mp info, the PDU if received and 
 *                      other information related to the functionality.
 :*                      
 * Output(s)          : None
 *
 * Returns            : ECFM_SUCCESS / ECFM_FAILURE
 ******************************************************************************/
VOID
EcfmInitiateStubsForApiTest (UINT1 choice)
{
    tMacAddr            DestMacAddr = { 0x0, 0x1, 0x2, 0x3, 0x4, 0x9 };
    tEcfmMepInfoParams  EcfmMepInfoParams;
    tEcfmConfigTstInfo  EcfmConfigTstInfo;
    tEcfmConfigLbmInfo  EcfmConfigLbmInfo;
    UINT4               u4Period = 5;
    UINT4               u4NearEndThreshold = 0;
    UINT4               u4FarEndThreshold = 0;
    UINT4               u4Deadline = 10;
    UINT4               u4Threshold = 300;
    UINT4               u4NumObservation;
    UINT4               u4TxFcf;
    UINT4               u4TxFcb;
    UINT4               u4RxFcf;
    UINT2               u2NumObservation = 2;
    UINT2               u2Interval = 1;
    UINT1               u1Interval = 1;
    UINT1               u1Status = 2;
    UINT1               u1Type = 1;
    UINT1               u1VlanPriority = 7;
    BOOL1               b1Enable = TRUE;
    UINT1               u1Enable;

    UINT1               u1Data[] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x5, 0x06, 0x07, 0x08, 0x09
    };
    char               *data;
    data = (char *) malloc (10);

    data[0] = 0x00;
    data[1] = 0x01;
    data[2] = 0x02;
    data[3] = 0x03;
    data[4] = 0x04;
    data[5] = 0x05;
    data[6] = 0x06;
    data[7] = 0x07;
    data[8] = 0x08;
    data[9] = 0x09;

/*     UINT1 au1Data[10];
   UINT1 au1DestAddr[10]= {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09};

     MEMCPY(au1Data, au1DestAddr, 10);*/
    switch (choice)
    {
        case 50:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;

            if (EcfmGetLmCounters
                (&EcfmMepInfoParams, &u4TxFcf, &u4TxFcb,
                 &u4RxFcf) == ECFM_FAILURE)
            {
                printf ("TC 50 fail\n ");
            }
            else
            {
                printf ("TC 50 Pass\n");
            }
            break;

        case 33:

            EcfmMepInfoParams.u4ContextId = 2;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Enable = 1;
            u1Interval = 1;
            u4Period = 0;
            if (EcfmSetAisCapability
                (&EcfmMepInfoParams, u1Enable, u1Interval,
                 u4Period) == ECFM_FAILURE)
            {
                printf ("TC 33 fail\n ");
            }
            else
            {
                printf ("TC 33 Pass\n");
            }
            break;

        case 34:

            EcfmMepInfoParams.u4ContextId = 2;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Enable = 2;
            u1Interval = 0;
            u4Period = 0;
            if (EcfmSetAisCapability
                (&EcfmMepInfoParams, u1Enable, u1Interval,
                 u4Period) == ECFM_FAILURE)
            {
                printf ("TC 34 fail\n ");
            }
            else
            {
                printf ("TC 34 Pass\n");
            }
            break;

        case 35:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Enable = 1;
            u1Interval = 1;
            u4Period = 0;
            if (EcfmSetOutOfService
                (&EcfmMepInfoParams, b1Enable, u1Interval,
                 u4Period) == ECFM_FAILURE)
            {
                printf ("TC 35 fails\n");
            }
            else
            {
                printf ("TC 35 Pass\n");
            }

            break;

        case 152:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u4NearEndThreshold = 10;
            u4FarEndThreshold = 20;

            if (EcfmSetLmThreshold
                (&EcfmMepInfoParams,
                 u4NearEndThreshold, u4FarEndThreshold) == ECFM_FAILURE)
            {
                printf ("TC 152 fails \n");
            }
            else
            {
                printf ("TC 152 Pass\n");
            }
            break;

        case 11:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Interval = 2;
            u2NumObservation = 2;
            u4Deadline = 0;

            if (EcfmConfigLm (&EcfmMepInfoParams, DestMacAddr,
                              u1Interval, u2NumObservation,
                              u4Deadline) == ECFM_FAILURE)
            {
                printf ("TC 11 fails \n");
            }
            else
            {
                printf (" TC 11 Pass\n");
            }
            break;

        case 12:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Interval = 2;
            u2NumObservation = 0;
            u4Deadline = 5;

            if (EcfmConfigLm (&EcfmMepInfoParams, DestMacAddr,
                              u1Interval, u2NumObservation,
                              u4Deadline) == ECFM_FAILURE)
            {
                printf ("TC 12 fails \n");
            }
            else
            {
                printf (" TC 12 Pass\n");
            }
            break;

        case 32:

            EcfmMepInfoParams.u4ContextId = 2;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Enable = 2;
            u1Interval = 0;
            u4Period = 0;
            u4Period = 20;
            if (EcfmSetRdiCapability
                (&EcfmMepInfoParams,
                 u1Enable, u1Interval, u4Period) == ECFM_FAILURE)
            {
                printf ("TC 32 fails \n");
            }
            else
            {
                printf (" TC 32 Pass\n");
            }
            break;

        case 21:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;

            EcfmConfigLbmInfo.u1Status = 2;
            EcfmConfigLbmInfo.u1TstTlvPatterType = 3;
            EcfmConfigLbmInfo.u2TstTlvPatterSize = 0;
            EcfmConfigLbmInfo.u1TlvOrNone = 2;
            EcfmConfigLbmInfo.pu1DataTlvData = &u1Data[0];
            EcfmConfigLbmInfo.b1VariableByte = TRUE;
            EcfmConfigLbmInfo.b1DropEnable = TRUE;
            EcfmConfigLbmInfo.u2TxLbmMessages = 5;
            EcfmConfigLbmInfo.u4DataTlvSize = 0;
            EcfmConfigLbmInfo.u4LbmInterval = 1;
            EcfmConfigLbmInfo.u4Deadline = 0;

            ECFM_MEMCPY (EcfmConfigLbmInfo.DestMacAddr, DestMacAddr, 6);
            if (EcfmConfigLbm (&EcfmMepInfoParams, &EcfmConfigLbmInfo) ==
                ECFM_FAILURE)

            {
                printf ("TC 21 fails \n");
            }
            else
            {
                printf ("TC 21 pass\n");
            }
            break;

        case 22:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            EcfmConfigLbmInfo.u1Status = 2;
            EcfmConfigLbmInfo.u1TstTlvPatterType = 3;
            EcfmConfigLbmInfo.u2TstTlvPatterSize = 0;
            EcfmConfigLbmInfo.u1TlvOrNone = 1;
            EcfmConfigLbmInfo.pu1DataTlvData = (UINT1 *) (&data[0]);

            EcfmConfigLbmInfo.b1VariableByte = TRUE;
            EcfmConfigLbmInfo.b1DropEnable = TRUE;
            EcfmConfigLbmInfo.u2TxLbmMessages = 0;
            EcfmConfigLbmInfo.u4DataTlvSize = 100;
            EcfmConfigLbmInfo.u4LbmInterval = 1;
            EcfmConfigLbmInfo.u4Deadline = 10;
            ECFM_MEMCPY (EcfmConfigLbmInfo.DestMacAddr, DestMacAddr, 6);
            if (EcfmConfigLbm (&EcfmMepInfoParams, &EcfmConfigLbmInfo) ==
                ECFM_FAILURE)

            {
                printf ("TC 22 fails \n");
            }
            else
            {
                printf ("TC 22 pass\n");
            }
            break;
        case 23:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            EcfmConfigTstInfo.u1Status = 2;
            EcfmConfigTstInfo.u1TstTlvPatterType = 3;
            /*u1TstPatterSize=33 */
            EcfmConfigTstInfo.b1VariableByte = TRUE;
            EcfmConfigTstInfo.b1DropEnable = FALSE;
            EcfmConfigTstInfo.u4TxTstMessages = 5;
            EcfmConfigTstInfo.u4TstInterval = 1;
            EcfmConfigTstInfo.u2TstTlvPatterSize = 0;
            EcfmConfigTstInfo.u4Deadline = 0;
            ECFM_MEMCPY (EcfmConfigTstInfo.DestMacAddr, DestMacAddr, 6);
            if (EcfmConfigTst (&EcfmMepInfoParams, &EcfmConfigTstInfo) ==
                ECFM_FAILURE)

            {
                printf ("TC 23 fails \n");
            }
            else
            {
                printf ("TC 23 pass \n");
            }
            break;
        case 151:
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u4Threshold = 500;

            if (EcfmSetDmThreshold
                (&EcfmMepInfoParams, u4Threshold) == ECFM_FAILURE)

            {
                printf ("TC 151 fails \n");
            }
            else
            {
                printf ("TC 151 pass \n");
            }
            break;

        case 13:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Status = 2;
            u4NumObservation = 2;
            u1Type = ECFM_LBLT_DM_TYPE_DMM;
            u2Interval = 100;
            u4Deadline = 0;
            u1VlanPriority = ECFM_INIT_VAL;
            if (EcfmConfigDm (&EcfmMepInfoParams, u1Status, DestMacAddr,
                              u1Type, u2Interval, u4NumObservation,
                              u4Deadline, u1VlanPriority) == ECFM_FAILURE)
            {
                printf ("TC 13 fails \n");
            }
            else
            {
                printf ("TC 13 pass\n");
            }
            break;

        case 14:

            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 1;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u4VlanIdIsid = 5;
            EcfmMepInfoParams.u1Direction = 1;
            u1Status = 2;
            u4NumObservation = 0;
            u1Type = ECFM_LBLT_DM_TYPE_1DM;
            u2Interval = 100;
            u4Deadline = 5;
            u1VlanPriority = ECFM_INIT_VAL;
            if (EcfmConfigDm (&EcfmMepInfoParams, u1Status, DestMacAddr,
                              u1Type, u2Interval, u4NumObservation,
                              u4Deadline, u1VlanPriority) == ECFM_FAILURE)
            {
                printf ("TC 14 fails \n");
            }
            else
            {
                printf ("TC 14 pass\n");
            }
    }
}

/*****************************************************************************
  End of File testapi.c
 ******************************************************************************/
