#include "cfminc.h"
#include "cfmapit.h"

#define APPLICATION_DUMMY1 1;
#define APPLICATION_DUMMY2 2;

UINT4               u4Event;
void
Dummy1Register (UINT4 u4ContextId, UINT1 choice)
{
    tEcfmRegParams      DummyRegister;

    ECFM_MEMSET (&DummyRegister, ECFM_INIT_VAL, sizeof (tEcfmRegParams));

    DummyRegister.u4ModId = APPLICATION_DUMMY1;

    switch (choice)
    {

        case 1:
            DummyRegister.u4EventsId =
                ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD |
                ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD;
            break;

        case 2:
            DummyRegister.u4EventsId = ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD;
            break;

        case 3:
            DummyRegister.u4EventsId = ECFM_DEFECT_CONDITION_ENCOUNTERED;
            break;

        case 4:
            DummyRegister.u4EventsId = ECFM_DEFECT_CONDITION_ENCOUNTERED;
            break;

        case 5:
            DummyRegister.u4EventsId = ECFM_CHECKSUM_COMPARISON_FAILED;
            break;

        case 6:
            DummyRegister.u4EventsId =
                ECFM_AIS_CONDITION_ENCOUNTERED | ECFM_AIS_CONDITION_CLEARED;
            break;

        case 7:
            DummyRegister.u4EventsId = ECFM_AIS_CONDITION_CLEARED;
            break;

        case 8:
            DummyRegister.u4EventsId =
                ECFM_LCK_CONDITION_ENCOUNTERED | ECFM_LCK_CONDITION_CLEARED;
            break;

        case 9:
            DummyRegister.u4EventsId = ECFM_LCK_CONDITION_CLEARED;
            break;

        case 10:
            DummyRegister.u4EventsId =
                ECFM_RDI_CONDITION_ENCOUNTERED | ECFM_RDI_CONDITION_CLEARED;
            break;

        case 11:
            DummyRegister.u4EventsId = ECFM_RDI_CONDITION_CLEARED;
            break;
        case 12:
            DummyRegister.u4EventsId =
                ECFM_MCC_FRAME_RECEIVED |
                ECFM_APS_FRAME_RECEIVED |
                ECFM_EXM_FRAME_RECEIVED |
                ECFM_EXR_FRAME_RECEIVED |
                ECFM_VSM_FRAME_RECEIVED | ECFM_VSR_FRAME_RECEIVED;

    }

    printf ("DummyRegister.u4EventsId=%04x\n", DummyRegister.u4EventsId);
    DummyRegister.pFnRcvPkt = CallbackDummy2;

    if (EcfmRegisterProtocols (&DummyRegister, u4ContextId) == ECFM_FAILURE)
    {
        printf ("Dummy 1 Module Not Regisetered\n");
    }
    else
    {
        printf ("Dummy 1 Registered\n");
    }

}

void
Dummy2Register (UINT4 u4ContextId)
{
    tEcfmRegParams      DummyRegister;

    ECFM_MEMSET (&DummyRegister, ECFM_INIT_VAL, sizeof (tEcfmRegParams));

    DummyRegister.u4ModId = APPLICATION_DUMMY2;

    DummyRegister.u4EventsId = ECFM_DEFECT_CONDITION_ENCOUNTERED |
        ECFM_APS_FRAME_RECEIVED;
    printf ("DummyRegister.u4EventsId=%d\n", DummyRegister.u4EventsId);
    DummyRegister.pFnRcvPkt = CallbackDummy1;
    u4ContextId = 0;

    if (EcfmRegisterProtocols (&DummyRegister, u4ContextId) == ECFM_FAILURE)
    {
        printf ("Dummy 2 Module Not Regisetered\n");
    }
    else
    {
        printf ("Dummy 2 Registered\n");
    }

}

void
CallbackDummy1 (tEcfmEventNotification * pReceivedNotification)
{

    u4Event = pReceivedNotification->u4Event;

    printf ("call back Arrived to dummy2 \n");
    printf ("Printing one of values received in  tEcfmEventNotification");

    printf ("u4Event%d", u4Event);

}

void
CallbackDummy2 (tEcfmEventNotification * pReceivedNotification)
{

    u4Event = pReceivedNotification->u4Event;

    printf ("call back Arrived to dummy 1 \n");
    printf ("and Printing one of values received intEcfmEventNotification\n");
    printf ("u4Event%d\n", pReceivedNotification->u4Event);

}

void
Dummy1DeRegister (void)
{
    UINT4               u4ModId;
    UINT4               u4ContextId = 1;

    u4ModId = APPLICATION_DUMMY1;

    if (EcfmDeRegisterProtocols (u4ModId, u4ContextId) == ECFM_FAILURE)
    {
        printf ("Dummy 1 Module Not DeRegisetered\n");
    }
    else
    {
        printf ("Dummy 1 DeRegistered\n");
    }
}

void
Dummy2DeRegister (void)
{
    UINT4               u4ModId;
    UINT4               u4ContextId = 1;

    u4ModId = APPLICATION_DUMMY2;

    if (EcfmDeRegisterProtocols (u4ModId, u4ContextId) == ECFM_FAILURE)
    {
        printf ("Dummy 2 Module Not DeRegisetered\n");
    }
    else
    {
        printf ("Dummy 2 DeRegistered\n");
    }

}

void
EcfmPrintNotiCli (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\r\n %d\n\n", u4Event);
}

void
EcfmTestNotifyProtocols (UINT1 u1Choice)
{
    tEcfmCmnMepInfo     EcfmParams;
    tEcfmBufChainHeader u1DataPtr;
    UINT4               u4Event = ECFM_DEFECT_CONDITION_ENCOUNTERED;
    tMacAddr            MacAddr;
    UINT4               u4Var = 0;
    UINT1               u1Flag = 0;
    UINT1               u1Offset = 4;
    UINT1               u1CfmPduOffset = 0;
    UINT1               u1OuiPtr = 0;
    UINT1               u1SubOpcodeVal = 0;

    EcfmParams.u2PortNum = 1;
    EcfmParams.u4VlanIdIsid = 1;
    EcfmParams.u1MdLevel = 0;
    EcfmParams.u1Direction = 1;

    EcfmParams.u4MdIndex = 1;
    EcfmParams.u4MaIndex = 1;
    EcfmParams.u2MepId = 1;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));

    switch (u1Choice)
    {
        case 1:
            MEMCPY (&MacAddr, "00:11:22:33:44:55", sizeof (tMacAddr));
            EcfmUtilPrepareNotification (u4Event, &EcfmParams, &MacAddr, u4Var,
                                         u1Flag, u1Offset, &u1DataPtr,
                                         u1CfmPduOffset, &u1OuiPtr,
                                         u1SubOpcodeVal, ECFM_CC_TASK_ID);
            break;
        case 2:
            EcfmUtilPrepareNotification (u4Event, &EcfmParams, NULL, u4Var,
                                         u1Flag, u1Offset, &u1DataPtr,
                                         u1CfmPduOffset, &u1OuiPtr,
                                         u1SubOpcodeVal, ECFM_CC_TASK_ID);
            break;

    }

}
