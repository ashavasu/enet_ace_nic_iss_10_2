#include "cli.h"
PUBLIC VOID  Dummy1Register PROTO ((UINT4,UINT1));
PUBLIC VOID         Dummy2DeRegister PROTO ((VOID));
PUBLIC VOID         Dummy2Register PROTO ((UINT4));
PUBLIC VOID         Dummy1DeRegister PROTO ((VOID));
PUBLIC VOID         EcfmPrintNotiCli PROTO ((tCliHandle));
PUBLIC VOID        CallbackDummy1 PROTO ((tEcfmEventNotification *));
PUBLIC VOID         CallbackDummy2 PROTO ((tEcfmEventNotification *));
