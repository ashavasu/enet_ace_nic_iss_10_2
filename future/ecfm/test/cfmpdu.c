#include "cfminc.h"
#include "cfmapit.h"

VOID
EcfmInitiateStubsForPduTest (UINT1 choice)
{

    tEcfmMepInfoParams  EcfmMepInfoParams;
    UINT1               u1TlvOffset;
    UINT1               u1SubOpCode;
    UINT4               u4DataLen;
    tMacAddr            TxDestMacAddr = { 0x0, 0x1, 0x2, 0x3, 0x4, 0xb };

    UINT1               u1Data[] = { 0x01, 0x01, 0x01, 0x01 };

    switch (choice)
    {

        case 1:
            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 41;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;

            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams, u1TlvOffset, u1SubOpCode, &u1Data[0],
                 u4DataLen, TxDestMacAddr, ECFM_OPCODE_MCC) == ECFM_SUCCESS)
            {
                printf ("MCC test case 1 pass\n");
            }
            else
            {
                printf ("MCC test case 1 FAIL\n");
            }

            break;

        case 101:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 41;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 2;

            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams, u1TlvOffset, u1SubOpCode, &u1Data[0],
                 u4DataLen, TxDestMacAddr, ECFM_OPCODE_MCC) == ECFM_SUCCESS)
            {
                printf ("MCC test case 1 pass\n");
            }
            else
            {
                printf ("MCC test case 1 FAIL\n");
            }

            break;

        case 2:
            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 39;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 2;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams, u1TlvOffset, u1SubOpCode, &u1Data[0],
                 u4DataLen, TxDestMacAddr, ECFM_OPCODE_APS) == ECFM_SUCCESS)
            {
                printf ("APS test case 1 pass\n");
            }
            else
            {
                printf ("APS Tx fail\n");
            }

            break;

        case 201:
            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 39;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams, u1TlvOffset, u1SubOpCode, &u1Data[0],
                 u4DataLen, TxDestMacAddr, ECFM_OPCODE_APS) == ECFM_SUCCESS)
            {
                printf ("APS test case 1 pass\n");
            }
            else
            {
                printf ("APS Tx fail\n");
            }

            break;

        case 3:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 49;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_EXM) == ECFM_SUCCESS)
            {
                printf ("EXM test case 1 pass\n");
            }
            else
            {
                printf ("EXM Tx fail\n");
            }
            break;

        case 31:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 49;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 2;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_EXM) == ECFM_SUCCESS)
            {
                printf ("EXM test case 1 pass\n");
            }
            else
            {
                printf ("EXM Tx fail\n");
            }
            break;

        case 4:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 48;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_EXR) == ECFM_SUCCESS)
            {
                printf ("EXR test case 1 pass\n");
            }
            else
            {
                printf ("EXR Tx fail\n");
            }

            break;

        case 41:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 48;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 2;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_EXR) == ECFM_SUCCESS)
            {
                printf ("EXR test case 1 pass\n");
            }
            else
            {
                printf ("EXR Tx fail\n");
            }

            break;

        case 5:
            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 51;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 2;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_VSM) == ECFM_SUCCESS)
            {
                printf ("VSM test case 1 pass\n");
            }
            else
            {
                printf ("VSM Tx fail\n");
            }

            break;

        case 15:
            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 51;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_VSM) == ECFM_SUCCESS)
            {
                printf ("VSM test case 1 pass\n");
            }
            else
            {
                printf ("VSM Tx fail\n");
            }

            break;

        case 6:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 40;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 2;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_VSR) == ECFM_SUCCESS)
            {
                printf ("VSR test case 1 pass\n");
            }
            else
            {
                printf ("VSR Tx fail\n");
            }
            break;

        case 16:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 50;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 3;
            EcfmMepInfoParams.u4VlanIdIsid = 2;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 ECFM_OPCODE_VSR) == ECFM_SUCCESS)
            {
                printf ("VSR test case 1 pass\n");
            }
            else
            {
                printf ("VSR Tx fail\n");
            }
            break;

        case 7:

            u4DataLen = 4;
            u1TlvOffset = 0;
            u1SubOpCode = 48;
            EcfmMepInfoParams.u4ContextId = 1;
            EcfmMepInfoParams.u2PortNum = 2;
            EcfmMepInfoParams.u4VlanIdIsid = 6;
            EcfmMepInfoParams.u1MegLevel = 6;
            EcfmMepInfoParams.u1Direction = 1;
            if (EcfmInitiateExPdu
                (&EcfmMepInfoParams,
                 u1TlvOffset, u1SubOpCode, &u1Data[0], u4DataLen, TxDestMacAddr,
                 92) == ECFM_SUCCESS)
            {
                printf ("Incorrect Opcode PDU Sent!!!!\n");
            }
            else
            {
                printf ("Incorrect Opcode PDU NOT Sent \n");
            }
            break;

    }

}
