/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanapi.c,v 1.393.2.1 2018/03/15 12:59:57 siva Exp $
 *
 * Description: This file contains VLAN routines used by other modules.
 *
 *******************************************************************/

#include "vlaninc.h"
#include "brgnp.h"
#include "l2iwf.h"

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanMain                                                */
/*                                                                            */
/*  Description     : This the main routine for the VLAN module.              */
/*                                                                            */
/*  Input(s)        : pi1Param                                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanMain (INT1 *pi1Param)
{
    tVlanQInPkt        *pMsg = NULL;
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4Events;
    UNUSED_PARAM (pi1Param);

    if (VlanTaskInit () != VLAN_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        VLAN_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (VlanHandleStartModule () != VLAN_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        VlanRedDeInitRedundancyInfo ();
#ifdef L2RED_WANTED
        VlanRmDeRegisterProtocols ();
#endif
        VlanMainDeInit ();
        VLAN_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
        OSIX_SUCCESS)
    {
        VlanRedDeInitRedundancyInfo ();
#ifdef L2RED_WANTED
        VlanRmDeRegisterProtocols ();
#endif
        VlanMainDeInit ();
        VLAN_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    VLAN_INIT_COMPLETE (OSIX_SUCCESS);

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsVlanSizingInfo);
#ifdef SNMP_3_WANTED
    /* Register the protocol MIBs with SNMP */
    VlanRegisterMIB ();
#endif

#ifdef SYSLOG_WANTED
    gi4VlanSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "VLAN", SYSLOG_CRITICAL_LEVEL);
#endif

    while (1)
    {

        if ((VLAN_RECEIVE_EVENT (VLAN_TASK_ID, (VLAN_TIMER_EXP_EVENT
                                                | VLAN_MSG_ENQ_EVENT
                                                | VLAN_CFG_MSG_EVENT
                                                | VLAN_MSR_COMPLETE_EVENT
                                                | VLAN_RED_BULK_UPD_EVENT),
                                 OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {
            if (u4Events & VLAN_MSR_COMPLETE_EVENT)
            {
                VlanSetDefaultVlan ();
            }

            if (u4Events & VLAN_TIMER_EXP_EVENT)
            {
                VLAN_LOCK ();
                VlanProcessTimerEvent ();
                VLAN_UNLOCK ();
            }
            if (u4Events & VLAN_MSG_ENQ_EVENT)
            {
                while (VLAN_RECV_FROM_QUEUE (VLAN_TASK_QUEUE_ID,
                                             (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                                             OSIX_NO_WAIT) == OSIX_SUCCESS)
                {

                    VLAN_LOCK ();
                    /* switching the context will happen in side this 
                     * function */

                    VlanHandleProcessPkt (pMsg->pPktInQ, &(pMsg->VlanIfMsg));

                    VlanReleaseContext ();
                    VLAN_UNLOCK ();

                    VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER,
                                      (UINT1 *) pMsg->pPktInQ);
                    VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pMsg);
                }
            }

            if (u4Events & VLAN_CFG_MSG_EVENT)
            {
                while (VLAN_RECV_FROM_QUEUE (VLAN_CFG_QUEUE_ID,
                                             (UINT1 *) &pVlanQMsg,
                                             OSIX_DEF_MSG_LEN,
                                             OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    VLAN_LOCK ();
                    VlanProcessCfgEvent (pVlanQMsg);
                    /* Relese the context here */
                    VlanReleaseContext ();

                    VLAN_UNLOCK ();
                    VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
                }
            }

#ifdef L2RED_WANTED
            if (u4Events & VLAN_RED_BULK_UPD_EVENT)
            {
                VLAN_LOCK ();
                VlanRedHandleBulkUpdateEvent ();
                VLAN_UNLOCK ();
            }
#endif
        }
    }
}

/*****************************************************************************/
/* Function Name      : VlanStartModule                                      */
/*                                                                           */
/* Description        : This function is an API to start the VLAN module     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanStartModule (VOID)
{
    VLAN_LOCK ();
    if (VlanHandleStartModule () == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanShutdownModule                                   */
/*                                                                           */
/* Description        : This function will shutdown the VLAN module          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanShutdownModule (VOID)
{
    tVlanQInPkt        *pMsg = NULL;
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4Context;

    /* VLAN initialised status should be made false for avoiding the
     * Message posting, configurations when mempools, timers etc.,
     * are deleted here*/

    VLAN_LOCK ();
    gu1IsVlanInitialised = VLAN_FALSE;

#ifdef L2RED_WANTED
    /* HW Audit task should be deleted if running, 
     * before flushing out the data structures otherwise it will access 
     * unavailable entries.*/

    if (VLAN_AUDIT_TASK_ID != 0)
    {
        VLAN_RED_AUDIT_FLAG () = VLAN_RED_AUDIT_STOP;

        VLAN_SEND_EVENT (VLAN_AUDIT_TASK_ID, VLAN_RED_AUDIT_TASK_DEL_EVENT);
    }
#endif
    while (VLAN_RECV_FROM_QUEUE (VLAN_TASK_QUEUE_ID,
                                 (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pMsg->pPktInQ);
        VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pMsg);
    }

    while (VLAN_RECV_FROM_QUEUE (VLAN_CFG_QUEUE_ID,
                                 (UINT1 *) &pVlanQMsg,
                                 OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
    }

    for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
    {
        /* Here the deinitilizations start from 1 as default 
         * context (0) handled seperately */
        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VLAN_GLOBAL_TRC (u4Context, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " System control status is shutdown\r\n");
            VlanReleaseContext ();
            continue;
        }
        VlanHandleDeleteContext (u4Context);
        VlanReleaseContext ();
    }
    VlanDeInitGlobalInfo ();
#ifdef SW_LEARNING
    VLAN_DELETE_SEM (VLAN_SHADOW_TBL_SEM_ID);
    VLAN_SHADOW_TBL_SEM_ID = 0;
#endif
#ifdef L2RED_WANTED
    VlanRedDeInitGlobalInfo ();
    if (VlanRmDeRegisterProtocols () == RM_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "Vlan RM deregistration failed \n");
    }
#endif
    if (gVlanTaskInfo.VlanTmrListId != 0)
    {
        VLAN_DELETE_TMR_LIST (gVlanTaskInfo.VlanTmrListId);
        gVlanTaskInfo.VlanTmrListId = 0;
    }
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCreatePort                                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is created by */
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2IfIndex)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostPortCreateMessage (u4ContextId, u4IfIndex, u2IfIndex,
                                          VLAN_PORT_CREATE_MSG);

    if (i4RetVal == VLAN_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePort                                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is deleted   */
/*                          by management module.                            */
/*                                                                           */
/*    Input(s)            : u2IfIndex - The number of the port that is deleted. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeletePort (UINT4 u4IfIndex)
{

    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_PORT_DELETE_MSG, u4IfIndex);

    if (i4RetVal == VLAN_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateIntfType                               */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever the Interface Type     */
/*                          gets changed                                     */
/*                                                                           */
/*    Input(s)            : u4ContextId    -  Context Id                     */
/*                          u1IntfType     -  Interface Type to be Changed   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateIntfType (UINT4 u4ContextId, UINT1 u1IntfType)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostIntfChangeMessage (u4ContextId, u1IntfType,
                                          VLAN_UPDATE_INTF_TYPE_MSG);

    if (i4RetVal == VLAN_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCreateContext                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever a context created, this*/
/*                          function post a message to VLAN for creating a   */
/*                          context                                          */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCreateContext (UINT4 u4ContextId)
{
    /* post message to VLAN */
    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_CREATE_CONTEXT_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_CREATE_CONTEXT_MSG;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", pVlanQMsg->u2MsgType);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteContext                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever a context deleted, this*/
/*                          function ports a Delete context Message to VLAN  */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeleteContext (UINT4 u4ContextId)
{
    /* post message to VLAN */
    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_DELETE_CONTEXT_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_DELETE_CONTEXT_MSG;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", pVlanQMsg->u2MsgType);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMapPort                                      */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever some port is mapped to */
/*                          any  context                                     */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                          u2IfIndex    - mapped Port number                */
/*    Input(s)            : u2IfIndex    - local Port number                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2IfIndex)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostPortCreateMessage (u4ContextId, u4IfIndex, u2IfIndex,
                                          VLAN_PORT_MAP_MSG);

    if (i4RetVal == VLAN_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUnMapPort                                    */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is unmapped  */
/*                          from a context                                   */
/*                                                                           */
/*    Input(s)            : u2IfIndex - port that is unmapped .              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUnmapPort (UINT4 u4IfIndex)
{

    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_PORT_UNMAP_MSG, u4IfIndex);

    if (i4RetVal == VLAN_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateContextName                            */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever the name of the        */
/*                          context is modified. function post a message to  */
/*                          VLAN for updating the name of the context.       */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateContextName (UINT4 u4ContextId)
{
    /* post message to VLAN */
    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_UPDATE_CONTEXT_NAME);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_UPDATE_CONTEXT_NAME;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", pVlanQMsg->u2MsgType);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePortAndCopyProperties                  */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is added to  */
/*                          port-channel in LA module                        */
/*                                                                           */
/*    Input(s)            : u2IfIndex- The number of the port that is deleted*/
/*                          u4PoIndex- The portchannel index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeletePortAndCopyProperties (UINT4 u4IfIndex, UINT4 u4PoIndex)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    /* Both port-channel and port are assumed to be in the same context */
    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_PORT_DEL_COPY_PROPERTIES_MSG);

        return VLAN_FAILURE;

    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_PORT_DEL_COPY_PROPERTIES_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_PORT_DEL_COPY_PROPERTIES_MSG;
    pVlanQMsg->u4Port = u4PoIndex;
    pVlanQMsg->u4DstPort = u4IfIndex;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_PORT_DEL_COPY_PROPERTIES_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {

        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Since it is already Queued */
            return VLAN_FAILURE;
        }

    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortOperInd()                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever Port Oper Status is    */
/*                          changed. If Link Aggregation is enabled, then    */
/*                          Oper Down indication is given for each port      */
/*                          initially. When the Link Aggregation Group is    */
/*                          formed, then Oper Up indication is given.        */
/*                          In the Oper Down state, frames will neither      */
/*                          be transmitted nor be received.                  */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                          u1OperStatus - VLAN_OPER_UP / VLAN_OPER_DOWN     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortOperInd (UINT4 u4IfIndex, UINT1 u1OperStatus)
{

    INT4                i4RetVal;

    if (u1OperStatus == VLAN_OPER_UP)
    {
        i4RetVal = VlanPostCfgMessage (VLAN_PORT_OPER_UP_MSG, u4IfIndex);
    }
    else
    {
        i4RetVal = VlanPostCfgMessage (VLAN_PORT_OPER_DOWN_MSG, u4IfIndex);
    }
    if (i4RetVal == VLAN_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessPacket                                */
/*                                                                           */
/*    Description         : Fills the neccessary info in the module data and */
/*                          post the Incoming Packet to VLAN Task.           */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Port on which the packet arrived.     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanProcessPacket (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4InPort)
{
    tVlanIfMsg         *pVlanIfMsg;
    tVlanQInPkt        *pVlanQInPkt = NULL;
    UINT4               u4RetVal;
    UINT4               u4ContextId = 0;
    UINT4               u4VlanOffset = 0;
    UINT1               u1IfType;
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2LocalPort;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_SUCCESS;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4InPort, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_FAILURE;
    }
    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is not enabled. Dropping the incoming "
                         "packet received on port %d.\r\n", u4InPort);

        return VLAN_FAILURE;
    }

    pVlanQInPkt = (tVlanQInPkt *) (VOID *) VLAN_GET_BUF (VLAN_Q_IN_PKT_BUFF,
                                                         sizeof (tVlanQInPkt));
    if (NULL == pVlanQInPkt)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Queue - Incoming Packet \r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&(pVlanQInPkt->VlanIfMsg), VLAN_INIT_VAL, sizeof (tVlanIfMsg));
    pVlanIfMsg = &(pVlanQInPkt->VlanIfMsg);

    VLAN_COPY_FROM_BUF (pFrame, &(pVlanIfMsg->DestAddr), 0, ETHERNET_ADDR_SIZE);
    VLAN_COPY_FROM_BUF (pFrame, &(pVlanIfMsg->SrcAddr),
                        ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);

    pVlanIfMsg->u4IfIndex = u4InPort;
    pVlanIfMsg->u2Length = (UINT2) VLAN_GET_BUF_LEN (pFrame);
    pVlanIfMsg->SVlanId = VLAN_NULL_VLAN_ID;

    if (VlanCfaGetIfInfo (u4InPort, &CfaIfInfo) == CFA_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "Unable to get Port Information. Dropping the incoming "
                         "packet received on port %d.\r\n", u4InPort);

        return VLAN_FAILURE;
    }

    u1IfType = CfaIfInfo.u1IfType;

    if (VlanGetTagLenInFrame (pFrame, u4InPort, &u4VlanOffset) == VLAN_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                         "FrameLength %d Dropping the "
                         "incoming packet received on port %d.\r\n",
                         pVlanIfMsg->u2Length, u4InPort);

        VlanBrgIncrFilterInDiscards (u4InPort);
        return VLAN_FAILURE;
    }

    if (CfaIfInfo.u4IfSpeed < VLAN_ENET_SPEED_1G)
    {
        if (((u4VlanOffset > VLAN_TAG_OFFSET) &&
             (pVlanIfMsg->u2Length > (VLAN_UNTAGGED_FRAME_MAXIMUM_LENGTH +
                                      (u4VlanOffset - VLAN_TAG_OFFSET))))
            || ((pVlanIfMsg->u2Length > VLAN_UNTAGGED_FRAME_MAXIMUM_LENGTH)))
        {
            VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
            VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                             "FrameLength %d Dropping the "
                             "incoming packet received on port %d.\r\n",
                             pVlanIfMsg->u2Length, u4InPort);

            VlanBrgIncrFilterInDiscards (u4InPort);

            return VLAN_FAILURE;
        }
    }

    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG))
    {
        pVlanIfMsg->u2Length -= CRU_ENET_HEADER_SIZE;
    }

    VLAN_GET_TIMESTAMP (&(pVlanIfMsg->u4TimeStamp));

    pVlanQInPkt->pPktInQ = pFrame;

    u4RetVal
        = VLAN_SEND_TO_QUEUE (VLAN_TASK_QUEUE_ID, (UINT1 *) &pVlanQInPkt,
                              OSIX_DEF_MSG_LEN);

    if (u4RetVal == OSIX_SUCCESS)
    {
        if (VLAN_TASK_ID == VLAN_INIT_VAL)
        {
            if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
                OSIX_SUCCESS)
            {
                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                                 (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                 " Failed to get Vlan Task Id for "
                                 "sending the event  \r\n");

                return VLAN_FAILURE;
                /* No Need to Free Message Sice it is already Queued */
            }

        }

        VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_MSG_ENQ_EVENT);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, DATA_PATH_TRC,
                         "Frame received on Port %d is enqueued to "
                         "VLAN message Queue.\n", u4InPort);

        return VLAN_SUCCESS;
    }

    VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
    VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

    VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                     "Frame received on Port %d is NOT enqueued to "
                     "VLAN message Queue.\n", u4InPort);

    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanGetAdminMacLearnStatus                           */
/*                                                                           */
/* Description        : This function will get mac learning status           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context id                             */
/*                      u4VlanIndex - Vlan Id                                */
/*                                                                           */
/* Output(s)          : pi4VlanAdminMacLearningStatus - mac learnign status  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/

INT4
VlanGetAdminMacLearnStatus (UINT4 u4ContextId, UINT4 u4VlanIndex,
                            INT4 *pi4VlanAdminMacLearningStatus)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VLAN_GET_CURR_ENTRY ((UINT2) u4VlanIndex) == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    VlanGetVlanAdminMacLearnStatus (u4VlanIndex, pi4VlanAdminMacLearningStatus);
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;

}

#ifdef VLAN_EXTENDED_FILTER
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateDynamicDefGroupInfo                    */
/*                                                                           */
/*    Description         : This function updates the dynamic entries in the */
/*                          Default GroupInfo table.                         */
/*                                                                           */
/*    Input(s)            : u1Type   - The Type of group has to be updated   */
/*                          VlanId   - The VlanId                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateDynamicDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type, tVlanId VlanId,
                               UINT2 u2Port, UINT1 u1Action)
{
    UINT1              *pHwPortList = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT1              *pu1PortList;
    UINT1              *pu1ForbiddenPortList;
    UINT1              *pu1StaticPortList;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4Port;

    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (u1Type == VLAN_ALL_GROUPS)
    {
        pu1PortList = pVlanEntry->AllGrps.Ports;
        pu1StaticPortList = pVlanEntry->AllGrps.StaticPorts;
        pu1ForbiddenPortList = pVlanEntry->AllGrps.ForbiddenPorts;

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Updating Fwd All Groups Info for VLAN %d. \n", VlanId);

    }
    else
    {
        pu1PortList = pVlanEntry->UnRegGrps.Ports;
        pu1StaticPortList = pVlanEntry->UnRegGrps.StaticPorts;
        pu1ForbiddenPortList = pVlanEntry->UnRegGrps.ForbiddenPorts;

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Updating UnReg Groups Info for VLAN %d. \n", VlanId);
    }

    VLAN_IS_MEMBER_PORT (pu1ForbiddenPortList, u2Port, u1Result);

    if (u1Result == VLAN_TRUE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    pHwPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pHwPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanUpdateDynamicDefGroupInfo: Error in allocating memory "
                  "for pHwPortList \r\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    u4Port = VLAN_GET_IFINDEX (u2Port);
    switch (u1Action)
    {
        case VLAN_ADD:

            VLAN_IS_MEMBER_PORT (pu1PortList, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                break;
            }

            MEMCPY (pHwPortList, pu1PortList, sizeof (tLocalPortList));

            VLAN_SET_MEMBER_PORT (pHwPortList, u2Port);

            i4RetVal = VlanHwSetDefGroupInfo (u4ContextId, VlanId,
                                              u1Type, pHwPortList);

            if (i4RetVal == VLAN_FAILURE)
            {
                break;
            }

            VLAN_SET_MEMBER_PORT (pu1PortList, u2Port);

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Port %d added as member port\n", u4Port);
            break;

        case VLAN_DELETE:

            VLAN_IS_MEMBER_PORT (pu1PortList, u2Port, u1Result);

            if (u1Result == VLAN_FALSE)
            {
                break;
            }

            VLAN_IS_MEMBER_PORT (pu1StaticPortList, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                break;
            }

            MEMSET (pHwPortList, 0, sizeof (tLocalPortList));

            VLAN_SET_MEMBER_PORT (pHwPortList, u2Port);

            i4RetVal = VlanHwResetDefGroupInfo (u4ContextId, VlanId,
                                                u1Type, pHwPortList);

            if (i4RetVal == VLAN_FAILURE)
            {
                break;
            }

            VLAN_RESET_MEMBER_PORT (pu1PortList, u2Port);

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Port %d deleted from member list\n", u4Port);
            break;

        default:
            i4RetVal = VLAN_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pHwPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteAllDynamicDefGroupInfo                 */
/*                                                                           */
/*    Description         : This function removes all the dynamic service req*/
/*                          information from the table.                      */
/*                                                                           */
/*    Input(s)            : u2Port - This is currently called from GARP, so  */
/*                          the port is local port                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteAllDynamicDefGroupInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    VLAN_LOCK ();
    VlanHandleDeleteAllDynamicDefGroupInfo (u4ContextId, u2Port);
    VLAN_UNLOCK ();
}
#endif /* EXTENDED_FILTERING_CHANGE */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateDynamicVlanInfo                        */
/*                                                                           */
/*    Description         : This function updates the Current vlan table.    */
/*                                                                           */
/*    Input(s)            : VlanId   - The VlanId which is learnt by GVRP    */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the Port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the Port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateDynamicVlanInfo (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2Port,
                           UINT1 u1Action)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanGroupEntry    *pNextGroupEntry = NULL;
    INT4                i4RetVal = VLAN_FAILURE;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1IsDynInfoPresent;
    UINT1               u1StPortCheck = VLAN_FALSE;
    tMacAddr            McastAddr;
    UINT4               u4Port;
    tPortList          *pPortList = NULL;

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "Invalid port \r\n");

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    /* If the service type for a vlan is 'eLine', then learning
     * member ports through GVRP will not be allowed.*/
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if (pVlanEntry != NULL)
        {
            if (VlanL2IwfIsVlanElan (VLAN_CURR_CONTEXT_ID (),
                                     VlanId) != OSIX_TRUE)
            {
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }
        }
    }

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    u4Port = VLAN_GET_PHY_PORT (u2Port);
    switch (u1Action)
    {

        case VLAN_ADD:

            if (pVlanEntry == NULL)
            {

                /* Break incase VLAN ID is Already used by CFA for RouterPort VLAN */
                if (OSIX_SUCCESS == CfaIsPortVlanExists ((UINT2) VlanId))
                {
                    break;
                }

                /* Vlan first added before indication to Mstp */
                i4RetVal = VlanAddVlanEntry (VlanId, u2Port, NULL);

                if (i4RetVal == VLAN_FAILURE)
                {
                    break;
                }

                pVlanEntry = VlanGetVlanEntry (VlanId);
                if (pVlanEntry == NULL)
                {
                    break;
                }

#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
                VlanRedSyncSwUpdate (u4ContextId, VlanId, VLAN_ADD);
#endif /* L2RED_WANTED */

                VlanNotifySetVlanMemberToL2IwfAndEnhFltCriteria (VlanId,
                                                                 u2Port,
                                                                 VLAN_ADD_TAGGED_PORT);

                VlanInitializeDefGroupInfo (pVlanEntry);

                /* Delete the VLAN deleted entry from the VLAN deleted      
                 * table */
                VlanRedDelVlanDeletedEntry (VlanId);
                /* Set changed flag for this VLAN to indicate the need
                 * for sync up */
                VlanRedSetVlanChangedFlag (pVlanEntry);
            }
            else
            {
                u1IsDynInfoPresent = VLAN_TRUE;

                VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT (pVlanEntry, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    u1Result = VLAN_FALSE;
                    /* first member join...need to delete deleted entry */
                    u1IsDynInfoPresent = VLAN_FALSE;
                }
                else
                {
                    VLAN_IS_CURR_LEARNT_PORT (pVlanEntry, u2Port, u1Result);
                }

                if (pVlanEntry->pStVlanEntry != NULL)
                {
                    VLAN_IS_MEMBER_PORT (pVlanEntry->pStVlanEntry->EgressPorts,
                                         u2Port, u1StPortCheck);
                }
                if (u1Result == VLAN_FALSE)
                {

                    if (u1StPortCheck == VLAN_FALSE)
                    {
                        /* Update the hardware  to add this port as member port */
                        i4RetVal
                            =
                            VlanHwSetVlanMemberPort (VlanId, u2Port, VLAN_TRUE);
                        if (i4RetVal == VLAN_FAILURE)
                        {
                            break;
                        }
                    }

                    VLAN_SET_SINGLE_CURR_LEARNT_PORT (pVlanEntry, u2Port);

                    VLAN_GET_TIMESTAMP (&pVlanEntry->u4TimeStamp);

                    VlanUpdtVlanEgressPorts (pVlanEntry);

                    VlanNotifySetVlanMemberToL2IwfAndEnhFltCriteria (VlanId,
                                                                     u2Port,
                                                                     VLAN_ADD_TAGGED_PORT);

                    /* 
                     * Need to update trunk ports changes for the updated
                     * VLAN 
                     */
                    VlanAddTrunkPortsToNewVlan (VlanId);

                    if (VlanGmrpIsGmrpEnabledInContext
                        (VLAN_CURR_CONTEXT_ID ()) == GMRP_TRUE)
                    {

                        VlanNotifyMemberAddToAttrRP (pVlanEntry, u2Port);
                    }
                    VlanRedSetVlanChangedFlag (pVlanEntry);

                    if (u1IsDynInfoPresent == VLAN_FALSE)
                    {
                        /* first learnt port */
                        VlanRedDelVlanDeletedEntry (VlanId);
                    }
                }
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
                VlanRedSyncSwUpdate (u4ContextId, VlanId, VLAN_ADD);
#endif /* L2RED_WANTED */

                i4RetVal = VLAN_SUCCESS;
            }

            if (SNOOP_ENABLED == VlanSnoopIsIgmpSnoopingEnabled (u4ContextId) ||
                SNOOP_ENABLED == VlanSnoopIsMldSnoopingEnabled (u4ContextId))
            {
                MEMSET (McastAddr, 0, sizeof (tMacAddr));
                MEMSET (*pPortList, 0, sizeof (tPortList));
                if (u2Port >= VLAN_MAX_PORTS + 1)
                {
                    i4RetVal = VLAN_FAILURE;
                    break;
                }
                VLAN_SET_MEMBER_PORT ((*pPortList), VLAN_GET_IFINDEX (u2Port));

                VlanSnoopMiUpdatePortList (u4ContextId, VlanId, McastAddr,
                                           *pPortList, gNullIfPortList,
                                           VLAN_UPDT_VLAN_PORTS);
            }

            if (u2Port <= VLAN_MAX_PORTS)
            {

                VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "Port %d added as member port for VLAN %d \n",
                               u4Port, VlanId);
            }

            break;

        case VLAN_DELETE:

            /* The port is getting removed from vlan, so delete this port
             * from the dynamic mcast entries in this vlan. */
            if (pVlanEntry != NULL)
            {
                /* Remove the port from group table. Scan the group table, 
                 * and remove the port */
                pGroupEntry = pVlanEntry->pGroupTable;

                while (pGroupEntry != NULL)
                {
                    pNextGroupEntry = pGroupEntry->pNextNode;

                    VlanUtlRemPortFromGroupEntry (pVlanEntry, pGroupEntry,
                                                  u2Port);

                    pGroupEntry = pNextGroupEntry;
                }

                if (pVlanEntry->pStVlanEntry == NULL)
                {
                    /* Only learnt info present in this VLAN entry */
                    VLAN_RESET_SINGLE_CURR_LEARNT_PORT (pVlanEntry, u2Port);
                    VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT (pVlanEntry, u1Result);
                    if (u1Result != VLAN_TRUE)
                    {
                        /* more learnt info present */
                        i4RetVal = VlanHwResetVlanMemberPort (VlanId, u2Port);

                        if (i4RetVal == VLAN_FAILURE)
                        {
                            VLAN_SET_SINGLE_CURR_LEARNT_PORT (pVlanEntry,
                                                              u2Port);
                            break;
                        }

                        if (IGS_ENABLED ==
                            VlanSnoopIsIgmpSnoopingEnabled (u4ContextId)
                            || SNOOP_ENABLED ==
                            VlanSnoopIsMldSnoopingEnabled (u4ContextId))
                        {
                            MEMSET (McastAddr, 0, sizeof (tMacAddr));
                            MEMSET (*pPortList, 0, sizeof (tPortList));

                            if (u2Port >= VLAN_MAX_PORTS + 1)
                            {
                                i4RetVal = VLAN_FAILURE;
                                break;
                            }
                            VLAN_SET_MEMBER_PORT ((*pPortList),
                                                  VLAN_GET_IFINDEX (u2Port));

                            VlanSnoopMiUpdatePortList (u4ContextId, VlanId,
                                                       McastAddr,
                                                       gNullIfPortList,
                                                       *pPortList,
                                                       VLAN_UPDT_VLAN_PORTS);
                        }

                        VLAN_RESET_SINGLE_CURR_EGRESS_PORT (pVlanEntry, u2Port);

                        VLAN_GET_TIMESTAMP (&pVlanEntry->u4TimeStamp);

                        VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                            (VlanId, u2Port);

                        VlanRedSetVlanChangedFlag (pVlanEntry);
                    }
                    else
                    {
                        /* Need to delete the VLAN Entry itself */
                        i4RetVal = VlanHwDelDynVlanEntry (VlanId, u2Port);

                        if (i4RetVal == VLAN_FAILURE)
                        {
                            VLAN_SET_SINGLE_CURR_LEARNT_PORT (pVlanEntry,
                                                              u2Port);
                            break;
                        }

                        VlanRedAddVlanDeletedEntry (pVlanEntry->VlanId);

                        VlanDelVlanEntry (pVlanEntry);
                    }
                }
                else
                {
                    /* Static info present for this VLAN */
                    VLAN_IS_CURR_LEARNT_PORT (pVlanEntry, u2Port, u1Result);

                    VLAN_IS_MEMBER_PORT (pVlanEntry->pStVlanEntry->EgressPorts,
                                         u2Port, u1StPortCheck);

                    if ((u1Result == VLAN_TRUE)
                        && (u1StPortCheck == VLAN_FALSE))
                    {
                        i4RetVal = VlanHwResetVlanMemberPort (VlanId, u2Port);

                        if (i4RetVal == VLAN_FAILURE)
                        {
                            break;
                        }

                        if (SNOOP_ENABLED ==
                            VlanSnoopIsIgmpSnoopingEnabled (u4ContextId)
                            || SNOOP_ENABLED ==
                            VlanSnoopIsMldSnoopingEnabled (u4ContextId))
                        {
                            MEMSET (McastAddr, 0, sizeof (tMacAddr));
                            MEMSET (*pPortList, 0, sizeof (tPortList));

                            if (u2Port >= VLAN_MAX_PORTS + 1)
                            {
                                i4RetVal = VLAN_FAILURE;
                                break;
                            }
                            VLAN_SET_MEMBER_PORT ((*pPortList),
                                                  VLAN_GET_IFINDEX (u2Port));

                            VlanSnoopMiUpdatePortList (u4ContextId, VlanId,
                                                       McastAddr,
                                                       gNullIfPortList,
                                                       *pPortList,
                                                       VLAN_UPDT_VLAN_PORTS);
                        }

                        VLAN_RESET_SINGLE_CURR_LEARNT_PORT (pVlanEntry, u2Port);

                        VlanUpdtVlanEgressPorts (pVlanEntry);

                        VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                            (VlanId, u2Port);

                        /* 
                         * Need to update trunk ports changes for the updated
                         * VLAN 
                         */
                        VlanAddTrunkPortsToNewVlan (pVlanEntry->VlanId);

                        VLAN_GET_TIMESTAMP (&pVlanEntry->u4TimeStamp);

                        VlanRedSetVlanChangedFlag (pVlanEntry);
                    }
                }

                i4RetVal = VLAN_SUCCESS;
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                                   "Port %d deleted from member port list of VLAN %d \n",
                                   u4Port, VlanId);
                }
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
                VlanRedSyncSwUpdate (u4ContextId, VlanId, VLAN_DELETE);
#endif /* L2RED_WANTED */
            }
            else
            {
                i4RetVal = VLAN_FAILURE;
            }
            break;

        default:
            break;
    }

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteAllDynamicVlanInfo                     */
/*                                                                           */
/*    Description         : Removes all the dynamic vlan information         */
/*                          from the Current table.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Number of the input port.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteAllDynamicVlanInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    VLAN_LOCK ();
    VlanHandleDeleteAllDynamicVlanInfo (u4ContextId, u2Port);
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetBaseBridgeMode ()                         */
/*                                                                           */
/*    Description         : This function gets the Base Bridge Mode          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->i4BaseBridgeMode        */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : i4BaseBridgeMode                                  */
/*****************************************************************************/
INT4
VlanGetBaseBridgeMode ()
{
    INT4                i4BaseBridgeMode = VLAN_INIT_VAL;

    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return i4BaseBridgeMode;
    }

    i4BaseBridgeMode = (INT4) VLAN_BASE_BRIDGE_MODE ();

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4BaseBridgeMode;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGvrpEnableInd ()                             */
/*                                                                           */
/*    Description         : Gvrp indicates Vlan, that Gvrp is enabled.       */
/*                          If there are any Static Vlan entry configured    */
/*                          then, they must be propagated.                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanGvrpEnableInd (UINT4 u4ContextId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanPropagateStaticVlanInfo ();
        VLAN_GVRP_STATUS () = VLAN_ENABLED;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMvrpEnableInd ()                             */
/*                                                                           */
/*    Description         : Mvrp indicates Vlan, that Mvrp is enabled.       */
/*                          If there are any Static Vlan entry configured    */
/*                          then, they must be propagated.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMvrpEnableInd (UINT4 u4ContextId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanPropagateStaticVlanInfo ();
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMvrpPortEnableInd                            */
/*                                                                           */
/*    Description         : Mvrp indicates Vlan, that Mvrp is enabled in the */
/*                          port. If there are any Static Vlan entry         */
/*                          configured then, they must be propagated.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2Port      - LocalPort Identifier               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMvrpPortEnableInd (UINT4 u4ContextId, UINT2 u2Port)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        if (VLAN_GET_PORT_ENTRY (u2Port) != NULL)
        {
            VlanPropStaticVlanInfoForPort (u2Port);
        }
        VlanReleaseContext ();
    }
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGmrpEnableInd ()                             */
/*                                                                           */
/*    Description         : Gmrp indicates Vlan, that Gmrp is enabled.       */
/*                          If there are any Static Mcast entry configured   */
/*                          then, they must be propagated.                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanGmrpEnableInd (UINT4 u4ContextId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanPropagateStaticMacInfo ();
        VLAN_GMRP_STATUS () = VLAN_ENABLED;
        VlanReleaseContext ();
    }
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMmrpEnableInd ()                             */
/*                                                                           */
/*    Description         : Mmrp indicates Vlan, that Mmrp is enabled.       */
/*                          If there are any Static Mcast / Ucast entry      */
/*                          configured then, they must be propagated.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMmrpEnableInd (UINT4 u4ContextId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanPropagateStaticMacInfo ();
        VlanReleaseContext ();
    }
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMmrpPortEnableInd ()                         */
/*                                                                           */
/*    Description         : Mmrp indicates Vlan, that Mmrp is enabled on port*/
/*                          If there are any Static Mcast / Ucast entry      */
/*                          configured then, they must be propagated.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2Port      - LocalPort Identifier               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMmrpPortEnableInd (UINT4 u4ContextId, UINT2 u2Port)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        if (VLAN_GET_PORT_ENTRY (u2Port) != NULL)
        {
            VlanPropStaticMacInfoForPort (u2Port);
        }
        VlanReleaseContext ();
    }
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGvrpDisableInd                               */
/*                                                                           */
/*    Description         : Gvrp indicates Vlan, that Gvrp is disabled.      */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanGvrpDisableInd (UINT4 u4ContextId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VLAN_GVRP_STATUS () = VLAN_DISABLED;
        VlanReleaseContext ();
    }
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGmrpDisableInd                               */
/*                                                                           */
/*    Description         : Gmrp indicates Vlan, that Gmrp is disabled.      */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanGmrpDisableInd (UINT4 u4ContextId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VLAN_GMRP_STATUS () = VLAN_DISABLED;
        VlanReleaseContext ();
    }
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : VlanDeleteFdbEntries                                 */
/*                                                                           */
/*    Description     : This function flushes the FDB entries in FDB  Tbl    */
/*                      learned on this port.                                */
/*                       This function will be called                        */
/*                      1) By RSTP during topology change in the port        */
/*                      2) By PNAC module during port down indication        */
/*                      3) By VLAN module during port deletion               */
/*                                                                           */
/*    Input(s)        : u2Port - port number                                 */
/*                      i4OptimizeFlag - Indicates whether this call can be  */
/*                                       grouped with the flush call for     */
/*                                       other ports as a single flush call  */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeleteFdbEntries (UINT4 u4Port, INT4 i4OptimizeFlag)
{
    INT4                i4RetVal = VLAN_FAILURE;

#ifdef NPAPI_WANTED
    tVlanCurrEntry     *pVlanEntry;
    UINT4               u4ContextId;
    tVlanId             VlanId;
    UINT2               u2LocalPort;
    UINT1               u1Result;
    UINT4               u4FdbId;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return VLAN_FAILURE;
    }
    if (VlanVcmGetContextInfoFromIfIndex (u4Port, &u4ContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "Flushing based on Port - invalid port \r\n");
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_GET_PORT_ENTRY (u2LocalPort) == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if ((VlanMrpIsMvrpEnabled (u4ContextId) == MRP_ENABLED) &&
        (VLAN_LEARNING_MODE () == VLAN_INDEP_LEARNING))
    {
        VLAN_SCAN_VLAN_TABLE (VlanId)
        {
            pVlanEntry = VlanGetVlanEntry (VlanId);

            if ((pVlanEntry != NULL) && (pVlanEntry->pStVlanEntry != NULL))
            {
                /* VLAN id is one-to-one mapped with FDB Id in case of
                 * Indepenant VLAN learning.*/
                u4FdbId = VlanId;
                VLAN_IS_EGRESS_PORT (pVlanEntry->pStVlanEntry,
                                     u2LocalPort, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    i4RetVal = VlanHwFlushPortFdbId (u4ContextId, u2LocalPort,
                                                     u4FdbId, i4OptimizeFlag);
                }
            }
        }
    }
    else
    {
        i4RetVal = VlanHwFlushPort (u4ContextId, u2LocalPort, i4OptimizeFlag);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
#else
    UNUSED_PARAM (i4OptimizeFlag);
    i4RetVal = VlanPostCfgMessage (VLAN_DELETE_FDB_MSG, u4Port);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFlushFdbEntriesOnPort                        */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and FdbId                    */
/*                                                                           */
/*    Input(s)            : u2Port   - Port number                           */
/*                          u4FdbId  - FdbId                                 */
/*                          i4ModId  - STP_MODULE/MRP_MODULE                 */
/*                          i4OptimizeFlag - Indicates whether this call can */
/*                                           be grouped with  the flush call */
/*                                           for other ports as a single     */
/*                                           bridge  flush                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFlushFdbEntriesOnPort (UINT4 u4Port, UINT4 u4FdbId, INT4 i4ModId,
                           INT4 i4OptimizeFlag)
{
    UINT4               u4ContextId;
    INT4                i4RetVal = VLAN_FAILURE;
#ifndef NPAPI_WANTED
    tVlanQMsg          *pVlanQMsg = NULL;
#else
    tVlanCurrEntry     *pVlanEntry;
    tVlanId             VlanId;
    UINT1               u1Result;
#endif
    UINT2               u2LocalPort;

    UNUSED_PARAM (i4ModId);
    UNUSED_PARAM (i4RetVal);

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4Port, &u4ContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

#ifdef NPAPI_WANTED
    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_GET_PORT_ENTRY (u2LocalPort) == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (i4ModId == STP_MODULE)
    {
        if (VlanMrpIsMvrpEnabled (u4ContextId) == MRP_ENABLED)
        {
            if (VLAN_LEARNING_MODE () == VLAN_INDEP_LEARNING)
            {
                /* VLAN id is one-to-one mapped with FDB Id in case of 
                 * Indepenant VLAN learning.*/
                VlanId = (tVlanId) u4FdbId;

                pVlanEntry = VlanGetVlanEntry (VlanId);

                if ((pVlanEntry == NULL) || (pVlanEntry->pStVlanEntry == NULL))
                {
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return VLAN_SUCCESS;
                }
                VLAN_IS_EGRESS_PORT (pVlanEntry->pStVlanEntry,
                                     u2LocalPort, u1Result);

                if (u1Result == VLAN_FALSE)
                {
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return VLAN_SUCCESS;
                }
            }
        }
    }
    i4RetVal = VlanHwFlushPortFdbId (u4ContextId, u2LocalPort, u4FdbId,
                                     i4OptimizeFlag);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
#else
    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n", VLAN_FLUSH_FDB_MSG);

        return VLAN_FAILURE;

    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_FLUSH_FDB_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FLUSH_FDB_MSG;
    pVlanQMsg->u4Port = u4Port;
    pVlanQMsg->u4FdbId = u4FdbId;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u1ModType = (UINT1) i4ModId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_FLUSH_FDB_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {

        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }

    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
    UNUSED_PARAM (i4OptimizeFlag);
    return VLAN_SUCCESS;
#endif

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFlushFdbEntries                              */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and FdbId                    */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u4FdbId - FdbId                                  */
/*                          i4OptimizeFlag - Indicates whether this call can */
/*                                           be grouped with  the flush call */
/*                                           for other ports as a single     */
/*                                           bridge  flush                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFlushFdbEntries (UINT4 u4Port, UINT4 u4FdbId, INT4 i4OptimizeFlag)
{
    UINT4               u4ContextId;
#ifndef NPAPI_WANTED
    tVlanQMsg          *pVlanQMsg = NULL;
#endif
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT2               u2LocalPort;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4Port, &u4ContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

#ifdef NPAPI_WANTED
    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_GET_PORT_ENTRY (u2LocalPort) == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    i4RetVal = VlanHwFlushPortFdbId (u4ContextId, u2LocalPort, u4FdbId,
                                     i4OptimizeFlag);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
#else

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n", VLAN_FLUSH_FDB_MSG);

        return VLAN_FAILURE;

    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_FLUSH_FDB_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FLUSH_FDB_MSG;
    pVlanQMsg->u4Port = u4Port;
    pVlanQMsg->u4FdbId = u4FdbId;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_FLUSH_FDB_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {

        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }

    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
    UNUSED_PARAM (i4OptimizeFlag);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanLock                                             */
/*                                                                           */
/* Description        : This function is used to take the VLAN mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanLock (VOID)
{
    if (VLAN_TAKE_SEM (VLAN_SEM_ID) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "MSG: Failed to Take VLAN Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    /* VlanSelectContext and VlanReleaseContext does not have stubs in case of
     * SI mode. Also context selection will not be done implicitly even in SI
     * mode. 
     * Therefore in SI mode also before calling SI nmh we have to do select 
     * context. 
     * Registering select and release context with SNMP is not possible as, 
     * Vlan and Garp shares same mibs. Putting select and release contexts in 
     * SI mib wrapper routines is painful. So it is better to do 
     * select context for default context in Lock function.*/

    VlanSelectContext (VLAN_DEF_CONTEXT_ID);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the VLAN mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUnLock (VOID)
{
    VlanReleaseContext ();
    VLAN_RELEASE_SEM (VLAN_SEM_ID);
    return SNMP_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************************/
/* Function Name    : VlanCopyPortPropertiesToHw ()                        */
/*                                                                         */
/* Description      : This function programs the hardware with all         */
/*                    the Port properties for the port "u2DstPort".        */
/*                    The properties to be copied to "u2DstPort" is        */
/*                    obtained from "u2Port".                              */
/*                    "u2SrcPort" must have been created in Vlan. This     */
/*                    function does not program the Dynamic Vlan and Mcast */
/*                    membership and hence must not be called for ports    */
/*                    that are in OPER UP state. Dynamic Learning is NOT   */
/*                    done when the port is in OPER DOWN state.            */
/*                                                                         */
/* Input(s)         : u4DstPort - The Port whose properties have to be     */
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u4SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanCopyPortPropertiesToHw (UINT4 u4DstPort, UINT4 u4SrcPort, UINT4 u4ContextId)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT2               u2LocalPort = 0;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_COPY_PORT_INFO_MSG);

        return VLAN_FAILURE;

    }

    if (VlanGetContextInfoFromIfIndex (u4DstPort,
                                       &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex (u4SrcPort,
                                       &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_COPY_PORT_INFO_MSG);

        return VLAN_FAILURE;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_COPY_PORT_INFO_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_COPY_PORT_INFO_MSG;
    pVlanQMsg->u4Port = u4SrcPort;
    pVlanQMsg->u4DstPort = u4DstPort;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_COPY_PORT_INFO_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {

        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event  \r\n");
            /* No Need to Free Message Since it is already Queued */
            return VLAN_FAILURE;
        }

    }
    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send Event failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_COPY_PORT_INFO_MSG);
        /* No Need to Free Message Since it is already Queued */
        return VLAN_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanRemovePortPropertiesFromHw ()                    */
/*                                                                         */
/* Description      : This function removes from the Hardware all the      */
/*                    port properties of "u2DstPort". The exact properties */
/*                    which need to be removed is obtained from the        */
/*                    port "u4SrcPort". This function is typically called  */
/*                    when a physical port is removed from a Port Channel. */
/*                    When the physical port is removed from a Port        */
/*                    Channel, the Port Channel properties must be removed */
/*                    from the physical port. This function will be        */
/*                    invoked with "u4DstPort" as the Physical Port Id and */
/*                    "u4SrcPort" as the Port Channel Id.                  */
/*                    "u4SrcPort" must have been created in VLAN.          */
/*                                                                         */
/*                    This function will also be invoked whenever a Port   */
/*                    is deleted in VLAN. In which, u4SrcPort and          */
/*                    u2DstPort will be same.                              */
/*                                                                         */
/* Input(s)         : u4DstPort - Port whose properties have to be removed */
/*                                from the Hardware.                       */
/*                                                                         */
/*                    u4SrcPort - Port from where the properties to be     */
/*                                removed, have to be obtained.            */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanRemovePortPropertiesFromHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{

    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4DstPort, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan Config "
                         "Message Type %u \r\n", VLAN_REMOVE_PORT_INFO_MSG);

        return VLAN_FAILURE;

    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_REMOVE_PORT_INFO_MSG);

        return VLAN_FAILURE;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u\r\n",
                         VLAN_REMOVE_PORT_INFO_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_REMOVE_PORT_INFO_MSG;
    pVlanQMsg->u4Port = u4SrcPort;
    pVlanQMsg->u4DstPort = u4DstPort;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_REMOVE_PORT_INFO_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }
    }

    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send Event failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_REMOVE_PORT_INFO_MSG);

        /* No Need to Free Message Sice it is already Queued */
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanCopyPortMcastPropertiesToHw ()                   */
/*                                                                         */
/* Description      : This function programs the Multicast entries in the  */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER UP state or when a member link is      */
/*                    added/removed from a Port Channel.                   */
/*                                                                         */
/*                    This function must called only for Port Channel      */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanCopyPortMcastPropertiesToHw (UINT4 u4Port)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_COPY_PORT_MCAST_INFO_MSG, u4Port);

    return i4RetVal;

}

/***************************************************************************/
/* Function Name    : VlanRemovePortMcastPropertiesFromHw ()               */
/*                                                                         */
/* Description      : This function removes the Multicast entries from the */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER DOWN state or a member link is         */
/*                    added/removed from a Port Channel.                   */
/*                                                                         */
/*                    This function must be called only for Port Channel.  */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanRemovePortMcastPropertiesFromHw (UINT4 u4Port)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_REMOVE_PORT_MCAST_INFO_MSG, u4Port);

    return i4RetVal;

}

/***************************************************************************/
/* Function Name    : VlanCopyPortUcastPropertiesToHw ()                   */
/*                                                                         */
/* Description      : This function programs the Unicast entries in the    */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER UP state or when a member link is      */
/*                    added/removed from a Port Channel.                   */
/*                                                                         */
/*                    This function must called only for Port Channel      */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanCopyPortUcastPropertiesToHw (UINT4 u4Port)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_COPY_PORT_UCAST_INFO_MSG, u4Port);

    return i4RetVal;

}

/***************************************************************************/
/* Function Name    : VlanRemovePortUcastPropertiesFromHw ()               */
/*                                                                         */
/* Description      : This function removes the Unicast entries from the   */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER DOWN state or a member link is         */
/*                    added/removed from a Port Channel.                   */
/*                                                                         */
/*                    This function must be called only for Port Channel.  */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanRemovePortUcastPropertiesFromHw (UINT4 u4Port)
{
    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_REMOVE_PORT_UCAST_INFO_MSG, u4Port);

    return i4RetVal;

}

#ifdef SW_LEARNING

/*****************************************************************************/
/*    Function Name       : VlanFdbTableAdd                                  */
/*    Description         : This function adds the FDB entry to the RB tree. */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanAddFdbTable (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                 UINT1 u1EntryType)
{
    tVlanQMsg           VlanQMsg;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return;
    }

    VLAN_MEMSET (&VlanQMsg, 0, sizeof (tVlanQMsg));

    VlanQMsg.u2MsgType = VLAN_FDBENTRY_ADD_MSG;
    VlanQMsg.FdbData.VlanId = VlanId;
    VlanQMsg.u4ContextId = u4ContextId;
    VlanQMsg.u4Port = u4IfIndex;
    VlanQMsg.u1EntryType = u1EntryType;
    MEMCPY (VlanQMsg.FdbData.au1MacAddr, MacAddr, CFA_ENET_ADDR_LEN);
    VLAN_LOCK ();
    VlanProcessCfgEvent (&VlanQMsg);
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*    Function Name       : VlanAddExFdbTable                                */
/*    Description         : This function adds the FDB entry to the RB tree. */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*                          ConnectionId - BackBone MacAddress               */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanAddExFdbTable (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                   UINT1 u1EntryType, tMacAddr ConnectionId)
{
    tVlanQMsg           VlanQMsg;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return;
    }

    VLAN_MEMSET (&VlanQMsg, 0, sizeof (tVlanQMsg));

    VlanQMsg.u2MsgType = VLAN_FDBENTRY_ADD_MSG;
    VlanQMsg.FdbData.VlanId = VlanId;
    VlanQMsg.u4ContextId = u4ContextId;
    VlanQMsg.u4Port = u4IfIndex;
    VlanQMsg.u1EntryType = u1EntryType;
    MEMCPY (VlanQMsg.FdbData.au1MacAddr, MacAddr, CFA_ENET_ADDR_LEN);
    MEMCPY (VlanQMsg.FdbData.au1ConnectionId, ConnectionId, CFA_ENET_ADDR_LEN);
    VLAN_LOCK ();
    VlanProcessCfgEvent (&VlanQMsg);
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*    Function Name       : VlanAddFdbTable                                  */
/*    Description         : This function adds the FDB entry to the RB tree. */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanFdbTableAdd (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                 UINT1 u1EntryType, UINT4 u4PwIndex)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        return;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FDBENTRY_ADD_MSG;
    pVlanQMsg->FdbData.VlanId = VlanId;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u4Port = u4IfIndex;
    pVlanQMsg->u1EntryType = u1EntryType;
    pVlanQMsg->u4PwIndex = u4PwIndex;

    MEMCPY (pVlanQMsg->FdbData.au1MacAddr, MacAddr, CFA_ENET_ADDR_LEN);

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            /* No Need to Free Message Since it is already Queued */
            return;
        }
    }

    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        return;
    }

    return;
}

/*****************************************************************************/
/*    Function Name       : VlanFdbTableAddEx                                */
/*    Description         : This function adds the FDB entry to the RB tree. */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*                          ConnectionId - BackBone MacAddress               */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanFdbTableAddEx (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                   UINT1 u1EntryType, tMacAddr ConnectionId)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        return;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FDBENTRY_ADD_MSG;
    pVlanQMsg->FdbData.VlanId = VlanId;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u4Port = u4IfIndex;
    pVlanQMsg->u1EntryType = u1EntryType;
    MEMCPY (pVlanQMsg->FdbData.au1MacAddr, MacAddr, CFA_ENET_ADDR_LEN);
    MEMCPY (pVlanQMsg->FdbData.au1ConnectionId, ConnectionId,
            CFA_ENET_ADDR_LEN);

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            /* No Need to Free Message Since it is already Queued */
            return;
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return;
}

/*****************************************************************************/
/*    Function Name       : VlanFdbTableRemove                               */
/*    Description         : This function removes the FDB entry got from HW  */
/*                          from the RB tree                                 */
/*    Input(s)            : VlanId - VLAN ID                                 */
/*                          MacAddr - MAC address                            */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanFdbTableRemove (tVlanId VlanId, tMacAddr MacAddr)
{

    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }

    VlanFdbTableRemoveInCtxt (VlanId, MacAddr, VLAN_LOCAL_FDB);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

#endif /* SW_LEARNING */

#endif /* NPAPI_WANTED */

/*****************************************************************************/
/*    Function Name       : VlanIvrGetTxPortOrPortListInCxt                  */
/*                                                                           */
/*    Description         : This function is called when ever IP wants to Tx */
/*                          packet on an logical vlan interface. This fuction*/
/*                          returns the physical ports for trransmission.    */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          VlanId   - Vlan ID                               */
/*                          u4L2ContextId - L2 Context Id to which VlanId is */
/*                                          associated                       */
/*                          u1BcastFlag - Flag to indicate whether the pkt   */
/*                                        should be broadcast or not         */
/*                                                                           */
/*    Output(s)           : pu2OutPort    - Out Port for Tx if known Ucast.  */
/*                        : pbIsTag       - Pkt should be tagged / Untagged  */
/*                        : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanIvrGetTxPortOrPortListInCxt (UINT4 u4L2ContextId,
                                 tMacAddr DestAddr, tVlanId VlanId,
                                 UINT1 u1BcastFlag, UINT4 *pu4OutPort,
                                 BOOL1 * pbIsTag, tPortList TagPortList,
                                 tPortList UntagPortList)
{
    UINT1              *pFwdPortList = NULL;
    UINT1              *pLocalTagPortList = NULL;
    UINT1              *pLocalUntagPortList = NULL;
    tVlanCurrEntry     *pVlanRec;
    tVlanGroupEntry    *pGroupEntry;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanFidEntry      *pFidEntry;
    tVlanTempFdbInfo    VlanTempFdbInfo;
    INT4                i4RetVal;
    UINT4               u4Port;
    UINT2               u2OutPort = 0;
    UINT1               u1Result = VLAN_TRUE;
    UINT1               u1IsMemberPort = VLAN_FALSE;
    UINT1               u1IsUnTaggedPort = VLAN_FALSE;
    UINT1               u1TagOut = VLAN_TRUE;

    VLAN_LOCK ();
    if (VlanSelectContext (u4L2ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (VLAN_IS_VLAN_ENABLED () == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    *pu4OutPort = VLAN_INVALID_PORT;

    *pbIsTag = VLAN_FALSE;

    /* IVR - VLAN will not know about VLAN interfaces. So when a packet is 
     * received on a VLAN interface, port validity need not be checked.
     * and always the packet Tx From IVR is Untagged 
     */
    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "IVR: VLAN 0x%x associated with the packet is"
                       "unknown \n", VlanId);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    pFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pFwdPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleStMcastDeletion: Error in allocating memory "
                  "for pFwdPortList\r\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    VLAN_MEMSET (pFwdPortList, 0, sizeof (tLocalPortList));

    if (VLAN_IS_MCASTADDR (DestAddr) == VLAN_TRUE)
    {
        /* Given Packet is Multicast / Broadcast  Packet */
        /* When the incoming interface is a VLAN interface, no need to check
         * for the port validity.
         */

        pGroupEntry = VlanGetGroupEntry (DestAddr, pVlanRec);

        if (pGroupEntry != NULL)
        {
            if (MEMCMP (pGroupEntry->LearntPorts,
                        gNullPortList, sizeof (tLocalPortList)) == 0)
            {
#ifdef VLAN_EXTENDED_FILTER
                /* Use unregister group ports if no dynamic info is present */
                MEMCPY (pFwdPortList, pVlanRec->UnRegGrps.Ports,
                        sizeof (tLocalPortList));
#endif
            }
            else
            {
                MEMCPY (pFwdPortList, pGroupEntry->LearntPorts,
                        sizeof (tLocalPortList));
            }

            if (pGroupEntry->u1StRefCount != 0)
            {
                /* some static entry exist for this mcast address */
                pStMcastEntry = VlanGetStMcastEntry (DestAddr, 0, pVlanRec);
            }
        }
        else
        {
#ifdef VLAN_EXTENDED_FILTER
            MEMCPY (pFwdPortList, pVlanRec->UnRegGrps.Ports,
                    sizeof (tLocalPortList));
#else
            VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, pFwdPortList);
#endif
        }
        if ((VLAN_IS_BCASTADDR (DestAddr) == VLAN_TRUE)
            || (VLAN_IS_MCAST_RESERVED_ADDR (DestAddr) == VLAN_TRUE)
            || (VLAN_IS_VRRP_IPV6_MCAST_RESERVED_ADDR (DestAddr) == VLAN_TRUE))
        {
            /* In this case, Broadcast/reserved Multicast address  to be taken
             * care for all the member ports. for example arp cannot be dropped
             * because of non existance of unregistered ports
             */
            VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pFwdPortList);
        }
#ifdef VLAN_EXTENDED_FILTER
        VLAN_ADD_PORT_LIST (pFwdPortList, pVlanRec->AllGrps.Ports);
#endif

        if (pStMcastEntry != NULL)
        {
            VLAN_ADD_PORT_LIST (pFwdPortList, pStMcastEntry->EgressPorts);
            VLAN_RESET_PORT_LIST (pFwdPortList, pStMcastEntry->ForbiddenPorts);
        }
    }
    else
    {
        /* Unicast Packet */
        pFidEntry = VLAN_GET_FID_ENTRY (pVlanRec->u4FidIndex);

        if (pFidEntry == NULL)
        {
            UtilPlstReleaseLocalPortList (pFwdPortList);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_NO_FORWARD;
        }
        if (VlanGetFdbInfo (pFidEntry->u4Fid, DestAddr,
                            &VlanTempFdbInfo) == VLAN_FAILURE)
        {
            u2OutPort = VLAN_INVALID_PORT;
        }
        else
        {
            u2OutPort = VlanTempFdbInfo.u2Port;
        }

        if (u2OutPort != VLAN_INVALID_PORT)
        {
            /* Dynamic unicast entry is present */
            i4RetVal = VlanEgressFiltering (u2OutPort, pVlanRec, DestAddr);

            if ((i4RetVal == VLAN_FORWARD) && (u2OutPort <= VLAN_MAX_PORTS))
            {
                u4Port = VLAN_GET_PHY_PORT (u2OutPort);

                VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "IVR: Unicast Data Packet received to "
                               "be forwarded on Port %d \n", u4Port);

                /* 
                 * Check whether the u2Port is a member of UnTagPorts if
                 * a static vlan entry is present for the VLAN 
                 */

                if (pVlanRec->pStVlanEntry != NULL)
                {
                    VLAN_IS_UNTAGGED_PORT (pVlanRec->pStVlanEntry,
                                           u2OutPort, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        u1TagOut = VLAN_FALSE;
                    }
                }
                *pu4OutPort = (VLAN_GET_IFINDEX (u2OutPort));
                *pbIsTag = (BOOL1) u1TagOut;
                UtilPlstReleaseLocalPortList (pFwdPortList);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FORWARD;
            }
            else
            {
                /* Packet Dropped due to Egress Filtering */
                VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                          "IVR: Unicast Data Packet received to "
                          "be dropped due to Egress Filtering.\n");
                UtilPlstReleaseLocalPortList (pFwdPortList);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
        }
        else
        {
            /* 
             * No Dynamic forwarding information present. So
             * flood the frame on all the member ports of the Vlan.
             */

            if (u1BcastFlag == CFA_TRUE)
            {
                VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                          "IVR: Unicast Data Packet received to be "
                          "forwarded on VLAN Egress Ports ");

                VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pFwdPortList);
            }
            else
            {
                /* No Need to flood the packet WGS_WANTED enabled
                 * will be trying to send the packet on next vlan*/
                UtilPlstReleaseLocalPortList (pFwdPortList);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
        }
    }

    /* Reset the Forbidden Portlist */
    if (pVlanRec->pStVlanEntry != NULL)
    {
        VLAN_REMOVE_FORBIDDEN_PORTS_FROM_PORTLIST (pVlanRec->pStVlanEntry,
                                                   pFwdPortList);
    }
    pLocalTagPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalTagPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleStMcastDeletion: Error in allocating memory "
                  "for pLocalTagPortList\r\n");
        UtilPlstReleaseLocalPortList (pFwdPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    VLAN_MEMSET (pLocalTagPortList, 0, sizeof (tLocalPortList));

    pLocalUntagPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalUntagPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleStMcastDeletion: Error in allocating memory "
                  "for pLocalUntagPortList\r\n");
        UtilPlstReleaseLocalPortList (pFwdPortList);
        UtilPlstReleaseLocalPortList (pLocalTagPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    VLAN_MEMSET (pLocalUntagPortList, 0, sizeof (tLocalPortList));

    VLAN_SCAN_PORT_TABLE (u2OutPort)
    {
        u1IsMemberPort = VLAN_FALSE;
        u1IsUnTaggedPort = VLAN_FALSE;

        VLAN_IS_MEMBER_PORT (pFwdPortList, u2OutPort, u1IsMemberPort)
            if (u1IsMemberPort == VLAN_FALSE)
        {
            continue;
        }

        i4RetVal = VlanEgressFiltering (u2OutPort, pVlanRec, DestAddr);

        if (i4RetVal == VLAN_FORWARD)
        {
            if (pVlanRec->pStVlanEntry != NULL)
            {
                VLAN_IS_UNTAGGED_PORT (pVlanRec->pStVlanEntry,
                                       u2OutPort, u1IsUnTaggedPort);
            }
            if (u1IsUnTaggedPort == VLAN_TRUE)
            {
            VLAN_SET_MEMBER_PORT (pLocalUntagPortList, u2OutPort)}
            else
            {
            VLAN_SET_MEMBER_PORT (pLocalTagPortList, u2OutPort)}
        }
    }

    VlanConvertToIfPortList (pLocalTagPortList, TagPortList);
    VlanConvertToIfPortList (pLocalUntagPortList, UntagPortList);

    UtilPlstReleaseLocalPortList (pFwdPortList);
    UtilPlstReleaseLocalPortList (pLocalTagPortList);
    UtilPlstReleaseLocalPortList (pLocalUntagPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrGetVlanIfOperStatus                       */
/*                                                                           */
/*    Description         :This function is for returning the oper status    */
/*                          of VLAN interface                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : pu1OperStatus - Oper status of the VLAN interface*/
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrGetVlanIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
#ifdef WGS_WANTED
    UINT1               u1VlanFlag = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    tMgmtVlanList       MgmtVlanList;
#endif
    tVlanId             VlanId;
    UINT4               u4L2CxtId = 0;

    *pu1OperStatus = CFA_IF_DOWN;

#ifndef WGS_WANTED
    if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return;
    }
#else
    u4L2CxtId = VLAN_DEF_CONTEXT_ID;
#endif

    VLAN_LOCK ();
    if (VlanSelectContext (u4L2CxtId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }

#ifdef WGS_WANTED

    MEMSET (MgmtVlanList, 0, sizeof (tMgmtVlanList));

    if (u4IfIndex == CFA_MGMT_IF_INDEX)
    {
        if (CfaGetMgmtVlanList (MgmtVlanList) == CFA_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return;
        }
        for (u2ByteInd = 0; u2ByteInd < CFA_MGMT_VLAN_LIST_SIZE; u2ByteInd++)
        {
            if (MgmtVlanList[u2ByteInd] != 0)
            {
                u1VlanFlag = MgmtVlanList[u2ByteInd];

                for (u2BitIndex = 0;
                     ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1VlanFlag != 0));
                     u2BitIndex++)
                {
                    if ((u1VlanFlag & VLAN_BIT8) != 0)
                    {
                        VlanId =
                            (tVlanId) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                       u2BitIndex + 1);

                        VlanIvrGetOperStatus (VlanId, pu1OperStatus);

                        if (*pu1OperStatus == CFA_IF_UP)
                        {
                            VlanReleaseContext ();
                            VLAN_UNLOCK ();
                            return;
                        }
                    }
                    u1VlanFlag = (UINT1) (u1VlanFlag << 1);
                }
            }
        }
    }
#else
    if (VlanCfaGetVlanId (u4IfIndex, &VlanId) == CFA_SUCCESS)
    {
        VlanIvrGetOperStatus (VlanId, pu1OperStatus);
    }
#endif
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetStartedStatus                             */
/*                                                                           */
/*    Description         : This function returns whether VLAN is started or */
/*                          shutdown                                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gau1VlanShutDownStatus                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE or VLAN_FALSE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetStartedStatus (UINT4 u4ContextId)
{
    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_FALSE)
    {
        return VLAN_TRUE;
    }
    else
    {
        return VLAN_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetEnabledStatus                             */
/*                                                                           */
/*    Description         : This function returns whether VLAN is enabled or */
/*                          not in the system                                */
/*                                                                           */
/*    Input(s)            : None                          .                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gau1VlanStatus                              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE or VLAN_FALSE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetEnabledStatus ()
{
    if (VLAN_MODULE_STATUS_FOR_CONTEXT (VLAN_DEF_CONTEXT_ID) == VLAN_ENABLED)
    {
        return VLAN_TRUE;
    }
    else
    {
        return VLAN_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsVlanStaticAndRegNormal                     */
/*                                                                           */
/*    Description         : This function checks if a static Vlan Entry is   */
/*                          available for the given Vlan Id and if the       */
/*                          VlanId is registered NORMAL in u2Port            */
/*                                                                           */
/*                                 .                                         */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID                                 */
/*                          u2Port - Port Number for which Reg NORMAL is to  */
/*                          be checked.                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsVlanStaticAndRegNormal (UINT4 u4ContxtId, UINT2 VlanId, UINT2 u2Port)
{

    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    INT4                i4RetVal = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContxtId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }
    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry != NULL)
    {
        if (pVlanCurrEntry->pStVlanEntry != NULL)
        {

            if (pVlanCurrEntry->pStVlanEntry->VlanId == VlanId)
            {
                VLAN_IS_EGRESS_PORT (pVlanCurrEntry->pStVlanEntry,
                                     u2Port, u1Result);

                if (u1Result != VLAN_TRUE)
                {
                    VLAN_IS_FORBIDDEN_PORT (pVlanCurrEntry->pStVlanEntry,
                                            u2Port, u1Result);

                    if (u1Result != VLAN_TRUE)
                    {
                        i4RetVal = VLAN_TRUE;
                        VlanReleaseContext ();
                        VLAN_UNLOCK ();
                        return i4RetVal;
                    }
                }

            }
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMcastStaticAndRegNormal                    */
/*                                                                           */
/*    Description         : This function checks if the Multicast MacAddress */
/*                          is registered statically for the given VlanId    */
/*                          and checks if the MacAddress is registered NORMAL*/
/*                          on u2Port                                        */
/*                                                                           */
/*    Input(s)            : MacAddr -  MAC address                           */
/*                          u2VlanId - VlanId                                */
/*                          u2Port   - Port Number                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMcastStaticAndRegNormal (UINT4 u4ContxtId, tMacAddr MacAddr,
                               UINT2 u2VlanId, UINT2 u2Port)
{
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    INT4                i4RetVal = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContxtId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }
    pVlanEntry = VlanGetVlanEntry (u2VlanId);

    if (pVlanEntry != NULL)
    {
        pStMcastEntry = pVlanEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            /* Check if the Mac Address from the Input and the Mac Address in the 
             * static Multicast Table are equal */

            if (VLAN_ARE_MAC_ADDR_EQUAL (MacAddr, pStMcastEntry->MacAddr)
                == VLAN_TRUE)
            {
                VLAN_IS_MEMBER_PORT (pStMcastEntry->EgressPorts, u2Port,
                                     u1Result);
                if (u1Result != VLAN_TRUE)
                {
                    VLAN_IS_MEMBER_PORT (pStMcastEntry->ForbiddenPorts, u2Port,
                                         u1Result);
                    if (u1Result != VLAN_TRUE)
                    {
                        i4RetVal = VLAN_TRUE;
                        VlanReleaseContext ();
                        VLAN_UNLOCK ();
                        return i4RetVal;
                    }
                }
                break;

            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

#ifdef VLAN_EXTENDED_FILTER
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsServiceAttrStaticAndRegNormal              */
/*                                                                           */
/*    Description         : This function checks if the Service Attribute    */
/*                          is registered statically for the given VlanId    */
/*                          and checks if the MacAddress is registered NORMAL*/
/*                          on u2Port                                        */
/*                                                                           */
/*    Input(s)            : u1Type   -  Service Attribute Type               */
/*                          u2VlanId - VlanId                                */
/*                          u2Port   - Port Number                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsServiceAttrStaticAndRegNormal (UINT4 u4ContxtId, UINT1 u1Type,
                                     UINT2 u2VlanId, UINT2 u2Port)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    INT4                i4RetVal = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContxtId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (u2VlanId);

    if (pVlanCurrEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    if (pVlanCurrEntry->pStVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    if (u1Type == VLAN_ALL_GROUPS)
    {
        VLAN_IS_MEMBER_PORT (pVlanCurrEntry->AllGrps.StaticPorts,
                             u2Port, u1Result);
        if (u1Result != VLAN_TRUE)
        {
            VLAN_IS_MEMBER_PORT (pVlanCurrEntry->AllGrps.ForbiddenPorts,
                                 u2Port, u1Result);
            if (u1Result != VLAN_TRUE)
            {
                i4RetVal = VLAN_TRUE;
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return i4RetVal;
            }
        }
    }
    else
    {
        VLAN_IS_MEMBER_PORT (pVlanCurrEntry->UnRegGrps.StaticPorts,
                             u2Port, u1Result);
        if (u1Result != VLAN_TRUE)
        {
            VLAN_IS_MEMBER_PORT (pVlanCurrEntry->UnRegGrps.
                                 ForbiddenPorts, u2Port, u1Result);
            if (u1Result != VLAN_TRUE)
            {
                i4RetVal = VLAN_TRUE;
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return i4RetVal;
            }

        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}
#endif /*GARP_EXTENDED_FILTER */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckAndTagOutgoingGmrpFrame                 */
/*                                                                           */
/*    Description         : This function is called by GARP/MRP module to    */
/*                          transmit GARP/MRP PDUs.                          */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanId   - VlanId                                */
/*                          u2Port   - Port on which the GARP PDU is to be   */
/*                                     transmitted.                          */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD/                                     */
/*                         VLAN_NO_FORWARD                                   */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckAndTagOutgoingGmrpFrame (UINT4 u4ContextId, tCRU_BUF_CHAIN_DESC * pBuf,
                                  tVlanId VlanId, UINT2 u2Port)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    /* Egress Filtering */
    VLAN_IS_CURR_EGRESS_PORT (pVlanEntry, u2Port, u1Result);

    if (u1Result != VLAN_TRUE)
    {
        if (u2Port <= VLAN_MAX_PORTS)
        {
            /* EGRESS FILTERING - Out Port should be a
               member port of the Vlan. */
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                           "Outbound local port %d is NOT a member port of Vlan %d."
                           "Frame filtered. \n", u2Port, pVlanEntry->VlanId);
        }

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    /* Check whether the u2Port is a member of UnTagPorts */
    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_IS_UNTAGGED_PORT (pVlanEntry->pStVlanEntry, u2Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FORWARD;
        }
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    VlanAddTagToFrame (pBuf, VlanId, VLAN_HIGHEST_PRIORITY, VLAN_DE_FALSE,
                       pVlanPortEntry);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIdentifyVlanIdAndUntagFrame                  */
/*                                                                           */
/*    Description         : This function is called by GARP/IGS/IVR to get   */
/*                          VlanId                                           */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : i4ModId  - Module for which the packet is to be  */
/*                                     given.                                */
/*                        : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pFrame   - Pointer to the updated packet         */
/*                          pVlanId  - Gip Associated with the packet.       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIdentifyVlanIdAndUntagFrame (INT4 i4ModId, tCRU_BUF_CHAIN_DESC * pFrame,
                                 UINT4 u4InPort, tVlanId * pVlanId)
{
    tMacAddr            DestAddr;
    tMacAddr            SrcAddr;
    tVlanId             VlanId;
    tVlanTag            VlanTag;
    tVlanCurrEntry     *pVlanRec;
    tVlanPortEntry     *pPortEntry;
    INT4                i4Result;
    INT4                i4IsFrameDiscarded = VLAN_FALSE;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1PortState;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1StripTag = VLAN_TRUE;
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan;
#endif

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4InPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    *pVlanId = 0;

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanL2IwfIncrFilterInDiscards (u2LocalPort);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC,
                  VLAN_NAME, "Port Oper Status DOWN. Frame received\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    VLAN_COPY_FROM_BUF (pFrame, &DestAddr, 0, ETHERNET_ADDR_SIZE);
    VLAN_COPY_FROM_BUF (pFrame, &SrcAddr, ETHERNET_ADDR_SIZE,
                        ETHERNET_ADDR_SIZE);

    VlanId = VlanGetVlanId (pFrame, SrcAddr, u2LocalPort, &VlanTag);

    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "VLAN ID 0x%x associated with the Frame "
                       "is unknown \n", VlanId);

        VlanL2IwfIncrFilterInDiscards (u2LocalPort);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    i4Result = VlanIngressFiltering (pVlanRec, u2LocalPort);

    if (i4Result == VLAN_NO_FORWARD)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Frame received on port %d is discarded due"
                       " to Ingress Filtering.\n", u4InPort);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

            if (NULL != pPortInfoPerVlan)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif /*NPAPI_WANTED */

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    u1PortState = VlanL2IwfGetVlanPortState (VlanId, u4InPort);

    VLAN_IS_CURR_EGRESS_PORT (pVlanRec, u2LocalPort, u1Result);

    switch (i4ModId)
    {
        case SNOOP_MODULE:

            if (u1PortState != AST_PORT_STATE_FORWARDING)
            {
                i4IsFrameDiscarded = VLAN_TRUE;
            }

            if (u1Result == VLAN_FALSE)
            {
                u1StripTag = VLAN_FALSE;
            }

            break;

        case GARP_MODULE:

            if (u1Result == VLAN_FALSE)
            {
                i4IsFrameDiscarded = VLAN_TRUE;
            }
            break;

        case MRP_MODULE:

            if (u1Result == VLAN_FALSE)
            {
                i4IsFrameDiscarded = VLAN_TRUE;
            }
            break;

        case IVR_MODULE:

            if (u1PortState != AST_PORT_STATE_FORWARDING)
            {
                i4IsFrameDiscarded = VLAN_TRUE;
            }

            if (u1Result == VLAN_FALSE)
            {
                i4IsFrameDiscarded = VLAN_TRUE;
            }
            break;

        case RMON_MODULE:
            i4IsFrameDiscarded = VLAN_FALSE;
            u1StripTag = VLAN_FALSE;
            break;

        case PTP_MODULE:

            if (u1PortState != AST_PORT_STATE_FORWARDING)
            {
                i4IsFrameDiscarded = VLAN_TRUE;
            }
            break;

        default:
            i4IsFrameDiscarded = VLAN_TRUE;
            break;
    }

    if (i4IsFrameDiscarded == VLAN_TRUE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Frame received on port %d is discarded ", u4InPort);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

            if (NULL != pPortInfoPerVlan)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif /*NPAPI_WANTED */

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

#ifndef NPAPI_WANTED

    if (u1Result == VLAN_TRUE)
    {
        VlanLearn (SrcAddr, u2LocalPort, pVlanRec);
    }

    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

        if (NULL != pPortInfoPerVlan)
        {
            VLAN_INC_IN_FRAMES (pPortInfoPerVlan);
        }
    }
#endif

    if ((VlanTag.OuterVlanTag.u1TagType != VLAN_UNTAGGED) &&
        (u1StripTag == VLAN_TRUE))
    {

        VlanUnTagAllInFrame (pFrame, u2LocalPort);
    }

    *pVlanId = VlanId;

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*    Function Name       : VlanMiSnoopGetTxPortList                         */
/*                                                                           */
/*    Description         : This function is invoked by IGS to get the       */
/*                          list of ports for Transmission of packet for a   */
/*                          given instance.                                  */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance Id                         */
/*                        : DestAddr - Destination mac address of the frame  */
/*                          u2InPort - Port on which the packet is received  */
/*                          VlanId   - Vlan ID associated with the packet    */
/*                          u1Action - Flag  to indicate whether the pkt     */
/*                                     should be Forwarded on all ports or   */
/*                                     on the specific ports given by IGS    */
/*                          IgsPortList - a specific Port list in which      */
/*                                        the packet s to be transmitted.    */
/*                                                                           */
/*    Output(s)           : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanMiSnoopGetTxPortList (UINT4 u4ContextId, tMacAddr DestAddr, UINT4 u4InPort,
                          tVlanId VlanId, UINT1 u1Action,
                          tPortList IgsPortList, tPortList TagPortList,
                          tPortList UntagPortList)
{
    UINT1              *pLocalFwdPortList = NULL;
    tVlanCurrEntry     *pVlanEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tPortList          *pFwdPortList = NULL;
    tPortList          *pTmpPortList = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT4               u4Forward = VLAN_FALSE;
    UINT4               u4TempContextId;
    UINT2               u2LocalOutPort;
    UINT2               u2LocalInPort = 0;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (u4InPort != VLAN_INVALID_PORT)
    {
        if (VlanGetContextInfoFromIfIndex (u4InPort, &u4TempContextId,
                                           &u2LocalInPort) != VLAN_SUCCESS)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_NO_FORWARD;
        }
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    pFwdPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pFwdPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    pTmpPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTmpPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    VLAN_MEMSET (*pFwdPortList, 0, sizeof (tPortList));
    VLAN_MEMSET (*pTmpPortList, 0, sizeof (tPortList));

    if (u1Action == VLAN_FORWARD_ALL)
    {
        pLocalFwdPortList =
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pLocalFwdPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanHandleCopyPortMcastPropertiesToHw: Error in allocating memory "
                      "for pLocalFwdPortList\r\n");
            FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
            FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_NO_FORWARD;
        }

        VLAN_MEMSET (pLocalFwdPortList, 0, sizeof (tLocalPortList));
        {
            /*
             * Received packet is being forwarded based on
             * Multicast forwarding database
             */
            pGroupEntry = VlanGetGroupEntry (DestAddr, pVlanEntry);

            if (pGroupEntry != NULL)
            {
                if (MEMCMP (pGroupEntry->LearntPorts,
                            gNullPortList, sizeof (tLocalPortList)) == 0)
                {
#ifdef VLAN_EXTENDED_FILTER
                    /* Use unregister group ports if no dynamic info is present */
                    MEMCPY (pLocalFwdPortList, pVlanEntry->UnRegGrps.Ports,
                            sizeof (tLocalPortList));
#endif
                }
                else
                {
                    MEMCPY (pLocalFwdPortList, pGroupEntry->LearntPorts,
                            sizeof (tLocalPortList));
                }

                if (pGroupEntry->u1StRefCount != 0)
                {
                    /* some static entry exist for this mcast address */
                    pStMcastEntry =
                        VlanGetStMcastEntry (DestAddr, u2LocalInPort,
                                             pVlanEntry);
                }
            }
            else
            {
#ifdef VLAN_EXTENDED_FILTER
                MEMCPY (pLocalFwdPortList, pVlanEntry->UnRegGrps.Ports,
                        sizeof (tLocalPortList));
#else
                VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanEntry,
                                                    pLocalFwdPortList);
#endif
            }

#ifdef VLAN_EXTENDED_FILTER
            VLAN_ADD_PORT_LIST (pLocalFwdPortList, pVlanEntry->AllGrps.Ports);
#endif

            if (pStMcastEntry != NULL)
            {
                VLAN_ADD_PORT_LIST (pLocalFwdPortList,
                                    pStMcastEntry->EgressPorts);
                VLAN_RESET_PORT_LIST (pLocalFwdPortList,
                                      pStMcastEntry->ForbiddenPorts);
            }
        }
        VlanConvertToIfPortList (pLocalFwdPortList, *pFwdPortList);
        UtilPlstReleaseLocalPortList (pLocalFwdPortList);
    }
    else
    {
        /*
         * u1Action == VLAN_FORWARD_SPECIFIC.
         * Need to transmit on the ports specified by IGS
         */
        VLAN_MEMCPY (pFwdPortList, IgsPortList, sizeof (tPortList));
    }

    if (u4InPort != VLAN_INVALID_PORT)
    {
        VLAN_RESET_MEMBER_PORT_LIST ((*pFwdPortList), u4InPort);
    }

    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pVlanEntry->pStVlanEntry,
                                              (*pTmpPortList));
        OSIX_RESET_PORT_LIST ((*pFwdPortList), (*pTmpPortList),
                              sizeof (tPortList), sizeof (tPortList));
    }

    MEMSET (UntagPortList, 0, sizeof (tPortList));
    MEMSET (TagPortList, 0, sizeof (tPortList));

    L2PvlanMappingInfo.u4ContextId = u4ContextId;
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

    /* Get the PVLAN Mapping information. */
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    VLAN_SCAN_PORT_TABLE (u2LocalOutPort)
    {
        VLAN_IS_MEMBER_PORT_LIST ((*pFwdPortList),
                                  VLAN_GET_PHY_PORT (u2LocalOutPort), u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if (VlanEgressFiltering (u2LocalOutPort, pVlanEntry,
                                     DestAddr) == VLAN_FORWARD)
            {
                u4Forward = VLAN_TRUE;
                VLAN_SET_MEMBER_PORT_LIST (TagPortList,
                                           VLAN_GET_PHY_PORT (u2LocalOutPort));

                if (pVlanEntry->pStVlanEntry != NULL)
                {
                    VLAN_IS_UNTAGGED_PORT (pVlanEntry->pStVlanEntry,
                                           u2LocalOutPort, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        VLAN_SET_MEMBER_PORT_LIST (UntagPortList,
                                                   VLAN_GET_IFINDEX
                                                   (u2LocalOutPort));
                        VLAN_RESET_MEMBER_PORT_LIST (TagPortList,
                                                     VLAN_GET_IFINDEX
                                                     (u2LocalOutPort));
                    }
                }
            }
        }
    }
    FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
    VlanReleaseContext ();

    if (u4Forward == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    VLAN_UNLOCK ();
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*    Function Name       : VlanSnoopGetTxPortList                           */
/*                                                                           */
/*    Description         : This function is invoked by IGS to get the       */
/*                          list of ports for Transmission of packet.        */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : DestAddr - Destination mac address of the frame  */
/*                          u4InPort - Port on which the packet is received  */
/*                          VlanId   - Vlan ID associated with the packet    */
/*                          u1Action - Flag  to indicate whether the pkt     */
/*                                     should be Forwarded on all ports or   */
/*                                     on the specific ports given by IGS    */
/*                          IgsPortList - a specific Port list in which      */
/*                                        the packet s to be transmitted.    */
/*                                                                           */
/*    Output(s)           : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanSnoopGetTxPortList (tMacAddr DestAddr, UINT4 u4InPort,
                        tVlanId VlanId, UINT1 u1Action,
                        tPortList IgsPortList, tPortList TagPortList,
                        tPortList UntagPortList)
{
    /* call MI call with Default context Id */
    return VlanMiSnoopGetTxPortList (VLAN_DEF_CONTEXT_ID, DestAddr,
                                     u4InPort, VlanId, u1Action, IgsPortList,
                                     TagPortList, UntagPortList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckReservedGroupForFwding                  */
/*                                                                           */
/*    Description         : This function checks whether the given           */
/*                          MacAddress belongs to a resevered group address  */
/*                          in a context.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId -  Context ID                        */
/*                          Address     - Destination address to be verified */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : VLAN_TRUE / VLAN_FALSE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckReservedGroupForFwding (UINT4 u4ContextId, tMacAddr Address)
{
    INT4                i4RetVal = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    i4RetVal = VlanCheckReservedGroup (Address);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUnTagFrame                                   */
/*                                                                           */
/*    Description         : This function removes the Vlan Tag from the      */
/*                          incoming packet.                                 */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanUnTagFrame (tCRU_BUF_CHAIN_DESC * pFrame)
{
    UINT1               au1EnetHdr[VLAN_TAG_OFFSET];

    VLAN_COPY_FROM_BUF (pFrame, au1EnetHdr, 0, VLAN_TAG_OFFSET);

    VLAN_MOVE_OFFSET (pFrame, VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN);

    VLAN_PREPEND_BUF (pFrame, au1EnetHdr, VLAN_TAG_OFFSET);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsVlanDynamic                                */
/*                                                                           */
/*    Description         : This function checks if the VLAN is dynamically  */
/*    Input(s)            : VlanId - Vlan ID                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsVlanDynamic (UINT4 u4ContextId, UINT2 VlanId)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    INT4                i4RetVal = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry != NULL)
    {
        if (pVlanCurrEntry->pStVlanEntry == NULL)
        {
            i4RetVal = VLAN_TRUE;
        }
        else
        {
            i4RetVal = VLAN_FALSE;
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : VlanSetShortAgeoutTime                               */
/*                                                                           */
/*    Description     : This function applies a short ageout time for aging  */
/*                      out dynamically learnt mac address on this port.     */
/*                       This function will be called                        */
/*                         By RSTP when operating in StpCompatible mode      */
/*                         during topology change in the port                */
/*                                                                           */
/*    Input(s)        : u2Port - port number                                 */
/*                      i4Agingtime - The short age out time to be applied   */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetShortAgeoutTime (UINT4 u4PortNum, INT4 i4AgingTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

#if !(! defined (SW_AGE_TIMER) && defined (NPAPI_WANTED))    /*When SW_AGE_TIMER 
                                                               is defined or 
                                                               NPAPI_WANTED is not
                                                               defined */
    tVlanQMsg          *pVlanQMsg = NULL;
#endif

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4PortNum, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }
#if ! defined (SW_AGE_TIMER) && defined (NPAPI_WANTED)
    UNUSED_PARAM (u2LocalPort);
    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return VLAN_SUCCESS;
    }

    /* Though the aging timer runs in the software to age out multicast
     * entries, the short ageout should be applied only to the FDB entries
     * and hence is applied only to the hardware */
    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_GET_PORT_ENTRY (u2LocalPort) == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanHwSetShortAgingTime (u4ContextId, u2LocalPort, i4AgingTime);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
#else

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n", VLAN_SHORT_AGEOUT_MSG);

        return VLAN_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_SHORT_AGEOUT_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_SHORT_AGEOUT_MSG;
    pVlanQMsg->u4Port = u4PortNum;
    pVlanQMsg->i4AgingTime = i4AgingTime;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_SHORT_AGEOUT_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : VlanResetShortAgeoutTime                             */
/*                                                                           */
/*    Description     : This function reverts back to the long ageout time   */
/*                      for aging out out dynamically learnt mac address on  */
/*                      this port.                                           */
/*                       This function will be called                        */
/*                         By RSTP when operating in StpCompatible mode      */
/*                                                                           */
/*    Input(s)        : u2Port - port number                                 */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanResetShortAgeoutTime (UINT4 u4PortNum)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort;

#if ! defined (SW_AGE_TIMER) && defined (NPAPI_WANTED)
    INT4                i4LongAgeout;
#else
    tVlanQMsg          *pVlanQMsg = NULL;
#endif

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4PortNum, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

#if ! defined (SW_AGE_TIMER) && defined (NPAPI_WANTED)

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return VLAN_SUCCESS;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_GET_PORT_ENTRY (u2LocalPort) == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    i4LongAgeout = VlanL2IwfGetAgeoutInt ();

    VlanHwResetShortAgingTime (u4ContextId, u2LocalPort, i4LongAgeout);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
#else

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n", VLAN_LONG_AGEOUT_MSG);

        return VLAN_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_LONG_AGEOUT_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_LONG_AGEOUT_MSG;
    pVlanQMsg->u4Port = u4PortNum;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_LONG_AGEOUT_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {

        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "Sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }

    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
#endif
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : VlanGetNodeStatus                                       */
/*                                                                            */
/*  Description     : This function will be invoked by GARP module to get     */
/*                    the node status from the VLAN module. This function     */
/*                    will be invoked when the GARP module is started.        */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
tVlanNodeStatus
VlanGetNodeStatus (VOID)
{
    return VLAN_NODE_STATUS ();
}

/*****************************************************************************/
/*    Function Name   : VlanGetVlanMemberPorts                               */
/*                                                                           */
/*    Description     : This function is used to retrieve the member ports   */
/*                      of the given VlanId                                  */
/*                                                                           */
/*    Input(s)        : VlanId - Vlan Identifier                             */
/*                                                                           */
/*    Output(s)       : EgressPortList - Member Ports of Vlan                */
/*                      UntagPortList - Untag Member Ports of Vlan           */
/*                                                                           */
/*    Returns         : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetVlanMemberPorts (tVlanId VlanId, tPortList EgressPortList,
                        tPortList UntagPortList)
{
    tVlanCurrEntry     *pVlanEntry;
    tStaticVlanEntry   *pStVlanEntry;

    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VLAN_FALSE == VLAN_IS_VLAN_ENABLED ())
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (NULL == pVlanEntry)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pStVlanEntry = pVlanEntry->pStVlanEntry;

    if (NULL == pStVlanEntry)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    MEMSET (UntagPortList, 0, sizeof (tPortList));
    MEMSET (EgressPortList, 0, sizeof (tPortList));

    VLAN_CONVERT_CURR_EGRESS_TO_IFPORTLIST (pVlanEntry, EgressPortList);
    VLAN_CONVERT_UNTAGGED_TO_IFPORTLIST (pStVlanEntry, UntagPortList);

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetFdbEntryDetails                           */
/*                                                                           */
/*    Description         : This function returns Fdb Entry Information.     */
/*                                                                           */
/*    Input(s)            : u4FdbId - Fdb Id.                                */
/*                          MacAddr - Mac Address.                           */
/*                                                                           */
/*    Output(s)           : pu2Port - Port on which the Mac Addr is learnt.  */
/*                        : pu1Status - Flag for Learnt or not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetFdbEntryDetails (UINT4 u4FdbId, tMacAddr MacAddr,
                        UINT2 *pu2Port, UINT1 *pu1Status)
{
    INT4                i4RetVal = VLAN_FAILURE;
    tVlanTempFdbInfo    VlanTempFdbInfo;

    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_TRUE == VLAN_IS_VLAN_ENABLED ())
    {
        i4RetVal = VlanGetFdbInfo (u4FdbId, MacAddr, &VlanTempFdbInfo);
        *pu2Port = VlanTempFdbInfo.u2Port;
        *pu1Status = VlanTempFdbInfo.u1Status;
        if (*pu1Status == VLAN_FDB_LEARNT)
        {
            *pu2Port = VLAN_GET_IFINDEX (*pu2Port);
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name       : VlanCheckPortListPortTypeValid ()                   */
/*                                                                           */
/* Description         : This function returns false if any of the ports in  */
/*                       the portlist is of port type given in the input     */
/*                       as a parameter.                                     */
/*                                                                           */
/* Input(s)            : pu1Ports - A list of egress ports/                  */
/*                                  A list of untagged ports                 */
/*                       i4Len    - Port list length                         */
/*                       u1PortType - VLAN_ACCESS_PORT/VLAN_TRUNK_PORT       */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred            : Vlan Port Table                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified            : None.                                               */
/*                                                                           */
/* Exceptions or OS                                                          */
/* Error Handling      : None.                                               */
/*                                                                           */
/* Use of Recursion    : None.                                               */
/*                                                                           */
/* Returns             : VLAN_TRUE/VLAN_FALSE                                */
/*****************************************************************************/

INT4
VlanCheckPortListPortTypeValid (UINT1 *pu1Ports, INT4 i4Len, UINT1 u1PortType)
{
    VLAN_LOCK ();

    if (VlanIsPortListPortTypeValid (pu1Ports, i4Len, u1PortType) == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    VLAN_UNLOCK ();
    return VLAN_TRUE;
}

INT4
VlanIsVlanEnabledInContext (UINT4 u4ContextId)
{
    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_ENABLED)
    {
        return VLAN_TRUE;
    }
    else
    {
        return VLAN_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiUpdateDynamicMcastInfo                     */
/*                                                                           */
/*    Description         : This function updated the Multicast table.       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the multicast  */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiUpdateDynamicMcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                              tVlanId VlanId, UINT4 u4Port, UINT1 u1Action)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    INT4                i4DynInfoFlag;
/* To avoid deletion of member port when it is not actually */
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4TempContext;
    UINT2               u2LocalPort;

    VLAN_LOCK ();
    if (u1Action == VLAN_ADD || u1Action == VLAN_DELETE)
    {
        if (VlanGetContextInfoFromIfIndex (u4Port, &u4TempContext,
                                           &u2LocalPort) == VLAN_FAILURE)
        {
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }
        if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
        {
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (u1Action == VLAN_ADD || u1Action == VLAN_DELETE)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry == NULL)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }

        pGroupEntry = VlanGetGroupEntry (MacAddr, pVlanEntry);
    }

    switch (u1Action)
    {

        case VLAN_ADD:

            if (pGroupEntry == NULL)
            {
                if (VlanPbIsMulticastMacLimitExceeded () == VLAN_TRUE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME, "Exceeding Multicast MAC limit \n");

                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return VLAN_FAILURE;
                }

                i4RetVal =
                    VlanHwAddDynMcastEntry (VlanId, MacAddr, u2LocalPort);

                if (i4RetVal == VLAN_FAILURE)
                {
                    break;
                }

                pGroupEntry = (tVlanGroupEntry *) (VOID *)
                    VLAN_GET_BUF (VLAN_GROUP_ENTRY, sizeof (tVlanGroupEntry));

                if (pGroupEntry == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME, "No Free Group Entry \n");

                    i4RetVal = VLAN_FAILURE;
                    break;
                }

                MEMSET (pGroupEntry, 0, sizeof (tVlanGroupEntry));
                /* 
                 * no static entry will be there... so we can directly
                 * update the port lists...
                 */
                VLAN_CPY_MAC_ADDR (pGroupEntry->MacAddr, MacAddr);

                VLAN_SET_MEMBER_PORT (pGroupEntry->LearntPorts, u2LocalPort);

                /* Adding the entry to the group table */
                pGroupEntry->pNextNode = pVlanEntry->pGroupTable;

                pVlanEntry->pGroupTable = pGroupEntry;

                VlanRedDelGroupDeletedEntry (VlanId, MacAddr);

                VlanRedSetGroupChangedFlag (pVlanEntry, pGroupEntry);

                VlanPbUpdateCurrentMulticastMacCount (VLAN_ADD);
#ifdef SW_LEARNING
                VlanUpdateCurrentMulticastDynamicMacCount (VlanId, VLAN_ADD);
#endif

            }
            else
            {
                /* Group entry is already present in the VLAN Group table */
                /* 
                 * Check whether this port is already a member of this group.
                 * If so we can avoid the programming of the hardware.
                 */
                i4DynInfoFlag = VLAN_GROUP_DYN_INFO_PRESENT;

                if (MEMCMP (pGroupEntry->LearntPorts, gNullPortList,
                            sizeof (tLocalPortList)) == 0)
                {
                    /* first dynamic member in the group...so check and
                     * delete the entry from the VLAN deleted table */
                    i4DynInfoFlag = VLAN_GROUP_DYN_INFO_NOT_PRESENT;
                }

                VLAN_IS_MEMBER_PORT (pGroupEntry->LearntPorts, u2LocalPort,
                                     u1Result);

                if (u1Result == VLAN_FALSE)
                {
                    /* Port is not a member port of the vlan so add it. */
                    i4RetVal =
                        VlanHwSetMcastPort (VlanId, MacAddr, u2LocalPort);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        break;
                    }
                    VLAN_SET_MEMBER_PORT (pGroupEntry->LearntPorts,
                                          u2LocalPort);

                    VlanRedSetGroupChangedFlag (pVlanEntry, pGroupEntry);

                    if (i4DynInfoFlag == VLAN_GROUP_DYN_INFO_NOT_PRESENT)
                    {
                        /* 
                         * In case if the last member leaves and if we have
                         * st. mcast entry we wud have sent a delete update
                         * message to the standby node. So as a result of
                         * this when the first member comes, we need to 
                         * check whether the delete update is still 
                         * present or not...if present just delete that.
                         */
                        VlanRedDelGroupDeletedEntry (VlanId, MacAddr);
                    }
                }
            }

            VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Adding Multicast Member port %d for VLAN %d with "
                           "MAC address ", u4Port, VlanId);
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncMcastSwUpdate (u4ContextId, VlanId, MacAddr, 0,
                                      VLAN_ADD);
#endif /* L2RED_WANTED */

            VLAN_PRINT_MAC_ADDR (VLAN_MOD_TRC, MacAddr);
            break;

        case VLAN_DELETE:

            if (pGroupEntry == NULL)
            {
                i4RetVal = VLAN_FAILURE;
                break;
            }

            /* This check has been introduced to check if the port to be deleted
             * from group entry is present in static multicast entry. If so,
             * do not delete the port from hardware */

            pStMcastEntry = VlanGetStMcastEntry (MacAddr, 0, pVlanEntry);

            if (pStMcastEntry != NULL)
            {
                VLAN_IS_MEMBER_PORT (pStMcastEntry->EgressPorts, u2LocalPort,
                                     u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    i4RetVal = VLAN_SUCCESS;
                    break;
                }
            }

            /* avoid giving delete if not a member port of learnt ports */
            VLAN_IS_MEMBER_PORT (pGroupEntry->LearntPorts, u2LocalPort,
                                 u1Result);

            if (u1Result == VLAN_FALSE)
            {
                /* The port to be deleted is not a member of learnt port list */
                i4RetVal = VLAN_FAILURE;
                break;
            }

            VlanHwResetMcastPort (VlanId, MacAddr, u2LocalPort);

            i4DynInfoFlag = VlanDeleteGroupPortInfo (pVlanEntry,
                                                     pGroupEntry, u2LocalPort);
            if (i4DynInfoFlag == VLAN_GROUP_DYN_INFO_NOT_PRESENT)
            {
                /* 
                 * no more dynamic info...send delete update message.
                 * The group entry might or might not be present 
                 * depending on the static mcast entry.
                 */
                VlanRedAddGroupDeletedEntry (VlanId, MacAddr);
            }
            else
            {
                /* more learnt members present...group entry must be there */
                VlanRedSetGroupChangedFlag (pVlanEntry, pGroupEntry);
            }

            VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Deleting Multicast Member port %d for VLAN %d with "
                           "MAC address ", u4Port, VlanId);

            VLAN_PRINT_MAC_ADDR (VLAN_MOD_TRC, MacAddr);

#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncMcastSwUpdate (u4ContextId, VlanId, MacAddr, 0,
                                      VLAN_DELETE);
#endif /* L2RED_WANTED */
            break;

        case VLAN_DROP_UNKNOWN_MCAST:

            if (VlanPbIsMulticastMacLimitExceeded () == VLAN_TRUE)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "Exceeding Multicast MAC limit \n");

                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }

            i4RetVal = VlanHwAddDynMcastEntry (VlanId, MacAddr, u4Port);

            if (i4RetVal == VLAN_FAILURE)
            {
                break;
            }
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Adding Multicast Entry with NULL port vector for VLAN %d with "
                           "MAC address ", VlanId);

            VLAN_PRINT_MAC_ADDR (VLAN_MOD_TRC, MacAddr);
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncMcastSwUpdate (u4ContextId, VlanId, MacAddr, 0,
                                      VLAN_ADD);
#endif /* L2RED_WANTED */

            break;

        case VLAN_CLEAR_UNKNOWN_MCAST:

            VlanHwDelDynMcastEntry (VlanId, MacAddr, (UINT2) u4Port);

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Deleting Multicast entry with NULL port vector for VLAN %d with "
                           "MAC address ", VlanId);

            VLAN_PRINT_MAC_ADDR (VLAN_MOD_TRC, MacAddr);
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncMcastSwUpdate (u4ContextId, VlanId, MacAddr, 0,
                                      VLAN_DELETE);
#endif /* L2RED_WANTED */

            break;

        default:
            i4RetVal = VLAN_FAILURE;
            break;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/* Function added for MMRP */
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiUpdateDynamicUcastInfo                     */
/*                                                                           */
/*    Description         : This function updated the Group table.           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the unicast    */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiUpdateDynamicUcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                              tVlanId VlanId, UINT4 u4Port, UINT1 u1Action)
{
    /* As per the MMRP standard implemntation the dynamic learning of 
     * unicast address also be allowed and it should be updated in 
     * Group table. So the following API is called to update the group table */
    return (VlanMiUpdateDynamicMcastInfo (u4ContextId, MacAddr, VlanId, u4Port,
                                          u1Action));
}

/*****************************************************************************/
/*                                                                           */
/*    function Name       : VlanMiDeleteAllDynamicMcastInfo                  */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table when called from IGS, */
/*                          MLDS and GARP. When called from MRP this function*/
/*                          takes care of deleting both unicast and multicast*/
/*                          addresses learnt from MMRPDU.                    */
/*                                                                           */
/*    Input(s)            : u4ContextID - Context Id                         */
/*                          u2Port - Port Number of the input port           */
/*                          u1MacAddrType - Mac Address type of the dynamic  */
/*                          mcast entries to be deleted (IPV4 or IPV6 mac    */
/*                          address type or both).Value 1 indicates ipv4 mac */
/*                          address type, value 2 indicates ipv6 mac address */
/*                          type and value 3 indicates both type of mac      */
/*                          addresses.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDeleteAllDynamicMcastInfo (UINT4 u4ContextId, UINT4 u4Port,
                                 UINT1 u1MacAddrType)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanHandleDeleteAllDynamicMcastInfo (u4Port, VLAN_FALSE, u1MacAddrType);
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDelDynamicMcastInfoForVlan                 */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table for the specified     */
/*                          Vlan for the given context                       */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDelDynamicMcastInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanHandleDelDynamicMcastInfoForVlan (VlanId);
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return;
}

#ifdef VLAN_EXTENDED_FILTER
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDelDynamicDefGroupInfoForVlan              */
/*                                                                           */
/*    Description         : This function removes all the dynamic default    */
/*                          group information from the table for the         */
/*                          specified Vlan                                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDelDynamicDefGroupInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }

    VlanHandleDelDynamicDefGroupInfoForVlan (VlanId);
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiFlushFdbId                                 */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in FdbId   */
/*                          for the given context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - context Identifier                 */
/*                          u4FdbId - FdbId                                  */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId)
{
#ifndef NPAPI_WANTED
    tVlanQMsg          *pVlanQMsg = NULL;
#endif

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (VlanHwFlushFdbId (u4ContextId, u4FdbId) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }
#else

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return VLAN_SUCCESS;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_FLUSH_FDBID_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FLUSH_FDBID_MSG;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u4FdbId = u4FdbId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_FLUSH_FDBID_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiGetVlanLearningMode                        */
/*                                                                           */
/*    Description         : This function returns the VLAN learning mode     */
/*                          one of IVL/SVL/HYBRID for a given context        */
/*                                                                           */
/*    Input(s)            : None                          .                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->VlanInfo         */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_CURR_CONTEXT_PTR ()->VlanInfo.u1LearningType */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanMiGetVlanLearningMode (UINT4 u4ContextId)
{
    UINT1               u1LearnMode;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_INVALID_LEARNING;
    }
    u1LearnMode = VLAN_LEARNING_MODE ();
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return u1LearnMode;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDeleteAllFdbEntries                        */
/*                                                                           */
/*    Description         : This function calls the Vlan NPAPI to flush      */
/*                          all entries learnt for this context.             */
/*                                                                           */
/*    Input(s)            : u4ContextId.                                     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDeleteAllFdbEntries (UINT4 u4ContextId)
{

    VLAN_LOCK ();
    VlanHwDeleteAllFdbEntries (u4ContextId);
    VLAN_UNLOCK ();

}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetTagLenInFrame                             */
/*                                                                           */
/*    Description         : This function returns the VLAN tag offset from   */
/*                          the incoming frame.  For PBB bridge mode it      */
/*                          returns both VLAN and ISID tag offset if present */
/*                   Get the offset where length/type field needs to be read.*/
/*                   In case of single vlan tagged packet the offset = 16    */
/*                   In case of double vlan tagged packet the offset = 20    */
/*                   In case of B+I+S tagged packet the offset = 38          */
/*                   In case of B+I tagged packet the offset = 34            */
/*                   In case of I+S tagged packet the offset = 34            */
/*                   In case of I tagged packet the offset = 30              */
/*                   For PBB bridge mode it returns both VLAN and ISID tag   */
/*                   offset if present.                                      */
/*                                                                           */
/*    Input(s)            : pFrame     - Incoming frame                      */
/*                                                                           */
/*    Output(s)           : pu4VlanOffset - VLAN tag offset in the incoming  */
/*                                          frame based on the number of     */
/*                                          tags present.                    */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetTagLenInFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                      UINT4 *pu4VlanOffset)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2AggId = 0;
    tCfaIfInfo          IfInfo;

    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the give u4IfIndex. */
    if (VlanL2IwfGetPortChannelForPort ((UINT2) u4IfIndex, &u2AggId) ==
        L2IWF_SUCCESS)
    {
        u4IfIndex = u2AggId;
    }
    /* Input port can be PIP which vlan does not know.
       So for checking whether this port is mapped to that context,
       Vcm api is called. */
    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex,
                                          &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }
    if (VlanCfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return VLAN_FAILURE;
    }
    VLAN_LOCK ();
    /* switch context */
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /*In PBB Bridge Mode, get the TagLen only if the port is a PBB Port */
    if ((VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE) && ((IfInfo.u1BrgPortType ==
                                                      CFA_PROVIDER_INSTANCE_PORT)
                                                     || (IfInfo.u1BrgPortType ==
                                                         CFA_VIRTUAL_INSTANCE_PORT)
                                                     || (IfInfo.u1BrgPortType ==
                                                         CFA_CUSTOMER_BACKBONE_PORT)
                                                     || (IfInfo.u1BrgPortType ==
                                                         CFA_PROVIDER_NETWORK_PORT)))
    {
        i4RetVal = VlanPbbGetTagLenInFrame (pFrame, u4ContextId,
                                            u2LocalPort,
                                            IfInfo.u1BrgPortType,
                                            pu4VlanOffset);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return i4RetVal;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanGetTagLenInFrameForLocalPort (pFrame, u2LocalPort, pu4VlanOffset);

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanTagOutFrameForIvr                            */
/*                                                                           */
/*    Description         : This function adds VLAN tag in the outgoing      */
/*                          frame for the packets sent out of IP             */
/*                                                                           */
/*    Input(s)            : pBuf       - Incoming frame                      */
/*                          u2Port     - Port number                         */
/*                          VlanId     - VLAN ID                             */
/*                          u1Priority - Priority to be put in the out tag   */
/*                                                                           */
/*    Output(s)           : pBuf - with tag added                            */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanTagOutFrameForIvr (UINT1 *pBuf, UINT4 u4IfIndex,
                       tVlanId VlanId, UINT1 u1Priority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanTag            VlanTag;
    UINT4               u4ContextId;
    INT4                i4RetVal;
    tVlanId             LocalVid = VLAN_NULL_VLAN_ID;
    UINT2               u2LocalPort;
    UINT2               u2Protocol;
    UINT2               u2Tag = 0;
    UINT1               u1Pcp = 0;

    MEMSET (&VlanTag, 0, sizeof (tVlanTag));

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /* switch context */
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanTag.OuterVlanTag.u2VlanId = VlanId;
    VlanTag.OuterVlanTag.u1Priority = u1Priority;
    VlanTag.OuterVlanTag.u1DropEligible = VLAN_DE_FALSE;

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        /* Get the Pcp and the local vid. */
        i4RetVal = VlanGetPcpAndLocalVid (VlanTag, pVlanPortEntry, &LocalVid,
                                          &u1Pcp);

        if (i4RetVal == VLAN_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }

        VlanTag.OuterVlanTag.u2VlanId = LocalVid;
        VlanTag.OuterVlanTag.u1Priority = u1Pcp;
    }
    VlanFormTag (VlanTag, pVlanPortEntry, &u2Tag);
    u2Protocol = OSIX_HTONS (pVlanPortEntry->u2EgressEtherType);

    MEMCPY (&pBuf[0], (UINT1 *) &u2Protocol, VLAN_TYPE_OR_LEN_SIZE);
    MEMCPY (&pBuf[VLAN_TYPE_OR_LEN_SIZE], (UINT1 *) &u2Tag, VLAN_TAG_SIZE);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanTagFrame                                     */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanTagFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT1 u1Priority)
{
    UINT1               au1Buf[VLAN_TAGGED_HEADER_SIZE];
    UINT2               u2Tag = 0;
    UINT2               u2Protocol;

    u2Protocol = OSIX_HTONS (VLAN_PROTOCOL_ID);

    /* copying source and dest mac address */
    VLAN_COPY_FROM_BUF (pFrame, au1Buf, 0, VLAN_TAG_OFFSET);

    u2Tag = (UINT2) u1Priority;

    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);

    u2Tag = (UINT2) (u2Tag | (VlanId & VLAN_ID_MASK));

    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));

    MEMCPY (&au1Buf[VLAN_TAG_OFFSET], (UINT1 *) &u2Protocol,
            VLAN_TYPE_OR_LEN_SIZE);

    MEMCPY (&au1Buf[VLAN_TAG_VLANID_OFFSET], (UINT1 *) &u2Tag, VLAN_TAG_SIZE);

    VLAN_MOVE_OFFSET (pFrame, VLAN_TAG_OFFSET);

    VLAN_PREPEND_BUF (pFrame, au1Buf, VLAN_TAGGED_HEADER_SIZE);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanNpRetainTag                                  */
/*                                                                           */
/*    Description         : This function returns whether VLAN tag in a      */
/*                          particular frame can be retainted or removed.    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanNpRetainTag (UINT4 u4IfIndex, tMacAddr DstAddr, UINT2 u2Protocol,
                 INT4 *pi4RetVal)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               au1IsisL1Addr[6] =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x14 };
    UINT1               au1IsisL2Addr[6] =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x15 };
    UINT1               au1GvrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x21 };
    UINT1               au1StpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    UINT1               au1LacpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x02 };
#ifdef PVRST_WANTED
    UINT1               au1PvrstDot1qAddr[6] =
        { 0x01, 0x00, 0x0c, 0xcc, 0xcc, 0xcd };
#endif
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2AggId = 0;

    *pi4RetVal = VLAN_FALSE;

    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the give u4IfIndex. */
    if (VlanL2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4IfIndex = u2AggId;
    }

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                       &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /* switch context */
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if ((MEMCMP (DstAddr, au1GvrpAddr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (DstAddr, au1StpAddr, CFA_ENET_ADDR_LEN) == 0))
    {
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {
            /* 
             * S-tag received on PNPs and CNP S-tagged interfaces must be 
             * retained in case of 802.1ad Bridges for customer GVRP and 
             * STP packets for tunneling.
             */
            if (pVlanPbPortEntry != NULL)
            {
                if ((pVlanPbPortEntry->u1PbPortType ==
                     VLAN_PROVIDER_NETWORK_PORT)
                    || (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT))
                {
                    *pi4RetVal = VLAN_TRUE;
                }
            }
        }
    }

    if ((MEMCMP (DstAddr, au1LacpAddr, CFA_ENET_ADDR_LEN) == 0) &&
        (u2Protocol == LA_SLOW_PROT_TYPE))
    {
        *pi4RetVal = VLAN_TRUE;
    }

    if ((u2Protocol == PNAC_PAE_ENET_TYPE) ||
        (u2Protocol == PNAC_PAE_RSN_PREAUTH_ENET_TYPE))
    {
        if (VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE)
        {
            *pi4RetVal = VLAN_TRUE;
        }
    }
    if ((MEMCMP (DstAddr, au1IsisL1Addr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (DstAddr, au1IsisL2Addr, CFA_ENET_ADDR_LEN) == 0))
    {
        *pi4RetVal = VLAN_TRUE;
    }

#ifdef ECFM_WANTED
    if (u2Protocol == ECFM_PDU_TYPE_CFM)
    {
        *pi4RetVal = VLAN_TRUE;
    }
#endif

#ifdef PVRST_WANTED
    if (AstIsPvrstStartedInContext (u4ContextId))
    {
        if (MEMCMP (DstAddr, au1PvrstDot1qAddr, 6) == 0)
        {
            *pi4RetVal = VLAN_TRUE;
        }
    }
#endif

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUnTagGvrpPacket                              */
/*                                                                           */
/*    Description         : This function removes the Vlan Tag from the      */
/*                          incoming  gvrp packet.Gvrp packets are generally */
/*                          untagged but when the switch is gvrp unaware,the */
/*                          gvrp frames will be propagated as L2 data packets*/
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          u2Port - Port on which the gvrp frame was        */
/*                          received                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : VLAN_SUCCESS                                 */
/*                              VLAN_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUnTagGvrpPacket (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4Port)
{
#ifndef NPAPI_WANTED
    tVlanTag            VlanTag;
    tMacAddr            SrcAddr;
    UINT2               u2LocalPort = 0;
    UINT4               u4ContextId = 0;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4Port, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_COPY_FROM_BUF (pFrame, &SrcAddr, ETHERNET_ADDR_SIZE,
                        ETHERNET_ADDR_SIZE);

    MEMSET (&VlanTag, 0, sizeof (tVlanTag));

    /*This function is called to check if the vlan is tagged */
    VlanGetVlanId (pFrame, SrcAddr, u2LocalPort, &VlanTag);

    if (VlanTag.OuterVlanTag.u1TagType != VLAN_UNTAGGED)
    {
        VlanUnTagAllInFrame (pFrame, u2LocalPort);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
#else
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4Port);
    return VLAN_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMvrpFrameTagged                            */
/*                                                                           */
/*    Description         : This function checks whether the given the MVRP  */
/*                          frame has any Vlan Tag or Not.                   */
/*                          MVRP packet should not contain any VLAN tag. If  */
/*                          it is present in that case the frame needs to    */
/*                          be discarded by the caller.                      */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          u4Port - Port on which the mvrp frame was        */
/*                          received                                         */
/*                          *pi4FrameType - VLAN_TAGGED/VLAN_UNTAGGED        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns                 : VLAN_SUCCESS                                 */
/*                              VLAN_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMvrpFrameTagged (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4Port,
                       INT4 *pi4FrameType)
{
    UINT2               u2LocalPort = 0;
    UINT4               u4ContextId = 0;
    UINT1               au1Buf[VLAN_TAGGED_PROTOCOL_HEADER_OFFSET];
    UINT1              *pu1Data = NULL;
    UINT2               u2EtherType = 0;

    MEMSET (au1Buf, 0, VLAN_TAGGED_PROTOCOL_HEADER_OFFSET);

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4Port, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_COPY_FROM_BUF (pFrame, au1Buf, 0, VLAN_TAGGED_PROTOCOL_HEADER_OFFSET);
    pu1Data = au1Buf;

    MEMCPY ((UINT1 *) &u2EtherType, &pu1Data[VLAN_TAG_OFFSET],
            VLAN_TYPE_OR_LEN_SIZE);

    u2EtherType = OSIX_NTOHS (u2EtherType);

    *pi4FrameType = VlanClassifyFrame (pFrame, u2EtherType, u2LocalPort);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetTunnelProtocolMac                         */
/*                                                                           */
/*    Description         : This function is used to get the MAC address of  */
/*                          the protocols which can be tunneled through VLAN.*/
/*                          This function is called from the lower layer on  */
/*                          reception of a GMRP comtrol frame to decide if   */
/*                          the tag in the frame should be retained.         */
/*                                                                           */
/*    Input(s)            : u1ContextId      - Context Identifier            */
/*                                                                           */
/*                          u1ProtocolId     - Protocol Identifier           */
/*                                                                           */
/*    Output(s)           : MacAddr      - Tunnel Mac Address                */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS                                     */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetTunnelProtocolMac (UINT4 u4ContextId, UINT1 u1ProtocolId,
                          tMacAddr MacAddr)
{
    if (u4ContextId == VLAN_INVALID_CONTEXT)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VlanTunnelProtocolMac (u1ProtocolId, MacAddr);

        VlanReleaseContext ();

        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }

    VLAN_UNLOCK ();
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCreateDefaultVlan                            */
/*                                                                           */
/*    Description         : This function is used to create the default VLAN */
/*                          for the switch. Since the default vlan creation  */
/*                          is blocked in vlansys.c this API is called to    */
/*                          create the default VLAN.                         */
/*                                                                           */
/*    Input(s)            : pi1Param - Parameter should be NULL when called  */
/*                                     from lrmain.c                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanCreateDefaultVlan (INT1 *pi1Param)
{
    INT4                i4ResType = 0;

    /* Get Restoration Type - whether CSR or CSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    /* This function is called from lrmain.c with pi1Param as non-NULL.
     * In other places, pi1Param should be NULL value */

    if (pi1Param != NULL)
    {
        /* Called from lrmain.c */

        VLAN_INIT_COMPLETE (OSIX_SUCCESS);

        /* If restore Flag is set we are not suppose to create default vlan.
         * Since it will get created in the restoration thread itself. */

        if (((VlanIssGetRestoreFlagFromNvRam ()) == VLAN_SET_RESTORE_FLAG) &&
            (i4ResType != SET_FLAG))
        {
            return;
        }
    }

    VlanSetDefaultVlan ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteDefaultVlan                            */
/*                                                                           */
/*    Description         : This function is used to delete the default VLAN */
/*                          for the switch.                                  */
/*                                                                           */
/*    Input(s)            : pi1Param         - Parameter                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteDefaultVlan (VOID)
{

    tStaticVlanEntry   *pStVlanEntry = NULL;

    VLAN_LOCK ();
    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }
    pStVlanEntry = VlanGetStaticVlanEntry (VLAN_DEF_VLAN_ID);
    if (pStVlanEntry != NULL)
    {
        if (VlanDelStVlanInfo (pStVlanEntry) != VLAN_SUCCESS)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return;
        }
        /* To make the system to initialisation state we are initialising 
         * this variable to 0 because this variable is incremented 
         * in the above function VlanDelStVlanInfo. */
        gpVlanContextInfo->VlanInfo.u4NumDelVlans = 0;
        VlanDeleteStVlanEntry (pStVlanEntry);
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanValidatePortType                             */
/*                                                                           */
/*    Description         : This function validates whether given bridge     */
/*                          port type can be set for the given port or not.  */
/*                                                                           */
/*    Input(s)            : u4IfIndex - IfIndex of the port                  */
/*                                                                           */
/*                          u1BrgPortType - Bridge port type to be validated */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanValidatePortType (UINT4 u4IfIndex, UINT1 u1BrgPortType)
{
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = VLAN_TRUE;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();

    /* If the port is not mapped to any context, then don't
     * allow setting of brg port type. */
    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    i4RetVal = VlanPbValidatePortType (u2LocalPort, u1BrgPortType);

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnAddL2VpnInfo                            */
/*                                                                           */
/*    Description         : This function is called by MPLS Module to create */
/*                          a L2Vpn Map Entry in the VLAN Module             */
/*                                                                           */
/*    Input(s)            : pVplsInfo - Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnAddL2VpnInfo (tVplsInfo * pVplsInfo)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (pVplsInfo->u4ContextId)
        == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         VLAN_NAME,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "L2VPN Add Map Entry message  \r\n");

        return VLAN_FAILURE;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME, " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_QMSG_BUFF);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->L2VpnData.u4ContextId = pVplsInfo->u4ContextId;
    pVlanQMsg->L2VpnData.u4IfIndex = pVplsInfo->u4IfIndex;
    pVlanQMsg->L2VpnData.u4VplsIndex = pVplsInfo->u4VplsIndex;
    pVlanQMsg->L2VpnData.VlanId = pVplsInfo->VlanId;
    pVlanQMsg->L2VpnData.u2MplsServiceType = pVplsInfo->u2MplsServiceType;

#ifdef MPLS_WANTED
    pVlanQMsg->L2VpnData.u1PwVcMode = pVplsInfo->u1PwVcMode;
#endif

    pVlanQMsg->u2MsgType = VLAN_ADD_L2VPN_INFO;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME,
                         " Send To Q failed for Vlan Add l2VpnInfo message "
                         "Type \r\n");

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             VLAN_NAME,
                             " Failed to get the task id for "
                             "Vlan Task %u \r\n");
            return VLAN_FAILURE;
            /* No Need to Free Message Sice it is already Queued */
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnDelL2VpnInfo                            */
/*                                                                           */
/*    Description         : This function is called by MPLS Module to delete */
/*                          a L2Vpn Map Entry in the VLAN Module             */
/*                                                                           */
/*    Input(s)            : pVplsInfo - Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2VpnDelL2VpnInfo (tVplsInfo * pVplsInfo)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (pVplsInfo->u4ContextId)
        == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         VLAN_NAME,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "L2VPN Delete Map Entry message  \r\n");

        return VLAN_FAILURE;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME, " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", VLAN_QMSG_BUFF);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->L2VpnData.u4ContextId = pVplsInfo->u4ContextId;
    pVlanQMsg->L2VpnData.u4IfIndex = pVplsInfo->u4IfIndex;
    pVlanQMsg->L2VpnData.u4VplsIndex = pVplsInfo->u4VplsIndex;
    pVlanQMsg->L2VpnData.VlanId = pVplsInfo->VlanId;
    pVlanQMsg->L2VpnData.u2MplsServiceType = pVplsInfo->u2MplsServiceType;

#ifdef MPLS_WANTED
    pVlanQMsg->L2VpnData.u1PwVcMode = pVplsInfo->u1PwVcMode;
#endif

    pVlanQMsg->u2MsgType = VLAN_DEL_L2VPN_INFO;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME,
                         " Send To Q failed for Vlan Add l2VpnInfo message "
                         "Type \r\n");

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (pVplsInfo->u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnFwdMplsPktOnPorts                       */
/*                                                                           */
/*    Description         : This function is called by MPLS Module to post a */
/*                          Packet received on a MPLS Interface to be fwd to */
/*                          Ethernet Ports                                   */
/*                                                                           */
/*    Input(s)            : pBuf - Packet received on MPLS Interface         */
/*                          u4ContextId - Context in which the Packet should */
/*                          be processed.                                    */
/*                          VlanId - VlanId for which the packet is          */
/*                          classified                                       */
/*                          u1Priority - priority of the pkt recvd from MPLS */
/*                          u1PwMode - Raw / Tagged Mode                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnFwdMplsPktOnPorts (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                            tVlanId VlanId, UINT1 u1Priority, UINT1 u1PwMode,
                            UINT4 u4OutPort)
{
    tVlanIfMsg         *pVlanIfMsg;
    tVlanQInPkt        *pVlanQInPkt = NULL;
    UINT4               u4BridgeMode = 0;
    UINT4               u4RetVal = 0;
    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        return VLAN_SUCCESS;
    }
    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is not enabled. Dropping the incoming "
                         "packet received from MPLS on Vlan %d.\r\n", VlanId);

        return VLAN_FAILURE;
    }

    pVlanQInPkt = (tVlanQInPkt *) (VOID *) VLAN_GET_BUF (VLAN_Q_IN_PKT_BUFF,
                                                         sizeof (tVlanQInPkt));
    if (NULL == pVlanQInPkt)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Queue - Incoming Packet from MPLS\r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&(pVlanQInPkt->VlanIfMsg), VLAN_INIT_VAL, sizeof (tVlanIfMsg));
    pVlanIfMsg = &(pVlanQInPkt->VlanIfMsg);
#ifdef HVPLS_WANTED
    VLAN_COPY_FROM_BUF (pBuf, &(pVlanIfMsg->DestAddr), sizeof (UINT4),
                        ETHERNET_ADDR_SIZE);
#else
    VLAN_COPY_FROM_BUF (pBuf, &(pVlanIfMsg->DestAddr), 0, ETHERNET_ADDR_SIZE);
#endif
    VLAN_COPY_FROM_BUF (pBuf, &(pVlanIfMsg->SrcAddr),
                        ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);

    pVlanIfMsg->u4ContextId = u4ContextId;
    pVlanIfMsg->u2Length = (UINT2) VLAN_GET_BUF_LEN (pBuf);
    pVlanIfMsg->SVlanId = VlanId;
    pVlanIfMsg->u1Priority = u1Priority;
    pVlanIfMsg->u1MplsMode = u1PwMode;
    pVlanIfMsg->u2LocalPort = (UINT2) u4OutPort;

    if (VlanL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode) != L2IWF_SUCCESS)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);
        return VLAN_FAILURE;
    }

    if (pVlanIfMsg->u1MplsMode == VLAN_L2VPN_RAW_MODE)
    {
        if ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
        {
            /* In case of PB, Packet can contain customer tags, so
             * in raw mode, the pkt length should not be greater
             * than 1518*/
            if (pVlanIfMsg->u2Length > VLAN_SINGLE_TAGGED_FRAME_LENGTH)
            {
                VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
                VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                                 "FrameLength %d Dropping the "
                                 "incoming packet received from MPLS on "
                                 "Vlan%d.\r\n", pVlanIfMsg->u2Length, VlanId);

                return VLAN_FAILURE;

            }
        }
        else
        {
            /* In case of Customer, in raw mode, the pkt length should 
             * not be greater than 1514*/
            if (pVlanIfMsg->u2Length > VLAN_UNTAGGED_FRAME_MAXIMUM_LENGTH)
            {
                VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
                VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                                 "FrameLength %d Dropping the "
                                 "incoming packet received from MPLS on "
                                 "Vlan%d.\r\n", pVlanIfMsg->u2Length, VlanId);

                return VLAN_FAILURE;

            }
        }
    }
    else if (pVlanIfMsg->u1MplsMode == VLAN_L2VPN_TAGGED_MODE)
    {
        if ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
        {
            /* In case of PB, Packet can be double tagged, so
             * in tagged mode, the pkt length should not be greater
             * than 1522*/
            if (pVlanIfMsg->u2Length > VLAN_FRAME_MAXIMUM_LENGTH)
            {
                VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
                VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                                 "FrameLength %d Dropping the "
                                 "incoming packet received from MPLS on "
                                 "Vlan%d.\r\n", pVlanIfMsg->u2Length, VlanId);

                return VLAN_FAILURE;

            }
        }
        else
        {
            /* In case of PB, Packet can contain only single tag, so
             * in tagged mode, the pkt length should not be greater
             * than 1518*/
            if (pVlanIfMsg->u2Length > VLAN_SINGLE_TAGGED_FRAME_LENGTH)
            {
                VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
                VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                                 "FrameLength %d Dropping the "
                                 "incoming packet received from MPLS on "
                                 "Vlan%d.\r\n", pVlanIfMsg->u2Length, VlanId);

                return VLAN_FAILURE;

            }
        }
    }

    VLAN_GET_TIMESTAMP (&(pVlanIfMsg->u4TimeStamp));
    pVlanQInPkt->pPktInQ = pBuf;

    u4RetVal
        =
        VLAN_SEND_TO_QUEUE (VLAN_TASK_QUEUE_ID, (UINT1 *) &pVlanQInPkt,
                            OSIX_DEF_MSG_LEN);

    if (u4RetVal == OSIX_SUCCESS)
    {

        if (VLAN_TASK_ID == VLAN_INIT_VAL)
        {
            if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
                OSIX_SUCCESS)
            {
                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                                 (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                 " Failed to get Vlan Task Id for "
                                 "sending the event \r\n");
                /* No Need to Free Message Sice it is already Queued */
                return VLAN_FAILURE;
            }
        }

        VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_MSG_ENQ_EVENT);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, DATA_PATH_TRC,
                         "Frame received from MPLS on Vlan %d is enqueued to "
                         "VLAN message Queue.\n", VlanId);

        return VLAN_SUCCESS;
    }

    VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
    VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);

    VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                     "Frame received from MPLS on Vlan %d is NOT enqueued to "
                     "VLAN message Queue.\n", VlanId);

    return VLAN_FAILURE;
}

#ifdef MPLS_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : VlanVplsLearn                                              */
/*                                                                           */
/* Description  : Learns the VC-Mac-Vlan association from Vlan MacMap table  */
/*                Here physical port number is ZERO since recv port can be a */
/*                L3 port too and PW is operating on it.                     */
/*                                                                           */
/* Input        : MacAddr - mac address to be learnt                         */
/*                ValnId - source vlan id                                    */
/*                u4PwVcIndex - VC id on which packet was received           */
/*                u4ContextId - Virtual switch instance number               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
VlanVplsLearn (UINT4 u4ContextId, tMacAddr MacAddr, tVlanId VlanId,
               UINT4 u4PwVcIndex)
{
#ifndef SW_LEARNING
    UINT4               u4FidIndex;
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanCurrEntry     *pVlanRec = NULL;
#ifdef HVPLS_WANTED
    UINT4               u4VplsFDBHighThreshold = 0;
    UINT4               u4VplsFDBCounter = 0;
    UINT4               u4VpnId = 0;
#endif
    /* No learning if the source address is multicast mac */
    if (VLAN_IS_MCASTADDR (MacAddr) == VLAN_TRUE)
    {
        return;
    }

    VLAN_LOCK ();
    /* Switch to the context first and do your stuff */
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }

    pVlanRec = VlanGetVlanEntry (VlanId);
    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "-E- VLAN 0x%x associated with the data packet "
                       "is unknown \n", VlanId);

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (VlanId) == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    u4FidIndex = pVlanRec->u4FidIndex;

    pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

    if (pFidEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    if (pFidEntry->u4DynamicUnicastCount >=
        VLAN_CONTROL_MAC_LEARNING_LIMIT (VlanId))
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    pFdbEntry = VlanGetUcastEntry (u4FidIndex, MacAddr);
    if (pFdbEntry != NULL && pFdbEntry->u2Port != 0)
    {
        /* learnt from PHY port so, Delete it */
        VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
    }
    else if (pFdbEntry != NULL)
    {
        /* Entry already presents so update with the new details */
        VLAN_GET_TIMESTAMP (&pFdbEntry->u4TimeStamp);
        pFdbEntry->VlanId = pVlanRec->VlanId;
        pFdbEntry->u4PwVcIndex = u4PwVcIndex;
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "Fdb Entry Updated\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    pFdbEntry = (tVlanFdbEntry *) (VOID *) VLAN_GET_BUF (VLAN_FDB_ENTRY,
                                                         sizeof
                                                         (tVlanFdbEntry));
    if (pFdbEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "-E- No Free FDB Entry \n");

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    /* Now got the green signal to add the new Fdb entry */
    MEMSET (pFdbEntry, 0, sizeof (tVlanFdbEntry));
    MEMCPY (pFdbEntry->MacAddr, MacAddr, MAC_ADDR_LEN);
    VLAN_GET_TIMESTAMP (&pFdbEntry->u4TimeStamp);
    pFdbEntry->VlanId = pVlanRec->VlanId;
    pFdbEntry->u4PwVcIndex = u4PwVcIndex;
    pFdbEntry->u1Status = VLAN_FDB_LEARNT;
#ifdef HVPLS_WANTED
    u4VplsFDBCounter = L2VpnVlanIncFDBCounter (pFdbEntry->u4PwVcIndex);
    L2VpnVlanGetFDBHighThreshold (pFdbEntry->u4PwVcIndex,
                                  &u4VplsFDBHighThreshold);
    L2VpnVlanGetVpnIdFromPwIndex (pFdbEntry->u4PwVcIndex, &u4VpnId);
    if (u4VpnId == 0)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }
    if (u4VplsFDBHighThreshold != 0)
    {
        if (u4VplsFDBCounter > u4VplsFDBHighThreshold)
        {
            if (L2VpnSendVplsFDBAlarmRaised (u4VpnId) == L2VPN_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                          "Sending Trap message failed\n");
            }
        }
    }
#endif
    VlanAddFdbEntry (u4FidIndex, pFdbEntry);
    VlanUpdateVlanMacCount (pVlanRec, VLAN_ADD);

    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "VPLS FDB Entry added successfully\n");

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
#else /* Software learning - will triggered by the hw */
    /* This part to be tested when BCM supports vpls learning */
    tVlanFdbInfo       *pVlanFdbInfo;
    tVlanFdbInfo        VlanFdbTable;
    UINT4               u4FdbId = VLAN_ZERO;

    VLAN_LOCK ();

    VLAN_CPY_MAC_ADDR (VlanFdbTable.MacAddr, MacAddr);
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }
    if (VlanGetFdbIdFromVlanId (VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }
    VlanFdbTable.u4FdbId = u4FdbId;

    pVlanFdbInfo =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                   (tRBElem *) (&VlanFdbTable));

    if (pVlanFdbInfo == NULL)
    {
        /* new entry */
        pVlanFdbInfo = (tVlanFdbInfo *) (VOID *) VLAN_GET_BUF (VLAN_FDB_ENTRY,
                                                               sizeof
                                                               (tVlanFdbInfo));
        if (pVlanFdbInfo == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "No Free FDB Entry \n");
            VlanReleaseContext ();

            VLAN_UNLOCK ();
            return;
        }

        MEMSET (pVlanFdbInfo, 0, sizeof (tVlanFdbInfo));
        VLAN_CPY_MAC_ADDR (pVlanFdbInfo->MacAddr, MacAddr);
        pVlanFdbInfo->u4PwVcIndex = u4PwVcIndex;
        pVlanFdbInfo->u1EntryType = VLAN_FDB_LEARNT;
        pVlanFdbInfo->u4FdbId = u4FdbId;
        VLAN_SLL_INIT_NODE (&pVlanFdbInfo->NextNode);

        if (RBTreeAdd
            (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
             (tRBElem *) pVlanFdbInfo) == RB_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_RELEASE_BUF (VLAN_FDB_ENTRY, pVlanFdbInfo);
            VLAN_UNLOCK ();
            return;
        }
    }
    else                        /* source movement */
    {
        pVlanFdbInfo->u2Port = 0;
        pVlanFdbInfo->u4PwVcIndex = u4PwVcIndex;
        pVlanFdbInfo->u1EntryType = VLAN_FDB_LEARNT;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
#endif
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAllFdbEntryRemove                            */
/*                                                                           */
/*    Description         : This function is an API for                      */
/*                          deleting the entire FDB database (RB Tree)       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual switch context for which   */
/*                                        the FDB table is to be deleted.    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanAllFdbEntryRemove (UINT4 u4ContextId)
{
    tVlanQMsg           VlanQMsg;
    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_ALL_DELETE_MSG);

        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_ALL_DELETE_MSG);

        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&VlanQMsg, 0, sizeof (tVlanQMsg));

    VlanQMsg.u2MsgType = VLAN_FDBENTRY_ALL_DELETE_MSG;
    VlanQMsg.u4ContextId = u4ContextId;
    VLAN_LOCK ();
    VlanProcessCfgEvent (&VlanQMsg);
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFdbEntryRemove                               */
/*                                                                           */
/*    Description         : This function is an API for                      */
/*                          deleting an FDB entry from database              */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID of the entry                    */
/*                          MacAddress - MAC address of the entry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFdbEntryRemove (UINT4 u4ContextId, tVlanId VlanId, tMacAddr pMacAddress)
{

    tVlanQMsg           VlanQMsg;
    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);

        return VLAN_FAILURE;

    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);

        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&VlanQMsg, 0, sizeof (tVlanQMsg));

    VlanQMsg.u2MsgType = VLAN_FDBENTRY_DELETE_MSG;
    VlanQMsg.u4ContextId = u4ContextId;
    VlanQMsg.FdbData.VlanId = VlanId;
    MEMCPY (VlanQMsg.FdbData.au1MacAddr, pMacAddress, CFA_ENET_ADDR_LEN);
    VLAN_LOCK ();
    VlanProcessCfgEvent (&VlanQMsg);
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanRemoveFdbEntry                               */
/*                                                                           */
/*    Description         : This function is an API to post a message for    */
/*                          deleting an FDB entry from database              */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID of the entry                    */
/*                          MacAddress - MAC address of the entry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRemoveFdbEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr pMacAddress)
{

    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);

        return VLAN_FAILURE;

    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);

        return VLAN_FAILURE;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FDBENTRY_DELETE_MSG;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->FdbData.VlanId = VlanId;
    MEMCPY (pVlanQMsg->FdbData.au1MacAddr, pMacAddress, CFA_ENET_ADDR_LEN);

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_FDBENTRY_DELETE_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Since it is already Queued */
            return VLAN_FAILURE;
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanRemoveAgedFdbEntry                           */
/*                                                                           */
/*    Description         : This function is an API to post a message for    */
/*                          deleting an FDB entry from database              */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID of the entry                    */
/*                          MacAddress - MAC address of the entry        */
/*                          u4IfIndex - Interface Index              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRemoveAgedFdbEntry (UINT4 u4ContextId, tVlanId VlanId,
                        tMacAddr pMacAddress, UINT4 u4IfIndex)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT2               u2LocalPort = 0;
    UINT1               u1IsMemberPort = VLAN_FALSE;
    UINT1               u1OperStatus = CFA_IF_DOWN;
#ifdef ICCH_WANTED
    tMcagQMsg          *pMcagQMsg = NULL;
    tLocalPortList      AllowedToGoPort;
    tMacAddr            ConnectionId;
    UINT4               u4PeerNodeState = 0;
    UINT4               u4IcclIndex = 0;
    UINT1               u1McLagStatus = 0;
    UINT1               u1IsMclagEnabled = VLAN_FALSE;
    UINT1               u1IsIcclInterface = 0;
    UINT1               u1FdbSyncStatus = OSIX_FAILURE;
    UINT1               au1Str[VLAN_CLI_MAX_MAC_STRING_SIZE];

    MEMSET (AllowedToGoPort, 0, sizeof (tLocalPortList));
    MEMSET (ConnectionId, 0, sizeof (tMacAddr));
    MEMSET (au1Str, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);

    u4PeerNodeState = VlanIcchGetPeerNodeState ();
    u1McLagStatus = VlanLaGetMCLAGSystemStatus ();
#endif

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);

        return VLAN_FAILURE;

    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_DELETE_MSG);

        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VCM_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /* ICCH: Significance of doing the below validation : In case of port shutdown,
     * Vlan membership removal, there is no need to post Fdb removal to MCAG */
    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "Flushing Fdb - invalid port \r\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_IS_MEMBER_PORT (pVlanEntry->EgressPorts, u2LocalPort, u1IsMemberPort);

    if (u1IsMemberPort == VLAN_FALSE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "Flushing Fdb - Port is not a member of vlan \r\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;

    }

    /* When a port is oper-down dont perform ageing */
    VlanCfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    if (CFA_IF_DOWN == u1OperStatus)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;

    }

#ifdef ICCH_WANTED
    /* Checking whether MC-LAG is enabled in the interface */
    u1IsMclagEnabled = (UINT1) VlanLaIsMclagInterface (u4IfIndex);

    VlanIcchGetIcclIfIndex (&u4IcclIndex);
    if (u4IcclIndex == u4IfIndex)
    {
        u1IsIcclInterface = OSIX_TRUE;
    }

    /* Get the FDB Sync State. If disabled, no need to post
     * FDB Aging indication to MCAG */
    u1FdbSyncStatus = (UINT1) IcchApiCheckProtoSyncEnabled ();

    /* MAC Aging Indication can be posted to MCAG task for the 
     * fdb entries learnt on ICCL interface and MC-LAG interface.
     * For Non ICCL interface and Non MC-LAG interface, uniform MAC
     * Aging is not required, so indication can be posted to 
     * vlan task */

    if ((u1McLagStatus == LA_MCLAG_ENABLED) && (u4PeerNodeState == ICCH_PEER_UP)
        && (u1FdbSyncStatus == OSIX_SUCCESS) &&
        ((u1IsMclagEnabled == VLAN_TRUE) || (u1IsIcclInterface == OSIX_TRUE)))
    {
        /* Re-Program the aged entry as if the entry is learned
         * dynamically. The entry has to be deleted after consulting
         * with the other MC-LAG Node */

        VLAN_SET_MEMBER_PORT (AllowedToGoPort, u2LocalPort);

        /* Notify MAC Aging Task to handle Aging Notification for the FDB entry */
        pMcagQMsg = (tMcagQMsg *) (VOID *)
            VLAN_GET_BUF (VLAN_MCAG_QMSG_BUFF, sizeof (tMcagQMsg));
        if (NULL == pMcagQMsg)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Message Allocation failed for MCAG "
                             "Config Message Type %u \r\n", VLAN_MCAG_FDBENTRY);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }
        VLAN_MEMSET (pMcagQMsg, 0, sizeof (tMcagQMsg));

        pMcagQMsg->u2MsgType = VLAN_MCAG_FDBENTRY;
        pMcagQMsg->FdbData.VlanId = VlanId;
        MEMCPY (pMcagQMsg->FdbData.au1MacAddr, pMacAddress, CFA_ENET_ADDR_LEN);

        if (VLAN_SEND_TO_QUEUE (MCAG_TASK_QUEUE_ID,
                                (UINT1 *) &pMcagQMsg,
                                OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Send To Q failed for  Vlan Config Message "
                             "Type %u \r\n", VLAN_FDBENTRY_DELETE_MSG);

            VLAN_RELEASE_BUF (VLAN_MCAG_QMSG_BUFF, pMcagQMsg);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }

        if (MCAG_TASK_ID == VLAN_INIT_VAL)
        {
            if (VLAN_GET_TASK_ID (VLAN_SELF, MCAG_TASK, &(MCAG_TASK_ID)) !=
                OSIX_SUCCESS)
            {
                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                                 (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                 " Failed to get Vlan Task Id for "
                                 "sending the event \r\n");
                /* No Need to Free Message Since it is already Queued */
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }
        }

        VLAN_SEND_EVENT (MCAG_TASK_ID, VLAN_MAC_AGING_EVENT);

        if (VlanHwAddStaticUcastEntry (VlanId, pMacAddress, VLAN_DEF_RECVPORT,
                                       AllowedToGoPort, VLAN_DELETE_ON_TIMEOUT,
                                       ConnectionId) == VLAN_FAILURE)
        {
            PrintMacAddress (pMacAddress, au1Str);
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "\n\n Failed to add static unicast entry"
                           " for MAC:%s of VlanId %d\r\n", au1Str, VlanId);
        }
    }
    else
#endif
    {
        pVlanQMsg = (tVlanQMsg *) (VOID *)
            VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

        if (NULL == pVlanQMsg)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Message Allocation failed for Vlan "
                             "Config Message Type %u \r\n",
                             VLAN_FDBENTRY_DELETE_MSG);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }

        VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

        pVlanQMsg->u2MsgType = VLAN_FDBENTRY_DELETE_MSG;
        pVlanQMsg->u4ContextId = u4ContextId;
        pVlanQMsg->FdbData.VlanId = VlanId;
        MEMCPY (pVlanQMsg->FdbData.au1MacAddr, pMacAddress, CFA_ENET_ADDR_LEN);

        if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                                (UINT1 *) &pVlanQMsg,
                                OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Send To Q failed for  Vlan Config Message "
                             "Type %u \r\n", VLAN_FDBENTRY_DELETE_MSG);

            VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }
        if (VLAN_TASK_ID == VLAN_INIT_VAL)
        {
            if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
                OSIX_SUCCESS)
            {
                VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                                 (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                 " Failed to get Vlan Task Id for "
                                 "sending the event \r\n");
                /* No Need to Free Message Since it is already Queued */
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }
        }

        VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

        UNUSED_PARAM (u2LocalPort);

    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanRemoveAllFdbEntry                            */
/*                                                                           */
/*    Description         : This function is an API to post a message for    */
/*                          deleting the entire FDB database (RB Tree)       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual switch context for which   */
/*                                        the FDB table is to be deleted.    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRemoveAllFdbEntry (UINT4 u4ContextId)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_ALL_DELETE_MSG);

        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) != VLAN_ENABLED)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "VLAN module is disabled. Cannot Post Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_ALL_DELETE_MSG);

        return VLAN_FAILURE;
    }

    pVlanQMsg = (tVlanQMsg *) (VOID *)
        VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FDBENTRY_ALL_DELETE_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FDBENTRY_ALL_DELETE_MSG;
    pVlanQMsg->u4ContextId = u4ContextId;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_FDBENTRY_ALL_DELETE_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {
        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Since it is already Queued */
            return VLAN_FAILURE;
        }
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanIncrRxDiscardCounters                                  */
/*                                                                           */
/* Description  : Increments the Discard counters maintained for L2          */
/*                protocols on the reception side.                           */
/*                                                                           */
/* Input        : u4IfIndex - Interface on which the counters are to be      */
/*                            incremented.                                   */
/*                u2ProtocolId - Protocol for which the counters are to      */
/*                               be incremented.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIncrRxDiscardCounters (UINT4 u4IfIndex, UINT2 u2ProtocolId)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                       &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    switch (u2ProtocolId)
    {
        case L2_PROTO_DOT1X:
            VLAN_INCR_DISCARD_DOT1X_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_LACP:
            VLAN_INCR_DISCARD_LACP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_STP:
            VLAN_INCR_DISCARD_STP_BPDUS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_GVRP:
            VLAN_INCR_DISCARD_GVRP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_GMRP:
            VLAN_INCR_DISCARD_GMRP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_MVRP:
            VLAN_INCR_DISCARD_MVRP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_MMRP:
            VLAN_INCR_DISCARD_MMRP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_IGMP:
            VLAN_INCR_DISCARD_IGMP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_ELMI:
            VLAN_INCR_DISCARD_ELMI_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_LLDP:
            VLAN_INCR_DISCARD_LLDP_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_ECFM:
            VLAN_INCR_DISCARD_ECFM_PKTS_RX (pVlanPortEntry);
            break;
        case L2_PROTO_EOAM:
            VLAN_INCR_DISCARD_EOAM_PKTS_RX (pVlanPortEntry);
            break;
        default:
            break;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanIncrRxDiscardCountersPerVlan                           */
/*                                                                           */
/* Description  : Increments the Discard counters maintained for L2          */
/*                protocols on the reception side.                           */
/*                                                                           */
/* Input        : u4ContextId - Context Id                     */
/*                VlanId      VlanId on which counters has to be incremented */
/*                u2ProtocolId - Protocol for which the counters are to      */
/*                               be incremented.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIncrRxDiscardCountersPerVlan (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2ProtocolId)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "\n\n Current VLAN entry is NULL. \n\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    switch (u2ProtocolId)
    {
        case L2_PROTO_GVRP:
            VLAN_INCR_DISCARD_GVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_GMRP:
            VLAN_INCR_DISCARD_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_MVRP:
            VLAN_INCR_DISCARD_MVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_MMRP:
            VLAN_INCR_DISCARD_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_IGMP:
            VLAN_INCR_DISCARD_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_ECFM:
            VLAN_INCR_DISCARD_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        default:
            break;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanIncrRxTunnelCountersPerVlan                           */
/*                                                                           */
/* Description  : Increments the tunnel counters maintained for L2          */
/*                protocols on the reception side.                           */
/*                                                                           */
/* Input        : u4ContextId - Context Id                     */
/*                VlanId      VlanId on which counters has to be incremented */
/*                u2ProtocolId - Protocol for which the counters are to      */
/*                               be incremented.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIncrRxTunnelCountersPerVlan (UINT4 u4ContextId, tVlanId VlanId,
                                 UINT2 u2ProtocolId)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "\n\n Current VLAN entry is NULL. \n\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    switch (u2ProtocolId)
    {
        case L2_PROTO_GVRP:
            VLAN_INCR_TUNNEL_GVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_GMRP:
            VLAN_INCR_TUNNEL_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_MVRP:
            VLAN_INCR_TUNNEL_MVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_MMRP:
            VLAN_INCR_TUNNEL_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_IGMP:
            VLAN_INCR_TUNNEL_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_ECFM:
            VLAN_INCR_TUNNEL_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry);
            break;
        default:
            break;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanIncrTxTunnelCountersPerVlan                           */
/*                                                                           */
/* Description  : Increments the tunnel counters maintained for L2          */
/*                protocols on the transmission side.                        */
/*                                                                           */
/* Input        : u4ContextId - Context Id                     */
/*                VlanId      VlanId on which counters has to be incremented */
/*                u2ProtocolId - Protocol for which the counters are to      */
/*                               be incremented.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIncrTxTunnelCountersPerVlan (UINT4 u4ContextId, tVlanId VlanId,
                                 UINT2 u2ProtocolId)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "\n\n Current VLAN entry is NULL. \n\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    switch (u2ProtocolId)
    {
        case L2_PROTO_GVRP:
            VLAN_INCR_TUNNEL_GVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_GMRP:
            VLAN_INCR_TUNNEL_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_MVRP:
            VLAN_INCR_TUNNEL_MVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_MMRP:
            VLAN_INCR_TUNNEL_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_IGMP:
            VLAN_INCR_TUNNEL_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
            break;
        case L2_PROTO_ECFM:
            VLAN_INCR_TUNNEL_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry);
            break;
        default:
            break;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/**************************************************************************/
/* Function Name       : VlanGetDefPortType                               */
/*                                                                        */
/* Description         : This function finds out the default port type to */
/*                       be kept for a port.                              */
/*                                                                        */
/* Input(s)            : u4IfIndex - The port in which the default        */
/*                                   port-type needs to be determined.    */
/*                                                                        */
/* Output(s)           : None.                                            */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : Bridge port type.                                */
/*                                                                        */
/**************************************************************************/
UINT1
VlanGetDefPortType (UINT4 u4IfIndex)
{
    UINT1               u1BridgePortType = VLAN_CUSTOMER_BRIDGE_PORT;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (VlanVcmGetSystemMode (VLANMOD_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VlanVcmGetContextInfoFromIfIndex
            (u4IfIndex, &u4ContextId, &u2LocalPort) == VCM_FAILURE)
        {
            u1BridgePortType = CFA_INVALID_BRIDGE_PORT;
            return u1BridgePortType;
        }
        return u1BridgePortType;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return u1BridgePortType;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        u1BridgePortType = VLAN_PROVIDER_NETWORK_PORT;
    }

    VlanReleaseContext ();

    VLAN_UNLOCK ();

    return u1BridgePortType;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanInfoFromFrame                         */
/*                                                                           */
/*    Description         : Get the data offset (offset after all vlan tags) */
/*                          Get the outer and inner vlan information         */
/*                          Apply ingress filtering rule and convey that     */
/*                          through pu1IngressAction out parameter.          */
/*                          In case of 1ah bridge, it even scans for S-Vlan  */
/*                          data offset (After B/I/S). This function is not  */
/*                          called for PIP                                   */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                                                                           */
/*    Output(s)           : *pVlanTagInfo- VlanId, Priority, DE for recvd    */
/*                           frame                                           */
/*                          *pu1IngressAction - VLAN_TRUE (If Port is not    */
/*                           the member port of VLAN and Ingress filtering is*/
/*                           enabled)                                        */
/*                           VLAN_FALSE (If port is member of VLAN)          */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetVlanInfoFromFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
                          tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                          UINT1 *pu1IngressAction, UINT4 *pu4TagOffSet)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tMacAddr            SrcAddr;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;

    MEMSET (SrcAddr, 0, sizeof (tMacAddr));

    *pu1IngressAction = VLAN_FALSE;
    u1Result = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    /* vlan classification should happen for all other packets */
    VLAN_COPY_FROM_BUF (pBuf, &SrcAddr, ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);
    VlanId = VlanGetVlanId (pBuf, SrcAddr, u2LocalPort, pVlanTag);

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        if (pVlanTag->OuterVlanTag.u1TagType == VLAN_UNTAGGED)
        {

            /* Return Pkt Tag Offset to the CFM module */
            VlanGetTagLenInFrameForLocalPort (pBuf, u2LocalPort, pu4TagOffSet);
            pVlanTag->OuterVlanTag.u2VlanId = 0;
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_SUCCESS;
        }
        else
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                           "VLAN ID 0x%x associated with the Frame "
                           "is unknown \n", VlanId);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }
    }

    VLAN_IS_CURR_EGRESS_PORT (pVlanEntry, u2LocalPort, u1Result);

    if ((u1Result == VLAN_FALSE) && (pPortEntry->u1IngFiltering ==
                                     VLAN_SNMP_TRUE))
    {
        /* If the port on which the pkt is recvd is not a member port
         * of the vlan and the ingress filtering is enabled , set 
         * IngressAction as true */
        *pu1IngressAction = VLAN_TRUE;
    }
    /* Return Pkt Tag Offset to the CFM module */
    VlanGetTagLenInFrameForLocalPort (pBuf, u2LocalPort, pu4TagOffSet);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessPktForCep                             */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          apply the PB rules for a CFM-PDU received        */
/*                          on a CEP.                                        */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u2LocalPort- Port on which CFM-PDU was received. */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : pBuf     - CRU Buffer recieved.                  */
/*                          pVlanTag - Classified Vlan Information.          */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanProcessPktForCep (UINT4 u4ContextId, UINT2 u2LocalPort,
                      tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPort);

    if (pVlanPbPortEntry != NULL)
    {
        if ((VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
            (pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT))
        {
            /* If the cfm packet is recvd on a CEP */
            if (VlanPbHandlePktRxOnCep (pBuf, u2LocalPort, pVlanTag) ==
                VLAN_FAILURE)
            {
                pVlanTag->OuterVlanTag.u2VlanId = 0;
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMemberPortForCfm                           */
/*                                                                           */
/*    Description         : This function called by ECFM module to verify    */
/*                          the given port is member of the given vlan       */
/*                                                                           */
/*    Input(s)            : VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMemberPortForCfm (UINT4 u4ContextId, UINT2 u2Port, tVlanTag VlanTagInfo)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT1               u1Result;

    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanTagInfo.OuterVlanTag.u2VlanId);
    /*ECFM frames should not be transmitted out if
     * a) Vlan is not present
     * b) Port is not member of Static Vlan */
    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_IS_MEMBER_PORT (pVlanEntry->EgressPorts, u2Port, u1Result);
    }
    else
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (u1Result == VLAN_FALSE)
    {

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanTransmitCfmFrame                             */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanTransmitCfmFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                      UINT2 u2Port, tVlanTag VlanTagInfo, UINT1 u1FrameType)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanIfMsg          VlanIfMsg;
    UINT1               u1Result;

    MEMSET (&VlanIfMsg, 0, sizeof (tVlanIfMsg));

    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanTagInfo.OuterVlanTag.u2VlanId);

    /*ECFM frames should not be transmitted out if
     * a) Vlan is not present
     * b) Port is not member of Static Vlan */
    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_IS_MEMBER_PORT (pVlanEntry->EgressPorts, u2Port, u1Result);
    if (u1Result == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanIfMsg.u4ContextId = u4ContextId;
    VlanIfMsg.u2LocalPort = u2Port;
    VlanIfMsg.u4IfIndex = VLAN_GET_IFINDEX (u2Port);
    VLAN_COPY_FROM_BUF (pBuf, &VlanIfMsg.DestAddr, 0, ETHERNET_ADDR_SIZE);
    VLAN_COPY_FROM_BUF (pBuf, &VlanIfMsg.SrcAddr, ETHERNET_ADDR_SIZE,
                        ETHERNET_ADDR_SIZE);
    VlanIfMsg.u2Length = (UINT2) VLAN_GET_BUF_LEN (pBuf);

    VlanFwdOnSinglePort (pBuf, &VlanIfMsg, u2Port, pVlanEntry,
                         &VlanTagInfo, u1FrameType);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetFwdPortList                               */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the list of ports to which the pkt is fwded. */
/*                                                                           */
/*    Input(s)            : pFrame- Incoming frame                           */
/*                          u4ContextId - Context Identifier                 */
/*                          u2LocalPort - Port Identifier                    */
/*                          SrcAddr - Source MacAddress to be learnt         */
/*                          DstMacAddr - Destination Mac Address for lookup  */
/*                          VlanId - VlanId of the frame                     */
/*                                                                           */
/*    Output(s)           : FwdPortList- list of port(s) to which the pkt    */
/*                          has to be forwarded                              */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetFwdPortList (UINT4 u4ContextId, UINT2 u2LocalPort, tMacAddr SrcAddr,
                    tMacAddr DestAddr, tVlanId VlanId, tLocalPortList
                    FwdPortList)
{
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    tVlanTempFdbInfo    VlanTempFdbInfo;
    tMacAddr            NullMacAddr;
    UINT1               u1FloodStatus = VLAN_ENABLED;

#ifdef NPAPI_WANTED
    UNUSED_PARAM (SrcAddr);
#endif

    MEMSET (FwdPortList, 0, sizeof (tLocalPortList));
    MEMSET (NullMacAddr, 0, sizeof (tMacAddr));

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Vlan does not exists in this system\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

#ifndef NPAPI_WANTED
    /*Check if vlan learning is required */
    if (MEMCMP (SrcAddr, NullMacAddr, 6) != 0)
    {
        VlanLearn (SrcAddr, u2LocalPort, pVlanCurrEntry);
    }
#else
    UNUSED_PARAM (SrcAddr);
#endif

    if (VLAN_IS_MCASTADDR (DestAddr) == VLAN_TRUE)
    {
        VlanGetMcastFwdPortList (DestAddr, u2LocalPort, pVlanCurrEntry,
                                 FwdPortList);
    }
    else
    {
        pStUcastEntry = VlanGetStaticUcastEntry (pVlanCurrEntry->u4FidIndex,
                                                 DestAddr, u2LocalPort);

        if (pStUcastEntry != NULL)
        {
            VLAN_MEMCPY (FwdPortList, pStUcastEntry->AllowedToGo,
                         sizeof (tLocalPortList));
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_SUCCESS;
        }

        pFidEntry = VLAN_GET_FID_ENTRY (pVlanCurrEntry->u4FidIndex);

        if (VlanGetFdbInfo (pFidEntry->u4Fid, DestAddr,
                            &VlanTempFdbInfo) != VLAN_SUCCESS)
        {
            VlanL2IwfGetVlanFloodingStatus (VLAN_CURR_CONTEXT_ID (),
                                            pVlanCurrEntry->VlanId,
                                            &u1FloodStatus);

            VlanReleaseContext ();
            VLAN_UNLOCK ();

            /* If this function returns failure then the portlist given as
             * output will be ignored and the packet will be flooded on all
             * the member portsi. So if flooding is enabled then return
             * Success */
            if (u1FloodStatus == VLAN_DISABLED)
            {
                return VLAN_SUCCESS;
            }
            return VLAN_FAILURE;
        }

        VLAN_SET_MEMBER_PORT (FwdPortList, VlanTempFdbInfo.u2Port);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanConfPortVlanLckStatus                           */
/*                                                                           */
/* Description        : This routine is called by ECFM module to update the  */
/*                      the LCK status for Data corresponding to Vlan        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      VlanId - Vlan Id                                     */
/*                      b1LckStatus - LCk status to be updated               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanConfPortVlanLckStatus (UINT4 u4IfIndex, UINT2 u2VlanId, BOOL1 b1LckStatus)
{
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan;
    tVlanCurrEntry     *pCurrEntry = NULL;
#endif
    tVlanPortEntry     *pVlanPortEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortNum = 0;

    VLAN_LOCK ();

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortNum) == VCM_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VLAN_IS_PORT_VALID (u2LocalPortNum) == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortNum);

    if (pVlanPortEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

#ifndef NPAPI_WANTED
    pCurrEntry = VlanGetVlanEntry (u2VlanId);

    if (pCurrEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        pPortInfoPerVlan = VlanGetPortStatsEntry (pCurrEntry, u2LocalPortNum);

        if (pPortInfoPerVlan == NULL)
        {
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }

        pPortInfoPerVlan->b1LckStatus = b1LckStatus;
    }

#else
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (b1LckStatus);
     /*TODO*/
/* Call NPAPI to lock data traffic*/
#endif
        VlanReleaseContext ();
    VLAN_UNLOCK ();

    return bResult;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfCheckForPvrst                            */
/*                                                                           */
/*    Description         : This function will be called before starting     */
/*                          PVRST for the given context. This function       */
/*                          checks whether the configurations in vlan are    */
/*                          suitable for PVRST operation.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Denotes the mismatch type.        */
/*                          VLAN_PVRST_VLAN_NOTSTARTED_ERR- Vlan Module is   */
/*                                                          Shutdown.        */
/*                          VLAN_PVRST_CXT_NOTPRESENT_ERR - Context Not      */
/*                                                          present in VLAN. */
/*                          VLAN_PVRST_HYBRID_PVID_ERR    - Hybrid port Pvid */
/*                                                          is other than    */
/*                                                          default VLAN.    */
/*                          VLAN_PVRST_HYBRID_UNTAG_ERR   - Hybrid port is   */
/*                                                          not an untagged  */
/*                                                          member of VLAN 1.*/
/*                          VLAN_PVRST_HYBRID_VLAN_ERR    - Hybrid port is   */
/*                                                          member of vlan   */
/*                                                          other than       */
/*                                                          default vlan.    */
/*                          VLAN_PVRST_ACCESS_PVID_ERR    - Access port is   */
/*                                                          member of VLANs  */
/*                                                          other than pvid  */
/*                                                          vlan.            */
/*                          VLAN_PVRST_FORBIDDEN_PORT_ERR - Forbidden port-  */
/*                                                          list of any vlan */
/*                                                          is not NULL.     */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanConfigCheckForPvrst (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT2               u2Port;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1Result1 = VLAN_FALSE;

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = VLAN_PVRST_VLAN_NOTSTARTED_ERR;
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = VLAN_PVRST_CXT_NOTPRESENT_ERR;
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry->u1PortType == VLAN_TRUNK_PORT)
        {
            /* If the port is a trunk port then no further check is 
             * required for PVRST. */
            continue;
        }

        VLAN_SCAN_VLAN_TABLE (VlanId)
        {
            pVlanEntry = VlanGetVlanEntry (VlanId);
            if (pVlanEntry == NULL)
            {
                continue;
            }

            VLAN_IS_CURR_EGRESS_PORT (pVlanEntry, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if (pVlanPortEntry->u1PortType == VLAN_HYBRID_PORT)
                {
                    if (VlanId != VLAN_DEF_VLAN_ID)
                    {
                        /* Hybrid port is a member of VLAN other than 
                         * default VLAN. */
                        *pu4ErrorCode = VLAN_PVRST_HYBRID_VLAN_ERR;
                        i4RetVal = VLAN_FAILURE;
                        break;
                    }
                }
                else
                {
                    if (pVlanPortEntry->Pvid != VlanId)
                    {
                        /* Access-port is a member of VLAN other the 
                         * corresponding PVID's VLAN. */
                        *pu4ErrorCode = VLAN_PVRST_ACCESS_PVID_ERR;
                        i4RetVal = VLAN_FAILURE;
                        break;
                    }
                }
            }
        }

        if (i4RetVal == VLAN_FAILURE)
        {
            break;
        }
    }

    if (i4RetVal != VLAN_FAILURE)
    {
        pStVlanEntry = VLAN_CURR_CONTEXT_PTR ()->pStaticVlanTable;

        while (pStVlanEntry != NULL)
        {
            /* Check whether the Forbidden list is NULL */
            VLAN_IS_NO_FORBIDDEN_PORTS_PRESENT (pStVlanEntry, u1Result);
            if (u1Result == VLAN_FALSE)
            {
                *pu4ErrorCode = VLAN_PVRST_FORBIDDEN_PORT_ERR;
                i4RetVal = VLAN_FAILURE;
                break;
            }

            /* 
             * Consider the following scenario:
             *       - Create a new VLAN through SNMP by setting CreateAndWait.
             *       - Set the Hybrid port as a Tagged port.
             *       - Start PVRST.
             *       - Make Vlan active
             *       - In this case, this hybrid port becomes tagged member 
             *         port of this vlan, which is not expected for PVRST 
             *         running.
             *         The above loops only take care of active vlans, hence 
             *         this issue has to be addressed seperately by the 
             *         following.
             *
             * So for enabling PVRST Check the Non-active static vlans.
             */
            if (pStVlanEntry->u1RowStatus != VLAN_ACTIVE)
            {
                VLAN_SCAN_PORT_TABLE (u2Port)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                    if (pVlanPortEntry->u1PortType == VLAN_TRUNK_PORT)
                    {
                        /* If the port is a trunk port then no further check is 
                         * required for PVRST. */
                        continue;
                    }
                    VLAN_IS_EGRESS_PORT (pStVlanEntry, u2Port, u1Result);

                    if (u1Result != VLAN_TRUE)
                    {
                        continue;
                    }

                    if (pVlanPortEntry->u1PortType == VLAN_HYBRID_PORT)
                    {
                        if (pStVlanEntry->VlanId != VLAN_DEF_VLAN_ID)
                        {
                            /* Hybrid port is a member of VLAN other than 
                             * default VLAN. */
                            *pu4ErrorCode = VLAN_PVRST_HYBRID_VLAN_ERR;
                            i4RetVal = VLAN_FAILURE;
                            break;
                        }
                        /* Checking the untagged membership */
                        VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result1);
                        if (u1Result1 != VLAN_TRUE)
                        {
                            *pu4ErrorCode = VLAN_PVRST_HYBRID_UNTAG_ERR;
                            i4RetVal = VLAN_FAILURE;
                            break;
                        }
                    }
                    else
                    {
                        if (pVlanPortEntry->Pvid != pStVlanEntry->VlanId)
                        {
                            /* Access-port is a member of VLAN other the 
                             * corresponding PVID's VLAN. */
                            *pu4ErrorCode = VLAN_PVRST_ACCESS_PVID_ERR;
                            i4RetVal = VLAN_FAILURE;
                            break;
                        }
                    }
                }
            }

            pStVlanEntry = pStVlanEntry->pNextNode;
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfigUpdtForPvrstStart                      */
/*                                                                           */
/*    Description         : This function is called by PVRST module when     */
/*                          it comes up. The changes required in VLAN for    */
/*                          enabling pvrst will be done in this function.    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanConfigUpdtForPvrstStart (UINT4 u4ContextId)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Result = VLAN_FALSE;

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry->u1PortType != VLAN_ACCESS_PORT)
        {
            /* If the port is a trunk port then no further check is
             * required for PVRST. */
            pVlanEntry = VlanGetVlanEntry (VLAN_DEF_VLAN_ID);

            if (NULL != pVlanEntry)
            {
                VLAN_IS_MEMBER_PORT (pVlanEntry->EgressPorts, u2Port, u1Result);
            }

            if (u1Result == VLAN_TRUE)
            {
                VlanUtilSetPvid (u2Port, VLAN_DEF_VLAN_ID);
            }
            continue;
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfigUpdtForPvrstShutDown                   */
/*                                                                           */
/*    Description         : This function is called by PVRST module when     */
/*                          it goes down. The changes required in VLAN for   */
/*                          disabling pvrst will be done in this function.   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanConfigUpdtForPvrstShutDown (UINT4 u4ContextId)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry->u1PortType != VLAN_TRUNK_PORT)
        {
            continue;
        }

        /* Modify trunk port as a tagged member for corresponding 
         * PVID's VLAN */
        pStVlanEntry = VlanGetStaticVlanEntry (pVlanPortEntry->Pvid);

        if (pStVlanEntry != NULL)
        {
            VLAN_RESET_SINGLE_UNTAGGED_PORT (pStVlanEntry, u2Port);
            VLAN_RESET_SINGLE_EGRESS_PORT (pStVlanEntry, u2Port);
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanSendSrcRelearnTrapAndSysLog                      */
/*                                                                           */
/* Description        : This function will post an event to sysLog and send  */
/*                      an trap to the administrator whenever there is a     */
/*                      source relearning in a VLAN                          */
/*                                                                           */
/* Input(s)           : pVlanSrcRelearnTrap   - pointer to structure         */
/*                                              containing the               */
/*                                              information to generate trap */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSendSrcRelearnTrapAndSysLog (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap)
{
    UINT4               u4ContextId;
    UINT2               u2LocalNewPort;
    UINT2               u2LocalOldPort;

    if (pVlanSrcRelearnTrap == NULL)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (pVlanSrcRelearnTrap->u4OldPort,
                                       &u4ContextId,
                                       &u2LocalOldPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanGetContextInfoFromIfIndex (pVlanSrcRelearnTrap->u4NewPort,
                                       &u4ContextId,
                                       &u2LocalNewPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanSrcRelearnTrap->u4NewPort = u2LocalNewPort;
    pVlanSrcRelearnTrap->u4OldPort = u2LocalOldPort;

#ifdef SYSLOG_WANTED
    VlanSendSrcRelearnSyslog (pVlanSrcRelearnTrap);
#endif
#ifdef  SNMP_2_WANTED
    if (VlanSendSrcRelearnTrap (pVlanSrcRelearnTrap) != VLAN_SUCCESS)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Sending Trap of MAC Relearn event failed\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
#endif
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*            APIs called from L2IWF when the BRIDGE module is NO            */
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetAgeOutTime ()                             */
/*                                                                           */
/*    Description         : Function is called to get the mac address table  */
/*                          Age Out Time from vlan when bridge is disabled   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Age Out time of mac address table                 */
/*****************************************************************************/
UINT4
VlanGetAgeOutTime (VOID)
{
    return (gpVlanContextInfo->u4AgeOutTime);
}

/*****************************************************************************/
/* Function Name      : VlanIncrFilterInDiscards                              */
/*                                                                           */
/* Description        : This function increments the Filter Indiscard        */
/*                                                                           */
/* Input(s)           : u2Port - port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanIncrFilterInDiscards (UINT2 u2Port)
{
    /* Assumption - 
     * 1) VLAN Lock already taken
     * 2) Appropriate context alredy selected
     * since this function is called from VLAN module.
     * Hence need not take lock or select context here*/
#ifdef SYS_HC_WANTED
    /*BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u2Port); */
    VLAN_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u2Port);
#else /* SYS_HC_WANTED */
    /*BRG_INCR_PORT_FILTER_IN_DISCARDS (u2Port); */
    VLAN_INCR_PORT_FILTER_IN_DISCARDS (u2Port);
#endif /* SYS_HC_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanStartHwAgeTimer ()                           */
/*                                                                           */
/*    Description         : Function is called to set the hardware aging     */
/*                          time when bridge is disabled                     */
/*                                                                           */
/*    Input(s)            : u4AgeOut - AgeOut time                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanStartHwAgeTimer (UINT4 u4ContextId, UINT4 u4AgeOut)
{
    VlanHwSetAgingTime (u4ContextId, (INT4) u4AgeOut);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortOperPointToPointUpdate                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port's Oper P2P   */
/*                          status get changed.                              */
/*                                                                           */
/*    Input(s)            : u2IfIndex - Port for which P2P status changed. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortOperPointToPointUpdate (UINT4 u4IfIndex)
{

    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_PORT_P2P_UPDATE_MSG, u4IfIndex);

    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortStpStateUpdate                           */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port changes to   */
/*                          discarding or Forwarding                         */
/*                                                                           */
/*    Input(s)            : u2IfIndex - Port for which STP state changed.    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortStpStateUpdate (UINT4 u4IfIndex)
{

    INT4                i4RetVal;

    i4RetVal = VlanPostCfgMessage (VLAN_PORT_STP_STATE_UPDATE_MSG, u4IfIndex);

    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsUcastWildCardEntryPresent                  */
/*                                                                           */
/*    Description         : This function checks if the Unicast MacAddress   */
/*                          is registered as wild card entry                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2VlanId    - Vlan Id                            */
/*                          MacAddr     -  MAC address                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_TRUE/OSIX_FALSE                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsUcastWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId,
                                 tMacAddr MacAddr)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    INT4                i4RetVal = OSIX_TRUE;

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        return OSIX_FALSE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return OSIX_FALSE;
    }

    pVlanEntry = VlanGetVlanEntry (u2VlanId);

    if (pVlanEntry != NULL)
    {
        pStUcastEntry = VlanGetStaticUcastEntry (pVlanEntry->u4FidIndex,
                                                 MacAddr, 0);
        if (pStUcastEntry != NULL)
        {
            i4RetVal = OSIX_TRUE;
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return i4RetVal;
        }
    }

    pWildCardEntry = VlanGetWildCardEntry (MacAddr);

    if (pWildCardEntry != NULL)
    {
        /*Wild card entry present */
        i4RetVal = OSIX_FALSE;

    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMcastWildCardEntryPresent                  */
/*                                                                           */
/*    Description         : This function checks if the Multicast MacAddress */
/*                          is registered as wild card entry                 */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          MacAddr -  MAC address                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMcastWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId,
                                 tMacAddr MacAddr)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    INT4                i4RetVal = OSIX_TRUE;

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        return OSIX_FALSE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return OSIX_FALSE;
    }

    pVlanEntry = VlanGetVlanEntry (u2VlanId);

    if (pVlanEntry != NULL)
    {
        pStMcastEntry = pVlanEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            /* Check if the Mac Address from the Input and the Mac Address
             * in the static Multicast Table are equal,
             * allow learning if there is static entry present for
             * this Mac Address.*/
            if (VLAN_ARE_MAC_ADDR_EQUAL (MacAddr, pStMcastEntry->MacAddr)
                == VLAN_TRUE)
            {
                i4RetVal = OSIX_TRUE;
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return i4RetVal;
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }
    }

    pWildCardEntry = VlanGetWildCardEntry (MacAddr);

    if (pWildCardEntry != NULL)
    {
        /*Wild card entry present */
        i4RetVal = OSIX_FALSE;

    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPortEtherType                             */
/*                                                                           */
/*    Description         : This function returns the EtherType configured   */
/*                          for a particular port.                           */
/*                                                                           */
/*    Input(s)            : u2IfIndex  - Interface for which the EtherType   */
/*                                       has to be returned.                 */
/*                                                                           */
/*    Output(s)           : pu2EtherType - EtherType configured for the port */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetPortEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherType)
{
    tVlanPortEntry     *pPortEntry;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    *pu2EtherType = pPortEntry->u2EgressEtherType;

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetIngressPortEtherType                      */
/*                                                                           */
/*    Description         : This function returns the ingress EtherType      */
/*                          configured for a particular port.                */
/*                                                                           */
/*    Input(s)            : u2IfIndex  - Interface for which the EtherType   */
/*                                       has to be returned.                 */
/*                                                                           */
/*    Output(s)           : pu2EtherType - EtherType configured for the port */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetIngressPortEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherType)
{
    tVlanPortEntry     *pPortEntry;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    *pu2EtherType = pPortEntry->u2IngressEtherType;

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsProtocolVlanSupported                      */
/*                                                                           */
/*    Description         : This function is called by LLDP to get the       */
/*                          Protocol based Vlan Classification support       */
/*                          in the switch.                                   */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE / VLAN_FALSE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsProtocolVlanSupported (VOID)
{
    /* As there is support for protocol based vlan, always return TRUE */
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetProtoVlanStatusOnPort                     */
/*                                                                           */
/*    Description         : This function is called by LLDP to get the       */
/*                          status of Protocol based Vlan Classification     */
/*                          on the port                                      */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Number                          */
/*                                                                           */
/*    Output(s)           : pu1ProtoVlanStatus - Protocol based Vlan status  */
/*                                               (Enabled or Diabled )       */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetProtoVlanStatusOnPort (UINT4 u4IfIndex, UINT1 *pu1ProtoVlanStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        *pu1ProtoVlanStatus = VLAN_SNMP_FALSE;
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }

    *pu1ProtoVlanStatus = VLAN_PORT_PORT_PROTOCOL_BASED (u2LocalPort);
    VlanReleaseContext ();

    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetProtoVlanOnPort                           */
/*                                                                           */
/*    Description         : This function is called by LLDP to get the       */
/*                          list of protocol vlans mapped to the port        */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Number                          */
/*                                                                           */
/*    Output(s)           : apConfProtoVlans - Pointer to array of protocol  */
/*                                             vlans mapped to the port      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetProtoVlanOnPort (UINT4 u4IfIndex, UINT2 *apConfProtoVlans,
                        UINT1 *pu1NumVlan)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSet = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    *pu1NumVlan = 0;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }

    VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSet,
                   tVlanPortVidSet *)
    {
        apConfProtoVlans[*pu1NumVlan] = pVlanPortVidSet->VlanId;
        (*pu1NumVlan)++;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

#ifdef MRVLLS
/*****************************************************************************/
/*    Function Name       : VlanHwFdbTableAdd                                */
/*    Description         : This function is used to program the Source      */
/*                          MAC address in hardware.                         */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanHwFdbTableAdd (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                   UINT1 u1EntryType)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1PortState = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return;
    }
    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return;
    }
    u1PortState = VlanL2IwfGetVlanPortState (VlanId, u4IfIndex);

    if ((u1PortState == AST_PORT_STATE_LEARNING) ||
        (u1PortState == AST_PORT_STATE_FORWARDING))
    {

        pVlanQMsg = (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                                         sizeof (tVlanQMsg));

        if (NULL == pVlanQMsg)
        {
            return;
        }

        VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

        pVlanQMsg->u2MsgType = VLAN_FDBENTRY_ADD_MSG;
        pVlanQMsg->FdbData.VlanId = VlanId;
        pVlanQMsg->u4ContextId = u4ContextId;
        pVlanQMsg->u4Port = u4IfIndex;
        pVlanQMsg->u1EntryType = u1EntryType;
        MEMCPY (pVlanQMsg->FdbData.au1MacAddr, MacAddr, CFA_ENET_ADDR_LEN);

        if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                                (UINT1 *) &pVlanQMsg,
                                OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
        {
            VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

            return;
        }

        if (VLAN_TASK_ID == VLAN_INIT_VAL)
        {
            if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
                OSIX_SUCCESS)
            {
                /* No Need to Free Message Since it is already Queued */
                return;
            }
        }

        if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) == OSIX_FAILURE)
        {
            return;
        }
    }
    return;
}
#endif
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAddOuterTagInFrame                           */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame since the ethertype will be   */
/*                          based on the port type                           */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                          u4IfIndex  - Interface Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD/VLAN_NO_FORWARD                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanAddOuterTagInFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId,
                        UINT1 u1Priority, UINT4 u4IfIndex)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) != VLAN_SUCCESS)
    {
        return VLAN_NO_FORWARD;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return VLAN_NO_FORWARD;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    VlanAddTagToFrame (pFrame, VlanId, u1Priority, VLAN_DE_FALSE,
                       pVlanPortEntry);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanInfoAndUntagFrame                     */
/*                                                                           */
/*    Description         : This function is called by L2Iwf to get VLAN     */
/*                          Information and strip of the outer-vlan from the */
/*                          packet before posting the packet to Snoop.       */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u4InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pFrame   - Pointer to the updated packet         */
/*                          pVlantag  - Pointer to TAG Info of the packet    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetVlanInfoAndUntagFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                              UINT4 u4InPort, tVlanTag * pVlanTag)
{
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan;
#endif
    tMacAddr            SrcAddr;
    tVlanId             VlanId;
    tVlanCurrEntry     *pVlanRec;
    tVlanPortEntry     *pPortEntry;
    tVlanPortEntry     *pSchPortEntry;
    INT4                i4Result;
    INT4                i4IfBrgPortType = 0;
    UINT4               u4SChIfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1PortState;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4InPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanL2IwfIncrFilterInDiscards (u2LocalPort);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC,
                  VLAN_NAME, "Port Oper Status DOWN. Frame received\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    VLAN_COPY_FROM_BUF (pFrame, &SrcAddr, ETHERNET_ADDR_SIZE,
                        ETHERNET_ADDR_SIZE);

    VlanId = VlanGetVlanId (pFrame, SrcAddr, u2LocalPort, pVlanTag);
    /*If the Port is UAP, S-vlan should be removed and the c-vlan Id should be used.
     *If the c-vlan is not present, pvid of the s-channel interface is used.
     */
    if ((VlanCfaGetInterfaceBrgPortType (u4InPort, &i4IfBrgPortType)
         == VLAN_SUCCESS) && (i4IfBrgPortType == CFA_UPLINK_ACCESS_PORT))
    {
        if (pVlanTag->InnerVlanTag.u2VlanId != VLAN_NULL_VLAN_ID)
        {
            VlanUnTagFrame (pFrame);
            VlanId = pVlanTag->InnerVlanTag.u2VlanId;
        }
        else
        {
            VlanUnTagFrame (pFrame);
            VlanGetSChIfIndex (u4InPort, (UINT4) VlanId, &u4SChIfIndex);
            if (VlanVcmGetContextInfoFromIfIndex (u4SChIfIndex, &u4ContextId,
                                                  &u2LocalPortId) ==
                VCM_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "VCM Context Info is not present for "
                               "S-Channel entry is not present for SVID 0x%x \n",
                               VlanId);
                VlanL2IwfIncrFilterInDiscards (u2LocalPort);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
            pSchPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);
            if (pSchPortEntry != NULL)
            {
                VlanId = pSchPortEntry->Pvid;
                pVlanTag->InnerVlanTag.u2VlanId = VlanId;
            }
            else
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "S-Channel entry is not present for SVID 0x%x \n",
                               VlanId);
                VlanL2IwfIncrFilterInDiscards (u2LocalPort);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
        }
    }
    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "VLAN ID 0x%x associated with the Frame "
                       "is unknown \n", VlanId);

        VlanL2IwfIncrFilterInDiscards (u2LocalPort);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    i4Result = VlanIngressFiltering (pVlanRec, u2LocalPort);

    if (i4Result == VLAN_NO_FORWARD)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Frame received on port %d is discarded due"
                       " to Ingress Filtering.\n", u4InPort);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

            if (NULL != pPortInfoPerVlan)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif /*NPAPI_WANTED */

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    u1PortState = VlanL2IwfGetVlanPortState (VlanId, u4InPort);

    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Frame received on port %d is discarded ", u4InPort);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

            if (NULL != pPortInfoPerVlan)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    VLAN_IS_CURR_EGRESS_PORT (pVlanRec, u2LocalPort, u1Result);

#ifndef NPAPI_WANTED

    if (u1Result == VLAN_TRUE)
    {
        VlanLearn (SrcAddr, u2LocalPort, pVlanRec);
    }

    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

        if (NULL != pPortInfoPerVlan)
        {
            VLAN_INC_IN_FRAMES (pPortInfoPerVlan);
        }
    }
#endif

    if ((pVlanTag->OuterVlanTag.u1TagType != VLAN_UNTAGGED) &&
        (u1Result == VLAN_TRUE))
    {
        /* Before sending the packet to Snooping module only the 
         * Outer tag should get removed. */
        VlanUnTagFrame (pFrame);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCfgLearningFlooding                          */
/*                                                                           */
/*    Description         : This function is called to                       */
/*                          enable/disable MAC learning for the given vlan   */
/*                          and also to enable/disable multicast flooding    */
/*                          for the vlan by configuring the ForwardAll and   */
/*                          ForwardUnreg Portlist.                           */
/*                                                                           */
/*    Input(s)            : u4ContextID - Virtual Context Id                 */
/*                          VlanId      - VlanId to which the learning and   */
/*                                        flooding has to diasble.           */
/*                          u1Action    - VLAN_ENABLED / VLAN_DISABLED       */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE /                     */
/*                         VLAN_CONTEXT_INVALID / VLAN_NOT_PRESENT /         */
/*                         VLAN_HW_FAILURE                                   */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCfgLearningFlooding (UINT4 u4ContextId, tVlanId VlanId,
                         UINT1 u1Action, UINT1 *pu1ErrorCode)
{
    UINT1              *pLocalPortList = NULL;
    UINT1              *pOldLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE StaticOldPortList;
    UINT4               u4ErrorCode = VLAN_INIT_VAL;
    INT4                i4OldAction = VLAN_INIT_VAL;
    INT4                i4RetVal = 0;

    VLAN_LOCK ();
    pLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pLocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCfgLearningFlooding: Error in allocating memory "
                  "for pLocalPortList\r\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pOldLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pOldLocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCfgLearningFlooding: Error in allocating memory "
                  "for pOldLocalPortList\r\n");
        UtilPlstReleaseLocalPortList (pLocalPortList);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    MEMSET (pLocalPortList, VLAN_INIT_VAL, sizeof (tLocalPortList));
    StaticPortList.i4_Length = sizeof (tLocalPortList);
    StaticPortList.pu1_OctetList = pLocalPortList;

    MEMSET (pOldLocalPortList, VLAN_INIT_VAL, sizeof (tLocalPortList));
    StaticOldPortList.i4_Length = sizeof (tLocalPortList);
    StaticOldPortList.pu1_OctetList = pOldLocalPortList;

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Invalid Context\n");
        *pu1ErrorCode = (UINT1) VLAN_API_ERR_CONTEXT_INVALID;
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanTstVlanAdminMacLearnStatus (&u4ErrorCode, VlanId, u1Action)
        == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Not Present\n");
        *pu1ErrorCode = (UINT1) VLAN_API_ERR_ENTRY_NOT_PRESENT;
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanGetVlanAdminMacLearnStatus (VlanId, &i4OldAction);
    VlanGetVlanForwardAllStaticPorts (VlanId, &StaticOldPortList);

    if (VlanSetVlanAdminMacLearnStatus (VlanId, u1Action) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Configuring Mac Learning"
                  " status for the VLAN in hardware Failed\n");
        *pu1ErrorCode = (UINT1) VLAN_API_ERR_HW_FAILURE;
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /* Configuration of Forward All */

    /* The FwdAll portlist will always be set to all 0's irrespective of
     * whether Flooding is being enabled or disabled */

    if (VlanTestForwardAllStaticPorts (&u4ErrorCode, VlanId, &StaticPortList)
        == VLAN_FAILURE)
    {
        i4RetVal = VlanSetVlanAdminMacLearnStatus (VlanId, i4OldAction);

        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Configuring NULL Ports"
                  " For ForwardAll for the VLAN has Failed\n");
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSetForwardAllStaticPorts (VlanId, &StaticPortList) == VLAN_FAILURE)
    {
        i4RetVal = VlanSetVlanAdminMacLearnStatus (VlanId, i4OldAction);

        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Configuring NULL Ports"
                  " For ForwardAll for the VLAN in the Hardware Failed\n");
        *pu1ErrorCode = (UINT1) VLAN_API_ERR_HW_FAILURE;
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /* Configuration of Forward Unreg */
    if (u1Action == VLAN_ENABLED)
    {
        /* If the action is to enable flooding then the ForwardUnreg ports 
         * will be configured as all 1's. If the action is to disable,
         * the PortList will be NULL. */

        VlanSetAllPorts (pLocalPortList);
    }

    if (VlanTestForwardUnregStaticPorts (&u4ErrorCode, VlanId,
                                         &StaticPortList) == VLAN_FAILURE)
    {
        i4RetVal = VlanSetVlanAdminMacLearnStatus (VlanId, i4OldAction);

        VlanSetForwardAllStaticPorts (VlanId, &StaticOldPortList);

        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Testing Ports"
                  " For ForwardUnreg for the VLAN has Failed\n");
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSetForwardUnregStaticPorts (VlanId, &StaticPortList)
        == VLAN_FAILURE)
    {
        i4RetVal = VlanSetVlanAdminMacLearnStatus (VlanId, i4OldAction);

        VlanSetForwardAllStaticPorts (VlanId, &StaticOldPortList);

        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Configuring Ports"
                  " For ForwardUnreg for the VLAN Failed\n");
        *pu1ErrorCode = (UINT1) VLAN_API_ERR_HW_FAILURE;
        UtilPlstReleaseLocalPortList (pLocalPortList);
        UtilPlstReleaseLocalPortList (pOldLocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Disabling of Mac Learning"
              " and Flooding for the VLAN Succeeded\n");
    UNUSED_PARAM (i4RetVal);
    UtilPlstReleaseLocalPortList (pLocalPortList);
    UtilPlstReleaseLocalPortList (pOldLocalPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetStaticUnicastEntry                        */
/*                                                                           */
/*    Description         : This function is called to get                   */
/*                          the information about the static unicast entry   */
/*                          for the given DA and VlanID.                     */
/*                                                                           */
/*    Input(s)            :  pStaticUnicastInfo - This structure contains    */
/*                                                the following variables    */
/*                                                to hold the static unicast */
/*                                                information.               */
/*                           u4ContextId -  To get the static unicast        */
/*                                          information of this context      */
/*                           VlanId      - Vlan Id.                          */
/*                 DestMac     - Destibnation Mac Address          */
/*                                                                           */
/*    Output(s)           :  u4EgressIfIndex - Egress port.                  */
/*                           u4Status - Status of the Static Unicast Entry.  */
/*                                      It will take any one of the          */
/*                                      following parameters.                */
/*                                      PERMENANT / DELETE_ON_RESET.         */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE /                     */
/*                         VLAN_CONTEXT_INVALID / VLAN_NOT_ACTIVE /          */
/*                          VLAN_NOT_PRESENT                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetStaticUnicastEntry (tConfigStUcastInfo * pStaticUnicastInfo,
                           UINT1 *pu1ErrCode)
{
    UINT1              *pau1StaticPorts = NULL;
    tSNMP_OCTET_STRING_TYPE AllowedToGo;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    UINT4               u4FidIndex = VLAN_INIT_VAL;
    UINT4               u4FdbId = VLAN_INIT_VAL;
    UINT2               u2Port;
    UINT2               u2ByteInd = VLAN_INIT_VAL;
    UINT2               u2BitIndex = VLAN_INIT_VAL;
    UINT1               u1PortFlag = VLAN_INIT_VAL;
    VLAN_LOCK ();
    pau1StaticPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanGetStaticUnicastEntry: "
                  "Error in allocating memory for pau1StaticPorts\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pau1StaticPorts, 0, VLAN_PORT_LIST_SIZE);

    AllowedToGo.i4_Length = VLAN_PORT_LIST_SIZE;
    AllowedToGo.pu1_OctetList = pau1StaticPorts;

    if (pStaticUnicastInfo == NULL)
    {
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (pStaticUnicastInfo->u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Invalid Context\n");

        *pu1ErrCode = (UINT1) VLAN_API_ERR_CONTEXT_INVALID;
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanGetFdbIdFromVlanId (pStaticUnicastInfo->VlanId, &u4FdbId)
        == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Is Not Active\n");

        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        *pu1ErrCode = (UINT1) VLAN_API_ERR_VLAN_NOT_ACTIVE;
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pStUcastEntry = VlanGetStaticUcastEntry (u4FidIndex,
                                             pStaticUnicastInfo->DestMac, 0);
    if (pStUcastEntry != NULL)
    {
        MEMCPY (AllowedToGo.pu1_OctetList, pStUcastEntry->AllowedToGo,
                VLAN_PORT_LIST_SIZE);
    }
    else
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Is Not Present\n");
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        VlanReleaseContext ();

        VLAN_UNLOCK ();
        *pu1ErrCode = (UINT1) VLAN_API_ERR_ENTRY_NOT_PRESENT;
        return VLAN_FAILURE;
    }

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {

        if (AllowedToGo.pu1_OctetList[u2ByteInd] != 0)
        {

            u1PortFlag = AllowedToGo.pu1_OctetList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {

                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);
                    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
                    {
                        UtilPlstReleaseLocalPortList (pau1StaticPorts);
                        VlanReleaseContext ();
                        VLAN_UNLOCK ();
                        return VLAN_FAILURE;
                    }
                    pStaticUnicastInfo->u4EgressIfIndex =
                        VLAN_GET_IFINDEX (u2Port);

                    /* Only one port will be set in the AllowedToGo. 
                     * So break here */
                    break;
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    pStaticUnicastInfo->u4Status = (UINT4) pStUcastEntry->u1Status;
    UtilPlstReleaseLocalPortList (pau1StaticPorts);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : VlanGetStaticMulticastEntry                     */
/*                                                                          */
/*    Description         : This function is called                         */
/*                          to get the static multicast entry from the      */
/*                          static multicast table of the given VLAN        */
/*                          entry.                                          */
/*                                                                          */
/*    Input(s)            : pStaticUnicastInfo - This structure contains    */
/*                                               the following variables    */
/*                                               to hold the static unicast */
/*                         information.                                     */
/*                         u4ContextId -  To get the static unicast         */
/*                                        information of this context       */
/*                         DestMac     -  Destination Mac Address           */
/*                                                                          */
/*     Output(s)           :     u4EgressIfIndex - Egress port.             */
/*                               u4Status - Status of the Static Unicast    */
/*                                            Entry.                        */
/*                                          It will take any one of the     */
/*                                          following parameters.           */
/*                                          PERMENANT / DELETE_ON_RESET.    */
/*                                                                          */
/*    Global Variables Referred : None                                      */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE /                    */
/*                         VLAN_CONTEXT_INVALID / VLAN_NOT_ACTIVE /         */
/*                         VLAN_NOT_PRESENT                                 */
/****************************************************************************/
INT4
VlanGetStaticMulticastEntry (tConfigStMcastInfo * pStaticMCastInfo,
                             UINT1 *pu1ErrCode)
{
    tVlanCurrEntry     *pVlanRec = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT4               u4PrvIfIndex = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_LOCK ();
    if (VlanSelectContext (pStaticMCastInfo->u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Invalid Context\n");

        VLAN_UNLOCK ();

        *pu1ErrCode = (UINT1) VLAN_API_ERR_CONTEXT_INVALID;
        return VLAN_FAILURE;
    }

    pVlanRec = VlanGetVlanEntry (pStaticMCastInfo->VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Is Not Active\n");

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        *pu1ErrCode = (UINT1) VLAN_API_ERR_VLAN_NOT_ACTIVE;
        return VLAN_FAILURE;
    }

    pStMcastEntry = VlanGetStMcastEntry (pStaticMCastInfo->DestMac,
                                         0, pVlanRec);

    if (pStMcastEntry != NULL)
    {
        if (VlanGetFirstPortInContext ((UINT4) pStaticMCastInfo->u4ContextId,
                                       &u4IfIndex) == VLAN_SUCCESS)
        {
            do
            {
                if (VlanGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                   &u4ContextId,
                                                   &u2LocalPortId)
                    != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (pStMcastEntry->EgressPorts,
                                     u2LocalPortId, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    VLAN_SET_MEMBER_PORT
                        (*(pStaticMCastInfo->pEgressIfIndexList), u4IfIndex);
                }

                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInContext ((UINT4)
                                             pStaticMCastInfo->u4ContextId,
                                             u4PrvIfIndex, &u4IfIndex)
                   == VLAN_SUCCESS);
        }
    }
    else
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Is Not Present\n");

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        *pu1ErrCode = (UINT1) VLAN_API_ERR_ENTRY_NOT_PRESENT;
        return VLAN_FAILURE;
    }
    pStaticMCastInfo->u4Status = (UINT4) pStMcastEntry->u1Status;

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*    Function Name       : VlanConfigStaticUcastEntry                     */
/*                                                                         */
/*    Description         : This function is called to                     */
/*                          configure / delete the static unicast entry    */
/*                          in the static unicast table. This routine      */
/*                          performs the same operation as that when a     */
/*                          static unicast entry is configured from        */
/*                          management.                                    */
/*                                                                         */
/*    Input(s)            : pStaticUnicastInfo - This structure contains   */
/*                          the following information about static unicast */
/*                          entry that has to be configured in the static  */
/*                          unicast table.                                 */
/*                           u4ContextId . Virtual context ID              */
/*                           VlanId  -  Vlan Identifier                    */
/*                           DestMac . Destination mac address.            */
/*                           u4EgressIfIndex . Allowed to go port.         */
/*                           u4Status -  Persistance status                */
/*                                                                         */
/*                          u1Action    - Create / Delete the entry with   */
/*                          the given index.                               */
/*                                                                         */
/*    Output(s)           :                                                */
/*                                                                         */
/*                                                                         */
/*    Global Variables Referred : None                                     */
/*                                                                         */
/*    Global Variables Modified : None.                                    */
/*                                                                         */
/*    Exceptions or Operating                                              */
/*    System Error Handling    : None.                                     */
/*                                                                         */
/*    Use of Recursion        : None.                                      */
/*                                                                         */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE /                   */
/*                         VLAN_CONTEXT_INVALID / VLAN_NOT_PRESENT /       */
/*                         VLAN_INVALID_PORTS / VLAN_HW_FAILURE            */
/*                                                                         */
/***************************************************************************/
INT4
VlanConfigStaticUcastEntry (tConfigStUcastInfo * pStaticUnicastInfo,
                            UINT1 u1Action, UINT1 *pu1ErrorCode)
{
    UINT4               u4FdbId = VLAN_INIT_VAL;
    INT4                i4RetVal = VLAN_FAILURE;

    VLAN_LOCK ();

    if (VlanSelectContext (pStaticUnicastInfo->u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Invalid Context\n");

        VLAN_UNLOCK ();

        *pu1ErrorCode = (UINT1) VLAN_API_ERR_CONTEXT_INVALID;
        return VLAN_FAILURE;
    }

    if (VlanGetFdbIdFromVlanId (pStaticUnicastInfo->VlanId,
                                &u4FdbId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Is Not Active\n");

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        *pu1ErrorCode = (UINT1) VLAN_API_ERR_VLAN_NOT_ACTIVE;
        return VLAN_FAILURE;
    }

    if (u1Action == VLAN_CREATE)
    {
        i4RetVal = VlanCreateStaticUnicastEntry (u4FdbId, pStaticUnicastInfo,
                                                 pu1ErrorCode);
    }
    else
    {
        i4RetVal = VlanDeleteStaticUnicastEntry (u4FdbId, pStaticUnicastInfo,
                                                 pu1ErrorCode);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/***************************************************************************/
/*                                                                         */
/*    Function Name       : VlanConfigStaticMcastEntry                     */
/*                                                                         */
/*    Description         : This function is called to                     */
/*                          configure / delete the Static Multicast entry  */
/*                          in the  static multicast table. This routine   */
/*                          performs the same operation as that when a     */
/*                          static multicast entry is configured from      */
/*                          management.                                    */
/*                                                                         */
/*    Input(s)            : StaticMulticastInfo - This structure contains  */
/*                          the following information about static         */
/*                          multicast entry that has to be configured in   */
/*                          the static multicast table.                    */
/*                           u4ContextId - Virtual context ID              */
/*                           VlanId  -  Vlan Identifier                    */
/*                           DestMac - Destination mac address.            */
/*                           EgressIfIndexList - Allowed to go portList.   */
/*                           u4Status -  permanent or deleteOnReset        */
/*                                                                         */
/*                          u1Action    - Create / Delete the entry with   */
/*                          the given index.                               */
/*                                                                         */
/*    Output(s)           :                                                */
/*                                                                         */
/*                                                                         */
/*    Global Variables Referred : None                                     */
/*                                                                         */
/*    Global Variables Modified : None.                                    */
/*                                                                         */
/*    Exceptions or Operating                                              */
/*    System Error Handling    : None.                                     */
/*                                                                         */
/*    Use of Recursion        : None.                                      */
/*                                                                         */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE /                   */
/*                         VLAN_CONTEXT_INVALID / VLAN_NOT_PRESENT /       */
/*                         VLAN_INVALID_PORTS / VLAN_HW_FAILURE            */
/*                                                                         */
/***************************************************************************/
INT4
VlanConfigStaticMcastEntry (tConfigStMcastInfo * pStaticMCastInfo,
                            UINT1 u1Action, UINT1 *pu1ErrorCode)
{
    INT4                i4RetVal = VLAN_FAILURE;

    VLAN_LOCK ();
    if (VlanSelectContext (pStaticMCastInfo->u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Invalid Context\n");

        VLAN_UNLOCK ();

        *pu1ErrorCode = (UINT1) VLAN_API_ERR_CONTEXT_INVALID;
        return VLAN_FAILURE;
    }

    if (u1Action == VLAN_CREATE)
    {
        i4RetVal = VlanCreateStaticMcastEntry (pStaticMCastInfo, pu1ErrorCode);
    }
    else
    {
        i4RetVal = VlanDeleteStaticMcastEntry (pStaticMCastInfo, pu1ErrorCode);

    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i4RetVal;
}

/***************************************************************************/
/*                                                                         */
/*    Function Name       : VlanDelStFiltEntStatusOther                    */
/*                                                                         */
/*    Description         : This function is called to delete all          */
/*                          Static Unicast/Multicast entries that have the */
/*                          the status 'OTHER'.                            */
/*                                                                         */
/*    Input(s)            :u4ContextId - Context Identifier.               */
/*                         VlanId      - The entry of the vlan which is    */
/*                                       specified.                        */
/*                                                                         */
/*    Output(s)           :                                                */
/*                                                                         */
/*                                                                         */
/*    Global Variables Referred : None                                     */
/*                                                                         */
/*    Global Variables Modified : None.                                    */
/*                                                                         */
/*    Exceptions or Operating                                              */
/*    System Error Handling    : None.                                     */
/*                                                                         */
/*    Use of Recursion        : None.                                      */
/*                                                                         */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE /                   */
/*                         VLAN_CONTEXT_INVALID / VLAN_NOT_PRESENT /       */
/*                           VLAN_INVALID_PORTS / VLAN_HW_FAILURE          */
/*                                                                         */
/***************************************************************************/
INT4
VlanDelStFiltEntStatusOther (UINT4 u4ContextId, tVlanId VlanId,
                             UINT1 *pu1ErrCode)
{
    tVlanCurrEntry     *pVlanRec = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanStUcastEntry  *pNextStUcastEntry = NULL;
    tVlanStUcastEntry  *pPrevStUcastEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStMcastEntry  *pNextStMcastEntry = NULL;
    tVlanStMcastEntry  *pPrevStMcastEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Invalid Context\n");

        VLAN_UNLOCK ();

        *pu1ErrCode = (UINT1) VLAN_API_ERR_CONTEXT_INVALID;
        return VLAN_FAILURE;
    }

    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Vlan Is Not Active\n");

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        *pu1ErrCode = (UINT1) VLAN_API_ERR_VLAN_NOT_ACTIVE;
        return VLAN_FAILURE;
    }

    pFidEntry = VLAN_GET_FID_ENTRY (pVlanRec->u4FidIndex);
    /* The Fdb Id should have been allocated to a VLAN */

    pStUcastEntry = pFidEntry->pStUcastTbl;

    while (pStUcastEntry != NULL)
    {
        pNextStUcastEntry = pStUcastEntry->pNextNode;

        if (pStUcastEntry->u1Status == VLAN_OTHER)
        {
            /* This function performs all dependent steps
             * before deleting a Static Ucast entry
             * except deleting the entry itself. */
            if (VlanHandleStUcastDeletion (pVlanRec, pStUcastEntry)
                == VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                          "Deletion of static unicast "
                          "entry for this vlan Failed\n");
            }

            /* Calling VlanDeleteStUcastEntry directly is inefficient
             * since it involves a scan to find the previous node in SLL. 
             * Since we know the Prev node here we can directly delete
             * the Node. */

            /* Rearrange the SLL nodes */
            if (pPrevStUcastEntry == NULL)
            {
                pFidEntry->pStUcastTbl = pStUcastEntry->pNextNode;
            }
            else
            {
                pPrevStUcastEntry->pNextNode = pStUcastEntry->pNextNode;
            }

            if (pStUcastEntry != NULL)
            {
                RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->VlanStaticUnicastInfo,
                              (tRBElem *) pStUcastEntry);
            }

            /* Free the memory allocated for this entry */
            VLAN_RELEASE_BUF (VLAN_ST_UCAST_ENTRY, (UINT1 *) pStUcastEntry);
            VlanUpdateCurrentStaticUcastMacCount (VLAN_DELETE);
        }

        pPrevStUcastEntry = pStUcastEntry;
        pStUcastEntry = pNextStUcastEntry;
    }

    pStMcastEntry = pVlanRec->pStMcastTable;

    while (pStMcastEntry != NULL)
    {
        pNextStMcastEntry = pStMcastEntry->pNextNode;

        if (pStMcastEntry->u1Status == VLAN_OTHER)
        {
            /* This function performs all dependent steps
             * before deleting a Static Mcast entry
             * except deleting the entry itself. */
            VlanHandleStMcastDeletion (pVlanRec, pStMcastEntry);

            /* Calling VlanDeleteStMcastEntry directly is inefficient
             * since it involves a scan to find the previous node in SLL. 
             * Since we know the Prev node here we can directly delete
             * the Node. */

            /* Rearrange the SLL nodes */
            if (pPrevStMcastEntry == NULL)
            {
                pVlanRec->pStMcastTable = pNextStMcastEntry;
            }
            else
            {
                pPrevStMcastEntry->pNextNode = pStMcastEntry->pNextNode;
            }

            /* Free the memory allocated for this entry */
            VLAN_RELEASE_BUF (VLAN_ST_MCAST_ENTRY, (UINT1 *) pStMcastEntry);
        }

        pPrevStMcastEntry = pStMcastEntry;
        pStMcastEntry = pNextStMcastEntry;
    }

    VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME, "Deletion of static unicast "
              "entry for this vlan Success\n");

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanSetVlanFloodingStatus                            */
/*                                                                           */
/* Description        : This API is called from PBB-TE to enable or disable  */
/*                      control plane flooding, of unknown unicast/multicast */
/*                      frames that are trapped to the software, on all Vlan */
/*                      member ports. It also disables wild-card Vlan entry  */
/*                      look-up in control-plane for all frames that are     */
/*                      classified to the given Vlan. This is called when    */
/*                      the Vlan is mapped/unmapped to the ESP-VID.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId - Vlan Index                                */
/*                      u1Status - Flag to Disable/Enable the flooding of    */
/*                      the given vlan                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanSetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId, UINT1 u1Status)
{
    tVlanCurrEntry     *pVlanRec = NULL;

    if (u4ContextId >= VLAN_MAX_CONTEXT)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanRec = VlanGetVlanEntry (u2VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                       "Vlan %d Is Not Active\n", u2VlanId);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_FAILURE;
    }

    if (VlanL2IwfSetVlanFloodingStatus (VLAN_CURR_CONTEXT_ID (), u2VlanId,
                                        u1Status) == L2IWF_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                       "Unable to set flooding status in Vlan %d\n", u2VlanId);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanReleaseContext ();

    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPortStats                                 */
/*                                                                           */
/*    Description         : This function is called by ECFM to get the       */
/*                          transmit and receive data counters for the port  */
/*                          and vlan                                         */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Number                          */
/*                          VlanId - Vlan ID for which stats are required    */
/*                                                                           */
/*    Output(s)           : pu4TxFCl - Value of Transmission counter         */
/*                          pu4RxFCl - Value of Reception counter            */
/*                                                                           */
/*    Returns            : NONE                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetPortStats (UINT4 u4IfIndex, tVlanId VlanId,
                  UINT4 *pu4TxFCl, UINT4 *pu4RxFCl)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();
    *pu4TxFCl = 0;
    *pu4RxFCl = 0;
    if (VlanGetContextInfoFromIfIndex ((UINT2) u4IfIndex, &u4ContextId,
                                       &u2LocalPort) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }
    /* Get Port Stats for In Frames and Out Frames */
    VlanGetStats (u2LocalPort, VlanId, VLAN_STAT_VLAN_PORT_OUT_FRAMES,
                  pu4TxFCl);
    VlanGetStats (u2LocalPort, VlanId, VLAN_STAT_VLAN_PORT_IN_FRAMES, pu4RxFCl);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanValidateVlanId                                 */
/*                                                                           */
/*    Description         : This function Validates the VlanId                 */
/*                                                                           */
/*    Input(s)            : u4VlanId - Vlan Idto be validated                 */
/*                          : u4ContextId - Context Id                         */
/*                                                                             */
/*    Output(s)           :                                                     */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_SUCCESS -                             */
/*                                                                           */
/*****************************************************************************/

INT4
VlanValidateVlanId (UINT4 u4ContextId, UINT4 u4VlanId)
{
    INT1                i1RetVal = VLAN_SUCCESS;
    tStaticVlanEntry   *pStVlanEntry = NULL;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanId);

    if (pStVlanEntry == NULL)
    {
        i1RetVal = VLAN_FAILURE;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return i1RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanIsDeiBitSet                                     */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Dei Bit ise set TRUE or FALSE on the Port.           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Actual Port Number                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
VlanIsDeiBitSet (UINT4 u4IfIndex)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortNum = 0;

    VLAN_LOCK ();

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return VLAN_FAILURE;
    }
    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortNum) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VLAN_IS_PORT_VALID (u2LocalPortNum) == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortNum);

    if (pVlanPortEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    bResult = pVlanPbPortEntry->u1UseDei;

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return bResult;
}

/*****************************************************************************/
/* Function Name      : VlanTagOutFrame                                      */
/*                                                                           */
/* Description        : This routine is called to add/delete/update the      */
/*                      VLAN TAG in the frame as per the current VLAN rules. */
/*                      Dei Bit ise set TRUE or FALSE on the Port.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier                     */
/*                      u2PortNum - local port number                        */
/*                      pBuf - Pointer to the CRU buffer.                    */
/*                      pVlanTagInfo - VLAN tag information of the frame     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanTagOutFrame (UINT4 u4ContextId, UINT2 u2PortNum,
                 tCRU_BUF_CHAIN_HEADER * pBuf, tVlanTagInfo * pVlanTagInfo)
{
    tVlanTag            VlanTag;
    tVlanIfMsg          VlanIf;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1               u1TagOut = VLAN_TRUE;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_MEMSET (&VlanIf, 0, sizeof (tVlanIfMsg));

    if (pVlanTagInfo->u2VlanId == 0)
    {
        /* If we dont have VLAN information in the input also, PDU
         * originating from PIP we need to send the PDU untagged*/
        return VLAN_SUCCESS;
    }
    MEMSET (&VlanTag, 0, sizeof (tVlanTag));
    MEMCPY (&(VlanTag.OuterVlanTag), pVlanTagInfo, sizeof (tVlanTagInfo));
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    /* Check whether the u2PortNum is a member of UnTagPorts if
     * a static vlan entry is present for the VLAN
     */
    pVlanEntry = VlanGetVlanEntry (VlanTag.OuterVlanTag.u2VlanId);
    if (pVlanEntry != NULL)
    {
        if (pVlanEntry->pStVlanEntry != NULL)
        {

            VLAN_IS_UNTAGGED_PORT (pVlanEntry->pStVlanEntry,
                                   u2PortNum, u1Result);

            if (u1Result == VLAN_TRUE)
            {

                u1TagOut = VLAN_FALSE;
            }
        }
    }
    else
    {
        /* No Vlan Tag information found */
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    VlanIf.u2Length = CRU_BUF_Get_ChainValidByteCount (pBuf);
    /* Add/Updated/Delete the vlan tag as required. */
    if (VlanHandleTagOutFrame (pBuf, u2PortNum, &VlanIf, u1TagOut, &VlanTag)
        != VLAN_FORWARD)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    MEMCPY (pVlanTagInfo, &(VlanTag.OuterVlanTag), sizeof (tVlanTagInfo));
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanTagInfo                               */
/*                                                                           */
/*    Description         : This function used to get the VlanId in the      */
/*                          ingress packet.                                  */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pVlanId  - Vlan Associated with the packet.      */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetVlanTagInfo (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4InPort,
                    tVlanId * pVlanId)
{
    UINT1               au1Buf[VLAN_TAGGED_PROTOCOL_HEADER_OFFSET];
    tVlanPortEntry     *pPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2EtherType;
    UINT2               u2VlanTag;
    UINT1               u1FrameType;
    UINT1              *pu1Data;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4InPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    *pVlanId = 0;

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /* Identification of EtherType from the ingress frame */
    pu1Data = VLAN_BUF_IF_LINEAR (pFrame, 0,
                                  VLAN_TAGGED_PROTOCOL_HEADER_OFFSET);

    if (pu1Data == NULL)
    {
        VLAN_COPY_FROM_BUF (pFrame, au1Buf, 0,
                            VLAN_TAGGED_PROTOCOL_HEADER_OFFSET);
        pu1Data = au1Buf;
    }

    MEMCPY ((UINT1 *) &u2EtherType, &pu1Data[VLAN_TAG_OFFSET],
            VLAN_TYPE_OR_LEN_SIZE);

    u2EtherType = OSIX_NTOHS (u2EtherType);

    u1FrameType = (UINT1) VlanClassifyFrame (pFrame, u2EtherType, u2LocalPort);

    if (u1FrameType == VLAN_TAGGED)
    {
        VLAN_COPY_FROM_BUF (pFrame, &u2VlanTag, VLAN_TAG_VLANID_OFFSET,
                            VLAN_TAG_SIZE);
        u2VlanTag = OSIX_NTOHS (u2VlanTag);

        *pVlanId = (tVlanId) (u2VlanTag & VLAN_ID_MASK);

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;

    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : VlanHwAddFdbEntry                                */
/*    Description         : This function adds a FDB entry to the HW Table   */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*                          u1EntryType - Type of entry to be added          */
/*                          entry takes values like permanent, deleteOnReset */
/*                          deleteOnTimeout                                  */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanHwAddFdbEntry (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                   UINT1 u1EntryType)
{
    UINT1              *pAllowedToGoPortList = NULL;
    tMacAddr            ConnectionId;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();
    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        /* Vlan Task is not initialised  */
        return;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        if (VlanId != VLAN_WILD_CARD_ID)
        {
            VLAN_UNLOCK ();
            return;
        }
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "SelectContext FAILED for VlanHwAddFdbEntry\n");
        VLAN_UNLOCK ();
        return;
    }
    if (L2IwfMiIsVlanActive (u4ContextId, VlanId) == OSIX_FALSE)
    {
        if (VlanId != VLAN_WILD_CARD_ID)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Vlan is not active in the context %d\n",
                           u4ContextId);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return;
        }
    }
    pAllowedToGoPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAllowedToGoPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHwAddFdbEntry: Error in allocating memory "
                  "for pAllowedToGoPortList\r\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    MEMSET (pAllowedToGoPortList, 0, sizeof (tLocalPortList));
    MEMSET (ConnectionId, 0, sizeof (ConnectionId));

    /* Program the hardware */
    VLAN_SET_MEMBER_PORT (pAllowedToGoPortList, u2LocalPort);

    if (VlanHwAddStaticUcastEntry (VlanId,
                                   MacAddr, 0,
                                   pAllowedToGoPortList,
                                   u1EntryType, ConnectionId) == VLAN_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pAllowedToGoPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }
    UtilPlstReleaseLocalPortList (pAllowedToGoPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDelFdbEntry                                */
/*    Description         : This function deletes a FDB entry to the HW Table*/
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanHwDelFdbEntry (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();
    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        /* Vlan Task is not initialised  */
        return;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        if (VlanId != VLAN_WILD_CARD_ID)
        {
            VLAN_UNLOCK ();
            return;
        }
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "SelectContext FAILED for VlanHwAddFdbEntry\n");
        VLAN_UNLOCK ();
        return;
    }
    if (L2IwfMiIsVlanActive (u4ContextId, VlanId) == OSIX_FALSE)
    {
        if (VlanId != VLAN_WILD_CARD_ID)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Vlan is not active in the context %d\n",
                           u4ContextId);
            VLAN_UNLOCK ();
            return;
        }
    }

    if (VlanHwDelStaticUcastEntry (VlanId, MacAddr, 0) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }

    VLAN_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : VlanProcessRestorationComplete                       */
/*                                                                           */
/* Description        : This routine notifies MSR complete event to VLAN     */
/*                      task                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanProcessRestorationComplete ()
{
    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_MSR_COMPLETE_EVENT);
}

/*****************************************************************************/
/*    Function Name       : VlanApiForwardOnPorts                            */
/*                                                                           */
/*    Description         : This function is invoked by IGS for forwarding   */
/*                          the mcast packets received on a port to a list of*/
/*                          ports for a given instance.                      */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pBuf - Packet to be forwarded.                   */
/*                          pVlanFwdInfo- Forwarding information needed by   */
/*                                        VLAN to transmit the packet.       */
/*                                                                           */
/*    Output(s)           :  None                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanApiForwardOnPorts (tCRU_BUF_CHAIN_DESC * pBuf, tVlanFwdInfo * pVlanFwdInfo)
{
    UINT1              *pLocalFwdPortList = NULL;
    UINT1              *pTmpPortList = NULL;
    tVlanCurrEntry     *pVlanEntry;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanGroupEntry    *pGroupEntry;
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tLocalPortList     *pInLocalPortList = NULL;
    tVlanIfMsg          VlanIf;
    tVlanTag            InVlanTag;
    tVlanId             CVlanId = 0;
    UINT4               u4TempContextId;
    UINT4               u4TblIndex = 0;
    tMacAddr            SrcMacAddr;
    UINT2               u2LocalOutPort = 0;
    UINT2               u2LocalInPort = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitInd = 0;
    UINT1               u1VlanFlag = 0;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1IsOutPortCEP = OSIX_FALSE;

    VLAN_MEMSET (&VlanIf, 0, sizeof (tVlanIfMsg));
    VLAN_MEMSET (&InVlanTag, 0, sizeof (tVlanTag));
    VLAN_MEMSET (SrcMacAddr, 0, sizeof (tMacAddr));

    /* NOTE: This function shall be extended for forwarding Unicast packet. Currently
     *      * it supports  multicast frame forwarding and it is called only by IGS */

    VLAN_LOCK ();

    if (VlanSelectContext (pVlanFwdInfo->u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_RELEASE_CRU_BUF (pBuf);
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (pVlanFwdInfo->u4ContextId) ==
        VLAN_DISABLED)
    {
        VLAN_RELEASE_CRU_BUF (pBuf);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (pVlanFwdInfo->u4InPort != VLAN_INVALID_PORT)
    {
        if (VlanGetContextInfoFromIfIndex
            (pVlanFwdInfo->u4InPort, &u4TempContextId,
             &u2LocalInPort) != VLAN_SUCCESS)
        {
            VLAN_RELEASE_CRU_BUF (pBuf);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_NO_FORWARD;
        }
    }

    pVlanEntry = VlanGetVlanEntry (pVlanFwdInfo->VlanTag.OuterVlanTag.u2VlanId);

    if (pVlanEntry == NULL)
    {
        VLAN_RELEASE_CRU_BUF (pBuf);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (pVlanFwdInfo->u1IsPortListLocal == OSIX_FALSE)
    {
        VLAN_MEMSET (gVlanLocalPortList, 0, sizeof (tLocalPortList));

        if (VlanConvertToLocalPortList
            (pVlanFwdInfo->pPortList, gVlanLocalPortList) != VLAN_SUCCESS)
        {
            VLAN_RELEASE_CRU_BUF (pBuf);
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_NO_FORWARD;
        }
        pVlanFwdInfo->pPortList = gVlanLocalPortList;
    }

    pInLocalPortList = (tLocalPortList *) pVlanFwdInfo->pPortList;
    pLocalFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalFwdPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanApiForwardOnPorts: Error in allocating memory "
                  "for pLocalFwdPortList\r\n");
        VLAN_RELEASE_CRU_BUF (pBuf);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    VLAN_MEMSET (pLocalFwdPortList, 0, sizeof (tLocalPortList));

    if (pVlanFwdInfo->u1Action == VLAN_FORWARD_ALL)
    {
        {
            /*
             *              * Received packet is being forwarded based on
             *                           * Multicast forwarding database
             *                                        */
            pGroupEntry =
                VlanGetGroupEntry (pVlanFwdInfo->DestAddr, pVlanEntry);

            if (pGroupEntry != NULL)
            {
                if (MEMCMP (pGroupEntry->LearntPorts,
                            gNullPortList, sizeof (tLocalPortList)) == 0)
                {
#ifdef GARP_EXTENDED_FILTER
                    /* Use unregister group ports if no dynamic info is present */
                    MEMCPY (pLocalFwdPortList, pVlanEntry->UnRegGrps.Ports,
                            sizeof (tLocalPortList));
#endif
                }
                else
                {
                    MEMCPY (pLocalFwdPortList, pGroupEntry->LearntPorts,
                            sizeof (tLocalPortList));
                }

                if (pGroupEntry->u1StRefCount != 0)
                {
                    /* some static entry exist for this mcast address */
                    pStMcastEntry =
                        VlanGetStMcastEntry (pVlanFwdInfo->DestAddr,
                                             u2LocalInPort, pVlanEntry);
                }
            }
            else
            {
#ifdef GARP_EXTENDED_FILTER
                MEMCPY (pLocalFwdPortList, pVlanEntry->UnRegGrps.Ports,
                        sizeof (tLocalPortList));
#else
                VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanEntry,
                                                    pLocalFwdPortList);
#endif
            }

#ifdef GARP_EXTENDED_FILTER
            VLAN_ADD_PORT_LIST (pLocalFwdPortList, pVlanEntry->AllGrps.Ports);
#endif
            if (pStMcastEntry != NULL)
            {
                VLAN_ADD_PORT_LIST (pLocalFwdPortList,
                                    pStMcastEntry->EgressPorts);
                VLAN_RESET_PORT_LIST (pLocalFwdPortList,
                                      pStMcastEntry->ForbiddenPorts);
            }
        }
    }
    else
    {
        /*
         *          * pVlanFwdInfo->u1Action == VLAN_FORWARD_SPECIFIC.
         *                   * Needs to transmit on the ports specified by IGS
         *                            */
        VLAN_MEMCPY (pLocalFwdPortList, pInLocalPortList,
                     sizeof (tLocalPortList));
    }

    if (pVlanFwdInfo->u4InPort != VLAN_INVALID_PORT)
    {
        VLAN_RESET_MEMBER_PORT (pLocalFwdPortList, u2LocalInPort);
    }

    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_REMOVE_FORBIDDEN_PORTS_FROM_PORTLIST (pVlanEntry->pStVlanEntry,
                                                   pLocalFwdPortList);
    }

    /* Filling up VlanIf Message details required for VLAN processing */
    MEMCPY (VlanIf.DestAddr, pVlanFwdInfo->DestAddr, VLAN_MAC_ADDR_LEN);
    VlanIf.u4ContextId = pVlanFwdInfo->u4ContextId;
    VlanIf.SVlanId = pVlanFwdInfo->VlanTag.OuterVlanTag.u2VlanId;
    VlanIf.u2Length = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (pVlanFwdInfo->u4InPort != VLAN_INVALID_PORT)
    {
        VlanIf.u4IfIndex = pVlanFwdInfo->u4InPort;
    }
    else
    {
        /* If the packet is generated by our own stack, it should be of highest priority */
        VlanIf.u1Priority = pVlanFwdInfo->VlanTag.OuterVlanTag.u1Priority;
    }

    VlanIf.u2LocalPort = u2LocalInPort;
    VLAN_GET_TIMESTAMP (&VlanIf.u4TimeStamp);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalInPort);

    /*Copying vlan tag information */
    InVlanTag = pVlanFwdInfo->VlanTag;
    if (pVlanPbPortEntry != NULL)
    {

        if ((VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
            (pVlanFwdInfo->u4InPort != VLAN_INVALID_PORT) &&
            (pVlanFwdInfo->VlanTag.InnerVlanTag.u2VlanId == VLAN_NULL_VLAN_ID)
            && (pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT))
        {
            InVlanTag.InnerVlanTag.u2VlanId = pVlanPbPortEntry->CVlanId;
        }

    }
    /* Try to send the packet through the NP, if NP fails then we need to loop
     *      * in VLAN*/
    pTmpPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pTmpPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanApiForwardOnPorts: Error in allocating memory "
                  "for pTmpPortList\r\n");
        UtilPlstReleaseLocalPortList (pLocalFwdPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pTmpPortList, 0, sizeof (tLocalPortList));
    VLAN_MEMCPY (pTmpPortList, pLocalFwdPortList, sizeof (tLocalPortList));

    if (VlanHwForwardOnPortList (pVlanFwdInfo->u4ContextId,
                                 &InVlanTag, pBuf,
                                 pTmpPortList) == VLAN_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pTmpPortList);
        UtilPlstReleaseLocalPortList (pLocalFwdPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }
    UtilPlstReleaseLocalPortList (pTmpPortList);

    /* If the inport is CEP, the packet has to go through the VLAN processing to add or remove the ctag */
    if (pVlanPbPortEntry != NULL)
    {
        if ((VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
            (pVlanFwdInfo->u4InPort != VLAN_INVALID_PORT) &&
            (pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbHandlePktRxOnCep (pBuf, u2LocalInPort, &InVlanTag) ==
                VLAN_FAILURE)
            {
                UtilPlstReleaseLocalPortList (pLocalFwdPortList);
                VLAN_RELEASE_CRU_BUF (pBuf);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
        }
    }

    VLAN_SCAN_PORT_TABLE (u2LocalOutPort)
    {
        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalOutPort);

        if (VlanEgressFiltering
            (u2LocalOutPort, pVlanEntry,
             pVlanFwdInfo->DestAddr) == VLAN_FORWARD)
        {
            VLAN_IS_MEMBER_PORT (pLocalFwdPortList, u2LocalOutPort, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if (pVlanPbPortEntry != NULL)
                {
                    if ((VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
                        (pVlanPbPortEntry->u1PbPortType ==
                         VLAN_CUSTOMER_EDGE_PORT)
                        && (pVlanFwdInfo->u1SendOnAllCVlans == OSIX_TRUE)
                        && (pVlanFwdInfo->VlanTag.InnerVlanTag.u2VlanId ==
                            VLAN_NULL_VLAN_ID))
                    {
                        /* This is a special case required for IGS to transmit packet on all CVLANs.
                         *                  * OutPort is CEP, And the packet is untagged, then we have to transmit on all cvlans of the CEP
                         *                                   * port */
                        u1IsOutPortCEP = OSIX_TRUE;
                        continue;
                    }
                }
                pDupBuf = VlanDupBuf (pBuf);

                if (pDupBuf == NULL)
                {
                    UtilPlstReleaseLocalPortList (pLocalFwdPortList);
                    VLAN_RELEASE_CRU_BUF (pBuf);
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return VLAN_NO_FORWARD;
                }

                if (VlanGetSrcMacAddress
                    (VLAN_GET_PHY_PORT (u2LocalOutPort), &InVlanTag,
                     SrcMacAddr) == VLAN_SUCCESS)
                {
                    CRU_BUF_Copy_OverBufChain (pDupBuf, SrcMacAddr,
                                               VLAN_MAC_ADDR_LEN,
                                               VLAN_MAC_ADDR_LEN);
                    VLAN_MEMSET (SrcMacAddr, 0, VLAN_MAC_ADDR_LEN);
                }

                VlanFwdOnSinglePort (pDupBuf, &VlanIf, u2LocalOutPort,
                                     pVlanEntry, &InVlanTag,
                                     pVlanFwdInfo->u1FrameType);
            }
        }
    }

    if (u1IsOutPortCEP == OSIX_FALSE)
    {
        UtilPlstReleaseLocalPortList (pLocalFwdPortList);
        /* There is no out port which consists of a CEP, So, return here */
        VLAN_RELEASE_CRU_BUF (pBuf);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FORWARD;
    }

    /* LocalFwdPortList consists atleast one outgoing CEP port */
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    if (VLAN_IS_PORT_VALID (u2LocalOutPort) == VLAN_FALSE)
    {
        UtilPlstReleaseLocalPortList (pLocalFwdPortList);
        VLAN_RELEASE_CRU_BUF (pBuf);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    VLAN_SCAN_PORT_TABLE (u2LocalOutPort)
    {
        if (VlanEgressFiltering
            (u2LocalOutPort, pVlanEntry,
             pVlanFwdInfo->DestAddr) == VLAN_FORWARD)
        {
            VLAN_IS_MEMBER_PORT (pLocalFwdPortList, u2LocalOutPort, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if (pVlanPbPortEntry != NULL)
                {
                    if ((VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
                        (pVlanPbPortEntry->u1PbPortType !=
                         VLAN_CUSTOMER_EDGE_PORT))
                    {
                        /* If the outport is not cep then continue, */
                        continue;
                    }
                }

                MEMSET (&gVlanPbCVlanInfo, 0, sizeof (tCVlanInfo));

                /* Getting CVAN-list to transmit the packet on all CVLANs */
                VlanGetCVlansFromCVlanSVlanTable (u4TblIndex, u2LocalOutPort,
                                                  pVlanFwdInfo->VlanTag.
                                                  OuterVlanTag.u2VlanId,
                                                  &gVlanPbCVlanInfo);
                if (VLAN_MEMCMP
                    (gVlanPbCVlanInfo.TaggedCVlanList, gVlanNullVlanList,
                     sizeof (tVlanList)) != 0)
                {
                    for (u2ByteInd = 0; u2ByteInd < VLAN_DEV_VLAN_LIST_SIZE;
                         u2ByteInd++)
                    {
                        u1VlanFlag =
                            gVlanPbCVlanInfo.TaggedCVlanList[u2ByteInd];

                        for (u2BitInd = 0; ((u2BitInd < VLAN_PORTS_PER_BYTE)
                                            && (u1VlanFlag != 0)); u2BitInd++)
                        {
                            if ((u1VlanFlag & VLAN_BIT8) != 0)
                            {
                                CVlanId =
                                    (tVlanId) ((u2ByteInd *
                                                VLAN_PORTS_PER_BYTE) +
                                               u2BitInd + 1);

                                pDupBuf = VlanDupBuf (pBuf);

                                if (pDupBuf != NULL)
                                {
                                    InVlanTag.InnerVlanTag.u2VlanId = CVlanId;
                                    InVlanTag.InnerVlanTag.u1TagType =
                                        VLAN_TAGGED;
                                    InVlanTag.InnerVlanTag.u1Priority =
                                        VLAN_HIGHEST_PRIORITY;
                                    InVlanTag.InnerVlanTag.u1DropEligible = 0;

                                    /* Adding Ctag frame and sending the packet out on CEP for each CVLAN configured for this
                                     *                                  * outgoing CEP,SVLAN */

                                    VlanAddTagToFrame (pDupBuf, CVlanId,
                                                       VLAN_HIGHEST_PRIORITY, 0,
                                                       VLAN_GET_PORT_ENTRY
                                                       (u2LocalOutPort));

                                    VlanIf.u2Length =
                                        CRU_BUF_Get_ChainValidByteCount
                                        (pDupBuf);

                                    if (VlanGetSrcMacAddress
                                        (VLAN_GET_PHY_PORT (u2LocalOutPort),
                                         &InVlanTag,
                                         SrcMacAddr) == VLAN_SUCCESS)
                                    {
                                        CRU_BUF_Copy_OverBufChain (pDupBuf,
                                                                   SrcMacAddr,
                                                                   VLAN_MAC_ADDR_LEN,
                                                                   VLAN_MAC_ADDR_LEN);
                                        VLAN_MEMSET (SrcMacAddr, 0,
                                                     VLAN_MAC_ADDR_LEN);
                                    }
                                    VlanFwdOnSinglePort (pDupBuf, &VlanIf,
                                                         u2LocalOutPort,
                                                         pVlanEntry, &InVlanTag,
                                                         pVlanFwdInfo->
                                                         u1FrameType);

                                }
                            }
                            u1VlanFlag = (UINT1) (u1VlanFlag << 1);
                        }
                    }
                }

                if (VLAN_MEMCMP
                    (gVlanPbCVlanInfo.UntaggedCVlanList, gVlanNullVlanList,
                     sizeof (tVlanList)) != 0)
                {
                    for (u2ByteInd = 0; u2ByteInd < VLAN_DEV_VLAN_LIST_SIZE;
                         u2ByteInd++)
                    {
                        u1VlanFlag =
                            gVlanPbCVlanInfo.UntaggedCVlanList[u2ByteInd];

                        for (u2BitInd = 0; ((u2BitInd < VLAN_PORTS_PER_BYTE)
                                            && (u1VlanFlag != 0)); u2BitInd++)
                        {
                            if ((u1VlanFlag & VLAN_BIT8) != 0)
                            {
                                CVlanId =
                                    (tVlanId) ((u2ByteInd *
                                                VLAN_PORTS_PER_BYTE) +
                                               u2BitInd + 1);

                                pDupBuf = VlanDupBuf (pBuf);

                                if (pDupBuf != NULL)
                                {
                                    InVlanTag.InnerVlanTag.u2VlanId = CVlanId;
                                    InVlanTag.InnerVlanTag.u1TagType =
                                        VLAN_UNTAGGED;
                                    InVlanTag.InnerVlanTag.u1Priority = 0;
                                    InVlanTag.InnerVlanTag.u1DropEligible = 0;

                                    VlanIf.u2Length =
                                        CRU_BUF_Get_ChainValidByteCount
                                        (pDupBuf);

                                    /* Enough to send one untagged packet on the CEP, for all the CVLANs */
                                    if (VlanGetSrcMacAddress
                                        (VLAN_GET_PHY_PORT (u2LocalOutPort),
                                         &InVlanTag,
                                         SrcMacAddr) == VLAN_SUCCESS)
                                    {
                                        CRU_BUF_Copy_OverBufChain (pDupBuf,
                                                                   SrcMacAddr,
                                                                   VLAN_MAC_ADDR_LEN,
                                                                   VLAN_MAC_ADDR_LEN);
                                        VLAN_MEMSET (SrcMacAddr, 0,
                                                     VLAN_MAC_ADDR_LEN);
                                    }

                                    VlanFwdOnSinglePort (pDupBuf, &VlanIf,
                                                         u2LocalOutPort,
                                                         pVlanEntry, &InVlanTag,
                                                         pVlanFwdInfo->
                                                         u1FrameType);
                                    break;

                                }
                            }
                            u1VlanFlag = (UINT1) (u1VlanFlag << 1);
                        }
                    }
                }
            }
        }
    }

    /* The pDupBuf will be released after transmitted, Hence release only pBuf here */

    UtilPlstReleaseLocalPortList (pLocalFwdPortList);
    VLAN_RELEASE_CRU_BUF (pBuf);
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIdentifyVlanId                               */
/*                                                                           */
/*    Description         : This function is called by IVR to get the VlanId */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pVlanId  - Gip Associated with the packet.       */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIdentifyVlanId (tCRU_BUF_CHAIN_DESC * pFrame,
                    UINT4 u4InPort, tVlanId * pVlanId)
{
    tMacAddr            DestAddr;
    tMacAddr            SrcAddr;
    tVlanId             VlanId;
    tVlanTag            VlanTag;
    tVlanCurrEntry     *pVlanRec;
    tVlanPortEntry     *pPortEntry;
    INT4                i4Result;
    INT4                i4IsFrameDiscarded = VLAN_FALSE;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1PortState;
    UINT1               u1Result = VLAN_FALSE;
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan;
#endif

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4InPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    *pVlanId = 0;

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanL2IwfIncrFilterInDiscards (u2LocalPort);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Frame received on Invalid Port \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC,
                  VLAN_NAME, "Port Oper Status DOWN. Frame received\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    VLAN_COPY_FROM_BUF (pFrame, &DestAddr, 0, ETHERNET_ADDR_SIZE);
    VLAN_COPY_FROM_BUF (pFrame, &SrcAddr, ETHERNET_ADDR_SIZE,
                        ETHERNET_ADDR_SIZE);

    VlanId = VlanGetVlanId (pFrame, SrcAddr, u2LocalPort, &VlanTag);

    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "VLAN ID 0x%x associated with the Frame "
                       "is unknown \n", VlanId);

        VlanL2IwfIncrFilterInDiscards (u2LocalPort);

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    i4Result = VlanIngressFiltering (pVlanRec, u2LocalPort);

    if (i4Result == VLAN_NO_FORWARD)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Frame received on port %d is discarded due"
                       " to Ingress Filtering.\n", u4InPort);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

            if (NULL != pPortInfoPerVlan)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

    u1PortState = VlanL2IwfGetVlanPortState (VlanId, u4InPort);

    VLAN_IS_CURR_EGRESS_PORT (pVlanRec, u2LocalPort, u1Result);

    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        i4IsFrameDiscarded = VLAN_TRUE;
    }

    if (u1Result == VLAN_FALSE)
    {
        i4IsFrameDiscarded = VLAN_TRUE;
    }

    if (i4IsFrameDiscarded == VLAN_TRUE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Frame received on port %d is discarded ", u4InPort);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

            if (NULL != pPortInfoPerVlan)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return VLAN_NO_FORWARD;
    }

#ifndef NPAPI_WANTED

    if (u1Result == VLAN_TRUE)
    {
        VlanLearn (SrcAddr, u2LocalPort, pVlanRec);
    }

    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2LocalPort);

        if (NULL != pPortInfoPerVlan)
        {
            VLAN_INC_IN_FRAMES (pPortInfoPerVlan);
        }
    }
#endif

    *pVlanId = VlanId;

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*    Function Name       : VlanIvrGetTxPortOrPortList                       */
/*                                                                           */
/*    Description         : This function is called when ever IP wants to Tx */
/*                          packet on an logical vlan interface. This fuction*/
/*                          returns the physical ports for trransmission.    */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          VlanId   - Vlan ID                               */
/*                          u1BcastFlag - Flag to indicate whether the pkt   */
/*                                        should be broadcast or not         */
/*                                                                           */
/*    Output(s)           : pu2OutPort    - Out Port for Tx if known Ucast.  */
/*                        : pbIsTag       - Pkt should be tagged / Untagged  */
/*                        : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanIvrGetTxPortOrPortList (tMacAddr DestAddr, tVlanId VlanId,
                            UINT1 u1BcastFlag, UINT4 *pu4OutPort,
                            BOOL1 * pbIsTag, tPortList TagPortList,
                            tPortList UntagPortList)
{
    UINT1              *pFwdPortList = NULL;
    UINT1              *pLocalTagPortList = NULL;
    UINT1              *pLocalUntagPortList = NULL;
    tVlanCurrEntry     *pVlanRec;
    tVlanGroupEntry    *pGroupEntry;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanFidEntry      *pFidEntry;
    tVlanTempFdbInfo    VlanTempFdbInfo;
    INT4                i4RetVal;
    UINT2               u2OutPort;
    UINT1               u1Result;
    UINT1               u1IsMemberPort = VLAN_FALSE;
    UINT1               u1IsUnTaggedPort = VLAN_FALSE;
    UINT1               u1TagOut = VLAN_TRUE;
    UINT4               u4PhyPort = 0;

    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    if (VLAN_IS_VLAN_ENABLED () == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    *pu4OutPort = VLAN_INVALID_PORT;

    *pbIsTag = VLAN_FALSE;

    /* IVR - VLAN will not know about VLAN interfaces. So when a packet is 
     * received on a VLAN interface, port validity need not be checked.
     * and always the packet Tx From IVR is Untagged 
     */
    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "IVR: VLAN 0x%x associated with the packet is"
                       "unknown \n", VlanId);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    pFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pFwdPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleStMcastDeletion: Error in allocating memory "
                  "for pFwdPortList\r\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }

    VLAN_MEMSET (pFwdPortList, 0, sizeof (tLocalPortList));

    if (VLAN_IS_MCASTADDR (DestAddr) == VLAN_TRUE)
    {
        /* Given Packet is Multicast / Broadcast  Packet */
        /* When the incoming interface is a VLAN interface, no need to check
         * for the port validity.
         */

        pGroupEntry = VlanGetGroupEntry (DestAddr, pVlanRec);

        if (pGroupEntry != NULL)
        {
            if (MEMCMP (pGroupEntry->LearntPorts,
                        gNullPortList, sizeof (tLocalPortList)) == 0)
            {
#ifdef GARP_EXTENDED_FILTER
                /* Use unregister group ports if no dynamic info is present */
                MEMCPY (pFwdPortList, pVlanRec->UnRegGrps.Ports,
                        sizeof (tLocalPortList));
#endif
            }
            else
            {
                MEMCPY (pFwdPortList, pGroupEntry->LearntPorts,
                        sizeof (tLocalPortList));
            }

            if (pGroupEntry->u1StRefCount != 0)
            {
                /* some static entry exist for this mcast address */
                pStMcastEntry = VlanGetStMcastEntry (DestAddr, 0, pVlanRec);
            }
        }
        else
        {
#ifdef GARP_EXTENDED_FILTER
            MEMCPY (pFwdPortList, pVlanRec->UnRegGrps.Ports,
                    sizeof (tLocalPortList));
#else
            VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, pFwdPortList);
#endif
        }

#ifdef GARP_EXTENDED_FILTER
        VLAN_ADD_PORT_LIST (pFwdPortList, pVlanRec->AllGrps.Ports);
#endif

        if (pStMcastEntry != NULL)
        {
            VLAN_ADD_PORT_LIST (pFwdPortList, pStMcastEntry->EgressPorts);
            VLAN_RESET_PORT_LIST (pFwdPortList, pStMcastEntry->ForbiddenPorts);
        }
    }
    else
    {
        /* Unicast Packet */
        pFidEntry = VLAN_GET_FID_ENTRY (pVlanRec->u4FidIndex);

        if (VlanGetFdbInfo (pFidEntry->u4Fid, DestAddr,
                            &VlanTempFdbInfo) == VLAN_FAILURE)
        {
            u2OutPort = VLAN_INVALID_PORT;
        }
        else
        {
            u2OutPort = VlanTempFdbInfo.u2Port;
        }

        if (u2OutPort != VLAN_INVALID_PORT)
        {
            /* Dynamic unicast entry is present */
            i4RetVal = VlanEgressFiltering (u2OutPort, pVlanRec, DestAddr);

            if (i4RetVal == VLAN_FORWARD)
            {
                u4PhyPort = VLAN_GET_PHY_PORT (u2OutPort);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "IVR: Unicast Data Packet received to "
                               "be forwarded on Port %d \n", u4PhyPort);
                /* 
                 * Check whether the u2Port is a member of UnTagPorts if
                 * a static vlan entry is present for the VLAN 
                 */

                if (pVlanRec->pStVlanEntry != NULL)
                {
                    VLAN_IS_UNTAGGED_PORT (pVlanRec->pStVlanEntry,
                                           u2OutPort, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        u1TagOut = VLAN_FALSE;
                    }
                }
                *pu4OutPort = (VLAN_GET_PHY_PORT (u2OutPort));
                *pbIsTag = (BOOL1) u1TagOut;
                UtilPlstReleaseLocalPortList (pFwdPortList);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_FORWARD;
            }
            else
            {
                /* Packet Dropped due to Egress Filtering */
                VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                          "IVR: Unicast Data Packet received to "
                          "be dropped due to Egress Filtering.\n");
                UtilPlstReleaseLocalPortList (pFwdPortList);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
        }
        else
        {
            /* 
             * No Dynamic forwarding information present. So
             * flood the frame on all the member ports of the Vlan.
             */

            if (u1BcastFlag == CFA_TRUE)
            {
                VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                          "IVR: Unicast Data Packet received to be "
                          "forwarded on VLAN Egress Ports ");

                VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pFwdPortList);
            }
            else
            {
                /* No Need to flood the packet WGS_WANTED enabled
                 * will be trying to send the packet on next vlan*/
                UtilPlstReleaseLocalPortList (pFwdPortList);
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return VLAN_NO_FORWARD;
            }
        }
    }

    /* Reset the Forbidden Portlist */
    if (pVlanRec->pStVlanEntry != NULL)
    {
        VLAN_REMOVE_FORBIDDEN_PORTS_FROM_PORTLIST (pVlanRec->pStVlanEntry,
                                                   pFwdPortList);
    }
    pLocalTagPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalTagPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleStMcastDeletion: Error in allocating memory "
                  "for pLocalTagPortList\r\n");
        UtilPlstReleaseLocalPortList (pFwdPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    VLAN_MEMSET (pLocalTagPortList, 0, sizeof (tLocalPortList));

    pLocalUntagPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalUntagPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleStMcastDeletion: Error in allocating memory "
                  "for pLocalUntagPortList\r\n");
        UtilPlstReleaseLocalPortList (pFwdPortList);
        UtilPlstReleaseLocalPortList (pLocalTagPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_NO_FORWARD;
    }
    VLAN_MEMSET (pLocalUntagPortList, 0, sizeof (tLocalPortList));

    VLAN_SCAN_PORT_TABLE (u2OutPort)
    {
        u1IsMemberPort = VLAN_FALSE;
        u1IsUnTaggedPort = VLAN_FALSE;

        VLAN_IS_MEMBER_PORT (pFwdPortList, u2OutPort, u1IsMemberPort)
            if (u1IsMemberPort == VLAN_FALSE)
        {
            continue;
        }

        i4RetVal = VlanEgressFiltering (u2OutPort, pVlanRec, DestAddr);

        if (i4RetVal == VLAN_FORWARD)
        {
            if (pVlanRec->pStVlanEntry != NULL)
            {
                VLAN_IS_UNTAGGED_PORT (pVlanRec->pStVlanEntry,
                                       u2OutPort, u1IsUnTaggedPort);
            }
            if (u1IsUnTaggedPort == VLAN_TRUE)
            {
            VLAN_SET_MEMBER_PORT (pLocalUntagPortList, u2OutPort)}
            else
            {
            VLAN_SET_MEMBER_PORT (pLocalTagPortList, u2OutPort)}
        }
    }

    VlanConvertToIfPortList (pLocalTagPortList, TagPortList);
    VlanConvertToIfPortList (pLocalUntagPortList, UntagPortList);

    UtilPlstReleaseLocalPortList (pFwdPortList);
    UtilPlstReleaseLocalPortList (pLocalTagPortList);
    UtilPlstReleaseLocalPortList (pLocalUntagPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiDoEgressFiltering                         */
/*                                                                           */
/*    Description         : This function is used to do egress filtering     */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          VlanId - Vlan Identifier                         */
/*                          DstMacAddr - Destination MAC address             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
VlanApiDoEgressFiltering (UINT4 u4IfIndex, tVlanId VlanId, tMacAddr DstMacAddr)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = VLAN_FORWARD;
    UINT2               u2LocalPort = 0;

    /* Take the VLAN Lock */
    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "VLAN 0x%x & port %d associated with the data packet "
                       "is unknown \r\n", VlanId, u4IfIndex);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    i4RetVal = VlanEgressFiltering (u2LocalPort, pVlanEntry, DstMacAddr);

    if (i4RetVal != VLAN_FORWARD)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Packet dropped in egress filtering for Port %d "
                       "and VLAN %d egress \r\n", u2LocalPort, VlanId);

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFlushFdbTable                                */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and FdbId                    */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u4FdbId - FdbId                                  */
/*                          i4OptimizeFlag - Indicates whether this call can */
/*                                           be grouped with  the flush call */
/*                                           for other ports as a single     */
/*                                           bridge  flush                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFlushFdbTable (tVlanFlushInfo * pVlanFlushInfo)
{
    UINT4               u4ContextId;
#ifndef NPAPI_WANTED
    tVlanQMsg          *pVlanQMsg = NULL;
#endif
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT2               u2LocalPort;

    if (gu1IsVlanInitialised == VLAN_FALSE)
    {
        /* Vlan Task is not initialised  */
        return VLAN_SUCCESS;
    }

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex
        (pVlanFlushInfo->u4IfIndex, &u4ContextId, &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return VLAN_SUCCESS;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();

#ifdef NPAPI_WANTED
    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    i4RetVal = VlanHwFlushFdbList (u4ContextId, pVlanFlushInfo);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
#else

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n",
                         VLAN_FLUSH_FDB_LIST_MSG);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_FLUSH_FDB_LIST_MSG;
    pVlanQMsg->u2LocalPort = u2LocalPort;
    MEMCPY (pVlanQMsg->au1FdbList, pVlanFlushInfo->au1FdbList,
            VLAN_DEV_VLAN_LIST_SIZE);
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u1ModType = pVlanFlushInfo->u1Module;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", VLAN_FLUSH_FDB_LIST_MSG);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    if (VLAN_TASK_ID == VLAN_INIT_VAL)
    {

        if (VLAN_GET_TASK_ID (VLAN_SELF, VLAN_TASK, &(VLAN_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Failed to get Vlan Task Id for "
                             "sending the event \r\n");
            /* No Need to Free Message Sice it is already Queued */
            return VLAN_FAILURE;
        }

    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetForwardUnregPortsFromIgs                  */
/*                                                                           */
/*    Description         : This function is called from IGS to update       */
/*                          the Router Port list of IGS into Vlan Forwarding */
/*                          Database .                                       */
/*    Input(s)            : VlanIndex - Vlan Identifier                      */
/*                          pPortList - Port List                            */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSetForwardUnregPortsFromIgs (UINT4 VlanIndex, tPortList * pPortList)
{
    UINT4               u4ContextId = 0;
    UINT1               au1LocalPorts[VLAN_PORT_LIST_SIZE];
    tSNMP_OCTET_STRING_TYPE PortList;

    VLAN_MEMSET (au1LocalPorts, 0, VLAN_PORT_LIST_SIZE);

    VLAN_LOCK ();

    if (VlanConvertToLocalPortList (*pPortList, au1LocalPorts) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " VlanConvertToLocalPortList () returns failure \r\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    PortList.i4_Length = VLAN_PORT_LIST_SIZE;
    PortList.pu1_OctetList = au1LocalPorts;

    if (VlanSetForwardUnregStaticPorts (VlanIndex, &PortList) == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " VlanSetForwardUnregStaticPorts() returns failure \r\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckWildCardEntry                           */
/*                                                                           */
/*    Description         : This function checks for WildCard entry for      */
/*                          given mac                                        */
/*                                                                           */
/*    Input(s)            : MacAddr - Mac address of wild card entry         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE/VLAN_FALSE                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckWildCardEntry (tMacAddr MacAddr)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;

    VLAN_LOCK ();
    pWildCardEntry = VlanGetWildCardEntry (MacAddr);
    VLAN_UNLOCK ();

    if (pWildCardEntry != NULL)
    {
        return VLAN_FALSE;
    }

    return VLAN_TRUE;
}

/*****************************************************************************/
/*  Function Name   : VlanUtlWalkFnGetFdbByMacAddr                           */
/*  Description     : This function walks through the RB Tree to find the    */
/*                    node with the corresponding MAC address                */
/*  Input(s)        : None                                                   */
/*  Output(s)       : pOut - Node matching the given MAC address             */
/*  Return (s)      : RB_WALK_CONT                                           */
/*****************************************************************************/
INT4
VlanUtlWalkFnGetFdbByMacAddr (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                              void *pArg, void *pOut)
{
    tVlanFdbInfo       *pVlanFdbInfoPtr = NULL;
    tMacAddr            MacAddr;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pOut);

    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMCPY (MacAddr, pArg, VLAN_MAC_ADDR_LEN);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pVlanFdbInfoPtr = (tVlanFdbInfo *) pRBElem;

            if (VLAN_ARE_MAC_ADDR_EQUAL (MacAddr, pVlanFdbInfoPtr->MacAddr)
                == VLAN_TRUE)
            {
#ifdef NPAPI_WANTED
                VlanHwDelStaticUcastEntry (pVlanFdbInfoPtr->u4FdbId,
                                           pVlanFdbInfoPtr->MacAddr,
                                           VLAN_DEF_RECVPORT);
#endif
            }
        }
    }
    return RB_WALK_CONT;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDelFdbEntryByMacAddr                         */
/*                                                                           */
/*    Description         : This function deletes Fdb Entry Information based*/
/*                          on MAC addr input                                */
/*                                                                           */
/*    Input(s)            : MacAddr - Mac Address.                           */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanDelFdbEntryByMacAddr (UINT1 *pu1MacAddr)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    UINT4               u4FidIndex = 0;
    UINT2               u2HashIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry;
    VLAN_LOCK ();

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }

    VlanStartId = VLAN_START_VLAN_INDEX ();
    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if ((pFidEntry == NULL) || (pFidEntry->u4Count == 0))
        {
            continue;
        }

        for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
        {
            pFdbEntry = VLAN_GET_FDB_ENTRY (u4FidIndex, u2HashIndex);

            while (pFdbEntry != NULL)
            {

                pNextFdbEntry = pFdbEntry->pNextHashNode;

                /* Delete the FDB entry for MAC Address learned */
                if (VLAN_ARE_MAC_ADDR_EQUAL (pFdbEntry->MacAddr, pu1MacAddr)
                    == VLAN_TRUE)
                {
#ifdef NPAPI_WANTED
                    VlanHwDelStaticUcastEntry (pFidEntry->u4Fid,
                                               pFdbEntry->MacAddr,
                                               VLAN_DEF_RECVPORT);
#endif
                    if (pFdbEntry->u1StRefCount == 0)
                    {
                        /* No static entries present */
                        if (pFdbEntry->u1Status == VLAN_FDB_LEARNT)
                        {
                            VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
                            pFdbEntry = NULL;
                        }
                    }
                    if (pFdbEntry != NULL)
                    {
                        pFdbEntry->u2Port = VLAN_INVALID_PORT;
                    }

                }

                pFdbEntry = pNextFdbEntry;
            }
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanValidatePortEntry                                */
/*                                                                           */
/* Description        : This routine is check if the Port Entry for the      */
/*                      corresponding Port Index is valid or not.            */
/*                                                                           */
/* Input(s)           : u4PortIndex - Interface Index                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE/ VLAN_FALSE                                */
/*****************************************************************************/

INT4
VlanValidatePortEntry (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == VCM_SUCCESS)
    {
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (VLAN_GET_PORT_ENTRY (u2LocalPortId) != NULL)
            {
                return VLAN_TRUE;
            }
        }
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPvid                                      */
/*                                                                           */
/*    Description         : This Api Gets the pvid of the given port.        */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port IfIndex                         */
/*                                                                           */
/*    Output(s)           : *pu4RetValFsDot1qPvid  - pointe of port PVid     */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiGetPvid (UINT4 u4IfIndex, UINT4 *pu4RetValFsDot1qPvid)
{

    INT1                i1RetVal = VLAN_FAILURE;

    *pu4RetValFsDot1qPvid = 0;

    VLAN_LOCK ();

    i1RetVal = VlanUtilGetPvid (u4IfIndex, pu4RetValFsDot1qPvid);

    VLAN_UNLOCK ();

    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPortDefaultPriority                    */
/*                                                                           */
/*    Description         : This Api Gets the Default Priority of the given  */
/*                          port.                                            */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port IfIndex                         */
/*                                                                           */
/*    Output(s)           : pi4Dot1dDefaultUserPriority -pointer of the port */
/*                                                       default priority    */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetPortDefaultPriority (INT4 i4IfIndex, INT4 *pi4Dot1dDefaultUserPriority)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    VLAN_LOCK ();
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4IfIndex,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPortId) == VLAN_FALSE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    *pi4Dot1dDefaultUserPriority = (INT4) pVlanPortEntry->u1DefUserPriority;
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHandleSTPStatusChange                            */
/*                                                                           */
/* Description        : This routine handles STP status change from STP      */
/*                      for setting the corresponding tunnel status          */
/*                      in the required port                                 */
/*                                                                           */
/* Input(s)           : tAstSTPTunnelInfo structure from STP                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanHandleSTPStatusChange (tAstSTPTunnelInfo STPTunnelInfo)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = STPTunnelInfo.u4ContextId;
    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME, " Message Allocation failed for Vlan "
                         "tunnel status config message \r\n");
        return;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_STP_STATUS_CHG_MSG;
    pVlanQMsg->u4ContextId = STPTunnelInfo.u4ContextId;
    pVlanQMsg->u4Port = STPTunnelInfo.u4PortNum;
    pVlanQMsg->bTunnelSTPStatus = STPTunnelInfo.bSTPStatus;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME,
                         " Send To Q failed for tunnel status config  "
                         "Message \r\n");

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
}

/*****************************************************************************/
/* Function Name      : VlanUpdateTunnelStatusforSTP                         */
/*                                                                           */
/* Description        : This routine updates the tunnel status of the port   */
/*                      based on the STP status                              */
/*                                                                           */
/* Input(s)           : u2Port - port number                                 */
/*                      bSTPStatus - STP enable/Disable                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanUpdateTunnelStatusforSTP (UINT4 u4Port, BOOL1 bSTPStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4BrgPortType = 0;
#ifdef NPAPI_WANTED
    INT4                i4VlanTunnelProtocolStp = 0;
#endif
    UINT4               u4BridgeMode;
    UINT1               u1Update = VLAN_TRUE;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (VlanGetContextInfoFromIfIndex (u4Port, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);
    if (pVlanPortEntry != NULL)
    {
        if (VlanCfaGetInterfaceBrgPortType (u4Port, &i4BrgPortType) !=
            VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (VlanL2IwfGetBridgeMode (VLAN_CURR_CONTEXT_ID (), &u4BridgeMode) !=
            L2IWF_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        switch (bSTPStatus)
        {
            case AST_TRUE:
                switch (u4BridgeMode)
                {
                    case VLAN_CUSTOMER_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CUSTOMER_BRIDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_PEER;
                        }
                        break;
                    case VLAN_PROVIDER_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CUSTOMER_BRIDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_PEER;
                        }
                        break;
                    case VLAN_PROVIDER_CORE_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CNP_PORTBASED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        else if (i4BrgPortType == CFA_CNP_STAGGED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        break;
                    case VLAN_PROVIDER_EDGE_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CUSTOMER_EDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_PEER;
                        }
                        else if (i4BrgPortType == CFA_CNP_PORTBASED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        else if (i4BrgPortType == CFA_CNP_STAGGED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        else if (i4BrgPortType == CFA_PROP_CUSTOMER_EDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_PEER;
                        }
                        break;
                    default:
                        u1Update = VLAN_FALSE;
                        break;
                }

                break;
            case AST_FALSE:
                switch (u4BridgeMode)
                {
                    case VLAN_CUSTOMER_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CUSTOMER_BRIDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_DISCARD;
                        }
                        break;
                    case VLAN_PROVIDER_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CUSTOMER_BRIDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_DISCARD;
                        }
                        break;
                    case VLAN_PROVIDER_CORE_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CNP_PORTBASED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        else if (i4BrgPortType == CFA_CNP_STAGGED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        break;
                    case VLAN_PROVIDER_EDGE_BRIDGE_MODE:
                        if (i4BrgPortType == CFA_CUSTOMER_EDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_DISCARD;
                        }
                        else if (i4BrgPortType == CFA_CNP_PORTBASED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        else if (i4BrgPortType == CFA_CNP_STAGGED_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_TUNNEL;
                        }
                        else if (i4BrgPortType == CFA_PROP_CUSTOMER_EDGE_PORT)
                        {
                            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
                                VLAN_TUNNEL_PROTOCOL_DISCARD;
                        }
                        break;
                    default:
                        u1Update = VLAN_FALSE;
                        break;
                }
                break;
            default:
                u1Update = VLAN_FALSE;
                break;
        }

        if (u1Update == VLAN_TRUE)
        {
            VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                    u2LocalPort, L2_PROTO_STP,
                                                    VLAN_STP_TUNNEL_STATUS
                                                    (pVlanPortEntry));
#ifdef NPAPI_WANTED
            i4VlanTunnelProtocolStp =
                (INT4) VLAN_STP_TUNNEL_STATUS (pVlanPortEntry);
            VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                 (UINT2) u2LocalPort,
                                                 VLAN_NP_STP_PROTO_ID,
                                                 i4VlanTunnelProtocolStp);
#endif
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanGetRmonStatsPerVlan                              */
/*                                                                           */
/* Description        : This API call collects the per VLAN statistics       */
/*                      from Hardware by calling the specific HW API         */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextId                              */
/*                      VlanId      - Vlan Id                                */
/*                                                                           */
/* Output(s)          : pVlanStatsEntry structure filled with VLAN statistics */
/*                      values                                               */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/

INT4
VlanGetRmonStatsPerVlan (UINT4 u4ContextId,
                         tVlanId VlanId, tVlanEthStats * pVlanStatsEntry)
{
#ifdef NPAPI_WANTED
    UINT4               u4VlanStatsValue[2] = { 0, 0 };
    UINT1               u1StatsType = VLAN_STAT_VLAN_ETHER_DROP_EVENTS;
#endif
    tVlanCurrEntry     *pVlanEntry = NULL;
    /* Initialize Vlan stats structure by zero */
    MEMSET (pVlanStatsEntry, 0, sizeof (tVlanEthStats));

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    /*
     * Check Vlan Counter status is enabled or not.
     * If not enabled, NPSIM/NP can't read stats from h/w registers
     * So do simple return and set the stats as zero.
     * If enabled, NPSIM/NP can read the stats from  h/w registers
     * So allow future to invoke NP-API to retrieve stats values.
     */
    if (VLAN_CNTR_STATUS == VLAN_COUNTER_DISABLE)
    {
        /* Initialize Vlan stats structure by zero */
        MEMSET (pVlanStatsEntry, 0, sizeof (tVlanEthStats));
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return (VLAN_SUCCESS);
    }
#ifdef NPAPI_WANTED
    /* Read Statictics one by one and update the tRmonPerVlanStats structure with 
     * the statictics value. By default start the stats from VLAN_STAT_VLAN_ETHER_DROP_EVENTS
     * (80) and end with VLAN_STAT_VLAN_ETHER_FCS_ERRORS(97)
     */
    while (u1StatsType <= VLAN_STAT_VLAN_ETHER_FCS_ERRORS)
    {
        MEMCPY (u4VlanStatsValue, pVlanEntry->pVlanHwStatsEntry,
                VLAN_HW_STATS_ENTRY_LEN);
        VlanFsMiVlanHwGetVlanStats (u4ContextId, VlanId, u1StatsType,
                                    u4VlanStatsValue);
        switch (u1StatsType)
        {
            case VLAN_STAT_VLAN_ETHER_DROP_EVENTS:
                pVlanStatsEntry->u4VlanStatsDropEvents = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_MCAST_PKTS:
                pVlanStatsEntry->u4VlanStatsMulticastPkts = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_BCAST_PKTS:
                pVlanStatsEntry->u4VlanStatsBroadcastPkts = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_UNDERSIZE_PKTS:
                pVlanStatsEntry->u4VlanStatsUndersizePkts = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_FRAGMENTS:
                pVlanStatsEntry->u4VlanStatsFragments = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS_64_OCTETS:
                pVlanStatsEntry->u4VlanStatsPkts64Octets = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS_65_TO_127_OCTETS:
                pVlanStatsEntry->u4VlanStatsPkts65to127Octets =
                    u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS_128_TO_255_OCTETS:
                pVlanStatsEntry->u4VlanStatsPkts128to255Octets =
                    u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS_256_TO_511_OCTETS:
                pVlanStatsEntry->u4VlanStatsPkts256to511Octets =
                    u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS_512_TO_1023_OCTETS:
                pVlanStatsEntry->u4VlanStatsPkts512to1023Octets =
                    u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS_1024_TO_1518_OCTETS:
                pVlanStatsEntry->u4VlanStatsPkts1024to1518Octets =
                    u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_OVERSIZE_PKTS:
                pVlanStatsEntry->u4VlanStatsOversizePkts = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_JABBERS:
                pVlanStatsEntry->u4VlanStatsJabbers = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_OCTETS:
                pVlanStatsEntry->u4VlanStatsOctets = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_PKTS:
                pVlanStatsEntry->u4VlanStatsPkts = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_COLLISIONS:
                pVlanStatsEntry->u4VlanStatsCollisions = u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_CRC_ALIGN_ERRORS:
                pVlanStatsEntry->u4VlanStatsCRCAlignErrors =
                    u4VlanStatsValue[0];
                break;
            case VLAN_STAT_VLAN_ETHER_FCS_ERRORS:
                pVlanStatsEntry->u4VlanStatsFCS = u4VlanStatsValue[0];
                break;
            default:
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return (VLAN_FAILURE);
        }
        u1StatsType++;
        u4VlanStatsValue[0] = 0;
        u4VlanStatsValue[1] = 0;
    }
#else
    UNUSED_PARAM (u4ContextId);
#endif
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : VlanPbPortEncapTypeIsDot1q                           */
/*                                                                           */
/* Description        : This function will check the encaptype of the port as*/
/*                      DOT1q or not.                                        */
/*                                                                           */
/* Input(s)           : u4FsPbPort                                           */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPbPortEncapTypeIsDot1q (UINT4 u4FsPbPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2LocalPort = 0;
    UINT4               u4ContextId = 0;
    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4FsPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in  getting context info from interface index\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in  selecting the context\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME, "Invalid port\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);
    if (pVlanPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in port get\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) u2LocalPort);
    if (pVlanPbPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in PB port get\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (pVlanPbPortEntry->u1EncapType == VLAN_PB_PORT_AS_DOT1Q_DIS)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Port is not Configured as DOT1q \n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : VlanApiDeleteFdbEntry                                */
/*                                                                           */
/* Description        : This function is an API for deleting an FDB          */
/*                      entry from database                                  */
/*                                                                           */
/* Input(s)           : VlanId, MacAddress                                   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanApiDeleteFdbEntry (tVlanId VlanId, tMacAddr MacAddress)
{
    VLAN_LOCK ();
    if (VlanSelectContext (L2IWF_DEFAULT_CONTEXT) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return;
    }
    VlanHandleDeleteFdbEntry (VlanId, MacAddress, VLAN_LOCAL_FDB);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : VlanRegDeRegProtocol                                 */
/*                                                                           */
/* Description        : This fucntion is used by other protocols to register */
/*                        with VLAN for receving the vlan basic information.   */
/*                                                                           */
/* Input(s)           : u4Type - VLAN_INDICATION_REGISTER                    */
/*                               VLAN_INDICATION_DEREGISTER                  */
/*                    : pVlanRegTbl    - Information about the registration  */
/*                                         protocol and call back function.    */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
VlanRegDeRegProtocol (UINT4 u4Type, tVlanRegTbl * pVlanRegTbl)
{

    if (u4Type >= VLAN_INDICATION_INVALID)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRegDeRegProtocol - Failed - Invalid Type\n");
        return OSIX_FAILURE;
    }

    if (pVlanRegTbl->u1ProtoId >= VLAN_MAX_REG_MODULES)
    {
        if (u4Type == VLAN_INDICATION_REGISTER)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanRegDeRegProtocol - Registration Failed - Max protocols reached\n");
        }
        else
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanRegDeRegProtocol - De-Registration Failed - Max protocols reached\n");
        }

        return OSIX_FAILURE;
    }

    VLAN_LOCK ();
    if (u4Type == VLAN_INDICATION_REGISTER)
    {
        if (pVlanRegTbl->pVlanBasicInfo == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanRegDeRegProtocol - Registration Failed - "
                      "Input Callback function is NULL\n");
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (gaVlanRegTbl[pVlanRegTbl->u1ProtoId].pVlanBasicInfo != NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanRegDeRegProtocol - Registration Failed - Already registered\n");
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        gaVlanRegTbl[pVlanRegTbl->u1ProtoId].u1ProtoId = pVlanRegTbl->u1ProtoId;

        gaVlanRegTbl[pVlanRegTbl->u1ProtoId].pVlanBasicInfo =
            pVlanRegTbl->pVlanBasicInfo;
    }
    else
    {
        gaVlanRegTbl[pVlanRegTbl->u1ProtoId].u1ProtoId = 0;
        gaVlanRegTbl[pVlanRegTbl->u1ProtoId].pVlanBasicInfo = NULL;
    }
    VLAN_UNLOCK ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : vlanGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
VlanGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (VlanCliGetShowCmdOutputToFile ((UINT1 *) VLAN_AUDIT_FILE_ACTIVE) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "GetShRunFile Failed\n");
            return VLAN_FAILURE;
        }
        if (VlanCliCalcSwAudCheckSum
            ((UINT1 *) VLAN_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "CalcSwAudChkSum Failed\n");
            return VLAN_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (VlanCliGetShowCmdOutputToFile ((UINT1 *) VLAN_AUDIT_FILE_STDBY) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "GetShRunFile Failed\n");
            return VLAN_FAILURE;
        }
        if (VlanCliCalcSwAudCheckSum
            ((UINT1 *) VLAN_AUDIT_FILE_STDBY, pu2SwAudChkSum) != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "CalcSwAudChkSum Failed\n");
            return VLAN_FAILURE;
        }
    }
    else
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Node is neither active nor standby\n");
        return VLAN_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return VLAN_SUCCESS;
}

#ifdef VXLAN_WANTED
/*****************************************************************************/
/* Function Name      : VlanApiSetMemberPort                                 */
/*                                                                           */
/* Description        : This function sets the interface as the member       */
/*                      port of the VLAN                                     */
/*                                                                           */
/* Input(s)           : u4VlanId - VLAN ID                                   */
/*                      u4IfIndex - if index                                 */
/*                      u4Flag - Flag value                                  */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/

INT4
VlanApiSetMemberPort (UINT4 u4VlanId, UINT4 u4IfIndex, UINT4 u4Flag)
{
    UINT2               u2LocalPortId = 0;
    UINT4               u4ContextId = 0;
    UINT1              *pau1LocalMemberPorts = NULL;
    UINT1              *pau1LocalForbiddenPorts = NULL;
    UINT1              *pau1LocalUntaggedPorts = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    INT4                i4RetStatus = VLAN_FAILURE;
    UINT1               u1Result = 0;

    if (u4IfIndex == 0)
    {
        return VLAN_SUCCESS;
    }

    VLAN_LOCK ();

    VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId);

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanId);

    if (pStVlanEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_IS_EGRESS_PORT (pStVlanEntry, u2LocalPortId, u1Result);

    if (((u1Result == VLAN_TRUE) && (u4Flag == VLAN_UPDATE)) ||
        ((u1Result == VLAN_FALSE) && (u4Flag == VLAN_DELETE)))
    {
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }

    pau1LocalUntaggedPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1LocalUntaggedPorts == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    MEMSET (pau1LocalUntaggedPorts, 0, VLAN_PORT_LIST_SIZE);

    pau1LocalForbiddenPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1LocalForbiddenPorts == NULL)
    {
        VLAN_UNLOCK ();
        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
        return VLAN_FAILURE;
    }
    MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

    pau1LocalMemberPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1LocalMemberPorts == NULL)
    {
        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

    VLAN_GET_EGRESS_PORTS (pStVlanEntry, pau1LocalMemberPorts);
    if (u4Flag == VLAN_UPDATE)
    {
        VLAN_SET_MEMBER_PORT (pau1LocalMemberPorts, u2LocalPortId);
    }
    else
    {
        VLAN_RESET_MEMBER_PORT (pau1LocalMemberPorts, u2LocalPortId);
        VLAN_GET_UNTAGGED_PORTS (pStVlanEntry, pau1LocalUntaggedPorts);
        VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pau1LocalForbiddenPorts);
    }

    i4RetStatus = VlanSetVxlanNvePorts (u4VlanId, pau1LocalMemberPorts,
                                        pau1LocalUntaggedPorts,
                                        pau1LocalForbiddenPorts, u4Flag);
    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);

    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
    VLAN_UNLOCK ();
    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetVxlanNvePorts                               */
/*                                                                           */
/*     DESCRIPTION      : This function will configure egress and forbidden  */
/*                        Nve ports on a VLAN.                               */
/*                                                                           */
/*     INPUT            : u4VlanId          - Vlan ID                        */
/*                        pu1MemberPorts    - Contains member ports          */
/*                        pu1UntaggedPorts  - Contains untagged ports        */
/*                        pu1ForbiddenPorts - Contains forbidden ports       */
/*                        pau1Name          - Vlan name identifier           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : VLAN_SUCCESS/VLAN_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetVxlanNvePorts (UINT4 u4VlanId, UINT1 *pu1MemberPorts,
                      UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts,
                      UINT4 u4Flag)
{
    UINT1              *pau1OldEgressPortList = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    UINT1              *pau1OldForbiddenPortList = NULL;
    INT4                i4Dot1qVlanStaticRowStatus;
    tSNMP_OCTET_STRING_TYPE OldEgressPorts;
    tSNMP_OCTET_STRING_TYPE OldForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE ForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE UntaggedPorts;

    /* Check whether the Vlan entry is already present */
    if ((nmhGetDot1qVlanStaticRowStatus (u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        == SNMP_FAILURE)
    {
        /* A row in the Vlan table must have been created */
        return VLAN_FAILURE;
    }
    pau1OldEgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldEgressPortList == NULL)
    {
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldEgressPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldUntaggedPortList == NULL)
    {
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1OldForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldForbiddenPortList == NULL)
    {
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    EgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    EgressPorts.pu1_OctetList = pu1MemberPorts;

    ForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    ForbiddenPorts.pu1_OctetList = pu1ForbiddenPorts;

    UntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    UntaggedPorts.pu1_OctetList = pu1UntaggedPorts;

    OldEgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldEgressPorts.pu1_OctetList = pau1OldEgressPortList;

    OldForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldForbiddenPorts.pu1_OctetList = pau1OldForbiddenPortList;

    OldUntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldUntaggedPorts.pu1_OctetList = pau1OldUntaggedPortList;

    nmhGetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
    nmhGetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);
    nmhGetDot1qVlanForbiddenEgressPorts (u4VlanId, &OldForbiddenPorts);

    if (u4Flag == VLAN_UPDATE)
    {
        VLAN_ADD_PORT_LIST (EgressPorts.pu1_OctetList,
                            OldEgressPorts.pu1_OctetList);
        VLAN_ADD_PORT_LIST (UntaggedPorts.pu1_OctetList,
                            OldUntaggedPorts.pu1_OctetList);
        VLAN_ADD_PORT_LIST (ForbiddenPorts.pu1_OctetList,
                            OldForbiddenPorts.pu1_OctetList);
    }

    if (nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &EgressPorts)
        == SNMP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return VLAN_FAILURE;
    }

    if (nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &UntaggedPorts)
        == SNMP_FAILURE)
    {
        nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);

        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return VLAN_FAILURE;
    }
    if (nmhSetDot1qVlanForbiddenEgressPorts (u4VlanId, &ForbiddenPorts)
        == SNMP_FAILURE)
    {
        nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
        nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);

        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return VLAN_FAILURE;

    }

    if (i4Dot1qVlanStaticRowStatus != VLAN_ACTIVE)
    {
        if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_ACTIVE)
            == SNMP_FAILURE)
        {
            nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
            nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);
            nmhSetDot1qVlanForbiddenEgressPorts (u4VlanId, &OldForbiddenPorts);

            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
            return VLAN_FAILURE;
        }
    }

    UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
    UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
    return VLAN_SUCCESS;
}

#endif

/*****************************************************************************/
/* Function Name      : VlanCheckIsTunnelEnabledForOtherMac                  */
/*                                                                           */
/* Description        : This function check for the destination MAC address  */
/*                      and if find match, it will return the Tunnel status. */
/*                                                                           */
/* Input(s)           : pFrame - Pointer to the incoming packet.             */
/*                      u4IfIndex - if index                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_FALSE/VLAN_TRUE/TUNNEL/PEER/DISCARD             */
/*****************************************************************************/
INT4
VlanCheckIsTunnelEnabledForOtherMac (UINT4 u4IfIndex,
                                     tCRU_BUF_CHAIN_DESC * pFrame)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    tVlanId             SVlanId = 0;
    UINT1               au1DestOtherMacAddr[VLAN_MAC_ADDR_LEN];
    INT4                i4index = 0;
    INT4                i4minindex = 0;
    INT4                i4maxindex = L2_PROTO_MAX - L2_PROTO_OTHER;
    INT4                i4RetVal = 0;
    INT4                i4ServiceVlanTunnelProtocolStatus = 0;
    INT4                VlanId = 0;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    MEMSET (au1DestOtherMacAddr, 0, VLAN_MAC_ADDR_LEN);

    CRU_BUF_Copy_FromBufChain (pFrame, au1DestOtherMacAddr,
                               0, VLAN_MAC_ADDR_LEN);

    VLAN_LOCK ();
    /* Get context Id from IfIndex */
    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    /* Get SVlan Id based on the port type */
    if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        VlanGetSVlanId (pFrame, u2LocalPort, u4ContextId, &SVlanId);
    }

    VlanId = (INT4) SVlanId;

    pVlanCurrEntry = VlanGetVlanEntry (VlanId);
    if (pVlanCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "\n\n Current VLAN entry is NULL. \n\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    for (i4index = i4minindex; i4index <= i4maxindex; i4index++)
    {
        if (MEMCMP (au1DestOtherMacAddr,
                    VLAN_RSVD_OTHER_ADDR_PER_VLAN (pVlanCurrEntry, i4index),
                    MAC_ADDR_LEN) == 0)
        {
            /* Get the tunnel status for the protocol */
            i4ServiceVlanTunnelProtocolStatus =
                pVlanCurrEntry->VlanTunnelL2ProtoInfo.
                u1OtherTunnelStatus[i4index];
            if (i4ServiceVlanTunnelProtocolStatus ==
                VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                i4RetVal = VLAN_TUNNEL_PROTOCOL_TUNNEL;
                pVlanPortEntry->u1OtherProtEnableStatus = VLAN_TRUE;
            }
            else if (i4ServiceVlanTunnelProtocolStatus ==
                     VLAN_TUNNEL_PROTOCOL_PEER)
            {
                i4RetVal = VLAN_TUNNEL_PROTOCOL_PEER;
            }
            else if (i4ServiceVlanTunnelProtocolStatus ==
                     VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                i4RetVal = VLAN_TUNNEL_PROTOCOL_DISCARD;
            }
            break;

        }
        else if (MEMCMP (au1DestOtherMacAddr,
                         VLAN_TUNNEL_OTHER_ADDR_PER_VLAN (pVlanCurrEntry,
                                                          i4index),
                         MAC_ADDR_LEN) == 0)
        {
            /* Get the tunnel status for the protocol */
            i4ServiceVlanTunnelProtocolStatus =
                pVlanCurrEntry->VlanTunnelL2ProtoInfo.
                u1OtherTunnelStatus[i4index];
            if (i4ServiceVlanTunnelProtocolStatus ==
                VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                i4RetVal = VLAN_TUNNEL_PROTOCOL_TUNNEL;
                pVlanPortEntry->u1OtherProtEnableStatus = VLAN_TRUE;
            }
            else if (i4ServiceVlanTunnelProtocolStatus ==
                     VLAN_TUNNEL_PROTOCOL_PEER)
            {
                i4RetVal = VLAN_TUNNEL_PROTOCOL_PEER;
            }
            else if (i4ServiceVlanTunnelProtocolStatus ==
                     VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                i4RetVal = VLAN_TUNNEL_PROTOCOL_DISCARD;
            }
            break;

        }
        else
        {
            i4RetVal = VLAN_TRUE;
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanCheckPortType                                    */
/*                                                                           */
/* Description        : This function will check if the port is a tagged     */
/*                      member of the Vlan                                   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2VlanId - Vlan Id                                   */
/*                      u2Port - Port Number                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE/VLAN_FALSE                                 */
/*****************************************************************************/

INT4
VlanCheckPortType (UINT2 u2VlanId, UINT2 u2Port)
{
    tStaticVlanEntry   *pStVlanEntry;
    UINT1               u1Result = VLAN_TRUE;

    VLAN_LOCK ();
    pStVlanEntry = VlanGetStaticVlanEntry (u2VlanId);
    if (pStVlanEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }
    VLAN_IS_EGRESS_PORT (pStVlanEntry, u2Port, u1Result);
    if (u1Result == VLAN_TRUE)
    {
        VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            VLAN_UNLOCK ();
            return VLAN_FALSE;
        }

    }
    else
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }
    VLAN_IS_FORBIDDEN_PORT (pStVlanEntry, u2Port, u1Result);
    if (u1Result == VLAN_TRUE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }
    VLAN_UNLOCK ();
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfigDot1qPvidForPvrstStart                 */
/*                                                                           */
/*    Description         : This function is called by PVRST module when     */
/*                          the module is started.                  */
/*                          When PVID is already configured                  */
/*                          on the port, the port will be set to untagged    */
/*                          member of that PVID                              */
/*                                      */
/*    Input(s)            : u2Port - Port Number                             */
/*                          VlanId - VLAN ID (PVID)                          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanConfigDot1qPvidForPvrstStart (UINT2 u2Port, tVlanId VlanId)
{

    UINT4               u4ContextId;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanCurrEntry     *pVlanNewEntry = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_LOCK ();

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        pStVlanEntry = VlanGetStaticVlanEntry (VLAN_DEF_VLAN_ID);

        if (pStVlanEntry != NULL)
        {
            VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result);

            if ((pVlanPortEntry->u1PortType == VLAN_HYBRID_PORT) &&
                (u1Result == VLAN_TRUE))
            {
                /*When the port type is hybrid port and pvid is not equal to
                 *default vlan, delete the port from the default vlan
                 *This has been done to be compliance with CISCO(5K-Nexus).
                 */
                VLAN_RESET_SINGLE_UNTAGGED_PORT (pStVlanEntry, u2Port);
                VLAN_RESET_SINGLE_EGRESS_PORT (pStVlanEntry, u2Port);

                pVlanEntry = VLAN_GET_CURR_ENTRY (VLAN_DEF_VLAN_ID);

                if (pVlanEntry != NULL)
                {
                    VLAN_RESET_SINGLE_CURR_EGRESS_PORT (pVlanEntry, u2Port);

                    if (VlanHwResetVlanMemberPort (pVlanPortEntry->Pvid, u2Port)
                        != VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                        (pVlanEntry->VlanId, u2Port);

                    if (VlanCfaGetIvrStatus () == CFA_ENABLED)
                    {
                        VlanIvrComputeVlanMclagIfOperStatus (pVlanEntry);
                        VlanIvrComputeVlanIfOperStatus (pVlanEntry);
                    }
                }
            }
        }

        if (pVlanPortEntry->u1PortType == VLAN_HYBRID_PORT)
        {
            pVlanNewEntry = VLAN_GET_CURR_ENTRY (VlanId);

            if (pVlanNewEntry != NULL)
            {
                VLAN_SET_SINGLE_CURR_EGRESS_PORT (pVlanNewEntry, u2Port);
                VLAN_SET_SINGLE_UNTAGGED_PORT (pVlanNewEntry->pStVlanEntry,
                                               u2Port);
                VLAN_SET_SINGLE_EGRESS_PORT (pVlanNewEntry->pStVlanEntry,
                                             u2Port);
                if (VlanHwSetVlanMemberPort (VlanId, u2Port, VLAN_FALSE)
                    != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return VLAN_FAILURE;
                }
                VLAN_GET_TIMESTAMP (&pVlanNewEntry->u4TimeStamp);
                VlanNotifySetVlanMemberToL2IwfAndEnhFltCriteria
                    (pVlanNewEntry->VlanId, u2Port, VLAN_ADD_UNTAGGED_PORT);
                if (VlanCfaGetIvrStatus () == CFA_ENABLED)
                {
                    VlanIvrComputeVlanMclagIfOperStatus (pVlanEntry);
                    VlanIvrComputeVlanIfOperStatus (pVlanNewEntry);
                }
            }

            VlanL2IwfSetVlanPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port, VlanId);
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanUpdatePortIsolationForMcLag                         */
/*                                                                            */
/*  Description     : This funtion creates port isolation for MC-LAG.         */
/*                    It masks/unmasks the MC-LAG interface(s) in egress list */
/*                                                                            */
/*  Input(s)        : u4IfIndex - MC-LAG interface index                      */
/*                    u1Action - Masks/Unmasks                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS/VLAN_FAILURE                               */
/******************************************************************************/

INT4
VlanUpdatePortIsolationForMcLag (UINT4 u4IfIndex, UINT1 u1Action)
{
    tIssUpdtPortIsolation PortIsolation;
    UINT4               u4IcclIfIndex = 0;
    UINT4               au4PortArray[VLAN_PORT_LIST_SIZE];

    MEMSET (au4PortArray, 0, (VLAN_PORT_LIST_SIZE * sizeof (UINT4)));
    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));

    VlanIcchGetIcclIfIndex (&u4IcclIfIndex);

    PortIsolation.InVlanId = VLAN_ZERO;
    PortIsolation.u4IngressPort = u4IcclIfIndex;
    PortIsolation.u2NumEgressPorts = 1;
    au4PortArray[0] = u4IfIndex;
    PortIsolation.pu4EgressPorts = au4PortArray;
    OSIX_BITLIST_SET_BIT (PortIsolation.ExcludeList, u4IfIndex,
                          BRG_PORT_LIST_SIZE);

    if (VLAN_PI_MASK == u1Action)
    {
        PortIsolation.u1Action = ISS_PI_DELETE;
    }
    else if (VLAN_PI_UNMASK == u1Action)
    {
        PortIsolation.u1Action = ISS_PI_ADD;
    }
    VlanIssApiUpdtPortIsolationEntry (&PortIsolation);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsEcfmTunnelPkt                              */
/*                                                                           */
/*    Description         : This function is used to check whether the       */
/*                          pkt is ecfm tunnel pkt                           */
/*                                                                           */
/*    Input(s)            : DestAddr - Destination MAc Address               */
/*                          u4ContextId - Context ID                         */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanIsEcfmTunnelPkt (UINT4 u4ContextId, tMacAddr DestAddr)
{

    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MEMCMP (DestAddr, VLAN_TUNNEL_ECFM_ADDR (), VLAN_MAC_ADDR_LEN) ==
        0)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return VLAN_FAILURE;
}

#ifdef ICCH_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanApiSetIcchPortProperties                            */
/*                                                                            */
/*  Description     : Main routine to set the ICCH following VLAN port        */
/*                    properties                                              */
/*                    1. Set the ICCL port as trunk port                      */
/*                    2. Set ICCL VLAN ID as PVID on ICCL port                */
/*                    3. Disable MAC-learning on ICCL port                    */
/*                                                                            */
/*  Input(s)        : pi1Param                                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS/VLAN_FAILURE                               */
/******************************************************************************/
INT4
VlanApiSetIcchPortProperties (VOID)
{
    tLocalPortList      LocalPortList;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    tVlanId             VlanId = 0;

    MEMSET (LocalPortList, 0, sizeof (tLocalPortList));
    VLAN_LOCK ();

    /* Fetch ICCL interface  */
    IcchGetIcclIfIndex (&u4IfIndex);

    /* Get the VLAN ID for ICCL Instance 0 */
    VlanId = (tVlanId) IcchApiGetIcclVlanId (ICCH_DEFAULT_INST);

    /* Derive the local port information corresponding to this CFA index */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "\n\n Local port information is unavailable for %d. \n\n",
                       u4IfIndex);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (NULL == pVlanPortEntry)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    pVlanPortEntry->u1DefUserPriority = VLAN_HIGHEST_PRIORITY;

    /* Set the Port as TRUNK type */
    if (VLAN_FAILURE == VlanUtilSetVlanPortType (u2LocalPort, VLAN_TRUNK_PORT))
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "\n\r Setting ICCL interface as TRUNK"
                       "type failed %d. \n\n", u4IfIndex);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    /* Add Trunk port to existing VLANs */
    VlanAddTrunkPortToExistingVlans (u2LocalPort);

    /* Set ICCL VLAN ID as PVID on ICCL interface */
    VlanUtilSetPvid (u2LocalPort, VlanId);

    pStVlanEntry = VlanGetStaticVlanEntry (VLAN_DEF_VLAN_ID);
    if (NULL != pStVlanEntry)
    {
        VLAN_RESET_SINGLE_UNTAGGED_PORT (pStVlanEntry, u2LocalPort);
    }

    /* Disable MAC learning on ICCL interface */
    if (VLAN_FAILURE ==
        VlanHwPortMacLearningStatus (u4ContextId, u4IfIndex, VLAN_DISABLED))
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "\n\r Disabling MAC learning status on ICCL port failed \n\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    else
    {
        if (pVlanPortEntry != NULL)
        {
            pVlanPortEntry->u1MacLearningStatus = VLAN_DISABLED;
        }
    }

    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiDeleteRemoteFdb                           */
/*                                                                           */
/*    Description         : This function deletes the remote fdb entries     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/

VOID
VlanApiDeleteRemoteFdb (VOID)
{
    VLAN_LOCK ();

    VlanFlushRemoteFdb (VLAN_TRUE);

    VLAN_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiSendBulkReq                               */
/*                                                                           */
/*    Description         : This function sends bulk request message to peer */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanApiSendBulkReq (VOID)
{
    VlanIcchSendBulkReqMsg ();
    return;
}

/*****************************************************************************/
/* Function Name      : VlanAddIsolationTblForIccl                           */
/*                                                                           */
/* Description        : This routine is called to add port isolation table   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanAddIsolationTblForIccl (VOID)
{
    tIssUpdtPortIsolation PortIsolation;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               au4PortArray[ISS_MAX_UPLINK_PORTS];
    UINT2               u2Temp = 0;
    UINT4               u4NullPort = 0;
    UINT2               u2Port = 0;

    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (au4PortArray, 0, ISS_MAX_UPLINK_PORTS);

    IcchGetIcclIfIndex (&(PortIsolation.u4IngressPort));

    if (u4NullPort == PortIsolation.u4IngressPort)
    {
        return;
    }
    PortIsolation.InVlanId = 0;
    PortIsolation.u1Action = ISS_PI_ADD;

    VlanSelectContext (VLAN_DEFAULT_CONTEXT_ID);
    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
        {
            break;
        }

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if ((pVlanPortEntry != NULL) &&
            (PortIsolation.u4IngressPort != pVlanPortEntry->u4IfIndex))
        {
            au4PortArray[u2Temp++] = pVlanPortEntry->u4IfIndex;
        }
    }

    PortIsolation.pu4EgressPorts = au4PortArray;
    PortIsolation.u2NumEgressPorts = u2Temp;
    VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanApiSetIcchPortMacLearningStatus                  */
/*                                                                           */
/* Description        : This routine is called to set MAC learning status    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                     u1Status - VLAN_ENABLED/VLAN_DISABLED                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
INT4
VlanApiSetIcchPortMacLearningStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    UINT4               u4ContextId = 0;

    VLAN_LOCK ();

    /* Set MAC learning status */
    if (VLAN_FAILURE == VlanHwPortMacLearningStatus (u4ContextId,
                                                     u4IfIndex, u1Status))
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "\r\n Disabling MAC learning status on ICCL port failed \r\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

#endif /* ICCH_WANTED */
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiUpdateStaticVlanInfo                      */
/*                                                                           */
/*    Description         : This function updates the Static VLAN table.     */
/*                          with the input interface being added as  a tagged*/
/*                          member port.                                     */
/*                                                                           */
/*    Input(s)            : VlanId   - The VlanId which is to be statically  */
/*                                     created.                              */
/*                          u4IfIndex - u4IfIndex  Number                    */
/*                          u4Action  - vlan create or delete                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FAILURE/VLAN_SUCCESS                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiUpdateStaticVlanInfo (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex,
                             UINT4 u4Action)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT2               u2LocalPortId = 0;

    VLAN_LOCK ();

    /* Return If the PortEntry for the Port is NULL */
    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPortId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_SUCCESS;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (u4Action == VLAN_CREATE)
    {
        if (pStVlanEntry != NULL)
        {
            if (VlanUtilMISetVlanStEgressPort ((INT4) u4ContextId,
                                               (UINT4) VlanId,
                                               (INT4) u4IfIndex,
                                               VLAN_ADD_TAGGED_PORT)
                == VLAN_FAILURE)
            {
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }

        }
        else
        {
            pStVlanEntry = (tStaticVlanEntry *) (VOID *)
                VlanCustGetBuf (VlanId, VLAN_ST_VLAN_ENTRY, sizeof
                                (tStaticVlanEntry));
            if (pStVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Static VLAN Entry \n");
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }

            MEMSET (pStVlanEntry, 0, sizeof (tStaticVlanEntry));

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            pStVlanEntry->VlanId = VlanId;
            pStVlanEntry->u1NameModifiedFlag = VLAN_FALSE;
            pStVlanEntry->pNextNode = gpVlanContextInfo->pStaticVlanTable;
            gpVlanContextInfo->pStaticVlanTable = pStVlanEntry;
            pStVlanEntry->u1ServiceType = VLAN_E_LAN;
            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                pStVlanEntry->u2EgressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
            }
            else
            {
                pStVlanEntry->u2EgressEtherType = VLAN_PROTOCOL_ID;
            }

            if (VLAN_FAILURE == VlanAddCurrEntry (pStVlanEntry))
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VlanAddCurrEntry failed for %d\n", VlanId);

            }
            pStVlanEntry->u1RowStatus = VLAN_ACTIVE;

            if (VlanUtilMISetVlanStEgressPort ((INT4) u4ContextId,
                                               (UINT4) VlanId,
                                               (INT4) u4IfIndex,
                                               VLAN_ADD_TAGGED_PORT)
                == VLAN_FAILURE)
            {
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }
        }
    }

    if (u4Action == VLAN_DELETE)
    {
        if (pStVlanEntry != NULL)
        {
            if (VlanDelStVlanInfo (pStVlanEntry) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "Deletion of static VLAN failed for %d\n",
                               VlanId);
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }
            VlanDeleteStVlanEntry (pStVlanEntry);
        }

    }
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/* *************************************************************
 * Function    : VlanApiGetMACUpdate
 * Input       : u1ProtoId - Protocol Id
 *               u2VlanId - Vlan Id
 * Description : This function scans FDB table and sends MAC
 *               indication to registered protocol, if FDB entry
 *               is present
 * Output      : NONE
 * Returns     : VLAN_SUCCESS/VLAN_FAILURE
 * *************************************************************/
INT4
VlanApiGetMACUpdate (UINT1 u1ProtoId, UINT2 u2VlanId)
{
    UINT4               u4CurrFdbId = 0;
    UINT4               u4NextFdbId = 0;
    UINT4               u4Status = 0;
    UINT4               i4ContextId = 0;
    UINT4               u4MacCount = 0;
    UINT4               u4FdbPort = 0;
    INT4                i4NextContextId = 0;
    INT4                i4RetVal = 0;
    tMacAddr            MacAddr;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tVlanId             VlanId;
    tVlanBasicInfo      VlanBasicInfo;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&VlanBasicInfo, 0, sizeof (tVlanBasicInfo));

    if (u1ProtoId >= VLAN_MAX_REG_MODULES)
    {
        return VLAN_FAILURE;
    }
    VLAN_LOCK ();
    VlanId = (tVlanId) u2VlanId;
    if (VLAN_FAILURE == VlanGetFdbIdFromVlanId (VlanId, &u4CurrFdbId))
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (nmhGetNextIndexFsDot1qTpFdbTable
        (i4ContextId, &i4NextContextId, u4CurrFdbId, &u4NextFdbId,
         CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    do
    {
        if (i4ContextId != (UINT4) i4NextContextId)
        {
            break;
        }
        if (VlanSelectContext (i4ContextId) != VLAN_SUCCESS)
        {
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }

        if (VLAN_FAILURE == VlanGetVlanIdFromFdbId (u4NextFdbId, &VlanId))
        {
            VlanReleaseContext ();
            break;
        }
        VlanReleaseContext ();
        if (VlanId != (tVlanId) u2VlanId)
        {
            break;
        }

        u4Status = 0;
        nmhGetFsDot1qTpFdbStatus (i4ContextId, u4NextFdbId,
                                  NextMacAddr, (INT4 *) &u4Status);

        nmhGetFsDot1qTpFdbPort (i4ContextId, u4NextFdbId,
                                NextMacAddr, (INT4 *) &u4FdbPort);

        if ((u4Status == VLAN_FDB_LEARNT) || (u4Status == VLAN_FDB_MGMT))
        {
            VlanBasicInfo.u2Port = (UINT2) u4FdbPort;
            VlanBasicInfo.u2VlanId = VlanId;
            MEMCPY (VlanBasicInfo.MacAddr, NextMacAddr, sizeof (tMacAddr));
            VlanBasicInfo.u1EntryStatus = u4Status;

            VlanBasicInfo.u1Action = VLAN_CB_UPDATE_MAC;
            u4MacCount++;

            if ((gaVlanRegTbl[u1ProtoId].pVlanBasicInfo) != NULL)
            {
                (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo) (&VlanBasicInfo);
            }
        }

        u4CurrFdbId = u4NextFdbId;
        i4ContextId = i4NextContextId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));

        i4RetVal = nmhGetNextIndexFsDot1qTpFdbTable
            (i4ContextId, &i4NextContextId,
             u4CurrFdbId, &u4NextFdbId, CurrMacAddr, &NextMacAddr);

    }
    while (i4RetVal != SNMP_FAILURE);
    VLAN_UNLOCK ();

    if (u4MacCount == 0)
    {
        return VLAN_FAILURE;
    }
    else
    {
        return VLAN_SUCCESS;
    }
}
#endif /* EVPN_VXLAN_WANTED */

/*****************************************************************************/
/* Function Name      : VlanMemberPortType                                   */
/*                                                                           */
/* Description        : This funcion provide the member port type.           */
/*                                                                           */
/* Input(s)           : i4Vlanid and u4PortNo                                */
/*                                                                           */
/* Output(s)          : vlan member port type                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/

INT1
VlanMemberPortType (INT4 i4Vlanid, UINT4 u4IfIndex, UINT1 *u1MemPortType)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1Result = VLAN_FALSE;
    INT1                i1RetVal = VLAN_FAILURE;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry ((UINT2) i4Vlanid);
    if (pVlanEntry != NULL)
    {
        if (pVlanEntry->pStVlanEntry != NULL)
        {
            /* check for Forbidden Port */
            VLAN_IS_MEMBER_PORT (pVlanEntry->pStVlanEntry->ForbiddenPorts,
                                 u2LocalPort, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                *u1MemPortType = FORBIDDEN_PORT;
                i1RetVal = VLAN_SUCCESS;
            }
        }
        if (u1Result == VLAN_FALSE)
        {
            /* check for Member Port */
            VLAN_IS_MEMBER_PORT (pVlanEntry->EgressPorts, u2LocalPort,
                                 u1Result);
            if (u1Result == VLAN_TRUE)
            {
                *u1MemPortType = MEMBER_PORT;
                i1RetVal = VLAN_SUCCESS;
            }
            else
            {
                *u1MemPortType = NON_MEMBER_PORT;
                i1RetVal = VLAN_SUCCESS;
            }
        }
    }
    else
    {
        i1RetVal = VLAN_FAILURE;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i1RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanApiIcchProcessMclagDown                          */
/*                                                                           */
/* Description        : This function is used to process MC-LAG PortChannel  */
/*                      down scenario.                                       */
/*                                                                           */
/* Input(s)           : u4IfIndex - InterfaceIndex of the MC-LAG PortChannel.*/
/*                      u1Status - MCLAG PortChannel Status in Slave         */
/*                                 LA_PORT_DOWN/LA_PORT_UP_IN_BNDL           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanApiIcchProcessMclagDown (UINT4 u4IfIndex, UINT1 u1Status)
{
#ifdef ICCH_WANTED
    VlanIcchProcessMclagDown (u4IfIndex, u1Status);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : VlanApiIcchProcessMclagOperUp                        */
/*                                                                           */
/* Description        : This function is used to process MC-LAG PortChannel  */
/*                      OperUp Scenario.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanApiIcchProcessMclagOperUp (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    VlanIcchProcessMclagOperUp (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : VlanApiIcchCheckPortStatus                           */
/*                                                                           */
/* Description        : This function initiates Check Port Status message    */
/*                      to the Peer.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanApiIcchCheckPortStatus (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    if (VLAN_FAILURE == VlanIcchMoveFdbtoIccl (u4IfIndex))
    {
        return;
    }
    VlanIcchCheckPortStatus (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return;
}

/***************************************************************************
 *  FUNCTION NAME : VlanApiEvbCallBackFunc
 *
 *  DESCRIPTION   : This is the application call back function. This function
 *                  will be registered with LLDP and LLDP will post events to
 *                  VLAN module through  this function to hanlde the Application
 *                  Priority TLV related messages
 *
 *  INPUT         : tLldpAppTlv - Structure with TLV info.
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 * **************************************************************************/
VOID
VlanApiEvbCallBackFunc (tLldpAppTlv * pLldpAppTlv)
{
    VlanEvbPostCdcpMsg (pLldpAppTlv);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanApiEvbGetSystemStatus                            */
/*                                                                           */
/* Description        : This function returns the EVB system status on this  */
/*                      context Id.                                          */
/*                                                                           */
/* Input(s)           : u4ContextID - Context Id.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE/VLAN_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanApiEvbGetSystemStatus (UINT4 u4ContextId)
{
    UINT1               u1RetVal = VLAN_FALSE;

    VLAN_LOCK ();
    u1RetVal = VlanEvbGetSystemStatus (u4ContextId);
    VLAN_UNLOCK ();

    return u1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiEvbGetSbpPortsOnUap                       */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
VlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray * pSbpArray)
{
    VLAN_LOCK ();

    VlanEvbGetSbpPortsOnUap (u4UapIfIndex, pSbpArray);

    VLAN_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiGetSChIfIndex                             */
/*                                                                           */
/*    Description         : This function provides the S-Channel IfIndex for */
/*                          the given SVID and UAP IfIndex.                  */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4SChIfIndex- S-Channel IfIndex                 */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiGetSChIfIndex (UINT4 u4ContextId, UINT4 u4UapIfIndex,
                      UINT2 u2SVID, UINT4 *pu4SChIfIndex)
{
    INT4                i4RetVal = VLAN_FAILURE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    i4RetVal = VlanGetSChIfIndex (u4UapIfIndex, (UINT4) u2SVID, pu4SChIfIndex);

    VlanReleaseContext ();

    VLAN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiGetSChInfoFromSChIfIndex                  */
/*                                                                           */
/*    Description         : This function provides the UAP IfIndex and SVID  */
/*                          for the given SCh IfIndex.                       */
/*                                                                           */
/*    Input(s)            : u4SChIfIndex - S-Channel IfIndex.                */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4UapIfIndex- UAP IfIndex                       */
/*                          pu2SVID      - SVID                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                 UINT2 *pu2SVID)
{
    INT4                i4RetVal = VLAN_FAILURE;

    i4RetVal = VlanGetSChInfoFromSChIfIndex (u4SChIfIndex, pu4UapIfIndex,
                                             pu2SVID);

    return i4RetVal;
}

/***************************************************************************/
/* Function Name    : VlanUpdateMclagStatus                                */
/*                                                                         */
/* Description      : This function posts Mclag status of a port-channel   */
/*                    to vlan queue                                        */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                    u1MclagStatus - Port Channel Mclag status            */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/

INT4
VlanUpdateMclagStatus (UINT2 u2Port, UINT1 u1MclagStatus)
{
    INT4                i4RetVal;

    if (u1MclagStatus == LA_MCLAG_ENABLED)
    {
        i4RetVal =
            VlanPostCfgMessage (VLAN_MCLAG_ENABLE_STATUS, (UINT4) u2Port);
    }
    else
    {
        i4RetVal =
            VlanPostCfgMessage (VLAN_MCLAG_DISABLE_STATUS, (UINT4) u2Port);
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrGetMclagEnabledStatus                     */
/*                                                                           */
/*    Description         : This function returns the Mclag status of Vlan   */
/*                          based on Mclag member ports present/not          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId - VlanId                                  */
/*                                                                           */
/*    Output(s)           : pu1MclagStatus - Port oper status                */
/*                                                                           */
/*    Global Variables    : None                                             */
/*    Referred                                                               */
/*                                                                           */
/*    Global Variables    : None.                                            */
/*    Modified                                                               */
/*                                                                           */
/*    Use of Recursion   : None.                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrGetMclagEnabledStatus (UINT4 u4ContextId, tVlanId VlanId,
                              UINT1 *pu1MclagStatus)
{
#ifdef ICCH_WANTED
    tVlanCurrEntry     *pCurrEntry = NULL;

    /* Set the Mclag status as OSIX_FALSE */

    *pu1MclagStatus = OSIX_FALSE;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanIvrGetMclagEnabledStatus "
                  "Context Selection failed \r\n");

        VLAN_UNLOCK ();
        return;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (NULL == pCurrEntry)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME,
                       "VlanIvrGetMclagEnabledStatus "
                       "Vlan entry not present for Vlan = %u \r\n", VlanId);
        VLAN_UNLOCK ();
        return;
    }

    if (pCurrEntry->u1IsMclagEnabled == VLAN_TRUE)
    {
        *pu1MclagStatus = OSIX_TRUE;
    }

    VLAN_UNLOCK ();
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pu1MclagStatus);
#endif

    return;
}

/*****************************************************************************/
/* Function Name      : VlanGetCpvidForCep                                   */
/*                                                                           */
/* Description        : This function is called to get the CPVID of the      */
/*                      customer edge port                                   */
/*                                                                           */
/* Input(s)           : u4Port - Port index                                  */
/*                                                                           */
/* Output(s)          : pu2CVid - Default customer vlan id of the port       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanGetCpvidForCep (UINT4 u4Port, UINT2 *pu2CVid)
{
    tVlanPbPortEntry   *pPbPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4Port, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u4Port);

    if (pPbPortEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (pPbPortEntry != NULL)
    {
        if (((pPbPortEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT) &&
            (pPbPortEntry->u1DefCVlanClassify == VLAN_ENABLED))
        {
            *pu2CVid = pPbPortEntry->CVlanId;
        }
    }

    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanApiSetFdbFreezeStatus                            */
/*                                                                           */
/* Description        : This function freezes / unfreezes the FDB table      */
/*                                                                           */
/* Input(s)           : i4MacLearnStatus - Enable/Disable Mac Learning       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE  :  FDB table will be frozen               */
/*                      (a) New MAC learning  will be disabled irrespective  */
/*                      of MAC learning status                               */
/*                      (b) Already learnt entries will not be aged out      */
/*                      OSIX_FALSE  :  FDB table will be un-frozen.          */
/*                      (a) MAC learning  status will be restored.           */
/*                      (b) Age out timer will be restored                   */
/*****************************************************************************/
INT4
VlanApiSetFdbFreezeStatus (INT4 i4MacLearnStatus)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;
    UINT4               u4Context = 0;
    UINT4               u4VlanId = 0;
    INT4                i4AgingTimeout = 0;

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return VLAN_FAILURE;
    }
    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));

    VLAN_LOCK ();

    for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
    {
        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            /* Context is not present so fetch the next context */
            continue;
        }

        if (i4MacLearnStatus == VLAN_DISABLED)
        {
            /* Set 0 to stop the ageing timer, inorder to freeze
             * mac entries from aging out */
            i4AgingTimeout = 0;
        }
        else
        {
            /* Set default Mac learning time out value */
            i4AgingTimeout = (INT4) VLAN_CURR_CONTEXT_PTR ()->u4AgeOutTime;
        }

        /* Scan through vlan table and set vlan mac learning status */
        VLAN_SCAN_VLAN_TABLE (u4VlanId)
        {
            pVlanEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);
            if (pVlanEntry == NULL)
            {
                continue;
            }

            if (VLAN_CONTROL_ADM_MAC_LEARNING_STATUS (pVlanEntry->VlanId)
                == i4MacLearnStatus)
            {
                /* Mac Learning is disabled in this vlan do nothing */
                continue;
            }
            VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pu1LocalPortList);

            /* Program Mac learning status for vlan */
            if (VlanHwMacLearningStatus (u4Context,
                                         (tVlanId) u4VlanId,
                                         pu1LocalPortList,
                                         (UINT1) i4MacLearnStatus) !=
                VLAN_SUCCESS)
            {
                continue;
            }
        }

        /* Stop/Start the MAC ageing timer */
        VlanHwSetAgingTime (u4Context, i4AgingTimeout);
    }

    UtilPlstReleaseLocalPortList (pu1LocalPortList);

    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

#ifdef ICCH_SYNC_OPTIMIZED_WRITE
 /********************************************************************************
  *    Function Name      : VlanPostSyncPktToVlan
  *
  *    Description        : This function is invoked whenever ICCH HB packet is
  *                         received in the driver. This will take care of
  *                         posting the packet in CruBuffer format to ICCH
  *
  *    Input(s)           : pBuf - pointer to the ICCH HB Data
  *
  *
  *
  *    Output(s)          : None
  *
  *    Returns            : OSIX_SUCCESS / OSIX_FAILURE
  ********************************************************************************/

INT4
VlanPostSyncPktToVlan (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    CRU_BUF_Move_ValidOffset (pBuf, 38);

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (pVlanQMsg == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                         "No memory for VLAN Q Message !!!!!! \n");
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        return ICCH_FAILURE;
    }

    MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_ICCH_SYNC_MSG;
    pVlanQMsg->IcchData.pIcchMsg = pBuf;
    pVlanQMsg->IcchData.u2DataLen = VLAN_ICCH_SYNC_MSG_LEN;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         ALL_FAILURE_TRC, "ICCH Message enqueue FAILED\n");
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, (UINT1 *) pVlanQMsg);
        return ICCH_FAILURE;
    }

    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         ALL_FAILURE_TRC, "ICCH Event send FAILED\n");

        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : VlanApiSetVlanMacLearningStatus                      */
/*                                                                           */
/* Description        : This Api is used to enable / diasable Mac-leraing    */
/*                      status for a VLAN                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u2VlanId - VLAN Index                                */
/*                      u1MacLearningStatus - VLAN_ENABLE / VLAN_DISABLE     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanApiSetVlanMacLearningStatus (UINT4 u4ContextId, tVlanId u2VlanId,
                                 UINT1 u1MacLearningStatus)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4FdbId = 0;
    UINT1              *pu1LocalPortList = NULL;

    VLAN_LOCK ();
    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanApiSetVlanMacLearningStatus : Memory allocation failed"
                  " for tLocalPortList\n");
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));

    VlanSelectContext (u4ContextId);

    pStVlanEntry = VlanGetStaticVlanEntry (u2VlanId);
    if (pStVlanEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanApiSetVlanMacLearningStatus : pStVlanEntry"
                  " is NULL\n");
        VlanReleaseContext ();
        UtilPlstReleaseLocalPortList (pu1LocalPortList);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);
    if (VlanHwMacLearningStatus (u4ContextId,
                                 u2VlanId,
                                 pu1LocalPortList,
                                 u1MacLearningStatus) != VLAN_SUCCESS)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanApiSetVlanMacLearningStatus : VlanHwMacLearningStatus"
                  " failed\n");
        VlanReleaseContext ();
        UtilPlstReleaseLocalPortList (pu1LocalPortList);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (u2VlanId) = u1MacLearningStatus;

    /* If the oper mac-learning is disabled then FDB entries should
     * be flushed */
    if (u1MacLearningStatus == VLAN_DISABLED)
    {
        VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId);
        VlanMiFlushFdbId (u4ContextId, u4FdbId);
    }

    VlanReleaseContext ();
    UtilPlstReleaseLocalPortList (pu1LocalPortList);

    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanApiIsOnlyUntaggedPorts                           */
/*                                                                           */
/* Description        : This Api is used to check whether the VLAN is having */
/*                      only untagged ports                                  */
/*                                                                           */
/* Input(s)           : u2VlanId - VLAN Index                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE / VLAN_FALSE                               */
/*****************************************************************************/
INT4
VlanApiIsOnlyUntaggedPorts (tVlanId VlanId)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tLocalPortList      ResultList;

    VLAN_LOCK ();
    VlanSelectContext (VLAN_DEFAULT_CONTEXT_ID);

    MEMSET (ResultList, 0, sizeof (tLocalPortList));

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if ((pStVlanEntry != NULL) && (pVlanEntry != NULL))
    {
        VLAN_XOR_PORT_LIST (ResultList, pVlanEntry->EgressPorts,
                            pStVlanEntry->UnTagPorts);

        if (VLAN_IS_NULL_PORTLIST (ResultList) == VLAN_TRUE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_TRUE;
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FALSE;
}

/*****************************************************************************/
/* Function Name      : VlanApiIsPvidVlanOfAnyPorts                          */
/*                                                                           */
/* Description        : This Api is used to check whether the VLAN is PVID   */
/*                      of any ports                                         */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Index                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE / VLAN_FALSE                               */
/*****************************************************************************/
INT4
VlanApiIsPvidVlanOfAnyPorts (tVlanId VlanId)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port = 0;

    VLAN_LOCK ();
    VlanSelectContext (VLAN_DEFAULT_CONTEXT_ID);
    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
        {
            break;
        }

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if ((pVlanPortEntry != NULL) && (pVlanPortEntry->Pvid == VlanId))
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_TRUE;
        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckMemberPort                              */
/*                                                                           */
/*    Description         : This function called by CFA module to verify    */
/*                          the given port is member of the given vlan       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Tag Info to be filled in PDU       */
/*                          u4IfIndex   - Port Index to be verified          */
/*                          u4VlanId    - VlanId                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckMemberPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4VlanId)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    UINT2               u2LocalPort;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_IS_MEMBER_PORT (pVlanEntry->EgressPorts, u2LocalPort, u1Result);
    }
    else
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (u1Result == VLAN_FALSE)
    {

        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/**** UNKNOWN_MULTICAST_CHANGE - START ****/

/*****************************************************************************/
/* Function Name      : VlanGetEgressPorts                                   */
/*                                                                           */
/* Description        : This function will return EgressPorts of the         */
/*                      given VLAN.                                          */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      PortBitmap - Portlist to be updated.                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : NONE                                                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCCESS/OSIX_FAILURE                           */
/******************************************************************************/

INT4
VlanGetEgressPorts (tVlanId VlanId, tLocalPortList * pPortList)
{

    tPortList           UntagPortList;
    INT4                i44RetVal = 0;

    i44RetVal = VlanGetVlanMemberPorts (VlanId, *pPortList, UntagPortList);
    UNUSED_PARAM (i44RetVal);

    return VLAN_SUCCESS;
}

/**** UNKNOWN_MULTICAST_CHANGE - END ****/

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetForwardUnregPorts                         */
/*                                                                           */
/*    Description         : This function is called to update the Router     */
/*                          Port list of IGS into Vlan Forwarding Database   */
/*                                                                           */
/*    Input(s)            : VlanIndex - Vlan Identifier                      */
/*                          pPortList - Port List                            */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSetForwardUnregPorts (UINT4 VlanIndex, UINT1 *pu1PortList)
{
    UINT4               u4ContextId = 0;
    tSNMP_OCTET_STRING_TYPE PortList;

    MEMSET (&PortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PortList.i4_Length = VLAN_PORT_LIST_SIZE;
    PortList.pu1_OctetList = pu1PortList;

    if (VlanSetForwardUnregStaticPorts (VlanIndex, &PortList) == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " VlanSetForwardUnregStaticPorts() returns failure \r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

 /***************************************************************************
  *
  *    Function Name       : VlanApiIsVlanExists
  *
  *    Description         : This function is used to check whether VLAN is
  *                          already present.
  *
  *
  *    Input(s)            : Vlan ID - Vlan Identifier
  *
  *    Output(s)           : None
  *
  *    Global Var Referred : None
  *
  *    Global Var Modified : None
  *
  *    Use of Recursion    : None.
  *
  *    Exceptions or Operating
  *    System Error Handling    : None.
  *
  *
  *    Returns                  : VLAN_TRUE / VLAN_FALSE
  *
  *****************************************************************************/

UINT1
VlanApiIsVlanExists (tVlanId VlanId)
{

    tVlanCurrEntry     *pVlanEntry = NULL;

    VLAN_LOCK ();
    pVlanEntry = VlanGetVlanEntry ((tVlanId) VlanId);

    if (pVlanEntry == NULL)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    VLAN_UNLOCK ();
    return VLAN_TRUE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanApiFlushFdbId                                       */
/*                                                                            */
/* Description      : This function flushes the Fdb entires in FdbId          */
/*                     for the given context                                  */
/*                                                                            */
/*  Input(s)        : u4ContextId -  Context Id                               */
/*                    u4FdbId - FDB Id                                        */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS/VLAN_FAILURE                               */
/******************************************************************************/
INT4
VlanApiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId)
{
    INT4                i4RetVal = VLAN_FAILURE;
    VLAN_LOCK ();
    VlanSelectContext (u4ContextId);

    i4RetVal = VlanMiFlushFdbId (u4ContextId, u4FdbId);

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return i4RetVal;
}
