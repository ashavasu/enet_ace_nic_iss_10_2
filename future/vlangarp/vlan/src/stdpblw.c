/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpblw.c,v 1.3 2011/10/25 10:39:08 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"

/* LOW LEVEL Routines for Table : Ieee8021PbVidTranslationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbVidTranslationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbVidTranslationTable (UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       INT4
                                                       i4Ieee8021PbVidTranslationLocalVid)
{

    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceDot1adMIVidTranslationTable
            (u4Ieee8021BridgeBasePort, i4Ieee8021PbVidTranslationLocalVid));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbVidTranslationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbVidTranslationTable (UINT4
                                               *pu4Ieee8021BridgeBasePortComponentId,
                                               UINT4 *pu4Ieee8021BridgeBasePort,
                                               INT4
                                               *pi4Ieee8021PbVidTranslationLocalVid)
{

    if (nmhGetFirstIndexDot1adMIVidTranslationTable
        ((INT4 *) pu4Ieee8021BridgeBasePort,
         pi4Ieee8021PbVidTranslationLocalVid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VlanGetCompIdFromIfIndex
        (*pu4Ieee8021BridgeBasePort,
         pu4Ieee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbVidTranslationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid
                nextIeee8021PbVidTranslationLocalVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbVidTranslationTable (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4
                                              *pu4NextIeee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              UINT4
                                              *pu4NextIeee8021BridgeBasePort,
                                              INT4
                                              i4Ieee8021PbVidTranslationLocalVid,
                                              INT4
                                              *pi4NextIeee8021PbVidTranslationLocalVid)
{

    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1adMIVidTranslationTable
        ((INT4) u4Ieee8021BridgeBasePort,
         (INT4 *) pu4NextIeee8021BridgeBasePort,
         i4Ieee8021PbVidTranslationLocalVid,
         pi4NextIeee8021PbVidTranslationLocalVid) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanGetCompIdFromIfIndex
        (*pu4NextIeee8021BridgeBasePort,
         pu4NextIeee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbVidTranslationRelayVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid

                The Object 
                retValIeee8021PbVidTranslationRelayVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbVidTranslationRelayVid (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4 i4Ieee8021PbVidTranslationLocalVid,
                                        INT4
                                        *pi4RetValIeee8021PbVidTranslationRelayVid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMIVidTranslationRelayVid (u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021PbVidTranslationLocalVid,
                                                  pi4RetValIeee8021PbVidTranslationRelayVid));
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbVidTranslationRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid

                The Object 
                retValIeee8021PbVidTranslationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbVidTranslationRowStatus (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         i4Ieee8021PbVidTranslationLocalVid,
                                         INT4
                                         *pi4RetValIeee8021PbVidTranslationRowStatus)
{

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMIVidTranslationRowStatus (u4Ieee8021BridgeBasePort,
                                                   i4Ieee8021PbVidTranslationLocalVid,
                                                   pi4RetValIeee8021PbVidTranslationRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbVidTranslationRelayVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid

                The Object 
                setValIeee8021PbVidTranslationRelayVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbVidTranslationRelayVid (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4 i4Ieee8021PbVidTranslationLocalVid,
                                        INT4
                                        i4SetValIeee8021PbVidTranslationRelayVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIVidTranslationRelayVid (u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021PbVidTranslationLocalVid,
                                                  i4SetValIeee8021PbVidTranslationRelayVid));
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbVidTranslationRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid

                The Object 
                setValIeee8021PbVidTranslationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbVidTranslationRowStatus (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         i4Ieee8021PbVidTranslationLocalVid,
                                         INT4
                                         i4SetValIeee8021PbVidTranslationRowStatus)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIVidTranslationRowStatus (u4Ieee8021BridgeBasePort,
                                                   i4Ieee8021PbVidTranslationLocalVid,
                                                   i4SetValIeee8021PbVidTranslationRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbVidTranslationRelayVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid

                The Object 
                testValIeee8021PbVidTranslationRelayVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbVidTranslationRelayVid (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021PbVidTranslationLocalVid,
                                           INT4
                                           i4TestValIeee8021PbVidTranslationRelayVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2Dot1adMIVidTranslationRelayVid (pu4ErrorCode,
                                                     u4Ieee8021BridgeBasePort,
                                                     i4Ieee8021PbVidTranslationLocalVid,
                                                     i4TestValIeee8021PbVidTranslationRelayVid));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbVidTranslationRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid

                The Object 
                testValIeee8021PbVidTranslationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbVidTranslationRowStatus (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021PbVidTranslationLocalVid,
                                            INT4
                                            i4TestValIeee8021PbVidTranslationRowStatus)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2Dot1adMIVidTranslationRowStatus (pu4ErrorCode,
                                                      u4Ieee8021BridgeBasePort,
                                                      i4Ieee8021PbVidTranslationLocalVid,
                                                      i4TestValIeee8021PbVidTranslationRowStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbVidTranslationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbVidTranslationLocalVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbVidTranslationTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbCVidRegistrationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbCVidRegistrationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbCVidRegistrationTable (UINT4
                                                         u4Ieee8021BridgeBasePortComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeBasePort,
                                                         INT4
                                                         i4Ieee8021PbCVidRegistrationCVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
            (u4Ieee8021BridgeBasePort, i4Ieee8021PbCVidRegistrationCVid));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbCVidRegistrationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbCVidRegistrationTable (UINT4
                                                 *pu4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgeBasePort,
                                                 INT4
                                                 *pi4Ieee8021PbCVidRegistrationCVid)
{

    if (nmhGetFirstIndexDot1adMICVidRegistrationTable
        ((INT4 *) pu4Ieee8021BridgeBasePort,
         pi4Ieee8021PbCVidRegistrationCVid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetCompIdFromIfIndex
        (*pu4Ieee8021BridgeBasePort,
         pu4Ieee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbCVidRegistrationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid
                nextIeee8021PbCVidRegistrationCVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbCVidRegistrationTable (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePort,
                                                INT4
                                                i4Ieee8021PbCVidRegistrationCVid,
                                                INT4
                                                *pi4NextIeee8021PbCVidRegistrationCVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1adMICVidRegistrationTable
        ((INT4) u4Ieee8021BridgeBasePort,
         (INT4 *) pu4NextIeee8021BridgeBasePort,
         i4Ieee8021PbCVidRegistrationCVid,
         pi4NextIeee8021PbCVidRegistrationCVid) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetCompIdFromIfIndex
        (*pu4NextIeee8021BridgeBasePort,
         pu4NextIeee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbCVidRegistrationSVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                retValIeee8021PbCVidRegistrationSVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCVidRegistrationSVid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4 i4Ieee8021PbCVidRegistrationCVid,
                                      INT4
                                      *pi4RetValIeee8021PbCVidRegistrationSVid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMICVidRegistrationSVid (u4Ieee8021BridgeBasePort,
                                                i4Ieee8021PbCVidRegistrationCVid,
                                                pi4RetValIeee8021PbCVidRegistrationSVid));
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCVidRegistrationUntaggedPep
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                retValIeee8021PbCVidRegistrationUntaggedPep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCVidRegistrationUntaggedPep (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4Ieee8021PbCVidRegistrationCVid,
                                             INT4
                                             *pi4RetValIeee8021PbCVidRegistrationUntaggedPep)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMICVidRegistrationUntaggedPep (u4Ieee8021BridgeBasePort,
                                                       i4Ieee8021PbCVidRegistrationCVid,
                                                       pi4RetValIeee8021PbCVidRegistrationUntaggedPep));
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCVidRegistrationUntaggedCep
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                retValIeee8021PbCVidRegistrationUntaggedCep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCVidRegistrationUntaggedCep (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4Ieee8021PbCVidRegistrationCVid,
                                             INT4
                                             *pi4RetValIeee8021PbCVidRegistrationUntaggedCep)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMICVidRegistrationUntaggedCep (u4Ieee8021BridgeBasePort,
                                                       i4Ieee8021PbCVidRegistrationCVid,
                                                       pi4RetValIeee8021PbCVidRegistrationUntaggedCep));

}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCVidRegistrationRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                retValIeee8021PbCVidRegistrationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCVidRegistrationRowStatus (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021PbCVidRegistrationCVid,
                                           INT4
                                           *pi4RetValIeee8021PbCVidRegistrationRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMICVidRegistrationRowStatus (u4Ieee8021BridgeBasePort,
                                                     i4Ieee8021PbCVidRegistrationCVid,
                                                     pi4RetValIeee8021PbCVidRegistrationRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbCVidRegistrationSVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                setValIeee8021PbCVidRegistrationSVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCVidRegistrationSVid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4 i4Ieee8021PbCVidRegistrationCVid,
                                      INT4
                                      i4SetValIeee8021PbCVidRegistrationSVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }
    return (nmhSetDot1adMICVidRegistrationSVid (u4Ieee8021BridgeBasePort,
                                                i4Ieee8021PbCVidRegistrationCVid,
                                                i4SetValIeee8021PbCVidRegistrationSVid));
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbCVidRegistrationUntaggedPep
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                setValIeee8021PbCVidRegistrationUntaggedPep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCVidRegistrationUntaggedPep (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4Ieee8021PbCVidRegistrationCVid,
                                             INT4
                                             i4SetValIeee8021PbCVidRegistrationUntaggedPep)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }
    return (nmhSetDot1adMICVidRegistrationUntaggedPep (u4Ieee8021BridgeBasePort,
                                                       i4Ieee8021PbCVidRegistrationCVid,
                                                       i4SetValIeee8021PbCVidRegistrationUntaggedPep));
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbCVidRegistrationUntaggedCep
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                setValIeee8021PbCVidRegistrationUntaggedCep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCVidRegistrationUntaggedCep (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4Ieee8021PbCVidRegistrationCVid,
                                             INT4
                                             i4SetValIeee8021PbCVidRegistrationUntaggedCep)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }
    return (nmhSetDot1adMICVidRegistrationUntaggedCep (u4Ieee8021BridgeBasePort,
                                                       i4Ieee8021PbCVidRegistrationCVid,
                                                       i4SetValIeee8021PbCVidRegistrationUntaggedCep));
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbCVidRegistrationRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                setValIeee8021PbCVidRegistrationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCVidRegistrationRowStatus (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021PbCVidRegistrationCVid,
                                           INT4
                                           i4SetValIeee8021PbCVidRegistrationRowStatus)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }
    return (nmhSetDot1adMICVidRegistrationRowStatus (u4Ieee8021BridgeBasePort,
                                                     i4Ieee8021PbCVidRegistrationCVid,
                                                     i4SetValIeee8021PbCVidRegistrationRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCVidRegistrationSVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                testValIeee8021PbCVidRegistrationSVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCVidRegistrationSVid (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4 i4Ieee8021PbCVidRegistrationCVid,
                                         INT4
                                         i4TestValIeee8021PbCVidRegistrationSVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMICVidRegistrationSVid (pu4ErrorCode,
                                                   u4Ieee8021BridgeBasePort,
                                                   i4Ieee8021PbCVidRegistrationCVid,
                                                   i4TestValIeee8021PbCVidRegistrationSVid));

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCVidRegistrationUntaggedPep
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                testValIeee8021PbCVidRegistrationUntaggedPep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCVidRegistrationUntaggedPep (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4
                                                i4Ieee8021PbCVidRegistrationCVid,
                                                INT4
                                                i4TestValIeee8021PbCVidRegistrationUntaggedPep)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMICVidRegistrationUntaggedPep (pu4ErrorCode,
                                                          u4Ieee8021BridgeBasePort,
                                                          i4Ieee8021PbCVidRegistrationCVid,
                                                          i4TestValIeee8021PbCVidRegistrationUntaggedPep));

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCVidRegistrationUntaggedCep
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                testValIeee8021PbCVidRegistrationUntaggedCep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCVidRegistrationUntaggedCep (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4
                                                i4Ieee8021PbCVidRegistrationCVid,
                                                INT4
                                                i4TestValIeee8021PbCVidRegistrationUntaggedCep)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMICVidRegistrationUntaggedCep (pu4ErrorCode,
                                                          u4Ieee8021BridgeBasePort,
                                                          i4Ieee8021PbCVidRegistrationCVid,
                                                          i4TestValIeee8021PbCVidRegistrationUntaggedCep));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCVidRegistrationRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid

                The Object 
                testValIeee8021PbCVidRegistrationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCVidRegistrationRowStatus (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4Ieee8021PbCVidRegistrationCVid,
                                              INT4
                                              i4TestValIeee8021PbCVidRegistrationRowStatus)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMICVidRegistrationRowStatus (pu4ErrorCode,
                                                        u4Ieee8021BridgeBasePort,
                                                        i4Ieee8021PbCVidRegistrationCVid,
                                                        i4TestValIeee8021PbCVidRegistrationRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbCVidRegistrationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbCVidRegistrationCVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbCVidRegistrationTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbEdgePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbEdgePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbEdgePortTable (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 INT4 i4Ieee8021PbEdgePortSVid)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceDot1adMIPepTable (u4Ieee8021BridgeBasePort,
                                                      i4Ieee8021PbEdgePortSVid));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbEdgePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbEdgePortTable (UINT4
                                         *pu4Ieee8021BridgeBasePortComponentId,
                                         UINT4 *pu4Ieee8021BridgeBasePort,
                                         INT4 *pi4Ieee8021PbEdgePortSVid)
{
    if (nmhGetFirstIndexDot1adMIPepTable ((INT4 *) pu4Ieee8021BridgeBasePort,
                                          pi4Ieee8021PbEdgePortSVid) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetCompIdFromIfIndex
        (*pu4Ieee8021BridgeBasePort,
         pu4Ieee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbEdgePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021PbEdgePortSVid
                nextIeee8021PbEdgePortSVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbEdgePortTable (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4
                                        *pu4NextIeee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        UINT4 *pu4NextIeee8021BridgeBasePort,
                                        INT4 i4Ieee8021PbEdgePortSVid,
                                        INT4 *pi4NextIeee8021PbEdgePortSVid)
{

    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1adMIPepTable ((INT4) u4Ieee8021BridgeBasePort,
                                         (INT4 *) pu4NextIeee8021BridgeBasePort,
                                         i4Ieee8021PbEdgePortSVid,
                                         pi4NextIeee8021PbEdgePortSVid) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetCompIdFromIfIndex
        (*pu4NextIeee8021BridgeBasePort,
         pu4NextIeee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbEdgePortPVID
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                retValIeee8021PbEdgePortPVID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbEdgePortPVID (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 i4Ieee8021PbEdgePortSVid,
                              INT4 *pi4RetValIeee8021PbEdgePortPVID)
{

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMIPepPvid (u4Ieee8021BridgeBasePort,
                                   i4Ieee8021PbEdgePortSVid,
                                   pi4RetValIeee8021PbEdgePortPVID));

}

/****************************************************************************
 Function    :  nmhGetIeee8021PbEdgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                retValIeee8021PbEdgePortDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbEdgePortDefaultUserPriority (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4 i4Ieee8021PbEdgePortSVid,
                                             UINT4
                                             *pu4RetValIeee8021PbEdgePortDefaultUserPriority)
{

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMIPepDefaultUserPriority
            ((INT4) u4Ieee8021BridgeBasePort, i4Ieee8021PbEdgePortSVid,
             (INT4 *) pu4RetValIeee8021PbEdgePortDefaultUserPriority));

}

/****************************************************************************
 Function    :  nmhGetIeee8021PbEdgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                retValIeee8021PbEdgePortAcceptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbEdgePortAcceptableFrameTypes (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4 i4Ieee8021PbEdgePortSVid,
                                              INT4
                                              *pi4RetValIeee8021PbEdgePortAcceptableFrameTypes)
{

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    nmhGetDot1adMIPepAccptableFrameTypes (u4Ieee8021BridgeBasePort,
                                          i4Ieee8021PbEdgePortSVid,
                                          pi4RetValIeee8021PbEdgePortAcceptableFrameTypes);

    if (*pi4RetValIeee8021PbEdgePortAcceptableFrameTypes ==
        VLAN_ADMIT_ALL_FRAMES)
    {
        *pi4RetValIeee8021PbEdgePortAcceptableFrameTypes = STD_ADMIT_ALL_FRAMES;
    }

    else if (*pi4RetValIeee8021PbEdgePortAcceptableFrameTypes ==
             VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
    {
        *pi4RetValIeee8021PbEdgePortAcceptableFrameTypes =
            STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
    }

    else if (*pi4RetValIeee8021PbEdgePortAcceptableFrameTypes ==
             VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
    {
        *pi4RetValIeee8021PbEdgePortAcceptableFrameTypes =
            STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021PbEdgePortEnableIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                retValIeee8021PbEdgePortEnableIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbEdgePortEnableIngressFiltering (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4 i4Ieee8021PbEdgePortSVid,
                                                INT4
                                                *pi4RetValIeee8021PbEdgePortEnableIngressFiltering)
{

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMIPepIngressFiltering (u4Ieee8021BridgeBasePort,
                                               i4Ieee8021PbEdgePortSVid,
                                               pi4RetValIeee8021PbEdgePortEnableIngressFiltering));

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbEdgePortPVID
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                setValIeee8021PbEdgePortPVID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbEdgePortPVID (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 i4Ieee8021PbEdgePortSVid,
                              INT4 i4SetValIeee8021PbEdgePortPVID)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIPepPvid (u4Ieee8021BridgeBasePort,
                                   i4Ieee8021PbEdgePortSVid,
                                   i4SetValIeee8021PbEdgePortPVID));

}

/****************************************************************************
 Function    :  nmhSetIeee8021PbEdgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                setValIeee8021PbEdgePortDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbEdgePortDefaultUserPriority (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4 i4Ieee8021PbEdgePortSVid,
                                             UINT4
                                             u4SetValIeee8021PbEdgePortDefaultUserPriority)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIPepDefaultUserPriority (u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021PbEdgePortSVid,
                                                  u4SetValIeee8021PbEdgePortDefaultUserPriority));

}

/****************************************************************************
 Function    :  nmhSetIeee8021PbEdgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                setValIeee8021PbEdgePortAcceptableFrameTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbEdgePortAcceptableFrameTypes (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4 i4Ieee8021PbEdgePortSVid,
                                              INT4
                                              i4SetValIeee8021PbEdgePortAcceptableFrameTypes)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValIeee8021PbEdgePortAcceptableFrameTypes == STD_ADMIT_ALL_FRAMES)
    {
        i4SetValIeee8021PbEdgePortAcceptableFrameTypes = VLAN_ADMIT_ALL_FRAMES;
    }

    else if (i4SetValIeee8021PbEdgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
    {
        i4SetValIeee8021PbEdgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
    }

    else if (i4SetValIeee8021PbEdgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
    {
        i4SetValIeee8021PbEdgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    }

    else if (i4SetValIeee8021PbEdgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_FRAMES)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIPepAccptableFrameTypes (u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021PbEdgePortSVid,
                                                  i4SetValIeee8021PbEdgePortAcceptableFrameTypes));

}

/****************************************************************************
 Function    :  nmhSetIeee8021PbEdgePortEnableIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                setValIeee8021PbEdgePortEnableIngressFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbEdgePortEnableIngressFiltering (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4 i4Ieee8021PbEdgePortSVid,
                                                INT4
                                                i4SetValIeee8021PbEdgePortEnableIngressFiltering)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIPepIngressFiltering (u4Ieee8021BridgeBasePort,
                                               i4Ieee8021PbEdgePortSVid,
                                               i4SetValIeee8021PbEdgePortEnableIngressFiltering));

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbEdgePortPVID
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                testValIeee8021PbEdgePortPVID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbEdgePortPVID (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 INT4 i4Ieee8021PbEdgePortSVid,
                                 INT4 i4TestValIeee8021PbEdgePortPVID)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2Dot1adMIPepPvid (pu4ErrorCode,
                                      u4Ieee8021BridgeBasePort,
                                      i4Ieee8021PbEdgePortSVid,
                                      i4TestValIeee8021PbEdgePortPVID));

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbEdgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                testValIeee8021PbEdgePortDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbEdgePortDefaultUserPriority (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4 i4Ieee8021PbEdgePortSVid,
                                                UINT4
                                                u4TestValIeee8021PbEdgePortDefaultUserPriority)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2Dot1adMIPepDefaultUserPriority (pu4ErrorCode,
                                                     u4Ieee8021BridgeBasePort,
                                                     i4Ieee8021PbEdgePortSVid,
                                                     u4TestValIeee8021PbEdgePortDefaultUserPriority));

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbEdgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                testValIeee8021PbEdgePortAcceptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbEdgePortAcceptableFrameTypes (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 INT4 i4Ieee8021PbEdgePortSVid,
                                                 INT4
                                                 i4TestValIeee8021PbEdgePortAcceptableFrameTypes)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021PbEdgePortAcceptableFrameTypes == STD_ADMIT_ALL_FRAMES)
    {
        i4TestValIeee8021PbEdgePortAcceptableFrameTypes = VLAN_ADMIT_ALL_FRAMES;
    }

    else if (i4TestValIeee8021PbEdgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
    {
        i4TestValIeee8021PbEdgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
    }

    else if (i4TestValIeee8021PbEdgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
    {
        i4TestValIeee8021PbEdgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    }

    else if (i4TestValIeee8021PbEdgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_FRAMES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    nmhTestv2Dot1adMIPepAccptableFrameTypes (pu4ErrorCode,
                                             u4Ieee8021BridgeBasePort,
                                             i4Ieee8021PbEdgePortSVid,
                                             i4TestValIeee8021PbEdgePortAcceptableFrameTypes);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbEdgePortEnableIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid

                The Object 
                testValIeee8021PbEdgePortEnableIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbEdgePortEnableIngressFiltering (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021BridgeBasePortComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeBasePort,
                                                   INT4
                                                   i4Ieee8021PbEdgePortSVid,
                                                   INT4
                                                   i4TestValIeee8021PbEdgePortEnableIngressFiltering)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2Dot1adMIPepIngressFiltering (pu4ErrorCode,
                                                  u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021PbEdgePortSVid,
                                                  i4TestValIeee8021PbEdgePortEnableIngressFiltering));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbEdgePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbEdgePortSVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbEdgePortTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbServicePriorityRegenerationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbServicePriorityRegenerationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbServicePriorityRegenerationTable (UINT4
                                                                    u4Ieee8021BridgeBasePortComponentId,
                                                                    UINT4
                                                                    u4Ieee8021BridgeBasePort,
                                                                    INT4
                                                                    i4Ieee8021PbServicePriorityRegenerationSVid,
                                                                    UINT4
                                                                    u4Ieee8021PbServicePriorityRegenerationReceivedPriority)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceDot1adMIServicePriorityRegenerationTable
            (u4Ieee8021BridgeBasePort,
             i4Ieee8021PbServicePriorityRegenerationSVid,
             u4Ieee8021PbServicePriorityRegenerationReceivedPriority));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbServicePriorityRegenerationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbServicePriorityRegenerationTable (UINT4
                                                            *pu4Ieee8021BridgeBasePortComponentId,
                                                            UINT4
                                                            *pu4Ieee8021BridgeBasePort,
                                                            INT4
                                                            *pi4Ieee8021PbServicePriorityRegenerationSVid,
                                                            UINT4
                                                            *pu4Ieee8021PbServicePriorityRegenerationReceivedPriority)
{
    if (nmhGetFirstIndexDot1adMIServicePriorityRegenerationTable
        ((INT4 *) pu4Ieee8021BridgeBasePort,
         pi4Ieee8021PbServicePriorityRegenerationSVid,
         (INT4 *) pu4Ieee8021PbServicePriorityRegenerationReceivedPriority) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetCompIdFromIfIndex
        (*pu4Ieee8021BridgeBasePort,
         pu4Ieee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbServicePriorityRegenerationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                nextIeee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority
                nextIeee8021PbServicePriorityRegenerationReceivedPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbServicePriorityRegenerationTable (UINT4
                                                           u4Ieee8021BridgeBasePortComponentId,
                                                           UINT4
                                                           *pu4NextIeee8021BridgeBasePortComponentId,
                                                           UINT4
                                                           u4Ieee8021BridgeBasePort,
                                                           UINT4
                                                           *pu4NextIeee8021BridgeBasePort,
                                                           INT4
                                                           i4Ieee8021PbServicePriorityRegenerationSVid,
                                                           INT4
                                                           *pi4NextIeee8021PbServicePriorityRegenerationSVid,
                                                           UINT4
                                                           u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
                                                           UINT4
                                                           *pu4NextIeee8021PbServicePriorityRegenerationReceivedPriority)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1adMIServicePriorityRegenerationTable
        ((INT4) u4Ieee8021BridgeBasePort,
         (INT4 *) pu4NextIeee8021BridgeBasePort,
         i4Ieee8021PbServicePriorityRegenerationSVid,
         pi4NextIeee8021PbServicePriorityRegenerationSVid,
         (INT4) u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
         (INT4 *) pu4NextIeee8021PbServicePriorityRegenerationReceivedPriority)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetCompIdFromIfIndex
        (*pu4NextIeee8021BridgeBasePort,
         pu4NextIeee8021BridgeBasePortComponentId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbServicePriorityRegenerationRegeneratedPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority

                The Object 
                retValIeee8021PbServicePriorityRegenerationRegeneratedPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbServicePriorityRegenerationRegeneratedPriority (UINT4
                                                                u4Ieee8021BridgeBasePortComponentId,
                                                                UINT4
                                                                u4Ieee8021BridgeBasePort,
                                                                INT4
                                                                i4Ieee8021PbServicePriorityRegenerationSVid,
                                                                UINT4
                                                                u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
                                                                UINT4
                                                                *pu4RetValIeee8021PbServicePriorityRegenerationRegeneratedPriority)
{

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    return (nmhGetDot1adMIServicePriorityRegenRegeneratedPriority
            ((INT4) u4Ieee8021BridgeBasePort,
             i4Ieee8021PbServicePriorityRegenerationSVid,
             (INT4) u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
             (INT4 *)
             pu4RetValIeee8021PbServicePriorityRegenerationRegeneratedPriority));

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbServicePriorityRegenerationRegeneratedPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority

                The Object 
                setValIeee8021PbServicePriorityRegenerationRegeneratedPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbServicePriorityRegenerationRegeneratedPriority (UINT4
                                                                u4Ieee8021BridgeBasePortComponentId,
                                                                UINT4
                                                                u4Ieee8021BridgeBasePort,
                                                                INT4
                                                                i4Ieee8021PbServicePriorityRegenerationSVid,
                                                                UINT4
                                                                u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
                                                                UINT4
                                                                u4SetValIeee8021PbServicePriorityRegenerationRegeneratedPriority)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetDot1adMIServicePriorityRegenRegeneratedPriority
            (u4Ieee8021BridgeBasePort,
             i4Ieee8021PbServicePriorityRegenerationSVid,
             u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
             u4SetValIeee8021PbServicePriorityRegenerationRegeneratedPriority));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbServicePriorityRegenerationRegeneratedPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority

                The Object 
                testValIeee8021PbServicePriorityRegenerationRegeneratedPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbServicePriorityRegenerationRegeneratedPriority (UINT4
                                                                   *pu4ErrorCode,
                                                                   UINT4
                                                                   u4Ieee8021BridgeBasePortComponentId,
                                                                   UINT4
                                                                   u4Ieee8021BridgeBasePort,
                                                                   INT4
                                                                   i4Ieee8021PbServicePriorityRegenerationSVid,
                                                                   UINT4
                                                                   u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
                                                                   UINT4
                                                                   u4TestValIeee8021PbServicePriorityRegenerationRegeneratedPriority)
{
    if (VlanIsValidCompId
        (u4Ieee8021BridgeBasePortComponentId,
         u4Ieee8021BridgeBasePort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2Dot1adMIServicePriorityRegenRegeneratedPriority
            (pu4ErrorCode, u4Ieee8021BridgeBasePort,
             i4Ieee8021PbServicePriorityRegenerationSVid,
             u4Ieee8021PbServicePriorityRegenerationReceivedPriority,
             u4TestValIeee8021PbServicePriorityRegenerationRegeneratedPriority));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbServicePriorityRegenerationTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021PbServicePriorityRegenerationSVid
                Ieee8021PbServicePriorityRegenerationReceivedPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbServicePriorityRegenerationTable (UINT4 *pu4ErrorCode,
                                                    tSnmpIndexList *
                                                    pSnmpIndexList,
                                                    tSNMP_VAR_BIND *
                                                    pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbCnpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbCnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbCnpTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbCnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbCnpTable (UINT4 *pu4Ieee8021BridgeBasePortComponentId,
                                    UINT4 *pu4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbCnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbCnpTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4
                                   *pu4NextIeee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   UINT4 *pu4NextIeee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbCnpCComponentId
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbCnpCComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCnpCComponentId (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 UINT4 *pu4RetValIeee8021PbCnpCComponentId)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4RetValIeee8021PbCnpCComponentId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCnpSVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbCnpSVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCnpSVid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                         UINT4 u4Ieee8021BridgeBasePort,
                         INT4 *pi4RetValIeee8021PbCnpSVid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021PbCnpSVid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCnpRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbCnpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCnpRowStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 *pi4RetValIeee8021PbCnpRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021PbCnpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbCnpCComponentId
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021PbCnpCComponentId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCnpCComponentId (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 UINT4 u4SetValIeee8021PbCnpCComponentId)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4SetValIeee8021PbCnpCComponentId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbCnpSVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021PbCnpSVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCnpSVid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                         UINT4 u4Ieee8021BridgeBasePort,
                         INT4 i4SetValIeee8021PbCnpSVid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021PbCnpSVid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbCnpRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021PbCnpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCnpRowStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 i4SetValIeee8021PbCnpRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021PbCnpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCnpCComponentId
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021PbCnpCComponentId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCnpCComponentId (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021BridgeBasePortComponentId,
                                    UINT4 u4Ieee8021BridgeBasePort,
                                    UINT4 u4TestValIeee8021PbCnpCComponentId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4TestValIeee8021PbCnpCComponentId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCnpSVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021PbCnpSVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCnpSVid (UINT4 *pu4ErrorCode,
                            UINT4 u4Ieee8021BridgeBasePortComponentId,
                            UINT4 u4Ieee8021BridgeBasePort,
                            INT4 i4TestValIeee8021PbCnpSVid)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021PbCnpSVid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCnpRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021PbCnpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCnpRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 INT4 i4TestValIeee8021PbCnpRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021PbCnpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbCnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbCnpTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbPnpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbPnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbPnpTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbPnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbPnpTable (UINT4 *pu4Ieee8021BridgeBasePortComponentId,
                                    UINT4 *pu4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbPnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbPnpTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4
                                   *pu4NextIeee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   UINT4 *pu4NextIeee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbPnpRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbPnpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbPnpRowStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 *pi4RetValIeee8021PbPnpRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021PbPnpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbPnpRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021PbPnpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbPnpRowStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 i4SetValIeee8021PbPnpRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021PbPnpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbPnpRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021PbPnpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbPnpRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 INT4 i4TestValIeee8021PbPnpRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021PbPnpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbPnpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbPnpTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbCepTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbCepTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbCepTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbCepTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbCepTable (UINT4 *pu4Ieee8021BridgeBasePortComponentId,
                                    UINT4 *pu4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbCepTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbCepTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4
                                   *pu4NextIeee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   UINT4 *pu4NextIeee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbCepCComponentId
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbCepCComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCepCComponentId (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 UINT4 *pu4RetValIeee8021PbCepCComponentId)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4RetValIeee8021PbCepCComponentId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCepCepPortNumber
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbCepCepPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCepCepPortNumber (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  UINT4 *pu4RetValIeee8021PbCepCepPortNumber)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4RetValIeee8021PbCepCepPortNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbCepRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021PbCepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbCepRowStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 *pi4RetValIeee8021PbCepRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021PbCepRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbCepRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021PbCepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbCepRowStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              INT4 i4SetValIeee8021PbCepRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021PbCepRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbCepRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021PbCepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbCepRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 INT4 i4TestValIeee8021PbCepRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021PbCepRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbCepTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbCepTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
