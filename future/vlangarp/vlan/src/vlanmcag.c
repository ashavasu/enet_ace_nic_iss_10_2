/***********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanmcag.c,v 1.8 2015/11/26 11:00:04 siva Exp $
 *
 * Description     : This file contains MAC Aging Handler
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : vlanmcag.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Mar 2015                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/

#include "vlaninc.h"

#ifdef ICCH_WANTED

/******************************************************************************/
/*                                                                            */
/*  Function Name   : McagMain                                                */
/*                                                                            */
/*  Description     : This function is the main routine to handle             */
/*                    MAC Aging Indication.                                   */
/*                                                                            */
/*  Input(s)        : piParam - Parameter                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
McagMain (INT1 *pi1Param)
{
    UINT4               u4Events;
    tMcagQMsg          *pMcagQMsg = NULL;

    UNUSED_PARAM (pi1Param);

    if (McagTaskInit () != VLAN_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        VLAN_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (VLAN_GET_TASK_ID (VLAN_SELF, MCAG_TASK,&(MCAG_TASK_ID)) !=
        OSIX_SUCCESS)
    {
        McagMainDeInit ();
        VLAN_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    VLAN_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if ((VLAN_RECEIVE_EVENT (MCAG_TASK_ID, (VLAN_MAC_AGING_EVENT
                                                |VLAN_MAC_AGING_TIMER_EXP_EVENT),
                                 OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {
            /*Take MAC aging Semaphore*/
            MCAG_LOCK ();

            if (u4Events & VLAN_MAC_AGING_EVENT)
            {
                /*Call the MAC Aging Handler*/
                while (VLAN_RECV_FROM_QUEUE (MCAG_TASK_QUEUE_ID,
                            (UINT1 *) &pMcagQMsg,
                            OSIX_DEF_MSG_LEN,
                            OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    VlanMcagHandleAgingEvent(pMcagQMsg);
                    VLAN_RELEASE_BUF (VLAN_MCAG_QMSG_BUFF, pMcagQMsg);
                }
            }

            if (u4Events & VLAN_MAC_AGING_TIMER_EXP_EVENT)
            {
                /*Notify Application about MAC aging*/
                VlanMcagHandleMacAgingTmrExpiry();
            }

            /*Give Semaphore*/
            MCAG_UNLOCK ();
        }
    }

}
/************************************************************************/
/* Function Name    : McagTaskInit                                      */
/*                                                                      */
/* Description      : Creates Mac Aging Task Queue and Semaphore        */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gMcagTaskInfo                                     */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gMcagTaskInfo                                     */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VLAN_SUCCESS / VLAN_FAILURE                       */
/************************************************************************/
INT4
McagTaskInit (VOID)
{

    VLAN_MEMSET (&(gMcagTaskInfo), 0, sizeof (tMcagTaskInfo));

    if (VLAN_CREATE_TMR_LIST (MCAG_TASK, VLAN_MAC_AGING_TIMER_EXP_EVENT,
                              NULL, &(gMcagTaskInfo.MacAgingTmrListId))
        != TMR_SUCCESS)
    {
        MCAG_TRC (CRITICAL_DEFAULT_TRC, "VLAN Timer creation Failed \r\n");
        return VLAN_FAILURE;
    }

    if (VLAN_CREATE_SEM (MCAG_SEM_NAME, 1, 0, &(MCAG_SEM_ID)) != OSIX_SUCCESS)
    {
        if (gMcagTaskInfo.MacAgingTmrListId != 0)
        {
            VLAN_DELETE_TMR_LIST (gMcagTaskInfo.MacAgingTmrListId);
        }

        MCAG_TRC (CRITICAL_DEFAULT_TRC, "Failed to Create MACA Mutual "
                                        "Exclusion Sema4 \r\n");
        return VLAN_FAILURE;
    }

    if (VLAN_CREATE_QUEUE (MCAG_TASK_QUEUE, OSIX_MAX_Q_MSG_LEN, MCAG_Q_DEPTH,
                           &(MCAG_TASK_QUEUE_ID)) != OSIX_SUCCESS)
    {
       MCAG_TRC (CRITICAL_DEFAULT_TRC, "Failed to create queue for MACA Task \r\n");

       if (gMcagTaskInfo.MacAgingTmrListId != 0)
        {
            VLAN_DELETE_TMR_LIST (gMcagTaskInfo.MacAgingTmrListId);
        }
        VLAN_DELETE_SEM (VLAN_SEM_ID);
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}
/************************************************************************/
/* Function Name    : McagMainDeInit                                    */
/*                                                                      */
/* Description      : Deletes MACA Task Queue, Sema4,                   */
/*                    and global informations                           */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gMcagTaskInfo                                     */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
McagMainDeInit (VOID)
{
    VlanMcagDelMacAgingEntries();
    if (gMcagTaskInfo.MacAgingTmrListId != 0)
    {
        VLAN_DELETE_TMR_LIST (gMcagTaskInfo.MacAgingTmrListId);
    }
    VLAN_DELETE_QUEUE (MCAG_TASK_QUEUE_ID);
    VLAN_DELETE_SEM (MCAG_SEM_ID);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMcagHandleAgingEvent                         */
/*                                                                           */
/*    Description         : This function handles MAC Address Aging Event    */
/*                                                                           */
/*    Input(s)            : tMcagQMsg - Queue Message Structure              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMcagEntries                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID VlanMcagHandleAgingEvent(tMcagQMsg *pMcagQMsg)
{
    tVlanMcagEntries  *pVlanMcagEntries = NULL;

    pVlanMcagEntries =
        (tVlanMcagEntries *) (VOID *)
        VLAN_GET_BUF (VLAN_MCAG_MAC_AGING_ENTRIES, 0);

    if (pVlanMcagEntries == NULL)
    {
        MCAG_TRC (CRITICAL_DEFAULT_TRC, "Memory Allocation failed for"
                                       " VLAN MAC Aging Entry \r\n");
#ifdef SW_LEARNING
        VlanFdbRemoveLocalOrRemote (pMcagQMsg->FdbData.VlanId,
                                    pMcagQMsg->FdbData.au1MacAddr);
#endif 
        VlanHwDelStaticUcastEntry (pMcagQMsg->FdbData.VlanId,
                                   pMcagQMsg->FdbData.au1MacAddr,
                                   VLAN_DEF_RECVPORT);
        return;
    }

    MEMSET (pVlanMcagEntries, 0, sizeof (tVlanMcagEntries));

    VLAN_SLL_INIT_NODE (&(pVlanMcagEntries->NextNode));

    pVlanMcagEntries->FdbData.VlanId = pMcagQMsg->FdbData.VlanId;

    MEMCPY (pVlanMcagEntries->FdbData.au1MacAddr,
            pMcagQMsg->FdbData.au1MacAddr, MAC_ADDR_LEN);

    VLAN_SLL_ADD (&gVlanMcagEntries,
                  (tTMO_SLL_NODE *) & (pVlanMcagEntries->NextNode));

    if (gVlanMcagEntries.u4_Count == VLAN_MCAG_MAX_MAC_AGING_ENTRIES)
    {
        /*Notify ICCH Module about MAC Aging Entries*/
        if (VlanIcchFormHitBitRequest() != VLAN_SUCCESS)
	{
	     /* Form hit bit request will return failure when there is 
	      * no memory for CRU Buffer allocation.
	      * In such case, delete all the entries in SLL maintained by
	      * mac aging task. These entries would have been programmed 
	      * in HW as dynamic entry. There is no need to delete the 
	      * programmed entry as this will be handled again on next age out.
	      */
	      VlanMcagDelMacAgingEntries();
	}
    }
    /*If the timer is not running and there is atleast one entry in DS,
     *start the timer*/
    if ((gMcagTimer.u1IsTmrActive == VLAN_FALSE) &&
        (gVlanMcagEntries.u4_Count != VLAN_INIT_VAL ))
    {
       MacAgingStartTimer (VLAN_INIT_VAL, VLAN_MAC_AGING_TIME);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMcagHandleMacAgingTmrExpiry                  */
/*                                                                           */
/*    Description         : This function handles MAC Address Aging Timer    */
/*                         Expiry                                            */
/*                                                                           */
/*    Input(s)            : tMcagQMsg - Queue Message Structure              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMcagEntries                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID VlanMcagHandleMacAgingTmrExpiry(VOID)
{

    UINT4 u4TimeOut = VLAN_MAC_AGING_TIME;
    UINT1 u1TimerType = 0;
    gMcagTimer.u1IsTmrActive = VLAN_FALSE;

    if (gVlanMcagEntries.u4_Count != VLAN_INIT_VAL)
    {
        /* Its time to send Hit Bit Request */
        if (VlanIcchFormHitBitRequest() != VLAN_SUCCESS)
	{
             /* Form hit bit request will return failure when there is
              * no memory for CRU Buffer allocation.
              * In such case, delete all the entries in SLL maintained by
              * mac aging task. These entries would have been programmed
              * in HW as dynamic entry. There is no need to delete the
              * programmed entry as this will be handled again on next age out.
	      */
	      VlanMcagDelMacAgingEntries();	
	}

        /* Restart the Mac Aging Timer */
        MacAgingStartTimer (u1TimerType, u4TimeOut);
    }
    return;
}
/*****************************************************************************/
/* Function Name      : McagLock                                             */
/*                                                                           */
/* Description        : This function is used to take the MCAG mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4 McagLock (VOID)
{
    if (VLAN_TAKE_SEM (MCAG_SEM_ID) != OSIX_SUCCESS)
    {
       MCAG_TRC (CRITICAL_DEFAULT_TRC, "Failed in MCAG Mutual Exclusion Sema4\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : McagUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the MCAG mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4 McagUnLock (VOID)
{
    VLAN_RELEASE_SEM (MCAG_SEM_ID);
    return SNMP_SUCCESS;
}
/************************************************************************/
/* Function Name    : MacAgingStartTimer ()                             */
/*                                                                      */
/* Description      : Starts the MAC Aging timer of the given duration  */
/*                                                                      */
/* Input(s)         : u4TimeOut - The duration of the timer in seconds  */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gMcagTaskInfo,gMcagTimer                          */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         :                                                   */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VLAN_SUCCESS, if timer is started successfully    */
/*                    otherwise VLAN_FAILURE                            */
/************************************************************************/
INT4
MacAgingStartTimer (UINT1 u1TimerType, UINT4 u4TimeOut)
{
    UINT4               u4RetVal;

    UNUSED_PARAM (u1TimerType);

    u4RetVal = VLAN_TMR_START (gMcagTaskInfo.MacAgingTmrListId,
                               &(gMcagTimer.TmrNode), u4TimeOut);

    if (u4RetVal == TMR_SUCCESS)
    {
        gMcagTimer.u1IsTmrActive = VLAN_TRUE;
        return VLAN_SUCCESS;
    }

    gMcagTimer.u1IsTmrActive = VLAN_FALSE;
    return VLAN_FAILURE;
}
/************************************************************************/
/* Function Name    : MacAgingStopTimer ()                              */
/*                                                                      */
/* Description      : Stops the Mac Aging Timer                         */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         :                                                   */
/* Global Variables                                                     */
/* Modified         : gMcagTaskInfo,gMcagTimer                          */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VLAN_SUCCESS if timer is stopped successfully     */
/*                    otherwise VLAN_FAILURE                           */
/************************************************************************/
INT4
MacAgingStopTimer (UINT1 u1TimerType)
{
    UINT4               u4RetVal;

    UNUSED_PARAM(u1TimerType);

    if (gMcagTimer.u1IsTmrActive == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    u4RetVal = VLAN_TMR_STOP (gMcagTaskInfo.MacAgingTmrListId,
                              &(gMcagTimer.TmrNode));

    if (u4RetVal == TMR_SUCCESS)
    {
        gMcagTimer.u1IsTmrActive = VLAN_FALSE;
        return VLAN_SUCCESS;
    }

    return VLAN_FAILURE;
}
/************************************************************************/
/* Function Name    : VlanMcagDelMacAgingEntries                        */
/*                                                                      */
/* Description      : Deletes the MAC Aging entries present             */
/*                    in SLL                                            */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         :                                                   */
/* Global Variables                                                     */
/* Modified         : gVlanMcagEntries                                  */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID VlanMcagDelMacAgingEntries (VOID)
{
    tVlanMcagEntries  *pVlanMcagEntry = NULL;
    tVlanMcagEntries  *pNextVlanMcagEntry = NULL;
    pVlanMcagEntry =
            (tVlanMcagEntries *) VLAN_SLL_FIRST (&gVlanMcagEntries);
    while (pVlanMcagEntry != NULL)
    {
          pNextVlanMcagEntry = (tVlanMcagEntries *) VLAN_SLL_NEXT
                  (&gVlanMcagEntries,
                   (tVLAN_SLL_NODE *) & pVlanMcagEntry->NextNode);

          /* Deleting the entries in Data structure */
          VLAN_SLL_DEL (&gVlanMcagEntries,
                          (tVLAN_SLL_NODE *) & pVlanMcagEntry->NextNode);

          VLAN_RELEASE_BUF (VLAN_MCAG_MAC_AGING_ENTRIES, (UINT1 *) pVlanMcagEntry);

          pVlanMcagEntry = pNextVlanMcagEntry;
    }
    return;
}
#endif
