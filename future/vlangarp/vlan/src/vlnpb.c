/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpb.c,v 1.132 2016/05/10 11:01:20 siva Exp $
 *
 * Description: This file contains utility routines used in VLANmodule
 *              for provider bridges
 *
 *******************************************************************/
#ifndef _VLNPB_C
#define _VLNPB_C
#include "vlaninc.h"
#include "vlnpbglb.h"

extern UINT4        FsMIPbSVlanConfigServiceType[12];

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbInit                                       */
/*                                                                           */
/*    Description         : Calls functions to initialise S-VLAN             */
/*                          classification tables, Ether type swap table,    */
/*                          VID translation table.                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbInit (VOID)
{

    if (VlanPbTableInit () == VLAN_FAILURE)
    {
        VlanPbDeInit ();
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "PB Tables creation Failed \n");
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbDeInit                                     */
/*                                                                           */
/*    Description         : Calls function to delete data structures         */
/*                          initialised for S-VLAN classification tables,    */
/*                          Ether type swap table, VID translation table. It */
/*                          frees memory into FID control memmory pool       */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbDeInit (VOID)
{
    VlanPbTableDeInit ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbTableInit                                  */
/*                                                                           */
/*    Description         : Initialises the RB tree data structures for      */
/*                          S-VLAN classification, Ethertype swap            */
/*                          ,S-VLAN swap tables, allocate memory for FID     */
/*                          control and creates memory pool for all the      */
/*                          tables.                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbTableInit ()
{

    VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tEtherTypeSwapEntry, RBNode),
                              VlanPbCmpEtherTypeSwapEntry);

    if (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "RBTree Creation failed for Ether type" "swapping table\r\n");
        return VLAN_FAILURE;
    }

    if (VlanL2IwfPbCreateVidTransTable (VLAN_CURR_CONTEXT_ID ()) ==
        L2IWF_FAILURE)
    {
        return VLAN_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        VlanPbConfHwFidProperties ();
    }
#endif

    if (VlanPbCreateSVlanTables () == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "S-VLAN classification tables creation failed\n");
        return VLAN_FAILURE;
    }

    VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanPbLogicalPortEntry, RBNode),
                              VlanPbCmpLogicalPortEntry);

    if (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "RBTree Creation failed for logical port table\r\n");
        return VLAN_FAILURE;
    }

    MEMSET (&gVlanPbCVlanInfo, 0, sizeof (tCVlanInfo));
    MEMSET (&gVlanNullVlanList, 0, sizeof (tVlanList));

    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbTableDeInit                                */
/*                                                                           */
/*    Description         : Deletes the RB tree data structures for          */
/*                          S-VLAN classification, Ethertype swap            */
/*                          and S-VLAN swap tables and frees the memory      */
/*                          allocated for FID control table.                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbTableDeInit (VOID)
{
    UINT4               u4Index;

    if (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable != NULL)
    {
        RBTreeDelete (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable);
        VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable = NULL;
    }

    VlanL2IwfPbDeleteVidTransTable (VLAN_CURR_CONTEXT_ID ());

    VLAN_NUM_VID_TRANS_ENTS () = 0;

    if (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable != NULL)
    {
        RBTreeDelete (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable);
        VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable = NULL;
    }

    for (u4Index = 0; u4Index < VLAN_NUM_SVLAN_CLASSIFY_TABLES; u4Index++)
    {
        if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] != NULL)
        {
            RBTreeDelete (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index]);

            VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] = NULL;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetMemBlock                                */
/*                                                                           */
/*    Description         : This function allocates memory from the          */
/*                          requested memory pool.                           */
/*                                                                           */
/*    Input(s)            : u1BlockType - Indicates the memory pool.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanPbPoolInfo                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated memory if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/
UINT1              *
VlanPbGetMemBlock (UINT1 u1BlockType)
{
    UINT1              *pu1Buf = NULL;

    switch (u1BlockType)
    {
        case VLAN_PB_PORT_ENTRY_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.PbPortTblPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_SRCMAC_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanSrcMacPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_DSTMAC_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanDstMacPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_SRCIP_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanSrcIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_DSTIP_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanDstIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_SRCDSTIP_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanSrcDstIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_DSCP_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanDscpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanSrcMacPoolId,
                                  pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanDstMacPoolId,
                                  pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanDscpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanDstIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanPoolId, pu1Buf);
            break;

        case VLAN_PB_LOGICAL_PORT_ENTRY_TYPE:

            VLAN_ALLOC_MEM_BLOCK (gVlanPbPoolInfo.LogicalPortTblPoolId, pu1Buf);
            break;

        default:
            break;
    }

    return pu1Buf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbReleaseMemBlock                            */
/*                                                                           */
/*    Description         : This function allocates memory from the          */
/*                          requested memory pool.                           */
/*                                                                           */
/*    Input(s)            : u1BlockType - Indicates the memory pool.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanPbPoolInfo                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated memory if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbReleaseMemBlock (UINT1 u1BlockType, UINT1 *pu1Buf)
{

    switch (u1BlockType)
    {

        case VLAN_SVLAN_PORT_SRCMAC_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanSrcMacPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_DSTMAC_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanDstMacPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_SRCIP_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanSrcIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_DSTIP_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanDstIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_DSCP_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanDscpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_SRCDSTIP_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanSrcDstIpPoolId, pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanSrcMacPoolId,
                                    pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanDstMacPoolId,
                                    pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanDscpPoolId,
                                    pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanDstIpPoolId,
                                    pu1Buf);
            break;

        case VLAN_SVLAN_PORT_CVLAN_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.VlanCVlanPoolId, pu1Buf);
            break;

        case VLAN_PB_PORT_ENTRY_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.PbPortTblPoolId, pu1Buf);
            break;

        case VLAN_PB_LOGICAL_PORT_ENTRY_TYPE:

            VLAN_RELEASE_MEM_BLOCK (gVlanPbPoolInfo.LogicalPortTblPoolId,
                                    pu1Buf);
            break;

        default:
            break;
    }

    return;
}

/* S-VLAN Classification tables Comparison functions */

/*****************************************************************************/
/*    Function Name       : VlanPbPortSrcMacCmp                                */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Source MAC based table.                          */
/*    Input(s)            : Two Port, Source MAC based S-VLAN table entries  */
/*                          to be compared                                   */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortSrcMacCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    u2Port1 = ((tVlanSrcMacSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanSrcMacSVlanEntry *) pRBElem2)->u2Port;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }
    if (VLAN_MEMCMP (((tVlanSrcMacSVlanEntry *) pRBElem1)->SrcMac,
                     ((tVlanSrcMacSVlanEntry *) pRBElem2)->SrcMac,
                     ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }

    if (VLAN_MEMCMP (((tVlanSrcMacSVlanEntry *) pRBElem1)->SrcMac,
                     ((tVlanSrcMacSVlanEntry *) pRBElem2)->SrcMac,
                     ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortDstMacCmp                                */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Destination MAC based table.                     */
/*    Input(s)            : Two Port, Destination MAC based S-VLAN table     */
/*                          entries to be compared.                           */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortDstMacCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    u2Port1 = ((tVlanDstMacSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanDstMacSVlanEntry *) pRBElem2)->u2Port;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }
    if (VLAN_MEMCMP (((tVlanDstMacSVlanEntry *) pRBElem1)->DstMac,
                     ((tVlanDstMacSVlanEntry *) pRBElem2)->DstMac,
                     ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }

    if (VLAN_MEMCMP (((tVlanDstMacSVlanEntry *) pRBElem1)->DstMac,
                     ((tVlanDstMacSVlanEntry *) pRBElem2)->DstMac,
                     ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortDscpCmp                                  */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Dscp based table.                                */
/*    Input(s)            : Two Port, Dscp based S-VLAN table                */
/*                          entries to be compared.                          */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortDscpCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    UINT4               u4Dscp1;
    UINT4               u4Dscp2;

    u2Port1 = ((tVlanDscpSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanDscpSVlanEntry *) pRBElem2)->u2Port;

    u4Dscp1 = ((tVlanDscpSVlanEntry *) pRBElem1)->u4Dscp;
    u4Dscp2 = ((tVlanDscpSVlanEntry *) pRBElem2)->u4Dscp;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (u4Dscp1 < u4Dscp2)
    {
        return -1;
    }

    if (u4Dscp1 > u4Dscp2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortSrcIpCmp                                 */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Source IP based table.                           */
/*    Input(s)            : Two Port, Source IP based S-VLAN table           */
/*                          entries to be compared.                          */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortSrcIpCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    UINT4               u4SrcIp1;
    UINT4               u4SrcIp2;

    u2Port1 = ((tVlanSrcIpSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanSrcIpSVlanEntry *) pRBElem2)->u2Port;

    u4SrcIp1 = ((tVlanSrcIpSVlanEntry *) pRBElem1)->u4SrcIp;
    u4SrcIp2 = ((tVlanSrcIpSVlanEntry *) pRBElem2)->u4SrcIp;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (u4SrcIp1 < u4SrcIp2)
    {
        return -1;
    }

    if (u4SrcIp1 > u4SrcIp2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortDstIpCmp                                 */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Destination IP based table.                      */
/*    Input(s)            : Two Port, Destination IP based S-VLAN table      */
/*                          entries to be compared.                          */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortDstIpCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    UINT4               u4DstIp1;
    UINT4               u4DstIp2;

    u2Port1 = ((tVlanDstIpSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanDstIpSVlanEntry *) pRBElem2)->u2Port;

    u4DstIp1 = ((tVlanDstIpSVlanEntry *) pRBElem1)->u4DstIp;
    u4DstIp2 = ((tVlanDstIpSVlanEntry *) pRBElem2)->u4DstIp;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (u4DstIp1 < u4DstIp2)
    {
        return -1;
    }

    if (u4DstIp1 > u4DstIp2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortSrcDstIpCmp                              */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Source, Destination IP based table.              */
/*                                                                           */
/*    Input(s)            : Two Port,Source, Destination IP based S-VLAN     */
/*                          table entries to be compared.                    */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortSrcDstIpCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    UINT4               u4SrcIp1;
    UINT4               u4SrcIp2;

    UINT4               u4DstIp1;
    UINT4               u4DstIp2;

    u2Port1 = ((tVlanSrcDstIpSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanSrcDstIpSVlanEntry *) pRBElem2)->u2Port;

    u4SrcIp1 = ((tVlanSrcDstIpSVlanEntry *) pRBElem1)->u4SrcIp;
    u4SrcIp2 = ((tVlanSrcDstIpSVlanEntry *) pRBElem2)->u4SrcIp;

    u4DstIp1 = ((tVlanSrcDstIpSVlanEntry *) pRBElem1)->u4DstIp;
    u4DstIp2 = ((tVlanSrcDstIpSVlanEntry *) pRBElem2)->u4DstIp;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (u4SrcIp1 < u4SrcIp2)
    {
        return -1;
    }

    if (u4SrcIp1 > u4SrcIp2)
    {
        return 1;
    }

    if (u4DstIp1 < u4DstIp2)
    {
        return -1;
    }

    if (u4DstIp1 > u4DstIp2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortCVlanCmp                                 */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Customer VLAN based table.                       */
/*                                                                           */
/*    Input(s)            : Two Port, Customer VLAN  based S-VLAN            */
/*                          table entries to be compared.                    */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortCVlanCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    tVlanId             CVlanId1;
    tVlanId             CVlanId2;

    u2Port1 = ((tVlanCVlanSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanCVlanSVlanEntry *) pRBElem2)->u2Port;

    CVlanId1 = ((tVlanCVlanSVlanEntry *) pRBElem1)->CVlanId;
    CVlanId2 = ((tVlanCVlanSVlanEntry *) pRBElem2)->CVlanId;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (CVlanId1 < CVlanId2)
    {
        return -1;
    }

    if (CVlanId1 > CVlanId2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortCVlanSrcMacCmp                           */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Customer VLAN, Src MAC based table.              */
/*                                                                           */
/*    Input(s)            : Two Port, Customer VLAN, Source MAC based        */
/*                          S-VLAN table entries to be compared.             */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortCVlanSrcMacCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    tVlanId             CVlanId1;
    tVlanId             CVlanId2;

    u2Port1 = ((tVlanCVlanSrcMacSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanCVlanSrcMacSVlanEntry *) pRBElem2)->u2Port;

    CVlanId1 = ((tVlanCVlanSrcMacSVlanEntry *) pRBElem1)->CVlanId;
    CVlanId2 = ((tVlanCVlanSrcMacSVlanEntry *) pRBElem2)->CVlanId;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (CVlanId1 < CVlanId2)
    {
        return -1;
    }

    if (CVlanId1 > CVlanId2)
    {
        return 1;
    }

    if (VLAN_MEMCMP (((tVlanCVlanSrcMacSVlanEntry *) pRBElem1)->SrcMac,
                     ((tVlanCVlanSrcMacSVlanEntry *) pRBElem2)->SrcMac,
                     ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }

    if (VLAN_MEMCMP (((tVlanCVlanSrcMacSVlanEntry *) pRBElem1)->SrcMac,
                     ((tVlanCVlanSrcMacSVlanEntry *) pRBElem2)->SrcMac,
                     ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortCVlanDstMacCmp                           */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Customer VLAN, Dst MAC based table.              */
/*                                                                           */
/*    Input(s)            : Two Port, Customer VLAN, Destination MAC based   */
/*                          S-VLAN table entries to be compared.             */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortCVlanDstMacCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    tVlanId             CVlanId1;
    tVlanId             CVlanId2;

    u2Port1 = ((tVlanCVlanDstMacSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanCVlanDstMacSVlanEntry *) pRBElem2)->u2Port;

    CVlanId1 = ((tVlanCVlanDstMacSVlanEntry *) pRBElem1)->CVlanId;
    CVlanId2 = ((tVlanCVlanDstMacSVlanEntry *) pRBElem2)->CVlanId;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (CVlanId1 < CVlanId2)
    {
        return -1;
    }

    if (CVlanId1 > CVlanId2)
    {
        return 1;
    }

    if (VLAN_MEMCMP (((tVlanCVlanDstMacSVlanEntry *) pRBElem1)->DstMac,
                     ((tVlanCVlanDstMacSVlanEntry *) pRBElem2)->DstMac,
                     ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }

    if (VLAN_MEMCMP (((tVlanCVlanDstMacSVlanEntry *) pRBElem1)->DstMac,
                     ((tVlanCVlanDstMacSVlanEntry *) pRBElem2)->DstMac,
                     ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortCVlanDscpCmp                             */
/*    Description         : Comparison function for two entries in the Port, */
/*                          Customer VLAN, Dscp based table.                 */
/*                                                                           */
/*    Input(s)            : Two Port, Customer VLAN, Dscp based              */
/*                          S-VLAN table entries to be compared.             */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortCVlanDscpCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    tVlanId             CVlanId1;
    tVlanId             CVlanId2;

    UINT4               u4Dscp1;
    UINT4               u4Dscp2;

    u2Port1 = ((tVlanCVlanDscpSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanCVlanDscpSVlanEntry *) pRBElem2)->u2Port;

    CVlanId1 = ((tVlanCVlanDscpSVlanEntry *) pRBElem1)->CVlanId;
    CVlanId2 = ((tVlanCVlanDscpSVlanEntry *) pRBElem2)->CVlanId;

    u4Dscp1 = ((tVlanCVlanDscpSVlanEntry *) pRBElem1)->u4Dscp;
    u4Dscp2 = ((tVlanCVlanDscpSVlanEntry *) pRBElem2)->u4Dscp;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (CVlanId1 < CVlanId2)
    {
        return -1;
    }

    if (CVlanId1 > CVlanId2)
    {
        return 1;
    }

    if (u4Dscp1 < u4Dscp2)
    {
        return -1;
    }

    if (u4Dscp1 > u4Dscp2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbPortCVlanDstIpCmp                             */
/*    Description         : Comparison function for two entries in the Port,  */
/*                          Customer VLAN, Destination IP based table.       */
/*                                                                           */
/*    Input(s)            : Two Port, Customer VLAN, Destination IP based    */
/*                          S-VLAN table entries to be compared.             */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VlanPbPortCVlanDstIpCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1;
    UINT2               u2Port2;

    tVlanId             CVlanId1;
    tVlanId             CVlanId2;

    UINT4               u4DstIp1;
    UINT4               u4DstIp2;

    u2Port1 = ((tVlanCVlanDstIpSVlanEntry *) pRBElem1)->u2Port;
    u2Port2 = ((tVlanCVlanDstIpSVlanEntry *) pRBElem2)->u2Port;

    CVlanId1 = ((tVlanCVlanDstIpSVlanEntry *) pRBElem1)->CVlanId;
    CVlanId2 = ((tVlanCVlanDstIpSVlanEntry *) pRBElem2)->CVlanId;

    u4DstIp1 = ((tVlanCVlanDstIpSVlanEntry *) pRBElem1)->u4DstIp;
    u4DstIp2 = ((tVlanCVlanDstIpSVlanEntry *) pRBElem2)->u4DstIp;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 > u2Port2)
    {
        return 1;
    }

    if (CVlanId1 < CVlanId2)
    {
        return -1;
    }

    if (CVlanId1 > CVlanId2)
    {
        return 1;
    }

    if (u4DstIp1 < u4DstIp2)
    {
        return -1;
    }

    if (u4DstIp1 > u4DstIp2)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetSVlanTableIndex                           */
/*                                                                           */
/*    Description         : This function returns the index of the S-VLAN    */
/*                          classification table in the global array.        */
/*                                                                           */
/*    Input(s)            : i4TableType - Indicates the Table Type           */
/*                                        Port, C-VLAN and the likes.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanSVlanTableInfo                        */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : The array index of the gVlanSVlanTableInfo        */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanPbGetSVlanTableIndex (INT4 i4TableType)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < VLAN_NUM_SVLAN_CLASSIFY_TABLES; u4Index++)
    {
        if (gVlanSVlanTableInfo[u4Index].i4SVlanTableType == i4TableType)
        {
            break;
        }
    }

    if (u4Index == VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return (u4Index - 1);
    }

    return u4Index;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstSrcMacSVlanEntry                     */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, SrcMac based S-VLAN classification     */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : u4TableIndex - Index into                        */
/*                                         VLAN_CURR_CONTEXT_PTR ()->        */
/*                                         apVlanSVlanTable                  */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          FsPbSrcMacAddress - First SrcMac Index in table  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, SrcMac based table S-VLAN entry  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstSrcMacSVlanEntry (UINT4 u4TableIndex,
                                INT4 *pi4FsPbPort, tMacAddr FsPbSrcMacAddress)
{
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;

    pSrcMacSVlanEntry =
        (tVlanSrcMacSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                        apVlanSVlanTable[u4TableIndex]);

    if (pSrcMacSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pSrcMacSVlanEntry->u2Port;
        VLAN_CPY_MAC_ADDR (FsPbSrcMacAddress, pSrcMacSVlanEntry->SrcMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstDstMacSVlanEntry                     */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, DstMac based S-VLAN classification     */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : u4TableIndex - Index into 
 *                          VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable        */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          FsPbDstMacAddress - First DstMac Index in table  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, DstMac based table S-VLAN entry  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstDstMacSVlanEntry (UINT4 u4TableIndex,
                                INT4 *pi4FsPbPort, tMacAddr FsPbDstMacAddress)
{
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;

    pDstMacSVlanEntry =
        (tVlanDstMacSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                        apVlanSVlanTable[u4TableIndex]);

    if (pDstMacSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pDstMacSVlanEntry->u2Port;
        VLAN_CPY_MAC_ADDR (FsPbDstMacAddress, pDstMacSVlanEntry->DstMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstDscpSVlanEntry                       */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, Dscp based S-VLAN classification       */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : u4TableIndex - Index into 
 *                          VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable        */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pi4FsPbDscp - First Dscp Index in table          */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Dscp based table S-VLAN entry    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstDscpSVlanEntry (UINT4 u4TableIndex,
                              INT4 *pi4FsPbPort, INT4 *pi4FsPbDscp)
{
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;

    pDscpSVlanEntry =
        (tVlanDscpSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                        apVlanSVlanTable[u4TableIndex]);

    if (pDscpSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pDscpSVlanEntry->u2Port;

        *pi4FsPbDscp = (INT4) pDscpSVlanEntry->u4Dscp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstSrcIpSVlanEntry                      */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, SrcIp based S-VLAN classification      */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbSrcIpAddr - First Src IP Index in table   */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Src IP based table S-VLAN entry  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstSrcIpSVlanEntry (UINT4 u4TblIndex,
                               INT4 *pi4FsPbPort, UINT4 *pu4FsPbSrcIpAddr)
{
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;

    pSrcIpSVlanEntry =
        (tVlanSrcIpSVlanEntry *) RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                                 apVlanSVlanTable[u4TblIndex]);

    if (pSrcIpSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pSrcIpSVlanEntry->u2Port;

        *pu4FsPbSrcIpAddr = pSrcIpSVlanEntry->u4SrcIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstDstIpSVlanEntry                      */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, DstIp based S-VLAN classification      */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbDstIpAddr - First Dst IP Index in table   */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Dst IP based table S-VLAN entry  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstDstIpSVlanEntry (UINT4 u4TblIndex,
                               INT4 *pi4FsPbPort, UINT4 *pu4FsPbDstIpAddr)
{
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;

    pDstIpSVlanEntry =
        (tVlanDstIpSVlanEntry *) RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                                 apVlanSVlanTable[u4TblIndex]);

    if (pDstIpSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pDstIpSVlanEntry->u2Port;

        *pu4FsPbDstIpAddr = pDstIpSVlanEntry->u4DstIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstSrcDstIpSVlanEntry                   */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, SrcIP, DstIp based S-VLAN              */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbSrcIpAddr - First Src IP Index in table */
/*                          pu4FsPbDstIpAddr - First Dst IP Index in table   */
/*                                                                           */
/*    Global Variables Referred : None.                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Src IP, Dst IP based table       */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstSrcDstIpSVlanEntry (UINT4 u4TblIndex,
                                  INT4 *pi4FsPbPort,
                                  UINT4 *pu4FsPbSrcIpAddr,
                                  UINT4 *pu4FsPbDstIpAddr)
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;

    pSrcDstIpSVlanEntry =
        (tVlanSrcDstIpSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex]);

    if (pSrcDstIpSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pSrcDstIpSVlanEntry->u2Port;

        *pu4FsPbSrcIpAddr = pSrcDstIpSVlanEntry->u4SrcIp;

        *pu4FsPbDstIpAddr = pSrcDstIpSVlanEntry->u4DstIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstCVlanSVlanEntry                      */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, C-VLAN based S-VLAN                    */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbCVlan - First C-VLAN Index in table       */
/*                                                                           */
/*    Global Variables Referred : None.                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN based table               */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstCVlanSVlanEntry (UINT4 u4TblIndex,
                               INT4 *pi4FsPbPort, UINT4 *pu4FsPbCVlan)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    pCVlanSVlanEntry =
        (tVlanCVlanSVlanEntry *) RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                                 apVlanSVlanTable[u4TblIndex]);

    if (pCVlanSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pCVlanSVlanEntry->u2Port;

        *pu4FsPbCVlan = (UINT4) pCVlanSVlanEntry->CVlanId;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstCVlanSrcMacSVlanEntry                */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, C-VLAN, Src MAC based S-VLAN           */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbCVlan - First C-VLAN Index in table       */
/*                          FsPbSrcMac - First Src MAC Index in table        */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Src MAC based table      */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstCVlanSrcMacSVlanEntry (UINT4 u4TblIndex,
                                     INT4 *pi4FsPbPort,
                                     UINT4 *pu4FsPbCVlan, tMacAddr FsPbSrcMac)
{
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;

    pCVlanSrcMacSVlanEntry =
        (tVlanCVlanSrcMacSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex]);

    if (pCVlanSrcMacSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pCVlanSrcMacSVlanEntry->u2Port;

        *pu4FsPbCVlan = (UINT4) pCVlanSrcMacSVlanEntry->CVlanId;

        VLAN_CPY_MAC_ADDR (FsPbSrcMac, pCVlanSrcMacSVlanEntry->SrcMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstCVlanDstMacSVlanEntry                */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, C-VLAN, Dst MAC based S-VLAN           */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbCVlan - First C-VLAN Index in table       */
/*                          FsPbDstMac - First Dst MAC Index in table        */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Dst MAC based table      */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstCVlanDstMacSVlanEntry (UINT4 u4PrioIndex,
                                     INT4 *pi4FsPbPort,
                                     UINT4 *pu4FsPbCVlan, tMacAddr FsPbDstMac)
{
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;

    pCVlanDstMacSVlanEntry =
        (tVlanCVlanDstMacSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                        apVlanSVlanTable[u4PrioIndex]);

    if (pCVlanDstMacSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pCVlanDstMacSVlanEntry->u2Port;

        *pu4FsPbCVlan = (UINT4) pCVlanDstMacSVlanEntry->CVlanId;

        VLAN_CPY_MAC_ADDR (FsPbDstMac, pCVlanDstMacSVlanEntry->DstMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstCVlanDscpSVlanEntry                  */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, C-VLAN, Dscp based S-VLAN           */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbCVlan - First C-VLAN Index in table       */
/*                          pu4FsPbDscp - First Dscp Index in table          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Dscp based table         */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstCVlanDscpSVlanEntry (UINT4 u4TblIndex,
                                   INT4 *pi4FsPbPort,
                                   UINT4 *pu4FsPbCVlan, UINT4 *pu4FsPbDscp)
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;

    pCVlanDscpSVlanEntry =
        (tVlanCVlanDscpSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex]);

    if (pCVlanDscpSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pCVlanDscpSVlanEntry->u2Port;

        *pu4FsPbCVlan = (UINT4) pCVlanDscpSVlanEntry->CVlanId;

        *pu4FsPbDscp = pCVlanDscpSVlanEntry->u4Dscp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstCVlanDstIpSVlanEntry                 */
/*                                                                           */
/*    Description         : This function gets the first index values from   */
/*                          the Port, C-VLAN, DstIP based S-VLAN             */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbCVlan - First C-VLAN Index in table       */
/*                          pu4FsPbDstIp - First Destination IP Index in     */
/*                          table                                            */
/*                                                                           */
/*    Global Variables Referred : None.                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Destination IP based     */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetFirstCVlanDstIpSVlanEntry (UINT4 u4TblIndex,
                                    INT4 *pi4FsPbPort,
                                    UINT4 *pu4FsPbCVlan, UINT4 *pu4FsPbDstIp)
{
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;

    pCVlanDstIpSVlanEntry =
        (tVlanCVlanDstIpSVlanEntry *)
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex]);

    if (pCVlanDstIpSVlanEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pCVlanDstIpSVlanEntry->u2Port;

        *pu4FsPbCVlan = (UINT4) pCVlanDstIpSVlanEntry->CVlanId;

        *pu4FsPbDstIp = pCVlanDstIpSVlanEntry->u4DstIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextSrcMacSVlanEntry                      */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, Source MAC based S-VLAN           */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          FsPbSrcMacAddress - Current Src MAC              */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          NextFsPbSrcMacAddress - Next Src MAC address     */
/*                                                  Index in table           */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Source MAC based table           */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextSrcMacSVlanEntry (UINT4 u4TblIndex,
                               INT4 i4FsPbPort,
                               tMacAddr FsPbSrcMacAddress,
                               INT4 *pi4NextFsPbPort,
                               tMacAddr NextFsPbSrcMacAddress)
{
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;
    tVlanSrcMacSVlanEntry SrcMacSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&SrcMacSVlanTable, 0, sizeof (tVlanSrcMacSVlanEntry));

    SrcMacSVlanTable.u2Port = (UINT2) i4FsPbPort;

    VLAN_CPY_MAC_ADDR (SrcMacSVlanTable.SrcMac, FsPbSrcMacAddress);

    pSrcMacSVlanEntry =
        (tVlanSrcMacSVlanEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                 apVlanSVlanTable[u4TblIndex],
                                                 (tRBElem *) & SrcMacSVlanTable,
                                                 NULL);

    if (pSrcMacSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pSrcMacSVlanEntry->u2Port;
        VLAN_CPY_MAC_ADDR (NextFsPbSrcMacAddress, pSrcMacSVlanEntry->SrcMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextDstMacSVlanEntry                      */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, Destination MAC based S-VLAN      */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR () */
/*                          ->apVlanSVlanTable                               */
/*                          i4FsPbPort - Current Port Index value            */
/*                          FsPbDstMacAddress - Current Dst MAC              */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          NextFsPbDstMacAddress - Next Dst MAC address     */
/*                                                  Index in table           */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Destination MAC based table      */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextDstMacSVlanEntry (UINT4 u4TblIndex,
                               INT4 i4FsPbPort,
                               tMacAddr FsPbDstMacAddress,
                               INT4 *pi4NextFsPbPort,
                               tMacAddr NextFsPbDstMacAddress)
{
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;
    tVlanDstMacSVlanEntry DstMacSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&DstMacSVlanTable, 0, sizeof (tVlanDstMacSVlanEntry));

    DstMacSVlanTable.u2Port = (UINT2) i4FsPbPort;

    VLAN_CPY_MAC_ADDR (DstMacSVlanTable.DstMac, FsPbDstMacAddress);

    pDstMacSVlanEntry =
        (tVlanDstMacSVlanEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                 apVlanSVlanTable[u4TblIndex],
                                                 (tRBElem *) & DstMacSVlanTable,
                                                 NULL);

    if (pDstMacSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pDstMacSVlanEntry->u2Port;
        VLAN_CPY_MAC_ADDR (NextFsPbDstMacAddress, pDstMacSVlanEntry->DstMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextDscpSVlanEntry                        */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, Dscp based S-VLAN                 */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          i4FsPbDscp - Current Dscp Index                  */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pi4NextFsPbDscp - Next Dscp value Index          */
/*                                                                           */
/*    Global Variables Referred : None.                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Dscp based table                 */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextDscpSVlanEntry (UINT4 u4TblIndex,
                             INT4 i4FsPbPort,
                             INT4 i4FsPbDscp,
                             INT4 *pi4NextFsPbPort, INT4 *pi4NextFsPbDscp)
{
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;
    tVlanDscpSVlanEntry DscpSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS)
        || (i4FsPbDscp < 0) || (i4FsPbDscp > 63))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&DscpSVlanTable, 0, sizeof (tVlanDscpSVlanEntry));

    DscpSVlanTable.u2Port = (UINT2) i4FsPbPort;
    DscpSVlanTable.u4Dscp = (UINT4) i4FsPbDscp;

    pDscpSVlanEntry =
        (tVlanDscpSVlanEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                               apVlanSVlanTable[u4TblIndex],
                                               (tRBElem *) & DscpSVlanTable,
                                               NULL);

    if (pDscpSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pDscpSVlanEntry->u2Port;

        *pi4NextFsPbDscp = (UINT4) pDscpSVlanEntry->u4Dscp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextSrcIpSVlanEntry                       */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, Src IP based S-VLAN               */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4SrcIp -    Current Source IP Index             */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbSrcIp - Next Source IP value Index    */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Source IP based table            */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextSrcIpSVlanEntry (UINT4 u4TblIndex,
                              INT4 i4FsPbPort,
                              UINT4 u4SrcIp,
                              INT4 *pi4NextFsPbPort, UINT4 *pu4NextFsPbSrcIp)
{
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;
    tVlanSrcIpSVlanEntry SrcIpSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&SrcIpSVlanTable, 0, sizeof (tVlanSrcIpSVlanEntry));

    SrcIpSVlanTable.u2Port = (UINT2) i4FsPbPort;
    SrcIpSVlanTable.u4SrcIp = u4SrcIp;

    pSrcIpSVlanEntry =
        (tVlanSrcIpSVlanEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                apVlanSVlanTable[u4TblIndex],
                                                (tRBElem *) & SrcIpSVlanTable,
                                                NULL);

    if (pSrcIpSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pSrcIpSVlanEntry->u2Port;

        *pu4NextFsPbSrcIp = (UINT4) pSrcIpSVlanEntry->u4SrcIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextDstIpSVlanEntry                       */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, Dst IP based S-VLAN               */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4DstIp -    Current Destination IP Index        */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbDstIp - Next Destination IP value     */
/*                                             Index                         */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Destination IP based table       */
/*                         S-VLAN entry                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextDstIpSVlanEntry (UINT4 u4TblIndex,
                              INT4 i4FsPbPort,
                              UINT4 u4DstIp,
                              INT4 *pi4NextFsPbPort, UINT4 *pu4NextFsPbDstIp)
{
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;
    tVlanDstIpSVlanEntry DstIpSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&DstIpSVlanTable, 0, sizeof (tVlanDstIpSVlanEntry));

    DstIpSVlanTable.u2Port = (UINT2) i4FsPbPort;
    DstIpSVlanTable.u4DstIp = u4DstIp;

    pDstIpSVlanEntry =
        (tVlanDstIpSVlanEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                apVlanSVlanTable[u4TblIndex],
                                                (tRBElem *) & DstIpSVlanTable,
                                                NULL);

    if (pDstIpSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pDstIpSVlanEntry->u2Port;

        *pu4NextFsPbDstIp = (UINT4) pDstIpSVlanEntry->u4DstIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextSrcDstIpSVlanEntry                    */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, Dst IP based S-VLAN               */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4SrcIp -    Current Source IP Index             */
/*                          u4DstIp -    Current Destination IP Index        */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbSrcIp - Next Source IP value          */
/*                                             Index                         */
/*                          pu4NextFsPbDstIp - Next Destination IP value     */
/*                                             Index                         */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Source IP, Destination IP        */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextSrcDstIpSVlanEntry (UINT4 u4TblIndex,
                                 INT4 i4FsPbPort,
                                 UINT4 u4SrcIp,
                                 UINT4 u4DstIp,
                                 INT4 *pi4NextFsPbPort,
                                 UINT4 *pu4NextFsPbSrcIp,
                                 UINT4 *pu4NextFsPbDstIp)
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;
    tVlanSrcDstIpSVlanEntry SrcDstIpSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&SrcDstIpSVlanTable, 0, sizeof (tVlanSrcDstIpSVlanEntry));

    SrcDstIpSVlanTable.u2Port = (UINT2) i4FsPbPort;
    SrcDstIpSVlanTable.u4SrcIp = u4SrcIp;
    SrcDstIpSVlanTable.u4DstIp = u4DstIp;

    pSrcDstIpSVlanEntry =
        (tVlanSrcDstIpSVlanEntry *)
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                       (tRBElem *) & SrcDstIpSVlanTable, NULL);

    if (pSrcDstIpSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pSrcDstIpSVlanEntry->u2Port;

        *pu4NextFsPbSrcIp = (UINT4) pSrcDstIpSVlanEntry->u4SrcIp;

        *pu4NextFsPbDstIp = (UINT4) pSrcDstIpSVlanEntry->u4DstIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextCVlanSVlanEntry                       */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, C-VLAN based S-VLAN               */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4CVid -     Current C-VLAN Index                */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbCVid - Next C-VLAN Index in table     */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN                           */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextCVlanSVlanEntry (UINT4 u4TblIndex,
                              INT4 i4FsPbPort,
                              UINT4 u4CVid,
                              INT4 *pi4NextFsPbPort, UINT4 *pu4NextFsPbCVid)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanCVlanSVlanEntry CVlanSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    if (u4CVid > VLAN_MAX_VLAN_ID)
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&CVlanSVlanTable, 0, sizeof (tVlanCVlanSVlanEntry));

    CVlanSVlanTable.u2Port = (UINT2) i4FsPbPort;
    CVlanSVlanTable.CVlanId = (tVlanId) u4CVid;

    pCVlanSVlanEntry =
        (tVlanCVlanSVlanEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                apVlanSVlanTable[u4TblIndex],
                                                (tRBElem *) & CVlanSVlanTable,
                                                NULL);

    if (pCVlanSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pCVlanSVlanEntry->u2Port;

        *pu4NextFsPbCVid = (UINT4) pCVlanSVlanEntry->CVlanId;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextCVlanSrcMacSVlanEntry                 */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          classification table.                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4CVid -     Current C-VLAN Index                */
/*                          FsPbMacAddr - Current Source MAC Index           */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbCVid -  Next C-VLAN                   */
/*                                             Index                         */
/*                          FsPbNextMacAddr -  Next Source MAC Address       */
/*                                             Index                         */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Source MAC               */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextCVlanSrcMacSVlanEntry (UINT4 u4TblIndex,
                                    INT4 i4FsPbPort,
                                    UINT4 u4CVid,
                                    tMacAddr FsPbMacAddr,
                                    INT4 *pi4NextFsPbPort,
                                    UINT4 *pu4NextFsPbCVid,
                                    tMacAddr FsPbNextMacAddr)
{
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tVlanCVlanSrcMacSVlanEntry CVlanSrcMacSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS) ||
        (u4CVid > VLAN_MAX_VLAN_ID))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&CVlanSrcMacSVlanTable, 0,
                 sizeof (tVlanCVlanSrcMacSVlanEntry));

    CVlanSrcMacSVlanTable.u2Port = (UINT2) i4FsPbPort;
    CVlanSrcMacSVlanTable.CVlanId = (tVlanId) u4CVid;

    VLAN_CPY_MAC_ADDR (CVlanSrcMacSVlanTable.SrcMac, FsPbMacAddr);

    pCVlanSrcMacSVlanEntry =
        (tVlanCVlanSrcMacSVlanEntry *)
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                       (tRBElem *) & CVlanSrcMacSVlanTable, NULL);

    if (pCVlanSrcMacSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pCVlanSrcMacSVlanEntry->u2Port;

        *pu4NextFsPbCVid = (UINT4) pCVlanSrcMacSVlanEntry->CVlanId;

        VLAN_CPY_MAC_ADDR (FsPbNextMacAddr, pCVlanSrcMacSVlanEntry->SrcMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextCVlanDstMacSVlanEntry                 */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, C-VLAN, Destination MAC based     */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4CVid -     Current C-VLAN Index                */
/*                          FsPbMacAddr - Current Destination MAC Index      */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbCVid -  Next C-VLAN                   */
/*                                             Index                         */
/*                          FsPbNextMacAddr -  Next Destination MAC Address  */
/*                                             Index                         */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Destination MAC          */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextCVlanDstMacSVlanEntry (UINT4 u4TblIndex,
                                    INT4 i4FsPbPort,
                                    UINT4 u4CVid,
                                    tMacAddr FsPbMacAddr,
                                    INT4 *pi4NextFsPbPort,
                                    UINT4 *pu4NextFsPbCVid,
                                    tMacAddr FsPbNextMacAddr)
{
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tVlanCVlanDstMacSVlanEntry CVlanDstMacSVlanTable;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS)
        || (u4CVid > VLAN_MAX_VLAN_ID))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&CVlanDstMacSVlanTable, 0,
                 sizeof (tVlanCVlanDstMacSVlanEntry));

    CVlanDstMacSVlanTable.u2Port = (UINT2) i4FsPbPort;
    CVlanDstMacSVlanTable.CVlanId = (tVlanId) u4CVid;

    VLAN_CPY_MAC_ADDR (CVlanDstMacSVlanTable.DstMac, FsPbMacAddr);

    pCVlanDstMacSVlanEntry =
        (tVlanCVlanDstMacSVlanEntry *)
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                       (tRBElem *) & CVlanDstMacSVlanTable, NULL);

    if (pCVlanDstMacSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pCVlanDstMacSVlanEntry->u2Port;

        *pu4NextFsPbCVid = (UINT4) pCVlanDstMacSVlanEntry->CVlanId;

        VLAN_CPY_MAC_ADDR (FsPbNextMacAddr, pCVlanDstMacSVlanEntry->DstMac);

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextCVlanDscpSVlanEntry                   */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, C-VLAN, Dscp IP based             */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4CVid -     Current C-VLAN Index                */
/*                          i4Dscp  -    Current Dscp Index                  */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbCVid -  Next C-VLAN                   */
/*                                             Index                         */
/*                          pi4FsPbNextDscp - Next Dscp Index                */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Dscp                     */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextCVlanDscpSVlanEntry (UINT4 u4PrioIndex,
                                  INT4 i4FsPbPort,
                                  UINT4 u4CVid,
                                  INT4 i4Dscp,
                                  INT4 *pi4NextFsPbPort,
                                  UINT4 *pu4NextFsPbCVid, INT4 *pi4FsPbNextDscp)
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tVlanCVlanDscpSVlanEntry CVlanDscpSVlanTable;

    if (i4FsPbPort < 0 || u4CVid > VLAN_MAX_VLAN_ID
        || (i4Dscp < 0 || i4Dscp > 63 || i4FsPbPort > VLAN_MAX_PORTS))
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&CVlanDscpSVlanTable, 0, sizeof (tVlanCVlanDscpSVlanEntry));

    CVlanDscpSVlanTable.u2Port = (UINT2) i4FsPbPort;
    CVlanDscpSVlanTable.CVlanId = (tVlanId) u4CVid;
    CVlanDscpSVlanTable.u4Dscp = (UINT4) i4Dscp;

    pCVlanDscpSVlanEntry =
        (tVlanCVlanDscpSVlanEntry *)
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                       apVlanSVlanTable[u4PrioIndex],
                       (tRBElem *) & CVlanDscpSVlanTable, NULL);

    if (pCVlanDscpSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pCVlanDscpSVlanEntry->u2Port;

        *pu4NextFsPbCVid = (UINT4) pCVlanDscpSVlanEntry->CVlanId;

        *pi4FsPbNextDscp = (INT4) pCVlanDscpSVlanEntry->u4Dscp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextCVlanDstIpSVlanEntry                  */
/*                                                                           */
/*    Description         : This function gets the next valid index values   */
/*                          from the Port, C-VLAN, Destination IP based      */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          i4FsPbPort - Current Port Index value            */
/*                          u4CVid -     Current C-VLAN Index                */
/*                          u4DstIp  -   Current Destination IP Index      */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort -  Next Interface Index in table */
/*                          pu4NextFsPbCVid -  Next C-VLAN                   */
/*                                             Index                         */
/*                          pu4FsPbNextIpAddr - Next Destination IP Address  */
/*                                             Index                         */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLAN, Destination IP           */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetNextCVlanDstIpSVlanEntry (UINT4 u4TblIndex,
                                   INT4 i4FsPbPort,
                                   UINT4 u4CVid,
                                   UINT4 u4DstIp,
                                   INT4 *pi4NextFsPbPort,
                                   UINT4 *pu4NextFsPbCVid,
                                   UINT4 *pu4FsPbNextIpAddr)
{
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;
    tVlanCVlanDstIpSVlanEntry CVlanDstIpSVlanTable;

    if (i4FsPbPort < 0 || i4FsPbPort > VLAN_MAX_PORTS ||
        u4CVid > VLAN_MAX_VLAN_ID)
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&CVlanDstIpSVlanTable, 0, sizeof (tVlanCVlanDstIpSVlanEntry));

    CVlanDstIpSVlanTable.u2Port = (UINT2) i4FsPbPort;
    CVlanDstIpSVlanTable.CVlanId = (tVlanId) u4CVid;
    CVlanDstIpSVlanTable.u4DstIp = u4DstIp;

    pCVlanDstIpSVlanEntry =
        (tVlanCVlanDstIpSVlanEntry *)
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                       apVlanSVlanTable[u4TblIndex],
                       (tRBElem *) & CVlanDstIpSVlanTable, NULL);

    if (pCVlanDstIpSVlanEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pCVlanDstIpSVlanEntry->u2Port;

        *pu4NextFsPbCVid = (UINT4) pCVlanDstIpSVlanEntry->CVlanId;

        *pu4FsPbNextIpAddr = pCVlanDstIpSVlanEntry->u4DstIp;

        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetSrcMacAddrSVlanEntry                      */
/*                                                                           */
/*    Description         : This function returns the Port, Source MAC based */
/*                          S-VLAN classification entry for the given indices*/
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          SrcMacAddress  - Source MAC Address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Source MAC                       */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanSrcMacSVlanEntry *
VlanPbGetSrcMacAddrSVlanEntry (UINT4 u4TblIndex,
                               UINT2 u2Port, tMacAddr SrcMacAddress)
{
    tVlanSrcMacSVlanEntry SrcMacAddrSVlanEntry;

    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;

    VLAN_MEMSET (&SrcMacAddrSVlanEntry, 0, sizeof (tVlanSrcMacSVlanEntry));

    SrcMacAddrSVlanEntry.u2Port = u2Port;

    VLAN_CPY_MAC_ADDR (SrcMacAddrSVlanEntry.SrcMac, SrcMacAddress);

    pSrcMacSVlanEntry =
        (tVlanSrcMacSVlanEntry *) RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->
                                             apVlanSVlanTable[u4TblIndex],
                                             (tRBElem *) &
                                             SrcMacAddrSVlanEntry);

    return pSrcMacSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetDstMacAddrSVlanEntry                      */
/*                                                                           */
/*    Description         : This function returns the Port, Destination MAC  */
/*                          based S-VLAN classification entry for the given  */
/*                          indices                                          */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable       */
/*                          u2Port -     Interface Index                     */
/*                          DstMacAddress  - Source MAC Address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Destination MAC                  */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanDstMacSVlanEntry *
VlanPbGetDstMacAddrSVlanEntry (UINT4 u4TblIndex,
                               UINT2 u2Port, tMacAddr DstMacAddress)
{
    tVlanDstMacSVlanEntry DstMacAddrSVlanEntry;

    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;

    VLAN_MEMSET (&DstMacAddrSVlanEntry, 0, sizeof (tVlanDstMacSVlanEntry));

    DstMacAddrSVlanEntry.u2Port = u2Port;

    VLAN_CPY_MAC_ADDR (DstMacAddrSVlanEntry.DstMac, DstMacAddress);

    pDstMacSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & DstMacAddrSVlanEntry);

    return pDstMacSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetDscpSVlanEntry                            */
/*                                                                           */
/*    Description         : This function returns the Port, Dscp             */
/*                          based S-VLAN classification entry for the given  */
/*                          indices                                          */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          u4Dscp  - Dscp value                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Dscp                             */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanDscpSVlanEntry *
VlanPbGetDscpSVlanEntry (UINT4 u4TblIndex, UINT2 u2Port, UINT4 u4Dscp)
{
    tVlanDscpSVlanEntry DscpSVlanEntry;

    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;

    VLAN_MEMSET (&DscpSVlanEntry, 0, sizeof (tVlanDscpSVlanEntry));

    DscpSVlanEntry.u2Port = u2Port;

    DscpSVlanEntry.u4Dscp = u4Dscp;

    pDscpSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & DscpSVlanEntry);

    return pDscpSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetSrcIpSVlanEntry                           */
/*                                                                           */
/*    Description         : This function returns the Port, Source IP        */
/*                          based S-VLAN classification entry for the given  */
/*                          indices                                          */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          u4SrcIp  - Source IP address                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Source IP                        */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanSrcIpSVlanEntry *
VlanPbGetSrcIpSVlanEntry (UINT4 u4TblIndex, UINT2 u2Port, UINT4 u4SrcIp)
{
    tVlanSrcIpSVlanEntry SrcIpSVlanEntry;

    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;

    VLAN_MEMSET (&SrcIpSVlanEntry, 0, sizeof (tVlanSrcIpSVlanEntry));

    SrcIpSVlanEntry.u2Port = u2Port;

    SrcIpSVlanEntry.u4SrcIp = u4SrcIp;

    pSrcIpSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & SrcIpSVlanEntry);

    return pSrcIpSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetDstIpSVlanEntry                           */
/*                                                                           */
/*    Description         : This function returns the Port, Destination IP   */
/*                          based S-VLAN classification entry for the given  */
/*                          indices                                          */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          u4DstIp  -   Destination IP address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Destination IP                   */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanDstIpSVlanEntry *
VlanPbGetDstIpSVlanEntry (UINT4 u4TblIndex, UINT2 u2Port, UINT4 u4DstIp)
{
    tVlanDstIpSVlanEntry DstIpSVlanEntry;

    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;

    VLAN_MEMSET (&DstIpSVlanEntry, 0, sizeof (tVlanDstIpSVlanEntry));

    DstIpSVlanEntry.u2Port = u2Port;

    DstIpSVlanEntry.u4DstIp = u4DstIp;

    pDstIpSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & DstIpSVlanEntry);

    return pDstIpSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetSrcDstIpSVlanEntry                        */
/*                                                                           */
/*    Description         : This function returns the Port, Source IP,       */
/*                          Destination IP based S-VLAN classification       */
/*                          entry for the given indices                      */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          u4SrcIp  - Source IP address                     */
/*                          u4DstIp  - Destination IP address                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, Source IP, Destination IP        */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanSrcDstIpSVlanEntry *
VlanPbGetSrcDstIpSVlanEntry (UINT4 u4TblIndex,
                             UINT2 u2Port, UINT4 u4SrcIp, UINT4 u4DstIp)
{
    tVlanSrcDstIpSVlanEntry SrcDstIpSVlanEntry;

    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;

    VLAN_MEMSET (&SrcDstIpSVlanEntry, 0, sizeof (tVlanSrcDstIpSVlanEntry));

    SrcDstIpSVlanEntry.u2Port = u2Port;

    SrcDstIpSVlanEntry.u4SrcIp = u4SrcIp;

    SrcDstIpSVlanEntry.u4DstIp = u4DstIp;

    pSrcDstIpSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & SrcDstIpSVlanEntry);

    return pSrcDstIpSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetCVlanSVlanEntry                           */
/*                                                                           */
/*    Description         : This function returns the Port, C-VLAN           */
/*                          based S-VLAN classification entry for the given  */
/*                          indices                                          */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          CVlanId  -   Customer VLAN ID                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLANID                         */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanCVlanSVlanEntry *
VlanPbGetCVlanSVlanEntry (UINT4 u4TblIndex, UINT2 u2Port, tVlanId CVlanId)
{
    tVlanCVlanSVlanEntry CVlanSVlanEntry;

    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanSVlanEntry, 0, sizeof (tVlanCVlanSVlanEntry));

    CVlanSVlanEntry.u2Port = u2Port;

    CVlanSVlanEntry.CVlanId = CVlanId;

    pCVlanSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & CVlanSVlanEntry);

    return pCVlanSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetCVlanSrcMacSVlanEntry                     */
/*                                                                           */
/*    Description         : This function returns the Port, C-VLAN, Source   */
/*                          MAC based S-VLAN classification entry for the    */
/*                          given indices.                                   */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          CVlanId  -   Customer VLAN ID                    */
/*                          SrcMac   - Source MAC address                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLANID, Source MAC             */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanCVlanSrcMacSVlanEntry *
VlanPbGetCVlanSrcMacSVlanEntry (UINT4 u4TblIndex,
                                UINT2 u2Port, tVlanId CVlanId, tMacAddr SrcMac)
{
    tVlanCVlanSrcMacSVlanEntry CVlanSrcMacSVlanEntry;

    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanSrcMacSVlanEntry, 0, sizeof (tVlanCVlanSVlanEntry));

    CVlanSrcMacSVlanEntry.u2Port = u2Port;

    CVlanSrcMacSVlanEntry.CVlanId = CVlanId;

    VLAN_CPY_MAC_ADDR (CVlanSrcMacSVlanEntry.SrcMac, SrcMac);

    pCVlanSrcMacSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & CVlanSrcMacSVlanEntry);

    return pCVlanSrcMacSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetCVlanDstMacSVlanEntry                     */
/*                                                                           */
/*    Description         : This function returns the Port, C-VLAN,          */
/*                          Destination MAC based S-VLAN classification      */
/*                          entry for the given indices.                     */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          CVlanId  -   Customer VLAN ID                    */
/*                          DstMac   -   Destination MAC address             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLANID, Destination MAC        */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanCVlanDstMacSVlanEntry *
VlanPbGetCVlanDstMacSVlanEntry (UINT4 u4TblIndex,
                                UINT2 u2Port, tVlanId CVlanId, tMacAddr DstMac)
{
    tVlanCVlanDstMacSVlanEntry CVlanDstMacSVlanEntry;

    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanDstMacSVlanEntry, 0,
                 sizeof (tVlanCVlanDstMacSVlanEntry));

    CVlanDstMacSVlanEntry.u2Port = u2Port;

    CVlanDstMacSVlanEntry.CVlanId = CVlanId;

    VLAN_CPY_MAC_ADDR (CVlanDstMacSVlanEntry.DstMac, DstMac);

    pCVlanDstMacSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & CVlanDstMacSVlanEntry);

    return pCVlanDstMacSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetCVlanDscpSVlanEntry                       */
/*                                                                           */
/*    Description         : This function returns the Port, C-VLAN, DSCP     */
/*                          based S-VLAN classification entry for the        */
/*                          given indices.                                   */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          CVlanId  -   Customer VLAN ID                    */
/*                          u4Dscp   -   DSCP Value                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLANID, Dscp                   */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanCVlanDscpSVlanEntry *
VlanPbGetCVlanDscpSVlanEntry (UINT4 u4TblIndex,
                              UINT2 u2Port, tVlanId CVlanId, UINT4 u4Dscp)
{
    tVlanCVlanDscpSVlanEntry CVlanDscpSVlanEntry;

    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanDscpSVlanEntry, 0, sizeof (tVlanCVlanDscpSVlanEntry));

    CVlanDscpSVlanEntry.u2Port = u2Port;

    CVlanDscpSVlanEntry.CVlanId = CVlanId;

    CVlanDscpSVlanEntry.u4Dscp = u4Dscp;

    pCVlanDscpSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & CVlanDscpSVlanEntry);

    return pCVlanDscpSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetCVlanDstIpSVlanEntry                      */
/*                                                                           */
/*    Description         : This function returns the Port, C-VLAN, DSCP     */
/*                          based S-VLAN classification entry for the        */
/*                          given indices.                                   */
/*                                                                           */
/*    Input(s)            : u4TblIndex - Index into VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable          */
/*                          u2Port -     Interface Index                     */
/*                          CVlanId  -   Customer VLAN ID                    */
/*                          u4DstIp   -   Destination IP Addr                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to Port, C-VLANID, Destination IP         */
/*                         based table S-VLAN entry                          */
/*                                                                           */
/*****************************************************************************/
tVlanCVlanDstIpSVlanEntry *
VlanPbGetCVlanDstIpSVlanEntry (UINT4 u4TblIndex,
                               UINT2 u2Port, tVlanId CVlanId, UINT4 u4DstIp)
{
    tVlanCVlanDstIpSVlanEntry CVlanDstIpSVlanEntry;

    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanDstIpSVlanEntry, 0, sizeof (tVlanCVlanDstIpSVlanEntry));

    CVlanDstIpSVlanEntry.u2Port = u2Port;

    CVlanDstIpSVlanEntry.CVlanId = CVlanId;

    CVlanDstIpSVlanEntry.u4DstIp = u4DstIp;

    pCVlanDstIpSVlanEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                   (tRBElem *) & CVlanDstIpSVlanEntry);

    return pCVlanDstIpSVlanEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateSVlanClassifyParams                    */
/*                                                                           */
/*    Description         : Updates the all available S-VLAN classification  */
/*                          table index values from the frame.               */
/*                                                                           */
/*    Input(s)            : pFrame - The incoming frame buffer               */
/*                                                                           */
/*    Output(s)           : pVlanSVlanIndex - The S-VLAN classification      */
/*                          table indices updated in the pointer             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : au1VlanIpHdr                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbUpdateSVlanClassifyParams (tCRU_BUF_CHAIN_DESC * pFrame,
                                 tVlanSVlanClassificationParams *
                                 pVlanSVlanIndex)
{
    tVlanIpHeader      *pIpHdr = NULL;
    UINT1               au1VlanIpHdr[VLAN_IP_HDR_LEN];
    UINT1              *pu1Data;
    UINT1               au1Buf[VLAN_DT_LLCSNAP_IPHDR_OFFSET];
    UINT2               u2TypeLen;
    UINT2               u2TypeLenOffset = VLAN_TYPE_LEN_OFFSET;
    UINT2               u2Protocol;
    UINT1               u1FrameType;
    UINT4               u4IpHdrOffset;
    UINT4               u4SrcIp;
    UINT4               u4DstIp;
    UINT1               u1Dscp = 0;

    /* Get to the type/len field to see if the frame is
     * an ENETv2 frame.*/

    u1FrameType = pVlanSVlanIndex->u1FrameType;

    if (u1FrameType == VLAN_DT_FRAME)
    {
        u2TypeLenOffset = VLAN_DT_TYPE_LEN_OFFSET;
    }
    else if (u1FrameType == VLAN_ST_FRAME)
    {
        u2TypeLenOffset = VLAN_ST_TYPE_LEN_OFFSET;
    }
    else if (u1FrameType == VLAN_UNTAGGED_FRAME)
    {
        u2TypeLenOffset = VLAN_TYPE_LEN_OFFSET;
    }

    VLAN_COPY_FROM_BUF (pFrame, au1Buf, 0, VLAN_DT_LLCSNAP_IPHDR_OFFSET);
    pu1Data = au1Buf;

    VLAN_MEMCPY ((UINT1 *) &u2TypeLen, &pu1Data[u2TypeLenOffset],
                 VLAN_TYPE_OR_LEN_SIZE);

    u2TypeLen = (UINT2) (OSIX_NTOHS (u2TypeLen));

    if (VLAN_ENET_IS_TYPE (u2TypeLen))
    {
        /* The frame is an ENETV2 frame */
        u2Protocol = u2TypeLen;

        /* If the protocol is IP/ARP/RARP the following is the offset of the
         * IP/ARP/RARP header */

        if (u1FrameType == VLAN_DT_FRAME)
        {
            u4IpHdrOffset = VLAN_ENETV2_DT_IP_HEADER_OFFSET;
        }
        else if (u1FrameType == VLAN_ST_FRAME)
        {
            u4IpHdrOffset = VLAN_ENETV2_ST_IP_HEADER_OFFSET;
        }
        else
        {
            u4IpHdrOffset = VLAN_ENETV2_IP_HEADER_OFFSET;
        }
    }
    else
    {
        /* LLC-SNAP frame */
        if (u1FrameType == VLAN_DT_FRAME)
        {
            VLAN_MEMCPY ((UINT1 *) &u2Protocol,
                         &pu1Data[VLAN_DT_LLCSNAP_PROTOCOL_OFFSET],
                         VLAN_PROTOCOL_LEN);

            /* If the protocol is IP/ARP/RARP this is the offset of the
             * IP/ARP/RARP header */

            u4IpHdrOffset = VLAN_DT_LLCSNAP_IPHDR_OFFSET;

        }
        else if (u1FrameType == VLAN_ST_FRAME)
        {
            VLAN_MEMCPY ((UINT1 *) &u2Protocol,
                         &pu1Data[VLAN_ST_LLCSNAP_PROTOCOL_OFFSET],
                         VLAN_PROTOCOL_LEN);

            /* If the protocol is IP/ARP/RARP this is the offset of the
             * IP/ARP/RARP header */

            u4IpHdrOffset = VLAN_ST_LLCSNAP_IPHDR_OFFSET;

        }
        else
        {
            VLAN_MEMCPY ((UINT1 *) &u2Protocol,
                         &pu1Data[VLAN_LLCSNAP_PROTOCOL_OFFSET],
                         VLAN_PROTOCOL_LEN);

            /* If the protocol is IP/ARP/RARP this is the offset of the
             * IP/ARP/RARP header */

            u4IpHdrOffset = VLAN_LLCSNAP_IPHDR_OFFSET;

        }

        u2Protocol = (UINT2) OSIX_NTOHS (u2Protocol);
    }

    VLAN_MEMSET (au1VlanIpHdr, 0, VLAN_IP_HDR_LEN);

    if (u2Protocol == VLAN_IP_PROTO_ID)
    {
        VLAN_COPY_FROM_BUF (pFrame, au1VlanIpHdr, u4IpHdrOffset,
                            VLAN_IP_HDR_LEN);
        pu1Data = au1VlanIpHdr;

        pIpHdr = (tVlanIpHeader *) (VOID *) pu1Data;

        u4SrcIp = pIpHdr->u4Src;
        u4SrcIp = (UINT4) OSIX_NTOHL (u4SrcIp);

        u4DstIp = pIpHdr->u4Dest;
        u4DstIp = (UINT4) OSIX_NTOHL (u4DstIp);
        u1Dscp = pIpHdr->u1Tos;
        u1Dscp >>= 2;

        pVlanSVlanIndex->u4SrcIp = u4SrcIp;
        pVlanSVlanIndex->u4DstIp = u4DstIp;
        pVlanSVlanIndex->u4Dscp = (UINT4) u1Dscp;

    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetServiceVlanId                             */
/*                                                                           */
/*    Description         : This function gets the service VLAN Id for a     */
/*                          port from the port's associated table type.      */
/*                                                                           */
/*    Input(s)            : SVlanClassificationParams - Updated with all     */
/*                                                      possible index       */
/*                                                      values from the      */
/*                                                       frame               */
/*                                                                           */
/*    Output(s)           : pVlanId - Updated with the classified S- VLAN    */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetServiceVlanId (tVlanSVlanClassificationParams
                        SVlanClassificationParams, tVlanId * pSVlanId)
{
    tVlanPortEntry     *pPortEntry;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;
    INT4                i4RetVal = VLAN_FAILURE;

    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;

    pPortEntry = VLAN_GET_PORT_ENTRY (SVlanClassificationParams.u2Port);

    pVlanPbPortEntry =
        VLAN_GET_PB_PORT_ENTRY (SVlanClassificationParams.u2Port);

    if ((pPortEntry == NULL) || (pVlanPbPortEntry == NULL))
    {
        return VLAN_FAILURE;
    }

    u4TblIndex =
        VlanPbGetSVlanTableIndex ((INT4) pVlanPbPortEntry->u1SVlanTableType);
    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return VLAN_FAILURE;
    }

    switch (pVlanPbPortEntry->u1SVlanTableType)
    {
        case VLAN_SVLAN_PORT_SRCMAC_TYPE:

            pSrcMacSVlanEntry =
                VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                               SVlanClassificationParams.
                                               u2Port,
                                               SVlanClassificationParams.
                                               SrcMacAddr);

            if (pSrcMacSVlanEntry != NULL &&
                pSrcMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pSrcMacSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }

            break;
        case VLAN_SVLAN_PORT_DSTMAC_TYPE:

            pDstMacSVlanEntry =
                VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                               SVlanClassificationParams.
                                               u2Port,
                                               SVlanClassificationParams.
                                               DstMacAddr);
            if (pDstMacSVlanEntry != NULL
                && pDstMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pDstMacSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }

            break;
        case VLAN_SVLAN_PORT_DSCP_TYPE:

            pDscpSVlanEntry =
                VlanPbGetDscpSVlanEntry (u4TblIndex,
                                         SVlanClassificationParams.u2Port,
                                         SVlanClassificationParams.u4Dscp);

            if (pDscpSVlanEntry != NULL &&
                pDscpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pDscpSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_SRCIP_TYPE:

            pSrcIpSVlanEntry =
                VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                          SVlanClassificationParams.u2Port,
                                          SVlanClassificationParams.u4SrcIp);

            if (pSrcIpSVlanEntry != NULL &&
                pSrcIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pSrcIpSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_DSTIP_TYPE:

            pDstIpSVlanEntry =
                VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                          SVlanClassificationParams.u2Port,
                                          SVlanClassificationParams.u4DstIp);
            if (pDstIpSVlanEntry != NULL &&
                pDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pDstIpSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_SRCDSTIP_TYPE:

            pSrcDstIpSVlanEntry =
                VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                             SVlanClassificationParams.u2Port,
                                             SVlanClassificationParams.
                                             u4SrcIp,
                                             SVlanClassificationParams.u4DstIp);

            if (pSrcDstIpSVlanEntry != NULL &&
                pSrcDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pSrcDstIpSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_CVLAN_TYPE:

            pCVlanSVlanEntry =
                VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                          SVlanClassificationParams.u2Port,
                                          SVlanClassificationParams.CVlanId);

            if (pCVlanSVlanEntry != NULL &&
                pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pCVlanSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE:

            pCVlanSrcMacSVlanEntry =
                VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                                SVlanClassificationParams.
                                                u2Port,
                                                SVlanClassificationParams.
                                                CVlanId,
                                                SVlanClassificationParams.
                                                SrcMacAddr);

            if (pCVlanSrcMacSVlanEntry != NULL &&
                pCVlanSrcMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pCVlanSrcMacSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE:

            pCVlanDstMacSVlanEntry =
                VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                                SVlanClassificationParams.
                                                u2Port,
                                                SVlanClassificationParams.
                                                CVlanId,
                                                SVlanClassificationParams.
                                                DstMacAddr);

            if (pCVlanDstMacSVlanEntry != NULL &&
                pCVlanDstMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pCVlanDstMacSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE:

            pCVlanDscpSVlanEntry =
                VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                              SVlanClassificationParams.
                                              u2Port,
                                              SVlanClassificationParams.
                                              CVlanId,
                                              SVlanClassificationParams.u4Dscp);

            if (pCVlanDscpSVlanEntry != NULL &&
                pCVlanDscpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pCVlanDscpSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        case VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE:

            pCVlanDstIpSVlanEntry =
                VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                               SVlanClassificationParams.
                                               u2Port,
                                               SVlanClassificationParams.
                                               CVlanId,
                                               SVlanClassificationParams.
                                               u4DstIp);

            if (pCVlanDstIpSVlanEntry != NULL &&
                pCVlanDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                *pSVlanId = pCVlanDstIpSVlanEntry->SVlanId;
                i4RetVal = VLAN_SUCCESS;
            }
            break;
        default:
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbAddEtherTypeSwapEntry                        */
/*                                                                           */
/*    Description         : This function is used to add a ether type swap   */
/*                          entry  in the Ethertype Swap Table (RBTREE)      */
/*                          and returns the Ether type swap entry.           */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2LocalEtherType - SVLAN Ethertype received from */
/*                          packet.                                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Pointer to the tEtherTypeSwapEntry.              */
/*                                                                           */
/*****************************************************************************/
tEtherTypeSwapEntry *
VlanPbAddEtherTypeSwapEntry (UINT2 u2Port, UINT2 u2LocalEtherType)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    pEtherTypeSwapEntry = (tEtherTypeSwapEntry *)
                           MemAllocMemBlk(gVlanPbPoolInfo.EtherTypeSwapPoolId);
    if (pEtherTypeSwapEntry == NULL)
    {
        return NULL;
    }

    pEtherTypeSwapEntry->u2Port = u2Port;
    pEtherTypeSwapEntry->u2LocalEtherType = u2LocalEtherType;
    pEtherTypeSwapEntry->u2RelayEtherType = 0;

    if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable,
                   pEtherTypeSwapEntry) != RB_SUCCESS)
    {
        MemReleaseMemBlock (gVlanPbPoolInfo.EtherTypeSwapPoolId,
                            (UINT1 *) pEtherTypeSwapEntry);
        return NULL;
    }

    return pEtherTypeSwapEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbDelEtherTypeSwapEntry                        */
/*                                                                           */
/*    Description         : This function is used to deleter ether type swap */
/*                          entry  from the Ethertype Swap Table (RBTREE)    */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2LocalEtherType - SVLAN Ethertype received from */
/*                          packet.                                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbDelEtherTypeSwapEntry (UINT2 u2Port, UINT2 u2LocalEtherType)
{
    tEtherTypeSwapEntry TempEtherTypeSwapEntry;
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    VLAN_MEMSET (&TempEtherTypeSwapEntry, 0, sizeof (TempEtherTypeSwapEntry));

    TempEtherTypeSwapEntry.u2Port = u2Port;
    TempEtherTypeSwapEntry.u2LocalEtherType = u2LocalEtherType;

    pEtherTypeSwapEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable,
                   &TempEtherTypeSwapEntry);

    if (pEtherTypeSwapEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable,
                      pEtherTypeSwapEntry) != RB_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    if (MemReleaseMemBlock (gVlanPbPoolInfo.EtherTypeSwapPoolId,
                            (UINT1 *) pEtherTypeSwapEntry) != MEM_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetEtherTypeSwapEntry                        */
/*                                                                           */
/*    Description         : This function is used to get the ether type swap */
/*                          entry  from the Ethertype Swap Table (RBTREE)    */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2LocalEtherType - SVLAN Ethertype received from */
/*                          packet.                                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2LocalEtherType - SVLAN Ethertype received from */
/*                          packet.                                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to the tEtherTypeSwapEntry               */
/*                                                                           */
/*****************************************************************************/
tEtherTypeSwapEntry *
VlanPbGetEtherTypeSwapEntry (UINT2 u2Port, UINT2 u2LocalEtherType)
{
    tEtherTypeSwapEntry TempEtherTypeSwapEntry;
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    VLAN_MEMSET (&TempEtherTypeSwapEntry, 0, sizeof (TempEtherTypeSwapEntry));

    TempEtherTypeSwapEntry.u2Port = u2Port;
    TempEtherTypeSwapEntry.u2LocalEtherType = u2LocalEtherType;

    pEtherTypeSwapEntry
        = RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanEtherTypeSwapTable,
                     &TempEtherTypeSwapEntry);

    return pEtherTypeSwapEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCmpEtherTypeSwapEntry                      */
/*                                                                           */
/*    Description         : This function is used to compare the two Ethertype*/
/*                          swap entries                                     */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - Ether type swap entries          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCmpEtherTypeSwapEntry (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tEtherTypeSwapEntry *pEtherSwapEntryA = pNodeA;
    tEtherTypeSwapEntry *pEtherSwapEntryB = pNodeB;

    if (pEtherSwapEntryA->u2Port != pEtherSwapEntryB->u2Port)
    {
        if (pEtherSwapEntryA->u2Port > pEtherSwapEntryB->u2Port)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    if (pEtherSwapEntryA->u2LocalEtherType !=
        pEtherSwapEntryB->u2LocalEtherType)
    {
        if (pEtherSwapEntryA->u2LocalEtherType >
            pEtherSwapEntryB->u2LocalEtherType)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfEtherSwapEntry                           */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          enabled for adding ether type swap info on per port*/
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbConfEtherSwapEntry (VOID)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        pEtherTypeSwapEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                              pVlanEtherTypeSwapTable);

        while (pEtherTypeSwapEntry != NULL)
        {
            if (pEtherTypeSwapEntry->u1RowStatus == VLAN_ACTIVE)
            {
                if (pEtherTypeSwapEntry->u2Port < VLAN_MAX_PORTS + 1)
                {
                    if (VlanHwAddEtherTypeSwapEntry (VLAN_CURR_CONTEXT_ID (),
                                                     VLAN_GET_PHY_PORT
                                                     (pEtherTypeSwapEntry->
                                                      u2Port),
                                                     pEtherTypeSwapEntry->
                                                     u2LocalEtherType,
                                                     pEtherTypeSwapEntry->
                                                     u2RelayEtherType) !=
                        VLAN_SUCCESS)
                    {
                        return VLAN_FAILURE;
                    }
                }
            }
            pEtherTypeSwapEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                 pVlanEtherTypeSwapTable,
                                                 pEtherTypeSwapEntry,
                                                 VlanPbCmpEtherTypeSwapEntry);
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfSVlanTranslationTable                    */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          enabled for adding SVlan swap info on per port   */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2DstPort - Used to refer physical port in case  */
/*                          of LA scenario                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbConfSVlanTranslationTable (UINT2 u2Port, UINT2 u2DstPort)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;
#
	MEMSET (&OutVidTransEntryInfo, 0, sizeof (tVidTransEntryInfo));
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        i4RetVal = VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                       u2Port, 0,
                                                       &OutVidTransEntryInfo);
        while (i4RetVal == L2IWF_SUCCESS)
        {
            if (u2Port != 0)
            {
                if (OutVidTransEntryInfo.u2Port != u2Port)
                {
                    /* In case of next port, break the loop */
                    break;
                }
            }

            if (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)
            {
                if (VlanAddVidTransEntryInHw (OutVidTransEntryInfo.u2Port,
                                              u2DstPort,
                                              OutVidTransEntryInfo.LocalVid,
                                              OutVidTransEntryInfo.RelayVid)
                    != VLAN_SUCCESS)
                {
                    return VLAN_FAILURE;
                }
            }
            i4RetVal =
                VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                    OutVidTransEntryInfo.u2Port,
                                                    OutVidTransEntryInfo.
                                                    LocalVid,
                                                    &OutVidTransEntryInfo);
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfHwSVlanClassificationTable               */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          enabled for adding SVlan Classification table    */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbConfHwSVlanClassificationTable (VOID)
{

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        VlanPbHwAddPortCVidSVlans ();
        VlanPbHwAddPortSrcMacSVlans ();
        VlanPbHwAddPortDstMacSVlans ();
        VlanPbHwAddPortDscpSVlans ();
        VlanPbHwAddPortSrcIpSVlans ();
        VlanPbHwAddPortDstIpSVlans ();
        VlanPbHwAddPortSrcDstIpSVlans ();
        VlanPbHwAddPortCVlanSrcMacSVlans ();
        VlanPbHwAddPortCVlanDstMacSVlans ();
        VlanPbHwAddPortCVlanDscpSVlans ();
        VlanPbHwAddPortCVlanDstIpSVlans ();
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveHwSVlanClassificationTable             */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbRemoveHwSVlanClassificationTable (VOID)
{

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        VlanPbHwRemovePortSrcMacSVlans ();
        VlanPbHwRemovePortDstMacSVlans ();
        VlanPbHwRemovePortCVlanSrcMacSVlans ();
        VlanPbHwRemovePortCVlanDstMacSVlans ();
        VlanPbHwRemovePortDscpSVlans ();
        VlanPbHwRemovePortCVlanDscpSVlans ();
        VlanPbHwRemovePortSrcIpSVlans ();
        VlanPbHwRemovePortDstIpSVlans ();
        VlanPbHwRemovePortSrcDstIpSVlans ();
        VlanPbHwRemovePortCVlanDstIpSVlans ();
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveHwSVlanTranslationTable                */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan swap entries         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2DstPort - Used to refer physical port in case  */
/*                          of LA scenario                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbRemoveHwSVlanTranslationTable (UINT2 u2Port, UINT2 u2DstPort)
{

    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        i4RetVal = VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                       u2Port, 0,
                                                       &OutVidTransEntryInfo);
        while (i4RetVal == L2IWF_SUCCESS)
        {
            if (u2Port != 0)
            {
                if (OutVidTransEntryInfo.u2Port != u2Port)
                {
                    break;
                }
            }

            if (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)
            {
                VlanDelVidTransEntryInHw (OutVidTransEntryInfo.u2Port,
                                          u2DstPort,
                                          OutVidTransEntryInfo.LocalVid,
                                          OutVidTransEntryInfo.RelayVid);
            }

            i4RetVal =
                VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                    OutVidTransEntryInfo.u2Port,
                                                    OutVidTransEntryInfo.
                                                    LocalVid,
                                                    &OutVidTransEntryInfo);
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveHwSVlanEtherTypeSwapTable              */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan ether type swap entries*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbRemoveHwSVlanEtherTypeSwapTable (VOID)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;
    UINT4               u4Port;

    pEtherTypeSwapEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                          pVlanEtherTypeSwapTable);
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        while (pEtherTypeSwapEntry != NULL)
        {

            if (pEtherTypeSwapEntry->u1RowStatus == VLAN_ACTIVE)
            {

                if (VlanHwDelEtherTypeSwapEntry (VLAN_CURR_CONTEXT_ID (),
                                                 VLAN_GET_PHY_PORT
                                                 (pEtherTypeSwapEntry->
                                                  u2Port),
                                                 pEtherTypeSwapEntry->
                                                 u2LocalEtherType,
                                                 pEtherTypeSwapEntry->
                                                 u2RelayEtherType) !=
                    VLAN_SUCCESS)
                {

                    u4Port = VLAN_GET_IFINDEX (pEtherTypeSwapEntry->u2Port);
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Deleting Ether type swap Entry"
                                   "Failed for Port %d \r\n", u4Port);
                }

            }

            pEtherTypeSwapEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                 pVlanEtherTypeSwapTable,
                                                 pEtherTypeSwapEntry,
                                                 VlanPbCmpEtherTypeSwapEntry);

        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveSVlanClassificationEntriesForPort      */
/*                                                                           */
/*    Description        : This function is called when the the port is      */
/*                         deleted                                           */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbRemoveSVlanClassificationEntriesForPort (UINT2 u2Port)
{
    UINT1               u1Index = 0;
    INT4                i4RetVal = VLAN_SUCCESS;

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    if (VlanPbHwRemovePortSrcMacSVlanForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    if (VlanPbHwRemovePortDstMacSVlanForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    if (VlanPbHwRemovePortDscpSVlanForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    if (VlanPbHwRemovePortSrcIpSVlanForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    if (VlanPbHwRemovePortDstIpSVlanForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    if (VlanPbHwRemovePortSrcDstIpSVlanForPort (u2Port, u1Index) ==
        VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index =
        (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    if (VlanPbHwRemovePortCVlanSrcMacForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index =
        (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    if (VlanPbHwRemovePortCVlanDstMacSVlanForPort (u2Port, u1Index) ==
        VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index =
        (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    if (VlanPbHwRemovePortCVlanDscpForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    u1Index =
        (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    if (VlanPbHwRemovePortCVlanDstIpForPort (u2Port, u1Index) == VLAN_FAILURE)
    {
        i4RetVal = VLAN_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveSVlanTranslationEntriesForPort       */
/*                                                                           */
/*    Description         : This function is called when the Port type is    */
/*                          Changed/ Port is deleted                         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbRemoveSVlanTranslationEntriesForPort (UINT2 u2Port)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal = VLAN_SUCCESS;
#endif

    i4RetVal = VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                   u2Port, 0,
                                                   &OutVidTransEntryInfo);
    while ((i4RetVal == L2IWF_SUCCESS) &&
           (OutVidTransEntryInfo.u2Port == u2Port))
    {
#ifdef NPAPI_WANTED
        if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
        {
            if (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)
            {

                if (VlanDelVidTransEntryInHw (OutVidTransEntryInfo.u2Port, 0,
                                              OutVidTransEntryInfo.LocalVid,
                                              OutVidTransEntryInfo.RelayVid)
                    != VLAN_SUCCESS)
                {
                    /* Even though this call has failed, proceed with
                     * other calls. There is no other way here. */
                    i4HwRetVal = VLAN_FAILURE;
                }
            }
        }
        UNUSED_PARAM(i4HwRetVal);
#endif

        /* Remove the entry from the Vid translation table. */
        OutVidTransEntryInfo.u1RowStatus = VLAN_DESTROY;
        if (VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            OutVidTransEntryInfo) ==
            L2IWF_FAILURE)
        {
            return VLAN_FAILURE;
        }
        i4RetVal =
            VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                OutVidTransEntryInfo.u2Port,
                                                OutVidTransEntryInfo.LocalVid,
                                                &OutVidTransEntryInfo);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveEtherTypeSwapEntriesForPort            */
/*                                                                           */
/*    Description         : This function is called when the Port type is    */
/*                          Changed/ Port is deleted                         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbRemoveEtherTypeSwapEntriesForPort (UINT2 u2Port)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;
    tEtherTypeSwapEntry *pTempEtherTypeSwapEntry = NULL;

    UINT4               u4Port;

    pEtherTypeSwapEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                          pVlanEtherTypeSwapTable);

    while (pEtherTypeSwapEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (pEtherTypeSwapEntry->u2Port);
        if (pEtherTypeSwapEntry->u2Port == u2Port)
        {

            if (pEtherTypeSwapEntry->u1RowStatus == VLAN_ACTIVE)
            {
                if (pEtherTypeSwapEntry->u2Port < VLAN_MAX_PORTS + 1)
                {
                    if (VlanHwDelEtherTypeSwapEntry
                        (VLAN_CURR_CONTEXT_ID (),
                         VLAN_GET_PHY_PORT (pEtherTypeSwapEntry->u2Port),
                         pEtherTypeSwapEntry->u2LocalEtherType,
                         pEtherTypeSwapEntry->u2RelayEtherType) != VLAN_SUCCESS)
                    {

                        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC |
                                       INIT_SHUT_TRC, VLAN_NAME,
                                       "Deleting Ethertype swap "
                                       "Entry Failed for Port %d \r\n", u4Port);

                        return VLAN_FAILURE;
                    }
                }
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              pVlanEtherTypeSwapTable, pEtherTypeSwapEntry)
                != RB_SUCCESS)
            {
                if (pEtherTypeSwapEntry->u2Port >= VLAN_MAX_PORTS + 1)
                {
                    return VLAN_FAILURE;
                }
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC, VLAN_NAME,
                               "Deleting Ethertype swap Entry "
                               "in Tree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

        }

        pTempEtherTypeSwapEntry = pEtherTypeSwapEntry;

        pEtherTypeSwapEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                             pVlanEtherTypeSwapTable,
                                             pTempEtherTypeSwapEntry,
                                             VlanPbCmpEtherTypeSwapEntry);

        if (pTempEtherTypeSwapEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock (gVlanPbPoolInfo.EtherTypeSwapPoolId,
                                    (UINT1 *) pTempEtherTypeSwapEntry) !=
                MEM_SUCCESS)

            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC, VLAN_NAME,
                                   "Free Ether type swap Entry "
                                   "in Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
                return VLAN_FAILURE;
            }

        }

    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandleResetPbPortPropertiesToHw            */
/*                                                                           */
/*    Description         : This function is used to reset the PB port       */
/*                          properties in pSrcPortEntry to physical port     */
/*                          u2DstPort in hardware. This function will be     */
/*                          called when a port is Deleted in the Vlan.       */
/*                                                                           */
/*    Input(s)            : u2Port - Physical Port Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHandleResetPbPortPropertiesToHw (UINT2 u2Port)
{
    tVlanPbPortEntry   *pSrcPbPortEntry = NULL;
    UINT2               u2Pcp;
    UINT1               u1Priority;
    UINT1               u1DropEligible;
    tHwVlanPbPcpInfo    HwVlanPbPcpInfo;
    UINT4               u4Port;

    MEMSET (&HwVlanPbPcpInfo, 0, sizeof (tHwVlanPbPcpInfo));
    pSrcPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pSrcPbPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "PCP encoding/decoding tables initialization failed. PB Port entry "
                  "NULL. \n");
        return;
    }
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return;
    }
    u4Port = VLAN_GET_IFINDEX (u2Port);
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanHwSetPortSVlanTranslationStatus (VLAN_CURR_CONTEXT_ID (),
                                                 VLAN_GET_PHY_PORT (u2Port),
                                                 VLAN_DISABLED) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB S-VLAN translation status"
                           "failed Port %d .\n", u4Port);

            return;
        }

        if (VlanHwSetPortEtherTypeSwapStatus (VLAN_CURR_CONTEXT_ID (),
                                              VLAN_GET_PHY_PORT (u2Port),
                                              VLAN_DISABLED) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Ethertype swap status"
                           "failed Port %d .\n", u4Port);

            return;
        }

        if (VlanHwPortMacLearningStatus (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_GET_PHY_PORT (u2Port),
                                         VLAN_ENABLED) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Mac Learning Status"
                           "failed Port %d .\n", u4Port);
        }

        if (VlanHwPortMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                        VLAN_GET_PHY_PORT (u2Port),
                                        VLAN_PB_DYNAMIC_UNICAST_SIZE) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Mac Learning Limit"
                           "failed Port %d .\n", u4Port);

            return;
        }

        if (VlanHwSetPortPcpSelection (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_PHY_PORT (u2Port),
                                       VLAN_8P0D_SEL_ROW) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port PCP Selection Row "
                           "failed Port %d .\n", u4Port);

            return;
        }

        if (VlanHwSetPortReqDropEncoding (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (u2Port),
                                          VLAN_SNMP_FALSE) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Required Drop Encoding"
                           "failed Port %d .\n", u4Port);

            return;
        }

        if (VlanHwSetPortUseDei (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2Port),
                                 VLAN_SNMP_FALSE) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Use DEI "
                           "failed Port %d .\n", u4Port);

            return;
        }

        VlanPbInitPcpValues (u2Port);

        HwVlanPbPcpInfo.u2PcpSelRow = pSrcPbPortEntry->u1PcpSelRow;

        for (u2Pcp = 0; u2Pcp < VLAN_PB_MAX_PCP; u2Pcp++)
        {
            HwVlanPbPcpInfo.u2PcpValue = u2Pcp;
            HwVlanPbPcpInfo.u2Priority
                = VLAN_PB_PCP_DECODE_PRIORITY
                (pSrcPbPortEntry, pSrcPbPortEntry->u1PcpSelRow, u2Pcp);
            HwVlanPbPcpInfo.u1DropEligible
                = VLAN_PB_PCP_DECODE_DROP_ELIGIBLE
                (pSrcPbPortEntry, pSrcPbPortEntry->u1PcpSelRow, u2Pcp);

            if (VlanHwSetPcpDecodTbl (VLAN_CURR_CONTEXT_ID (),
                                      VLAN_GET_PHY_PORT (u2Port),
                                      HwVlanPbPcpInfo) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting PB Port PCP Decoding Table "
                               "failed Port %d .\n", u4Port);

                return;
            }
        }

        for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
        {
            for (u1DropEligible = 0; u1DropEligible < VLAN_PB_MAX_DROP_ELIGIBLE;
                 u1DropEligible++)
            {
                HwVlanPbPcpInfo.u2Priority = u1Priority;
                HwVlanPbPcpInfo.u1DropEligible = u1DropEligible;
                HwVlanPbPcpInfo.u2PcpValue
                    = VLAN_PB_PCP_ENCODE_PCP (pSrcPbPortEntry,
                                              pSrcPbPortEntry->u1PcpSelRow,
                                              u1Priority, u1DropEligible);

                if (VlanHwSetPcpEncodTbl (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (u2Port),
                                          HwVlanPbPcpInfo) != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   VLAN_NAME,
                                   "Setting PB Port PCP Encoding Table "
                                   "failed Port %d .\n", u4Port);
                    return;
                }
            }
        }

        if (VlanHwSetProviderBridgePortType (VLAN_CURR_CONTEXT_ID (),
                                             VLAN_GET_PHY_PORT
                                             (u2Port),
                                             VLAN_CUSTOMER_BRIDGE_PORT) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port type failed."
                           "Port %d .\n", u4Port);

            return;
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandleCopyPbPortPropertiesToHw             */
/*                                                                           */
/*    Description         : This function is used to set the PB port         */
/*                          properties in pSrcPortEntry to physical port     */
/*                          u2DstPort in hardware. This function will be     */
/*                          called when a port is created or a physical port */
/*                          is added to a port channel.                      */
/*                                                                           */
/*    Input(s)            : u2DstPort - Physical Port Index                  */
/*                          pSrcPortEntry - Port Entry structure whose       */
/*                          properties need to be propgrammed in physical    */
/*                          u2DstPort port                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandleCopyPbPortPropertiesToHw (UINT2 u2DstPort, UINT2 u2SrcPort,
                                      UINT1 u1InterfaceType)
{
    tVlanPbPortEntry   *pSrcPbPortEntry = NULL;
    tVlanPortEntry     *pSrcPortEntry = NULL;
    UINT4               u4SourcePort;
    UINT2               u2Pcp;
    UINT1               u1Priority;
    UINT1               u1SVlanTranslationStatus = OSIX_DISABLED;
    UINT1               u1DropEligible;
    UINT1               u1PbPortType;
    tHwVlanPbPcpInfo    HwVlanPbPcpInfo;
    UINT4               u4DstPort;

    MEMSET (&HwVlanPbPcpInfo, 0, sizeof (tHwVlanPbPcpInfo));

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        pSrcPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2SrcPort);
        pSrcPortEntry = VLAN_GET_PORT_ENTRY (u2SrcPort);
        if ((NULL == pSrcPortEntry) || (NULL == pSrcPbPortEntry))
        {
            return VLAN_FAILURE;
        }
        u1PbPortType = pSrcPbPortEntry->u1PbPortType;

        u4SourcePort = VLAN_GET_PHY_PORT (u2SrcPort);

        if (VcmGetIfIndexFromLocalPort
            (VLAN_CURR_CONTEXT_ID (), u2DstPort, &u4DstPort) != VCM_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "VCM Information for port %d failed.",
                           u2DstPort);

            return VLAN_FAILURE;
        }

        if (u1PbPortType == VLAN_CNP_TAGGED_PORT)
        {
            if (u1InterfaceType == VLAN_C_INTERFACE_TYPE)
            {
                /* In NP set the Port_Type as CNP_CTagged */
                u1PbPortType = CFA_CNP_CTAGGED_PORT;
            }
        }

        if (VlanHwSetProviderBridgePortType (VLAN_CURR_CONTEXT_ID (),
                                             u4DstPort,
                                             u1PbPortType) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port type failed."
                           "Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                           u4DstPort,
                                           pSrcPortEntry->
                                           u2IngressEtherType) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Ingress ethertype failed."
                           "Port %d , Src Port %d.\n", u4DstPort, u4SourcePort);
            return VLAN_FAILURE;
        }

        if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                          u4DstPort,
                                          pSrcPortEntry->
                                          u2EgressEtherType) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Egress ethertype failed."
                           "Port %d , SrcPort %d \n", u4DstPort, u4SourcePort);
            return VLAN_FAILURE;
        }

        if (VlanL2IwfPbGetVidTransStatus (VLAN_CURR_CONTEXT_ID (),
                                          u2SrcPort, &u1SVlanTranslationStatus)
            == L2IWF_FAILURE)
        {
            return VLAN_FAILURE;
        }

        if (VlanHwSetPortSVlanTranslationStatus (VLAN_CURR_CONTEXT_ID (),
                                                 u4DstPort,
                                                 u1SVlanTranslationStatus) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB S-VLAN translation status"
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (VlanHwSetPortEtherTypeSwapStatus (VLAN_CURR_CONTEXT_ID (),
                                              u4DstPort,
                                              pSrcPbPortEntry->
                                              u1EtherTypeSwapStatus) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Ethertype swap status"
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (pSrcPbPortEntry->u1DefCVlanClassify == VLAN_ENABLED)
        {
            if (VlanHwSetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                           (UINT2)
                                           u4DstPort,
                                           pSrcPbPortEntry->CVlanId) !=
                VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting PB Port Default Customer VLAN"
                               "failed Port %d .\n", u4DstPort);
                return VLAN_FAILURE;
            }
        }

        if (VlanHwSetPortSVlanClassifyMethod (VLAN_CURR_CONTEXT_ID (),
                                              u4DstPort,
                                              (UINT1) pSrcPbPortEntry->
                                              u1SVlanTableType) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port S-VLAN Table Type"
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (VlanHwPortMacLearningStatus (VLAN_CURR_CONTEXT_ID (),
                                         u4DstPort,
                                         pSrcPbPortEntry->
                                         u1MacLearningStatus) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Mac Learning Status"
                           "failed Port %d .\n", u4DstPort);
        }

        if (VlanHwPortMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                        u4DstPort,
                                        pSrcPbPortEntry->
                                        u4MacLearningLimit) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Mac Learning Limit"
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (VlanHwSetPortPcpSelection (VLAN_CURR_CONTEXT_ID (),
                                       u4DstPort,
                                       pSrcPbPortEntry->u1PcpSelRow) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port PCP Selection Row "
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (VlanHwSetPortReqDropEncoding (VLAN_CURR_CONTEXT_ID (),
                                          u4DstPort,
                                          pSrcPbPortEntry->u1ReqDropEncoding) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Required Drop Encoding"
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        if (VlanHwSetPortUseDei (VLAN_CURR_CONTEXT_ID (),
                                 u4DstPort,
                                 pSrcPbPortEntry->u1UseDei) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting PB Port Use DEI "
                           "failed Port %d .\n", u4DstPort);

            return VLAN_FAILURE;
        }

        /* In case of port channel is oper up, then the CVID registration entry
         * has to be copied to the member port of the port-channel.
         * If the port-channel is oper down, it is not need to program the CVID
         * registration entry, since it has been taken cared in the port-channel
         * oper up thread */

        if (pSrcPortEntry->u1OperStatus == VLAN_OPER_UP)
        {
            VlanPbNpUpdateCvidAndPepInHw (u2SrcPort, 0, u2DstPort, VLAN_ADD);
        }

        /* Copying the translation table from port-channel to port */
        VlanPbConfSVlanTranslationTable (u2SrcPort, u2DstPort);

        HwVlanPbPcpInfo.u2PcpSelRow = pSrcPbPortEntry->u1PcpSelRow;

        for (u2Pcp = 0; u2Pcp < VLAN_PB_MAX_PCP; u2Pcp++)
        {
            HwVlanPbPcpInfo.u2PcpValue = u2Pcp;
            HwVlanPbPcpInfo.u2Priority
                = VLAN_PB_PCP_DECODE_PRIORITY
                (pSrcPbPortEntry, pSrcPbPortEntry->u1PcpSelRow, u2Pcp);
            HwVlanPbPcpInfo.u1DropEligible
                = VLAN_PB_PCP_DECODE_DROP_ELIGIBLE
                (pSrcPbPortEntry, pSrcPbPortEntry->u1PcpSelRow, u2Pcp);

            if (VlanHwSetPcpDecodTbl (VLAN_CURR_CONTEXT_ID (),
                                      u4DstPort,
                                      HwVlanPbPcpInfo) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting PB Port PCP Decoding Table "
                               "failed Port %d .\n", u4DstPort);

                return VLAN_FAILURE;
            }
        }

        for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
        {
            for (u1DropEligible = 0; u1DropEligible < VLAN_PB_MAX_DROP_ELIGIBLE;
                 u1DropEligible++)
            {
                HwVlanPbPcpInfo.u2Priority = u1Priority;
                HwVlanPbPcpInfo.u1DropEligible = u1DropEligible;
                HwVlanPbPcpInfo.u2PcpValue
                    = VLAN_PB_PCP_ENCODE_PCP (pSrcPbPortEntry,
                                              pSrcPbPortEntry->u1PcpSelRow,
                                              u1Priority, u1DropEligible);

                if (VlanHwSetPcpEncodTbl (VLAN_CURR_CONTEXT_ID (),
                                          u4DstPort,
                                          HwVlanPbPcpInfo) != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   VLAN_NAME,
                                   "Setting PB Port PCP Encoding Table "
                                   "failed Port %d .\n", u4DstPort);
                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbConfHwFidProperties                           */
/*                                                                          */
/*     DESCRIPTION      : This function will be called when the bridge mode */
/*                        is changed to provider bridge mode. This function */
/*                        will configure fid mac learning limit and status and*/
/*                        also multicast mac limit                          */
/*                        Information                                       */
/*                                                                          */
/*     INPUT            : None                                              */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : None.                                             */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbConfHwFidProperties (VOID)
{

    VlanHwMulticastMacTableLimit (VLAN_CURR_CONTEXT_ID (),
                                  VLAN_PB_MULTICAST_TABLE_LIMIT);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsPortMacLearnLimitExceeded                */
/*                                                                           */
/*    Description         : This function checks whether port MAC Learning   */
/*                          limit has been exceeded or not                   */
/*                                                                           */
/*    Input(s)            : pVlanPortEntry - Port Entry                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_TRUE - if Limit exceeds                      */
/*                       : VLAN_FALSE - if Limit doesn't exceed              */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanPbIsPortMacLearnLimitExceeded (tVlanPbPortEntry * pVlanPbPortEntry)
{
    if (pVlanPbPortEntry->u4NumLearntMacEntries + 1 >
        pVlanPbPortEntry->u4MacLearningLimit)
    {
        return VLAN_TRUE;
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetPortMacLearningStatus                   */
/*                                                                           */
/*    Description         : This function returns port MAC learning status   */
/*                                                                           */
/*    Input(s)            : pVlanPortEntry - Port Entry                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_ENABLED/VLAN_DISABLED                        */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanPbGetPortMacLearningStatus (tVlanPbPortEntry * pVlanPbPortEntry)
{

    if (pVlanPbPortEntry->u1MacLearningStatus == VLAN_ENABLED)
    {
        return VLAN_ENABLED;
    }

    return VLAN_DISABLED;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsMulticastMacLimitExceeded                */
/*                                                                           */
/*    Description         : This function check multicast mac limit          */
/*                          limit                                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_TRUE - if Limit exceeds                      */
/*                       : VLAN_FALSE - if Limit doesn't exceed              */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanPbIsMulticastMacLimitExceeded (VOID)
{
    if ((VLAN_PB_MULTICAST_TABLE_COUNT + 1) > VLAN_PB_MULTICAST_TABLE_LIMIT)
    {
        return VLAN_TRUE;
    }
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateCurrentMulticastMacCount             */
/*                                                                           */
/*    Description         : This function updateds multicast mac count       */
/*                                                                           */
/*    Input(s)            : u1flag - ADD/DELETE option for count             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbUpdateCurrentMulticastMacCount (UINT1 u1Flag)
{
    if (u1Flag == VLAN_ADD)
    {
        VLAN_PB_MULTICAST_TABLE_COUNT++;
    }
    else
    {
        VLAN_PB_MULTICAST_TABLE_COUNT--;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateTagEtherType()                         */
/*                                                                           */
/*    Description         : This function updates the Rthertype field of the */
/*                          tagged frames.                                   */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          u2EtherType  - EtherType value to be filled in   */
/*                          the frame.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanPbUpdateTagEtherType (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2EtherType)
{
    UINT1              *pu1Ptr = NULL;

    u2EtherType = (UINT2) (OSIX_HTONS (u2EtherType));

    pu1Ptr = VLAN_BUF_IF_LINEAR (pFrame, VLAN_TAG_OFFSET, VLAN_TAG_SIZE);

    if (pu1Ptr != NULL)
    {

        MEMCPY (pu1Ptr, &u2EtherType, VLAN_TAG_SIZE);
    }
    else
    {

        VLAN_COPY_TO_BUF (pFrame, &u2EtherType, VLAN_TAG_OFFSET, VLAN_TAG_SIZE);
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateTagEtherType()                         */
/*                                                                           */
/*    Description         : This function updates the Rthertype field of the */
/*                          tagged frames.                                   */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          u2EtherType  - EtherType value to be filled in   */
/*                          the frame.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanUpdateTagEtherType (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2EtherType)
{
    UINT1              *pu1Ptr = NULL;

    u2EtherType = (UINT2) (OSIX_HTONS (u2EtherType));

    pu1Ptr = VLAN_BUF_IF_LINEAR (pFrame, VLAN_TAG_OFFSET, VLAN_TAG_SIZE);

    if (pu1Ptr != NULL)
    {

        MEMCPY (pu1Ptr, &u2EtherType, VLAN_TAG_SIZE);
    }
    else
    {

        VLAN_COPY_TO_BUF (pFrame, &u2EtherType, VLAN_TAG_OFFSET, VLAN_TAG_SIZE);
    }
}

/********************PROVIDER BRIDGE CHANGES*********************************/

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfCustomerEdgePort                       */
/*                                                                           */
/*    Description         : This function is used to configure the           */
/*                          properties of the customer edge port.            */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index for which port type is       */
/*                                   changed                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbConfCustomerEdgePort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* If this Port is previously a trunk port, then we should remove
     * all the trunk related properties from that port*/

    pVlanPortEntry->u1PortType = VLAN_ACCESS_PORT;

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_ENABLED;

    pVlanPortEntry->u1AccpFrmTypes =
        VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_CVLAN_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_ENABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1SVlanPriorityType = VLAN_SVLAN_PRIORITY_TYPE_NONE;
    pVlanPbPortEntry->u1EncapType = VLAN_PB_PORT_AS_DOT1Q_DIS;
    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u1BundleStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1MultiplexStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;
    VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_CUSTOMER_PROTOCOL_ID;
    VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1UntagFrameOnCEP = VLAN_DISABLED;
    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_DISABLED);

    /* If the default tunnel status of this port for any protocol below is modified, it needs
     * to be taken care on the global variable gau1PbDefPortProtoTunnelStatus also for this port*/

    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_TUNNEL_PROTOCOL_DISCARD);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_TUNNEL_PROTOCOL_DISCARD);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_TUNNEL_PROTOCOL_DISCARD);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_TUNNEL_PROTOCOL_DISCARD);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfCustomerNetworkPort                    */
/*                                                                           */
/*    Description         : This function is used to configure the           */
/*                          properties of the customer network port.         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index for which port type is changed*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/

INT4
VlanPbConfCustomerNetworkPort (UINT2 u2Port, UINT1 u1ServiceType)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT1               u1InterfaceType = VLAN_INVALID_INTERFACE_TYPE;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1SVlanPriorityType = VLAN_SVLAN_PRIORITY_TYPE_NONE;
    pVlanPbPortEntry->u1EncapType = VLAN_PB_PORT_AS_DOT1Q_DIS;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;

    pVlanPortEntry->u1PortProtoBasedClassification = VLAN_DISABLED;
    pVlanPortEntry->u1MacBasedClassification = VLAN_DISABLED;

    if (u1ServiceType == VLAN_PORT_BASED_SERVICE)
    {
	VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROTOCOL_ID;
        pVlanPortEntry->u1PortType = VLAN_ACCESS_PORT;
        VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_ENABLED;
        pVlanPbPortEntry->u1PortServiceType = VLAN_PORT_BASED_SERVICE;

        pVlanPortEntry->u1AccpFrmTypes =
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
        pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;

        L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                     pVlanPortEntry->u1AccpFrmTypes);

        VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                      OSIX_DISABLED);
    }
    else if (u1ServiceType == VLAN_STAG_BASED_SERVICE)
    {
        pVlanPortEntry->u1PortType = VLAN_HYBRID_PORT;
        VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_DISABLED;
        pVlanPbPortEntry->u1PortServiceType = VLAN_STAG_BASED_SERVICE;

        pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
        pVlanPortEntry->u1IngFiltering = VLAN_SNMP_TRUE;

        L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                     pVlanPortEntry->u1AccpFrmTypes);
        VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                      OSIX_ENABLED);
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
    {
        VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (), &u1InterfaceType);

        if ((u1InterfaceType == VLAN_C_INTERFACE_TYPE))
        {
            VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROTOCOL_ID;
            VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROTOCOL_ID;
        }

        /*For C-Tagged to port-based and port-based to c-tagged: delete and
           create indication is sent instead of change interface type 
           indication from L2IWF, therefore updating the interface type here. */
        if ((u1InterfaceType == VLAN_C_INTERFACE_TYPE) ||
            (u1ServiceType == VLAN_PORT_BASED_SERVICE))
        {
            VlanPbbUpdateInterfaceType (u1InterfaceType);
            if (u1ServiceType == VLAN_PORT_BASED_SERVICE)
            {
                VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_ENABLED;
                pVlanPortEntry->u1AccpFrmTypes =
                    VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
            }
        }

    /* If the default tunnel status of this port for any protocol below is modified, it needs
     * to be taken care on the global variable gau1PbDefPortProtoTunnelStatus also for this port*/
        VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    }
    else
    {
        VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    }
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_DOT1X_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_LACP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_STP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_GVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_GMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_MVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_MMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_IGMP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfPropCustomerEdgePort                   */
/*                                                                           */
/*    Description         : This function is used to configure the           */
/*                          properties of the Proprietary customer Edge port.*/
/*                                                                           */
/*    Input(s)            : u2Port - Port Id for which port type is changed  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/

INT4
VlanPbConfPropCustomerEdgePort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    pVlanPortEntry->u1PortType = VLAN_ACCESS_PORT;

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_ENABLED;

    pVlanPortEntry->u1AccpFrmTypes =
        VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;
    VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;

    /* If the default tunnel status of this port for any protocol below is modified, it needs
     * to be taken care on the global variable gau1PbDefPortProtoTunnelStatus also for this port*/
    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_DISABLED);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfPropCustomerNetworkPort                */
/*                                                                           */
/*    Description         : This function is used to configure the           */
/*                          properties of the Proprietary customer network   */
/*                          port.                                            */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id for which port type is changed  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbConfPropCustomerNetworkPort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    /* To enable tunnel status for a port, the port-type needs to be set as 
     * access port. But for PCNP port alone we deviate from our convention by 
     * setting tunnel status as enable for an non access-port. */

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_ENABLED;

    pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;
    VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;

    /* If the default tunnel status of this port for any protocol below is modified, it needs
     * to be taken care on the global variable gau1PbDefPortProtoTunnelStatus also for this port*/
    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_TUNNEL_PROTOCOL_TUNNEL);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_DISABLED);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfPropProviderNetworkPort                */
/*                                                                           */
/*    Description         : This function is used to configure the           */
/*                          properties of the Proprietary Provider network   */
/*                          port.                                            */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id for which port type is changed  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbConfPropProviderNetworkPort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2EgressEtherType;
    UINT2               u2IngressEtherType;

    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return VLAN_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_DISABLED;

    pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
    {
        pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
        pVlanPortEntry->u1IngFiltering = VLAN_SNMP_TRUE;

        VlanGetPrimaryContextEtherType (pVlanPortEntry->u4PhyIfIndex,
                                        &u2EgressEtherType,
                                        VLAN_EGRESS_ETHERTYPE);
        VlanGetPrimaryContextEtherType (pVlanPortEntry->u4PhyIfIndex,
                                        &u2IngressEtherType,
                                        VLAN_INGRESS_ETHERTYPE);

        VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = u2EgressEtherType;
        VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = u2IngressEtherType;
    }
    else
    {
        pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
        pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;

        VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_CUSTOMER_PROTOCOL_ID;
        VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_CUSTOMER_PROTOCOL_ID;
    }

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;

    /* If the default tunnel status of this port for any protocol below is modified, it needs
     * to be taken care on the global variable gau1PbDefPortProtoTunnelStatus also for this port*/
    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_DISABLED);

    return VLAN_SUCCESS;
}

/* PB routines related to PCP encoding/decoding */
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbInitPcpValues                              */
/*                                                                           */
/*    Description         : This function initialises the PCP decoding and   */
/*                          encoding tables for physical ports with default  */
/*                          values.                                          */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbInitPcpValues (UINT2 u2Port)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1PcpSelRow;
    UINT1               u1PcpVal;
    UINT1               u1Priority;
    UINT1               u1DropEligible;

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "PCP encoding/decoding tables initialization failed. PB Port entry "
                  "NULL. \n");
        return;
    }

    /* Initialise PCP decoding table */
    for (u1PcpSelRow = 0; u1PcpSelRow < VLAN_PB_MAX_PCP_SEL_ROW; u1PcpSelRow++)
    {
        for (u1PcpVal = 0; u1PcpVal < VLAN_PB_MAX_PCP; u1PcpVal++)
        {
            pVlanPbPortEntry->au1PcpDecoding[u1PcpSelRow][u1PcpVal].u1Priority
                = gau1PcpDecPriority[u1PcpSelRow][u1PcpVal];

            pVlanPbPortEntry->au1PcpDecoding[u1PcpSelRow]
                [u1PcpVal].u1DropEligible
                = gau1PcpDecDropEligible[u1PcpSelRow][u1PcpVal];
        }
    }

    /* Initialise PCP encoding table */

    for (u1PcpSelRow = 0; u1PcpSelRow < VLAN_PB_MAX_PCP_SEL_ROW; u1PcpSelRow++)
    {
        for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
        {
            for (u1DropEligible = 0; u1DropEligible < VLAN_PB_MAX_DROP_ELIGIBLE;
                 u1DropEligible++)
            {
                pVlanPbPortEntry->au1PcpEncoding[u1PcpSelRow][u1Priority]
                    [u1DropEligible]
                    = gau1PcpEncValue[u1PcpSelRow][u1Priority][u1DropEligible];
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetPcpDecodeVal                            */
/*                                                                           */
/*    Description         : This function returns the priority and drop      */
/*                          eligible values associated with the given PCP.   */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u2Port       - Port number                       */
/*                          u1Pcp        - PCP value                         */
/*                                                                           */
/*    Output(s)           : pu1Priority  - Priority associated with the      */
/*                                         incoming frame                    */
/*                          u1DropEligible - Drop eligible value associated  */
/*                                           with the incoming frame.        */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbGetPcpDecodeVal (UINT2 u2Port, UINT1 u1InPcp, UINT1 *pu1Priority,
                       UINT1 *pu1DropEligible)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1PcpSelRow;

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "PCP decoding table value get failed. PB Port entry "
                  "NULL. \n");
        return;
    }
    u1PcpSelRow = pVlanPbPortEntry->u1PcpSelRow;
    if (u1PcpSelRow == 0)
    {
        return;
    }

    *pu1Priority = VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry, u1PcpSelRow,
                                                u1InPcp);
    *pu1DropEligible = VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                                         u1PcpSelRow, u1InPcp);
    return;
}

/* PB routines related to PEP creation,deletion and oper status updation */
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCmpLogicalPortEntry                        */
/*                                                                           */
/*    Description         : This function is used to compare two logical port*/
/*                          table entries                                    */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - Logical port table entries       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCmpLogicalPortEntry (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntryA = pNodeA;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntryB = pNodeB;

    if (pVlanPbLogicalPortEntryA->u2CepPort !=
        pVlanPbLogicalPortEntryB->u2CepPort)
    {
        if (pVlanPbLogicalPortEntryA->u2CepPort >
            pVlanPbLogicalPortEntryB->u2CepPort)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    if (pVlanPbLogicalPortEntryA->SVlanId != pVlanPbLogicalPortEntryB->SVlanId)
    {
        if (pVlanPbLogicalPortEntryA->SVlanId >
            pVlanPbLogicalPortEntryB->SVlanId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    return 0;
}

/*****************************************************************************/
/*    Function Name       : VlanPbCreateLogicalPortEntry                     */
/*                                                                           */
/*    Description         : This function creates logical port (Provider     */
/*                          Edge port and internal customer network port)    */
/*                          in VLAN.This would be called whenever the        */
/*                          row status of a particular C-VID registration    */
/*                          table entry is made Active.                      */
/*                                                                           */
/*    Input(s)            : u2Port  - CEP port number                        */
/*                          CVlanId - C-VLAN ID to be set the PVID of PEP    */
/*                          SVlanId - Service VLAN ID                        */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT4
VlanPbCreateLogicalPortEntry (UINT2 u2Port, tVlanId CVlanId, tVlanId SVlanId)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry;
    UINT1               u1Priority;
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return VLAN_FAILURE;
    }

    pVlanPbLogicalPortEntry = VlanPbGetLogicalPortEntry (u2Port, SVlanId);

    /* Create the logical port (PEP) only when the S-VLAN is mapped
     * to the CEP for the first time. This is achieved through the
     * counter 'u2NoOfCvidEntries'. It will be set to 1 when the first
     * entry is created.
     * Upon calling this function further, this counter alone will be
     * incremented.This is to ensure that the PEP entry is not created
     * whenever this function is called.
     */
    if (pVlanPbLogicalPortEntry != NULL)
    {
        pVlanPbLogicalPortEntry->u2NoOfCvidEntries++;
        return VLAN_SUCCESS;
    }

    pVlanPbLogicalPortEntry =
        (tVlanPbLogicalPortEntry *) (VOID *)
        VlanPbGetMemBlock (VLAN_PB_LOGICAL_PORT_ENTRY_TYPE);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Logical port entry creation failed for port %d VLAN %d."
                       "Memory allocation failed. \n ", u2Port, SVlanId);
        return VLAN_FAILURE;
    }

    MEMSET (pVlanPbLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    pVlanPbLogicalPortEntry->u2CepPort = u2Port;
    pVlanPbLogicalPortEntry->SVlanId = SVlanId;
    pVlanPbLogicalPortEntry->Cpvid = CVlanId;
    pVlanPbLogicalPortEntry->u1DefUserPriority = VLAN_DEF_USER_PRIORITY;
    pVlanPbLogicalPortEntry->u1IngressFiltering = VLAN_SNMP_FALSE;
    pVlanPbLogicalPortEntry->u1AccptFrameType = VLAN_ADMIT_ALL_FRAMES;
    pVlanPbLogicalPortEntry->u2NoOfCvidEntries = 1;
    pVlanPbLogicalPortEntry->u1UnTagPepSet = VLAN_FALSE;
    pVlanPbLogicalPortEntry->u1CosPreservation = VLAN_DISABLED;

    pVlanPbLogicalPortEntry->u1OperStatus = VLAN_OPER_DOWN;

    for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
    {
        pVlanPbLogicalPortEntry->au1RegenPriority[u1Priority] = u1Priority;
    }

    /* The number of Active CVID entries should be set to 0.
     * This will be updated when the row status for the particular 
     * port,CVLANID, SVLANID tuple is made Active.
     */
    pVlanPbLogicalPortEntry->u2NoOfActiveCvidEntries = 0;

    if (RBTreeAdd
        (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
         pVlanPbLogicalPortEntry) != RB_SUCCESS)
    {
        VlanPbReleaseMemBlock (VLAN_PB_LOGICAL_PORT_ENTRY_TYPE,
                               (UINT1 *) pVlanPbLogicalPortEntry);
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Logical port entry creation failed for port %d VLAN %d."
                       "RBTreeAdd failed. \n ", u2Port, SVlanId);
        return VLAN_FAILURE;
    }

    if (VlanL2IwfPbCreateProviderEdgePort (VLAN_GET_IFINDEX (u2Port), SVlanId)
        == L2IWF_FAILURE)
    {
        /* PEP creation in L2IWF failed. Delete the PEP created in VLAN 
         * as well
         */
        if (RBTreeRemove
            (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
             pVlanPbLogicalPortEntry) != RB_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Logical port entry deletion failed for port %d VLAN %d."
                           "RBTree remove failed \n ", u2Port, SVlanId);
            return VLAN_FAILURE;
        }
        VlanPbReleaseMemBlock (VLAN_PB_LOGICAL_PORT_ENTRY_TYPE,
                               (UINT1 *) pVlanPbLogicalPortEntry);
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbDelLogicalPortEntry                        */
/*                                                                           */
/*    Description         : This function deletes the logical port entry     */
/*                          created for a particular (CEP,S-VLAN) pair.      */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                          SVlanId - Service VLAN ID                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbDelLogicalPortEntry (UINT2 u2Port, tVlanId SVlanId)
{
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    /* Delete the logical port only when the number of active CVID 
     * entries associated with this PEP (u2NoOfCvidEntries) is 1.
     * Otherwise just decrement the counter and return.
     */
    VLAN_MEMSET (&VlanPbLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    VlanPbLogicalPortEntry.u2CepPort = u2Port;
    VlanPbLogicalPortEntry.SVlanId = SVlanId;

    pVlanPbLogicalPortEntry
        =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                   &VlanPbLogicalPortEntry);
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return VLAN_FAILURE;
    }

    if (pVlanPbLogicalPortEntry != NULL)
    {
        if (pVlanPbLogicalPortEntry->u2NoOfCvidEntries == 1)
        {
            /* 
             * Number of entries in CVID registration table is 
             * just 1. Hence delete the corresponding provider edge port.
             */

            VlanL2IwfPbPepDeleteIndication (VLAN_GET_IFINDEX (u2Port), SVlanId);

            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                 pVlanPbLogicalPortEntry) != RB_SUCCESS)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "Logical port entry deletion failed for port %d VLAN %d."
                               "RBTree remove failed \n ", u2Port, SVlanId);
                return VLAN_FAILURE;
            }
            VlanPbReleaseMemBlock (VLAN_PB_LOGICAL_PORT_ENTRY_TYPE,
                                   (UINT1 *) pVlanPbLogicalPortEntry);
        }
        else
        {
            /* 
             * Number of active entries in CVID registration table
             * is more than 1. Hence just decrement the counter and 
             * return.
             */
            pVlanPbLogicalPortEntry->u2NoOfCvidEntries--;
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetLogicalPortEntry                        */
/*                                                                           */
/*    Description         : This function returns the logical port entry     */
/*                          configured for a particular (CEP,S-VLAN) pair.   */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                          SVlanId - Service VLAN ID                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Pointer to the tVlanPbLogicalPortEntry.          */
/*                                                                           */
/*****************************************************************************/
tVlanPbLogicalPortEntry *
VlanPbGetLogicalPortEntry (UINT2 u2Port, tVlanId SVlanId)
{
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    VLAN_MEMSET (&VlanPbLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    VlanPbLogicalPortEntry.u2CepPort = u2Port;
    VlanPbLogicalPortEntry.SVlanId = SVlanId;

    pVlanPbLogicalPortEntry
        =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                   &VlanPbLogicalPortEntry);

    return pVlanPbLogicalPortEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetFirstLogicalPortEntry                   */
/*                                                                           */
/*    Description         : This function returns the first index values     */
/*                          from PEP configuration table                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : pi4FsPbPort - First Interface Index in table     */
/*                          pu4FsPbSVlan - First S-VLAN Index in table       */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the tVlanPbLogicalPortEntry.           */
/*                                                                           */
/*****************************************************************************/
tVlanPbLogicalPortEntry *
VlanPbGetFirstLogicalPortEntry (INT4 *pi4FsPbPort, UINT4 *pu4FsPbSVlan)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    pVlanPbLogicalPortEntry =
        (tVlanPbLogicalPortEntry *) RBTreeGetFirst
        (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable);

    if (pVlanPbLogicalPortEntry != NULL)
    {
        *pi4FsPbPort = (INT4) pVlanPbLogicalPortEntry->u2CepPort;
        *pu4FsPbSVlan = pVlanPbLogicalPortEntry->SVlanId;
        return pVlanPbLogicalPortEntry;
    }
    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Get First index of logical port entry  failed ");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetNextLogicalPortEntry                    */
/*                                                                           */
/*    Description         : This function returns the next valid index       */
/*                          values from the PEP configuration table.         */
/*                                                                           */
/*    Input(s)            :  i4FsPbPort - Current Port Index value           */
/*                           SVlanId  -   Current SVLAN Index                */
/*                                                                           */
/*    Output(s)           : pi4NextFsPbPort - Next Interface Index in table  */
/*                          pu4NextSVlanId  - Next S-VLAN Index in table     */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the tVlanPbLogicalPortEntry.           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
tVlanPbLogicalPortEntry *
VlanPbGetNextLogicalPortEntry (INT4 i4FsPbPort, tVlanId SVlanId,
                               INT4 *pi4NextFsPbPort, UINT4 *pu4NextSVlanId)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return NULL;
    }

    if (SVlanId > VLAN_MAX_VLAN_ID)
    {
        return NULL;
    }

    VLAN_MEMSET (&VlanPbLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    VlanPbLogicalPortEntry.u2CepPort = (UINT2) i4FsPbPort;
    VlanPbLogicalPortEntry.SVlanId = SVlanId;

    pVlanPbLogicalPortEntry =
        (tVlanPbLogicalPortEntry *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                                   pVlanLogicalPortTable,
                                                   (tRBElem *) &
                                                   VlanPbLogicalPortEntry,
                                                   NULL);
    if (pVlanPbLogicalPortEntry != NULL)
    {
        *pi4NextFsPbPort = (INT4) pVlanPbLogicalPortEntry->u2CepPort;
        *pu4NextSVlanId = (UINT4) pVlanPbLogicalPortEntry->SVlanId;
        return pVlanPbLogicalPortEntry;
    }
    VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "Get next index of logical port entry failed for port %d"
                   "VLAN %d \n", i4FsPbPort, SVlanId);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemovePortCVidEntries                      */
/*                                                                           */
/*    Description         : This function updates the Port CVID Registration */
/*                          table entries and PEP Entries                    */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_FAILURE/VLAN_SUCCESS                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbRemovePortCVidEntries (UINT2 u2Port)
{
    tVlanCVlanSVlanEntry *pCVidEntry = NULL;
    tVlanCVlanSVlanEntry *pTempCVidEntry = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    tVlanId             SVlanId;
    UINT1               u1Index = 0;
    UINT4               u4Port;

    u1Index = (UINT1) VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    u4Port = VLAN_GET_IFINDEX (u2Port);

    if (u1Index >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return VLAN_FAILURE;
    }

    pCVidEntry = RBTreeGetFirst
        (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pCVidEntry != NULL)
    {
        if (pCVidEntry->u2Port == u2Port)
        {
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pCVidEntry) != RB_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Removing CVID Entry"
                                   "from RBTree Failed for Port %d \r\n",
                                   u4Port);
                    i4RetVal = VLAN_FAILURE;
                }
            }

        }
        pTempCVidEntry = pCVidEntry;
        SVlanId = pCVidEntry->SVlanId;

        pCVidEntry = RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
             pTempCVidEntry, VlanPbPortCVlanCmp);

        if (pTempCVidEntry->u2Port == u2Port)
        {
            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_TYPE,
                                   (UINT1 *) pTempCVidEntry);
            VlanPbDelLogicalPortEntry (u2Port, SVlanId);
        }

    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdatePepOperStatus                        */
/*                                                                           */
/*    Description         : This function updates the oper status of logical */
/*                          ports (PEP/internal CNP) maintained in VLAN      */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                          SVlanId - Service VLAN ID                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbUpdatePepOperStatus (UINT2 u2Port, tVlanId SVlanId, UINT1 u1OperUpdate,
                           tVlanPbLogicalPortEntry * pVlanPbLogicalPortEntry)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT1               u1PepPrevOperStatus;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "PEP oper status updation failed for port %d VLAN %d \n"
                       "VLAN port entry is NULL for this port", u2Port,
                       SVlanId);
        return;
    }

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return;
    }

    u1PepPrevOperStatus = pVlanPbLogicalPortEntry->u1OperStatus;

    if (((u1PepPrevOperStatus == VLAN_OPER_UP) && (u1OperUpdate == VLAN_TRUE))
        || ((u1PepPrevOperStatus == VLAN_OPER_DOWN)
            && (u1OperUpdate == VLAN_FALSE)))
    {
        return;
    }
    if (u1OperUpdate == VLAN_TRUE)
    {
        /* Call for PEP oper status updation */

        /* The PEP oper status can be made UP only when all the following conditions
         * are satisfied.
         *  - The CEP must be operationally UP
         *  - S-VLAN row status must be active
         *  - There must be atleast one entry in C-VID registration table for this
         *    S-VLAN with row status Active.
         */

        if (VLAN_PEP_MAKE_OPER_UP (pVlanPortEntry, pVlanPbLogicalPortEntry)
            == VLAN_TRUE)
        {
            pVlanPbLogicalPortEntry->u1OperStatus = VLAN_OPER_UP;
        }
        else
        {
            pVlanPbLogicalPortEntry->u1OperStatus = VLAN_OPER_DOWN;
        }
    }
    else
    {
        /* Call for making the PEP oper down */
        pVlanPbLogicalPortEntry->u1OperStatus = VLAN_OPER_DOWN;
    }

    /* L2IWF and RSTP are notified with the PEP oper status only when
     * there is a change in PEP oper status
     */

    if (u1PepPrevOperStatus == pVlanPbLogicalPortEntry->u1OperStatus)
    {
        /*No Change in Oper Status, so no need to program Hw and L2IWf */
        return;
    }

#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (pVlanPbLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
        {
            if (VlanPbNpUpdateCvidAndPepInHw (u2Port, SVlanId, 0, VLAN_ADD) ==
                VLAN_FAILURE)
            {
                return;
            }
        }
        else if (pVlanPbLogicalPortEntry->u1OperStatus == VLAN_OPER_DOWN)
        {
            if (VlanPbNpUpdateCvidAndPepInHw (u2Port, SVlanId, 0, VLAN_DELETE)
                == VLAN_FAILURE)
            {
                return;
            }
        }
    }
#endif /* NPAPI_WANTED */
    if (u2Port < VLAN_MAX_PORTS + 1)
    {
        VlanL2IwfPbUpdatePepOperStatus (VLAN_GET_IFINDEX (u2Port), SVlanId,
                                        pVlanPbLogicalPortEntry->u1OperStatus);
    }
    return;
}

/*****************************************************************************/
/*    Function Name       : VlanPbIsSVlanActive                              */
/*    Description         : This function checks whether the given SVLAN is  */
/*                          Active.                                          */
/*    Input(s)            : SVlanId - Service VLAN ID                        */
/*    Output(s)           : None                                             */
/*    Returns             : VLAN_TRUE/VLAN_FALSE                             */
/*****************************************************************************/
INT4
VlanPbIsSVlanActive (tVlanId SVlanId)
{
    tVlanCurrEntry     *pVlanEntry = NULL;

    /* The SVLAN is assumed to be Active when VlanCurrEntry  
     * for this VLAN is not NULL.
     */
    pVlanEntry = VlanGetVlanEntry (SVlanId);
    if (pVlanEntry == NULL)
    {
        return VLAN_FALSE;
    }
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNotifySVlanStatusForPep                    */
/*                                                                           */
/*    Description         : This function updates the PEP oper status        */
/*                          based on the row status of SVLAN. This would     */
/*                          be called whenever the row status of any static  */
/*                          VLAN changes.                                    */
/*                                                                           */
/*    Input(s)            : SVlanId     - Service VLAN ID                    */
/*                          u2RowStatus - SVLAN row status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbNotifySVlanStatusForPep (tVlanId SVlanId, UINT1 u1RowStatus)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanId             FirstSVid;
    UINT4               u4NextSVid;
    INT4                i4FirstPort;
    INT4                i4NextPort;

    /* Update the oper status of PEP whose SVLAN is same as
     * the one for which the row status is getting modified.
     * - When the row status of S-VLAN is made not-in-service or destroy,
     *   all the related PEPs can be made oper down. 
     * - When the row status of S-VLAN is made Active, the PEP oper status 
     *   should be updated based on the other factors (CEP oper status, CVID 
     *   entry row status, CEP member ship with S-VLAN) as well.
     */

    pVlanPbLogicalPortEntry = VlanPbGetFirstLogicalPortEntry (&i4NextPort,
                                                              &u4NextSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "PEP oper status updation based on SVLAN row status failed for"
                       "S-VLAN %d. No PEP associated with this SVLAN ID. \n",
                       SVlanId);
        return;
    }

    if (pVlanPbLogicalPortEntry->u2NoOfCvidEntries == 0)
    {
        return;
    }

    do
    {
        if (u4NextSVid == (UINT4) SVlanId)
        {
            /* First matching entry is found. Check whether PEP oper status 
             * must be made down or updated
             */
            if (u1RowStatus == VLAN_ACTIVE)
            {
                /* S-VLAN row status is Active. Update the oper status of 
                 * PEP */
                VlanPbUpdatePepOperStatus ((UINT2) i4NextPort,
                                           (tVlanId) u4NextSVid, VLAN_TRUE,
                                           pVlanPbLogicalPortEntry);
            }
            else
            {
                /* S-VLAN row status is NotInService or Destroy. 
                 * Make the oper status of PEP down. 
                 */
                VlanPbUpdatePepOperStatus ((UINT2) i4NextPort,
                                           (tVlanId) u4NextSVid, VLAN_FALSE,
                                           pVlanPbLogicalPortEntry);
            }
        }

        /* Continue with other entries to update the oper status of 
         * all PEPs for this S-VLAN ID.
         */

        i4FirstPort = i4NextPort;
        FirstSVid = (tVlanId) u4NextSVid;
    }
    while ((pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry
            (i4FirstPort, FirstSVid, &i4NextPort, &u4NextSVid)) != NULL);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNotifyCepOperStatusForPep                  */
/*                                                                           */
/*    Description         : This function updates the PEP oper status        */
/*                          based on the oper status of CEP.This would be    */
/*                          called whenever the CEP oper status changes.     */
/*                                                                           */
/*    Input(s)            : u2Port       - Customer Edge Port                */
/*                          u1OperStatus - CEP oper status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbNotifyCepOperStatusForPep (UINT2 u2Port, UINT1 u1OperStatus)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanId             FirstSVid = 0;
    UINT4               u4NextSVid;
    INT4                i4FirstPort;
    INT4                i4NextPort;

    /* Update the oper status of PEP based on the CEP oper status.
     * - When the oper status of CEP is made Down, all the
     *   related PEPs can be made oper down.
     * - When the oper staus of CEP is made UP, the PEP oper status
     *   must be updated based on other factors (CEP oper status, CVID
     *   entry row status, CEP member ship with S-VLAN) as well.
     */
    i4FirstPort = (INT4) u2Port;

    pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry (i4FirstPort,
                                                             FirstSVid,
                                                             &i4NextPort,
                                                             &u4NextSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "PEP oper status updation based on CEP oper status"
                       "failed for CEP %d. No PEP associated with"
                       "this CEP. \n", u2Port);
        return;
    }

    do
    {
        if (i4NextPort == u2Port)
        {
            if (u1OperStatus == VLAN_OPER_UP)
            {
                /* CEP oper status is UP. Hence update PEP oper 
                 * status */
                VlanPbUpdatePepOperStatus ((UINT2) i4NextPort,
                                           (tVlanId) u4NextSVid, VLAN_TRUE,
                                           pVlanPbLogicalPortEntry);
            }
            else
            {
                /* CEP oper status is Down. Make PEP oper down. */
                VlanPbUpdatePepOperStatus ((UINT2) i4NextPort,
                                           (tVlanId) u4NextSVid, VLAN_FALSE,
                                           pVlanPbLogicalPortEntry);
            }
        }
        else
        {
            break;
        }
        i4FirstPort = i4NextPort;
        FirstSVid = (tVlanId) u4NextSVid;
    }
    while ((pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry
            (i4FirstPort, FirstSVid, &i4NextPort, &u4NextSVid)) != NULL);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNotifyCVidStatusForPep                     */
/*                                                                           */
/*    Description         : This function updates the PEP oper status        */
/*                          based on the row status associated with the C-VID*/
/*                          registration table entries.                      */
/*                                                                           */
/*    Input(s)            : u2Port       - Customer Edge Port                */
/*                          SVid         - Service VLAN ID                   */
/*                          u1RowStatus  - Row status for this CVID entry    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbNotifyCVidStatusForPep (UINT2 u2Port, tVlanId SVid, UINT1 u1RowStatus)
{
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    UINT1               u1PrevOperStatus;

    VLAN_MEMSET (&VlanPbLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    VlanPbLogicalPortEntry.u2CepPort = u2Port;
    VlanPbLogicalPortEntry.SVlanId = SVid;

    /* Update the oper status of PEP based on the row status of
     * C-VID registration table entries.
     * - When the row status of all the C-VID registration table entries 
     *   associated with the (CEP,SVLAN) is made NotInService/Destroy, 
     *   the related PEP can be made oper down.
     * - When there is atleast one C-VID entry for this PEP with row status 
     *   Active,the oper status must be updated based on other factors 
     *   (CEP oper status, CVID entry row status, * CEP member ship 
     *   with S-VLAN) as well.
     */

    pVlanPbLogicalPortEntry
        = RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                     &VlanPbLogicalPortEntry);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "PEP oper status updation based on CVID row status failed for"
                       "port %d SVLAN %d. PEP port entry is NULL. \n",
                       u2Port, SVid);
        return;
    }
    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /*  There exists an entry in Logical port table. */
    if (u1RowStatus == VLAN_ACTIVE)
    {
        /* Row status is made Active.Hence increment the counter for the number 
         * of active CVID entries.
         */
        pVlanPbLogicalPortEntry->u2NoOfActiveCvidEntries++;
    }
    u1PrevOperStatus = pVlanPbLogicalPortEntry->u1OperStatus;

    if (VLAN_PEP_MAKE_OPER_UP (pVlanPortEntry, pVlanPbLogicalPortEntry)
        == VLAN_TRUE)
    {
        pVlanPbLogicalPortEntry->u1OperStatus = VLAN_OPER_UP;
    }
    else
    {
        pVlanPbLogicalPortEntry->u1OperStatus = VLAN_OPER_DOWN;
    }

    if ((u1PrevOperStatus != pVlanPbLogicalPortEntry->u1OperStatus)
        && (u2Port < VLAN_MAX_PORTS + 1))
    {
        VlanL2IwfPbUpdatePepOperStatus (VLAN_GET_IFINDEX (u2Port), SVid,
                                        pVlanPbLogicalPortEntry->u1OperStatus);
    }
    return;
}

/* PB routines related to packet processing */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbClassifySVlan                              */
/*                                                                           */
/*    Description         : This function associates service VLAN ID for the */
/*                          incoming frame for PCEP or PCNP based on the     */
/*                          proprietary S-VLAN classification mechanisms.    */
/*                          Otherwise port PVID will be assumed to be the    */
/*                          service VLAN ID                                  */
/*                                                                           */
/*    Input(s)            : u2Port       - Port number                       */
/*                          pFrame       - Incoming frame                    */
/*                          u1PktType    - single or double tagged.          */
/*                          CVlanId      - Customer VLAN ID associated       */
/*                                         with the incoming frame           */
/*                                                                           */
/*    Output(s)           : pSVlanId  - Service VLAN ID associated with      */
/*                                      the incoming frame                   */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbClassifySVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                     UINT1 u1PktType, tVlanId CVlanId, tVlanId * pSVlanId)
{
    tVlanSVlanClassificationParams SVlanClassify;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (CVlanId == VLAN_NULL_VLAN_ID)
    {
        return;
    }
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        return;
    }

    if (pVlanPbPortEntry->u1SVlanTableType != VLAN_SVLAN_PORT_PVID_TYPE)
    {
        MEMSET (&SVlanClassify, 0, sizeof (tVlanSVlanClassificationParams));
        SVlanClassify.u2Port = u2Port;
        SVlanClassify.u1FrameType = u1PktType;
        SVlanClassify.CVlanId = CVlanId;

        /* In case of PCEP/PCNP we need to know various fields (dst addr,
         * src mac addr, dst ip addr, src ip addr etc.,) present in the
         * packet for S-VLAN classication. So do them. */
        if ((pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_NETWORK_PORT)
            || (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
        {
            VLAN_COPY_FROM_BUF (pFrame, &(SVlanClassify.DstMacAddr),
                                0, ETHERNET_ADDR_SIZE);
            VLAN_COPY_FROM_BUF (pFrame, &(SVlanClassify.SrcMacAddr),
                                ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);
            VlanPbUpdateSVlanClassifyParams (pFrame, &SVlanClassify);
        }

        if (VlanPbGetServiceVlanId (SVlanClassify, pSVlanId) == VLAN_SUCCESS)
        {
            return;
        }
        else if ((pVlanPbPortEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT)
        {
            /* The frame should be dropped when there is no entry found 
             * in C-VID registration table for CEP ports.
             */
            *pSVlanId = VLAN_NULL_VLAN_ID;
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandlePktRxOnCep                           */
/*                                                                           */
/*    Description         : This function handles frame received on customer */
/*                          edge port.                                       */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pFrame    - Incoming frame                       */
/*                          u2Port    - Port number                          */
/*                          u1PktType - Packet type                          */
/*                          CVlanId   - C-VLAN ID                            */
/*                          pVlanTag  - S-VLAN tag information               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandlePktRxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanTag * pVlanTag)
{
    UINT4               u4TblIndex;
    tVlanId             CVlanId;
    UINT1               u1Priority;
    UINT1               u1TagType;
    UINT1               u1RegenPriority;
    UINT1               u1PortState;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry;
    tVlanPortEntry     *pVlanPortEntry;

    /* This function does the following.
     * - When the incoming frame is tagged and untaggedPep is set to True,
     *   the C-VLAN tag is removed.
     * - When the incoming frame is untagged and untagged Pep is set to 
     *   False, the C-VLAN tag is added with port PVID and port priority
     *   information configured for the CEP.
     * - Service priority regeneration. 
     */

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    CVlanId = pVlanTag->InnerVlanTag.u2VlanId;
    u1TagType = pVlanTag->InnerVlanTag.u1TagType;
    u1Priority = pVlanTag->InnerVlanTag.u1Priority;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return VLAN_FAILURE;
    }

    pCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port, CVlanId);

    if (pCVlanSVlanEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Packet handling on CEP failed. No entry found in"
                       "CVID registration table for CEP %d CVLAN %d \n",
                       u2Port, CVlanId);
        return VLAN_FAILURE;
    }
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return VLAN_FAILURE;
    }

    /* Get the port state for the outgoing provider edge port. */
    u1PortState = VlanL2IwfGetPbPortState (VLAN_GET_IFINDEX (u2Port),
                                           pVlanTag->OuterVlanTag.u2VlanId);

    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Dropping packet as the Provider Edge Port with Id as "
                       "Port: %d, SVId : %d is not in forwarding state.\n",
                       u2Port, (pVlanTag->OuterVlanTag.u2VlanId));
        return VLAN_FAILURE;
    }

    /* If UntaggedPep is True, then C-Tag the tagged or priority tagged
     * frames. */
    if ((pCVlanSVlanEntry->u1PepUntag == VLAN_SNMP_TRUE) &&
        (u1TagType != VLAN_UNTAGGED))
    {
        VlanUnTagFrame (pFrame);
    }
    else if (pCVlanSVlanEntry->u1PepUntag == VLAN_SNMP_FALSE)
    {
        if (u1TagType == VLAN_UNTAGGED)
        {
            /* Incoming frame is untagged. untaggedPep is False. Tag the 
             * incoming frame.
             */
            VlanAddTagToFrame (pFrame, CVlanId, u1Priority, VLAN_DE_FALSE,
                               pVlanPortEntry);
        }
        else if (u1TagType == VLAN_PRIORITY_TAGGED)
        {
            /* Incoming frame is priority c-tagged.untaggedPep is False. Update the 
             * tag in the incoming frame with the C-VLAN ID.
             */
            VlanUpdateVlanTag (pFrame, CVlanId, u1Priority, VLAN_DE_FALSE,
                               pVlanPortEntry);
        }
    }

    /* Service priority regeneration for customer packets. 
     * If the corresponding PEP/internal CNP does not exist, then
     * the below function will return failure.*/
    if (VlanPbServicePriorityRegen (u2Port, pVlanTag->OuterVlanTag.u2VlanId,
                                    u1Priority, &u1RegenPriority)
        == VLAN_FAILURE)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Packet handling on CEP %d failed. Service priority"
                       "regeneration failed for priority %d \n", u2Port,
                       u1Priority);
        return VLAN_FAILURE;
    }
    pVlanTag->OuterVlanTag.u1Priority = u1RegenPriority;
    pVlanTag->OuterVlanTag.u1DropEligible =
        pVlanTag->InnerVlanTag.u1DropEligible;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbPepIngressFiltering                        */
/*                                                                           */
/*    Description         : This function does ingress filtering check on PEP*/
/*                          for the frame received on CEPs.                  */
/*                                                                           */
/*    Input(s)            : pVlanPbLogicalPortEntry - PEP entry associated   */
/*                                                    with the incoming port */
/*                                                    and SVlanId.           */
/*                          CvidRegSvid   - SVlanId present in CVID          */
/*                                          registration table               */
/*                          u1IsFrameTagged - Flag to indicate whether the   */
/*                                            frame is tagged or not         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbPepIngressFiltering (tVlanPbLogicalPortEntry * pVlanPbLogicalPortEntry,
                           tVlanId CvidRegSvid, UINT1 u1FrameType)
{
    if ((pVlanPbLogicalPortEntry->u1AccptFrameType ==
         VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES) && (u1FrameType == VLAN_UNTAGGED))
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "PEP ingress filtering failed for port %d SVLAN %d "
                       "Acceptable frame type check failed \n",
                       pVlanPbLogicalPortEntry->u2CepPort,
                       pVlanPbLogicalPortEntry->SVlanId);
        return VLAN_FAILURE;
    }
    if ((pVlanPbLogicalPortEntry->u1AccptFrameType ==
         VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES) &&
        (u1FrameType == VLAN_TAGGED))
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "PEP ingress filtering failed for port %d SVLAN %d "
                       "Acceptable frame type check failed \n",
                       pVlanPbLogicalPortEntry->u2CepPort,
                       pVlanPbLogicalPortEntry->SVlanId);
        return VLAN_FAILURE;
    }
    if (pVlanPbLogicalPortEntry->u1IngressFiltering == VLAN_SNMP_TRUE)
    {
        if (pVlanPbLogicalPortEntry->SVlanId != CvidRegSvid)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                           "PEP ingress filtering failed for port %d SVLAN %d "
                           "Acceptable frame type check failed \n",
                           pVlanPbLogicalPortEntry->u2CepPort,
                           pVlanPbLogicalPortEntry->SVlanId);
            return VLAN_FAILURE;
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbServicePriorityRegen                       */
/*                                                                           */
/*    Description         : This function returns the regenerated priority   */
/*                          for the received service priority on CEPs.       */
/*                                                                           */
/*    Input(s)            : u2InPort - Incoming port                         */
/*                          u1InPriority - Received service priority value   */
/*                                                                           */
/*    Output(s)           : RegenPriority - Regenerated service priority     */
/*                                          value.                           */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbServicePriorityRegen (UINT2 u2Port, tVlanId SVlanId, UINT1 u1InPriority,
                            UINT1 *pu1RegenPriority)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry;

    pVlanPbLogicalPortEntry = VlanPbGetLogicalPortEntry (u2Port, SVlanId);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Service priority regeneration failed for port  %d"
                       "SVLAN %d.No entry found in the logical port table \n",
                       u2Port, SVlanId);
        return VLAN_FAILURE;
    }

    *pu1RegenPriority = pVlanPbLogicalPortEntry->au1RegenPriority[u1InPriority];
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandlePktTxOnCep                           */
/*                                                                           */
/*    Description         : This function handles packet transmission on     */
/*                          customer edge ports.                             */
/*                                                                           */
/*    Input(s)            : pFrame    - Outgoing frame                       */
/*                          u2Port    - Port number                          */
/*                          SVlanId   - Service VLAN ID                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_FORWARD/VLAN_NO_FORWARD                     */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandlePktTxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanId SVlanId, tVlanIfMsg * pVlanIf,
                        UINT1 u1IsCustBpdu)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4TblIndex;
    tVlanId             CVlanTag;
    tVlanId             CVlanId = VLAN_NULL_VLAN_ID;
    UINT2               u2EtherType;
    UINT2               u2Len;
    tMacAddr            DestAddr;
    UINT1               u1FrameType = VLAN_UNTAGGED;    /* Incoming frame type. */
    UINT1               u1PortState = AST_PORT_STATE_FORWARDING;
    UINT1               u1Priority;

    /* This function does the following.
     * - When the frame is customer tagged, if the untaggedCEP is set to 
     *   TRUE in CVID registration table, the tag is removed and sent out 
     *   on CEP.
     * - Else, if the frame is untagged and untaggedCEP is set to False, 
     *   the tag is added and sent out on CEP.
     * - For C-tagged packets, the CVLAN ID from the Tag is used to scan 
     *   the CVID registration table.
     * - For untagged packets, the PVID configured in the PEP configuration
     *   table is used as CVLAN ID to scan the C-VID registration table.
     * - To add tag in the outgoing frame, the PVID and priority are taken 
     *   from the PEP configuration table.
     */
    u2Len = pVlanIf->u2Length;

    /* Get the PEP. If no pep then drop. */

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    pVlanPbLogicalPortEntry = VlanPbGetLogicalPortEntry (u2Port, SVlanId);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_NO_FORWARD;
    }
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_NO_FORWARD;
    }
    if ((u1IsCustBpdu == VLAN_TRUE) &&
        (VLAN_STP_TUNNEL_STATUS (VLAN_GET_PORT_ENTRY (u2Port))
         == VLAN_TUNNEL_PROTOCOL_PEER))
    {
        /* 
         * Cutomer BPDU received on PEP from PNP. Give it to 
         * AST task for processing.
         * This pakcet should be untagged.
         */
        VlanL2IwfPbPepHandleInCustBpdu (pFrame, VLAN_GET_IFINDEX (u2Port),
                                        SVlanId);
        return VLAN_NO_FORWARD;
    }

    /* Get the port state for the provider edge port. */
    u1PortState = VlanL2IwfGetPbPortState (VLAN_GET_IFINDEX (u2Port), SVlanId);

    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Dropping packet as the Provider Edge Port with Id as "
                       "Port: %d, SVId : %d is not in forwarding state.\n",
                       u2Port, SVlanId);
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_NO_FORWARD;
    }

    VLAN_MEMCPY (DestAddr, VLAN_IFMSG_DST_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    VLAN_COPY_FROM_BUF (pFrame, &u2EtherType,
                        VLAN_TAG_OFFSET, VLAN_TYPE_OR_LEN_SIZE);

    u2EtherType = OSIX_NTOHS (u2EtherType);

    u1Priority = pVlanPbLogicalPortEntry->u1DefUserPriority;
    if (u2EtherType == VLAN_CUSTOMER_PROTOCOL_ID)
    {
        VLAN_COPY_FROM_BUF (pFrame, &CVlanTag, VLAN_TAG_VLANID_OFFSET,
                            VLAN_TAG_SIZE);
        CVlanTag = OSIX_NTOHS (CVlanTag);
        u1Priority = (UINT1) (CVlanTag >> VLAN_TAG_PRIORITY_SHIFT);
        /* Frame is customer tagged. Use the CVLAN ID from the tag for 
         * CVID registration table scanning.
         */
        CVlanId = (tVlanId) (CVlanTag & VLAN_ID_MASK);
        if (CVlanId == VLAN_NULL_VLAN_ID)
        {
            u1FrameType = VLAN_PRIORITY_TAGGED;
        }
        else
        {
            u1FrameType = VLAN_TAGGED;
        }
    }
    if (CVlanId == VLAN_NULL_VLAN_ID)
    {
        /* Frame is untagged/priority tagged.Use the PVID configured in 
         * PEP configuration table for scanning the CVID registration 
         * table.
         */
        CVlanId = pVlanPbLogicalPortEntry->Cpvid;
    }
    /* If customer BPDU peering is set, and the received packet is a
     * customer bpdu then send it to AST task. */

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_NO_FORWARD;
    }

    /* Get the C-VID registration table entry for (port, cvid). */
    pCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port, CVlanId);

    if (pCVlanSVlanEntry == NULL)
    {
        VLAN_TRC_ARG3 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Packet transmission on CEP failed. Port %d SVLAN %d "
                       "No entry found in CVID registration table for this port"
                       " and CVLAN %d \n", u2Port, SVlanId, CVlanId);
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_NO_FORWARD;
    }

    /* Go for PEP verifications - Acceptable frame type verifications 
     * and ingress filtering checking.
     */
    if ((VlanPbPepIngressFiltering (pVlanPbLogicalPortEntry,
                                    pCVlanSVlanEntry->SVlanId, u1FrameType)
         == VLAN_FAILURE))
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Packet transmission on CEP failed. Port %d SVLAN %d "
                       "PEP ingress filtering failed. \n", u2Port, SVlanId);
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_NO_FORWARD;
    }
    if (pCVlanSVlanEntry->u1CepUntag == VLAN_SNMP_TRUE)
    {
        if (u1FrameType != VLAN_UNTAGGED)
        {
            VlanUnTagFrame (pFrame);
            u2Len -= VLAN_TAG_PID_LEN;
        }
    }
    else if (pCVlanSVlanEntry->u1CepUntag == VLAN_SNMP_FALSE)
    {
        if (u1FrameType == VLAN_UNTAGGED)
        {
            /* Use the PVID and priority configured in the PEP configuration 
             * table for tagging the outgoing frame.
             */
            VlanAddTagToFrame (pFrame, CVlanId,
                               pVlanPbLogicalPortEntry->u1DefUserPriority,
                               VLAN_DE_FALSE, pVlanPortEntry);
            u2Len += VLAN_TAG_PID_LEN;
        }
        else if (u1FrameType == VLAN_PRIORITY_TAGGED)
        {
            /* Incoming frame is priority c-tagged.untaggedPep is False. 
             *  Update the tag in the incoming frame with the C-VLAN ID.
             */
            VlanUpdateVlanTag (pFrame, CVlanId, u1Priority, VLAN_DE_FALSE,
                               pVlanPortEntry);
        }
    }

    pVlanIf->u2Length = u2Len;

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandlePktOnPep                             */
/*                                                                           */
/*    Description         : This function processes the packets received     */
/*                          on PEP ports.                                    */
/*                                                                           */
/*    Input(s)            : pFrame       - Pointer to the incoming frame     */
/*                          pVlanIfMsg   - Pointer to the VLAN if message    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandlePktOnPep (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf)
{
    UINT1              *pFwdPortList = NULL;
    tVlanTag            VlanTag;
    tVlanCurrEntry     *pVlanRec = NULL;

    MEMSET (&VlanTag, 0, sizeof (VlanTag));

    pVlanRec = VlanGetVlanEntry (pVlanIf->SVlanId);

    /* S-VLAN is not active. So drop the frame. */
    if (pVlanRec == NULL)
    {
        return VLAN_FAILURE;
    }

    VlanTag.OuterVlanTag.u2VlanId = pVlanIf->SVlanId;
    VlanTag.OuterVlanTag.u1TagType = VLAN_UNTAGGED;
    VlanTag.OuterVlanTag.u1Priority = VLAN_HIGHEST_PRIORITY;
    VlanTag.OuterVlanTag.u1DropEligible = VLAN_DE_FALSE;
    pFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pFwdPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanPbHandlePktOnPep: Error in allocating memory "
                  "for pFwdPortList\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pFwdPortList, 0, sizeof (tLocalPortList));
    /* BPDU has come out of PEP, and has come now in to the internal
     * CNP. So don't look for the PEP port state. */

    VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, pFwdPortList);

    if (VlanL2VpnPktFloodOnMplsPort (pFrame, pVlanIf, pVlanRec, &VlanTag)
		!= VLAN_FORWARD)
	{
		VlanFwdOnPorts (pFrame, pVlanIf,
                    pFwdPortList, pVlanRec, &VlanTag, VLAN_MCAST_FRAME);
	}
   
    UtilPlstReleaseLocalPortList (pFwdPortList);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfProviderNetworkPort                    */
/*                                                                           */
/*    Description         : This function is used to set the default PB port */
/*                          values. The calling function should ensure that  */
/*                          pPortEntry is not NULL.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbConfProviderNetworkPort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2EgressEtherType;
    UINT2               u2IngressEtherType;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if ((NULL == pVlanPortEntry) || (NULL == pVlanPbPortEntry))
    {
        return VLAN_FAILURE;
    }

    pVlanPbPortEntry->u1PbPortType = VLAN_PROVIDER_NETWORK_PORT;
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;
    pVlanPbPortEntry->u1EtherTypeSwapStatus = VLAN_DISABLED;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return VLAN_FAILURE;
    }

    pVlanPortEntry->u1PortProtoBasedClassification = VLAN_DISABLED;
    pVlanPortEntry->u1MacBasedClassification = VLAN_DISABLED;

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
    {
        pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
        pVlanPortEntry->u1IngFiltering = VLAN_SNMP_TRUE;

        VlanGetPrimaryContextEtherType (pVlanPortEntry->u4PhyIfIndex,
                                        &u2EgressEtherType,
                                        VLAN_EGRESS_ETHERTYPE);
        VlanGetPrimaryContextEtherType (pVlanPortEntry->u4PhyIfIndex,
                                        &u2IngressEtherType,
                                        VLAN_INGRESS_ETHERTYPE);

        VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = u2EgressEtherType;
        VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = u2IngressEtherType;
    }
    else
    {
        pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
        pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;

        VLAN_EGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
        VLAN_INGRESS_VLAN_ETHER_TYPE (u2Port) = VLAN_PROVIDER_PROTOCOL_ID;
    }

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_DISABLED;
    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
    {
        VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    }
    else
    {
        VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    }

    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_DOT1X_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_LACP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_STP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_GVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_GMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_MVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_MMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_IGMP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_ENABLED);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfVirtualInstancePort                   */
/*                                                                           */
/*    Description         : This function is used to set the default PB port */
/*                          values. The calling function should ensure that  */
/*                          pPortEntry is not NULL.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbConfVirtualInstancePort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1InterfaceType = VLAN_INVALID_INTERFACE_TYPE;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if ((pVlanPortEntry == NULL) || (pVlanPortEntry->pVlanPbPortEntry == NULL))
    {
        return FAILURE;
    }

    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;

    pVlanPortEntry->u1PortType = VLAN_HYBRID_PORT;
    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;

    pVlanPortEntry->u1PortProtoBasedClassification = VLAN_DISABLED;
    pVlanPortEntry->u1MacBasedClassification = VLAN_DISABLED;

    VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (), &u1InterfaceType);
    VlanL2IwfUpdatePortDeiBit (pVlanPortEntry->u4IfIndex,
                               pVlanPbPortEntry->u1UseDei);

    if (u1InterfaceType == VLAN_C_INTERFACE_TYPE)
    {
        pVlanPortEntry->u2EgressEtherType = VLAN_PROTOCOL_ID;
        pVlanPortEntry->u2IngressEtherType = VLAN_PROTOCOL_ID;
    }
    else
    {
        pVlanPortEntry->u2EgressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
        pVlanPortEntry->u2IngressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
    }

    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;

    pVlanPbPortEntry->u1EtherTypeSwapStatus = VLAN_DISABLED;

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_DISABLED;
    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_INVALID;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_INVALID;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_INVALID;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_INVALID;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_INVALID;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_INVALID;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_DOT1X_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_LACP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_STP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_GVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_GMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_MVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_MMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_IGMP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_ENABLED);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfCustomerBackbonePort                   */
/*                                                                           */
/*    Description         : This function is used to set the default PB port */
/*                          values. The calling function should ensure that  */
/*                          pPortEntry is not NULL.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbConfCustomerBackbonePort (UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if ((pVlanPortEntry == NULL) || (pVlanPortEntry->pVlanPbPortEntry == NULL))
    {
        return FAILURE;
    }

    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;

    pVlanPbPortEntry->u1SVlanTableType = VLAN_SVLAN_PORT_PVID_TYPE;
    pVlanPbPortEntry->u1DefCVlanClassify = VLAN_DISABLED;
    pVlanPbPortEntry->CVlanId = VLAN_DEF_VLAN_ID;
    pVlanPbPortEntry->u1MacLearningStatus = VLAN_ENABLED;
    pVlanPbPortEntry->u1PcpSelRow = VLAN_8P0D_SEL_ROW;
    pVlanPbPortEntry->u1ReqDropEncoding = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1UseDei = VLAN_SNMP_FALSE;
    pVlanPbPortEntry->u1PortServiceType = VLAN_INVALID_SERVICE_TYPE;
    pVlanPortEntry->u2EgressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPortEntry->u2IngressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPortEntry->u1AccpFrmTypes =
        VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    pVlanPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;
    pVlanPortEntry->u1PortProtoBasedClassification = VLAN_DISABLED;
    pVlanPortEntry->u1MacBasedClassification = VLAN_DISABLED;
    pVlanPbPortEntry->u4MacLearningLimit = VLAN_PB_DYNAMIC_UNICAST_SIZE;
    pVlanPbPortEntry->u4NumLearntMacEntries = 0;

    pVlanPbPortEntry->u1EtherTypeSwapStatus = VLAN_DISABLED;

    L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                 pVlanPortEntry->u1AccpFrmTypes);

    VlanL2IwfUpdatePortDeiBit (pVlanPortEntry->u4IfIndex,
                               pVlanPbPortEntry->u1UseDei);
    VLAN_TUNNEL_STATUS (pVlanPortEntry) = VLAN_DISABLED;
    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_DOT1X_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LACP,
                                            VLAN_LACP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_STP,
                                            VLAN_STP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GVRP,
                                            VLAN_GVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_GMRP,
                                            VLAN_GMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MVRP,
                                            VLAN_MVRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_MMRP,
                                            VLAN_MMRP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_IGMP,
                                            VLAN_IGMP_TUNNEL_STATUS
                                            (pVlanPortEntry));
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                            L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);

    VlanL2IwfPbSetVidTransStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  OSIX_DISABLED);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbValidatePcpDecodingValues                  */
/*                                                                           */
/*    Description         : This function is used to validate the PCP        */
/*                          decoding table priority and drop eligible values */
/*                          configured for a port.                           */
/*                                                                           */
/*    Input(s)            : i4Port      - PortNumber                         */
/*                          i4PcpSelRow - PCP selection row                  */
/*                          i4PcpValue  - PCP value                          */
/*                          u1InPriority - Priority                          */
/*                          u1InDropEligible - Drop Eligible(VLAN_SNMP_TRUE/ */
/*                          VLAN_SNMP_FALSE)                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbValidatePcpDecodingValues (INT4 i4Port, INT4 i4PcpSelRow,
                                 INT4 i4PcpValue, UINT1 u1InPriority,
                                 UINT1 u1InDropEligible)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Pcp;
    UINT1               u1PrioVal;
    UINT1               u1DropEligible;

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Port);
    if (NULL == pVlanPbPortEntry)
    {
        return VLAN_FAILURE;
    }

    /* The lower of any two PCP shall not map to a higher priority than 
     * the higher PCP.
     */
    for (u2Pcp = 0; u2Pcp < VLAN_PB_MAX_PCP; u2Pcp++)
    {
        u1PrioVal = VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                                 i4PcpSelRow, u2Pcp);

        if (i4PcpValue < u2Pcp)
        {
            /* Configured PCP is lower */
            if (u1InPriority > u1PrioVal)
            {
                /* Priority is high */
                return VLAN_FAILURE;
            }
        }
    }

    if ((i4PcpValue != 0) && (i4PcpValue != 1))
    {
        for (u2Pcp = 2; u2Pcp < VLAN_PB_MAX_PCP; u2Pcp++)
        {
            u1PrioVal = VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                                     i4PcpSelRow, u2Pcp);
            u1DropEligible = VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                                               i4PcpSelRow,
                                                               u2Pcp);

            if (i4PcpValue > u2Pcp)
            {
                if (u1InPriority == u1PrioVal)
                {
                    if ((u1InDropEligible == VLAN_SNMP_TRUE)
                        && (u1DropEligible == VLAN_DE_FALSE))
                    {
                        /* Lower PCP maps to the same priority with drop eligible 
                         * false. Return failure.
                         */
                        return VLAN_FAILURE;
                    }
                }
            }
            if (i4PcpValue < u2Pcp)
            {
                if (u1InPriority == u1PrioVal)
                {
                    if ((u1InDropEligible == VLAN_SNMP_FALSE)
                        && (u1DropEligible == VLAN_DE_TRUE))
                    {
                        /* Higher PCP maps to the same priority with drop eligible 
                         * True. Return failure.
                         */
                        return VLAN_FAILURE;
                    }
                }
            }
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdatePepOperStatusToAst                   */
/*                                                                           */
/*    Description         : This function sets PEP oper status to Down state */
/*                          in AST when VLAN module is disabled.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbUpdatePepOperStatusToAst ()
{
    tVlanId             FirstSVid = 0;
    tVlanId             NextSVid;
    INT4                i4FirstPort;
    INT4                i4NextPort;
    UINT4               u4NextSvid;
    if (VlanPbGetFirstLogicalPortEntry (&i4NextPort, &u4NextSvid) == NULL)
    {
        return;
    }
    do
    {
        NextSVid = (UINT2) u4NextSvid;
        VlanL2IwfPbUpdatePepOperStatus ((UINT4) i4NextPort, NextSVid,
                                        VLAN_OPER_DOWN);
        i4FirstPort = i4NextPort;
        FirstSVid = NextSVid;
    }
    while (VlanPbGetNextLogicalPortEntry (i4FirstPort, FirstSVid, &i4NextPort,
                                          &u4NextSvid) != NULL);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbValidateBridgeMode                         */
/*                                                                           */
/*    Description         : This function is used to validate the bridge     */
/*                          mode configured in the switch.                   */
/*    Input (s)           : u2BridgeMode - Bridge mode                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbValidateBridgeMode (INT4 i4BridgeMode, UINT4 *pu4ErrorCode)
{
    if ((i4BridgeMode < VLAN_CUSTOMER_BRIDGE_MODE) ||
        (i4BridgeMode > VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        /* Bridge mode should be customer or provider or provider core or
         * provider edge or provider backbone (I or B)
         */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsPortTypePresentInPortList                */
/*                                                                           */
/*    Description         : This function checks whether the given port      */
/*                          type is present in the given port list.          */
/*                                                                           */
/*    Input(s)            : pu1Ports - Port bit map containing the list of   */
/*                                     untagged ports.                       */
/*                          i4Len - Length of the given port bit map.        */
/*                          u1PbPortType - Port type to be checked.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the given bit map contains  */
/*                                a port of given type, otherwise VLAN_FALSE */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsPortTypePresentInPortList (UINT1 *pu1Ports, INT4 i4Len,
                                   UINT1 u1PbPortType)
{
    tVlanPbPortEntry   *pPbPortEntry = NULL;
    UINT2               u2Port;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    for (u2ByteInd = 0; u2ByteInd < i4Len; u2ByteInd++)
    {
        if (pu1Ports[u2ByteInd] != 0)
        {
            u1PortFlag = pu1Ports[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);
                    if (u2Port >= VLAN_MAX_PORTS + 1)
                    {
                        break;
                    }

                    pPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

                    if (pPbPortEntry != NULL)
                    {
                        if (pPbPortEntry->u1PbPortType == u1PbPortType)
                        {
                            return VLAN_TRUE;
                        }
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsTunnelingValidOnCep                      */
/*                                                                           */
/*    Description         : This function checks whether tunnelling for any  */
/*                          protocol can be enabled on this port.            */
/*                                                                           */
/*    Input(s)            : u2Port - Port id.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the tunnelling can be       */
/*                                enabled on this port otherwise VLAN_FALSE. */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsTunnelingValidOnCep (UINT2 u2Port)
{
    UINT4               u4NextSvid = 0;
    UINT4               u4FirstSvid = VLAN_NULL_VLAN_ID;
    INT4                i4NextPort = 0;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry (u2Port,
                                                             VLAN_NULL_VLAN_ID,
                                                             &i4NextPort,
                                                             &u4FirstSvid);

    if ((pVlanPbLogicalPortEntry != NULL) && (i4NextPort == u2Port))

    {
        /* There is already a PEP present in this C-VLAN component.
         * Now check whether there are any more PEPs in the C-VLAN
         * component containing this port. */

        pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry
            (u2Port, (tVlanId) u4FirstSvid, &i4NextPort, &u4NextSvid);

        if ((pVlanPbLogicalPortEntry != NULL) && (i4NextPort == u2Port))
        {
            /* There are more than one PEP in the C-VLAN component containing
             * this port. So don't permit tunneling on this port. */
            return VLAN_FALSE;
        }
    }
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsAnyProtTunnelSet                         */
/*                                                                           */
/*    Description         : This function checks whether tunnelling for any  */
/*                          protocol is enabled on this port.                */
/*                                                                           */
/*    Input(s)            : pPortEntry - Pointer to the port.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the tunnelling is enabled   */
/*                                for atleast one protocol on this port.     */
/*                                otherwise VLAN_FALSE.                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsAnyProtTunnelSet (UINT2 u2Port)
{

    tVlanPortEntry     *pPortEntry = NULL;

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (VLAN_DOT1X_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Pnac pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_LACP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Lacp pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_STP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Spanning tree BPDU tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_GVRP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Gvrp pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_GMRP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Gmrp pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_MVRP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Mvrp pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_MMRP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Mmrp pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    if (VLAN_IGMP_TUNNEL_STATUS (pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Igmp pkt tunneling is enabled. */
        return VLAN_TRUE;
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsServiceTypeValid                         */
/*                                                                           */
/*    Description         : This function checks whether the given serive    */
/*                          type can be set for the given vlan.              */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan id                                 */
/*                          u1ServiceType - Service type to be checked.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the given service type can  */
/*                                set for the given vlan otherwise           */
/*                                VLAN_FALSE.                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsServiceTypeValid (tVlanId VlanId, UINT1 u1InServiceType)
{
    UINT1              *pu1LocalPortList = NULL;
    tVlanCurrEntry     *pVlanRec = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Len;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT2               u2NumPorts = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1ServiceType;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanL2IwfPbGetVlanServiceType (VLAN_CURR_CONTEXT_ID (),
                                       VlanId, &u1ServiceType) == L2IWF_FAILURE)
    {
        return VLAN_FALSE;
    }

    if ((u1ServiceType == u1InServiceType) || (u1InServiceType == VLAN_E_LAN))

    {
        return VLAN_TRUE;
    }

    /* Get Vlan current entry. */
    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "VLAN 0x%x is unknown \n", VlanId);

        return VLAN_FALSE;
    }

    i4Len = sizeof (tLocalPortList);

    VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT (pVlanRec, u1Result);
    if (u1Result != VLAN_TRUE)
    {
        return VLAN_FALSE;
    }

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return VLAN_FALSE;
    }
    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
    VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pu1LocalPortList);

    /* Now Loop through the egress ports. */
    for (u2ByteInd = 0; u2ByteInd < i4Len; u2ByteInd++)
    {
        if (pu1LocalPortList[u2ByteInd] != 0)
        {
            u1PortFlag = pu1LocalPortList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);
                    u2Port = MEM_MAX_BYTES (u2Port, VLAN_MAX_PORTS);
                    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
                    if (pPortEntry != NULL)
                    {
                        VLAN_IS_EGRESS_PORT (pVlanRec->pStVlanEntry,
                                             u2Port, u1Result);
                        if (u1Result == VLAN_TRUE)
                        {
                            u2NumPorts++;

                            if (u2NumPorts > 2)
                            {
                                UtilPlstReleaseLocalPortList (pu1LocalPortList);
                                return VLAN_FALSE;
                            }
                        }
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }
    UtilPlstReleaseLocalPortList (pu1LocalPortList);
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsAnyCustomerPortPresent                   */
/*                                                                           */
/*    Description         : This function checks whether any customer port   */
/*                          is present in the given port list.               */
/*                                                                           */
/*    Input(s)            : pu1Ports - Port bit map containing the list of   */
/*                                     untagged ports.                       */
/*                          i4Len - Length of the given port bit map.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the given bit map contains  */
/*                                a customer port, otherwise VLAN_FALSE      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsAnyCustomerPortPresent (UINT1 *pu1Ports, INT4 i4Len)
{
    tVlanPortEntry     *pPortEntry;
    UINT2               u2Port;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1PbPortType;

    for (u2ByteInd = 0; u2ByteInd < i4Len; u2ByteInd++)
    {
        if (pu1Ports[u2ByteInd] != 0)
        {
            u1PortFlag = pu1Ports[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);
                    if (u2Port >= VLAN_MAX_PORTS + 1)
                    {
                        break;
                    }

                    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                    if ((pPortEntry != NULL) &&
                        (pPortEntry->pVlanPbPortEntry != NULL))
                    {
                        u1PbPortType =
                            pPortEntry->pVlanPbPortEntry->u1PbPortType;

                        if ((u1PbPortType != VLAN_PROVIDER_NETWORK_PORT) &&
                            (u1PbPortType != VLAN_PROP_PROVIDER_NETWORK_PORT) &&
                            (u1PbPortType != VLAN_PROVIDER_INSTANCE_PORT) &&
                            (u1PbPortType != VLAN_VIRTUAL_INSTANCE_PORT))
                        {
                            return VLAN_TRUE;
                        }
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsThisInterfaceVlan                          */
/*                                                                           */
/*    Description         : This function is a wrapper function for          */
/*                          CfaIsThisInterfaceVlan. Once Cfa is instantiated */
/*                          this function will get the current context id    */
/*                          and then will pass that to cfa as one more       */
/*                          parameter.                                       */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan id.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if IVR interface is present    */
/*                                for given vlan, otherwise VLAN_FALSE       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsThisInterfaceVlan (tVlanId VlanId)
{
    /* Once Cfa is instantiated, call Cfa with context id, till then
     * use the existing interface. */
    if (VlanCfaIsThisInterfaceVlan (VlanId) == CFA_TRUE)
    {
        return VLAN_TRUE;
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetDecodingPriority                        */
/*                                                                           */
/*    Description         : This function is used by CLI routine to get the  */
/*                          Default Priority in a PCP Decoding Table for a   */
/*                          particular row and Pcp value                     */
/*                                                                           */
/*    Input(s)            : i4SelRow - Pcp Selection Row                     */
/*                          i4PcpVal - Priority Code Point Value             */
/*                                                                           */
/*    Output(s)           : *pi4Priority - Default Priority val              */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbGetDecodingPriority (INT4 i4SelRow, INT4 i4PcpVal, INT4 *pi4Priority)
{
    *pi4Priority = gau1PcpDecPriority[i4SelRow - 1][i4PcpVal];
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetDecodingDropEligible                    */
/*                                                                           */
/*    Description         : This function is used by CLI routine to get the  */
/*                          Default Drop eligible in a PCP Decoding Table for a*/
/*                          particular row and Pcp value                     */
/*                                                                           */
/*    Input(s)            : i4SelRow - Pcp Selection Row                     */
/*                          i4PcpVal - Priority Code Point Value             */
/*                                                                           */
/*    Output(s)           : *pi4DropEligible - Default DropEligible          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbGetDecodingDropEligible (INT4 i4SelRow, INT4 i4PcpVal,
                               INT4 *pi4DropEligible)
{
    *pi4DropEligible = gau1PcpDecDropEligible[i4SelRow - 1][i4PcpVal];
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetEncodingPcpVal                          */
/*                                                                           */
/*    Description         : This function is used by CLI routine to get the  */
/*                          Default Pcp Value in a PCP Encoding Table for a  */
/*                          particular row , Priority and Drop Eligible      */
/*                                                                           */
/*    Input(s)            : i4SelRow - Pcp Selection Row                     */
/*                          i4Priority - Priority Value                      */
/*                          i4DropEligible - DropEligible Value              */
/*                                                                           */
/*    Output(s)           : *pi4PcpVal - Default Pcp                         */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbGetEncodingPcpVal (INT4 i4SelRow, INT4 i4Priority, INT4 i4DropEligible,
                         INT4 *pi4PcpVal)
{
    *pi4PcpVal = gau1PcpEncValue[i4SelRow - 1][i4Priority][i4DropEligible];
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbEtherTypeSwap                              */
/*                                                                           */
/*    Description         : This function checks whether the incoming frame  */
/*                          is tagged based on the entries configured in     */
/*                          ether type swap table.                           */
/*                                                                           */
/*    Input(s)            : pFrame      - Incoming frame                     */
/*                          u2Port      - Receive port                       */
/*                          u2EtherType - Ethertype in the incoming frame    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TAGGED/VLAN_UNTAGGED                  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbEtherTypeSwap (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                     UINT2 u2EtherType)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_UNTAGGED;
    }

    if (pVlanPbPortEntry->u1EtherTypeSwapStatus == VLAN_ENABLED)
    {
        pEtherTypeSwapEntry = VlanPbGetEtherTypeSwapEntry (u2Port, u2EtherType);
        if (pEtherTypeSwapEntry != NULL)
        {
            VlanUpdateTagEtherType (pFrame,
                                    pEtherTypeSwapEntry->u2RelayEtherType);
            /* Ether type does not match, ether type swapping is enabled and 
             * there exists valid ethertype swap entry. Classify the frame 
             * as S-VLAN tagged */
            return VLAN_TAGGED;
        }
    }
    /*
     * Ethertype does not match. No entry found in ethertype swap 
     * table. Classify the frame as untagged.
     */
    return VLAN_UNTAGGED;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPcpAndLocalVid                            */
/*                                                                           */
/*    Description         : This function returns the PCP and local VLAN ID  */
/*                          to be used in the outgoing frame tag.            */
/*                                                                           */
/*    Input(s)            : VlanTag  - VLAN tag filled with VLAN-ID,         */
/*                                     priority and drop-eligible.           */
/*                        : pVlanPortEntry - Pointer to port entry.          */
/*                                                                           */
/*    Output(s)           : pLocalVid - Local VLAN ID to be used in the      */
/*                                       outgoing frame.                     */
/*                          pu2Pcp    - The PCP to be used in the outgoing   */
/*                                       frame.                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/

INT4
VlanGetPcpAndLocalVid (tVlanTag VlanTag, tVlanPortEntry * pVlanPortEntry,
                       tVlanId * pLocalVid, UINT1 *pu1Pcp)
{

    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanId             RelayVid = VLAN_NULL_VLAN_ID;
    UINT1               u1PcpSelRow;
    UINT1               u1Priority;
    UINT1               u1DropEligible;

    /* 
     * PCP encoding is common for all the packet types when 
     * the outgoing port is tagged.
     * Get the PCP value associated with the incoming priority and 
     * drop eligible value.
     */

    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;
    u1PcpSelRow = pVlanPbPortEntry->u1PcpSelRow;
    u1Priority = VlanTag.OuterVlanTag.u1Priority;
    u1DropEligible = VlanTag.OuterVlanTag.u1DropEligible;
    if (u1PcpSelRow == 0)
    {
        return VLAN_FAILURE;
    }

    *pu1Pcp = VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry, u1PcpSelRow, u1Priority,
                                      u1DropEligible);

    RelayVid = VlanTag.OuterVlanTag.u2VlanId;

    if (VlanL2IwfPbGetLocalVidFromRelayVid (VLAN_CURR_CONTEXT_ID (),
                                            pVlanPortEntry->u2Port,
                                            RelayVid,
                                            pLocalVid) == L2IWF_FAILURE)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "PB packet transmission failed on port %d. VID "
                       " translation failed for SVLAN %d \n",
                       pVlanPortEntry->u2Port, RelayVid);

        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetPepOperStatus                           */
/*                                                                           */
/*    Description         : This function returns the oper status of         */
/*                          Provider edge port                               */
/*                                                                           */
/*    Input(s)            : u2Port  - Customer Edge Port                     */
/*                          SVlanId - Service VLAN ID.                       */
/*                                                                           */
/*    Output(s)           : pu1OperStatus - PEP oper status                  */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbGetPepOperStatus (UINT2 u2Port, tVlanId SVlanId, UINT1 *pu1OperStatus)
{
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    VLAN_MEMSET (&VlanPbLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    VlanPbLogicalPortEntry.u2CepPort = u2Port;
    VlanPbLogicalPortEntry.SVlanId = SVlanId;

    pVlanPbLogicalPortEntry
        = RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                     &VlanPbLogicalPortEntry);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    *pu1OperStatus = pVlanPbLogicalPortEntry->u1OperStatus;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCheckVlanServiceType                       */
/*                                                                           */
/*    Description         : This function checks the VLAN service type and   */
/*                          number of ports getting configured for a VLAN.   */
/*                          If the service type is E-Line and the number of  */
/*                          ports is greater than two, failure is returned.  */
/*                                                                           */
/*    Input(s)            : VlanId  - VLAN ID                                */
/*                          InPorts - Member ports getting configured for    */
/*                                    the VLAN.                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbCheckVlanServiceType (tVlanId VlanId, tLocalPortList InPorts)
{
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1PortCount = 0;
    UINT1               u1VlanServiceType = 0;
    tStaticVlanEntry   *pStVlanEntry = NULL;

    /* Get the VLAN service type for this VLAN */

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry != NULL)
    {
        if (pStVlanEntry->u1RowStatus == VLAN_NOT_IN_SERVICE)
        {
            u1VlanServiceType = pStVlanEntry->u1ServiceType;
        }

        else
        {
            if (VlanL2IwfPbGetVlanServiceType (VLAN_CURR_CONTEXT_ID (),
                                               VlanId,
                                               &u1VlanServiceType) ==
                L2IWF_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
    }

    else
    {
        return VLAN_FAILURE;
    }

    /* Return SUCCESS, if the VLAN service type is E-LAN */

    if (u1VlanServiceType == VLAN_E_LAN)
    {
        return VLAN_SUCCESS;
    }

    /* If the VLAN service type is E-Line and the number of ports
     * getting configured is greater than 2, return FAILURE.
     * Else, return SUCCESS.
     */

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (InPorts[u2ByteInd] != 0)
        {
            u1PortFlag = InPorts[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u1PortCount++;

                    if (u1PortCount > 2)
                    {
                        return VLAN_FAILURE;
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiPbIsTunnelingValid                         */
/*                                                                           */
/*    Description         : This function checks whether tunneling is valid  */
/*                          on given port type.                              */
/*                                                                           */
/*    Input(s)            : u2IfIndex  - Interface index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE / VLAN_FALSE                           */
/*****************************************************************************/
INT4
VlanMiPbIsTunnelingValid (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT1               u1RetVal = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return VLAN_FALSE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return VLAN_FALSE;
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_FALSE)
    {
        u1RetVal = VLAN_PB_IS_TUNNELING_VALID (u2LocalPort);
    }
    else
    {
        u1RetVal = VLAN_TRUE;
    }

    VlanReleaseContext ();
    return u1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiPbGetPepOperStatus                         */
/*                                                                           */
/*    Description         : This function returns the oper status of a       */
/*                          provider edge port.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex  - Interface index                     */
/*                          u4SVlanId  - Service VLAN ID                     */
/*                                                                           */
/*    Output(s)           : pu1OperStatus - PEP oper status                  */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanMiPbGetPepOperStatus (UINT4 u4IfIndex, UINT4 u4SVlanId,
                          UINT1 *pu1OperStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    VlanPbGetPepOperStatus (u2LocalPort, (tVlanId) u4SVlanId, pu1OperStatus);

    VlanReleaseContext ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsMacLearningAllowed                       */
/*                                                                           */
/*    Description         : This function checks whether a mac has to be     */
/*                          learned or not on a port or an fid.This function */
/*                          is called from VlanLearn                         */
/*                                                                           */
/*    Input(s)            : pFdbEntry - Learned Entry                        */
/*                          pVlanRec  - Vlan Entry                           */
/*                          pVlanPortEntry - Port on which Mac is learned    */
/*                          pFidEntry - FID Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE / VLAN_FALSE                           */
/*****************************************************************************/

INT4
VlanPbIsMacLearningAllowed (tVlanFdbEntry * pFdbEntry,
                            tVlanPortEntry * pVlanPortEntry)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;

    if (pFdbEntry == NULL)
    {
        if (VlanPbGetPortMacLearningStatus (pVlanPbPortEntry) == VLAN_DISABLED)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                      ALL_FAILURE_TRC, VLAN_NAME,
                      "Learning status disabled for the port\n");
            return VLAN_FALSE;
        }

        if (VlanPbIsPortMacLearnLimitExceeded (pVlanPbPortEntry) == VLAN_TRUE)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                      ALL_FAILURE_TRC, VLAN_NAME,
                      "Learning Limit exceeded for the port\n");
            return VLAN_FALSE;
        }
    }
    else
    {
        if (pFdbEntry->u2Port != pVlanPortEntry->u2Port)
        {
            if (pVlanPbPortEntry->u1MacLearningStatus == VLAN_DISABLED)
            {
                return VLAN_FALSE;
            }
            if ((pVlanPbPortEntry->u4NumLearntMacEntries + 1) >
                pVlanPbPortEntry->u4MacLearningLimit)
            {
                return VLAN_FALSE;
            }
        }
    }
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateLearntMacCount                       */
/*                                                                           */
/*    Description         : This function updates the Mac Count learnt on the*/
/*                          port and vlan.                                   */
/*                                                                           */
/*    Input(s)            : pFdbEntry - Learned Entry                        */
/*                          pVlanPortEntry - Port on which Mac is learned    */
/*                          u1Flag - VLAN_ADD/VLAN_DELETE/VLAN_UPDATE        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanPbUpdateLearntMacCount (tVlanPortEntry * pVlanPortEntry,
                            tVlanFdbEntry * pFdbEntry, UINT1 u1Flag)
{
    tVlanPbPortEntry   *pPrevVlanPbPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;

    if (pVlanPbPortEntry != NULL)
    {
        /*Updating Port Mac Count when an FdbEntry is added / deleted */
        if (u1Flag == VLAN_ADD)
        {
            pVlanPbPortEntry->u4NumLearntMacEntries++;
        }
        else if (u1Flag == VLAN_DELETE)
        {
            pVlanPbPortEntry->u4NumLearntMacEntries--;
        }

    }

    if (u1Flag == VLAN_UPDATE)
    {
        if (pFdbEntry->u2Port != pVlanPortEntry->u2Port)
        {
            /*If there is a change in the port in the fdbentry */
            pPrevVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (pFdbEntry->u2Port);

            if (pPrevVlanPbPortEntry != NULL)
            {
                /* Decrement from the Previous Port mac count */
                pPrevVlanPbPortEntry->u4NumLearntMacEntries--;
            }
            if (pVlanPbPortEntry != NULL)
            {
                /*Increment the current port mac count */
                pVlanPbPortEntry->u4NumLearntMacEntries++;
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetBridgeMode                              */
/*                                                                           */
/*    Description         : This function is used to get the Bridge mode     */
/*                                                                           */
/*    Input(s)            : pi4BridgeMode - Bridge mode                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*****************************************************************************/
INT4
VlanPbGetBridgeMode (INT4 i4Context, INT4 *pi4BridgeMode)
{
    if (VlanSelectContext ((UINT4) i4Context) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        /* VLAN module is shutdown...return default bridge mode */
        *pi4BridgeMode = (INT4) VLAN_INVALID_BRIDGE_MODE;
        VlanReleaseContext ();
        return VLAN_SUCCESS;
    }

    *pi4BridgeMode = VLAN_BRIDGE_MODE ();
    VlanReleaseContext ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbValidatePortType                           */
/*                                                                           */
/*    Description         : This function tests whether given port type can  */
/*                          set for port in the given bridge.                */
/*                                                                           */
/*    Input(s)            : u1PbPortType - PbPortType to be validated.       */
/*                          u2LocalPort - Local Port                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->VlanInfo.u4BridgeMode   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE / VLAN_FALSE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbValidatePortType (UINT2 u2LocalPort, UINT1 u1BrgPortType)
{
    tVlanPortEntry     *pPortEntry;
    INT4                i4RetVal = VLAN_TRUE;
    UINT1               u1Status = L2IWF_DISABLED;
    UINT4               u4BridgeMode;

    /* In Customer bridge and in Provider (q-in-q) bridge the bridge
     * port type can be only CustomerPort. */
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        if ((u1BrgPortType == VLAN_CUSTOMER_BRIDGE_PORT) ||
            (u1BrgPortType == VLAN_UPLINK_ACCESS_PORT))
        {
            return VLAN_TRUE;
        }
        return VLAN_FALSE;
    }
    else if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

        if (pPortEntry == NULL)
        {
            return VLAN_FALSE;
        }

        /* In Provider Bridges, if SISP is enabled on the port,
           Port Type can be either PNP or P-PNP */
        if (u2LocalPort >= VLAN_MAX_PORTS + 1)
        {
            return VLAN_FALSE;
        }

        VlanL2IwfGetSispPortCtrlStatus (VLAN_GET_IFINDEX (u2LocalPort),
                                        &u1Status);

        if (u1Status == L2IWF_ENABLED)
        {
            if ((u1BrgPortType != VLAN_PROP_PROVIDER_NETWORK_PORT) &&
                (u1BrgPortType != VLAN_PROVIDER_NETWORK_PORT))
            {
                return VLAN_FALSE;
            }
        }
        u4BridgeMode = gpVlanContextInfo->VlanInfo.u4BridgeMode;
        if ((u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE)
            && (u1BrgPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
        {
            return VLAN_FALSE;
        }
    }

    /* It is a 1ad bridge. Customer Port Type configuration
     * should not be allowed on a 1ad bridge. */
    if (u1BrgPortType == VLAN_CUSTOMER_BRIDGE_PORT)
    {
        return VLAN_FALSE;
    }

    /* Customer Edge Port can be present only in Provider Edge Bridge not
     * in Provider Core Bridge (S-VLAN Bridge). */
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) &&
        (u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT))
    {
        return VLAN_FALSE;
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_FALSE)
    {
        /* If Bridge Mode is not PBB, 
           then dont allow the PBB Port Types */
        if ((u1BrgPortType == CFA_CNP_CTAGGED_PORT) ||
            (u1BrgPortType == VLAN_VIRTUAL_INSTANCE_PORT) ||
            (u1BrgPortType == VLAN_PROVIDER_INSTANCE_PORT) ||
            (u1BrgPortType == VLAN_CUSTOMER_BACKBONE_PORT))
        {
            return VLAN_FALSE;
        }
    }
    else                        /* PBB Bridge Mode */
    {
        i4RetVal = VlanPbbValidatePortType (u2LocalPort, u1BrgPortType);
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSetDot1adPortType                          */
/*                                                                           */
/*    Description         : This function sets the given port type for the   */
/*                          given port and the corresponding port properties.*/
/*                                                                           */
/*    Input(s)            : pVlanPortEntry - Pointer to port entry.          */
/*                          u1PbPortType - Port type to be changed.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT1
VlanPbSetDot1adPortType (tVlanPortEntry * pVlanPortEntry, UINT1 u1PbPortType)
{
    INT4                i4RetVal;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (pVlanPortEntry->u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    switch (u1PbPortType)
    {

        case VLAN_CUSTOMER_EDGE_PORT:
            i4RetVal = VlanPbConfCustomerEdgePort (pVlanPortEntry->u2Port);
            break;

        case VLAN_CNP_PORTBASED_PORT:
            i4RetVal = VlanPbConfCustomerNetworkPort (pVlanPortEntry->u2Port,
                                                      VLAN_PORT_BASED_SERVICE);
            break;

        case VLAN_CNP_TAGGED_PORT:
            i4RetVal = VlanPbConfCustomerNetworkPort (pVlanPortEntry->u2Port,
                                                      VLAN_STAG_BASED_SERVICE);
            break;

        case VLAN_VIRTUAL_INSTANCE_PORT:
            i4RetVal = VlanPbConfVirtualInstancePort (pVlanPortEntry->u2Port);
            break;

        case VLAN_CUSTOMER_BACKBONE_PORT:
            i4RetVal = VlanPbConfCustomerBackbonePort (pVlanPortEntry->u2Port);
            break;

        case VLAN_PROVIDER_NETWORK_PORT:
            i4RetVal = VlanPbConfProviderNetworkPort (pVlanPortEntry->u2Port);
            break;

        case VLAN_PROP_CUSTOMER_EDGE_PORT:
            i4RetVal = VlanPbConfPropCustomerEdgePort (pVlanPortEntry->u2Port);
            break;

        case VLAN_PROP_CUSTOMER_NETWORK_PORT:
            i4RetVal =
                VlanPbConfPropCustomerNetworkPort (pVlanPortEntry->u2Port);
            break;

        case VLAN_PROP_PROVIDER_NETWORK_PORT:
            i4RetVal =
                VlanPbConfPropProviderNetworkPort (pVlanPortEntry->u2Port);
            break;

        default:
            return VLAN_FAILURE;

    }

    if (i4RetVal != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    pVlanPbPortEntry->u1PbPortType = u1PbPortType;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCreateSVlanTables                          */
/*                                                                           */
/*    Description         : This function creates the RBTree's for all the   */
/*                          SVLAN tables.                                    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbCreateSVlanTables ()
{
    UINT4               u4Index = 0;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanCVlanSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tVlanCVlanSrcMacSVlanEntry, RBNode),
         gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tVlanCVlanDstMacSVlanEntry, RBNode),
         gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tVlanCVlanDscpSVlanEntry, RBNode),
         gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tVlanCVlanDstIpSVlanEntry, RBNode),
         gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanSrcMacSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanDstMacSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanSrcIpSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanDstIpSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanDscpSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }

    u4Index++;

    VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanSrcDstIpSVlanEntry, RBNode),
                              gVlanSVlanTableInfo[u4Index].pSVlanTableCmp);

    if (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4Index] == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                       "S-VLAN classification table type %d RB Tree"
                       "creation failed\n",
                       gVlanSVlanTableInfo[u4Index].i4SVlanTableType);
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetUntaggedCepStatus                       */
/*                                                                           */
/*    Description         : This function returns the status (VLAN_TRUE/     */
/*                          VLAN_FALSE) of Untagged-CEP for this             */
/*                          (CEP,S-VLAN) pair.                               */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                          SVlanId - Service VLAN ID                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE / VLAN_FALSE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetUntaggedCepStatus (UINT2 u2Port, tVlanId SVlanId)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4CurrCVlanId = 0;
    UINT4               u4NextCVlanId;
    UINT4               u4TblIndex;
    INT4                i4CurrPort;
    INT4                i4NextPort;
    INT4                i4RetVal = VLAN_FALSE;

    i4CurrPort = (INT4) u2Port;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return i4RetVal;
    }
    if (VlanPbGetNextCVlanSVlanEntry (u4TblIndex, i4CurrPort, u4CurrCVlanId,
                                      &i4NextPort, &u4NextCVlanId)
        == VLAN_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        if (i4CurrPort != i4NextPort)
        {
            break;
        }

        pCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                                     (UINT2) i4NextPort,
                                                     (tVlanId) u4NextCVlanId);

        if (pCVlanSVlanEntry == NULL)
        {
            return i4RetVal;
        }

        if ((pCVlanSVlanEntry->SVlanId == SVlanId) &&
            (pCVlanSVlanEntry->u1CepUntag == VLAN_SNMP_TRUE))
        {
            i4RetVal = VLAN_TRUE;
            break;
        }

        i4CurrPort = i4NextPort;
        u4CurrCVlanId = u4NextCVlanId;

    }
    while (VlanPbGetNextCVlanSVlanEntry
           (u4TblIndex, i4CurrPort, u4CurrCVlanId,
            &i4NextPort, &u4NextCVlanId) == VLAN_SUCCESS);

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlansFromCVlanSVlanTable                 */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in the CVID     */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4TblIndex   - Index to identify the S-VLAN Table*/
/*                          u2Port       - Local Port Number                 */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetCVlansFromCVlanSVlanTable (UINT4 u4TblIndex, UINT2 u2Port,
                                  tVlanId SVlanId, tCVlanInfo * pCVlanInfo)
{
    tVlanCVlanSVlanEntry CVlanSVlanEntry;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanCVlanSVlanEntry *pNextCVlanSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanSVlanEntry, 0, sizeof (tVlanCVlanSVlanEntry));

    CVlanSVlanEntry.u2Port = u2Port;

    pCVlanSVlanEntry = (tVlanCVlanSVlanEntry *) RBTreeGetNext
        (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
         (tRBElem *) & CVlanSVlanEntry, NULL);

    while ((pCVlanSVlanEntry != NULL) && (pCVlanSVlanEntry->u2Port == u2Port))
    {
        if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanSVlanEntry->SVlanId == SVlanId)
            {
                if (pCVlanSVlanEntry->u1CepUntag == VLAN_SNMP_FALSE)
                {
                    OSIX_BITLIST_SET_BIT (pCVlanInfo->TaggedCVlanList,
                                          pCVlanSVlanEntry->CVlanId,
                                          VLAN_DEV_VLAN_LIST_SIZE);
                }
                else
                {
                    OSIX_BITLIST_SET_BIT (pCVlanInfo->UntaggedCVlanList,
                                          pCVlanSVlanEntry->CVlanId,
                                          VLAN_DEV_VLAN_LIST_SIZE);
                }
            }
        }

        pNextCVlanSVlanEntry = (tVlanCVlanSVlanEntry *) RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
             (tRBElem *) pCVlanSVlanEntry, NULL);

        pCVlanSVlanEntry = pNextCVlanSVlanEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlansFromCVlanSrcMacSVlanTable           */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in              */
/*                          CVlanSrcMacSVlan Table                           */
/*                                                                           */
/*    Input(s)            : u4TblIndex   - Index to identify the S-VLAN Table*/
/*                          u2Port       - Local Port Number                 */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetCVlansFromCVlanSrcMacSVlanTable (UINT4 u4TblIndex, UINT2 u2Port,
                                        tVlanId SVlanId, tVlanList CVlanList)
{
    tVlanCVlanSrcMacSVlanEntry CVlanSrcMacSVlanEntry;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tVlanCVlanSrcMacSVlanEntry *pNextCVlanSrcMacSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanSrcMacSVlanEntry, 0,
                 sizeof (tVlanCVlanSrcMacSVlanEntry));

    CVlanSrcMacSVlanEntry.u2Port = u2Port;

    pCVlanSrcMacSVlanEntry = (tVlanCVlanSrcMacSVlanEntry *) RBTreeGetNext
        (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
         (tRBElem *) & CVlanSrcMacSVlanEntry, NULL);

    while ((pCVlanSrcMacSVlanEntry != NULL) &&
           (pCVlanSrcMacSVlanEntry->u2Port == u2Port))
    {
        if (pCVlanSrcMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanSrcMacSVlanEntry->SVlanId == SVlanId)
            {
                OSIX_BITLIST_SET_BIT (CVlanList,
                                      pCVlanSrcMacSVlanEntry->CVlanId,
                                      VLAN_DEV_VLAN_LIST_SIZE);
            }
        }

        pNextCVlanSrcMacSVlanEntry =
            (tVlanCVlanSrcMacSVlanEntry *) RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
             (tRBElem *) pCVlanSrcMacSVlanEntry, NULL);

        pCVlanSrcMacSVlanEntry = pNextCVlanSrcMacSVlanEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlansFromCVlanDstMacSVlanTable           */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in              */
/*                          CVlanDstMacSVlan Table                           */
/*                                                                           */
/*    Input(s)            : u4TblIndex   - Index to identify the S-VLAN Table*/
/*                          u2Port       - Local Port Number                 */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetCVlansFromCVlanDstMacSVlanTable (UINT4 u4TblIndex, UINT2 u2Port,
                                        tVlanId SVlanId, tVlanList CVlanList)
{
    tVlanCVlanDstMacSVlanEntry CVlanDstMacSVlanEntry;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tVlanCVlanDstMacSVlanEntry *pNextCVlanDstMacSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanDstMacSVlanEntry, 0,
                 sizeof (tVlanCVlanDstMacSVlanEntry));

    CVlanDstMacSVlanEntry.u2Port = u2Port;

    pCVlanDstMacSVlanEntry = (tVlanCVlanDstMacSVlanEntry *) RBTreeGetNext
        (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
         (tRBElem *) & CVlanDstMacSVlanEntry, NULL);

    while ((pCVlanDstMacSVlanEntry != NULL) &&
           (pCVlanDstMacSVlanEntry->u2Port == u2Port))
    {
        if (pCVlanDstMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDstMacSVlanEntry->SVlanId == SVlanId)
            {
                OSIX_BITLIST_SET_BIT (CVlanList,
                                      pCVlanDstMacSVlanEntry->CVlanId,
                                      VLAN_DEV_VLAN_LIST_SIZE);
            }
        }

        pNextCVlanDstMacSVlanEntry =
            (tVlanCVlanDstMacSVlanEntry *) RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
             (tRBElem *) pCVlanDstMacSVlanEntry, NULL);

        pCVlanDstMacSVlanEntry = pNextCVlanDstMacSVlanEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlansFromCVlanDscpSVlanTable             */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in              */
/*                          CVlanDscpSVlan Table                             */
/*                                                                           */
/*    Input(s)            : u4TblIndex   - Index to identify the S-VLAN Table*/
/*                          u2Port       - Local Port Number                 */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetCVlansFromCVlanDscpSVlanTable (UINT4 u4TblIndex, UINT2 u2Port,
                                      tVlanId SVlanId, tVlanList CVlanList)
{
    tVlanCVlanDscpSVlanEntry CVlanDscpSVlanEntry;
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tVlanCVlanDscpSVlanEntry *pNextCVlanDscpSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanDscpSVlanEntry, 0, sizeof (tVlanCVlanDscpSVlanEntry));

    CVlanDscpSVlanEntry.u2Port = u2Port;

    pCVlanDscpSVlanEntry = (tVlanCVlanDscpSVlanEntry *) RBTreeGetNext
        (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
         (tRBElem *) & CVlanDscpSVlanEntry, NULL);

    while ((pCVlanDscpSVlanEntry != NULL) &&
           (pCVlanDscpSVlanEntry->u2Port == u2Port))
    {
        if (pCVlanDscpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDscpSVlanEntry->SVlanId == SVlanId)
            {
                OSIX_BITLIST_SET_BIT (CVlanList,
                                      pCVlanDscpSVlanEntry->CVlanId,
                                      VLAN_DEV_VLAN_LIST_SIZE);
            }
        }

        pNextCVlanDscpSVlanEntry =
            (tVlanCVlanDscpSVlanEntry *) RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
             (tRBElem *) pCVlanDscpSVlanEntry, NULL);

        pCVlanDscpSVlanEntry = pNextCVlanDscpSVlanEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlansFromCVlanDstIpSVlanTable            */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in              */
/*                          CVlanDstIpSVlan Table                            */
/*                                                                           */
/*    Input(s)            : u4TblIndex   - Index to identify the S-VLAN Table*/
/*                          u2Port       - Local Port Number                 */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetCVlansFromCVlanDstIpSVlanTable (UINT4 u4TblIndex, UINT2 u2Port,
                                       tVlanId SVlanId, tVlanList CVlanList)
{
    tVlanCVlanDstIpSVlanEntry CVlanDstIpSVlanEntry;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;
    tVlanCVlanDstIpSVlanEntry *pNextCVlanDstIpSVlanEntry = NULL;

    VLAN_MEMSET (&CVlanDstIpSVlanEntry, 0, sizeof (tVlanCVlanDstIpSVlanEntry));

    CVlanDstIpSVlanEntry.u2Port = u2Port;

    pCVlanDstIpSVlanEntry = (tVlanCVlanDstIpSVlanEntry *) RBTreeGetNext
        (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
         (tRBElem *) & CVlanDstIpSVlanEntry, NULL);

    while ((pCVlanDstIpSVlanEntry != NULL) &&
           (pCVlanDstIpSVlanEntry->u2Port == u2Port))
    {
        if (pCVlanDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDstIpSVlanEntry->SVlanId == SVlanId)
            {
                OSIX_BITLIST_SET_BIT (CVlanList,
                                      pCVlanDstIpSVlanEntry->CVlanId,
                                      VLAN_DEV_VLAN_LIST_SIZE);
            }
        }

        pNextCVlanDstIpSVlanEntry =
            (tVlanCVlanDstIpSVlanEntry *) RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
             (tRBElem *) pCVlanDstIpSVlanEntry, NULL);

        pCVlanDstIpSVlanEntry = pNextCVlanDstIpSVlanEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCheckEtherTypeSwap                         */
/*                                                                           */
/*    Description         : This function checks whether the incoming frame  */
/*                          is tagged based on the entries configured in     */
/*                          ether type swap table.                           */
/*                                                                           */
/*    Input(s)            : u2Port      - Receive port                       */
/*                          u2EtherType - Ethertype in the incoming frame    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE/VLAN_FALSE                       */
/*                                                                           */
/*****************************************************************************/
BOOL1
VlanPbCheckEtherTypeSwap (UINT2 u2Port, UINT2 u2EtherType)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if ((pVlanPbPortEntry != NULL) &&
        (pVlanPbPortEntry->u1EtherTypeSwapStatus == VLAN_ENABLED))
    {
        pEtherTypeSwapEntry = VlanPbGetEtherTypeSwapEntry (u2Port, u2EtherType);
        if (pEtherTypeSwapEntry != NULL)
        {
            return VLAN_TRUE;
        }
    }
    /*
     * Ethertype does not match. No entry found in ethertype swap 
     * table.
     */
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPrimaryContextEtherType                   */
/*                                                                           */
/*    Description         : This function used to get the ethertype(Ingress  */
/*                          or Egress) of given physical port.               */
/*                                                                           */
/*    Input(s)            : u4IfIndex     - IfIndex of the port              */
/*                          pu2EtherValue - Returned Ether typeming frame    */
/*                          u1EtherType   - Flag to identify Ingress or      */
/*                                          Egress ethertype.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetPrimaryContextEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherValue,
                                UINT1 u1EtherType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (u1EtherType == VLAN_INGRESS_ETHERTYPE)
    {
        *pu2EtherValue =
            VLAN_CONTEXT_PTR (u4ContextId)->apVlanPortEntry[u2LocalPort]->
            u2IngressEtherType;
    }
    else
    {
        *pu2EtherValue =
            VLAN_CONTEXT_PTR (u4ContextId)->apVlanPortEntry[u2LocalPort]->
            u2EgressEtherType;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandlePbRemovePortTablesFromHw               */
/*                                                                           */
/*    Description         : This function is used to remove the PB port      */
/*                          properties in pSrcPortEntry from physical port   */
/*                          u2DstPort in hardware. This function will be     */
/*                          called when a port comes out of the port channel */
/*                                                                           */
/*    Input(s)            : u2DstPort - Physical Port Index                  */
/*                          u2SrcPort - Port channel properties need to be   */
/*                          propgrammed in physical                          */
/*                          u2DstPort port                                   */
/*                          pVlanQMsg - Message received by VLAN             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHandlePbRemovePortTablesFromHw (tVlanQMsg * pVlanQMsg, UINT2 u2SrcPort,
                                    UINT2 u2DstPort)
{
    tVlanPortEntry     *pVlanSrcPortEntry = NULL;

    pVlanSrcPortEntry = VLAN_GET_PORT_ENTRY (u2SrcPort);

    if (pVlanSrcPortEntry->u1OperStatus == VLAN_OPER_UP)
    {
        VlanPbNpUpdateCvidAndPepInHw (u2SrcPort, 0, u2DstPort, VLAN_DELETE);
    }

    VlanPbRemoveHwSVlanTranslationTable (u2SrcPort, u2DstPort);

    UNUSED_PARAM (pVlanQMsg);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSetSVlanConfigServiceType                 */
/*    Description         : This function is used to set the service-type   */
/*                          being configured                                 */
/*    Input               : The Indices Dot1qVlanIndex &                     */
/*                          value testValFsPbSVlanConfigServiceType          */
/*    Output              : -                                                */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT1
VlanPbSetSVlanConfigServiceType (UINT4 u4Dot1qVlanIndex,
                                 INT4 i4SetValFsPbSVlanConfigServiceType)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;
    UINT1               u1ServiceType = 0;
    UINT1               u1VlanLearningMode = 0;
    UINT1               u1PrevMacLearningStatus = 0;
    INT4                i4RetVal = 0;
    UINT4               u4FdbId = 0;
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (VlanL2IwfPbGetVlanServiceType (VLAN_CURR_CONTEXT_ID (),
                                       (tVlanId) u4Dot1qVlanIndex,
                                       &u1ServiceType) != L2IWF_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u1ServiceType == i4SetValFsPbSVlanConfigServiceType)
    {
        return SNMP_SUCCESS;
    }

    if (u1ServiceType != i4SetValFsPbSVlanConfigServiceType)
    {
        /*Service Type Changing From E-LINE to E-LAN */

        if (i4SetValFsPbSVlanConfigServiceType == VLAN_E_LAN)
        {
            VlanAddTrunkPortsToNewVlan (u4Dot1qVlanIndex);
        }

        /*Service Type Changing From E-LAN to E-LINE */

        else if (i4SetValFsPbSVlanConfigServiceType == VLAN_E_LINE)
        {
            VlanRemoveTrunkPortsForElineVlan (u4Dot1qVlanIndex);
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    /* Service type change for a vlan will affect the oper P2P
     * status for the PEPs corresponding to this service vlan.
     * So make those PEPs oper down, then change the service type.
     * Once the service type is changed, make the PEPs oper up
     * (if all the conditions for pep oper up is met. */

    if (VlanPbIsSVlanActive ((tVlanId) u4Dot1qVlanIndex) == VLAN_TRUE)
    {
        VlanPbNotifySVlanStatusForPep ((tVlanId) u4Dot1qVlanIndex,
                                       VLAN_DESTROY);
    }

    i4RetVal = VlanL2IwfPbSetVlanServiceType (VLAN_CURR_CONTEXT_ID (),
                                              (tVlanId) u4Dot1qVlanIndex,
                                              (UINT1)
                                              i4SetValFsPbSVlanConfigServiceType);

    /* If the S-VLAN is active (Curr entry is present) then given
     * vlan-active notification to pep. */
    if (VlanPbIsSVlanActive ((tVlanId) u4Dot1qVlanIndex) == VLAN_TRUE)
    {
        VlanPbNotifySVlanStatusForPep ((tVlanId) u4Dot1qVlanIndex, VLAN_ACTIVE);
    }

    if (i4RetVal == L2IWF_FAILURE)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSVlanConfigServiceType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                              SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", VLAN_CURR_CONTEXT_ID (),
                          u4Dot1qVlanIndex,
                          i4SetValFsPbSVlanConfigServiceType));
        VlanSelectContext (u4CurrContextId);

        return SNMP_FAILURE;
    }

    u1VlanLearningMode = VlanGetVlanLearningMode ();
    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qVlanIndex);
    if (pStVlanEntry != NULL)
    {
        pStVlanEntry->u1ServiceType =
            (UINT1) i4SetValFsPbSVlanConfigServiceType;
    }

    if (u1VlanLearningMode == VLAN_INDEP_LEARNING)
    {
        if (VlanGetFdbIdFromVlanId ((tVlanId) u4Dot1qVlanIndex,
                                    &u4FdbId) == VLAN_FAILURE)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSVlanConfigServiceType,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                              VLAN_CURR_CONTEXT_ID (), u4Dot1qVlanIndex,
                              i4SetValFsPbSVlanConfigServiceType));
            VlanSelectContext (u4CurrContextId);

            return SNMP_FAILURE;
        }

        if (pStVlanEntry == NULL)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSVlanConfigServiceType,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                              VLAN_CURR_CONTEXT_ID (), u4Dot1qVlanIndex,
                              i4SetValFsPbSVlanConfigServiceType));
            VlanSelectContext (u4CurrContextId);

            return SNMP_FAILURE;
        }

        u1PrevMacLearningStatus =
            VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId) u4Dot1qVlanIndex);

        if (VLAN_CONTROL_ADM_MAC_LEARNING_STATUS ((tVlanId) u4Dot1qVlanIndex) ==
            VLAN_DISABLED)
        {
            VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId) u4Dot1qVlanIndex) =
                VLAN_DISABLED;
        }
        else
        {
            if (i4SetValFsPbSVlanConfigServiceType == VLAN_E_LINE)
            {
                VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId)
                                                       u4Dot1qVlanIndex) =
                    VLAN_DISABLED;
            }
            if (i4SetValFsPbSVlanConfigServiceType == VLAN_E_LAN)
            {
                VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId)
                                                       u4Dot1qVlanIndex) =
                    VLAN_DISABLED;
                VLAN_IS_NO_EGRESS_PORTS_PRESENT (pStVlanEntry, u1Result);
                if (u1Result == VLAN_FALSE)
                {
                    if (VLAN_CONTROL_ADM_MAC_LEARNING_STATUS
                        ((tVlanId) u4Dot1qVlanIndex) == VLAN_ENABLED)
                    {
                        VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId)
                                                               u4Dot1qVlanIndex)
                            = VLAN_ENABLED;
                    }
                    if (VLAN_CONTROL_ADM_MAC_LEARNING_STATUS
                        ((tVlanId) u4Dot1qVlanIndex) == VLAN_DEFAULT)
                    {
                        VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId)
                                                               u4Dot1qVlanIndex)
                            = VLAN_GLOBAL_MAC_LEARNING_STATUS ();
                    }
                }
            }
        }

        if (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId) u4Dot1qVlanIndex)
            != u1PrevMacLearningStatus)
        {
            pu1LocalPortList = UtilPlstAllocLocalPortList
                (sizeof (tLocalPortList));
            if (pu1LocalPortList == NULL)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbSVlanConfigServiceType, u4SeqNum,
                                      FALSE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                                  VLAN_CURR_CONTEXT_ID (), u4Dot1qVlanIndex,
                                  i4SetValFsPbSVlanConfigServiceType));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));

            VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

            if (VlanHwMacLearningStatus (VLAN_CURR_CONTEXT_ID (),
                                         (tVlanId) u4Dot1qVlanIndex,
                                         pu1LocalPortList,
                                         VLAN_CONTROL_OPER_MAC_LEARNING_STATUS
                                         ((tVlanId) u4Dot1qVlanIndex))
                != VLAN_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbSVlanConfigServiceType, u4SeqNum,
                                      FALSE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                                  VLAN_CURR_CONTEXT_ID (), u4Dot1qVlanIndex,
                                  i4SetValFsPbSVlanConfigServiceType));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (i4SetValFsPbSVlanConfigServiceType == VLAN_E_LINE)
            {
                VlanMiFlushFdbId (u4CurrContextId, u4Dot1qVlanIndex);
            }
            UtilPlstReleaseLocalPortList (pu1LocalPortList);
        }
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSVlanConfigServiceType,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", VLAN_CURR_CONTEXT_ID (),
                      u4Dot1qVlanIndex, i4SetValFsPbSVlanConfigServiceType));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;

}
#endif
