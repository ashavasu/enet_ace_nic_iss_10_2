/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbnpapi.c,v 1.25 2015/12/24 10:34:52 siva Exp $
 *
 * Description: This file contains api related changes used in VLANmodule
 *              for provider bridges
 *
 *******************************************************************/
#ifndef _VLAN_PB_H_
#define _VLAN_PB_H_
#include "vlaninc.h"

extern UINT4        FsMIPbCVlanDstMacRowStatus[12];
extern UINT4        FsMIPbCVlanSrcMacRowStatus[12];
extern UINT4        FsMIPbCVlanDscpRowStatus[12];
extern UINT4        FsMIPbCVlanDstIpRowStatus[12];
extern UINT4        FsMIPbSrcMacRowStatus[12];
extern UINT4        FsMIPbDstMacRowStatus[12];
extern UINT4        FsMIPbSrcIpRowStatus[12];
extern UINT4        FsMIPbDstIpRowStatus[12];
extern UINT4        FsMIPbDscpRowStatus[12];
extern UINT4        FsMIPbSrcDstIpRowStatus[12];

#define VLAN_DEV_INVALID_VLAN_ID 0
/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortCVlanSrcMacForPort
*                          
*    Description         : This function removes the C-VLAN,source MAC based 
*                          S-VLAN classification Entries for a particular Port,
*                          from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortCVlanSrcMacForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacEntry = NULL;
    tVlanCVlanSrcMacSVlanEntry *pTempCVlanSrcMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pCVlanSrcMacEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pCVlanSrcMacEntry != NULL)
    {

        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (pCVlanSrcMacEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pCVlanSrcMacEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pCVlanSrcMacEntry->u2Port);
                VlanSVlanMap.CVlanId = pCVlanSrcMacEntry->CVlanId;
                VlanSVlanMap.SVlanId = pCVlanSrcMacEntry->SVlanId;
                MEMCPY (&VlanSVlanMap.SrcMac,
                        &pCVlanSrcMacEntry->SrcMac, VLAN_MAC_ADDR_LEN);
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Deleting CVID SRC MAC"
                                   "Entry Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }

                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pCVlanSrcMacEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing CVID SRC MAC "
                               "Entry from RBTree Failed for Port %d \r\n",
                               u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanSrcMacRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pCVlanSrcMacEntry->CVlanId,
                                  pCVlanSrcMacEntry->SrcMac, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempCVlanSrcMacEntry = pCVlanSrcMacEntry;

        pCVlanSrcMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempCVlanSrcMacEntry, VlanPbPortCVlanSrcMacCmp);

        if (pTempCVlanSrcMacEntry->u2Port == u2Port)
        {
            if (MemReleaseMemBlock
                (gVlanPbPoolInfo.VlanCVlanSrcMacPoolId,
                 (UINT1 *) pTempCVlanSrcMacEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Freeing CVID SRC MAC Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortCVlanDstMacSVlanForPort 
*                          
*    Description         : This function removes the C-VLAN,Destination MAC
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortCVlanDstMacSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacEntry = NULL;
    tVlanCVlanDstMacSVlanEntry *pTempCVlanDstMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pCVlanDstMacEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pCVlanDstMacEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);

        if (pCVlanDstMacEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pCVlanDstMacEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pCVlanDstMacEntry->u2Port);
                VlanSVlanMap.CVlanId = pCVlanDstMacEntry->CVlanId;
                VlanSVlanMap.SVlanId = pCVlanDstMacEntry->SVlanId;
                MEMCPY (&VlanSVlanMap.DstMac,
                        &pCVlanDstMacEntry->DstMac, VLAN_MAC_ADDR_LEN);
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Deleting CVID DST MAC "
                                   "Entry Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }
                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pCVlanDstMacEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing CVID DST MAC "
                               "Entry from RBTree Failed for Port %d \r\n",
                               u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstMacRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pCVlanDstMacEntry->CVlanId,
                                  pCVlanDstMacEntry->DstMac, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);

        }
        pTempCVlanDstMacEntry = pCVlanDstMacEntry;

        pCVlanDstMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempCVlanDstMacEntry, VlanPbPortCVlanDstMacCmp);

        if (pTempCVlanDstMacEntry->u2Port == u2Port)
        {
            if (MemReleaseMemBlock
                (gVlanPbPoolInfo.VlanCVlanDstMacPoolId,
                 (UINT1 *) pTempCVlanDstMacEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing CVID DST MAC "
                                   "Entry from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortCVlanDscpForPort
*                          
*    Description         : This function removes the C-VLAN, Dscp
*                          based S-VLAN classification Entry for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortCVlanDscpForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpEntry = NULL;
    tVlanCVlanDscpSVlanEntry *pTempCVlanDscpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pCVlanDscpEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pCVlanDscpEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (pCVlanDscpEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pCVlanDscpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pCVlanDscpEntry->u2Port);
                VlanSVlanMap.CVlanId = pCVlanDscpEntry->CVlanId;
                VlanSVlanMap.SVlanId = pCVlanDscpEntry->SVlanId;
                VlanSVlanMap.u4Dscp = pCVlanDscpEntry->u4Dscp;
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Deleting CVID DSCP "
                                   "Entry Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }
                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pCVlanDscpEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing CVID DSCP Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pCVlanDscpEntry->CVlanId,
                                  pCVlanDscpEntry->u4Dscp, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempCVlanDscpEntry = pCVlanDscpEntry;

        pCVlanDscpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempCVlanDscpEntry, VlanPbPortCVlanDscpCmp);

        if (pTempCVlanDscpEntry->u2Port == u2Port)
        {
            if (MemReleaseMemBlock
                (gVlanPbPoolInfo.VlanCVlanDscpPoolId,
                 (UINT1 *) pTempCVlanDscpEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing CVID DSCP Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_FAILURE;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortCVlanDstIpForPort
*                          
*    Description         : This function removes the C-VLAN, Destination IP
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortCVlanDstIpForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpEntry = NULL;
    tVlanCVlanDstIpSVlanEntry *pTempCVlanDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pCVlanDstIpEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pCVlanDstIpEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);

        if (pCVlanDstIpEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pCVlanDstIpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pCVlanDstIpEntry->u2Port);
                VlanSVlanMap.CVlanId = pCVlanDstIpEntry->CVlanId;
                VlanSVlanMap.SVlanId = pCVlanDstIpEntry->SVlanId;
                VlanSVlanMap.u4DstIp = pCVlanDstIpEntry->u4DstIp;
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Deleting CVID DST IP"
                                   "Entry Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }

                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pCVlanDstIpEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Removing CVID DST IP Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pCVlanDstIpEntry->CVlanId,
                                  pCVlanDstIpEntry->u4DstIp, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempCVlanDstIpEntry = pCVlanDstIpEntry;

        pCVlanDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempCVlanDstIpEntry, VlanPbPortCVlanDstIpCmp);

        if (pTempCVlanDstIpEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock
                (gVlanPbPoolInfo.VlanCVlanDstIpPoolId,
                 (UINT1 *) pTempCVlanDstIpEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Freeing CVID DST IP Entry "
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortSrcMacSVlanForPort
*                          
*    Description         : This function removes the Source MAC 
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortSrcMacSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanSrcMacSVlanEntry *pSrcMacEntry = NULL;
    tVlanSrcMacSVlanEntry *pTempSrcMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pSrcMacEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pSrcMacEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);

        if (pSrcMacEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pSrcMacEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pSrcMacEntry->u2Port);
                VlanSVlanMap.SVlanId = pSrcMacEntry->SVlanId;
                MEMCPY (&VlanSVlanMap.SrcMac, &pSrcMacEntry->SrcMac,
                        VLAN_MAC_ADDR_LEN);
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Deleting SRC MAC Entry"
                                   "Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }
                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pSrcMacEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing SRC MAC Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pSrcMacEntry->SrcMac, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempSrcMacEntry = pSrcMacEntry;

        pSrcMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempSrcMacEntry, VlanPbPortSrcMacCmp);

        if (pTempSrcMacEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock
                (gVlanPbPoolInfo.VlanSrcMacPoolId,
                 (UINT1 *) pTempSrcMacEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing SRC MAC Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortDstMacSVlanForPort
*
*    Description         : This function removes the Destination MAC 
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*
*****************************************************************************/
INT4
VlanPbHwRemovePortDstMacSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanDstMacSVlanEntry *pDstMacEntry = NULL;
    tVlanDstMacSVlanEntry *pTempDstMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;
    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pDstMacEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pDstMacEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);

        if (pDstMacEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pDstMacEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pDstMacEntry->u2Port);
                VlanSVlanMap.SVlanId = pDstMacEntry->SVlanId;
                MEMCPY (&VlanSVlanMap.DstMac, &pDstMacEntry->DstMac,
                        VLAN_MAC_ADDR_LEN);
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Deleting DST MAC"
                                   "Entry Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }

                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pDstMacEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing DST MAC "
                               "Entry from RBTree Failed for Port %d \r\n",
                               u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pDstMacEntry->DstMac, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempDstMacEntry = pDstMacEntry;

        pDstMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempDstMacEntry, VlanPbPortDstMacCmp);

        if (pTempDstMacEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock
                (gVlanPbPoolInfo.VlanDstMacPoolId,
                 (UINT1 *) pTempDstMacEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing DST MAC Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortSrcIpSVlanForPort
*                          
*    Description         : This function removes the Source IP 
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortSrcIpSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanSrcIpSVlanEntry *pSrcIpEntry = NULL;
    tVlanSrcIpSVlanEntry *pTempSrcIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pSrcIpEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pSrcIpEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (pSrcIpEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pSrcIpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pSrcIpEntry->u2Port);
                VlanSVlanMap.SVlanId = pSrcIpEntry->SVlanId;
                VlanSVlanMap.u4SrcIp = pSrcIpEntry->u4SrcIp;
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Deleting SRC IP Entry"
                                   "Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }

                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pSrcIpEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing SRC IP Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pSrcIpEntry->u4SrcIp, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempSrcIpEntry = pSrcIpEntry;

        pSrcIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempSrcIpEntry, VlanPbPortSrcIpCmp);

        if (pTempSrcIpEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock (gVlanPbPoolInfo.VlanSrcIpPoolId,
                                    (UINT1 *) pTempSrcIpEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing SRC IP Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortDstIpSVlanForPort
*                          
*    Description         : This function removes the Destination IP 
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortDstIpSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanDstIpSVlanEntry *pDstIpEntry = NULL;
    tVlanDstIpSVlanEntry *pTempDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;
    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pDstIpEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pDstIpEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (pDstIpEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pDstIpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pDstIpEntry->u2Port);
                VlanSVlanMap.SVlanId = pDstIpEntry->SVlanId;
                VlanSVlanMap.u4DstIp = pDstIpEntry->u4DstIp;
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME,
                                   "Deleting DST IP Entry"
                                   "Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }
                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pDstIpEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing DST IP Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pDstIpEntry->u4DstIp, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempDstIpEntry = pDstIpEntry;

        pDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempDstIpEntry, VlanPbPortDstIpCmp);

        if (pTempDstIpEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock (gVlanPbPoolInfo.VlanDstIpPoolId,
                                    (UINT1 *) pTempDstIpEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing DST IP Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortDscpSVlanForPort
*                          
*    Description         : This function removes the Dscp 
*                          based S-VLAN classification Entry for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortDscpSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanDscpSVlanEntry *pDscpEntry = NULL;
    tVlanDscpSVlanEntry *pTempDscpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;

    UINT4               u4Port;
    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pDscpEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pDscpEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (pDscpEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pDscpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pDscpEntry->u2Port);
                VlanSVlanMap.SVlanId = pDscpEntry->SVlanId;
                VlanSVlanMap.u4Dscp = pDscpEntry->u4Dscp;
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSCP_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Deleting DSCP Entry"
                                   "Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }
                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pDscpEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Removing DSCP Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pDscpEntry->u4Dscp, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempDscpEntry = pDscpEntry;

        pDscpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempDscpEntry, VlanPbPortDscpCmp);

        if (pTempDscpEntry->u2Port == u2Port)
        {

            if (MemReleaseMemBlock (gVlanPbPoolInfo.VlanDscpPoolId,
                                    (UINT1 *) pTempDscpEntry) != MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing DSCP Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;

}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwRemovePortSrcDstIpSVlanForPort
*                          
*    Description         : This function removes the Source IP, Destination IP 
*                          based S-VLAN classification Entries for a particular
*                          Port, from the H/w as well as software database.
*                          
*    Input(s)            : u2Port  - Port Number
*                          u1Index - S-VLAN classification table Index
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
INT4
VlanPbHwRemovePortSrcDstIpSVlanForPort (UINT2 u2Port, UINT1 u1Index)
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpEntry = NULL;
    tVlanSrcDstIpSVlanEntry *pTempSrcDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pSrcDstIpEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index]);

    while (pSrcDstIpEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (pSrcDstIpEntry->u2Port == u2Port)
        {
#ifdef NPAPI_WANTED
            if (pSrcDstIpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanSVlanMap.u2Port =
                    (UINT2) VLAN_GET_IFINDEX (pSrcDstIpEntry->u2Port);
                VlanSVlanMap.SVlanId = pSrcDstIpEntry->SVlanId;
                VlanSVlanMap.u4SrcIp = pSrcDstIpEntry->u4SrcIp;
                VlanSVlanMap.u4DstIp = pSrcDstIpEntry->u4DstIp;
                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

                if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap) != VLAN_SUCCESS)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Deleting SRC DST IP"
                                   "Entry Failed for Port %d \r\n", u4Port);

                    return VLAN_FAILURE;
                }

                MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
            }
#endif

            if (u2Port >= VLAN_MAX_PORTS + 1)
            {
                return VLAN_FAILURE;
            }
            if (RBTreeRemove
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                 pSrcDstIpEntry) != RB_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Removing SRC DST IP Entry"
                               "from RBTree Failed for Port %d \r\n", u4Port);

                return VLAN_FAILURE;
            }

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (u2Port),
                                  pSrcDstIpEntry->u4SrcIp,
                                  pSrcDstIpEntry->u4DstIp, VLAN_DESTROY));
            VlanSelectContext (u4CurrContextId);
        }
        pTempSrcDstIpEntry = pSrcDstIpEntry;

        pSrcDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u1Index],
                           pTempSrcDstIpEntry, VlanPbPortSrcDstIpCmp);

        if (pTempSrcDstIpEntry->u2Port == u2Port)
        {
            if (MemReleaseMemBlock (gVlanPbPoolInfo.VlanSrcDstIpPoolId,
                                    (UINT1 *) pTempSrcDstIpEntry) !=
                MEM_SUCCESS)
            {
                if (u2Port < VLAN_MAX_PORTS + 1)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                   VLAN_NAME, "Freeing SRC DST IP Entry"
                                   "from Mempool Failed for Port %d \r\n",
                                   u4Port);

                    return VLAN_FAILURE;
                }
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortCVlanSrcMacSVlans              */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortCVlanSrcMacSVlans ()
{
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanSrcMacEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                        apVlanSVlanTable[u1TableIndex]);

    while (pCVlanSrcMacEntry != NULL)
    {
        if (pCVlanSrcMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanSrcMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanSrcMacEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanSrcMacEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanSrcMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.SrcMac,
                    &pCVlanSrcMacEntry->SrcMac, VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pCVlanSrcMacEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting CVID SRC MAC "
                               "Entry Failed for Port %d \r\n", u4Port);
            }

            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pCVlanSrcMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanSrcMacEntry, VlanPbPortCVlanSrcMacCmp);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortCVlanDstMacSVlans              */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortCVlanDstMacSVlans ()
{
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanDstMacEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                        apVlanSVlanTable[u1TableIndex]);

    while (pCVlanDstMacEntry != NULL)
    {
        if (pCVlanDstMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDstMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                return;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstMacEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanDstMacEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanDstMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.DstMac,
                    &pCVlanDstMacEntry->DstMac, VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pCVlanDstMacEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting CVID DST MAC "
                               "Entry Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pCVlanDstMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanDstMacEntry, VlanPbPortCVlanDstMacCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortCVlanDscpSVlans                */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortCVlanDscpSVlans ()
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanDscpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                      apVlanSVlanTable[u1TableIndex]);

    while (pCVlanDscpEntry != NULL)
    {
        if (pCVlanDscpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDscpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDscpEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanDscpEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanDscpEntry->SVlanId;
            VlanSVlanMap.u4Dscp = pCVlanDscpEntry->u4Dscp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pCVlanDscpEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Deleting CVID DSCP "
                               "Entry Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pCVlanDscpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanDscpEntry, VlanPbPortCVlanDscpCmp);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortCVlanDstIpSVlans               */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortCVlanDstIpSVlans ()
{
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanDstIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                       apVlanSVlanTable[u1TableIndex]);

    while (pCVlanDstIpEntry != NULL)
    {
        if (pCVlanDstIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDstIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstIpEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanDstIpEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanDstIpEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pCVlanDstIpEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pCVlanDstIpEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting CVID DST IP "
                               "Entry Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pCVlanDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanDstIpEntry, VlanPbPortCVlanDstIpCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortSrcMacSVlans                   */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortSrcMacSVlans ()
{
    tVlanSrcMacSVlanEntry *pSrcMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pSrcMacEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                   apVlanSVlanTable[u1TableIndex]);

    while (pSrcMacEntry != NULL)
    {
        if (pSrcMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pSrcMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcMacEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.SrcMac, &pSrcMacEntry->SrcMac,
                    VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pSrcMacEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting SRC MAC Entry"
                               "Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pSrcMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pSrcMacEntry, VlanPbPortSrcMacCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortDstMacSVlans                   */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortDstMacSVlans ()
{
    tVlanDstMacSVlanEntry *pDstMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pDstMacEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                   apVlanSVlanTable[u1TableIndex]);

    while (pDstMacEntry != NULL)
    {
        if (pDstMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pDstMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstMacEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.DstMac, &pDstMacEntry->DstMac,
                    VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pDstMacEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting DST MAC Entry"
                               "Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pDstMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pDstMacEntry, VlanPbPortDstMacCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortDscpSVlans                     */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortDscpSVlans ()
{
    tVlanDscpSVlanEntry *pDscpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_DSCP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pDscpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                 apVlanSVlanTable[u1TableIndex]);

    while (pDscpEntry != NULL)
    {
        if (pDscpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pDscpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDscpEntry->u2Port));
            VlanSVlanMap.SVlanId = pDscpEntry->SVlanId;
            VlanSVlanMap.u4Dscp = pDscpEntry->u4Dscp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSCP_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pDscpEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Deleting DSCP Entry"
                               "Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pDscpEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                    apVlanSVlanTable[u1TableIndex],
                                    pDscpEntry, VlanPbPortDscpCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortSrcIpSVlans                    */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortSrcIpSVlans ()
{
    tVlanSrcIpSVlanEntry *pSrcIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;
    UINT4               u4Port;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pSrcIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                  apVlanSVlanTable[u1TableIndex]);

    while (pSrcIpEntry != NULL)
    {
        if (pSrcIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pSrcIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcIpEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcIpEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcIpEntry->u4SrcIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                u4Port = VLAN_GET_IFINDEX (pSrcIpEntry->u2Port);
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting SRC IP Entry"
                               "Failed for Port %d \r\n", u4Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pSrcIpEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                     apVlanSVlanTable[u1TableIndex],
                                     pSrcIpEntry, VlanPbPortSrcIpCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortDstIpSVlans                    */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortDstIpSVlans ()
{
    tVlanDstIpSVlanEntry *pDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pDstIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                  apVlanSVlanTable[u1TableIndex]);

    while (pDstIpEntry != NULL)
    {
        if (pDstIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pDstIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstIpEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstIpEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pDstIpEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;
            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Deleting DST IP Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pDstIpEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                     apVlanSVlanTable[u1TableIndex],
                                     pDstIpEntry, VlanPbPortDstIpCmp);

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHwRemovePortSrcDstIpSVlans                 */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan Classification table */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHwRemovePortSrcDstIpSVlans ()
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableType = 0;
    UINT1               u1TableIndex = 0;

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
    u1TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pSrcDstIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                     apVlanSVlanTable[u1TableIndex]);

    while (pSrcDstIpEntry != NULL)
    {
        if (pSrcDstIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pSrcDstIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcDstIpEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcDstIpEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcDstIpEntry->u4SrcIp;
            VlanSVlanMap.u4DstIp = pSrcDstIpEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME, "Deleting SRC DST IP"
                               "Entry Failed for Port %d \r\n",
                               VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pSrcDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pSrcDstIpEntry, VlanPbPortSrcDstIpCmp);

    }

    return;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortCVidSVlans
*                          
*    Description         : This function adds the Port, C-VLAN,  
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table  to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortCVidSVlans ()
{
    tVlanCVlanSVlanEntry *pCVidEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVidEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                 apVlanSVlanTable[u1TableIndex]);

    while (pCVidEntry != NULL)
    {
        if (pCVidEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVidEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVidEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVidEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVidEntry->SVlanId;
            VlanSVlanMap.u1PepUntag = pCVidEntry->u1PepUntag;
            VlanSVlanMap.u1CepUntag = pCVidEntry->u1CepUntag;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring CVID Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }

            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pCVidEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                    apVlanSVlanTable[u1TableIndex],
                                    pCVidEntry, VlanPbPortCVlanCmp);
    }

    return;

}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortCVlanSrcMacSVlans
*                          
*    Description         : This function adds the Port, C-VLAN, Src MAC 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortCVlanSrcMacSVlans ()
{
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanSrcMacEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                        apVlanSVlanTable[u1TableIndex]);

    while (pCVlanSrcMacEntry != NULL)
    {
        if (pCVlanSrcMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanSrcMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanSrcMacEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanSrcMacEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanSrcMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.SrcMac,
                    &pCVlanSrcMacEntry->SrcMac, VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring CVID SRC MAC "
                               "Entry Failed for Port %d \r\n",
                               VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pCVlanSrcMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanSrcMacEntry, VlanPbPortCVlanSrcMacCmp);
    }

    return;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortCVlanDstMacSVlans
*                          
*    Description         : This function extract the  Port, C-VLAN,Dest MAC 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortCVlanDstMacSVlans ()
{
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanDstMacEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                        apVlanSVlanTable[u1TableIndex]);

    while (pCVlanDstMacEntry != NULL)
    {
        if (pCVlanDstMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDstMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstMacEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanDstMacEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanDstMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.DstMac,
                    &pCVlanDstMacEntry->DstMac, VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring CVID DST MAC "
                               "Entry Failed for Port %d \r\n",
                               VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pCVlanDstMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanDstMacEntry, VlanPbPortCVlanDstMacCmp);

    }

    return;

}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortCVlanDscpSVlans
*                          
*    Description         : This function extract the Port,C-VLAN,  Dscp
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortCVlanDscpSVlans ()
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanDscpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                      apVlanSVlanTable[u1TableIndex]);

    while (pCVlanDscpEntry != NULL)
    {
        if (pCVlanDscpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDscpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDscpEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanDscpEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanDscpEntry->SVlanId;
            VlanSVlanMap.u4Dscp = pCVlanDscpEntry->u4Dscp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring CVID DSCP  "
                               "Entry Failed for Port %d \r\n",
                               VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pCVlanDscpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanDscpEntry, VlanPbPortCVlanDscpCmp);

    }

    return;

}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortCVlanDstIpSVlans
*                          
*    Description         : This function extract the Port, C-VLAN, Dest IP
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortCVlanDstIpSVlans ()
{
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pCVlanDstIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                       apVlanSVlanTable[u1TableIndex]);

    while (pCVlanDstIpEntry != NULL)
    {
        if (pCVlanDstIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pCVlanDstIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstIpEntry->u2Port));
            VlanSVlanMap.CVlanId = pCVlanDstIpEntry->CVlanId;
            VlanSVlanMap.SVlanId = pCVlanDstIpEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pCVlanDstIpEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring CVID DST IP"
                               "Entry Failed for Port %d \r\n",
                               VlanSVlanMap.u2Port);
            }

            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pCVlanDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pCVlanDstIpEntry, VlanPbPortCVlanDstIpCmp);
    }

    return;

}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortSrcMacSVlans
*                          
*    Description         : This function extract the Port, Src MAC 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortSrcMacSVlans ()
{
    tVlanSrcMacSVlanEntry *pSrcMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pSrcMacEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                        apVlanSVlanTable[u1TableIndex]);

    while (pSrcMacEntry != NULL)
    {
        if (pSrcMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pSrcMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcMacEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.SrcMac, &pSrcMacEntry->SrcMac,
                    VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring SRC MAC Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pSrcMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex], pSrcMacEntry,
                           VlanPbPortSrcMacCmp);
    }

    return;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortDstMacSVlans
*                          
*    Description         : This function extract the Port, Dest MAC 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortDstMacSVlans ()
{
    tVlanDstMacSVlanEntry *pDstMacEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pDstMacEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                   apVlanSVlanTable[u1TableIndex]);

    while (pDstMacEntry != NULL)
    {
        if (pDstMacEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pDstMacEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstMacEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstMacEntry->SVlanId;
            MEMCPY (&VlanSVlanMap.DstMac, &pDstMacEntry->DstMac,
                    VLAN_MAC_ADDR_LEN);
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring DST MAC Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pDstMacEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pDstMacEntry, VlanPbPortDstMacCmp);
    }

    return;

}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortSrcIpSVlans
*                          
*    Description         : This function extract the Port, Src IP 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortSrcIpSVlans ()
{
    tVlanSrcIpSVlanEntry *pSrcIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pSrcIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                  apVlanSVlanTable[u1TableIndex]);

    while (pSrcIpEntry != NULL)
    {

        if (pSrcIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pSrcIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcIpEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcIpEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcIpEntry->u4SrcIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring SRC IP Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pSrcIpEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                     apVlanSVlanTable[u1TableIndex],
                                     pSrcIpEntry, VlanPbPortSrcIpCmp);

    }

    return;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortDstIpSVlans
*                          
*    Description         : This function extract the Port, Dest IP 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortDstIpSVlans ()
{
    tVlanDstIpSVlanEntry *pDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pDstIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                  apVlanSVlanTable[u1TableIndex]);

    while (pDstIpEntry != NULL)
    {

        if (pDstIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pDstIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstIpEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstIpEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pDstIpEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring DST IP Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pDstIpEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                     apVlanSVlanTable[u1TableIndex],
                                     pDstIpEntry, VlanPbPortDstIpCmp);

    }

    return;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortDscpSVlans
*                          
*    Description         : This function extract the Port, Dscp 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortDscpSVlans ()
{
    tVlanDscpSVlanEntry *pDscpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_DSCP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pDscpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                 apVlanSVlanTable[u1TableIndex]);

    while (pDscpEntry != NULL)
    {
        if (pDscpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pDscpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDscpEntry->u2Port));
            VlanSVlanMap.SVlanId = pDscpEntry->SVlanId;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSCP_TYPE;
            VlanSVlanMap.u4Dscp = pDscpEntry->u4Dscp;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring DSCP Entry"
                               "Failed for Port %d \r\n", VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }

        pDscpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pDscpEntry, VlanPbPortDscpCmp);
    }

    return;
}

/*****************************************************************************
*                          
*    Function Name       : VlanPbHwAddPortSrcDstIpSVlans
*                          
*    Description         : This function extract the Port, Src-Dest IP 
*                          based S-VLAN classification Entries form the S-VLAN
*                          Classification table and add that entry to the H/W.
*                          
*    Input(s)            : NONE
*                          
*    Output(s)           : NONE
*                          
*    Returns             : VLAN_SUCCESS/VLAN_FAILURE
*                                                         
*****************************************************************************/
VOID
VlanPbHwAddPortSrcDstIpSVlans ()
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    UINT1               u1TableIndex;
    UINT1               u1TableType;

    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u1TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

    u1TableIndex = (UINT1) VlanPbGetSVlanTableIndex ((INT4) u1TableType);

    if (u1TableIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return;
    }

    pSrcDstIpEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                     apVlanSVlanTable[u1TableIndex]);

    while (pSrcDstIpEntry != NULL)
    {
        if (pSrcDstIpEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pSrcDstIpEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                break;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcDstIpEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcDstIpEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcDstIpEntry->u4SrcIp;
            VlanSVlanMap.u4DstIp = pSrcDstIpEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;
            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) != VLAN_SUCCESS)
            {

                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Configuring SRC DST IP"
                               "Entry Failed for Port %d \r\n",
                               VlanSVlanMap.u2Port);
            }
            MEMSET (&VlanSVlanMap, 0, sizeof (VlanSVlanMap));
        }
        pSrcDstIpEntry =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u1TableIndex],
                           pSrcDstIpEntry, VlanPbPortSrcDstIpCmp);

    }
    return;
}

/***************************************************************************/
/* Function Name    : VlanPbNpUpdateCvidEntriesInHw                        */
/* Description      : This function updates CVID registration table entries*/
/*                    in HW based on u1Flag.                               */
/*                                                                         */
/* Input(s)         : u2Port   -  Port number                              */
/*                    SVlanId  -  Service VLAN ID                          */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanPbNpUpdateCvidEntriesInHw (UINT2 u2Port, tVlanId SVlanId, UINT2 u2DstPort,
                               UINT1 u1Flag)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tVlanPbLogicalPortEntry *pPbLogicalPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
#ifdef NPAPI_WANTED    
    tHwVlanCVlanStat     VlanStat;
#endif
    UINT4               u4TblIndex;
    tVlanId             CVid = 0;
    UINT2               u2CurrPort = 0;
    INT4                i4NextPort;
    UINT4               u4NextCVid;
    UINT4               u4IfIndex = 0;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        return VLAN_FAILURE;
    }

    if (VlanPbGetNextCVlanSVlanEntry (u4TblIndex, u2Port,
                                      CVid, &i4NextPort,
                                      &u4NextCVid) == VLAN_FAILURE)
    {
        /* 
         * There is no entry available for this CEP in CVID
         * registration table. Hence return Failure.
         */
        return VLAN_FAILURE;
    }

    do
    {
        pCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                                     (UINT2) i4NextPort,
                                                     (tVlanId) u4NextCVid);
        if (pCVlanSVlanEntry != NULL)
        {
            if ((i4NextPort == (INT4) u2Port) &&
                (pCVlanSVlanEntry->SVlanId == SVlanId)
                && (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE))
            {
                VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

                if (u2DstPort == 0)
                {
                    u2DstPort = pCVlanSVlanEntry->u2Port;

                }

                /* In case of LA scenario, u2DstPort may refer the following port:
                 * a) the port which is going to aggregation. In this case, the port will be visible in VLAN.
                 * b) the port which is coming out of aggregation. In this case, the port will not be visible
                 * in VLAN 
                 * So, to take care of both scenarios, VCM API was used to get the ifIndex. Because, NPAPI has
                 * to be called using IfIndex only */
                if (VcmGetIfIndexFromLocalPort
                    (VLAN_CURR_CONTEXT_ID (), u2DstPort,
                     &u4IfIndex) != VCM_SUCCESS)
                {
                    return VLAN_FAILURE;
                }

                VlanSVlanMap.u2Port = (UINT2) u4IfIndex;
                VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
                VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
                VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
                VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;
                VlanSVlanMap.u1SVlanPriorityType =
                    pCVlanSVlanEntry->u1SVlanPriorityType;
                VlanSVlanMap.u1SVlanPriority =
                    pCVlanSVlanEntry->u1SVlanPriority;

                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;

#ifdef NPAPI_WANTED
                /*customer vlan statistics information */
                VlanStat.CVlanId  =  pCVlanSVlanEntry->CVlanId;
                VlanStat.u2Port   =  (UINT2) u4IfIndex;
                VlanStat.u1Status =  pCVlanSVlanEntry->u1CvlanStatus;
#endif
                pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) u2Port);


                if (u1Flag == VLAN_ADD)
                {
                    if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                           VlanSVlanMap) != VLAN_SUCCESS)
                    {
                        return VLAN_FAILURE;
                    }
                if ((pVlanPbPortEntry != NULL) && 
                    (pVlanPbPortEntry->CVlanId != 0) && 
                    (pVlanPbPortEntry->u1DefCVlanClassify == VLAN_ENABLED))
                    {
                        if (VlanHwSetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                                         (UINT2) u4IfIndex,
                                                       pVlanPbPortEntry->CVlanId) !=
                            VLAN_SUCCESS)
                        {

                            return VLAN_FAILURE;

                        }
                    }
#ifdef NPAPI_WANTED
                    if(pCVlanSVlanEntry->u1CvlanStatus == VLAN_ENABLED)
                    {
                        if (VlanFsMiVlanHwSetCVlanStat(VLAN_CURR_CONTEXT_ID (),VlanStat)!= VLAN_SUCCESS)
                        {
                            return VLAN_FAILURE;
                        }
                    }
#endif
                }
                else if (u1Flag == VLAN_DELETE)
                {
                    if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                              VlanSVlanMap) != VLAN_SUCCESS)
                    {
                        return VLAN_FAILURE;
                    }

                    if (pVlanPbPortEntry != NULL) 
                    {
                        if (VlanHwResetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                                         (UINT2) u4IfIndex)
                            != VLAN_SUCCESS)
                        {
                            return VLAN_FAILURE;
                        }
                    }
#ifdef NPAPI_WANTED
                    if(pCVlanSVlanEntry->u1CvlanStatus == VLAN_ENABLED)
                    {
                        if (VlanFsMiVlanHwSetCVlanStat(VLAN_CURR_CONTEXT_ID (),VlanStat)!= VLAN_SUCCESS)
                        {
                            return VLAN_FAILURE;
                        }
                    }
#endif
                }
            }
        }
        u2CurrPort = (UINT2) i4NextPort;
        CVid = (tVlanId) u4NextCVid;
    }
    while (VlanPbGetNextCVlanSVlanEntry (u4TblIndex, u2CurrPort,
                                         CVid, &i4NextPort,
                                         &u4NextCVid) == VLAN_SUCCESS);

    if ((pPbLogicalPortEntry = VlanPbGetLogicalPortEntry ((UINT2) u4IfIndex,
                                                          (tVlanId) SVlanId)) !=
        NULL)
    {
        if (u1Flag == VLAN_ADD)
        {
            if (VlanHwSetPepPvid (VLAN_CURR_CONTEXT_ID (),
                                  VLAN_GET_PHY_PORT (u4IfIndex),
                                  (tVlanId) SVlanId,
                                  (tVlanId) pPbLogicalPortEntry->Cpvid) !=
                VLAN_SUCCESS)
            {
                return VLAN_FAILURE;
            }
        }
        if (u1Flag == VLAN_DELETE)
        {
            if (VlanHwSetPepPvid (VLAN_CURR_CONTEXT_ID (),
                                  VLAN_GET_PHY_PORT (u4IfIndex),
                                  (tVlanId) SVlanId,
                                  (tVlanId) VLAN_ZERO) != VLAN_SUCCESS)
            {
                return VLAN_FAILURE;
            }
        }

    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNpUpdateCvidAndPepInHw                     */
/*                                                                           */
/*    Description         : This function creates the logical port entry and */
/*                          CVID registration table entries in HW.           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbNpUpdateCvidAndPepInHw (UINT2 u2Port, tVlanId SVlanId, UINT2 u2DstPort,
                              UINT1 u1Flag)
{
    INT4                i4FirstPort = 0;
    INT4                i4NextPort = 0;
    UINT4               u4NextSVid = 0;
    tVlanId             FirstSVid = 0;
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tHwVlanPbPepInfo    HwVlanPbPepInfo;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    UINT1               u1ScanAllEntries = VLAN_FALSE;
    UINT1               u1ScanPortEntries = VLAN_FALSE;
    UINT4               u4IfIndex = 0;

    if ((u2Port == 0) && (SVlanId == 0))
    {
        pVlanPbLogicalPortEntry = VlanPbGetFirstLogicalPortEntry (&i4NextPort,
                                                                  &u4NextSVid);
        if (pVlanPbLogicalPortEntry == NULL)
        {
            return VLAN_FAILURE;
        }
        u1ScanAllEntries = VLAN_TRUE;
    }

    else if ((u2Port != 0) && (SVlanId != 0))
    {
        i4NextPort = (INT4) u2Port;
        u4NextSVid = SVlanId;

        VLAN_MEMSET (&VlanPbLogicalPortEntry, 0,
                     sizeof (tVlanPbLogicalPortEntry));

        VlanPbLogicalPortEntry.u2CepPort = (UINT2) i4NextPort;
        VlanPbLogicalPortEntry.SVlanId = (tVlanId) u4NextSVid;

        pVlanPbLogicalPortEntry
            = RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                         &VlanPbLogicalPortEntry);

        if (pVlanPbLogicalPortEntry == NULL)
        {
            return VLAN_FAILURE;
        }
    }

    else if ((u2Port != 0) && (SVlanId == 0))
    {
        i4FirstPort = (INT4) u2Port;
        FirstSVid = 0;

        pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry (i4FirstPort,
                                                                 FirstSVid,
                                                                 &i4NextPort,
                                                                 &u4NextSVid);
        if (pVlanPbLogicalPortEntry == NULL)
        {
            return VLAN_FAILURE;
        }
        u1ScanPortEntries = VLAN_TRUE;
    }

    else
    {
        VLAN_MEMSET (&VlanPbLogicalPortEntry, 0,
                     sizeof (tVlanPbLogicalPortEntry));

        VlanPbLogicalPortEntry.u2CepPort = (UINT2) i4NextPort;
        VlanPbLogicalPortEntry.SVlanId = (tVlanId) u4NextSVid;

        pVlanPbLogicalPortEntry
            = RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                         &VlanPbLogicalPortEntry);

        if (pVlanPbLogicalPortEntry == NULL)
        {
            return VLAN_FAILURE;
        }
    }

    do
    {
        if ((u1ScanPortEntries == VLAN_TRUE) && (i4NextPort != u2Port))
        {
            return VLAN_FAILURE;
        }

        if (u2DstPort == 0)
        {
            u2DstPort = i4NextPort;
        }

        /* In case of LA scenario, u2DstPort may refer the following port:
         * a) the port which is going to aggregation. In this case, the port will be visible in VLAN.
         * b) the port which is coming out of aggregation. In this case, the port will not be visible
         * in VLAN 
         * So, to take care of both scenarios, VCM API was used to get the ifIndex. Because, NPAPI has
         * to be called using IfIndex only */
        if (VcmGetIfIndexFromLocalPort
            (VLAN_CURR_CONTEXT_ID (), u2DstPort, &u4IfIndex) != VCM_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        if (u1Flag == VLAN_ADD)
        {
            VlanPbNpUpdateCvidEntriesInHw ((UINT2) i4NextPort,
                                           (tVlanId) u4NextSVid, u2DstPort,
                                           VLAN_ADD);

            HwVlanPbPepInfo.i4DefUserPri =
                pVlanPbLogicalPortEntry->u1DefUserPriority;
            HwVlanPbPepInfo.Cpvid = pVlanPbLogicalPortEntry->Cpvid;
            HwVlanPbPepInfo.u1AccptFrameType =
                pVlanPbLogicalPortEntry->u1AccptFrameType;
            HwVlanPbPepInfo.u1IngFiltering =
                pVlanPbLogicalPortEntry->u1IngressFiltering;

            if (VlanHwCreateProviderEdgePort (VLAN_CURR_CONTEXT_ID (),
                                              (UINT2) u4IfIndex,
                                              (tVlanId) u4NextSVid,
                                              HwVlanPbPepInfo) != VLAN_SUCCESS)
            {
                return VLAN_FAILURE;
            }
        }
        else
        {
	    if (VlanPbIsSVlanActive ((tVlanId) u4NextSVid) == VLAN_TRUE)
            {

            	if (VlanHwDelProviderEdgePort (VLAN_CURR_CONTEXT_ID (),
            	                               u4IfIndex,
            	                               (tVlanId) u4NextSVid) !=
            	    VLAN_SUCCESS)
            	{
            	    return VLAN_FAILURE;
            	}
            	VlanPbNpUpdateCvidEntriesInHw ((UINT2) i4NextPort,
            	                               (tVlanId) u4NextSVid, u2DstPort,
            	                               VLAN_DELETE);
	    }
        }
        i4FirstPort = i4NextPort;
        FirstSVid = (tVlanId) u4NextSVid;
    }
    while (((pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry
             (i4FirstPort, FirstSVid, &i4NextPort,
              &u4NextSVid)) != NULL)
           && ((u1ScanPortEntries == VLAN_TRUE)
               || (u1ScanAllEntries == VLAN_TRUE)));
    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanAddVidTransEntryInHw                             */
/*                                                                         */
/* Description      : This function adds a vid translation table entry in  */
/*                    Hardware. It also takes of maintaining the 1-1       */
/*                    birectionality feature in hardware.                  */
/*                                                                         */
/* NOTE               if logic changed for this functions, same should be  */
/*                    updated for  PB MBSM Function                        */
/*                    VlanMbsmAddVidTransEntryToHw to reflect the same Hw  */
/*                    behavior in chasis environment                       */
/*                                                                         */
/* Input(s)         : u2Port - Id of Port on which the entry is getting    */
/*                             programmed.                                 */
/*                    LocalVid - Local vlan id of the entry.               */
/*                    RelayVid - Relay vlan id of the entry.               */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanAddVidTransEntryInHw (UINT2 u2Port, UINT2 u2DstPort,
                          tVlanId LocalVid, tVlanId RelayVid)
{

    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    if (u2DstPort == 0)
    {
        /* In case of no LA, u2DstPort is equal to the u2Port */
        u2DstPort = u2Port;
    }

    /*Any change in logic should be updated in VlanMbsmAddVidTransEntryToHw also */

    if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                        VLAN_GET_IFINDEX (u2DstPort),
                                        LocalVid, RelayVid) != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    /* Do the following if hardware does not take care of
     * maintainig 1-1 birectional feature of vid translation
     * table. */

    /* By default local vid == relay vid in the vid translation
     * table. Default entries are not maintained in the ingress
     * and egress vid translation table RB trees.
     * But these default entries are to programmed in the hardware.
     * When we program (Port = u2Port, LocalVid = LocalVid, 
     * RelayVid = RelayVid, it may be possible for default entry 
     * (Port = u2Port, LocalVid = RelayVid, RelayVid = RelayVid)
     * to be present in the harware. This entry should be modified
     * to achieve the 1-1 birectionality of vid translation table.
     * So if such entry exists (no explicit configuration for 
     * (Port = u2Port, LocalVid = RelayVid) is made) then make it 
     * to (Port = u2Port, LocalVid = RelayVid, RelayVid = 0).
     * Similarly for (Port = u2Port, LocalVid = LocalVid,
     * RelayVid = LocalVid). */

    /* Search the ingress vid translation table for (port = u2Port,
     * LocalVid = RelayVid) */
    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, RelayVid,
                                            OSIX_TRUE, &OutVidTransEntryInfo);
    if ((i4RetVal == L2IWF_FAILURE) ||
        (OutVidTransEntryInfo.u1RowStatus != VLAN_ACTIVE))
    {
        /* 
         * (Port = u2Port, LocalVid = RelayVid, RelayVid = RelayVid) 
         * entry exists in hardware. Change that to
         * (Port = u2Port, LocalVid = RelayVid, RelayVid = 0) 
         */
        if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                            VLAN_GET_IFINDEX (u2DstPort),
                                            RelayVid,
                                            VLAN_DEV_INVALID_VLAN_ID) !=
            VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    /* Note: L2IwfPbGetLocalVidFromRelayVid / L2IwfPbGetRelayVidFromLocalVid
     * cannot be used. Because from the information we get from these functions
     * we cannot find whether (port, vid, vid) is actually present in the tree
     * or not (whether to program hw or not will be decided from this). */

    /* Search the egress vid translation table for (port = u2Port,
     * RelayVid = LocalVid) */
    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, LocalVid,
                                            OSIX_FALSE, &OutVidTransEntryInfo);
    if ((i4RetVal == L2IWF_FAILURE) ||
        (OutVidTransEntryInfo.u1RowStatus != VLAN_ACTIVE))
    {
        /* 
         * (Port = u2Port, LocalVid = LocalVid, RelayVid = LocalVid) 
         * entry exists in hardware. Change that to
         * (Port = u2Port, LocalVid = 0, RelayVid = LocalVid) 
         */
        if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                            VLAN_GET_IFINDEX (u2DstPort),
                                            VLAN_DEV_INVALID_VLAN_ID,
                                            LocalVid) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanDelVidTransEntryInHw                             */
/*                                                                         */
/* Description      : This function deletes a vid translation table entry  */
/*                    from Hardware. It also takes of maintaining the 1-1  */
/*                    birectionality feature in hardware, while deleting   */
/*                    an entry from hardware.                              */
/*                                                                         */
/* Input(s)         : u2Port - Id of Port on which the entry is getting    */
/*                             programmed.                                 */
/*                    LocalVid - Local vlan id of the entry.               */
/*                    RelayVid - Relay vlan id of the entry.               */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanDelVidTransEntryInHw (UINT2 u2Port, UINT2 u2DstPort,
                          tVlanId LocalVid, tVlanId RelayVid)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal;
    UINT4               u4IfIndex = 0;

    if (u2DstPort == 0)
    {
        u2DstPort = u2Port;
    }

    /* In case of LA scenario, u2DstPort may refer the following port:
     * a) the port which is going to aggregation. In this case, the port will be visible in VLAN.
     * b) the port which is coming out of aggregation. In this case, the port will not be visible
     * in VLAN 
     * So, to take care of both scenarios, VCM API was used to get the ifIndex. Because, NPAPI has
     * to be called using IfIndex only */

    if (VcmGetIfIndexFromLocalPort
        (VLAN_CURR_CONTEXT_ID (), u2DstPort, &u4IfIndex) != VCM_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    /* Does the following one equal to programming (u2Port, LocalVid,
     * 0) in hardware. If so then we don't have to have this function
     * at all. */
    if (VlanHwDelSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                        u4IfIndex,
                                        LocalVid, RelayVid) != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    /* Do the following if hardware does not ensure
     * 1-1 birectional feature of vid translation table. */

    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, RelayVid,
                                            OSIX_TRUE, &OutVidTransEntryInfo);
    if ((i4RetVal == L2IWF_SUCCESS) &&
        (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE))
    {
        if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                            u4IfIndex,
                                            VLAN_DEV_INVALID_VLAN_ID,
                                            RelayVid) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
    else
    {
        if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                            u4IfIndex,
                                            RelayVid, RelayVid) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, LocalVid,
                                            OSIX_FALSE, &OutVidTransEntryInfo);
    if ((i4RetVal == L2IWF_SUCCESS) &&
        (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE))
    {
        if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                            u4IfIndex,
                                            LocalVid,
                                            VLAN_DEV_INVALID_VLAN_ID) !=
            VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
    else
    {
        if (VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                            u4IfIndex,
                                            LocalVid, LocalVid) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    return VLAN_SUCCESS;
}
#endif /*  _VLAN_PB_H_ */
