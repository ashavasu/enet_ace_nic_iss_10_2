/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbapi.c,v 1.12 2015/10/01 06:09:55 siva Exp $
 *
 * Description: This file contains the APIs provided by VLAN module   
 *              for provider bridge support to other modules.
 *
 *******************************************************************/
#ifndef _VLNPBAPI_C
#define _VLNPBAPI_C
#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSendCustBpduOnPep                          */
/*                                                                           */
/*    Description         : This function posts an event to VLAN task for    */
/*                          packets handling on PEP ports.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context ID                        */
/*                          pBuf         - Pointer to the incoming frame     */
/*                          u4CepIfIndex - CEP associated with the PEP       */
/*                          SVlanId      - Service VLAN ID                   */
/*                          u4PktSize    - Packet size                       */
/*    Output(s)           : None                                             */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbSendCustBpduOnPep (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4CepIfIndex,
                         tVlanId SVlanId)
{
    tVlanIfMsg         *pVlanIfMsg;
    tVlanQInPkt        *pVlanQInPkt = NULL;
    UINT4               u4RetVal;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_FAILURE;
    }
    if (VlanVcmGetContextInfoFromIfIndex (u4CepIfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_FAILURE;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return VLAN_FAILURE;
    }

    pVlanQInPkt = (tVlanQInPkt *) (VOID *) VLAN_GET_BUF (VLAN_Q_IN_PKT_BUFF,
                                                         sizeof (tVlanQInPkt));
    if (NULL == pVlanQInPkt)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         " Message Allocation failed for Vlan "
                         "Queue - Incoming Packet \r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&(pVlanQInPkt->VlanIfMsg), VLAN_INIT_VAL, sizeof (tVlanIfMsg));
    pVlanIfMsg = &(pVlanQInPkt->VlanIfMsg);

    VLAN_COPY_FROM_BUF (pFrame, &(pVlanIfMsg->DestAddr), 0, ETHERNET_ADDR_SIZE);
    VLAN_COPY_FROM_BUF (pFrame, &(pVlanIfMsg->SrcAddr),
                        ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);

    pVlanIfMsg->u4IfIndex = u4CepIfIndex;
    pVlanIfMsg->SVlanId = SVlanId;
    pVlanIfMsg->u2Length = (UINT2) VLAN_GET_BUF_LEN (pFrame);

    VLAN_GET_TIMESTAMP (&(pVlanIfMsg->u4TimeStamp));

    pVlanQInPkt->pPktInQ = pFrame;

    u4RetVal
        = VLAN_SEND_TO_QUEUE (VLAN_TASK_QUEUE_ID, (UINT1 *) &pVlanQInPkt,
                              OSIX_DEF_MSG_LEN);

    if (u4RetVal == OSIX_SUCCESS)
    {
        VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_MSG_ENQ_EVENT);
        return VLAN_SUCCESS;
    }

    VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
    VLAN_RELEASE_BUF (VLAN_Q_IN_PKT_BUFF, (UINT1 *) pVlanQInPkt);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsCreateIvrValid                           */
/*                                                                           */
/*    Description         : This function checks whether an IVR interface    */
/*                          can be created for the given vlan. This function */
/*                          calls another function that does the exact       */
/*                          funtionality by passing the default context id.  */
/*                                                                           */
/*    Input(s)            : u2IfIvrVlanId - Vlan id over which ivr is being  */
/*                                          created.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if IVR can be created for the  */
/*                                given vlan, otherwise VLAN_FALSE           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsCreateIvrValid (UINT2 u2IfIvrVlanId)
{
    return (VlanMiPbIsCreateIvrValid (VLAN_DEF_CONTEXT_ID, u2IfIvrVlanId));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiPbIsCreateIvrValid                         */
/*                                                                           */
/*    Description         : This function checks whether an IVR interface    */
/*                          can be created for the given vlan in the given   */
/*                          given context.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual context id.                */
/*                          u2IfIvrVlanId - Vlan id over which ivr is being  */
/*                                          created.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if IVR can be created for the  */
/*                                given vlan, otherwise VLAN_FALSE           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiPbIsCreateIvrValid (UINT4 u4ContextId, UINT2 u2IfIvrVlanId)
{
    UINT1              *pu1LocalPortList = NULL;
    tVlanCurrEntry     *pVlanRec = NULL;

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        /* No restriction in customer bridge or in 
         * Q-in-Q bridge. */
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_TRUE;
    }

    pVlanRec = VlanGetVlanEntry (u2IfIvrVlanId);

    if (pVlanRec == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_TRUE;
    }

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }
    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
    VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pu1LocalPortList);

    if (VlanPbIsAnyCustomerPortPresent (pu1LocalPortList,
                                        sizeof (tLocalPortList)) == VLAN_TRUE)
    {
        UtilPlstReleaseLocalPortList (pu1LocalPortList);
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FALSE;
    }

    UtilPlstReleaseLocalPortList (pu1LocalPortList);
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlanInfoOnCustomerPort                   */
/*                                                                           */
/*    Description         : This function returns the tagged Customer VLAN   */
/*                          list and untagged customer VLAN list for the     */
/*                          given Port, Service VLAN in the CVID             */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : pCVlanInfo - Customer VLAN Information           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetCVlanInfoOnCustomerPort (UINT4 u4IfIndex, tVlanId SVlanId,
                                tCVlanInfo * pCVlanInfo)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
        (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        return VLAN_SUCCESS;
    }

    VLAN_LOCK ();

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPort);

    if (pVlanPbPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    pCVlanInfo->u1IsCEPPort = OSIX_FALSE;

    u4TblIndex = VlanPbGetSVlanTableIndex (pVlanPbPortEntry->u1SVlanTableType);

    switch (pVlanPbPortEntry->u1SVlanTableType)
    {
        case VLAN_SVLAN_PORT_CVLAN_TYPE:
        {
            if (VlanL2IwfGetPbPortState (u4IfIndex, SVlanId) ==
                AST_PORT_STATE_FORWARDING)
            {
                VlanGetCVlansFromCVlanSVlanTable (u4TblIndex, u2LocalPort,
                                                  SVlanId, pCVlanInfo);
            }
            pCVlanInfo->u1IsCEPPort = OSIX_TRUE;
        }
            break;
            /* passing pCVlanInfo for CVID Registration table is not required for the
             * below table, as the below table does not have the concept of untagged-cep
             * true/false. By, default all CVLANs of those table will be given in 
             * the tagged cvlan list. CARE will be taken by the IGS or the Higher layer
             * module, that the packet has to be sent tagged/un-tagged */

        case VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE:

            VlanGetCVlansFromCVlanSrcMacSVlanTable (u4TblIndex, u2LocalPort,
                                                    SVlanId,
                                                    pCVlanInfo->
                                                    TaggedCVlanList);
            break;
        case VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE:

            VlanGetCVlansFromCVlanDstMacSVlanTable (u4TblIndex, u2LocalPort,
                                                    SVlanId,
                                                    pCVlanInfo->
                                                    TaggedCVlanList);
            break;
        case VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE:

            VlanGetCVlansFromCVlanDscpSVlanTable (u4TblIndex, u2LocalPort,
                                                  SVlanId,
                                                  pCVlanInfo->TaggedCVlanList);
            break;
        case VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE:

            VlanGetCVlansFromCVlanDstIpSVlanTable (u4TblIndex, u2LocalPort,
                                                   SVlanId,
                                                   pCVlanInfo->TaggedCVlanList);
            break;
        default:
            break;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlanIdList                               */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in the CVID     */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetCVlanIdList (UINT4 u4IfIndex, tVlanId SVlanId, tVlanList CVlanList)
{
    INT4                i4RetVal = VLAN_FAILURE;

    MEMSET (&gVlanPbCVlanInfo, 0, sizeof (tCVlanInfo));

    i4RetVal =
        VlanGetCVlanInfoOnCustomerPort (u4IfIndex, SVlanId, &gVlanPbCVlanInfo);

    if (i4RetVal != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    MEMCPY (CVlanList, gVlanPbCVlanInfo.TaggedCVlanList, sizeof (tVlanList));

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanGetSVlanId                                       */
/*                                                                           */
/* Description        : This function is used to get SVlanId                 */
/*                                                                           */
/* Input (s)          : pFrame     - Pointer to the incoming packet          */
/*                      u2Port     - Port Number                             */
/*                      u4ContextId - Context ID                             */
/*                                                                           */
/* Output (s)         : SVlanId   - VlanId                                   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSVlanId (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port, UINT4 u4ContextId,
                tVlanId * SVlanId)
{
    UINT4               u4TblIndex = 0;
    UINT4               u4TagOffset = 0;
    UINT4               u4CurrContextId = 0;
    tVlanId             CVid = 0;
    UINT1               u1EgrOption = 0;
    tVlanTag            VlanTag;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4ContextId);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (pVlanPbPortEntry == NULL)
    {
        SVlanId = NULL;
        return VLAN_FAILURE;
    }

    /* For port types PNP/CNP_Tagged/PPNP, SVlanID is fetched from the outer vlan tag
       using port number */
    if ((pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT))
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        if (VlanGetVlanInfoFromFrame (u4ContextId, u2Port,
                                      pFrame, &VlanTag,
                                      &u1EgrOption,
                                      &u4TagOffset) == VLAN_SUCCESS)
        {
            *SVlanId = VlanTag.OuterVlanTag.u2VlanId;

        }
        VLAN_LOCK ();
        VlanSelectContext (u4CurrContextId);
    }
    /* For port types CEP/PCEP/PCNP, SVlanId is fetched using CVlanId and port number */
    else if ((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
             (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT) ||
             (pVlanPbPortEntry->u1PbPortType ==
              VLAN_PROP_CUSTOMER_NETWORK_PORT))
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        /* CVlanId is fetched from inner vlan tag using Port number */
        if (VlanGetVlanInfoFromFrame (u4ContextId, u2Port,
                                      pFrame, &VlanTag,
                                      &u1EgrOption,
                                      &u4TagOffset) == VLAN_SUCCESS)
        {
            CVid = VlanTag.InnerVlanTag.u2VlanId;

            VLAN_LOCK ();
            VlanSelectContext (u4CurrContextId);
            pCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                                         u2Port, CVid);

            if (pCVlanSVlanEntry != NULL)
            {
                *SVlanId = pCVlanSVlanEntry->SVlanId;
            }
            VlanReleaseContext ();
            VLAN_UNLOCK ();
        }
        VLAN_LOCK ();
        VlanSelectContext (u4CurrContextId);
    }
    /* For Port type CNP_Portbased, SVlanId is same as the Port Vlan Id */
    else if (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry != NULL)
        {
            *SVlanId = pVlanPortEntry->Pvid;
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     Function Name    : VlanPbClearEvcL2TunnelCounters                     */
/*                                                                           */
/*     Description      : This function Clears the L2Protocol Tunnel Counters*/
/*                        ,which is invoked from MEF.                        */
/*                                                                           */
/*     Input (s)        : tCliHandle  CliHandle - Context in which the CLI   */
/*                                                command is processed       */
/*                        u4ContextId           - Context ID                 */
/*                        tVlanId VlanId        - Vlan ID                    */
/*                                                                           */
/*     Output (s)       : None                                               */
/*                                                                           */
/*     Global Variables Referred  : gpVlanContextInfo                        */
/*                                                                           */
/*     Global Variables Modified  : None                                     */
/*                                                                           */
/*     RETURNS          : VLAN_SUCCESS/VLAN_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbClearEvcL2TunnelCounters (UINT4 u4ContextId, tVlanId VlanId)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanId             NextVlanId = VLAN_NULL_VLAN_ID;

    VLAN_LOCK ();
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }
    if (!VlanId)
    {
        for (VlanId = VLAN_START_VLAN_INDEX ();
             VlanId != VLAN_INVALID_VLAN_INDEX; VlanId = NextVlanId)
        {

            pCurrEntry = VLAN_GET_CURR_ENTRY (VlanId);

            if (pCurrEntry == NULL)
            {
                continue;
            }
            NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId);

            VLAN_TUNNEL_GVRP_PDUS_RX_PER_VLAN (pCurrEntry) = 0;
            VLAN_TUNNEL_GMRP_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
            VLAN_TUNNEL_IGMP_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
            VLAN_TUNNEL_MVRP_PDUS_RX_PER_VLAN (pCurrEntry) = 0;
            VLAN_TUNNEL_MMRP_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
            VLAN_TUNNEL_ECFM_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
        }
    }
    else
    {
        pCurrEntry = VLAN_GET_CURR_ENTRY (VlanId);
        if (pCurrEntry == NULL)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return VLAN_FAILURE;
        }
        VLAN_TUNNEL_GVRP_PDUS_RX_PER_VLAN (pCurrEntry) = 0;
        VLAN_TUNNEL_GMRP_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
        VLAN_TUNNEL_IGMP_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
        VLAN_TUNNEL_MVRP_PDUS_RX_PER_VLAN (pCurrEntry) = 0;
        VLAN_TUNNEL_MMRP_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
        VLAN_TUNNEL_ECFM_PKTS_RX_PER_VLAN (pCurrEntry) = 0;
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return VLAN_SUCCESS;
}

#endif /* _VLNPBAPI_C */
