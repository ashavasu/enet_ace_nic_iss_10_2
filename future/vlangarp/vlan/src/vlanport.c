/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanport.c,v 1.71 2016/10/03 10:34:47 siva Exp $
 *
 * Description: This file contains the external APIs.                    
 *
 *******************************************************************/

#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanVcmGetAliasName                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName (u4ContextId, pu1Alias));
}

/*****************************************************************************/
/* Function Name      : VlanVcmGetContextInfoFromIfIndex                     */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}

/* This is added for Pseudo wire visibility */
/*****************************************************************************/
/* Function Name      : VcmGetIfIndexFromLocalPort                           */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      IfIndex from Context-Id and the Localport number.    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanVcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                UINT4 *pu4IfIndex)
{
    return (VcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                        pu4IfIndex));
}

/*****************************************************************************/
/* Function Name      : VlanVcmGetSystemMode                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : VlanVcmGetSystemModeExt                              */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : VlanVcmIsSwitchExist                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to check whether  */
/*                      the context exist or not.                            */
/*                                                                           */
/* Input(s)           : pu1Alias - Alias Name                                */
/*                                                                           */
/* Output(s)          : pu4VcNum - Context Identifier                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*****************************************************************************/
/* Function Name      : VlanSnoopIsIgmpSnoopingEnabled                       */
/*                                                                           */
/* Description        : This function checks if IGMP snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
VlanSnoopIsIgmpSnoopingEnabled (UINT4 u4ContextId)
{
    return (SnoopIsIgmpSnoopingEnabled (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanSnoopIsMldSnoopingEnabled                        */
/*                                                                           */
/* Description        : This function checks if MLD snooping feature is      */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
VlanSnoopIsMldSnoopingEnabled (UINT4 u4ContextId)
{
    return (SnoopIsMldSnoopingEnabled (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanSnoopMiDelMcastFwdEntryForVlan                   */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to indicate the deletion of a VLAN for a given       */
/*                      Instance.                                            */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Identifier                             */
/*                      u4Context  - Instance for which the operation        */
/*                                   should be done                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : VLAN Module                                          */
/*****************************************************************************/
INT4
VlanSnoopMiDelMcastFwdEntryForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    return (SnoopMiDelMcastFwdEntryForVlan (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanSnoopMiUpdatePortList                            */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to indicate the port list updation for a given       */
/*                      instance.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance for which the operation        */
/*                                   should be done                          */
/*                      VlanId  - Vlan Identifier                            */
/*                    : McastAddress - Multicast mac address, will be NULL   */
/*                                     for vlan updation.                    */
/*                    : AddPortBitmap - Ports to be added to snoop Fwd Table */
/*                    : DelPortBitmap - Ports to be deleted from snoop Fwd   */
/*                                      Table                                */
/*                      u1PortType - To indicate VLAN/Mcast Ports updation   */
/*                                   VLAN_UPDT_VLAN_PORTS /                  */
/*                                   VLAN_UPDT_MCAST_PORTS                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
VlanSnoopMiUpdatePortList (UINT4 u4ContextId, tVlanId VlanId,
                           tMacAddr McastAddr, tSnoopIfPortBmp AddPortBitmap,
                           tSnoopIfPortBmp DelPortBitmap, UINT1 u1PortType)
{
    return (SnoopMiUpdatePortList (u4ContextId, VlanId, McastAddr,
                                   AddPortBitmap, DelPortBitmap, u1PortType));
}

/*****************************************************************************/
/* Function Name      : VlanSnoopUpdateVlanStatus                            */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to post a message to IGS in order to do the foll:    */
/*                      ->flush the informations learned via bridge when     */
/*                        Vlan is Disabled in the system                     */
/*                      ->flush the informations learned via Vlan   when     */
/*                        Vlan is enabled in the system                      */
/*                                                                           */
/* Input(s)           : b1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : VLAN Module                                          */
/*****************************************************************************/
INT4
VlanSnoopUpdateVlanStatus (BOOL1 b1Status)
{
    return (SnoopUpdateVlanStatus (b1Status));
}

/*****************************************************************************/
/* Function Name      : VlanPnacGetPnacEnableStatus                          */
/*                                                                           */
/* Description        : This function gets the PNAC Module Status - i.e      */
/*                      Whether it is enabled or disabled                    */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.u2SystemAuthControl                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if enabled                            */
/*                      PNAC_FAILURE - if disabled                           */
/*****************************************************************************/
INT4
VlanPnacGetPnacEnableStatus (VOID)
{
    return (PnacGetPnacEnableStatus ());
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : VlanMstMiDeleteAllVlans                    */
/*                                                                           */
/*    Description               : This routine is called when VLAN module is */
/*                                shutdown. It unmaps all active Vlans from  */
/*                                the instance to which they are mapped in   */
/*                                the hardware.                              */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/

INT4
VlanMstMiDeleteAllVlans (UINT4 u4ContextId)
{
    return (MstMiDeleteAllVlans (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanAttrRPFillBulkMessage                            */
/*                                                                           */
/* Description        : This function is used to fill the Vlan / Mcast /     */
/*                      Def Group Info in the Message. This funtion is called*/
/*                      whenever a Gvrp/Mvrp and Gmrp/Mmrp is enabled.       */
/*                      This function also post the message if the max no    */
/*                      of message is filled.                                */
/*                                                                           */
/* Input(s)           : ppAttrRpQMsg - Double Pointer to store ppAttrRpQMsg  */
/*                      u1MsgType  - Type of Message - Vlan / Mcast / Ucast  */
/*                      / Def Grp                                            */
/*                      VlanId     - VlanId                                  */
/*                      u2Port     - Port Id                                 */
/*                      Ports      - Added Portlist                          */
/*                      pu1MacAddr - MacAddres                               */
/*                                                                           */
/* Output(s)          : ppAttrRpQMsg - pointer to Allocated Message          */
/*                      pi4Count   - No of Message Filled                    */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanAttrRPFillBulkMessage (UINT4 u4ContextId, VOID **ppAttrRpQMsg,
                           UINT1 u1MsgType, tVlanId VlanId, UINT2 u2Port,
                           tLocalPortList Ports, UINT1 *pu1MacAddr)
{
    tMrpVlanInfo        VlanInfo;
    INT4                i4RetVal = VLAN_FAILURE;

    MEMSET (&VlanInfo, 0, sizeof (tMrpVlanInfo));

    if (VlanGarpIsGarpEnabledInContext (u4ContextId) == GARP_SUCCESS)
    {
        i4RetVal = GarpFillBulkMessage (u4ContextId,
                                        (tGarpQMsg **) ppAttrRpQMsg,
                                        u1MsgType, VlanId, Ports, pu1MacAddr);
        i4RetVal = (i4RetVal == GARP_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE;
    }
    else if (VlanMrpIsMrpStarted (u4ContextId) == OSIX_TRUE)
    {
        VlanInfo.u1MsgType = VLAN_UPDT_OR_POST_MSG;
        VlanInfo.u1SubMsgType = u1MsgType;
        VlanInfo.u4ContextId = u4ContextId;

        if (pu1MacAddr != NULL)
        {
            MEMCPY (VlanInfo.MacAddr, pu1MacAddr, VLAN_MAC_ADDR_LEN);
        }

        VlanInfo.VlanId = VlanId;
        VlanInfo.u4IfIndex = VLAN_GET_PHY_PORT (u2Port);
        VlanInfo.ppMrpQMsg = (tMrpQMsg **) ppAttrRpQMsg;

        if (MEMCMP (Ports, gNullPortList, sizeof (tLocalPortList)) != 0)
        {
            VlanInfo.pAddedPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (VlanInfo.pAddedPorts == NULL)
            {
                return VLAN_FAILURE;
            }

            MEMSET (VlanInfo.pAddedPorts, 0, sizeof (tPortList));

            VlanConvertToIfPortList (Ports, *(VlanInfo.pAddedPorts));
        }

        i4RetVal = MrpApiNotifyVlanInfo (&VlanInfo);

        if (VlanInfo.pAddedPorts != NULL)
        {
            FsUtilReleaseBitList (*(VlanInfo.pAddedPorts));
        }

        i4RetVal = (i4RetVal == OSIX_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE;
    }

    return (i4RetVal);

}

/**************************************************************************/
/*  Function Name    : VlanUpdatePortsToAttrRP                            */
/*  Description      : This function transmits Join messages on the given */
/*                     port, for all the Attributes registered for the    */
/*                     given MapId.                                       */
/*                     This function is called by VLAN whenever static    */
/*                     member ports are added.                            */
/*                     Updation of Map Port list affects only MMRP.       */
/*  Input(s)         : AddPortList - Ports that is added to the           */
/*                              given MAP context.                        */
/*                     u2Gip  - Id of the MAP context for which           */
/*                              new port is added.                        */
/*  Output(s)        : None                                               */
/*  Returns          : None                                               */
/**************************************************************************/
VOID
VlanUpdatePortsToAttrRP (UINT4 u4ContextId, tLocalPortList AddPortList,
                         tLocalPortList DelPortList, UINT2 u2PropCtxtId)
{
    if (VlanGarpIsGarpEnabledInContext (u4ContextId) == GARP_TRUE)
    {
        GarpGipUpdateGipPorts (u4ContextId, AddPortList, DelPortList,
                               u2PropCtxtId);
    }
    else if (VlanMrpIsMrpStarted (u4ContextId) == OSIX_TRUE)
    {
        if ((VlanPbbTeVidIsEspVid (u4ContextId, u2PropCtxtId)) != VLAN_TRUE)
        {
            VlanFillInfoToNotifyMrp (u4ContextId, u2PropCtxtId, NULL,
                                     AddPortList, DelPortList,
                                     VLAN_UPDATE_MAP_PORTS_MSG);
        }
    }
}

/**************************************************************************/
/*  Function Name    : VlanFillInfoToNotifyMrp                            */
/*  Description      : This function posts the VLAN module related        */
/*                     information to MRP module.                         */
/*  Input(s)         : u1MsgType - Identifies the type of information to  */
/*                                 be posted to MRP module                */
/*                     u4ContextId - Context Indentifier                  */
/*                     VlanId - VLAN Identifier                           */
/*                     MacAddr - MAC Address                              */
/*                     AddPortList - Added Port List                      */
/*                     DelPortList - Deleted Port List                    */
/*  Output(s)        : None                                               */
/*  Returns          : VLAN_SUCCESS/ VLAN_FAILURE                         */
/**************************************************************************/

INT4
VlanFillInfoToNotifyMrp (UINT4 u4ContextId, tVlanId VlanId,
                         tMacAddr MacAddr, tLocalPortList AddPortList,
                         tLocalPortList DelPortList, UINT1 u1MsgType)
{
    tMrpVlanInfo        VlanInfo;

    MEMSET (&VlanInfo, 0, sizeof (tMrpVlanInfo));

    VlanInfo.u1MsgType = u1MsgType;
    VlanInfo.u4ContextId = u4ContextId;
    VlanInfo.VlanId = VlanId;

    if (MacAddr != NULL)
    {
        MEMCPY (VlanInfo.MacAddr, MacAddr, VLAN_MAC_ADDR_LEN);
    }

    if (MEMCMP (AddPortList, gNullPortList, sizeof (tLocalPortList)) != 0)
    {
        VlanInfo.pAddedPorts =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (VlanInfo.pAddedPorts == NULL)
        {
            return VLAN_FAILURE;
        }

        MEMSET (VlanInfo.pAddedPorts, 0, sizeof (tPortList));
        VlanConvertToIfPortList (AddPortList, *(VlanInfo.pAddedPorts));
    }

    if (MEMCMP (DelPortList, gNullPortList, sizeof (tLocalPortList)) != 0)
    {
        VlanInfo.pDeletedPorts =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (VlanInfo.pDeletedPorts == NULL)
        {
            if (VlanInfo.pAddedPorts != NULL)
            {
                FsUtilReleaseBitList ((UINT1 *) (VlanInfo.pAddedPorts));
            }

            return VLAN_FAILURE;
        }

        MEMSET (VlanInfo.pDeletedPorts, 0, sizeof (tPortList));
        VlanConvertToIfPortList (DelPortList, *(VlanInfo.pDeletedPorts));
    }

    MrpApiNotifyVlanInfo (&VlanInfo);

    if (VlanInfo.pAddedPorts != NULL)
    {
        FsUtilReleaseBitList (*(VlanInfo.pAddedPorts));
    }

    if (VlanInfo.pDeletedPorts != NULL)
    {
        FsUtilReleaseBitList (*(VlanInfo.pDeletedPorts));
    }

    return VLAN_SUCCESS;
}

/***************************************************************/
/*  Function Name   : VlanGarpIsGarpEnabledInContext           */
/*  Description     : Returns the GARP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         : GARP_ENABLED /GARP_DISABLED              */
/***************************************************************/
INT4
VlanGarpIsGarpEnabledInContext (UINT4 u4ContextId)
{
    return (GarpIsGarpEnabledInContext (u4ContextId));
}

/************************************************************************/
/*  Function Name    : VlanGarpPortOperInd                              */
/*  Description      : Invoked by VLAN whenever Port Oper Status is     */
/*                     changed. If Link Aggregation is enabled, then    */
/*                     Oper Down indication is given for each port      */
/*                     initially. When the Link Aggregation Group is    */
/*                     formed, then Oper Up indication is given.        */
/*                     In the Oper Down state, frames will neither      */
/*                     be transmitted nor be received.                  */
/*  Input(s)         : u2Port - Port for which Oper Status is changed.  */
/*                     u1OperStatus - GARP_OPER_UP/GARP_OPER_DOWN      */
/*  Output(s)        : None                                             */
/*  Returns          : GARP_SUCCESS / GARP_FAILURE                      */
/************************************************************************/
INT4
VlanGarpPortOperInd (UINT2 u2IfIndex, UINT1 u1OperStatus)
{
    return (GarpPortOperInd (u2IfIndex, u1OperStatus));
}

/***************************************************************/
/*  Function Name   : VlanVlanGmrpIsGmrpEnabledInContext           */
/*  Description     : Returns the GMRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         : GMRP_ENABLED /GMRP_DISABLED              */
/***************************************************************/
INT4
VlanGmrpIsGmrpEnabledInContext (UINT4 u4ContextId)
{
    return (GmrpIsGmrpEnabledInContext (u4ContextId));
}

/****************************************************************/
/*  Function Name   :VlanPropagateDefGrpInfoToAttrRP            */
/*  Description     :This function propagates the default Group */
/*                   info to all the ports during initialisation*/
/*  Input(s)        :u1Type - Whether it is VLAN_ALL_GROUPS or  */
/*                   VLAN_UNREG_GROUPS                          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Returns         :None                                       */
/****************************************************************/
void
VlanPropagateDefGrpInfoToAttrRP (UINT4 u4ContextId, UINT1 u1Type,
                                 tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
    UINT1               u1MsgType = 0;

    if (VlanGmrpIsGmrpEnabledInContext (u4ContextId) == GMRP_TRUE)
    {
        GmrpPropagateDefGroupInfo (u4ContextId, u1Type, VlanId,
                                   AddPortList, DelPortList);
    }
    else if (VlanMrpIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
    {
        if (VlanPbbTeVidIsEspVid (u4ContextId, VlanId) != VLAN_TRUE)
        {
            if (u1Type == VLAN_ALL_GROUPS)
            {
                u1MsgType = VLAN_PROP_FWDALL_INFO_MSG;
            }
            else
            {
                u1MsgType = VLAN_PROP_FWDUNREG_INFO_MSG;
            }

            VlanFillInfoToNotifyMrp (u4ContextId, VlanId, NULL,
                                     AddPortList, DelPortList, u1MsgType);
        }
    }
}

/****************************************************************/
/*  Function Name   :VlanPropagateMacInfoToAttrRP               */
/*  Description     :depending on u1action we will add a static */
/*                   Multicast entry or delete an entry         */
/*  Input(s)        :pMacAddr - Pointer to mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Returns         :None                                       */
/****************************************************************/
void
VlanPropagateMacInfoToAttrRP (UINT4 u4ContextId, tMacAddr MacAddr,
                              tVlanId VlanId, tLocalPortList AddPortList,
                              tLocalPortList DelPortList)
{
    if (VlanGmrpIsGmrpEnabledInContext (u4ContextId) == GMRP_TRUE)
    {
        GmrpPropagateMcastInfo (u4ContextId, MacAddr, VlanId, AddPortList,
                                DelPortList);
    }
    else if (VlanMrpIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
    {
        if (VlanPbbTeVidIsEspVid (u4ContextId, VlanId) != VLAN_TRUE)
        {
            VlanFillInfoToNotifyMrp (u4ContextId, VlanId, MacAddr, AddPortList,
                                     DelPortList, VLAN_PROP_MAC_INFO_MSG);
        }
    }
}

/****************************************************************/
/*  Function Name   :VlanAttrRPSetDefGrpForbiddenPorts          */
/*  Description     :depending on u1action we will add a static */
/*                   entry or delete it                         */
/*  Input(s)        :pMacAddr - Pointer to Mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Returns         :None                                       */
/****************************************************************/
void
VlanAttrRPSetDefGrpForbiddPorts (UINT4 u4ContextId, UINT1 u1Type,
                                 tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
    UINT1               u1MsgType = 0;

    if (VlanGmrpIsGmrpEnabledInContext (u4ContextId) == GMRP_TRUE)
    {
        GmrpSetDefGroupForbiddenPorts (u4ContextId, u1Type, VlanId,
                                       AddPortList, DelPortList);
    }
    else if (VlanMrpIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
    {
        if (VlanPbbTeVidIsEspVid (u4ContextId, VlanId) != VLAN_TRUE)
        {
            if (u1Type == VLAN_ALL_GROUPS)
            {
                u1MsgType = VLAN_SET_FWDALL_FORBID_MSG;
            }
            else
            {
                u1MsgType = VLAN_SET_FWDUNREG_FORBID_MSG;
            }

            VlanFillInfoToNotifyMrp (u4ContextId, VlanId, NULL, AddPortList,
                                     DelPortList, u1MsgType);
        }
    }
}

/****************************************************************/
/*  Function Name   :VlanAttrRPSetMcastForbiddPorts           */
/*  Description     :This function set the forbidden ports for a*/
/*                   given multicast entry                      */
/*  Input(s)        :pMacAddr - Pointer to multicast mac address*/
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Returns         :None                                       */
/****************************************************************/
void
VlanAttrRPSetMcastForbiddPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId VlanId,
                                tLocalPortList AddPortList,
                                tLocalPortList DelPortList)
{
    if (VlanGmrpIsGmrpEnabledInContext (u4ContextId) == GMRP_TRUE)
    {
        GmrpSetMcastForbiddenPorts (u4ContextId, MacAddr, VlanId,
                                    AddPortList, DelPortList);
    }
    else if (VlanMrpIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
    {
        if (VlanPbbTeVidIsEspVid (u4ContextId, VlanId) != VLAN_TRUE)
        {
            VlanFillInfoToNotifyMrp (u4ContextId, VlanId, MacAddr, AddPortList,
                                     DelPortList, VLAN_SET_MCAST_FORBID_MSG);
        }
    }
}

/***************************************************************/
/*  Function Name   : VlanGvrpIsGvrpEnabledInContext           */
/*  Description     : Returns the GVRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         : GVRP_ENABLED /GVRP_DISABLED              */
/***************************************************************/

INT4
VlanGvrpIsGvrpEnabledInContext (UINT4 u4ContextId)
{
    return (GvrpIsGvrpEnabledInContext (u4ContextId));
}

/****************************************************************************/
/*  Function Name   :VlanPropagateVlanInfoToAttrRP                          */
/*  Description     :depending on u1action we will add a static             */
/*                   entry or delete it                                     */
/*  Input(s)        :u1AttrType -The attribute type received                */
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*                   u1Action - depending on u1Action we will add a static  */
/*                              vlan  entry or delete it                    */
/*  Output(s)       :  None                                                 */
/*  Returns         : None                                                  */
/****************************************************************************/
void
VlanPropagateVlanInfoToAttrRP (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList AddPortList,
                               tLocalPortList DelPortList)
{
    if (VlanGvrpIsGvrpEnabledInContext (u4ContextId) == GVRP_TRUE)
    {
        GvrpPropagateVlanInfo (u4ContextId, VlanId, AddPortList, DelPortList);
    }
    else if (VlanMrpIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
    {
        if (VlanPbbTeVidIsEspVid (u4ContextId, VlanId) != VLAN_TRUE)
        {
            VlanFillInfoToNotifyMrp (u4ContextId, VlanId, NULL, AddPortList,
                                     DelPortList, VLAN_PROP_VLAN_INFO_MSG);
        }
    }
}

/****************************************************************************/
/*  Function Name   :VlanAttrRPSetVlanForbiddenPorts                        */
/*  Description     :This function sets forbidden ports for a specific      */
/*                    VlanId                                                */
/*  Input(s)        :VlanId - VlanId for which Forbidden ports to be updated*/
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*  Output(s)       :  None                                                 */
/*  Returns         : None                                                  */
/****************************************************************************/
void
VlanAttrRPSetVlanForbiddenPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
    if (VlanGvrpIsGvrpEnabledInContext (u4ContextId) == GVRP_TRUE)
    {
        GvrpSetVlanForbiddenPorts (u4ContextId, VlanId, AddPortList,
                                   DelPortList);
    }
    else if (VlanMrpIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
    {
        if (VlanPbbTeVidIsEspVid (u4ContextId, VlanId) != VLAN_TRUE)
        {
            VlanFillInfoToNotifyMrp (u4ContextId, VlanId, NULL, AddPortList,
                                     DelPortList, VLAN_SET_VLAN_FORBID_MSG);
        }
    }
}

/*****************************************************************************/
/* Function Name      : VlanIssGetAggregatorMac                              */
/*                                                                           */
/* Description        : This function calls the ISS module to get the        */
/*                      Aggregator MAC address.                              */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggregator Index.                       */
/*                                                                           */
/* Output(s)          : AggrMac.                                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac)
{
    return (IssGetAggregatorMac (u2AggIndex, AggrMac));
}

/*****************************************************************************/
/* Function Name      : VlanIssGetContextMacAddress                          */
/*                                                                           */
/* Description        : This function calls the CFA module to get the        */
/*                      system MAC address.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance Id                             */
/*                                                                           */
/* Output(s)          : pSwitchMac - pointer to the switch MAC address       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanIssGetContextMacAddress (UINT4 u4ContextId, tMacAddr pSwitchMac)
{
    IssGetContextMacAddress (u4ContextId, pSwitchMac);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : VlanIssGetRestoreFlagFromNvRam                   */
/*                                                                          */
/*    Description        : This function is invoked to get Restore Flag.    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Flag                                     */
/****************************************************************************/
INT1
VlanIssGetRestoreFlagFromNvRam (VOID)
{
    return (IssGetRestoreFlagFromNvRam ());
}

/*****************************************************************************/
/* Function Name      : VlanAstIsMstStartedInContext                         */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is started   */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Mstp is started in the given context         */
/*                      0 - No, Mstp is NOT started in the given context     */
/*****************************************************************************/
INT4
VlanAstIsMstStartedInContext (UINT4 u4ContextId)
{
    return (AstIsMstStartedInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanAstIsPvrstEnabledInContext                       */
/*                                                                           */
/* Description        : Checks whether PVRST is enabled in this context      */
/*                      or not                                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Pvrst is enabled in the given context        */
/*                      0 - No, Pvrst is NOT enabled in the given context    */
/*****************************************************************************/
INT4
VlanAstIsPvrstEnabledInContext (UINT4 u4ContextId)
{
    return (AstIsPvrstEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanAstIsPvrstStartedInContext                       */
/*                                                                           */
/* Description        : Called by other modules to know if PVRST is started  */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes PVRST is started in the given context        */
/*                      0 - No, PVRST is NOT started in the given context    */
/*****************************************************************************/
INT4
VlanAstIsPvrstStartedInContext (UINT4 u4ContextId)
{
    return (AstIsPvrstStartedInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanBrgGetAgeoutTime                                 */
/*                                                                           */
/* Description        : This function returns the AgeoutTime to be used      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AgeoutTime                                           */
/*****************************************************************************/
UINT4
VlanBrgGetAgeoutTime (VOID)
{
    return (BrgGetAgeoutTime ());
}

/*****************************************************************************/
/* Function Name      : VlanBrgIncrFilterInDiscards                          */
/*                                                                           */
/* Description        : This function increments the Filter Indiscard        */
/*                                                                           */
/* Input(s)           : u4Port - port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : base , tpInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanBrgIncrFilterInDiscards (UINT4 u4Port)
{
    BrgIncrFilterInDiscards (u4Port);
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaCliConfGetIfName
 *
 *    Description         : This function returns Interface name for specified
 *                          interface
 *
 *    Input(s)            : u4IfIndex - Interface Index 
 *
 *    Output(s)           : pi1IfName - Pointer to buffer
 *
 *
 *    Returns            : CLI_SUCCESS if name assigned for pi1IfName
 *                         CLI_FAILURE if name is not assign pi1IfName
 *                         CFA_FAILURE if u4IfIndex is not valid interface
 *
 *****************************************************************************/
INT4
VlanCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : VlanCfaCliGetIfList                                  */
/*                                                                           */
/* Description        : This function calls the functions which convert      */
/*                      the given string to interface list                   */
/*                                                                           */
/* Input(s)           : pi1IfName :Port Number                               */
/*                      pi1IfListStr : Pointer to the string                 */
/*                      pu1IfListArray:Pointer to the string in which the    */
/*                      bits for the port list will be set                   */
/*                      u4IfListArrayLen:Array Length                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanCfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                     UINT4 u4IfListArrayLen)
{
#ifdef CLI_WANTED
    return (CfaCliGetIfList (pi1IfName, pi1IfListStr, pu1IfListArray,
                             u4IfListArrayLen));
#else
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfListStr);
    UNUSED_PARAM (pu1IfListArray);
    UNUSED_PARAM (u4IfListArrayLen);
    return CLI_SUCCESS;
#endif

}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaGetInterfaceBrgPortTypeString
 *
 *    Description         : This function is used to get the Bridge Port type
 *                          name for the given interface index.
 *
 *    Input(s)            : Interface index
 *
 *    Output(s)           : pu1IfBrgPortType - Bridge port type name of the
 *                          given interface index.
 *
 *    Returns             : CLI_SUCCESS or CLI_FAILURE.
 *
 *****************************************************************************/
INT4
VlanCfaGetInterfaceBrgPortTypeString (UINT4 u4IfIndex, UINT1 *pu1IfBrgPortType)
{
#ifdef CLI_WANTED
    CfaGetInterfaceBrgPortTypeString ((INT4)u4IfIndex, pu1IfBrgPortType);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IfBrgPortType);
#endif

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaGetInterfaceBrgPortType
 *
 *    Description         : This function is used to get the Bridge Port type
 *                          for the given interface index.
 *
 *    Input(s)            : Interface index
 *
 *    Output(s)           :i4IfBrgPortType - Bridge port type of the
 *                          given interface index.
 *
 *    Returns             : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
VlanCfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4IfBrgPortType)
{
    if ((CfaGetInterfaceBrgPortType (u4IfIndex, pi4IfBrgPortType))
        == CFA_SUCCESS)
    {
        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaCliGetIfName
 *
 *    Description         : This function is used to get the Interface name 
 *                          for given interface index.
 *
 *    Input(s)            : Interface index
 *
 *
 *    Output(s)           : pi1IfName - Interface name of the given 
 *                          interface index.
 *
 *    Returns             : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/
INT4
VlanCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
#endif
}

/*****************************************************************************
 *
 *    Function Name        : VlanCfaGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
VlanCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************
 *
 *    Function Name        : VlanOfcCreateOpenflowVlan 
 *
 *    Description          : This function creates openflow vlan in OFC
 *
 *    Input(s)             : VlanId
 *
 *    Output(s)            : None.
 *
 *    Returns            :OFC_SUCCESS if vlan creation is successful
 *                        OFC_FAILURE if vlan creation is not successful
 *****************************************************************************/
INT4
VlanOfcCreateOpenflowVlan (UINT4 u4VlanId)
{
#if OPENFLOW_WANTED
    return (OfcVlanCreateOpenflowVlan (u4VlanId));
#else
    UNUSED_PARAM (u4VlanId);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************
 *
 *    Function Name        : VlanOfcDeleteOpenflowVlan 
 *
 *    Description          : This function deletes openflow vlan in OFC
 *
 *    Input(s)             : VlanId
 *
 *    Output(s)            : None.
 *
 *    Returns            :OFC_SUCCESS if vlan creation is successful
 *                        OFC_FAILURE if vlan creation is not successful
 *****************************************************************************/
INT4
VlanOfcDeleteOpenflowVlan (UINT4 u4VlanId)
{
#if OPENFLOW_WANTED
    return (OfcVlanDeleteOpenflowVlan (u4VlanId));
#else
    UNUSED_PARAM (u4VlanId);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : VlanCfaIsThisOpenflowVlan                            */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      Openflow Vlan or not.                                */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
VlanCfaIsThisOpenflowVlan (UINT4 u4VlanId)
{
    return (CfaIsThisOpenflowVlan (u4VlanId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanOfcVlanSetPorts                                        */
/*                                                                           */
/* Description  : Populates the portlist for the openflow vlan               */
/*                                                                           */
/* INPUT        : pu1MemberPorts    - Contains member ports                  */
/*                pu1UntaggedPorts  - Contains untagged ports                */
/*                pu1ForbiddenPorts - Contains forbidden ports               */
/*                u4VlanId          - Vlan ID                                */
/*                u4Flag            - Flag to indicate                       */
/*                                    addition/removal of ports              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanOfcVlanSetPorts (UINT4 u4VlanId, UINT1 *pu1MemberPorts,
                     UINT1 *pu1UntaggedPorts, UINT4 u4Flag)
{
#if OPENFLOW_WANTED
    return (OfcVlanSetPorts
            (u4VlanId, pu1MemberPorts, pu1UntaggedPorts, u4Flag));
#else
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (pu1MemberPorts);
    UNUSED_PARAM (pu1UntaggedPorts);
    UNUSED_PARAM (u4Flag);
    return OSIX_SUCCESS;
#endif
}

/* Added for pseudo wire visibility */
/*****************************************************************************
 *
 *    Function Name        : VlanCfaGetIfType
 *
 *    Description          : This function returns the interface type
 *
 *    Input(s)             : u4IfIndex, *pu1IfType 
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
VlanCfaGetIfType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    return (CfaGetIfaceType (u4IfIndex, pu1IfType));
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaGetIvrStatus
 *
 *    Description         : This function returns the IVR status 
 *
 *    Input(s)            : None. 
 *
 *    Output(s)           : None.
 *
 *    Returns            :  CFA_ENABLED - If IVR is enabled.
 *                          CFA_DISABLED - If IVR is disabled.
 *****************************************************************************/

UINT1
VlanCfaGetIvrStatus (VOID)
{
    return (CfaGetIvrStatus ());
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaGetSysMacAddress
 *
 *    Description         : This function returns the Switch's Base Mac address
 *                          read from NVRAM
 *    Input(s)            : None. 
 *
 *    Output(s)           : pSwitchMac - the Switch Mac address.
 *
 *    Returns            : None 
 *****************************************************************************/

VOID
VlanCfaGetSysMacAddress (tMacAddr SwitchMac)
{
    CfaGetSysMacAddress (SwitchMac);
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaGetVlanId
 *
 *    Description         : This function returns the VLAN ID for a particular
 *                          VLAN interface.
 *
 *    Input(s)            : Interface index. 
 *
 *    Output(s)           : VLAN ID.
 *
 *    Returns            :  VLAN ID correspoding to the VLAN interface index.
 *****************************************************************************/

INT1
VlanCfaGetVlanId (UINT4 u4IfIndex, tVlanIfaceVlanId * pVlanId)
{
    return (CfaGetVlanId (u4IfIndex, pVlanId));
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaGetVlanInterfaceIndexInCxt
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : VLan L2 context Id, VLAN ID.
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
VlanCfaGetVlanInterfaceIndexInCxt (UINT4 u4ContextId, tVlanIfaceVlanId VlanId)
{
    return (CfaGetVlanInterfaceIndexInCxt (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanCfaIsThisInterfaceVlan                           */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      interface Vlan or not.                               */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
VlanCfaIsThisInterfaceVlan (UINT2 VlanId)
{
    return (CfaIsThisInterfaceVlan (VlanId));
}

/*****************************************************************************
 *
 *    Function Name       : VlanCfaIvrNotifyVlanIfOperStatus 
 *
 *    Description         : This function updates the oper status of L3 VLAN 
 *                          interface when there is a change in oper status
 *                          indication from VLAN.
 *
 *    Input(s)            : VlanId and u1InputOperStatus
 *
 *    Output(s)           : None. 
 *
 *    Returns            :    CFA_SUCCESS when the oper status updation is 
 *                            successful. Else CFA_FAILURE.
 *****************************************************************************/

INT1
VlanCfaIvrNotifyVlanIfOperStatus (tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{
    return (CfaIvrNotifyVlanIfOperStatusInCxt
            (VLAN_CURR_CONTEXT_ID (), VlanId, u1OperStatus));
}


/*****************************************************************************
 *
 *    Function Name       : VlanCfaIvrNotifyMclagIfOperStatus
 *
 *    Description         : This function updates the oper status of L3 VLAN
 *                          when Mclag membership changes for L2 VLAN
 *
 *    Input(s)            : VlanId and u1OperStatus
 *
 *    Output(s)           : None.
 *
 *    Returns            :    CFA_SUCCESS when the oper status updation is
 *                            successful. Else CFA_FAILURE.
 *****************************************************************************/

INT1
VlanCfaIvrNotifyMclagIfOperStatus (tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{
    return (CfaIvrNotifyMclagIfOperStatusInCxt
            (VLAN_CURR_CONTEXT_ID (), VlanId, u1OperStatus));
}

/*****************************************************************************
 *
 *    Function Name        : VlanCfaSetIfInfo
 *
 *    Description          : This function updates the interface related params
 *                           assigned to this interface by the external modules.
 *
 *    Input(s)             : i4CfaIfParam, u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
VlanCfaSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaSetIfInfo (i4CfaIfParam, u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfCreateVlan                                  */
/*                                                                           */
/* Description        : This routine is called from VLAN module to           */
/*                      intialise the entry corresponding to the given Vlan  */
/*                      in the L2Iwf common database. It also indicates MSTP */
/*                      about the Vlan creation                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                      VlanId    - VlanId which has been newly created.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfCreateVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfCreateVlan (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfDeleteAllVlans                              */
/*                                                                           */
/* Description        : This routine is called when VLAN module is disabled. */
/*                      It deintialises entries corresponding to all Vlans   */
/*                      in the L2Iwf common database. It also indicates MSTP */
/*                      about each Vlan deletion                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfDeleteAllVlans (UINT4 u4ContextId)
{
    return (L2IwfDeleteAllVlans (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfDeleteVlan                                  */
/*                                                                           */
/* Description        : This routine is called from VLAN module to           */
/*                      deintialise the entry corresponding to the given Vlan*/
/*                      in the L2Iwf common database. It also indicates MSTP */
/*                      about the Vlan deletion                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                      VlanId    - VlanId which has been newly created.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfDeleteVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfDeleteVlan (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetAgeoutInt                                */
/*                                                                           */
/* Description        : This routine is called to get the mac address table  */
/*                      aging time.                                          */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                      Vlan if bridge module is disabled                    */
/*****************************************************************************/

UINT4
VlanL2IwfGetAgeoutInt (VOID)
{
    return (L2IwfGetAgeoutInt ());
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetNextValidPortForContext                  */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context as ordered by the HL Port ID. This is used   */
/*                      by the L2 modules to know the active ports in the    */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                     UINT2 *pu2NextLocalPort,
                                     UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = u4ContextId;
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_VLAN;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPortChannelForPort                       */
/*                                                                           */
/* Description        : This routine returns the Port Channel of which the   */
/*                      given port is a member. It accesses the L2Iwf common */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose         */
/*                                  PortChannel Id to be obtained.           */
/*                                                                           */
/* Output(s)          : u2AggId     - Port Channel Index                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
VlanL2IwfGetPortChannelForPort (UINT4 u4IfIndex, UINT2 *pu2AggId)
{
    return (L2IwfGetPortChannelForPort (u4IfIndex, pu2AggId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPortOperStatus                           */
/*                                                                           */
/* Description        : This routine determines the operational status of a  */
/*                      port indicated to a given module by it's lower layer */
/*                      modules (determines the lower layer port oper status)*/
/*                                                                           */
/* Input(s)           : i4ModuleId - Module for which the lower layer        */
/*                                   oper status is to be determined         */
/*                      u4IfIndex - Global IfIndex of the port whose lower   */
/*                                  layer oper status is to be determined    */
/*                                                                           */
/* Output(s)          : u1OperStatus - Lower layer port oper status for a    */
/*                                     given module                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex,
                            UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u4IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetVlanPortState                            */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Vlan Id and the Port Index. It accesses the L2Iwf    */
/*                      common database to find the Mst Instance to which    */
/*                      the Vlan is mapped and then gets the ports state for */
/*                      that instance.                                       */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the port state is to be    */
/*                               obtained.                                   */
/*                    : u4IfIndex - Global IfIndex of the port whose state   */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
UINT1
VlanL2IwfGetVlanPortState (tVlanId VlanId, UINT4 u4IfIndex)
{
    return (L2IwfGetVlanPortState (VlanId, u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetVlanIdFromFdbId                          */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the VLAN */
/*                      Id from FDB Id                                       */
/*                                                                           */
/* Input(s)           : pBuf - pointer to the outgoing packet                */
/*                      TaggedPortBitmap - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfGetVlanIdFromFdbId (UINT4 u4ContextId, UINT4 u4FdbId,
                             tVlanId * pVlanId)
{
    return (L2IwfMiGetVlanIdFromFdbId (u4ContextId, u4FdbId, pVlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfHandleOutgoingPktOnPort                     */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      PNAC packet.                                         */
/*                                                                           */
/* Input(s)           : pBuf - pointer to the outgoing packet                */
/*                      TaggedPortBitmap - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT2 u4IfIndex, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1Encap)
{
    return (L2IwfHandleOutgoingPktOnPort (pBuf, u4IfIndex, u4PktSize,
                                          u2Protocol, u1Encap));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfHwPbPepCreate                               */
/*                                                                           */
/* Description        : This function is used to indicate Ast about          */
/*                      provider edge port is created in Hw.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      SVlanId    - Service vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
VlanL2IwfHwPbPepCreate (UINT4 u4IfIndex, tVlanId SVlanId)
{
    return (L2IwfHwPbPepCreate (u4IfIndex, SVlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfIncrFilterInDiscards                        */
/*                                                                           */
/* Description        : This routine is called to register the standard      */
/*                      bridge mib when bridge module is disabled            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanL2IwfIncrFilterInDiscards (UINT2 u2Port)
{
    L2IwfIncrFilterInDiscards (u2Port);
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfMiGetVlanInstMapping                        */
/*                                                                           */
/* Description        : This routine returns the Instance to which the given */
/*                      Vlan Id is mapped. It accesses the L2Iwf common      */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
VlanL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanInstMapping (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbSetDefBrgPortTypeInCfa                    */
/*                                                                           */
/* Description        : This function set the default bridge port type for   */
/*                      the given port based on the bridge mode. By using    */
/*                      the u1BrgModePortType argument we can force this     */
/*                      function to set the bridge port type as customerPort */
/*                      irrespective of the bridge mode.                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port IfIndex                             */
/*                      u4BridgeMode - Bridge Mode                           */
/*                      u1BrgModePortType - Whether to set the bridge port   */
/*                                          type based on bridge mode or     */
/*                                          not.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbSetDefBrgPortTypeInCfa (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT1 u1BrgModePortType, UINT1 u1IsCnpCheck)
{
    return (L2IwfPbSetDefBrgPortTypeInCfa (u4ContextId, u4IfIndex,
                                           u1BrgModePortType, u1IsCnpCheck));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetBridgeMode                               */
/*                                                                           */
/* Description        : This function sets the Bridge mode in L2IWF.         */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      i4BridgeMode - Bridge mode to be set in L2IWF        */
/*                      This takes one of the following values               */
/*                      CustomerBridge/ProviderEdgeBridge/ProviderCoreBridge */
/*                      ProviderBridge                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfSetBridgeMode (UINT4 u4ContextId, UINT4 u4BridgeMode)
{
    return (L2IwfSetBridgeMode (u4ContextId, u4BridgeMode));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetVlanPortPvid                             */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port VLAN Id in the L2Iwf common database.           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual Switch ID                    */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      port type is to be updated.          */
/*                      VlanId        - Port VLAN Id.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetVlanPortPvid (UINT4 u4ContextId, UINT2 u2LocalPortId,
                          tVlanId VlanId)
{
    return (L2IwfSetVlanPortPvid (u4ContextId, u2LocalPortId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetNextActiveVlan                           */
/*                                                                           */
/* Description        : This routine is called from VLAN to get the next     */
/*                      Active VLAN Id from the L2Iwf common database.       */
/*                                                                           */
/* Input(s)           : u2VlanId       - Vlan Index whose next vlan is to be */
/*                                       determined.                         */
/* Output(s)          : *pu2NextVlanId - Pointer to the next active VlanId   */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfGetNextActiveVlan (UINT2 u2VlanId, UINT2 *pu2NextVlanId)
{
    return (L2IwfGetNextActiveVlan (u2VlanId, pu2NextVlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetVlanPortType                             */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port type in the L2Iwf common                        */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      port type is to be updated.          */
/*                      u1PortType  - Port Type could be one of Hybrid,      */
/*                                    Access or Trunk.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetVlanPortType (UINT4 u4ContextId, UINT2 u2LocalPortId,
                          UINT1 u1PortType)
{
    return (L2IwfSetVlanPortType (u4ContextId, u2LocalPortId, u1PortType));
}

#ifdef MPLS_WANTED
/* ************************************************************************* *
 *  Function Name   : VlanMplsProcessL2Pkt                                   * 
 *  Description     : Called from CFA-VLAN task to post packet to MPLS if    *
 *                    VPLS info is configured for the particualr vlan.       *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4VfId - PW index if the u1PktType is UCAST            *
 *                             VPLS instance index if the u1PktType is BCAST *
 *                    u1PktType - CFA_LINK_UCAST / CFA_LINK_BCAST            *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */
INT4
VlanMplsProcessL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT4 u4VfId, UINT1 u1PktType)
{
    return (MplsProcessL2Pkt (pBuf, u4IfIndex, u4VfId, u1PktType));
}
#ifdef HVPLS_WANTED
/* ************************************************************************* *
 *  Function Name   : VlanMplsProcessMplsPkt                                 *
 *  Description     : called to trigger MPLS_TO_MPLS handling.               *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4VfId - PW index if the u1PktType is UCAST            *
 *                             VPLS instance index if the u1PktType is BCAST *
 *                    u1PktType - CFA_LINK_UCAST / CFA_LINK_BCAST            *
 *                    u4PwVcIndex - Incoming Pw Index                                                       *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * * * ************************************************************************* */

    INT4
VlanMplsProcessMplsPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
        UINT4 u4VfId, UINT1 u1PktType, UINT4 u4PwVcIndex)
{
    return (MplsProcessMplsPkt (pBuf, u4IfIndex, u4VfId, u1PktType, u4PwVcIndex));
}
#endif
#endif

#ifdef L2RED_WANTED
#if defined (IGS_WANTED) || defined (MLDS_WANTED)    /* SNOOP_APPROACH */
/*****************************************************************************/
/* Function Name      : VlanSnoopRedRcvPktFromRm                             */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to SNOOP*/
/*                      task. For other events this function enqueues the    */
/*                      given data to the queue and sends the corresponding  */
/*                      event to SNOOP task.                                 */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event is sent then return     */
/*                      SNOOP_SUCCESS otherwise return SNOOP_FAILURE         */
/*****************************************************************************/
INT4
VlanSnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (SnoopRedRcvPktFromRm (u1Event, pData, u2DataLen));
}

#endif

#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : VlanLldpRedRcvPktFromRm                              */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to LLDP */
/*                      task.                                                */
/*                                                                           */
/* Input(s)           : u1Event   - Event to be sent to LLDP                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
INT4
VlanLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif

#ifdef ECFM_WANTED
/*****************************************************************************/
/* Function Name      : VlanEcfmRedRcvPktFromRm                              */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to      */
/*                      ECFM's CC task. For other events this function       */
/*                      enqueues the given data to the queue and sends the   */
/*                      corresponding event to ECFM's CC task.               */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event is sent then return     */
/*                      ECFM_SUCCESS otherwise return ECFM_FAILURE           */
/*****************************************************************************/
INT4
VlanEcfmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (EcfmRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif

#ifdef ELMI_WANTED
/*****************************************************************************/
/* Function Name      : VlanElmRedRcvPktFromRm                               */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the ELMI*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RM_FAILURE/RM_SUCCESS                                */
/*****************************************************************************/

INT4
VlanElmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (ElmRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif

/*****************************************************************************/
/* Function Name      : VlanGarpRedSendMsg                                   */
/*                                                                           */
/* Description        : This function sends GO_ACTIVE and GO_STANDBY msgs    */
/*                      to GARP module. This fn. will be invoked by VLAN.    */
/*                                                                           */
/* Input(s)           : u4MessageType                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanGarpRedSendMsg (UINT4 u4MessageType)
{
    GarpRedSendMsg (u4MessageType);
}

/*****************************************************************************/
/* Function Name      : VlanRmEnqMsgToRmFromAppl                             */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : VlanRmEnqChkSumMsgToRm                               */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return VLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRmGetNodeState                                   */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : VlanRmGetStandbyNodeCount                            */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : VlanRmGetStaticConfigStatus                          */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : VlanRmHandleProtocolEvent                         */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
VlanRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : VlanRmRegisterProtocols                              */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : VlanRmDeRegisterProtocols                            */
/*                                                                           */
/* Description        : This function deregisters VLAN with RM.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_VLANGARP_APP_ID));
}

/*****************************************************************************/
/* Function Name      : VlanRmReleaseMemoryForMsg                            */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : VlanRmSetBulkUpdatesStatus                           */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}

/*****************************************************************************/
/* Function Name      : VlanPbbVlanAuditStatusInd                            */
/*                                                                           */
/* Description        : This routine is called to when VLAN Audit is over    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanPbbVlanAuditStatusInd (tPbbVlanAuditStatus * pPbbVlanStatus)
{
    return (PbbVlanAuditStatusInd (pPbbVlanStatus));
}
#endif

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbCreateProviderEdgePort                    */
/*                                                                           */
/* Description        : This function is used to create Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is created in VLAN.               */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
VlanL2IwfPbCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId)
{
    return (L2IwfPbCreateProviderEdgePort (u4IfIndex, SVlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbPepDeleteIndication                       */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is deleted from VLAN.             */
/*                      This function also indicated PEP deletion to ASTP.   */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      SVlanId    - Service Vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
VlanL2IwfPbPepDeleteIndication (UINT4 u4IfIndex, tVlanId SVlanId)
{
    return (L2IwfPbPepDeleteIndication (u4IfIndex, SVlanId));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : VlanL2IwfPbPepHandleInCustBpdu             */
/*                                                                           */
/*    Description               : This API is used to post the customer BPDUs*/
/*                                received on PEPs to AST task.              */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u2Port - Incoming port.                    */
/*                                SVlanId - Service VLAN ID                  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2IwfPbPepHandleInCustBpdu (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex,
                                tVlanId SVlanId)
{
    L2IwfPbPepHandleInCustBpdu (pBuf, u4IfIndex, SVlanId);
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbUpdatePepOperStatus                       */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is deleted from VLAN.             */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanL2IwfPbUpdatePepOperStatus (UINT4 u4IfIndex, tVlanId SVlanId,
                                UINT1 u1OperStatus)
{
    L2IwfPbUpdatePepOperStatus (u4IfIndex, SVlanId, u1OperStatus);
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPortOperPointToPointStatus               */
/*                                                                           */
/* Description        : This function is used to get port oper point to point*/
/*                      status from L2Iwf. It will be called by VLAN Module. */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      pbOperPointToPoint - Port oper point to point status */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
VlanL2IwfGetPortOperPointToPointStatus (UINT4 u4IfIndex,
                                        BOOL1 * pbOperPointToPoint)
{
    L2IwfGetPortOperPointToPointStatus (u4IfIndex, pbOperPointToPoint);
}

/*****************************************************************************/
/* Function Name      : VlanLldpApiNotifyProtoVlanStatus                     */
/*                                                                           */
/* Description        : This function posts a message to LLDP task's message */
/*                      queue to notify  the change in the status of Protocol*/
/*                      based Vlan Classification on the port. If the status */
/*                      is changed globally then this function will be called*/
/*                      with u4IfIndex as zero.                              */
/*                                                                           */
/* Input(s)           : u2Port            - Local Port                       */
/*                      u1ProtVlanStatus  - Status of protocol based vlan    */
/*                                          classification (Enabled /        */
/*                                          Disabled)                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VlanLldpApiNotifyProtoVlanStatus (UINT2 u2Port, UINT1 u1ProtVlanStatus)
{
    LldpApiNotifyProtoVlanStatus (VLAN_GET_IFINDEX (u2Port), u1ProtVlanStatus);
}

/*****************************************************************************/
/* Function Name      : VlanLldpApiNotifyProtoVlanId                         */
/*                                                                           */
/* Description        : This function posts a message to LLDP task's message */
/*                      queue to notify  the change in the Protocol Vlan Id  */
/*                      mapped to the port.                                  */
/*                                                                           */
/* Input(s)           : u2Port       - Local Port                            */
/*                      u2VlanId     - Protocol Vlan Id which need to be     */
/*                                     added or deleted to/from LLDP         */
/*                                     data structure                        */
/*                      u1ActionFlag - Operation to be performed             */
/*                                     (Add/ Delete/ Updation)               */
/*                      u2OldVlanId  - Non-zero only if u1ActionFlag =       */
/*                                     Updation i.e if for the same protocol */
/*                                     group the protocol vlan id is replaced*/
/*                                     with another protocol vlan id. In this*/
/*                                     case the node containing the          */
/*                                     u2OldVlanId will be deleted and a new */
/*                                     node with u2VlanId will be added      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VlanLldpApiNotifyProtoVlanId (UINT2 u2Port, UINT2 u2VlanId, UINT1 u1ActionFlag,
                              UINT2 u2OldVlanId)
{
    LldpApiNotifyProtoVlanId (VLAN_GET_IFINDEX (u2Port), u2VlanId,
                              u1ActionFlag, u2OldVlanId);
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPortOperEdgeStatus                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the oper */
/*                      edge status of the port.                             */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pbOperEdge - OperEdge Status                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfGetPortOperEdgeStatus (UINT2 u2IfIndex, BOOL1 * pbOperEdge)
{
    return (L2IwfGetPortOperEdgeStatus (u2IfIndex, pbOperEdge));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetVlanFloodingStatus                       */
/*                                                                           */
/* Description        : This routine is called to Disable/Enable the         */
/*                      flooding for the given vlan.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId - Vlan Index                                */
/*                      Status - Flag to Disable/Enable the flooding of      */
/*                      the given vlan                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId,
                                UINT1 u1FloodStatus)
{
    return (L2IwfSetVlanFloodingStatus (u4ContextId, u2VlanId, u1FloodStatus));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetVlanFloodingStatus                       */
/*                                                                           */
/* Description        : This routine is called to get the vlan flooding      */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId - Vlan Index                                */
/*                      u1VlanFloodStatus - VLAN_FLOODING_ENABLED            */
/*                                          VLAN_FLOODING_DISABLED           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfGetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId,
                                UINT1 *pu1VlanFloodStatus)
{
    return (L2IwfGetVlanFloodingStatus (u4ContextId, u2VlanId,
                                        pu1VlanFloodStatus));
}

INT4
VlanPbbGetIsidOfVip (UINT4 *pu4Isid, UINT4 u4IfIndex, UINT4 u4ContextId)
{
#ifdef PBB_WANTED
    return (PbbGetIsidOfVip (pu4Isid, u4IfIndex, u4ContextId));
#else
    UNUSED_PARAM (pu4Isid);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4ContextId);
    return L2IWF_SUCCESS;
#endif

}

#ifdef CLI_WANTED
INT4
VlanPbbCliGetVipIndexOfIsid (UINT4 u4ContextId, UINT4 u4Isid, UINT4 *pu4IfIndex)
{
    return (PbbCliGetVipIndexOfIsid (u4ContextId, u4Isid, pu4IfIndex));
}
#endif
/***************************************************************/
/*  Function Name   : VlanMrpIsMrpStarted                      */
/*  Description     : Returns the MRP Module Status            */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         : MRP_ENABLED / MRP_DISABLED               */
/***************************************************************/
INT4
VlanMrpIsMrpStarted (UINT4 u4ContextId)
{
    return (MrpApiIsMrpStarted (u4ContextId));
}

/***************************************************************/
/*  Function Name   : VlanMrpIsMvrpEnabled                     */
/*  Description     : Returns the MVRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         :OSIX_TRUE/MRP_FALSE                       */
/***************************************************************/

INT4
VlanMrpIsMvrpEnabled (UINT4 u4ContextId)
{
    return (MrpApiIsMvrpEnabled (u4ContextId));
}

/***************************************************************/
/*  Function Name   : VlanMrpIsMmrpEnabled                     */
/*  Description     : Returns the MMRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         :OSIX_TRUE/MRP_FALSE                       */
/***************************************************************/
INT4
VlanMrpIsMmrpEnabled (UINT4 u4ContextId)
{
    return (MrpApiIsMmrpEnabled (u4ContextId));
}

/***************************************************************/
/*  Function Name   : VlanPbbTeVidIsEspVid                     */
/*  Description     : It returns the given VLAN is ESP VLAN    */
/*                    or not.                                  */
/*  Input(s)        : u4ContextId - Context Identifier         */
/*                    VlanId      - Vlan Identifier            */
/*  Output(s)       :                                          */
/*  Returns         : ENABLED /DISABLED                        */
/***************************************************************/
INT4
VlanPbbTeVidIsEspVid (UINT4 u4ContextId, tVlanId VlanId)
{
    if (PbbTeVidIsEspVid (u4ContextId, VlanId) == OSIX_TRUE)
    {
        return VLAN_TRUE;
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispIsPortVlanMappingValid                    */
/*                                                                           */
/* Description        : This API will used to verify whether the Port-VLAN-  */
/*                      Context Mapping Table configuration is allowable.    */
/*                      The Local ports will be converted to the underlying  */
/*                      physical or port channel interface before verifying  */
/*                      the entry in the table.                              */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       tLocalPortList - AddedPorts                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispIsPortVlanMappingValid (UINT4 u4ContextId, tVlanId VlanId,
                                   tLocalPortList AddedPorts)
{
    return (VcmSispIsPortVlanMappingValid (u4ContextId, VlanId, AddedPorts));
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispUpdatePortVlanMapping                     */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table with the input parameters. The Local ports     */
/*                      will be converted to the underlying physical or port */
/*                      channel interface before updating the entry in the   */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       tLocalPortList -  AddedPorts,                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispUpdatePortVlanMapping (UINT4 u4ContextId, tVlanId VlanId,
                                  tLocalPortList AddedPorts,
                                  tLocalPortList DeletedPorts)
{
    return (VcmSispUpdatePortVlanMapping (u4ContextId, VlanId,
                                          AddedPorts, DeletedPorts));
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispUpdatePortVlanMappingOnPort               */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table for the given port.                            */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       u4IfIndex      - Physical Port Identifier           */
/*                       u1Status       - Add or Delete flag                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispUpdatePortVlanMappingOnPort (UINT4 u4ContextId, tVlanId VlanId,
                                        UINT4 u4IfIndex, UINT1 u1Status)
{
    return (VcmSispUpdatePortVlanMappingOnPort (u4ContextId, VlanId,
                                                u4IfIndex, u1Status));
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispUpdatePortVlanMappingInHw                 */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table with the input parameters. The Local ports     */
/*                      will be converted to the underlying physical or port */
/*                      channel interface before updating the entry in the   */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       tLocalPortList - AddedPorts,                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispUpdatePortVlanMappingInHw (UINT4 u4ContextId, tVlanId VlanId,
                                      tLocalPortList PortList, UINT1 u1Action)
{
    return (VcmSispUpdatePortVlanMappingInHw (u4ContextId, VlanId,
                                              PortList, u1Action));
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispGetPhysicalPortOfLogicalport              */
/*                                                                           */
/* Description        : This function will provide the physical IfIndex      */
/*                      of the input Logical IfIndex.                        */
/*                                                                           */
/* Input(s)           : u4ILogicalfIndex - Logical Port identifier.          */
/*                                                                           */
/* Output(s)          : pu4PhysicalIndex - Physical Index of the logical port*/
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispGetPhysicalPortOfSispPort (UINT4 u4LogicalIfIndex,
                                      UINT4 *pu4PhysicalIndex)
{
    return (VcmSispGetPhysicalPortOfSispPort (u4LogicalIfIndex,
                                              pu4PhysicalIndex));
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispGetSispPortOfPhysicalPortInCtx            */
/*                                                                           */
/* Description        : This function used to get the SISP port information  */
/*                      of a particual physical port. This API will return   */
/*                      the logical port  IfIndex and LocalPort information  */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                      u4ContextId    - Secondary context ID.               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispGetSispPortOfPhysicalPortInCtx (UINT4 u4PhyIfIndex,
                                           UINT4 u4ContextId,
                                           UINT4 *pu4SispPort,
                                           UINT2 *pu2LocalPort)
{
    return (VcmSispGetSispPortOfPhysicalPortInCtx (u4PhyIfIndex, u4ContextId,
                                                   pu4SispPort, pu2LocalPort));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetSispPortCtrlStatus                       */
/*                                                                           */
/* Description        : This function used to get whether SISP is enabled    */
/*                      on the particular physical or port channel port.     */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Physical or port channel port Id.      */
/*                      *pu1Status  - SISP Enabled/Disabled status           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
VlanL2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    return (L2IwfGetSispPortCtrlStatus (u4IfIndex, pu1Status));
}

/*****************************************************************************/
/* Function Name      : VlanVcmSispGetSispPortInfoOfPhysicalPort             */
/*                                                                           */
/* Description        : This function used to get the SISP port information  */
/*                      of a particual physical port. This API will return   */
/*                      the logical ports in IfIndex or LocalPort based on   */
/*                      the u1RetLocalPorts variable. paSispPort array will  */
/*                      be typecasted to UINT4 if the return value is to be  */
/*                      in IfIndex; will be typecasted to UINT2, if the      */
/*                      return value is to be in LocalPort.                  */
/*                                                                           */
/*                      This API can be used to get the number of SISP ports */
/*                      existing for the particular physical port. When      */
/*                      paSispPorts is NULL, SISP port count alone will be   */
/*                      returned.                                            */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
                                           UINT1 u1RetLocalPorts,
                                           VOID *paSispPorts,
                                           UINT4 *pu4PortCount)
{
    return (VcmSispGetSispPortsInfoOfPhysicalPort (u4PhyIfIndex,
                                                   u1RetLocalPorts,
                                                   paSispPorts, pu4PortCount));
}

/*****************************************************************************/
/* Function Name      : VlanMstSispValidateInstRestriction                   */
/*                                                                           */
/* Description        : This MSTP API will be invoked from VLAN,whenever     */
/*                      interfaces are added to VLAN. This API will check    */
/*                      against the restriction that the same port cannot be */
/*                      present in same instance in two different contexts.  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u4PortNum   - Port number                            */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMstSispValidateInstRestriction (UINT4 u4ContextId, UINT4 u4PortNum,
                                    tVlanId VlanId)
{
    return (MstSispValidateInstRestriction (u4ContextId, u4PortNum, VlanId));
}
/*****************************************************************************/
/* Function Name      : VlanValidateForL3Subinterface                        */
/*                                                                           */
/* Description        : This API will Validate if the given physical port    */
/*                      is associated with any l3subinterface                */                                
/*                      port.                                                */
/*                                                                           */                                
/* Input(s)           : u4PortNum       - Physical Port Identifier           */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanValidateForL3Subinterface (UINT4 u4PortNum, UINT2 u2L2VlanId)
{
    if (CfaVlanValidateL3SubIf (u4PortNum,u2L2VlanId) == 
                            CFA_SUCCESS)
    {
        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}
/*****************************************************************************/
/* Function Name      : VlanCfaIsL3SubIfVlanId                               */
/*                                                                           */
/* Description        : This API will Validate if the given VLAN ID is       */
/*                      is associated with any l3subinterface port.          */
/* Input(s)           : u4VlanId       - VLAN ID                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanCfaIsL3SubIfVlanId (UINT4 u4VlanId)
{
    if (CfaIsL3SubIfVLAN ((UINT2)u4VlanId) == CFA_TRUE)
    {
        return VLAN_SUCCESS;
    }
    return VLAN_FAILURE;
}
/*****************************************************************************/
/* Function Name      : VlanVcmSispDeleteAllPortVlanMapEntriesForPort        */
/*                                                                           */
/* Description        : This API will delete all the Port-VLAN-Context Map   */
/*                      table for the particular Physical Port or logical    */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           :  u4IfIndex      - Physical Port Identifier           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VlanVcmSispDeleteAllPortVlanMapEntriesForPort (UINT4 u4IfIndex)
{
    return (VcmSispDeletePortVlanMapEntriesForPort (u4IfIndex));
}

/*****************************************************************************
 * Function Name      : VlanEcfmEtherTypeChngInd                              *
 *                                                                            *
 * Description        : This function is called by the VLAN Module            *
 *                      when ether Type for VLAN is changed to give an        *
 *                      indication to ECFM Module                             * 
 *                                                                            *
 * Input(s)           : u4ContextId - Current Context Id                      *
 *                      u4IfIndex   - IfIndex of the port for which Ether Type*
 *                                    updated.                                *
 *                      u1EtherType  - Ingress of Egress Ether Type           * 
 *                      u2EtherTypeValue - Ether Type Value                   * 
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                           *
 *****************************************************************************/
INT4
VlanEcfmEtherTypeChngInd (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1EtherType,
                          UINT2 u2EtherTypeValue)
{
#ifdef ECFM_WANTED
    if (EcfmVlanEtherTypeChngInd (u4ContextId, u4IfIndex, u1EtherType,
                                  u2EtherTypeValue) != ECFM_SUCCESS)
    {
        return VLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1EtherType);
    UNUSED_PARAM (u2EtherTypeValue);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAttrRPPostBulkCfgMessage                     */
/*                                                                           */
/*    Description         : Posts the message to Garp/Mrp Config Q.          */
/*                                                                           */
/*    Input(s)            : pAttrRPQMsg -  Pointer to GarpQMsg / MrpQMsg     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanAttrRPPostBulkCfgMessage (UINT4 u4ContextId, VOID *pAttrRpQMsg)
{
    INT4                i4RetVal = VLAN_FAILURE;

    if (VlanGarpIsGarpEnabledInContext (u4ContextId) == GARP_TRUE)
    {
        i4RetVal = GarpPostBulkCfgMessage (pAttrRpQMsg);
        i4RetVal = (i4RetVal == GARP_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE;
    }
    else if (VlanMrpIsMrpStarted (u4ContextId) == OSIX_TRUE)
    {
        i4RetVal = MrpApiPostBulkCfgMessage (pAttrRpQMsg);
        i4RetVal = (i4RetVal == OSIX_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanQoSRegenerateVlanPriority                    */
/*                                                                           */
/*    Description         : This function Regenerates the user priority      */
/*                          for the u4IfIndex on which the frame is received.*/
/*                                                                           */
/*    Input(s)            : u2Port - Port on which the frame was received.   */
/*                          u1Priority - Priority associated with the frame. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Regenerated user priority value.                  */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanQoSRegenerateVlanPriority (UINT2 u2InPort, UINT2 u2VlanId, UINT1 u1Priority)
{
    UINT1               u1TagPri;
    tVlanPortEntry     *pPortEntry = NULL;

    pPortEntry = VLAN_GET_PORT_ENTRY (u2InPort);

    u1TagPri = u1Priority;

    if ((pPortEntry->u1IfType != VLAN_ETHERNET_INTERFACE_TYPE) &&
        (pPortEntry->u1IfType != VLAN_LAGG_INTERFACE_TYPE))
    {

        u1TagPri =
            QoSRegenerateVlanPriority (pPortEntry->u4IfIndex, u2VlanId,
                                       u1Priority);
    }

    return u1TagPri;
}

/*****************************************************************************/
/* Function Name      : VlanGetSrcMacAddress                                 */
/*                                                                           */
/* Description        : This function is called to get the source MAC        */
/*                      address of the outgoing packat.                      */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Physical Port Index                    */
/*                      VlanId      - Vlan Identifier of the packet.         */
/*                                                                           */
/* Output(s)          : SrcMacAddr     -  Src MAC address                    */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSrcMacAddress (UINT4 u4IfIndex, tVlanTag * pVlanTag, tMacAddr SrcMacAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (SrcMacAddr);

    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanIssApiUpdtPortIsolationEntry                     */
/*                                                                           */
/* Description        : This function is called to update the port isolation */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           : pPortIsolationEntry - Pointer to the port isolation  */
/*                      information.                                         */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanIssApiUpdtPortIsolationEntry (tIssUpdtPortIsolation * pPortIsolationEntry)
{
    IssApiUpdtPortIsolationEntry (pPortIsolationEntry);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanIssApiGetIsolationFwdStatus                      */
/*                                                                           */
/* Description        : This function is called to get the isolation         */
/*                      status from the port isolation table.                */
/*                                                                           */
/* Input(s)           : u4IngressPort - Ingress Port.                        */
/*                      InVlanId      - Ingress VlanId                       */
/*                      u4EgressPort  - Egress Port.                         */
/*                      information.                                         */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIssApiGetIsolationFwdStatus (UINT4 u4IngressPort,
                                 tVlanId InVlanId, UINT4 u4EgressPort)
{
    tIssPortIsoInfo     IssPortIsoInfo;

    MEMSET (&IssPortIsoInfo, 0, sizeof (IssPortIsoInfo));

    IssPortIsoInfo.u4IngressPort = u4IngressPort;
    IssPortIsoInfo.u4EgressPort = u4EgressPort;
    IssPortIsoInfo.InVlanId = InVlanId;
    IssPortIsoInfo.u1Action = ISS_PI_GET_FWD_STATUS;

    /* Check if isolation is allowed with the ingress,vlan, egress tuple.
     * This check is required for all trunk ports. 
     * This check will always succeed for  a non trunk port.
     */
    if (IssApiGetIsolationFwdStatus (&IssPortIsoInfo) == ISS_FAILURE)
    {
        return VLAN_FAILURE;
    }

    IssPortIsoInfo.InVlanId = 0;

    /* Check if isolation is allowed with the ingress,null vlan, egress tuple.
     * This check is required for all non-trunk ports. 
     * This check will always succeed for trunk ports. 
     */
    if (IssApiGetIsolationFwdStatus (&IssPortIsoInfo) == ISS_FAILURE)
    {
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}                                /* End of VlanIssApiGetIsolationFwdStatus */

/*****************************************************************************/
/* Function Name      : VlanL2iwfValidatePortType                            */
/*                                                                           */
/* Description        : This function is called to ivalidate the bridge      */
/*                      for given bridge mode                                */
/*                                                                           */
/* Input(s)           : u4BridgeMode - Bridge Mode against which port type   */
/*                                     needs to ev validated                 */
/*                      u1BrgPortType - Bridge port type                     */
/*                      information.                                         */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Return Value(s)    : TRUE/FALSE                                           */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanL2iwfValidatePortType (UINT4 u4BridgeMode, UINT1 u1BrgPortType)
{
    return L2iwfValidatePortType (u4BridgeMode, u1BrgPortType);
}

/*****************************************************************************/
/* Function Name      : VlanSnoopDeleteFwdAndGroupTableDynamicInfo           */
/*                                                                           */
/* Description        : This function deletes the multicast forwarding       */
/*                      and group entries for a MAC address                  */
/*                                                                           */
/* Input(s)           : MacAddr          - Mac address                       */
/*                      u4ContextId      - Current Context Id                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanSnoopDeleteFwdAndGroupTableDynamicInfo (UINT4 u4ContextId, tMacAddr MacAddr)
{
    SnoopDeleteFwdAndGroupTableDynamicInfo (u4ContextId, MacAddr);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetVlanLearningType                         */
/*                                                                           */
/* Description        : This routine is called to set the vlan learning      */
/*                      type.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u1LearnMode - Learning type                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearnMode)
{
    return (L2IwfSetVlanLearningType (u4ContextId, u1LearnMode));
}

/*****************************************************************************/
/* Function Name      : VlanLaSetDefaultPropForAggregator                    */
/*                                                                           */
/* Description        : This routine is called to set default properties for */
/*                      an aggregator                                        */
/*                                                                           */
/* Input(s)           : u4AggId - Aggregator ID                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/ VLAN_FAILURE                           */
/*****************************************************************************/
INT4
VlanLaSetDefaultPropForAggregator (UINT4 u4AggId)
{
    if (LaSetDefaultPropForAggregator ((UINT2) u4AggId) == LA_FAILURE)
    {
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

#ifdef ICCH_WANTED

/*****************************************************************************/
/* Function Name      : VlanIcchRegisterProtocols                            */
/*                                                                           */
/* Description        : This function calls the ICCH module to register.     */
/*                                                                           */
/* Input(s)           : tIcchRegParams - Reg. params to be provided by       */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanIcchRegisterProtocols (tIcchRegParams * pIcchReg)
{
    return (IcchRegisterProtocols (pIcchReg));
}




/*****************************************************************************/
/* Function Name      : VlanIcchEnqMsgToIcchFromAppl                         */
/*                                                                           */
/* Description        : This function calls the ICCH Module to enqueue the   */
/*                      message from applications to ICCH task.              */
/*                      ICCH_MESSAGE, ICCH_BULK_REQ_MESSAGE,                 */
/*                      ICCH_BULK_TAIL_MESSAGE types are enqueued to ICCH    */
/*                                                                           */
/* Input(s)           : pIcchMsg - msg from appl.                            */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_FAILURE/ICCH_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanIcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId)
{
    return (IcchEnqMsgToIcchFromAppl (pIcchMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : VlanIcchHandleProtocolEvent                          */
/*                                                                           */
/* Description        : This function calls the ICCH module to intimate about*/
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                    (ICCH_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                     ICCH_BULK_UPDT_ABORT /                */
/*                                     ICCH_SLAVE_EVT_PROCESSED              */
/*                                     ICCH_MASTER_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (ICCH_MEMALLOC_FAIL /     */
/*                                      ICCH_SENDTO_FAIL / ICCH_PROCESS_FAIL)*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_FAILURE/ICCH_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
VlanIcchHandleProtocolEvent (tIcchProtoEvt * pEvt)
{
   return (IcchApiHandleProtocolEvent (pEvt));                                           
}
#endif /* ICCH_WANTED */

/*****************************************************************************/
/* Function Name      : VlanIcchSetBulkUpdatesStatus                         */
/*                                                                           */
/* Description        : This function calls the ICCH module to set the Status*/
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_FAILURE/ICCH_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIcchSetBulkUpdatesStatus (UINT4 u4AppId)
{
#ifdef ICCH_WANTED
    return (IcchSetBulkUpdatesStatus (u4AppId));
#else
   UNUSED_PARAM (u4AppId);
   return VLAN_FAILURE;
#endif
}

/*****************************************************************************/
/* Function Name      : VlanCfaGetInterfaceNameFromIndex                     */
/*                                                                           */
/* Description        : This routine is used to get the InterfaceName        */
/*                      corresponding to the IfIndex.                        */
/*                                                                           */
/* Input(s)           : u4Index   - InterfaceIndex                           */
/*                      pu1Alias  - Pointer to the InterfaceName.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
UINT4
VlanCfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
#ifdef ICCH_WANTED
	return (CfaGetInterfaceNameFromIndex (u4Index,pu1Alias));
#else
	UNUSED_PARAM (u4Index);
	UNUSED_PARAM (pu1Alias);
	return OSIX_FAILURE;
#endif
}
/*****************************************************************************/
/* Function Name      : VlanLaIsMclagInterface                               */
/*                                                                           */
/* Description        : This function is used to check whether the interface */
/*                      is MC-LAG interface or not.                          */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE/VLAN_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanLaIsMclagInterface (UINT4 u4IfIndex)
{
#ifdef LA_WANTED
    UINT1   u1IsMclag = 0;
    LaApiIsMclagInterface (u4IfIndex, &u1IsMclag);
    if (u1IsMclag == OSIX_TRUE)
    {
        return VLAN_TRUE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return VLAN_FALSE;
}

/*****************************************************************************/
/* Function Name      : VlanCfaGetIfOperStatus                               */
/*                                                                           */
/* Description        : This function is used to get the Interface OperStatus*/
/*                                                                           */
/* Input(s)           : u4IfIndex - InterfaceIndex                           */
/*                                                                           */
/* Output(s)          : *pu1OperStatus - Status                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCfaGetIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    if (CFA_FAILURE == CfaGetIfOperStatus (u4IfIndex, pu1OperStatus))
    {
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanLaGetMCLAGSystemStatus                           */
/*                                                                           */
/* Description        : This function is used to get the MCLAG system status */
/*                      whether MCLAG is enabled/disbaled in the system      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1MCLAGSystemStatus                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DISABLED - If MCLAG is disabled in the system     */
/*                      LA_ENABLED  - If MCLAG is enabled in the system      */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanLaGetMCLAGSystemStatus (VOID)
{
#ifdef LA_WANTED
	return (LaGetMCLAGSystemStatus ());
#else
	return LA_MCLAG_DISABLED;
#endif
}


/*****************************************************************************/
/* Function Name      : VlanLaGetLaEnableStatus                              */
/*                                                                           */
/* Description        : This function is used to get the LA system status    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1MCLAGSystemStatus                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LaEnabled - If Link Aggregation is enabled.          */
/*                      LaDisabled -If Link Aggregation is disabled.         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanLaGetLaEnableStatus (VOID)
{
#ifdef LA_WANTED
	return (LaGetLaEnableStatus ());
#else
	return LA_DISABLED;
#endif
}
/*****************************************************************************/
/* Function Name      : VlanIcchGetIcclIfIndex                               */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : *pu4IfIndex - ICCL interface index.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VlanIcchGetIcclIfIndex (UINT4 *pu4IfIndex)
{
#ifdef ICCH_WANTED
    IcchGetIcclIfIndex (pu4IfIndex);
#else
	UNUSED_PARAM (pu4IfIndex);
#endif
	return ;    	
}
/*****************************************************************************/
/*  Function Name   : VlanIcchApiGetIcclVlanId                               */
/*  Description     : This Function fetches the ICCH VlanId                  */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH Vlan Id from iccl.conf               */
/*****************************************************************************/
UINT4 VlanIcchApiGetIcclVlanId  (UINT2 u2Index)
{
#ifdef ICCH_WANTED
	return (IcchApiGetIcclVlanId (u2Index));
#else
	UNUSED_PARAM (u2Index);
	return 0;    	
#endif
}
/*****************************************************************************/
/*  Function Name   : VlanIcchIsIcclVlan                                     */
/*  Description     : This function checks whether the VLAN ID is ICCL VLAN  */
/*  Input(s)        : u2VlanId - VLAN Identifier                             */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIcchSessionInfo                          */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  OSIX_SUCCESS or OSIX_FAILURE              */
/*****************************************************************************/
UINT4 VlanIcchIsIcclVlan (UINT2 u2VlanId)
{
#ifdef ICCH_WANTED
	return (IcchIsIcclVlan (u2VlanId));
#else
	UNUSED_PARAM (u2VlanId);
	return OSIX_FAILURE;
#endif
}
/*******************************************************************************
 *  Function           : VlanIcchGetPeerNodeState
 *  Input              : None
 *  Output             : None
 *  Returns            : None
 *  Action             : Routine to get peer node state of ICCH.
 *  ***************************************************************************/
UINT4
VlanIcchGetPeerNodeState (VOID)
{
#ifdef ICCH_WANTED
	return (IcchGetPeerNodeState ());
#else
	return 0;
#endif
}
/*******************************************************************************
 *  Function           : VlanIcchGetPeerNodeState
 *  Input              : u4AggIndex - AggIndex
 *  Output             : pu1Status - Oper Status 
 *  Returns            : None
 *  Action             : Routine to get the bundle state of the AggIndex
 *  ***************************************************************************/
UINT4
VlanLaGetOperStatusonAggIndex (UINT4 u4AggIndex, UINT1 *pu1Status)
{
    UINT4      u4RetVal = LA_FALSE;  
#ifdef LA_WANTED
    VLAN_UNLOCK ();
    u4RetVal = LaApiGetOperStatusonAggIndex (u4AggIndex, pu1Status);
    VLAN_LOCK ();
#else
    UNUSED_PARAM (u4AggIndex);
    UNUSED_PARAM (pu1Status);
#endif
return u4RetVal;
    
}
/*****************************************************************************/
/* Function Name      : VlanPortCfaCreateSChannelInterface                   */
/*                                                                           */
/* Description        : Creates the S-Channel interface in CFA and thereby   */
/*                      indicating to L2IWF to create the S-Channel interface*/
/*                      to VLAN, IGMP Snooping and FIP Snooping.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - S-Channel interface index.            */
/*                      u4UapIfIndex - UAP Interface Index.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex, 
                                    UINT1 u1Status)
{
    /* S-Channel interface creation is supported from 
     * Ieee8021BridgeEvbCAPRowStatus of std1evb.mib. There is no specific MIB
     * objects present for creating s-channel interface from IF/CFA MIBs.
     * Hence S-Channel creation is done from managment routine(takes MGMT_LOCK)
     * through VLAN module (takes VLAN_LOCK). Since this interface creation 
     * should be done in CFA and in L2 modules like VLAN, IGS and FSB
     * sequentially, here VLAN_LOCK is realsed to avoid dead lock.
     * (i.e) CFA will take CFA_LOCK and calls L2IwfPortCreateIndication
     * which inturn triggers creation of interface in VLAN with L2SyncTakeSem.
     * Vlan to create this interface in VLAN module, it requires VLAN_LOCK, 
     * and hence VLAN_UNLOCK is done in this function.
     * Once after creating the interface in VLAN, IGS and FSB, VLAN_LOCK is
     * taken again.
     *
     * This complete flow will be done within MGMT_LOCK so that it will be in 
     * sync to give proper response to the CLI/SNMP Manager */
    VLAN_UNLOCK ();
 
    if (CfaCreateSChannelInterface (u4UapIfIndex, u4IfIndex, u1Status) 
        == CFA_FAILURE)
    {
        VLAN_LOCK ();
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                "VlanPortCfaCreateSChannelInterface",
                "Creation of S-Channel interface in CFA has failed\r\n");
        return VLAN_FAILURE;
    }
    VLAN_LOCK ();
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanPortCfaCreateDynamicSChannelInterface            */
/*                                                                           */
/* Description        : Creates the dynamic S-Channel interface in CFA and   */
/*                      thereby indicating to L2IWF to create the S-Channel  */
/*                      interface to VLAN, IGMP Snooping and FIP Snooping.   */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - S-Channel interface index.            */
/*                      u4UapIfIndex - UAP Interface Index.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaCreateDynamicSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4SChIfIndex)
{
    if (CfaCreateDynamicSChannelInterface (u4UapIfIndex, u4SChIfIndex) == CFA_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
               "VlanPortCfaCreateDynamicSChannelInterface",
               "Creation of dynamic S-Channel interface in CFA has failed\r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPortCfaDeleteSChannelInterface                   */
/*                                                                           */
/* Description        : Deletes the S-Channel interface in CFA and thereby   */
/*                      indicating to L2IWF to create the S-Channel interface*/
/*                      to VLAN, IGMP Snooping and FIP Snooping.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - S-Channel interface index.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaDeleteSChannelInterface (UINT4 u4SChIfIndex)
{
    /* S-Channel interface deletion is supported from 
     * Ieee8021BridgeEvbCAPRowStatus of std1evb.mib. There is no specific MIB
     * objects present for deleting s-channel interface from IF/CFA MIBs.
     * Hence S-Channel deletion is done from managment routine(takes MGMT_LOCK)
     * through VLAN module (takes VLAN_LOCK). Since this interface deletion 
     * should be done in CFA and in L2 modules like VLAN, IGS and FSB
     * sequentially, here VLAN_LOCK is realsed to avoid dead lock.
     * (i.e) CFA will take CFA_LOCK and calls L2IwfPortDeleteIndication
     * which inturn triggers deletion of interface in VLAN with L2SyncTakeSem.
     * Vlan to delete this interface in VLAN module, it requires VLAN_LOCK, 
     * and hence VLAN_UNLOCK is done in this function.
     * Once after deleting the interface in VLAN, IGS and FSB, VLAN_LOCK is
     * taken again.
     *
     * This complete flow will be done within MGMT_LOCK so that it will be in 
     * sync to give proper response to the CLI/SNMP Manager */
    VLAN_UNLOCK ();

    if (CfaDeleteSChannelInterface (u4SChIfIndex) == CFA_FAILURE)
    {
        VLAN_LOCK ();
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                "VlanEvbCfaDeleteSChannelInterface",
                "Deletion of S-Channel interface in CFA has failed\r\n");
        return VLAN_FAILURE;
    }
    VLAN_LOCK ();
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanPortCfaDeleteDynamicSChannelInterface            */
/*                                                                           */
/* Description        : Deletes the Dynamic S-Channel interface in CFA       */
/*                      and thereby                                          */
/*                      indicating to L2IWF to create the S-Channel interface*/
/*                      to VLAN, IGMP Snooping and FIP Snooping.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - S-Channel interface index.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaDeleteDynamicSChannelInterface (UINT4 u4SChIfIndex)
{
    if (CfaDeleteDynamicSChannelInterface (u4SChIfIndex) == CFA_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
               "VlanPortCfaDeleteDynamicSChannelInterface",
               "Deletion of dynamic S-Channel interface in CFA has failed\r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPortEvbL2IwfApplPortRequest                      */
/*                                                                           */
/* Description        : This function is used to call the port related       */
/*                      L2IWF functions to post the EVB message.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - S-Channel interface index.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortEvbL2IwfApplPortRequest (UINT4 u4UapIfIndex, 
                                  tLldpAppPortMsg * pLldpAppPortMsg,
                                  UINT1 u1MsgType)
{
    if (L2IwfLldpHandleApplPortRequest (u4UapIfIndex, pLldpAppPortMsg, 
                                        u1MsgType) == OSIX_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                  "VlanPortEvbL2IwfApplPortRequest",
                  "calling the port related L2IWF functions has failed\r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : VlanPortCfaGetFreeSChIndexForUap                     */
/*                                                                           */
/* Description        :This function provides the unique interface index for */
/*                     the  S-Channel interface for the given UAP.           */
/*                     Using this interface index, SBP port is               */
/*                     created in CFA and in the respective layer 2 modules  */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                                                                           */
/* Output(s)          : u4IfIndex - Unique interface index if available      */
/*                      zero - otherwise                                     */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
VlanPortCfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4SChIfIndex)
{
    if(CFA_FAILURE == CfaGetFreeSChIndexForUap 
                        (u4UapIfIndex, pu4SChIfIndex))
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                  "VlanPortCfaGetFreeSChIndexForUap",
                  "Failed to get Free S-channel Index for the given UAP.\r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanPortLldpApiGetInstanceId                         */
/*                                                                           */
/* Description        :This function retreives the Instance Id for the given */
/*                     Mac Address which has been registered with the LLDP   */
/*                     module                                                */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                      pu1MacAddr - Destination Mac Address                 */
/*                                                                           */
/* Output(s)          :  pu4InstanceId  - Instance Id                        */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortLldpApiGetInstanceId (UINT1 *pu1MacAddr, UINT4 *pu4InstanceId)
{
    if(OSIX_FAILURE == L2IwfLldpApiGetInstanceId
                        (pu1MacAddr, pu4InstanceId))
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                  "VlanPortLldpApiGetInstanceId",
                  "Failed to get instance id for the Mac Address.\r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPortCfaSetIfMainBrgPortType                      */
/*                                                                           */
/* Description        :This function sets  the bridge type as  default for   */
/*                        the interface index                                */
/*                                                                           */
/* Input(s)           : i4IfIndex - interface index                          */
/*                      i4SetValIfMainBrgPortType - Bridge Port Type         */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaSetIfMainBrgPortType (INT4 i4IfIndex, INT4 i4SetValIfMainBrgPortType)
{
    /* Changing the bridge port type is setting through from 
     * fsMIEvbSystemControl. This deletes all the s-channels present in this 
     * Uplink Access Port. To avoid dead lock situation in deleting the 
     * S-Channel interfaces, here VLAN_UNLOCK is done and after deleting them
     * from VLAN, IGS, FIPS and CFA, VLAN_LOCK is  taken back again.
     * Please refer VlanPortCfaDeleteSChannelInterface for more reference.
     *
     * This complete flow will be done within MGMT_LOCK so that it will be in 
     * sync to give proper response to the CLI/SNMP Manager */
    VLAN_UNLOCK ();

    if (CfaSetIfMainBrgPortType (i4IfIndex, i4SetValIfMainBrgPortType) 
            == SNMP_FAILURE)
    {
        VLAN_LOCK ();
        return VLAN_FAILURE;
    }

    VLAN_LOCK ();
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetBridgePortOperStatus                     */
/*                                                                           */
/* Description        : This routine Gets the given port's Bridge Oper       */
/*                      Status from the L2Iwf common database.               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : u1BridgeOperStatus - oper status of the given IfIndex*/
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanL2IwfGetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 *pu1BridgeOperStatus)
{
    if (L2IwfGetBridgePortOperStatus(u4IfIndex, pu1BridgeOperStatus)
        == L2IWF_FAILURE)
    {
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanAstIsStpEnabled                                  */
/*                                                                           */
/* Description        : This routine Gets information whether spanning-tree  */
/*                      is enabled from STP module                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u4ContextId - Context Id                             */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanAstIsStpEnabled(INT4 i4IfIndex,UINT4 u4ContextId)
{
    return (AstIsStpEnabled(i4IfIndex,u4ContextId));
}
/*****************************************************************************/
/* Function Name      : VlanPortLldpApiUpdateSvidOnUap                       */
/*                                                                           */
/* Description        : This function gives the  newly created/deleted on    */
/*                      SVID on the UAP port to the LLDP module              */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Ifindex                           */
/*                      u4Svid - SVLAN ID                                    */
/*                      u4Request - Creation/Deletion Request                */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanPortLldpApiUpdateSvidOnUap (UINT4 u4UapIfIndex, UINT4 u4Svid, UINT4 u4Request)
{
    L2IwfLldpApiUpdateSvidOnUap (u4UapIfIndex,u4Svid,u4Request);
    return;
}
/*****************************************************************************/
/* Function Name      : VlanPortCfaUpdateSChannelOperStatus                  */
/*                                                                           */
/* Description        :This function sets the Oper status and gives indication
                        to other registered modules.                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - interface index                          */
/*                      u1OperStatus - Oper Status                           */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaUpdateSChannelOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{

    if(CfaUpdateSChannelOperStatus (u4IfIndex,u1OperStatus)
            == CFA_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                "VlanPortCfaUpdateSChannelOperStatus",
                "Failed to set the Oper Status \r\n");
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}
