/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevtrp.c,v 1.6 2016/07/22 06:46:32 siva Exp $
 *
 * Description: This file contains VLAN EVB Trap related routines.
 *
 *******************************************************************/
#ifndef __VLNEVTRP_C__
#define __VLNEVTRP_C__

#include "vlaninc.h"
#ifdef SNMP_3_WANTED
#include "fsmid1evb.h"
static INT1         ai1TempBuffer[VLAN_EVB_SNMP_MAX_OID_LENGTH];
#endif

/***************************************************************************/
/* Function Name      : VlanEvbSendSChannelStatusTrap                      */
/*                                                                         */
/* Description        : This function will send an trap to the             */
/*                      administrator when there is a SBP port's admin or  */
/*                      operational status change                          */
/*                                                                         */
/* Input(s)           : pSchIfEntry  - S-Channel Entry.                    */
/*                                                                         */
/* Output(s)          : None                                               */
/*                                                                         */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                          */
/***************************************************************************/
INT4
VlanEvbSendSChannelStatusTrap (tEvbSChIfEntry *pSchIfEntry)
{
    tSNMP_OID_TYPE      *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND      *pVbList        = NULL;
    tSNMP_VAR_BIND      *pStartVb       = NULL;
    tSNMP_OID_TYPE      *pOid           = NULL;
    tSNMP_OID_TYPE      *pSnmpTrapOid = NULL;
    UINT4                u4GenTrapType  = 0;
    UINT4                u4SbpCompId    = 0;
    UINT2                u2LocalPortId  = 0;
    UINT4                au4EvbStatusTrapOid[] = {1,3,6,1,4,1,29601,2,104,1,4,0,5};
    UINT4                au4snmpTrapOid[] = {1,3,6,1,6,3,1,1,4,1,0};
    UINT1                au1Buf[VLAN_OBJ_NAME_LEN];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tEvbContextInfo     *pEvbContextInfo = NULL;
    tCfaIfInfo           CfaIfInfo;

    MEMSET (au1Buf, 0, VLAN_OBJ_NAME_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));


    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4) pSchIfEntry->i4SChIfIndex,
                                      &u4SbpCompId,
                                      &u2LocalPortId) != VCM_SUCCESS)
    {
             VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                   "VlanEvbSendSChannelStatusTrap: VlanVcmGetContextInfoFromIfIndex "
                   "failed for S-Channel  : %d \n",pSchIfEntry->i4SChIfIndex);
             return VLAN_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[u4SbpCompId];

    if (NULL == pEvbContextInfo)
    {
        return VLAN_SUCCESS;
    }


    if (pEvbContextInfo->i4EvbSysTrapStatus != VLAN_EVB_TRAP_ENABLE)
    {
        return VLAN_SUCCESS;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0; 



    /* Check for the OperStatus */
    if (pSchIfEntry->u1OperStatus == CFA_IF_DOWN)
    {
        u4GenTrapType = CFA_IF_DOWN;
    }
    else if (pSchIfEntry->u1OperStatus == CFA_IF_UP)
    {
        u4GenTrapType = CFA_IF_UP;
    }
    else
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelStatusTrap: Invalid Oper Status \n");
        return VLAN_FAILURE;
    }

    if ((u4GenTrapType == CFA_IF_DOWN)
        || (u4GenTrapType == CFA_IF_UP))
    {

        pEnterpriseOid = alloc_oid (VLAN_EVB_TRAP_OID_LEN);
        if (pEnterpriseOid == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelStatusTrap: pEnterpriseOid NULL\n");
            return VLAN_FAILURE;
        }

        MEMCPY (pEnterpriseOid->pu4_OidList, au4EvbStatusTrapOid, 
                                         sizeof (au4EvbStatusTrapOid));
        pEnterpriseOid->u4_Length = sizeof (au4EvbStatusTrapOid) / sizeof (UINT4);

    /* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and
     * the OID of the MIB object for those trap is going to be generated.
     */
    pSnmpTrapOid = alloc_oid (VLAN_EVB_SNMP_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelStatusTrap: pSnmpTrapOid NULL\n");
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Assigning the OID value and its length of snmpTrapOid to psnmpTrapOid */
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);


    /* Form a VarBind for snmpTrapOID OID and its value */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
            SNMP_DATA_TYPE_OBJECT_ID, 0L, 0, NULL, pEnterpriseOid, u8CounterVal);

    /* If the pVbList is empty or values are binded then free the memory
       allocated to pEnterpriseOid and pSnmpTrapOid. */
    if (pVbList == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelStatusTrap: pVbList NULL\n");
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return VLAN_FAILURE;
    }

    pStartVb = pVbList;


        /* For a SBP LINK UP/DOWN Trap, SBP component id , SBP port number,*/
        /*  ifMainAdminStatus and ifMainOperStatus have to be notified */
        /*  through the TRAP. */
        SPRINTF ((char *) au1Buf, "fsMIEvbPhyPort");

        /* Pass the Object Name SBP Component id to get the SNMP Oid */
        pOid = VlanEvbMakeObjIdFromDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbSendSChannelStatusTrap: pOid NULL\n");
            /* Release the memory Assigned for Entrerprise Oid */
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }

        pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                SNMP_DATA_TYPE_INTEGER,
                0L,
                (INT4) pSchIfEntry->u4UapIfIndex,
                NULL, NULL,
                u8CounterVal);
        if (pVbList->pNextVarBind == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbSendSChannelStatusTrap: pVbList->pNextVarBind NULL\n");
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

        SPRINTF ((char *) au1Buf, "fsMIEvbSchID");

        /* Pass the Object Name SBP Component id to get the SNMP Oid */
        pOid = VlanEvbMakeObjIdFromDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbSendSChannelStatusTrap: pOid for fsMIEvbSchID NULL\n");
            /* Release the memory Assigned for Entrerprise Oid */
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }


        pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                           SNMP_DATA_TYPE_INTEGER,
                                                           0L,
                                                           (INT4) pSchIfEntry->u4SVId,
                                                           NULL, NULL,
                                                           u8CounterVal);
        if (pVbList->pNextVarBind == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbSendSChannelStatusTrap: pVbList->pNextVarBind is NULL\n");
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

        SPRINTF ((char *) au1Buf, "ifMainAdminStatus");
        /* Pass the Object Name ifMainAdminStatus to get the SNMP Oid */
        pOid = VlanEvbCfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
        if (pOid == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSendSChannelStatusTrap: ifMainAdminStatus-pOid "
                    "is NULL\n");
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }

        /* For the SNMP Variable Binding of OID and associated Value
         * for ifMainAdminStatus*/
        /* getting the admin status and oper status from CFA */
        if (VlanCfaGetIfInfo ((UINT4)pSchIfEntry->i4SChIfIndex,&CfaIfInfo) 
            != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSendSChannelStatusTrap: VlanCfaGetIfInfo "
                    "Failed for S-Channel Interface Index:%d \n",
                    pSchIfEntry->i4SChIfIndex);
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }
        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER,
                                                     0,
                                                     (INT4) CfaIfInfo.u1IfAdminStatus,
                                                     NULL, NULL,
                                                     SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSendSChannelStatusTrap: ifMainAdminStatus-"
                    "pNextVarBind is NULL\n");
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }
        /* Start of Next Variable Binding */
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

        SPRINTF ((char *) au1Buf, "ifMainOperStatus");
        /* Pass the Object Name ifMainOperStatus to get the SNMP Oid */
        pOid = VlanEvbCfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
        if (pOid == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSendSChannelStatusTrap: ifMainOperStatus-"
                    "pOid is NULL\n");
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }

        /* For the SNMP Variable Binding of OID and associated Value
         * for ifMainOperStatus */
        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER,
                                                     0, (INT4) CfaIfInfo.u1IfOperStatus,
                                                     NULL, NULL,
                                                     SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            VLAN_TRC (VLAN_EVB_TRC,ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSendSChannelStatusTrap: ifMainOperStatus-"
                    "pNextVarBind is NULL\n");
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb); 


    return VLAN_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/* Function     : VlanEvbSendSchannelSuccessTrap                           */
/*                                                                         */
/* Description  : This function is used to send EVB S-channel              */
/*                creation/deletion success/fail trap to the administrator */
/*                                                                         */
/* Input        : u1TrapId    : Trap Identifier to identfy whether         */
/*                              creation or deletion                       */
/*                u4UapIfIndex: CFA interface index for UAP                */
/*                u4SVId      : SVID from 1-4094                           */
/*                                                                         */
/* Output       : None                                                     */
/*                                                                         */
/* Returns      : VLAN_FAILURE/VLAN_SUCCESS                                */
/*                                                                         */
/***************************************************************************/
INT4
VlanEvbSendSChannelTrap (UINT1 u1TrapId, UINT4 u4UapIfIndex, 
                                UINT4 u4SVId)
{
    tSNMP_VAR_BIND      *pVbList          = NULL;
    tSNMP_VAR_BIND      *pStartVb         = NULL;
    tSNMP_OID_TYPE      *pEnterpriseOid   = NULL;
    tSNMP_OID_TYPE      *pOid             = NULL;
    UINT2                u2LocalPortId    = 0;
    tSNMP_OID_TYPE      *pSnmpTrapOid     = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal     = { 0, 0 };
    UINT4                au4snmpTrapOid[] = {1,3,6,1,6,3,1,1,4,1,0};
    UINT1                au1Buf[VLAN_OBJ_NAME_LEN];
    UINT4                au4EvbTrapOid[]  = {1,3,6,1,4,1,29601,2,104,1,4,0};
    UINT4                u4ContextId      = VLAN_EVB_ZERO;
    tEvbContextInfo     *pEvbContextInfo  = NULL;


    MEMSET (au1Buf, 0, VLAN_OBJ_NAME_LEN);
    if (VlanVcmGetContextInfoFromIfIndex (u4UapIfIndex,&u4ContextId,
                            &u2LocalPortId) != VCM_SUCCESS)
    {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelTrap: VlanVcmGetContextInfoFromIfIndex "
                "failed for UAP : %d \n",u4UapIfIndex);
            return VLAN_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[u4ContextId];

    if (NULL == pEvbContextInfo)
    {
        return VLAN_SUCCESS;
    }


    if (pEvbContextInfo->i4EvbSysTrapStatus != VLAN_EVB_TRAP_ENABLE)
    {
        return VLAN_SUCCESS;
    }

    pEnterpriseOid = alloc_oid (VLAN_EVB_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelTrap: pEnterpriseOid is NULL \n");
        return VLAN_FAILURE;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, au4EvbTrapOid, sizeof (au4EvbTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4EvbTrapOid) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and
     * the OID of the MIB object for those trap is going to be generated.
     */
    pSnmpTrapOid = alloc_oid (VLAN_EVB_SNMP_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelTrap: pSnmpTrapOid is NULL \n");
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Assigning the OID value and its length of snmpTrapOid to psnmpTrapOid */
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);


    /* Form a VarBind for snmpTrapOID OID and its value */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
            SNMP_DATA_TYPE_OBJECT_ID, 0L, 0, NULL, pEnterpriseOid, u8CounterVal);

    /* If the pVbList is empty or values are binded then free the memory
       allocated to pEnterpriseOid and pSnmpTrapOid. */
    if (pVbList == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSendSChannelTrap: pVbList is NULL \n");
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return VLAN_FAILURE;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {

        case VLAN_EVB_SBP_CREATE_TRAP:
        case VLAN_EVB_SBP_DELETE_TRAP:
        case VLAN_EVB_SBP_CREATE_FAIL_TRAP:
        case VLAN_EVB_SBP_DELETE_FAIL_TRAP:

            SPRINTF ((char *) au1Buf, "fsMIEvbPhyPort");
            pOid = VlanEvbMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                        "VlanEvbSendSChannelTrap: fsMIEvbPhyPort-pOid"
                        " is NULL \n");
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return VLAN_FAILURE;
            }
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid, 
                        SNMP_DATA_TYPE_INTEGER, 0L,
                        (INT4) u4UapIfIndex, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                        "VlanEvbSendSChannelTrap: fsMIEvbPhyPort-pNextVarBind"
                        " is NULL \n");
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return VLAN_FAILURE;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((char *) au1Buf, "fsMIEvbSchID");
            pOid = VlanEvbMakeObjIdFromDotNew ((INT1 *) au1Buf);

            if (pOid == NULL)
            {
                VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                        "VlanEvbSendSChannelTrap: fsMIEvbSchID-pOid"
                        " is NULL \n");
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return VLAN_FAILURE;
            }
           
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0L,
                        (INT4) u4SVId, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                        "VlanEvbSendSChannelTrap: fsMIEvbSChId-NextVarBind"
                        " is NULL \n");
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return VLAN_FAILURE;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            break;

        default:
            VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSendSChannelTrap: default case\r\n");
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            return VLAN_FAILURE;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbMakeObjIdFromDotNew                           */
/*                                                                           */
/* Description        : This function will return the OID from name of the   */
/*                      object                                               */
/*                                                                           */
/* Input(s)           : pi1TextStr - Object Name                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pOidPtr  or NULL                                     */
/*****************************************************************************/
tSNMP_OID_TYPE     *
VlanEvbMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
#ifdef SNMP_3_WANTED
    /* TRAP_ADDITON */
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = VLAN_INIT_VAL;
    UINT2               u2DotCount = VLAN_INIT_VAL;
    UINT1              *pu1TempPtr = NULL;
    UINT2               u2Len = 0;

    if (pi1TextStr == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbMakeObjIdFromDotNew: pi1TextStr"
                " is NULL \n");
        return NULL;
    }
    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
                u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0;
                ((u2Index <
                  (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
                 && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                        (orig_mib_oid_table[u2Index].pName,
                         (INT1 *) ai1TempBuffer) == 0)
                    && (STRLEN ((INT1 *) ai1TempBuffer) ==
                        STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                        orig_mib_oid_table[u2Index].pNumber,
                        VLAN_MIN (sizeof (ai1TempBuffer),
                            STRLEN (orig_mib_oid_table[u2Index].
                                pNumber)));
                ai1TempBuffer[sizeof (ai1TempBuffer) - 1] = '\0';
                break;
            }
        }

        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        u2Len =
            (UINT2) ((sizeof(ai1TempBuffer)-STRLEN(ai1TempBuffer)-1) <
                    STRLEN ((INT1 *) pi1DotPtr) ?
                    (sizeof(ai1TempBuffer)-STRLEN(ai1TempBuffer)-1) : 
                    STRLEN ((INT1 *) pi1DotPtr));
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                u2Len);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; ai1TempBuffer[u2Index] != '\0'; u2Index++)
    {
        if (u2Index >= 256)
        {
            break;
        }
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }
    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (u2Index >= 256)
        {
            break;
        }
        if (VlanParseSubIdNew
                ((&(pu1TempPtr)), &(pOidPtr->pu4_OidList[u2Index])) == RST_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            UNUSED_PARAM(pOidPtr);
            return (NULL);
        }
        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            UNUSED_PARAM(pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */
    return (pOidPtr);
#else
    UNUSED_PARAM (pi1TextStr);
    return (pOidPtr);
#endif
}

/**********************************************************************
* Function             : VlanEvbCfaMakeObjIdFromDotNew                *
*                                                                     *
* Role of the function : This function returns the pointer to the     *
*                         object id of the passed object to it.       *
* Formal Parameters    : textStr  :  pointer to the object whose      *
                                      object id is to be found        *
* Global Variables     : None                                         *
* Side Effects         : None                                         *
* Exception Handling   : None                                         *
* Use of Recursion     : None                                         *
* Return Value         : pOid : pointer to object id.                 *
**********************************************************************/
tSNMP_OID_TYPE     *
VlanEvbCfaMakeObjIdFromDotNew (UINT1 *textStr)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    INT1               *pTemp = NULL, *pDot = NULL;
    UINT1               au1tempBuffer[VLAN_EVB_CFA_OBJECT_NAME_LEN + 1];
    UINT4               au4IfIndex[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 1 };
    UINT4               au4IfMainAdminStatus[] = { 1, 3, 6, 1, 4, 1, 2076, 27, 1, 4, 1, 4 };
    UINT4               au4IfMainOperStatus[]  = { 1, 3, 6, 1, 4, 1, 2076, 27, 1, 4, 1, 5 };

    /* NOTE: Currently this function is hardcoded to return the Oid of
     * the Object names. Once ifmib.mib is compiled with latest MIB
     * compiler, this function will be modified to generate the OID
     * from the given string .*/

    /* Is there an alpha descriptor at begining ?? */
    if (CFA_ISALPHA (*textStr))
    {
        pDot = ((INT1 *) STRCHR ((char *) textStr, '.'));

        /* if no dot, point to end of string */
        if (pDot == NULL)
        {
            pDot = ((INT1 *) (textStr + STRLEN ((char *) textStr)));
        }
        pTemp = ((INT1 *) textStr);
        MEMSET (au1tempBuffer, 0, VLAN_EVB_CFA_OBJECT_NAME_LEN + 1);
        STRNCPY (au1tempBuffer, pTemp, (pDot - pTemp));
        au1tempBuffer[(pDot - pTemp)] = '\0';

        if ((pOid = alloc_oid (SNMP_TRAP_OID_LEN)) == NULL)
        {
            return (NULL);
        }

        if (STRCMP ("ifIndex", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4IfIndex,
                    (SNMP_TRAP_OID_LEN - 1) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN;
            return (pOid);

        }
        else if (STRCMP ("ifMainAdminStatus", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4IfMainAdminStatus,
                    (SNMP_TRAP_OID_LEN + 1) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN+1;
            return (pOid);

        }
        else if (STRCMP ("ifMainOperStatus", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4IfMainOperStatus,
                    (SNMP_TRAP_OID_LEN + 1) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN+1;
            return (pOid);

        }
    }

    if (pOid != NULL)
    {
        free_oid (pOid);
    }
    return NULL;

}
#endif  /*__VLNEVTRP_C__*/ 
