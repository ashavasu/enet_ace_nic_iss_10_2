/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbrlw.c,v 1.10 2011/10/25 10:39:08 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsmsbrlw.h"

/* LOW LEVEL Routines for Table : FsDot1dBaseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dBaseTable
 Input       :  The Indices
                FsDot1dBaseContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dBaseTable (INT4 i4FsDot1dBaseContextId)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dBaseTable
 Input       :  The Indices
                FsDot1dBaseContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dBaseTable (INT4 *pi4FsDot1dBaseContextId)
{
    UNUSED_PARAM (pi4FsDot1dBaseContextId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dBaseTable
 Input       :  The Indices
                FsDot1dBaseContextId
                nextFsDot1dBaseContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dBaseTable (INT4 i4FsDot1dBaseContextId,
                                 INT4 *pi4NextFsDot1dBaseContextId)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pi4NextFsDot1dBaseContextId);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dBaseBridgeAddress
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                retValFsDot1dBaseBridgeAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBaseBridgeAddress (INT4 i4FsDot1dBaseContextId,
                                tMacAddr * pRetValFsDot1dBaseBridgeAddress)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pRetValFsDot1dBaseBridgeAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dBaseNumPorts
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                retValFsDot1dBaseNumPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBaseNumPorts (INT4 i4FsDot1dBaseContextId,
                           INT4 *pi4RetValFsDot1dBaseNumPorts)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pi4RetValFsDot1dBaseNumPorts);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dBaseType
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                retValFsDot1dBaseType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBaseType (INT4 i4FsDot1dBaseContextId,
                       INT4 *pi4RetValFsDot1dBaseType)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pi4RetValFsDot1dBaseType);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsDot1dBasePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dBasePortTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dBasePortTable (INT4 i4FsDot1dBasePort)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dBasePortTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dBasePortTable (INT4 *pi4FsDot1dBasePort)
{
    UNUSED_PARAM (pi4FsDot1dBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dBasePortTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dBasePortTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4NextFsDot1dBasePort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dBasePortIfIndex
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1dBasePortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBasePortIfIndex (INT4 i4FsDot1dBasePort,
                              INT4 *pi4RetValFsDot1dBasePortIfIndex)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1dBasePortIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dBasePortCircuit
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1dBasePortCircuit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBasePortCircuit (INT4 i4FsDot1dBasePort,
                              tSNMP_OID_TYPE * pRetValFsDot1dBasePortCircuit)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pRetValFsDot1dBasePortCircuit);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dBasePortDelayExceededDiscards
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1dBasePortDelayExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBasePortDelayExceededDiscards (INT4 i4FsDot1dBasePort,
                                            UINT4
                                            *pu4RetValFsDot1dBasePortDelayExceededDiscards)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pu4RetValFsDot1dBasePortDelayExceededDiscards);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dBasePortMtuExceededDiscards
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1dBasePortMtuExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dBasePortMtuExceededDiscards (INT4 i4FsDot1dBasePort,
                                          UINT4
                                          *pu4RetValFsDot1dBasePortMtuExceededDiscards)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pu4RetValFsDot1dBasePortMtuExceededDiscards);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsDot1dTpTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dTpTable
 Input       :  The Indices
                FsDot1dBaseContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dTpTable (INT4 i4FsDot1dBaseContextId)
{
    if (VLAN_IS_VC_VALID (i4FsDot1dBaseContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dTpTable
 Input       :  The Indices
                FsDot1dBaseContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dTpTable (INT4 *pi4FsDot1dBaseContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1dBaseContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dTpTable
 Input       :  The Indices
                FsDot1dBaseContextId
                nextFsDot1dBaseContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dTpTable (INT4 i4FsDot1dBaseContextId,
                               INT4 *pi4NextFsDot1dBaseContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsDot1dBaseContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1dBaseContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dTpLearnedEntryDiscards
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                retValFsDot1dTpLearnedEntryDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpLearnedEntryDiscards (INT4 i4FsDot1dBaseContextId,
                                     UINT4
                                     *pu4RetValFsDot1dTpLearnedEntryDiscards)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pu4RetValFsDot1dTpLearnedEntryDiscards);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpAgingTime
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                retValFsDot1dTpAgingTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpAgingTime (INT4 i4FsDot1dBaseContextId,
                          INT4 *pi4RetValFsDot1dTpAgingTime)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1dBaseContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dTpAgingTime (pi4RetValFsDot1dTpAgingTime);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dTpAgingTime
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                setValFsDot1dTpAgingTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dTpAgingTime (INT4 i4FsDot1dBaseContextId,
                          INT4 i4SetValFsDot1dTpAgingTime)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1dBaseContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dTpAgingTime (i4SetValFsDot1dTpAgingTime);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dTpAgingTime
 Input       :  The Indices
                FsDot1dBaseContextId

                The Object 
                testValFsDot1dTpAgingTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dTpAgingTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBaseContextId,
                             INT4 i4TestValFsDot1dTpAgingTime)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1dBaseContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dTpAgingTime (pu4ErrorCode,
                                          i4TestValFsDot1dTpAgingTime);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dTpTable
 Input       :  The Indices
                FsDot1dBaseContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dTpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDot1dTpFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dTpFdbTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dTpFdbTable (INT4 i4FsDot1dBaseContextId,
                                           tMacAddr FsDot1dTpFdbAddress)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dTpFdbAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dTpFdbTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dTpFdbTable (INT4 *pi4FsDot1dBaseContextId,
                                   tMacAddr * pFsDot1dTpFdbAddress)
{
    UNUSED_PARAM (pi4FsDot1dBaseContextId);
    UNUSED_PARAM (pFsDot1dTpFdbAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dTpFdbTable
 Input       :  The Indices
                FsDot1dBaseContextId
                nextFsDot1dBaseContextId
                FsDot1dTpFdbAddress
                nextFsDot1dTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dTpFdbTable (INT4 i4FsDot1dBaseContextId,
                                  INT4 *pi4NextFsDot1dBaseContextId,
                                  tMacAddr FsDot1dTpFdbAddress,
                                  tMacAddr * pNextFsDot1dTpFdbAddress)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pi4NextFsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dTpFdbAddress);
    UNUSED_PARAM (pNextFsDot1dTpFdbAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dTpFdbPort
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dTpFdbAddress

                The Object 
                retValFsDot1dTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpFdbPort (INT4 i4FsDot1dBaseContextId,
                        tMacAddr FsDot1dTpFdbAddress,
                        INT4 *pi4RetValFsDot1dTpFdbPort)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dTpFdbAddress);
    UNUSED_PARAM (pi4RetValFsDot1dTpFdbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpFdbStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dTpFdbAddress

                The Object 
                retValFsDot1dTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpFdbStatus (INT4 i4FsDot1dBaseContextId,
                          tMacAddr FsDot1dTpFdbAddress,
                          INT4 *pi4RetValFsDot1dTpFdbStatus)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dTpFdbAddress);
    UNUSED_PARAM (pi4RetValFsDot1dTpFdbStatus);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsDot1dTpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dTpPortTable
 Input       :  The Indices
                FsDot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dTpPortTable (INT4 i4FsDot1dTpPort)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dTpPortTable
 Input       :  The Indices
                FsDot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dTpPortTable (INT4 *pi4FsDot1dTpPort)
{
    UNUSED_PARAM (pi4FsDot1dTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dTpPortTable
 Input       :  The Indices
                FsDot1dTpPort
                nextFsDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dTpPortTable (INT4 i4FsDot1dTpPort,
                                   INT4 *pi4NextFsDot1dTpPort)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pi4NextFsDot1dTpPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortMaxInfo
 Input       :  The Indices
                FsDot1dTpPort

                The Object 
                retValFsDot1dTpPortMaxInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortMaxInfo (INT4 i4FsDot1dTpPort,
                            INT4 *pi4RetValFsDot1dTpPortMaxInfo)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pi4RetValFsDot1dTpPortMaxInfo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortInFrames
 Input       :  The Indices
                FsDot1dTpPort

                The Object 
                retValFsDot1dTpPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortInFrames (INT4 i4FsDot1dTpPort,
                             UINT4 *pu4RetValFsDot1dTpPortInFrames)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu4RetValFsDot1dTpPortInFrames);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortOutFrames
 Input       :  The Indices
                FsDot1dTpPort

                The Object 
                retValFsDot1dTpPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortOutFrames (INT4 i4FsDot1dTpPort,
                              UINT4 *pu4RetValFsDot1dTpPortOutFrames)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu4RetValFsDot1dTpPortOutFrames);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortInDiscards
 Input       :  The Indices
                FsDot1dTpPort

                The Object 
                retValFsDot1dTpPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortInDiscards (INT4 i4FsDot1dTpPort,
                               UINT4 *pu4RetValFsDot1dTpPortInDiscards)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dTpPortInDiscards ((INT4) u2LocalPortId,
                                            pu4RetValFsDot1dTpPortInDiscards);

    VlanReleaseContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsDot1dStaticTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dStaticTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dStaticTable (INT4 i4FsDot1dBaseContextId,
                                            tMacAddr FsDot1dStaticAddress,
                                            INT4 i4FsDot1dStaticReceivePort)
{
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dStaticTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dStaticTable (INT4 *pi4FsDot1dBaseContextId,
                                    tMacAddr * pFsDot1dStaticAddress,
                                    INT4 *pi4FsDot1dStaticReceivePort)
{
    UNUSED_PARAM (pi4FsDot1dBaseContextId);
    UNUSED_PARAM (pFsDot1dStaticAddress);
    UNUSED_PARAM (pi4FsDot1dStaticReceivePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dStaticTable
 Input       :  The Indices
                FsDot1dBaseContextId
                nextFsDot1dBaseContextId
                FsDot1dStaticAddress
                nextFsDot1dStaticAddress
                FsDot1dStaticReceivePort
                nextFsDot1dStaticReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dStaticTable (INT4 i4FsDot1dBaseContextId,
                                   INT4 *pi4NextFsDot1dBaseContextId,
                                   tMacAddr FsDot1dStaticAddress,
                                   tMacAddr * pNextFsDot1dStaticAddress,
                                   INT4 i4FsDot1dStaticReceivePort,
                                   INT4 *pi4NextFsDot1dStaticReceivePort)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pi4NextFsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (pNextFsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (pi4NextFsDot1dStaticReceivePort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dStaticRowStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort

                The Object 
                retValFsDot1dStaticRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStaticRowStatus (INT4 i4FsDot1dBaseContextId,
                              tMacAddr FsDot1dStaticAddress,
                              INT4 i4FsDot1dStaticReceivePort,
                              INT4 *pi4RetValFsDot1dStaticRowStatus)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (pi4RetValFsDot1dStaticRowStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dStaticStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort

                The Object 
                retValFsDot1dStaticStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStaticStatus (INT4 i4FsDot1dBaseContextId,
                           tMacAddr FsDot1dStaticAddress,
                           INT4 i4FsDot1dStaticReceivePort,
                           INT4 *pi4RetValFsDot1dStaticStatus)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (pi4RetValFsDot1dStaticStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dStaticRowStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort

                The Object 
                setValFsDot1dStaticRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStaticRowStatus (INT4 i4FsDot1dBaseContextId,
                              tMacAddr FsDot1dStaticAddress,
                              INT4 i4FsDot1dStaticReceivePort,
                              INT4 i4SetValFsDot1dStaticRowStatus)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4SetValFsDot1dStaticRowStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dStaticStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort

                The Object 
                setValFsDot1dStaticStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStaticStatus (INT4 i4FsDot1dBaseContextId,
                           tMacAddr FsDot1dStaticAddress,
                           INT4 i4FsDot1dStaticReceivePort,
                           INT4 i4SetValFsDot1dStaticStatus)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4SetValFsDot1dStaticStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStaticRowStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort

                The Object 
                testValFsDot1dStaticRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStaticRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot1dBaseContextId,
                                 tMacAddr FsDot1dStaticAddress,
                                 INT4 i4FsDot1dStaticReceivePort,
                                 INT4 i4TestValFsDot1dStaticRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4TestValFsDot1dStaticRowStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStaticStatus
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort

                The Object 
                testValFsDot1dStaticStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStaticStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBaseContextId,
                              tMacAddr FsDot1dStaticAddress,
                              INT4 i4FsDot1dStaticReceivePort,
                              INT4 i4TestValFsDot1dStaticStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4TestValFsDot1dStaticStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dStaticTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dStaticTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDot1dStaticAllowedToGoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
                FsDot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dStaticAllowedToGoTable (INT4
                                                       i4FsDot1dBaseContextId,
                                                       tMacAddr
                                                       FsDot1dStaticAddress,
                                                       INT4
                                                       i4FsDot1dStaticReceivePort,
                                                       INT4 i4FsDot1dTpPort)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4FsDot1dTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
                FsDot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dStaticAllowedToGoTable (INT4 *pi4FsDot1dBaseContextId,
                                               tMacAddr * pFsDot1dStaticAddress,
                                               INT4
                                               *pi4FsDot1dStaticReceivePort,
                                               INT4 *pi4FsDot1dTpPort)
{
    UNUSED_PARAM (pi4FsDot1dBaseContextId);
    UNUSED_PARAM (pFsDot1dStaticAddress);
    UNUSED_PARAM (pi4FsDot1dStaticReceivePort);
    UNUSED_PARAM (pi4FsDot1dTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1dBaseContextId
                nextFsDot1dBaseContextId
                FsDot1dStaticAddress
                nextFsDot1dStaticAddress
                FsDot1dStaticReceivePort
                nextFsDot1dStaticReceivePort
                FsDot1dTpPort
                nextFsDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dStaticAllowedToGoTable (INT4 i4FsDot1dBaseContextId,
                                              INT4 *pi4NextFsDot1dBaseContextId,
                                              tMacAddr FsDot1dStaticAddress,
                                              tMacAddr *
                                              pNextFsDot1dStaticAddress,
                                              INT4 i4FsDot1dStaticReceivePort,
                                              INT4
                                              *pi4NextFsDot1dStaticReceivePort,
                                              INT4 i4FsDot1dTpPort,
                                              INT4 *pi4NextFsDot1dTpPort)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (pi4NextFsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (pNextFsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (pi4NextFsDot1dStaticReceivePort);
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pi4NextFsDot1dTpPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dStaticAllowedIsMember
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
                FsDot1dTpPort

                The Object 
                retValFsDot1dStaticAllowedIsMember
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStaticAllowedIsMember (INT4 i4FsDot1dBaseContextId,
                                    tMacAddr FsDot1dStaticAddress,
                                    INT4 i4FsDot1dStaticReceivePort,
                                    INT4 i4FsDot1dTpPort,
                                    INT4 *pi4RetValFsDot1dStaticAllowedIsMember)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pi4RetValFsDot1dStaticAllowedIsMember);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dStaticAllowedIsMember
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
                FsDot1dTpPort

                The Object 
                setValFsDot1dStaticAllowedIsMember
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStaticAllowedIsMember (INT4 i4FsDot1dBaseContextId,
                                    tMacAddr FsDot1dStaticAddress,
                                    INT4 i4FsDot1dStaticReceivePort,
                                    INT4 i4FsDot1dTpPort,
                                    INT4 i4SetValFsDot1dStaticAllowedIsMember)
{
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (i4SetValFsDot1dStaticAllowedIsMember);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStaticAllowedIsMember
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
                FsDot1dTpPort

                The Object 
                testValFsDot1dStaticAllowedIsMember
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStaticAllowedIsMember (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1dBaseContextId,
                                       tMacAddr FsDot1dStaticAddress,
                                       INT4 i4FsDot1dStaticReceivePort,
                                       INT4 i4FsDot1dTpPort,
                                       INT4
                                       i4TestValFsDot1dStaticAllowedIsMember)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBaseContextId);
    UNUSED_PARAM (FsDot1dStaticAddress);
    UNUSED_PARAM (i4FsDot1dStaticReceivePort);
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (i4TestValFsDot1dStaticAllowedIsMember);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1dBaseContextId
                FsDot1dStaticAddress
                FsDot1dStaticReceivePort
                FsDot1dTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dStaticAllowedToGoTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
