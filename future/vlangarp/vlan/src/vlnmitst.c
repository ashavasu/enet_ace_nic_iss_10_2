/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnmitst.c,v 1.47.22.1 2018/03/15 13:00:06 siva Exp $
 *
 * Description: This file contains test routines for VLAN objects
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlantest.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains test routines for VLAN objects*/
/*****************************************************************************/
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"
#include "cli.h"
#include "vcm.h"

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qBaseTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qBaseTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qForwardAllRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                testValFsDot1qForwardAllRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qForwardAllRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qVlanIndex,
                                     INT4 i4TestValFsDot1qForwardAllRowStatus)
{
#ifdef GARP_EXTENDED_FILTER
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qForwardAllRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsDot1qForwardAllRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VlanId < VLAN_DEFAULT_PORT_VID) || (VlanId >= VLAN_MGMT_PORT_VID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);
    if (pCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4TestValFsDot1qForwardAllRowStatus);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qForwardAllStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qForwardAllStatusTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qForwardAllPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                testValFsDot1qForwardAllPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qForwardAllPort (UINT4 *pu4ErrorCode,
                                INT4 i4FsDot1qVlanContextId,
                                UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                                INT4 i4TestValFsDot1qForwardAllPort)
{
#ifdef GARP_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VlanId < VLAN_DEFAULT_PORT_VID) || (VlanId >= VLAN_MGMT_PORT_VID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qForwardAllPort < VLAN_ADD_MEMBER_PORT) ||
        (i4TestValFsDot1qForwardAllPort > VLAN_DEL_FORBIDDEN_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);
    if (pCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (i4TestValFsDot1qForwardAllPort);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qForwardAllPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qForwardAllPortConfigTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qForwardUnregRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                testValFsDot1qForwardUnregRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qForwardUnregRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       INT4
                                       i4TestValFsDot1qForwardUnregRowStatus)
{
#ifdef GARP_EXTENDED_FILTER
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qForwardUnregRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsDot1qForwardUnregRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VlanId < VLAN_DEFAULT_PORT_VID) || (VlanId >= VLAN_MGMT_PORT_VID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);
    if (pCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4TestValFsDot1qForwardUnregRowStatus);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qForwardUnregStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qForwardUnregStatusTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qForwardUnregPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                testValFsDot1qForwardUnregPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qForwardUnregPort (UINT4 *pu4ErrorCode,
                                  INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qVlanIndex,
                                  INT4 i4FsDot1qTpPort,
                                  INT4 i4TestValFsDot1qForwardUnregPort)
{
#ifdef GARP_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VlanId < VLAN_DEFAULT_PORT_VID) || (VlanId >= VLAN_MGMT_PORT_VID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qForwardUnregPort < VLAN_ADD_MEMBER_PORT) ||
        (i4TestValFsDot1qForwardUnregPort > VLAN_DEL_FORBIDDEN_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);
    if (pCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (i4TestValFsDot1qForwardUnregPort);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qForwardUnregPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qForwardUnregPortConfigTable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qStaticUnicastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                

                The Object 
                testValFsDot1qStaticUnicastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qStaticUnicastRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDot1qVlanContextId,
                                        UINT4 u4FsDot1qFdbId,
                                        tMacAddr FsDot1qStaticUnicastAddress,
                                        INT4 i4FsDot1qStaticUnicastReceivePort,
                                        INT4
                                        i4TestValFsDot1qStaticUnicastRowStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT4               u4FidIndex;
    UINT1               u1VlanLearningType;
    tMacAddr            MacAddr;

    VLAN_PERF_MARK_START_TIME ();

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qStaticUnicastRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsDot1qStaticUnicastRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (&MacAddr, 0, sizeof (tMacAddr));

    if (VLAN_MEMCMP (FsDot1qStaticUnicastAddress,
                     MacAddr, sizeof (tMacAddr)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (VLAN_IS_MCASTADDR (FsDot1qStaticUnicastAddress) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VlanIsValidUcastAddr (FsDot1qStaticUnicastAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_UCAST_MAC_ERR);
        return SNMP_FAILURE;
    }

    /* If it is shared VLAN learning it can be only
     * the shared default fdb id.*/
    u1VlanLearningType = VlanGetVlanLearningMode ();

    if ((u1VlanLearningType == VLAN_SHARED_LEARNING) &&
        (u4FsDot1qFdbId != VLAN_SHARED_DEF_FDBID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* The fdb Id should be allocated previusly to a VLAN id
     * to add an unicast entry based on this fdb id.*/

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();

    VLAN_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qStaticUnicastStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                

                The Object 
                testValFsDot1qStaticUnicastStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qStaticUnicastStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qFdbId,
                                     tMacAddr FsDot1qStaticUnicastAddress,
                                     INT4 i4FsDot1qStaticUnicastReceivePort,
                                     INT4 i4TestValFsDot1qStaticUnicastStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    VLAN_PERF_MARK_START_TIME ();

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qStaticUnicastStatus (pu4ErrorCode,
                                                  u4FsDot1qFdbId,
                                                  FsDot1qStaticUnicastAddress,
                                                  u2LocalPortId,
                                                  i4TestValFsDot1qStaticUnicastStatus);

    VlanReleaseContext ();

    VLAN_PERF_MARK_END_TIME ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qStaticUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qStaticUnicastTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qStaticAllowedIsMember
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
                

                The Object 
                testValFsDot1qStaticAllowedIsMember
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qStaticAllowedIsMember (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qFdbId,
                                       tMacAddr FsDot1qStaticUnicastAddress,
                                       INT4 i4FsDot1qStaticUnicastReceivePort,
                                       INT4 i4FsDot1qTpPort,
                                       INT4
                                       i4TestValFsDot1qStaticAllowedIsMember)
{
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    UINT2               u2LocalPortId;
    UINT2               u2RcvLocalPortId;
    UINT4               u4FidIndex;
    UINT1               u1VlanLearningType;
    UINT1               u1Result = VLAN_FALSE;
    tMacAddr            MacAddr;
    tVlanId             VlanId = 0;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;

    VLAN_PERF_MARK_START_TIME ();

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qStaticAllowedIsMember != VLAN_SNMP_TRUE) &&
        (i4TestValFsDot1qStaticAllowedIsMember != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (&MacAddr, 0, sizeof (tMacAddr));

    if (VLAN_MEMCMP (FsDot1qStaticUnicastAddress,
                     MacAddr, sizeof (tMacAddr)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (VLAN_IS_MCASTADDR (FsDot1qStaticUnicastAddress) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (i4FsDot1qStaticUnicastReceivePort == i4FsDot1qTpPort)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_INCONSISTENT_RECEIVEPORT_ERR);
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VlanIsValidUcastAddr (FsDot1qStaticUnicastAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_UCAST_MAC_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* If it is shared VLAN learning it can be only
     * the shared default fdb id.*/
    u1VlanLearningType = VlanGetVlanLearningMode ();

    if ((u1VlanLearningType == VLAN_SHARED_LEARNING) &&
        (u4FsDot1qFdbId != VLAN_SHARED_DEF_FDBID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* The fdb Id should be allocated previusly to a VLAN id
     * to add an unicast entry based on this fdb id.*/

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanGetVlanIdFromFdbId (u4FsDot1qFdbId, &VlanId);
    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_STATIC_ENTRY_NOT_PRESENT);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VLAN_IS_EGRESS_PORT (pStVlanEntry, u2LocalPortId, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_IS_LOGICAL_BIT_LIST_SET (pVlanEntry->TrunkPorts, u2LocalPortId,
                                      CONTEXT_PORT_LIST_SIZE, u1Result);
        if (u1Result == VLAN_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_MCAST_EGR_PORTLIST_SUBSET_ERR);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    VlanReleaseContext ();
    VLAN_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qStaticAllowedToGoTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qStaticMulticastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                

                The Object 
                testValFsDot1qStaticMulticastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qStaticMulticastRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDot1qVlanContextId,
                                          UINT4 u4FsDot1qVlanIndex,
                                          tMacAddr
                                          FsDot1qStaticMulticastAddress,
                                          INT4
                                          i4FsDot1qStaticMulticastReceivePort,
                                          INT4
                                          i4TestValFsDot1qStaticMulticastRowStatus)
{
    UINT4               u4ContextId;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qStaticMulticastRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsDot1qStaticMulticastRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanIsValidMcastAddr (FsDot1qStaticMulticastAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_VLAN_MAC_STATIC_ADDR_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (u2LocalPortId != 0)
    {
        if (u2LocalPortId >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        if ((VLAN_GET_PORT_ENTRY (u2LocalPortId)) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qStaticMulticastStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                

                The Object 
                testValFsDot1qStaticMulticastStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qStaticMulticastStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       tMacAddr FsDot1qStaticMulticastAddress,
                                       INT4 i4FsDot1qStaticMulticastReceivePort,
                                       INT4
                                       i4TestValFsDot1qStaticMulticastStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qStaticMulticastStatus (pu4ErrorCode,
                                                    u4FsDot1qVlanIndex,
                                                    FsDot1qStaticMulticastAddress,
                                                    u2LocalPortId,
                                                    i4TestValFsDot1qStaticMulticastStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qStaticMulticastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qStaticMulticastTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qStaticMcastPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
                

                The Object 
                testValFsDot1qStaticMcastPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qStaticMcastPort (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot1qVlanContextId,
                                 UINT4 u4FsDot1qVlanIndex,
                                 tMacAddr FsDot1qStaticMulticastAddress,
                                 INT4 i4FsDot1qStaticMulticastReceivePort,
                                 INT4 i4FsDot1qTpPort,
                                 INT4 i4TestValFsDot1qStaticMcastPort)
{
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;
    UINT2               u2RcvLocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qStaticMcastPort < VLAN_ADD_MEMBER_PORT) ||
        (i4TestValFsDot1qStaticMcastPort > VLAN_DEL_FORBIDDEN_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanIsValidMcastAddr (FsDot1qStaticMulticastAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_VLAN_MAC_STATIC_ADDR_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VLAN_IS_EGRESS_PORT (pStVlanEntry, u2LocalPortId, u1Result);

    if ((u1Result == VLAN_FALSE) &&
        (i4TestValFsDot1qStaticMcastPort == VLAN_ADD_MEMBER_PORT))
    {
        VLAN_IS_CURR_TRUNK_PORT (pVlanEntry, u2LocalPortId, u1Result);
        if (u1Result == VLAN_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_MCAST_EGR_PORTLIST_SUBSET_ERR);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    if (u2LocalPortId != 0)
    {
        if (u2LocalPortId >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        if ((VLAN_GET_PORT_ENTRY (u2LocalPortId)) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qStaticMcastPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qStaticMcastPortTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qVlanStaticName
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                testValFsDot1qVlanStaticName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qVlanStaticName (UINT4 *pu4ErrorCode,
                                INT4 i4FsDot1qVlanContextId,
                                UINT4 u4FsDot1qVlanIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsDot1qVlanStaticName)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qVlanStaticName (pu4ErrorCode,
                                             u4FsDot1qVlanIndex,
                                             pTestValFsDot1qVlanStaticName);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qVlanStaticRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                testValFsDot1qVlanStaticRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qVlanStaticRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qVlanIndex,
                                     INT4 i4TestValFsDot1qVlanStaticRowStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qVlanStaticRowStatus (pu4ErrorCode,
                                                  u4FsDot1qVlanIndex,
                                                  i4TestValFsDot1qVlanStaticRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qVlanStaticTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qVlanStaticTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qVlanStaticPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                testValFsDot1qVlanStaticPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qVlanStaticPort (UINT4 *pu4ErrorCode,
                                INT4 i4FsDot1qVlanContextId,
                                UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                                INT4 i4TestValFsDot1qVlanStaticPort)
{
    UINT1              *pAddedPortList = NULL;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    UINT4               u4ContextId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pPbPortEntry = NULL;
    UINT2               u2LocalPortId;
    UINT1               u1PortType = VLAN_ACCESS_PORT;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    VLAN_PBB_PERF_MARK_START_TIME ();
    VLAN_PBB_PERF_RESET (gu4DelVlanIsidTimeTaken);
    if ((i4TestValFsDot1qVlanStaticPort < VLAN_ADD_TAGGED_PORT) ||
        (i4TestValFsDot1qVlanStaticPort > VLAN_DEL_ST_FORBIDDEN_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDot1qVlanStaticPort == VLAN_ADD_TAGGED_PORT) ||
        (i4TestValFsDot1qVlanStaticPort == VLAN_ADD_UNTAGGED_PORT))
    {
        VLAN_IS_FORBIDDEN_PORT (pStVlanEntry, u2LocalPortId, u1Result);
    }

    if (i4TestValFsDot1qVlanStaticPort == VLAN_ADD_ST_FORBIDDEN_PORT)
    {
        VLAN_IS_EGRESS_PORT (pStVlanEntry, u2LocalPortId, u1Result);
    }

    if (u1Result == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_OVERLAP_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    /*if the trunk port is tried to be removed from the VLAN, it is prevented */
    if ((pVlanPortEntry->u1PortType == VLAN_TRUNK_PORT) &&
        (i4TestValFsDot1qVlanStaticPort == VLAN_DEL_TAGGED_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_TRUNK_UNTAGGED_PORT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((pVlanPortEntry->u1PortType == VLAN_TRUNK_PORT) &&
        (i4TestValFsDot1qVlanStaticPort == VLAN_ADD_UNTAGGED_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_TRUNK_UNTAGGED_PORT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* In case Bridge Mode is PBB, then CBP can always be
       set as Untagged Member Port */
    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
    {
        if ((i4TestValFsDot1qVlanStaticPort == VLAN_ADD_TAGGED_PORT) &&
            (pVlanPortEntry->pVlanPbPortEntry != NULL))
        {
            if (pVlanPortEntry->pVlanPbPortEntry->
                u1PbPortType == VLAN_CUSTOMER_BACKBONE_PORT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
        }
    }

    if ((i4TestValFsDot1qVlanStaticPort == VLAN_ADD_TAGGED_PORT) &&
        (pVlanPortEntry->u1PortType == u1PortType))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pAddedPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddedPortList == NULL)
    {
        VlanReleaseContext ();
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhTestv2FsDot1qVlanStaticPort: Error in allocating memory "
                  "for pAddedPortList\r\n");
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (pAddedPortList, 0, sizeof (tLocalPortList));

    VLAN_SET_MEMBER_PORT (pAddedPortList, u2LocalPortId);

    if (VlanVcmSispIsPortVlanMappingValid (u4ContextId,
                                           (tVlanId) u4FsDot1qVlanIndex,
                                           pAddedPortList) == VCM_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        UtilPlstReleaseLocalPortList (pAddedPortList);
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if ((pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
            || (pStVlanEntry->u1RowStatus == VLAN_NOT_IN_SERVICE))
        {
            if (VlanPbCheckVlanServiceType ((tVlanId) u4FsDot1qVlanIndex,
                                            pAddedPortList) == VLAN_FAILURE)
            {
                CLI_SET_ERR (CLI_VLAN_PB_SERV_TYPE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pAddedPortList);
                return SNMP_FAILURE;
            }
        }
        pPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPortId);

        if (pPbPortEntry != NULL)
        {
            if ((pPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT) &&
                (i4TestValFsDot1qVlanStaticPort == VLAN_ADD_UNTAGGED_PORT))
            {
                /* PNP is present in the untagged list. */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_PB_PNP_UNTAG_ERR);
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pAddedPortList);
                return SNMP_FAILURE;
            }
        }

    }

    if ((i4TestValFsDot1qVlanStaticPort == VLAN_ADD_TAGGED_PORT) ||
        (i4TestValFsDot1qVlanStaticPort == VLAN_ADD_UNTAGGED_PORT))
    {
        /* Check for Spanning tree sisp port restriction */
        if (VlanMstSispValidateInstRestriction ((UINT4) i4FsDot1qVlanContextId,
                                                (UINT4) i4FsDot1qTpPort,
                                                (tVlanId) u4FsDot1qVlanIndex)
            != MST_SUCCESS)
        {
            CLI_SET_ERR (CLI_VLAN_MSTI_MAP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pAddedPortList);
            return SNMP_FAILURE;
        }
    }

    /* Check the member port eligibility for pvlans */
    EgressPorts.pu1_OctetList = pAddedPortList;
    EgressPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanTestConfigMembersOnPvlan ((tVlanId) u4FsDot1qVlanIndex,
                                      pu4ErrorCode,
                                      &EgressPorts) == VLAN_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pAddedPortList);
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
    VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);
    UtilPlstReleaseLocalPortList (pAddedPortList);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qVlanStaticPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qVlanStaticPortConfigTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPvid
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPvid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPvid (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                      UINT4 u4TestValFsDot1qPvid)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qPvid (pu4ErrorCode,
                                   (INT4) u2LocalPortId, u4TestValFsDot1qPvid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPortAcceptableFrameTypes
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPortAcceptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPortAcceptableFrameTypes (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDot1dBasePort,
                                          INT4
                                          i4TestValFsDot1qPortAcceptableFrameTypes)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qPortAcceptableFrameTypes (pu4ErrorCode,
                                                       (INT4) u2LocalPortId,
                                                       i4TestValFsDot1qPortAcceptableFrameTypes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPortIngressFiltering
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPortIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPortIngressFiltering (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dBasePort,
                                      INT4 i4TestValFsDot1qPortIngressFiltering)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qPortIngressFiltering (pu4ErrorCode,
                                                   (INT4) u2LocalPortId,
                                                   i4TestValFsDot1qPortIngressFiltering);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qPortVlanTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qPortVlanTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qConstraintType
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet

                The Object 
                testValFsDot1qConstraintType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qConstraintType (UINT4 *pu4ErrorCode,
                                INT4 i4FsDot1qVlanContextId,
                                UINT4 u4FsDot1qConstraintVlan,
                                INT4 i4FsDot1qConstraintSet,
                                INT4 i4TestValFsDot1qConstraintType)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qConstraintType (pu4ErrorCode,
                                             u4FsDot1qConstraintVlan,
                                             i4FsDot1qConstraintSet,
                                             i4TestValFsDot1qConstraintType);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qConstraintStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet

                The Object 
                testValFsDot1qConstraintStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qConstraintStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qConstraintVlan,
                                  INT4 i4FsDot1qConstraintSet,
                                  INT4 i4TestValFsDot1qConstraintStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qConstraintStatus (pu4ErrorCode,
                                               u4FsDot1qConstraintVlan,
                                               i4FsDot1qConstraintSet,
                                               i4TestValFsDot1qConstraintStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qLearningConstraintsTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qLearningConstraintsTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qConstraintSetDefault
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                testValFsDot1qConstraintSetDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qConstraintSetDefault (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1qVlanContextId,
                                      INT4 i4TestValFsDot1qConstraintSetDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qConstraintSetDefault (pu4ErrorCode,
                                                   i4TestValFsDot1qConstraintSetDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qConstraintTypeDefault
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                testValFsDot1qConstraintTypeDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qConstraintTypeDefault (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1qVlanContextId,
                                       INT4
                                       i4TestValFsDot1qConstraintTypeDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qConstraintTypeDefault (pu4ErrorCode,
                                                    i4TestValFsDot1qConstraintTypeDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1qConstraintDefaultTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1qConstraintDefaultTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1vProtocolGroupId
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue

                The Object 
                testValFsDot1vProtocolGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1vProtocolGroupId (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot1qVlanContextId,
                                 INT4 i4FsDot1vProtocolTemplateFrameType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsDot1vProtocolTemplateProtocolValue,
                                 INT4 i4TestValFsDot1vProtocolGroupId)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1vProtocolGroupId (pu4ErrorCode,
                                              i4FsDot1vProtocolTemplateFrameType,
                                              pFsDot1vProtocolTemplateProtocolValue,
                                              i4TestValFsDot1vProtocolGroupId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1vProtocolGroupRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue

                The Object 
                testValFsDot1vProtocolGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1vProtocolGroupRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDot1qVlanContextId,
                                        INT4 i4FsDot1vProtocolTemplateFrameType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsDot1vProtocolTemplateProtocolValue,
                                        INT4
                                        i4TestValFsDot1vProtocolGroupRowStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1vProtocolGroupRowStatus (pu4ErrorCode,
                                                     i4FsDot1vProtocolTemplateFrameType,
                                                     pFsDot1vProtocolTemplateProtocolValue,
                                                     i4TestValFsDot1vProtocolGroupRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1vProtocolGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1vProtocolGroupTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1vProtocolPortGroupVid
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId

                The Object 
                testValFsDot1vProtocolPortGroupVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1vProtocolPortGroupVid (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dBasePort,
                                      INT4 i4FsDot1vProtocolPortGroupId,
                                      INT4 i4TestValFsDot1vProtocolPortGroupVid)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1vProtocolPortGroupVid (pu4ErrorCode,
                                                   (INT4) u2LocalPortId,
                                                   i4FsDot1vProtocolPortGroupId,
                                                   i4TestValFsDot1vProtocolPortGroupVid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1vProtocolPortRowStatus
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId

                The Object 
                testValFsDot1vProtocolPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1vProtocolPortRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1dBasePort,
                                       INT4 i4FsDot1vProtocolPortGroupId,
                                       INT4
                                       i4TestValFsDot1vProtocolPortRowStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1vProtocolPortRowStatus (pu4ErrorCode,
                                                    (INT4) u2LocalPortId,
                                                    i4FsDot1vProtocolPortGroupId,
                                                    i4TestValFsDot1vProtocolPortRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1vProtocolPortTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1vProtocolPortTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/************************** Proprietary MIB Set Routines ********************/

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsMIDot1qFutureVlanGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanGlobalTrace (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsMIDot1qFutureVlanGlobalTrace)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIDot1qFutureVlanGlobalTrace != VLAN_ENABLED) &&
        (i4TestValFsMIDot1qFutureVlanGlobalTrace != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIDot1qFutureVlanGlobalTrace);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanGlobalTrace (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIDot1qFutureVlanContextId,
                                    INT4 i4TestValFsMIDot1qFutureVlanStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanStatus (pu4ErrorCode,
                                               i4TestValFsMIDot1qFutureVlanStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanMacBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanMacBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4FsMIDot1qFutureVlanContextId,
                                                INT4
                                                i4TestValFsMIDot1qFutureVlanMacBasedOnAllPorts)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts (pu4ErrorCode,
                                                           i4TestValFsMIDot1qFutureVlanMacBasedOnAllPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortProtoBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortProtoBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                                      INT4
                                                      i4FsMIDot1qFutureVlanContextId,
                                                      INT4
                                                      i4TestValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts
        (pu4ErrorCode, i4TestValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanShutdownStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanShutdownStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanContextId,
                                            INT4
                                            i4TestValFsMIDot1qFutureVlanShutdownStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanShutdownStatus (pu4ErrorCode,
                                                       i4TestValFsMIDot1qFutureVlanShutdownStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanDebug
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanDebug (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDot1qFutureVlanContextId,
                                   INT4 i4TestValFsMIDot1qFutureVlanDebug)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanDebug (pu4ErrorCode,
                                              i4TestValFsMIDot1qFutureVlanDebug);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanLearningMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanLearningMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanLearningMode (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIDot1qFutureVlanContextId,
                                          INT4
                                          i4TestValFsMIDot1qFutureVlanLearningMode)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanLearningMode (pu4ErrorCode,
                                                     i4TestValFsMIDot1qFutureVlanLearningMode);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanHybridTypeDefault
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanHybridTypeDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanHybridTypeDefault (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4FsMIDot1qFutureVlanContextId,
                                               INT4
                                               i4TestValFsMIDot1qFutureVlanHybridTypeDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanHybridTypeDefault (pu4ErrorCode,
                                                          i4TestValFsMIDot1qFutureVlanHybridTypeDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortType (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIDot1qFutureVlanPort,
                                      INT4 i4TestValFsMIDot1qFutureVlanPortType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortType (pu4ErrorCode,
                                                 (INT4) u2LocalPortId,
                                                 i4TestValFsMIDot1qFutureVlanPortType);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortMacBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanPortMacBasedClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortMacBasedClassification (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4FsMIDot1qFutureVlanPort,
                                                        INT4
                                                        i4TestValFsMIDot1qFutureVlanPortMacBasedClassification)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortMacBasedClassification
        (pu4ErrorCode, (INT4) u2LocalPortId,
         i4TestValFsMIDot1qFutureVlanPortMacBasedClassification);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortPortProtoBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanPortPortProtoBasedClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortPortProtoBasedClassification (UINT4
                                                              *pu4ErrorCode,
                                                              INT4
                                                              i4FsMIDot1qFutureVlanPort,
                                                              INT4
                                                              i4TestValFsMIDot1qFutureVlanPortPortProtoBasedClassification)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification
        (pu4ErrorCode, (INT4) u2LocalPortId,
         i4TestValFsMIDot1qFutureVlanPortPortProtoBasedClassification);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanFilteringUtilityCriteria
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanFilteringUtilityCriteria
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanFilteringUtilityCriteria (UINT4 *pu4ErrorCode,
                                                      INT4
                                                      i4FsMIDot1qFutureVlanPort,
                                                      INT4
                                                      i4TestValFsMIDot1qFutureVlanFilteringUtilityCriteria)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria
        (pu4ErrorCode, u2LocalPortId,
         i4TestValFsMIDot1qFutureVlanFilteringUtilityCriteria);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortMacMapVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                testValFsMIDot1qFutureVlanPortMacMapVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapVid (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIDot1qFutureVlanPort,
                                           tMacAddr
                                           FsMIDot1qFutureVlanPortMacMapAddr,
                                           INT4
                                           i4TestValFsMIDot1qFutureVlanPortMacMapVid)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapVid
        (pu4ErrorCode, (INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         i4TestValFsMIDot1qFutureVlanPortMacMapVid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortMacMapName
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                testValFsMIDot1qFutureVlanPortMacMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapName (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanPort,
                                            tMacAddr
                                            FsMIDot1qFutureVlanPortMacMapAddr,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValFsMIDot1qFutureVlanPortMacMapName)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapName
        (pu4ErrorCode, (INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         pTestValFsMIDot1qFutureVlanPortMacMapName);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortMacMapMcastBcastOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                testValFsMIDot1qFutureVlanPortMacMapMcastBcastOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapMcastBcastOption (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4FsMIDot1qFutureVlanPort,
                                                        tMacAddr
                                                        FsMIDot1qFutureVlanPortMacMapAddr,
                                                        INT4
                                                        i4TestValFsMIDot1qFutureVlanPortMacMapMcastBcastOption)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption
        (pu4ErrorCode, (INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         i4TestValFsMIDot1qFutureVlanPortMacMapMcastBcastOption);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortMacMapRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                testValFsMIDot1qFutureVlanPortMacMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapRowStatus (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsMIDot1qFutureVlanPort,
                                                 tMacAddr
                                                 FsMIDot1qFutureVlanPortMacMapAddr,
                                                 INT4
                                                 i4TestValFsMIDot1qFutureVlanPortMacMapRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapRowStatus
        (pu4ErrorCode, (INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         i4TestValFsMIDot1qFutureVlanPortMacMapRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanPortMacMapTable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanFid
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object 
                testValFsMIDot1qFutureVlanFid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanFid (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIDot1qFutureVlanContextId,
                                 UINT4 u4FsMIDot1qFutureVlanIndex,
                                 UINT4 u4TestValFsMIDot1qFutureVlanFid)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanFid (pu4ErrorCode,
                                            u4FsMIDot1qFutureVlanIndex,
                                            u4TestValFsMIDot1qFutureVlanFid);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanFidMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanFidMapTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanBridgeMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanBridgeMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanBridgeMode (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIDot1qFutureVlanContextId,
                                        INT4
                                        i4TestValFsMIDot1qFutureVlanBridgeMode)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (i4TestValFsMIDot1qFutureVlanBridgeMode);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanTunnelBpduPri
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanTunnelBpduPri
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanTunnelBpduPri (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIDot1qFutureVlanContextId,
                                           INT4
                                           i4TestValFsMIDot1qFutureVlanTunnelBpduPri)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (i4TestValFsMIDot1qFutureVlanTunnelBpduPri);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanTunnelConfigTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanTunnelConfigTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanTunnelStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanTunnelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanTunnelStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIDot1qFutureVlanPort,
                                          INT4
                                          i4TestValFsMIDot1qFutureVlanTunnelStatus)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4TestValFsMIDot1qFutureVlanTunnelStatus);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanTunnelTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanTunnelTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanTunnelStpPDUs
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanTunnelStpPDUs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanTunnelStpPDUs (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIDot1qFutureVlanPort,
                                           INT4
                                           i4TestValFsMIDot1qFutureVlanTunnelStpPDUs)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4TestValFsMIDot1qFutureVlanTunnelStpPDUs);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanTunnelGvrpPDUs
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanTunnelGvrpPDUs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanTunnelGvrpPDUs (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanPort,
                                            INT4 i4TestValTunnelGvrpPDUs)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4TestValTunnelGvrpPDUs);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanTunnelIgmpPkts
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                testValFsMIDot1qFutureVlanTunnelIgmpPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanTunnelIgmpPkts (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanPort,
                                            INT4 i4TestValTunnelIgmpPkts)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4TestValTunnelIgmpPkts);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanCounterStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                testValFsMIDot1qFutureVlanCounterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanCounterStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIDot1qFutureVlanContextId,
                                           UINT4 u4FsMIDot1qFutureVlanIndex,
                                           INT4
                                           i4TestValFsMIDot1qFutureVlanCounterStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanCounterStatus
        (pu4ErrorCode, u4FsMIDot1qFutureVlanIndex,
         i4TestValFsMIDot1qFutureVlanCounterStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanCounterTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanCounterTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                                tSnmpIndexList * pSnmpIndexList,
                                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/************************** fsmsbext MIB Test Routines **********************/

/****************************************************************************
 Function    :  nmhTestv2FsDot1dTrafficClassesEnabled
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                testValFsDot1dTrafficClassesEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dTrafficClassesEnabled (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1dBridgeContextId,
                                       INT4
                                       i4TestValFsDot1dTrafficClassesEnabled)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1dBridgeContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dTrafficClassesEnabled (pu4ErrorCode,
                                                    i4TestValFsDot1dTrafficClassesEnabled);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortDefaultUserPriority
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortDefaultUserPriority (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDot1dBasePort,
                                         INT4
                                         i4TestValFsDot1dPortDefaultUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortDefaultUserPriority (pu4ErrorCode,
                                                      (INT4) u2LocalPortId,
                                                      i4TestValFsDot1dPortDefaultUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortNumTrafficClasses
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortNumTrafficClasses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortNumTrafficClasses (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1dBasePort,
                                       INT4
                                       i4TestValFsDot1dPortNumTrafficClasses)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortNumTrafficClasses (pu4ErrorCode,
                                                    (INT4) u2LocalPortId,
                                                    i4TestValFsDot1dPortNumTrafficClasses);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dPortPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dPortPriorityTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dRegenUserPriority
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dUserPriority

                The Object
                testValFsDot1dRegenUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dRegenUserPriority (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                   INT4 i4FsDot1dUserPriority,
                                   INT4 i4TestValFsDot1dRegenUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dRegenUserPriority (pu4ErrorCode,
                                                (INT4) u2LocalPortId,
                                                i4FsDot1dUserPriority,
                                                i4TestValFsDot1dRegenUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dUserPriorityRegenTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dUserPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dUserPriorityRegenTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dTrafficClass
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dTrafficClassPriority

                The Object
                testValFsDot1dTrafficClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dTrafficClass (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                              INT4 i4FsDot1dTrafficClassPriority,
                              INT4 i4TestValFsDot1dTrafficClass)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dTrafficClass (pu4ErrorCode,
                                           (INT4) u2LocalPortId,
                                           i4FsDot1dTrafficClassPriority,
                                           i4TestValFsDot1dTrafficClass);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dTrafficClassTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dTrafficClassPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dTrafficClassTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanUnicastMacLimit
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                testValFsMIDot1qFutureVlanUnicastMacLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanUnicastMacLimit (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIDot1qFutureVlanContextId,
                                             UINT4 u4FsMIDot1qFutureVlanIndex,
                                             UINT4
                                             u4TestValFsMIDot1qFutureVlanUnicastMacLimit)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanUnicastMacLimit (pu4ErrorCode,
                                                        u4FsMIDot1qFutureVlanIndex,
                                                        u4TestValFsMIDot1qFutureVlanUnicastMacLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanAdminMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                testValFsMIDot1qFutureVlanAdminMacLearningStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIDot1qFutureVlanAdminMacLearningStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     INT4 i4TestValFsMIDot1qFutureVlanAdminMacLearningStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanAdminMacLearningStatus (pu4ErrorCode,
                                                               u4FsMIDot1qFutureVlanIndex,
                                                               i4TestValFsMIDot1qFutureVlanAdminMacLearningStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanUnicastMacControlTable (UINT4 *pu4ErrorCode,
                                                   tSnmpIndexList *
                                                   pSnmpIndexList,
                                                   tSNMP_VAR_BIND *
                                                   pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanWildCardRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress

                The Object 
                testValFsMIDot1qFutureVlanWildCardRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIDot1qFutureVlanWildCardRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsDot1qVlanContextId,
     tMacAddr WildCardMacAddress, INT4 i4TestValWildCardRowStatus)
{
    INT1                i1RetVal;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValWildCardRowStatus < VLAN_ACTIVE) ||
        (i4TestValWildCardRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanWildCardRowStatus
        (pu4ErrorCode, WildCardMacAddress, i4TestValWildCardRowStatus);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanWildCardTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanWildCardTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanIsWildCardEgressPort
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort

                The Object 
                testValFsMIDot1qFutureVlanIsWildCardEgressPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIDot1qFutureVlanIsWildCardEgressPort
    (UINT4 *pu4ErrorCode, INT4 i4FsDot1qVlanContextId,
     tMacAddr WildCardMacAddress, INT4 i4FsDot1qTpPort,
     INT4 i4TestValIsWildCardEgressPort)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if ((i4FsDot1qVlanContextId < 0) ||
        ((UINT4) i4FsDot1qVlanContextId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIsWildCardEgressPort != VLAN_SNMP_TRUE) &&
        (i4TestValIsWildCardEgressPort != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_WILDCARD_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanWildCardPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanWildCardPortTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortProtected
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortProtected
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortProtected (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIDot1qFutureVlanPort,
                                           INT4
                                           i4TestValFsMIDot1qFutureVlanPortProtected)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortProtected
        (pu4ErrorCode, (INT4) u2LocalPortId,
         i4TestValFsMIDot1qFutureVlanPortProtected);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanPortTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureUnicastMacLearningLimit
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureUnicastMacLearningLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureUnicastMacLearningLimit (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4FsMIDot1qFutureVlanContextId,
                                                 UINT4
                                                 u4TestValFsMIDot1qFutureUnicastMacLearningLimit)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureUnicastMacLearningLimit (pu4ErrorCode,
                                                            u4TestValFsMIDot1qFutureUnicastMacLearningLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureBaseBridgeMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureBaseBridgeMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureBaseBridgeMode (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIDot1qFutureVlanContextId,
                                        INT4
                                        i4TestValFsMIDot1qFutureBaseBridgeMode)
{
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = (nmhTestv2Dot1qFutureVlanBaseBridgeMode (pu4ErrorCode,
                                                        i4TestValFsMIDot1qFutureBaseBridgeMode));

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanSubnetBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object                                                                         testValFsMIDot1qFutureVlanSubnetBasedOnAllPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanSubnetBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4FsMIDot1qFutureVlanContextId,
                                                   INT4
                                                   i4TestValFsMIDot1qFutureVlanSubnetBasedOnAllPorts)
{
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts (pu4ErrorCode,
                                                              i4TestValFsMIDot1qFutureVlanSubnetBasedOnAllPorts);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanGlobalMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                testValFsMIDot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanGlobalMacLearningStatus (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4FsMIDot1qFutureVlanContextId,
                                                     INT4
                                                     i4TestValFsMIDot1qFutureVlanGlobalMacLearningStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus (pu4ErrorCode,
                                                                i4TestValFsMIDot1qFutureVlanGlobalMacLearningStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria (UINT4 *pu4ErrorCode,
                                                            INT4
                                                            i4FsMIDot1qFutureVlanContextId,
                                                            INT4
                                                            i4TestValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria
        (pu4ErrorCode,
         i4TestValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object                                                                         testValFsMIDot1qFutureVlanPortSubnetBasedClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetBasedClassification (UINT4 *pu4ErrorCode,
                                                           INT4
                                                           i4FsMIDot1qFutureVlanPort,
                                                           INT4
                                                           i4TestValFsMIDot1qFutureVlanPortSubnetBasedClassification)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         i4TestValFsMIDot1qFutureVlanPortSubnetBasedClassification);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortUnicastMacLearning
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortUnicastMacLearning
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortUnicastMacLearning (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    INT4
                                                    i4TestValFsMIDot1qFutureVlanPortUnicastMacLearning)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortUnicastMacLearning (pu4ErrorCode,
                                                        (INT4) u2LocalPort,
                                                        i4TestValFsMIDot1qFutureVlanPortUnicastMacLearning);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortIngressEtherType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object                                                                                                                    testValFsMIDot1qFutureVlanPortIngressEtherType                                                                 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned                                                                                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)                                                                         SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortIngressEtherType (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4FsMIDot1qFutureVlanPort,
                                                  INT4
                                                  i4TestValFsMIDot1qFutureVlanPortIngressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortIngressEtherType (pu4ErrorCode,
                                                      (INT4) u2LocalPort,
                                                      i4TestValFsMIDot1qFutureVlanPortIngressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortEgressEtherType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortEgressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortEgressEtherType (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsMIDot1qFutureVlanPort,
                                                 INT4
                                                 i4TestValFsMIDot1qFutureVlanPortEgressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortEgressEtherType (pu4ErrorCode,
                                                     (INT4) u2LocalPort,
                                                     i4TestValFsMIDot1qFutureVlanPortEgressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortEgressTPIDType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortEgressTPIDType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortEgressTPIDType (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIDot1qFutureVlanPort,
                                                INT4
                                                i4TestValFsMIDot1qFutureVlanPortEgressTPIDType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortEgressTPIDType (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4TestValFsMIDot1qFutureVlanPortEgressTPIDType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID1
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortAllowableTPID1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID1 (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIDot1qFutureVlanPort,
                                                INT4
                                                i4TestValFsMIDot1qFutureVlanPortAllowableTPID1)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortAllowableTPID1 (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4TestValFsMIDot1qFutureVlanPortAllowableTPID1);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID2
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortAllowableTPID2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID2 (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIDot1qFutureVlanPort,
                                                INT4
                                                i4TestValFsMIDot1qFutureVlanPortAllowableTPID2)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortAllowableTPID2 (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4TestValFsMIDot1qFutureVlanPortAllowableTPID2);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID3
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortAllowableTPID3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID3 (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIDot1qFutureVlanPort,
                                                INT4
                                                i4TestValFsMIDot1qFutureVlanPortAllowableTPID3)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIDot1qFutureVlanPort <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortAllowableTPID3 (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4TestValFsMIDot1qFutureVlanPortAllowableTPID3);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanGlobalsTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanGlobalsTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureStaticConnectionIdentifier
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort

                The Object
                testValFsMIDot1qFutureStaticConnectionIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsMIDot1qFutureStaticConnectionIdentifier
    (UINT4 *pu4ErrorCode,
     INT4 i4FsDot1qVlanContextId,
     UINT4 u4FsDot1qFdbId,
     tMacAddr FsDot1qStaticUnicastAddress,
     INT4 i4FsDot1qStaticUnicastReceivePort,
     tMacAddr TestValFsMIDot1qFutureStaticConnectionIdentifier)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureStaticConnectionIdentifier
        (pu4ErrorCode,
         u4FsDot1qFdbId,
         FsDot1qStaticUnicastAddress,
         u2LocalPortId, TestValFsMIDot1qFutureStaticConnectionIdentifier);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureStaticUnicastExtnTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetMapVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                testValFsMIDot1qFutureVlanPortSubnetMapVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapVid (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIDot1qFutureVlanPort,
                                              UINT4
                                              u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                              INT4
                                              i4TestValFsMIDot1qFutureVlanPortSubnetMapVid)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortSubnetMapVid
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         i4TestValFsMIDot1qFutureVlanPortSubnetMapVid);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetMapARPOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                testValFsMIDot1qFutureVlanPortSubnetMapARPOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapARPOption (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    UINT4
                                                    u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                    INT4
                                                    i4TestValFsMIDot1qFutureVlanPortSubnetMapARPOption)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         i4TestValFsMIDot1qFutureVlanPortSubnetMapARPOption);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetMapRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                testValFsMIDot1qFutureVlanPortSubnetMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapRowStatus (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    UINT4
                                                    u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                    INT4
                                                    i4TestValFsMIDot1qFutureVlanPortSubnetMapRowStatus)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         i4TestValFsMIDot1qFutureVlanPortSubnetMapRowStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanPortSubnetMapTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureStVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureStVlanExtTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanSwStatsEnabled
 Input       :  The Indices

                The Object 
                testValFsMIDot1qFutureVlanSwStatsEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanSwStatsEnabled (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFsMIDot1qFutureVlanSwStatsEnabled)
{

    return ((nmhTestv2Dot1qFutureVlanSwStatsEnabled
             (pu4ErrorCode, i4TestValFsMIDot1qFutureVlanSwStatsEnabled)));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanSwStatsEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanSwStatsEnabled (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                testValFsMIDot1qFutureVlanPortSubnetMapExtVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtVid
    (UINT4 *pu4ErrorCode, INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4TestValFsMIDot1qFutureVlanPortSubnetMapExtVid)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         i4TestValFsMIDot1qFutureVlanPortSubnetMapExtVid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtARPOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                testValFsMIDot1qFutureVlanPortSubnetMapExtARPOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtARPOption
    (UINT4 *pu4ErrorCode, INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4TestValFsMIDot1qFutureVlanPortSubnetMapExtARPOption)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         i4TestValFsMIDot1qFutureVlanPortSubnetMapExtARPOption);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                testValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtRowStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4TestValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus
        (pu4ErrorCode,
         (INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         i4TestValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsMIDot1qFutureVlanPortSubnetMapExtTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanGlobalsFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                testValFsMIDot1qFutureVlanGlobalsFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanGlobalsFdbFlush (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIDot1qFutureVlanContextId,
                                             INT4
                                             i4TestValFsMIDot1qFutureVlanGlobalsFdbFlush)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VcmIsL2VcExist ((UINT4) i4FsMIDot1qFutureVlanContextId) != VCM_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanGlobalsFdbFlush
        (pu4ErrorCode, i4TestValFsMIDot1qFutureVlanGlobalsFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanUserDefinedTPID                                                                    Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                testValFsMIDot1qFutureVlanUserDefinedTPID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                                        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)                                                                         SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanUserDefinedTPID (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIDot1qFutureVlanContextId,
                                             INT4
                                             i4TestValFsMIDot1qFutureVlanUserDefinedTPID)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanUserDefinedTPID
        (pu4ErrorCode, i4TestValFsMIDot1qFutureVlanUserDefinedTPID);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanRemoteFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                testValFsMIDot1qFutureVlanRemoteFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanRemoteFdbFlush (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanContextId,
                                            INT4
                                            i4TestValFsMIDot1qFutureVlanRemoteFdbFlush)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanRemoteFdbFlush
        (pu4ErrorCode, i4TestValFsMIDot1qFutureVlanRemoteFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortFdbFlush (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIDot1qFutureVlanPort,
                                          INT4
                                          i4TestValFsMIDot1qFutureVlanPortFdbFlush)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if ((i4TestValFsMIDot1qFutureVlanPortFdbFlush != VLAN_TRUE) &&
        (i4TestValFsMIDot1qFutureVlanPortFdbFlush != VLAN_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPortId) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureStVlanFdbFlush
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object
                testValFsMIDot1qFutureStVlanFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureStVlanFdbFlush (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDot1qVlanContextId,
                                        UINT4 u4FsDot1qVlanIndex,
                                        INT4
                                        i4TestValFsMIDot1qFutureStVlanFdbFlush)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanPortFdbFlush
        (pu4ErrorCode, u4FsDot1qVlanIndex,
         i4TestValFsMIDot1qFutureStVlanFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureStVlanEgressEthertype
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object
                testValFsMIDot1qFutureStVlanEgressEthertype
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                                        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureStVlanEgressEthertype (UINT4 *pu4ErrorCode,
                                               INT4 i4FsDot1qVlanContextId,
                                               UINT4 u4FsDot1qVlanIndex,
                                               INT4
                                               i4TestValFsMIDot1qFutureStVlanEgressEthertype)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureStVlanEgressEthertype
        (pu4ErrorCode, u4FsDot1qVlanIndex,
         i4TestValFsMIDot1qFutureStVlanEgressEthertype);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFuturePortVlanFdbFlush
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object
                testValFsMIDot1qFuturePortVlanFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFuturePortVlanFdbFlush (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDot1qVlanContextId,
                                          UINT4 u4FsDot1qVlanIndex,
                                          INT4 i4FsDot1qTpPort,
                                          INT4
                                          i4TestValFsMIDot1qFuturePortVlanFdbFlush)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_ZERO;
    UINT2               u2LocalPortId = VLAN_ZERO;

    if ((i4TestValFsMIDot1qFuturePortVlanFdbFlush != VLAN_TRUE) &&
        (i4TestValFsMIDot1qFuturePortVlanFdbFlush != VLAN_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VLAN_GET_PORT_VLAN_MAP_ENTRY (u4FsDot1qVlanIndex, u2LocalPortId) ==
        NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFuturePortVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFuturePortVlanExtTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanLoopbackStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                testValFsMIDot1qFutureVlanLoopbackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanLoopbackStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanContextId,
                                            UINT4 u4FsMIDot1qFutureVlanIndex,
                                            INT4
                                            i4TestValFsMIDot1qFutureVlanLoopbackStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureVlanLoopbackStatus
        (pu4ErrorCode, u4FsMIDot1qFutureVlanIndex,
         i4TestValFsMIDot1qFutureVlanLoopbackStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureVlanLoopbackTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFuturePortPacketReflectionStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                The Object
                testValFsMIDot1qFuturePortPacketReflectionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFuturePortPacketReflectionStatus (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    INT4
                                                    i4TestValFsMIDot1qFuturePortPacketReflectionStatus)
{

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    /*Get context ID and Local port ID from interface index */
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFuturePortPacketReflectionStatus (pu4ErrorCode,
                                                               (INT4)
                                                               u2LocalPortId,
                                                               i4TestValFsMIDot1qFuturePortPacketReflectionStatus);

    if (i1RetVal == SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;

    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
