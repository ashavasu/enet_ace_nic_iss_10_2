/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbslw.c,v 1.83 2016/07/06 10:57:27 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"
# include  "fsm1adcli.h"
/* LOW LEVEL Routines for Table : Dot1adPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adPortTable
 Input       :  The Indices
                Dot1adPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */


INT1
nmhValidateIndexInstanceDot1adPortTable (INT4 i4Dot1adPortNum)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adPortTable
 Input       :  The Indices
                Dot1adPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adPortTable (INT4 *pi4Dot1adPortNum)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adPortTable (0, pi4Dot1adPortNum));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adPortTable
 Input       :  The Indices
                Dot1adPortNum
                nextDot1adPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adPortTable (INT4 i4Dot1adPortNum,
                                INT4 *pi4NextDot1adPortNum)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1adPortNum < 0) || (i4Dot1adPortNum > VLAN_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4Dot1adPortNum + 1); u2Port <= VLAN_MAX_PORTS;
         u2Port++)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry == NULL)
        {
            continue;
        }

        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

        if (pVlanPbPortEntry != NULL)
        {
            *pi4NextDot1adPortNum = (INT4) u2Port;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adPortPcpSelectionRow
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                retValDot1adPortPcpSelectionRow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adPortPcpSelectionRow (INT4 i4Dot1adPortNum,
                                 INT4 *pi4RetValDot1adPortPcpSelectionRow)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adPortPcpSelectionRow =
        (INT4) (pVlanPbPortEntry->u1PcpSelRow);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adPortUseDei
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                retValDot1adPortUseDei
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adPortUseDei (INT4 i4Dot1adPortNum, INT4 *pi4RetValDot1adPortUseDei)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adPortUseDei = (INT4) pVlanPbPortEntry->u1UseDei;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adPortReqDropEncoding
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                retValDot1adPortReqDropEncoding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adPortReqDropEncoding (INT4 i4Dot1adPortNum,
                                 INT4 *pi4RetValDot1adPortReqDropEncoding)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPortReqDropEncoding =
        (INT4) pVlanPbPortEntry->u1ReqDropEncoding;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adPortSVlanPriorityType
 Input       :  The Indices
                Dot1adPortNum

                The Object
                retValDot1adPortSVlanPriorityType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adPortSVlanPriorityType (INT4 i4Dot1adPortNum,
                                   INT4 *pi4RetValDot1adPortSVlanPriorityType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPortSVlanPriorityType =
        (INT4) pVlanPbPortEntry->u1SVlanPriorityType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adPortSVlanPriority
 Input       :  The Indices
                Dot1adPortNum

                The Object
                retValDot1adPortSVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adPortSVlanPriority (INT4 i4Dot1adPortNum,
                               INT4 *pi4RetValDot1adPortSVlanPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPortSVlanPriority =
        (INT4) pVlanPbPortEntry->u1SVlanPriority;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adPortPcpSelectionRow
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                setValDot1adPortPcpSelectionRow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adPortPcpSelectionRow (INT4 i4Dot1adPortNum,
                                 INT4 i4SetValDot1adPortPcpSelectionRow)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PcpSelRow != i4SetValDot1adPortPcpSelectionRow)
    {
        if (VlanHwSetPortPcpSelection (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_PHY_PORT
                                       (i4Dot1adPortNum),
                                       (UINT2)
                                       i4SetValDot1adPortPcpSelectionRow)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pVlanPbPortEntry->u1PcpSelRow =
            (UINT1) i4SetValDot1adPortPcpSelectionRow;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPortPcpSelectionRow, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4SetValDot1adPortPcpSelectionRow));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adPortUseDei
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                setValDot1adPortUseDei
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adPortUseDei (INT4 i4Dot1adPortNum, INT4 i4SetValDot1adPortUseDei)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1UseDei != i4SetValDot1adPortUseDei)
    {
        if (VlanHwSetPortUseDei (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (i4Dot1adPortNum),
                                 (UINT1) i4SetValDot1adPortUseDei) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        pVlanPbPortEntry->u1UseDei = (UINT1) i4SetValDot1adPortUseDei;
    }

    VlanL2IwfUpdatePortDeiBit (pVlanPortEntry->u4IfIndex,
                               i4SetValDot1adPortUseDei);

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPortUseDei, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4SetValDot1adPortUseDei));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adPortReqDropEncoding
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                setValDot1adPortReqDropEncoding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adPortReqDropEncoding (INT4 i4Dot1adPortNum,
                                 INT4 i4SetValDot1adPortReqDropEncoding)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pVlanPbPortEntry->u1ReqDropEncoding !=
        i4SetValDot1adPortReqDropEncoding)
    {
        if (VlanHwSetPortReqDropEncoding (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT
                                          (i4Dot1adPortNum),
                                          (UINT1)
                                          i4SetValDot1adPortReqDropEncoding)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        pVlanPbPortEntry->u1ReqDropEncoding =
            (UINT1) i4SetValDot1adPortReqDropEncoding;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPortReqDropEncoding, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4SetValDot1adPortReqDropEncoding));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adPortSVlanPriorityType
 Input       :  The Indices
                Dot1adPortNum

                The Object
                setValDot1adPortSVlanPriorityType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adPortSVlanPriorityType (INT4 i4Dot1adPortNum,
                                   INT4 i4SetValDot1adPortSVlanPriorityType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty PbPortProperty;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&PbPortProperty, 0, sizeof (tHwVlanPortProperty));
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1SVlanPriorityType ==
        i4SetValDot1adPortSVlanPriorityType)
    {
        return SNMP_SUCCESS;
    }
    PbPortProperty.u2OpCode = PB_PORT_PROPERTY_SVLAN_PRIORITY_TYPE;
    PbPortProperty.u1SVlanPriorityType =
        (UINT1) i4SetValDot1adPortSVlanPriorityType;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_PHY_PORT (i4Dot1adPortNum),
                               PbPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pVlanPbPortEntry->u1SVlanPriorityType =
        (UINT1) i4SetValDot1adPortSVlanPriorityType;
    /*Set Priority to default in case changing Type from FIXED to CPOY/NONE */
    pVlanPbPortEntry->u1SVlanPriority = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPortSVlanPriorityType,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4SetValDot1adPortSVlanPriorityType));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adPortSVlanPriority
 Input       :  The Indices
                Dot1adPortNum

                The Object
                setValDot1adPortSVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adPortSVlanPriority (INT4 i4Dot1adPortNum,
                               INT4 i4SetValDot1adPortSVlanPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty PbPortProperty;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&PbPortProperty, 0, sizeof (tHwVlanPortProperty));
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1SVlanPriority == i4SetValDot1adPortSVlanPriority)
    {
        return SNMP_SUCCESS;
    }

    PbPortProperty.u2OpCode = PB_PORT_PROPERTY_SVLAN_PRIORITY;
    PbPortProperty.u1SVlanPriority = (UINT1) i4SetValDot1adPortSVlanPriority;
    PbPortProperty.u1SVlanPriorityType =
        (UINT1) pVlanPbPortEntry->u1SVlanPriorityType;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_PHY_PORT (i4Dot1adPortNum),
                               PbPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry->u1SVlanPriority = (UINT1) i4SetValDot1adPortSVlanPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPortSVlanPriority, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4SetValDot1adPortSVlanPriority));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adPortPcpSelectionRow
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                testValDot1adPortPcpSelectionRow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adPortPcpSelectionRow (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                    INT4 i4TestValDot1adPortPcpSelectionRow)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PCP_ROW_VALID (i4TestValDot1adPortPcpSelectionRow) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PcpSelRow == i4TestValDot1adPortPcpSelectionRow)
    {
        return SNMP_SUCCESS;
    }

    /* For CEP and PCEP the PCP selection row is only 8P0D row */
    if (((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
         (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
        && (i4TestValDot1adPortPcpSelectionRow != VLAN_8P0D_SEL_ROW))
    {
        CLI_SET_ERR (CLI_PB_CEP_PCP_SEL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adPortUseDei
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                testValDot1adPortUseDei
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adPortUseDei (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                           INT4 i4TestValDot1adPortUseDei)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_USE_DEI_VALID (i4TestValDot1adPortUseDei) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* For CEP and PCEP the USE Dei is always false */
    if (((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
         (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
        && (i4TestValDot1adPortUseDei != VLAN_SNMP_FALSE))
    {
        CLI_SET_ERR (CLI_PB_CEP_USEDEI_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adPortReqDropEncoding
 Input       :  The Indices
                Dot1adPortNum

                The Object 
                testValDot1adPortReqDropEncoding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adPortReqDropEncoding (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                    INT4 i4TestValDot1adPortReqDropEncoding)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_REQ_DROP_ENCODING_VALID (i4TestValDot1adPortReqDropEncoding) !=
        VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1ReqDropEncoding
        == i4TestValDot1adPortReqDropEncoding)
    {
        return SNMP_SUCCESS;
    }

    /* For CEP, PCEP, the required drop encoding is always
     * false*/

    if (((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
         (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
        && (i4TestValDot1adPortReqDropEncoding != VLAN_SNMP_FALSE))
    {
        CLI_SET_ERR (CLI_PB_CEP_REQDROP_ENC_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adPortSVlanPriorityType
 Input       :  The Indices
                Dot1adPortNum

                The Object
                testValDot1adPortSVlanPriorityType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adPortSVlanPriorityType (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                      INT4 i4TestValDot1adPortSVlanPriorityType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_TYPE_VALID (i4TestValDot1adPortSVlanPriorityType) ==
        VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType != VLAN_CNP_PORTBASED_PORT) &&
        (pVlanPbPortEntry->u1PbPortType != VLAN_CNP_TAGGED_PORT))
    {
        CLI_SET_ERR (CLI_PB_CNP_CONFIG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adPortSVlanPriority
 Input       :  The Indices
                Dot1adPortNum

                The Object
                testValDot1adPortSVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adPortSVlanPriority (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                  INT4 i4TestValDot1adPortSVlanPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_VALID (i4TestValDot1adPortSVlanPriority) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType != VLAN_CNP_PORTBASED_PORT) &&
        (pVlanPbPortEntry->u1PbPortType != VLAN_CNP_TAGGED_PORT))
    {
        CLI_SET_ERR (CLI_PB_CNP_CONFIG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1SVlanPriorityType == VLAN_SVLAN_PRIORITY_TYPE_COPY)
        || (pVlanPbPortEntry->u1SVlanPriorityType ==
            VLAN_SVLAN_PRIORITY_TYPE_NONE))
    {
        CLI_SET_ERR (CLI_PB_INVALID_SVLAN_PRI_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1adPortTable
 Input       :  The Indices
                Dot1adPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adVidTranslationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adVidTranslationTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adVidTranslationTable (INT4 i4Dot1adPortNum,
                                                   INT4
                                                   i4Dot1adVidTranslationLocalVid)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_VLAN_ID_VALID (i4Dot1adVidTranslationLocalVid) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adVidTranslationTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adVidTranslationTable (INT4 *pi4Dot1adPortNum,
                                           INT4
                                           *pi4Dot1adVidTranslationLocalVid)
{
    return (nmhGetNextIndexDot1adVidTranslationTable (0, pi4Dot1adPortNum,
                                                      0,
                                                      pi4Dot1adVidTranslationLocalVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adVidTranslationTable
 Input       :  The Indices
                Dot1adPortNum
                nextDot1adPortNum
                Dot1adVidTranslationLocalVid
                nextDot1adVidTranslationLocalVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adVidTranslationTable (INT4 i4Dot1adPortNum,
                                          INT4 *pi4NextDot1adPortNum,
                                          INT4 i4Dot1adVidTranslationLocalVid,
                                          INT4
                                          *pi4NextDot1adVidTranslationLocalVid)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    i4RetVal =
        VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                            (UINT2) i4Dot1adPortNum,
                                            (tVlanId)
                                            i4Dot1adVidTranslationLocalVid,
                                            &OutVidTransEntryInfo);

    if (i4RetVal == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextDot1adPortNum = OutVidTransEntryInfo.u2Port;
    *pi4NextDot1adVidTranslationLocalVid = OutVidTransEntryInfo.LocalVid;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adVidTranslationRelayVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid

                The Object 
                retValDot1adVidTranslationRelayVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adVidTranslationRelayVid (INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adVidTranslationLocalVid,
                                    INT4 *pi4RetValDot1adVidTranslationRelayVid)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            (UINT2) i4Dot1adPortNum,
                                            (tVlanId)
                                            i4Dot1adVidTranslationLocalVid,
                                            OSIX_TRUE, &OutVidTransEntryInfo);

    if (i4RetVal == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adVidTranslationRelayVid = OutVidTransEntryInfo.RelayVid;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1adVidTranslationRowStatus
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid

                The Object 
                retValDot1adVidTranslationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adVidTranslationRowStatus (INT4 i4Dot1adPortNum,
                                     INT4 i4Dot1adVidTranslationLocalVid,
                                     INT4
                                     *pi4RetValDot1adVidTranslationRowStatus)
{
    tVidTransEntryInfo  VidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            (UINT2) i4Dot1adPortNum,
                                            (tVlanId)
                                            i4Dot1adVidTranslationLocalVid,
                                            OSIX_TRUE, &VidTransEntryInfo);

    if (i4RetVal == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adVidTranslationRowStatus = VidTransEntryInfo.u1RowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adVidTranslationRelayVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid

                The Object 
                setValDot1adVidTranslationRelayVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adVidTranslationRelayVid (INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adVidTranslationLocalVid,
                                    INT4 i4SetValDot1adVidTranslationRelayVid)
{
    tVidTransEntryInfo  VidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VidTransEntryInfo, 0, sizeof (tVidTransEntryInfo));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    VidTransEntryInfo.u2Port = (UINT2) i4Dot1adPortNum;
    VidTransEntryInfo.LocalVid = (tVlanId) i4Dot1adVidTranslationLocalVid;
    VidTransEntryInfo.RelayVid = (tVlanId) i4SetValDot1adVidTranslationRelayVid;
    VidTransEntryInfo.u1RowStatus = VLAN_NOT_IN_SERVICE;

    i4RetVal = VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                               VidTransEntryInfo);

    if (i4RetVal == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIVidTranslationRelayVid,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adVidTranslationLocalVid,
                      i4SetValDot1adVidTranslationRelayVid));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1adVidTranslationRowStatus
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid

                The Object 
                setValDot1adVidTranslationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adVidTranslationRowStatus (INT4 i4Dot1adPortNum,
                                     INT4 i4Dot1adVidTranslationLocalVid,
                                     INT4 i4SetValDot1adVidTranslationRowStatus)
{
    tVidTransEntryInfo  VidTransEntryInfo;
    tVidTransEntryInfo  OutVidTransEntryInfo;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    INT4                i4RetVal = L2IWF_SUCCESS;
    INT4                i4L2GetVal = L2IWF_SUCCESS;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VidTransEntryInfo, 0, sizeof (tVidTransEntryInfo));

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    i4L2GetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                              (UINT2) i4Dot1adPortNum,
                                              (tVlanId)
                                              i4Dot1adVidTranslationLocalVid,
                                              OSIX_TRUE, &OutVidTransEntryInfo);

    switch (i4SetValDot1adVidTranslationRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            /* Check whether it is possible to create vid translation table
             * entries in this port. */

            /* The following 2 errors are possible only if between the 
             * test and set routine some entries were created or
             * some other changes were done (through cli to through some 
             * other snmp manager) */
            if (VLAN_NUM_VID_TRANS_ENTS () >= VLAN_MAX_VID_TRANSLATION_ENTRIES)
            {
                return SNMP_FAILURE;
            }

            pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

            if (pVlanPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
                (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE))
            {
                if ((VLAN_IS_VID_TRANS_VALID_ONPORT (i4Dot1adPortNum) ==
                     VLAN_FALSE) || (i4L2GetVal == L2IWF_SUCCESS))
                {
                    return SNMP_FAILURE;
                }
            }

            pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

            if (pVlanPbPortEntry != NULL)
            {
                if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
                {
                    if ((pVlanPbPortEntry->u1PbPortType ==
                         VLAN_PROVIDER_INSTANCE_PORT) ||
                        (VLAN_PB_PORT_TYPE (i4Dot1adPortNum) ==
                         VLAN_CUSTOMER_BACKBONE_PORT) ||
                        (i4L2GetVal == L2IWF_SUCCESS))

                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            VidTransEntryInfo.u2Port = (UINT2) i4Dot1adPortNum;
            VidTransEntryInfo.LocalVid =
                (tVlanId) i4Dot1adVidTranslationLocalVid;
            VidTransEntryInfo.RelayVid = VLAN_NULL_VLAN_ID;
            VidTransEntryInfo.u1RowStatus = VLAN_CREATE_AND_WAIT;

            i4RetVal = VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                                       VidTransEntryInfo);
            if (i4RetVal == L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }

            VLAN_NUM_VID_TRANS_ENTS ()++;
            break;

        case VLAN_ACTIVE:
            if (i4L2GetVal == L2IWF_FAILURE)
            {
                /* Entry not present in the table. */
                return SNMP_FAILURE;
            }
            if (OutVidTransEntryInfo.u1RowStatus == VLAN_NOT_IN_SERVICE)
            {

                VidTransEntryInfo.u2Port = (UINT2) i4Dot1adPortNum;
                VidTransEntryInfo.LocalVid =
                    (tVlanId) i4Dot1adVidTranslationLocalVid;
                VidTransEntryInfo.RelayVid = OutVidTransEntryInfo.RelayVid;
                VidTransEntryInfo.u1RowStatus = VLAN_ACTIVE;

                i4RetVal =
                    VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                                    VidTransEntryInfo);
                if (i4RetVal == L2IWF_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
                {
#ifdef NPAPI_WANTED
                    /*Since the Logic of Adding VID Translation entry in
                     * hw depends on the Row status of the Entry, The Row
                     * status has to be updated first in the L2IWF*/
                    if (VlanAddVidTransEntryInHw
                        ((UINT4) i4Dot1adPortNum, 0,
                         OutVidTransEntryInfo.LocalVid,
                         OutVidTransEntryInfo.RelayVid) != VLAN_SUCCESS)
                    {
                        /*If Hw Call Failed, then revert the L2IWF data base */
                        VidTransEntryInfo.u2Port = (UINT2) i4Dot1adPortNum;
                        VidTransEntryInfo.LocalVid =
                            (tVlanId) i4Dot1adVidTranslationLocalVid;
                        VidTransEntryInfo.RelayVid
                            = OutVidTransEntryInfo.RelayVid;
                        VidTransEntryInfo.u1RowStatus = VLAN_NOT_IN_SERVICE;

                        VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                                        VidTransEntryInfo);

                        return SNMP_FAILURE;
                    }
#endif /*NPAPI_WANTED */
                }

                if (VlanGetVlanEntry ((tVlanId) i4Dot1adVidTranslationLocalVid)
                    == VLAN_FALSE)
                {
                    /* When the Relay VLAN is not existing, PVC table entry 
                     * has to be added for the Relay VLAN. 
                     * If the Relay VLAN  is existing, PVC table entry would 
                     * have already added while creation of the VLAN itself */
                    VlanVcmSispUpdatePortVlanMappingOnPort
                        (VLAN_CURR_CONTEXT_ID (), (tVlanId)
                         i4Dot1adVidTranslationLocalVid,
                         VLAN_GET_IFINDEX (i4Dot1adPortNum), SISP_ADD);
                }
            }
            break;

        case VLAN_NOT_IN_SERVICE:
            if (i4L2GetVal == L2IWF_FAILURE)
            {
                /* Entry not present in the table. */
                return SNMP_FAILURE;
            }

            if (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)
            {
                if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
                {
#ifdef NPAPI_WANTED
                    if (VlanDelVidTransEntryInHw
                        ((UINT4) i4Dot1adPortNum, 0,
                         OutVidTransEntryInfo.LocalVid,
                         OutVidTransEntryInfo.RelayVid) != VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
#endif
                }
            }

            /* No need to check for not-ready case, because that would have
             * been verified in the test routine. */
            VidTransEntryInfo.u2Port = (UINT2) i4Dot1adPortNum;
            VidTransEntryInfo.LocalVid =
                (tVlanId) i4Dot1adVidTranslationLocalVid;
            VidTransEntryInfo.RelayVid = OutVidTransEntryInfo.RelayVid;
            VidTransEntryInfo.u1RowStatus = VLAN_NOT_IN_SERVICE;

            i4RetVal = VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                                       VidTransEntryInfo);
            if (i4RetVal == L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case VLAN_DESTROY:
            if (i4L2GetVal == L2IWF_FAILURE)
            {
                /* Entry not present in the table, so return success. */
                return SNMP_SUCCESS;
            }

            if (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)
            {
                if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
                {
#ifdef NPAPI_WANTED
                    if (VlanDelVidTransEntryInHw
                        ((UINT4) i4Dot1adPortNum, 0,
                         OutVidTransEntryInfo.LocalVid,
                         OutVidTransEntryInfo.RelayVid) != VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
#endif
                }
            }

            VidTransEntryInfo.u2Port = (UINT2) i4Dot1adPortNum;
            VidTransEntryInfo.LocalVid = OutVidTransEntryInfo.LocalVid;
            VidTransEntryInfo.RelayVid = OutVidTransEntryInfo.RelayVid;
            VidTransEntryInfo.u1RowStatus = VLAN_DESTROY;

            i4RetVal = VlanL2IwfPbConfigVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                                       VidTransEntryInfo);
            if (i4RetVal == L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* Updating the SISP table */

            if (VlanGetVlanEntry ((tVlanId) i4Dot1adVidTranslationLocalVid)
                == VLAN_FALSE)
            {
                VlanVcmSispUpdatePortVlanMappingOnPort
                    (VLAN_CURR_CONTEXT_ID (), (tVlanId)
                     i4Dot1adVidTranslationLocalVid,
                     VLAN_GET_IFINDEX (i4Dot1adPortNum), SISP_DELETE);
            }

            VLAN_NUM_VID_TRANS_ENTS ()--;
            break;
        default:
            return SNMP_FAILURE;
    }

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adVidTranslationRelayVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid

                The Object 
                testValDot1adVidTranslationRelayVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adVidTranslationRelayVid (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adVidTranslationLocalVid,
                                       INT4
                                       i4TestValDot1adVidTranslationRelayVid)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;
    INT4                i4L2GetVal = L2IWF_SUCCESS;
    tVidTransEntryInfo  EgrVidTransEntryInfo;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_VLAN_ID_VALID (i4Dot1adVidTranslationLocalVid) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4TestValDot1adVidTranslationRelayVid) !=
         VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Even if the Relay Vlan is not present, we can create the corresponding
     * Vid Translation table entry.*/

    i4L2GetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                              (UINT2) i4Dot1adPortNum,
                                              (tVlanId)
                                              i4Dot1adVidTranslationLocalVid,
                                              OSIX_TRUE, &OutVidTransEntryInfo);

    if (i4L2GetVal == L2IWF_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check whether some other entry is having this relay
     * vid, if so then return failure.*/
    i4RetVal = VlanL2IwfPbGetVidTransEntry
        (VLAN_CURR_CONTEXT_ID (), (UINT2) i4Dot1adPortNum,
         (tVlanId) i4TestValDot1adVidTranslationRelayVid, OSIX_FALSE,
         &EgrVidTransEntryInfo);

    if ((i4RetVal == L2IWF_SUCCESS) &&
        (EgrVidTransEntryInfo.LocalVid != i4Dot1adVidTranslationLocalVid))
    {
        /* Already there is an entry with this relay vid. Hence
         * return failure. */
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adVidTranslationRowStatus
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid

                The Object 
                testValDot1adVidTranslationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adVidTranslationRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1adPortNum,
                                        INT4 i4Dot1adVidTranslationLocalVid,
                                        INT4
                                        i4TestValDot1adVidTranslationRowStatus)
{
    UINT1              *pPortList = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4L2GetVal = L2IWF_SUCCESS;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adVidTranslationLocalVid) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
        (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE))
    {
        if (VLAN_IS_VID_TRANS_VALID_ONPORT (i4Dot1adPortNum) == VLAN_FALSE)
        {
            CLI_SET_ERR (CLI_PB_SVLAN_TRANS_CNP_PNP_PCNP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry != NULL)
    {
        if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
        {
            if ((pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_INSTANCE_PORT)
                || (pVlanPbPortEntry->u1PbPortType ==
                    VLAN_CUSTOMER_BACKBONE_PORT))

            {
                CLI_SET_ERR (CLI_PB_SVLAN_TRANS_PIP_CBP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    i4L2GetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                              (UINT2) i4Dot1adPortNum,
                                              (tVlanId)
                                              i4Dot1adVidTranslationLocalVid,
                                              OSIX_TRUE, &OutVidTransEntryInfo);

    switch (i4TestValDot1adVidTranslationRowStatus)
    {
        case VLAN_CREATE_AND_GO:
            /* Without configuring relay vid, row status cannot be
             * set to active. So create-and-go option cannot be
             * supported for this table. */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        case VLAN_CREATE_AND_WAIT:
	    if (i4L2GetVal == L2IWF_SUCCESS)
            {
                /* Entry already exists. */
                CLI_SET_ERR (CLI_PB_VLAN_MAPNG_EXIST_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* Check whether the number of vid translation table entries
             * has reached its max value or not. */
            if (VLAN_NUM_VID_TRANS_ENTS () >= VLAN_MAX_VID_TRANSLATION_ENTRIES)
            {
                CLI_SET_ERR (CLI_PB_VLAN_MAX_ENTRIES_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

            if (pPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "nmhTestv2Dot1adVidTranslationRowStatus: Error in allocating memory "
                          "for pPortList\r\n");
                return SNMP_FAILURE;
            }
            VLAN_MEMSET (pPortList, 0, sizeof (tLocalPortList));

            OSIX_BITLIST_SET_BIT (pPortList, i4Dot1adPortNum,
                                  VLAN_PORT_LIST_SIZE);

            if (VlanVcmSispIsPortVlanMappingValid (VLAN_CURR_CONTEXT_ID (),
                                                   (tVlanId)
                                                   i4Dot1adVidTranslationLocalVid,
                                                   pPortList) == VCM_FALSE)
            {
                CLI_SET_ERR (CLI_PB_SISP_VLAN_TRANS_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                UtilPlstReleaseLocalPortList (pPortList);
                return SNMP_FAILURE;
            }
            UtilPlstReleaseLocalPortList (pPortList);
            break;
        case VLAN_ACTIVE:

            if (i4L2GetVal == L2IWF_FAILURE)
            {
                CLI_SET_ERR (CLI_PB_VLAN_MAPNG_NOT_EXIST_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (OutVidTransEntryInfo.u1RowStatus == VLAN_NOT_READY)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* If the row status is active or not-in-service, then 
             * return success. */

            break;
        case VLAN_DESTROY:
            if (i4L2GetVal == L2IWF_FAILURE)
            {
                /* Entry does not exists, so return failure. */

                CLI_SET_ERR (CLI_PB_VLAN_MAPNG_NOT_EXIST_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            /* If the entry is not present, then it is not an error. */
            break;
        case VLAN_NOT_IN_SERVICE:
            if ((i4L2GetVal == L2IWF_FAILURE) ||
                (OutVidTransEntryInfo.u1RowStatus == VLAN_NOT_READY))
            {
                CLI_SET_ERR (CLI_PB_VLAN_NOT_IN_SERVICE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1adVidTranslationTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adVidTranslationLocalVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adVidTranslationTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adCVidRegistrationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adCVidRegistrationTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adCVidRegistrationTable (INT4 i4Dot1adPortNum,
                                                     INT4
                                                     i4Dot1adCVidRegistrationCVid)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationCVid) == VLAN_FALSE)
    {
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adCVidRegistrationTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adCVidRegistrationTable (INT4 *pi4Dot1adPortNum,
                                             INT4
                                             *pi4Dot1adCVidRegistrationCVid)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    i4Result = VlanPbGetFirstCVlanSVlanEntry (u4TblIndex,
                                              pi4Dot1adPortNum,
                                              (UINT4 *)
                                              pi4Dot1adCVidRegistrationCVid);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adCVidRegistrationTable
 Input       :  The Indices
                Dot1adPortNum
                nextDot1adPortNum
                Dot1adCVidRegistrationCVid
                nextDot1adCVidRegistrationCVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adCVidRegistrationTable (INT4 i4Dot1adPortNum,
                                            INT4 *pi4NextDot1adPortNum,
                                            INT4 i4Dot1adCVidRegistrationCVid,
                                            INT4
                                            *pi4NextDot1adCVidRegistrationCVid)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    i4Result = VlanPbGetNextCVlanSVlanEntry (u4TblIndex,
                                             i4Dot1adPortNum,
                                             (UINT4)
                                             i4Dot1adCVidRegistrationCVid,
                                             pi4NextDot1adPortNum,
                                             (UINT4 *)
                                             pi4NextDot1adCVidRegistrationCVid);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adCVidRegistrationSVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                retValDot1adCVidRegistrationSVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVidRegistrationSVid (INT4 i4Dot1adPortNum,
                                  INT4 i4Dot1adCVidRegistrationCVid,
                                  INT4 *pi4RetValDot1adCVidRegistrationSVid)
{
    UINT4               u4TblIndex;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adCVidRegistrationSVid = (UINT4) pCVlanSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adCVidRegistrationUntaggedPep
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                retValDot1adCVidRegistrationUntaggedPep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVidRegistrationUntaggedPep (INT4 i4Dot1adPortNum,
                                         INT4 i4Dot1adCVidRegistrationCVid,
                                         INT4
                                         *pi4RetValDot1adCVidRegistrationUntaggedPep)
{
    UINT4               u4TblIndex;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adCVidRegistrationUntaggedPep = pCVlanSVlanEntry->u1PepUntag;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adCVidRegistrationUntaggedCep
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                retValDot1adCVidRegistrationUntaggedCep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVidRegistrationUntaggedCep (INT4 i4Dot1adPortNum,
                                         INT4 i4Dot1adCVidRegistrationCVid,
                                         INT4
                                         *pi4RetValDot1adCVidRegistrationUntaggedCep)
{
    UINT4               u4TblIndex;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adCVidRegistrationUntaggedCep = pCVlanSVlanEntry->u1CepUntag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adCVidRegistrationRowStatus
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                retValDot1adCVidRegistrationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVidRegistrationRowStatus (INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adCVidRegistrationCVid,
                                       INT4
                                       *pi4RetValDot1adCVidRegistrationRowStatus)
{
    UINT4               u4TblIndex;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry != NULL)
    {
        *pi4RetValDot1adCVidRegistrationRowStatus =
            (INT4) pCVlanSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1adCVIdRegistrationRelayCVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                retValDot1adCVIdRegistrationRelayCVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVIdRegistrationRelayCVid (INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adCVidRegistrationCVid,
                                       INT4
                                       *pi4RetValDot1adCVIdRegistrationRelayCVid)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adCVIdRegistrationRelayCVid =
        (INT4) pCVlanSVlanEntry->RelayCVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1adCVidRegistrationSVlanPriorityType
 Input       :  The Indices
                Dot1adPortNum                                                                                                                                                Dot1adCVidRegistrationCVid

                The Object
                retValDot1adCVidRegistrationSVlanPriorityType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVidRegistrationSVlanPriorityType (INT4 i4Dot1adPortNum,
                                               INT4
                                               i4Dot1adCVidRegistrationCVid,
                                               INT4
                                               *pi4RetValDot1adCVidRegistrationSVlanPriorityType)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adCVidRegistrationSVlanPriorityType =
        (INT4) pCVlanSVlanEntry->u1SVlanPriorityType;

    return SNMP_SUCCESS;

}

/****************************************************************************                                                                                 Function    :  nmhGetDot1adCVidRegistrationSVlanPriority                                                                                                     Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object
                retValDot1adCVidRegistrationSVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adCVidRegistrationSVlanPriority (INT4 i4Dot1adPortNum,
                                           INT4 i4Dot1adCVidRegistrationCVid,
                                           INT4
                                           *pi4RetValDot1adCVidRegistrationSVlanPriority)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adCVidRegistrationSVlanPriority =
        (INT4) pCVlanSVlanEntry->u1SVlanPriority;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adCVidRegistrationSVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                setValDot1adCVidRegistrationSVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adCVidRegistrationSVid (INT4 i4Dot1adPortNum,
                                  INT4 i4Dot1adCVidRegistrationCVid,
                                  INT4 i4SetValDot1adCVidRegistrationSVid)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCVlanSVlanEntry->SVlanId = (tVlanId) i4SetValDot1adCVidRegistrationSVid;
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pCVlanSVlanEntry->u1RowStatus != VLAN_NOT_READY)
    {
        return SNMP_FAILURE;
    }

    pCVlanSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

    /* Create the PEP in VLAN and L2IWF when the SVLAN is mapped 
     * to the CEP.
     */
    if (VlanPbCreateLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                      pCVlanSVlanEntry->CVlanId,
                                      pCVlanSVlanEntry->SVlanId) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
         
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMICVidRegistrationSVid,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);
    
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVidRegistrationSVid));

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adCVidRegistrationUntaggedPep
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                setValDot1adCVidRegistrationUntaggedPep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adCVidRegistrationUntaggedPep (INT4 i4Dot1adPortNum,
                                         INT4 i4Dot1adCVidRegistrationCVid,
                                         INT4
                                         i4SetValDot1adCVidRegistrationUntaggedPep)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry;
    tVlanPbLogicalPortEntry *pVlanLogicalPortEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* By default UntaggedPEP will be initialized to False. */
    if (pCVlanSVlanEntry->u1PepUntag == (UINT1)
        i4SetValDot1adCVidRegistrationUntaggedPep)
    {
        return SNMP_SUCCESS;
    }

    /* UntaggedPEP/UntaggedCEP can be set only in not-in-service or active 
     * states. */
    if (pCVlanSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        return SNMP_FAILURE;
    }

    /* Get the entry used for storing PEP and corresponding internal
     * CNP informations. If Cvid registration entry is present then
     * the corresponding PEP should be present. So no need to check
     * for pVlanLogicalPortEntry == NULL */
    pVlanLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   pCVlanSVlanEntry->SVlanId);
    if (pVlanLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValDot1adCVidRegistrationUntaggedPep == VLAN_SNMP_TRUE)
    {
        if (VLAN_PB_UNTAG_PEPSET (pVlanLogicalPortEntry) == VLAN_TRUE)
        {
            /* Already for one cvlan mapped to the given service instance
             * untagged pep is set. So by section 15.7 of IEEE 802.1ad/D6, 
             * don't allow this configuration. */
            return SNMP_FAILURE;
        }
        VLAN_PB_UNTAG_PEPSET (pVlanLogicalPortEntry) = VLAN_TRUE;
    }
    else
    {
        /* UntaggedPEP is set from True to False. */
        VLAN_PB_UNTAG_PEPSET (pVlanLogicalPortEntry) = VLAN_FALSE;
    }

    /* Update the NP Only when the logical port entry is active
     * and the entry is already created in hardware*/

    if (pVlanLogicalPortEntry->u1OperStatus == VLAN_ACTIVE)
    {

        if (pCVlanSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1)
        {
            return SNMP_FAILURE;
        }
        VlanSVlanMap.u2Port =
            (UINT2) (VLAN_GET_IFINDEX (pCVlanSVlanEntry->u2Port));
        VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
        VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
        VlanSVlanMap.u1PepUntag
            = (UINT1) i4SetValDot1adCVidRegistrationUntaggedPep;
        VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;
        VlanSVlanMap.u1SVlanPriorityType =
            pCVlanSVlanEntry->u1SVlanPriorityType;
        VlanSVlanMap.u1SVlanPriority = pCVlanSVlanEntry->u1SVlanPriority;

        if (VlanHwSetCvidUntagPep (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  Dot1adMICVidRegistrationUntaggedPep,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                              VLAN_GET_IFINDEX (i4Dot1adPortNum),
                              i4Dot1adCVidRegistrationCVid,
                              i4SetValDot1adCVidRegistrationUntaggedPep));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }
    }

    pCVlanSVlanEntry->u1PepUntag
        = (UINT1) i4SetValDot1adCVidRegistrationUntaggedPep;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMICVidRegistrationUntaggedPep,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVidRegistrationUntaggedPep));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adCVidRegistrationUntaggedCep
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                setValDot1adCVidRegistrationUntaggedCep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adCVidRegistrationUntaggedCep (INT4 i4Dot1adPortNum,
                                         INT4 i4Dot1adCVidRegistrationCVid,
                                         INT4
                                         i4SetValDot1adCVidRegistrationUntaggedCep)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        if (pCVlanSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  Dot1adMICVidRegistrationUntaggedCep,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                              VLAN_GET_IFINDEX (i4Dot1adPortNum),
                              i4Dot1adCVidRegistrationCVid,
                              i4SetValDot1adCVidRegistrationUntaggedCep));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }

        VlanSVlanMap.u2Port =
            (UINT2) (VLAN_GET_IFINDEX (pCVlanSVlanEntry->u2Port));
        VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
        VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
        VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
        VlanSVlanMap.u1CepUntag
            = (UINT1) i4SetValDot1adCVidRegistrationUntaggedCep;

        if (VlanHwSetCvidUntagCep (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pCVlanSVlanEntry->u1CepUntag
        = (UINT1) i4SetValDot1adCVidRegistrationUntaggedCep;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMICVidRegistrationUntaggedCep,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVidRegistrationUntaggedCep));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adCVidRegistrationRowStatus
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                setValDot1adCVidRegistrationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adCVidRegistrationRowStatus (INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adCVidRegistrationCVid,
                                       INT4
                                       i4SetValDot1adCVidRegistrationRowStatus)
{
    tVlanSVlanMap       VlanSVlanMap;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanCVlanSVlanEntry *pNextCVlanSVlanEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanLogicalPortEntry = NULL;
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tHwVlanPbPepInfo    HwVlanPbPepInfo;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVlanId             CVlanId = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex;
    UINT4               u4NextCVidRegistrationCVid = 0;
    INT4                i4NextDot1adPortNum = 0;
    INT4                i4PortNum = i4Dot1adPortNum;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
   
    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if ((pCVlanSVlanEntry == NULL)
        && (i4SetValDot1adCVidRegistrationRowStatus != VLAN_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry != NULL)
    {
        if (i4SetValDot1adCVidRegistrationRowStatus == VLAN_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
        if (pCVlanSVlanEntry->u1RowStatus ==
            i4SetValDot1adCVidRegistrationRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValDot1adCVidRegistrationRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pCVlanSVlanEntry =
                (tVlanCVlanSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_CVLAN_TYPE);

            if (pCVlanSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, C-VLAN based"
                          "S-VLAN Classification Entry \n");

                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pCVlanSVlanEntry, 0, sizeof (tVlanCVlanSVlanEntry));

            pCVlanSVlanEntry->u1RowStatus = VLAN_NOT_READY;
            pCVlanSVlanEntry->u2Port = (UINT2) i4Dot1adPortNum;
            pCVlanSVlanEntry->CVlanId = (tVlanId) i4Dot1adCVidRegistrationCVid;
            pCVlanSVlanEntry->u1PepUntag = VLAN_SNMP_FALSE;
            pCVlanSVlanEntry->u1CepUntag = VLAN_SNMP_FALSE;
            pCVlanSVlanEntry->RelayCVlanId =
                (tVlanId) i4Dot1adCVidRegistrationCVid;
            pCVlanSVlanEntry->u1SVlanPriorityType =
                VLAN_SVLAN_PRIORITY_TYPE_NONE;

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_TYPE,
                                       (UINT1 *) pCVlanSVlanEntry);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pCVlanSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_TYPE,
                                       (UINT1 *) pCVlanSVlanEntry);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            pVlanLogicalPortEntry =
                VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                           pCVlanSVlanEntry->SVlanId);

            if (pVlanLogicalPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pVlanLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
            {
                if (pVlanLogicalPortEntry->u2NoOfActiveCvidEntries == 1)
                {
                    if (VlanHwDelProviderEdgePort (VLAN_CURR_CONTEXT_ID (),
                                                   i4Dot1adPortNum,
                                                   pCVlanSVlanEntry->SVlanId) !=
                        VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }

                if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
                {

                    VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

                    VlanSVlanMap.u2Port =
                        (UINT2) (VLAN_GET_IFINDEX (pCVlanSVlanEntry->u2Port));
                    VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
                    VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
                    VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
                    VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;
                    VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;
                    pVlanLogicalPortEntry->u2NoOfActiveCvidEntries--;
                    pCVlanSVlanEntry->u1CvlanStatus = VLAN_DISABLED;
                    pCVlanSVlanEntry->bCvlanStatFlush = FALSE;

                    if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                              VlanSVlanMap) == VLAN_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            pCVlanSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            VlanPbNotifyCVidStatusForPep ((UINT2) i4Dot1adPortNum,
                                          (tVlanId) pCVlanSVlanEntry->SVlanId,
                                          (UINT1)
                                          i4SetValDot1adCVidRegistrationRowStatus);

            break;
        case VLAN_ACTIVE:
            pCVlanSVlanEntry->u1RowStatus = VLAN_ACTIVE;
            VlanPbNotifyCVidStatusForPep ((UINT2) i4Dot1adPortNum,
                                          (tVlanId) pCVlanSVlanEntry->SVlanId,
                                          (UINT1)
                                          i4SetValDot1adCVidRegistrationRowStatus);
            /* 
             * Create the Logical/Internal ports (PEP/CNPs) in 
             * VLAN.
             */
            pVlanLogicalPortEntry =
                VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                           pCVlanSVlanEntry->SVlanId);

            if (pVlanLogicalPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pVlanLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
            {
                VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

                VlanSVlanMap.u2Port =
                    (UINT2) (VLAN_GET_IFINDEX (pCVlanSVlanEntry->u2Port));
                VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
                VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
                VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
                VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;
                VlanSVlanMap.u1SVlanPriorityType =
                    pCVlanSVlanEntry->u1SVlanPriorityType;
                VlanSVlanMap.u1SVlanPriority =
                    pCVlanSVlanEntry->u1SVlanPriority;

                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;
                pCVlanSVlanEntry->u1CvlanStatus = VLAN_DISABLED;
                pCVlanSVlanEntry->bCvlanStatFlush = FALSE;

                if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                       VlanSVlanMap) == VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                if (pVlanLogicalPortEntry->u2NoOfActiveCvidEntries == 1)
                {
                    VLAN_MEMSET (&HwVlanPbPepInfo, 0,
                                 sizeof (tHwVlanPbPepInfo));

                    HwVlanPbPepInfo.i4DefUserPri =
                        pVlanLogicalPortEntry->u1DefUserPriority;
                    HwVlanPbPepInfo.Cpvid = pVlanLogicalPortEntry->Cpvid;
                    HwVlanPbPepInfo.u1AccptFrameType =
                        pVlanLogicalPortEntry->u1AccptFrameType;
                    HwVlanPbPepInfo.u1IngFiltering =
                        pVlanLogicalPortEntry->u1IngressFiltering;

                    if (VlanHwCreateProviderEdgePort (VLAN_CURR_CONTEXT_ID (),
                                                      (UINT2)
                                                      (VLAN_GET_PHY_PORT
                                                       (pCVlanSVlanEntry->
                                                        u2Port)),
                                                      pCVlanSVlanEntry->SVlanId,
                                                      HwVlanPbPepInfo) !=
                        VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    if (VlanHwSetPepPvid (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (pCVlanSVlanEntry->
                                                             u2Port),
                                          (tVlanId) pCVlanSVlanEntry->SVlanId,
                                          (tVlanId) pCVlanSVlanEntry->
                                          CVlanId) != VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        case VLAN_DESTROY:

            pVlanLogicalPortEntry =
                VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                           pCVlanSVlanEntry->SVlanId);

            if (pVlanLogicalPortEntry != NULL)
            {
                if (pVlanLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
                {
                    if (pVlanLogicalPortEntry->u2NoOfCvidEntries == 1)
                    {
                        /* Convert and pass the Global port id */
                        if (VlanHwDelProviderEdgePort
                            (VLAN_CURR_CONTEXT_ID (),
                             VLAN_GET_PHY_PORT ((UINT2) i4Dot1adPortNum),
                             pCVlanSVlanEntry->SVlanId) != VLAN_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        if (VlanHwSetPepPvid (VLAN_CURR_CONTEXT_ID (),
                                              VLAN_GET_PHY_PORT
                                              (pCVlanSVlanEntry->u2Port),
                                              (tVlanId) pCVlanSVlanEntry->
                                              SVlanId,
                                              (tVlanId) VLAN_ZERO) !=
                            VLAN_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        while ((VlanPbGetNextCVlanSVlanEntry
                                (u4TblIndex, i4PortNum,
                                 CVlanId,
                                 &i4NextDot1adPortNum,
                                 &u4NextCVidRegistrationCVid)) == VLAN_SUCCESS)
                        {
                            if (i4NextDot1adPortNum != i4PortNum)
                            {
                                i4PortNum = i4NextDot1adPortNum;
                                break;
                            }

                            if (u4NextCVidRegistrationCVid
                                == (UINT4) i4Dot1adCVidRegistrationCVid)
                            {
                                CVlanId = u4NextCVidRegistrationCVid;
                                continue;
                            }

                            pNextCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry
                                (u4TblIndex, i4NextDot1adPortNum,
                                 u4NextCVidRegistrationCVid);

                            if (pNextCVlanSVlanEntry != NULL)
                            {
                                if (pNextCVlanSVlanEntry->SVlanId
                                    == pCVlanSVlanEntry->SVlanId)
                                {
                                    VLAN_MEMSET (&VlanPbLogicalPortEntry, 0,
                                                 sizeof
                                                 (tVlanPbLogicalPortEntry));

                                    VlanPbLogicalPortEntry.u2CepPort =
                                        i4NextDot1adPortNum;
                                    VlanPbLogicalPortEntry.SVlanId =
                                        pNextCVlanSVlanEntry->SVlanId;

                                    pVlanPbLogicalPortEntry =
                                        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->
                                                   pVlanLogicalPortTable,
                                                   &VlanPbLogicalPortEntry);

                                    if (pVlanPbLogicalPortEntry != NULL)
                                    {
                                        if (i4Dot1adCVidRegistrationCVid ==
                                            pVlanPbLogicalPortEntry->Cpvid)
                                        {
                                            pVlanPbLogicalPortEntry->Cpvid =
                                                pNextCVlanSVlanEntry->CVlanId;
                                            if (VlanHwSetPepPvid
                                                (VLAN_CURR_CONTEXT_ID (),
                                                 VLAN_GET_PHY_PORT
                                                 (pNextCVlanSVlanEntry->u2Port),
                                                 (tVlanId)
                                                 pNextCVlanSVlanEntry->SVlanId,
                                                 (tVlanId)
                                                 pNextCVlanSVlanEntry->
                                                 CVlanId) != VLAN_SUCCESS)
                                            {
                                                return SNMP_FAILURE;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            i4PortNum = i4NextDot1adPortNum;
                            CVlanId = u4NextCVidRegistrationCVid;
                        }
                    }

                    if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
                    {
                        VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

                        VlanSVlanMap.u2Port =
                            (UINT2) (VLAN_GET_IFINDEX (pCVlanSVlanEntry->
                                                       u2Port));
                        VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
                        VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
                        VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
                        VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;

                        VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;
                        pVlanLogicalPortEntry->u2NoOfActiveCvidEntries--;
                        pCVlanSVlanEntry->u1CvlanStatus = VLAN_DISABLED;
                        pCVlanSVlanEntry->bCvlanStatFlush = FALSE;
                        if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                                  VlanSVlanMap) == VLAN_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
                VlanPbNotifyCVidStatusForPep
                    ((UINT2) i4Dot1adPortNum,
                     (tVlanId) pCVlanSVlanEntry->SVlanId,
                     (UINT1) i4SetValDot1adCVidRegistrationRowStatus);

                /* Untag pep can be true only for entries whose row status is
                 * either not-in-service or active. For Cvid registration table
                 * entries with row status "not-in-service" or "active", there
                 * will be a corresponding PEP and hence valid logicalportentry.*/

                if (pCVlanSVlanEntry->u1RowStatus != VLAN_NOT_READY)
                {
                    /* If the untaggedPEP is true for this entry, then reset
                     * u1UnTagPepSet in the pVlanLogicalPortEntry. */
                    if (pCVlanSVlanEntry->u1PepUntag == VLAN_SNMP_TRUE)
                    {
                        VLAN_PB_UNTAG_PEPSET (pVlanLogicalPortEntry) =
                            VLAN_FALSE;
                    }
                }
                /* 
                 * Delete the Logical/Internal ports (PEP/CNPs) from 
                 * VLAN.
                 */
                if (VlanPbDelLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                               pCVlanSVlanEntry->SVlanId) ==
                    VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pCVlanSVlanEntry) != RB_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_TYPE,
                                   (UINT1 *) pCVlanSVlanEntry);

            break;
        default:
            return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMICVidRegistrationRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVidRegistrationRowStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adCVIdRegistrationRelayCVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                setValDot1adCVIdRegistrationRelayCVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adCVIdRegistrationRelayCVid (INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adCVidRegistrationCVid,
                                       INT4
                                       i4SetValDot1adCVIdRegistrationRelayCVid)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry->RelayCVlanId ==
        i4SetValDot1adCVIdRegistrationRelayCVid)
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pCVlanSVlanEntry->RelayCVlanId =
        (tVlanId) i4SetValDot1adCVIdRegistrationRelayCVid;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMICVidRegistrationRelayCVid,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVIdRegistrationRelayCVid));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1adCVidRegistrationSVlanPriorityType
 Input       :  The Indices                                                                                                                                                  Dot1adPortNum                                                                                                                                                Dot1adCVidRegistrationCVid

                The Object
                setValDot1adCVidRegistrationSVlanPriorityType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adCVidRegistrationSVlanPriorityType (INT4 i4Dot1adPortNum,
                                               INT4
                                               i4Dot1adCVidRegistrationCVid,
                                               INT4
                                               i4SetValDot1adCVidRegistrationSVlanPriorityType)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry->u1SVlanPriorityType ==
        i4SetValDot1adCVidRegistrationSVlanPriorityType)
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pCVlanSVlanEntry->u1SVlanPriorityType =
        (UINT1) i4SetValDot1adCVidRegistrationSVlanPriorityType;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          Dot1adMICVidRegistrationSVlanPriorityType, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVidRegistrationSVlanPriorityType));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1adCVidRegistrationSVlanPriority
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object
                setValDot1adCVidRegistrationSVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.                                                                                                                   Returns     :  SNMP_SUCCESS or SNMP_FAILURE                                                                                                                 ****************************************************************************/
INT1
nmhSetDot1adCVidRegistrationSVlanPriority (INT4 i4Dot1adPortNum,
                                           INT4 i4Dot1adCVidRegistrationCVid,
                                           INT4
                                           i4SetValDot1adCVidRegistrationSVlanPriority)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4TblIndex = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry->u1SVlanPriority ==
        i4SetValDot1adCVidRegistrationSVlanPriority)
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pCVlanSVlanEntry->u1SVlanPriority =
        (UINT1) i4SetValDot1adCVidRegistrationSVlanPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMICVidRegistrationSVlanPriority,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationCVid,
                      i4SetValDot1adCVidRegistrationSVlanPriority));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVidRegistrationSVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                testValDot1adCVidRegistrationSVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVidRegistrationSVid (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                     INT4 i4Dot1adCVidRegistrationCVid,
                                     INT4 i4TestValDot1adCVidRegistrationSVid)
{
    UINT2               u2Port = VLAN_ZERO;
    INT4                i4NextFsPbPort = VLAN_ZERO;
    UINT4               u4NextSVlanId = VLAN_ZERO;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValDot1adCVidRegistrationSVid == (INT4) VlanIcchApiGetIcclVlanId (VLAN_ICCH_DEFAULT_INST))
    {
        CLI_SET_ERR (CLI_PB_ICCH_VLAN_ERR); 
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValDot1adCVidRegistrationSVid) ==
        VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValDot1adCVidRegistrationSVid == pCVlanSVlanEntry->SVlanId)
    {
        return SNMP_SUCCESS;
    }
    /* Svid for a C-VID registration table cannot be changed. */
    if (pCVlanSVlanEntry->u1RowStatus != VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /*If Bundling is enable than multiple CVlan to same SVlan can be mapped 
     * otherwise only one CVlan can be mapped with per SVlan.*/
    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry (u2Port, i4TestValDot1adCVidRegistrationSVid);

    if ((pVlanPbLogicalPortEntry != NULL) &&
        (pVlanPbPortEntry->u1BundleStatus == VLAN_DISABLED))
    {
        if (pVlanPbLogicalPortEntry->u2NoOfCvidEntries == 1)
        {
            CLI_SET_ERR (CLI_PB_BUNDLE_DISABLE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    /*If Multiplexing is enable multiple SVlan can be mapped to this port,
     * otherwise only one SVlan can be mapped to this port.*/
    pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry (u2Port, VLAN_ZERO,
                                                             &i4NextFsPbPort,
                                                             &u4NextSVlanId);
    if ((pVlanPbLogicalPortEntry != NULL)
        && (pVlanPbPortEntry->u1MultiplexStatus == VLAN_DISABLED))
    {
        if (u4NextSVlanId != (UINT4) i4TestValDot1adCVidRegistrationSVid)
        {
            CLI_SET_ERR (CLI_PB_MULTIPLEX_DISABLE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVidRegistrationUntaggedPep
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                testValDot1adCVidRegistrationUntaggedPep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVidRegistrationUntaggedPep (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1adPortNum,
                                            INT4 i4Dot1adCVidRegistrationCVid,
                                            INT4
                                            i4TestValDot1adCVidRegistrationUntaggedPep)
{
    UINT2               u2Port;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanLogicalPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adCVidRegistrationUntaggedPep != VLAN_SNMP_TRUE) &&
        (i4TestValDot1adCVidRegistrationUntaggedPep != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* u1PepUntag will have VLAN_SNMP_TRUE/VLAN_SNMP_FALSE */
    if (pCVlanSVlanEntry->u1PepUntag ==
        i4TestValDot1adCVidRegistrationUntaggedPep)
    {
        return SNMP_SUCCESS;
    }

    if (pCVlanSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1adCVidRegistrationUntaggedPep == VLAN_SNMP_TRUE)
    {
        /* Get the entry used for storing PEP and corresponding internal
         * CNP informations. */
        pVlanLogicalPortEntry =
            VlanPbGetLogicalPortEntry (u2Port, pCVlanSVlanEntry->SVlanId);

        if (pVlanLogicalPortEntry->u1CosPreservation == VLAN_ENABLED)
        {
            /* Setting the UntaggedPEP to TRUE will not be allowed when the COS 
             * preservation is enabled on the corresponding PEP. */
            CLI_SET_ERR (CLI_PB_UNTAGPEP_COS_PRESERVATION_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (VLAN_PB_UNTAG_PEPSET (pVlanLogicalPortEntry) == VLAN_TRUE)
        {
            /* Already for one cvlan mapped to the given service instance
             * untagged pep is set. So by section 15.7 of IEEE 802.1ad/D6, 
             * don't allow this configuration. */
            CLI_SET_ERR (CLI_PB_UNTAGPEP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVidRegistrationUntaggedCep
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                testValDot1adCVidRegistrationUntaggedCep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVidRegistrationUntaggedCep (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1adPortNum,
                                            INT4 i4Dot1adCVidRegistrationCVid,
                                            INT4
                                            i4TestValDot1adCVidRegistrationUntaggedCep)
{
    UINT2               u2Port;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanLogicalPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adCVidRegistrationUntaggedCep != VLAN_SNMP_TRUE) &&
        (i4TestValDot1adCVidRegistrationUntaggedCep != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pCVlanSVlanEntry->u1CepUntag ==
        i4TestValDot1adCVidRegistrationUntaggedCep)
    {
        return SNMP_SUCCESS;
    }

    /* UntaggedPEP/UntaggedCEP can be set only in not-in-service or active 
     * states. */
    if (pCVlanSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1adCVidRegistrationUntaggedCep == VLAN_SNMP_TRUE)
    {
        /* Get the corresponding PEP */
        pVlanLogicalPortEntry =
            VlanPbGetLogicalPortEntry (u2Port, pCVlanSVlanEntry->SVlanId);

        if (pVlanLogicalPortEntry->u1CosPreservation == VLAN_ENABLED)
        {
            /* Setting the UntaggedCEP to TRUE will not be allowed when the COS 
             * preservation is enabled on the corresponding PEP. */
            CLI_SET_ERR (CLI_PB_UNTAGPEP_COS_PRESERVATION_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVidRegistrationSVlanPriorityType
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object
                testValDot1adCVidRegistrationSVlanPriorityType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVidRegistrationSVlanPriorityType (UINT4 *pu4ErrorCode,
                                                  INT4 i4Dot1adPortNum,
                                                  INT4
                                                  i4Dot1adCVidRegistrationCVid,
                                                  INT4
                                                  i4TestValDot1adCVidRegistrationSVlanPriorityType)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_TYPE_VALID
        (i4TestValDot1adCVidRegistrationSVlanPriorityType) == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_SVLAN_PRI_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pCVlanSVlanEntry->u1SVlanPriorityType ==
        i4TestValDot1adCVidRegistrationSVlanPriorityType)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVidRegistrationSVlanPriority
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object
                testValDot1adCVidRegistrationSVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVidRegistrationSVlanPriority (UINT4 *pu4ErrorCode,
                                              INT4 i4Dot1adPortNum,
                                              INT4 i4Dot1adCVidRegistrationCVid,
                                              INT4
                                              i4TestValDot1adCVidRegistrationSVlanPriority)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adCVidRegistrationSVlanPriority < VLAN_MIN_PRIORITY) ||
        (i4TestValDot1adCVidRegistrationSVlanPriority > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pCVlanSVlanEntry->u1SVlanPriorityType == VLAN_SVLAN_PRIORITY_TYPE_COPY)
        || (pCVlanSVlanEntry->u1SVlanPriorityType ==
            VLAN_SVLAN_PRIORITY_TYPE_NONE))
    {
        CLI_SET_ERR (CLI_PB_INVALID_SVLAN_PRI_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVidRegistrationRowStatus
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                testValDot1adCVidRegistrationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVidRegistrationRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4Dot1adPortNum,
                                          INT4 i4Dot1adCVidRegistrationCVid,
                                          INT4
                                          i4TestValDot1adCVidRegistrationRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

        if((VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4Dot1adCVidRegistrationCVid) ==VLAN_FALSE))
        {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adCVidRegistrationRowStatus < VLAN_ACTIVE) ||
        (i4TestValDot1adCVidRegistrationRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1adCVidRegistrationRowStatus == VLAN_CREATE_AND_GO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) i4Dot1adPortNum,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL &&
        i4TestValDot1adCVidRegistrationRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry != NULL &&
        i4TestValDot1adCVidRegistrationRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1adCVidRegistrationRowStatus == VLAN_NOT_IN_SERVICE &&
        pCVlanSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1adCVidRegistrationRowStatus == VLAN_ACTIVE &&
        pCVlanSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot1adCVidRegistrationRowStatus == VLAN_CREATE_AND_WAIT)
        &&(MemGetFreeUnits(gVlanPbPoolInfo.VlanCVlanPoolId) == 0))
    {

        	CLI_SET_ERR (CLI_PB_MAX_CVLAN_ERR);
        	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        	return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1adCVIdRegistrationRelayCVid
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationCVid

                The Object 
                testValDot1adCVIdRegistrationRelayCVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adCVIdRegistrationRelayCVid (UINT4 *pu4ErrorCode,
                                          INT4 i4Dot1adPortNum,
                                          INT4 i4Dot1adCVidRegistrationCVid,
                                          INT4
                                          i4TestValDot1adCVIdRegistrationRelayCVid)
{
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;
    tVlanCVlanSVlanEntry *pNextCVlanSVlanEntry = NULL;
    tVlanId             CVlanId = 0;
    UINT4               u4NextCVidRegistrationCVid = 0;
    INT4                i4NextDot1adPortNum = 0;
    INT4                i4PortNum = i4Dot1adPortNum;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4Dot1adPortNum;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValDot1adCVIdRegistrationRelayCVid) ==
        VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex, u2Port,
                                  (tVlanId) i4Dot1adCVidRegistrationCVid);

    if (pCVlanSVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanSVlanEntry->RelayCVlanId ==
        i4TestValDot1adCVIdRegistrationRelayCVid)
    {
        return SNMP_SUCCESS;
    }

    while ((VlanPbGetNextCVlanSVlanEntry
            (u4TblIndex, i4PortNum,
             CVlanId,
             &i4NextDot1adPortNum,
             &u4NextCVidRegistrationCVid)) == VLAN_SUCCESS)
    {
        if (i4NextDot1adPortNum != i4PortNum)
        {
            i4PortNum = i4NextDot1adPortNum;
            continue;
        }

        pNextCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry
            (u4TblIndex, i4NextDot1adPortNum, u4NextCVidRegistrationCVid);

        if (pNextCVlanSVlanEntry != NULL)
        {
            if (pNextCVlanSVlanEntry->RelayCVlanId
                == i4TestValDot1adCVIdRegistrationRelayCVid)
            {
                CLI_SET_ERR (CLI_PB_INVALID_RELAY_VLAN_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        i4PortNum = i4NextDot1adPortNum;
        CVlanId = u4NextCVidRegistrationCVid;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Dot1adCVidRegistrationTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationCVid
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Dot1adCVidRegistrationTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adPepTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot1adPepTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adPepTable (INT4 i4Dot1adPortNum,
                                        INT4 i4Dot1adCVidRegistrationSVid)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexDot1adPepTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adPepTable (INT4 *pi4Dot1adPortNum,
                                INT4 *pi4Dot1adCVidRegistrationSVid)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VlanPbGetFirstLogicalPortEntry
        (pi4Dot1adPortNum, (UINT4 *) pi4Dot1adCVidRegistrationSVid) != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexDot1adPepTable
Input       :  The Indices
Dot1adPortNum
nextDot1adPortNum
Dot1adCVidRegistrationSVid
nextDot1adCVidRegistrationSVid
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adPepTable (INT4 i4Dot1adPortNum, INT4 *pi4NextDot1adPortNum,
                               INT4 i4Dot1adCVidRegistrationSVid,
                               INT4 *pi4NextDot1adCVidRegistrationSVid)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }
    if (VlanPbGetNextLogicalPortEntry
        (i4Dot1adPortNum, (tVlanId) i4Dot1adCVidRegistrationSVid,
         pi4NextDot1adPortNum,
         (UINT4 *) pi4NextDot1adCVidRegistrationSVid) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot1adPepPvid
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
retValDot1adPepPvid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPepPvid (INT4 i4Dot1adPortNum, INT4 i4Dot1adCVidRegistrationSVid,
                     INT4 *pi4RetValDot1adPepPvid)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPepPvid = pVlanPbLogicalPortEntry->Cpvid;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot1adPepDefaultUserPriority
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
retValDot1adPepDefaultUserPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPepDefaultUserPriority (INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adCVidRegistrationSVid,
                                    INT4 *pi4RetValDot1adPepDefaultUserPriority)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPepDefaultUserPriority =
        pVlanPbLogicalPortEntry->u1DefUserPriority;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDot1adPepAccptableFrameTypes
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
retValDot1adPepAccptableFrameTypes
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPepAccptableFrameTypes (INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adCVidRegistrationSVid,
                                    INT4 *pi4RetValDot1adPepAccptableFrameTypes)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPepAccptableFrameTypes =
        pVlanPbLogicalPortEntry->u1AccptFrameType;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDot1adPepIngressFiltering
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
retValDot1adPepIngressFiltering
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPepIngressFiltering (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adCVidRegistrationSVid,
                                 INT4 *pi4RetValDot1adPepIngressFiltering)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1adPepIngressFiltering =
        pVlanPbLogicalPortEntry->u1IngressFiltering;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDot1adPepPvid
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
setValDot1adPepPvid
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPepPvid (INT4 i4Dot1adPortNum, INT4 i4Dot1adCVidRegistrationSVid,
                     INT4 i4SetValDot1adPepPvid)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pVlanPbLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
    {
    if (VlanHwSetPepPvid
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (i4Dot1adPortNum),
         (tVlanId) i4Dot1adCVidRegistrationSVid,
         (tVlanId) i4SetValDot1adPepPvid) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    }

    pVlanPbLogicalPortEntry->Cpvid = (tVlanId) i4SetValDot1adPepPvid;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPepPvid, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationSVid, i4SetValDot1adPepPvid));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1adPepDefaultUserPriority
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
setValDot1adPepDefaultUserPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPepDefaultUserPriority (INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adCVidRegistrationSVid,
                                    INT4 i4SetValDot1adPepDefaultUserPriority)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry->u1DefUserPriority =
        (UINT1) i4SetValDot1adPepDefaultUserPriority;

    if (VlanHwSetPepDefUserPriority
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (i4Dot1adPortNum),
         (tVlanId) i4Dot1adCVidRegistrationSVid,
         i4SetValDot1adPepDefaultUserPriority) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPepDefaultUserPriority,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationSVid,
                      i4SetValDot1adPepDefaultUserPriority));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1adPepAccptableFrameTypes
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
setValDot1adPepAccptableFrameTypes
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPepAccptableFrameTypes (INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adCVidRegistrationSVid,
                                    INT4 i4SetValDot1adPepAccptableFrameTypes)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry->u1AccptFrameType =
        (UINT1) i4SetValDot1adPepAccptableFrameTypes;

    if (VlanHwSetPepAccFrameType
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (i4Dot1adPortNum),
         (tVlanId) i4Dot1adCVidRegistrationSVid,
         (UINT1) i4SetValDot1adPepAccptableFrameTypes) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPepAccptableFrameTypes,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationSVid,
                      i4SetValDot1adPepAccptableFrameTypes));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1adPepIngressFiltering
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
setValDot1adPepIngressFiltering
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPepIngressFiltering (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adCVidRegistrationSVid,
                                 INT4 i4SetValDot1adPepIngressFiltering)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (VlanHwSetPepIngFiltering
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (i4Dot1adPortNum),
         (tVlanId) i4Dot1adCVidRegistrationSVid,
         (UINT1) i4SetValDot1adPepIngressFiltering) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pVlanPbLogicalPortEntry->u1IngressFiltering =
        (UINT1) i4SetValDot1adPepIngressFiltering;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPepIngressFiltering, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationSVid,
                      i4SetValDot1adPepIngressFiltering));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot1adPepPvid
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
testValDot1adPepPvid
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPepPvid (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                        INT4 i4Dot1adCVidRegistrationSVid,
                        INT4 i4TestValDot1adPepPvid)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValDot1adPepPvid) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_VLANID_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);
    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_NO_PEP_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Dot1adPepDefaultUserPriority
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
testValDot1adPepDefaultUserPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPepDefaultUserPriority (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adCVidRegistrationSVid,
                                       INT4
                                       i4TestValDot1adPepDefaultUserPriority)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adPepDefaultUserPriority < 0) ||
        (i4TestValDot1adPepDefaultUserPriority > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);
    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_NO_PEP_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Dot1adPepAccptableFrameTypes
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
testValDot1adPepAccptableFrameTypes
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPepAccptableFrameTypes (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1adPortNum,
                                       INT4 i4Dot1adCVidRegistrationSVid,
                                       INT4
                                       i4TestValDot1adPepAccptableFrameTypes)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);
    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_NO_PEP_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adPepAccptableFrameTypes == VLAN_ADMIT_ALL_FRAMES) ||
        (i4TestValDot1adPepAccptableFrameTypes ==
         VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
        || (i4TestValDot1adPepAccptableFrameTypes ==
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Dot1adPepIngressFiltering
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid

The Object 
testValDot1adPepIngressFiltering
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPepIngressFiltering (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adCVidRegistrationSVid,
                                    INT4 i4TestValDot1adPepIngressFiltering)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adPepIngressFiltering != VLAN_SNMP_TRUE) &&
        (i4TestValDot1adPepIngressFiltering != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);
    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_NO_PEP_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Dot1adPepTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Dot1adPepTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adServicePriorityRegenerationTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot1adServicePriorityRegenerationTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adServicePriorityRegenerationTable (INT4
                                                                i4Dot1adPortNum,
                                                                INT4
                                                                i4Dot1adCVidRegistrationSVid,
                                                                INT4
                                                                i4Dot1adServicePriorityRegenReceivedPriority)
{
    tVlanPbLogicalPortEntry *pLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pLogicalPortEntry = VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                                   (tVlanId)
                                                   i4Dot1adCVidRegistrationSVid);

    if (pLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_VALID (i4Dot1adServicePriorityRegenReceivedPriority) !=
        VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexDot1adServicePriorityRegenerationTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adServicePriorityRegenerationTable (INT4 *pi4Dot1adPortNum,
                                                        INT4
                                                        *pi4Dot1adCVidRegistrationSVid,
                                                        INT4
                                                        *pi4Dot1adServicePriorityRegenReceivedPriority)
{
    tVlanPbLogicalPortEntry *pLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pLogicalPortEntry =
        RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable);

    if (pLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4Dot1adPortNum = (UINT4) pLogicalPortEntry->u2CepPort;

    *pi4Dot1adCVidRegistrationSVid = (UINT4) pLogicalPortEntry->SVlanId;

    *pi4Dot1adServicePriorityRegenReceivedPriority = VLAN_MIN_PRIORITY;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexDot1adServicePriorityRegenerationTable
Input       :  The Indices
Dot1adPortNum
nextDot1adPortNum
Dot1adCVidRegistrationSVid
nextDot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority
nextDot1adServicePriorityRegenReceivedPriority
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adServicePriorityRegenerationTable (INT4 i4Dot1adPortNum,
                                                       INT4
                                                       *pi4NextDot1adPortNum,
                                                       INT4
                                                       i4Dot1adCVidRegistrationSVid,
                                                       INT4
                                                       *pi4NextDot1adCVidRegistrationSVid,
                                                       INT4
                                                       i4Dot1adServicePriorityRegenReceivedPriority,
                                                       INT4
                                                       *pi4NextDot1adServicePriorityRegenReceivedPriority)
{
    tVlanPbLogicalPortEntry *pLogicalPortEntry = NULL;
    tVlanPbLogicalPortEntry *pNextLogicalPortEntry = NULL;
    tVlanPbLogicalPortEntry TempLogicalPortEntry;
    MEMSET (&TempLogicalPortEntry, 0, sizeof (tVlanPbLogicalPortEntry));

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adServicePriorityRegenReceivedPriority > VLAN_HIGHEST_PRIORITY)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adServicePriorityRegenReceivedPriority < 0)
    {
        /*Next Priority depends on present priority.so make it -1 */
        i4Dot1adServicePriorityRegenReceivedPriority = -1;
    }

    TempLogicalPortEntry.u2CepPort = (UINT2) i4Dot1adPortNum;
    TempLogicalPortEntry.SVlanId = (tVlanId) i4Dot1adCVidRegistrationSVid;

    pLogicalPortEntry =
        RBTreeGet (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                   &TempLogicalPortEntry);
    pNextLogicalPortEntry =
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                       &TempLogicalPortEntry, NULL);

    /*Checking whether this is the last index of this table */
    if ((pNextLogicalPortEntry == NULL) && (pLogicalPortEntry != NULL) &&
        (i4Dot1adServicePriorityRegenReceivedPriority == VLAN_HIGHEST_PRIORITY))
    {
        return SNMP_FAILURE;
    }

    /*Checking whether there are no more index in this table */
    if ((pLogicalPortEntry == NULL) && (pNextLogicalPortEntry == NULL))
    {
        return SNMP_FAILURE;
    }

    /*Checking whether the index is first in the table */
    if (pLogicalPortEntry == NULL)
    {
        /*Next priority depends on present priority.so make it -1 */
        *pi4NextDot1adPortNum = pNextLogicalPortEntry->u2CepPort;
        *pi4NextDot1adCVidRegistrationSVid = pNextLogicalPortEntry->SVlanId;
        i4Dot1adServicePriorityRegenReceivedPriority = -1;
    }
    else
    {
        *pi4NextDot1adPortNum = pLogicalPortEntry->u2CepPort;
        *pi4NextDot1adCVidRegistrationSVid = pLogicalPortEntry->SVlanId;
    }

    if (i4Dot1adServicePriorityRegenReceivedPriority < VLAN_HIGHEST_PRIORITY)
    {
        *pi4NextDot1adServicePriorityRegenReceivedPriority =
            i4Dot1adServicePriorityRegenReceivedPriority + 1;
    }

    if (i4Dot1adServicePriorityRegenReceivedPriority == VLAN_HIGHEST_PRIORITY)
    {
        *pi4NextDot1adPortNum = pNextLogicalPortEntry->u2CepPort;
        *pi4NextDot1adCVidRegistrationSVid = pNextLogicalPortEntry->SVlanId;
        *pi4NextDot1adServicePriorityRegenReceivedPriority = VLAN_MIN_PRIORITY;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot1adServicePriorityRegenRegeneratedPriority
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority

The Object 
retValDot1adServicePriorityRegenRegeneratedPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adServicePriorityRegenRegeneratedPriority (INT4 i4Dot1adPortNum,
                                                     INT4
                                                     i4Dot1adCVidRegistrationSVid,
                                                     INT4
                                                     i4Dot1adServicePriorityRegenReceivedPriority,
                                                     INT4
                                                     *pi4RetValDot1adServicePriorityRegenRegeneratedPriority)
{
    tVlanPbLogicalPortEntry *pLogicalPortEntry = NULL;

    pLogicalPortEntry = VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                                   (tVlanId)
                                                   i4Dot1adCVidRegistrationSVid);

    if (pLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adServicePriorityRegenRegeneratedPriority =
        pLogicalPortEntry->
        au1RegenPriority[i4Dot1adServicePriorityRegenReceivedPriority];

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDot1adServicePriorityRegenRegeneratedPriority
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority

The Object 
setValDot1adServicePriorityRegenRegeneratedPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adServicePriorityRegenRegeneratedPriority (INT4 i4Dot1adPortNum,
                                                     INT4
                                                     i4Dot1adCVidRegistrationSVid,
                                                     INT4
                                                     i4Dot1adServicePriorityRegenReceivedPriority,
                                                     INT4
                                                     i4SetValDot1adServicePriorityRegenRegeneratedPriority)
{
    tVlanPbLogicalPortEntry *pLogicalPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pLogicalPortEntry = VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                                   (tVlanId)
                                                   i4Dot1adCVidRegistrationSVid);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pLogicalPortEntry->
        au1RegenPriority[i4Dot1adServicePriorityRegenReceivedPriority]
        != i4SetValDot1adServicePriorityRegenRegeneratedPriority)
    {
        if (VlanHwSetServicePriRegenEntry (VLAN_CURR_CONTEXT_ID (),
                                           VLAN_GET_PHY_PORT
                                           (i4Dot1adPortNum),
                                           (tVlanId)
                                           i4Dot1adCVidRegistrationSVid,
                                           i4Dot1adServicePriorityRegenReceivedPriority,
                                           i4SetValDot1adServicePriorityRegenRegeneratedPriority)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    pLogicalPortEntry->
        au1RegenPriority[i4Dot1adServicePriorityRegenReceivedPriority] =
        (UINT1) i4SetValDot1adServicePriorityRegenRegeneratedPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          Dot1adMIServicePriorityRegenRegeneratedPriority,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationSVid,
                      i4Dot1adServicePriorityRegenReceivedPriority,
                      i4SetValDot1adServicePriorityRegenRegeneratedPriority));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot1adServicePriorityRegenRegeneratedPriority
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority

The Object 
testValDot1adServicePriorityRegenRegeneratedPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adServicePriorityRegenRegeneratedPriority (UINT4 *pu4ErrorCode,
                                                        INT4 i4Dot1adPortNum,
                                                        INT4
                                                        i4Dot1adCVidRegistrationSVid,
                                                        INT4
                                                        i4Dot1adServicePriorityRegenReceivedPriority,
                                                        INT4
                                                        i4TestValDot1adServicePriorityRegenRegeneratedPriority)
{
    tVlanPbLogicalPortEntry *pLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_VALID (i4Dot1adServicePriorityRegenReceivedPriority) !=
        VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_VALID
        (i4TestValDot1adServicePriorityRegenRegeneratedPriority) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pLogicalPortEntry = VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                                   (tVlanId)
                                                   i4Dot1adCVidRegistrationSVid);

    if (pLogicalPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PB_NO_PEP_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Dot1adServicePriorityRegenerationTable
Input       :  The Indices
Dot1adPortNum
Dot1adCVidRegistrationSVid
Dot1adServicePriorityRegenReceivedPriority
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Dot1adServicePriorityRegenerationTable (UINT4 *pu4ErrorCode,
                                                tSnmpIndexList * pSnmpIndexList,
                                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adPcpDecodingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot1adPcpDecodingTable
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adPcpDecodingTable (INT4 i4Dot1adPortNum,
                                                INT4
                                                i4Dot1adPcpDecodingPcpSelRow,
                                                INT4
                                                i4Dot1adPcpDecodingPcpValue)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1adPortNum) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PCP_ROW_VALID (i4Dot1adPcpDecodingPcpSelRow) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_PRIORITY_VALID (i4Dot1adPcpDecodingPcpValue) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexDot1adPcpDecodingTable
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adPcpDecodingTable (INT4 *pi4Dot1adPortNum,
                                        INT4 *pi4Dot1adPcpDecodingPcpSelRow,
                                        INT4 *pi4Dot1adPcpDecodingPcpValue)
{

    return (nmhGetNextIndexDot1adPcpDecodingTable (0, pi4Dot1adPortNum, 0,
                                                   pi4Dot1adPcpDecodingPcpSelRow,
                                                   0,
                                                   pi4Dot1adPcpDecodingPcpValue));

}

/****************************************************************************
Function    :  nmhGetNextIndexDot1adPcpDecodingTable
Input       :  The Indices
Dot1adPortNum
nextDot1adPortNum
Dot1adPcpDecodingPcpSelRow
nextDot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue
nextDot1adPcpDecodingPcpValue
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adPcpDecodingTable (INT4 i4Dot1adPortNum,
                                       INT4 *pi4NextDot1adPortNum,
                                       INT4 i4Dot1adPcpDecodingPcpSelRow,
                                       INT4 *pi4NextDot1adPcpDecodingPcpSelRow,
                                       INT4 i4Dot1adPcpDecodingPcpValue,
                                       INT4 *pi4NextDot1adPcpDecodingPcpValue)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Port = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adPortNum > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adPortNum < 0)
    {
        i4Dot1adPortNum = 0;
    }
    if (i4Dot1adPcpDecodingPcpSelRow < 0)
    {
        i4Dot1adPcpDecodingPcpSelRow = 0;
    }
    if (i4Dot1adPcpDecodingPcpValue < 0)
    {
        i4Dot1adPcpDecodingPcpValue = -1;
    }

    if (i4Dot1adPortNum == 0)
    {
        if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pi4NextDot1adPortNum = (INT4) u2Port;
        *pi4NextDot1adPcpDecodingPcpSelRow = VLAN_8P0D_SEL_ROW;
        *pi4NextDot1adPcpDecodingPcpValue = VLAN_MIN_PRIORITY;

        return SNMP_SUCCESS;
    }
    else
    {
        /*Checking whether the Port is valid */
        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (i4Dot1adPortNum);

        if (pVlanPbPortEntry == NULL)
        {
            if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) !=
                VLAN_SUCCESS)
            {
                /* No more ports in this table */
                return SNMP_FAILURE;
            }

            *pi4NextDot1adPortNum = (INT4) u2Port;
            *pi4NextDot1adPcpDecodingPcpSelRow = VLAN_8P0D_SEL_ROW;
            *pi4NextDot1adPcpDecodingPcpValue = VLAN_MIN_PRIORITY;

            return SNMP_SUCCESS;
        }
    }

    if ((i4Dot1adPcpDecodingPcpValue < VLAN_HIGHEST_PRIORITY) &&
        (i4Dot1adPcpDecodingPcpSelRow <= VLAN_5P3D_SEL_ROW))

    {
        if (i4Dot1adPcpDecodingPcpSelRow == 0)
        {
            *pi4NextDot1adPcpDecodingPcpSelRow = VLAN_8P0D_SEL_ROW;
            *pi4NextDot1adPcpDecodingPcpValue = VLAN_MIN_PRIORITY;
        }
        else
        {
            *pi4NextDot1adPcpDecodingPcpSelRow = i4Dot1adPcpDecodingPcpSelRow;
            *pi4NextDot1adPcpDecodingPcpValue = i4Dot1adPcpDecodingPcpValue + 1;
        }

        *pi4NextDot1adPortNum = i4Dot1adPortNum;
        return SNMP_SUCCESS;
    }

    if ((i4Dot1adPcpDecodingPcpValue >= VLAN_HIGHEST_PRIORITY) &&
        (i4Dot1adPcpDecodingPcpSelRow < VLAN_5P3D_SEL_ROW))
    {
        *pi4NextDot1adPcpDecodingPcpSelRow = i4Dot1adPcpDecodingPcpSelRow + 1;
        *pi4NextDot1adPcpDecodingPcpValue = VLAN_MIN_PRIORITY;
        *pi4NextDot1adPortNum = i4Dot1adPortNum;

        return SNMP_SUCCESS;
    }

    if (i4Dot1adPcpDecodingPcpSelRow >= VLAN_5P3D_SEL_ROW)
    {
        if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) != VLAN_SUCCESS)
        {
            /* No more ports in this table */
            return SNMP_FAILURE;
        }

        *pi4NextDot1adPortNum = (INT4) u2Port;
        *pi4NextDot1adPcpDecodingPcpSelRow = VLAN_8P0D_SEL_ROW;
        *pi4NextDot1adPcpDecodingPcpValue = VLAN_MIN_PRIORITY;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot1adPcpDecodingPriority
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue

The Object 
retValDot1adPcpDecodingPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPcpDecodingPriority (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adPcpDecodingPcpSelRow,
                                 INT4 i4Dot1adPcpDecodingPcpValue,
                                 INT4 *pi4RetValDot1adPcpDecodingPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1adPcpDecodingPriority
        = VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                       i4Dot1adPcpDecodingPcpSelRow,
                                       i4Dot1adPcpDecodingPcpValue);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDot1adPcpDecodingDropEligible
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue

The Object 
retValDot1adPcpDecodingDropEligible
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPcpDecodingDropEligible (INT4 i4Dot1adPortNum,
                                     INT4 i4Dot1adPcpDecodingPcpSelRow,
                                     INT4 i4Dot1adPcpDecodingPcpValue,
                                     INT4
                                     *pi4RetValDot1adPcpDecodingDropEligible)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                          i4Dot1adPcpDecodingPcpSelRow,
                                          i4Dot1adPcpDecodingPcpValue) ==
        VLAN_DE_TRUE)
    {
        *pi4RetValDot1adPcpDecodingDropEligible = VLAN_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1adPcpDecodingDropEligible = VLAN_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDot1adPcpDecodingPriority
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue

The Object 
setValDot1adPcpDecodingPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPcpDecodingPriority (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adPcpDecodingPcpSelRow,
                                 INT4 i4Dot1adPcpDecodingPcpValue,
                                 INT4 i4SetValDot1adPcpDecodingPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tHwVlanPbPcpInfo    PcpTblEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&PcpTblEntry, 0, sizeof (tHwVlanPbPcpInfo));

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    PcpTblEntry.u2PcpSelRow = (UINT2) i4Dot1adPcpDecodingPcpSelRow;
    PcpTblEntry.u1DropEligible =
        VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                          i4Dot1adPcpDecodingPcpSelRow,
                                          i4Dot1adPcpDecodingPcpValue);
    PcpTblEntry.u2PcpValue = (UINT2) i4Dot1adPcpDecodingPcpValue;

    PcpTblEntry.u2Priority = (UINT2) i4SetValDot1adPcpDecodingPriority;

    if (VlanHwSetPcpDecodTbl (VLAN_CURR_CONTEXT_ID (),
                              VLAN_GET_PHY_PORT (i4Dot1adPortNum),
                              PcpTblEntry) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                 i4Dot1adPcpDecodingPcpSelRow,
                                 i4Dot1adPcpDecodingPcpValue) =
        (UINT1) i4SetValDot1adPcpDecodingPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPcpDecodingPriority, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adPcpDecodingPcpSelRow,
                      i4Dot1adPcpDecodingPcpValue,
                      i4SetValDot1adPcpDecodingPriority));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1adPcpDecodingDropEligible
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue

The Object 
setValDot1adPcpDecodingDropEligible
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPcpDecodingDropEligible (INT4 i4Dot1adPortNum,
                                     INT4 i4Dot1adPcpDecodingPcpSelRow,
                                     INT4 i4Dot1adPcpDecodingPcpValue,
                                     INT4 i4SetValDot1adPcpDecodingDropEligible)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tHwVlanPbPcpInfo    PcpTblEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&PcpTblEntry, 0, sizeof (tHwVlanPbPcpInfo));

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValDot1adPcpDecodingDropEligible == VLAN_SNMP_TRUE)
    {
        i4SetValDot1adPcpDecodingDropEligible = VLAN_DE_TRUE;
    }
    else
    {
        i4SetValDot1adPcpDecodingDropEligible = VLAN_DE_FALSE;
    }

    PcpTblEntry.u2PcpSelRow = (UINT2) i4Dot1adPcpDecodingPcpSelRow;
    PcpTblEntry.u2Priority =
        VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                     i4Dot1adPcpDecodingPcpSelRow,
                                     i4Dot1adPcpDecodingPcpValue);
    PcpTblEntry.u2PcpValue = (UINT2) i4Dot1adPcpDecodingPcpValue;

    PcpTblEntry.u1DropEligible = (UINT1) i4SetValDot1adPcpDecodingDropEligible;

    if (VlanHwSetPcpDecodTbl (VLAN_CURR_CONTEXT_ID (),
                              VLAN_GET_PHY_PORT (i4Dot1adPortNum),
                              PcpTblEntry) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                      i4Dot1adPcpDecodingPcpSelRow,
                                      i4Dot1adPcpDecodingPcpValue) =
        (UINT1) i4SetValDot1adPcpDecodingDropEligible;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPcpDecodingDropEligible,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adPcpDecodingPcpSelRow,
                      i4Dot1adPcpDecodingPcpValue,
                      i4SetValDot1adPcpDecodingDropEligible));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot1adPcpDecodingPriority
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue

The Object 
testValDot1adPcpDecodingPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPcpDecodingPriority (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adPcpDecodingPcpSelRow,
                                    INT4 i4Dot1adPcpDecodingPcpValue,
                                    INT4 i4TestValDot1adPcpDecodingPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1DropEligible;

    if (nmhValidateIndexInstanceDot1adPcpDecodingTable (i4Dot1adPortNum,
                                                        i4Dot1adPcpDecodingPcpSelRow,
                                                        i4Dot1adPcpDecodingPcpValue)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adPcpDecodingPriority < VLAN_MIN_PRIORITY) ||
        (i4TestValDot1adPcpDecodingPriority > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
    {
        if (i4Dot1adPcpDecodingPcpSelRow != VLAN_8P0D_SEL_ROW)
        {
            CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (i4TestValDot1adPcpDecodingPriority !=
            VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                         i4Dot1adPcpDecodingPcpSelRow,
                                         i4Dot1adPcpDecodingPcpValue))
        {
            CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValDot1adPcpDecodingPriority ==
        VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                     i4Dot1adPcpDecodingPcpSelRow,
                                     i4Dot1adPcpDecodingPcpValue))
    {
        return SNMP_SUCCESS;
    }

    u1DropEligible = VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                                       i4Dot1adPcpDecodingPcpSelRow,
                                                       i4Dot1adPcpDecodingPcpValue);

    /*
     * The Drop Eligible value passed to the validate function for 
     * PCP decoding values must always be VLAN_SNMP_TRUE or VLAN_SNMP_FALSE.
     * Because the validate routine is getting called from the test routine 
     * of DropEligible as well.
     */
    if (u1DropEligible == VLAN_DE_FALSE)
    {
        u1DropEligible = VLAN_SNMP_FALSE;
    }
    else
        u1DropEligible = VLAN_SNMP_TRUE;

    if (VlanPbValidatePcpDecodingValues
        (i4Dot1adPortNum, i4Dot1adPcpDecodingPcpSelRow,
         i4Dot1adPcpDecodingPcpValue,
         (UINT1) i4TestValDot1adPcpDecodingPriority,
         u1DropEligible) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Dot1adPcpDecodingDropEligible
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue

The Object 
testValDot1adPcpDecodingDropEligible
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPcpDecodingDropEligible (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1adPortNum,
                                        INT4 i4Dot1adPcpDecodingPcpSelRow,
                                        INT4 i4Dot1adPcpDecodingPcpValue,
                                        INT4
                                        i4TestValDot1adPcpDecodingDropEligible)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1Priority;
    UINT1               u1TestDropEligible;

    if (nmhValidateIndexInstanceDot1adPcpDecodingTable (i4Dot1adPortNum,
                                                        i4Dot1adPcpDecodingPcpSelRow,
                                                        i4Dot1adPcpDecodingPcpValue)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adPcpDecodingDropEligible != VLAN_SNMP_TRUE) &&
        (i4TestValDot1adPcpDecodingDropEligible != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
    {
        if (i4Dot1adPcpDecodingPcpSelRow != VLAN_8P0D_SEL_ROW)
        {
            CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (i4TestValDot1adPcpDecodingDropEligible != VLAN_SNMP_FALSE)
        {
            CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValDot1adPcpDecodingDropEligible == VLAN_SNMP_TRUE)
    {
        u1TestDropEligible = VLAN_DE_TRUE;
    }
    else
    {
        u1TestDropEligible = VLAN_DE_FALSE;
    }
    if (u1TestDropEligible ==
        VLAN_PB_PCP_DECODE_DROP_ELIGIBLE (pVlanPbPortEntry,
                                          i4Dot1adPcpDecodingPcpSelRow,
                                          i4Dot1adPcpDecodingPcpValue))
    {
        return SNMP_SUCCESS;
    }

    u1Priority = VLAN_PB_PCP_DECODE_PRIORITY (pVlanPbPortEntry,
                                              i4Dot1adPcpDecodingPcpSelRow,
                                              i4Dot1adPcpDecodingPcpValue);

    if (VlanPbValidatePcpDecodingValues
        (i4Dot1adPortNum, i4Dot1adPcpDecodingPcpSelRow,
         i4Dot1adPcpDecodingPcpValue, u1Priority,
         (UINT1) i4TestValDot1adPcpDecodingDropEligible) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Dot1adPcpDecodingTable
Input       :  The Indices
Dot1adPortNum
Dot1adPcpDecodingPcpSelRow
Dot1adPcpDecodingPcpValue
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Dot1adPcpDecodingTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adPcpEncodingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot1adPcpEncodingTable
Input       :  The Indices
Dot1adPortNum
Dot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adPcpEncodingTable (INT4 i4Dot1adPortNum,
                                                INT4
                                                i4Dot1adPcpEncodingPcpSelRow,
                                                INT4
                                                i4Dot1adPcpEncodingPriority,
                                                INT4
                                                i4Dot1adPcpEncodingDropEligible)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1adPortNum) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PCP_ROW_VALID (i4Dot1adPcpEncodingPcpSelRow) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_PRIORITY_VALID (i4Dot1adPcpEncodingPriority) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1adPcpEncodingDropEligible != VLAN_SNMP_TRUE) &&
        (i4Dot1adPcpEncodingDropEligible != VLAN_SNMP_FALSE))
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexDot1adPcpEncodingTable
Input       :  The Indices
Dot1adPortNum
Dot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adPcpEncodingTable (INT4 *pi4Dot1adPortNum,
                                        INT4 *pi4Dot1adPcpEncodingPcpSelRow,
                                        INT4 *pi4Dot1adPcpEncodingPriority,
                                        INT4 *pi4Dot1adPcpEncodingDropEligible)
{

    return nmhGetNextIndexDot1adPcpEncodingTable
        (0, pi4Dot1adPortNum, 0, pi4Dot1adPcpEncodingPcpSelRow, 0,
         pi4Dot1adPcpEncodingPriority, 0, pi4Dot1adPcpEncodingDropEligible);

}

/****************************************************************************
Function    :  nmhGetNextIndexDot1adPcpEncodingTable
Input       :  The Indices
Dot1adPortNum
nextDot1adPortNum
Dot1adPcpEncodingPcpSelRow
nextDot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
nextDot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible
nextDot1adPcpEncodingDropEligible
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adPcpEncodingTable (INT4 i4Dot1adPortNum,
                                       INT4 *pi4NextDot1adPortNum,
                                       INT4 i4Dot1adPcpEncodingPcpSelRow,
                                       INT4 *pi4NextDot1adPcpEncodingPcpSelRow,
                                       INT4 i4Dot1adPcpEncodingPriority,
                                       INT4 *pi4NextDot1adPcpEncodingPriority,
                                       INT4 i4Dot1adPcpEncodingDropEligible,
                                       INT4
                                       *pi4NextDot1adPcpEncodingDropEligible)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Port = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adPortNum > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adPortNum < 0)
    {
        i4Dot1adPortNum = 0;
    }
    if (i4Dot1adPcpEncodingPcpSelRow < 0)
    {
        i4Dot1adPcpEncodingPcpSelRow = VLAN_INVALID_SEL_ROW;
    }
    if (i4Dot1adPcpEncodingPriority < 0)
    {
        i4Dot1adPcpEncodingPriority = -1;
    }

    if (i4Dot1adPortNum == 0)
    {
        if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pi4NextDot1adPortNum = (INT4) u2Port;
        *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
        *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
        *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;

        return SNMP_SUCCESS;
    }
    else
    {
        /*Checking whether the Port is valid */
        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (i4Dot1adPortNum);

        if (pVlanPbPortEntry == NULL)
        {
            if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) !=
                VLAN_SUCCESS)
            {
                /* No more ports in this table */
                return SNMP_FAILURE;
            }

            *pi4NextDot1adPortNum = (INT4) u2Port;
            *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
            *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
            *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;

            return SNMP_SUCCESS;
        }
    }

    /* Takes care if these type of index(4,9,0,1) --> (5,1,0,0) */
    if (i4Dot1adPcpEncodingPcpSelRow > VLAN_5P3D_SEL_ROW)
    {

        if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        *pi4NextDot1adPortNum = (INT4) u2Port;
        *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
        *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
        *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;

        return SNMP_SUCCESS;
    }

    if ((i4Dot1adPcpEncodingDropEligible == VLAN_SNMP_TRUE) &&
        (i4Dot1adPcpEncodingPriority < VLAN_HIGHEST_PRIORITY))
    {
        if (i4Dot1adPcpEncodingPcpSelRow == VLAN_INVALID_SEL_ROW)
        {
            *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
            *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
            *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;
        }
        else
        {
            *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_FALSE;

            if (i4Dot1adPcpEncodingPriority == -1)
            {
                *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
                *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;
            }
            else
            {
                *pi4NextDot1adPcpEncodingPriority = i4Dot1adPcpEncodingPriority;
            }

            *pi4NextDot1adPcpEncodingPcpSelRow = i4Dot1adPcpEncodingPcpSelRow;
        }

        *pi4NextDot1adPortNum = i4Dot1adPortNum;

        return SNMP_SUCCESS;
    }

    if ((i4Dot1adPcpEncodingDropEligible == VLAN_SNMP_FALSE) &&
        (i4Dot1adPcpEncodingPriority < VLAN_HIGHEST_PRIORITY))
    {

        if (i4Dot1adPcpEncodingPcpSelRow == VLAN_INVALID_SEL_ROW)
        {
            *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
            *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
            *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;
        }
        else
        {
            *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;
            *pi4NextDot1adPcpEncodingPriority = i4Dot1adPcpEncodingPriority + 1;
            *pi4NextDot1adPcpEncodingPcpSelRow = i4Dot1adPcpEncodingPcpSelRow;
        }

        *pi4NextDot1adPortNum = i4Dot1adPortNum;
        return SNMP_SUCCESS;
    }

    if ((i4Dot1adPcpEncodingPriority == VLAN_HIGHEST_PRIORITY) &&
        (i4Dot1adPcpEncodingPcpSelRow <= VLAN_5P3D_SEL_ROW) &&
        (i4Dot1adPcpEncodingDropEligible == VLAN_SNMP_TRUE))
    {
        *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_FALSE;
        *pi4NextDot1adPcpEncodingPriority = i4Dot1adPcpEncodingPriority;
        *pi4NextDot1adPcpEncodingPcpSelRow = i4Dot1adPcpEncodingPcpSelRow;
        *pi4NextDot1adPortNum = i4Dot1adPortNum;

        return SNMP_SUCCESS;
    }

    if ((i4Dot1adPcpEncodingPriority >= VLAN_HIGHEST_PRIORITY) &&
        (i4Dot1adPcpEncodingPcpSelRow < VLAN_5P3D_SEL_ROW))
    {
        *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;
        *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
        *pi4NextDot1adPcpEncodingPcpSelRow = i4Dot1adPcpEncodingPcpSelRow + 1;
        *pi4NextDot1adPortNum = i4Dot1adPortNum;

        return SNMP_SUCCESS;
    }

    if (i4Dot1adPcpEncodingPcpSelRow == VLAN_5P3D_SEL_ROW)
    {
        if (VlanGetNextPort ((UINT2) i4Dot1adPortNum, &u2Port) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        *pi4NextDot1adPortNum = (INT4) u2Port;
        *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
        *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
        *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;

        return SNMP_SUCCESS;
    }
    if ((i4Dot1adPcpEncodingPcpSelRow == VLAN_INVALID_SEL_ROW) ||
        (i4Dot1adPcpEncodingPriority == -1) ||
        (i4Dot1adPcpEncodingDropEligible == VLAN_INVALID_DE))
    {
        *pi4NextDot1adPortNum = i4Dot1adPortNum;
        *pi4NextDot1adPcpEncodingPcpSelRow = VLAN_8P0D_SEL_ROW;
        *pi4NextDot1adPcpEncodingPriority = VLAN_MIN_PRIORITY;
        *pi4NextDot1adPcpEncodingDropEligible = VLAN_SNMP_TRUE;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot1adPcpEncodingPcpValue
Input       :  The Indices
Dot1adPortNum
Dot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible

The Object 
retValDot1adPcpEncodingPcpValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1adPcpEncodingPcpValue (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adPcpEncodingPcpSelRow,
                                 INT4 i4Dot1adPcpEncodingPriority,
                                 INT4 i4Dot1adPcpEncodingDropEligible,
                                 INT4 *pi4RetValDot1adPcpEncodingPcpValue)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adPcpEncodingDropEligible == VLAN_SNMP_TRUE)
    {
        i4Dot1adPcpEncodingDropEligible = VLAN_DE_TRUE;
    }
    else
    {
        i4Dot1adPcpEncodingDropEligible = VLAN_DE_FALSE;
    }

    *pi4RetValDot1adPcpEncodingPcpValue =
        VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry, i4Dot1adPcpEncodingPcpSelRow,
                                i4Dot1adPcpEncodingPriority,
                                i4Dot1adPcpEncodingDropEligible);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDot1adPcpEncodingPcpValue
Input       :  The Indices
Dot1adPortNum
Dot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible

The Object 
setValDot1adPcpEncodingPcpValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1adPcpEncodingPcpValue (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adPcpEncodingPcpSelRow,
                                 INT4 i4Dot1adPcpEncodingPriority,
                                 INT4 i4Dot1adPcpEncodingDropEligible,
                                 INT4 i4SetValDot1adPcpEncodingPcpValue)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tHwVlanPbPcpInfo    PcpTblEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&PcpTblEntry, 0, sizeof (tHwVlanPbPcpInfo));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1adPcpEncodingDropEligible == VLAN_SNMP_TRUE)
    {
        i4Dot1adPcpEncodingDropEligible = VLAN_DE_TRUE;
    }
    else
    {
        i4Dot1adPcpEncodingDropEligible = VLAN_DE_FALSE;
    }

    PcpTblEntry.u2PcpSelRow = (UINT2) i4Dot1adPcpEncodingPcpSelRow;
    PcpTblEntry.u2Priority = (UINT2) i4Dot1adPcpEncodingPriority;
    PcpTblEntry.u1DropEligible = (UINT1) i4Dot1adPcpEncodingDropEligible;

    PcpTblEntry.u2PcpValue = (UINT2) i4SetValDot1adPcpEncodingPcpValue;

    if (VlanHwSetPcpEncodTbl (VLAN_CURR_CONTEXT_ID (),
                              VLAN_GET_PHY_PORT (i4Dot1adPortNum),
                              PcpTblEntry) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry, i4Dot1adPcpEncodingPcpSelRow,
                            i4Dot1adPcpEncodingPriority,
                            i4Dot1adPcpEncodingDropEligible) =
        (UINT1) i4SetValDot1adPcpEncodingPcpValue;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot1adMIPcpEncodingPcpValue, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 4, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adPcpEncodingPcpSelRow,
                      i4Dot1adPcpEncodingPriority,
                      i4Dot1adPcpEncodingDropEligible,
                      i4SetValDot1adPcpEncodingPcpValue));

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot1adPcpEncodingPcpValue
Input       :  The Indices
Dot1adPortNum
Dot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible

The Object 
testValDot1adPcpEncodingPcpValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1adPcpEncodingPcpValue (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adPcpEncodingPcpSelRow,
                                    INT4 i4Dot1adPcpEncodingPriority,
                                    INT4 i4Dot1adPcpEncodingDropEligible,
                                    INT4 i4TestValDot1adPcpEncodingPcpValue)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Priority = 0;
    INT4                i4PcpVal;

    if (nmhValidateIndexInstanceDot1adPcpEncodingTable (i4Dot1adPortNum,
                                                        i4Dot1adPcpEncodingPcpSelRow,
                                                        i4Dot1adPcpEncodingPriority,
                                                        i4Dot1adPcpEncodingDropEligible)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1adPcpEncodingPcpValue < VLAN_MIN_PRIORITY) ||
        (i4TestValDot1adPcpEncodingPcpValue > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4Dot1adPcpEncodingDropEligible == VLAN_SNMP_TRUE)
    {
        i4Dot1adPcpEncodingDropEligible = VLAN_DE_TRUE;
    }
    else
    {
        i4Dot1adPcpEncodingDropEligible = VLAN_DE_FALSE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);
    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
    {
        if (i4Dot1adPcpEncodingPcpSelRow != VLAN_8P0D_SEL_ROW)
        {
            CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (i4TestValDot1adPcpEncodingPcpValue !=
            pVlanPbPortEntry->
            au1PcpEncoding[i4Dot1adPcpEncodingPcpSelRow][u2Priority]
            [i4Dot1adPcpEncodingDropEligible])
        {
            CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValDot1adPcpEncodingPcpValue ==
        VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry,
                                i4Dot1adPcpEncodingPcpSelRow,
                                u2Priority, i4Dot1adPcpEncodingDropEligible))

    {
        return SNMP_SUCCESS;
    }
    /* 
     * For any two priority values with the same drop_eligible value, the
     * lower priority shall not map to a higher PCP than the higher
     * priority.
     */
    for (u2Priority = 0; u2Priority < 7; u2Priority++)
    {
        i4PcpVal = VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry,
                                           i4Dot1adPcpEncodingPcpSelRow,
                                           u2Priority,
                                           i4Dot1adPcpEncodingDropEligible);

        if (i4Dot1adPcpEncodingPriority < u2Priority)
        {
            /* The configured priority is less than some of the other 
             * priorities with same drop eligible.
             */
            if (i4TestValDot1adPcpEncodingPcpValue > i4PcpVal)
            {
                /* The PCP value is greater */
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    /*
     * With the exception of values 0 and 1, any priority value combined with
     * drop_eligible true shall not map to a higher PCP than the same priority
     * combined with drop_eligible false.
     */
    if ((i4Dot1adPcpEncodingPriority != 0)
        && (i4Dot1adPcpEncodingPriority != 1))
    {
        if (i4Dot1adPcpEncodingDropEligible == VLAN_DE_TRUE)
        {
            i4PcpVal = VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry,
                                               i4Dot1adPcpEncodingPcpSelRow,
                                               i4Dot1adPcpEncodingPriority,
                                               VLAN_DE_FALSE);

            if (i4TestValDot1adPcpEncodingPcpValue > i4PcpVal)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                return SNMP_FAILURE;
            }
        }
        if (i4Dot1adPcpEncodingDropEligible == VLAN_DE_FALSE)
        {
            i4PcpVal =
                VLAN_PB_PCP_ENCODE_PCP (pVlanPbPortEntry,
                                        i4Dot1adPcpEncodingPcpSelRow,
                                        i4Dot1adPcpEncodingPriority,
                                        VLAN_DE_TRUE);

            if (i4TestValDot1adPcpEncodingPcpValue < i4PcpVal)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Dot1adPcpEncodingTable
Input       :  The Indices
Dot1adPortNum
Dot1adPcpEncodingPcpSelRow
Dot1adPcpEncodingPriority
Dot1adPcpEncodingDropEligible
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Dot1adPcpEncodingTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
