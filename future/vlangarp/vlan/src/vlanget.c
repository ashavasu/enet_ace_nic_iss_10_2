/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanget.c,v 1.83.2.1 2018/03/15 13:00:03 siva Exp $
 *
 * Description     : This file contains routines to get the VLAN objects.
 *
 *****************************************************************************/

/*****************************************************************************/
/*  FILE NAME             : vlanget.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains routines to get the VLAN      */
/*                          objects.                                         */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"

/************************ Q-MIB Set routines *******************************/

/****************************************************************************
 Function    :  nmhGetDot1qVlanVersionNumber
 Input       :  The Indices

                The Object 
                retValDot1qVlanVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanVersionNumber (INT4 *pi4RetValDot1qVlanVersionNumber)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qVlanVersionNumber = VLAN_VERSION;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qVlanVersionNumber = VLAN_VERSION;
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qVlanVersionNumber =
        gpVlanContextInfo->VlanInfo.u1VlanVersion;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanLearningMode
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanLearningMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanLearningMode (INT4 *pi4RetValDot1qFutureVlanLearningMode)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanLearningMode = VLAN_INVALID_LEARNING;
        return SNMP_SUCCESS;
    }
    *pi4RetValDot1qFutureVlanLearningMode = (INT4) VlanGetVlanLearningMode ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanHybridTypeDefault
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanHybridTypeDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanHybridTypeDefault (INT4
                                        *pi4RetValDot1qFutureVlanHybridTypeDefault)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanHybridTypeDefault = VLAN_INVALID_LEARNING;
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanHybridTypeDefault =
        (INT4) gpVlanContextInfo->VlanInfo.u1DefConstType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanGlobalMacLearningStatus
 Input       :  The Indices

                The Object
                retValDot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanGlobalMacLearningStatus (INT4
                                              *pi4RetValDot1qFutureVlanGlobalMacLearningStatus)
{
    *pi4RetValDot1qFutureVlanGlobalMacLearningStatus =
        (INT4) VLAN_GLOBAL_MAC_LEARNING_STATUS ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanApplyEnhancedFilteringCriteria
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureVlanApplyEnhancedFilteringCriteria
    (INT4 *pi4RetValDot1qFutureVlanApplyEnhancedFilteringCriteria)
{
    *pi4RetValDot1qFutureVlanApplyEnhancedFilteringCriteria =
        (INT4) VLAN_CURR_CTX_ENHANCE_FILTERING_STATUS ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanSwStatsEnabled
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanSwStatsEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanSwStatsEnabled (INT4
                                     *pi4RetValDot1qFutureVlanSwStatsEnabled)
{
    *pi4RetValDot1qFutureVlanSwStatsEnabled = (INT4) gu1SwStatsEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qMaxVlanId
 Input       :  The Indices

                The Object 
                retValDot1qMaxVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qMaxVlanId (INT4 *pi4RetValDot1qMaxVlanId)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qMaxVlanId = VLAN_MAX_VLAN_ID;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qMaxVlanId = VLAN_MAX_VLAN_ID;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qMaxVlanId = gpVlanContextInfo->VlanInfo.MaxVlanId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qMaxSupportedVlans
 Input       :  The Indices

                The Object 
                retValDot1qMaxSupportedVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qMaxSupportedVlans (UINT4 *pu4RetValDot1qMaxSupportedVlans)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4RetValDot1qMaxSupportedVlans = VLAN_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4RetValDot1qMaxSupportedVlans = VLAN_INIT_VAL;

        return SNMP_SUCCESS;
    }

    *pu4RetValDot1qMaxSupportedVlans = gpVlanContextInfo->VlanInfo.u4MaxVlans;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qNumVlans
 Input       :  The Indices

                The Object 
                retValDot1qNumVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qNumVlans (UINT4 *pu4RetValDot1qNumVlans)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4RetValDot1qNumVlans = VLAN_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4RetValDot1qNumVlans = VLAN_INIT_VAL;

        return SNMP_SUCCESS;
    }

    *pu4RetValDot1qNumVlans = gpVlanContextInfo->VlanInfo.u4NumActiveVlans;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFdbDynamicCount
 Input       :  The Indices
                Dot1qFdbId

                The Object 
                retValDot1qFdbDynamicCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFdbDynamicCount (UINT4 u4Dot1qFdbId,
                            UINT4 *pu4RetValDot1qFdbDynamicCount)
{
    tVlanFidEntry      *pFidEntry = NULL;
    UINT4               u4FidIndex = VLAN_INIT_VAL;
#ifdef NPAPI_WANTED
    INT4                i4Result;
#endif

#ifdef NPAPI_WANTED
    UNUSED_PARAM (pFidEntry);
    UNUSED_PARAM (u4FidIndex);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
#ifdef SW_LEARNING
    i4Result = VlanGetFdbDynamicCount (u4Dot1qFdbId,
                                       pu4RetValDot1qFdbDynamicCount);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#else
    i4Result =
        VlanHwGetFdbCount (VLAN_CURR_CONTEXT_ID (), u4Dot1qFdbId,
                           pu4RetValDot1qFdbDynamicCount);

    if (i4Result == VLAN_SUCCESS)
    {

        return SNMP_SUCCESS;
    }
#endif /* SW_LEARNING */

#else
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4Dot1qFdbId);

    if (VLAN_INVALID_FID_INDEX != u4FidIndex)
    {

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        *pu4RetValDot1qFdbDynamicCount = pFidEntry->u4DynamicUnicastCount;
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpFdbPort
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress

                The Object 
                retValDot1qTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpFdbPort (UINT4 u4Dot1qFdbId,
                      tMacAddr Dot1qTpFdbAddress, INT4 *pi4RetValDot1qTpFdbPort)
{
    tVlanTempFdbInfo    VlanTempFdbInfo;
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    UINT4               u4Context = 0;
    UINT2               u2Port = 0;
#endif
#endif

    if (VlanGetFdbInfo (u4Dot1qFdbId, Dot1qTpFdbAddress,
                        &VlanTempFdbInfo) == VLAN_SUCCESS)
    {
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
        /* In case of this scenorio , i4LocalPortNum is nothing but the 
         * ifindex.This is the correction for show mac-address-table printing*/
        if (VlanTempFdbInfo.u1Status == VLAN_FDB_MGMT)
        {
            VlanVcmGetContextInfoFromIfIndex ((UINT4) VlanTempFdbInfo.u2Port,
                                              &u4Context, &u2Port);
            VlanTempFdbInfo.u2Port = u2Port;
            UNUSED_PARAM (u4Context);
        }
#endif
#endif
        *pi4RetValDot1qTpFdbPort = VlanTempFdbInfo.u2Port;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpFdbStatus
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress

                The Object 
                retValDot1qTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpFdbStatus (UINT4 u4Dot1qFdbId,
                        tMacAddr Dot1qTpFdbAddress,
                        INT4 *pi4RetValDot1qTpFdbStatus)
{
    tVlanTempFdbInfo    VlanTempFdbInfo;

    if (VlanGetFdbInfo (u4Dot1qFdbId, Dot1qTpFdbAddress,
                        &VlanTempFdbInfo) == VLAN_SUCCESS)
    {
        *pi4RetValDot1qTpFdbStatus = VlanTempFdbInfo.u1Status;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpGroupEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex
                GroupAddr

                The Object 
                retValDot1qTpGroupEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpGroupEgressPorts (UINT4 u4VlanIndex,
                               tMacAddr GroupAddr,
                               tSNMP_OCTET_STRING_TYPE * pEgressPorts)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    UINT1               u1Result;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        pGroupEntry = VlanGetGroupEntry (GroupAddr, pCurrEntry);

        if (pGroupEntry != NULL)
        {

            MEMSET (pEgressPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

            VLAN_ADD_PORT_LIST (pEgressPorts->pu1_OctetList,
                                pGroupEntry->LearntPorts);

            pEgressPorts->i4_Length = VLAN_PORT_LIST_SIZE;

            pStMcastEntry = pCurrEntry->pStMcastTable;

            while (pStMcastEntry != NULL)
            {

                u1Result = VlanCmpMacAddr (GroupAddr, pStMcastEntry->MacAddr);

                if (u1Result == VLAN_EQUAL)
                {

                    VLAN_ADD_PORT_LIST (pEgressPorts->pu1_OctetList,
                                        pStMcastEntry->EgressPorts);
                }

                pStMcastEntry = pStMcastEntry->pNextNode;
            }

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpGroupLearnt
 Input       :  The Indices
                Dot1qVlanIndex
                GroupAddr

                The Object 
                retValDot1qTpGroupLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpGroupLearnt (UINT4 u4VlanIndex,
                          tMacAddr GroupAddr,
                          tSNMP_OCTET_STRING_TYPE * pLearntPorts)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanGroupEntry    *pGroupEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        pGroupEntry = VlanGetGroupEntry (GroupAddr, pCurrEntry);

        if (pGroupEntry != NULL)
        {

            MEMSET (pLearntPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

            VLAN_ADD_PORT_LIST (pLearntPorts->pu1_OctetList,
                                pGroupEntry->LearntPorts);

            pLearntPorts->i4_Length = VLAN_PORT_LIST_SIZE;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qForwardAllPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qForwardAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qForwardAllPorts (UINT4 u4VlanIndex,
                            tSNMP_OCTET_STRING_TYPE * pFwdAllPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pu1LocalPortList = NULL;
    tVlanCurrEntry     *pCurrEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        MEMSET (pFwdAllPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        VLAN_ADD_PORT_LIST (pFwdAllPorts->pu1_OctetList,
                            pCurrEntry->AllGrps.Ports);

        pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pu1LocalPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Memory allocation failed for tLocalPortList\n");
            return SNMP_FAILURE;
        }
        MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
        VLAN_GET_CURR_EGRESS_PORTS (pCurrEntry, pu1LocalPortList);

        VLAN_AND_PORT_LIST (pFwdAllPorts->pu1_OctetList, pu1LocalPortList);
        UtilPlstReleaseLocalPortList (pu1LocalPortList);

        pFwdAllPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pFwdAllPorts);
#endif /* EXTENDED_FILTERING_CHANGE */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qForwardAllStaticPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qForwardAllStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qForwardAllStaticPorts (UINT4 u4VlanIndex,
                                  tSNMP_OCTET_STRING_TYPE * pFwdAllStPorts)
{
    if (VlanGetVlanForwardAllStaticPorts (u4VlanIndex, pFwdAllStPorts)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qForwardAllForbiddenPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qForwardAllForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qForwardAllForbiddenPorts (UINT4 u4VlanIndex,
                                     tSNMP_OCTET_STRING_TYPE * pForbiddenPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    tVlanCurrEntry     *pCurrEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        MEMSET (pForbiddenPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        VLAN_ADD_PORT_LIST (pForbiddenPorts->pu1_OctetList,
                            pCurrEntry->AllGrps.ForbiddenPorts);

        pForbiddenPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pForbiddenPorts);
#endif /* EXTENDED_FILTERING_CHANGE */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qForwardUnregisteredPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qForwardUnregisteredPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qForwardUnregisteredPorts (UINT4 u4VlanIndex,
                                     tSNMP_OCTET_STRING_TYPE * pUnRegPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pu1LocalPortList = NULL;
    tVlanCurrEntry     *pCurrEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {
        MEMSET (pUnRegPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        VLAN_ADD_PORT_LIST (pUnRegPorts->pu1_OctetList,
                            pCurrEntry->UnRegGrps.Ports);

        pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pu1LocalPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Memory allocation failed for tLocalPortList\n");
            return SNMP_FAILURE;
        }
        MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
        VLAN_GET_CURR_EGRESS_PORTS (pCurrEntry, pu1LocalPortList);

        VLAN_AND_PORT_LIST (pUnRegPorts->pu1_OctetList, pu1LocalPortList);
        UtilPlstReleaseLocalPortList (pu1LocalPortList);

        pUnRegPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pUnRegPorts);
#endif /* EXTENDED_FILTERING_CHANGE */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qForwardUnregisteredStaticPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qForwardUnregisteredStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qForwardUnregisteredStaticPorts (UINT4 u4VlanIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pUnRegStPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pu1LocalPortList = NULL;
    tVlanCurrEntry     *pCurrEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {
        MEMSET (pUnRegStPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        VLAN_ADD_PORT_LIST (pUnRegStPorts->pu1_OctetList,
                            pCurrEntry->UnRegGrps.StaticPorts);

        pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pu1LocalPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Memory allocation failed for tLocalPortList\n");
            return SNMP_FAILURE;
        }
        MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
        VLAN_GET_CURR_EGRESS_PORTS (pCurrEntry, pu1LocalPortList);

        VLAN_AND_PORT_LIST (pUnRegStPorts->pu1_OctetList, pu1LocalPortList);
        UtilPlstReleaseLocalPortList (pu1LocalPortList);

        pUnRegStPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pUnRegStPorts);
#endif /* EXTENDED_FILTERING_CHANGE */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qForwardUnregisteredForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qForwardUnregisteredForbiddenPorts (UINT4 u4VlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pForbiddenPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    tVlanCurrEntry     *pCurrEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        MEMSET (pForbiddenPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        VLAN_ADD_PORT_LIST (pForbiddenPorts->pu1_OctetList,
                            pCurrEntry->UnRegGrps.ForbiddenPorts);

        pForbiddenPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pForbiddenPorts);
#endif /* EXTENDED_FILTERING_CHANGE */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qStaticUnicastAllowedToGoTo
 Input       :  The Indices
                Dot1qFdbId
                StUcastAddr
                Dot1qStaticUnicastReceivePort

                The Object 
                retValDot1qStaticUnicastAllowedToGoTo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qStaticUnicastAllowedToGoTo (UINT4 u4Dot1qFdbId,
                                       tMacAddr StUcastAddr,
                                       INT4 i4RcvPort,
                                       tSNMP_OCTET_STRING_TYPE * pAllowedToGoTo)
{
    if (VlanGetStaticUcastAllowedToGoTo (u4Dot1qFdbId, StUcastAddr,
                                         i4RcvPort,
                                         pAllowedToGoTo) == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qStaticUnicastStatus
 Input       :  The Indices
                Dot1qFdbId
                StUcastAddr
                Dot1qStaticUnicastReceivePort

                The Object 
                retValDot1qStaticUnicastStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qStaticUnicastStatus (UINT4 u4Dot1qFdbId,
                                tMacAddr StUcastAddr,
                                INT4 i4RcvPort, INT4 *pi4Status)
{
    if (VlanGetStaticUnicastStatus (u4Dot1qFdbId, StUcastAddr, i4RcvPort,
                                    pi4Status) != VLAN_FAILURE)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex
                StMcastAddr
                Dot1qStaticMulticastReceivePort

                The Object 
                retValDot1qStaticMulticastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qStaticMulticastStaticEgressPorts (UINT4 u4VlanIndex,
                                             tMacAddr StMcastAddr,
                                             INT4 i4RcvPort,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pEgressPorts)
{
    if (VlanGetStaticMcastEgressPorts (u4VlanIndex, StMcastAddr,
                                       i4RcvPort, pEgressPorts) == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex
                StMcastAddr
                Dot1qStaticMulticastReceivePort

                The Object 
                retValDot1qStaticMulticastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qStaticMulticastForbiddenEgressPorts (UINT4 u4VlanIndex,
                                                tMacAddr StMcastAddr,
                                                INT4 i4RcvPort,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pForbiddenPorts)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanStMcastEntry  *pStMcastEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort (StMcastAddr,
                                                             (UINT2) i4RcvPort,
                                                             pCurrEntry);

        if (pStMcastEntry != NULL)
        {

            MEMSET (pForbiddenPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

            VLAN_ADD_PORT_LIST (pForbiddenPorts->pu1_OctetList,
                                pStMcastEntry->ForbiddenPorts);

            pForbiddenPorts->i4_Length = VLAN_PORT_LIST_SIZE;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qStaticMulticastStatus
 Input       :  The Indices
                Dot1qVlanIndex
                StMcastAddr
                Dot1qStaticMulticastReceivePort

                The Object 
                retValDot1qStaticMulticastStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qStaticMulticastStatus (UINT4 u4VlanIndex,
                                  tMacAddr StMcastAddr,
                                  INT4 i4RcvPort, INT4 *pi4Status)
{
    if (VlanGetStaticMulticastStatus (u4VlanIndex, StMcastAddr,
                                      i4RcvPort, pi4Status) == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanNumDeletes
 Input       :  The Indices

                The Object 
                retValDot1qVlanNumDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanNumDeletes (UINT4 *pu4RetValDot1qVlanNumDeletes)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4RetValDot1qVlanNumDeletes = VLAN_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4RetValDot1qVlanNumDeletes = VLAN_INIT_VAL;

        return SNMP_SUCCESS;
    }

    *pu4RetValDot1qVlanNumDeletes = gpVlanContextInfo->VlanInfo.u4NumDelVlans;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanFdbId
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanFdbId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanFdbId (UINT4 u4TimeMark, UINT4 u4VlanIndex, UINT4 *pu4FdbId)
{

    tVlanCurrEntry     *pCurrEntry;
    tVlanFidEntry      *pFidEntry;

    UNUSED_PARAM (u4TimeMark);
    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {
        pFidEntry = VLAN_GET_FID_ENTRY (pCurrEntry->u4FidIndex);

        *pu4FdbId = pFidEntry->u4Fid;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanCurrentEgressPorts
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanCurrentEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanCurrentEgressPorts (UINT4 u4TimeMark,
                                   UINT4 u4VlanIndex,
                                   tSNMP_OCTET_STRING_TYPE * pEgressPorts)
{
    tVlanCurrEntry     *pCurrEntry;

    UNUSED_PARAM (u4TimeMark);

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        MEMSET (pEgressPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pCurrEntry,
                                            pEgressPorts->pu1_OctetList);

        pEgressPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanCurrentUntaggedPorts
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanCurrentUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanCurrentUntaggedPorts (UINT4 u4TimeMark,
                                     UINT4 u4VlanIndex,
                                     tSNMP_OCTET_STRING_TYPE * pUnTagPorts)
{
    tVlanCurrEntry     *pCurrEntry;

    UNUSED_PARAM (u4TimeMark);

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        MEMSET (pUnTagPorts->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);

        if (pCurrEntry->pStVlanEntry != NULL)
        {

            VLAN_ADD_UNTAGGED_PORTS_TO_LIST (pCurrEntry->pStVlanEntry,
                                             pUnTagPorts->pu1_OctetList);
        }

        pUnTagPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanStatus
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanStatus (UINT4 u4TimeMark, UINT4 u4VlanIndex, INT4 *pi4Status)
{
    tVlanCurrEntry     *pCurrEntry;

    UNUSED_PARAM (u4TimeMark);

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {

        if (pCurrEntry->pStVlanEntry != NULL)
        {

            *pi4Status = VLAN_CURR_ENTRY_PERMANENT;
        }
        else
        {

            *pi4Status = VLAN_CURR_ENTRY_DYNAMIC;

        }

        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanCreationTime
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanCreationTime (UINT4 u4TimeMark,
                             UINT4 u4VlanIndex,
                             UINT4 *pu4RetValDot1qVlanCreationTime)
{
    tVlanCurrEntry     *pCurrEntry;

    UNUSED_PARAM (u4TimeMark);

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

    if (pCurrEntry != NULL)
    {
        *pu4RetValDot1qVlanCreationTime = pCurrEntry->u4CreationTimeStamp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanStaticName
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanStaticName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanStaticName (UINT4 u4VlanIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValDot1qVlanStaticName)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    if (pStVlanEntry != NULL)
    {

        MEMCPY (pRetValDot1qVlanStaticName->pu1_OctetList,
                pStVlanEntry->au1VlanName, pStVlanEntry->u1VlanNameLen);

        pRetValDot1qVlanStaticName->i4_Length = pStVlanEntry->u1VlanNameLen;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanStaticEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanStaticEgressPorts (UINT4 u4VlanIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDot1qVlanStaticEgressPorts)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    if (pStVlanEntry != NULL)
    {

        MEMSET (pRetValDot1qVlanStaticEgressPorts->pu1_OctetList, 0,
                VLAN_PORT_LIST_SIZE);
        VLAN_GET_EGRESS_PORTS (pStVlanEntry,
                               pRetValDot1qVlanStaticEgressPorts->
                               pu1_OctetList);

        pRetValDot1qVlanStaticEgressPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanForbiddenEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanForbiddenEgressPorts (UINT4 u4VlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot1qVlanForbiddenEgressPorts)
{
    tStaticVlanEntry   *pStVlanEntry;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    if (pStVlanEntry != NULL)
    {

        MEMSET (pRetValDot1qVlanForbiddenEgressPorts->pu1_OctetList, 0,
                VLAN_PORT_LIST_SIZE);

        VLAN_ADD_FORBIDDEN_PORTS_TO_LIST (pStVlanEntry,
                                          pRetValDot1qVlanForbiddenEgressPorts->
                                          pu1_OctetList);

        pRetValDot1qVlanForbiddenEgressPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanStaticUntaggedPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanStaticUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanStaticUntaggedPorts (UINT4 u4VlanIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValDot1qVlanStaticUntaggedPorts)
{
    tStaticVlanEntry   *pStVlanEntry;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    if (pStVlanEntry != NULL)
    {

        MEMSET (pRetValDot1qVlanStaticUntaggedPorts->pu1_OctetList, 0,
                VLAN_PORT_LIST_SIZE);

        VLAN_ADD_UNTAGGED_PORTS_TO_LIST (pStVlanEntry,
                                         pRetValDot1qVlanStaticUntaggedPorts->
                                         pu1_OctetList);

        pRetValDot1qVlanStaticUntaggedPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qVlanStaticRowStatus
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qVlanStaticRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qVlanStaticRowStatus (UINT4 u4VlanIndex,
                                INT4 *pi4RetValDot1qVlanStaticRowStatus)
{
    tStaticVlanEntry   *pStVlanEntry;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    if (pStVlanEntry != NULL)
    {

        *pi4RetValDot1qVlanStaticRowStatus = (INT4) pStVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1qNextFreeLocalVlanIndex
 Input       :  The Indices

                The Object 
                retValDot1qNextFreeLocalVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qNextFreeLocalVlanIndex (INT4 *pi4RetValDot1qNextFreeLocalVlanIndex)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qNextFreeLocalVlanIndex = VLAN_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qNextFreeLocalVlanIndex = VLAN_INIT_VAL;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qNextFreeLocalVlanIndex = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPvid
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1qPvid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPvid (INT4 i4Dot1dBasePort, UINT4 *pu4RetValDot1qPvid)
{
    tVlanPortEntry     *pVlanPortEntry;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValDot1qPvid = pVlanPortEntry->Pvid;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortAcceptableFrameTypes
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1qPortAcceptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortAcceptableFrameTypes (INT4 i4Dot1dBasePort,
                                     INT4
                                     *pi4RetValDot1qPortAcceptableFrameTypes)
{
    tVlanPortEntry     *pVlanPortEntry;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValDot1qPortAcceptableFrameTypes =
            pVlanPortEntry->u1AccpFrmTypes;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortIngressFiltering
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1qPortIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortIngressFiltering (INT4 i4Dot1dBasePort,
                                 INT4 *pi4RetValDot1qPortIngressFiltering)
{
    tVlanPortEntry     *pVlanPortEntry;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValDot1qPortIngressFiltering = pVlanPortEntry->u1IngFiltering;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortInFrames
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortInFrames (INT4 i4Dot1dBasePort,
                               UINT4 u4VlanIndex,
                               UINT4 *pu4RetValDot1qTpVlanPortInFrames)
{
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    i4Result =
        VlanGetStats (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                      VLAN_STAT_VLAN_PORT_IN_FRAMES,
                      pu4RetValDot1qTpVlanPortInFrames);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortOutFrames
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortOutFrames (INT4 i4Dot1dBasePort,
                                UINT4 u4VlanIndex,
                                UINT4 *pu4RetValDot1qTpVlanPortOutFrames)
{
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4Result = VlanGetStats (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                             VLAN_STAT_VLAN_PORT_OUT_FRAMES,
                             pu4RetValDot1qTpVlanPortOutFrames);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortInDiscards
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortInDiscards (INT4 i4Dot1dBasePort,
                                 UINT4 u4VlanIndex,
                                 UINT4 *pu4RetValDot1qTpVlanPortInDiscards)
{
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4Result = VlanGetStats (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                             VLAN_STAT_VLAN_PORT_IN_DISCARDS,
                             pu4RetValDot1qTpVlanPortInDiscards);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortInOverflowFrames
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortInOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortInOverflowFrames (INT4 i4Dot1dBasePort,
                                       UINT4 u4VlanIndex,
                                       UINT4
                                       *pu4RetValDot1qTpVlanPortInOverflowFrames)
{
#ifdef SYS_HC_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if ((pPortEntry == NULL) || (pPortEntry->u1IsHCPort == VLAN_FALSE))
    {
        *pu4RetValDot1qTpVlanPortInOverflowFrames = 0;
        return SNMP_SUCCESS;
    }

    i4Result = VlanGetStats (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                             VLAN_STAT_VLAN_PORT_IN_OVERFLOW,
                             pu4RetValDot1qTpVlanPortInOverflowFrames);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (u4VlanIndex);

    /* There is no support for this object. Returning zero. */
    *pu4RetValDot1qTpVlanPortInOverflowFrames = 0;
    return SNMP_SUCCESS;
#endif /* SYS_HC_WANTED */
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortOutOverflowFrames
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortOutOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortOutOverflowFrames (INT4 i4Dot1dBasePort,
                                        UINT4 u4VlanIndex,
                                        UINT4
                                        *pu4RetValDot1qTpVlanPortOutOverflowFrames)
{
#ifdef SYS_HC_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if ((pPortEntry == NULL) || (pPortEntry->u1IsHCPort == VLAN_FALSE))
    {
        *pu4RetValDot1qTpVlanPortOutOverflowFrames = 0;
        return SNMP_SUCCESS;
    }

    i4Result = VlanGetStats (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                             VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,
                             pu4RetValDot1qTpVlanPortOutOverflowFrames);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (u4VlanIndex);
    /* There is no support for this object. Returning zero. */
    *pu4RetValDot1qTpVlanPortOutOverflowFrames = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortInOverflowDiscards
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortInOverflowDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortInOverflowDiscards (INT4 i4Dot1dBasePort,
                                         UINT4 u4VlanIndex,
                                         UINT4
                                         *pu4RetValDot1qTpVlanPortInOverflowDiscards)
{
#ifdef SYS_HC_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if ((pPortEntry == NULL) || (pPortEntry->u1IsHCPort == VLAN_FALSE))
    {
        *pu4RetValDot1qTpVlanPortInOverflowDiscards = 0;
        return SNMP_SUCCESS;
    }

    i4Result = VlanGetStats (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                             VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW,
                             pu4RetValDot1qTpVlanPortInOverflowDiscards);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (u4VlanIndex);
    /* There is no support for this object. Returning zero. */
    *pu4RetValDot1qTpVlanPortInOverflowDiscards = 0;
    return SNMP_SUCCESS;
#endif /* SYS_HC_WANTED */
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortHCInFrames
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortHCInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortHCInFrames (INT4 i4Dot1dBasePort,
                                 UINT4 u4VlanIndex,
                                 tSNMP_COUNTER64_TYPE * pu8HCInFrames)
{
#ifdef SYS_HC_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if ((pPortEntry == NULL) || (pPortEntry->u1IsHCPort == VLAN_FALSE))
    {
        pu8HCInFrames->lsn = 0;
        pu8HCInFrames->msn = 0;
        return SNMP_SUCCESS;
    }

    i4Result = VlanGetStats64 (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                               VLAN_STAT_VLAN_PORT_IN_FRAMES, pu8HCInFrames);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#else /* SYS_HC_WANTED */
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pu8HCInFrames);
#endif /* SYS_HC_WANTED */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortHCOutFrames
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortHCOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortHCOutFrames (INT4 i4Dot1dBasePort,
                                  UINT4 u4VlanIndex,
                                  tSNMP_COUNTER64_TYPE * pu8HCOutFrames)
{
#ifdef SYS_HC_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if ((pPortEntry == NULL) || (pPortEntry->u1IsHCPort == VLAN_FALSE))
    {
        pu8HCOutFrames->lsn = 0;
        pu8HCOutFrames->msn = 0;
        return SNMP_SUCCESS;
    }

    i4Result = VlanGetStats64 (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                               VLAN_STAT_VLAN_PORT_OUT_FRAMES, pu8HCOutFrames);
    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#else /* SYS_HC_WANTED */
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pu8HCOutFrames);
#endif /* SYS_HC_WANTED */
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetDot1qTpVlanPortHCInDiscards
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex

                The Object 
                retValDot1qTpVlanPortHCInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpVlanPortHCInDiscards (INT4 i4Dot1dBasePort,
                                   UINT4 u4VlanIndex,
                                   tSNMP_COUNTER64_TYPE * pu8HCInDiscards)
{
#ifdef SYS_HC_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Result;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if ((pPortEntry == NULL) || (pPortEntry->u1IsHCPort == VLAN_FALSE))
    {
        pu8HCInDiscards->lsn = 0;
        pu8HCInDiscards->msn = 0;
        return SNMP_SUCCESS;
    }

    i4Result = VlanGetStats64 (i4Dot1dBasePort, (tVlanId) u4VlanIndex,
                               VLAN_STAT_VLAN_PORT_IN_DISCARDS,
                               pu8HCInDiscards);
    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else /* SYS_HC_WANTED */
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pu8HCInDiscards);
    UNUSED_PARAM (u4VlanIndex);
#endif /* SYS_HC_WANTED */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qConstraintType
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet

                The Object 
                retValDot1qConstraintType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qConstraintType (UINT4 u4Dot1qConstraintVlan,
                           INT4 i4Dot1qConstraintSet,
                           INT4 *pi4RetValDot1qConstraintType)
{
    UNUSED_PARAM (u4Dot1qConstraintVlan);
    UNUSED_PARAM (i4Dot1qConstraintSet);

    *pi4RetValDot1qConstraintType = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qConstraintStatus
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet

                The Object 
                retValDot1qConstraintStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qConstraintStatus (UINT4 u4Dot1qConstraintVlan,
                             INT4 i4Dot1qConstraintSet,
                             INT4 *pi4RetValDot1qConstraintStatus)
{
    UNUSED_PARAM (u4Dot1qConstraintVlan);
    UNUSED_PARAM (i4Dot1qConstraintSet);

    *pi4RetValDot1qConstraintStatus = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qConstraintSetDefault
 Input       :  The Indices

                The Object 
                retValDot1qConstraintSetDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qConstraintSetDefault (INT4 *pi4RetValDot1qConstraintSetDefault)
{
    *pi4RetValDot1qConstraintSetDefault = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qConstraintTypeDefault
 Input       :  The Indices

                The Object 
                retValDot1qConstraintTypeDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qConstraintTypeDefault (INT4 *pi4RetValDot1qConstraintTypeDefault)
{
    *pi4RetValDot1qConstraintTypeDefault = 1;
    return SNMP_SUCCESS;
}

/************************ P-MIB Get routines *******************************/

/****************************************************************************
 Function    :  nmhGetDot1dDeviceCapabilities
 Input       :  The Indices

                The Object 
                retValDot1dDeviceCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dDeviceCapabilities (tSNMP_OCTET_STRING_TYPE *
                               pRetValDot1dDeviceCapabilities)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        pRetValDot1dDeviceCapabilities->i4_Length =
            VLAN_CAPABILITIES_MASK_LENGTH;
        pRetValDot1dDeviceCapabilities->pu1_OctetList[0] =
            (UINT1) VLAN_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pRetValDot1dDeviceCapabilities->i4_Length = VLAN_CAPABILITIES_MASK_LENGTH;
    pRetValDot1dDeviceCapabilities->pu1_OctetList[0] =
        (UINT1) VLAN_CAPABILITIES_MASK;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTrafficClassesEnabled
 Input       :  The Indices

                The Object 
                retValDot1dTrafficClassesEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTrafficClassesEnabled (INT4 *pi4RetValDot1dTrafficClassesEnabled)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1dTrafficClassesEnabled = VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1dTrafficClassesEnabled = VLAN_SNMP_FALSE;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1dTrafficClassesEnabled =
        gpVlanContextInfo->VlanInfo.u1TrfClassEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortCapabilities
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortCapabilities (INT4 i4Dot1dBasePort,
                             tSNMP_OCTET_STRING_TYPE
                             * pRetValDot1dPortCapabilities)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValDot1dPortCapabilities->i4_Length = VLAN_CAPABILITIES_MASK_LENGTH;
    pRetValDot1dPortCapabilities->pu1_OctetList[0] =
        (UINT1) VLAN_PORT_CAPABILITIES_MASK;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortDefaultUserPriority
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortDefaultUserPriority (INT4 i4Dot1dBasePort,
                                    INT4 *pi4RetValDot1dPortDefaultUserPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dPortDefaultUserPriority =
        (INT4) pPortEntry->u1DefUserPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortNumTrafficClasses
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortNumTrafficClasses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortNumTrafficClasses (INT4 i4Dot1dBasePort,
                                  INT4 *pi4RetValDot1dPortNumTrafficClasses)
{

    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dPortNumTrafficClasses = (INT4) pPortEntry->u1NumTrfClass;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dRegenUserPriority
 Input       :  The Indices
                Dot1dBasePort
                Dot1dUserPriority

                The Object 
                retValDot1dRegenUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dRegenUserPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dRegenUserPriority (INT4 i4Dot1dBasePort, INT4 i4Dot1dUserPriority,
                              INT4 *pi4RetValDot1dRegenUserPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dRegenUserPriority
        = pPortEntry->au1PriorityRegen[i4Dot1dUserPriority];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTrafficClass
 Input       :  The Indices
                Dot1dBasePort
                Dot1dTrafficClassPriority

                The Object 
                retValDot1dTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTrafficClass (INT4 i4Dot1dBasePort, INT4 i4Dot1dTrafficClassPriority,
                         INT4 *pi4RetValDot1dTrafficClass)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1dTrafficClassPriority < 0) ||
        (i4Dot1dTrafficClassPriority > VLAN_HIGHEST_PRIORITY))
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dTrafficClass =
        (INT4) pPortEntry->au1TrfClassMap[i4Dot1dTrafficClassPriority];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortOutboundAccessPriority
 Input       :  The Indices
                Dot1dBasePort
                Dot1dPortOutboundRegenPriority

                The Object 
                retValDot1dPortOutboundAccessPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortOutboundAccessPriority (INT4 i4Dot1dBasePort,
                                       INT4 i4Dot1dPortOutboundRegenPriority,
                                       INT4
                                       *pi4RetValDot1dPortOutboundAccessPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1dPortOutboundRegenPriority < 0) ||
        (i4Dot1dPortOutboundRegenPriority > VLAN_HIGHEST_PRIORITY))
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dPortOutboundAccessPriority = 0;
    /* only ethernet is supported. */

    return SNMP_SUCCESS;
}

/************************** Proprietary MIB Get Routines ********************/

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanStatus
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1qFutureVlanStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1qFutureVlanStatus (INT4 *pi4RetValDot1qFutureVlanStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanStatus = VLAN_DISABLED;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qFutureVlanStatus = VLAN_DISABLED;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanStatus = (INT4) VLAN_MODULE_ADMIN_STATUS ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanOperStatus
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanOperStatus (INT4 *pi4RetValDot1qFutureVlanOperStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanOperStatus = VLAN_DISABLED;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qFutureVlanOperStatus = VLAN_DISABLED;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanOperStatus = (INT4) VLAN_MODULE_STATUS ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1qFutureVlanPortType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1qFutureVlanPortType (INT4 i4Dot1qFutureVlanPort,
                               INT4 *pi4RetValDot1qFutureVlanPortType)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureVlanPortType = (INT4) pVlanPortEntry->u1PortType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortMacMapVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                retValDot1qFutureVlanPortMacMapVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortMacMapVid (INT4 i4Dot1qFutureVlanPort,
                                    tMacAddr Dot1qFutureVlanPortMacMapAddr,
                                    INT4 *pi4RetValDot1qFutureVlanPortMacMapVid)
{
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           (UINT4) i4Dot1qFutureVlanPort);

    if (pVlanMacMapEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanPortMacMapVid =
            (INT4) pVlanMacMapEntry->VlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanBaseBridgeMode
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanBaseBridgeMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanBaseBridgeMode (INT4
                                     *pi4RetValDot1qFutureVlanBaseBridgeMode)
{
    *pi4RetValDot1qFutureVlanBaseBridgeMode = VLAN_BASE_BRIDGE_MODE ();
    return SNMP_SUCCESS;
}

/****************************************************************************       Function    :  nmhGetDot1qFutureVlanSubnetBasedOnAllPorts
 Input       :  The Indices

                The Object
                retValDot1qFutureVlanSubnetBasedOnAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanSubnetBasedOnAllPorts (INT4
                                            *pi4RetValDot1qFutureVlanSubnetBasedOnAllPorts)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanSubnetBasedOnAllPorts =
            (UINT1) VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qFutureVlanSubnetBasedOnAllPorts =
            (UINT1) VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanSubnetBasedOnAllPorts = (UINT1) VLAN_SUBNET_BASED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortMacMapName
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                retValDot1qFutureVlanPortMacMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortMacMapName (INT4 i4Dot1qFutureVlanPort,
                                     tMacAddr Dot1qFutureVlanPortMacMapAddr,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot1qFutureVlanPortMacMapName)
{
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           (UINT4) i4Dot1qFutureVlanPort);

    if (pVlanMacMapEntry != NULL)
    {
        pRetValDot1qFutureVlanPortMacMapName->i4_Length =
            (INT4) pVlanMacMapEntry->u1VlanNameLen;

        MEMCPY (pRetValDot1qFutureVlanPortMacMapName->pu1_OctetList,
                pVlanMacMapEntry->au1VlanName, pVlanMacMapEntry->u1VlanNameLen);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortMacMapMcastBcastOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                retValDot1qFutureVlanPortMacMapMcastBcastOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortMacMapMcastBcastOption (INT4 i4Dot1qFutureVlanPort,
                                                 tMacAddr
                                                 Dot1qFutureVlanPortMacMapAddr,
                                                 INT4
                                                 *pi4RetValDot1qFutureVlanPortMacMapMcastBcastOption)
{
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           (UINT4) i4Dot1qFutureVlanPort);

    if (pVlanMacMapEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanPortMacMapMcastBcastOption =
            (INT4) pVlanMacMapEntry->u1McastOption;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortMacMapRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                retValDot1qFutureVlanPortMacMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortMacMapRowStatus (INT4 i4Dot1qFutureVlanPort,
                                          tMacAddr
                                          Dot1qFutureVlanPortMacMapAddr,
                                          INT4
                                          *pi4RetValDot1qFutureVlanPortMacMapRowStatus)
{
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           (UINT4) i4Dot1qFutureVlanPort);

    if (pVlanMacMapEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanPortMacMapRowStatus =
            (INT4) pVlanMacMapEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanShutdownStatus
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanShutdownStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1qFutureVlanShutdownStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1qFutureVlanShutdownStatus (INT4
                                     *pi4RetValDot1qFutureVlanShutdownStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanShutdownStatus = VLAN_SNMP_TRUE;
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanShutdownStatus = (INT4) VLAN_SYSTEM_CONTROL ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanDebug
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanDebug (INT4 *pi4RetValDot1qFutureVlanDebug)
{
#ifdef TRACE_WANTED
    *pi4RetValDot1qFutureVlanDebug = (INT4) gpVlanContextInfo->u4VlanDbg;
    return SNMP_SUCCESS;
#else
    *pi4RetValDot1qFutureVlanDebug = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanMacBasedOnAllPorts
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanMacBasedOnAllPorts (INT4
                                         *pi4RetValDot1qFutureVlanMacBasedOnAllPorts)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanMacBasedOnAllPorts = (UINT1) VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qFutureVlanMacBasedOnAllPorts = (UINT1) VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanMacBasedOnAllPorts = (UINT1) VLAN_MAC_BASED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortProtoBasedOnAllPorts
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortProtoBasedOnAllPorts (INT4
                                               *pi4RetValDot1qFutureVlanPortProtoBasedOnAllPorts)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValDot1qFutureVlanPortProtoBasedOnAllPorts =
            (UINT1) VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pi4RetValDot1qFutureVlanPortProtoBasedOnAllPorts =
            (UINT1) VLAN_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanPortProtoBasedOnAllPorts =
        (UINT1) VLAN_PORT_PROTO_BASED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortMacBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanPortMacBasedClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortMacBasedClassification (INT4 i4Dot1qFutureVlanPort,
                                                 INT4
                                                 *pi4RetValDot1qFutureVlanPortMacBasedClassification)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureVlanPortMacBasedClassification =
        (INT4) pVlanPortEntry->u1MacBasedClassification;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortPortProtoBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanPortPortProtoBasedClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortPortProtoBasedClassification (INT4
                                                       i4Dot1qFutureVlanPort,
                                                       INT4
                                                       *pi4RetValDot1qFutureVlanPortPortProtoBasedClassification)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureVlanPortPortProtoBasedClassification =
        (INT4) pVlanPortEntry->u1PortProtoBasedClassification;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1vProtocolGroupId
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue

                The Object 
                retValDot1vProtocolGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1vProtocolGroupId (INT4 i4Dot1vProtocolTemplateFrameType,
                            tSNMP_OCTET_STRING_TYPE *
                            pDot1vProtocolTemplateProtocolValue,
                            INT4 *pi4RetValDot1vProtocolGroupId)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];

    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);

    if ((pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_2BYTE_PROTO_TEMP_SIZE) &&
        (pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_5BYTE_PROTO_TEMP_SIZE))
    {
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;
    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry != NULL)
    {
        *pi4RetValDot1vProtocolGroupId = (INT4) pVlanProtGrpEntry->u4ProtGrpId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1vProtocolGroupRowStatus
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue

                The Object 
                retValDot1vProtocolGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1vProtocolGroupRowStatus (INT4 i4Dot1vProtocolTemplateFrameType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pDot1vProtocolTemplateProtocolValue,
                                   INT4 *pi4RetValDot1vProtocolGroupRowStatus)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];

    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);

    if ((pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_2BYTE_PROTO_TEMP_SIZE) &&
        (pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_5BYTE_PROTO_TEMP_SIZE))
    {
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;
    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry != NULL)
    {
        *pi4RetValDot1vProtocolGroupRowStatus =
            (INT4) pVlanProtGrpEntry->u1RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1vProtocolPortGroupVid
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId

                The Object 
                retValDot1vProtocolPortGroupVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1vProtocolPortGroupVid (INT4 i4Dot1dBasePort,
                                 INT4 i4Dot1vProtocolPortGroupId,
                                 INT4 *pi4RetValDot1vProtocolPortGroupVid)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT2               u2BasePort = 0;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u2BasePort = (UINT2) i4Dot1dBasePort;

    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2BasePort,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry != NULL)
    {
        *pi4RetValDot1vProtocolPortGroupVid =
            (INT4) pVlanPortVidSetEntry->VlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1vProtocolPortRowStatus
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId

                The Object 
                retValDot1vProtocolPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1vProtocolPortRowStatus (INT4 i4Dot1dBasePort,
                                  INT4 i4Dot1vProtocolPortGroupId,
                                  INT4 *pi4RetValDot1vProtocolPortRowStatus)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT2               u2BasePort = 0;

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u2BasePort = (UINT2) i4Dot1dBasePort;

    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2BasePort,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry != NULL)
    {
        *pi4RetValDot1vProtocolPortRowStatus
            = pVlanPortVidSetEntry->u1RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanBridgeMode
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanBridgeMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanBridgeMode (INT4 *pi4RetValDot1qFutureVlanBridgeMode)
{
    /* VLAN module is shutdown...return default bridge mode */
    *pi4RetValDot1qFutureVlanBridgeMode = (INT4) VLAN_INVALID_BRIDGE_MODE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanTunnelBpduPri
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanTunnelBpduPri
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelBpduPri (INT4 *pi4RetValDot1qFutureVlanTunnelBpduPri)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureVlanTunnelBpduPri =
        (INT4) VLAN_DEF_TUNNEL_BPDU_PRIORITY;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanTunnelStatus
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanTunnelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelStatus (INT4 i4Dot1qFutureVlanPort,
                                   INT4 *pi4RetValDot1qFutureVlanTunnelStatus)
{

    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    *pi4RetValDot1qFutureVlanTunnelStatus = VLAN_DISABLED;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanTunnelStpPDUs
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanTunnelStpPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelStpPDUs (INT4 i4Dot1qFutureVlanPort,
                                    INT4 *pi4RetValDot1qFutureVlanTunnelStpPDUs)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    *pi4RetValDot1qFutureVlanTunnelStpPDUs = VLAN_DISABLED;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanTunnelStpPDUsRecvd
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanTunnelStpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelStpPDUsRecvd (INT4 i4Dot1qFutureVlanPort,
                                         UINT4
                                         *pu4RetValDot1qFutureVlanTunnelStpPDUsRecvd)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4RetValDot1qFutureVlanTunnelStpPDUsRecvd = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanTunnelStpPDUsSent
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanTunnelStpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelStpPDUsSent (INT4 i4Dot1qFutureVlanPort,
                                        UINT4
                                        *pu4RetValDot1qFutureVlanTunnelStpPDUsSent)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    *pu4RetValDot1qFutureVlanTunnelStpPDUsSent = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanTunnelGvrpPDUs
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
retValDot1qFutureVlanTunnelGvrpPDUs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelGvrpPDUs (INT4 i4Dot1qFutureVlanPort,
                                     INT4
                                     *pi4RetValDot1qFutureVlanTunnelGvrpPDUs)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    *pi4RetValDot1qFutureVlanTunnelGvrpPDUs = VLAN_DISABLED;

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanTunnelGvrpPDUsRecvd
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
retValDot1qFutureVlanTunnelGvrpPDUsRecvd
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelGvrpPDUsRecvd (INT4 i4Dot1qFutureVlanPort,
                                          UINT4
                                          *pu4RetValDot1qFutureVlanTunnelGvrpPDUsRecvd)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4RetValDot1qFutureVlanTunnelGvrpPDUsRecvd = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanTunnelGvrpPDUsSent
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
retValDot1qFutureVlanTunnelGvrpPDUsSent
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelGvrpPDUsSent (INT4 i4Dot1qFutureVlanPort,
                                         UINT4
                                         *pu4RetValDot1qFutureVlanTunnelGvrpPDUsSent)
{

    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4RetValDot1qFutureVlanTunnelGvrpPDUsSent = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanTunnelIgmpPkts
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
retValDot1qFutureVlanTunnelIgmpPkts
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelIgmpPkts (INT4 i4Dot1qFutureVlanPort,
                                     INT4
                                     *pi4RetValDot1qFutureVlanTunnelIgmpPkts)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pi4RetValDot1qFutureVlanTunnelIgmpPkts = VLAN_DISABLED;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanTunnelIgmpPktsRecvd
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
retValDot1qFutureVlanTunnelIgmpPktsRecvd
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelIgmpPktsRecvd (INT4 i4Dot1qFutureVlanPort,
                                          UINT4
                                          *pu4RetValDot1qFutureVlanTunnelIgmpPktsRecvd)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4RetValDot1qFutureVlanTunnelIgmpPktsRecvd = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanTunnelIgmpPktsSent
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
retValDot1qFutureVlanTunnelIgmpPktsSent
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanTunnelIgmpPktsSent (INT4 i4Dot1qFutureVlanPort,
                                         UINT4
                                         *pu4RetValDot1qFutureVlanTunnelIgmpPktsSent)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4RetValDot1qFutureVlanTunnelIgmpPktsSent = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanFid
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object 
                retValDot1qFutureVlanFid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanFid (UINT4 u4Dot1qFutureVlanIndex,
                          UINT4 *pu4RetValDot1qFutureVlanFid)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot1qFutureVlanFid =
        VlanL2IwfMiGetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                                 (tVlanId) u4Dot1qFutureVlanIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterRxUcast
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId

                The Object
                retValDot1qFutureVlanCounterRxUcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureVlanCounterRxUcast
    (UINT4 u4Dot1qFutureVlanIndex,
     UINT4 *pu4RetValDot1qFutureVlanCounterRxUcast)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterRxUcast = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_UCAST_IN_FRAMES,
                            pu4RetValDot1qFutureVlanCounterRxUcast)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    : nmhGetDot1qFutureVlanCounterRxMcastBcast 
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId

                The Object
                retValDot1qFutureVlanCounterRxMcastBcast 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureVlanCounterRxMcastBcast
    (UINT4 u4Dot1qFutureVlanIndex,
     UINT4 *pu4RetValDot1qFutureVlanCounterRxMcastBcast)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterRxMcastBcast = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_MCAST_BCAST_IN_FRAMES,
                            pu4RetValDot1qFutureVlanCounterRxMcastBcast)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterTxUnknUcast 
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId

                The Object
                retValDot1qFutureVlanCounterTxUnknUcast 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureVlanCounterTxUnknUcast
    (UINT4 u4Dot1qFutureVlanIndex,
     UINT4 *pu4RetValDot1qFutureVlanCounterTxUnknUcast)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterTxUnknUcast = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_UNKNOWN_UCAST_OUT_FRAMES,
                            pu4RetValDot1qFutureVlanCounterTxUnknUcast)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterTxUcast
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId

                The Object
                retValDot1qFutureVlanCounterTxUcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureVlanCounterTxUcast
    (UINT4 u4Dot1qFutureVlanIndex,
     UINT4 *pu4RetValDot1qFutureVlanCounterTxUcast)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterTxUcast = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_UCAST_OUT_FRAMES,
                            pu4RetValDot1qFutureVlanCounterTxUcast)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    : nmhGetDot1qFutureVlanCounterTxBcast 
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId

                The Object
                retValDot1qFutureVlanCounterTxBcast 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureVlanCounterTxBcast
    (UINT4 u4Dot1qFutureVlanIndex,
     UINT4 *pu4RetValDot1qFutureVlanCounterTxBcast)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterTxBcast = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_BCAST_OUT_FRAMES,
                            pu4RetValDot1qFutureVlanCounterTxBcast)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDot1qFutureVlanCounterRxFrames
Input       :  The Indices
               Dot1qFutureVlanIndex
               The Object
               retValDot1qFutureVlanCounterRxFrames
Output      :  The Get Low Lev Routine Take the Indices &                                                                                                                   store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterRxFrames (UINT4 u4Dot1qFutureVlanIndex,
                                      UINT4
                                      *pu4RetValDot1qFutureVlanCounterRxFrames)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterRxFrames = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_VLAN_IN_FRAMES,
                            pu4RetValDot1qFutureVlanCounterRxFrames)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterRxBytes
 Input       :  The Indices
                Dot1qFutureVlanIndex
                The Object
                retValDot1qFutureVlanCounterRxBytes
 Output      :  The Get Low Lev Routine Take the Indices &*                                                                                                                                               store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterRxBytes (UINT4 u4Dot1qFutureVlanIndex,
                                     UINT4
                                     *pu4RetValDot1qFutureVlanCounterRxBytes)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterRxBytes = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_VLAN_IN_BYTES,
                            pu4RetValDot1qFutureVlanCounterRxBytes)
        != VLAN_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterTxFrames
 Input       :  The Indices
                Dot1qFutureVlanIndex
                                    
               The Object
               retValDot1qFutureVlanCounterTxFrames
 Output      :  The Get Low Lev Routine Take the Indices &*                                                                                                                                               store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterTxFrames (UINT4 u4Dot1qFutureVlanIndex,
                                      UINT4
                                      *pu4RetValDot1qFutureVlanCounterTxFrames)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterTxFrames = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_VLAN_OUT_FRAMES,
                            pu4RetValDot1qFutureVlanCounterTxFrames)
        != VLAN_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterTxBytes
 Input       :  The Indices
                Dot1qFutureVlanIndex
                The Object
                retValDot1qFutureVlanCounterTxBytes
 Output      :  The Get Low Lev Routine Take the Indices &                                                                                                              store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterTxBytes (UINT4 u4Dot1qFutureVlanIndex,
                                     UINT4
                                     *pu4RetValDot1qFutureVlanCounterTxBytes)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterTxBytes = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_VLAN_OUT_BYTES,
                            pu4RetValDot1qFutureVlanCounterTxBytes)
        != VLAN_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterDiscardFrames
 Input       :  The Indices
                Dot1qFutureVlanIndex
                The Object
                retValDot1qFutureVlanCounterDiscardFrames
 Output      :  The Get Low Lev Routine Take the Indices &                                                                                                              store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterDiscardFrames (UINT4 u4Dot1qFutureVlanIndex,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanCounterDiscardFrames)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterDiscardFrames = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_VLAN_DISCARD_FRAMES,
                            pu4RetValDot1qFutureVlanCounterDiscardFrames)
        != VLAN_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterDiscardBytes
 Input       :  The Indices
                Dot1qFutureVlanIndex
                The Object
                retValDot1qFutureVlanCounterDiscardBytes
 Output      :  The Get Low Lev Routine Take the Indices &                                                                                                              store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterDiscardBytes (UINT4 u4Dot1qFutureVlanIndex,
                                          UINT4
                                          *pu4RetValDot1qFutureVlanCounterDiscardBytes)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == VLAN_DISABLED)
    {
        *pu4RetValDot1qFutureVlanCounterDiscardBytes = 0;
        return SNMP_SUCCESS;
    }

    if (VlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                            u4Dot1qFutureVlanIndex,
                            VLAN_STAT_VLAN_DISCARD_BYTES,
                            pu4RetValDot1qFutureVlanCounterDiscardBytes)
        != VLAN_SUCCESS)

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanCounterStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                retValDot1qFutureVlanCounterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanCounterStatus (UINT4 u4Dot1qFutureVlanIndex,
                                    INT4 *pi4RetValDot1qFutureVlanCounterStatus)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);

    if (pCurrEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanCounterStatus =
            pCurrEntry->i4VlanCounterStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanUnicastMacLimit
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                retValDot1qFutureVlanUnicastMacLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanUnicastMacLimit (UINT4 u4Dot1qFutureVlanIndex,
                                      UINT4
                                      *pu4RetValDot1qFutureVlanUnicastMacLimit)
{
    tVlanCurrEntry     *pCurrEntry = NULL;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);

    if (pCurrEntry != NULL)
    {

        *pu4RetValDot1qFutureVlanUnicastMacLimit =
            VLAN_CONTROL_MAC_LEARNING_LIMIT (u4Dot1qFutureVlanIndex);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanAdminMacLearningStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                retValDot1qFutureVlanAdminMacLearningStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanAdminMacLearningStatus (UINT4 u4Dot1qFutureVlanIndex,
                                             INT4
                                             *pi4RetValDot1qFutureVlanAdminMacLearningStatus)
{
    if (VlanGetVlanAdminMacLearnStatus
        (u4Dot1qFutureVlanIndex,
         pi4RetValDot1qFutureVlanAdminMacLearningStatus) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanOperMacLearningStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                retValDot1qFutureVlanOperMacLearningStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanOperMacLearningStatus (UINT4 u4Dot1qFutureVlanIndex,
                                            INT4
                                            *pi4RetValDot1qFutureVlanOperMacLearningStatus)
{
    *pi4RetValDot1qFutureVlanOperMacLearningStatus =
        (INT4) VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (u4Dot1qFutureVlanIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortFdbFlush
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object 
                retValDot1qFutureVlanPortFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortFdbFlush (UINT4 u4Dot1qFutureVlanIndex,
                                   INT4 *pi4RetValDot1qFutureVlanPortFdbFlush)
{

    tStaticVlanEntry   *pStVlanEntry = NULL;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pStVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pStVlanEntry->bVlanFdbFlush == VLAN_FALSE)
    {
        *pi4RetValDot1qFutureVlanPortFdbFlush = VLAN_DISABLED;
    }
    else
    {
        *pi4RetValDot1qFutureVlanPortFdbFlush = VLAN_ENABLED;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanTpFdbTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1qFutureVlanTpFdbTable (UINT4 *pu4Dot1qFdbId,
                                           tMacAddr * pDot1qTpFdbAddress)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    return (nmhGetFirstIndexDot1qTpFdbTable (pu4Dot1qFdbId,
                                             pDot1qTpFdbAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanTpFdbTable
 Input       :  The Indices
                Dot1qFdbId
                nextDot1qFdbId
                Dot1qTpFdbAddress
                nextDot1qTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1qFutureVlanTpFdbTable (UINT4 u4Dot1qFdbId,
                                          UINT4 *pu4NextDot1qFdbId,
                                          tMacAddr Dot1qTpFdbAddress,
                                          tMacAddr * pNextDot1qTpFdbAddress)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }
    return (nmhGetNextIndexDot1qTpFdbTable (u4Dot1qFdbId, pu4NextDot1qFdbId,
                                            Dot1qTpFdbAddress,
                                            pNextDot1qTpFdbAddress));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanTpFdbPw
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress
                

                The Object 
                retValDot1qFutureVlanTpFdbPw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanTpFdbPw (UINT4 u4Dot1qFdbId, tMacAddr Dot1qTpFdbAddress,
                              UINT4 *pu4RetValDot1qFutureVlanTpFdbPw)
{
#ifdef MPLS_WANTED
    UINT1               u1Status;

    if (VlanGetVplsFdbInfo (u4Dot1qFdbId, Dot1qTpFdbAddress,
                            pu4RetValDot1qFutureVlanTpFdbPw, &u1Status) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (u4Dot1qFdbId);
    UNUSED_PARAM (Dot1qTpFdbAddress);
    *pu4RetValDot1qFutureVlanTpFdbPw = 0;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qTpOldFdbPort
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress
                                                                                                                            
                The Object
                retValDot1qTpOldFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qTpOldFdbPort (UINT4 u4Dot1qFdbId, tMacAddr Dot1qTpFdbAddress,
                         INT4 *pi4RetValDot1qTpOldFdbPort)
{
    UNUSED_PARAM (u4Dot1qFdbId);
    UNUSED_PARAM (Dot1qTpFdbAddress);

    *pi4RetValDot1qTpOldFdbPort = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureConnectionIdentifier
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress

                The Object
                retValDot1qFutureConnectionIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1qFutureConnectionIdentifier
    (UINT4 u4Dot1qFdbId,
     tMacAddr Dot1qTpFdbAddress,
     tMacAddr * pRetValDot1qFutureConnectionIdentifier)
{
    tVlanTempFdbInfo    VlanTempFdbInfo;

    if (VlanGetFdbInfo (u4Dot1qFdbId, Dot1qTpFdbAddress,
                        &VlanTempFdbInfo) == VLAN_SUCCESS)
    {
        MEMCPY (pRetValDot1qFutureConnectionIdentifier,
                VlanTempFdbInfo.au1ConnectionId, sizeof (tMacAddr));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanFilteringUtilityCriteria
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                retValDot1qFutureVlanFilteringUtilityCriteria
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanFilteringUtilityCriteria (INT4 i4Dot1qFutureVlanPort,
                                               INT4
                                               *pi4RetValDot1qFutureVlanFilteringUtilityCriteria)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureVlanFilteringUtilityCriteria =
        pVlanPortEntry->u2FiltUtilityCriteria;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanWildCardEgressPorts
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress

                The Object 
                retValDot1qFutureVlanWildCardEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanWildCardEgressPorts (tMacAddr WildCardMacAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValWildCardEgressPorts)
{
    tVlanWildCardEntry *pWildCardEntry;

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry != NULL)
    {
        MEMSET (pRetValWildCardEgressPorts->pu1_OctetList, 0,
                VLAN_PORT_LIST_SIZE);

        VLAN_ADD_PORT_LIST (pRetValWildCardEgressPorts->pu1_OctetList,
                            pWildCardEntry->EgressPorts);

        pRetValWildCardEgressPorts->i4_Length = VLAN_PORT_LIST_SIZE;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanWildCardRowStatus
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress

                The Object 
                retValDot1qFutureVlanWildCardRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanWildCardRowStatus (tMacAddr WildCardMacAddress,
                                        INT4 *pi4RetValWildCardRowStatus)
{
    tVlanWildCardEntry *pWildCardEntry;

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry != NULL)
    {
        *pi4RetValWildCardRowStatus = pWildCardEntry->u1RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortProtected
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortProtected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortProtected (INT4 i4Dot1qFutureVlanPort,
                                    INT4 *pi4RetValDot1qFutureVlanPortProtected)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureVlanPortProtected = pVlanPortEntry->u1PortProtected;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortSubnetBasedClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortSubnetBasedClassification (INT4 i4Dot1qFutureVlanPort,
                                                    INT4
                                                    *pi4RetValDot1qFutureVlanPortSubnetBasedClassification)
{
    UINT2               u2Port = VLAN_INIT_VAL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureVlanPortSubnetBasedClassification =
        (INT4) pVlanPortEntry->u1SubnetBasedClassification;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortUnicastMacLearning
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortUnicastMacLearning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortUnicastMacLearning (INT4 i4Dot1qFutureVlanPort,
                                             INT4
                                             *pi4RetValDot1qFutureVlanPortUnicastMacLearning)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortUnicastMacLearning =
        (INT4) pVlanPortEntry->u1MacLearningStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortIngressEtherType                                                                      Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortIngressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortIngressEtherType (INT4 i4Dot1qFutureVlanPort,
                                           INT4
                                           *pi4RetValDot1qFutureVlanPortIngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortIngressEtherType =
        (INT4) pVlanPortEntry->u2IngressEtherType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortEgressEtherType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortEgressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortEgressEtherType (INT4 i4Dot1qFutureVlanPort,
                                          INT4
                                          *pi4RetValDot1qFutureVlanPortEgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortEgressEtherType =
        (INT4) pVlanPortEntry->u2EgressEtherType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortEgressTPIDType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortEgressTPIDType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortEgressTPIDType (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         *pi4RetValDot1qFutureVlanPortEgressTPIDType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortEgressTPIDType =
        (INT4) pVlanPortEntry->u1EgressTPIDType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortAllowableTPID1
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortAllowableTPID1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortAllowableTPID1 (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         *pi4RetValDot1qFutureVlanPortAllowableTPID1)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortAllowableTPID1 =
        (INT4) pVlanPortEntry->u2AllowableTPID1;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortAllowableTPID2
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortAllowableTPID2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortAllowableTPID2 (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         *pi4RetValDot1qFutureVlanPortAllowableTPID2)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortAllowableTPID2 =
        (INT4) pVlanPortEntry->u2AllowableTPID2;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortAllowableTPID3
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                retValDot1qFutureVlanPortAllowableTPID3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortAllowableTPID3 (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         *pi4RetValDot1qFutureVlanPortAllowableTPID3)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortAllowableTPID3 =
        (INT4) pVlanPortEntry->u2AllowableTPID3;
    return SNMP_SUCCESS;

}

/****************************************************************************
* Function    :  nmhGetDot1qFutureVlanPortUnicastMacSecType
* Input       :  The Indices
*                Dot1qFutureVlanPort
*                The Object
*                      retValDot1qFutureVlanPortUnicastMacSecType
* Output      :  The Get Low Lev Routine Take the Indices &
* store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortUnicastMacSecType (INT4 i4Dot1qFutureVlanPort,
                                            INT4
                                            *pi4RetValDot1qFutureVlanPortUnicastMacSecType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1qFutureVlanPortUnicastMacSecType =
        (INT4) pVlanPortEntry->i4UnicastMacSecType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureUnicastMacLearningLimit
 Input       :  The Indices

                The Object 
                retValDot1qFutureUnicastMacLearningLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureUnicastMacLearningLimit (UINT4
                                          *pu4RetValDot1qFutureUnicastMacLearningLimit)
{
    *pu4RetValDot1qFutureUnicastMacLearningLimit = VLAN_DYNAMIC_UNICAST_SIZE;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1qFutureStaticConnectionIdentifier
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort

                The Object
                retValDot1qFutureStaticConnectionIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot1qFutureStaticConnectionIdentifier
    (UINT4 u4Dot1qFdbId,
     tMacAddr Dot1qStaticUnicastAddress,
     INT4 i4Dot1qStaticUnicastReceivePort,
     tMacAddr * pRetValDot1qFutureStaticConnectionIdentifier)
{
    if (VlanGetConnectionIdentifier (u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                                     i4Dot1qStaticUnicastReceivePort,
                                     pRetValDot1qFutureStaticConnectionIdentifier)
        == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetMapVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                retValDot1qFutureVlanPortSubnetMapVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortSubnetMapVid (INT4 i4Dot1qFutureVlanPort,
                                       UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
                                       INT4
                                       *pi4RetValDot1qFutureVlanPortSubnetMapVid)
{

    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhGetDot1qFutureVlanPortSubnetMapExtVid
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask, pi4RetValDot1qFutureVlanPortSubnetMapVid));
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetMapARPOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                retValDot1qFutureVlanPortSubnetMapARPOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1qFutureVlanPortSubnetMapARPOption
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 *pi4RetValDot1qFutureVlanPortSubnetMapARPOption)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhGetDot1qFutureVlanPortSubnetMapExtARPOption
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask, pi4RetValDot1qFutureVlanPortSubnetMapARPOption));
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetMapRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                retValDot1qFutureVlanPortSubnetMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1qFutureVlanPortSubnetMapRowStatus
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 *pi4RetValDot1qFutureVlanPortSubnetMapRowStatus)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask, pi4RetValDot1qFutureVlanPortSubnetMapRowStatus));
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetMapExtVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                retValDot1qFutureVlanPortSubnetMapExtVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1qFutureVlanPortSubnetMapExtVid
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 *pi4RetValDot1qFutureVlanPortSubnetMapExtVid)
{
    tVlanSubnetMapEntry *pVlanSubnetMapEntry = NULL;

    pVlanSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               (UINT4) i4Dot1qFutureVlanPort,
                               u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pVlanSubnetMapEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanPortSubnetMapExtVid =
            (INT4) pVlanSubnetMapEntry->VlanId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetMapExtARPOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask
                The Object
                retValDot1qFutureVlanPortSubnetMapExtARPOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1qFutureVlanPortSubnetMapExtARPOption
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 *pi4RetValDot1qFutureVlanPortSubnetMapExtARPOption)
{
    tVlanSubnetMapEntry *pVlanSubnetMapEntry = NULL;

    pVlanSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               (UINT4) i4Dot1qFutureVlanPort,
                               u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pVlanSubnetMapEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanPortSubnetMapExtARPOption =
            (INT4) pVlanSubnetMapEntry->u1ArpOption;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                retValDot1qFutureVlanPortSubnetMapExtRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 *pi4RetValDot1qFutureVlanPortSubnetMapExtRowStatus)
{
    tVlanSubnetMapEntry *pVlanSubnetMapEntry = NULL;

    pVlanSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               (UINT4) i4Dot1qFutureVlanPort,
                               u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pVlanSubnetMapEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanPortSubnetMapExtRowStatus =
            (INT4) pVlanSubnetMapEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1qFutureStVlanEgressEthertype
 Input       :  The Indices
                Dot1qVlanIndex

                The Object
                retValDot1qFutureStVlanEgressEthertype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureStVlanEgressEthertype (UINT4 u4Dot1qVlanIndex,
                                        INT4
                                        *pi4RetValDot1qFutureStVlanEgressEthertype)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qVlanIndex);
    if (pStVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qFutureStVlanEgressEthertype
        = (INT4) pStVlanEntry->u2EgressEtherType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanGlobalsFdbFlush
 Input       :  The Indices

                The Object 
                retValDot1qFutureVlanGlobalsFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanGlobalsFdbFlush (INT4
                                      *pi4RetValDot1qFutureVlanGlobalsFdbFlush)
{
    *pi4RetValDot1qFutureVlanGlobalsFdbFlush =
        gpVlanContextInfo->bGlobalFdbFlush;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanUserDefinedTPID
 Input       :  The Indices

                The Object
                retValDot1qFutureVlanUserDefinedTPID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanUserDefinedTPID (INT4
                                      *pi4RetValDot1qFutureVlanUserDefinedTPID)
{
    *pi4RetValDot1qFutureVlanUserDefinedTPID = 0;

    if (VLAN_CURR_CONTEXT_PTR () != NULL)
    {
        *pi4RetValDot1qFutureVlanUserDefinedTPID =
            (INT4) VLAN_PORT_USER_DEFINED_TPID;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanLoopbackStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                retValDot1qFutureVlanLoopbackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanLoopbackStatus (UINT4 u4Dot1qFutureVlanIndex,
                                     INT4
                                     *pi4RetValDot1qFutureVlanLoopbackStatus)
{
    tVlanCurrEntry     *pCurrEntry;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);

    if (pCurrEntry != NULL)
    {
        *pi4RetValDot1qFutureVlanLoopbackStatus =
            pCurrEntry->i4VlanLoopbackStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFuturePortPacketReflectionStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
               
                The Object
                retValDot1qFuturePortPacketReflectionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFuturePortPacketReflectionStatus (INT4 i4Dot1qFutureVlanPort,
                                             INT4
                                             *pi4RetValDot1qFuturePortPacketReflectionStatus)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    /*Get the port entry */
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    /*If port entry is NULL no need to proceed */
    if (NULL == pVlanPortEntry)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValDot1qFuturePortPacketReflectionStatus =
            pVlanPortEntry->i4ReflectionStatus;
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanRemoteFdbFlush
 Input       :  The Indices

                The Object
                retValDot1qFutureVlanRemoteFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDot1qFutureVlanRemoteFdbFlush (INT4
                                     *pi4RetValDot1qFutureVlanRemoteFdbFlush)
{
    *pi4RetValDot1qFutureVlanRemoteFdbFlush =
        gpVlanContextInfo->bRemoteFdbFlush;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureStVlanType
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qFutureStVlanType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureStVlanType (UINT4 u4Dot1qVlanIndex,
                             INT4 *pi4RetValDot1qFutureStVlanType)
{
    UNUSED_PARAM (u4Dot1qVlanIndex);
    *pi4RetValDot1qFutureStVlanType = L2IWF_NORMAL_VLAN;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1qFutureStVlanVid
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValDot1qFutureStVlanVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureStVlanVid (UINT4 u4Dot1qVlanIndex,
                            INT4 *pi4RetValDot1qFutureStVlanVid)
{
    UNUSED_PARAM (u4Dot1qVlanIndex);
    *pi4RetValDot1qFutureStVlanVid = u4Dot1qVlanIndex;
    return SNMP_SUCCESS;
}
