/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1d1lw.c,v 1.22 2016/07/14 11:07:18 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"

/* LOW LEVEL Routines for Table : Ieee8021BridgeBaseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeBaseTable (UINT4
                                                 u4Ieee8021BridgeBaseComponentId)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeBaseTable (UINT4
                                         *pu4Ieee8021BridgeBaseComponentId)
{
    if (VlanGetFirstActiveContext (pu4Ieee8021BridgeBaseComponentId) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgeBaseComponentId));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
                nextIeee8021BridgeBaseComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeBaseTable (UINT4 u4Ieee8021BridgeBaseComponentId,
                                        UINT4
                                        *pu4NextIeee8021BridgeBaseComponentId)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    if (VlanGetNextActiveContext (u4Ieee8021BridgeBaseComponentId,
                                  pu4NextIeee8021BridgeBaseComponentId)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBaseComponentId));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseBridgeAddress
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseBridgeAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseBridgeAddress (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       tMacAddr *
                                       pRetValIeee8021BridgeBaseBridgeAddress)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_MEMSET (pRetValIeee8021BridgeBaseBridgeAddress, 0, sizeof (tMacAddr));
    MEMCPY (pRetValIeee8021BridgeBaseBridgeAddress,
            VLAN_CURR_CONTEXT_PTR ()->VlanSysMacAddress, sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseNumPorts
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseNumPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseNumPorts (UINT4 u4Ieee8021BridgeBaseComponentId,
                                  INT4 *pi4RetValIeee8021BridgeBaseNumPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1dBaseNumPorts (pi4RetValIeee8021BridgeBaseNumPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseComponentType
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseComponentType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseComponentType (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       INT4
                                       *pi4RetValIeee8021BridgeBaseComponentType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    return (nmhGetFsMIDot1qFutureVlanBridgeMode
            (u4Ieee8021BridgeBaseComponentId,
             pi4RetValIeee8021BridgeBaseComponentType));
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseDeviceCapabilities
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseDeviceCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseDeviceCapabilities (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021BridgeBaseDeviceCapabilities)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    return (nmhGetFsDot1dDeviceCapabilities (u4Ieee8021BridgeBaseComponentId,
                                             pRetValIeee8021BridgeBaseDeviceCapabilities));
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseTrafficClassesEnabled
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseTrafficClassesEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseTrafficClassesEnabled (UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               INT4
                                               *pi4RetValIeee8021BridgeBaseTrafficClassesEnabled)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    return (nmhGetFsDot1dTrafficClassesEnabled (u4Ieee8021BridgeBaseComponentId,
                                                pi4RetValIeee8021BridgeBaseTrafficClassesEnabled));
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseMmrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBaseComponentId,
                                           INT4
                                           *pi4RetValIeee8021BridgeBaseMmrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MmrpEnabledStatus = 0;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.u4ContextId =
        u4Ieee8021BridgeBaseComponentId;
    MrpConfigInfo.u4InfoType = MMRP_ENABLED_STATUS_GET;
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pi4MmrpEnabledStatus =
        &i4MmrpEnabledStatus;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        *pi4RetValIeee8021BridgeBaseMmrpEnabledStatus =
            *(MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pi4MmrpEnabledStatus);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseRowStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseRowStatus (UINT4 u4Ieee8021BridgeBaseComponentId,
                                   INT4 *pi4RetValIeee8021BridgeBaseRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeBaseRowStatus =
        (VLAN_CURR_CONTEXT_PTR ()->i4BridgeBaseRowStatus);
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseBridgeAddress
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseBridgeAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseBridgeAddress (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       tMacAddr
                                       SetValIeee8021BridgeBaseBridgeAddress)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (VLAN_CURR_CONTEXT_PTR ()->VlanSysMacAddress,
            SetValIeee8021BridgeBaseBridgeAddress, sizeof (tMacAddr));
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseComponentType
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseComponentType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseComponentType (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       INT4
                                       i4SetValIeee8021BridgeBaseComponentType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    return (nmhSetFsMIDot1qFutureVlanBridgeMode
            (u4Ieee8021BridgeBaseComponentId,
             i4SetValIeee8021BridgeBaseComponentType));
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseDeviceCapabilities
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseDeviceCapabilities
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseDeviceCapabilities (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021BridgeBaseDeviceCapabilities)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pSetValIeee8021BridgeBaseDeviceCapabilities);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseTrafficClassesEnabled
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseTrafficClassesEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseTrafficClassesEnabled (UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               INT4
                                               i4SetValIeee8021BridgeBaseTrafficClassesEnabled)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1dTrafficClassesEnabled
        (i4SetValIeee8021BridgeBaseTrafficClassesEnabled);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseMmrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBaseComponentId,
                                           INT4
                                           i4SetValIeee8021BridgeBaseMmrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MmrpEnabledStatus = 0;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.u4ContextId =
        u4Ieee8021BridgeBaseComponentId;
    MrpConfigInfo.u4InfoType = MMRP_ENABLED_STATUS_SET;
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.i4MmrpEnabledStatus =
        i4SetValIeee8021BridgeBaseMmrpEnabledStatus;
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pi4MmrpEnabledStatus =
        &i4MmrpEnabledStatus;
    *(MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pi4MmrpEnabledStatus) =
        i4SetValIeee8021BridgeBaseMmrpEnabledStatus;
    VLAN_UNLOCK ();
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        VLAN_LOCK ();
        return SNMP_SUCCESS;
    }
    VLAN_LOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseRowStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseRowStatus (UINT4 u4Ieee8021BridgeBaseComponentId,
                                   INT4 i4SetValIeee8021BridgeBaseRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    switch (i4SetValIeee8021BridgeBaseRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
            if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) !=
                VLAN_SUCCESS)
            {
                VlanHandleCreateContext (u4Ieee8021BridgeBaseComponentId);
            }
            break;
        case VLAN_ACTIVE:
            if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) ==
                VLAN_SUCCESS)
            {
                VLAN_CURR_CONTEXT_PTR ()->i4BridgeBaseRowStatus = VLAN_ACTIVE;
            }
            VlanReleaseContext ();
            break;

        case VLAN_NOT_IN_SERVICE:
            if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) ==
                VLAN_SUCCESS)
            {
                VLAN_CURR_CONTEXT_PTR ()->i4BridgeBaseRowStatus =
                    VLAN_NOT_IN_SERVICE;
            }
            VlanReleaseContext ();
            break;

        case VLAN_DESTROY:
            VlanHandleDeleteContext (u4Ieee8021BridgeBaseComponentId);
            break;

        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseBridgeAddress
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseBridgeAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseBridgeAddress (UINT4 *pu4ErrorCode,
                                          UINT4 u4Ieee8021BridgeBaseComponentId,
                                          tMacAddr
                                          TestValIeee8021BridgeBaseBridgeAddress)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) == VLAN_SUCCESS)
    {
        if (VlanIsValidUcastAddr (TestValIeee8021BridgeBaseBridgeAddress)
            == VLAN_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseComponentType
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseComponentType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseComponentType (UINT4 *pu4ErrorCode,
                                          UINT4 u4Ieee8021BridgeBaseComponentId,
                                          INT4
                                          i4TestValIeee8021BridgeBaseComponentType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    return (nmhTestv2FsMIDot1qFutureVlanBridgeMode (pu4ErrorCode,
                                                    u4Ieee8021BridgeBaseComponentId,
                                                    i4TestValIeee8021BridgeBaseComponentType));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseDeviceCapabilities
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseDeviceCapabilities
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseDeviceCapabilities (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021BridgeBaseDeviceCapabilities)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pTestValIeee8021BridgeBaseDeviceCapabilities);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseTrafficClassesEnabled
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseTrafficClassesEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseTrafficClassesEnabled (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBaseComponentId,
                                                  INT4
                                                  i4TestValIeee8021BridgeBaseTrafficClassesEnabled)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBaseComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1dTrafficClassesEnabled (pu4ErrorCode,
                                                    i4TestValIeee8021BridgeBaseTrafficClassesEnabled);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseMmrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseMmrpEnabledStatus (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBaseComponentId,
                                              INT4
                                              i4TestValIeee8021BridgeBaseMmrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MmrpEnabledStatus = 0;
    UINT4               u4ErrorCode = SNMP_FAILURE;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.u4ContextId =
        u4Ieee8021BridgeBaseComponentId;
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.i4MmrpEnabledStatus =
        i4TestValIeee8021BridgeBaseMmrpEnabledStatus;
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pi4MmrpEnabledStatus =
        &i4MmrpEnabledStatus;
    *(MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pi4MmrpEnabledStatus) =
        i4TestValIeee8021BridgeBaseMmrpEnabledStatus;
    MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pu4ErrorCode = &u4ErrorCode;
    MrpConfigInfo.u4InfoType = MMRP_ENABLED_STATUS_TEST;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = *(MrpConfigInfo.unMrpInfo.MmrpStatusInfo.pu4ErrorCode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseRowStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021BridgeBaseComponentId,
                                      INT4 i4TestValIeee8021BridgeBaseRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBaseComponentId);

    if (((INT4) u4Ieee8021BridgeBaseComponentId < 0) ||
        (u4Ieee8021BridgeBaseComponentId >= VLAN_SIZING_CONTEXT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValIeee8021BridgeBaseRowStatus < VLAN_ACTIVE) ||
        (i4TestValIeee8021BridgeBaseRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeBaseTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeBasePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeBasePortTable (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        VlanValidateIndexIeee8021dBasePortTable (u4Ieee8021BridgeBasePort);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeBasePortTable (UINT4
                                             *pu4Ieee8021BridgeBasePortComponentId,
                                             UINT4 *pu4Ieee8021BridgeBasePort)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
        if (VlanGetNextIndexIEEE8021Dot1dBasePortTable
            (0, pu4Ieee8021BridgeBasePort) == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID
                ((*pu4Ieee8021BridgeBasePortComponentId));
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021BridgeBasePortComponentId,
            &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeBasePortTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePort)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetNextIndexIEEE8021Dot1dBasePortTable
        ((INT4) u4Ieee8021BridgeBasePort,
         pu4NextIeee8021BridgeBasePort) == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortIfIndex
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortIfIndex (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4
                                     *pi4RetValIeee8021BridgeBasePortIfIndex)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort);
    if (pBasePortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgeBasePortIfIndex = pBasePortEntry->u4IfIndex;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortDelayExceededDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortDelayExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortDelayExceededDiscards (UINT4
                                                   u4Ieee8021BridgeBasePortComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeBasePort,
                                                   tSNMP_COUNTER64_TYPE *
                                                   pu8RetValIeee8021BridgeBasePortDelayExceededDiscards)
{

    UINT4              *pu4lsn = NULL;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    pu8RetValIeee8021BridgeBasePortDelayExceededDiscards->lsn = 0;
    pu8RetValIeee8021BridgeBasePortDelayExceededDiscards->msn = 0;
    pu4lsn = &(pu8RetValIeee8021BridgeBasePortDelayExceededDiscards->lsn);
    if ((nmhGetDot1dBasePortDelayExceededDiscards ((INT4)
                                                   u4Ieee8021BridgeBasePort,
                                                   pu4lsn)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pu8RetValIeee8021BridgeBasePortDelayExceededDiscards->lsn = *pu4lsn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortMtuExceededDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortMtuExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortMtuExceededDiscards (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 tSNMP_COUNTER64_TYPE *
                                                 pu8RetValIeee8021BridgeBasePortMtuExceededDiscards)
{
    UINT4              *pu4lsn = NULL;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    pu8RetValIeee8021BridgeBasePortMtuExceededDiscards->lsn = 0;
    pu8RetValIeee8021BridgeBasePortMtuExceededDiscards->msn = 0;
    pu4lsn = &(pu8RetValIeee8021BridgeBasePortMtuExceededDiscards->lsn);
    if ((nmhGetDot1dBasePortMtuExceededDiscards ((INT4)
                                                 u4Ieee8021BridgeBasePort,
                                                 pu4lsn)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pu8RetValIeee8021BridgeBasePortMtuExceededDiscards->lsn = *pu4lsn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortCapabilities
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortCapabilities (UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValIeee8021BridgeBasePortCapabilities)
{
/* The BridgeBasePortComponentId & BridgeBasePort are not needed to provide the BridgeBase attributes.The validation of BridgeBasePortComponentId & BridgeBasePort is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);

    pRetValIeee8021BridgeBasePortCapabilities->i4_Length =
        VLAN_CAPABILITIES_MASK_LENGTH;
    pRetValIeee8021BridgeBasePortCapabilities->pu1_OctetList[0] =
        (UINT1) VLAN_PORT_ALL_CAPABILITIES_MASK;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortTypeCapabilities
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortTypeCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortTypeCapabilities (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021BridgeBasePortTypeCapabilities)
{
/* The BridgeBasePortComponentId & BridgeBasePort are not needed to provide the BridgeBase attributes.The validation of BridgeBasePortComponentId & BridgeBasePort is done in the validate routine.*/

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);

    pRetValIeee8021BridgeBasePortTypeCapabilities->i4_Length =
        VLAN_CAPABILITIES_MASK_LENGTH;
    pRetValIeee8021BridgeBasePortTypeCapabilities->pu1_OctetList[0] =
        (UINT1) VLAN_PORT_ALL_CAPABILITIES_MASK;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortType
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortType (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  INT4 *pi4RetValIeee8021BridgeBasePortType)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;
    INT4                i4BrgPortType = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort);
    if (pBasePortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VlanCfaGetInterfaceBrgPortType
        (pBasePortEntry->u4IfIndex, &i4BrgPortType) == VLAN_SUCCESS)
    {
        switch (i4BrgPortType)
        {
            case CFA_PROVIDER_NETWORK_PORT:
                *pi4RetValIeee8021BridgeBasePortType = VLAN_PROVID_NETWORK_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;

            case CFA_CNP_PORTBASED_PORT:
                *pi4RetValIeee8021BridgeBasePortType =
                    VLAN_CUSTOMER_NETWORK_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;

            case CFA_CUSTOMER_EDGE_PORT:
                *pi4RetValIeee8021BridgeBasePortType = VLAN_CUST_EDGE_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;

            case CFA_CUSTOMER_BACKBONE_PORT:
                *pi4RetValIeee8021BridgeBasePortType = VLAN_CUST_BACKBONE_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;

            case CFA_VIRTUAL_INSTANCE_PORT:
                *pi4RetValIeee8021BridgeBasePortType = VLAN_VIRT_INSTANCE_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;

            default:
                break;
        }
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        *pi4RetValIeee8021BridgeBasePortType = VLAN_CUSTOMER_VLAN_PORT;
    }
    else if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pi4RetValIeee8021BridgeBasePortType = VLAN_BRIDGE_PORT;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortExternal
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortExternal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortExternal (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4
                                      *pi4RetValIeee8021BridgeBasePortExternal)
{
/* The BridgeBasePortComponentId & BridgeBasePort are not needed to provide the BridgeBase attributes.The validation of BridgeBasePortComponentId & BridgeBasePort is done in the validate routine.*/

    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);

    *pi4RetValIeee8021BridgeBasePortExternal = VLAN_TRUE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortAdminPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortAdminPointToPoint (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               *pi4RetValIeee8021BridgeBasePortAdminPointToPoint)
{
    tStpConfigInfo      StpConfigInfo;
    INT4                i4PortAdminPointToPoint = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    MEMSET (&StpConfigInfo, 0, sizeof (tStpConfigInfo));

    StpConfigInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    StpConfigInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    StpConfigInfo.u1InfoType = STP_ADMIN_POINT_POINT_STATUS_GET;
    StpConfigInfo.pi4PortAdminPointToPoint = &i4PortAdminPointToPoint;
    if ((AstApiStpConfigInfo (&StpConfigInfo)) == OSIX_SUCCESS)
    {
        /* If the value is not updated return the default value as FORCE FALSE
           as per std1d1ap.mib IEEE MIB for 802.1 D devices */
        if (StpConfigInfo.bAdminP2PUpdated == AST_FALSE)
        {
            *pi4RetValIeee8021BridgeBasePortAdminPointToPoint =
                IEEE802ap_FORCEFALSE;
            return SNMP_SUCCESS;
        }
        else
        {
            if (*(StpConfigInfo.pi4PortAdminPointToPoint) == RST_P2P_FORCETRUE)
            {
                *pi4RetValIeee8021BridgeBasePortAdminPointToPoint =
                    IEEE802ap_FORCETRUE;
            }
            if (*(StpConfigInfo.pi4PortAdminPointToPoint) == RST_P2P_FORCEFALSE)
            {
                *pi4RetValIeee8021BridgeBasePortAdminPointToPoint =
                    IEEE802ap_FORCEFALSE;
            }
            if (*(StpConfigInfo.pi4PortAdminPointToPoint) == RST_P2P_AUTO)
            {
                *pi4RetValIeee8021BridgeBasePortAdminPointToPoint =
                    IEEE802ap_AUTO;
            }
        }
        return SNMP_SUCCESS;
    }
    *pi4RetValIeee8021BridgeBasePortAdminPointToPoint = IEEE802ap_FORCEFALSE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortOperPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortOperPointToPoint (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              *pi4RetValIeee8021BridgeBasePortOperPointToPoint)
{
    tStpConfigInfo      StpConfigInfo;
    INT4                i4PortOperPointToPoint = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    MEMSET (&StpConfigInfo, 0, sizeof (tStpConfigInfo));
    StpConfigInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    StpConfigInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    StpConfigInfo.u1InfoType = STP_OPER_POINT_POINT_STATUS_GET;
    StpConfigInfo.pi4PortOperPointToPoint = &i4PortOperPointToPoint;
    if ((AstApiStpConfigInfo (&StpConfigInfo)) == OSIX_SUCCESS)
    {
        if (*(StpConfigInfo.pi4PortOperPointToPoint) == RST_P2P_FORCETRUE)
        {
            *pi4RetValIeee8021BridgeBasePortOperPointToPoint =
                IEEE802ap_FORCETRUE;
        }
        if ((*(StpConfigInfo.pi4PortOperPointToPoint) == RST_P2P_FORCEFALSE) ||
            (*(StpConfigInfo.pi4PortOperPointToPoint) == RST_P2P_AUTO))
        {
            *pi4RetValIeee8021BridgeBasePortOperPointToPoint =
                IEEE802ap_FORCEFALSE;
        }

        return SNMP_SUCCESS;
    }
    *pi4RetValIeee8021BridgeBasePortOperPointToPoint = IEEE802ap_FORCEFALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortName
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortName (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValIeee8021BridgeBasePortName)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    if ((CfaApiGetPortDesc (u4Ieee8021BridgeBasePort,
                            pRetValIeee8021BridgeBasePortName)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBasePortIfIndex
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgeBasePortIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBasePortIfIndex (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4 i4SetValIeee8021BridgeBasePortIfIndex)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort);
    if (pBasePortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pBasePortEntry->u4IfIndex = i4SetValIeee8021BridgeBasePortIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBasePortAdminPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgeBasePortAdminPointToPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBasePortAdminPointToPoint (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4SetValIeee8021BridgeBasePortAdminPointToPoint)
{
    tStpConfigInfo      StpConfigInfo;
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    MEMSET (&StpConfigInfo, 0, sizeof (tStpConfigInfo));
    StpConfigInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    StpConfigInfo.u4IfIndex = u4Ieee8021BridgeBasePort;

    StpConfigInfo.u1InfoType = STP_ADMIN_POINT_POINT_STATUS_SET;

    if (i4SetValIeee8021BridgeBasePortAdminPointToPoint == IEEE802ap_FORCETRUE)
    {
        StpConfigInfo.i4PortAdminPointToPoint = RST_P2P_FORCETRUE;
    }
    if (i4SetValIeee8021BridgeBasePortAdminPointToPoint == IEEE802ap_FORCEFALSE)
    {
        StpConfigInfo.i4PortAdminPointToPoint = RST_P2P_FORCEFALSE;
    }
    if (i4SetValIeee8021BridgeBasePortAdminPointToPoint == IEEE802ap_AUTO)
    {
        StpConfigInfo.i4PortAdminPointToPoint = RST_P2P_AUTO;
    }

    if ((AstApiStpConfigInfo (&StpConfigInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBasePortIfIndex
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgeBasePortIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBasePortIfIndex (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        i4TestValIeee8021BridgeBasePortIfIndex)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgeBasePortIfIndex > VLAN_MAX_PORTS_IN_SYSTEM) ||
        (i4TestValIeee8021BridgeBasePortIfIndex == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort);
    if (pBasePortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBasePortAdminPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgeBasePortAdminPointToPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBasePortAdminPointToPoint (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  INT4
                                                  i4TestValIeee8021BridgeBasePortAdminPointToPoint)
{
    tStpConfigInfo      StpConfigInfo;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    MEMSET (&StpConfigInfo, 0, sizeof (tStpConfigInfo));
    StpConfigInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    StpConfigInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    StpConfigInfo.u1InfoType = STP_ADMIN_POINT_POINT_STATUS_TEST;
    StpConfigInfo.pu4ErrorCode = &u4ErrCode;
    if (i4TestValIeee8021BridgeBasePortAdminPointToPoint == IEEE802ap_FORCETRUE)
    {
        StpConfigInfo.i4PortAdminPointToPoint = RST_P2P_FORCETRUE;
    }
    else if (i4TestValIeee8021BridgeBasePortAdminPointToPoint ==
             IEEE802ap_FORCEFALSE)
    {
        StpConfigInfo.i4PortAdminPointToPoint = RST_P2P_FORCEFALSE;
    }
    else if (i4TestValIeee8021BridgeBasePortAdminPointToPoint == IEEE802ap_AUTO)
    {
        StpConfigInfo.i4PortAdminPointToPoint = RST_P2P_AUTO;
    }
    else
    {
        *pu4ErrorCode = *(StpConfigInfo.pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((AstApiStpConfigInfo (&StpConfigInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = *(StpConfigInfo.pu4ErrorCode);
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeBasePortTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeTpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeTpPortTable
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeTpPortTable (UINT4
                                                   u4Ieee8021BridgeTpPortComponentId,
                                                   UINT4 u4Ieee8021BridgeTpPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeTpPortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeTpPortComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhValidateIndexInstanceDot1dTpPortTable (u4Ieee8021BridgeTpPort);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeTpPortTable
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeTpPortTable (UINT4
                                           *pu4Ieee8021BridgeTpPortComponentId,
                                           UINT4 *pu4Ieee8021BridgeTpPort)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021BridgeTpPortComponentId = u4ContextId;

        if (nmhGetFirstIndexDot1dTpPortTable ((INT4 *) pu4Ieee8021BridgeTpPort)
            == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
            *pu4Ieee8021BridgeTpPortComponentId = u4ContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext (*pu4Ieee8021BridgeTpPortComponentId,
                                     &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeTpPortTable
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                nextIeee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort
                nextIeee8021BridgeTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeTpPortTable (UINT4
                                          u4Ieee8021BridgeTpPortComponentId,
                                          UINT4
                                          *pu4NextIeee8021BridgeTpPortComponentId,
                                          UINT4 u4Ieee8021BridgeTpPort,
                                          UINT4 *pu4NextIeee8021BridgeTpPort)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeTpPortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeTpPortComponentId,
                                  u4Ieee8021BridgeTpPort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1dTpPortTable ((INT4) u4Ieee8021BridgeTpPort,
                                         (INT4 *)
                                         pu4NextIeee8021BridgeTpPort) ==
        SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeTpPort,
             pu4NextIeee8021BridgeTpPortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeTpPortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortMaxInfo
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortMaxInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortMaxInfo (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                   UINT4 u4Ieee8021BridgeTpPort,
                                   INT4 *pi4RetValIeee8021BridgeTpPortMaxInfo)
{
/* The BridgeTpPortComponentId is not needed in this table because BridgeTpPort itself provides required attributes.The validation of BridgeTpPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);

    return (nmhGetDot1dTpPortMaxInfo ((INT4) u4Ieee8021BridgeTpPort,
                                      pi4RetValIeee8021BridgeTpPortMaxInfo));
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortInFrames
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortInFrames (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                    UINT4 u4Ieee8021BridgeTpPort,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValIeee8021BridgeTpPortInFrames)
{
    UINT4              *pu4lsn = NULL;

/* The BridgeTpPortComponentId is not needed in this table because BridgeTpPort itself provides required attributes.The validation of BridgeTpPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);

    pu8RetValIeee8021BridgeTpPortInFrames->lsn = 0;
    pu8RetValIeee8021BridgeTpPortInFrames->msn = 0;
    pu4lsn = &(pu8RetValIeee8021BridgeTpPortInFrames->lsn);
    if ((nmhGetDot1dTpPortInFrames ((INT4) u4Ieee8021BridgeTpPort,
                                    pu4lsn)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pu8RetValIeee8021BridgeTpPortInFrames->lsn = *pu4lsn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortOutFrames
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortOutFrames (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                     UINT4 u4Ieee8021BridgeTpPort,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValIeee8021BridgeTpPortOutFrames)
{
    UINT4              *pu4lsn = NULL;

/* The BridgeTpPortComponentId is not needed in this table because BridgeTpPort itself provides required attributes.The validation of BridgeTpPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);

    pu8RetValIeee8021BridgeTpPortOutFrames->lsn = 0;
    pu8RetValIeee8021BridgeTpPortOutFrames->msn = 0;
    pu4lsn = &(pu8RetValIeee8021BridgeTpPortOutFrames->lsn);
    if ((nmhGetDot1dTpPortOutFrames ((INT4) u4Ieee8021BridgeTpPort,
                                     pu4lsn)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pu8RetValIeee8021BridgeTpPortOutFrames->lsn = *pu4lsn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortInDiscards
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortInDiscards (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                      UINT4 u4Ieee8021BridgeTpPort,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValIeee8021BridgeTpPortInDiscards)
{
    UINT4              *pu4lsn = NULL;

/* The BridgeTpPortComponentId is not needed in this table because BridgeTpPort itself provides required attributes.The validation of BridgeTpPortComponentId is done in the validate routine.*/
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeTpPortComponentId);

    pu8RetValIeee8021BridgeTpPortInDiscards->lsn = 0;
    pu8RetValIeee8021BridgeTpPortInDiscards->msn = 0;
    pu4lsn = &(pu8RetValIeee8021BridgeTpPortInDiscards->lsn);
    if (VlanSelectContext (u4Ieee8021BridgeTpPortComponentId) == VLAN_SUCCESS)
    {
        if ((nmhGetDot1dTpPortInDiscards ((INT4) u4Ieee8021BridgeTpPort,
                                          pu4lsn)) == SNMP_FAILURE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        pu8RetValIeee8021BridgeTpPortInDiscards->lsn = *pu4lsn;
        VlanReleaseContext ();
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortPriorityTable (UINT4
                                                         u4Ieee8021BridgeBasePortComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeBasePort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsDot1dPortPriorityTable
        (u4Ieee8021BridgeBasePort);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortPriorityTable (UINT4
                                                 *pu4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgeBasePort)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;

        if (nmhGetFirstIndexFsDot1dPortPriorityTable
            ((INT4 *) pu4Ieee8021BridgeBasePort) == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
            *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021BridgeBasePortComponentId,
            &u4ContextId) == VLAN_SUCCESS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexIeee8021BridgePortPriorityTable
    (UINT4
     u4Ieee8021BridgeBasePortComponentId,
     UINT4
     *pu4NextIeee8021BridgeBasePortComponentId,
     UINT4 u4Ieee8021BridgeBasePort, UINT4 *pu4NextIeee8021BridgeBasePort)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexFsDot1dPortPriorityTable
        ((INT4) u4Ieee8021BridgeBasePort,
         (INT4 *) pu4NextIeee8021BridgeBasePort) == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortDefaultUserPriority (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4RetValIeee8021BridgePortDefaultUserPriority)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1dPortDefaultUserPriority ((INT4)
                                                     u4Ieee8021BridgeBasePort,
                                                     (INT4 *)
                                                     pu4RetValIeee8021BridgePortDefaultUserPriority);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortNumTrafficClasses
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortNumTrafficClasses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortNumTrafficClasses (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021BridgePortNumTrafficClasses)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1dPortNumTrafficClasses ((INT4)
                                                   u4Ieee8021BridgeBasePort,
                                                   pi4RetValIeee8021BridgePortNumTrafficClasses);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortPriorityCodePointSelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortPriorityCodePointSelection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortPriorityCodePointSelection (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort,
                                                    INT4
                                                    *pi4RetValIeee8021BridgePortPriorityCodePointSelection)
{
#ifdef PB_WANTED
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/

    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((u4Ieee8021BridgeBasePortComponentId - 1)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        i1RetVal =
            nmhGetDot1adMIPortPcpSelectionRow ((INT4) u4Ieee8021BridgeBasePort,
                                               pi4RetValIeee8021BridgePortPriorityCodePointSelection);
    }
    VlanReleaseContext ();
    return i1RetVal;

#else
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortPriorityCodePointSelection);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortUseDEI
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortUseDEI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortUseDEI (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 *pi4RetValIeee8021BridgePortUseDEI)
{
#ifdef PB_WANTED
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((u4Ieee8021BridgeBasePortComponentId - 1)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        i1RetVal = nmhGetDot1adMIPortUseDei ((INT4) u4Ieee8021BridgeBasePort,
                                             pi4RetValIeee8021BridgePortUseDEI);
    }
    VlanReleaseContext ();
    return i1RetVal;
#else
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortUseDEI);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortRequireDropEncoding
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortRequireDropEncoding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortRequireDropEncoding (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             *pi4RetValIeee8021BridgePortRequireDropEncoding)
{
#ifdef PB_WANTED
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the validate routine.*/
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((u4Ieee8021BridgeBasePortComponentId - 1)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        i1RetVal =
            nmhGetDot1adMIPortReqDropEncoding ((INT4) u4Ieee8021BridgeBasePort,
                                               pi4RetValIeee8021BridgePortRequireDropEncoding);
    }
    VlanReleaseContext ();
    return i1RetVal;
#else
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortRequireDropEncoding);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortServiceAccessPrioritySelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortServiceAccessPrioritySelection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortServiceAccessPrioritySelection (UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        *pi4RetValIeee8021BridgePortServiceAccessPrioritySelection)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortServiceAccessPrioritySelection);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortDefaultUserPriority (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             u4SetValIeee8021BridgePortDefaultUserPriority)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetFsDot1dPortDefaultUserPriority
            ((INT4) u4Ieee8021BridgeBasePort,
             (INT4) u4SetValIeee8021BridgePortDefaultUserPriority));
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortNumTrafficClasses
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortNumTrafficClasses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortNumTrafficClasses (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021BridgePortNumTrafficClasses)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetFsDot1dPortNumTrafficClasses ((INT4) u4Ieee8021BridgeBasePort,
                                                i4SetValIeee8021BridgePortNumTrafficClasses));
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortPriorityCodePointSelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortPriorityCodePointSelection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortPriorityCodePointSelection (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort,
                                                    INT4
                                                    i4SetValIeee8021BridgePortPriorityCodePointSelection)
{
#ifdef PB_WANTED

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetDot1adMIPortPcpSelectionRow ((INT4) u4Ieee8021BridgeBasePort,
                                               i4SetValIeee8021BridgePortPriorityCodePointSelection));
#else
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortPriorityCodePointSelection);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortUseDEI
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortUseDEI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortUseDEI (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 i4SetValIeee8021BridgePortUseDEI)
{
#ifdef PB_WANTED
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetDot1adMIPortReqDropEncoding ((INT4) u4Ieee8021BridgeBasePort,
                                               i4SetValIeee8021BridgePortUseDEI));
#else
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortUseDEI);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortRequireDropEncoding
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortRequireDropEncoding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortRequireDropEncoding (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4SetValIeee8021BridgePortRequireDropEncoding)
{
#ifdef PB_WANTED
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetDot1adMIPortReqDropEncoding ((INT4) u4Ieee8021BridgeBasePort,
                                               i4SetValIeee8021BridgePortRequireDropEncoding));
#else
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortRequireDropEncoding);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortServiceAccessPrioritySelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortServiceAccessPrioritySelection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortServiceAccessPrioritySelection (UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4SetValIeee8021BridgePortServiceAccessPrioritySelection)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortServiceAccessPrioritySelection);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortDefaultUserPriority (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                u4TestValIeee8021BridgePortDefaultUserPriority)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsDot1dPortDefaultUserPriority (pu4ErrorCode,
                                                     (INT4)
                                                     u4Ieee8021BridgeBasePort,
                                                     (INT4)
                                                     u4TestValIeee8021BridgePortDefaultUserPriority));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortNumTrafficClasses
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortNumTrafficClasses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortNumTrafficClasses (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021BridgePortNumTrafficClasses)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsDot1dPortNumTrafficClasses (pu4ErrorCode,
                                                   (INT4)
                                                   u4Ieee8021BridgeBasePort,
                                                   i4TestValIeee8021BridgePortNumTrafficClasses));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortPriorityCodePointSelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortPriorityCodePointSelection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortPriorityCodePointSelection (UINT4 *pu4ErrorCode,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       INT4
                                                       i4TestValIeee8021BridgePortPriorityCodePointSelection)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMIPortPcpSelectionRow (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021BridgeBasePort,
                                                  i4TestValIeee8021BridgePortPriorityCodePointSelection));
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortPriorityCodePointSelection);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortUseDEI
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortUseDEI
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortUseDEI (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   INT4 i4TestValIeee8021BridgePortUseDEI)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMIPortUseDei (pu4ErrorCode,
                                         (INT4) u4Ieee8021BridgeBasePort,
                                         i4TestValIeee8021BridgePortUseDEI));
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortUseDEI);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortRequireDropEncoding
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortRequireDropEncoding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortRequireDropEncoding (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4
                                                i4TestValIeee8021BridgePortRequireDropEncoding)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMIPortReqDropEncoding (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021BridgeBasePort,
                                                  i4TestValIeee8021BridgePortRequireDropEncoding));
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortRequireDropEncoding);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortServiceAccessPrioritySelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortServiceAccessPrioritySelection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortServiceAccessPrioritySelection (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021BridgeBasePortComponentId,
                                                           UINT4
                                                           u4Ieee8021BridgeBasePort,
                                                           INT4
                                                           i4TestValIeee8021BridgePortServiceAccessPrioritySelection)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortServiceAccessPrioritySelection);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortPriorityTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeUserPriorityRegenTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeUserPriorityRegenTable (UINT4
                                                              u4Ieee8021BridgeBasePortComponentId,
                                                              UINT4
                                                              u4Ieee8021BridgeBasePort,
                                                              UINT4
                                                              u4Ieee8021BridgeUserPriority)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return ((nmhValidateIndexInstanceFsDot1dUserPriorityRegenTable ((INT4)
                                                                    u4Ieee8021BridgeBasePort,
                                                                    u4Ieee8021BridgeUserPriority)));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeUserPriorityRegenTable (UINT4
                                                      *pu4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      *pu4Ieee8021BridgeBasePort,
                                                      UINT4
                                                      *pu4Ieee8021BridgeUserPriority)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;

        if (nmhGetFirstIndexFsDot1dUserPriorityRegenTable ((INT4 *)
                                                           pu4Ieee8021BridgeBasePort,
                                                           (INT4 *)
                                                           pu4Ieee8021BridgeUserPriority)
            == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
            *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021BridgeBasePortComponentId,
            &u4ContextId) == VLAN_SUCCESS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021BridgeUserPriority
                nextIeee8021BridgeUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeUserPriorityRegenTable (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     *pu4NextIeee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     UINT4
                                                     *pu4NextIeee8021BridgeBasePort,
                                                     UINT4
                                                     u4Ieee8021BridgeUserPriority,
                                                     UINT4
                                                     *pu4NextIeee8021BridgeUserPriority)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetNextIndexFsDot1dUserPriorityRegenTable ((INT4)
                                                      u4Ieee8021BridgeBasePort,
                                                      (INT4 *)
                                                      pu4NextIeee8021BridgeBasePort,
                                                      (INT4)
                                                      u4Ieee8021BridgeUserPriority,
                                                      (INT4 *)
                                                      pu4NextIeee8021BridgeUserPriority)
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeRegenUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority

                The Object 
                retValIeee8021BridgeRegenUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeRegenUserPriority (UINT4
                                       u4Ieee8021BridgeBasePortComponentId,
                                       UINT4 u4Ieee8021BridgeBasePort,
                                       UINT4 u4Ieee8021BridgeUserPriority,
                                       UINT4
                                       *pu4RetValIeee8021BridgeRegenUserPriority)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhGetFsDot1dRegenUserPriority ((INT4) u4Ieee8021BridgeBasePort,
                                            (INT4) u4Ieee8021BridgeUserPriority,
                                            (INT4 *)
                                            pu4RetValIeee8021BridgeRegenUserPriority));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeRegenUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority

                The Object 
                setValIeee8021BridgeRegenUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeRegenUserPriority (UINT4
                                       u4Ieee8021BridgeBasePortComponentId,
                                       UINT4 u4Ieee8021BridgeBasePort,
                                       UINT4 u4Ieee8021BridgeUserPriority,
                                       UINT4
                                       u4SetValIeee8021BridgeRegenUserPriority)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetFsDot1dRegenUserPriority ((INT4) u4Ieee8021BridgeBasePort,
                                            (INT4) u4Ieee8021BridgeUserPriority,
                                            (INT4)
                                            u4SetValIeee8021BridgeRegenUserPriority));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeRegenUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority

                The Object 
                testValIeee8021BridgeRegenUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeRegenUserPriority (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          UINT4 u4Ieee8021BridgeUserPriority,
                                          UINT4
                                          u4TestValIeee8021BridgeRegenUserPriority)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsDot1dRegenUserPriority (pu4ErrorCode,
                                               (INT4) u4Ieee8021BridgeBasePort,
                                               (INT4)
                                               u4Ieee8021BridgeUserPriority,
                                               (INT4)
                                               u4TestValIeee8021BridgeRegenUserPriority));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeUserPriorityRegenTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeTrafficClassTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeTrafficClassTable (UINT4
                                                         u4Ieee8021BridgeBasePortComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeBasePort,
                                                         UINT4
                                                         u4Ieee8021BridgeTrafficClassPriority)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return ((nmhValidateIndexInstanceFsDot1dTrafficClassTable ((INT4)
                                                               u4Ieee8021BridgeBasePort,
                                                               u4Ieee8021BridgeTrafficClassPriority)));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeTrafficClassTable (UINT4
                                                 *pu4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgeBasePort,
                                                 UINT4
                                                 *pu4Ieee8021BridgeTrafficClassPriority)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;

        if (nmhGetFirstIndexFsDot1dTrafficClassTable ((INT4 *)
                                                      pu4Ieee8021BridgeBasePort,
                                                      (INT4 *)
                                                      pu4Ieee8021BridgeTrafficClassPriority)
            == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
            *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021BridgeBasePortComponentId,
            &u4ContextId) == VLAN_SUCCESS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
                nextIeee8021BridgeTrafficClassPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeTrafficClassTable (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePort,
                                                UINT4
                                                u4Ieee8021BridgeTrafficClassPriority,
                                                UINT4
                                                *pu4NextIeee8021BridgeTrafficClassPriority)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetNextIndexFsDot1dTrafficClassTable ((INT4)
                                                 u4Ieee8021BridgeBasePort,
                                                 (INT4 *)
                                                 pu4NextIeee8021BridgeBasePort,
                                                 (INT4)
                                                 u4Ieee8021BridgeTrafficClassPriority,
                                                 (INT4 *)
                                                 pu4NextIeee8021BridgeTrafficClassPriority)
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTrafficClass
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority

                The Object 
                retValIeee8021BridgeTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTrafficClass (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  UINT4 u4Ieee8021BridgeTrafficClassPriority,
                                  INT4 *pi4RetValIeee8021BridgeTrafficClass)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate  routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhGetFsDot1dTrafficClass ((INT4) u4Ieee8021BridgeBasePort,
                                       (INT4)
                                       u4Ieee8021BridgeTrafficClassPriority,
                                       pi4RetValIeee8021BridgeTrafficClass));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeTrafficClass
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority

                The Object 
                setValIeee8021BridgeTrafficClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeTrafficClass (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  UINT4 u4Ieee8021BridgeTrafficClassPriority,
                                  INT4 i4SetValIeee8021BridgeTrafficClass)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhSetFsDot1dTrafficClass ((INT4) u4Ieee8021BridgeBasePort,
                                       (INT4)
                                       u4Ieee8021BridgeTrafficClassPriority,
                                       i4SetValIeee8021BridgeTrafficClass));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeTrafficClass
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority

                The Object 
                testValIeee8021BridgeTrafficClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeTrafficClass (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     UINT4 u4Ieee8021BridgeTrafficClassPriority,
                                     INT4 i4TestValIeee8021BridgeTrafficClass)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsDot1dTrafficClass (pu4ErrorCode,
                                          (INT4) u4Ieee8021BridgeBasePort,
                                          (INT4)
                                          u4Ieee8021BridgeTrafficClassPriority,
                                          i4TestValIeee8021BridgeTrafficClass));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeTrafficClassTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortOutboundAccessPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortOutboundAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortOutboundAccessPriorityTable (UINT4
                                                                       u4Ieee8021BridgeBasePortComponentId,
                                                                       UINT4
                                                                       u4Ieee8021BridgeBasePort,
                                                                       UINT4
                                                                       u4Ieee8021BridgeRegenUserPriority)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhValidateIndexInstanceFsDot1dPortOutboundAccessPriorityTable
            ((INT4) u4Ieee8021BridgeBasePort,
             (INT4) u4Ieee8021BridgeRegenUserPriority));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortOutboundAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortOutboundAccessPriorityTable (UINT4
                                                               *pu4Ieee8021BridgeBasePortComponentId,
                                                               UINT4
                                                               *pu4Ieee8021BridgeBasePort,
                                                               UINT4
                                                               *pu4Ieee8021BridgeRegenUserPriority)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;

        if (nmhGetFirstIndexFsDot1dPortOutboundAccessPriorityTable ((INT4 *)
                                                                    pu4Ieee8021BridgeBasePort,
                                                                    (INT4 *)
                                                                    pu4Ieee8021BridgeRegenUserPriority)
            == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
            *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021BridgeBasePortComponentId,
            &u4ContextId) == VLAN_SUCCESS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortOutboundAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority
                nextIeee8021BridgeRegenUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortOutboundAccessPriorityTable (UINT4
                                                              u4Ieee8021BridgeBasePortComponentId,
                                                              UINT4
                                                              *pu4NextIeee8021BridgeBasePortComponentId,
                                                              UINT4
                                                              u4Ieee8021BridgeBasePort,
                                                              UINT4
                                                              *pu4NextIeee8021BridgeBasePort,
                                                              UINT4
                                                              u4Ieee8021BridgeRegenUserPriority,
                                                              UINT4
                                                              *pu4NextIeee8021BridgeRegenUserPriority)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexFsDot1dPortOutboundAccessPriorityTable ((INT4)
                                                               u4Ieee8021BridgeBasePort,
                                                               (INT4 *)
                                                               pu4NextIeee8021BridgeBasePort,
                                                               (INT4)
                                                               u4Ieee8021BridgeRegenUserPriority,
                                                               (INT4 *)
                                                               pu4NextIeee8021BridgeRegenUserPriority)
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortOutboundAccessPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority

                The Object 
                retValIeee8021BridgePortOutboundAccessPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortOutboundAccessPriority (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                u4Ieee8021BridgeRegenUserPriority,
                                                UINT4
                                                *pu4RetValIeee8021BridgePortOutboundAccessPriority)
{
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    return (nmhGetFsDot1dPortOutboundAccessPriority ((INT4)
                                                     u4Ieee8021BridgeBasePort,
                                                     (INT4)
                                                     u4Ieee8021BridgeRegenUserPriority,
                                                     (INT4 *)
                                                     pu4RetValIeee8021BridgePortOutboundAccessPriority));
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortDecodingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortDecodingTable (UINT4
                                                         u4Ieee8021BridgePortDecodingComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgePortDecodingPortNum,
                                                         INT4
                                                         i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                         INT4
                                                         i4Ieee8021BridgePortDecodingPriorityCodePoint)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortDecodingComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgePortDecodingComponentId,
                                  u4Ieee8021BridgePortDecodingPortNum)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhValidateIndexInstanceDot1adMIPcpDecodingTable ((INT4)
                                                              u4Ieee8021BridgePortDecodingPortNum,
                                                              (INT4)
                                                              i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                              (INT4)
                                                              i4Ieee8021BridgePortDecodingPriorityCodePoint));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortDecodingTable (UINT4
                                                 *pu4Ieee8021BridgePortDecodingComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgePortDecodingPortNum,
                                                 INT4
                                                 *pi4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                 INT4
                                                 *pi4Ieee8021BridgePortDecodingPriorityCodePoint)
{
#ifdef PB_WANTED
    UINT2               u2LocalPortId = 0;

    if ((nmhGetFirstIndexDot1adMIPcpDecodingTable ((INT4 *)
                                                   pu4Ieee8021BridgePortDecodingPortNum,
                                                   pi4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                   pi4Ieee8021BridgePortDecodingPriorityCodePoint))
        == SNMP_SUCCESS)
    {
        if ((VlanGetContextInfoFromIfIndex
             (*pu4Ieee8021BridgePortDecodingPortNum,
              pu4Ieee8021BridgePortDecodingComponentId,
              &u2LocalPortId)) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgePortDecodingComponentId));
            return SNMP_SUCCESS;
        }

    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pu4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (pi4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (pi4Ieee8021BridgePortDecodingPriorityCodePoint);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                nextIeee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                nextIeee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                nextIeee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
                nextIeee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortDecodingTable (UINT4
                                                u4Ieee8021BridgePortDecodingComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgePortDecodingComponentId,
                                                UINT4
                                                u4Ieee8021BridgePortDecodingPortNum,
                                                UINT4
                                                *pu4NextIeee8021BridgePortDecodingPortNum,
                                                INT4
                                                i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                INT4
                                                *pi4NextIeee8021BridgePortDecodingPriorityCodePointRow,
                                                INT4
                                                i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                INT4
                                                *pi4NextIeee8021BridgePortDecodingPriorityCodePoint)
{
#ifdef PB_WANTED

    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortDecodingComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgePortDecodingComponentId,
                                  u4Ieee8021BridgePortDecodingPortNum)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexDot1adMIPcpDecodingTable ((INT4)
                                                  u4Ieee8021BridgePortDecodingPortNum,
                                                  (INT4 *)
                                                  pu4NextIeee8021BridgePortDecodingPortNum,
                                                  i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                  pi4NextIeee8021BridgePortDecodingPriorityCodePointRow,
                                                  i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                  pi4NextIeee8021BridgePortDecodingPriorityCodePoint))
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgePortDecodingPortNum,
             pu4NextIeee8021BridgePortDecodingComponentId,
             &u2LocalPortId) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgePortDecodingComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (pu4NextIeee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (pi4NextIeee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (pi4NextIeee8021BridgePortDecodingPriorityCodePoint);
    return SNMP_FAILURE;

#endif

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortDecodingPriority
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                retValIeee8021BridgePortDecodingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortDecodingPriority (UINT4
                                          u4Ieee8021BridgePortDecodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortDecodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                          UINT4
                                          *pu4RetValIeee8021BridgePortDecodingPriority)
{
#ifdef PB_WANTED
/* The BridgePortDecodingComponentId is not needed in this table because BridgePortDecodingPortNum itself provides required attributes.The validation of BridgePortDecodingComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);

    return (nmhGetDot1adMIPcpDecodingPriority ((INT4)
                                               u4Ieee8021BridgePortDecodingPortNum,
                                               i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                               i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                               (INT4 *)
                                               pu4RetValIeee8021BridgePortDecodingPriority));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (pu4RetValIeee8021BridgePortDecodingPriority);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortDecodingDropEligible
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                retValIeee8021BridgePortDecodingDropEligible
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortDecodingDropEligible (UINT4
                                              u4Ieee8021BridgePortDecodingComponentId,
                                              UINT4
                                              u4Ieee8021BridgePortDecodingPortNum,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                              INT4
                                              *pi4RetValIeee8021BridgePortDecodingDropEligible)
{
#ifdef PB_WANTED
/* The BridgePortDecodingComponentId is not needed in this table because BridgePortDecodingPortNum itself provides required attributes.The validation of BridgePortDecodingComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);

    return (nmhGetDot1adMIPcpDecodingDropEligible ((INT4)
                                                   u4Ieee8021BridgePortDecodingPortNum,
                                                   i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                   i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                   pi4RetValIeee8021BridgePortDecodingDropEligible));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortDecodingDropEligible);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortDecodingPriority
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                setValIeee8021BridgePortDecodingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortDecodingPriority (UINT4
                                          u4Ieee8021BridgePortDecodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortDecodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                          UINT4
                                          u4SetValIeee8021BridgePortDecodingPriority)
{
#ifdef PB_WANTED
/* The BridgePortDecodingComponentId is not needed in this table because BridgePortDecodingPortNum itself provides required attributes.The validation of BridgePortDecodingComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);

    return (nmhSetDot1adMIPcpDecodingPriority ((INT4)
                                               u4Ieee8021BridgePortDecodingPortNum,
                                               i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                               i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                               (INT4)
                                               u4SetValIeee8021BridgePortDecodingPriority));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (u4SetValIeee8021BridgePortDecodingPriority);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortDecodingDropEligible
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                setValIeee8021BridgePortDecodingDropEligible
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortDecodingDropEligible (UINT4
                                              u4Ieee8021BridgePortDecodingComponentId,
                                              UINT4
                                              u4Ieee8021BridgePortDecodingPortNum,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                              INT4
                                              i4SetValIeee8021BridgePortDecodingDropEligible)
{
#ifdef PB_WANTED
/* The BridgePortDecodingComponentId is not needed in this table because BridgePortDecodingPortNum itself provides required attributes.The validation of BridgePortDecodingComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);

    return (nmhSetDot1adMIPcpDecodingDropEligible ((INT4)
                                                   u4Ieee8021BridgePortDecodingPortNum,
                                                   i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                   i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                   i4SetValIeee8021BridgePortDecodingDropEligible));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (i4SetValIeee8021BridgePortDecodingDropEligible);
    return SNMP_SUCCESS;

#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortDecodingPriority
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                testValIeee8021BridgePortDecodingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortDecodingPriority (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021BridgePortDecodingComponentId,
                                             UINT4
                                             u4Ieee8021BridgePortDecodingPortNum,
                                             INT4
                                             i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                             INT4
                                             i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                             UINT4
                                             u4TestValIeee8021BridgePortDecodingPriority)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortDecodingComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgePortDecodingComponentId,
                                  u4Ieee8021BridgePortDecodingPortNum)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMIPcpDecodingPriority (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021BridgePortDecodingPortNum,
                                                  i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                  i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                  (INT4)
                                                  u4TestValIeee8021BridgePortDecodingPriority));
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (u4TestValIeee8021BridgePortDecodingPriority);
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortDecodingDropEligible
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                testValIeee8021BridgePortDecodingDropEligible
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortDecodingDropEligible (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021BridgePortDecodingComponentId,
                                                 UINT4
                                                 u4Ieee8021BridgePortDecodingPortNum,
                                                 INT4
                                                 i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                 INT4
                                                 i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                 INT4
                                                 i4TestValIeee8021BridgePortDecodingDropEligible)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortDecodingComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgePortDecodingComponentId,
                                  u4Ieee8021BridgePortDecodingPortNum)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMIPcpDecodingDropEligible (pu4ErrorCode,
                                                      (INT4)
                                                      u4Ieee8021BridgePortDecodingPortNum,
                                                      i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                      i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                      i4TestValIeee8021BridgePortDecodingDropEligible));
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (i4TestValIeee8021BridgePortDecodingDropEligible);
    return SNMP_SUCCESS;

#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortDecodingTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortEncodingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortEncodingTable (UINT4
                                                         u4Ieee8021BridgePortEncodingComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgePortEncodingPortNum,
                                                         INT4
                                                         i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                         INT4
                                                         i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                         INT4
                                                         i4Ieee8021BridgePortEncodingDropEligible)
{
#ifdef PB_WANTED

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortEncodingComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgePortEncodingComponentId,
                                  u4Ieee8021BridgePortEncodingPortNum))
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceDot1adMIPcpEncodingTable
            (u4Ieee8021BridgePortEncodingPortNum,
             i4Ieee8021BridgePortEncodingPriorityCodePointRow,
             i4Ieee8021BridgePortEncodingPriorityCodePoint,
             i4Ieee8021BridgePortEncodingDropEligible));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortEncodingTable (UINT4
                                                 *pu4Ieee8021BridgePortEncodingComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgePortEncodingPortNum,
                                                 INT4
                                                 *pi4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                 INT4
                                                 *pi4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                 INT4
                                                 *pi4Ieee8021BridgePortEncodingDropEligible)
{
#ifdef PB_WANTED
    UINT2               u2LocalPortId = 0;

    if ((nmhGetFirstIndexDot1adMIPcpEncodingTable ((INT4 *)
                                                   pu4Ieee8021BridgePortEncodingPortNum,
                                                   pi4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                   pi4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                   pi4Ieee8021BridgePortEncodingDropEligible))
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4Ieee8021BridgePortEncodingPortNum,
             pu4Ieee8021BridgePortEncodingComponentId,
             &u2LocalPortId) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgePortEncodingComponentId));
            return SNMP_SUCCESS;
        }

    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pu4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (pi4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (pi4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (pi4Ieee8021BridgePortEncodingDropEligible);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                nextIeee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                nextIeee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                nextIeee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                nextIeee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
                nextIeee8021BridgePortEncodingDropEligible
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortEncodingTable (UINT4
                                                u4Ieee8021BridgePortEncodingComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgePortEncodingComponentId,
                                                UINT4
                                                u4Ieee8021BridgePortEncodingPortNum,
                                                UINT4
                                                *pu4NextIeee8021BridgePortEncodingPortNum,
                                                INT4
                                                i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                INT4
                                                *pi4NextIeee8021BridgePortEncodingPriorityCodePointRow,
                                                INT4
                                                i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                INT4
                                                *pi4NextIeee8021BridgePortEncodingPriorityCodePoint,
                                                INT4
                                                i4Ieee8021BridgePortEncodingDropEligible,
                                                INT4
                                                *pi4NextIeee8021BridgePortEncodingDropEligible)
{
#ifdef PB_WANTED
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortEncodingComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgePortEncodingComponentId,
                                  u4Ieee8021BridgePortEncodingPortNum))
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexDot1adMIPcpEncodingTable ((INT4)
                                                  u4Ieee8021BridgePortEncodingPortNum,
                                                  (INT4 *)
                                                  pu4NextIeee8021BridgePortEncodingPortNum,
                                                  i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                  pi4NextIeee8021BridgePortEncodingPriorityCodePointRow,
                                                  i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                  pi4NextIeee8021BridgePortEncodingPriorityCodePoint,
                                                  i4Ieee8021BridgePortEncodingDropEligible,
                                                  pi4NextIeee8021BridgePortEncodingDropEligible))
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgePortEncodingPortNum,
             pu4NextIeee8021BridgePortEncodingComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgePortEncodingComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (pu4NextIeee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (pi4NextIeee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (pi4NextIeee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (pi4NextIeee8021BridgePortEncodingDropEligible);
    return SNMP_FAILURE;

#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortEncodingPriority
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible

                The Object 
                retValIeee8021BridgePortEncodingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortEncodingPriority (UINT4
                                          u4Ieee8021BridgePortEncodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortEncodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                          INT4
                                          i4Ieee8021BridgePortEncodingDropEligible,
                                          UINT4
                                          *pu4RetValIeee8021BridgePortEncodingPriority)
{
#ifdef PB_WANTED

/* The BridgePortEncodingComponentId is not needed in this table because BridgePortEncodingPortNum itself provides required attributes.The validation of BridgePortEncodingComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);

    return (nmhGetDot1adMIPcpEncodingPcpValue ((INT4)
                                               u4Ieee8021BridgePortEncodingPortNum,
                                               i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                               i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                               i4Ieee8021BridgePortEncodingDropEligible,
                                               (INT4 *)
                                               pu4RetValIeee8021BridgePortEncodingPriority));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (pu4RetValIeee8021BridgePortEncodingPriority);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortEncodingPriority
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible

                The Object 
                setValIeee8021BridgePortEncodingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortEncodingPriority (UINT4
                                          u4Ieee8021BridgePortEncodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortEncodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                          INT4
                                          i4Ieee8021BridgePortEncodingDropEligible,
                                          UINT4
                                          u4SetValIeee8021BridgePortEncodingPriority)
{
#ifdef PB_WANTED
/* The BridgePortEncodingComponentId is not needed in this table because BridgePortEncodingPortNum itself provides required attributes.The validation of BridgePortEncodingComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);

    return (nmhSetDot1adMIPcpEncodingPcpValue ((INT4)
                                               u4Ieee8021BridgePortEncodingPortNum,
                                               i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                               i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                               i4Ieee8021BridgePortEncodingDropEligible,
                                               (INT4)
                                               u4SetValIeee8021BridgePortEncodingPriority));
#else
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (u4SetValIeee8021BridgePortEncodingPriority);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortEncodingPriority
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible

                The Object 
                testValIeee8021BridgePortEncodingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortEncodingPriority (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021BridgePortEncodingComponentId,
                                             UINT4
                                             u4Ieee8021BridgePortEncodingPortNum,
                                             INT4
                                             i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                             INT4
                                             i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                             INT4
                                             i4Ieee8021BridgePortEncodingDropEligible,
                                             UINT4
                                             u4TestValIeee8021BridgePortEncodingPriority)
{
#ifdef PB_WANTED
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgePortEncodingComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgePortEncodingComponentId,
                                  u4Ieee8021BridgePortEncodingPortNum)) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2Dot1adMIPcpEncodingPcpValue (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021BridgePortEncodingPortNum,
                                                  i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                  i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                  i4Ieee8021BridgePortEncodingDropEligible,
                                                  (INT4)
                                                  u4TestValIeee8021BridgePortEncodingPriority));
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (u4TestValIeee8021BridgePortEncodingPriority);
    return SNMP_SUCCESS;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortEncodingTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeServiceAccessPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeServiceAccessPriorityTable (UINT4
                                                                  u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                                  UINT4
                                                                  u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                                  UINT4
                                                                  u4Ieee8021BridgeServiceAccessPriorityReceived)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeServiceAccessPriorityTable (UINT4
                                                          *pu4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                          UINT4
                                                          *pu4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                          UINT4
                                                          *pu4Ieee8021BridgeServiceAccessPriorityReceived)
{
    UNUSED_PARAM (pu4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (pu4Ieee8021BridgeServiceAccessPriorityReceived);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                nextIeee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                nextIeee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
                nextIeee8021BridgeServiceAccessPriorityReceived
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeServiceAccessPriorityTable (UINT4
                                                         u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                         UINT4
                                                         *pu4NextIeee8021BridgeServiceAccessPriorityComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                         UINT4
                                                         *pu4NextIeee8021BridgeServiceAccessPriorityPortNum,
                                                         UINT4
                                                         u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                         UINT4
                                                         *pu4NextIeee8021BridgeServiceAccessPriorityReceived)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (pu4NextIeee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (pu4NextIeee8021BridgeServiceAccessPriorityReceived);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeServiceAccessPriorityValue
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived

                The Object 
                retValIeee8021BridgeServiceAccessPriorityValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeServiceAccessPriorityValue (UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                UINT4
                                                *pu4RetValIeee8021BridgeServiceAccessPriorityValue)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (pu4RetValIeee8021BridgeServiceAccessPriorityValue);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeServiceAccessPriorityValue
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived

                The Object 
                setValIeee8021BridgeServiceAccessPriorityValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeServiceAccessPriorityValue (UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                UINT4
                                                u4SetValIeee8021BridgeServiceAccessPriorityValue)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (u4SetValIeee8021BridgeServiceAccessPriorityValue);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeServiceAccessPriorityValue
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived

                The Object 
                testValIeee8021BridgeServiceAccessPriorityValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeServiceAccessPriorityValue (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                   UINT4
                                                   u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                   UINT4
                                                   u4TestValIeee8021BridgeServiceAccessPriorityValue)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (u4TestValIeee8021BridgeServiceAccessPriorityValue);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeServiceAccessPriorityTable (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeILanIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeILanIfTable (INT4 i4IfIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    u4IfIndex = (UINT4) i4IfIndex;

    /* API provided by CFA to get the interface information */
    if (VlanCfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeILanIfTable (INT4 *pi4IfIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4Index = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* ILAN interfaces can be present in the range between
     * CFA_MIN_ILAN_IF_INDEX to  CFA_MAX_ILAN_IF_INDEX.
     * The maximum ILAN interfaces can be created upto SYS_DEF_MAX_ILAN_IFACES.
     */
    for (u4Index = CFA_MIN_ILAN_IF_INDEX; u4Index <= CFA_MAX_ILAN_IF_INDEX;
         u4Index++)
    {
        /* API provided by CFA to get the interface information */
        if (VlanCfaGetIfInfo (u4Index, &IfInfo) == CFA_SUCCESS)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    *pi4IfIndex = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeILanIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Counter = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    u4IfIndex = (UINT4) i4IfIndex;

    /* Next Index should always return the next used Index */
    for (u4Counter = (u4IfIndex + 1);
         u4Counter <= CFA_MAX_ILAN_IF_INDEX; u4Counter++)
    {
        /* API provided by CFA to validate the CFA entry */
        if (VlanCfaGetIfInfo (u4Counter, &IfInfo) == CFA_SUCCESS)
        {
            *pi4NextIfIndex = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }
    *pi4NextIfIndex = 0;
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeILanIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValIeee8021BridgeILanIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeILanIfRowStatus (INT4 i4IfIndex,
                                     INT4
                                     *pi4RetValIeee8021BridgeILanIfRowStatus)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    u4IfIndex = (UINT4) i4IfIndex;
    /* API provided by CFA to get the interface information */
    if (VlanCfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
    {
        *pi4RetValIeee8021BridgeILanIfRowStatus = IfInfo.i4Active;
        i1RetVal = SNMP_SUCCESS;
    }
    return (i1RetVal);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeILanIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValIeee8021BridgeILanIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeILanIfRowStatus (INT4 i4IfIndex,
                                     INT4 i4SetValIeee8021BridgeILanIfRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    switch (i4SetValIeee8021BridgeILanIfRowStatus)
    {
        case CREATE_AND_GO:
            /* NOT SUPPORTED */
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case CREATE_AND_WAIT:
        case DESTROY:
            VLAN_UNLOCK ();
            i1RetVal = CfaSetIfMainRowStatus
                (i4IfIndex, i4SetValIeee8021BridgeILanIfRowStatus);
            VLAN_LOCK ();
            break;
    }
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeILanIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValIeee8021BridgeILanIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeILanIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4
                                        i4TestValIeee8021BridgeILanIfRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    switch (i4TestValIeee8021BridgeILanIfRowStatus)
    {
        case CREATE_AND_GO:
            /* NOT SUPPORTED */
            break;

        case ACTIVE:
        case NOT_READY:
        case NOT_IN_SERVICE:
        case CREATE_AND_WAIT:
        case DESTROY:
            VLAN_UNLOCK ();
            i1RetVal = CfaTestv2IfMainRowStatus (pu4ErrorCode, i4IfIndex,
                                                 i4TestValIeee8021BridgeILanIfRowStatus);
            VLAN_LOCK ();
            break;
    }
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeILanIfTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeDot1dPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeDot1dPortTable (UINT4
                                                      u4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      u4Ieee8021BridgeBasePort)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u4Ieee8021BridgeBasePort) == VLAN_FALSE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort);

    if (pBasePortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeDot1dPortTable (UINT4
                                              *pu4Ieee8021BridgeBasePortComponentId,
                                              UINT4 *pu4Ieee8021BridgeBasePort)
{
    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanGetNextIndexIEEE8021Dot1dBasePortTable
            (0, pu4Ieee8021BridgeBasePort) == SNMP_SUCCESS)
        {
            *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgeBasePortComponentId));
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
        *pu4Ieee8021BridgeBasePortComponentId = u4ContextId;
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021BridgeBasePortComponentId,
            &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeDot1dPortTable (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePort)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetNextIndexIEEE8021Dot1dBasePortTable
        (u4Ieee8021BridgeBasePort,
         pu4NextIeee8021BridgeBasePort) == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex
            (*pu4NextIeee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePortComponentId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeDot1dPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeDot1dPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeDot1dPortRowStatus (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        *pi4RetValIeee8021BridgeDot1dPortRowStatus)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pBasePortEntry =
        VLAN_GET_BASE_PORT_ENTRY ((UINT2) u4Ieee8021BridgeBasePort);

    if (pBasePortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgeDot1dPortRowStatus = pBasePortEntry->u1RowStatus;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeDot1dPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgeDot1dPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeDot1dPortRowStatus (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        i4SetValIeee8021BridgeDot1dPortRowStatus)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;
    INT1                ai1Name[IF_PREFIX_LEN];
    INT1               *pi1Name = ai1Name;
    UINT1               au1Num[VLAN_MAX_IFNAME_LEN];
    UINT1               au1Alias[VCM_ALIAS_MAX_LEN];
    INT4                i4SlotNum = 0;
    UINT4               u4ContextId = u4Ieee8021BridgeBasePortComponentId;

    MEMSET (ai1Name, 0, IF_PREFIX_LEN);
    MEMSET (au1Num, 0, VLAN_MAX_IFNAME_LEN);
    MEMSET (au1Alias, 0, VCM_ALIAS_MAX_LEN);

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort);

    if ((i4SetValIeee8021BridgeDot1dPortRowStatus == CREATE_AND_WAIT) ||
        (i4SetValIeee8021BridgeDot1dPortRowStatus == CREATE_AND_GO))
    {
        if (pBasePortEntry != NULL)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4SetValIeee8021BridgeDot1dPortRowStatus != ACTIVE)
        {
            if (pBasePortEntry == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
        }
    }

    switch (i4SetValIeee8021BridgeDot1dPortRowStatus)
    {
        case CREATE_AND_WAIT:
            pBasePortEntry = (tVlanBasePortEntry *) (VOID *) VLAN_GET_BUF
                (VLAN_BASE_PORT_ENTRY, sizeof (tVlanBasePortEntry));
            if (pBasePortEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "No Memory for Base Port Entry creation\n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pBasePortEntry->u4ContextId = u4ContextId;

            pBasePortEntry->u4IfIndex = 0;
            pBasePortEntry->u4ComponentId = u4Ieee8021BridgeBasePortComponentId;
            pBasePortEntry->u4BasePort = u4Ieee8021BridgeBasePort;
            pBasePortEntry->u1IfType = CFA_GI_ENET;
            pBasePortEntry->u1RowStatus = NOT_READY;
            VLAN_GET_BASE_PORT_ENTRY (u4Ieee8021BridgeBasePort) =
                pBasePortEntry;

            break;

        case CREATE_AND_GO:
            /* Not supported */
            break;

        case ACTIVE:

            i4SlotNum = VLAN_DEFAULT_SLOT_ID;
            /* Default Gigabit ethernet IfType is assumed */
            CfaGetInterfaceNameFromIfType ((INT1 *) pi1Name, CFA_GI_ENET);
            SPRINTF ((CHR1 *) au1Num, "%d/%d", i4SlotNum,
                     pBasePortEntry->u4IfIndex);

            VlanUnLock ();
            if (CfaCreateAndWaitPhysicalInterface ((UINT1 *) pi1Name, au1Num,
                                                   &(pBasePortEntry->
                                                     u4IfIndex)) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (VcmGetAliasName (u4ContextId, au1Alias) == VCM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (VcmMapPortExt (pBasePortEntry->u4IfIndex, au1Alias) ==
                VCM_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (CfaActivatePhysicalInterface (pBasePortEntry->u4IfIndex) ==
                CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }

            VlanLock ();
            VlanSelectContext (u4ContextId);

            pBasePortEntry->u1BrgPortType =
                VlanGetDefPortType (pBasePortEntry->u4IfIndex);
            pBasePortEntry->u1RowStatus = ACTIVE;
            pBasePortEntry->u1OperStatus = VLAN_OPER_UP;
            break;

        case NOT_READY:
            pBasePortEntry->u1RowStatus = NOT_READY;
            pBasePortEntry->u1OperStatus = VLAN_OPER_DOWN;
            break;

        case NOT_IN_SERVICE:
            pBasePortEntry->u1RowStatus = NOT_IN_SERVICE;
            pBasePortEntry->u1OperStatus = VLAN_OPER_DOWN;
            break;

        case DESTROY:
            /* Get the Cfa Inteface Index from the context id and local port */
            VlanUnLock ();
            if (VcmGetAliasName (u4ContextId, au1Alias) == VCM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (VcmUnMapPortExt (pBasePortEntry->u4IfIndex, au1Alias) ==
                VCM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (CfaDeleteInterfaceExt (pBasePortEntry->u4IfIndex) ==
                CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            VlanLock ();

            break;

        default:
            break;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeDot1dPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgeDot1dPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeDot1dPortRowStatus (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4TestValIeee8021BridgeDot1dPortRowStatus)
{
    tVlanBasePortEntry *pBasePortEntry = NULL;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT2               u2NextFreeLocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);
    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4TestValIeee8021BridgeDot1dPortRowStatus == CREATE_AND_WAIT ||
        i4TestValIeee8021BridgeDot1dPortRowStatus == ACTIVE)
    {
        VcmGetVcNextFreeHlPortIdExt (u4Ieee8021BridgeBasePortComponentId,
                                     &u2NextFreeLocalPortId);
        if (u4Ieee8021BridgeBasePort != u2NextFreeLocalPortId)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    if (VLAN_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pBasePortEntry =
        VLAN_GET_BASE_PORT_ENTRY ((UINT2) u4Ieee8021BridgeBasePort);

    switch (i4TestValIeee8021BridgeDot1dPortRowStatus)
    {
        case ACTIVE:
            if (pBasePortEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            else if (pBasePortEntry->u1RowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            else if (pBasePortEntry->u4IfIndex == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
            if (pBasePortEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            else if (pBasePortEntry->u1RowStatus == NOT_IN_SERVICE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case NOT_READY:
            if (pBasePortEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            else if (pBasePortEntry->u1RowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;

        case CREATE_AND_WAIT:
            if (pBasePortEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pBasePortEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            break;
    }
    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeDot1dPortTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}


/* LOW LEVEL Routines for Table : Ieee8021BridgeBaseIfToPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeBaseIfToPortTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceIeee8021BridgeBaseIfToPortTable(INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeBaseIfToPortTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexIeee8021BridgeBaseIfToPortTable(INT4 *pi4IfIndex)
{
    UNUSED_PARAM (pi4IfIndex);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeBaseIfToPortTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexIeee8021BridgeBaseIfToPortTable(INT4 i4IfIndex ,INT4 *pi4NextIfIndex )
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseIfIndexComponentId
 Input       :  The Indices
                IfIndex

                The Object
                retValIeee8021BridgeBaseIfIndexComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeBaseIfIndexComponentId(INT4 i4IfIndex , UINT4 *pu4RetValIeee8021BridgeBaseIfIndexComponentId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValIeee8021BridgeBaseIfIndexComponentId);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseIfIndexPort
 Input       :  The Indices
                IfIndex

                The Object
                retValIeee8021BridgeBaseIfIndexPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeBaseIfIndexPort(INT4 i4IfIndex , UINT4 *pu4RetValIeee8021BridgeBaseIfIndexPort)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValIeee8021BridgeBaseIfIndexPort);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : Ieee8021BridgePhyPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePhyPortTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceIeee8021BridgePhyPortTable(UINT4 u4Ieee8021BridgePhyPort)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePhyPortTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexIeee8021BridgePhyPortTable(UINT4 *pu4Ieee8021BridgePhyPort)
{
    UNUSED_PARAM (pu4Ieee8021BridgePhyPort);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePhyPortTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                nextIeee8021BridgePhyPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexIeee8021BridgePhyPortTable(UINT4 u4Ieee8021BridgePhyPort ,UINT4 *pu4NextIeee8021BridgePhyPort )
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (pu4NextIeee8021BridgePhyPort);
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePhyPortIfIndex
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object
                retValIeee8021BridgePhyPortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgePhyPortIfIndex(UINT4 u4Ieee8021BridgePhyPort , INT4 *pi4RetValIeee8021BridgePhyPortIfIndex)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePhyPortIfIndex);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgePhyMacAddress
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object
                retValIeee8021BridgePhyMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgePhyMacAddress(UINT4 u4Ieee8021BridgePhyPort , tMacAddr * pRetValIeee8021BridgePhyMacAddress)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (pRetValIeee8021BridgePhyMacAddress);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgePhyPortToComponentId
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object
                retValIeee8021BridgePhyPortToComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgePhyPortToComponentId(UINT4 u4Ieee8021BridgePhyPort , UINT4 *pu4RetValIeee8021BridgePhyPortToComponentId)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (pu4RetValIeee8021BridgePhyPortToComponentId);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgePhyPortToInternalPort
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object
                retValIeee8021BridgePhyPortToInternalPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgePhyPortToInternalPort(UINT4 u4Ieee8021BridgePhyPort , UINT4 *pu4RetValIeee8021BridgePhyPortToInternalPort)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (pu4RetValIeee8021BridgePhyPortToInternalPort);
    return SNMP_SUCCESS;
}

