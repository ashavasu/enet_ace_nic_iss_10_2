/***********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanicch.c,v 1.37 2016/09/17 12:40:36 siva Exp $
 *
 * Description     : This file contains VLAN ICCH related routines
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : vlanicch.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Mar 2015                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/
#ifndef _VLANICCH_C_
#define _VLANICCH_C_
#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanIcchRegisterWithICCH                             */
/*                                                                           */
/* Description        : Registers VLAN module with ICCH to receive update    */
/*                      messages.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then VLAN_SUCCESS         */
/*                      Otherwise VLAN_FAILURE                               */
/*****************************************************************************/
INT4
VlanIcchRegisterWithICCH (VOID)
{
    tIcchRegParams        IcchRegParams;

    MEMSET (&IcchRegParams, 0, sizeof (tIcchRegParams));

    IcchRegParams.u4EntId = ICCH_VLAN_APP_ID;
    IcchRegParams.pFnRcvPkt = VlanIcchHandleUpdateEvents;

    if (VlanIcchRegisterProtocols (&IcchRegParams) == ICCH_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "VlanRegisterWithIcch: Registration with ICCH FAILED\n");

        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIcchHandleUpdateEvents                           */
/*                                                                           */
/* Description        : This function will be invoked by the ICCH module to  */
/*                      pass events  to VLAN module                          */
/*                                                                           */
/* Input(s)           : u1Event - Event given by ICCH module.                */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS - If the message is enq'ed successfully */
/*                      ICCH_FAILURE otherwise                               */
/*****************************************************************************/
INT4
VlanIcchHandleUpdateEvents (UINT1 u1Event, tIcchMsg * pData, UINT2 u2DataLen)
{
    tVlanQMsg          *pVlanQMsg = NULL;
    if ((u1Event != ICCH_PEER_DOWN) &&
        (u1Event != ICCH_MESSAGE)&&
        (u1Event != ICCH_PEER_UP)&&
        (u1Event != L2_INIT_BULK_UPDATES) &&
        (u1Event != MCLAG_ENABLED) &&
        (u1Event != MCLAG_DISABLED) 
		)

    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "ICCH event is invalid \n");

        return ICCH_FAILURE;
    }

    if ((pData == NULL) && (u1Event == ICCH_MESSAGE))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to VLAN task. */
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "Message from ICCH ignored - Buffer missing !!! \n");

        return ICCH_FAILURE;
    }

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return ICCH_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (pVlanQMsg == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                         "No memory for VLAN Q Message !!!!!! \n");
        return ICCH_FAILURE;
    }

    MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_ICCH_MSG;

    pVlanQMsg->IcchData.u4Events = (UINT4) u1Event;
    pVlanQMsg->IcchData.pIcchMsg = pData;
    pVlanQMsg->IcchData.u2DataLen = u2DataLen;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         ALL_FAILURE_TRC, "ICCH Message enqueue FAILED\n");
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, (UINT1 *) pVlanQMsg);
        return ICCH_FAILURE;
    }

    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         ALL_FAILURE_TRC, "ICCH Event send FAILED\n");

        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIcchProcessUpdateMsg                             */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the ICCH module. This function invokes          */
/*                      appropriate functions to handle the update message.  */
/*                                                                           */
/* Input(s)           : pVlanQMsg - Pointer to the VLAN Queue message.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessUpdateMsg (tVlanQMsg * pVlanQMsg)
{
    tIcchProtoAck         ProtoAck;
    UINT4                 u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tIcchProtoAck));

    switch (pVlanQMsg->IcchData.u4Events)
    {
        case ICCH_PEER_DOWN:
            VlanIcchProcessPeerDown ();
            IcchReleaseMemoryForMsg ( (UINT1 *)pVlanQMsg->IcchData.pIcchMsg);
            break;
        case ICCH_PEER_UP:
            VlanAddIsolationTblForIccl ();
            IcchReleaseMemoryForMsg ( (UINT1 *)pVlanQMsg->IcchData.pIcchMsg);
            break;
        case L2_INIT_BULK_UPDATES:
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                             CONTROL_PLANE_TRC,
                             "Received INITIATE_BULK_UPDATES from lower layer "
                             "module \n");
            if (OSIX_SUCCESS == IcchApiCheckProtoSyncEnabled ())
            {
            VlanIcchSendBulkReqMsg ();
            }
            break;

        case ICCH_MESSAGE:

            /* Read the sequence number from the ICCH Header */
            ICCH_PKT_GET_SEQNUM (pVlanQMsg->IcchData.pIcchMsg, &u4SeqNum);
            /* Remove the ICCH Header */
            ICCH_STRIP_OFF_ICCH_HDR (pVlanQMsg->IcchData.pIcchMsg,
                                 pVlanQMsg->IcchData.u2DataLen);

            ProtoAck.u4AppId = ICCH_VLAN_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            VlanIcchHandleUpdates (pVlanQMsg->IcchData.pIcchMsg,
                                  pVlanQMsg->IcchData.u2DataLen);

            ICCH_FREE (pVlanQMsg->IcchData.pIcchMsg);

            /* Sending ACK to ICCH */
            IcchApiSendProtoAckToICCH (&ProtoAck);
            break;

	case MCLAG_ENABLED:
	    VlanIcchProcessMCLAGEnableStatus ();	
	    break;
	case MCLAG_DISABLED:
	    VlanIcchProcessMCLAGDisableStatus ();	
	    break;
	default:
	    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
			    CONTROL_PLANE_TRC, "Received unknown event\n");

            break;
    } /* End of switch */
}

/*****************************************************************************/
/* Function Name      : VlanIcchHandleUpdates                                */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      ICCH module. This function invokes appropriate fns.  */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to tIcchMsg.                          */
/*                      u2DataLen - Data length.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchHandleUpdates (tIcchMsg * pMsg, UINT2 u2DataLen)
{
    tIcchProtoEvt              ProtoEvt;
    tVlanIcchUcastFdbSync      UcastFdbSync;
    tVlanIcchMcastFdbSync      McastFdbSync;
    UINT4               u4Context = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Entries = 0;
    tVlanId             VlanId = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2IcchMsgLen = 0;;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    MEMSET (&UcastFdbSync, 0, sizeof (tVlanIcchUcastFdbSync));
    MEMSET (&McastFdbSync, 0, sizeof (tVlanIcchMcastFdbSync));

    ProtoEvt.u4AppId = ICCH_VLAN_APP_ID;
    ProtoEvt.u4Event = ICCH_BULK_UPDT_ABORT;

    u2MinLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
               VLAN_ICCH_LEN_FIELD_SIZE +
               VLAN_ICCH_CONTEXT_FIELD_SIZE;

    VLAN_ICCH_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    if (u1MsgType == VLAN_ICCH_BULK_UPD_TAIL_MESSAGE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "Bulk update tail message received, which "
                         "indicates that bulk updates are completed "
                         "in VLAN module. It should be indicated to "
                         "ICCH modules. \n");
        ProtoEvt.u4Error = ICCH_NONE;
        ProtoEvt.u4Event = ICCH_PROTOCOL_BULK_UPDT_COMPLETION;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
        return;
    }            
    
    if (u1MsgType == VLAN_ICCH_BULK_UPD_REQUEST)
    {
        if (OSIX_SUCCESS == IcchApiCheckProtoSyncEnabled ())
        {
            VlanIcchSendBulkUpdates ();   
        }
        else
        {
            VlanIcchSetBulkUpdatesStatus (ICCH_VLAN_APP_ID);
            VlanIcchSendBulkUpdTailMsg ();
        }
        return;
    }
 
    u2OffSet -= VLAN_ICCH_TYPE_FIELD_SIZE;
    while ((u2OffSet + u2MinLen) <= u2DataLen)
    {

        VLAN_ICCH_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
        VLAN_ICCH_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* doesnot have room for type and length field itself.
             * Skip to next attribute */
            u2OffSet += u2Length;
            continue;
        }

        VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet, u4Context);

        u2IcchMsgLen = u2Length - u2MinLen;

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            u2OffSet = u2OffSet + u2IcchMsgLen;
            continue;
        }

        if (VLAN_MODULE_ADMIN_STATUS () != VLAN_ENABLED)
        {
            VlanReleaseContext ();
            u2OffSet = u2OffSet + u2IcchMsgLen;
            continue;
        }

        switch (u1MsgType)
        {
            /* Processing VLAN Ucast FDB sync-up message */
           case VLAN_ICCH_SYNC_UCAST_FDB:
                    VLAN_ICCH_GET_2_BYTE (pMsg, &u2OffSet, UcastFdbSync.VlanId);
                    VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet, UcastFdbSync.u4IfIndex);
                    VLAN_ICCH_GET_N_BYTE (pMsg, UcastFdbSync.au1IfName, &u2OffSet,
                                        CFA_MAX_PORT_NAME_LENGTH);
                    VLAN_ICCH_GET_N_BYTE (pMsg, UcastFdbSync.UcastAddr, &u2OffSet,
                                        VLAN_MAC_ADDR_LEN);
                    VLAN_ICCH_GET_N_BYTE (pMsg, UcastFdbSync.ConnectionId, &u2OffSet,
                                        VLAN_MAC_ADDR_LEN);
                    VLAN_ICCH_GET_2_BYTE (pMsg, &u2OffSet, UcastFdbSync.u2Flag);
                    VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet, UcastFdbSync.u4PwIndex);
                    VlanIcchProcessUcastSyncMessage (&UcastFdbSync); 
                    break;
           /* Delete the Remote FDB entries */
           case VLAN_ICCH_SYNC_FDB_FLUSH :
                    VlanFlushRemoteFdb (VLAN_TRUE);
                    break;

           /* Delete the Remote FDB entries on interface down */
           case VLAN_ICCH_SYNC_PORT_FLUSH: 
                    VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet, u4IfIndex);
                    VlanFlushRemoteFdbforIfIndex (u4IfIndex);
                    break; 
           
           /* Delete the Remote FDB entries on VLAN down */ 
           case VLAN_ICCH_SYNC_VLAN_FLUSH: 
                    VLAN_ICCH_GET_2_BYTE (pMsg, &u2OffSet, VlanId);
                    VlanFlushRemoteFdbforVlan (VlanId);
                    break; 

           /* Delete the Remote FDB entries on interface on a vlan */
           case VLAN_ICCH_SYNC_PORT_VLAN_FLUSH:
                    VLAN_ICCH_GET_2_BYTE (pMsg, &u2OffSet, VlanId);
                    VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet, u4IfIndex);
                    VlanFlushRemotePortFdbId (VlanId, u4IfIndex);
                    break;

           /* Processing Hit Bit Request Message */
           case VLAN_ICCH_HIT_BIT_REQUEST:
                    VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet,u4Entries);
                    VlanIcchProcessHitBitRequest (pMsg,u2OffSet,u4Entries);
                    break;

           /* Processing Hit Bit Reply Message*/
           case VLAN_ICCH_HIT_BIT_REPLY:
                    VLAN_ICCH_GET_4_BYTE (pMsg, &u2OffSet,u4Entries);
                    VlanIcchProcessHitBitReply (pMsg,u2OffSet,u4Entries);
                    break;

           /* Processing Port Down Message*/
           case VLAN_ICCH_SYNC_PORT_DOWN:
                    VLAN_ICCH_GET_N_BYTE (pMsg, UcastFdbSync.au1IfName, &u2OffSet,
                                        CFA_MAX_PORT_NAME_LENGTH);
                    if (OSIX_SUCCESS ==  CfaGetInterfaceIndexFromName 
                                 (UcastFdbSync.au1IfName, &u4IfIndex))
                    {
                        VlanFlushFdbForIfIndex (u4IfIndex, VLAN_LOCAL_FDB);
                    }
                    break;            

           /* Processing Check Port Status Message*/
           case VLAN_ICCH_CHECK_PORT_STATUS:
                    VLAN_ICCH_GET_N_BYTE (pMsg, UcastFdbSync.au1IfName, &u2OffSet,
                                        CFA_MAX_PORT_NAME_LENGTH);
                    VlanIcchProcessCheckPortStatus (&UcastFdbSync);
                    break;

            default:
            break;
        } /* End of switch */
        VlanReleaseContext ();

    }
    return;
}


/*****************************************************************************/
/* Function Name      : VlanIcchSendBulkUpdates                              */
/*                                                                           */
/* Description        : This function sends bulk updates to the remote node. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSendBulkUpdates (VOID)
{
    tIcchProtoEvt         ProtoEvt;
    UINT4               u4Context = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    ProtoEvt.u4AppId = ICCH_VLAN_APP_ID;
    ProtoEvt.u4Event = ICCH_BULK_UPDT_ABORT;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        /* VLAN completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        VlanIcchSetBulkUpdatesStatus (ICCH_VLAN_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        VlanIcchSendBulkUpdTailMsg ();

        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         ALL_FAILURE_TRC, "Vlan Module not started ! \n");

        return;
    }

      /* Scan all the contexts and bulk sync the unicast/multicast FDB and 
     * VLAN information available in each context.
     */
    for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
    {

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)                                         
        {
            break;
        }

           if (VLAN_FAILURE == VlanIcchSyncBulkVlanEntries (u4Context))
           {
               VlanReleaseContext ();
               VlanIcchSetBulkUpdatesStatus (ICCH_VLAN_APP_ID);
               VlanIcchSendBulkUpdTailMsg ();
               return;
           } 

           VlanReleaseContext ();
    }  /* End of for */

        VlanIcchSetBulkUpdatesStatus (ICCH_VLAN_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        VlanIcchSendBulkUpdTailMsg ();
        return;

}

/*****************************************************************************/
/* Function Name      : VlanIcchSyncBulkVlanEntries                          */
/*                                                                           */
/* Description        : This function will send vlan update                  */
/*                      syncup messages to remote node in bulk.              */
/*                      (i) VLAN table data                                  */
/*                      (ii) Unicast FDB table.                              */
/*                      (iii) Multicast FDB table.                           */
/*                                                                           */
/* Input(s)           : u4Context   - Virtual ContextId                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanIcchSyncBulkVlanEntries (UINT4 u4Context) 
{
    tIcchProtoEvt    ProtoEvt;
    UINT1            au1IfName [CFA_MAX_PORT_NAME_LENGTH];
    tIcchMsg           *pIcchMsg = NULL;
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
    tVlanFdbInfo       VlanFdbInfo;
    UINT2    u2UcastMsgLen = 0;
    UINT2    u2Offset = 0;
    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    MEMSET (&VlanFdbInfo, 0, sizeof (tVlanFdbInfo));
    MEMSET (au1IfName, 0, sizeof(au1IfName));
    /* For each table
     * Scan each entry in a table and do the following
     * -> Fetch the data to sync with remote from the table database.
     * -> Frame message in the following format
     *    <---TYPE---|---LENGTH---|---VALUE----->
     * -> keep filling the entries in the pMsg buffer till the maximum roomsize
     *    is reached (i.e ICCH_MAX_MSG_SIZE)
     * -> If the pMsg buffer is completely reached
     *    -> Send out the buffer to ICCH module
     *    else
     *    -> Repeat the loop and keep updating the pMsg buffer.
     * -> Once the complete table is scanned, send the pending buffer to ICCH module.
     * 
     */

    /* Once complete sync is done, send a TAIL message to ICCH to indicate
     * the synchronization is complete and other modules can proceed with
     * processing.
     */
    u2UcastMsgLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
                    VLAN_ICCH_LEN_FIELD_SIZE +
                    VLAN_ICCH_CONTEXT_FIELD_SIZE + 
                    VLAN_ICCH_CONTEXT_FIELD_SIZE +
                    VLAN_ICCH_FDB_ID_LEN +
                    CFA_MAX_PORT_NAME_LENGTH +
                    VLAN_ICCH_MAC_ADDR_LEN + 
                    VLAN_ICCH_MAC_ADDR_LEN +
                    VLAN_ICCH_FLAG_LEN +
                    VLAN_ICCH_CONTEXT_FIELD_SIZE;

    pIcchMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_MAX_MSG_SIZE);
    if (NULL ==  pIcchMsg)                            
    {
        VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME, 
                  "ICCH alloc failed. Bulk updates not sent\n");
        ProtoEvt.u4Error = ICCH_MEMALLOC_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
        return VLAN_FAILURE;  
    } 



    /* 1.Dynamic Learnt unicast fdb database */
    VlanFdbInfo.u2RemoteId = VLAN_LOCAL_FDB;  
    pVlanFdbInfo = (tVlanFdbInfo *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
            (tRBElem *) & VlanFdbInfo, NULL);

    while (NULL != pVlanFdbInfo)
    {

        if (VLAN_FAILURE == 
                VlanIcchCheckAndSendMsg (&pIcchMsg, u2UcastMsgLen, &u2Offset))
        {
            VlanIcchSendBulkUpdTailMsg ();
            return VLAN_FAILURE;
        }
        /* During Sync-up only the Self Learnt Entries are to be synced up */
        /* Remote Learnt entries and static entries need not be synced up */

        if ((pVlanFdbInfo->u2RemoteId == VLAN_LOCAL_FDB)&&
	    (pVlanFdbInfo->u1EntryType == VLAN_FDB_LEARNT))
        {
            /* Form a unicast FDB address addition sync message
             *
             * |---------------------------------------------------------|
             * |Msg. Type |   Length  | ContextId | FidIndex |  IfIndex  | 
             * | (1 byte) | (2 bytes) | (4 bytes) | (2 bytes)| (4 bytes) | 
             * |----------------------------------------------------------
             * |---------------------------------------------------|
             * | au1IfName |MAC Address |  connectionId |    Flag  |
             * | (N Bytes) | (6 bytes)  |   ( 6 bytes)  | (2 bytes)|
             * |---------------------------------------------------|

             * The ICCH Hdr shall be included by ICCH
             */
            MEMSET(au1IfName, 0, sizeof(au1IfName));
            VlanCfaGetInterfaceNameFromIndex ( VLAN_GET_IFINDEX (pVlanFdbInfo->u2Port),  au1IfName);

            VLAN_ICCH_PUT_1_BYTE (pIcchMsg, &u2Offset, VLAN_ICCH_SYNC_UCAST_FDB);
            VLAN_ICCH_PUT_2_BYTE (pIcchMsg, &u2Offset, u2UcastMsgLen);
            VLAN_ICCH_PUT_4_BYTE (pIcchMsg, &u2Offset, u4Context);
            VLAN_ICCH_PUT_2_BYTE (pIcchMsg, &u2Offset, (UINT2) pVlanFdbInfo->u4FdbId);
            VLAN_ICCH_PUT_4_BYTE (pIcchMsg, &u2Offset, (UINT4) VLAN_GET_IFINDEX(pVlanFdbInfo->u2Port));
            VLAN_ICCH_PUT_N_BYTE (pIcchMsg, au1IfName,
                    &u2Offset, CFA_MAX_PORT_NAME_LENGTH);
            VLAN_ICCH_PUT_N_BYTE (pIcchMsg, pVlanFdbInfo->MacAddr,
                    &u2Offset, VLAN_MAC_ADDR_LEN);
            VLAN_ICCH_PUT_N_BYTE (pIcchMsg, pVlanFdbInfo->ConnectionId,
                    &u2Offset, VLAN_MAC_ADDR_LEN);
            VLAN_ICCH_PUT_2_BYTE (pIcchMsg, &u2Offset, VLAN_ADD);
            VLAN_ICCH_PUT_4_BYTE (pIcchMsg, &u2Offset, pVlanFdbInfo->u4PwVcIndex);
        }    
        VlanFdbInfo.u4FdbId = pVlanFdbInfo->u4FdbId;
        MEMCPY(VlanFdbInfo.MacAddr,pVlanFdbInfo->MacAddr,sizeof(tMacAddr));
        VlanFdbInfo.u2RemoteId = pVlanFdbInfo->u2RemoteId;
        pVlanFdbInfo =(tVlanFdbInfo *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                (tRBElem *) & VlanFdbInfo, NULL);
    }

    /* Send Bulk Tail Message once the dynamic unicast entries are synced up. */                       
    if (u2Offset != 0)
    {
        /* Send the buffer out */
        if (VlanIcchSendMsgToIcch (pIcchMsg, u2Offset) == VLAN_FAILURE)
        {
            ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
            VlanIcchHandleProtocolEvent (&ProtoEvt);
        }
    }
    else if (pIcchMsg)
    {
        /* Empty buffer created without any update messages filled */
        ICCH_FREE (pIcchMsg);
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanSyncVlanEntriesforIfIndex                        */
/*                                                                           */
/* Description        : This function will send vlan update                  */
/*                      syncup messages to remote node in bulk.              */
/*                      (i) VLAN table data                                  */
/*                      (ii) Unicast FDB table.                              */
/*                      (iii) Multicast FDB table.                           */
/*                                                                           */
/* Input(s)           : u4Context   - Virtual ContextId                      */
/*                      ppMsg - pointer to pointer to IcchMsg.               */
/*                                                                           */
/* Output(s)          : *ppMsg - Updated pointer to IcchMsg after bulk update*/
/*                      pu2Offset - Updated offset.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanSyncVlanEntriesforIfIndex (tIcchMsg **ppMsg, UINT4 u4Context, 
                                       UINT2 *pu2OffSet, UINT4 u4IfIndex) 
{
    tIcchProtoEvt      ProtoEvt;
    UINT1              au1IfName [CFA_MAX_PORT_NAME_LENGTH];
    tIcchMsg           *pIcchMsg = *ppMsg;
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
    tVlanFdbInfo       VlanFdbInfo;
    UINT4              u4ContextId = 0;
    UINT2              u2LocalPort = 0;
    UINT2              u2UcastMsgLen = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    MEMSET (&VlanFdbInfo, 0, sizeof (tVlanFdbInfo));
    MEMSET (au1IfName, 0, sizeof(au1IfName));
    /* For each table
     * Scan each entry in a table and do the following
     * -> Fetch the data to sync with remote from the table database.
     * -> Frame message in the following format
     *    <---TYPE---|---LENGTH---|---VALUE----->
     * -> keep filling the entries in the pMsg buffer till the maximum roomsize
     *    is reached (i.e ICCH_MAX_MSG_SIZE)
     * -> If the pMsg buffer is completely reached
     *    -> Send out the buffer to ICCH module
     *    else
     *    -> Repeat the loop and keep updating the pMsg buffer.
     * -> Once the complete table is scanned, send the pending buffer to ICCH module.
     * 
     */

    u2UcastMsgLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
                    VLAN_ICCH_LEN_FIELD_SIZE +
                    VLAN_ICCH_CONTEXT_FIELD_SIZE + 
                    VLAN_ICCH_CONTEXT_FIELD_SIZE +
                    VLAN_ICCH_FDB_ID_LEN + 
                    CFA_MAX_PORT_NAME_LENGTH +
                    VLAN_ICCH_MAC_ADDR_LEN + 
                    VLAN_ICCH_MAC_ADDR_LEN + 
                    VLAN_ICCH_FLAG_LEN +
                    VLAN_ICCH_CONTEXT_FIELD_SIZE;

    VlanFdbInfo.u2RemoteId = VLAN_LOCAL_FDB;  
    pVlanFdbInfo = (tVlanFdbInfo *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
            (tRBElem *) & VlanFdbInfo, NULL);

    if (VLAN_FAILURE == VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                 &u2LocalPort))
    {
        return VLAN_FAILURE;
    }


    while (NULL != pVlanFdbInfo)
    {

        if (VLAN_FAILURE == 
                VlanIcchCheckAndSendMsg (&pIcchMsg, u2UcastMsgLen, pu2OffSet))
        {
            VlanIcchSendBulkUpdTailMsg ();
            return VLAN_FAILURE;
        }
        /* During Sync-up only the Self Learnt Entries are to be synced up */
        /* Remote Learnt entries and static entries need not be synced up */

        if ((pVlanFdbInfo->u2RemoteId == VLAN_LOCAL_FDB)&&
	    (pVlanFdbInfo->u1EntryType == VLAN_FDB_LEARNT)&&
        (pVlanFdbInfo->u2Port == u2LocalPort))
        {
            /* Form a unicast FDB address addition sync message
             *
             * |---------------------------------------------------------|
             * |Msg. Type |   Length  | ContextId | FidIndex |  IfIndex  | 
             * | (1 byte) | (2 bytes) | (4 bytes) | (2 bytes)| (4 bytes) | 
             * |----------------------------------------------------------
             * |---------------------------------------------------|
             * | au1IfName |MAC Address |  connectionId |    Flag  |
             * | (N Bytes) | (6 bytes)  |   ( 6 bytes)  | (2 bytes)|
             * |---------------------------------------------------|

             * The ICCH Hdr shall be included by ICCH
             */
            MEMSET(au1IfName, 0, sizeof(au1IfName));
            VlanCfaGetInterfaceNameFromIndex ( VLAN_GET_IFINDEX (pVlanFdbInfo->u2Port),  au1IfName);

            VLAN_ICCH_PUT_1_BYTE (pIcchMsg, pu2OffSet, VLAN_ICCH_SYNC_UCAST_FDB);
            VLAN_ICCH_PUT_2_BYTE (pIcchMsg, pu2OffSet, u2UcastMsgLen);
            VLAN_ICCH_PUT_4_BYTE (pIcchMsg, pu2OffSet, u4Context);
            VLAN_ICCH_PUT_2_BYTE (pIcchMsg, pu2OffSet, (UINT2) pVlanFdbInfo->u4FdbId);
            VLAN_ICCH_PUT_4_BYTE (pIcchMsg, pu2OffSet, (UINT4) VLAN_GET_IFINDEX(pVlanFdbInfo->u2Port));
            VLAN_ICCH_PUT_N_BYTE (pIcchMsg, au1IfName,
                    pu2OffSet, CFA_MAX_PORT_NAME_LENGTH);
            VLAN_ICCH_PUT_N_BYTE (pIcchMsg, pVlanFdbInfo->MacAddr,
                    pu2OffSet, VLAN_MAC_ADDR_LEN);
            VLAN_ICCH_PUT_N_BYTE (pIcchMsg, pVlanFdbInfo->ConnectionId,
                    pu2OffSet, VLAN_MAC_ADDR_LEN);
            VLAN_ICCH_PUT_2_BYTE (pIcchMsg, pu2OffSet, VLAN_ADD);
            VLAN_ICCH_PUT_4_BYTE (pIcchMsg, pu2OffSet, pVlanFdbInfo->u4PwVcIndex);
        }    
        VlanFdbInfo.u4FdbId = pVlanFdbInfo->u4FdbId;
        MEMCPY(VlanFdbInfo.MacAddr,pVlanFdbInfo->MacAddr,sizeof(tMacAddr));
        VlanFdbInfo.u2RemoteId = pVlanFdbInfo->u2RemoteId;
        pVlanFdbInfo =(tVlanFdbInfo *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                (tRBElem *) & VlanFdbInfo, NULL);
    }

    if (*pu2OffSet != 0)
    {
        /* Send the buffer out */
        if (VlanIcchSendMsgToIcch (pIcchMsg, *pu2OffSet) == VLAN_FAILURE)
        {
            ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
            VlanIcchHandleProtocolEvent (&ProtoEvt);
        }
    }
    else if (pIcchMsg)
    {
        /* Empty buffer created without any update messages filled */
        ICCH_FREE (pIcchMsg);
    }

    return VLAN_SUCCESS;
}


/*****************************************************************************/
/*    Function Name       : VlanIcchGetIfNameFromPortArray                   */
/*                                                                           */
/*    Description         : This function fetches Interface name from the    */
/*                          PortArray                                        */
/*                                                                           */
/*    Input(s)            : PortArray    - Local PortArray                   */
/*                          au1IfName  - Interface name                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT1
VlanIcchGetIfNameFromPortArray (tHwPortArray *pHwPortArray, UINT1 *au1IfName)
{
    tCfaIfInfo CfaIfInfo;
    INT4 i4Temp     = 0;

    MEMSET (&CfaIfInfo, 0 , sizeof (tCfaIfInfo));

    for (i4Temp = 0; i4Temp < pHwPortArray->i4Length; i4Temp++)
    {
        if (OSIX_TRUE == 
            IcchApiIsMcLagInterface (pHwPortArray->pu4PortArray[i4Temp]))            
        {
            if (CFA_SUCCESS == CfaGetIfInfo (pHwPortArray->pu4PortArray[i4Temp], 
                        &CfaIfInfo))
            {
                MEMCPY (au1IfName, CfaIfInfo.au1IfName, 
                        CFA_MAX_PORT_NAME_LENGTH);
                return VLAN_SUCCESS;
            }
        }
    } /* End of for */

    return VLAN_FAILURE;
}



/*****************************************************************************/
/* Function Name             : VlanIcchGetIfNameFromPortList                 */
/*                                                                           */
/* Description               : This function fetches Interface name from the */
/*                             PortList                                      */
/*                                                                           */
/* Input(s)                  : PortList    - Local PortList                  */
/*                             au1IfName  - Interface name                   */
/*                                                                           */
/* Output(s)                 : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Exceptions or Operating                                                   */
/* System Error Handling     : None.                                         */
/*                                                                           */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : VLAN_SUCCESS/VLAN_FAILURE                     */
/*****************************************************************************/
INT1
VlanIcchGetIfNameFromPortList (tLocalPortList PortList, UINT1 *au1IfName)
{
    tCfaIfInfo CfaIfInfo;
    UINT4 au4PortArray[VLAN_MAX_PORTS];
    UINT4 u4Temp     = 0;
    UINT2 u2NumPorts = 0;

    MEMSET (&CfaIfInfo, 0 , sizeof (tCfaIfInfo));
    MEMSET (au4PortArray, 0 , sizeof (UINT4) * VLAN_MAX_PORTS);

    /* If the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortList,
                sizeof (tLocalPortList)) == 0)
    {
        return VLAN_FAILURE;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortList,
                                         au4PortArray, &u2NumPorts);
         
        for (u4Temp = 0; u4Temp < u2NumPorts; u4Temp++)
        {
            if (OSIX_TRUE == IcchApiIsMcLagInterface (au4PortArray[u4Temp]))            
            {
                if (CFA_SUCCESS == CfaGetIfInfo (au4PortArray[u4Temp], &CfaIfInfo))
                {
                    MEMCPY (au1IfName, CfaIfInfo.au1IfName, 
                            CFA_MAX_PORT_NAME_LENGTH);
                    return VLAN_SUCCESS;
                }
            }
        } /* End of for */

    }

    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanIcchSyncUcastAddress                             */
/*                                                                           */
/* Description        : This function will send unicast address update       */
/*                      syncup message  to remote  node.                     */
/*                                                                           */
/* Input(s)           : pVlanIcchFdbSync - Pointer to VLAN FDB Syncup message*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanIcchSyncUcastAddress (tVlanIcchUcastFdbSync *pVlanIcchFdbSync) 
{

    tIcchProtoEvt       ProtoEvt;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tIcchMsg             *pMsg = NULL;
    UINT4               u4IcclIfIndex = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;

    MEMSET (au1IfName,0,CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return VLAN_FAILURE;
    }

  
    if(LA_MCLAG_DISABLED == VlanLaGetMCLAGSystemStatus())
    {
        return VLAN_FAILURE;
    }
    IcchGetIcclIfIndex (&u4IcclIfIndex);

    if (u4IcclIfIndex == pVlanIcchFdbSync->u4IfIndex)
    {
        return VLAN_SUCCESS;
    }

    /* Form a unicast FDB address addition sync message
     *
     * |---------------------------------------------------------|
     * |Msg. Type |   Length  | ContextId | FidIndex |  IfIndex  |
     * | (1 byte) | (2 bytes) | (4 bytes) | (2 bytes)| (4 bytes) |
     * |---------------------------------------------------------|
     * |---------------------------------------------------|
     * | au1IfName |MAC Address |  connectionId |    Flag  |
     * | (N Bytes) | (6 bytes)  |   ( 6 bytes)  | (2 bytes)|
     * |---------------------------------------------------|

     * The ICCH Hdr shall be included by ICCH 
     */
    u2MsgLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
               VLAN_ICCH_LEN_FIELD_SIZE +
               VLAN_ICCH_CONTEXT_FIELD_SIZE +
               VLAN_ICCH_CONTEXT_FIELD_SIZE +
               VLAN_ICCH_FDB_ID_LEN + 
               CFA_MAX_PORT_NAME_LENGTH +
               VLAN_ICCH_MAC_ADDR_LEN + 
               VLAN_ICCH_MAC_ADDR_LEN + 
               VLAN_ICCH_FLAG_LEN +
               VLAN_ICCH_CONTEXT_FIELD_SIZE;

    if ((pMsg = ICCH_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        VLAN_TRC (VLAN_ICCH_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME,
                  "ICCH alloc failed. Mac address update sync up message"
                  "not sent \n");
        return VLAN_FAILURE;
    }
    STRNCPY (au1IfName, pVlanIcchFdbSync->au1IfName, CFA_MAX_PORT_NAME_LENGTH);
	au1IfName[CFA_MAX_PORT_NAME_LENGTH-1]= '\0';

    VLAN_TRC (VLAN_ICCH_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Sending mac address addition sync up message\n");

    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_SYNC_UCAST_FDB);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, pVlanIcchFdbSync->u4Context);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, pVlanIcchFdbSync->VlanId);
        VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, pVlanIcchFdbSync->u4IfIndex);
    VLAN_ICCH_PUT_N_BYTE (pMsg, au1IfName, 
                          &u2OffSet, CFA_MAX_PORT_NAME_LENGTH);
    VLAN_ICCH_PUT_N_BYTE (pMsg, pVlanIcchFdbSync->UcastAddr, 
                          &u2OffSet, VLAN_MAC_ADDR_LEN);
    VLAN_ICCH_PUT_N_BYTE (pMsg, pVlanIcchFdbSync->ConnectionId, 
                          &u2OffSet, VLAN_MAC_ADDR_LEN);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, pVlanIcchFdbSync->u2Flag);
 
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, pVlanIcchFdbSync->u4PwIndex);

    if(VlanIcchSendMsgToIcch (pMsg, u2OffSet)== VLAN_FAILURE)

    {
       ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
       VlanIcchHandleProtocolEvent (&ProtoEvt);
    }


    return VLAN_SUCCESS;

}



/******************************************************************************/
/* Function Name             : VlanIcchSendMsgToIcch                          */
/*                                                                            */
/* Description               : This function enqueues the update messages to  */
/*                             the ICCH module.                               */
/*                                                                            */
/* Input(s)                  : pMsg  - Message to be sent to RM module        */
/*                             u2Len - Length of the Message                  */
/*                                                                            */
/* Output(s)                 : None                                           */
/*                                                                            */
/* Global Variables Referred : None                                           */
/*                                                                            */
/* Global variables Modified : None                                           */
/*                                                                            */
/* Use of Recursion          : None                                           */
/*                                                                            */
/* Returns                   : VLAN_SUCCESS/VLAN_FAILURE                      */
/******************************************************************************/
INT4
VlanIcchSendMsgToIcch (tIcchMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal;

    u4RetVal = VlanIcchEnqMsgToIcchFromAppl (pMsg, u2Len, ICCH_VLAN_APP_ID,
                                         ICCH_VLAN_APP_ID);

    if (u4RetVal != ICCH_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         ALL_FAILURE_TRC,
                         "Enqueuing of Messages from Appl to Icch failed \n");

        ICCH_FREE (pMsg);
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : VlanIcchProcessUcastSyncMessage                      */
/*                                                                           */
/* Description        : This function creates/deletes/updates the unicast    */
/*                      entry from the peer node                             */
/*                                                                           */
/* Input(s)           : pVlanIcchFdbSync - Pointer to VLAN FDB Syncup message*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIcchProcessUcastSyncMessage (tVlanIcchUcastFdbSync *pVlanIcchUcastFdbSync)
{
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    tVlanCurrEntry     *pCurrEntry = NULL;
    tLocalPortList      AllowedToGoPort;
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
    tVlanIcchUcastFdbSync VlanIcchUcastFdb;
    tHwUnicastMacEntry HwUnicastEntry;
    UINT4               u2PortChannel = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IcclIfIndex = 0;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT2               u2RemoteId = VLAN_REMOTE_FDB; 
    UINT2               u2LocalPort = 0;
    tVlanId             VlanId = 0;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;
    UINT1               u1Status  = VLAN_DELETE_ON_TIMEOUT;

    MEMSET (&VlanIcchUcastFdb, 0, sizeof (tVlanIcchUcastFdbSync));

    MEMSET (&HwUnicastEntry,0,sizeof(tHwUnicastMacEntry));

    MEMSET (AllowedToGoPort, 0, sizeof (tLocalPortList));
    VLAN_MEMSET (au1String, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);

    VlanIcchGetIcclIfIndex (&u4IcclIfIndex);

    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return VLAN_FAILURE;
    }

    if (VLAN_DELETE == pVlanIcchUcastFdbSync->u2Flag)
    {
        pVlanFdbInfo = VlanGetFdbEntry ((UINT4) pVlanIcchUcastFdbSync->VlanId,
                                        pVlanIcchUcastFdbSync->UcastAddr);
        if (NULL != pVlanFdbInfo)
        {
            /* LOCAL FDB */
            if (pVlanFdbInfo->u2RemoteId != VLAN_REMOTE_FDB)
            {
                i4RetVal = VlanHwGetFdbEntry (u4ContextId, (UINT4) pVlanIcchUcastFdbSync->VlanId,
                        pVlanIcchUcastFdbSync->UcastAddr, &HwUnicastEntry);
                if (i4RetVal == VLAN_FAILURE)
                {
                    /* Do nothing as the entry is already not available */
                    return VLAN_SUCCESS;
                }
                if (VLAN_FAILURE == VlanIcchCheckMacAddrOnMclag (pVlanIcchUcastFdbSync->UcastAddr,
                            pVlanIcchUcastFdbSync->VlanId))
                {
                    /* If VLAN_MCLAG_FDB_ENTRY is set on the VlanFdbInfo,
                     * We have to flush the Fdb Entries and send Hit Bit Reply
                     */
                    HwUnicastEntry.u1HitStatus = OSIX_FALSE;
                }
                if (HwUnicastEntry.u1HitStatus == OSIX_FALSE)
                {
                    /* Delete the entry in FDB which was added as a dynamic entry */
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
                    VlanFdbRemoveLocalOrRemote(pVlanIcchUcastFdbSync->VlanId, pVlanIcchUcastFdbSync->UcastAddr);
#endif
#endif
                    VlanHwDelStaticUcastEntry((UINT4) pVlanIcchUcastFdbSync->VlanId, pVlanIcchUcastFdbSync->UcastAddr, VLAN_INIT_VAL);
                }
                else
                {
                    MEMCPY (&u2PortChannel, pVlanFdbInfo->au1Pad, sizeof(UINT2));
                    /* pVlanFdbInfo->au1Pad (2 Bytes) will be populated with the
                     * port-channel number if it is a port-channel else for normal
                     * ports the value would be zero.
                     */
                    if (0 != u2PortChannel)
                    {
                        SPRINTF((CHR1 *) VlanIcchUcastFdb.au1IfName,"po%d", u2PortChannel);
                    }
                    else
                    {
                        STRNCPY (VlanIcchUcastFdb.au1IfName, pVlanIcchUcastFdbSync->au1IfName, CFA_MAX_PORT_NAME_LENGTH);
                    }

                    VlanIcchUcastFdb.au1IfName[CFA_MAX_PORT_NAME_LENGTH-1]= '\0';
                    VLAN_CPY_MAC_ADDR (VlanIcchUcastFdb.UcastAddr, pVlanIcchUcastFdbSync->UcastAddr);
                    VlanIcchUcastFdb.u4IfIndex = pVlanIcchUcastFdbSync->u4IfIndex;
                    VlanIcchUcastFdb.u4Context = pVlanIcchUcastFdbSync->u4Context;
                    VlanIcchUcastFdb.VlanId = pVlanIcchUcastFdbSync->VlanId;
                    VlanIcchUcastFdb.u2Flag = VLAN_ADD;
                    VlanIcchSyncUcastAddress (&VlanIcchUcastFdb);
                }
            }
            else
            {
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
                VlanFdbRemoveLocalOrRemote(pVlanIcchUcastFdbSync->VlanId, pVlanIcchUcastFdbSync->UcastAddr);
#endif
#endif
                VlanHwDelStaticUcastEntry((UINT4) pVlanIcchUcastFdbSync->VlanId, pVlanIcchUcastFdbSync->UcastAddr, VLAN_INIT_VAL);
            }
        }
        return VLAN_SUCCESS;
    }

    /* TO program the FDB entry received from remote node,
     * If the incoming interface is a  MC-LAG interface
     *  --> Program the FDB with destination port as MC-LAG index. 
     *  else
     *  --> Program the FDB with destination port as ICCL index. 
     */
    
    u4IfIndex = 0;
    if (OSIX_SUCCESS ==
            CfaGetInterfaceIndexFromName (pVlanIcchUcastFdbSync->au1IfName,
                &u4IfIndex))
    {

        LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);   
        
        if (u1IsMclagEnabled == OSIX_FALSE)
        {
            IcchGetIcclIfIndex (&u4IfIndex); 
        }
    }
    else
    {
       /* Interface name does not exist in the name; Hence,  fetch the 
        * ICCL interface index */
        IcchGetIcclIfIndex (&u4IfIndex);
    }
    if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                &u4ContextId,
                &u2LocalPort) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (u1IsMclagEnabled == OSIX_TRUE)
    { 
        u2RemoteId = VLAN_LOCAL_FDB;       
        pVlanIcchUcastFdbSync->u4IfIndex = u2LocalPort;

        if (CFA_FAILURE != VlanCfaGetIfOperStatus (u4IfIndex, &u1OperStatus))
        {
          /* We should program the fbd entry synced up from Peer on MC-LAG 
           * on ICCL since the Interface is down. 
           */
            if (u1OperStatus == CFA_IF_DOWN)
            {
                if (VlanGetContextInfoFromIfIndex (u4IcclIfIndex,
                            &u4ContextId,
                            &u2LocalPort) == VLAN_FAILURE)
                {
                    return VLAN_FAILURE;
                }
            }
        }
    }    

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    /* Check whether VLAN is created in context otherwise
       no need to create the entry  */
    
    pCurrEntry = VlanGetVlanEntry (pVlanIcchUcastFdbSync->VlanId);

    if (pCurrEntry == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, CONTROL_PLANE_TRC, ALL_FAILURE_TRC,
		  "VLAN entry not present. Unable to syncup remote FDB entry\n");
        return VLAN_FAILURE;
    }


    VLAN_SET_MEMBER_PORT (AllowedToGoPort, u2LocalPort);
    /* Ignore the first two letters po incase it is a portchannel */
    if (NULL != STRSTR (pVlanIcchUcastFdbSync->au1IfName,"po"))
    { 
       u2PortChannel = (UINT2)ATOI(pVlanIcchUcastFdbSync->au1IfName+VLAN_MCLAG_AGG_NUM_OFFSET);
    }
 
    VlanId = pVlanIcchUcastFdbSync->VlanId;

   
   /* Check if Entry is there in SW_LEARNING Database and proceed only if 
     * it is not there */
    if (VLAN_SUCCESS == VlanGetFdbEntryWithPort 
                        (pVlanIcchUcastFdbSync->UcastAddr,
                         pVlanIcchUcastFdbSync->VlanId,
                        (UINT2) pVlanIcchUcastFdbSync->u4IfIndex))
    {
        PrintMacAddress (pVlanIcchUcastFdbSync->UcastAddr, au1String);
        VLAN_TRC_ARG3 (VLAN_ICCH_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC , VLAN_NAME,
                      "FDB Entry Mac:%s VlanId:%d Port:%d Already Present"
                      "in SW Database. \n",au1String,pVlanIcchUcastFdbSync->VlanId,
                      pVlanIcchUcastFdbSync->u4IfIndex);
                      
        return VLAN_FAILURE;
    }

    if (pCurrEntry->u4UnicastDynamicCount < 
                           VLAN_CONTROL_MAC_LEARNING_LIMIT(VlanId))
    {
        if (VlanHwAddStaticUcastEntry (pVlanIcchUcastFdbSync->VlanId, 
            pVlanIcchUcastFdbSync->UcastAddr,
            VLAN_DEF_RECVPORT, AllowedToGoPort, 
            u1Status, pVlanIcchUcastFdbSync->ConnectionId) == VLAN_SUCCESS)
        {
            VlanFdbTableAddInCtxt ((UINT2) pVlanIcchUcastFdbSync->u4IfIndex,
				    pVlanIcchUcastFdbSync->UcastAddr,
                                    pVlanIcchUcastFdbSync->VlanId,
                                    VLAN_FDB_LEARNT,
                                    pVlanIcchUcastFdbSync->ConnectionId,
                                    u2RemoteId,
                                    u2PortChannel,pVlanIcchUcastFdbSync->u4PwIndex);
   
     if (u1OperStatus == CFA_IF_DOWN)
            {
                VlanIcchHandleMclagOperStatusChange 
                                          (pVlanIcchUcastFdbSync->UcastAddr,
                                           pVlanIcchUcastFdbSync->VlanId );
            }        
        }
        else
        {
            PrintMacAddress (pVlanIcchUcastFdbSync->UcastAddr, au1String);

            VLAN_TRC_ARG1 (VLAN_ICCH_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "Failed to program MAC address %s\r\n",
                           au1String);

            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VlanSysLogId,
                         "Failed to program MAC address %s\r\n", au1String));
            return VLAN_FAILURE;
        }
    }
    else
    {
        VLAN_TRC_ARG1 (VLAN_ICCH_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        VLAN_NAME, "MAC learning limit has reached for "
                        "VLAN ID %u\r\n", VlanId);

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VlanSysLogId, "MAC learning "
                     "limit has reached for VLAN ID %u\r\n", VlanId));
    }
#endif /* SW_LEARNING */
#endif /* NPAPI_WANTED */
    UNUSED_PARAM (pVlanIcchUcastFdbSync);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIcchSendBulkReqMsg                               */
/*                                                                           */
/* Description        : This function sends bulk request message to the      */
/*                      remote node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSendBulkReqMsg (VOID)
{
    tIcchMsg             *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT2               u2OffSet = 0;

    /*
     *    VLAN Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (3)   |   3        |
     *    |-------------------------
     *
     */

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    if ((pMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_BULK_REQ_MSG_SIZE)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. Bulk Request not sent \n");
        return;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending ICCH Bulk request message \n");
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_BULK_UPD_REQUEST);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_ICCH_BULK_REQ_MSG_SIZE);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }

    return;
}


/*****************************************************************************/
/* Function Name      : VlanRedSendBulkUpdTailMsg                            */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanIcchSendBulkUpdTailMsg (VOID)
{
    tIcchMsg              *pMsg;
    tIcchProtoEvt         ProtoEvt;
    UINT2                 u2OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    ProtoEvt.u4AppId = ICCH_VLAN_APP_ID;
    ProtoEvt.u4Event = ICCH_BULK_UPDT_ABORT;


    /* Form a bulk update tail message.

     *<------------1 Byte----------><--2 Byte-->
     *********************************************************|
     *          |                                 *           |
     * ICCH Hdr | VLAN_ICCH_BULK_UPD_TAIL_MESSAGE *Msg Length |
     *          |                                 *           |
     **********************************************************
     */

     /* The ICCH Hdr shall be included by ICCH.
     */

    if ((pMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. Bulk Update Tail message not sent \n");
        ProtoEvt.u4Error = ICCH_MEMALLOC_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
        return VLAN_FAILURE;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending Bulk Update tail message \n");

    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_BULK_UPD_TAIL_MESSAGE);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_ICCH_BULK_UPD_TAIL_MSG_SIZE);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIcchCheckAndSendMsg                              */
/*                                                                           */
/* Description        : This function will check if the room size exists in  */
/*                      the input buffer pIcchMsg.                           */
/*                      If yes, the buffer is sent to ICCH.                  */
/*                      else, return VLAN_SUCCESS.                           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanIcchCheckAndSendMsg (tIcchMsg **ppIcchMsg, UINT2 u2MsgLen, UINT2 *pu2OffSet)
{
    tIcchProtoEvt ProtoEvt;
    UINT2         u2BufSize = VLAN_ICCH_MAX_MSG_SIZE;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));


    if ((u2BufSize - *pu2OffSet) < u2MsgLen)
    {
        /* no room for the current message */
        if (VlanIcchSendMsgToIcch (*ppIcchMsg, *pu2OffSet) == VLAN_FAILURE)
        {
            ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
            VlanIcchHandleProtocolEvent (&ProtoEvt);
        }          

        *ppIcchMsg = ICCH_ALLOC_TX_BUF (u2BufSize);

        if (*ppIcchMsg == NULL)
        {
            VLAN_TRC (VLAN_ICCH_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                    VLAN_NAME,
                    "ICCH alloc failed. Bulk updates not sent\n");
            ProtoEvt.u4Error = ICCH_MEMALLOC_FAIL;
            VlanIcchHandleProtocolEvent (&ProtoEvt);
            return VLAN_FAILURE;
        }
        *pu2OffSet = 0;

    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIcchProcessPeerDown                              */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      ICCH_PEER_DOWN from ICCH module.                     */
/*                      It flushes down all the FDB entries learnt on this   */
/*                      ICCL interface.                                      */
/*                                                                           */
/* Input(s)           : pVlanQMsg - Pointer to the VLAN Queue message.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessPeerDown (VOID)
{
    UINT4 u4IfIndex   = 0;
    UINT4 u4ContextId = 0;
    UINT2 u2Port      = 0;

    IcchGetIcclIfIndex (&u4IfIndex);
  
    if (VCM_SUCCESS == VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2Port))
    {
        if (0 != u2Port)
        {
#ifdef NPAPI_WANTED
            /* Flush all the remote learnt fdb entries on Peer down */
            VlanFlushRemoteFdb (VLAN_TRUE);
#endif /* NPAPI_WANTED */
        }        
    }

   VlanDelIsolationTblForIccl ();
   return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchSyncClearMacTable                             */
/*                                                                           */
/* Description        : This function sends clear request message to the     */
/*                      remote node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSyncClearMacTable ( VOID )
{
    tIcchMsg           *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT2               u2OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return;
    }

    /*
     *    VLAN FDB delete request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (7)   |   3        |
     *    |-------------------------
     *
     */
    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    if ((pMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_FLUSH_FDB_LEN)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. FDB delete request not sent \n");
        return;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending FDB bulk request message \n");

    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_SYNC_FDB_FLUSH );
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_ICCH_FLUSH_FDB_LEN);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, L2IWF_DEFAULT_CONTEXT);
    

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchSyncPortFlush                                */
/*                                                                           */
/* Description        : This function sends Port flush indication to peer    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSyncPortFlush (UINT4 u4IfIndex)
{
    
    tIcchMsg           *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT4               u4IcclIfIndex = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;

    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return;
    }


    /*
     *    Interface flush message
     *
     *    <- 1 byte ->|<- 2Bytes ->|<- 4Bytes ->|<- 4Bytes ->|
     *    ---------------------------------------------------|
     *    | Msg. Type |  Length    | Context Id |  IfIndex   | 
     *    |--------------------------------------------------
     *
     */

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);
    if (OSIX_TRUE == u1IsMclagEnabled)
    {
        return;
    }

    IcchGetIcclIfIndex (&u4IcclIfIndex);
    if (u4IcclIfIndex == u4IfIndex)
    {
        return;
    }

    if ((pMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_PORT_FLUSH_MSG_LEN)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. FDB delete request not sent \n");
        return;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending Interface down notification message \n");
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_SYNC_PORT_FLUSH);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_ICCH_PORT_FLUSH_MSG_LEN);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, L2IWF_DEFAULT_CONTEXT);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, u4IfIndex);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchSendPortDown                                */
/*                                                                           */
/* Description        : This function sends PortDown Indication to peer. This*/
/*                      is exclusively used for Sending MC-LAG PortChannel   */
/*                      Down.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSendPortDown (UINT4 u4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tIcchMsg           *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2UcastMsgLen = 0;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;

    MEMSET (au1IfName,0,sizeof(au1IfName));

    LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);   
    if (u1IsMclagEnabled == OSIX_FALSE)
    {
       return;
    }

    u2UcastMsgLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
                    VLAN_ICCH_LEN_FIELD_SIZE +
                    VLAN_ICCH_CONTEXT_FIELD_SIZE + 
                    CFA_MAX_PORT_NAME_LENGTH;

    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return;
    }

    /*
     *   VLAN ICCH Port Down Notification
     *
     *    <- 1 byte ->|<- 2Bytes ->|<- 4Bytes ->|<- NBytes ->|
     *    ---------------------------------------------------|
     *    | Msg. Type |  Length    | Context Id |   IfName   | 
     *    |--------------------------------------------------
     *
     */

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    if ((pMsg = ICCH_ALLOC_TX_BUF (u2UcastMsgLen)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. Interface down notification not sent \n");
        return;
    }

    VlanCfaGetInterfaceNameFromIndex (u4IfIndex,  au1IfName);

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending Interface down notification message \n");
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_SYNC_PORT_DOWN);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2UcastMsgLen);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, L2IWF_DEFAULT_CONTEXT);
    VLAN_ICCH_PUT_N_BYTE (pMsg, au1IfName,
                          &u2OffSet, CFA_MAX_PORT_NAME_LENGTH);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchCheckPortStatus                              */
/*                                                                           */
/* Description        : This function is used to send Check Port Status Msg. */
/*                      This is used exclusively to check the MC-LAG         */
/*                      PortChannel OperStatus in Peer Node.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchCheckPortStatus (UINT4 u4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tIcchMsg           *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT4               u4IcclIfIndex = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2UcastMsgLen = 0;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;

    MEMSET (au1IfName,0,sizeof(au1IfName));

    u2UcastMsgLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
                    VLAN_ICCH_LEN_FIELD_SIZE +
                    VLAN_ICCH_CONTEXT_FIELD_SIZE + 
                    CFA_MAX_PORT_NAME_LENGTH;

    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return;
    }

    /*
     *    Interface flush message
     *
     *    <- 1 byte ->|<- 2Bytes ->|<- 4Bytes ->|<- N Bytes ->|
     *    ---------------------------------------------------|
     *    | Msg. Type |  Length    | Context Id |  IfName    | 
     *    |--------------------------------------------------
     *
     */

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    /* Return in-case the MC-LAG is disabled on the Interface */
    LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);
    if (OSIX_FALSE == u1IsMclagEnabled)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "MCLAG is disabled on the Interface. \n");
        return;
    }

    /* Return in-case the Interface is ICCL Interface Index */
    IcchGetIcclIfIndex (&u4IcclIfIndex);
    if (u4IcclIfIndex == u4IfIndex)
    {
        return;
    }
 
    
    if ((pMsg = ICCH_ALLOC_TX_BUF (u2UcastMsgLen)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. Check Port state request not sent \n");
        return;
    }

    VlanCfaGetInterfaceNameFromIndex ( u4IfIndex,  au1IfName);

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending Check Port State message \n");
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_CHECK_PORT_STATUS);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2UcastMsgLen);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, L2IWF_DEFAULT_CONTEXT);
    VLAN_ICCH_PUT_N_BYTE (pMsg, au1IfName,
                          &u2OffSet, CFA_MAX_PORT_NAME_LENGTH);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }
    return;
}


/*****************************************************************************/
/* Function Name      : VlanIcchSyncVlanFlush                                 */
/*                                                                           */
/* Description        : This function sends Vlan Flush indication to peer    */
/*                                                                           */
/* Input(s)           : VlanId  - VLAN Id                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSyncVlanFlush (tVlanId VlanId)
{
    tIcchMsg           *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT2               u2OffSet = 0;

    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return;
    }


    /*
     *    Vlan Flush Message
     *
     *    <-- 1 Byte -->|<--2Bytes-->|<--4Bytes-->|<--2Bytes-->
     *    -----------------------------------------------------|
     *    |  Msg. Type  |   Length   | Context Id |   VlanId   | 
     *    |----------------------------------------------------|
     *
     */

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    if ((pMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_VLAN_FLUSH_MSG_LEN)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. FDB delete request not sent \n");
        return;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending Vlan down notification message \n");
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_SYNC_VLAN_FLUSH);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_ICCH_VLAN_FLUSH_MSG_LEN);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, L2IWF_DEFAULT_CONTEXT);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VlanId);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }
    return;
}


/*****************************************************************************/
/* Function Name      : VlanIcchSyncPortVlanFlush                             */
/*                                                                           */
/* Description        : This function sends Port-Vlan Flush Indication.      */
/*                                                                           */
/* Input(s)           : VlanId  - VLAN Id                                    */
/*                      u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchSyncPortVlanFlush (tVlanId VlanId, UINT4 u4IfIndex)
{
    tIcchMsg           *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT2               u2OffSet = 0;
    
    /* Get the BulkProto Sync State */
    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        return;
    }


    /*
     *    PortVlan flush Message
     *
     *    <--1 Byte-->|<--2Bytes-->|<--4Bytes-->|<--2Bytes-->|<--4Bytes-->|
     *    -----------------------------------------------------------------
     *    | Msg. Type |   Length   | ContextId  |  VlanId    | u4IfIndex  | 
     *    |----------------------------------------------------------------
     *
    */
    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    if ((pMsg = ICCH_ALLOC_TX_BUF (VLAN_ICCH_PORT_VLAN_FLUSH_LEN)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "ICCH alloc failed. FDB delete request not sent \n");
        return;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_ICCH_TRC,
                     CONTROL_PLANE_TRC, "Sending Interface Vlan down notification message \n");
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_SYNC_PORT_VLAN_FLUSH);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_ICCH_PORT_VLAN_FLUSH_LEN);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, L2IWF_DEFAULT_CONTEXT);
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, VlanId);
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, u4IfIndex);

    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        VlanIcchHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchProcessCheckPortStatus                       */
/*                                                                           */
/* Description        : This routine is called to check the port-Status which*/
/*                      is passed to it. This function will be hit on Master */
/*                      Node when Slave initiates a Check Port Status Message*/
/*                      The Master will  respond by sending Port Down message*/
/*                      only when the interface is down.                     */
/*                                                                           */
/* Input(s)           : tVlanIcchUcastFdbSync->au1IfName - Interface Name    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessCheckPortStatus (tVlanIcchUcastFdbSync *pVlanIcchUcastFdbSync)
{
    UINT4          u4IfIndex = 0;
    UINT1          u1IsMclagEnabled = OSIX_FALSE;
    UINT1          u1OperStatus = CFA_IF_DOWN;

    if (OSIX_SUCCESS ==
            CfaGetInterfaceIndexFromName (pVlanIcchUcastFdbSync->au1IfName,
                &u4IfIndex))
    {
        /* Return In-Case MC-LAG is not enabled on the Interface */ 
        LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);   
        
        if (u1IsMclagEnabled == OSIX_FALSE)
        {
            return;
        }
    }
    else
    {
        return;
    }

    if (VlanL2IwfGetBridgePortOperStatus (u4IfIndex, &u1OperStatus)
        == VLAN_SUCCESS)
    {
       /* Get the OperStatus on the AggIndex, and Send Port-Down Message
        * if the OperStatus is down and flush the Interface Index. 
        */  

        if (u1OperStatus == CFA_IF_DOWN)
        {
            VlanIcchSendPortDown (u4IfIndex);
            VlanFlushFdbForIfIndex (u4IfIndex, VLAN_LOCAL_FDB);
        }
    }

return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchProcessMclagOperUp                           */
/*                                                                           */
/* Description        : This routine is called when MC-LAG PortChannel comes */
/*                      UP. The FDB Entries in NP are once again changed to  */
/*                      MC-LAG PortChannel.                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - InterfaceIndex of MC-LAG PortChannel     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessMclagOperUp (UINT4 u4IfIndex)
{
#ifdef SW_LEARNING
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
    tVlanFdbInfo       VlanFdbInfo;
    tLocalPortList AllowedToGoPort;
    UINT4    u4ContextId = 0;
    UINT4    u4IcclIfIndex = 0;
    UINT2    u2LocalPort = 0;
    UINT2    u2TempVlan  = 0;
    UINT1    u1IsMclagEnabled = OSIX_FALSE;
    UINT1    u1Status = VLAN_DELETE_ON_TIMEOUT;
 

    MEMSET (AllowedToGoPort, 0, sizeof (tLocalPortList));
    MEMSET (&VlanFdbInfo, 0, sizeof (tVlanFdbInfo));

    /* Disable the MAC learning on MCLAG interface */
    VlanApiSetIcchPortMacLearningStatus(u4IfIndex,VLAN_DISABLED);

    IcchGetIcclIfIndex (&u4IcclIfIndex);
    LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);
    if (u1IsMclagEnabled == OSIX_FALSE)
    {
        VlanApiSetIcchPortMacLearningStatus (u4IfIndex,VLAN_ENABLED);
        return;
    }   

    VLAN_LOCK ();

    if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                &u4ContextId,
                &u2LocalPort) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        VlanApiSetIcchPortMacLearningStatus (u4IfIndex,VLAN_ENABLED);
        return;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        VlanApiSetIcchPortMacLearningStatus (u4IfIndex,VLAN_ENABLED);
        return;
    }

    VLAN_SET_MEMBER_PORT (AllowedToGoPort, u2LocalPort);

    VlanFdbInfo.u2RemoteId = VLAN_LOCAL_FDB;  
    pVlanFdbInfo = (tVlanFdbInfo *) RBTreeGetNext 
                   (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                   (tRBElem *) & VlanFdbInfo, NULL);

    while (NULL != pVlanFdbInfo)
    {
        if ((pVlanFdbInfo->u2RemoteId == VLAN_LOCAL_FDB)&&
	        (pVlanFdbInfo->u1EntryType != VLAN_FDB_MGMT)&&
            (pVlanFdbInfo->u2Port == u2LocalPort))
        {
            /* Flush the FDB Entry on ICCL and then program it
             * on over the respective MC-LAG port-channel.
             */ 
            VlanHwDelStaticUcastEntry (pVlanFdbInfo->u4FdbId,                                                       					 pVlanFdbInfo->MacAddr,
                                       VLAN_DEF_RECVPORT);


            if (VLAN_FAILURE == VlanHwAddStaticUcastEntry 
                                (pVlanFdbInfo->u4FdbId, pVlanFdbInfo->MacAddr, 
                                VLAN_DEF_RECVPORT, AllowedToGoPort,
                                u1Status, pVlanFdbInfo->ConnectionId))
            {
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                VlanApiSetIcchPortMacLearningStatus (u4IfIndex,VLAN_ENABLED);
                return;
            }
            /* Reseting the flag VLAN_MCLAG_FDB_ENTRY */
            pVlanFdbInfo->au1Pad[2]= 0;
            /* Source movement handling is required in ARP module for this 
             * movement of FDB entries from ICCL to MCLAG interface. 
             * To achieve the same, ArpNotifyL2IfStChg notification is triggered
             * for this FDB and the corresponding IVR interface.
             */
            if (u2TempVlan != pVlanFdbInfo->u4FdbId)
            {
                u4IfIndex = CfaGetVlanInterfaceIndexInCxt (u4ContextId, 
                                                           pVlanFdbInfo->u4FdbId);
                ArpNotifyL2IfStChg (u4IfIndex, 
                                    u4IcclIfIndex);
            }
            u2TempVlan = pVlanFdbInfo->u4FdbId;


        }    
        VlanFdbInfo.u4FdbId = pVlanFdbInfo->u4FdbId;
        MEMCPY(VlanFdbInfo.MacAddr,pVlanFdbInfo->MacAddr,sizeof(tMacAddr));
        VlanFdbInfo.u2RemoteId = pVlanFdbInfo->u2RemoteId;
        pVlanFdbInfo =(tVlanFdbInfo *) RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                (tRBElem *) & VlanFdbInfo, NULL);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    /* Enable the MAC learning on MCLAG interface */
    VlanApiSetIcchPortMacLearningStatus (u4IfIndex,VLAN_ENABLED);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchFormHitBitRequest                            */
/*                                                                           */
/* Description        : This function will Form Hit Bit Request and send     */
/*                      to ICCH.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVlanMcagEntries                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanIcchFormHitBitRequest ()
{
    tIcchProtoEvt       ProtoEvt;
    tIcchMsg             *pMsg = NULL;
    tVlanMcagEntries     *pVlanMcagEntry = NULL;
    tVlanMcagEntries     *pNextVlanMcagEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgLen = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    /* Form Hit Bit Request Message
     *
     * |------------------------------------------------------------------------------|
     * |Msg. Type |Length   |Context  |  No.of.Entries|Value     | Value    |   :     |
     * | (1 byte) |(2 bytes)|(4 Bytes)|    (4 Bytes)  |(8 bytes) | (8 bytes)|   :     |
     * |------------------------------------------------------------------------------|
     */
    /* Value will be calculated by below method:
     * value (8 bytes) = MAC Address (6) + vlan (2) */

     /* The ICCH Hdr shall be included by ICCH
      */
    u2MsgLen = VLAN_ICCH_TYPE_FIELD_SIZE + 
               VLAN_ICCH_LEN_FIELD_SIZE +
               VLAN_ICCH_CONTEXT_FIELD_SIZE + 
               VLAN_ICCH_LEN_NO_OF_ENTRIES +
               (gVlanMcagEntries.u4_Count * VLAN_ICCH_LEN_HIT_REQ_VALUE);

    if ((pMsg = ICCH_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        MCAG_TRC (CRITICAL_DEFAULT_TRC, "ICCH Memory allocation failed for "
                                         "Hit bit Request message.\r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pMsg, 0, sizeof (u2MsgLen));

    /* Filling Type in Hit Bit Request Message */
    VLAN_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_ICCH_HIT_BIT_REQUEST);

    /* Filling Length */
    VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);

    /* Filling Context ID */
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, u4ContextId);

    /* Filling Number of Aged out Entries carried in Hit Bit Request Message */
    VLAN_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, gVlanMcagEntries.u4_Count);

     pVlanMcagEntry =
        (tVlanMcagEntries *) VLAN_SLL_FIRST (&gVlanMcagEntries);

    while (pVlanMcagEntry != NULL)
    {
        pNextVlanMcagEntry = (tVlanMcagEntries *) VLAN_SLL_NEXT (&gVlanMcagEntries,
                           (tVLAN_SLL_NODE *) & pVlanMcagEntry->NextNode);

        /* Filling Value - Mac Address and vlan in Hit Bit Request Message */
        VLAN_ICCH_PUT_N_BYTE (pMsg, pVlanMcagEntry->FdbData.au1MacAddr,
                              &u2OffSet, VLAN_MAC_ADDR_LEN);
        VLAN_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, pVlanMcagEntry->FdbData.VlanId);

        /* Deleting the entries in Data structure */
        VLAN_SLL_DEL (&gVlanMcagEntries,
                      (tVLAN_SLL_NODE *) & pVlanMcagEntry->NextNode);

        VLAN_RELEASE_BUF (VLAN_MCAG_MAC_AGING_ENTRIES, (UINT1 *) pVlanMcagEntry);

        pVlanMcagEntry = pNextVlanMcagEntry;
    }

    /* Send Message to ICCH */
    if (VlanIcchSendMsgToIcch (pMsg, u2OffSet) == VLAN_FAILURE)
    {
       ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
       VlanIcchHandleProtocolEvent (&ProtoEvt);
    }

    return VLAN_SUCCESS;
}


/****************************************************************************/
/* Function Name      : VlanIcchProcessHitBitRequest                         */
/*                                                                           */
/* Description        : This function process Hit Bit Request Message from   */
/*                      remote node.                                         */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to Icch Message                       */
/*                      u2ReqOffSet - Offset to move the pointer             */
/*                      u4Entries - Total number of Mac entries present      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 VlanIcchProcessHitBitRequest(tIcchMsg *pMsg, UINT2 u2ReqOffSet, UINT4 u4Entries)
{
  
     tIcchProtoEvt              ProtoEvt;
     tIcchMsg      *pReplyMsg = NULL;
     UINT4         u4ContextId = 0;
     INT4          i4RetVal = VLAN_FAILURE;
     UINT2         u2Entries = (UINT2)u4Entries;
     UINT2         u2OffSet = 0;
     UINT2         u2MsgLen = 0;
     tVlanId       VlanId = 0;
     INT2          i2Temp = 0;

     tHwUnicastMacEntry HwUnicastEntry;
     tMacAddr      au1MacAddr;

     MEMSET (au1MacAddr,0,sizeof(tMacAddr));
     MEMSET (&HwUnicastEntry,0,sizeof(tHwUnicastMacEntry));
     MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    /* Form Hit Bit Reply Message
     *
     * |------------------------------------------------------------------------------|
     * |Msg. Type |Length   |Context  |  No.of.Entries|Value     | Value    |   :     |
     * | (1 byte) |(2 bytes)|(4 Bytes)|    (4 Bytes)  |(9 bytes) | (9 bytes)|   :     |
     * |------------------------------------------------------------------------------|
     */

    /* Value will be calculated by below method:
     * value (9 bytes) = MAC Address (6) + vlan (2) + Hit bit status (1) */

     /* The ICCH Hdr shall be included by ICCH
     */
     u2MsgLen = VLAN_ICCH_TYPE_FIELD_SIZE +
                VLAN_ICCH_LEN_FIELD_SIZE +
                VLAN_ICCH_CONTEXT_FIELD_SIZE + 
                VLAN_ICCH_LEN_NO_OF_ENTRIES +
                (u4Entries * VLAN_ICCH_LEN_HIT_REQ_VALUE);

     if ((pReplyMsg = ICCH_ALLOC_TX_BUF (u2MsgLen)) == NULL)
     {
         MCAG_TRC (CRITICAL_DEFAULT_TRC, "ICCH Memory allocation failed for "
                                         "Hit bit Reply message.\r\n");
         return VLAN_FAILURE;
     }

    /* Filling Type in Hit Bit Request Message */
    VLAN_ICCH_PUT_1_BYTE (pReplyMsg, &u2OffSet, VLAN_ICCH_HIT_BIT_REPLY);

    /* Filling Length */
    VLAN_ICCH_PUT_2_BYTE (pReplyMsg, &u2OffSet, u2MsgLen);

    /* Filling Context ID */
    VLAN_ICCH_PUT_4_BYTE (pReplyMsg, &u2OffSet, u4ContextId);

    /* Filling Number of Aged out Entries carried in Hit Bit Reply Message */
    VLAN_ICCH_PUT_4_BYTE (pReplyMsg, &u2OffSet, u4Entries);

    for (i2Temp=0;i2Temp<u2Entries;i2Temp++)
    {
        VLAN_ICCH_GET_N_BYTE (pMsg,au1MacAddr, &u2ReqOffSet,
                                             VLAN_MAC_ADDR_LEN);
        VLAN_ICCH_GET_2_BYTE (pMsg, &u2ReqOffSet, VlanId);

#ifdef NPAPI_WANTED
             /* Get the Mac entry and delete if present */
        i4RetVal = VlanHwGetFdbEntry (u4ContextId, (UINT4) VlanId, 
                                      au1MacAddr, &HwUnicastEntry);
               
        if (VLAN_FAILURE == VlanIcchCheckMacAddrOnMclag (au1MacAddr, VlanId))
        {
             /* If VLAN_MCLAG_FDB_ENTRY is set on the VlanFdbInfo, 
              * We have to flush the Fdb Entries and send Hit Bit Reply
              */ 
             i4RetVal = VLAN_SUCCESS;
             HwUnicastEntry.u1HitStatus = OSIX_FALSE;
        }
#endif
        if ((i4RetVal == VLAN_SUCCESS) && 
            (HwUnicastEntry.u1HitStatus == OSIX_FALSE))
        {
            /* Delete the entry in FDB which was added as a dynamic entry */
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
            VlanFdbRemoveLocalOrRemote(VlanId, au1MacAddr);
#endif
#endif
            VlanHwDelStaticUcastEntry((UINT4) VlanId, au1MacAddr, VLAN_INIT_VAL);
        }

        /* Filling Value - Mac Address and vlan in Hit Bit Reply Message */
        VLAN_ICCH_PUT_N_BYTE (pReplyMsg, au1MacAddr,&u2OffSet, VLAN_MAC_ADDR_LEN);

        VLAN_ICCH_PUT_2_BYTE (pReplyMsg, &u2OffSet, VlanId);

        /* Filling Hit bit status */
        VLAN_ICCH_PUT_1_BYTE (pReplyMsg, &u2OffSet, HwUnicastEntry.u1HitStatus);

    }

    if(VlanIcchSendMsgToIcch (pReplyMsg, u2OffSet) == VLAN_FAILURE)
    {
       VlanIcchHandleProtocolEvent (&ProtoEvt); 
       ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
    }

    return VLAN_SUCCESS;
}


/****************************************************************************/
/* Function Name      : VlanIcchProcessHitBitReply                           */
/*                                                                           */
/* Description        : This function process Hit Bit Reply Message from     */
/*                      remote node.                                         */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to Icch Message                       */
/*                      u2OffSet - Offset to move the pointer                */
/*                      u4Entries - Total number of Mac entries present      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4 VlanIcchProcessHitBitReply(tIcchMsg *pMsg,UINT2 u2OffSet,UINT4 u4Entries)
{

     tMacAddr      au1MacAddr;
     tVlanId       VlanId = 0;
     UINT2          u2Loop = (UINT2)u4Entries;
     INT2           i2Temp = 0;
     UINT1          u1HitStatus = 0;

     MEMSET (au1MacAddr,0,sizeof(tMacAddr));

     for (i2Temp=0;i2Temp<u2Loop;i2Temp++)
     {
        VLAN_ICCH_GET_N_BYTE (pMsg,au1MacAddr, &u2OffSet,
                                             VLAN_MAC_ADDR_LEN);
        VLAN_ICCH_GET_2_BYTE (pMsg, &u2OffSet, VlanId);

        VLAN_ICCH_GET_1_BYTE (pMsg, &u2OffSet, u1HitStatus);

        if (u1HitStatus == OSIX_FALSE)
        {
            /* In remote node, the hit bit status of the entry is false so
             * Delete the entry in FDB which was added as a dynamic entry */
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
            VlanFdbRemoveLocalOrRemote(VlanId,au1MacAddr);
#endif
#endif
            VlanHwDelStaticUcastEntry((UINT4)VlanId,au1MacAddr,VLAN_INIT_VAL);
        }
     }
     return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanIcchProcessMCLAGEnableStatus                     */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      MCLAG_ENABLED from ICCH module.                      */
/*                      It flushes down all the FDB entries learnt on this   */
/*                      ICCL interface and deletes the entries in the port   */
/*						isolation table.									 */
/*                                                                           */
/* Input(s)           : pVlanQMsg - Pointer to the VLAN Queue message.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessMCLAGEnableStatus (VOID)
{
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId            VlanId = 0;

    VlanAddIsolationTblForIccl ();

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            continue;
        }

        VlanIvrComputeVlanMclagIfOperStatus (pCurrEntry);
    }

    /* Get the BulkProto Sync State */
    if (OSIX_SUCCESS == IcchApiCheckProtoSyncEnabled ())
    {
        VlanIcchSendBulkUpdates ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchProcessMCLAGDisableStatus                           */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      MCLAG_DISABLED from ICCH module.                     */
/*                      It flushes down all the FDB entries learnt on this   */
/*                      ICCL interface and deletes the entries in the port   */
/*						isolation table.									 */
/*                                                                           */
/* Input(s)           : pVlanQMsg - Pointer to the VLAN Queue message.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessMCLAGDisableStatus (VOID)
{
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId            VlanId = 0;

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            continue;
        }

        VlanIvrComputeVlanMclagIfOperStatus (pCurrEntry);
    }

    /* Deletes all the FDB entries on the ICCL interface */
    VlanIcchProcessPeerDown ();
    return;
}

/*****************************************************************************/
/* Function Name      : VlanDelIsolationTblForIccl                           */
/*                                                                           */
/* Description        : This routine is called to delete the port isolation  */
/*                      table .                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanDelIsolationTblForIccl (VOID)
{
    tIssUpdtPortIsolation PortIsolation;
    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
 
    IcchGetIcclIfIndex (&(PortIsolation.u4IngressPort));

    PortIsolation.InVlanId = 0;
    PortIsolation.u1Action = ISS_PI_DELETE;
    VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanIcchProcessMclagDown                             */
/*                                                                           */
/* Description        : This routine is called when MC-LAG PortChannel goes  */
/*                      down in Master Node with PortChannel IfIndex and the */
/*                      PortChannel Status in peer.                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - InterfaceIndex of MC-LAG PortChannel     */
/*                      u1Status - MC-LAG PortChannel Status in Peer Node    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanIcchProcessMclagDown (UINT4 u4IfIndex, UINT1 u1Status)
{

   /* Delete the FDB Entries learnt on the MC-LAG PortChannel if the MC-LAG
    * PortChannel is down on the remote side. Also send a PortDown Message to
    * the Peer to help flush the FDB Entries on MC-LAG PortChannel
    */ 

    if (VLAN_LA_PORT_DOWN == u1Status)
    {
        VlanDeleteFdbEntries (u4IfIndex, VLAN_NO_OPTIMIZE);
        VlanIcchSendPortDown (u4IfIndex);
        return;
    }
    
    if (VLAN_TRUE  != VlanLaIsMclagInterface (u4IfIndex))
    {
       return;
    }

    if (VLAN_LA_PORT_UP_IN_BNDL == u1Status)
    {
    /* If MC-LAG PortChannel is UP in peer then Change the port in FDB Entry 
     * with ICCL Interface so as to avoid traffic getting flooded.
     */
        if (VLAN_FAILURE == VlanIcchMoveFdbtoIccl (u4IfIndex))
        {
            return;
        }
    }

    return;
}


/*****************************************************************************/
/* Function Name      : VlanIcchCheckMacAddrOnMclag                          */
/*                                                                           */
/* Description        : This routine is called to check if a FDB Entry is    */
/*                      programmed as dynamic on h/w over MC-LAG PO or on    */
/*                      ICCL as static. This routine will be invoked from    */
/*                      process HIT Bit Request so as to achieve uniform MAC */
/*                      Aging.                                               */
/*                                                                           */
/* Input(s)           : MacAddr   - MAC Address                              */
/*                      VlanId    - VLAN Id of the FDB Entry                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/

INT4 
VlanIcchCheckMacAddrOnMclag (tMacAddr MacAddr,tVlanId VlanId)
{
#if defined (NPAPI_WANTED) && (SW_LEARNING)
    tVlanFdbInfo       VlanFdbInfo;
    tVlanFdbInfo       *pVlanFdbInfo = NULL;

    MEMSET (&VlanFdbInfo, 0 ,sizeof(tVlanFdbInfo));

    VlanFdbInfo.u4FdbId = (UINT4)VlanId;
    MEMCPY (VlanFdbInfo.MacAddr, MacAddr, sizeof(tMacAddr));
    VlanFdbInfo.u2RemoteId = VLAN_LOCAL_FDB;

    pVlanFdbInfo =(tVlanFdbInfo *) RBTreeGet
                  (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                  (tRBElem *) & VlanFdbInfo);
    if (pVlanFdbInfo == NULL)
    {
       return VLAN_FAILURE;
    }

    /* If this bit is set to VLAN_MCLAG_FDB_ENTRY Then the FDB Entry is 
     * programmed as static on h/w and should be removed. 
     */   

    if (pVlanFdbInfo->au1Pad[2] == VLAN_MCLAG_FDB_ENTRY)
    {
       return VLAN_FAILURE;
    }
#endif
UNUSED_PARAM (MacAddr);
UNUSED_PARAM (VlanId);
return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIcchHandleMclagOperStatusChange                          */
/*                                                                           */
/* Description        : This routine is used to set a bit of pvlanfdbinfo    */
/*                      with VLAN_MCLAG_FDB_ENTRY so as to denote that the   */
/*                      entry is present as h/w over ICCL.                   */
/*                                                                           */
/* Input(s)           : MacAddr   - MAC Address                              */
/*                      VlanId    - VLAN Id of the FDB Entry                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID 
VlanIcchHandleMclagOperStatusChange (tMacAddr MacAddr,tVlanId VlanId)
{
#if defined (NPAPI_WANTED) && (SW_LEARNING)
    tVlanFdbInfo       VlanFdbInfo;
    tVlanFdbInfo       *pVlanFdbInfo = NULL;

    MEMSET (&VlanFdbInfo, 0 ,sizeof(tVlanFdbInfo));

    VlanFdbInfo.u4FdbId = (UINT4)VlanId;
    MEMCPY (VlanFdbInfo.MacAddr, MacAddr, sizeof(tMacAddr));
    VlanFdbInfo.u2RemoteId = VLAN_LOCAL_FDB;

    pVlanFdbInfo =(tVlanFdbInfo *) RBTreeGet
                  (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                  (tRBElem *) & VlanFdbInfo);
    if (pVlanFdbInfo == NULL)
    {
       return;
    }

    /* Set the au1Pad[2] of pVlanFdbInfo to VLAN_MCLAG_FDB_ENTRY 
     */   
    pVlanFdbInfo->au1Pad[2] = VLAN_MCLAG_FDB_ENTRY;

#endif
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    return;
}
#endif  /*_VLANICCH_C_*/
