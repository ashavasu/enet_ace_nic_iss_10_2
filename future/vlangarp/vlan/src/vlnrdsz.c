#define _VLANREDSZ_C
#include "vlaninc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
INT4  VlanredSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < VLANRED_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsVLANREDSizingParams[i4SizingId].u4StructSize,
                          FsVLANREDSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(VLANREDMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            VlanredSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   VlanredSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsVLANREDSizingParams); 
      return OSIX_SUCCESS; 
}


VOID  VlanredSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < VLANRED_MAX_SIZING_ID; i4SizingId++) {
        if(VLANREDMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( VLANREDMemPoolIds[ i4SizingId] );
            VLANREDMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
