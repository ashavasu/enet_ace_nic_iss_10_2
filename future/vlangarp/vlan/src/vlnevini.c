
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevini.c,v 1.9 2016/08/19 11:07:58 siva Exp $
 *
 * Description: This file contains EVB Start up functions. 
 *
 *******************************************************************/
#ifndef __VLNEVINI_C__
#define __VLNEVINI_C__

#include "vlaninc.h"

static UINT1 gu1GlobalsInitStatus = OSIX_FALSE;

/*****************************************************************************/
/* Function Name      : VlanEvbStart                                         */
/*                                                                           */
/* Description        :This function does the following tasks:               */
/*                     - creates the context data base for u4ContextId.      */
/*                     For the first context creation in EVB, the below tasks*/
/*	                   - Initializes the global variables.                   */
/*	                   - Registers EVB MIBs with SNMP module.                */
/*	                   - Creates memory for EVB context info table, UAP      */
/*                       interface table and S-Channel interface tables.     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if initialization is successful         */
/*                      VLAN_FAILURE otherwise                               */
/*****************************************************************************/
INT4
VlanEvbStart (UINT4 u4ContextId)
{
    tL2IwfCfaIfTypeDenyProt L2IwfCfaIfTypeDenyProt;
    tEvbContextInfo         *pEvbContextEntry = NULL;
    INT4                    i4ProtoIndex      = L2IWF_PROTOCOL_ID_PNAC;

    /* This is called from SNMP and VLAN init , default context creation*/
    /*
    if ((VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE) ||
            (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED))
    {
        VLAN_TRC (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                 "VlanEvbStart: VLAN is shutdown/disabled on this context."
                 " Can not enable EVB\r\n");
        return VLAN_FAILURE;
    } */
    /* UAP port creation/deletion is allowed only for DCBX, LLDP, VLAN,
     * STP (BPDU Gaurd), QOS, ISS and ACL, LLDP-MED, RMONv2, 
     * modules. Adding the rest of the modules in deny list.
     *
     * SBP port creation/deletion is allowed only for VLAN, IGS and FIPS
     * modules. Adding the rest of the modules in deny list. */

    VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC, VLAN_NAME,  
                   "VlanEvbStart: Function start for context %d\r\n", 
                    u4ContextId);

    while (i4ProtoIndex < L2IWF_PROTOCOL_ID_MAX)
    {
        MEMSET (&L2IwfCfaIfTypeDenyProt, 0, sizeof (tL2IwfCfaIfTypeDenyProt));
        L2IwfCfaIfTypeDenyProt.u4ContextId = u4ContextId;
        L2IwfCfaIfTypeDenyProt.i4Protocol = i4ProtoIndex;
        L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_SET_RS_CRT;

        if (!((i4ProtoIndex == L2IWF_PROTOCOL_ID_LLDP) ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_VLAN) ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_XSTP) ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_QOS)  ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_PNAC)))
        {
            L2IwfCfaIfTypeDenyProt.i4IfType = CFA_ENET;
            L2IwfCfaIfTypeDenyProt.i4BrgPortType = CFA_UPLINK_ACCESS_PORT;

            if (L2IwfIfDenyProtocolEntry(&L2IwfCfaIfTypeDenyProt) ==
                    L2IWF_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                               VLAN_NAME, "VlanEvbStart: UAP port create/delete"
                               " deny list addition failed for context %d and "
                               "protocol %d\r\n", u4ContextId, i4ProtoIndex);
                /* For the rest of the modules, entries would be stagnent.
                 * do require proper clean up here. */
                return VLAN_FAILURE;
            }
        }
        
        if (!((i4ProtoIndex == L2IWF_PROTOCOL_ID_VLAN) ||
              (i4ProtoIndex == L2IWF_PROTOCOL_ID_SNOOP)))
        {
            L2IwfCfaIfTypeDenyProt.i4IfType = CFA_BRIDGED_INTERFACE;
            L2IwfCfaIfTypeDenyProt.i4BrgPortType=CFA_STATION_FACING_BRIDGE_PORT;

            if (L2IwfIfDenyProtocolEntry(&L2IwfCfaIfTypeDenyProt) ==
                    L2IWF_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                               VLAN_NAME, "VlanEvbStart: SBP port create/delete"
                               " deny list addition failed for context %d and "
                               "protocol %d\r\n", u4ContextId, i4ProtoIndex);
                /* For the rest of the modules, entries would be stagnent.
                 * do require proper clean up here. */
                return VLAN_FAILURE;
            }
        }
        i4ProtoIndex += 1;
    }

    if (gu1GlobalsInitStatus == OSIX_FALSE)
    {
        /*RB tree creation for UAP Interface Table */
        if (VlanEvbUapIfTblCreate () == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbStart: RB Tree creation for "
                      "gEvbGlobalInfo.EvbUapIfTree failed\r\n")
            return VLAN_FAILURE;
        }

        /* RB Trees creation for S-Channel Interface Table */    
        if (VlanEvbSChIfTblCreate () == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbStart: RB Tree creation for "
                      "S-Channel Interface Table(s) failed\r\n")
            return VLAN_FAILURE;
        }
        /* Memset for Context data base */
        MEMSET (gEvbGlobalInfo.apEvbContextInfo, 0,
                ((sizeof (tEvbContextInfo *)) * SYS_DEF_MAX_NUM_CONTEXTS));

        /* Initialize memory pools */
        gEvbGlobalInfo.EvbCxtPoolId =
            VLANMemPoolIds[MAX_VLAN_EVB_CONTEXT_INFO_SIZING_ID];
        gEvbGlobalInfo.EvbUapIfPoolId =
            VLANMemPoolIds[MAX_VLAN_EVB_UAP_ENTRIES_SIZING_ID];
        gEvbGlobalInfo.EvbSChIfPoolId =
            VLANMemPoolIds[MAX_VLAN_EVB_SCH_ENTRIES_SIZING_ID];
        gEvbGlobalInfo.EvbQueIfPoolId =
            VLANMemPoolIds[MAX_VLAN_EVB_QUE_ENTRIES_SIZING_ID];

        /* Intializing number of external ports available */
        gEvbGlobalInfo.u4EvbSysNumExternalPorts = 
                                    SYS_DEF_MAX_PHYSICAL_INTERFACES - SYS_DEF_MAX_INFRA_SYS_PORT_COUNT;

        /* Registers EVB MIBs with SNMP module */
        RegisterSTD1EV ();
        RegisterFSMID1 ();
        RegisterSTD1LL ();

        /* Setting up gu1GlobalsInitStatus */
        gu1GlobalsInitStatus = OSIX_TRUE;
    }

    /* Creates memory for EVB Context Information */
    pEvbContextEntry = (tEvbContextInfo *) (VOID *) VLAN_EVB_GET_BUF
                        (VLAN_EVB_CONTEXT_ENTRY, sizeof (tEvbContextInfo));

    if (pEvbContextEntry == NULL)
    {
       VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC | OS_RESOURCE_TRC | 
                      ALL_FAILURE_TRC, VLAN_NAME, "VlanEvbStart: Memory "
                      "allocation for Context %d failed\r\n", u4ContextId);
       return VLAN_FAILURE;
    }

    MEMSET (pEvbContextEntry, 0, sizeof (tEvbContextInfo));

    /* Setting default values for EVB Context Information */
    pEvbContextEntry->u4EvbSysCxtId = u4ContextId;
    pEvbContextEntry->i4EvbSysRowStatus = ACTIVE;
    pEvbContextEntry->i4EvbSysCtrlStatus = VLAN_EVB_DEF_SYSTEM_STATUS; 
    pEvbContextEntry->i4EvbSysModStatus = VLAN_EVB_DEF_MODULE_STATUS;
    pEvbContextEntry->i4EvbSysTrapStatus = VLAN_EVB_DEF_TRAP_STATUS;
    pEvbContextEntry->i4EvbSysSChMode = VLAN_EVB_DEF_SCH_MODE;

    /* Since EVB is coupled with VLAN traces, the following EVB traces
     * are initialized. This will be helpful when MSR restoring since
     * we should not store EVB traces */
    VLAN_DBG_FLAG = (UINT4) ((INT4)VLAN_DBG_FLAG & (~VLAN_EVB_TRC));
    VLAN_DBG_FLAG = (UINT4) ((INT4)VLAN_DBG_FLAG & (~VLAN_TRC_TYPE_ALL));
    /* Adding the context memory in EVB Context Data Base */
    gEvbGlobalInfo.apEvbContextInfo[u4ContextId] = pEvbContextEntry;
    VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC ,VLAN_NAME, 
              "VlanEvbStart: Function End with ContextId %d\n",u4ContextId);

    return VLAN_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanEvbShutdown                                      */
/*                                                                           */
/* Description        :This function does the following tasks:               */
/*                     - deletes the context data base for u4ContextId.      */
/*                     For the last context creation in EVB, the below tasks */
/*	                   - Initializes the global variables.                   */
/*	                   - De-Registers EVB MIBs with SNMP module.             */
/*	                   - Deletes memory for EVB context info table, UAP      */
/*                       interface table and S-Channel interface tables.     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID 
VlanEvbShutdown (UINT4 u4ContextId)
{
    /* This is called from SNMP and VLAN deinit , default context delete*/
    tL2IwfCfaIfTypeDenyProt L2IwfCfaIfTypeDenyProt;
    tEvbContextInfo         *pEvbContextEntry = NULL;
    UINT4                   u4Index           = 0;
    UINT1                   u1EvbExist        = OSIX_FALSE;
    INT4                    i4ProtoIndex      = L2IWF_PROTOCOL_ID_PNAC;
    INT4                    i4IfIndex      = 1;
    tEvbUapIfEntry          *pEvbUapIfEntry = NULL;
    VlanEvbDisable (u4ContextId);
    /* Getting the EVB context entry from the data base */
    pEvbContextEntry = gEvbGlobalInfo.apEvbContextInfo[u4ContextId];
 
   VLAN_TRC_ARG1(VLAN_EVB_TRC,INIT_SHUT_TRC,VLAN_NAME,"VlanEvbShutdown:"
          "Function Start with ContextId %d\r\n",u4ContextId);
      
    while (i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        pEvbUapIfEntry = VlanEvbUapIfGetEntry((UINT4)i4IfIndex); 
        if (pEvbUapIfEntry != NULL)
        {
            if ( pEvbUapIfEntry->u4UapIfCompId == u4ContextId )
            { 
                VlanPortCfaSetIfMainBrgPortType (i4IfIndex,
                            VLAN_CUSTOMER_BRIDGE_PORT);
            }
        }
        i4IfIndex = i4IfIndex + 1;
    }   
    /* Resets and release memory for EVB Context Information */
    gEvbGlobalInfo.apEvbContextInfo[u4ContextId] = NULL;
    VLAN_EVB_RELEASE_BUF (VLAN_EVB_CONTEXT_ENTRY, (VOID *)pEvbContextEntry);
   
    /* Check whether EVB is running on any other context */
    while (u4Index < SYS_DEF_MAX_NUM_CONTEXTS)
    {
        if (gEvbGlobalInfo.apEvbContextInfo[u4Index] != NULL)
        {
            /* EVB is running on some other context */
            u1EvbExist = OSIX_TRUE;
            break;
        }
        u4Index++;
    }

    /* UAP port creation/deletion is allowed only for DCBX, LLDP, VLAN,
     * STP (BPDU Gaurd), QOS, ISS and ACL, LLDP-MED, RMONv2, 
     * modules. Deny list had added for the rest of the modules and they 
     * have to be now deleted.
     *
     * SBP port creation/deletion is allowed only for VLAN, IGS and FIPS
     * modules. Deny list had added for the rest of the modules and they 
     * have to be now deleted. */

    while (i4ProtoIndex < L2IWF_PROTOCOL_ID_MAX)
    {
        MEMSET (&L2IwfCfaIfTypeDenyProt, 0, sizeof (tL2IwfCfaIfTypeDenyProt));
        L2IwfCfaIfTypeDenyProt.u4ContextId = u4ContextId;
        L2IwfCfaIfTypeDenyProt.i4Protocol = i4ProtoIndex;
        L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_SET_RS_DES;

        if (!((i4ProtoIndex == L2IWF_PROTOCOL_ID_LLDP) ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_VLAN) ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_XSTP) ||
                    (i4ProtoIndex == L2IWF_PROTOCOL_ID_QOS)))
        {
            L2IwfCfaIfTypeDenyProt.i4IfType = CFA_ENET;
            L2IwfCfaIfTypeDenyProt.i4BrgPortType = CFA_UPLINK_ACCESS_PORT;

            if (L2IwfIfDenyProtocolEntry(&L2IwfCfaIfTypeDenyProt) ==
                    L2IWF_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbShutdown: UAP port create/delete"
                               " deny list addition failed for context %d and "
                               "protocol %d\r\n", u4ContextId, i4ProtoIndex);
            }
        }
        
        if (!((i4ProtoIndex == L2IWF_PROTOCOL_ID_VLAN) ||
              (i4ProtoIndex == L2IWF_PROTOCOL_ID_SNOOP)))
        {
            L2IwfCfaIfTypeDenyProt.i4IfType = CFA_BRIDGED_INTERFACE;
            L2IwfCfaIfTypeDenyProt.i4BrgPortType=CFA_STATION_FACING_BRIDGE_PORT;

            if (L2IwfIfDenyProtocolEntry(&L2IwfCfaIfTypeDenyProt) ==
                    L2IWF_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "VlanEvbShutdown: SBP port create/delete"
                               " deny list addition failed for context %d and "
                               "protocol %d\r\n", u4ContextId, i4ProtoIndex);
            }
        }
        i4ProtoIndex += 1;
    }

    /* This is the last context where EVB is running and in this context 
     * also, EVB is stopped now. So delete the RBTrees and memory pools. 
     */
    if ((gu1GlobalsInitStatus == OSIX_TRUE) && (u1EvbExist == OSIX_FALSE))
    {
        /* RB Trees deletion and their entries deletion for S-Channel Interface
         * Table. */
        VlanEvbSChIfTblDelete ();

        /*RB tree deletion and its entries deletion for UAP Interface Table */
        VlanEvbUapIfTblDelete ();

        /* Memset for Context data base */
        MEMSET (gEvbGlobalInfo.apEvbContextInfo, 0,
                ((sizeof (tEvbContextInfo *)) * SYS_DEF_MAX_NUM_CONTEXTS));

        /* Initialize memory pools */
        gEvbGlobalInfo.EvbCxtPoolId = 0;
        gEvbGlobalInfo.EvbUapIfPoolId = 0;
        gEvbGlobalInfo.EvbSChIfPoolId = 0;
        gEvbGlobalInfo.EvbQueIfPoolId = 0;

        /* Intializing number of external ports available */
        gEvbGlobalInfo.u4EvbSysNumExternalPorts = 0; 

        /* De-Registers EVB MIBs with SNMP module */
        UnRegisterSTD1EV ();
        UnRegisterFSMID1 ();
        UnRegisterSTD1LL ();

        /* Setting up gu1GlobalsInitStatus */
        gu1GlobalsInitStatus = OSIX_FALSE;
    }
    VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC ,VLAN_NAME, "VlanEvbShutdown:"
          "Function End with ContextId %d\r\n",u4ContextId);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbEnable                                        */
/*                                                                           */
/* Description        : This function does the following tasks:-             */
/*                      - Indicates to create SBP ports to CFA.              */
/*                                                                           */
/*                      Static:-                                             */
/*                      -------                                              */
/*                      1. Programs S-Channel Entries in Hardware.           */
/*                      2. Sets S-Channel entries oper status - UP.          */
/*                                                                           */
/*                      Dynamic/Hyrbid:                                      */
/*                      ---------------                                      */
/*                      1. Registers and updates LLDP to TX CDCP TLV.        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID 
VlanEvbEnable (UINT4 u4ContextId)
{
     tEvbUapIfEntry *pUapIfEntry = NULL;
     tEvbUapIfEntry *pNextUapIfEntry = NULL;
     UINT1          u1OperStatus = 0;

     pUapIfEntry = VlanEvbUapIfGetFirstEntry ();
    
     VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC ,VLAN_NAME, "VlanEVbEnable:"
                     "Function Start with ContextId %d\n",u4ContextId);
     while(pUapIfEntry != NULL)
     {
         /* In this while loop checks, no return here. enabling EVB on
          * other UAP ports is continued, though one or some of the entries are 
          * not able to be enabled. */
         pNextUapIfEntry = VlanEvbUapIfGetNextEntry (pUapIfEntry->u4UapIfIndex);

         if (pUapIfEntry->u4UapIfCompId != u4ContextId)
         {
             pUapIfEntry = pNextUapIfEntry;
             continue;
         }
         /* If CDCP is enabled in dynamic/hybrid case, register with LLDP.*/
         if (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE)
         {
             if (VlanEvbUapEnableCdcpStatus (pUapIfEntry) == VLAN_FAILURE)
             {
                 VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, 
                            VLAN_NAME, "VlanEvbEnable: Setting CDCP status on "
                             "Context %d UAP port %d is failed\r\n",
                             u4ContextId, pUapIfEntry->u4UapIfIndex);
             }
         }
         /* Retreiving Oper status of the UAP */
         u1OperStatus = 0;
         VlanEvbUapGetOperStatus (pUapIfEntry->u4UapIfIndex, &u1OperStatus);

         if (u1OperStatus == CFA_IF_UP)
         {
             /* Handling for static/dynamic/hybrid cases are done */
             if (VlanEvbUapSetOperStatusUp (pUapIfEntry) == VLAN_FAILURE)
             {
                 VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, 
                         VLAN_NAME, "VlanEvbEnable: Failed in "
                         "setting VlanEvbUapSetOperStatusUp for"
                         "Context %d UAP port %d \r\n",
                         u4ContextId, pUapIfEntry->u4UapIfIndex);
             }
             /* This function is called to set the S-Channel Oper status for 
              * CFA and EVB Database , Static mode all the Created S-channels 
              * are made UP in CFA and EVB ,In Dynamic/Hybrid - CDCP 
              * enable Default S-channel alone is moved to UP State in CFA
              * and EVB , Non Default S-Channel will be taken Care In 
              * Packet Processing for Modes Respectively */
             VlanEvbSChUpdateCfaOperStatus (pUapIfEntry,CFA_IF_UP);
         }
         pUapIfEntry = pNextUapIfEntry;
     } /* End of while */
     VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC ,VLAN_NAME, "VlanEVbEnable:"
                     "Function End with Context ID %d\n", u4ContextId);
     return;
}


/*****************************************************************************/
/* Function Name      : VlanEvbDisable                                       */
/*                                                                           */
/* Description        : This function does the following tasks:-             */
/*                      - Indicates to delete SBP ports to CFA.              */
/*                      - Deletes S-Channel Entries from Hardware.           */
/*                                                                           */
/*                      Static:-                                             */
/*                      -------                                              */
/*                      - Sets S-Channel entries oper status - DOWN.         */
/*                                                                           */
/*                      Dynamic:-                                            */
/*                      ---------                                            */
/*                      - S-Channel entries in control plane removed.        */
/*                                                                           */
/*                      Dynamic/Hyrbid:                                      */
/*                      ---------------                                      */
/*                      - De-Registers and updates LLDP to stop TX CDCP TLV. */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID 
VlanEvbDisable (UINT4 u4ContextId)
{
     tEvbUapIfEntry *pUapIfEntry = NULL;
     tEvbUapIfEntry *pNextUapIfEntry = NULL;

     pUapIfEntry = VlanEvbUapIfGetFirstEntry ();
     
      VLAN_TRC_ARG1(VLAN_EVB_TRC,INIT_SHUT_TRC,VLAN_NAME,"VlanEVBDisable:"
                      "Function Start with ContextId %d\r\n",u4ContextId);


     while(pUapIfEntry != NULL)
     {
         /* In this while loop checks, no return here. Disabling EVB on
          * other UAP ports is continued, though one or some of the entries are 
          * not able to be disabled. */
         pNextUapIfEntry = VlanEvbUapIfGetNextEntry (pUapIfEntry->u4UapIfIndex);

         if (pUapIfEntry->u4UapIfCompId != u4ContextId)
         {
             pUapIfEntry = pNextUapIfEntry;
             continue;
         }
         /* If CDCP is enabled in dynamic/hybrid case, register with LLDP.*/
         if (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE)
         {
             if (VlanEvbUapDisableCdcpStatus (pUapIfEntry) == VLAN_FAILURE)
             {
                 VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, 
                            VLAN_NAME, "VlanEvbDisable: Setting CDCP status on "
                             "Context %d UAP port %d is failed\r\n",
                             u4ContextId, pUapIfEntry->u4UapIfIndex);
             }
         }
         else
		 {
			 if (VlanEvbUapSetOperStatusDown (pUapIfEntry) == VLAN_FAILURE)
			 {
				 VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, 
						 VLAN_NAME, "VlanEvbDisable: Disabling EVB on "
						 "Context %d UAP port %d is failed\r\n",
						 u4ContextId, pUapIfEntry->u4UapIfIndex);
             }
         }

         /* This function is called to set the S-Channel Oper status for 
          * CFA and EVB Database,for staic mode all the S-channel's are 
          * moved to down in CFA and EVB , Dynamic/Hybrid - CDCP 
          * enable Default S-channel is moved to Down State ,
          * Non-Default S-channels would have benn taken care in 
          * VlanEvbUapDisableCdcpStatus*/
         VlanEvbSChUpdateCfaOperStatus (pUapIfEntry,CFA_IF_DOWN);

         pUapIfEntry = pNextUapIfEntry;
     } /* End of while */
      VLAN_TRC_ARG1 (VLAN_EVB_TRC,INIT_SHUT_TRC,VLAN_NAME,"VlanEvbDisable:"
                      "Function End with Context Id %d\r\n", u4ContextId);
     return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbClearStats                                */
/*                                                                           */
/*    Description         : This function is used to clear the EVB statistics*/
/*                          for the given context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanEvbClearStats (UINT4 u4ContextId)
{
     tEvbUapIfEntry *pUapIfEntry = NULL;
     tEvbUapIfEntry *pNextUapIfEntry = NULL;

     pUapIfEntry = VlanEvbUapIfGetFirstEntry ();

     while(pUapIfEntry != NULL)
     {
         pNextUapIfEntry = VlanEvbUapIfGetNextEntry (pUapIfEntry->u4UapIfIndex);

         if (pUapIfEntry->u4UapIfCompId != u4ContextId)
         {
             pUapIfEntry = pNextUapIfEntry;
             continue;
         }
         /* NOTE: do require to clear stats from HW? */
         MEMSET (&(pUapIfEntry->UapStats), 0, sizeof (tEvbUapStatsEntry));
         pUapIfEntry = pNextUapIfEntry;
     }
     return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbGetBuf                                    */
/*                                                                           */
/*    Description         : This function allocates memory of the given      */
/*                          type.                                            */
/*                                                                           */
/*    Input(s)            : u1BufType - Type of memory needed.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gEvbGlobalInfo                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated buffer if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/

UINT1              *
VlanEvbGetBuf (UINT1 u1BufType, UINT4 u4Size)
{
    UINT1                 *pu1Buf = NULL;

    switch (u1BufType)
    {
        case VLAN_EVB_CONTEXT_ENTRY:
            VLAN_ALLOC_MEM_BLOCK(gEvbGlobalInfo.EvbCxtPoolId, pu1Buf);
            break;

        case VLAN_EVB_UAP_ENTRY:
            VLAN_ALLOC_MEM_BLOCK(gEvbGlobalInfo.EvbUapIfPoolId, pu1Buf);
            break;

        case VLAN_EVB_SCH_ENTRY:
            VLAN_ALLOC_MEM_BLOCK(gEvbGlobalInfo.EvbSChIfPoolId, pu1Buf);
            break;

        case VLAN_EVB_QUE_ENTRY:
            VLAN_ALLOC_MEM_BLOCK (gEvbGlobalInfo.EvbQueIfPoolId, pu1Buf);
            break;

        default:
            break;
    }
    UNUSED_PARAM(u4Size);
    return pu1Buf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbReleaseBuf                                */
/*                                                                           */
/*    Description         : This function releases the allocated memory to   */
/*                          the free pool specified by the buffer type.      */
/*                                                                           */
/*    Input(s)            : u1BufType - Type of memory.                      */
/*                          pu1Buf    - Buffer to be released.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMem                                   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanEvbReleaseBuf (UINT1 u1BufType, UINT1 *pu1Buf)
{

    switch (u1BufType)
    {
        case VLAN_EVB_CONTEXT_ENTRY:
            VLAN_RELEASE_MEM_BLOCK (gEvbGlobalInfo.EvbCxtPoolId, pu1Buf);
            break;

         case VLAN_EVB_UAP_ENTRY:
            VLAN_RELEASE_MEM_BLOCK (gEvbGlobalInfo.EvbUapIfPoolId ,pu1Buf);
            break;

         case VLAN_EVB_SCH_ENTRY:
            VLAN_RELEASE_MEM_BLOCK (gEvbGlobalInfo.EvbSChIfPoolId,pu1Buf);
            break;

        case VLAN_EVB_QUE_ENTRY:
            VLAN_RELEASE_MEM_BLOCK (gEvbGlobalInfo.EvbQueIfPoolId, pu1Buf);
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : VlanEvbGetSystemStatus                               */
/*                                                                           */
/* Description        : This function is used to call from VLAN API          */
/*                      to get the system status of EVB. this will be used   */
/*                      when other modules try to check EVB status.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE if EVB is running on the context           */
/*                      VLAN_FALSE otherwise                                 */
/*****************************************************************************/
UINT1
VlanEvbGetSystemStatus (UINT4 u4ContextId)
{
    if (VLAN_EVB_SYSTEM_STATUS (u4ContextId) == VLAN_EVB_SYSTEM_START)
    {
        return VLAN_TRUE;
    }
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbPostCdcpMsg                               */
/*                                                                           */
/*    Description         : Posts the CDCP message to Vlan Config Q for the  */
/*                          processing.                                      */
/*                                                                           */
/*    Input(s)            : pLldpAppTlv - CDCP TLV info filled.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanEvbPostCdcpMsg (tLldpAppTlv *pLldpAppTlv)
{
    tVlanQMsg           *pVlanQMsg = NULL;

    pVlanQMsg = (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF, 
                                                sizeof (tVlanQMsg));
    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                "VlanEvbPostCdcpMsg: Failure to allocate VLAN QMsg buf\r\n");
        return;
    }
    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_EVB_CDCP_LLDP_MSG;
   
    pVlanQMsg->pEvbQueMsg = (tEvbQueMsg *)(VOID *) VLAN_EVB_GET_BUF 
                (VLAN_EVB_QUE_ENTRY, sizeof (tEvbQueMsg));
                
    if (NULL == pVlanQMsg->pEvbQueMsg)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                "VlanEvbPostCdcpMsg: Failure to allocate EVB QMsg buf\r\n");
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
        return;
    }
                 
    pVlanQMsg->pEvbQueMsg->u4IfIndex = pLldpAppTlv->u4RxAppTlvPortId;
    pVlanQMsg->pEvbQueMsg->u4MsgType = pLldpAppTlv->u4MsgType;
    pVlanQMsg->pEvbQueMsg->EvbTlvParam.u2RxTlvLen = pLldpAppTlv->u2RxAppTlvLen;
    MEMCPY(pVlanQMsg->pEvbQueMsg->EvbAppId.au1OUI,
           pLldpAppTlv->LldpAppId.au1OUI,
           VLAN_EVB_MAX_OUI_LEN);

    pVlanQMsg->pEvbQueMsg->EvbAppId.u1SubType = pLldpAppTlv->LldpAppId.u1SubType;
    pVlanQMsg->pEvbQueMsg->EvbAppId.u2TlvType = pLldpAppTlv->LldpAppId.u2TlvType;

    /* check whether the u2RxAppTlvLen must not be zero */
    MEMCPY (pVlanQMsg->pEvbQueMsg->EvbTlvParam.au1EvbTlv, 
            pLldpAppTlv->pu1RxAppTlv,
            pLldpAppTlv->u2RxAppTlvLen);

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                (UINT1 *) &pVlanQMsg,
                OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                "VlanEvbPostCdcpMsg: Failure to send EVB QMsg buf\r\n");
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pVlanQMsg->pEvbQueMsg);
        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
        return;
    }
    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbGetSbpPortsOnUap                          */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
VlanEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray  *pSbpArray)
{
    tEvbSChIfEntry      *pSChIfEntry    = NULL;
    tEvbUapIfEntry      *pUapIfEntry    = NULL;
    UINT4                u4SVId         = 0;
    UINT4                u4LocalIndex   = u4UapIfIndex;
    UINT1                u1ArrayIndex   = 0;

    VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, 
                   VLAN_NAME,"VlanEvbGetSbpPortsOnUap: Hit for "
                   "UAP Index %d\r\n", u4UapIfIndex);

    if ((pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex)) == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC |ALL_FAILURE_TRC, 
                       VLAN_NAME,"VlanEvbGetSbpPortsOnUap: "
                       "UAP Index %d not present\r\n", u4UapIfIndex);
        return;
    }
    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4LocalIndex, u4SVId))
                != NULL)
    {
        if ((u4UapIfIndex != pSChIfEntry->u4UapIfIndex) ||
            (u1ArrayIndex >= VLAN_EVB_MAX_SBP_PER_UAP))
        {
            break;
        }
        if (((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID) &&
            (pSChIfEntry->i4NegoStatus == VLAN_EVB_SCH_CONFIRMED)) ||
            (pUapIfEntry->i4UapIfSChMode != VLAN_EVB_UAP_SCH_MODE_HYBRID))
        {
            pSbpArray->au4SbpArray[u1ArrayIndex++] =(UINT4)pSChIfEntry->i4SChIfIndex;

            VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, 
                           VLAN_NAME, "VlanEvbGetSbpPortsOnUap: "
                           "Filling SChIf %d SVID %d in pSbpArray\r\n",
                           pSChIfEntry->i4SChIfIndex, pSChIfEntry->u4SVId);
        }
        u4LocalIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetSChIfIndex                                */
/*                                                                           */
/*    Description         : This function provides the S-Channel IfIndex for */
/*                          the given SVID and UAP IfIndex.                  */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                                                                           */
/*    Output(s)           : pu4SChIfIndex- S-Channel IfIndex                 */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSChIfIndex (UINT4 u4UapIfIndex, UINT4 u4SVID, UINT4 *pu4SChIfIndex)
{
    tEvbSChIfEntry      *pSChIfEntry    = NULL;

    *pu4SChIfIndex = 0;

    pSChIfEntry = VlanEvbSChIfGetEntry (u4UapIfIndex, u4SVID);

    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                VLAN_NAME, "VlanGetSChIfIndex: S-Channel index is"
                "not present for UAP port %d and SVID %d\r\n",
                u4UapIfIndex, u4SVID);
        return VLAN_FAILURE;
    }
    *pu4SChIfIndex = (UINT4)pSChIfEntry->i4SChIfIndex;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetSChInfoFromSChIfIndex                     */
/*                                                                           */
/*    Description         : This function provides the UAP IfIndex and SVID  */
/*                          for the given SCh IfIndex.                       */
/*                                                                           */
/*    Input(s)            : u4SChIfIndex - S-Channel IfIndex.                */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4UapIfIndex- UAP IfIndex                       */
/*                          pu4SVID      - SVID                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                  UINT2 *pu2SVID)
{
    tEvbSChIfEntry      *pSChIfEntry    = NULL;
    UINT4                u4SVId         = 0;
    UINT4                u4UapIfIndex   = 0;
   
    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                != NULL)
    {
        if (pSChIfEntry->i4SChIfIndex == (INT4) u4SChIfIndex)
        {
            *pu4UapIfIndex = pSChIfEntry->u4UapIfIndex;
            *pu2SVID = (UINT2) pSChIfEntry->u4SVId;
            return VLAN_SUCCESS;
        }
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
    }
    return VLAN_FAILURE;
}
#endif  /* __VLNEVINI_C__ */
