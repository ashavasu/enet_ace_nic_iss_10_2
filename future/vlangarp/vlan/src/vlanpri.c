/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: vlanpri.c,v 1.19 2010/12/14 12:21:06 siva Exp $
 * 
 ********************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlanpri.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : Priority Module                                  */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains routines for priority traffic */
/*                          class module.                                    */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEnablePriorityModule                         */
/*                                                                           */
/*    Description         : This function performs the initialisation of     */
/*                         priority module task.                             */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->VlanPriTimer, gpVlanContextInfo->VlanInfo                   */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS if initialisation succeeds,          */
/*                            otherwise VLAN_FAILURE.                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanEnablePriorityModule (VOID)
{
    VlanEnablePriModule ();

    /* Set the VLAN Traffic Classes Enabled Flag to TRUE */
    gpVlanContextInfo->VlanInfo.u1TrfClassEnabled = VLAN_SNMP_TRUE;

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDisablePriorityModule                        */
/*                                                                           */
/*    Description         : This function deletes the created semaphore and  */
/*                          timer list for the priority task. This function  */
/*                          also drains all the buffers in the priority      */
/*                          traffic class queues of all the ports.           */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->VlanPriTimer                              */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->VlanInfo                                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS if initialisation succeeds,          */
/*                            otherwise VLAN_FAILURE.                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDisablePriorityModule (VOID)
{

    /* Set the VLAN Traffic Classes Enabled Flag to TRUE */
    VlanDisablePriModule ();
    gpVlanContextInfo->VlanInfo.u1TrfClassEnabled = VLAN_SNMP_FALSE;

    return VLAN_SUCCESS;
}

#ifndef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSendToPriorityQ                              */
/*                                                                           */
/*    Description         : This function takes care of adding the buffer to */
/*                          the corresponding traffic class queue of the     */
/*                          port. This function also starts the VLAN priority*/
/*                          timer if it is not active.                       */
/*                                                                           */
/*    Input(s)            : pBuf - Pointer to the data buffer.               */
/*                          pIfMsg - Pointer to interface message.           */
/*                          u1trfClass - Traffic class                       */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->VlanPriTimer                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanSendToPriorityQ (tCRU_BUF_CHAIN_DESC * pBuf, tVlanOutIfMsg * pIfMsg,
                     UINT1 u1TrfClass)
{
    tVlanOutIfMsg      *pVlanIfMsg = NULL;
    tVlanPortEntry     *pPortEntry = NULL;

    pVlanIfMsg = (tVlanOutIfMsg *) VLAN_GET_MODULE_DATA_PTR (pBuf);
    if (pVlanIfMsg == NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        return;
    }

    MEMSET (pVlanIfMsg, 0, sizeof (tVlanOutIfMsg));
    MEMCPY (pVlanIfMsg, pIfMsg, sizeof (tVlanOutIfMsg));

    /* add the buffer to the corresponding traffic class queue */

    pPortEntry = VLAN_GET_PORT_ENTRY (pIfMsg->u2Port);

    if (NULL == pPortEntry)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        return;
    }

    if (pPortEntry->VlanPriQ[u1TrfClass].pFirstBuf == NULL)
    {

        /* This is the first buffer in this queue */
        pPortEntry->VlanPriQ[u1TrfClass].pFirstBuf = pBuf;

    }
    else
    {
        /* Some buffers need to be send before this.
         * Append this to the buffer chain.
         */
        VLAN_LINK_BUFFERS (pPortEntry->VlanPriQ[u1TrfClass].pLastBuf, pBuf);
    }

    pPortEntry->VlanPriQ[u1TrfClass].pLastBuf = pBuf;

    if (gpVlanContextInfo->VlanPriTimer.u1IsTmrActive == VLAN_FALSE)
    {
        /* Start the priority timer here */
        VLAN_START_TIMER (VLAN_PRIORITY_TIMER, VLAN_PRI_SCHEDULING_INT);
        gpVlanContextInfo->VlanPriTimer.u1IsTmrActive = VLAN_TRUE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDrainAllPriQ                                 */
/*                                                                           */
/*    Description         : This function sends all the packets in the       */
/*                          traffic class queues of all the ports.           */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->VlanPriTimer                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDrainAllPriQ (VOID)
{
    UINT2               u2PortIndex;
    INT2                i2TrfClass;
    tCRU_BUF_CHAIN_DESC *pBufToSend = NULL;
    tVlanOutIfMsg      *pVlanOutIfMsg = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
#ifdef TRACE_WANTED
    UINT4               u4Port;
#endif

    /* Drain all the packets in the priority module queue */

    gpVlanContextInfo->VlanPriTimer.u1IsTmrActive = VLAN_FALSE;

    for (i2TrfClass = VLAN_MAX_TRAFF_CLASS - 1; i2TrfClass >= 0; i2TrfClass--)
    {

        VLAN_SCAN_PORT_TABLE (u2PortIndex)
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (u2PortIndex);
            /* Scan all the traffic classes for this port */

            if (pPortEntry->u1NumTrfClass <= i2TrfClass)
            {

                /* This traffic class queue not supported in this port */
                continue;
            }

#ifdef TRACE_WANTED
            if (pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf != NULL)
            {

                u4Port = VLAN_GET_IFINDEX (u2PortIndex);
                VLAN_TRC_ARG2 (VLAN_PRI_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "Priority sending all packets on traffic "
                               "class queue - %d of Port - %d \n", i2TrfClass,
                               u4Port);
            }
#endif
            /* Send the buffers in the queue */

            while (pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf != NULL)
            {

                /* We have some buffers to send */

                pBufToSend = pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf;

                pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf =
                    VLAN_UNLINK_BUFFERS (pBufToSend);

                pVlanOutIfMsg =
                    (tVlanOutIfMsg *) VLAN_GET_MODULE_DATA_PTR (pBufToSend);

                VlanHandleOutgoingPkt (pBufToSend, pVlanOutIfMsg);
            }                    /* END_WHILE */

            pPortEntry->VlanPriQ[i2TrfClass].pLastBuf = NULL;
        }                        /* FOR - ALL Ports */
    }                            /* FOR - ALL Traffic Classes */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFreePriorityQ ()                             */
/*                                                                           */
/*    Description         : This function releases all the packets in the    */
/*                          traffic class queues of the given port.          */
/*                                                                           */
/*    Input(s)            : pPortEntry - Entry corresponding to the port     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : gPriSem,                                   */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanFreePriorityQ (tVlanPortEntry * pPortEntry)
{
    INT2                i2TrfClass;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;

    VLAN_TRC (VLAN_PRI_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Releasing buffers in priority queues \n");

    for (i2TrfClass = (INT2) (pPortEntry->u1NumTrfClass - 1);
         i2TrfClass >= 0; i2TrfClass--)
    {

        while (pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf != NULL)
        {

            pBuf = pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf;

            pPortEntry->VlanPriQ[i2TrfClass].pFirstBuf
                = VLAN_UNLINK_BUFFERS (pBuf);

            VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        }
    }

}

#endif /* !NPAPI_WANTED  */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwEnablePriorityModule                       */
/*                                                                           */
/*    Description         : This function enables priority module in the     */
/*                         hardware. This function calls appropriate NPAPI's */
/*                         for the configurations made when the module is    */
/*                         disabled. VLAN Port table MUST be initialised     */
/*                         before calling this function.                     */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHwEnablePriorityModule (VOID)
{
    INT4                i4RetVal;
    tVlanPortEntry     *pPortEntry;
    UINT2               u2Port;
    UINT1               u1Index;
    UINT1               u1TrfClass;
    UINT1               u1Priority;
    UINT4               u4Port;

    /* Scan port table */
    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        /* No need to Set Default user priority since we will invoke the NPAPI
         * for setting def. user priority even the priority is disabled. */

        /* Set number of traffic class queues */
        i4RetVal = VlanHwSetPortNumTrafClasses (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                (INT4) pPortEntry->
                                                u1NumTrfClass);
        u4Port = VLAN_GET_IFINDEX (u2Port);
        if (i4RetVal == VLAN_FAILURE)
        {
            /* Setting number of traffic class queues in hardware failed.
             * Reverting back to default values */
            VLAN_TRC_ARG1 (VLAN_PRI_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "Setting number of traffic class queues"
                           "failed for Port - %d \n", u4Port);

            pPortEntry->u1NumTrfClass = VLAN_DEF_TRAFF_CLASS;

            /* Set traffic class map table here */
            for (u1Index = 0; u1Index < VLAN_MAX_PRIORITY; u1Index++)
            {
                pPortEntry->au1TrfClassMap[u1Index]
                    =
                    gau1PriTrfClassMap[u1Index][pPortEntry->u1NumTrfClass - 1];
            }

        }

        /* Set traffic class map table in hardware */
        for (u1Index = 0; u1Index < VLAN_MAX_PRIORITY; u1Index++)
        {
            u1TrfClass = pPortEntry->au1TrfClassMap[u1Index];

            i4RetVal =
                VlanHwSetTraffClassMap (VLAN_CURR_CONTEXT_ID (), u2Port,
                                        (INT4) u1Index, (INT4) u1TrfClass);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_PRI_TRC,
                               CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                               "Setting traffic class queue"
                               "failed for Priority - %d on Port - %d \n",
                               u1Index, u4Port);

                /* mapping this prioriy to default traffic class */
                pPortEntry->au1TrfClassMap[u1Index]
                    =
                    gau1PriTrfClassMap[u1Index][pPortEntry->u1NumTrfClass - 1];
            }
        }

        /* Set the priority regeneration table here */
        if ((pPortEntry->u1IfType != VLAN_ETHERNET_INTERFACE_TYPE) &&
            (pPortEntry->u1IfType != VLAN_LAGG_INTERFACE_TYPE))
        {
            /* Priority regen table valid only for non-ethernet interfaces.
             * Since LAGG interfaces are also formed only over ethernet      
             * interfaces, priority regen table not valid for LAGG interfaces
             * also. */
            for (u1Index = 0; u1Index < VLAN_MAX_PRIORITY; u1Index++)
            {
                u1Priority = pPortEntry->au1PriorityRegen[u1Index];
                i4RetVal =
                    VlanHwSetRegenUserPriority (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, (INT4) u1Index,
                                                (INT4) u1Priority);

                if (i4RetVal == VLAN_FAILURE)
                {
                    /* Set it to default value */
                    VLAN_TRC_ARG2 (VLAN_PRI_TRC,
                                   CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                   VLAN_NAME,
                                   "Setting priority regen mapping"
                                   "failed for Priority - %d on Port - %d \n",
                                   u1Index, u4Port);
                    pPortEntry->au1PriorityRegen[u1Index] = u1Index;
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwDisablePriorityModule                      */
/*                                                                           */
/*    Description         : This function disables priority module in the    */
/*                         hardware. This function calls appropriate NPAPI's */
/*                         for the revoking the configurable parameters to   */
/*                         to default values.                                */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHwDisablePriorityModule (VOID)
{
    INT4                i4RetVal;
    tVlanPortEntry     *pPortEntry;
    UINT2               u2Port;
    UINT1               u1Index;
    UINT1               u1TrafficClass;
    UINT1               u1NumTrfClass;
    UINT4               u4Port;

    /* Scan all active ports and set the 802.1p parameters to default values */
    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        /* 
         * No need to set the default user priority since we will allow
         * hardware updations for it even when the module is disabled
         */

        /* 
         * Set traffic class map table in hardware. Map all priorities to
         * traffic class queue no. 1
         */
        u4Port = VLAN_GET_IFINDEX (u2Port);
        u1NumTrfClass = VLAN_DEF_TRAFF_CLASS;
        for (u1Index = 0; u1Index < VLAN_MAX_PRIORITY; u1Index++)
        {
            u1TrafficClass = gau1PriTrfClassMap[u1Index][u1NumTrfClass - 1];
            i4RetVal =
                VlanHwSetTraffClassMap (VLAN_CURR_CONTEXT_ID (), u2Port,
                                        (INT4) u1Index, u1TrafficClass);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_PRI_TRC,
                               CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                               "Mapping priority %d to default traffic"
                               "class failed on Port - %d \n", u1Index, u4Port);

            }
        }

        /* 
         * Set number of traffic class queues to 1 so that all priorities will
         * map to same traffic class queue. Set number of traffic class queues
         * only after mapping all priority to default traffic class queue.
         */
        i4RetVal = VlanHwSetPortNumTrafClasses (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                u1NumTrfClass);
        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_PRI_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "Disabling traffic class queues failed"
                           " for Port - %d \n", u4Port);
        }

        /* Set the priority regeneration table here */
        if ((pPortEntry->u1IfType != VLAN_ETHERNET_INTERFACE_TYPE) &&
            (pPortEntry->u1IfType != VLAN_LAGG_INTERFACE_TYPE))
        {
            /* Priority regen table valid only for non-ethernet interfaces */
            /* Since LAGG interfaces are also formed only over ethernet      
             * interfaces, priority regen table not valid for LAGG interfaces
             * also. */
            for (u1Index = 0; u1Index < VLAN_MAX_PRIORITY; u1Index++)
            {
                /* By default regen priority will be equal to the original 
                 * priority */
                i4RetVal =
                    VlanHwSetRegenUserPriority (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, (INT4) u1Index,
                                                (INT4) u1Index);

                if (i4RetVal == VLAN_FAILURE)
                {
                    /* Set it to default value */
                    VLAN_TRC_ARG2 (VLAN_PRI_TRC,
                                   CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                   VLAN_NAME,
                                   "Resetting priority regen mapping"
                                   "failed for Priority - %d on Port - %d \n",
                                   u1Index, u2Port);
                }
            }
        }
    }

}
