/* $Id: fsm1adnc.c,v 1.1 2016/06/21 09:54:51 siva Exp $
    ISS Wrapper module
    module ARICENT-MIDOT1AD-MIB

 */
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include "fsm1adnc.h"
/********************************************************************
* FUNCTION NcDot1adMIPortPcpSelectionRowSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortPcpSelectionRowSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortPcpSelectionRow )
{

    INT1 i1RetVal;
	VLAN_LOCK();
    i1RetVal = nmhSetDot1adMIPortPcpSelectionRow(
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortPcpSelectionRow);
	VLAN_UNLOCK();

    return i1RetVal;


} /* NcDot1adMIPortPcpSelectionRowSet */

/********************************************************************
* FUNCTION NcDot1adMIPortPcpSelectionRowTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortPcpSelectionRowTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortPcpSelectionRow )
{

    INT1 i1RetVal;

	VLAN_LOCK();
    i1RetVal = nmhTestv2Dot1adMIPortPcpSelectionRow(pu4Error,
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortPcpSelectionRow);
	VLAN_UNLOCK();

    return i1RetVal;


} /* NcDot1adMIPortPcpSelectionRowTest */

/********************************************************************
* FUNCTION NcDot1adMIPortUseDeiSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortUseDeiSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortUseDei )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPortUseDei(
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortUseDei);

    return i1RetVal;


} /* NcDot1adMIPortUseDeiSet */

/********************************************************************
* FUNCTION NcDot1adMIPortUseDeiTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortUseDeiTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortUseDei )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPortUseDei(pu4Error,
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortUseDei);

    return i1RetVal;


} /* NcDot1adMIPortUseDeiTest */

/********************************************************************
* FUNCTION NcDot1adMIPortReqDropEncodingSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortReqDropEncodingSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortReqDropEncoding )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPortReqDropEncoding(
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortReqDropEncoding);

    return i1RetVal;


} /* NcDot1adMIPortReqDropEncodingSet */

/********************************************************************
* FUNCTION NcDot1adMIPortReqDropEncodingTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortReqDropEncodingTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortReqDropEncoding )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPortReqDropEncoding(pu4Error,
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortReqDropEncoding);

    return i1RetVal;


} /* NcDot1adMIPortReqDropEncodingTest */

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPriorityTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortSVlanPriorityTypeSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriorityType )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPortSVlanPriorityType(
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortSVlanPriorityType);

    return i1RetVal;


} /* NcDot1adMIPortSVlanPriorityTypeSet */

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPriorityTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortSVlanPriorityTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriorityType )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPortSVlanPriorityType(pu4Error,
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortSVlanPriorityType);

    return i1RetVal;


} /* NcDot1adMIPortSVlanPriorityTypeTest */

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortSVlanPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPortSVlanPriority(
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortSVlanPriority);

    return i1RetVal;


} /* NcDot1adMIPortSVlanPrioritySet */

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPortSVlanPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPortSVlanPriority(pu4Error,
                 i4Dot1adMIPortNum,
                i4Dot1adMIPortSVlanPriority);

    return i1RetVal;


} /* NcDot1adMIPortSVlanPriorityTest */

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRelayVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIVidTranslationRelayVidSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRelayVid )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIVidTranslationRelayVid(
                 i4Dot1adMIPortNum,
                 i4Dot1adMIVidTranslationLocalVid,
                i4Dot1adMIVidTranslationRelayVid);

    return i1RetVal;


} /* NcDot1adMIVidTranslationRelayVidSet */

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRelayVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIVidTranslationRelayVidTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRelayVid )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIVidTranslationRelayVid(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMIVidTranslationLocalVid,
                i4Dot1adMIVidTranslationRelayVid);

    return i1RetVal;


} /* NcDot1adMIVidTranslationRelayVidTest */

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIVidTranslationRowStatusSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRowStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIVidTranslationRowStatus(
                 i4Dot1adMIPortNum,
                 i4Dot1adMIVidTranslationLocalVid,
                i4Dot1adMIVidTranslationRowStatus);

    return i1RetVal;


} /* NcDot1adMIVidTranslationRowStatusSet */

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIVidTranslationRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRowStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIVidTranslationRowStatus(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMIVidTranslationLocalVid,
                i4Dot1adMIVidTranslationRowStatus);

    return i1RetVal;


} /* NcDot1adMIVidTranslationRowStatusTest */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationSVidSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVid )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMICVidRegistrationSVid(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationSVid);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationSVidSet */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationSVidTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVid )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMICVidRegistrationSVid(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationSVid);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationSVidTest */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedPepSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationUntaggedPepSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedPep )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMICVidRegistrationUntaggedPep(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationUntaggedPep);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationUntaggedPepSet */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedPepTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationUntaggedPepTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedPep )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMICVidRegistrationUntaggedPep(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationUntaggedPep);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationUntaggedPepTest */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedCepSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationUntaggedCepSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedCep )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMICVidRegistrationUntaggedCep(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationUntaggedCep);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationUntaggedCepSet */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedCepTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationUntaggedCepTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedCep )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMICVidRegistrationUntaggedCep(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationUntaggedCep);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationUntaggedCepTest */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationRowStatusSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationRowStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMICVidRegistrationRowStatus(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationRowStatus);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationRowStatusSet */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationRowStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMICVidRegistrationRowStatus(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationRowStatus);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationRowStatusTest */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPriorityTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationSVlanPriorityTypeSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriorityType )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMICVidRegistrationSVlanPriorityType(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationSVlanPriorityType);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationSVlanPriorityTypeSet */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPriorityTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationSVlanPriorityTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriorityType )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMICVidRegistrationSVlanPriorityType(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationSVlanPriorityType);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationSVlanPriorityTypeTest */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationSVlanPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMICVidRegistrationSVlanPriority(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationSVlanPriority);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationSVlanPrioritySet */

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMICVidRegistrationSVlanPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMICVidRegistrationSVlanPriority(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationCVid,
                i4Dot1adMICVidRegistrationSVlanPriority);

    return i1RetVal;


} /* NcDot1adMICVidRegistrationSVlanPriorityTest */

/********************************************************************
* FUNCTION NcDot1adMIPepPvidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepPvidSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepPvid )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPepPvid(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepPvid);

    return i1RetVal;


} /* NcDot1adMIPepPvidSet */

/********************************************************************
* FUNCTION NcDot1adMIPepPvidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepPvidTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepPvid )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPepPvid(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepPvid);

    return i1RetVal;


} /* NcDot1adMIPepPvidTest */

/********************************************************************
* FUNCTION NcDot1adMIPepDefaultUserPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepDefaultUserPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepDefaultUserPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPepDefaultUserPriority(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepDefaultUserPriority);

    return i1RetVal;


} /* NcDot1adMIPepDefaultUserPrioritySet */

/********************************************************************
* FUNCTION NcDot1adMIPepDefaultUserPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepDefaultUserPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepDefaultUserPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPepDefaultUserPriority(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepDefaultUserPriority);

    return i1RetVal;


} /* NcDot1adMIPepDefaultUserPriorityTest */

/********************************************************************
* FUNCTION NcDot1adMIPepAccptableFrameTypesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepAccptableFrameTypesSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepAccptableFrameTypes )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPepAccptableFrameTypes(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepAccptableFrameTypes);

    return i1RetVal;


} /* NcDot1adMIPepAccptableFrameTypesSet */

/********************************************************************
* FUNCTION NcDot1adMIPepAccptableFrameTypesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepAccptableFrameTypesTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepAccptableFrameTypes )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPepAccptableFrameTypes(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepAccptableFrameTypes);

    return i1RetVal;


} /* NcDot1adMIPepAccptableFrameTypesTest */

/********************************************************************
* FUNCTION NcDot1adMIPepIngressFilteringSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepIngressFilteringSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepIngressFiltering )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPepIngressFiltering(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepIngressFiltering);

    return i1RetVal;


} /* NcDot1adMIPepIngressFilteringSet */

/********************************************************************
* FUNCTION NcDot1adMIPepIngressFilteringTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPepIngressFilteringTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepIngressFiltering )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPepIngressFiltering(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                i4Dot1adMIPepIngressFiltering);

    return i1RetVal;


} /* NcDot1adMIPepIngressFilteringTest */

/********************************************************************
* FUNCTION NcDot1adMIServicePriorityRegenRegeneratedPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIServicePriorityRegenRegeneratedPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIServicePriorityRegenReceivedPriority,
                INT4 i4Dot1adMIServicePriorityRegenRegeneratedPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIServicePriorityRegenRegeneratedPriority(
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                 i4Dot1adMIServicePriorityRegenReceivedPriority,
                i4Dot1adMIServicePriorityRegenRegeneratedPriority);

    return i1RetVal;


} /* NcDot1adMIServicePriorityRegenRegeneratedPrioritySet */

/********************************************************************
* FUNCTION NcDot1adMIServicePriorityRegenRegeneratedPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIServicePriorityRegenRegeneratedPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIServicePriorityRegenReceivedPriority,
                INT4 i4Dot1adMIServicePriorityRegenRegeneratedPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIServicePriorityRegenRegeneratedPriority(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMICVidRegistrationSVid,
                 i4Dot1adMIServicePriorityRegenReceivedPriority,
                i4Dot1adMIServicePriorityRegenRegeneratedPriority);

    return i1RetVal;


} /* NcDot1adMIServicePriorityRegenRegeneratedPriorityTest */

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPcpDecodingPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPcpDecodingPriority(
                 i4Dot1adMIPortNum,
                 i4Dot1adMIPcpDecodingPcpSelRow,
                 i4Dot1adMIPcpDecodingPcpValue,
                i4Dot1adMIPcpDecodingPriority);

    return i1RetVal;


} /* NcDot1adMIPcpDecodingPrioritySet */

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPcpDecodingPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingPriority )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPcpDecodingPriority(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMIPcpDecodingPcpSelRow,
                 i4Dot1adMIPcpDecodingPcpValue,
                i4Dot1adMIPcpDecodingPriority);

    return i1RetVal;


} /* NcDot1adMIPcpDecodingPriorityTest */

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingDropEligibleSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPcpDecodingDropEligibleSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingDropEligible )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPcpDecodingDropEligible(
                 i4Dot1adMIPortNum,
                 i4Dot1adMIPcpDecodingPcpSelRow,
                 i4Dot1adMIPcpDecodingPcpValue,
                i4Dot1adMIPcpDecodingDropEligible);

    return i1RetVal;


} /* NcDot1adMIPcpDecodingDropEligibleSet */

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingDropEligibleTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPcpDecodingDropEligibleTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingDropEligible )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPcpDecodingDropEligible(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMIPcpDecodingPcpSelRow,
                 i4Dot1adMIPcpDecodingPcpValue,
                i4Dot1adMIPcpDecodingDropEligible);

    return i1RetVal;


} /* NcDot1adMIPcpDecodingDropEligibleTest */

/********************************************************************
* FUNCTION NcDot1adMIPcpEncodingPcpValueSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPcpEncodingPcpValueSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpEncodingPcpSelRow,
                INT4 i4Dot1adMIPcpEncodingPriority,
                INT4 i4Dot1adMIPcpEncodingDropEligible,
                INT4 i4Dot1adMIPcpEncodingPcpValue )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetDot1adMIPcpEncodingPcpValue(
                 i4Dot1adMIPortNum,
                 i4Dot1adMIPcpEncodingPcpSelRow,
                 i4Dot1adMIPcpEncodingPriority,
                 i4Dot1adMIPcpEncodingDropEligible,
                i4Dot1adMIPcpEncodingPcpValue);

    return i1RetVal;


} /* NcDot1adMIPcpEncodingPcpValueSet */

/********************************************************************
* FUNCTION NcDot1adMIPcpEncodingPcpValueTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1adMIPcpEncodingPcpValueTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpEncodingPcpSelRow,
                INT4 i4Dot1adMIPcpEncodingPriority,
                INT4 i4Dot1adMIPcpEncodingDropEligible,
                INT4 i4Dot1adMIPcpEncodingPcpValue )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2Dot1adMIPcpEncodingPcpValue(pu4Error,
                 i4Dot1adMIPortNum,
                 i4Dot1adMIPcpEncodingPcpSelRow,
                 i4Dot1adMIPcpEncodingPriority,
                 i4Dot1adMIPcpEncodingDropEligible,
                i4Dot1adMIPcpEncodingPcpValue);

    return i1RetVal;


} /* NcDot1adMIPcpEncodingPcpValueTest */

/* END i_ARICENT_MIDOT1AD_MIB.c */
