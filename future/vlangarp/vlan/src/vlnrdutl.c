/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/* $Id: vlnrdutl.c,v 1.32 2013/12/07 11:09:15 siva Exp $                    */
/*  FILE NAME             : vlnrdutl.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN Redundancy Module                           */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains utility routines added for    */
/*                          VLAN Redudancy.                                  */
/*****************************************************************************/
#ifdef L2RED_WANTED
#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanRedInitGlobalInfo                                */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanRedInitGlobalInfo (VOID)
{
    MEMSET (&gVlanRedInfo, 0, sizeof (tVlanRedGlobalInfo));
    MEMSET (&gVlanRedTaskInfo, 0, sizeof (tVlanRedTaskInfo));

    VLAN_NODE_STATUS () = VLAN_NODE_IDLE;
    gVlanRedInfo.PeriodicTimer.u1IsTmrActive = VLAN_FALSE;
    gVlanRedInfo.RelearnTimer.u1IsTmrActive = VLAN_FALSE;
    gVlanRedInfo.BulkUpdNextVlanId = 0;
    gVlanRedInfo.BulkUpdNextPort = 0;
    gVlanRedInfo.u4BulkUpdNextContext = 0;
    VLAN_RED_BULK_REQ_RECD () = VLAN_FALSE;
    VLAN_RED_NUM_STANDBY_NODES () = 0;
    gVlanRedInfo.u1ForceFullAudit = 0;
#ifdef OPTIMIZED_HW_AUDIT_WANTED
    gVlanRedInfo.u1OptimizedAudit = VLAN_TRUE;
#else
    gVlanRedInfo.u1OptimizedAudit = VLAN_FALSE;
#endif
    if (VLAN_CREATE_SEM (VLAN_RED_SEM_NAME, 1, 0,
                         &(gVlanRedTaskInfo.RedSemId)) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         (OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                         "MSG: Failed to Create VLAN Redundancy Sema4 \r\n");
        return VLAN_FAILURE;
    }

#ifdef NPAPI_WANTED
    gVlanRedInfo.VlanRedCacheTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanRedCacheNode, RbNode),
                              VlanRedCacheRBTreeCmp);
    if (gVlanRedInfo.VlanRedCacheTable == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                         "VLAN Redundancy cache Creation Failure.\n");
        VLAN_DELETE_SEM (gVlanRedTaskInfo.RedSemId);
        return VLAN_FAILURE;
    }

    gVlanRedInfo.VlanRedStUcastCacheTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanRedStUcastCacheNode, RbNode),
                              VlanRedStUcastCacheRBTreeCmp);
    if (gVlanRedInfo.VlanRedStUcastCacheTable == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                         "VLAN Redundancy static unicast cache Creation Failure.\n");
        VLAN_DELETE_SEM (gVlanRedTaskInfo.RedSemId);
        RBTreeDelete (gVlanRedInfo.VlanRedCacheTable);
        return VLAN_FAILURE;
    }
    gVlanRedInfo.VlanRedMcastCacheTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanRedMcastCacheNode, RbNode),
                              VlanRedMcastCacheRBTreeCmp);
    if (gVlanRedInfo.VlanRedMcastCacheTable == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                         "VLAN Redundancy static multicast cache Creation Failure.\n");
        VLAN_DELETE_SEM (gVlanRedTaskInfo.RedSemId);
        RBTreeDelete (gVlanRedInfo.VlanRedCacheTable);
        RBTreeDelete (gVlanRedInfo.VlanRedStUcastCacheTable);
        return VLAN_FAILURE;
    }
    gVlanRedInfo.VlanRedProtoVlanCacheTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tVlanRedProtoVlanCacheNode, RbNode),
                              VlanRedProtoVlanRBTreeCmp);
    if (gVlanRedInfo.VlanRedProtoVlanCacheTable == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                         "VLAN Redundancy static multicast cache Creation Failure.\n");
        VLAN_DELETE_SEM (gVlanRedTaskInfo.RedSemId);
        RBTreeDelete (gVlanRedInfo.VlanRedCacheTable);
        RBTreeDelete (gVlanRedInfo.VlanRedMcastCacheTable);
        RBTreeDelete (gVlanRedInfo.VlanRedStUcastCacheTable);
        return VLAN_FAILURE;
    }

#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedDeInitGlobalInfo                              */
/*                                                                           */
/* Description        : DeInitializes redundancy global variables.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS                                         */
/*****************************************************************************/
INT4
VlanRedDeInitGlobalInfo (VOID)
{
    MEMSET (&gVlanRedInfo, 0, sizeof (tVlanRedGlobalInfo));

    VLAN_DELETE_SEM (gVlanRedTaskInfo.RedSemId);
    gVlanRedTaskInfo.RedSemId = 0;
    gVlanRedTaskInfo.VlanLrntPoolId = 0;
    gVlanRedTaskInfo.GrpLrntPoolId = 0;
    gVlanRedTaskInfo.VlanDelPoolId = 0;
    gVlanRedTaskInfo.GrpDelPoolId = 0;

    VLAN_NODE_STATUS () = VLAN_NODE_IDLE;
    gVlanRedInfo.PeriodicTimer.u1IsTmrActive = VLAN_FALSE;
    gVlanRedInfo.RelearnTimer.u1IsTmrActive = VLAN_FALSE;
    gVlanRedInfo.BulkUpdNextVlanId = 0;
    gVlanRedInfo.BulkUpdNextPort = 0;
    gVlanRedInfo.u4BulkUpdNextContext = 0;
    VLAN_RED_BULK_REQ_RECD () = VLAN_FALSE;
    VLAN_RED_NUM_STANDBY_NODES () = 0;
#ifdef NPAPI_WANTED
    if (gVlanRedInfo.VlanRedCacheTable != NULL)
    {
        RBTreeDestroy (gVlanRedInfo.VlanRedCacheTable,
                       (tRBKeyFreeFn) VlanRedCacheFreeNodes, 0);
        gVlanRedInfo.VlanRedCacheTable = NULL;
    }
    if (gVlanRedInfo.VlanRedStUcastCacheTable != NULL)
    {
        RBTreeDestroy (gVlanRedInfo.VlanRedStUcastCacheTable,
                       (tRBKeyFreeFn) VlanRedStUcastCacheFreeNodes, 0);
        gVlanRedInfo.VlanRedStUcastCacheTable = NULL;
    }
    if (gVlanRedInfo.VlanRedMcastCacheTable != NULL)
    {
        RBTreeDestroy (gVlanRedInfo.VlanRedMcastCacheTable,
                       (tRBKeyFreeFn) VlanRedMcastCacheFreeNodes, 0);
        gVlanRedInfo.VlanRedMcastCacheTable = NULL;
    }
    if (gVlanRedInfo.VlanRedProtoVlanCacheTable != NULL)
    {
        RBTreeDestroy (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                       (tRBKeyFreeFn) VlanRedProtoVlanCacheFreeNodes, 0);
        gVlanRedInfo.VlanRedProtoVlanCacheTable = NULL;
    }

#endif /* L2RED_WANTED */

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedInitActiveInfo                                */
/*                                                                           */
/* Description        : This function allocates mempools for VLAN and Group  */
/*                      deleted table. This function also creates VLAN and   */
/*                      Group deleted hash tables.                           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - If mempools are created successfully  */
/*                      VLAN_FAILURE otherwise                               */
/*****************************************************************************/
INT4
VlanRedInitActiveInfo (VOID)
{
    VLAN_RED_LOCK ();

    if (VLAN_CREATE_MEM_POOL
        (sizeof (tVlanRedGvrpDeletedEntry),
         gFsVlanSizingParams[VLAN_RED_GVRP_DEL_SIZING_ID].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &gVlanRedTaskInfo.VlanDelPoolId)
        != MEM_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "MEM Pool creation for VLAN deleted table failed \n");

        VLAN_RED_UNLOCK ();
        return VLAN_FAILURE;
    }

    if (VLAN_CREATE_MEM_POOL
        (sizeof (tVlanRedGmrpDeletedEntry),
         gFsVlanSizingParams[VLAN_RED_GMRP_DEL_SIZING_ID].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &gVlanRedTaskInfo.GrpDelPoolId)
        != MEM_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "MEM Pool creation for GROUP deleted table failed \n");

        VLAN_RED_UNLOCK ();        /* Release sem here */
        VlanRedDeInitActiveInfo ();
        return VLAN_FAILURE;
    }

    gVlanRedInfo.pVlanDeletedTable =
        VLAN_HASH_CREATE_TABLE (VLAN_RED_MAX_VLAN_DEL_BKTS, NULL, FALSE);

    if (gVlanRedInfo.pVlanDeletedTable == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Creation of VLAN Deleted hash table  \n");

        VLAN_RED_UNLOCK ();
        VlanRedDeInitActiveInfo ();
        return VLAN_FAILURE;
    }

    gVlanRedInfo.pGrpDeletedTable =
        VLAN_HASH_CREATE_TABLE (VLAN_RED_MAX_GRP_DEL_BKTS, NULL, FALSE);

    if (gVlanRedInfo.pGrpDeletedTable == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Creation of GROUP Deleted hash table  \n");

        VLAN_RED_UNLOCK ();
        VlanRedDeInitActiveInfo ();
        return VLAN_FAILURE;
    }

    VLAN_RED_UNLOCK ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedDeInitActiveInfo                              */
/*                                                                           */
/* Description        : This function releases the mempools allocated for    */
/*                      VLAN and Group deleted table. This function also     */
/*                      deletes the VLAN and GROUP Deleted tables.           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDeInitActiveInfo (VOID)
{
    VLAN_RED_LOCK ();

    if (gVlanRedTaskInfo.VlanDelPoolId != 0)
    {
        VLAN_DELETE_MEM_POOL (gVlanRedTaskInfo.VlanDelPoolId);
        gVlanRedTaskInfo.VlanDelPoolId = 0;
    }

    if (gVlanRedTaskInfo.GrpDelPoolId != 0)
    {
        VLAN_DELETE_MEM_POOL (gVlanRedTaskInfo.GrpDelPoolId);
        gVlanRedTaskInfo.GrpDelPoolId = 0;
    }

    if (gVlanRedInfo.pVlanDeletedTable != NULL)
    {
        VLAN_HASH_DELETE_TABLE (gVlanRedInfo.pVlanDeletedTable, NULL);
        gVlanRedInfo.pVlanDeletedTable = NULL;
    }

    if (gVlanRedInfo.pGrpDeletedTable != NULL)
    {
        VLAN_HASH_DELETE_TABLE (gVlanRedInfo.pGrpDeletedTable, NULL);
        gVlanRedInfo.pGrpDeletedTable = NULL;
    }

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDeInitStandbyInfo                             */
/*                                                                           */
/* Description        : This function deallocates mempools created for VLAN  */
/*                      and Group learnt ports entries. This function        */
/*                      also deletes VLAN and Group learnt ports table.      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDeInitStandbyInfo (VOID)
{
    VLAN_RED_LOCK ();

    if (gVlanRedTaskInfo.VlanLrntPoolId != 0)
    {
        VLAN_DELETE_MEM_POOL (gVlanRedTaskInfo.VlanLrntPoolId);
        gVlanRedTaskInfo.VlanLrntPoolId = 0;
    }

    if (gVlanRedTaskInfo.GrpLrntPoolId != 0)
    {
        VLAN_DELETE_MEM_POOL (gVlanRedTaskInfo.GrpLrntPoolId);
        gVlanRedTaskInfo.GrpLrntPoolId = 0;
    }

    if (gVlanRedInfo.pGvrpLrntTable != NULL)
    {
        MEM_FREE (gVlanRedInfo.pGvrpLrntTable);
        gVlanRedInfo.pGvrpLrntTable = NULL;
    }

    if (gVlanRedInfo.GmrpLrntTable != NULL)
    {
        RBTreeDestroy (gVlanRedInfo.GmrpLrntTable, VlanRedGmrpLrntFreeFn, 0);
        gVlanRedInfo.GmrpLrntTable = NULL;
    }

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDelAllStandbyInfo                             */
/*                                                                           */
/* Description        : This function deletes all the entries in the GVRP    */
/*                      and Group learnt ports table.                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelAllStandbyInfo (VOID)
{
    VlanRedDelAllGvrpLrntEntries ();

    VlanRedDelAllGmrpLrntEntries ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDelAllActiveInfo                              */
/*                                                                           */
/* Description        : This function deletes all the entries in the VLAN    */
/*                      and Group deleted table.                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelAllActiveInfo (VOID)
{
    VlanRedDelAllVlanDeletes ();

    VlanRedDelAllGroupDeletes ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDelAllVlanDeletes                             */
/*                                                                           */
/* Description        : This function deletes all the entries in the VLAN    */
/*                      deleted table.                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelAllVlanDeletes (VOID)
{
    tVlanRedGvrpDeletedEntry *pGvrpDelEntry;
    UINT4               u4HashIndex;

    VLAN_RED_LOCK ();

    /* Deleting entries from VLAN deleted table */
    VLAN_HASH_SCAN_TABLE (gVlanRedInfo.pVlanDeletedTable, u4HashIndex)
    {
        pGvrpDelEntry = (tVlanRedGvrpDeletedEntry *)
            VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pVlanDeletedTable,
                                      u4HashIndex);
        while (pGvrpDelEntry != NULL)
        {
            VLAN_HASH_DELETE_NODE (gVlanRedInfo.pVlanDeletedTable,
                                   (tTMO_HASH_NODE *) & (pGvrpDelEntry->
                                                         NextDeletedEntry),
                                   (UINT4) u4HashIndex);

            VLAN_RELEASE_BUF (VLAN_RED_GVRP_DEL_BUFF, (UINT1 *) pGvrpDelEntry);

            pGvrpDelEntry = (tVlanRedGvrpDeletedEntry *)
                VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pVlanDeletedTable,
                                          u4HashIndex);
        }
    }

    VLAN_RED_VLAN_DELETES () = 0;

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDelAllGroupDeletes                            */
/*                                                                           */
/* Description        : This function deletes all the entries in the GROUP   */
/*                      deleted table.                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelAllGroupDeletes (VOID)
{
    tVlanRedGmrpDeletedEntry *pGmrpDelEntry;
    UINT4               u4HashIndex;

    VLAN_RED_LOCK ();

    /* Deleting entries from Group deleted table */
    VLAN_HASH_SCAN_TABLE (gVlanRedInfo.pGrpDeletedTable, u4HashIndex)
    {
        pGmrpDelEntry = (tVlanRedGmrpDeletedEntry *)
            VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pGrpDeletedTable,
                                      u4HashIndex);
        while (pGmrpDelEntry != NULL)
        {
            VLAN_HASH_DELETE_NODE (gVlanRedInfo.pGrpDeletedTable,
                                   (tTMO_HASH_NODE *) & (pGmrpDelEntry->
                                                         NextDeletedEntry),
                                   (UINT4) u4HashIndex);

            VLAN_RELEASE_BUF (VLAN_RED_GMRP_DEL_BUFF, (UINT1 *) pGmrpDelEntry);

            pGmrpDelEntry = (tVlanRedGmrpDeletedEntry *)
                VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pGrpDeletedTable,
                                          u4HashIndex);
        }
    }

    VLAN_RED_GRP_DELETES () = 0;
    VLAN_RED_UNLOCK ();

}

/*****************************************************************************/
/* Function Name      : VlanRedGmrpTableCmp                                  */
/*                                                                           */
/* Description        : This function compares two GMRP learnt port entries. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : +1 - If entry1 is greater than entry2                */
/*                      -1 - If entry1 is lesser than entry2                 */
/*                       0 - If entry1 is equal to entry2                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedGmrpTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVlanRedGmrpLearntEntry *pEntry1;
    tVlanRedGmrpLearntEntry *pEntry2;
    INT4                i4RetVal;

    pEntry1 = (tVlanRedGmrpLearntEntry *) pRBElem1;
    pEntry2 = (tVlanRedGmrpLearntEntry *) pRBElem2;

    if (pEntry1->VlanId < pEntry2->VlanId)
    {
        return -1;
    }
    else if (pEntry1->VlanId > pEntry2->VlanId)
    {
        return 1;
    }
    else
    {
        i4RetVal = VLAN_MEMCMP (pEntry1->McastAddr, pEntry2->McastAddr,
                                ETHERNET_ADDR_SIZE);
        if (i4RetVal > 0)
        {
            return 1;
        }
        else if (i4RetVal < 0)
        {
            return -1;
        }
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleVlanUpdate                              */
/*                                                                           */
/* Description        : This function handles the VLAN update messages       */
/*                      received from the active node. This function stores  */
/*                      the received update in the GVRP learnt ports table.  */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the update is received.   */
/*                      PortList - Learnt port list determined by GVRP in    */
/*                                 the active node.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedHandleVlanUpdate (tVlanId VlanId, tLocalPortList PortList)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "VLAN Update received with invalid "
                       "VLAN ID - %d \n", VlanId);
        return;
    }

    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "VLAN Add/Modify update message received "
                   "for VLAN - %d with ports ", VlanId);

    VLAN_PRINT_PORT_LIST (VLAN_RED_TRC, PortList, 0);

    if (MEMCMP (PortList, gNullPortList, sizeof (tLocalPortList)) == 0)
    {
        /* L2RED_VLAN_ST - Should invoke for deletion here */
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN Update with Empty portlist received. "
                  "Treating as if delete update is received. \n");
        VlanRedHandleVlanDelUpdate (VlanId);
        return;
    }

    pGvrpEntry = VlanRedModifyGvrpLearntEntry (VlanId, PortList);

    if (pGvrpEntry != NULL)
    {
        /* Entry already modified... */
        return;
    }

    /* Entry not present in the database */
    pGvrpEntry = (tVlanRedGvrpLearntEntry *) (VOID *)
        VLAN_GET_BUF (VLAN_RED_GVRP_LEARNT_BUFF,
                      sizeof (tVlanRedGvrpLearntEntry));

    if (pGvrpEntry == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "No memory to store VLAN update message \n");

        return;
    }

    MEMSET (pGvrpEntry, 0, sizeof (tVlanRedGvrpLearntEntry));

    pGvrpEntry->VlanId = VlanId;

    MEMCPY (pGvrpEntry->LearntPortList, PortList, sizeof (tLocalPortList));

    VlanRedAddGvrpLearntEntry (pGvrpEntry);
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleGroupUpdate                             */
/*                                                                           */
/* Description        : This function handles the GROUP update messages      */
/*                      received from the active node. This function stores  */
/*                      the received update in the GMRP learnt ports table.  */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the update is received.   */
/*                      McastAddr - Mcast address for which the update is    */
/*                                  Received.                                */
/*                      PortList - Learnt port list determined by GMRP in    */
/*                                 the active node.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedHandleGroupUpdate (tVlanId VlanId, tMacAddr McastAddr,
                          tLocalPortList PortList)
{
    tVlanRedGmrpLearntEntry *pGmrpEntry;
    INT4                i4RetVal;

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "GROUP Update received with invalid "
                       "VLAN ID - %d \n", VlanId);
        return;
    }

    if (VLAN_IS_BCASTADDR (McastAddr) == VLAN_TRUE)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "\r\n!! Got Broadcast Address for Attribute in GMRP !!\r\n");

        return;
    }

    if (VLAN_IS_MCASTADDR (McastAddr) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "\r\n!! Address not Multicast Address !!\r\n");

        return;
    }

    /* Check for Vlan Reserved Address */
    if (VLAN_IS_RESERVED_ADDR (McastAddr) == VLAN_TRUE)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "\r\n!! Address is Reserved Mac Address for Attribute !!\r\n");

        return;
    }

    /* Check for GVRP Reserved Address */
    if (VLAN_DESTADDR_IS_GVRPADDR (McastAddr) == VLAN_TRUE)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "\r\n!! Address is GVRP Mac Address for Attribute !!\r\n");

        return;
    }

    /* Check for GMRP Reserved Address */
    if (VLAN_DESTADDR_IS_GMRPADDR (McastAddr) == VLAN_TRUE)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "\r\n!! Address is GMRP Mac Address for Attribute !!\r\n");

        return;
    }

    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "GROUP Add/Modify update message received for \n");

    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, "", "VLAN - %d \n", VlanId);

    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, "", "Mcast Address - ");

    VLAN_PRINT_MAC_ADDR (VLAN_RED_TRC, McastAddr);

    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, "", "Port List - ");

    VLAN_PRINT_PORT_LIST (VLAN_RED_TRC, PortList, 0);

    if (MEMCMP (PortList, gNullPortList, sizeof (tLocalPortList)) == 0)
    {
        /* Should delete GMRP learnt entry here */
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "GROUP Update with Empty portlist received. "
                  "Treating as if delete update is received. \n");
        VlanRedHandleGrpDelUpdate (VlanId, McastAddr);
        return;
    }

    pGmrpEntry = VlanRedModifyGmrpLearntEntry (VlanId, McastAddr, PortList);

    if (pGmrpEntry != NULL)
    {
        /* Entry already modified ... */
        return;
    }

    /* Entry not present in the database */
    pGmrpEntry = (tVlanRedGmrpLearntEntry *) (VOID *)
        VLAN_GET_BUF (VLAN_RED_GMRP_LEARNT_BUFF,
                      sizeof (tVlanRedGmrpLearntEntry));

    if (pGmrpEntry == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "No memory to store GROUP update message \n");

        return;
    }

    MEMSET (pGmrpEntry, 0, sizeof (tVlanRedGmrpLearntEntry));

    pGmrpEntry->VlanId = VlanId;

    MEMCPY (pGmrpEntry->McastAddr, McastAddr, ETHERNET_ADDR_SIZE);

    MEMCPY (pGmrpEntry->LearntPortList, PortList, sizeof (tLocalPortList));

    i4RetVal = VlanRedAddGmrpLearntEntry (pGmrpEntry);

    if (i4RetVal != VLAN_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "Adding GMRP learnt entry failed.\n");

        VLAN_RELEASE_BUF (VLAN_RED_GMRP_LEARNT_BUFF, pGmrpEntry);
    }
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleVlanDelUpdate                           */
/*                                                                           */
/* Description        : This function handles the VLAN delete update messages*/
/*                      received from the active node. This function deletes */
/*                      the VLAN record present in GVRP learnt ports table.  */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id to be deleted.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedHandleVlanDelUpdate (tVlanId VlanId)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "VLAN Delete Update received with invalid "
                       "VLAN ID - %d \n", VlanId);
        return;
    }

    pGvrpEntry = VlanRedGetGvrpLearntEntry (VlanId);

    if (pGvrpEntry == NULL)
    {
        /* Entry not present... */
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "VLAN Delete update for unknown VLAN - %d \n", VlanId);
        return;
    }

    VlanRedDelGvrpLearntEntry (VlanId);
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleGrpDelUpdate                            */
/*                                                                           */
/* Description        : This function handles the GROUP delete update msgs.  */
/*                      received from the active node. This function deletes */
/*                      the group entry from   the GMRP learnt ports table.  */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the delete upd. is recvd. */
/*                      McastAddr - Mcast address for which the delete       */
/*                                  update is received.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedHandleGrpDelUpdate (tVlanId VlanId, tMacAddr McastAddr)
{
    tVlanRedGmrpLearntEntry *pGmrpEntry;

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "GROUP Delete Update received with invalid "
                       "VLAN ID - %d \n", VlanId);
        return;
    }

    pGmrpEntry = VlanRedGetGmrpLearntEntry (VlanId, McastAddr);

    if (pGmrpEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "GROUP Delete update for unknown VLAN - %d and"
                       " Mac Addr - ", VlanId);
        VLAN_PRINT_MAC_ADDR (VLAN_RED_TRC, McastAddr);

        return;
    }

    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "GROUP Delete update for VLAN - %d and"
                   " Mac Addr - ", VlanId);
    VLAN_PRINT_MAC_ADDR (VLAN_RED_TRC, McastAddr);

    VLAN_RED_LOCK ();

    RBTreeRemove (gVlanRedInfo.GmrpLrntTable, (tRBElem *) pGmrpEntry);

    VLAN_RELEASE_BUF (VLAN_RED_GMRP_LEARNT_BUFF, (UINT1 *) pGmrpEntry);
    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleDelAllVlanInfo                          */
/*                                                                           */
/* Description        : This function handles the Delete all VLAN information*/
/*                      update message from the active node. If u4Port is 0, */
/*                      it deletes all the entries present in GVRP learnt    */
/*                      ports table. If u4Port != 0, then this function      */
/*                      scans and removes the u4Port from all port list in   */
/*                      the GVRP learnt ports table.                         */
/*                                                                           */
/* Input(s)           : u4Port - Port number to be deleted.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedHandleDelAllVlanInfo (UINT4 u4Port)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;
    tVlanRedGvrpLearntEntry *pNextGvrpEntry;

    if (u4Port == 0)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "Deleting VLAN Info on all ports \n");

        VlanRedDelAllGvrpLrntEntries ();

        return;
    }

    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "Deleting ALL VLAN Info on port %d \n", u4Port);

    /* Deleting on a specific port */

    VLAN_RED_LOCK ();

    pGvrpEntry =
        (tVlanRedGvrpLearntEntry *) VLAN_SLL_FIRST (&gVlanRedInfo.GvrpLrntList);

    while (pGvrpEntry != NULL)
    {
        pNextGvrpEntry = (tVlanRedGvrpLearntEntry *)
            VLAN_SLL_NEXT (&gVlanRedInfo.GvrpLrntList,
                           (tVLAN_SLL_NODE *) & pGvrpEntry->NextLearntEntry);

        VLAN_RESET_MEMBER_PORT (pGvrpEntry->LearntPortList, u4Port);

        if (MEMCMP (pGvrpEntry->LearntPortList, gNullPortList,
                    sizeof (tLocalPortList)) == 0)
        {
            VLAN_SLL_DEL (&gVlanRedInfo.GvrpLrntList,
                          (tVLAN_SLL_NODE *) & pGvrpEntry->NextLearntEntry);

            gVlanRedInfo.pGvrpLrntTable[pGvrpEntry->VlanId].pLearntEntry = NULL;

            VLAN_RELEASE_BUF (VLAN_RED_GVRP_LEARNT_BUFF, (UINT1 *) pGvrpEntry);
        }

        pGvrpEntry = pNextGvrpEntry;
    }

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleDelAllGrpInfo                           */
/*                                                                           */
/* Description        : This function handles the Delete all GROUP Info.     */
/*                      update message from the active node. If u4Port is 0, */
/*                      it deletes all the entries present in GMRP learnt    */
/*                      ports table. If u4Port != 0, then this function      */
/*                      scans and removes the u4Port from all port list in   */
/*                      the GMRP learnt ports table.                         */
/*                                                                           */
/* Input(s)           : u4Port - Port number to be deleted.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedHandleDelAllGrpInfo (UINT4 u4Port)
{
    tVlanRedGmrpLearntEntry *pGmrpEntry;
    tVlanRedGmrpLearntEntry *pNextGmrpEntry;

    UINT4               u4PhyPort;
    if (u4Port == 0)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "Deleting GROUP Info on all ports \n");

        VlanRedDelAllGmrpLrntEntries ();

        return;
    }
    u4PhyPort = VLAN_GET_IFINDEX (u4Port);
    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "Deleting ALL GROUP Info on port %d \n", u4PhyPort);

    VLAN_RED_LOCK ();

    for (pGmrpEntry = RBTreeGetFirst (gVlanRedInfo.GmrpLrntTable); pGmrpEntry;)
    {
        pNextGmrpEntry = RBTreeGetNext (gVlanRedInfo.GmrpLrntTable,
                                        (tRBElem *) pGmrpEntry, NULL);

        VLAN_RESET_MEMBER_PORT (pGmrpEntry->LearntPortList, u4Port);

        if (MEMCMP (pGmrpEntry->LearntPortList, gNullPortList,
                    sizeof (tLocalPortList)) == 0)
        {
            RBTreeRemove (gVlanRedInfo.GmrpLrntTable, (tRBElem *) pGmrpEntry);

            VLAN_RELEASE_BUF (VLAN_RED_GMRP_LEARNT_BUFF, pGmrpEntry);
        }

        pGmrpEntry = pNextGmrpEntry;
    }

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleUcastAddrDelOnTimeOut                   */
/*                                                                           */
/* Description        : This function deletes the unicast entry from         */
/*                      standby node when the corresponding entry gets deleted*/
/*                      from active node on ageout timer expiry.             */
/*                                                                           */
/* Input(s)           : u4FidIndex  - Fid Index                              */
/*                      UcastAddr   - Unicast address which has to be deleted*/
/*                      u4RcvPortId - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedHandleUcastAddrDelOnTimeOut (UINT4 u4FidIndex, tMacAddr UcastAddr,
                                    UINT4 RcvPortId)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanStUcastEntry  *pNextStUcastEntry = NULL;

    UNUSED_PARAM (RcvPortId);
    pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

    pStUcastEntry = pFidEntry->pStUcastTbl;

    while (pStUcastEntry != NULL)
    {
        pNextStUcastEntry = pStUcastEntry->pNextNode;
        if (VLAN_MEMCMP (pStUcastEntry->MacAddr, UcastAddr, VLAN_MAC_ADDR_LEN)
            == 0)
        {
            if ((VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE) &&
                (VLAN_DEF_RECVPORT == pStUcastEntry->u2RcvPort))
            {

                VlanPropagateMacInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                              pStUcastEntry->MacAddr,
                                              pStUcastEntry->pFdbEntry->VlanId,
                                              gNullPortList,
                                              pStUcastEntry->AllowedToGo);
            }

            VlanDeleteStUcastEntry (pFidEntry, pStUcastEntry);
            return VLAN_SUCCESS;
        }
        pStUcastEntry = pNextStUcastEntry;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleMcastAddrDelOnTimeOut                   */
/*                                                                           */
/* Description        : This function deletes the multicast entry from       */
/*                      standby node when the corresponding entry gets deleted*/
/*                      from active node on ageout timer expiry.             */
/*                                                                           */
/* Input(s)           : u2VlanId  - Vlan Id.                                 */
/*                      McastAddr - Multicast address which has to be deleted*/
/*                      u4RcvPortId - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedHandleMcastAddrDelOnTimeOut (UINT2 u2VlanId, tMacAddr McastAddr,
                                    UINT4 RcvPortId)
{
    UINT1              *pAddedPorts = NULL;
    tPortList          *pEgressPorts = NULL;
    tPortList          *pForbiddenPorts = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStMcastEntry  *pNextStMcastEntry = NULL;

    UNUSED_PARAM (RcvPortId);
    pVlanEntry = VlanGetVlanEntry (u2VlanId);

    if (pVlanEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedHandleMcastAddrDelOnTimeOut: "
                  "Error in allocating memory for pEgressPorts\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (*pEgressPorts, 0, sizeof (tPortList));

    pForbiddenPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbiddenPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedHandleMcastAddrDelOnTimeOut: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return VLAN_FAILURE;
    }
    MEMSET (*pForbiddenPorts, 0, sizeof (tPortList));

    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedHandleMcastAddrDelOnTimeOut: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
        return VLAN_FAILURE;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));

    pStMcastEntry = pVlanEntry->pStMcastTable;
    while (pStMcastEntry != NULL)
    {
        pNextStMcastEntry = pStMcastEntry->pNextNode;
        if (VLAN_MEMCMP (pStMcastEntry->MacAddr, McastAddr, VLAN_MAC_ADDR_LEN)
            == 0)
        {
            if (VLAN_DEF_RECVPORT == pStMcastEntry->u2RcvPort)
            {
                VlanAttrRPSetMcastForbiddPorts (VLAN_CURR_CONTEXT_ID (),
                                                pStMcastEntry->MacAddr,
                                                pVlanEntry->VlanId,
                                                pAddedPorts,
                                                pStMcastEntry->ForbiddenPorts);

                VlanPropagateMacInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                              pStMcastEntry->MacAddr,
                                              pVlanEntry->VlanId, pAddedPorts,
                                              pStMcastEntry->EgressPorts);

                if (SNOOP_ENABLED == VlanSnoopIsIgmpSnoopingEnabled
                    (VLAN_CURR_CONTEXT_ID ()) ||
                    SNOOP_ENABLED == VlanSnoopIsMldSnoopingEnabled
                    (VLAN_CURR_CONTEXT_ID ()))
                {
                    /* Mcast Forbid Ports needs to be added now since entry 
                     * is deleted */
                    /* For MI the port list to the externa modules should be 
                       physical port list so convert and pass it */
                    MEMSET (*pEgressPorts, 0, sizeof (tPortList));
                    MEMSET (*pForbiddenPorts, 0, sizeof (tPortList));

                    VlanConvertToIfPortList (pStMcastEntry->ForbiddenPorts,
                                             *pForbiddenPorts);
                    VlanConvertToIfPortList (pAddedPorts, *pEgressPorts);

                    VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                               pVlanEntry->VlanId,
                                               pStMcastEntry->MacAddr,
                                               *pForbiddenPorts,
                                               *pEgressPorts,
                                               VLAN_UPDT_MCAST_PORTS);
                }

            }

            VlanDeleteStMcastEntry (pVlanEntry, pStMcastEntry, VLAN_TRUE);
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            UtilPlstReleaseLocalPortList (pAddedPorts);
            return VLAN_SUCCESS;

        }
        pStMcastEntry = pNextStMcastEntry;
    }
    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
    UtilPlstReleaseLocalPortList (pAddedPorts);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanRedHandlePortOperStatusUpdate                    */
/*                                                                           */
/* Description        : This function handles the Delete all GROUP Info.     */
/*                      update message from the active node. If u4Port is 0, */
/*                      it deletes all the entries present in GMRP learnt    */
/*                      ports table. If u4Port != 0, then this function      */
/*                      scans and removes the u4Port from all port list in   */
/*                      the GMRP learnt ports table.                         */
/*                                                                           */
/* Input(s)           : u4Port   - Port number.                              */
/*                    : u1Status - Port Oper status.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedHandlePortOperStatusUpdate (UINT4 u4Port, UINT1 u1Status)
{
    VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                   "\r\nHandling Oper sync up for Port %d", u4Port);

    if (VLAN_GET_PORT_ENTRY (u4Port) == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "Port entry %d not present \n", u4Port);
        return VLAN_SUCCESS;
    }

#ifdef GARP_WANTED
    VlanGarpPortOperInd ((UINT2) (VLAN_GET_IFINDEX (u4Port)), u1Status);
#endif

    if (VlanHandlePortOperInd ((UINT2) u4Port, u1Status) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VLAN Port Oper Ind sync up handle failed "
                       "for Port %d \n", u4Port);
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedGetGvrpLearntEntry                            */
/*                                                                           */
/* Description        : This function returns the GVRP learnt entry for the  */
/*                      given VLAN Id from the GVRP learnt ports table.      */
/*                                                                           */
/* Input(s)           : VlanId - VLAN id of the entry to be retrieved.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pGvrpEntry / NULL                                    */
/*                                                                           */
/*****************************************************************************/
tVlanRedGvrpLearntEntry *
VlanRedGetGvrpLearntEntry (tVlanId VlanId)
{

    tVlanRedGvrpLearntEntry *pGvrpEntry;

    VLAN_RED_LOCK ();

    pGvrpEntry = gVlanRedInfo.pGvrpLrntTable[VlanId].pLearntEntry;

    VLAN_RED_UNLOCK ();

    return pGvrpEntry;
}

/*****************************************************************************/
/* Function Name      : VlanRedModifyGvrpLearntEntry                         */
/*                                                                           */
/* Description        : This function modifies the GVRP learnt port list     */
/*                      with the given port list if the GVRP learnt port     */
/*                      entry is present in the database. If not present     */
/*                      this function just returns.                          */
/*                                                                           */
/* Input(s)           : VlanId - VLAN id of the entry to be retrieved.       */
/*                      PortList - Port list to be overwritten if present.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pGvrpEntry - if entry is present                     */
/*                      NULL - if the entry is not present                   */
/*                                                                           */
/*****************************************************************************/
tVlanRedGvrpLearntEntry *
VlanRedModifyGvrpLearntEntry (tVlanId VlanId, tLocalPortList PortList)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;
    pGvrpEntry = NULL;

    VLAN_RED_LOCK ();

    pGvrpEntry = gVlanRedInfo.pGvrpLrntTable[VlanId].pLearntEntry;

    if (pGvrpEntry != NULL)
    {
        MEMCPY (pGvrpEntry->LearntPortList, PortList, sizeof (tLocalPortList));
    }

    VLAN_RED_UNLOCK ();

    return pGvrpEntry;
}

/*****************************************************************************/
/* Function Name      : VlanRedAddGvrpLearntEntry                            */
/*                                                                           */
/* Description        : This function adds the given GVRP learnt entry to    */
/*                      the GVRP learnt ports table.                         */
/*                                                                           */
/* Input(s)           : pGvrpEntry - Pointer to the entry to be added.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedAddGvrpLearntEntry (tVlanRedGvrpLearntEntry * pGvrpEntry)
{
    VLAN_RED_LOCK ();

    gVlanRedInfo.pGvrpLrntTable[pGvrpEntry->VlanId].pLearntEntry = pGvrpEntry;

    VLAN_SLL_ADD (&gVlanRedInfo.GvrpLrntList,
                  (tVLAN_SLL_NODE *) & pGvrpEntry->NextLearntEntry);

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDelGvrpLearntEntry                            */
/*                                                                           */
/* Description        : This function deletes the given GVRP learnt entry    */
/*                      from the GVRP learnt ports table.                    */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id of the entry to be deleted.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelGvrpLearntEntry (tVlanId VlanId)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;

    VLAN_RED_LOCK ();

    pGvrpEntry = gVlanRedInfo.pGvrpLrntTable[VlanId].pLearntEntry;

    if (pGvrpEntry != NULL)
    {
        VLAN_SLL_DEL (&gVlanRedInfo.GvrpLrntList,
                      (tVLAN_SLL_NODE *) & pGvrpEntry->NextLearntEntry);

        gVlanRedInfo.pGvrpLrntTable[VlanId].pLearntEntry = NULL;

        VLAN_RELEASE_BUF (VLAN_RED_GVRP_LEARNT_BUFF, (UINT1 *) pGvrpEntry);
    }

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedGetGmrpLearntEntry                            */
/*                                                                           */
/* Description        : This function returns the GMRP learnt entry for the  */
/*                      given VLAN Id from the GMRP learnt ports table.      */
/*                                                                           */
/* Input(s)           : VlanId - VLAN id of the entry to be retrieved.       */
/*                      McastAddr - Multicast mac address to be retrieved.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pGmrpEntry / NULL                                    */
/*                                                                           */
/*****************************************************************************/
tVlanRedGmrpLearntEntry *
VlanRedGetGmrpLearntEntry (tVlanId VlanId, tMacAddr McastAddr)
{
    tVlanRedGmrpLearntEntry GmrpEntry;
    tVlanRedGmrpLearntEntry *pGmrpEntry;
    pGmrpEntry = NULL;

    VLAN_RED_LOCK ();

    GmrpEntry.VlanId = VlanId;
    VLAN_CPY_MAC_ADDR (GmrpEntry.McastAddr, McastAddr);

    pGmrpEntry = RBTreeGet (gVlanRedInfo.GmrpLrntTable,
                            (tRBElem *) (&GmrpEntry));

    VLAN_RED_UNLOCK ();
    return pGmrpEntry;
}

/*****************************************************************************/
/* Function Name      : VlanRedModifyGmrpLearntEntry                         */
/*                                                                           */
/* Description        : This function overwrites the learnt port list with   */
/*                      given port list if the GMRP learnt entry is present  */
/*                      for the given Vlan ID and mcast addr in the GMRP     */
/*                      learnt ports table. If not present this function     */
/*                      just returns.                                        */
/*                                                                           */
/* Input(s)           : VlanId - VLAN id of the entry                        */
/*                      McastAddr - Multicast mac address                    */
/*                      PortList - Port list to be overwritten               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
tVlanRedGmrpLearntEntry *
VlanRedModifyGmrpLearntEntry (tVlanId VlanId,
                              tMacAddr McastAddr, tLocalPortList PortList)
{
    tVlanRedGmrpLearntEntry GmrpEntry;
    tVlanRedGmrpLearntEntry *pGmrpEntry;

    pGmrpEntry = NULL;

    VLAN_RED_LOCK ();

    GmrpEntry.VlanId = VlanId;
    VLAN_CPY_MAC_ADDR (GmrpEntry.McastAddr, McastAddr);

    pGmrpEntry = RBTreeGet (gVlanRedInfo.GmrpLrntTable,
                            (tRBElem *) (&GmrpEntry));

    if (pGmrpEntry != NULL)
    {
        MEMCPY (pGmrpEntry->LearntPortList, PortList, sizeof (tLocalPortList));
    }

    VLAN_RED_UNLOCK ();

    return pGmrpEntry;
}

/*****************************************************************************/
/* Function Name      : VlanRedAddGmrpLearntEntry                            */
/*                                                                           */
/* Description        : This function adds the given GMRP learnt entry to    */
/*                      the GMRP learnt ports table.                         */
/*                                                                           */
/* Input(s)           : pGmrpEntry - Pointer to the entry to be added.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedAddGmrpLearntEntry (tVlanRedGmrpLearntEntry * pGmrpEntry)
{
    INT4                i4RetVal;

    VLAN_RED_LOCK ();

    i4RetVal = RBTreeAdd (gVlanRedInfo.GmrpLrntTable, (tRBElem *) pGmrpEntry);

    if (i4RetVal != RB_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "Adding node to GMRP learnt ports tree"
                  " failed \n");

        VLAN_RED_UNLOCK ();

        return VLAN_FAILURE;
    }

    VLAN_RED_UNLOCK ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedDelAllGvrpLrntEntries                         */
/*                                                                           */
/* Description        : This function deletes all the entries from the GVRP  */
/*                      learnt ports table.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelAllGvrpLrntEntries (VOID)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;

    VLAN_RED_LOCK ();

    /* Delete ALL Entries in the GVRP learnt ports table */
    pGvrpEntry = (tVlanRedGvrpLearntEntry *)
        VLAN_SLL_GET (&gVlanRedInfo.GvrpLrntList);

    while (pGvrpEntry != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_RED_GVRP_LEARNT_BUFF, (UINT1 *) pGvrpEntry);

        pGvrpEntry = (tVlanRedGvrpLearntEntry *)
            VLAN_SLL_GET (&gVlanRedInfo.GvrpLrntList);
    }

    MEMSET (gVlanRedInfo.pGvrpLrntTable, 0,
            ((VLAN_MAX_VLAN_ID + 1) * sizeof (tVlanRedVlanArray)));

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedDelAllGmrpLrntEntries                         */
/*                                                                           */
/* Description        : This function deletes all the entries from the GVRP  */
/*                      learnt ports table.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelAllGmrpLrntEntries (VOID)
{
    VLAN_RED_LOCK ();

    RBTreeDrain (gVlanRedInfo.GmrpLrntTable, VlanRedGmrpLrntFreeFn, 0);

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VlanRedSetVlanChangedFlag                            */
/*                                                                           */
/* Description        : This function sets the changed flag in the VLAN      */
/*                      current entry.                                       */
/*                                                                           */
/* Input(s)           : pVlanEntry - Pointer to the VLAN entry               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedSetVlanChangedFlag (tVlanCurrEntry * pVlanEntry)
{
    UNUSED_PARAM (pVlanEntry);

    return;

}

/*****************************************************************************/
/* Function Name      : VlanRedSetGroupChangedFlag                           */
/*                                                                           */
/* Description        : This function sets the changed flag in the VLAN      */
/*                      Group entry.                                         */
/*                                                                           */
/* Input(s)           : pVlanEntry - Pointer to the VLAN entry               */
/*                      pGroupEntry - Pointer to the Group entry             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedSetGroupChangedFlag (tVlanCurrEntry * pVlanEntry,
                            tVlanGroupEntry * pGroupEntry)
{
    UNUSED_PARAM (pVlanEntry);
    UNUSED_PARAM (pGroupEntry);

    return;

}

/*****************************************************************************/
/* Function Name      : VlanRedGetVlanDelHashIndex                           */
/*                                                                           */
/* Description        : This function returns the hash index into the VLAN   */
/*                      deleted table based on the hash key VLAN Id.         */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which hash index needs to be    */
/*                               derived.                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Hash index.                                          */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRedGetVlanDelHashIndex (tVlanId VlanId)
{
    UINT4               u4HashIndex;

    u4HashIndex = VLAN_HASH_2BYTE (VlanId) % VLAN_RED_MAX_VLAN_DEL_BKTS;

    return u4HashIndex;
}

/*****************************************************************************/
/* Function Name      : VlanRedAddVlanDeletedEntry                           */
/*                                                                           */
/* Description        : This function adds a VLAN deleted entry into VLAN    */
/*                      deleted table. This function is invoked whenever a   */
/*                      VLAN entry is deleted as a result of last member     */
/*                      leaving the group.                                   */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               added.                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedAddVlanDeletedEntry (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);

    return;

}

/*****************************************************************************/
/* Function Name      : VlanRedDelVlanDeletedEntry                           */
/*                                                                           */
/* Description        : This function deletes the VLAN deleted entry from    */
/*                      the VLAN deleted table.                              */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               deleted.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelVlanDeletedEntry (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedGetGroupDelHashIndex                          */
/*                                                                           */
/* Description        : This function returns the hash index into the GROUP  */
/*                      deleted table based on the hash key VLAN Id and      */
/*                      multicast mac address.                               */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which hash index needs to be    */
/*                               derived.                                    */
/*                      McastAddr - Multicast Mac Address                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Hash index.                                          */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRedGetGroupDelHashIndex (tVlanId VlanId, tMacAddr McastAddr)
{
    UINT4               u4HashIndex;

    u4HashIndex = VLAN_HASH_2BYTE (VlanId);
    u4HashIndex += VLAN_HASH_6BYTE (McastAddr);

    u4HashIndex = u4HashIndex % VLAN_RED_MAX_GRP_DEL_BKTS;

    return u4HashIndex;
}

/*****************************************************************************/
/* Function Name      : VlanRedAddGroupDeletedEntry                          */
/*                                                                           */
/* Description        : This function adds a GROOUP deleted entry into GROUP */
/*                      deleted table. This function is invoked whenever a   */
/*                      GROUP entry is deleted as a result of last member    */
/*                      leaving the multicast group.                         */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               added.                                      */
/*                      McastAddr - Multicast address for which the entry    */
/*                               needs to be added.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedAddGroupDeletedEntry (tVlanId VlanId, tMacAddr McastAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (McastAddr);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedDelGroupDeletedEntry                          */
/*                                                                           */
/* Description        : This function deletes the GROUP deleted entry from   */
/*                      the GROUP Deleted table.                             */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               added.                                      */
/*                      McastAddr - Multicast mac address.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelGroupDeletedEntry (tVlanId VlanId, tMacAddr McastAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (McastAddr);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedLock                                          */
/*                                                                           */
/* Description        : This function is used to take the VLAN redundancy    */
/*                      mutual exclusion sema4. This mutex is used to        */
/*                      protect the data structures added to support         */
/*                      redundancy like GVRP deleted table, GMRP deleted     */
/*                      table, GVRP learnt table and GMRP learnt table.      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedLock (VOID)
{
    if (VLAN_TAKE_SEM (gVlanRedTaskInfo.RedSemId) != OSIX_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME,
                  "MSG: Failed to Take VLAN Redundancy Sema4 \n");
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedUnLock                                        */
/*                                                                           */
/* Description        : This function is used to give the VLAN redundancy    */
/*                      sema4.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedUnLock (VOID)
{
    VLAN_RELEASE_SEM (gVlanRedTaskInfo.RedSemId);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedGmrpLrntFreeFn                                */
/*                                                                           */
/* Description        : This function will be invoked to free the nodes in   */
/*                      the GMRP learnt ports tree.                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanRedGmrpLrntFreeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    VLAN_RELEASE_BUF (VLAN_RED_GMRP_LEARNT_BUFF, (UINT1 *) pRBElem);

    return RB_SUCCESS;
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAddMcastCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedAddMcastCacheEntry (tVlanRedMcastCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedMcastCacheNode *pVlanRedCacheNode = NULL;
    pVlanRedCacheNode =
        (tVlanRedMcastCacheNode *) (VOID *)
        VLAN_GET_BUF (VLAN_RED_MCAST_CACHE_ENTRY,
                      sizeof (tVlanRedMcastCacheNode));
    if (NULL != pVlanRedCacheNode)
    {
        MEMCPY (pVlanRedCacheNode, pUpdtVlanRedCacheNode,
                sizeof (tVlanRedMcastCacheNode));
        if (RB_SUCCESS != RBTreeAdd (gVlanRedInfo.VlanRedMcastCacheTable,
                                     (tRBElem *) (pVlanRedCacheNode)))
        {
            VLAN_RELEASE_BUF (VLAN_RED_MCAST_CACHE_ENTRY, pVlanRedCacheNode);
            return VLAN_FAILURE;
        }
    }
    else
    {
        /* Memory allocation failed for Static Multicast Table.
         * Sync-up failure has reached the maximum limit. 
         * Allowed further could lead to configuration loss during audit.
         * Hence, Reverting to full audit mode from optimal audit mechanism.
         */
        gVlanRedInfo.u1ForceFullAudit |= VLAN_RED_MCAST_TABLE_MASK;
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN Multicast Table - Switching from optimal audit "
                  "to full audit mechanism.\n");
        RBTreeDrain (gVlanRedInfo.VlanRedMcastCacheTable,
                     VlanRedMcastCacheFreeNodes, 0);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedGetMcastCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to fetch an entry from    */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
tVlanRedMcastCacheNode *
VlanRedGetMcastCacheEntry (UINT4 u4ContextId,
                           tMacAddr MacAddress, tVlanId VlanId, UINT4 u4RcvPort)
{
    tVlanRedMcastCacheNode VlanRedCacheNode;
    tVlanRedMcastCacheNode *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedMcastCacheNode));

    VlanRedCacheNode.VlanId = VlanId;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4RcvPort = u4RcvPort;
    MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

    pVlanRedCacheNode = (tVlanRedMcastCacheNode *)
        RBTreeGet (gVlanRedInfo.VlanRedMcastCacheTable,
                   (tRBElem *) & VlanRedCacheNode);
    return pVlanRedCacheNode;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedDelMcastCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to delete a node from     */
/*                    VlanRedMcastCacheTable.                               */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedDelMcastCacheEntry (UINT4 u4ContextId,
                           tMacAddr MacAddress, tVlanId VlanId, UINT4 u4RcvPort)
{
    tVlanRedMcastCacheNode VlanRedCacheNode;
    tVlanRedMcastCacheNode *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedMcastCacheNode));

    VlanRedCacheNode.VlanId = VlanId;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4RcvPort = u4RcvPort;
    MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

    pVlanRedCacheNode = (tVlanRedMcastCacheNode *)
        RBTreeGet (gVlanRedInfo.VlanRedMcastCacheTable,
                   (tRBElem *) & VlanRedCacheNode);
    if (NULL != pVlanRedCacheNode)
    {
        RBTreeRem (gVlanRedInfo.VlanRedMcastCacheTable,
                   (tRBElem *) pVlanRedCacheNode);
        VLAN_RELEASE_BUF (VLAN_RED_MCAST_CACHE_ENTRY, pVlanRedCacheNode);
        pVlanRedCacheNode = NULL;
        return VLAN_SUCCESS;
    }

    return VLAN_FAILURE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAddStUcastCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedStUcastCacheTable                                */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedAddStUcastCacheEntry (tVlanRedStUcastCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedStUcastCacheNode *pVlanRedCacheNode = NULL;

    pVlanRedCacheNode =
        (tVlanRedStUcastCacheNode *) (VOID *)
        VLAN_GET_BUF (VLAN_RED_ST_UCAST_CACHE_ENTRY,
                      sizeof (tVlanRedStUcastCacheNode));
    if (NULL != pVlanRedCacheNode)
    {
        MEMCPY (pVlanRedCacheNode, pUpdtVlanRedCacheNode,
                sizeof (tVlanRedStUcastCacheNode));
        if (RB_SUCCESS != RBTreeAdd (gVlanRedInfo.VlanRedStUcastCacheTable,
                                     (tRBElem *) (pVlanRedCacheNode)))
        {
            VLAN_RELEASE_BUF (VLAN_RED_ST_UCAST_CACHE_ENTRY, pVlanRedCacheNode);
            return VLAN_FAILURE;
        }
    }
    else
    {
        /* Memory allocation failed for Static unicast Table.
         * Sync-up failure has reached the maximum limit. 
         * Allowed further could lead to configuration loss during audit.
         * Hence, Reverting to full audit mode from optimal audit mechanism.
         */
        gVlanRedInfo.u1ForceFullAudit |= VLAN_RED_UCAST_TABLE_MASK;
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN Static unicast Table - Switching from optimal audit "
                  "to full audit mechanism.\n");
        RBTreeDrain (gVlanRedInfo.VlanRedStUcastCacheTable,
                     VlanRedStUcastCacheFreeNodes, 0);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedGetProtoVlanCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to fetch an entry from    */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/

tVlanRedProtoVlanCacheNode *
VlanRedGetProtoVlanCacheEntry (UINT4 u4ContextId, UINT4 u4GroupId, UINT2 u2Port)
{
    tVlanRedProtoVlanCacheNode VlanRedCacheNode;
    tVlanRedProtoVlanCacheNode *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedProtoVlanCacheNode));

    VlanRedCacheNode.u2Port = u2Port;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4GroupId = u4GroupId;

    pVlanRedCacheNode = (tVlanRedProtoVlanCacheNode *)
        RBTreeGet (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                   (tRBElem *) & VlanRedCacheNode);
    return pVlanRedCacheNode;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedGetStUcastCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to fetch an entry from    */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/

tVlanRedStUcastCacheNode *
VlanRedGetStUcastCacheEntry (UINT4 u4ContextId,
                             tMacAddr MacAddress,
                             UINT4 u4FdbId, UINT4 u4RcvPort)
{
    tVlanRedStUcastCacheNode VlanRedCacheNode;
    tVlanRedStUcastCacheNode *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedStUcastCacheNode));

    VlanRedCacheNode.u4FdbId = u4FdbId;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4RcvPort = u4RcvPort;
    MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

    pVlanRedCacheNode = (tVlanRedStUcastCacheNode *)
        RBTreeGet (gVlanRedInfo.VlanRedStUcastCacheTable,
                   (tRBElem *) & VlanRedCacheNode);
    return pVlanRedCacheNode;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedDelStUcastCacheEntry                             */
/*                                                                            */
/*  Description     : This function will be invoked to delete a node from     */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedDelStUcastCacheEntry (UINT4 u4ContextId,
                             tMacAddr MacAddress,
                             UINT4 u4FdbId, UINT4 u4RcvPort)
{
    tVlanRedStUcastCacheNode VlanRedCacheNode;
    tVlanRedStUcastCacheNode *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedStUcastCacheNode));

    VlanRedCacheNode.u4FdbId = u4FdbId;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4RcvPort = u4RcvPort;
    MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

    pVlanRedCacheNode = (tVlanRedStUcastCacheNode *)
        RBTreeGet (gVlanRedInfo.VlanRedStUcastCacheTable,
                   (tRBElem *) & VlanRedCacheNode);
    if (NULL != pVlanRedCacheNode)
    {
        RBTreeRem (gVlanRedInfo.VlanRedStUcastCacheTable,
                   (tRBElem *) pVlanRedCacheNode);
        VLAN_RELEASE_BUF (VLAN_RED_ST_UCAST_CACHE_ENTRY, pVlanRedCacheNode);
        pVlanRedCacheNode = NULL;
        return VLAN_SUCCESS;
    }

    return VLAN_FAILURE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAddProtoVlanCacheEntry                           */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedProtoVlanCacheTable.                             */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure                        */
/*                    tVlanRedProtoVlanCacheNode                              */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedAddProtoVlanCacheEntry (tVlanRedProtoVlanCacheNode *
                               pUpdtVlanRedCacheNode)
{
    tVlanRedProtoVlanCacheNode *pVlanRedCacheNode = NULL;
    pVlanRedCacheNode =
        (tVlanRedProtoVlanCacheNode *) (VOID *)
        VLAN_GET_BUF (VLAN_RED_PROTO_VLAN_CACHE_ENTRY,
                      sizeof (tVlanRedProtoVlanCacheNode));
    if (NULL != pVlanRedCacheNode)
    {
        MEMCPY (pVlanRedCacheNode, pUpdtVlanRedCacheNode,
                sizeof (tVlanRedProtoVlanCacheNode));
        if (RB_SUCCESS != RBTreeAdd (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                                     (tRBElem *) (pVlanRedCacheNode)))
        {
            VLAN_RELEASE_BUF (VLAN_RED_PROTO_VLAN_CACHE_ENTRY,
                              pVlanRedCacheNode);
            return VLAN_FAILURE;
        }
    }
    else
    {
        /* Memory allocation failed for Protocol based VLAN Table.
         * Sync-up failure has reached the maximum limit. 
         * Allowed further could lead to configuration loss during audit.
         * Hence, Reverting to full audit mode from optimal audit mechanism.
         */
        gVlanRedInfo.u1ForceFullAudit |= VLAN_RED_PROTO_TABLE_MASK;
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN Proto VLAN Table - Switching from optimal audit "
                  "to full audit mechanism.\n");

        RBTreeDrain (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                     VlanRedProtoVlanCacheFreeNodes, 0);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAddCacheEntry                                    */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedAddCacheEntry (tVlanRedCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedCacheNode  *pVlanRedCacheNode = NULL;
    pVlanRedCacheNode =
        (tVlanRedCacheNode *) (VOID *) VLAN_GET_BUF (VLAN_RED_CACHE_ENTRY,
                                                     sizeof
                                                     (tVlanRedCacheNode));
    if (NULL != pVlanRedCacheNode)
    {
        MEMCPY (pVlanRedCacheNode, pUpdtVlanRedCacheNode,
                sizeof (tVlanRedCacheNode));
        if (RB_SUCCESS !=
            RBTreeAdd (gVlanRedInfo.VlanRedCacheTable,
                       (tRBElem *) (pVlanRedCacheNode)))
        {
            VLAN_RELEASE_BUF (VLAN_RED_CACHE_ENTRY, pVlanRedCacheNode);
            return VLAN_FAILURE;
        }
    }
    else
    {
        /* Memory allocation failed for  VLAN Table.
         * Sync-up failure has reached the maximum limit. 
         * Allowed further could lead to configuration loss during audit.
         * Hence, Reverting to full audit mode from optimal audit mechanism.
         */
        gVlanRedInfo.u1ForceFullAudit |= VLAN_RED_VLAN_TABLE_MASK;
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN Port Membership Table - Switching from optimal audit "
                  "to full audit mechanism.\n");

        RBTreeDrain (gVlanRedInfo.VlanRedCacheTable, VlanRedCacheFreeNodes, 0);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedGetCacheEntry                                    */
/*                                                                            */
/*  Description     : This function will be invoked to fetch an entry from    */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/

tVlanRedCacheNode  *
VlanRedGetCacheEntry (UINT4 u4ContextId, tVlanId VlanId)
{
    tVlanRedCacheNode   VlanRedCacheNode;
    tVlanRedCacheNode  *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedCacheNode));

    VlanRedCacheNode.VlanId = VlanId;
    VlanRedCacheNode.u4ContextId = u4ContextId;

    pVlanRedCacheNode =
        (tVlanRedCacheNode *) RBTreeGet (gVlanRedInfo.VlanRedCacheTable,
                                         (tRBElem *) & VlanRedCacheNode);
    return pVlanRedCacheNode;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedDelCacheEntry                                    */
/*                                                                            */
/*  Description     : This function will be invoked to delete a node from     */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedDelCacheEntry (UINT4 u4ContextId, tVlanId VlanId)
{
    tVlanRedCacheNode   VlanRedCacheNode;
    tVlanRedCacheNode  *pVlanRedCacheNode = NULL;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedCacheNode));

    VlanRedCacheNode.VlanId = VlanId;
    VlanRedCacheNode.u4ContextId = u4ContextId;

    pVlanRedCacheNode =
        (tVlanRedCacheNode *) RBTreeGet (gVlanRedInfo.VlanRedCacheTable,
                                         (tRBElem *) & VlanRedCacheNode);
    if (NULL != pVlanRedCacheNode)
    {
        RBTreeRem (gVlanRedInfo.VlanRedCacheTable,
                   (tRBElem *) pVlanRedCacheNode);
        VLAN_RELEASE_BUF (VLAN_RED_CACHE_ENTRY, pVlanRedCacheNode);
        pVlanRedCacheNode = NULL;
        return VLAN_SUCCESS;
    }

    return VLAN_FAILURE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedDelProtoVlanCache                                */
/*                                                                            */
/*  Description     : This function will be invoked to delete a node from     */
/*                    VlanRedProtoVlanTable.                                  */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    Group Id    - Group Identifier                          */
/*                    u2Port - Port Id                                        */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedDelProtoVlanCache (UINT4 u4ContextId, UINT4 u4GroupId, UINT2 u2Port)
{
    tVlanRedProtoVlanCacheNode VlanRedProtoVlanNode;
    tVlanRedProtoVlanCacheNode *pVlanRedProtoVlanNode = NULL;

    MEMSET (&VlanRedProtoVlanNode, 0, sizeof (tVlanRedProtoVlanCacheNode));

    VlanRedProtoVlanNode.u2Port = u2Port;
    VlanRedProtoVlanNode.u4GroupId = u4GroupId;
    VlanRedProtoVlanNode.u4ContextId = u4ContextId;

    pVlanRedProtoVlanNode = (tVlanRedProtoVlanCacheNode *)
        RBTreeGet (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                   (tRBElem *) & VlanRedProtoVlanNode);
    if (NULL != pVlanRedProtoVlanNode)
    {
        RBTreeRem (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                   (tRBElem *) pVlanRedProtoVlanNode);
        VLAN_RELEASE_BUF (VLAN_RED_PROTO_VLAN_CACHE_ENTRY,
                          pVlanRedProtoVlanNode);
        pVlanRedProtoVlanNode = NULL;
        return VLAN_SUCCESS;
    }

    return VLAN_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : VlanRedCacheRBTreeCmp                                  */
/*  Description     : This function is invoked by the RB Tree library during */
/*                    RB Tree traversal.                                     */
/*  Input(s)        : pRBElem1 - Pointer to the first Entry                  */
/*                    pRBElem2 - Pointer to the second Entry                 */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  1, if context1 is greater than context2   */
/*                                 -1, if context2 is greater than context1  */
/*                                 1, if vlan1 is greater than vlan2         */
/*                                 -1 if vlan2 is greater than vlan1         */
/*                                 0 if(context1==context2) && (vlan1==vlan2)*/
/*****************************************************************************/
INT4
VlanRedCacheRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVlanRedCacheNode  *pVlanRedCacheNode1 = (tVlanRedCacheNode *) pRBElem1;
    tVlanRedCacheNode  *pVlanRedCacheNode2 = (tVlanRedCacheNode *) pRBElem2;

    if (pVlanRedCacheNode1->u4ContextId > pVlanRedCacheNode2->u4ContextId)
    {
        return 1;
    }

    if (pVlanRedCacheNode1->u4ContextId < pVlanRedCacheNode2->u4ContextId)
    {
        return -1;
    }

    if (pVlanRedCacheNode1->VlanId > pVlanRedCacheNode2->VlanId)
    {
        return 1;
    }
    if (pVlanRedCacheNode1->VlanId < pVlanRedCacheNode2->VlanId)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanRedMcastCacheFreeNodes                                 */
                                                                                                                                                                        /*                                                                           *//* Description  : Frees VLAN redundancy RBTree Cache nodes                   */
                                                                                                                                                                        /*                                                                           *//* Input        : pRBElem- Pointer to the RBTree CAche nodes                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
VlanRedMcastCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_RED_MCAST_CACHE_ENTRY, pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanRedStUcastCacheFreeNodes                               */
                                                                                                                                                                        /*                                                                           *//* Description  : Frees VLAN redundancy RBTree Cache nodes                   */
                                                                                                                                                                        /*                                                                           *//* Input        : pRBElem- Pointer to the RBTree CAche nodes                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
VlanRedStUcastCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_RED_ST_UCAST_CACHE_ENTRY, pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanRedCacheFreeNodes                                      */
                                                                                                                                                                        /*                                                                           *//* Description  : Frees VLAN redundancy RBTree Cache nodes                   */
                                                                                                                                                                        /*                                                                           *//* Input        : pRBElem- Pointer to the RBTree CAche nodes                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
VlanRedCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_RED_CACHE_ENTRY, pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanRedProtoVlanCacheFreeNodes                                      */
                                                                                                                                                                        /*                                                                           *//* Description  : Frees VLAN redundancy RBTree Cache nodes                   */
                                                                                                                                                                        /*                                                                           *//* Input        : pRBElem- Pointer to the RBTree CAche nodes                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
VlanRedProtoVlanCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_RED_PROTO_VLAN_CACHE_ENTRY, pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : VlanRedMcastProtoVlanRBTreeCmp                         */
/*  Description     : This function is invoked by the RB Tree library during */
/*                    RB Tree traversal.                                     */
/*  Input(s)        : pRBElem1 - Pointer to the first Entry                  */
/*                    pRBElem2 - Pointer to the second Entry                 */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  1, if context1 is greater than context2   */
/*                                 -1, if context2 is greater than context1  */
/*                                 1, if vlan1 is greater than vlan2         */
/*                                 -1 if vlan2 is greater than vlan1         */
/*                                 0 if(context1==context2) && (vlan1==vlan2)*/
/*****************************************************************************/
INT4
VlanRedProtoVlanRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVlanRedProtoVlanCacheNode *pVlanRedProtoVlanNode1 =
        (tVlanRedProtoVlanCacheNode *) pRBElem1;
    tVlanRedProtoVlanCacheNode *pVlanRedProtoVlanNode2 =
        (tVlanRedProtoVlanCacheNode *) pRBElem2;

    if (pVlanRedProtoVlanNode1->u4ContextId >
        pVlanRedProtoVlanNode2->u4ContextId)
    {
        return 1;
    }

    if (pVlanRedProtoVlanNode1->u4ContextId <
        pVlanRedProtoVlanNode2->u4ContextId)
    {
        return -1;
    }

    if (pVlanRedProtoVlanNode1->u4GroupId > pVlanRedProtoVlanNode2->u4GroupId)
    {
        return 1;
    }

    if (pVlanRedProtoVlanNode1->u4GroupId < pVlanRedProtoVlanNode2->u4GroupId)
    {
        return -1;
    }

    if (pVlanRedProtoVlanNode1->u2Port > pVlanRedProtoVlanNode2->u2Port)
    {
        return 1;
    }
    if (pVlanRedProtoVlanNode1->u2Port < pVlanRedProtoVlanNode2->u2Port)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/*  Function Name   : VlanRedMcastCacheRBTreeCmp                           */
/*  Description     : This function is invoked by the RB Tree library during */
/*                    RB Tree traversal.                                     */
/*  Input(s)        : pRBElem1 - Pointer to the first Entry                  */
/*                    pRBElem2 - Pointer to the second Entry                 */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  1, if context1 is greater than context2   */
/*                                 -1, if context2 is greater than context1  */
/*                                 1, if vlan1 is greater than vlan2         */
/*                                 -1 if vlan2 is greater than vlan1         */
/*                                 0 if(context1==context2) && (vlan1==vlan2)*/
/*****************************************************************************/
INT4
VlanRedMcastCacheRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVlanRedMcastCacheNode *pVlanRedMcastCacheNode1 = (tVlanRedMcastCacheNode *)
        pRBElem1;
    tVlanRedMcastCacheNode *pVlanRedMcastCacheNode2 = (tVlanRedMcastCacheNode *)
        pRBElem2;

    if (pVlanRedMcastCacheNode1->u4ContextId >
        pVlanRedMcastCacheNode2->u4ContextId)
    {
        return 1;
    }

    if (pVlanRedMcastCacheNode1->u4ContextId <
        pVlanRedMcastCacheNode2->u4ContextId)
    {
        return -1;
    }

    if (pVlanRedMcastCacheNode1->VlanId > pVlanRedMcastCacheNode2->VlanId)
    {
        return 1;
    }

    if (pVlanRedMcastCacheNode1->VlanId < pVlanRedMcastCacheNode2->VlanId)
    {
        return -1;
    }

    if (pVlanRedMcastCacheNode1->u4RcvPort > pVlanRedMcastCacheNode2->u4RcvPort)
    {
        return 1;
    }
    if (pVlanRedMcastCacheNode1->u4RcvPort < pVlanRedMcastCacheNode2->u4RcvPort)
    {
        return -1;
    }
    if (MEMCMP (pVlanRedMcastCacheNode1->MacAddr,
                pVlanRedMcastCacheNode2->MacAddr, sizeof (tMacAddr)) > 0)
    {
        return 1;
    }
    if (MEMCMP (pVlanRedMcastCacheNode1->MacAddr,
                pVlanRedMcastCacheNode2->MacAddr, sizeof (tMacAddr)) < 0)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/*  Function Name   : VlanRedStUcastCacheRBTreeCmp                           */
/*  Description     : This function is invoked by the RB Tree library during */
/*                    RB Tree traversal.                                     */
/*  Input(s)        : pRBElem1 - Pointer to the first Entry                  */
/*                    pRBElem2 - Pointer to the second Entry                 */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  1, if context1 is greater than context2   */
/*                                 -1, if context2 is greater than context1  */
/*                                 1, if vlan1 is greater than vlan2         */
/*                                 -1 if vlan2 is greater than vlan1         */
/*                                 0 if(context1==context2) && (vlan1==vlan2)*/
/*****************************************************************************/
INT4
VlanRedStUcastCacheRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVlanRedStUcastCacheNode *pVlanRedStUcastCacheNode1 =
        (tVlanRedStUcastCacheNode *) pRBElem1;
    tVlanRedStUcastCacheNode *pVlanRedStUcastCacheNode2 =
        (tVlanRedStUcastCacheNode *) pRBElem2;

    if (pVlanRedStUcastCacheNode1->u4ContextId >
        pVlanRedStUcastCacheNode2->u4ContextId)
    {
        return 1;
    }

    if (pVlanRedStUcastCacheNode1->u4ContextId <
        pVlanRedStUcastCacheNode2->u4ContextId)
    {
        return -1;
    }

    if (pVlanRedStUcastCacheNode1->u4FdbId > pVlanRedStUcastCacheNode2->u4FdbId)
    {
        return 1;
    }

    if (pVlanRedStUcastCacheNode1->u4FdbId < pVlanRedStUcastCacheNode2->u4FdbId)
    {
        return -1;
    }

    if (pVlanRedStUcastCacheNode1->u4RcvPort >
        pVlanRedStUcastCacheNode2->u4RcvPort)
    {
        return 1;
    }
    if (pVlanRedStUcastCacheNode1->u4RcvPort <
        pVlanRedStUcastCacheNode2->u4RcvPort)
    {
        return -1;
    }
    if (MEMCMP (pVlanRedStUcastCacheNode1->MacAddr,
                pVlanRedStUcastCacheNode2->MacAddr, sizeof (tMacAddr)) > 0)
    {
        return 1;
    }
    if (MEMCMP (pVlanRedStUcastCacheNode1->MacAddr,
                pVlanRedStUcastCacheNode2->MacAddr, sizeof (tMacAddr)) < 0)
    {
        return -1;
    }
    return 0;
}
#endif /* NPAPI_WANTED */
#endif /* L2RED_WANTED */
