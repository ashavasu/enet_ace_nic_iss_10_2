/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbrwr.c,v 1.11 2011/03/21 12:26:45 siva Exp $
*
* Description: Protocol wrapper file 
*********************************************************************/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsmsbrlw.h"
# include  "fsmsbrwr.h"
# include  "fsmsbrdb.h"

INT4
GetNextIndexFsDot1dBaseTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dBaseTable (&i4FsDot1dContextId) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dBaseTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dContextId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dBaseBridgeAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    i4RetVal =
        nmhGetFsDot1dBaseBridgeAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                        (tMacAddr *) pMultiData->
                                        pOctetStrValue->pu1_OctetList);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dBaseNumPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dBaseNumPorts (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dBaseTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dBaseType (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dBasePortTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dBasePortTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dBasePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dBasePortIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dBasePortIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dBasePortCircuitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dBasePortCircuit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOidValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dBasePortDelayExceededDiscardsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dBasePortDelayExceededDiscards (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    &(pMultiData->
                                                      u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dBasePortMtuExceededDiscardsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dBasePortMtuExceededDiscards (pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dStpTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
#ifdef RSTP_WANTED
    INT4                i4FsDot1dContextId;

    AST_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dStpTable (&i4FsDot1dContextId) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dStpTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dContextId) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dContextId;
    AST_UNLOCK ();
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFirstMultiIndex);
    UNUSED_PARAM (pNextMultiIndex);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpProtocolSpecificationGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpProtocolSpecification (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else

    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpTimeSinceTopologyChangeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpTimeSinceTopologyChange (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 &(pMultiData->u4_ULongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpTopChangesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpTopChanges (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpDesignatedRootGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpDesignatedRoot (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
#else

    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpRootCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpRootCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpRootPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpRootPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpMaxAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpHelloTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpHelloTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpHoldTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpForwardDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpForwardDelay (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpBridgeMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpBridgeMaxAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpBridgeHelloTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpBridgeHelloTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpBridgeForwardDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpBridgeForwardDelay (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhSetFsDot1dStpPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpBridgeMaxAgeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal =
        nmhSetFsDot1dStpBridgeMaxAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpBridgeHelloTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal =
        nmhSetFsDot1dStpBridgeHelloTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpBridgeForwardDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal =
        nmhSetFsDot1dStpBridgeForwardDelay (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStpPriority (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpBridgeMaxAgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStpBridgeMaxAge (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpBridgeHelloTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStpBridgeHelloTime (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpBridgeForwardDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStpBridgeForwardDelay (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhDepv2FsDot1dStpTable (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);

    return SNMP_FAILURE;
#endif
}

INT4
GetNextIndexFsDot1dStpPortTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
#ifdef RSTP_WANTED
    INT4                i4FsDot1dBasePort;

    AST_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dStpPortTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dStpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    AST_UNLOCK ();
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFirstMultiIndex);
    UNUSED_PARAM (pNextMultiIndex);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpPortPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpPortState (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStpPortEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortPathCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortPathCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortDesignatedRootGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortDesignatedRoot (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortDesignatedCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortDesignatedCost (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortDesignatedBridgeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortDesignatedBridge (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortDesignatedPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortDesignatedPort (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortForwardTransitionsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortForwardTransitions (pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                &(pMultiData->u4_ULongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif
}

INT4
FsDot1dStpPortPathCost32Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();
    if (nmhValidateIndexInstanceFsDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStpPortPathCost32 (pMultiIndex->pIndex[0].
                                        i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));
    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_SUCCESS;
#endif

}

INT4
FsDot1dStpPortPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal =
        nmhSetFsDot1dStpPortPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpPortEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhSetFsDot1dStpPortEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpPortPathCostSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal =
        (nmhSetFsDot1dStpPortPathCost
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif

}

INT4
FsDot1dStpPortPathCost32Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal =
        (nmhSetFsDot1dStpPortPathCost32
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif

}

INT4
FsDot1dStpPortPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStpPortPriority (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpPortEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStpPortEnable (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif
}

INT4
FsDot1dStpPortPathCostTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = (nmhTestv2FsDot1dStpPortPathCost (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));
    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif

}

INT4
FsDot1dStpPortPathCost32Test (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = (nmhTestv2FsDot1dStpPortPathCost32 (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));
    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);

    return SNMP_FAILURE;
#endif

}

INT4
FsDot1dStpPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
#ifdef RSTP_WANTED
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhDepv2FsDot1dStpPortTable (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds);

    AST_UNLOCK ();
    return i4RetVal;
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);

    return SNMP_FAILURE;
#endif
}

INT4
GetNextIndexFsDot1dTpTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dTpTable (&i4FsDot1dContextId) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dTpTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dContextId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dTpLearnedEntryDiscardsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpLearnedEntryDiscards (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpAgingTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dTpAgingTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpAgingTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetFsDot1dTpAgingTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpAgingTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dTpAgingTime (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dTpTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dContextId;
    tMacAddr           *pFsDot1dTpFdbAddress;

    VLAN_LOCK ();

    pFsDot1dTpFdbAddress =
        (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList;

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dTpFdbTable (&i4FsDot1dContextId,
                                               pFsDot1dTpFdbAddress) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dTpFdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1dContextId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, pFsDot1dTpFdbAddress) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dContextId;
    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dTpFdbPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dTpFdbStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dTpPortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dTpPortTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dTpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dTpPortMaxInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dTpPortMaxInfo (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpPortOutFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpPortInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dStaticTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dContextId;
    tMacAddr           *pFsDot1dStaticAddress;
    INT4                i4FsDot1dReceivePort;

    VLAN_LOCK ();

    pFsDot1dStaticAddress =
        (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList;

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dStaticTable (&i4FsDot1dContextId,
                                                pFsDot1dStaticAddress,
                                                &i4FsDot1dReceivePort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dStaticTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1dContextId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, pFsDot1dStaticAddress,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1dReceivePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dContextId;
    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1dReceivePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dStaticRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStaticTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStaticRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStaticTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dStaticStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1dStaticRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetFsDot1dStaticStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStaticRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                (*(tMacAddr *) pMultiIndex->
                                                 pIndex[1].pOctetStrValue->
                                                 pu1_OctetList),
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStaticStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             (*(tMacAddr *) pMultiIndex->
                                              pIndex[1].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dStaticTable (pu4Error, pSnmpIndexList,
                                           pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dStaticAllowedToGoTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dContextId;
    INT4                i4FsDot1dReceivePort;
    INT4                i4FsDot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dStaticAllowedToGoTable (&i4FsDot1dContextId,
                                                           (tMacAddr *)
                                                           pNextMultiIndex->
                                                           pIndex[1].
                                                           pOctetStrValue->
                                                           pu1_OctetList,
                                                           &i4FsDot1dReceivePort,
                                                           &i4FsDot1dBasePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dStaticAllowedToGoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1dContextId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1dReceivePort, pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dContextId;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1dReceivePort;
    pNextMultiIndex->pIndex[3].i4_SLongValue = i4FsDot1dBasePort;
    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dStaticAllowedIsMemberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dStaticAllowedToGoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dStaticAllowedIsMember (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[1].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticAllowedIsMemberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1dStaticAllowedIsMember (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[1].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticAllowedIsMemberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dStaticAllowedIsMember (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dStaticAllowedToGoTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dStaticAllowedToGoTable (pu4Error,
                                                      pSnmpIndexList,
                                                      pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

VOID
RegisterFSMSBR ()
{
    SNMPRegisterMib (&FsDot1dBaseTableOID, &FsDot1dBaseTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dBasePortTableOID, &FsDot1dBasePortTableEntry,
                     SNMP_MSR_TGR_FALSE);
#ifdef RSTP_WANTED
    SNMPRegisterMib (&FsDot1dStpTableOID, &FsDot1dStpTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dStpPortTableOID, &FsDot1dStpPortTableEntry,
                     SNMP_MSR_TGR_FALSE);
#endif
    SNMPRegisterMib (&FsDot1dTpTableOID, &FsDot1dTpTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsDot1dTpFdbTableOID, &FsDot1dTpFdbTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dTpPortTableOID, &FsDot1dTpPortTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dStaticTableOID, &FsDot1dStaticTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsDot1dStaticAllowedToGoTableOID,
                     &FsDot1dStaticAllowedToGoTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmsbrOID, (const UINT1 *) "fsmsbrg");
}

VOID
UnRegisterFSMSBR ()
{
    SNMPUnRegisterMib (&FsDot1dBaseTableOID, &FsDot1dBaseTableEntry);
    SNMPUnRegisterMib (&FsDot1dBasePortTableOID, &FsDot1dBasePortTableEntry);
    SNMPUnRegisterMib (&FsDot1dStpTableOID, &FsDot1dStpTableEntry);
    SNMPUnRegisterMib (&FsDot1dStpPortTableOID, &FsDot1dStpPortTableEntry);
    SNMPUnRegisterMib (&FsDot1dTpTableOID, &FsDot1dTpTableEntry);
    SNMPUnRegisterMib (&FsDot1dTpFdbTableOID, &FsDot1dTpFdbTableEntry);
    SNMPUnRegisterMib (&FsDot1dTpPortTableOID, &FsDot1dTpPortTableEntry);
    SNMPUnRegisterMib (&FsDot1dStaticTableOID, &FsDot1dStaticTableEntry);
    SNMPUnRegisterMib (&FsDot1dStaticAllowedToGoTableOID,
                       &FsDot1dStaticAllowedToGoTableEntry);
    SNMPDelSysorEntry (&fsmsbrOID, (const UINT1 *) "fsmsbrg");
}
