/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1evlw.c,v 1.9 2016/07/22 06:46:31 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "vlaninc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysType
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbSysType (INT4 *pi4RetValIeee8021BridgeEvbSysType)
{
    *pi4RetValIeee8021BridgeEvbSysType = VLAN_EVB_ROLE_BRIDGE;
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysNumExternalPorts
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysNumExternalPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysNumExternalPorts
    (UINT4 *pu4RetValIeee8021BridgeEvbSysNumExternalPorts)
{
    *pu4RetValIeee8021BridgeEvbSysNumExternalPorts = 
        gEvbGlobalInfo.u4EvbSysNumExternalPorts;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysEvbLldpTxEnable
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysEvbLldpTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysEvbLldpTxEnable
    (INT4 *pi4RetValIeee8021BridgeEvbSysEvbLldpTxEnable)
{
/* Not supported */
    *pi4RetValIeee8021BridgeEvbSysEvbLldpTxEnable = 0;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysEvbLldpManual
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysEvbLldpManual
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysEvbLldpManual
    (INT4 *pi4RetValIeee8021BridgeEvbSysEvbLldpManual)
{
/* Not supported */
    *pi4RetValIeee8021BridgeEvbSysEvbLldpManual = 0;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysEvbLldpGidCapable
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysEvbLldpGidCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysEvbLldpGidCapable
    (INT4 *pi4RetValIeee8021BridgeEvbSysEvbLldpGidCapable)
{
/* Not supported */
    *pi4RetValIeee8021BridgeEvbSysEvbLldpGidCapable = 0;;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysEcpAckTimer
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysEcpAckTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysEcpAckTimer
    (INT4 *pi4RetValIeee8021BridgeEvbSysEcpAckTimer)
{
/* Not supported */
    (*pi4RetValIeee8021BridgeEvbSysEcpAckTimer) = 0;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysEcpMaxRetries
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysEcpMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysEcpMaxRetries
    (INT4 *pi4RetValIeee8021BridgeEvbSysEcpMaxRetries)
{
/* Not supported */
    (*pi4RetValIeee8021BridgeEvbSysEcpMaxRetries) = 0;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
    (INT4 *pi4RetValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay)
{
/* Not supported */
    (*pi4RetValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay) = 0;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Input       :  The Indices

                The Object 
                retValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
    (INT4 *pi4RetValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive)
{
/* Not supported */
    (*pi4RetValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive) = 0;
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysEvbLldpTxEnable
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysEvbLldpTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysEvbLldpTxEnable
    (INT4 i4SetValIeee8021BridgeEvbSysEvbLldpTxEnable)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysEvbLldpTxEnable);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysEvbLldpManual
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysEvbLldpManual
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysEvbLldpManual
    (INT4 i4SetValIeee8021BridgeEvbSysEvbLldpManual)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysEvbLldpManual);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysEvbLldpGidCapable
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysEvbLldpGidCapable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysEvbLldpGidCapable
    (INT4 i4SetValIeee8021BridgeEvbSysEvbLldpGidCapable)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysEvbLldpGidCapable);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysEcpAckTimer
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysEcpAckTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysEcpAckTimer
    (INT4 i4SetValIeee8021BridgeEvbSysEcpAckTimer)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysEcpAckTimer);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysEcpMaxRetries
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysEcpMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysEcpMaxRetries
    (INT4 i4SetValIeee8021BridgeEvbSysEcpMaxRetries)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysEcpMaxRetries);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
    (INT4 i4SetValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Input       :  The Indices

                The Object 
                setValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
    (INT4 i4SetValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive)
{
/* Not supported */
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive);
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysEvbLldpTxEnable
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysEvbLldpTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysEvbLldpTxEnable (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4TestValIeee8021BridgeEvbSysEvbLldpTxEnable)
{
/* Not supported */
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysEvbLldpTxEnable);
    UNUSED_PARAM(pu4ErrorCode);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysEvbLldpManual
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysEvbLldpManual
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysEvbLldpManual (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValIeee8021BridgeEvbSysEvbLldpManual)
{
/* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysEvbLldpManual);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysEvbLldpGidCapable
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysEvbLldpGidCapable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysEvbLldpGidCapable (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4TestValIeee8021BridgeEvbSysEvbLldpGidCapable)
{
/* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysEvbLldpGidCapable);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysEcpAckTimer
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysEcpAckTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysEcpAckTimer (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValIeee8021BridgeEvbSysEcpAckTimer)
{
/* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysEcpAckTimer);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysEcpMaxRetries
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysEcpMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysEcpMaxRetries (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValIeee8021BridgeEvbSysEcpMaxRetries)
{
/* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysEcpMaxRetries);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4TestValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay)
{
/* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Input       :  The Indices

                The Object 
                testValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4TestValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive)
{
/* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive);
    return SNMP_FAILURE;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysEvbLldpTxEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysEvbLldpTxEnable (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysEvbLldpManual
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysEvbLldpManual (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysEvbLldpGidCapable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysEvbLldpGidCapable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysEcpAckTimer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysEcpAckTimer (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysEcpMaxRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysEcpMaxRetries (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive (UINT4 *pu4ErrorCode,
                                                    tSnmpIndexList *
                                                    pSnmpIndexList,
                                                    tSNMP_VAR_BIND *
                                                    pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbSbpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID , 
     UINT4 u4Ieee8021BridgeEvbSbpPortNumber)
{
/* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbSbpTable
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexIeee8021BridgeEvbSbpTable
    (UINT4 *pu4Ieee8021BridgeEvbSbpComponentID , 
    UINT4 *pu4Ieee8021BridgeEvbSbpPortNumber)
{
    /* Not supported*/
    UNUSED_PARAM(pu4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbSbpPortNumber);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbSbpTable
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                nextIeee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber
                nextIeee8021BridgeEvbSbpPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexIeee8021BridgeEvbSbpTable
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID ,
    UINT4 *pu4NextIeee8021BridgeEvbSbpComponentID  , 
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber ,
    UINT4 *pu4NextIeee8021BridgeEvbSbpPortNumber )
{
    /* Not supported*/
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSbpLldpManual
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber

                The Object 
                retValIeee8021BridgeEvbSbpLldpManual
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSbpLldpManual
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID , 
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber , 
    INT4 *pi4RetValIeee8021BridgeEvbSbpLldpManual)
{
    /* Not supported*/
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbSbpLldpManual);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber

                The Object 
                retValIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID , 
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber , 
    UINT4 *pu4RetValIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay)
{
    /* Not supported*/
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSbpVdpOperReinitKeepAlive
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber

                The Object 
                retValIeee8021BridgeEvbSbpVdpOperReinitKeepAlive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSbpVdpOperReinitKeepAlive
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID , 
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber , 
    UINT4 *pu4RetValIeee8021BridgeEvbSbpVdpOperReinitKeepAlive)
{
    /* Not supported*/
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbSbpVdpOperReinitKeepAlive);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSbpVdpOperToutKeepAlive
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber

                The Object 
                retValIeee8021BridgeEvbSbpVdpOperToutKeepAlive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbSbpVdpOperToutKeepAlive
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID , 
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber , 
    UINT4 *pu4RetValIeee8021BridgeEvbSbpVdpOperToutKeepAlive)
{
    /* Not supported*/
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbSbpVdpOperToutKeepAlive);
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbSbpLldpManual
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber

                The Object 
                setValIeee8021BridgeEvbSbpLldpManual
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIeee8021BridgeEvbSbpLldpManual
    (UINT4 u4Ieee8021BridgeEvbSbpComponentID , 
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber , 
    INT4 i4SetValIeee8021BridgeEvbSbpLldpManual)
{
    /* Not supported*/
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbSbpLldpManual);
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbSbpLldpManual
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber

                The Object 
                testValIeee8021BridgeEvbSbpLldpManual
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbSbpLldpManual (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021BridgeEvbSbpComponentID,
    UINT4 u4Ieee8021BridgeEvbSbpPortNumber , 
                                         INT4
                                         i4TestValIeee8021BridgeEvbSbpLldpManual)
{
    /* Not supported*/
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbSbpPortNumber);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbSbpLldpManual);
    return SNMP_FAILURE;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbSbpTable
 Input       :  The Indices
                Ieee8021BridgeEvbSbpComponentID
                Ieee8021BridgeEvbSbpPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbSbpTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbVSIDBTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable
    (UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbVSIDBTable
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexIeee8021BridgeEvbVSIDBTable
    (UINT4 *pu4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 *pu4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 *pi4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE * pIeee8021BridgeEvbVSIID)
{
    UNUSED_PARAM(pu4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(pi4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbVSIDBTable
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                nextIeee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                nextIeee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                nextIeee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID
                nextIeee8021BridgeEvbVSIID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexIeee8021BridgeEvbVSIDBTable(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID ,
    UINT4 *pu4NextIeee8021BridgeEvbVSIComponentID  , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber ,
    UINT4 *pu4NextIeee8021BridgeEvbVSIPortNumber  , 
    INT4 i4Ieee8021BridgeEvbVSIIDType ,
    INT4 *pi4NextIeee8021BridgeEvbVSIIDType  , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID ,
    tSNMP_OCTET_STRING_TYPE * pNextIeee8021BridgeEvbVSIID )
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pi4NextIeee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pNextIeee8021BridgeEvbVSIID);
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVSITimeSinceCreate
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVSITimeSinceCreate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbVSITimeSinceCreate(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    UINT4 *pu4RetValIeee8021BridgeEvbVSITimeSinceCreate)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbVSITimeSinceCreate);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVsiVdpOperCmd
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVsiVdpOperCmd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbVsiVdpOperCmd(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    INT4 *pi4RetValIeee8021BridgeEvbVsiVdpOperCmd)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVsiVdpOperCmd);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVsiOperRevert
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVsiOperRevert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbVsiOperRevert(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    INT4 *pi4RetValIeee8021BridgeEvbVsiOperRevert)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVsiOperRevert);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVsiOperHard
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVsiOperHard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbVsiOperHard(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    INT4 *pi4RetValIeee8021BridgeEvbVsiOperHard)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVsiOperHard);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVsiOperReason
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVsiOperReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbVsiOperReason(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    tSNMP_OCTET_STRING_TYPE * pRetValIeee8021BridgeEvbVsiOperReason)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pRetValIeee8021BridgeEvbVsiOperReason);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVSIMgrID
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVSIMgrID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetIeee8021BridgeEvbVSIMgrID
    (UINT4 u4Ieee8021BridgeEvbVSIComponentID,
     UINT4 u4Ieee8021BridgeEvbVSIPortNumber,
     INT4 i4Ieee8021BridgeEvbVSIIDType,
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    tSNMP_OCTET_STRING_TYPE * pRetValIeee8021BridgeEvbVSIMgrID)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pRetValIeee8021BridgeEvbVSIMgrID);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVSIType
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVSIType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIeee8021BridgeEvbVSIType(
    UINT4 u4Ieee8021BridgeEvbVSIComponentID , 
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
    tSNMP_OCTET_STRING_TYPE *pIeee8021BridgeEvbVSIID , 
    INT4 *pi4RetValIeee8021BridgeEvbVSIType)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVSIType);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVSITypeVersion
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVSITypeVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVSITypeVersion (UINT4 u4Ieee8021BridgeEvbVSIComponentID,
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIeee8021BridgeEvbVSIID,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValIeee8021BridgeEvbVSITypeVersion)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pRetValIeee8021BridgeEvbVSITypeVersion);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVSIMvFormat
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVSIMvFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVSIMvFormat (UINT4 u4Ieee8021BridgeEvbVSIComponentID,
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                    tSNMP_OCTET_STRING_TYPE *
                                    pIeee8021BridgeEvbVSIID,
    INT4 *pi4RetValIeee8021BridgeEvbVSIMvFormat)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVSIMvFormat);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVSINumMACs
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVSINumMACs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVSINumMACs (UINT4 u4Ieee8021BridgeEvbVSIComponentID,
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                   tSNMP_OCTET_STRING_TYPE *
                                   pIeee8021BridgeEvbVSIID,
    INT4 *pi4RetValIeee8021BridgeEvbVSINumMACs)
{

    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVSINumMACs);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVDPMachineState
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVDPMachineState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVDPMachineState (UINT4 u4Ieee8021BridgeEvbVSIComponentID,
    UINT4 u4Ieee8021BridgeEvbVSIPortNumber , 
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIeee8021BridgeEvbVSIID,
                                        INT4
                                        *pi4RetValIeee8021BridgeEvbVDPMachineState)
{
/* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbVDPMachineState);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVDPCommandsSucceeded
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVDPCommandsSucceeded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVDPCommandsSucceeded (UINT4
                                             u4Ieee8021BridgeEvbVSIComponentID,
                                             UINT4
                                             u4Ieee8021BridgeEvbVSIPortNumber,
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021BridgeEvbVSIID,
                                             UINT4
                                             *pu4RetValIeee8021BridgeEvbVDPCommandsSucceeded)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbVDPCommandsSucceeded);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVDPCommandsFailed
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVDPCommandsFailed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVDPCommandsFailed (UINT4
                                          u4Ieee8021BridgeEvbVSIComponentID,
                                          UINT4
                                          u4Ieee8021BridgeEvbVSIPortNumber,
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                          tSNMP_OCTET_STRING_TYPE *
                                          pIeee8021BridgeEvbVSIID,
                                          UINT4
                                          *pu4RetValIeee8021BridgeEvbVDPCommandsFailed)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbVDPCommandsFailed);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVDPCommandReverts
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVDPCommandReverts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVDPCommandReverts (UINT4
                                          u4Ieee8021BridgeEvbVSIComponentID,
                                          UINT4
                                          u4Ieee8021BridgeEvbVSIPortNumber,
    INT4 i4Ieee8021BridgeEvbVSIIDType , 
                                          tSNMP_OCTET_STRING_TYPE *
                                          pIeee8021BridgeEvbVSIID,
                                          UINT4
                                          *pu4RetValIeee8021BridgeEvbVDPCommandReverts)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbVDPCommandReverts);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbVDPCounterDiscontinuity
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID

                The Object 
                retValIeee8021BridgeEvbVDPCounterDiscontinuity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbVDPCounterDiscontinuity (UINT4
                                                u4Ieee8021BridgeEvbVSIComponentID,
                                                UINT4
                                                u4Ieee8021BridgeEvbVSIPortNumber,
                                                INT4
                                                i4Ieee8021BridgeEvbVSIIDType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pIeee8021BridgeEvbVSIID,
                                                UINT4
                                                *pu4RetValIeee8021BridgeEvbVDPCounterDiscontinuity)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbVDPCounterDiscontinuity);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbVSIDBMacTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBMacTable
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID
                Ieee8021BridgeEvbGroupID
                Ieee8021BridgeEvbVSIMac
                Ieee8021BridgeEvbVSIVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBMacTable (UINT4
                                                        u4Ieee8021BridgeEvbVSIComponentID,
                                                        UINT4
                                                        u4Ieee8021BridgeEvbVSIPortNumber,
                                                        INT4
                                                        i4Ieee8021BridgeEvbVSIIDType,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pIeee8021BridgeEvbVSIID,
                                                        UINT4
                                                        u4Ieee8021BridgeEvbGroupID,
                                                        tMacAddr
                                                        Ieee8021BridgeEvbVSIMac,
                                                        UINT4
                                                        u4Ieee8021BridgeEvbVSIVlanId)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbGroupID);
    UNUSED_PARAM(Ieee8021BridgeEvbVSIMac);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIVlanId);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbVSIDBMacTable
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID
                Ieee8021BridgeEvbGroupID
                Ieee8021BridgeEvbVSIMac
                Ieee8021BridgeEvbVSIVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeEvbVSIDBMacTable (UINT4
                                                *pu4Ieee8021BridgeEvbVSIComponentID,
                                                UINT4
                                                *pu4Ieee8021BridgeEvbVSIPortNumber,
                                                INT4
                                                *pi4Ieee8021BridgeEvbVSIIDType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pIeee8021BridgeEvbVSIID,
                                                UINT4
                                                *pu4Ieee8021BridgeEvbGroupID,
                                                tMacAddr *
                                                pIeee8021BridgeEvbVSIMac,
                                                UINT4
                                                *pu4Ieee8021BridgeEvbVSIVlanId)
{
    UNUSED_PARAM(pu4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(pi4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbGroupID);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIMac);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbVSIVlanId);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbVSIDBMacTable
 Input       :  The Indices
                Ieee8021BridgeEvbVSIComponentID
                nextIeee8021BridgeEvbVSIComponentID
                Ieee8021BridgeEvbVSIPortNumber
                nextIeee8021BridgeEvbVSIPortNumber
                Ieee8021BridgeEvbVSIIDType
                nextIeee8021BridgeEvbVSIIDType
                Ieee8021BridgeEvbVSIID
                nextIeee8021BridgeEvbVSIID
                Ieee8021BridgeEvbGroupID
                nextIeee8021BridgeEvbGroupID
                Ieee8021BridgeEvbVSIMac
                nextIeee8021BridgeEvbVSIMac
                Ieee8021BridgeEvbVSIVlanId
                nextIeee8021BridgeEvbVSIVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeEvbVSIDBMacTable (UINT4
                                               u4Ieee8021BridgeEvbVSIComponentID,
                                               UINT4
                                               *pu4NextIeee8021BridgeEvbVSIComponentID,
                                               UINT4
                                               u4Ieee8021BridgeEvbVSIPortNumber,
                                               UINT4
                                               *pu4NextIeee8021BridgeEvbVSIPortNumber,
                                               INT4
                                               i4Ieee8021BridgeEvbVSIIDType,
                                               INT4
                                               *pi4NextIeee8021BridgeEvbVSIIDType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pIeee8021BridgeEvbVSIID,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextIeee8021BridgeEvbVSIID,
    UINT4 u4Ieee8021BridgeEvbGroupID ,
                                               UINT4
                                               *pu4NextIeee8021BridgeEvbGroupID,
    tMacAddr Ieee8021BridgeEvbVSIMac ,
                                               tMacAddr *
                                               pNextIeee8021BridgeEvbVSIMac,
                                               UINT4
                                               u4Ieee8021BridgeEvbVSIVlanId,
                                               UINT4
                                               *pu4NextIeee8021BridgeEvbVSIVlanId)
{
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbVSIComponentID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbVSIPortNumber);
    UNUSED_PARAM(i4Ieee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pi4NextIeee8021BridgeEvbVSIIDType);
    UNUSED_PARAM(pIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(pNextIeee8021BridgeEvbVSIID);
    UNUSED_PARAM(u4Ieee8021BridgeEvbGroupID);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbGroupID);
    UNUSED_PARAM(Ieee8021BridgeEvbVSIMac);
    UNUSED_PARAM(pNextIeee8021BridgeEvbVSIMac);
    UNUSED_PARAM(u4Ieee8021BridgeEvbVSIVlanId);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbVSIVlanId);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbUAPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable (UINT4
                                                         u4Ieee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeEvbUAPConfigTable (UINT4
                                                 *pu4Ieee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    pEvbUapIfEntry = VlanEvbUapIfGetFirstEntry();
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    *pu4Ieee8021BridgePhyPort = pEvbUapIfEntry->u4UapIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                nextIeee8021BridgePhyPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeEvbUAPConfigTable (UINT4 u4Ieee8021BridgePhyPort,
                                                UINT4
                                                *pu4NextIeee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfNxtEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfNxtEntry = VlanEvbUapIfGetNextEntry(u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfNxtEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetNextIndexIeee8021BridgeEvbUAPConfigTable: "
                        "UAP Entry %d not present \n",
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4NextIeee8021BridgePhyPort = pEvbUapIfNxtEntry->u4UapIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS; 
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPComponentId
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPComponentId (UINT4 u4Ieee8021BridgePhyPort,
                                       UINT4
                                       *pu4RetValIeee8021BridgeEvbUAPComponentId)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPComponentId: "
                       "UAP Entry %d not present \n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValIeee8021BridgeEvbUAPComponentId = 
        pEvbUapIfEntry->u4UapIfCompId;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPPort
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPPort (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValIeee8021BridgeEvbUAPPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
       VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetIeee8021BridgeEvbUAPPort: "
                      "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValIeee8021BridgeEvbUAPPort = pEvbUapIfEntry->u4UapIfPort;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUapConfigIfIndex
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUapConfigIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUapConfigIfIndex (UINT4 u4Ieee8021BridgePhyPort,
                                         INT4
                                         *pi4RetValIeee8021BridgeEvbUapConfigIfIndex)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting UAP entry for the given port */

    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetIeee8021BridgeEvbUapConfigIfIndex: "
                      "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUapConfigIfIndex = 
       (INT4) pEvbUapIfEntry->u4UapIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPSchCdcpAdminEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable (UINT4 u4Ieee8021BridgePhyPort,
                                              INT4
                                              *pi4RetValIeee8021BridgeEvbUAPSchCdcpAdminEnable)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable :"
                      "UAP Entry %d not present \n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUAPSchCdcpAdminEnable = 
        pEvbUapIfEntry->i4UapIfSchCdcpAdminEnable;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchAdminCDCPRole
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPSchAdminCDCPRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPRole (UINT4 u4Ieee8021BridgePhyPort,
                                            INT4
                                            *pi4RetValIeee8021BridgeEvbUAPSchAdminCDCPRole)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPSchAdminCDCPRole: "
                       "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    /* CDCP admin Role is always BRIDGE. 
       We don't have datastructure for the same.
       Hence assigning the value as  VLAN_EVB_ROLE_BRIDGE. */

    *pi4RetValIeee8021BridgeEvbUAPSchAdminCDCPRole = 
        VLAN_EVB_ROLE_BRIDGE;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
    
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPSchAdminCDCPChanCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap (UINT4 u4Ieee8021BridgePhyPort,
                                               INT4
                                               *pi4RetValIeee8021BridgeEvbUAPSchAdminCDCPChanCap)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap :"
                       "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUAPSchAdminCDCPChanCap = 
        pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchOperCDCPChanCap
 Input       :  The Indices
                Ieee8021BridgePhyPort


                The Object 
                retValIeee8021BridgeEvbUAPSchOperCDCPChanCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchOperCDCPChanCap (UINT4 u4Ieee8021BridgePhyPort,
                                              INT4
                                              *pi4RetValIeee8021BridgeEvbUAPSchOperCDCPChanCap)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetIeee8021BridgeEvbUAPSchOperCDCPChanCap :"
                      "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUAPSchOperCDCPChanCap = 
        pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow (UINT4
                                                   u4Ieee8021BridgePhyPort,
                                                   UINT4
                                                   *pu4RetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow: "
                       "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow = 
        pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh (UINT4
                                                    u4Ieee8021BridgePhyPort,
                                                    UINT4
                                                    *pu4RetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh: "
                      "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh = 
        pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPSchOperState
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPSchOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPSchOperState (UINT4 u4Ieee8021BridgePhyPort,
                                        INT4
                                        *pi4RetValIeee8021BridgeEvbUAPSchOperState)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPSchOperState :"
                       "UAP Entry %d not present \n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUAPSchOperState =
         pEvbUapIfEntry->i4UapIfCdcpOperState;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSchCdcpRemoteEnabled
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbSchCdcpRemoteEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbSchCdcpRemoteEnabled (UINT4 u4Ieee8021BridgePhyPort,
                                             INT4
                                             *pi4RetValIeee8021BridgeEvbSchCdcpRemoteEnabled)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetIeee8021BridgeEvbSchCdcpRemoteEnabled: "
                      "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbSchCdcpRemoteEnabled = 
        pEvbUapIfEntry->i4UapIfSchCdcpRemoteEnabled;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbSchCdcpRemoteRole
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbSchCdcpRemoteRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbSchCdcpRemoteRole (UINT4 u4Ieee8021BridgePhyPort,
                                          INT4
                                          *pi4RetValIeee8021BridgeEvbSchCdcpRemoteRole)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbSchCdcpRemoteRole: "
                       "UAP Entry %d not present \n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbSchCdcpRemoteRole = 
          pEvbUapIfEntry->i4UapIfSchCdcpRemoteRole;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPConfigStorageType
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPConfigStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPConfigStorageType (UINT4 u4Ieee8021BridgePhyPort,
                                             INT4
                                             *pi4RetValIeee8021BridgeEvbUAPConfigStorageType)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPConfigStorageType: "
                       "UAP Entry %d not present \n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUAPConfigStorageType = 
            pEvbUapIfEntry->i4UapIfStorageType;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbUAPConfigRowStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValIeee8021BridgeEvbUAPConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbUAPConfigRowStatus (UINT4 u4Ieee8021BridgePhyPort,
                                           INT4
                                           *pi4RetValIeee8021BridgeEvbUAPConfigRowStatus)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetIeee8021BridgeEvbUAPConfigRowStatus: "
                       "UAP Entry %d not present \n",u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021BridgeEvbUAPConfigRowStatus =
        pEvbUapIfEntry->i4UapIfRowStatus;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPSchCdcpAdminEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable (UINT4 u4Ieee8021BridgePhyPort,
                                              INT4
                                              i4SetValIeee8021BridgeEvbUAPSchCdcpAdminEnable)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable: "
                       "S-channel entry not  present at UAP Index %d \r\n", 
                       u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValIeee8021BridgeEvbUAPSchCdcpAdminEnable ==
          pEvbUapIfEntry->i4UapIfSchCdcpAdminEnable)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    if (VLAN_SUCCESS != VlanEvbUapSetCdcpStatus(u4Ieee8021BridgePhyPort, 
                        i4SetValIeee8021BridgeEvbUAPSchCdcpAdminEnable))
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable: "
                  "Failed to set UAP status %d\r\n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPSchAdminCDCPRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole (UINT4 u4Ieee8021BridgePhyPort,
                                            INT4
                                            i4SetValIeee8021BridgeEvbUAPSchAdminCDCPRole)
{ 
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
       CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
       VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole: "
                      "S-channel entry not  present at UAP Index %d \r\n",
                      u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if(VLAN_EVB_ROLE_STATION == 
        i4SetValIeee8021BridgeEvbUAPSchAdminCDCPRole)
    {
        /*CLI_SET_ERR(CLI_VLAN_EVB_INCORRECT_VALUE);*/
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole:"
                        "InCorrect Value  %d for UAP %d\r\n",
                         i4SetValIeee8021BridgeEvbUAPSchAdminCDCPRole, 
                         u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPSchAdminCDCPChanCap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap (UINT4 u4Ieee8021BridgePhyPort,
                                               INT4
                                               i4SetValIeee8021BridgeEvbUAPSchAdminCDCPChanCap)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap: "
                        "S-channel entry not  present at UAP Index %d \r\n",
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap= 
        i4SetValIeee8021BridgeEvbUAPSchAdminCDCPChanCap;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow (UINT4
                                                   u4Ieee8021BridgePhyPort,
                                                   UINT4
                                                   u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow: "
                        "S-channel entry not  present at UAP Index %d \r\n",
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow =
        u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh (UINT4
                                                    u4Ieee8021BridgePhyPort,
                                                    UINT4
                                                    u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh:"
                        " S-channel entry not  present at UAP Index %d \r\n",
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh=
        u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPConfigStorageType
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPConfigStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPConfigStorageType (UINT4 u4Ieee8021BridgePhyPort,
                                             INT4
                                             i4SetValIeee8021BridgeEvbUAPConfigStorageType)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbUAPConfigStorageType: "
                        "S-channel entry not  present at UAP Index %d \r\n",
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry->i4UapIfStorageType=
        i4SetValIeee8021BridgeEvbUAPConfigStorageType;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbUAPConfigRowStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValIeee8021BridgeEvbUAPConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbUAPConfigRowStatus (UINT4 u4Ieee8021BridgePhyPort,
                                           INT4
                                           i4SetValIeee8021BridgeEvbUAPConfigRowStatus)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry) 
    {
        /* NOT SUPPORTED through this MIB, since the UAP entry 
         * will be created/deleted from CFA IF MIB. */
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbUAPConfigRowStatus: "
                        "UAP %d creation is not allowed by this MIB  \r\n",
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    else
    {
        if (i4SetValIeee8021BridgeEvbUAPConfigRowStatus == DESTROY)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                    "nmhSetIeee8021BridgeEvbUAPConfigRowStatus: "
                    "UAP %d deletion is not allowed by this MIB \r\n",
                    u4Ieee8021BridgePhyPort);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    switch(i4SetValIeee8021BridgeEvbUAPConfigRowStatus)
    {
        case NOT_IN_SERVICE:
        case NOT_READY:
            pEvbUapIfEntry->i4UapIfRowStatus = 
                    i4SetValIeee8021BridgeEvbUAPConfigRowStatus;
            break;

        case ACTIVE:
            pEvbUapIfEntry->i4UapIfRowStatus = ACTIVE;
            break;

        default:
            VlanReleaseContext ();
            return SNMP_FAILURE;

    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPSchCdcpAdminEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable (UINT4 *pu4ErrorCode,
                                                 UINT4 u4Ieee8021BridgePhyPort,
                                                 INT4
                                                 i4TestValIeee8021BridgeEvbUAPSchCdcpAdminEnable)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable"
                       "S-channel entry not  present at UAP Index %d \r\n",
                        u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* Check if existing mode is STATIC . If Static mode is enabled, then return
       FAIL */

    if ((pEvbUapIfEntry->i4UapIfSChMode != VLAN_EVB_UAP_SCH_MODE_DYNAMIC) &&
        (pEvbUapIfEntry->i4UapIfSChMode != VLAN_EVB_UAP_SCH_MODE_HYBRID))
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                     "nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable"
                     "Wrong S-ch Mode Value: %d for UAP %d\r\n", 
                     pEvbUapIfEntry->i4UapIfSChMode, u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* Testing if the given Admin enable is either VLAN_EVB_UAP_CDCP_DISABLE
        or VLAN_EVB_UAP_CDCP_ENABLE */
    if ((i4TestValIeee8021BridgeEvbUAPSchCdcpAdminEnable !=
        VLAN_EVB_UAP_CDCP_DISABLE) &&
        (i4TestValIeee8021BridgeEvbUAPSchCdcpAdminEnable !=
        VLAN_EVB_UAP_CDCP_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable"
                        "Wrong Admin Status: %d for UAP %d\r\n", 
                        i4TestValIeee8021BridgeEvbUAPSchCdcpAdminEnable, 
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPRole
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPSchAdminCDCPRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPRole (UINT4 *pu4ErrorCode,
                                               UINT4 u4Ieee8021BridgePhyPort,
                                               INT4
                                               i4TestValIeee8021BridgeEvbUAPSchAdminCDCPRole)
{
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
         return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if(VLAN_EVB_ROLE_BRIDGE != i4TestValIeee8021BridgeEvbUAPSchAdminCDCPRole)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPRole: "
                         "Inconsistant Value: %d for UAP %d\r\n",
                        i4TestValIeee8021BridgeEvbUAPSchAdminCDCPRole,
                        u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPSchAdminCDCPChanCap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap (UINT4 *pu4ErrorCode,
                                                  UINT4 u4Ieee8021BridgePhyPort,
                                                  INT4
                                                  i4TestValIeee8021BridgeEvbUAPSchAdminCDCPChanCap)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    tEvbSChIfEntry   *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
             return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
   
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    /* Checking if the UAP entry is valid */
    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap:"
                   "UAP Entry %d not present: \n",u4Ieee8021BridgePhyPort);
       *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgeEvbUAPSchAdminCDCPChanCap <
            VLAN_EVB_MIN_CDCP_ADMIN_CHAN_CAP) ||
           (i4TestValIeee8021BridgeEvbUAPSchAdminCDCPChanCap >
            VLAN_EVB_MAX_CDCP_ADMIN_CHAN_CAP))
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap:"
                       "Wrong Value: %d for UAP %d\n",
                       i4TestValIeee8021BridgeEvbUAPSchAdminCDCPChanCap,
                       u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021BridgeEvbUAPSchAdminCDCPChanCap > 
        VLAN_EVB_MAX_SBP_PER_UAP)
    {
        /* Max S-channel supported per UAP is 17 in ISS */
         VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                 "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap:"
                 "test value %d is greater than Max value per UAP for UAP %d\n",
                  i4TestValIeee8021BridgeEvbUAPSchAdminCDCPChanCap,
                  u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetNextEntry (u4Ieee8021BridgePhyPort,
                                                VLAN_EVB_DEF_SVID);
    if((NULL != pEvbSChIfEntry) && 
            (pEvbSChIfEntry->u4UapIfIndex == u4Ieee8021BridgePhyPort))
    {
        CLI_SET_ERR(CLI_VLAN_EVB_MAX_SCH_VALUE_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap:"
                  "s-channel Entry for UAP %d SVID %d cant be NULL \n",
                  u4Ieee8021BridgePhyPort, VLAN_EVB_DEF_SVID);
       *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow (UINT4 *pu4ErrorCode,
                                                      UINT4
                                                      u4Ieee8021BridgePhyPort,
                                                      UINT4
                                                      u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
         return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow"
                   "UAP Entry %d not present: \n", u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if((u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow <
        VLAN_EVB_DEF_CDCP_SVID_POOL_LOW) ||
        (u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow >
        VLAN_EVB_DEF_CDCP_SVID_POOL_HIGH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow"
                       "Wrong Value:%d \n",
                        u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow);

        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* Configuring Pool Low value should not exceed the already
     * present high value */
    if (u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow >=
         pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
         VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow"
                        "Test value %d > already present high value:%d \n",
                        u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow,
                        pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh (UINT4 *pu4ErrorCode,
                                                       UINT4
                                                       u4Ieee8021BridgePhyPort,
                                                       UINT4
                                                       u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh"
                        "S-channel entry not present at UAP Index %d \r\n", 
                         u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh >
         VLAN_EVB_DEF_CDCP_SVID_POOL_HIGH) ||
         (u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh <
         VLAN_EVB_DEF_CDCP_SVID_POOL_LOW))
    {
        
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh"
                  "UAP %d: test value %d in range\n", u4Ieee8021BridgePhyPort,
                   u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Configuring Pool High value should not minimal the already
     * present low value */
    if (u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh <=
         pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh"
                       "Configure high value %d should not minimal the already "
                       "present low value %d for UAP %d\n",
                       u4TestValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh,
                       pEvbUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow, 
                       u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPConfigStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType (UINT4 *pu4ErrorCode,
                                                UINT4 u4Ieee8021BridgePhyPort,
                                                INT4
                                                i4TestValIeee8021BridgeEvbUAPConfigStorageType)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
       
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
         return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType"
                  "UAP Entry %d not present: \n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgeEvbUAPConfigStorageType !=
         VLAN_EVB_STORAGE_TYPE_VOLATILE) &&
         (i4TestValIeee8021BridgeEvbUAPConfigStorageType !=
          VLAN_EVB_STORAGE_TYPE_NON_VOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType"
                        "Wrong Value:%d for UAP %d\n",
                       i4TestValIeee8021BridgeEvbUAPConfigStorageType,
                       u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbUAPConfigRowStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValIeee8021BridgeEvbUAPConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbUAPConfigRowStatus (UINT4 *pu4ErrorCode,
                                              UINT4 u4Ieee8021BridgePhyPort,
                                              INT4
                                              i4TestValIeee8021BridgeEvbUAPConfigRowStatus)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;


    if ((i4TestValIeee8021BridgeEvbUAPConfigRowStatus == CREATE_AND_WAIT) ||
        (i4TestValIeee8021BridgeEvbUAPConfigRowStatus == CREATE_AND_GO) ||
        (i4TestValIeee8021BridgeEvbUAPConfigRowStatus == DESTROY))
    {
        /* NOT SUPPORTED through this MIB, since the UAP entry 
         * will be created/deleted from CFA IF MIB. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
       return SNMP_FAILURE;
    }

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
       return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2Ieee8021BridgeEvbUAPConfigRowStatus"
                  "UAP Entry %d not present:\n", u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValIeee8021BridgeEvbUAPConfigRowStatus == NOT_IN_SERVICE) ||
            (i4TestValIeee8021BridgeEvbUAPConfigRowStatus == ACTIVE) ||
            (i4TestValIeee8021BridgeEvbUAPConfigRowStatus == NOT_READY))
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2Ieee8021BridgeEvbUAPConfigRowStatus:"
                       "Inconsistent Value : %d for UAP %d\r\n",
                       i4TestValIeee8021BridgeEvbUAPConfigRowStatus, 
                       u4Ieee8021BridgePhyPort);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbUAPConfigTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbCAPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable (UINT4
                                                         u4Ieee8021BridgePhyPort,
                                                         UINT4
                                                         u4Ieee8021BridgeEvbSchID)
{

    tEvbSChIfEntry *pEvbSChIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbSChIfEntry = VlanEvbSChIfGetEntry(u4Ieee8021BridgePhyPort, 
                                          u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChIfEntry)
    {
        VlanReleaseContext ();
	    return SNMP_FAILURE;
    }
    VlanReleaseContext ();
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeEvbCAPConfigTable (UINT4
                                                 *pu4Ieee8021BridgePhyPort,
                                                 UINT4
                                                 *pu4Ieee8021BridgeEvbSchID)
{
    tEvbSChIfEntry  *pEvbSChIfEntry  = NULL;
    
    /* Getting the First entry from CAP config table */

    pEvbSChIfEntry = (tEvbSChIfEntry *) (VOID *) RBTreeGetFirst
                     (gEvbGlobalInfo.EvbSchIfTree);

    if(NULL == pEvbSChIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    
    *pu4Ieee8021BridgePhyPort  = pEvbSChIfEntry->u4UapIfIndex;
    *pu4Ieee8021BridgeEvbSchID = pEvbSChIfEntry->u4SVId;
    
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                nextIeee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
                nextIeee8021BridgeEvbSchID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeEvbCAPConfigTable (UINT4 u4Ieee8021BridgePhyPort,
                                                UINT4
                                                *pu4NextIeee8021BridgePhyPort,
                                                UINT4 u4Ieee8021BridgeEvbSchID,
                                                UINT4
                                                *pu4NextIeee8021BridgeEvbSchID)
{
    tEvbSChIfEntry  *pEvbSChIfEntry  = NULL;
    tEvbSChIfEntry   SChIfNode;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
        
    SChIfNode.u4UapIfIndex  = u4Ieee8021BridgePhyPort;
    SChIfNode.u4SVId        = u4Ieee8021BridgeEvbSchID;
  
    /* Getting the next entry from CAP config table */

    pEvbSChIfEntry = (tEvbSChIfEntry *) (VOID *) RBTreeGetNext
          ((gEvbGlobalInfo.EvbSchIfTree), (tRBElem *) (VOID *)&SChIfNode, NULL);

    if(NULL == pEvbSChIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pu4NextIeee8021BridgePhyPort  = pEvbSChIfEntry->u4UapIfIndex;
    *pu4NextIeee8021BridgeEvbSchID = pEvbSChIfEntry->u4SVId;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCAPComponentId
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCAPComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCAPComponentId (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
                                       UINT4
                                       *pu4RetValIeee8021BridgeEvbCAPComponentId)
{
    tEvbSChIfEntry  *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
  

    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                           u4Ieee8021BridgeEvbSchID);

    if(NULL == pEvbSChIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* SBP present in the same context as that of UAP */
    *pu4RetValIeee8021BridgeEvbCAPComponentId = u4ContextId; 
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCapConfigIfIndex
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCapConfigIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCapConfigIfIndex (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
                                         INT4
                                         *pi4RetValIeee8021BridgeEvbCapConfigIfIndex)
{
    tEvbSChIfEntry  *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }


    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                           u4Ieee8021BridgeEvbSchID);

    if(NULL == pEvbSChIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgeEvbCapConfigIfIndex = pEvbSChIfEntry->i4SChIfIndex;
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCAPPort
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCAPPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCAPPort (UINT4 u4Ieee8021BridgePhyPort,
                                UINT4 u4Ieee8021BridgeEvbSchID,
                                UINT4 *pu4RetValIeee8021BridgeEvbCAPPort)
{
    tEvbSChIfEntry  *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }


    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                           u4Ieee8021BridgeEvbSchID);

    if(NULL == pEvbSChIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValIeee8021BridgeEvbCAPPort = pEvbSChIfEntry->u4SChIfPort;        
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCAPSChannelID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCAPSChannelID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCAPSChannelID (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
                                      UINT4
                                      *pu4RetValIeee8021BridgeEvbCAPSChannelID)
{
    tEvbSChIfEntry *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting S-channel  entry for the given Index */

    pEvbSChIfEntry = VlanEvbSChIfGetEntry(u4Ieee8021BridgePhyPort,
            u4Ieee8021BridgeEvbSchID);

    if(pEvbSChIfEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pu4RetValIeee8021BridgeEvbCAPSChannelID = pEvbSChIfEntry->u4SChId;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID (UINT4
                                                   u4Ieee8021BridgePhyPort,
                                                   UINT4
                                                   u4Ieee8021BridgeEvbSchID,
                                                   UINT4
                                                   *pu4RetValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID)
{
    tEvbSChIfEntry *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting S-channel  entry for the given Index */

    pEvbSChIfEntry = VlanEvbSChIfGetEntry(u4Ieee8021BridgePhyPort,
            u4Ieee8021BridgeEvbSchID);

    if(pEvbSChIfEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    /* SBP present in the same context as of UAP */
    *pu4RetValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID = u4ContextId; 
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPPort
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCAPAssociateSBPOrURPPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPPort (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
                                                 UINT4
                                                 *pu4RetValIeee8021BridgeEvbCAPAssociateSBPOrURPPort)
{
    tEvbSChIfEntry *pEvbSChIfEntry = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetEntry(u4Ieee8021BridgePhyPort,
            u4Ieee8021BridgeEvbSchID);
    if(pEvbSChIfEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValIeee8021BridgeEvbCAPAssociateSBPOrURPPort = 
        pEvbSChIfEntry->u4SChIfPort;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbCAPRowStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValIeee8021BridgeEvbCAPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbCAPRowStatus (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
                                     INT4
                                     *pi4RetValIeee8021BridgeEvbCAPRowStatus)
{
    tEvbSChIfEntry *pEvbSChIfEntry  = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetEntry(u4Ieee8021BridgePhyPort,
                    u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgeEvbCAPRowStatus=
        pEvbSChIfEntry->i4SChIfRowStatus;

    VlanReleaseContext ();
	return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                setValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID (UINT4
                                                   u4Ieee8021BridgePhyPort,
                                                   UINT4
                                                   u4Ieee8021BridgeEvbSchID,
                                                   UINT4
                                                   u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    UNUSED_PARAM (u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPPort
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                setValIeee8021BridgeEvbCAPAssociateSBPOrURPPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPPort (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
                                                 UINT4
                                                 u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPPort)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    UNUSED_PARAM (u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPPort);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbCAPRowStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                setValIeee8021BridgeEvbCAPRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbCAPRowStatus (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 u4Ieee8021BridgeEvbSchID , 
    INT4 i4SetValIeee8021BridgeEvbCAPRowStatus)
{

    tEvbSChIfEntry *pEvbSChIfEntry = NULL;
    tEvbUapIfEntry *pUapIfEntry    = NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;
    UINT1       u1Status        = 0;
    UINT1       u1OperStatus    = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if (pUapIfEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetEntry(u4Ieee8021BridgePhyPort,
                                        u4Ieee8021BridgeEvbSchID);
    if(pEvbSChIfEntry != NULL)
    {
        if(pEvbSChIfEntry->i4SChIfRowStatus == 
            i4SetValIeee8021BridgeEvbCAPRowStatus)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        if((i4SetValIeee8021BridgeEvbCAPRowStatus == CREATE_AND_WAIT) || 
                (i4SetValIeee8021BridgeEvbCAPRowStatus == CREATE_AND_GO))
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                       "Rowstatus wrong %d value for UAP %d and SVID %d\r\n",
                       i4SetValIeee8021BridgeEvbCAPRowStatus, 
                       u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if((i4SetValIeee8021BridgeEvbCAPRowStatus != CREATE_AND_WAIT) &&
               (i4SetValIeee8021BridgeEvbCAPRowStatus != CREATE_AND_GO))
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                       "Rowstatus value %d wrong for UAP %d and SVID %d\r\n",
                       i4SetValIeee8021BridgeEvbCAPRowStatus, 
                       u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValIeee8021BridgeEvbCAPRowStatus)
    {
        case NOT_READY:
        case NOT_IN_SERVICE:
           if (pEvbSChIfEntry->i4SChIfIndex != 0)
           {
	           if (VlanPortCfaDeleteSChannelInterface ((UINT4)pEvbSChIfEntry->i4SChIfIndex)
                     == VLAN_FAILURE)
		       {
                   VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                           "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                           "Cfa deletion failed for UAP %d and SVID %d for "
                           "SChIfIndex %d\r\n",
                           u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                           pEvbSChIfEntry->i4SChIfIndex);
		            VlanReleaseContext ();
			        return SNMP_FAILURE;
		       }
	       }

            if ((((pEvbSChIfEntry->u1OperStatus == CFA_IF_UP) && 
                 (pUapIfEntry->i4UapIfSChMode != VLAN_EVB_UAP_SCH_MODE_HYBRID)) ||
                ((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID) &&
                 (pEvbSChIfEntry->i4NegoStatus == VLAN_EVB_SCH_CONFIRMED)))
                  &&(pEvbSChIfEntry->i4SChIfRowStatus == ACTIVE)) 
            {
                if (VlanEvbHwConfigSChInterface (pEvbSChIfEntry,
                            VLAN_EVB_HW_SCH_IF_DELETE) == VLAN_FAILURE)
                {
                    if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_DELETE_FAIL_TRAP,
                                pEvbSChIfEntry->u4UapIfIndex, pEvbSChIfEntry->u4SVId)
                            == VLAN_FAILURE)
                    {
                    }
#ifdef  SYSLOG_WANTED  
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) deletion is failed on the UAP port %d",
                        pEvbSChIfEntry->u4SChId, pEvbSChIfEntry->u4SVId,
                        pEvbSChIfEntry->u4UapIfIndex));
#endif
                   VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                           "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                           "HW deletion failed for UAP %d and SVID %d for "
                           "SChIfIndex %d\r\n",
                           u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                           pEvbSChIfEntry->i4SChIfIndex);
                        /* Less priority to return failure here */
		            VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                pEvbSChIfEntry->u1OperStatus = CFA_IF_DOWN;
                pEvbSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_FREE;
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) is deleted in hardware  on the UAP port %d",
                        pEvbSChIfEntry->u4SChId, pEvbSChIfEntry->u4SVId,
                        pEvbSChIfEntry->u4UapIfIndex));
#endif
            }
            pEvbSChIfEntry->i4SChIfIndex = 0;
            pEvbSChIfEntry->i4SChIfRowStatus = 
                  i4SetValIeee8021BridgeEvbCAPRowStatus;

            break;

        case CREATE_AND_WAIT:
            if (VLAN_FAILURE == VlanEvbSChIfAddEntry (
                        u4Ieee8021BridgePhyPort,
                        u4Ieee8021BridgeEvbSchID,
                        VLAN_EVB_UAP_SCH_MODE_STATIC, 0,
                        &pEvbSChIfEntry))
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                        "SCh addition failed for UAP %d and SVID %d for "
                        "SChIfIndex %d\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                        pEvbSChIfEntry->i4SChIfIndex);
                CLI_SET_ERR(CLI_VLAN_EVB_ADD_ENTRY_ERR);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pEvbSChIfEntry->i4SChIfRowStatus = 
                  i4SetValIeee8021BridgeEvbCAPRowStatus;
            break;

        case CREATE_AND_GO:
            if (VLAN_FAILURE == VlanEvbSChIfAddEntry (
                        u4Ieee8021BridgePhyPort,
                        u4Ieee8021BridgeEvbSchID,
                        VLAN_EVB_UAP_SCH_MODE_STATIC, 0,
                        &pEvbSChIfEntry))
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                        "SCH Create & Go for UAP %d and SVID %d for "
                        "SChIfIndex %d\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                        pEvbSChIfEntry->i4SChIfIndex);
                CLI_SET_ERR(CLI_VLAN_EVB_ADD_ENTRY_ERR);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            /* Fall through */ 
        case ACTIVE:
            if (pEvbSChIfEntry->i4SChIfIndex == 0)
            {
                /* This will fall in case of CREATE_AND_GO || 
                 * i4SChIfIndex is zero */
                /* Fetching free interface index from CFA for SBP port creation*/
                pEvbSChIfEntry->i4SChIfIndex = (INT4) (VlanEvbGetFreeSChIfIndex 
                    (u4Ieee8021BridgePhyPort));

                if ((pEvbSChIfEntry)->i4SChIfIndex == 0)
                {
                   VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                           "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                           "Free SCh IfIndex  for UAP %d and SVID %d for "
                           "SChIfIndex %d is not present\r\n",
                           u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                           pEvbSChIfEntry->i4SChIfIndex);

					if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_FAIL_TRAP,
								u4Ieee8021BridgePhyPort,u4Ieee8021BridgeEvbSchID)
							== VLAN_FAILURE)
					{
						VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
								VLAN_NAME, "nmhSetIeee8021BridgeEvbCAPRowStatus: "
								"EVB S-Channel create Trap generation failed"
								"for SCID-SVID (%d - %d) on"
								"the UAP port %d\r\n",
                                pEvbSChIfEntry->i4SChIfIndex, 
								u4Ieee8021BridgeEvbSchID,
								u4Ieee8021BridgePhyPort);
					}

                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
            }
            /* Indication to create SBP port */
            if (VLAN_EVB_MODULE_STATUS (pUapIfEntry->u4UapIfCompId)
                                             == VLAN_EVB_MODULE_ENABLE)
            {
                /* Check if CDCP Mode is Hybrid - 
                 * If it is so , set the Status to OPER_DOWN for the 
                 * created S-Channel */
                if(pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
                {
                    u1Status = VLAN_EVB_SCHANNEL_OPER_DOWN; 
                }
                if (VlanPortCfaCreateSChannelInterface (u4Ieee8021BridgePhyPort,
                        (UINT4)pEvbSChIfEntry->i4SChIfIndex,u1Status) 
                        == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                            "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                            "Cfa creation failed for UAP %d and SVID %d for "
                            "SChIfIndex %d\r\n",
                            u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                            pEvbSChIfEntry->i4SChIfIndex);

					if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_FAIL_TRAP,
								u4Ieee8021BridgePhyPort, pEvbSChIfEntry->u4SVId)
							== VLAN_FAILURE)
					{
						VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
								VLAN_NAME, "nmhSetIeee8021BridgeEvbCAPRowStatus: "
								"EVB S-Channel create Trap generation failed"
								"for SCID-SVID (%d - %d) on"
								"the UAP port %d\r\n", pEvbSChIfEntry->i4SChIfIndex, 
								u4Ieee8021BridgeEvbSchID,
								u4Ieee8021BridgePhyPort);
					}
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                if (VlanEvbHwConfigSChInterface (pEvbSChIfEntry,
                            VLAN_EVB_HW_SCH_IF_CREATE) == VLAN_FAILURE)
                {
                    if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_FAIL_TRAP,
                                pEvbSChIfEntry->u4UapIfIndex, pEvbSChIfEntry->u4SVId)
                            == VLAN_FAILURE)
                    {
						VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
								VLAN_NAME, "nmhSetIeee8021BridgeEvbCAPRowStatus: "
								"EVB S-Channel create Trap generation failed"
								"for SCID-SVID (%d - %d) on"
								"the UAP port %d\r\n", pEvbSChIfEntry->i4SChIfIndex, 
								pEvbSChIfEntry->u4SVId,
								pEvbSChIfEntry->u4UapIfIndex);
                    }
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) creation is failed in hardware on the UAP port %d",
                        pEvbSChIfEntry->u4SChId, pEvbSChIfEntry->u4SVId,
                        pEvbSChIfEntry->u4UapIfIndex));

#endif
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                            "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                            "HW creation failed for UAP %d and SVID %d for "
                            "SChIfIndex %d\r\n",
                            u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                            pEvbSChIfEntry->i4SChIfIndex);
                    /* Less priority to return failure here */
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
				if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_TRAP,
							pEvbSChIfEntry->u4UapIfIndex, pEvbSChIfEntry->u4SVId)
						== VLAN_FAILURE)
				{
                     VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                                    VLAN_NAME, "nmhSetIeee8021BridgeEvbCAPRowStatus: "
                                    "EVB S-Channel create Trap generation failed"
                                    "for SCID-SVID (%d - %d) on"
                                    "the UAP port %d\r\n", pEvbSChIfEntry->u4SChId, 
                                    pEvbSChIfEntry->u4SVId,
                                    pEvbSChIfEntry->u4UapIfIndex);
				}
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL, (UINT4)gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) is created in hardware  on the UAP port %d",
                        pEvbSChIfEntry->u4SChId, pEvbSChIfEntry->u4SVId,
                        pEvbSChIfEntry->u4UapIfIndex));
#endif
                /* Static mode - so get UAP oper status and if it is UP, 
                 * Allow traffic over the S-Channel in HW */
                VlanCfaGetIfOperStatus (pEvbSChIfEntry->u4UapIfIndex, 
                                        &u1OperStatus); 
                if ((u1OperStatus == CFA_IF_UP) && 
                    (pUapIfEntry->i4UapIfSChMode != VLAN_EVB_UAP_SCH_MODE_HYBRID)) 
                { 
                    /* S-channel Oper Up/Down trap on enabling Traffic Forward
                     * in the static case will be sent from the function 
                     * "VlanEvbSChSetAdminStatus"  present in EVB module 
                     * itself */
                    if (VlanEvbHwConfigSChInterface (pEvbSChIfEntry,
                                VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD) == VLAN_FAILURE)
                    {
                        VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                                "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                                "HW forward failed for UAP %d and SVID %d for "
                                "SChIfIndex %d\r\n",
                                u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, 
                                pEvbSChIfEntry->i4SChIfIndex);
                        /* Less priority to return failure here */
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
#ifdef SYSLOG_WANTED
                    SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                                "SBP oper Status is [UP] for SCID-SVID (%d - %d) on the UAP port %d",
                                pEvbSChIfEntry->u4SChId, pEvbSChIfEntry->u4SVId,
                                pEvbSChIfEntry->u4UapIfIndex));
#endif
                    pEvbSChIfEntry->u1OperStatus = CFA_IF_UP;
                }
            }
            (pEvbSChIfEntry)->i4SChIfRowStatus = ACTIVE;
            break;

        case DESTROY:
            if(VLAN_FAILURE == VlanEvbSChIfDelEntry 
                                (u4Ieee8021BridgePhyPort,
                                  u4Ieee8021BridgeEvbSchID, 0))
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhSetIeee8021BridgeEvbCAPRowStatus:"
                        "SCh deletion failed for UAP %d and SVID %d for "
                        "SChIfIndex %d\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
                CLI_SET_ERR(CLI_VLAN_EVB_DEL_ENTRY_ERR);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            break;

        default:
            VlanReleaseContext ();
            return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                testValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID (UINT4 *pu4ErrorCode,
                                                      UINT4
                                                      u4Ieee8021BridgePhyPort,
                                                      UINT4
                                                      u4Ieee8021BridgeEvbSchID,
                                                      UINT4
                                                      u4TestValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    UNUSED_PARAM (u4TestValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID);

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPPort
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                testValIeee8021BridgeEvbCAPAssociateSBPOrURPPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPPort (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021BridgePhyPort,
                                                    UINT4
                                                    u4Ieee8021BridgeEvbSchID,
                                                    UINT4
                                                    u4TestValIeee8021BridgeEvbCAPAssociateSBPOrURPPort)
{
    UNUSED_PARAM (u4Ieee8021BridgePhyPort);
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);
    UNUSED_PARAM (u4TestValIeee8021BridgeEvbCAPAssociateSBPOrURPPort);

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbCAPRowStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                testValIeee8021BridgeEvbCAPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbCAPRowStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021BridgePhyPort,
                                        UINT4 u4Ieee8021BridgeEvbSchID,
                                        INT4
                                        i4TestValIeee8021BridgeEvbCAPRowStatus)
{
	tEvbSChIfEntry  *pEvbSChIfEntry = NULL;
	tEvbUapIfEntry  *pUapIfEntry    = NULL;
    tEvbContextInfo *pContextInfo   = NULL;

	UINT4          u4ContextId     = 0;
	UINT2          u2LocalPortId   = 0;

	if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
				&u4ContextId,
				&u2LocalPortId) != VCM_SUCCESS)
	{
		CLI_SET_ERR (CLI_VLAN_EVB_GET_CONTEXT_FROM_IF_ERR);
		return SNMP_FAILURE;
	}
	UNUSED_PARAM (u2LocalPortId);

	/* Verifying the EVB System status */
	if(VLAN_EVB_SYSTEM_START != 
			VLAN_EVB_SYSTEM_STATUS (u4ContextId))
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		return SNMP_FAILURE;
	}

	if ((u4Ieee8021BridgeEvbSchID == VLAN_EVB_DEF_SVID) &&
			(i4TestValIeee8021BridgeEvbCAPRowStatus == CREATE_AND_WAIT))
	{
		CLI_SET_ERR (CLI_VLAN_EVB_DEF_SVID_CREATION_NOT_ALLOWED);
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}

	if ((u4Ieee8021BridgeEvbSchID == VLAN_EVB_DEF_SVID) &&
			(i4TestValIeee8021BridgeEvbCAPRowStatus == DESTROY))
	{
		CLI_SET_ERR (CLI_VLAN_EVB_DEF_SVID_DELETION_NOT_ALLOWED);
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}
	if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
	{
		return SNMP_FAILURE;
	}

	/* Check if EVB system status is Enabled */

	if (VLAN_EVB_SYSTEM_STATUS(u4ContextId) != VLAN_EVB_SYSTEM_START)
	{
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                "System not started for context %d when configuring"
                "UAP %d SVID %d \r\n", u4ContextId,
                u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
		CLI_SET_ERR (CLI_VLAN_EVB_MODULE_STATUS_ERR);
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		VlanReleaseContext ();
		return SNMP_FAILURE;
	}

    /* Get UAP Interface Entry for the given Port */
    pUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    /* Get S-Channel Interface Entry for the given Port and S-Vid */

    pEvbSChIfEntry = VlanEvbSChIfGetEntry 
        (u4Ieee8021BridgePhyPort,u4Ieee8021BridgeEvbSchID);

	switch (i4TestValIeee8021BridgeEvbCAPRowStatus)
	{
		case ACTIVE: 
		case NOT_IN_SERVICE:
		case NOT_READY:
		case DESTROY:
			if (pEvbSChIfEntry == NULL)
			{
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "UAP %d SVID %d - entry not present\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
				CLI_SET_ERR(CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
				*pu4ErrorCode = SNMP_ERR_NO_CREATION;
				VlanReleaseContext ();
				return SNMP_FAILURE;
			}
            if ((NULL != pUapIfEntry) && 
                ((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC) &&
                (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE)))
            {
                VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "For UAP %d - Mode is not Dynamic"
                        "and CDCP is Not Enable \r\n",
                        u4Ieee8021BridgePhyPort); 
				CLI_SET_ERR(CLI_VLAN_EVB_DEL_ENTRY_NOT_ALLOWED);
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				VlanReleaseContext ();
				return SNMP_FAILURE;
            }
			break;

		case CREATE_AND_WAIT:
		case CREATE_AND_GO:
			if (pEvbSChIfEntry != NULL)
			{
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "UAP %d SVID %d - CAW or CAG - entry already present\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
				CLI_SET_ERR(CLI_VLAN_EVB_SCH_ENTRY_ALREADY_PRESENT_ERR);
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				VlanReleaseContext ();
				return SNMP_FAILURE;
			}

			if (NULL == pUapIfEntry)
			{
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "UAP %d SVID %d - UAP entry not present\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
				CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				VlanReleaseContext ();
				return SNMP_FAILURE;
			}

			/* Check if existing mode is DYNAMIC. If CDCP mode is enabled, then 
			 * dont allow static configuratons.
			 * CDCP ENABLE or not validation is required because i4UapIfSChMode's
			 * default value is dynamic. */
			if ((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC) 
					&& (pUapIfEntry->i4UapIfSchCdcpAdminEnable == 
						VLAN_EVB_UAP_CDCP_ENABLE))
			{
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "UAP %d SVID %d - dynamic and CDCP enabled\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
				CLI_SET_ERR(CLI_VLAN_EVB_CDCP_ENABLE_SCH_NOT_ALLOWED_ERR);
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				VlanReleaseContext ();
				return SNMP_FAILURE;
			}
            pContextInfo = VLAN_EVB_GET_CONTEXT_PTR(u4ContextId);
            if (NULL == pContextInfo)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "UAP %d SVID %d - context entry not present\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID, u4ContextId);
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				VlanReleaseContext ();
				return SNMP_FAILURE;
			}
            if ((pContextInfo->i4EvbSysSChMode == VLAN_EVB_SCH_MODE_AUTO) &&
                ((VlanEvbSChGetNextSChannelId(pUapIfEntry->u4UapIfIndex)) == 0))
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2Ieee8021BridgeEvbCAPRowStatus: "
                        "UAP %d SVID %d - dynamic and CDCP enabled\r\n",
                        u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID); 
				CLI_SET_ERR(CLI_VLAN_EVB_MAX_SCHANNEL_ERR);
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				VlanReleaseContext ();
				return SNMP_FAILURE;
            }
			break;

		default:
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			VlanReleaseContext ();
			return SNMP_FAILURE;
	}
	VlanReleaseContext ();
	return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbCAPConfigTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbURPTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbURPTable
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeEvbURPTable (UINT4
                                                   u4Ieee8021BridgeEvbURPComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeEvbURPPort)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbURPTable
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeEvbURPTable (UINT4
                                           *pu4Ieee8021BridgeEvbURPComponentId,
    UINT4 *pu4Ieee8021BridgeEvbURPPort)
{
    /* Not supported */
    UNUSED_PARAM(pu4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbURPPort);
    return SNMP_FAILURE;  
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbURPTable
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                nextIeee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort
                nextIeee8021BridgeEvbURPPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeEvbURPTable (UINT4
                                          u4Ieee8021BridgeEvbURPComponentId,
                                          UINT4
                                          *pu4NextIeee8021BridgeEvbURPComponentId,
                                          UINT4 u4Ieee8021BridgeEvbURPPort,
                                          UINT4
                                          *pu4NextIeee8021BridgeEvbURPPort)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbURPPort);
    return SNMP_SUCCESS;  
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbURPIfIndex
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                retValIeee8021BridgeEvbURPIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbURPIfIndex (UINT4 u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
    INT4 *pi4RetValIeee8021BridgeEvbURPIfIndex)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbURPIfIndex);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbURPBindToISSPort
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                retValIeee8021BridgeEvbURPBindToISSPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbURPBindToISSPort (UINT4
                                         u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
                                         UINT4
                                         *pu4RetValIeee8021BridgeEvbURPBindToISSPort)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbURPBindToISSPort);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbURPLldpManual
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                retValIeee8021BridgeEvbURPLldpManual
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbURPLldpManual (UINT4 u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
                                      INT4
                                      *pi4RetValIeee8021BridgeEvbURPLldpManual)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pi4RetValIeee8021BridgeEvbURPLldpManual);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbURPVdpOperRsrcWaitDelay
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                retValIeee8021BridgeEvbURPVdpOperRsrcWaitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbURPVdpOperRsrcWaitDelay (UINT4
                                                u4Ieee8021BridgeEvbURPComponentId,
                                                UINT4
                                                u4Ieee8021BridgeEvbURPPort,
                                                UINT4
                                                *pu4RetValIeee8021BridgeEvbURPVdpOperRsrcWaitDelay)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbURPVdpOperRsrcWaitDelay);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbURPVdpOperRespWaitDelay
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                retValIeee8021BridgeEvbURPVdpOperRespWaitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbURPVdpOperRespWaitDelay (UINT4
                                                u4Ieee8021BridgeEvbURPComponentId,
                                                UINT4
                                                u4Ieee8021BridgeEvbURPPort,
                                                UINT4
                                                *pu4RetValIeee8021BridgeEvbURPVdpOperRespWaitDelay)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbURPVdpOperRespWaitDelay);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbURPVdpOperReinitKeepAlive
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                retValIeee8021BridgeEvbURPVdpOperReinitKeepAlive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbURPVdpOperReinitKeepAlive (UINT4
                                                  u4Ieee8021BridgeEvbURPComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeEvbURPPort,
                                                  UINT4
                                                  *pu4RetValIeee8021BridgeEvbURPVdpOperReinitKeepAlive)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbURPVdpOperReinitKeepAlive);
    return SNMP_SUCCESS;  
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbURPIfIndex
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                setValIeee8021BridgeEvbURPIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbURPIfIndex (UINT4 u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
    INT4 i4SetValIeee8021BridgeEvbURPIfIndex)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbURPIfIndex);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbURPBindToISSPort
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                setValIeee8021BridgeEvbURPBindToISSPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbURPBindToISSPort (UINT4
                                         u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
                                         UINT4
                                         u4SetValIeee8021BridgeEvbURPBindToISSPort)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(u4SetValIeee8021BridgeEvbURPBindToISSPort);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbURPLldpManual
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                setValIeee8021BridgeEvbURPLldpManual
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbURPLldpManual (UINT4 u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
                                      INT4
                                      i4SetValIeee8021BridgeEvbURPLldpManual)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(i4SetValIeee8021BridgeEvbURPLldpManual);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbURPVdpOperRespWaitDelay
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                setValIeee8021BridgeEvbURPVdpOperRespWaitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbURPVdpOperRespWaitDelay (UINT4
                                                u4Ieee8021BridgeEvbURPComponentId,
                                                UINT4
                                                u4Ieee8021BridgeEvbURPPort,
                                                UINT4
                                                u4SetValIeee8021BridgeEvbURPVdpOperRespWaitDelay)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(u4SetValIeee8021BridgeEvbURPVdpOperRespWaitDelay);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhSetIeee8021BridgeEvbURPVdpOperReinitKeepAlive
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                setValIeee8021BridgeEvbURPVdpOperReinitKeepAlive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeEvbURPVdpOperReinitKeepAlive (UINT4
                                                  u4Ieee8021BridgeEvbURPComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeEvbURPPort,
                                                  UINT4
                                                  u4SetValIeee8021BridgeEvbURPVdpOperReinitKeepAlive)
{
    /* Not supported */
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(u4SetValIeee8021BridgeEvbURPVdpOperReinitKeepAlive);
    return SNMP_SUCCESS;  
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbURPIfIndex
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                testValIeee8021BridgeEvbURPIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbURPIfIndex (UINT4 *pu4ErrorCode,
    UINT4 u4Ieee8021BridgeEvbURPComponentId , 
    UINT4 u4Ieee8021BridgeEvbURPPort , 
    INT4 i4TestValIeee8021BridgeEvbURPIfIndex)
{
    /* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbURPIfIndex);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbURPBindToISSPort
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                testValIeee8021BridgeEvbURPBindToISSPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbURPBindToISSPort (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
                                            UINT4
                                            u4TestValIeee8021BridgeEvbURPBindToISSPort)
{
    /* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(u4TestValIeee8021BridgeEvbURPBindToISSPort);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbURPLldpManual
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                testValIeee8021BridgeEvbURPLldpManual
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbURPLldpManual (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021BridgeEvbURPComponentId,
    UINT4 u4Ieee8021BridgeEvbURPPort , 
                                         INT4
                                         i4TestValIeee8021BridgeEvbURPLldpManual)
{
    /* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(i4TestValIeee8021BridgeEvbURPLldpManual);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbURPVdpOperRespWaitDelay
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                testValIeee8021BridgeEvbURPVdpOperRespWaitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbURPVdpOperRespWaitDelay (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021BridgeEvbURPComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeEvbURPPort,
                                                   UINT4
                                                   u4TestValIeee8021BridgeEvbURPVdpOperRespWaitDelay)
{
    /* Not supported */
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(u4TestValIeee8021BridgeEvbURPVdpOperRespWaitDelay);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeEvbURPVdpOperReinitKeepAlive
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort

                The Object 
                testValIeee8021BridgeEvbURPVdpOperReinitKeepAlive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeEvbURPVdpOperReinitKeepAlive (UINT4 *pu4ErrorCode,
                                                     UINT4
                                                     u4Ieee8021BridgeEvbURPComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeEvbURPPort,
                                                     UINT4
                                                     u4TestValIeee8021BridgeEvbURPVdpOperReinitKeepAlive)
{
    /* Not supported */ 
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbURPPort);
    UNUSED_PARAM(u4TestValIeee8021BridgeEvbURPVdpOperReinitKeepAlive);
    return SNMP_SUCCESS;  
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeEvbURPTable
 Input       :  The Indices
                Ieee8021BridgeEvbURPComponentId
                Ieee8021BridgeEvbURPPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeEvbURPTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : Ieee8021BridgeEvbEcpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable (UINT4
                                                   u4Ieee8021BridgeEvbEcpComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeEvbEcpPort)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeEvbEcpTable
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeEvbEcpTable (UINT4
                                           *pu4Ieee8021BridgeEvbEcpComponentId,
    UINT4 *pu4Ieee8021BridgeEvbEcpPort)
{
    /* Not supported */ 
    UNUSED_PARAM(pu4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(pu4Ieee8021BridgeEvbEcpPort);
    return SNMP_FAILURE;  
}
/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeEvbEcpTable
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                nextIeee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort
                nextIeee8021BridgeEvbEcpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeEvbEcpTable (UINT4
                                          u4Ieee8021BridgeEvbEcpComponentId,
                                          UINT4
                                          *pu4NextIeee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort ,
                                          UINT4
                                          *pu4NextIeee8021BridgeEvbEcpPort)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4NextIeee8021BridgeEvbEcpPort);
    return SNMP_SUCCESS;  
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbEcpOperAckTimerInit
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort

                The Object 
                retValIeee8021BridgeEvbEcpOperAckTimerInit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbEcpOperAckTimerInit (UINT4
                                            u4Ieee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort , 
                                            UINT4
                                            *pu4RetValIeee8021BridgeEvbEcpOperAckTimerInit)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbEcpOperAckTimerInit);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbEcpOperMaxRetries
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort

                The Object 
                retValIeee8021BridgeEvbEcpOperMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbEcpOperMaxRetries (UINT4
                                          u4Ieee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort , 
                                          UINT4
                                          *pu4RetValIeee8021BridgeEvbEcpOperMaxRetries)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbEcpOperMaxRetries);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbEcpTxFrameCount
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort

                The Object 
                retValIeee8021BridgeEvbEcpTxFrameCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbEcpTxFrameCount (UINT4 u4Ieee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort , 
                                        UINT4
                                        *pu4RetValIeee8021BridgeEvbEcpTxFrameCount)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbEcpTxFrameCount);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbEcpTxRetryCount
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort

                The Object 
                retValIeee8021BridgeEvbEcpTxRetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbEcpTxRetryCount (UINT4 u4Ieee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort , 
                                        UINT4
                                        *pu4RetValIeee8021BridgeEvbEcpTxRetryCount)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbEcpTxRetryCount);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbEcpTxFailures
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort

                The Object 
                retValIeee8021BridgeEvbEcpTxFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbEcpTxFailures (UINT4 u4Ieee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort , 
                                      UINT4
                                      *pu4RetValIeee8021BridgeEvbEcpTxFailures)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbEcpTxFailures);
    return SNMP_SUCCESS;  
}
/****************************************************************************
 Function    :  nmhGetIeee8021BridgeEvbEcpRxFrameCount
 Input       :  The Indices
                Ieee8021BridgeEvbEcpComponentId
                Ieee8021BridgeEvbEcpPort

                The Object 
                retValIeee8021BridgeEvbEcpRxFrameCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeEvbEcpRxFrameCount (UINT4 u4Ieee8021BridgeEvbEcpComponentId,
    UINT4 u4Ieee8021BridgeEvbEcpPort , 
                                        UINT4
                                        *pu4RetValIeee8021BridgeEvbEcpRxFrameCount)
{
    /* Not supported */ 
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpComponentId);
    UNUSED_PARAM(u4Ieee8021BridgeEvbEcpPort);
    UNUSED_PARAM(pu4RetValIeee8021BridgeEvbEcpRxFrameCount);
    return SNMP_SUCCESS;  
}


