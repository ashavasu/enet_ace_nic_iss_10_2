/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: vlannpwr.c,v 1.41 2016/03/03 10:19:51 siva Exp $ 
 * File Name  : vlannpwr.c
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "vlaninc.h"

/* VLAN FDB TABLE is currently supported for the Option NPAPI_WANTED.
 * Which means FDB table is present either in hardware or software.
 * If the table should be duplicated then whenever software is updated
 * then the hardware should be intimated */

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwInit                                     */
/*                                                                           */
/*    Description         : This function takes care of initialising the     */
/*                          hardware related parameters.                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsMiVlanHwInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwInit ();
    /* Set Egress filtering to enable in the hardware */
    /* gegrSetUnkUcEgressFilterCmd () - Egress filtering for Unknown Unicast */
    /* gegrSetUnregMcEgressFilterCmd ()  - Egress filtering for unregistered 
     * multicast*/
    /* gegrVlanEgressFilteringEnable () - Egress filtering for known unicast */

}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeInit                                   */
/*                                                                           */
/*    Description         : This function deinitialises all the hardware     */
/*                          related tables and parameters.                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsMiVlanHwDeInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDeInit ();
    /* Disable VLAN in hardware */
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetAllGroupsPorts                        */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tPortList PortBmp)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetAllGroupsPorts (VlanId, PortBmp);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwResetAllGroupsPorts                      */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward All Group Port list for the specified    */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwResetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tPortList PortBmp)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwResetAllGroupsPorts (VlanId, PortBmp);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetUnRegGroupsPorts                      */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId                                  */
/*                          PortBmp  - PortList.                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tPortList PortBmp)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetUnRegGroupsPorts (VlanId, PortBmp);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwResetUnRegGroupsPorts                  */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward UnReg Group Port list for the specified  */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwResetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tPortList PortBmp)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwResetUnRegGroupsPorts (VlanId, PortBmp);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddStaticUcastEntry                  */
/*                                                                           */
/*    Description         : This function adds the egress ports  into        */
/*                          hardware Static Unicast table                    */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                          AllowedToGoPortBmp - PortList                    */
/*                          u1Status         -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout                                 */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                 tMacAddr MacAddr, UINT4 u4Port,
                                 tPortList AllowedToGoPortBmp, UINT1 u1Status,
                                 tMacAddr ConnectionId)
{
    UNUSED_PARAM (u4ContextId);

    if (VLAN_ARE_MAC_ADDR_EQUAL (ConnectionId, gNullMacAddress) == VLAN_TRUE)
    {
        return FsVlanHwAddStaticUcastEntry (u4Fid, MacAddr, u4Port,
                                            AllowedToGoPortBmp, u1Status);
    }
    else
    {
        return FsVlanHwAddStaticUcastEntryEx (u4Fid, MacAddr, u4Port,
                                              AllowedToGoPortBmp, u1Status,
                                              ConnectionId);
    }
    /* If number of ports in the Port list is more than 1, then return failure */
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelStaticUcastEntry                      */
/*                                                                           */
/*    Description         : This function delets the entry from  hardware    */
/*                          Static Unicast table.                            */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                               tMacAddr MacAddr, UINT4 u4Port)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDelStaticUcastEntry (u4Fid, MacAddr, u4Port);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetPortFromFdb                         */
/*                                                                           */
/*    Description         : This function gets the port from the FDB table   */
/*                          for the given vlanId and mac                     */
/*                                                                           */
/*    Input(s)            : u2VlanId - Vlan Id                               */
/*                          MacAddr  - MacAddr                               */
/*                                                                           */
/*    Output(s)           : pu4Port  - Physical port number                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */                  /*****************************************************************************/
INT4
FsMiVlanHwGetPortFromFdb (UINT2 u2VlanId, UINT1 *MacAddr, UINT4 *pu4Port)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pu4Port);
    return FNP_SUCCESS;
}
#ifndef SW_LEARNING
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetFdbCount                            */
/*                                                                           */
/*    Description         : This function gets the Dynamic Fdb Count for the */
/*                          given FdbId                                      */
/*                                                                           */
/*    Input(s)            : u4FdbId  - FdbId.                                */
/*                                                                           */
/*    Output(s)           : pu4Count - Count of the given FdbId.             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetFdbCount (UINT4 u4ContextId, UINT4 u4FdbId, UINT4 *pu4Count)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetFdbCount (u4FdbId, pu4Count);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetFdbEntry                              */
/*                                                                           */
/*    Description         : This function gets the entry from  hardware      */
/*                           Unicast Mac table.                              */
/*                                                                           */
/*    Input(s)            : u4FdbId  - FdbId                                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : pEntry  - Pointer to Unicast Mac Entry           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId,
                       tMacAddr MacAddr, tHwUnicastMacEntry * pEntry)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetFdbEntry (u4FdbId, MacAddr, pEntry);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetFirstTpFdbEntry                       */
/*                                                                           */
/*    Description         : This function gets the first Fdb entry in the    */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : pu4FdbId - Pointer to the Fdb id                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : The first FdbId and Mac address.                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetFirstTpFdbEntry (UINT4 u4ContextId, UINT4 *pu4FdbId,
                              tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetFirstTpFdbEntry (pu4FdbId, MacAddr);

    /* 
     * First get the VlanId depending on the Vlan learning type and
     * then invoke the Hw api.
     */

}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetNextTpFdbEntry                        */
/*                                                                           */
/*    Description         : This function gets the next Fdb entry in the     */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : u4FdbId - The current FdbId.                     */
/*                          pMacAddr - Pointer to the current Mac addr.      */
/*    Input(s)            : pu4NextFdbId - Pointer to the next Fdb Id.       */
/*                          pNextMacAddr - Pointer to the next mac addr.     */
/*                                                                           */
/*    Output(s)           : The next FdbId and Mac address.                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetNextTpFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
                             UINT4 *pu4NextContext, UINT4 *pu4NextFdbId,
                             tMacAddr NextMacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu4NextContext);
    return FsVlanHwGetNextTpFdbEntry (u4FdbId, MacAddr, pu4NextFdbId,
                                      NextMacAddr);
    /* 
     * First get the next VlanId depending on the Vlan learning type and
     * then invoke the Hw api.
     */

}

#endif
/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddMcastEntry                            */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          PortBmp - List of Ports.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           tPortList PortBmp)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwAddMcastEntry (VlanId, MacAddr, PortBmp);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddStMcastEntry                          */
/*                                                                           */
/*    Description         : This function adds a static entry to the         */
/*                          hardware Multicast table                         */
/*                                                                           */
/*    Input(s)            : VlanId    - VlanId                               */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port                        */
/*                          PortBmp   - List of Ports.                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, INT4 i4RcvPort,
                             tPortList PortBmp)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwAddStMcastEntry (VlanId, MacAddr, i4RcvPort, PortBmp);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetMcastPort                             */
/*                                                                           */
/*    Description         : This function adds a member port to the existing */
/*                          Multicast entry. The Application has to ensure   */
/*                          that the existence of Mcast Entry for the port.  */
/*                          This API will be called only under dynamic       */
/*                          updation of the Multicast Entry by the S/W.      */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          u2IfIndex - Interface Index of New member port   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetMcastPort (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                        UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetMcastPort (VlanId, MacAddr, u4IfIndex);

}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetMcastPort                           */
/*                                                                           */
/*    Description         : This function resets a member port from the      */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          u2IfIndex - Interface Index of member port to be */
/*                                      removed                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetMcastPort (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                          UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwResetMcastPort (VlanId, MacAddr, u4IfIndex);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelMcastEntry                            */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Multicast table.                                 */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDelMcastEntry (VlanId, MacAddr);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelStMcastEntry                          */
/*                                                                           */
/*    Description         : This function deletes a static entry from the    */
/*                          hardware Multicast tale                          */
/*                                                                           */
/*    Input(s)            : VlanId    - VlanId                               */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           INT4 i4RcvPort)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDelStMcastEntry (VlanId, MacAddr, i4RcvPort);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddVlanEntry                             */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : VlanId     - VlanId                              */
/*                          PortBmp - List of Ports.                         */
/*                          UnTagPortBmp - List of Untagged Ports.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId, tPortList PortBmp,
                          tPortList UnTagPortBmp)
{
    INT4                i4RetVal = FNP_FAILURE;
    UNUSED_PARAM (u4ContextId);
    if (VlanId <= VLAN_VFI_MIN_ID)
    {
        i4RetVal = FsVlanHwAddVlanEntry (VlanId, PortBmp, UnTagPortBmp);
    }
    else if (VLAN_VFI_MAX_ID == VLAN_VFI_MIN_ID)
    {
        /* No VPLS VLAN exist */
        i4RetVal = FNP_FAILURE;
    }
    else
    {
        /* VLAN ID greater than VLAN_VFI_MIN_ID is for VPLS VLAN.
           So returning success */
        i4RetVal = FNP_SUCCESS;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelVlanEntry                             */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Vlan table.                                      */
/*                          In case of Hybrid learning switches, this fn.    */
/*                          should disassociate the VLAN to FID mappings and */
/*                          and shud delete the FDB table if the number of   */
/*                          VLANS mapped to the FDB is 0.                    */
/*                                                                           */
/*    Input(s)            : VlanId     - VlanId                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelVlanEntry (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDelVlanEntry (VlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetVlanMemberPort                        */
/*                                                                           */
/*    Description         : This function set the port as the member of      */
/*                          given Vlan                                       */
/*                                                                           */
/*    Input(s)            : VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u2IfIndex  - The Interface Index                 */
/*                          u1IsTagged - Flag to indicate whether this port  */
/*                                       will be tagged or untagged          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex,
                             UINT1 u1IsTagged)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetVlanMemberPort (VlanId, u4IfIndex, u1IsTagged);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetVlanMemberPort                      */
/*                                                                           */
/*    Description         : This function Reset the port as the member of    */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                               UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwResetVlanMemberPort (VlanId, u4IfIndex);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortPvid                              */
/*                                                                           */
/*    Description         : This function sets the given VlanId as PortPvid  */
/*                          for the Port.                                    */
/*                                                                           */
/*    Input(s)            : VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetPortPvid (u4IfIndex, VlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetDefaultVlanId                       */
/*                                                                           */
/*    Description         : This function sets the default VlanId            */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : VlanId     - Default Vlan Id                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetDefaultVlanId (VlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetBaseBridgeMode                     */
/*                                                                           */
/*    Description         : This function sets the 802.1D Mode in hardware   */
/*                                                                           */
/*    Input(s)            : u4Mode    - Enable / Diable 802.1D Mode          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetBaseBridgeMode (UINT4 u4ContextId, UINT4 u4Mode)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetBaseBridgeMode (u4Mode);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortAccFrameType                      */
/*                                                                           */
/*    Description         : This function sets the Acceptable frame types    */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex      - The Interface Index             */
/*                          u1AccFrameType - Acceptable Frame types          */
/*                                           parameter value.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                               INT1 u1AccFrameType)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetPortAccFrameType (u4IfIndex, u1AccFrameType);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortIngFiltering                      */
/*                                                                           */
/*    Description         : This function sets the Ingress Filtering         */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u2IfIndex      - The Interface Index             */
/*                          u1IngFilterEnable - FNP_TRUE if Ingress          */
/*                                              filtering enabled, otherwise */
/*                                              FNP_FALSE                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT1 u1IngFilterEnable)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetPortIngFiltering (u4IfIndex, u1IngFilterEnable);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwVlanEnable                               */
/*                                                                           */
/*    Description         : This function enables hardware.                  */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwVlanEnable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwVlanEnable ();
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwVlanDisable                              */
/*                                                                           */
/*    Description         : This function disables the hardware.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :                                                   */
/*                                FNP_SUCCESS: Disabled and entries flushed  */
/*                                FNP_FAILURE: Disabling not supported       */
/*                                FNP_VLAN_DISABLED_BUT_NOT_DELETED:         */
/*                                 Hardware supports disabling but the       */
/*                                 entries were not flushed                  */
/*****************************************************************************/
INT4
FsMiVlanHwVlanDisable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwVlanDisable ();
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetVlanLearningType                      */
/*                                                                           */
/*    Description         : This function programs the Vlan Learning Type    */
/*                                                                           */
/*    Input(s)            : u1LearningType - IVL/SVL/HVL(Hybrid Vlan Learn)  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearningType)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetVlanLearningType (u1LearningType);
    /* If the Hardware does not support the given Vlan Learning Type, then
     * return failure.
     */
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetMacBasedStatusOnPort                  */
/*                                                                           */
/*    Description         : This function enable or disable the Mac based    */
/*                        Vlan Type on the port                              */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                          u1MacBasedVlanEnable - FNP_TRUE/FNP_FALSE        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetMacBasedStatusOnPort (UINT4 u4ContextId,
                                   UINT4 u4IfIndex, UINT1 u1MacBasedVlanEnable)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetMacBasedStatusOnPort (u4IfIndex, u1MacBasedVlanEnable);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetSubnetBasedStatusOnPort             */
/*                                                                           */
/*    Description         : This function enable or disable the Subnet based */
/*                          Vlan classification on the port                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Interface Index                    */
/*                          u1SubnetBasedVlanEnable - FNP_TRUE/FNP_FALSE     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1SubnetBasedVlanEnable)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetSubnetBasedStatusOnPort (u4IfIndex,
                                                u1SubnetBasedVlanEnable));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwEnableProtoVlanOnPort                    */
/*                                                                           */
/*    Description         : This function enable or disable the  Port        */
/*                          Protocol based type on the port                  */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                           u1VlanProtoEnable - FNP_TRUE/FNP_FALSE          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1VlanProtoEnable)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwEnableProtoVlanOnPort (u4IfIndex, u1VlanProtoEnable);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetDefUserPriority                       */
/*                                                                           */
/*    Description         : This function set the default user priority to   */
/*                          port                                             */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                          i4DefPriority - default user priority            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                              INT4 i4DefPriority)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetDefUserPriority (u4IfIndex, i4DefPriority);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortNumTrafClasses                    */
/*                                                                           */
/*    Description         : This function set the number of traffic classes  */
/*                          for the port.                                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                          i4NumTraffClass - number of traffic classes      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 INT4 i4NumTraffClass)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetPortNumTrafClasses (u4IfIndex, i4NumTraffClass);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetRegenUserPriority                     */
/*                                                                           */
/*    Description         : This function overides the priority information  */
/*                          in the priority regeneration table maintained by */
/*                          each port for the user priority                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                         i4UserPriority  - user priority information       */
/*                         i4RegenPriority - regenerated user priority       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetRegenUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                INT4 i4UserPriority, INT4 i4RegenPriority)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetRegenUserPriority (u4IfIndex, i4UserPriority,
                                         i4RegenPriority);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetTraffClassMap                         */
/*                                                                           */
/*    Description         : This function set the traffic class for which    */
/*                          user priority of the port should map into        */
/*    Input(s)            : u2IfIndex               - The Interface Index    */
/*                          i4UserPriority  - user priority                  */
/*                          i4TraffClass - traffic class to be mapped        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetTraffClassMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                            INT4 i4UserPriority, INT4 i4TraffClass)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetTraffClassMap (u4IfIndex, i4UserPriority, i4TraffClass);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGvrpEnable                             */
/*                                                                           */
/*    Description         : This function will be used to enable GVRP in H/W */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGvrpEnable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGvrpEnable ();
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGvrpDisable                            */
/*                                                                           */
/*    Description         : This function will be used to disable GVRP in    */
/*                          H/W                                              */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGvrpDisable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGvrpDisable ();
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGmrpEnable                               */
/*                                                                           */
/*    Description         : This function will be used to enable GMRP in H/W */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGmrpEnable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGmrpEnable ();
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGmrpDisable                              */
/*                                                                           */
/*    Description         : This function will be used to disable GMRP in    */
/*                          H/W                                              */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGmrpDisable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGmrpDisable ();
}

/*****************************************************************************/
/*    Function Name       : FsMiNpDeleteAllFdbEntries                          */
/*                                                                           */
/*    Description         : This function flushes all entres in FDB table    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiNpDeleteAllFdbEntries (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    FsNpDeleteAllFdbEntries ();
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddVlanProtocolMap                       */
/*                                                                           */
/*    Description         : This function maps the valid and existence group */
/*                          ID to Vlan ID                                    */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u4GroupId  - Group Id                            */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          tVlanId - VLAN Id                                */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwAddVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 u4GroupId,
                              tVlanProtoTemplate * pProtoTemplate,
                              tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwAddVlanProtocolMap (u4IfIndex, u4GroupId, pProtoTemplate,
                                       VlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelVlanProtocolMap                       */
/*                                                                           */
/*    Description         : This function deletes the mapped VlanId  from    */
/*                          protocol group                                   */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          u4GroupId  - Group Id                            */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwDelVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 u4GroupId,
                              tVlanProtoTemplate * pProtoTemplate)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDelVlanProtocolMap (u4IfIndex, u4GroupId, pProtoTemplate);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanHwGetPortStats                             */
/*                                                                           */
/*    Description         : This function gets the statistics value for the  */
/*                          given statistics type for a port on a vlan       */
/*                                                                           */
/*    Input(s)            : u4Port     - The Port Number                     */
/*                          VlanId     - Vlan Id                             */
/*                          StatsType  - Statistics type                     */
/*                                                                           */
/*    Output(s)           : pu4PortStatsValue - Pointer to the               */
/*                          Statistics value                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanHwGetPortStats (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                        UINT1 u1StatsType, UINT4 *pu4PortStatsValue)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetPortStats (u4Port, VlanId, u1StatsType,
                                 pu4PortStatsValue);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanHwGetPortStats64                           */
/*                                                                           */
/*    Description         : This function gets the statistics value for the  */
/*                          given statistics type for a port on a vlan       */
/*                                                                           */
/*    Input(s)            : u4Port     - The Port Number                     */
/*                          VlanId     - Vlan Id                             */
/*                          StatsType  - Statistics type                     */
/*                                                                           */
/*    Output(s)           : pValue - Pointer to the Statistics value         */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanHwGetPortStats64 (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                          UINT1 u1StatsType, tSNMP_COUNTER64_TYPE * pValue)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetPortStats64 (u4Port, VlanId, u1StatsType, pValue);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortTunnelMode                        */
/*                                                                           */
/*    Description         : This function sets the tunnel mode of a port.    */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u4Mode    - Tunnel mode of a port.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortTunnelMode (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4Mode)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetPortTunnelMode (u4IfIndex, u4Mode);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetTunnelFilter                          */
/*                                                                           */
/*    Description         : This function creates/deletes h/w filters for    */
/*                          Provider Gvrp/Gmrp/STP Macaddress. This enables  */
/*                          the tunnel PDUs to be switched by software.      */
/*                                                                           */
/*    Input(s)            : i4BridgeMode - Bridge Mode                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanProviderGmrpAddr, gVlanProviderStpAddr*/
/*                                gVlanProviderGvrpAddr                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetTunnelFilter (i4BridgeMode);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwCheckTagAtEgress                         */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the exlusion of the provider VLAN   */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwCheckTagAtEgress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwCheckTagAtEgress (pu1TagSet);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwCheckTagAtEgress                         */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the inclusion of the provider VLAN  */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwCheckTagAtIngress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwCheckTagAtIngress (pu1TagSet);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwCreateFdbId                              */
/*                                                                           */
/*    Description         : This function will be used to create an Fid      */
/*                          entry in H/W.                                    */
/*                          If the FID is already created in the hardware,   */
/*                          the function should return success.              */
/*                                                                           */
/*    Input(s)            : u4Fid - Fdb Id                                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwCreateFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwCreateFdbId (u4Fid);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeleteFdbId                              */
/*                                                                           */
/*    Description         : This function will be used to delete an Fid      */
/*                          entry in H/W.                                    */
/*                                                                           */
/*    Input(s)            : u4Fid - FDB Id                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwDeleteFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDeleteFdbId (u4Fid);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAssociateVlanFdb                         */
/*                                                                           */
/*    Description         : This function will be used to associate a VlanId */
/*                          with an Fid in H/w.                              */
/*                                                                           */
/*    Input(s)            : u4Fid - FDB Id.                                  */
/*                          VlanId - Vlan Identifier.                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwAssociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwAssociateVlanFdb (u4Fid, VlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDisassociateVlanFdb                      */
/*                                                                           */
/*    Description         : This function will be used to disassociate a     */
/*                          VlanId from a Fid in H/W                         */
/*                                                                           */
/*    Input(s)            : u4Fid - FDB ID.                                  */
/*                          VlanId - VlanIdentifier.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwDisassociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwDisassociateVlanFdb (u4Fid, VlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushPortFdbId                          */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4Port               - The Interface Index    */
/*                          u4Fid   - Fdb Id in which the Entry has to be    */
/*                                   flushed.                                */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushPortFdbId (UINT4 u4ContextId, UINT4 u4Port, UINT4 u4Fid,
                          INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwFlushPortFdbId (u4Port, u4Fid, i4OptimizeFlag);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushPortFdbList                       */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4Port               - The Interface Index       */
/*                          u4Fid   - Fdb List;Entries have to be            */
/*                                   flushed.                                */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushPortFdbList (UINT4 u4ContextId, tVlanFlushInfo * pVlanFlushInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwFlushPortFdbList (pVlanFlushInfo);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushPort                                */
/*                                                                           */
/*    Description         : This function flushes the FDB entries            */
/*                          learnt on this port. Flushing                    */
/*                          is done based on port in all  FDB Table          */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushPort (UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwFlushPort (u4IfIndex, i4OptimizeFlag);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushFdbId                                */
/*                                                                           */
/*    Description         : This function flushes all the learnt FDB entries */
/*                          in the FDB table                                 */
/*                                                                           */
/*    Input(s)            : u4Fid               - The FDB ID                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwFlushFdbId (u4Fid);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : FsMiVlanHwSetShortAgeout                               */
/*                                                                           */
/*    Description     : This function sets the short age out time for        */
/*                      this port.                                           */
/*                                                                           */
/*    Input(s)        : u4Port - port number                                 */
/*                      i4AgingTime - Short ageout time to be set            */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetShortAgeout (UINT4 u4ContextId, UINT4 u4PortNum, INT4 i4AgingTime)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSetShortAgeout (u4PortNum, i4AgingTime);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : FsMiVlanHwResetShortAgeout                             */
/*                                                                           */
/*    Description     : This function resets the short age out time and      */
/*                      reverts back to the long ageout for this port.       */
/*                                                                           */
/*    Input(s)        : u4Port - port number                                 */
/*                      i4LongAgeout - Long ageout time to be reverted to    */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetShortAgeout (UINT4 u4ContextId, UINT4 u4PortNum,
                            INT4 i4LongAgeout)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwResetShortAgeout (u4PortNum, i4LongAgeout);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetVlanInfo                          */
/*                                                                           */
/*    Description         : This function returns the VLAN membership of the */
/*                          given VLAN in the hardware if it is present.     */
/*                          The VLAN membership returned should not include  */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN id for which the info needs to be  */
/*                          retrieved.                                       */
/*                                                                           */
/*    Output(s)           : pHwEntry - VLAN info pointer.                    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                         tHwMiVlanEntry * pHwEntry)
{
    tHwVlanEntry        HwVlanEntry;
    INT4                i4RetVal = FNP_FAILURE;

    UNUSED_PARAM (u4ContextId);

    MEMSET (&HwVlanEntry, 0, sizeof (tHwVlanEntry));

    HwVlanEntry.pHwMemberPbmp =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (HwVlanEntry.pHwMemberPbmp == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        return i4RetVal;
    }

    HwVlanEntry.pHwUntagPbmp =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (HwVlanEntry.pHwUntagPbmp == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList (*(HwVlanEntry.pHwMemberPbmp));
        return i4RetVal;
    }

    HwVlanEntry.pHwFwdAllPbmp =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (HwVlanEntry.pHwFwdAllPbmp == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList (*(HwVlanEntry.pHwMemberPbmp));
        FsUtilReleaseBitList (*(HwVlanEntry.pHwUntagPbmp));
        return i4RetVal;
    }

    HwVlanEntry.pHwFwdUnregPbmp =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (HwVlanEntry.pHwFwdUnregPbmp == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList (*(HwVlanEntry.pHwMemberPbmp));
        FsUtilReleaseBitList (*(HwVlanEntry.pHwUntagPbmp));
        FsUtilReleaseBitList (*(HwVlanEntry.pHwFwdAllPbmp));
        return i4RetVal;
    }

    i4RetVal = FsVlanHwGetVlanInfo (VlanId, &HwVlanEntry);

    if (i4RetVal == FNP_SUCCESS)
    {
        MEMCPY (pHwEntry, &HwVlanEntry, sizeof (tHwMiVlanEntry));
    }

    FsUtilReleaseBitList (*(HwVlanEntry.pHwMemberPbmp));
    FsUtilReleaseBitList (*(HwVlanEntry.pHwUntagPbmp));
    FsUtilReleaseBitList (*(HwVlanEntry.pHwFwdAllPbmp));
    FsUtilReleaseBitList (*(HwVlanEntry.pHwFwdUnregPbmp));
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetMcastEntry                        */
/*                                                                           */
/*    Description         : This function gets the given multicast entry     */
/*                          present in the hardware.                         */
/*                          The MCAST membership returned should not include */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                                                                           */
/*    Output(s)           : HwPortList - Hardware port list.                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           tPortList HwPortList)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetMcastEntry (VlanId, MacAddr, HwPortList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanHwTraffClassMapInit                        */
/*                                                                           */
/*    Description         : This function sets which cosq a given priority   */
/*                          should fall into in all units.                   */
/*                                                                           */
/*    Input(s)            : u1Priority - Cosq Priority                       */
/*                          i4CosqValue - Cosq Value                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority,
                             INT4 i4CosqValue)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwTraffClassMapInit (u1Priority, i4CosqValue);
}

/*******************************************************************************
* FsMiBrgSetAgingTime
*
* DESCRIPTION:
* Set the FDB aging time value for the given context. This will be called either 
* during the STAP operation during reconfiguration to time out entries learned 
* during this period. Also can be used to set aging time other than default value 
* 300secs supported by the 802.1D/802.1Q standard. 
*
* INPUTS:
* u4ContextId - context Identifier
* i4Aging - Aging Time
* OUTPUTS:
* None.
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
* None 

*
*******************************************************************************/
INT1
FsMiBrgSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime)
{
    UNUSED_PARAM (u4ContextId);
    return (FsBrgSetAgingTime (i4AgingTime));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetBrgMode                             */
/*                                                                           */
/*    Description         : This function sets the bridge mode in hardware.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4BridgeMode - VLAN_CUSTOMER_BRIDGE_MODE /       */
/*                          VLAN_PROVIDER_BRIDGE_MODE /                      */
/*                          VLAN_PROVIDER_EDGE_BRIDGE_MODE /                 */
/*                          VLAN_PROVIDER_CORE_BRIDGE_MODE                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsMiVlanHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanHwSetBrgMode (u4BridgeMode);
}

/****************************************************************************
 Function    :  FsMiVlanHwGetVlanStats

 Description :  Clears the VLAN statistics in the hardware

 Input       :  u4ContextId - Context Id
                VlanId - Vlan Id
                u1StatsType - Type of statistics required

 Output      :  pu4VlanStatsValue - returned statistics value

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsMiVlanHwGetVlanStats (UINT4 u4ContextId, tVlanId VlanId,
                        UINT1 u1StatsType, UINT4 *pu4VlanStatsValue)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetVlanStats (VlanId, u1StatsType, pu4VlanStatsValue);
}

/****************************************************************************
 Function    :  FsMiVlanHwResetVlanStats

 Description :  Clears the VLAN statistics in the hardware

 Input       :  u4ContextId - Context Id
                VlanId - Vlan Id

 Output      :  None

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsMiVlanHwResetVlanStats (UINT4 u4ContextId, tVlanId VlanId)
{

    UNUSED_PARAM (u4ContextId);
    return FsVlanHwResetVlanStats (VlanId);
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanHwMacLearningStatus                        */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a VLAN.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier.                           */
/*                      u2FdbId  - FDB Identifier.                           */
/*                      HwPortList - Static member port list.                */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On SUCCESS                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiWrVlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId,
                               tPortList HwPortList, UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwMacLearningStatus (VlanId, u2FdbId, HwPortList, u1Status);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwPortMacLearningStatus.                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwPortMacLearningStatus (u4IfIndex, u1Status));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwMacLearningLimit                           */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a VLAN.                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier.                           */
/*                      u2FdbId  - FDB Identifier.                           */
/*                      u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On SUCCESS                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                            UINT2 u2FdbId, UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwMacLearningLimit (VlanId, u2FdbId, u4MacLimit);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSwitchMacLearningLimit                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a Virtual Context.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On SUCCESS                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwSwitchMacLearningLimit (u4MacLimit);
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwScanProtocolVlanTbl                      */
/*                                                                           */
/*    Description         : This function scans the protocol VLAN table and  */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : ProtoVlanCallBack - Protocol VLAN call back      */
/*                          function pointer.                                */
/*                          u4Port - Port for which the scan needs to be     */
/*                          performed.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwScanProtocolVlanTbl (UINT4 u4ContextId, UINT4 u4Port,
                               FsMiVlanHwProtoCb ProtoVlanCallBack)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwScanProtocolVlanTbl (u4Port, ProtoVlanCallBack);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetVlanProtocolMap                       */
/*                                                                           */
/*    Description         : This function returns the VLAN id for which the  */
/*                          protocol group is mapped.                        */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u4GroupId  - Group Id                            */
/*                          pProtoTemplate - pointer to protocol template    */
/*                                                                           */
/*    Output(s)           : pVlanId - Pointer to the VLAN ID.                */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGetVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 u4GroupId,
                              tVlanProtoTemplate * pProtoTemplate,
                              tVlanId * pVlanId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetVlanProtocolMap (u4IfIndex, u4GroupId, pProtoTemplate,
                                       pVlanId);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwScanMulticastTbl                       */
/*                                                                           */
/*    Description         : This function scans the VLAN multicast table and */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : McastCallBack - Multicast call back function     */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwScanMulticastTbl (UINT4 u4ContextId, FsMiVlanHwMcastCb McastCallBack)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwScanMulticastTbl (McastCallBack);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetStMcastEntry                      */
/*                                                                           */
/*    Description         : This function gets the given static mcast entry  */
/*                          present in the hardware.                         */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                          u4RcvPort - Receive port.                        */
/*                          HwPortList - Hardware port list.                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, UINT4 u4RcvPort,
                             tPortList HwPortList)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetStMcastEntry (VlanId, MacAddr, u4RcvPort, HwPortList);
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwScanUnicastTbl                         */
/*                                                                           */
/*    Description         : This function scans the VLAN unicast table and   */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : UcastCallBack - Multicast call back function     */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwScanUnicastTbl (UINT4 u4ContextId, FsMiVlanHwUcastCb UcastCallBack)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwScanUnicastTbl (UcastCallBack);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetStaticUcastEntry                  */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : AllowedToGoPortBmp - PortList                    */
/*                          pu1Status        -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                 tMacAddr MacAddr, UINT4 u4Port,
                                 tPortList AllowedToGoPortBmp, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwGetStaticUcastEntry (u4Fid, MacAddr, u4Port,
                                        AllowedToGoPortBmp, pu1Status);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetSyncedTnlProtocolMacAddr                */
/*                                                                           */
/* Description        : This function validates filter mac address with sync */
/*                      mac address.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwGetSyncedTnlProtocolMacAddr (UINT4 u4ContextId, UINT2 u2Protocol,
                                       tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwGetSyncedTnlProtocolMacAddr (u2Protocol, MacAddr));
}

#endif /* L2RED_WANTED */

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddPortMacVlanEntry                   */
/*                                                                           */
/*    Description         : This function adds the MAC based VLAN entry in   */
/*                          the H/W table.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          MacAddr     - Mac Address.                       */
/*                          VlanId      - Vlan Identifier                    */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddPortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tMacAddr MacAddr, tVlanId VlanId,
                               BOOL1 bSuppressOption)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (bSuppressOption);
    return (FsVlanHwAddPortMacVlanEntry (u4IfIndex, MacAddr, VlanId));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeletePortMacVlanEntry                */
/*                                                                           */
/*    Description         : This function deletes the MAC based VLAN entry in*/
/*                          the H/W table.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          MacAddr     - Mac Address.                       */
/*                          VlanId      - Vlan Identifier                    */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDeletePortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwDeletePortMacVlanEntry (u4IfIndex, MacAddr));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddPortSubnetVlanEntry                 */
/*                                                                           */
/*    Description         : This function adds the Subnet based VLAN entry in*/
/*                          the H/W table.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          SubnetAddr     - Subnet Address.                 */
/*                          VlanId      - Vlan Identifier                    */
/*                          bARPOption - Option to enable/suppress ARPtraffic*/
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT4 SubnetAddr, tVlanId VlanId,
                                  BOOL1 bARPOption)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwAddPortSubnetVlanEntry (u4IfIndex, SubnetAddr,
                                            VlanId, bARPOption));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeletePortSubnetVlanEntry              */
/*                                                                           */
/*    Description         : This function deletes the Subnet based VLAN entry*/
/*                          in the H/W table.                                */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          SubnetAddr     - Subnet Address.                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDeletePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT4 SubnetAddr)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwDeletePortSubnetVlanEntry (u4IfIndex, SubnetAddr));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanNpHwRunMacAgeing                         */
/*                                                                           */
/*    Description         : The function initiates the "hardware FDB entries"*/
/*                          ageing process when a timer expiry event is      */
/*                          received for a given context ID                  */
/*                                                                           */
/*    Input(s)            : u4ContextId      - Virtual Switch ID             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanNpHwRunMacAgeing (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanNpHwRunMacAgeing ();
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetFidPortLearningStatus             */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward All Group Port list for the specified    */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                      tPortList PortBmp, UINT4 u4Port,
                                      UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanHwSetFidPortLearningStatus (u4Fid, PortBmp, u4Port, u1Action);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortProtectedStatus                     */
/*                                                                           */
/* Description        : This function is called for configuring the          */
/*                      Protected/Split Horizon Status on port.              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex - Interface index.                         */
/*                      i4ProtectedStatus - Port's protected status          */
/*                          VLAN_SNMP_TRUE - protected status enabled.       */
/*                          VLAN_SNMP_FALSE - protected status disabled.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  INT4 i4ProtectedStatus)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwSetPortProtectedStatus (u4IfIndex, i4ProtectedStatus));

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetProtocolTunnelStatusOnPort              */
/*                                                                           */
/* Description        : This function is called for configuring the action   */
/*                      to be taken on receiving control protocol PDUs on    */
/*                      the given port in the given virtual context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex   - Interface index                        */
/*                      ProtocolId  - Control Protocol for which action      */
/*                                    is being configured.                   */
/*                      u4TunnelStatus - Tunnel status                       */
/*                         VLAN_TUNNEL_PROTOCOL_PEER    - Normal             */
/*                         VLAN_TUNNEL_PROTOCOL_TUNNEL  - Encapsulate        */
/*                         VLAN_TUNNEL_PROTOCOL_DISCARD - Discard            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         tVlanHwTunnelFilters ProtocolId,
                                         UINT4 u4TunnelStatus)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwSetProtocolTunnelStatusOnPort (u4IfIndex, ProtocolId,
                                                   u4TunnelStatus));
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name       : FsMiVlanRedHwUpdateDBForDefaultVlanId               */
/*                                                                           */
/*                                                                           */
/* Input(s)            : u4ContextId - Context Identifier                    */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanRedHwUpdateDBForDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanRedHwUpdateDBForDefaultVlanId (VlanId));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSyncDefaultVlanId                      */
/*                                                                           */
/*    Description         : This function gets the default Vlan Id configured*/
/*                          in hardware                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSyncDefaultVlanId (UINT4 u4Context, tVlanId VlanId)
{
    UNUSED_PARAM (u4Context);
    return (FsVlanHwSyncDefaultVlanId (VlanId));
}
#endif

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetUnRegGroupsPortsForSisp           */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId                                  */
/*                          PortBmp  - PortList.                             */
/*                          u4PortId - Destination port id                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetUnRegGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                        tPortList PortBmp, UINT4 u4PortId)
{
    tPortList           UnRegGroupBmp;

    UNUSED_PARAM (u4ContextId);

    MEMCPY (UnRegGroupBmp, PortBmp, sizeof (tPortList));

    VLAN_SET_MEMBER_PORT (UnRegGroupBmp, u4PortId);

    return (FsVlanHwSetUnRegGroupsPorts (VlanId, UnRegGroupBmp));
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetAllGroupsPortsForSisp             */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetAllGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                      tPortList PortBmp, UINT4 u4PortId)
{

    tPortList           AllGroupBmp;

    UNUSED_PARAM (u4ContextId);

    MEMCPY (AllGroupBmp, PortBmp, sizeof (tPortList));

    VLAN_SET_MEMBER_PORT (AllGroupBmp, u4PortId);

    return (FsVlanHwSetAllGroupsPorts (VlanId, AllGroupBmp));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiWrVlanHwResetDefGroupInfoForPort             */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          reset the default group information.             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          VlanId - Vlan identifier number                  */
/*                          u4IfIndex - Interface Index                      */
/*                          u1Type  - All Groups / Unreg Groups              */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiWrVlanHwResetDefGroupInfoForPort (UINT4 u4ContextId, tVlanId VlanId,
                                      UINT4 u4IfIndex, UINT1 u1Type)
{
    tPortList           PortBmp;

    UNUSED_PARAM (u4ContextId);

    MEMSET (PortBmp, 0, sizeof (tPortList));

    VLAN_SET_MEMBER_PORT (PortBmp, u4IfIndex) if (u1Type == VLAN_ALL_GROUPS)
    {
        if (FsVlanHwResetAllGroupsPorts (VlanId, PortBmp) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
    else
    {
        if (FsVlanHwResetUnRegGroupsPorts (VlanId, PortBmp) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwEvbConfigSChIface                           */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : pVlanEvbHwConfigInfo - Vlan EVB hardware             */
/*                                             configuration information     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiWrVlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo)

{
    return (FsMiVlanHwEvbConfigSChIface (pVlanEvbHwConfigInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetBridgePortType                           */
/*                                                                           */
/* Description        : This function is called when the Bridge Port
                        Type is configured for a port.                       */
/*                                                                           */
/* Input(s)           : tVlanHwPortInfo - UAP Port Info                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiWrVlanHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo)
{
    return (FsMiVlanHwSetBridgePortType (pVlanHwPortInfo));
}

#ifdef MBSM_WANTED

/*****************************************************************************/
/* Function Name       : FsMiVlanMbsmHwInit                                  */
/*                                                                           */
/* Description         : This function takes care of initialising the        */
/*                       hardware related parameters.                        */
/*                                                                           */
/* Input(s)            : u4ContextId - Context Identifier                    */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwInit (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwInit (pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetBrgMode                         */
/*                                                                           */
/*    Description         : This function sets the bridge mode in hardware.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4BridgeMode - VLAN_CUSTOMER_BRIDGE_MODE /       */
/*                          VLAN_PROVIDER_BRIDGE_MODE /                      */
/*                          VLAN_PROVIDER_EDGE_BRIDGE_MODE /                 */
/*                          VLAN_PROVIDER_CORE_BRIDGE_MODE                   */
/*                          pSlotInfo - slot information                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanMbsmHwSetBrgMode (u4BridgeMode, pSlotInfo));
}

/*****************************************************************************/
/* Function Name       : FsMiVlanMbsmHwSetDefaultVlanId                      */
/*                                                                           */
/* Description         : This function takes care of setting defaut vlan id 
 *                       in h/w                                              */
/*                                                                           */
/* Input(s)            : u4ContextId - Context Identifier                    */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetDefaultVlanId (VlanId, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiBrgMbsmSetAgingTime                          */
/*                                                                           */
/*    Description         : Set the FDB aging time value for the given       */
/*                          context. This will be called either during the   */
/*                          STAP operation during reconfiguration to time-   */
/*                          out entries learned during this period. Also     */
/*                          can be used to set aging time other than default */
/*                          value 300secs supported by the 802.1D/802.1Q     */
/*                          standard.                                        */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context Identifier                */
/*                          i4AgingTime  - Aging Time                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT1
FsMiBrgMbsmSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsBrgMbsmSetAgingTime (i4AgingTime, pSlotInfo));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwTraffClassMapInit                  */
/*                                                                           */
/*    Description         : This function sets which cosq a given priority   */
/*                          should fall into in the given unit.              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u1Priority  - Cosq Priority                      */
/*                          i4CosqValue - Cosq Value                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority,
                                 INT4 i4CosqValue, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwTraffClassMapInit (u1Priority, i4CosqValue, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetTunnelFilter                    */
/*                                                                           */
/*    Description         : This function creates/deletes h/w filters for    */
/*                          Provider Gvrp/Gmrp/STP Macaddress. This enables  */
/*                          the tunnel PDUs to be switched by software.      */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context Identifier                */
/*                          i4BridgeMode - Bridge Mode                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode,
                               tMacAddr MacAddr, UINT2 u2Protocol,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetTunnelFilter (i4BridgeMode, MacAddr, u2Protocol,
                                         pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddVlanEntry                     */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - Vlan Index                         */
/*                          PortBmp     - Egress Portlist                    */
/*                          UnTagPortBmp- Untagged Portlist                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                              tPortList PortBmp, tPortList UnTagPortBmp,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwAddVlanEntry (VlanId, PortBmp,
                                      UnTagPortBmp, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddStaticUcastEntry              */
/*                                                                           */
/*    Description         : This function adds a static unicast entry to the */
/*                          hardware table                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId        - Context Identifier          */
/*                          u4Fid              - FID                         */
/*                          MacAddr            - Mac Address.                */
/*                          i4RcvPort          - Received port               */
/*                          AllowedToGoPortBmp - List of Ports.              */
/*                          u1Status           - Status of entry takes       */
/*                          values like  permanent, deleteOnReset and        */
/*                          deleteOnTimeout.                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                     tMacAddr MacAddr, UINT4 u4Port,
                                     tPortList AllowedToGoPortBmp,
                                     UINT1 u1Status, tMbsmSlotInfo * pSlotInfo,
                                     tMacAddr ConnectionId)
{
    UNUSED_PARAM (u4ContextId);

    if (VLAN_ARE_MAC_ADDR_EQUAL (ConnectionId, gNullMacAddress) == VLAN_TRUE)
    {
        return (FsVlanMbsmHwAddStaticUcastEntry (u4Fid, MacAddr, u4Port,
                                                 AllowedToGoPortBmp, u1Status,
                                                 pSlotInfo));
    }
    else
    {
        return (FsVlanMbsmHwAddStaticUcastEntryEx (u4Fid, MacAddr, u4Port,
                                                   AllowedToGoPortBmp, u1Status,
                                                   ConnectionId, pSlotInfo));
    }
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddStMcastEntry                  */
/*                                                                           */
/*    Description         : This function adds a static entry to the         */
/*                          hardware Multicast table                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          i4RcvPort   - Received port                      */
/*                          PortBmp     - List of Ports.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                                 tMacAddr MacAddr, INT4 i4RcvPort,
                                 tPortList PortBmp, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwAddStMcastEntry (VlanId, MacAddr, i4RcvPort,
                                         PortBmp, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwDelStMcastEntry                    */
/*                                                                           */
/*    Description         : This function deletes a static mcast entry from  */
/*                          the hardware Multicast table.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId   - Context Identifier               */
/*                          VlanId        - VlanId                           */
/*                          MacAddr       - Mac Address.                     */
/*                          u4RcvPort     - Receiver port.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwDelStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, UINT4 u4RcvPort,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwDelStMcastEntry (VlanId, MacAddr, u4RcvPort, pSlotInfo);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddMcastEntry                    */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          PortBmp     - List of Ports.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, tPortList PortBmp,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwAddMcastEntry (VlanId, MacAddr, PortBmp, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwSetAllGroupsPorts                */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          PortBmp     - PortList                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tPortList PortBmp, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetAllGroupsPorts (VlanId, PortBmp, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwSetUnRegGroupsPorts              */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          PortBmp     - PortList.                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                     tPortList PortBmp,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetUnRegGroupsPorts (VlanId, PortBmp, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortPvid                        */
/*                                                                           */
/*    Description         : This function sets the given VlanId as PortPvid  */
/*                          for the Port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u4Port      - The port number                    */
/*                          VlanId      - Vlan to which the port is going to */
/*                                        be an member                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortPvid (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortPvid (u4Port, VlanId, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortAccFrameType                */
/*                                                                           */
/*    Description         : This function sets the Acceptable frame types    */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId    - Context Identifier              */
/*                          u4Port         - The port number                 */
/*                          u1AccFrameType - Acceptable Frame types          */
/*                                           parameter value.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetPortAccFrameType (UINT4 u4ContextId, UINT4 u4Port,
                                   UINT1 u1AccFrameType,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortAccFrameType (u4Port, u1AccFrameType,
                                             pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortIngFiltering                */
/*                                                                           */
/*    Description         : This function sets the Ingress Filtering         */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId       - Context Identifier           */
/*                          u4Port            - The port number              */
/*                          u1IngFilterEnable - FNP_TRUE if Ingress          */
/*                                              filtering enabled, otherwise */
/*                                              FNP_FALSE                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortIngFiltering (UINT4 u4ContextId, UINT4 u4Port,
                                   UINT1 u1IngFilterEnable,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortIngFiltering (u4Port, u1IngFilterEnable,
                                             pSlotInfo));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwSetDefUserPriority                 */
/*                                                                           */
/*    Description         : This function set the default user priority to   */
/*                          port                                             */
/*                                                                           */
/*    Input(s)            : u4ContextId   - Context Identifier               */
/*                          u4Port        - The port number                  */
/*                          i4DefPriority - default user priority            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetDefUserPriority (UINT4 u4ContextId, UINT4 u4Port,
                                  INT4 i4DefPriority, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetDefUserPriority (u4Port, i4DefPriority, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortNumTrafClasses              */
/*                                                                           */
/*    Description         : This function set the number of traffic classes  */
/*                          for the port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId     - Context Identifier             */
/*                          u4Port          - logical port number            */
/*                          i4NumTraffClass - number of traffic classes      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT4 u4Port,
                                     INT4 i4NumTraffClass,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortNumTrafClasses (u4Port, i4NumTraffClass,
                                               pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetRegenUserPriority               */
/*                                                                           */
/*    Description         : This function overides the priority information  */
/*                          in the priority regeneration table maintained by */
/*                          each port for the user priority                  */
/*                                                                           */
/*    Input(s)            : u4ContextId     - Context Identifier             */
/*                          u4Port          - logical port number            */
/*                          i4UserPriority  - user priority information      */
/*                          i4RegenPriority - regenerated user priority      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetRegenUserPriority (UINT4 u4ContextId, UINT4 u4Port,
                                    INT4 i4UserPriority,
                                    INT4 i4RegenPriority,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetRegenUserPriority (u4Port, i4UserPriority,
                                              i4RegenPriority, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetTraffClassMap                   */
/*                                                                           */
/*    Description         : This function set the traffic class for which    */
/*                          user priority of the port should map into        */
/*                                                                           */
/*    Input(s)            : u4ContextId     - Context Identifier             */
/*                          u4Port          - logical port number            */
/*                          i4UserPriority  - user priority                  */
/*                          i4TraffClass    - traffic class to be mapped     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetTraffClassMap (UINT4 u4ContextId, UINT4 u4Port,
                                INT4 i4UserPriority, INT4 i4TraffClass,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetTraffClassMap (u4Port, i4UserPriority,
                                          i4TraffClass, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwEnableProtoVlanOnPort              */
/*                                                                           */
/*    Description         : This function enable or disable the  Port        */
/*                          Protocol based type on the port                  */
/*                                                                           */
/*    Input(s)            : u4ContextId       - Context Identifier           */
/*                          u4Port            - The Interface Index          */
/*                          u1VlanProtoEnable - FNP_TRUE/FNP_FALSE           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT4 u4Port,
                                     UINT1 u1VlanProtoEnable,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwEnableProtoVlanOnPort (u4Port, u1VlanProtoEnable,
                                               pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetVlanMemberPort                  */
/*                                                                           */
/*    Description         : This function adds the port as the member of     */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - Vlan to which the port is going to */
/*                                        be an member                       */
/*                          u4Port      - The port number                    */
/*                          u1IsTagged  - If value is VLAN_TRUE, then port   */
/*                                        is tagged port &                   */
/*                                        If value is VLAN_FALSE, then       */
/*                                        port is an untagged port           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                 UINT4 u4Port, UINT1 u1IsTagged,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetVlanMemberPort (VlanId, u4Port,
                                           u1IsTagged, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwResetVlanMemberPort                */
/*                                                                           */
/*    Description         : This function Reset the port as the member of    */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u4Port  - The port number                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                   UINT4 u4Port, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwResetVlanMemberPort (VlanId, u4Port, pSlotInfo));
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwDelStaticUcastEntry                */
/*                                                                           */
/*    Description         : This function deletes the entry from  hardware   */
/*                          Static Unicast table.                            */
/*                                                                           */
/*    Input(s)            : u4ContextId      - Context Identifier            */
/*                          u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4RcvPort        - Received port                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwDelStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4RcvPort,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwDelStaticUcastEntry (u4Fid, MacAddr, u4RcvPort,
                                             pSlotInfo));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwSetMcastPort                       */
/*                                                                           */
/*    Description         : This function adds a member port in              */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          u4Port      - Interface Index of member port to  */
/*                                        be added                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                            tMacAddr MacAddr, UINT4 u4Port,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetMcastPort (VlanId, MacAddr, u4Port, pSlotInfo));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwResetMcastPort                     */
/*                                                                           */
/*    Description         : This function resets a member port from the      */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          u4Port      - Interface Index of member port to  */
/*                                       be removed                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwResetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                              tMacAddr MacAddr, UINT4 u4Port,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwResetMcastPort (VlanId, MacAddr, u4Port, pSlotInfo));
}

/********************************************************************************/
/*                                                                              */
/* Function Name       : FsMiVlanMbsmHwSetUcastPort                             */
/*                                                                              */
/* Description         : This function adds the egress port in existing         */
/*                       Unicast entry or if the Unicast entry is not present   */
/*                       it will create it and update it's portlist             */
/*                                                                              */
/* Input(s)            : u4ContextId         - Context Identifier               */
/*                       u4FdbId             - Fid                              */
/*                       MacAddr             - Mac Address.                     */
/*                       u4RcvPort           - Received Interface Index         */
/*                       u4AllowedToGoPort   - Pointer to HwPortArray           */
/*                       u1Status            -  Status of entry takes values    */
/*                       like  permanent, deleteOnReset and                     */
/*                       deleteOnTimeout                                        */
/*                                                                              */
/* Output(s)           : None                                                   */
/*                                                                              */
/* Global Variables Referred : None                                             */
/*                                                                              */
/* Global Variables Modified : None.                                            */
/*                                                                              */
/* Exceptions or Operating                                                      */
/* System Error Handling    : None.                                             */
/*                                                                              */
/* Use of Recursion        : None.                                              */
/*                                                                              */
/* Returns            : FNP_SUCCESS/FNP_FAILURE                                 */
/********************************************************************************/
INT4
FsMiVlanMbsmHwSetUcastPort (UINT4 u4ContextId, UINT4 u4FdbId,
                            tMacAddr MacAddr, UINT4 u4RcvPort,
                            UINT4 u4AllowedToGoPort,
                            UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetUcastPort (u4FdbId, MacAddr, u4RcvPort,
                                      u4AllowedToGoPort, u1Status, pSlotInfo));
}

/********************************************************************************/
/*                                                                              */
/* Function Name       : FsMiVlanMbsmHwResetUcastPort                           */
/*                                                                              */
/* Description         : This function removes the egress port in existing      */
/*                       Unicast entry                                          */
/*                                                                              */
/* Input(s)            : u4ContextId         - Context Identifier               */
/*                       u4FdbId             - Fid                              */
/*                       MacAddr             - Mac Address.                     */
/*                       u4RcvPort           - Received Interface Index         */
/*                       u4AllowedToGoPort   - Port to be removed               */
/*                                                                              */
/* Output(s)           : None                                                   */
/*                                                                              */
/* Global Variables Referred : None                                             */
/*                                                                              */
/* Global Variables Modified : None.                                            */
/*                                                                              */
/* Exceptions or Operating                                                      */
/* System Error Handling    : None.                                             */
/*                                                                              */
/* Use of Recursion        : None.                                              */
/*                                                                              */
/* Returns            : FNP_SUCCESS/FNP_FAILURE                                 */
/********************************************************************************/
INT4
FsMiVlanMbsmHwResetUcastPort (UINT4 u4ContextId, UINT4 u4FdbId,
                              tMacAddr MacAddr, UINT4 u4RcvPort,
                              UINT4 u4AllowedToGoPort,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwResetUcastPort (u4FdbId, MacAddr, u4RcvPort,
                                        u4AllowedToGoPort, pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetFidPortLearningStatus               */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port in the given Fid.         */
/*                                                                           */
/*                                                                           */
/* Input (s)          : u4ContextId - Context Id                             */
/*                    : u4Fid       - Fdb Identifier                         */
/*                      should be applied                                    */
/*                      u4Port - Port where mac lreaning status will applied */
/*                               it will be used only if pHwPortList is null.*/
/*                      u1Action - learning enable/disable                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                        UINT4 u4Port, UINT1 u1Action,
                                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanMbsmHwSetFidPortLearningStatus (u4Fid, u4Port,
                                                  u1Action, pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort          */
/*                                                                           */
/* Description        : This function is called for configuring the action   */
/*                      to be taken on receiving control protocol PDUs on    */
/*                      the given port in the given virtual context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex   - Interface index                        */
/*                      ProtocolId  - Control Protocol for which action      */
/*                                    is being configured.                   */
/*                      u4TunnelStatus - Tunnel status                       */
/*                         VLAN_TUNNEL_PROTOCOL_PEER    - Normal             */
/*                         VLAN_TUNNEL_PROTOCOL_TUNNEL  - Encapsulate        */
/*                         VLAN_TUNNEL_PROTOCOL_DISCARD - Discard            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             tVlanHwTunnelFilters ProtocolId,
                                             UINT4 u4TunnelStatus,
                                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetProtocolTunnelStatusOnPort (u4ContextId,
                                                       u4IfIndex,
                                                       ProtocolId,
                                                       u4TunnelStatus,
                                                       pSlotInfo));

}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortProtectedStatus                 */
/*                                                                           */
/* Description        : This function is called for configuring the          */
/*                      Protected/Split Horizon Status on port.              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex - Interface index.                         */
/*                      u4ProtectedStatus - Port's protected status          */
/*                          VLAN_SNMP_TRUE - protected status enabled.       */
/*                          VLAN_SNMP_FALSE - protected status disabled.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      INT4 i4ProtectedStatus,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanMbsmHwSetPortProtectedStatus (u4ContextId,
                                                u4IfIndex,
                                                i4ProtectedStatus, pSlotInfo));

}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSwitchMacLearningLimit                 */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a Virtual Context.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On SUCCESS                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwSwitchMacLearningLimit (u4MacLimit, pSlotInfo);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwMacLearningLimit                       */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a VLAN in the given context.      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier.                           */
/*                      u2FdbId - Fdb Identifier                             */
/*                      u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                                UINT2 u2FdbId, UINT4 u4MacLimit,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwMacLearningLimit (VlanId, u2FdbId, u4MacLimit,
                                         pSlotInfo);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetStaticUcastEntryEx                */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : AllowedToGoPortBmp - PortList                    */
/*                          pu1Status        -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout                                 */
/*                            ConnectionId - Back bone Mac Address           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4Port,
                                   tPortList AllowedToGoPortBmp,
                                   UINT1 *pu1Status, tMacAddr ConnectionId)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanHwGetStaticUcastEntryEx (u4Fid, MacAddr, u4Port,
                                          AllowedToGoPortBmp, pu1Status,
                                          ConnectionId);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortIngressEtherType.                   */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT2 u2EtherType)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanHwSetPortIngressEtherType (u4IfIndex, u2EtherType);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortEgressEtherType.                      */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT2 u2EtherType)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanHwSetPortEgressEtherType (u4IfIndex, u2EtherType);
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanMbsmHwEvbConfigSChIface                      */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : pVlanEvbHwConfigInfo - Vlan EVB hardware             */
/*                                             configuration information     */
/*                      pSlotInfo            - MBSM information              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiWrVlanMbsmHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,
                                 tMbsmSlotInfo * pSlotInfo) 
{

    return(FsMiVlanMbsmHwEvbConfigSChIface(pVlanEvbHwConfigInfo,
                                      pSlotInfo));

}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanMbsmHwSetBridgePortType                      */
/*                                                                           */
/* Description        : This function is called when the Bridge Port
                        Type is configured for a port.                       */
/*                                                                           */
/* Input(s)           : tVlanHwPortInfo - UAP Port Info
                        tMbsmSlotInfo  - MBSM Slot Info                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiWrVlanMbsmHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo ,
                                     tMbsmSlotInfo * pSlotInfo)
{
    return (FsMiVlanMbsmHwSetBridgePortType (pVlanHwPortInfo,
                                        pSlotInfo));
}

#endif /* MBSM_WANTED */
