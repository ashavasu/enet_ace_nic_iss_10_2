/* 
   ISS Wrapper module
   module RSTP-MIB

*/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
#ifdef RSTP_WANTED
# include  "stdrslex.h"
#endif
# include  "stdbrinc.h"
# include  "mbsm.h"
# include  "fsvlan.h"
# include  "rstp.h"

/********************************************************************
 * FUNCTION NcDot1dStpProtocolSpecificationGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpProtocolSpecificationGet (
				INT4 *pi4Dot1dStpProtocolSpecification )
{

		INT1 i1RetVal;
#ifdef RSTP_WANTED
		i1RetVal = nmhGetDot1dStpProtocolSpecification(
						pi4Dot1dStpProtocolSpecification );
#else
		UNUSED_PARAM (pi4Dot1dStpProtocolSpecification);
#endif

		return i1RetVal;


} /* NcDot1dStpProtocolSpecificationGet */

/********************************************************************
 * FUNCTION NcDot1dStpPrioritySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpPrioritySet (
				INT4 i4Dot1dStpPriority )
{

		INT1 i1RetVal;
#ifdef RSTP_WANTED
		AST_LOCK ();
		i1RetVal = nmhSetDot1dStpPriority(
						i4Dot1dStpPriority);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpPriority);
#endif
		return i1RetVal;


} /* NcDot1dStpPrioritySet */

/********************************************************************
 * FUNCTION NcDot1dStpPriorityTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpPriorityTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpPriority )
{

		INT1 i1RetVal;
#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhTestv2Dot1dStpPriority(pu4Error,
						i4Dot1dStpPriority);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pu4Error);
#endif
		return i1RetVal;


} /* NcDot1dStpPriorityTest */

/********************************************************************
 * FUNCTION NcDot1dStpTimeSinceTopologyChangeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpTimeSinceTopologyChangeGet (
				UINT4 *pu4Dot1dStpTimeSinceTopologyChange )
{

		INT1 i1RetVal;
#ifdef RSTP_WANTED
		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpTimeSinceTopologyChange(
						pu4Dot1dStpTimeSinceTopologyChange );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pu4Dot1dStpTimeSinceTopologyChange);
#endif
		return i1RetVal;


} /* NcDot1dStpTimeSinceTopologyChangeGet */

/********************************************************************
 * FUNCTION NcDot1dStpTopChangesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpTopChangesGet (
				UINT4 *pu4Dot1dStpTopChanges )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpTopChanges(
						pu4Dot1dStpTopChanges );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pu4Dot1dStpTopChanges);

#endif

		return i1RetVal;


} /* NcDot1dStpTopChangesGet */

/********************************************************************
 * FUNCTION NcDot1dStpDesignatedRootGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpDesignatedRootGet (
				UINT1 *pau1Dot1dStpDesignatedRoot )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE Dot1dStpDesignatedRoot;
		MEMSET (&Dot1dStpDesignatedRoot,0, sizeof (tSNMP_OCTET_STRING_TYPE));
		Dot1dStpDesignatedRoot.pu1_OctetList = pau1Dot1dStpDesignatedRoot;
#ifdef RSTP_WANTED
		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpDesignatedRoot(
						&Dot1dStpDesignatedRoot );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pau1Dot1dStpDesignatedRoot);
#endif

		return i1RetVal;


} /* NcDot1dStpDesignatedRootGet */

/********************************************************************
 * FUNCTION NcDot1dStpRootCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpRootCostGet (
				INT4 *pi4Dot1dStpRootCost )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpRootCost(
						pi4Dot1dStpRootCost );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pi4Dot1dStpRootCost);
#endif
		return i1RetVal;


} /* NcDot1dStpRootCostGet */

/********************************************************************
 * FUNCTION NcDot1dStpRootPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpRootPortGet (
				INT4 *pi4Dot1dStpRootPort )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpRootPort(
						pi4Dot1dStpRootPort );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pi4Dot1dStpRootPort);
#endif
		return i1RetVal;


} /* NcDot1dStpRootPortGet */

/********************************************************************
 * FUNCTION NcDot1dStpMaxAgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpMaxAgeGet (
				INT4 *pi4Dot1dStpMaxAge )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpMaxAge(
						pi4Dot1dStpMaxAge );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pi4Dot1dStpMaxAge);
#endif
		return i1RetVal;


} /* NcDot1dStpMaxAgeGet */

/********************************************************************
 * FUNCTION NcDot1dStpHelloTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpHelloTimeGet (
				INT4 *pi4Dot1dStpHelloTime )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpHelloTime(
						pi4Dot1dStpHelloTime );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pi4Dot1dStpHelloTime);
#endif
		return i1RetVal;


} /* NcDot1dStpHelloTimeGet */

/********************************************************************
 * FUNCTION NcDot1dStpHoldTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpHoldTimeGet (
				INT4 *pi4Dot1dStpHoldTime )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpHoldTime(
						pi4Dot1dStpHoldTime );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pi4Dot1dStpHoldTime);
#endif
		return i1RetVal;


} /* NcDot1dStpHoldTimeGet */

/********************************************************************
 * FUNCTION NcDot1dStpForwardDelayGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpForwardDelayGet (
				INT4 *pi4Dot1dStpForwardDelay )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhGetDot1dStpForwardDelay(
						pi4Dot1dStpForwardDelay );
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pi4Dot1dStpForwardDelay);
#endif
		return i1RetVal;


} /* NcDot1dStpForwardDelayGet */

/********************************************************************
 * FUNCTION NcDot1dStpBridgeMaxAgeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpBridgeMaxAgeSet (
				INT4 i4Dot1dStpBridgeMaxAge )
{

		INT1 i1RetVal;
		if (i4Dot1dStpBridgeMaxAge == 0)
		{
				i4Dot1dStpBridgeMaxAge = (20 * CENTI_SECONDS);
		}

#ifdef RSTP_WANTED
		AST_LOCK ();

		i1RetVal = nmhSetDot1dStpBridgeMaxAge(
						i4Dot1dStpBridgeMaxAge);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpBridgeMaxAge);
#endif
		return i1RetVal;


} /* NcDot1dStpBridgeMaxAgeSet */

/********************************************************************
 * FUNCTION NcDot1dStpBridgeMaxAgeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpBridgeMaxAgeTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpBridgeMaxAge )
{

		INT1 i1RetVal;
		if (i4Dot1dStpBridgeMaxAge == 0)
		{
				i4Dot1dStpBridgeMaxAge = (20 * CENTI_SECONDS);
		}

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhTestv2Dot1dStpBridgeMaxAge(pu4Error,
						i4Dot1dStpBridgeMaxAge);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpBridgeMaxAge);
		UNUSED_PARAM (pu4Error);
#endif
		return i1RetVal;


} /* NcDot1dStpBridgeMaxAgeTest */

/********************************************************************
 * FUNCTION NcDot1dStpBridgeHelloTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpBridgeHelloTimeSet (
				INT4 i4Dot1dStpBridgeHelloTime )
{

		INT1 i1RetVal;
		if (i4Dot1dStpBridgeHelloTime == 0)
		{
				i4Dot1dStpBridgeHelloTime = (2 * CENTI_SECONDS);
		}

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhSetDot1dStpBridgeHelloTime(
						i4Dot1dStpBridgeHelloTime);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpBridgeHelloTime);
#endif
		return i1RetVal;


} /* NcDot1dStpBridgeHelloTimeSet */

/********************************************************************
 * FUNCTION NcDot1dStpBridgeHelloTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpBridgeHelloTimeTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpBridgeHelloTime )
{

		INT1 i1RetVal;
		if (i4Dot1dStpBridgeHelloTime == 0)
		{
				i4Dot1dStpBridgeHelloTime = (2 * CENTI_SECONDS);
		}

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhTestv2Dot1dStpBridgeHelloTime(pu4Error,
						i4Dot1dStpBridgeHelloTime);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpBridgeHelloTime);
		UNUSED_PARAM (pu4Error);
#endif
		return i1RetVal;


} /* NcDot1dStpBridgeHelloTimeTest */

/********************************************************************
 * FUNCTION NcDot1dStpBridgeForwardDelaySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpBridgeForwardDelaySet (
				INT4 i4Dot1dStpBridgeForwardDelay )
{

		INT1 i1RetVal;
		if (i4Dot1dStpBridgeForwardDelay == 0)
		{
				i4Dot1dStpBridgeForwardDelay = (15 * CENTI_SECONDS);
		}

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhSetDot1dStpBridgeForwardDelay(
						i4Dot1dStpBridgeForwardDelay);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpBridgeForwardDelay);
#endif
		return i1RetVal;


} /* NcDot1dStpBridgeForwardDelaySet */

/********************************************************************
 * FUNCTION NcDot1dStpBridgeForwardDelayTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpBridgeForwardDelayTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpBridgeForwardDelay )
{

		INT1 i1RetVal;
		if (i4Dot1dStpBridgeForwardDelay == 0)
		{
				i4Dot1dStpBridgeForwardDelay = (15 * CENTI_SECONDS);
		}

#ifdef RSTP_WANTED

		AST_LOCK ();
		i1RetVal = nmhTestv2Dot1dStpBridgeForwardDelay(pu4Error,
						i4Dot1dStpBridgeForwardDelay);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpBridgeForwardDelay);
		UNUSED_PARAM (pu4Error);
#endif
		return i1RetVal;


} /* NcDot1dStpBridgeForwardDelayTest */

/********************************************************************
 * FUNCTION NcDot1dStpVersionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpVersionSet (
				INT4 i4Dot1dStpVersion )
{

		INT1 i1RetVal;
#ifdef RSTP_WANTED
		AST_LOCK ();
		i1RetVal = nmhSetDot1dStpVersion(
						i4Dot1dStpVersion);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpVersion);
#endif
		return i1RetVal;


} /* NcDot1dStpVersionSet */

/********************************************************************
 * FUNCTION NcDot1dStpVersionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpVersionTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpVersion )
{

		INT1 i1RetVal;
#ifdef RSTP_WANTED
		AST_LOCK ();
		i1RetVal = nmhTestv2Dot1dStpVersion(pu4Error,
						i4Dot1dStpVersion);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pu4Error);
		UNUSED_PARAM (i4Dot1dStpVersion);
#endif
		return i1RetVal;


} /* NcDot1dStpVersionTest */

/********************************************************************
 * FUNCTION NcDot1dStpTxHoldCountSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpTxHoldCountSet (
				INT4 i4Dot1dStpTxHoldCount )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED
		AST_LOCK ();

		i1RetVal = nmhSetDot1dStpTxHoldCount(
						i4Dot1dStpTxHoldCount);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (i4Dot1dStpTxHoldCount);
#endif
		return i1RetVal;


} /* NcDot1dStpTxHoldCountSet */

/********************************************************************
 * FUNCTION NcDot1dStpTxHoldCountTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1dStpTxHoldCountTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpTxHoldCount )
{

		INT1 i1RetVal;

#ifdef RSTP_WANTED
		AST_LOCK ();

		i1RetVal = nmhTestv2Dot1dStpTxHoldCount(pu4Error,
						i4Dot1dStpTxHoldCount);
		AST_UNLOCK ();
#else
		UNUSED_PARAM (pu4Error);
		UNUSED_PARAM (i4Dot1dStpTxHoldCount);
#endif
		return i1RetVal;


} /* NcDot1dStpTxHoldCountTest */

/* END i_RSTP_MIB.c */
