/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlmpnpst.c,v 1.13 2013/12/05 12:45:01 siva Exp $
 *
 * Description: This file contains utility routines used in VLANmodule
 *              for provider bridges
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlmpnpst.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 14th July 2006                                   */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains Provider Bridge related MI    */
/*                          NPAPI wrappers. The finctions defined here call  */
/*                          the SI NPAPI functions. This file gets compiled  */
/*                          when NPAPI is YES, PB is YES and MI is NO */
/*****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "l2iwf.h"
#include "vlnpbnp.h"
#include "vlnmpbnp.h"
/*****************************************************************************/
/* Function Name      : FsMiVlanHwProviderBridgePortType.                    */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier             */
/*                      u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT4 u4PortType)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwSetProviderBridgePortType (u4IfIndex, u4PortType));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortSVlanTranslationStatus.             */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetPortSVlanTranslationStatus (u4IfIndex, u1Status));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwAddSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwAddSVlanTranslationEntry (u4IfIndex,
                                              u2LocalSVlan, u2RelaySVlan));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      from an VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDelSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwDelSVlanTranslationEntry (u4IfIndex,
                                              u2LocalSVlan, u2RelaySVlan));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortEtherTypeSwapStatus.                */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanHwSetPortEtherTypeSwapStatus (u4IfIndex, u1Status));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwAddEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet  */
/*                      u2RelayEtherType- Ether type understand by the bridge*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT2 u2LocalEtherType, UINT2 u2RelayEtherType)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwAddEtherTypeSwapEntry (u4IfIndex,
                                           u2LocalEtherType, u2RelayEtherType));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDelEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT2 u2LocalEtherType, UINT2 u2RelayEtherType)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwDelEtherTypeSwapEntry (u4IfIndex,
                                           u2LocalEtherType, u2RelayEtherType));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwAddSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                                                                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwAddSVlanMap (VlanSVlanMap));

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is deleted for */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDeleteSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwDeleteSVlanMap (VlanSVlanMap));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortSVlanClassifyMethod                 */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1TableType)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetPortSVlanClassifyMethod (u4IfIndex, u1TableType));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwPortMacLearningStatus.                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwPortMacLearningStatus (u4IfIndex, u1Status));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwPortMacLearningLimit.                      */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwPortMacLearningLimit (u4IfIndex, u4MacLimit));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwStaticMulticastMacTableLimit .             */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - Mac Limit of the Multicast Table        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwMulticastMacTableLimit (u4MacLimit));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortCustomerVlan.                       */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port IfIndex                             */
/*                      CVlanId - Configured Customer VLAN ID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId CVlanId)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetPortCustomerVlan (u4IfIndex, CVlanId));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwResetPortCustomerVlan.                     */
/*                                                                           */
/* Description        : This function is called for resetting the ports      */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port IfIndex                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwResetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwResetPortCustomerVlan (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwCreateProviderEdgePort                     */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id.                         */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId SVlanId, tHwVlanPbPepInfo PepConfig)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanHwCreateProviderEdgePort (u4IfIndex, SVlanId, PepConfig);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelProviderEdgePort                        */
/*                                                                           */
/* Description        : This function deletes the Provider Edge Port from    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDelProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId SVlanId)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwDelProviderEdgePort (u4IfIndex, SVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepPvid                                 */
/*                                                                           */
/* Description        : This function sets the PVID for the given Provider   */
/*                      Edge Port in hardware.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      Pvid       - PVID to be set.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                      tVlanId Pvid)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPepPvid (u4IfIndex, SVlanId, Pvid);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepAccFrameType                           */
/*                                                                           */
/* Description        : This function sets the Acceptable frame type for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      u1AccepFrameType - Acceptable frame type to be set   */
/*                                         for this PEP.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tVlanId SVlanId, UINT1 u1AccepFrameType)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPepAccFrameType (u4IfIndex, SVlanId, u1AccepFrameType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepDefUserPriority                        */
/*                                                                           */
/* Description        : This function sets the Default User priority for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      i4DefUsrPri  - Default user priority for this PEP.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 tVlanId SVlanId, INT4 i4DefUsrPri)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPepDefUserPriority (u4IfIndex, SVlanId, i4DefUsrPri);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepIngFiltering                           */
/*                                                                           */
/* Description        : This function sets the Enable Ingress Filetering     */
/*                      parameter for the given Provider Edge Port in        */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      u1IngFilterEnable - Enable / Disable Ingress         */
/*                                          filtering.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tVlanId SVlanId, UINT1 u1IngFilterEnable)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPepIngFiltering (u4IfIndex, SVlanId, u1IngFilterEnable);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPcpEncodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.         */
/*                          u2Priority  - Priority value                     */
/*                          u1DropEligible - Drop Eligible value             */
/*                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPcpEncodTbl (u4IfIndex, NpPbVlanPcpInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPcpDecodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.         */
/*                          u2PcpVlaue  - PCP value                          */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).   */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp).            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPcpDecodTbl (u4IfIndex, NpPbVlanPcpInfo);
    return FNP_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortUseDei.                               */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPortUseDei (u4IfIndex, u1UseDei);
    return FNP_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortReqDropEncoding.                      */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT1 u1ReqDrpEncoding)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPortReqDropEncoding (u4IfIndex, u1ReqDrpEncoding);
    return FNP_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortPcpSelRow.                            */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port Index.                              */
/*                      u2PcpSelection  - It can take the following values:  */
/*                                            8P0D, 7P1D, 6P2D, 5P3D         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT2 u2PcpSelection)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetPortPcpSelection (u4IfIndex, u2PcpSelection);
    return FNP_SUCCESS;
}

/* Internal CNP - Service Priority Regeneration Table NPAPIs */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetServicePriRegenEntry                      */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      i4RecvPriority - Receive Priority                    */
/*                      i4RegenPriority - Regenerated Priority               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId SVlanId, INT4 i4RecvPriority,
                                   INT4 i4RegenPriority)
{
    UNUSED_PARAM (u4ContextId);
    FsVlanHwSetServicePriRegenEntry (u4IfIndex, SVlanId, i4RecvPriority,
                                     i4RegenPriority);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetTunnelMacAddress                        */
/*                                                                           */
/* Description        : This function creates filters for receiving the      */
/*                      packets destined to proprietary tunnel MAC           */
/*                      address configured for different L2 protocols.       */
/*                                                                           */
/* Input(s)           : MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetTunnelMacAddress (UINT4 u4ContextId, tMacAddr MacAddr,
                               UINT2 u2Protocol)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetTunnelMacAddress (MacAddr, u2Protocol));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetCvidUntagPep                            */
/*                                                                           */
/* Description        : This function sets the UntagPep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Customer Edge Port Index.               */
/*                      VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetCvidUntagPep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetCvidUntagPep (VlanSVlanMap));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetCvidUntagCep                            */
/*                                                                           */
/* Description        : This function sets the UntagCep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Customer Edge Port Index.               */
/*                      VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetCvidUntagCep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwSetCvidUntagCep (VlanSVlanMap));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetNextSVlanMap                            */
/*                                                                           */
/* Description        : This function is called to get Cvid Entry for CVlan  */
/*                      for Port in the SVLAN classification tables          */
/*                                                                           */
/* Input(s)           : VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                      CvlanId - CVlan Id                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiVlanHwGetNextSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                           tVlanSVlanMap * pRetVlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwGetNextSVlanMap (VlanSVlanMap, pRetVlanSVlanMap));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetNextSVlanTranslationEntry               */
/*                                                                           */
/* Description        : This function is called to get entry                 */
/*                      from a  VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      pVidTransEntryInfo - Vid Translation Entry           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
FsMiVlanHwGetNextSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        tVlanId u2LocalSVlan,
                                        tVidTransEntryInfo * pVidTransEntryInfo)
{

    UNUSED_PARAM (u4ContextId);
    return (FsVlanHwGetNextSVlanTranslationEntry (u4IfIndex, u2LocalSVlan,
                                                  pVidTransEntryInfo));

}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwProviderBridgePortType.                */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier             */
/*                      u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT4 u4PortType,
                                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanMbsmHwSetProviderBridgePortType (u4IfIndex, u4PortType,
                                                   pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortIngressEtherType.                   */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       UINT2 u2EtherType,
                                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanMbsmHwSetPortIngressEtherType (u4IfIndex, u2EtherType,
                                                pSlotInfo);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortEgressEtherType.                      */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT2 u2EtherType,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanMbsmHwSetPortEgressEtherType (u4IfIndex, u2EtherType,
                                               pSlotInfo);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortSVlanTranslationStatus.             */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             UINT1 u1Status,
                                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortSVlanTranslationStatus (u4IfIndex, u1Status,
                                                       pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwAddSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2LocalSVlan, UINT2 u2RelaySVlan,
                                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanMbsmHwAddSVlanTranslationEntry (u4IfIndex, u2LocalSVlan,
                                                  u2RelaySVlan, pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortEtherTypeSwapStatus.                */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1Status,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return (FsVlanMbsmHwSetPortEtherTypeSwapStatus (u4IfIndex, u1Status,
                                                    pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwAddEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet  */
/*                      u2RelayEtherType- Ether type understand by the bridge*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT2 u2LocalEtherType,
                                     UINT2 u2RelayEtherType,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwAddEtherTypeSwapEntry (u4IfIndex,
                                               u2LocalEtherType,
                                               u2RelayEtherType, pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwAddSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                                                                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwAddSVlanMap (VlanSVlanMap, pSlotInfo));

}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortSVlanClassifyMethod                 */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1TableType,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortSVlanClassifyMethod (u4IfIndex, u1TableType,
                                                    pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwPortMacLearningStatus.                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwPortMacLearningStatus (u4IfIndex, u1Status, pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwPortMacLearningLimit.                      */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwPortMacLearningLimit (u4IfIndex, u4MacLimit,
                                              pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortCustomerVlan.                       */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port IfIndex                             */
/*                      CVlanId - Configured Customer VLAN ID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId CVlanId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsVlanMbsmHwSetPortCustomerVlan (u4IfIndex, CVlanId, pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwCreateProviderEdgePort                     */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id.                         */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      tVlanId SVlanId,
                                      tHwVlanPbPepInfo PepConfig,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwCreateProviderEdgePort (u4IfIndex, SVlanId, PepConfig,
                                               pSlotInfo);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPcpEncodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.         */
/*                          u2Priority  - Priority value                     */
/*                          u1DropEligible - Drop Eligible value             */
/*                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwSetPcpEncodTbl (u4IfIndex, NpPbVlanPcpInfo, pSlotInfo);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPcpDecodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.         */
/*                          u2PcpVlaue  - PCP value                          */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).   */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp).            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwSetPcpDecodTbl (u4IfIndex, NpPbVlanPcpInfo, pSlotInfo);
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortUseDei.                               */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwSetPortUseDei (u4IfIndex, u1UseDei, pSlotInfo);
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortReqDropEncoding.                      */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1ReqDrpEncoding,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwSetPortReqDropEncoding (u4IfIndex, u1ReqDrpEncoding,
                                               pSlotInfo);
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortPcpSelRow.                            */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port Index.                              */
/*                      u2PcpSelection  - It can take the following values:  */
/*                                            8P0D, 7P1D, 6P2D, 5P3D         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT2 u2PcpSelection,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsVlanMbsmHwSetPortPcpSelection (u4IfIndex, u2PcpSelection,
                                            pSlotInfo);
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetServicePriRegenEntry                      */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      i4RecvPriority - Receive Priority                    */
/*                      i4RegenPriority - Regenerated Priority               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       tVlanId SVlanId, INT4 i4RecvPriority,
                                       INT4 i4RegenPriority,
                                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanMbsmHwSetServicePriRegenEntry (u4IfIndex, SVlanId,
                                                i4RecvPriority,
                                                i4RegenPriority, pSlotInfo);

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwMulticastMacTableLimit                     */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - Mac Limit of the Multicast Table        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);

    return FsVlanHwMulticastMacTableLimit (u4MacLimit, pSlotInfo);
}
#endif
