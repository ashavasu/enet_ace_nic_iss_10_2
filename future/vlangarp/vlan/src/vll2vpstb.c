/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vll2vpstb.c,v 1.6 2015/04/18 10:58:37 siva Exp $
 *
 * Description: This file contains utility routines used in VLANmodule
 *              for MPLS Interactions. 
 *
 *******************************************************************/
#ifndef _VLL2VPSTB_C
#define _VLL2VPSTB_C
#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnInit                                    */
/*                                                                           */
/*    Description         : This function initialises the Data Structures    */
/*                          in VLAN modules required for MPLS-VLAN Interaction*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnInit (VOID)
{
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnDeInit                                  */
/*                                                                           */
/*    Description         : This function destroys the Data Structures and   */
/*                          Mempools in VLAN module required for MPLS-VLAN   */
/*                          Interaction                                      */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnDeInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnHandleAddL2VpnInfo                      */
/*                                                                           */
/*    Description         : This function process the message for creating   */
/*                          L2Vpn Map Entry based on Service type. This msg  */
/*                          was posted by MPLS Module                        */
/*                                                                           */
/*    Input(s)            : pL2VpnData- Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnHandleAddL2VpnInfo (tVplsInfo * pL2VpnData)
{
    UNUSED_PARAM (pL2VpnData);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnHandleDelL2VpnInfo                      */
/*                                                                           */
/*    Description         : This function process the message for deleting   */
/*                          L2Vpn Map Entry based on Service type. This msg  */
/*                          was posted by MPLS Module                        */
/*                                                                           */
/*    Input(s)            : pL2VpnData- Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnHandleDelL2VpnInfo (tVplsInfo * pL2VpnData)
{
    UNUSED_PARAM (pL2VpnData);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnHandleFwdMplsPktOnPorts                 */
/*                                                                           */
/*    Description         : This function process the packet recvd from the  */
/*                          message from MPLS Module. This Packet was recvd  */
/*                          on MPLS interface and to be fwd on ethernet ports*/
/*                                                                           */
/*    Input(s)            : pBuf - Packet received on MPLS Interface         */
/*                          pVlanIf - Message containing the information for */
/*                          processing the packet                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnHandleMplsPktFwdOnPorts (tCRU_BUF_CHAIN_DESC * pFrame,
                                  tVlanIfMsg * pVlanIf)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanIf);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnPktFloodOnMplsPort                      */
/*                                                                           */
/*    Description         : This function is called from VLAN module to flood*/
/*                          a unknown unicast pkt/ mcast pkt on MPLS         */
/*                          interface. This function calls MPLS API to flood.*/
/*                                                                           */
/*    Input(s)            : pFrame- Packet received on MPLS Interface        */
/*                          pVlanIf - Message containing the information for */
/*                          processing the packet                            */
/*                          pVlanEntry - VLAN Curr Entry                     */
/*                          pInVlanTag - Contains pkt tag information        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

UINT4
VlanL2VpnPktFloodOnMplsPort (tCRU_BUF_CHAIN_DESC * pFrame,
                             tVlanIfMsg * pVlanIf, tVlanCurrEntry
                             * pVlanEntry, tVlanTag * pInVlanTag)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanIf);
    UNUSED_PARAM (pVlanEntry);
    UNUSED_PARAM (pInVlanTag);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnFwdLearntPktOnMplsPort                  */
/*                                                                           */
/*    Description         : This function is called from VLAN module to switch*/
/*                          a known unicast pkt on MPLS interface. This      */
/*                          function calls MPLS API to fwd pkt on specific   */
/*                          MPLS Port.                                       */
/*                                                                           */
/*    Input(s)            : pFrame- Packet received on MPLS Interface        */
/*                          u4VplsIndex - VPLS Interface Identifier(MPLS Port)*/
/*                          processing the packet                            */
/*                          pVlanEntry - VLAN Curr Entry                     */
/*                          pInVlanTag - Contains pkt tag information        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnFwdLearntPktOnMplsPort (tCRU_BUF_CHAIN_DESC * pFrame,
                                 tVlanIfMsg * pVlanIf,
                                 tVlanCurrEntry * pVlanEntry,
                                 tVlanTag * pInVlanTag, UINT4 u4VplsIndex)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanIf);
    UNUSED_PARAM (pVlanEntry);
    UNUSED_PARAM (pInVlanTag);
    UNUSED_PARAM (u4VplsIndex);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnIsVlanMapEntryPresent                   */
/*                                                                           */
/*    Description         : This function is called from VLAN module to      */
/*                          decide whether the configuration of ELINE service*/
/*                          type is valid for the VLAN OR whether the VLAN   */
/*                          can be deleted in the system                     */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan Identifier                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE/VLAN_FALSE                             */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnIsVlanMapEntryPresent (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnIsMplsIfPresent                         */
/*                                                                           */
/*    Description         : This function is called from VLAN module to      */
/*                          decide whether the configuration of ELINE service*/
/*                          type is valid for the VLAN OR whether the VLAN   */
/*                          can be deleted in the system                     */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan Identifier                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE/VLAN_FALSE                             */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnIsMplsIfPresent (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return VLAN_TRUE;
}

#endif /*__VLL2VPSTB_C*/
