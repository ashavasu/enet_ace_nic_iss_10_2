/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vlanivr.c,v 1.23 2017/12/26 11:06:17 siva Exp $                      */
/* Licensee Aricent Inc., 2002-2003                                          */
/*                                                                           */
/*  FILE NAME             : vlanivr.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 18 Mar 2003                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains VLAN routines which are called*/
/*                          by other modules specifically for IVR support .  */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 18/03/2003                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"

/* Added the following functions for VLAN oper status */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrComputeVlanIfOperStatus                   */
/*                                                                           */
/*    Description         :This function is for computing the L3 VLAN        */
/*                         interface oper status based on the oper status of */
/*                         the member ports of the VLAN.                     */
/*                         This will update the number of operational ports  */
/*                         for the VLAN and notity CFA of the oper status    */
/*                         change.                                           */
/*                                                                           */
/*                        This will be called under the following conditions:*/
/*                                                                           */
/*                             1. New VLAN is created                        */
/*                             2. VLAN is deleted                            */
/*                             3. New ports are added to the VLAN            */
/*                             4. Ports are deleted from the VLAN            */
/*                             5. Static VLAN table rowstatus is made ACTIVE */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pCurrEntry  - Pointer to the current VLAN entry. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrComputeVlanIfOperStatus (tVlanCurrEntry * pCurrEntry)
{
    UINT2               u2Port;
    UINT1               u1IsMember = VLAN_FALSE;
    tVlanPortEntry     *pPortEntry;

    /* Recalculate the Oper status */
    if (VlanCfaIsThisInterfaceVlan (pCurrEntry->VlanId) == CFA_FALSE)
    {
        return;
    }

    pCurrEntry->u2OperPortsCount = 0;

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2Port, u1IsMember);
        if (u1IsMember == VLAN_TRUE)
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
            if ((pPortEntry != NULL) &&
                (pPortEntry->u1OperStatus == VLAN_OPER_UP))
            {
                pCurrEntry->u2OperPortsCount++;
            }
        }
    }

    /*** Notify CFA of the Oper status ***/
    if ((pCurrEntry->u2OperPortsCount > 0) &&
        (VLAN_IS_VLAN_ENABLED () == VLAN_TRUE))
    {
        VlanCfaIvrNotifyVlanIfOperStatus (pCurrEntry->VlanId, CFA_IF_UP);
    }
    else
    {

        VlanCfaIvrNotifyVlanIfOperStatus (pCurrEntry->VlanId, CFA_IF_DOWN);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrComputeVlanIfOperStatusForPort            */
/*                                                                           */
/*    Description         :This function is for computing the L3 VLAN        */
/*                         interface oper status based on the oper status of */
/*                         the member ports of the VLAN.                     */
/*                         This will update the number of operational ports  */
/*                         for the VLAN and notity CFA of the oper status    */
/*                         change.                                           */
/*                                                                           */
/*                        This will be called under the following conditions:*/
/*                                                                           */
/*                             1. New ports are added to the VLAN            */
/*                             2. Ports are deleted from the VLAN            */
/*                                                                           */
/*    Input(s)            : pCurrEntry  - Pointer to the current VLAN entry. */
/*                        : u1Flag - whether a port is added or deleted.     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrComputeVlanIfOperStatusForPort (tVlanCurrEntry * pCurrEntry,
                                       UINT1 u1Flag)
{
    UINT1               u1PrevOperStatus;
    UINT1               u1CurrOperStatus;

    /* Recalculate the Oper status */
    if (VlanCfaIsThisInterfaceVlan (pCurrEntry->VlanId) == CFA_FALSE)
    {
        return;
    }

    if (pCurrEntry->u2OperPortsCount > 0)
    {

        u1PrevOperStatus = CFA_IF_UP;
    }
    else
    {
        u1PrevOperStatus = CFA_IF_DOWN;
    }

    if (u1Flag == VLAN_ADD)
    {
        pCurrEntry->u2OperPortsCount++;
    }
    else
    {
        pCurrEntry->u2OperPortsCount--;

    }

    if (pCurrEntry->u2OperPortsCount > 0)
    {

        u1CurrOperStatus = CFA_IF_UP;
    }
    else
    {
        u1CurrOperStatus = CFA_IF_DOWN;
    }
    /*** Notify CFA of the Oper status ***/

    if (u1PrevOperStatus != u1CurrOperStatus)
    {
        VlanCfaIvrNotifyVlanIfOperStatus (pCurrEntry->VlanId, u1CurrOperStatus);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrUpdateVlanIfOperStatus                    */
/*                                                                           */
/*    Description         :This function is for updating the VLAN interface  */
/*                         oper status whenever there is a change in port    */
/*                         oper status.                                      */
/*                         This function also updates CFA of the oper status */
/*                         change.                                           */
/*                                                                           */
/*    Input(s)            : u2Port - Port number.                            */
/*                          u1OperStatus - Port oper status                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrUpdateVlanIfOperStatus (UINT2 u2Port, UINT1 u1OperStatus)
{
    UINT4               u4PortID = 0;
    UINT1               u1IsMemberPort = VLAN_FALSE;
    tVlanId             VlanId;
    tVlanCurrEntry     *pCurrEntry;
    UINT1               u1PrevOperStatus;
    UINT1               u1CurrOperStatus;

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            continue;
        }
        if (VlanCfaIsThisInterfaceVlan (pCurrEntry->VlanId) == CFA_FALSE)
        {
            continue;
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2Port, u1IsMemberPort);
        if (u1IsMemberPort == VLAN_TRUE)
        {

            if (pCurrEntry->u2OperPortsCount > 0)
            {
                u1PrevOperStatus = CFA_IF_UP;
            }
            else
            {
                u1PrevOperStatus = CFA_IF_DOWN;
            }

            if (u1OperStatus == VLAN_OPER_UP)
            {
                pCurrEntry->u2OperPortsCount++;
            }
            else
            {
                pCurrEntry->u2OperPortsCount--;
            }

            if (pCurrEntry->u2OperPortsCount > 0)
            {
                u1CurrOperStatus = CFA_IF_UP;
            }
            else
            {
                u1CurrOperStatus = CFA_IF_DOWN;
            }
            /* Getting Port Index from Local Port Id To Notify Sub-Interfaces 
             * about OperStatus Change */
            if (VlanVcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                &u4PortID) != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "\r%% Error in Getting Local Port"
                          "for current vlan. \r\n");
                return;
            }

            /* If VALID Index is not Found ,check if the VLAN Index is present in
             *  Layer 3 Subinterface and send notification to Subinterface*/
            CfaNotifyToL3SubIfVlanIndexInCxt ((UINT2) u4PortID,
                                              VLAN_CURR_CONTEXT_ID (),
                                              pCurrEntry->VlanId, u1OperStatus);
            if (u1PrevOperStatus != u1CurrOperStatus)
            {
                /** Notify CFA of the oper status change **/
                VlanCfaIvrNotifyVlanIfOperStatus (pCurrEntry->VlanId,
                                                  u1CurrOperStatus);
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrGetOperStatus                             */
/*                                                                           */
/*    Description         :This function returns the Oper Status of Vlan.    */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId                                  */
/*                                                                           */
/*    Output(s)           : pu1OperStatus - Port oper status                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrGetOperStatus (tVlanId VlanId, UINT1 *pu1OperStatus)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanPortEntry     *pPortEntry;
    UINT2               u2Port;
    UINT1               u1IsMember = VLAN_FALSE;

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (NULL == pCurrEntry)
    {
        *pu1OperStatus = CFA_IF_DOWN;
        return;
    }

    pCurrEntry->u2OperPortsCount = 0;

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2Port, u1IsMember);
        if (u1IsMember == VLAN_TRUE)
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
            if ((pPortEntry != NULL) &&
                (pPortEntry->u1OperStatus == VLAN_OPER_UP))
            {
                pCurrEntry->u2OperPortsCount++;
            }
        }
    }

    if (pCurrEntry->u2OperPortsCount > 0)
    {
        *pu1OperStatus = CFA_IF_UP;
    }
    else
    {
        *pu1OperStatus = CFA_IF_DOWN;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrUpdateMclagIfOperStatus                   */
/*                                                                           */
/*    Description         : This function updates the Oper Status to CFA     */
/*                          based on MCLAG interface membership in the vlan  */
/*                                                                           */
/*    Input(s)            : u2Port - port no                                 */
/*                          u1MclagStatus - MCLAG status of port channel     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables    : None                                             */
/*    Referred                                                               */
/*                                                                           */
/*    Global Variables    : None.                                            */
/*    Modified                                                               */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrUpdateMclagIfOperStatus (UINT2 u2Port, UINT1 u1MclagStatus)
{
#ifdef ICCH_WANTED
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanId             VlanId;
    UINT1               u1IsMemberPort = VLAN_FALSE;

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME,
                       "VlanIvrUpdateMclagIfOperStatus: "
                       "Port Entry not present for port= %u \r\n", u2Port);
        return;
    }

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            continue;
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2Port, u1IsMemberPort);

        if (u1IsMemberPort == VLAN_TRUE)
        {
            /* If the Mclag interface is already oper up, no need 
               to send indication to CFA.
             */
            if (u1MclagStatus == VLAN_ENABLED)
            {
                if (pCurrEntry->u1IsMclagEnabled == VLAN_FALSE)
                {
                    /* This is the first Mclag interface for this vlan.
                       Mark it is TRUE
                     */
                    pCurrEntry->u1IsMclagEnabled = VLAN_TRUE;

                    /** Notify CFA of the oper status change **/
                    VlanCfaIvrNotifyMclagIfOperStatus (pCurrEntry->VlanId,
                                                       CFA_IF_UP);
                }
                else
                {
                    /* Nothing to do. Already some other interface
                       is present 
                     */
                }
            }
            else
            {
                /* Mc-lag is disabled in the interface. Compute the 
                   Oper status based on other ports */

                VlanIvrComputeVlanMclagIfOperStatus (pCurrEntry);
            }
        }
    }

#else
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1MclagStatus);
#endif

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrComputeVlanMclagIfOperStatus              */
/*                                                                           */
/*    Description         : This function is for computing the L3 VLAN       */
/*                          interface oper status based on the oper status   */
/*                          of the mclag member ports of the VLAN.           */
/*                                                                           */
/*                          This will be called for the following conditions:*/
/*                                                                           */
/*                             1. New VLAN is created                        */
/*                             2. VLAN is deleted                            */
/*                             3. New ports are added to the VLAN            */
/*                             4. Ports are deleted from the VLAN            */
/*                             5. Static VLAN table rowstatus is made ACTIVE */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pCurrEntry  - Pointer to the current VLAN entry. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrComputeVlanMclagIfOperStatus (tVlanCurrEntry * pCurrEntry)
{
#ifdef ICCH_WANTED
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1IsMember = VLAN_FALSE;
    UINT1               u1PrevMclagEnabled = VLAN_FALSE;

    if (pCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanIvrComputeVlanMclagIfOperStatus: "
                  "Vlan Entry not present \r\n");
        return;
    }

    /* Set the Mclag Interface Present to FALSE and recalculate the 
       Mclag interface status again. This is done because the mc-lag
       ports might have got removed and it should be recalculated 
       based on the current port membership of the VLAN.
     */

    u1PrevMclagEnabled = pCurrEntry->u1IsMclagEnabled;
    pCurrEntry->u1IsMclagEnabled = VLAN_FALSE;

    for (u4IfIndex = (BRG_MAX_PHY_PORTS + 1);
         u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4IfIndex++)
    {
        /* Get the port entry from VLAN module */
        if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_FAILURE)
        {
            continue;
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2LocalPort, u1IsMember);

        if (u1IsMember == VLAN_TRUE)
        {
            /* Check atleast one mc-lag port is present */
            if (VlanLaIsMclagInterface (u4IfIndex) == VLAN_TRUE)
            {
                pCurrEntry->u1IsMclagEnabled = VLAN_TRUE;
                break;
            }

        }
    }

    /* Mclag port is not present. The value is already set to false */

    /* Notify CFA of the oper status change only if it has changed */

    if (u1PrevMclagEnabled != pCurrEntry->u1IsMclagEnabled)
    {
        if (pCurrEntry->u1IsMclagEnabled == VLAN_TRUE)
        {
            VlanCfaIvrNotifyMclagIfOperStatus (pCurrEntry->VlanId, CFA_IF_UP);
        }
        else
        {
            VlanCfaIvrNotifyMclagIfOperStatus (pCurrEntry->VlanId, CFA_IF_DOWN);
        }
    }

#else
    UNUSED_PARAM (pCurrEntry);
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessMclagStatus                           */
/*                                                                           */
/*    Description         : This function processes the MCLAG status from LA */
/*                                                                           */
/*    Input(s)            : pVlanQMsg - Pointer to queue message             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables    : None                                             */
/*    Referred                                                               */
/*                                                                           */
/*    Global Variables    : None.                                            */
/*    Modified                                                               */
/*                                                                           */
/*    Use of Recursion   : None.                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/

VOID
VlanProcessMclagStatus (tVlanQMsg * pVlanQMsg)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if (pVlanQMsg == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanProcessMclagStatus "
                  "Queue Message not present. Ignoring Event \r\n");
        return;
    }

    if ((pVlanQMsg->u2MsgType == VLAN_MCLAG_ENABLE_STATUS) ||
        (pVlanQMsg->u2MsgType == VLAN_MCLAG_DISABLE_STATUS))
    {
        if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME,
                           "VlanProcessMclagStatus "
                           "Context Information not present. Ignoring Event for"
                           "port = %u\r\n", pVlanQMsg->u4Port);
            return;
        }

        if (pVlanQMsg->u2MsgType == VLAN_MCLAG_ENABLE_STATUS)
        {
            VlanIvrUpdateMclagIfOperStatus (u2LocalPort, VLAN_ENABLED);
        }
        else
        {
            VlanIvrUpdateMclagIfOperStatus (u2LocalPort, VLAN_DISABLED);
        }
    }
    return;
}
