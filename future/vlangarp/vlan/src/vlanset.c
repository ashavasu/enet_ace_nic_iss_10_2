/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanset.c,v 1.251.2.1 2018/03/15 13:00:05 siva Exp $
 *
 * Description     : This file contains set routines for VLAN objects
 *
 *******************************************************************/
/*****************************************************************************/
/*  FILE NAME             : vlanset.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains routines to set VLAN objects  */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"
#include "fsmsbecli.h"
#include "fsmpvlcli.h"
#include "fsmsvlcli.h"

UINT4               fsDot1dTp[] = { 1, 3, 6, 1, 4, 1, 2076, 116, 4 };
UINT4               fsDot1dStatic[] = { 1, 3, 6, 1, 4, 1, 2076, 116, 5 };
UINT4               fsPBridgeMIB[] = { 1, 3, 6, 1, 4, 1, 2076, 116, 6 };
extern UINT4        fsmpb[8];
extern UINT4        fsm1ad[8];
extern UINT4        fsmvle[8];
extern UINT4        fsmpvl[8];
extern UINT4        fsmsvl[9];
extern UINT4        fsmsbe[10];
extern UINT4        FsMIDot1qFutureStVlanPVlanType[12];
extern UINT4        FsMIDot1qFutureStVlanPrimaryVid[12];
extern UINT4        FsMIDot1qFutureStVlanEgressEthertype[12];
extern UINT4        FsMIDot1qFutureVlanUserDefinedTPID[12];
extern UINT4        FsMIDot1qFutureVlanLoopbackStatus[12];

/************************ Q-MIB Get routines *******************************/

/* LOW LEVEL Routines for Table : Dot1qForwardAllTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDot1qForwardAllStaticPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qForwardAllStaticPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qForwardAllStaticPorts (UINT4 u4VlanIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValDot1qForwardAllStaticPorts)
{
    if (VlanSetForwardAllStaticPorts (u4VlanIndex,
                                      pSetValDot1qForwardAllStaticPorts)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qForwardAllForbiddenPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qForwardAllForbiddenPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qForwardAllForbiddenPorts (UINT4 u4VlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValDot1qForwardAllForbiddenPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pAddedPorts = NULL;
    UINT1              *pDeletedPorts = NULL;
    UINT1              *pAddPorts = NULL;
    UINT1              *pDelPorts = NULL;
    UINT1              *pForbiddenPorts = NULL;
    UINT1              *pau1OldForbiddenPortList = NULL;
    tVlanId             VlanId = (tVlanId) u4VlanIndex;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OldAllForbiddenPorts;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4SetValFsDot1qForwardAllPort = VLAN_NON_MEMBER_PORT;
    UINT4               u4ByteIndex;
    UINT2               u2Port = 0;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1MsrNotify = VLAN_FALSE;
    UINT1               u1SendRowStatusNotif = VLAN_FALSE;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {

        return SNMP_FAILURE;
    }
    pau1OldForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qForwardAllForbiddenPorts: Error in allocating memory "
                  "for pau1OldForbiddenPortList\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pau1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    /* Obtain the prev. static port list for the vlan */
    OldAllForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldAllForbiddenPorts.pu1_OctetList = pau1OldForbiddenPortList;

    nmhGetDot1qForwardAllForbiddenPorts (u4VlanIndex, &OldAllForbiddenPorts);

    pForbiddenPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pForbiddenPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qForwardAllForbiddenPorts: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pForbiddenPorts, 0, sizeof (tLocalPortList));

    MEMCPY (pForbiddenPorts,
            pSetValDot1qForwardAllForbiddenPorts->pu1_OctetList,
            pSetValDot1qForwardAllForbiddenPorts->i4_Length);
    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pAddedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qForwardAllForbiddenPorts: "
                  "Error in allocating memory for pAddedPorts\r\n");
        UtilPlstReleaseLocalPortList (pForbiddenPorts);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));

    pDelPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pDelPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qForwardAllForbiddenPorts: Error in allocating memory "
                  "for pDelPorts\r\n");
        UtilPlstReleaseLocalPortList (pAddedPorts);
        UtilPlstReleaseLocalPortList (pForbiddenPorts);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDelPorts, 0, sizeof (tLocalPortList));
    if ((VlanGmrpIsGmrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) == GMRP_TRUE)
        || (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE))
    {

        for (u4ByteIndex = 0; u4ByteIndex < VLAN_PORT_LIST_SIZE; u4ByteIndex++)
        {

            pAddedPorts[u4ByteIndex]
                = (UINT1) (~(pCurrEntry->AllGrps.ForbiddenPorts[u4ByteIndex])
                           & (pForbiddenPorts[u4ByteIndex]));

            pDelPorts[u4ByteIndex]
                = (UINT1) (~(pForbiddenPorts[u4ByteIndex])
                           & (pCurrEntry->AllGrps.ForbiddenPorts[u4ByteIndex]));
        }

        VlanAttrRPSetDefGrpForbiddPorts (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_ALL_GROUPS, VlanId, pAddedPorts,
                                         pDelPorts);
    }
    UtilPlstReleaseLocalPortList (pAddedPorts);
    UtilPlstReleaseLocalPortList (pDelPorts);

    VLAN_RESET_PORT_LIST (pCurrEntry->AllGrps.Ports, pForbiddenPorts);

    MEMCPY (pCurrEntry->AllGrps.ForbiddenPorts,
            pForbiddenPorts, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pForbiddenPorts);

    /* Sending Trigger to MSR & RM */
    pAddPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qForwardAllForbiddenPorts: Error in allocating memory "
                  "for pAddPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pAddPorts, 0, sizeof (tLocalPortList));

    pDeletedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDeletedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qForwardAllForbiddenPorts: "
                  "Error in allocating memory for pDeletedPorts\r\n");
        UtilPlstReleaseLocalPortList (pAddPorts);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDeletedPorts, 0, sizeof (tLocalPortList));

    VlanGetAddedAndDeletedPorts (&OldAllForbiddenPorts,
                                 pSetValDot1qForwardAllForbiddenPorts,
                                 pAddPorts, pDeletedPorts);
    UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    for (u2Port = 1; u2Port < VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_MEMBER_PORT (pAddPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsDot1qForwardAllPort = VLAN_ADD_FORBIDDEN_PORT;
        }

        VLAN_IS_MEMBER_PORT (pDeletedPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsDot1qForwardAllPort = VLAN_DEL_FORBIDDEN_PORT;
        }
        /* For port that are not added or deleted for this vlan,
         * i4SetValFsDot1qForwardAllPort will be Non Memeber. */
        if (i4SetValFsDot1qForwardAllPort != VLAN_NON_MEMBER_PORT)
        {
            if (u1SendRowStatusNotif == VLAN_FALSE)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1qForwardAllRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                                  VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                                  VLAN_NOT_IN_SERVICE));
                VlanSelectContext (u4CurrContextId);
                u1SendRowStatusNotif = VLAN_TRUE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qForwardAllPort,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                              VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                              VLAN_GET_IFINDEX (u2Port),
                              i4SetValFsDot1qForwardAllPort));
            VlanSelectContext (u4CurrContextId);
            u1MsrNotify = VLAN_TRUE;
        }
        i4SetValFsDot1qForwardAllPort = VLAN_NON_MEMBER_PORT;
    }
    UtilPlstReleaseLocalPortList (pAddPorts);
    UtilPlstReleaseLocalPortList (pDeletedPorts);

    if (u1MsrNotify == VLAN_TRUE)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qForwardAllRowStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                          VLAN_CURR_CONTEXT_ID (), u4VlanIndex, VLAN_ACTIVE));
        VlanSelectContext (u4CurrContextId);
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pSetValDot1qForwardAllForbiddenPorts);

    return SNMP_FAILURE;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************************
Function    :  nmhSetDot1qForwardUnregisteredStaticPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qForwardUnregisteredStaticPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qForwardUnregisteredStaticPorts (UINT4 u4VlanIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pSetValDot1qForwardUnregisteredStaticPorts)
{
    if (VlanSetForwardUnregStaticPorts
        (u4VlanIndex,
         pSetValDot1qForwardUnregisteredStaticPorts) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qForwardUnregisteredForbiddenPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qForwardUnregisteredForbiddenPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qForwardUnregisteredForbiddenPorts (UINT4 u4VlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValDot1qForwardUnregisteredForbiddenPorts)
{
#ifdef VLAN_EXTENDED_FILTER
    tVlanId             VlanId = (tVlanId) u4VlanIndex;
    tVlanCurrEntry     *pCurrEntry;
    tLocalPortList      AddedPorts;
    tLocalPortList      DelPorts;
    tLocalPortList      ForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE OldUnregForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE NewUnregForbiddenPorts;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tLocalPortList      AddPorts;
    tLocalPortList      DeletedPorts;
    UINT1               au1OldUnregForbiddenPortList[VLAN_PORT_LIST_SIZE];
    UINT1               au1NewUnregForbiddenPortList[VLAN_PORT_LIST_SIZE];
    INT4                i4SetValFsDot1qForwardUnregPort = VLAN_NON_MEMBER_PORT;
    UINT4               u4ByteIndex;
    UINT2               u2Port;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1MsrNotify = VLAN_FALSE;
    UINT1               u1SendRowStatusNotif = VLAN_FALSE;

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {

        return SNMP_FAILURE;
    }
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (ForbiddenPorts, 0, sizeof (tLocalPortList));

    MEMSET (AddedPorts, 0, sizeof (tLocalPortList));
    MEMSET (DelPorts, 0, sizeof (tLocalPortList));

    /* Obtain the prev. forbidden port list for the vlan */
    MEMSET (au1OldUnregForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    OldUnregForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldUnregForbiddenPorts.pu1_OctetList = au1OldUnregForbiddenPortList;

    nmhGetDot1qForwardUnregisteredForbiddenPorts (u4VlanIndex,
                                                  &OldUnregForbiddenPorts);

    MEMCPY (ForbiddenPorts,
            pSetValDot1qForwardUnregisteredForbiddenPorts->pu1_OctetList,
            pSetValDot1qForwardUnregisteredForbiddenPorts->i4_Length);

    if ((VlanGmrpIsGmrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) == GMRP_TRUE)
        || (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE))
    {

        for (u4ByteIndex = 0; u4ByteIndex < VLAN_PORT_LIST_SIZE; u4ByteIndex++)
        {

            AddedPorts[u4ByteIndex]
                = (UINT1) (~(pCurrEntry->UnRegGrps.ForbiddenPorts[u4ByteIndex])
                           & (ForbiddenPorts[u4ByteIndex]));

            DelPorts[u4ByteIndex]
                = (UINT1) (~(ForbiddenPorts[u4ByteIndex])
                           & (pCurrEntry->UnRegGrps.
                              ForbiddenPorts[u4ByteIndex]));
        }

        VlanAttrRPSetDefGrpForbiddPorts (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_UNREG_GROUPS, VlanId,
                                         AddedPorts, DelPorts);
    }

    VLAN_RESET_PORT_LIST (pCurrEntry->UnRegGrps.Ports, ForbiddenPorts);

    MEMCPY (pCurrEntry->UnRegGrps.ForbiddenPorts,
            ForbiddenPorts, VLAN_PORT_LIST_SIZE);

    /* Sending Trigger to MSR & RM */

    NewUnregForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    NewUnregForbiddenPorts.pu1_OctetList = au1NewUnregForbiddenPortList;

    MEMSET (au1NewUnregForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMSET (AddPorts, 0, sizeof (tLocalPortList));
    MEMSET (DeletedPorts, 0, sizeof (tLocalPortList));

    VLAN_ADD_PORT_LIST (NewUnregForbiddenPorts.pu1_OctetList,
                        pSetValDot1qForwardUnregisteredForbiddenPorts->
                        pu1_OctetList);

    /*    VLAN_AND_PORT_LIST (NewUnregForbiddenPorts.pu1_OctetList,
       EgressPorts.pu1_OctetList); */

    NewUnregForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    NewUnregForbiddenPorts.pu1_OctetList = au1NewUnregForbiddenPortList;

    MEMSET (au1NewUnregForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMSET (AddPorts, 0, sizeof (tLocalPortList));
    MEMSET (DeletedPorts, 0, sizeof (tLocalPortList));

    VLAN_ADD_PORT_LIST (NewUnregForbiddenPorts.pu1_OctetList,
                        pSetValDot1qForwardUnregisteredForbiddenPorts->
                        pu1_OctetList);

    /*    VLAN_AND_PORT_LIST (NewUnregForbiddenPorts.pu1_OctetList,
       EgressPorts.pu1_OctetList); */

    VlanGetAddedAndDeletedPorts (&OldUnregForbiddenPorts,
                                 &NewUnregForbiddenPorts,
                                 AddPorts, DeletedPorts);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    for (u2Port = 1; u2Port < VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_MEMBER_PORT (AddPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsDot1qForwardUnregPort = VLAN_ADD_FORBIDDEN_PORT;
        }

        VLAN_IS_MEMBER_PORT (DeletedPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsDot1qForwardUnregPort = VLAN_DEL_FORBIDDEN_PORT;
        }
        /* For port that are not added or deleted for this vlan,
         * i4SetValFsDot1qForwardUnregPort will be Non Memeber. */

        if (i4SetValFsDot1qForwardUnregPort != VLAN_NON_MEMBER_PORT)
        {
            if (u1SendRowStatusNotif == VLAN_FALSE)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1qForwardUnregRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                                  VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                                  VLAN_NOT_IN_SERVICE));
                VlanSelectContext (u4CurrContextId);
                u1SendRowStatusNotif = VLAN_TRUE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qForwardUnregPort,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                              VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                              VLAN_GET_IFINDEX (u2Port),
                              i4SetValFsDot1qForwardUnregPort));
            VlanSelectContext (u4CurrContextId);
            u1MsrNotify = VLAN_TRUE;
        }

        i4SetValFsDot1qForwardUnregPort = VLAN_NON_MEMBER_PORT;
    }

    if (u1MsrNotify == VLAN_TRUE)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qForwardUnregRowStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                          VLAN_CURR_CONTEXT_ID (), u4VlanIndex, VLAN_ACTIVE));
        VlanSelectContext (u4CurrContextId);
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pSetValDot1qForwardUnregisteredForbiddenPorts);

    return SNMP_FAILURE;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************************
Function    :  nmhSetDot1qStaticUnicastAllowedToGoTo
Input       :  The Indices
               Dot1qFdbId
               Dot1qStaticUnicastAddress
               Dot1qStaticUnicastReceivePort

               The Object 
               setValDot1qStaticUnicastAllowedToGoTo
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qStaticUnicastAllowedToGoTo (UINT4 u4Dot1qFdbId,
                                       tMacAddr Dot1qStaticUnicastAddress,
                                       INT4 i4Dot1qStaticUnicastReceivePort,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValDot1qStaticUnicastAllowedToGoTo)
{
    if (VlanSetStaticUcastAllowedToGoTo (u4Dot1qFdbId,
                                         Dot1qStaticUnicastAddress,
                                         i4Dot1qStaticUnicastReceivePort,
                                         pSetValDot1qStaticUnicastAllowedToGoTo)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qStaticUnicastStatus
Input       :  The Indices
               Dot1qFdbId
               Dot1qStaticUnicastAddress
               Dot1qStaticUnicastReceivePort

               The Object 
               setValDot1qStaticUnicastStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qStaticUnicastStatus (UINT4 u4Dot1qFdbId,
                                tMacAddr Dot1qStaticUnicastAddress,
                                INT4 i4Dot1qStaticUnicastReceivePort,
                                INT4 i4SetValDot1qStaticUnicastStatus)
{
    if (VlanSetStaticUnicastStatus (u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                                    i4Dot1qStaticUnicastReceivePort,
                                    i4SetValDot1qStaticUnicastStatus)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qStaticMulticastStaticEgressPorts
Input       :  The Indices
               Dot1qVlanIndex
               McastAddr
               Dot1qStaticMulticastReceivePort

               The Object 
               setValDot1qStaticMulticastStaticEgressPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qStaticMulticastStaticEgressPorts (UINT4 u4VlanIndex,
                                             tMacAddr McastAddr,
                                             INT4 i4RcvPort,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pStEgressPorts)
{
    if (VlanSetStaticMcastEgressPorts (u4VlanIndex,
                                       McastAddr,
                                       i4RcvPort,
                                       pStEgressPorts) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qStaticMulticastForbiddenEgressPorts
Input       :  The Indices
               Dot1qVlanIndex
               McastAddr
               Dot1qStaticMulticastReceivePort

               The Object 
               setValDot1qStaticMulticastForbiddenEgressPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qStaticMulticastForbiddenEgressPorts (UINT4 u4VlanIndex,
                                                tMacAddr McastAddr,
                                                INT4 i4RcvPort,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pForbiddenPorts)
{
    tVlanId             VlanId = (tVlanId) u4VlanIndex;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OldMcastForbiddenPorts;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4SetValFsDot1qStaticMcastPort = VLAN_NON_MEMBER_PORT;
    UINT2               u2Port;
    tLocalPortList      AddPorts;
    tLocalPortList      DeletedPorts;
    UINT1               au1OldForbiddenPortList[VLAN_PORT_LIST_SIZE];
    UINT1               u1MsrNotify = VLAN_FALSE;
    UINT1               u1RowStatus = VLAN_CREATE_AND_WAIT;
    UINT1               u1SendRowStatusNotif = VLAN_FALSE;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

#if !defined ISS_WANTED && !defined RM_WANTED
    UNUSED_PARAM (u1RowStatus);
#endif

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pStMcastEntry
        = VlanGetStMcastEntryWithExactRcvPort (McastAddr,
                                               (INT2) i4RcvPort, pVlanEntry);

    if (pStMcastEntry != NULL)
    {
        u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    /* Obtain the prev. multicast forbidden port list for the vlan */
    MEMSET (au1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    OldMcastForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldMcastForbiddenPorts.pu1_OctetList = au1OldForbiddenPortList;

    nmhGetDot1qStaticMulticastForbiddenEgressPorts (u4VlanIndex,
                                                    McastAddr,
                                                    i4RcvPort,
                                                    &OldMcastForbiddenPorts);

    if (VlanStaticMulticastForbiddenEgressPorts (u4VlanIndex, McastAddr,
                                                 i4RcvPort, pForbiddenPorts)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Sending trigger to MSR & RM */
    MEMSET (AddPorts, 0, sizeof (tLocalPortList));
    MEMSET (DeletedPorts, 0, sizeof (tLocalPortList));

    VlanGetAddedAndDeletedPorts (&OldMcastForbiddenPorts,
                                 pForbiddenPorts, AddPorts, DeletedPorts);

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    for (u2Port = 1; u2Port < VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_MEMBER_PORT (AddPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsDot1qStaticMcastPort = VLAN_ADD_FORBIDDEN_PORT;
        }

        VLAN_IS_MEMBER_PORT (DeletedPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsDot1qStaticMcastPort = VLAN_DEL_FORBIDDEN_PORT;
        }
        /* For port that are not added or deleted for this vlan,
         * i4SetValFsDot1qStaticMcastPort will be Non Memeber. */

        if (i4SetValFsDot1qStaticMcastPort != VLAN_NON_MEMBER_PORT)
        {
            if (u1SendRowStatusNotif == VLAN_FALSE)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1qStaticMulticastRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 4,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i",
                                  VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                                  McastAddr, VLAN_GET_IFINDEX (i4RcvPort),
                                  u1RowStatus));
                VlanSelectContext (u4CurrContextId);
                u1SendRowStatusNotif = VLAN_TRUE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticMcastPort,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 5,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i %i",
                              VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                              McastAddr,
                              VLAN_GET_IFINDEX (i4RcvPort),
                              VLAN_GET_IFINDEX (u2Port),
                              i4SetValFsDot1qStaticMcastPort));
            VlanSelectContext (u4CurrContextId);
            u1MsrNotify = VLAN_TRUE;
        }

        i4SetValFsDot1qStaticMcastPort = VLAN_NON_MEMBER_PORT;
    }

    if (u1MsrNotify == VLAN_TRUE)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticMulticastRowStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 4,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i",
                          VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                          McastAddr,
                          VLAN_GET_IFINDEX (i4RcvPort), VLAN_ACTIVE));
        VlanSelectContext (u4CurrContextId);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qStaticMulticastStatus
Input       :  The Indices
               Dot1qVlanIndex
               McastAddr
               Dot1qStaticMulticastReceivePort

               The Object 
               setValDot1qStaticMulticastStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qStaticMulticastStatus (UINT4 u4VlanIndex,
                                  tMacAddr McastAddr,
                                  INT4 i4RcvPort, INT4 i4Status)
{
    if (VlanSetStaticMulticastStatus (u4VlanIndex, McastAddr,
                                      i4RcvPort, i4Status) == VLAN_SUCCESS)
    {
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
        if (VLAN_MGMT_INVALID == i4Status)
        {
            VlanRedSyncMcastSwUpdate (VLAN_CURR_CONTEXT_ID (),
                                      u4VlanIndex,
                                      McastAddr,
                                      VLAN_GET_PHY_PORT (i4RcvPort),
                                      VLAN_DELETE);
        }
#endif /*L2RED_WANTED */

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDot1qVlanStaticName
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qVlanStaticName
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qVlanStaticName (UINT4 u4VlanIndex,
                           tSNMP_OCTET_STRING_TYPE * pVlanName)
{
    tVlanId             VlanId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VlanId = (tVlanId) u4VlanIndex;

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {

        return SNMP_FAILURE;
    }
    if (pVlanName->i4_Length > VLAN_STATIC_MAX_NAME_LEN)
    {

        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qVlanStaticName, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    pStVlanEntry->u1VlanNameLen = (UINT1) pVlanName->i4_Length;
    MEMSET (pStVlanEntry->au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN);
    MEMCPY (pStVlanEntry->au1VlanName,
            pVlanName->pu1_OctetList, pStVlanEntry->u1VlanNameLen);

    if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        VlanL2IwfUpdateVlanName (VLAN_CURR_CONTEXT_ID (), VlanId,
                                 pStVlanEntry->au1VlanName);
        /* Sending Trigger to MSR */
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %s", VLAN_CURR_CONTEXT_ID (),
                          u4VlanIndex, pVlanName));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    /* VLAN name is modified in VLAN module, So set u1NameModifiedFlag to 
     * VLAN_TRUE  */
    pStVlanEntry->u1NameModifiedFlag = VLAN_TRUE;

    /* Sending Trigger to MSR */
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %s", VLAN_CURR_CONTEXT_ID (),
                      u4VlanIndex, pVlanName));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qVlanStaticEgressPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qVlanStaticEgressPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qVlanStaticEgressPorts (UINT4 u4VlanIndex,
                                  tSNMP_OCTET_STRING_TYPE * pEgressPorts)
{
    UINT1              *pPortList = NULL;
    UINT1              *pAddedPorts = NULL;
    UINT1              *pDeletedPorts = NULL;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanBasicInfo      VlanBasicInfo;
#ifdef PVRST_WANTED
    INT4                i4Result = 0;
    UINT1               u1AddPortToInst = VLAN_TRUE;
#endif
    UINT1               u1ProtoId = 0;
    UINT2               u2Port = VLAN_INIT_VAL;
    BOOL1               bResult = VLAN_FALSE;
    UINT1               u1IfType = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1WarningFlag = VLAN_FALSE;
    MEMSET (&VlanBasicInfo, 0, sizeof (tVlanBasicInfo));

    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticEgressPorts: Error in allocating memory "
                  "for pPortList\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pAddedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticEgressPorts: "
                  "Error in allocating memory for pAddedPorts\r\n");
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));

    pDeletedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDeletedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticEgressPorts: "
                  "Error in allocating memory for pDeletedPorts\r\n");
        UtilPlstReleaseLocalPortList (pPortList);
        UtilPlstReleaseLocalPortList (pAddedPorts);
        return SNMP_FAILURE;
    }
    MEMSET (pDeletedPorts, 0, sizeof (tLocalPortList));

    VLAN_PBB_PERF_MARK_START_TIME ();

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    OldPortList.pu1_OctetList = pPortList;
    OldPortList.i4_Length = sizeof (tLocalPortList);

    if (pStVlanEntry != NULL)
    {
        VLAN_GET_EGRESS_PORTS (pStVlanEntry, OldPortList.pu1_OctetList);
        pVlanEntry = VlanGetVlanEntry (pStVlanEntry->VlanId);
    }

    /* Added for pseudo wire visibility and Attachment Circuit Interface */

    VlanGetAddedAndDeletedPorts (&OldPortList, pEgressPorts,
                                 pAddedPorts, pDeletedPorts);
    for (u2Port = 1; u2Port < VLAN_MAX_PORTS; u2Port++)
    {
        OSIX_BITLIST_IS_BIT_SET (pDeletedPorts, u2Port,
                                 CONTEXT_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            if (VlanVcmGetIfIndexFromLocalPort
                (VLAN_CURR_CONTEXT_ID (), u2Port, &u4IfIndex) == VCM_SUCCESS)
            {

                if (VlanValidateForL3Subinterface (u4IfIndex,
                                                   (UINT2) u4VlanIndex) ==
                    VLAN_SUCCESS)
                {
                    u1WarningFlag = VLAN_TRUE;
                }
            }

        }
    }                            /* For all ports present in port list */

    if (VLAN_TRUE == u1WarningFlag)
    {
        MOD_TRC (VLAN_TRUE, VLAN_TRUE, "",
                 "% WARNING!! Port is part of L3Subinterface\r\n");
    }

#ifdef VLAN_EXTENDED_FILTER
    if (pStVlanEntry != NULL)
    {
        if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
        {
            if (pVlanEntry != NULL)
            {
                VLAN_ADD_PORT_LIST (pVlanEntry->UnRegGrps.Ports, pAddedPorts);
            }
        }
    }
#endif

    UtilPlstReleaseLocalPortList (pPortList);
    /* Among the ports in the port list, PW and AC interfaces may be present.
     * PW and AC interfaces should be created while mapping to vlan */

    if (VlanUtilCreateExtPorts (VLAN_CURR_CONTEXT_ID (),
                                pAddedPorts) == VLAN_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pAddedPorts);
        UtilPlstReleaseLocalPortList (pDeletedPorts);
        return SNMP_FAILURE;
    }

    /* Among the ports in the port list, PW and AC interfaces may be present.
     * PW and AC interfaces should be deleted  while un mapping from vlan */

    if (VlanUtilDeleteExtPorts (VLAN_CURR_CONTEXT_ID (),
                                pDeletedPorts) == VLAN_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pAddedPorts);
        UtilPlstReleaseLocalPortList (pDeletedPorts);
        return SNMP_FAILURE;
    }
    if (VlanSetStaticEgressPorts (u4VlanIndex, pEgressPorts) == VLAN_FAILURE)
    {
        VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
        VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);
        UtilPlstReleaseLocalPortList (pAddedPorts);
        UtilPlstReleaseLocalPortList (pDeletedPorts);
        return SNMP_FAILURE;
    }
    if (pDeletedPorts != NULL)
    {
        for (u2Port = 0; u2Port <= VLAN_MAX_PORTS; u2Port++)
        {
            OSIX_BITLIST_IS_BIT_SET (pDeletedPorts, u2Port,
                                     CONTEXT_PORT_LIST_SIZE, bResult);
            if (bResult == VLAN_TRUE)
            {
                if (VlanVcmGetIfIndexFromLocalPort
                    (VLAN_CURR_CONTEXT_ID (), u2Port,
                     &u4IfIndex) == VCM_SUCCESS)
                {
                    CfaGetIfType (u4IfIndex, &u1IfType);
                }
                if (u1IfType != CFA_VXLAN_NVE)
                {
                    VlanHwFlushPortFdbId (VLAN_CURR_CONTEXT_ID (), u2Port,
                                          u4VlanIndex, VLAN_NO_OPTIMIZE);
                }
            }
        }
    }

    for (u1ProtoId = 0; u1ProtoId < VLAN_MAX_REG_MODULES; u1ProtoId++)
    {
        if (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo != NULL)
        {
            VlanBasicInfo.u2VlanId = (UINT2) u4VlanIndex;
            MEMCPY (VlanBasicInfo.LocalPortList, pAddedPorts,
                    sizeof (tLocalPortList));
            VlanBasicInfo.u1Action = VLAN_CB_ADD_EGRESS_PORTS;
            (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo) (&VlanBasicInfo);

            MEMCPY (VlanBasicInfo.LocalPortList, pDeletedPorts,
                    sizeof (tLocalPortList));
            VlanBasicInfo.u1Action = VLAN_CB_DELETE_EGRESS_PORTS;
            (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo) (&VlanBasicInfo);
        }
    }
    VlanSetPvlanMemberPorts ((tVlanId) u4VlanIndex, pAddedPorts, pDeletedPorts);
    VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
    VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);
#ifdef PVRST_WANTED
    if (pStVlanEntry != NULL)
    {
        pVlanEntry = VlanGetVlanEntry (pStVlanEntry->VlanId);
        i4Result = VlanUtilIsOnlyUntaggedPorts (pStVlanEntry->VlanId);
    }
    if (AstIsPvrstInstExist () == AST_FALSE)
    {
        if (((pVlanEntry != NULL)
             && (VLAN_IS_NULL_PORTLIST (pVlanEntry->EgressPorts) == VLAN_TRUE))
            || ((i4Result == VLAN_TRUE) &&
                (VlanUtilIsPvidVlanOfAnyPorts (pStVlanEntry->VlanId) ==
                 VLAN_FALSE)))

        {
            u1AddPortToInst = VLAN_FALSE;
            /* If there is no member port in the VLAN, delete the PVRST instance */
            if (PvrstMiVlanDeleteIndication (VLAN_CURR_CONTEXT_ID (),
                                             (tVlanId) u4VlanIndex) ==
                OSIX_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "PvrstMiVlanDeleteIndication returned failure....!\r\n");
            }
            UtilPlstReleaseLocalPortList (pAddedPorts);
            UtilPlstReleaseLocalPortList (pDeletedPorts);
            return SNMP_SUCCESS;

        }
    }
    if (u1AddPortToInst == VLAN_TRUE)
    {

        /* When a port is added, send indication to pvrst to add the port to 
           corresponding spanning-tree instance */
        PvrstSetTaggedPort (*(tLocalPortList *) pAddedPorts,
                            *(tLocalPortList *) pDeletedPorts,
                            (tVlanId) u4VlanIndex);
    }
    UNUSED_PARAM (u1AddPortToInst);
#endif
    UtilPlstReleaseLocalPortList (pAddedPorts);
    UtilPlstReleaseLocalPortList (pDeletedPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qVlanForbiddenEgressPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qVlanForbiddenEgressPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qVlanForbiddenEgressPorts (UINT4 u4VlanIndex,
                                     tSNMP_OCTET_STRING_TYPE * pForbiddenPorts)
{
    UINT1              *pEgressPortList = NULL;
    UINT1              *pForbidPortList = NULL;
    UINT1              *pAddedPorts = NULL;
    UINT1              *pDeletedPorts = NULL;
    tSNMP_OCTET_STRING_TYPE OldForbidPortList;
    tStaticVlanEntry   *pStVlanEntry = NULL;

/* Added for pseudo wire visibility */

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);

    if (pStVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pEgressPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pEgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanForbiddenEgressPorts: Error in allocating memory "
                  "for pEgressPortList\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pEgressPortList, 0, sizeof (tLocalPortList));

    pForbidPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pForbidPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanForbiddenEgressPorts: Error in allocating memory "
                  "for pForbidPortList\r\n");
        UtilPlstReleaseLocalPortList (pEgressPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pForbidPortList, 0, sizeof (tLocalPortList));

    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pAddedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanForbiddenEgressPorts: "
                  "Error in allocating memory for pAddedPorts\r\n");
        UtilPlstReleaseLocalPortList (pEgressPortList);
        UtilPlstReleaseLocalPortList (pForbidPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));

    pDeletedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDeletedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanForbiddenEgressPorts: "
                  "Error in allocating memory for pDeletedPorts\r\n");
        UtilPlstReleaseLocalPortList (pEgressPortList);
        UtilPlstReleaseLocalPortList (pForbidPortList);
        UtilPlstReleaseLocalPortList (pAddedPorts);
        return SNMP_FAILURE;
    }
    MEMSET (pDeletedPorts, 0, sizeof (tLocalPortList));

    MEMSET (&OldForbidPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    OldForbidPortList.pu1_OctetList = pForbidPortList;
    OldForbidPortList.i4_Length = sizeof (tLocalPortList);

    VLAN_GET_EGRESS_PORTS (pStVlanEntry, pEgressPortList);
    VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, OldForbidPortList.pu1_OctetList);

    VlanGetAddedAndDeletedPorts (&OldForbidPortList, pForbiddenPorts,
                                 pAddedPorts, pDeletedPorts);

    /* This will remove the newly configured egress ports from the
     * deleted port list. So that deleting the new egress pseudo wire or AC
     * port entry is avoided here.
     */
    VLAN_RESET_PORT_LIST (pDeletedPorts, pEgressPortList);
    UtilPlstReleaseLocalPortList (pEgressPortList);
    UtilPlstReleaseLocalPortList (pForbidPortList);

    /* Among the ports in the port list, PW and AC interfaces may be present.
     * PW and AC interfaces should be created while mapping to vlan.
     */
    if (VlanUtilCreateExtPorts (VLAN_CURR_CONTEXT_ID (),
                                pAddedPorts) == VLAN_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pAddedPorts);
        UtilPlstReleaseLocalPortList (pDeletedPorts);
        return SNMP_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pAddedPorts);

    /* Among the ports in the port list, PW and AC interfaces may be present.
     * PW and AC interfaces should be deleted  while un mapping from vlan.
     */
    if (VlanUtilDeleteExtPorts (VLAN_CURR_CONTEXT_ID (),
                                pDeletedPorts) == VLAN_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pDeletedPorts);
        return SNMP_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pDeletedPorts);
    if (VlanSetStaticForbiddenPorts (u4VlanIndex,
                                     pForbiddenPorts) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qVlanStaticUntaggedPorts
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qVlanStaticUntaggedPorts
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qVlanStaticUntaggedPorts (UINT4 u4VlanIndex,
                                    tSNMP_OCTET_STRING_TYPE * pUntagPorts)
{
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pInPorts = NULL;
    UINT1              *pAddPorts = NULL;
    UINT1              *pDelPorts = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    UINT1              *pPortList;
    tVlanId             VlanId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
    tSNMP_OCTET_STRING_TYPE OldMemberPorts;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVlanBasicInfo      VlanBasicInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4SetValFsDot1qVlanStaticPort = VLAN_NON_MEMBER_PORT;
    INT4                i4HwRetVal;
#ifdef PVRST_WANTED
    INT4                i4Result = 0;
#endif
    UINT2               u2Port;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1ProtoId = 0;
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticEgressPorts: Error in allocating memory "
                  "for pPortList\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanBasicInfo, 0, sizeof (tVlanBasicInfo));

    VlanId = (tVlanId) u4VlanIndex;

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }
    /* Obtain the prev. untagged port list for the vlan */
    OldMemberPorts.pu1_OctetList = pPortList;
    OldMemberPorts.i4_Length = sizeof (tLocalPortList);

    VLAN_GET_EGRESS_PORTS (pStVlanEntry, OldMemberPorts.pu1_OctetList);
    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldUntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticUntaggedPorts: "
                  "Error in allocating memory for pau1OldUntaggedPortList\r\n");
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    OldUntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldUntaggedPorts.pu1_OctetList = pau1OldUntaggedPortList;

    nmhGetDot1qVlanStaticUntaggedPorts (u4VlanIndex, &OldUntaggedPorts);
    pInPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pInPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticUntaggedPorts: Error in allocating memory "
                  "for pInPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }

    MEMSET (pInPorts, 0, sizeof (tLocalPortList));
    MEMCPY (pInPorts, pUntagPorts->pu1_OctetList, pUntagPorts->i4_Length);

    /* If the Ports are already configured return success silently */
    VLAN_IS_ALL_MATCHING_FOR_UNTAGGED (pStVlanEntry, pInPorts, u1Result);
    if (u1Result == VLAN_TRUE)
    {                            /* There is no change in the UnTagPorts ports */
        UtilPlstReleaseLocalPortList (pInPorts);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_SUCCESS;
    }

    if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {

        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            UtilPlstReleaseLocalPortList (pInPorts);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            UtilPlstReleaseLocalPortList (pPortList);
            return SNMP_FAILURE;
        }

        /* Need to program the hardware here */

        pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pu1LocalPortList == NULL)
        {
            UtilPlstReleaseLocalPortList (pInPorts);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            UtilPlstReleaseLocalPortList (pPortList);
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Memory allocation failed for tLocalPortList\n");
            return SNMP_FAILURE;
        }
        MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
        VLAN_GET_CURR_EGRESS_PORTS (pCurrEntry, pu1LocalPortList);

        i4HwRetVal = VlanHwAddVlanEntry (VlanId, pu1LocalPortList, pInPorts);
        UtilPlstReleaseLocalPortList (pu1LocalPortList);

        if (i4HwRetVal == VLAN_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pInPorts);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            UtilPlstReleaseLocalPortList (pPortList);
            return SNMP_FAILURE;
        }
        i4HwRetVal =
            VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (), VlanId,
                                   VLAN_UNREG_GROUPS, pCurrEntry->EgressPorts);

        if (i4HwRetVal == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }

    VLAN_OVERWRITE_UNTAGGED_PORTS (pStVlanEntry, pInPorts);

    VlanNotifyUntagMembersChangeToL2Iwf (VlanId, pInPorts);
    UtilPlstReleaseLocalPortList (pInPorts);

    /* Sending Trigger to MSR */
    pAddPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticUntaggedPorts: Error in allocating memory "
                  "for pAddPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pAddPorts, 0, sizeof (tLocalPortList));

    pDelPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pDelPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticUntaggedPorts: Error in allocating memory "
                  "for pDelPorts\r\n");
        UtilPlstReleaseLocalPortList (pAddPorts);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDelPorts, 0, sizeof (tLocalPortList));

    VlanGetAddedAndDeletedPorts (&OldUntaggedPorts,
                                 pUntagPorts, pAddPorts, pDelPorts);

    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    for (u1ProtoId = 0; u1ProtoId < VLAN_MAX_REG_MODULES; u1ProtoId++)
    {
        if (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo != NULL)
        {
            VlanBasicInfo.u2VlanId = VlanId;
            MEMCPY (VlanBasicInfo.LocalPortList, pAddPorts,
                    sizeof (tLocalPortList));
            VlanBasicInfo.u1Action = VLAN_CB_ADD_UNTAGGED_PORTS;
            (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo) (&VlanBasicInfo);

            MEMCPY (VlanBasicInfo.LocalPortList, pDelPorts,
                    sizeof (tLocalPortList));
            VlanBasicInfo.u1Action = VLAN_CB_DELETE_UNTAGGED_PORTS;
            (gaVlanRegTbl[u1ProtoId].pVlanBasicInfo) (&VlanBasicInfo);
        }
    }
    for (u2Port = 1; u2Port < VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_MEMBER_PORT (pAddPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            /* This is a new untagged Port. */
            i4SetValFsDot1qVlanStaticPort = VLAN_ADD_UNTAGGED_PORT;
        }

        VLAN_IS_MEMBER_PORT (pDelPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            VLAN_IS_EGRESS_PORT (pStVlanEntry, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                /* This means one untagged port got changed to tagged port. */
                i4SetValFsDot1qVlanStaticPort = VLAN_ADD_TAGGED_PORT;
            }
            else
            {
                i4SetValFsDot1qVlanStaticPort = VLAN_DEL_UNTAGGED_PORT;
            }
        }

        /* For port that are not added or deleted for this vlan, i4SetValFsDot1qVlanStaticPort
         * will be Non Member. */

        if (i4SetValFsDot1qVlanStaticPort != VLAN_NON_MEMBER_PORT)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qVlanStaticPort,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                              VLAN_CURR_CONTEXT_ID (), u4VlanIndex,
                              VLAN_GET_IFINDEX (u2Port),
                              i4SetValFsDot1qVlanStaticPort));
            VlanSelectContext (u4CurrContextId);
        }
        i4SetValFsDot1qVlanStaticPort = VLAN_NON_MEMBER_PORT;
    }
#ifdef PVRST_WANTED
    if (AstIsPvrstInstExist () == AST_FALSE)
    {
        i4Result = VlanUtilIsOnlyUntaggedPorts ((tVlanId) u4VlanIndex);
        pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanIndex);

        if (((pCurrEntry != NULL)
             && (VLAN_IS_NULL_PORTLIST (pCurrEntry->EgressPorts) == VLAN_TRUE))
            || ((i4Result == VLAN_TRUE)
                && (VlanUtilIsPvidVlanOfAnyPorts ((tVlanId) u4VlanIndex) ==
                    VLAN_FALSE)))
        {
            if (PvrstMiVlanDeleteIndication (VLAN_CURR_CONTEXT_ID (),
                                             (tVlanId) u4VlanIndex) ==
                OSIX_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "PvrstMiVlanDeleteIndication returned failure....!\r\n");
            }
            UtilPlstReleaseLocalPortList (pPortList);
            UtilPlstReleaseLocalPortList (pAddPorts);
            UtilPlstReleaseLocalPortList (pDelPorts);
            return SNMP_SUCCESS;

        }
    }
   /** In no ports case member port list will be NULL, Hence no need to send indication to add the 
                                        removed untagged port to spanning-tree instance **/
    if (!VLAN_IS_NULL_PORTLIST (OldMemberPorts.pu1_OctetList))
    {
        /* When a port is removed from untagged list, send an indication to pvrst 
           to add the port to spanning-tree instance */
        PvrstSetTaggedPort (*(tLocalPortList *) pDelPorts,
                            *(tLocalPortList *) pAddPorts,
                            (tVlanId) u4VlanIndex);
    }
#endif
    UtilPlstReleaseLocalPortList (pPortList);
    UtilPlstReleaseLocalPortList (pAddPorts);
    UtilPlstReleaseLocalPortList (pDelPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qVlanStaticRowStatus
Input       :  The Indices
               Dot1qVlanIndex

               The Object 
               setValDot1qVlanStaticRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qVlanStaticRowStatus (UINT4 u4VlanIndex, INT4 i4RowStatus)
{
    UINT1              *pu1LocalPortList = NULL;
    tPortList          *pEgressPorts = NULL;
    tPortList          *pForbiddenPorts = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tVlanId             VlanId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tMacAddr            McastAddr;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal;
#ifdef NPAPI_WANTED
    tVlanCurrEntry     *pCurrEntry = NULL;
#endif

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    VlanId = (tVlanId) u4VlanIndex;

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry != NULL)
    {

        if (pStVlanEntry->u1RowStatus == (UINT1) i4RowStatus)
        {

            return SNMP_SUCCESS;
        }

        if ((i4RowStatus == VLAN_CREATE_AND_WAIT) ||
            (i4RowStatus == VLAN_CREATE_AND_GO))
        {

            return SNMP_FAILURE;
        }
    }
    else
    {

        /* this static vlan entry is not there in the database */

        if ((i4RowStatus != VLAN_CREATE_AND_WAIT) &&
            (i4RowStatus != VLAN_CREATE_AND_GO))
        {

            return SNMP_FAILURE;
        }
    }
    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticRowStatus: "
                  "Error in allocating memory for pEgressPorts\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pEgressPorts, 0, sizeof (tPortList));

    pForbiddenPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbiddenPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qVlanStaticRowStatus: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return SNMP_FAILURE;
    }
    MEMSET (pForbiddenPorts, 0, sizeof (tPortList));

    switch (i4RowStatus)
    {

        case VLAN_CREATE_AND_WAIT:

            VLAN_PBB_PERF_MARK_START_TIME ();
            VLAN_PBB_PERF_RESET (gu4CreateBVLANTimeTaken);
            VLAN_PBB_PERF_RESET (gu4CreateBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_RESET (gu4VlanIsidTimeTaken);
            pStVlanEntry = (tStaticVlanEntry *) (VOID *)
                VlanCustGetBuf (VlanId, VLAN_ST_VLAN_ENTRY, sizeof
                                (tStaticVlanEntry));
            if (pStVlanEntry == NULL)
            {
                CLI_SET_ERR (CLI_VLAN_MAX_LIMIT_REACHED);
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Static VLAN Entry \n");
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            MEMSET (pStVlanEntry, 0, sizeof (tStaticVlanEntry));

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            pStVlanEntry->VlanId = VlanId;
            /* Initialize u1NameModifiedFlag with default value(VLAN_FALSE) */
            pStVlanEntry->u1NameModifiedFlag = VLAN_FALSE;
            pStVlanEntry->pNextNode = gpVlanContextInfo->pStaticVlanTable;
            pStVlanEntry->u1ServiceType = VLAN_E_LAN;
            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                pStVlanEntry->u2EgressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
            }
            else
            {
                pStVlanEntry->u2EgressEtherType = VLAN_PROTOCOL_ID;
            }
            gpVlanContextInfo->pStaticVlanTable = pStVlanEntry;

            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
            break;

        case VLAN_NOT_IN_SERVICE:

            if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {

                i4RetVal = VlanDelStVlanInfo (pStVlanEntry);

                if (i4RetVal == VLAN_FAILURE)
                {

                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                pStVlanEntry->pCurrEntry = NULL;

                /* Indicate GVRP about the VLAN Deletion */

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");

                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                VlanAttrRPSetVlanForbiddenPorts (VLAN_CURR_CONTEXT_ID (),
                                                 pStVlanEntry->VlanId,
                                                 gNullPortList,
                                                 pu1LocalPortList);

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId,
                                               gNullPortList, pu1LocalPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);

                if ((SNOOP_ENABLED ==
                     VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
                    || (SNOOP_ENABLED ==
                        VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID
                                                       ())))
                {
                    MEMSET (McastAddr, 0, sizeof (tMacAddr));

                    /* For MI the port list to the externa modules should be 
                       physical port list so convert and pass it */
                    VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pStVlanEntry,
                                                          *pForbiddenPorts);
                    VLAN_CONVERT_EGRESS_TO_IFPORTLIST (pStVlanEntry,
                                                       *pEgressPorts);

                    VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId, McastAddr,
                                               *pForbiddenPorts, *pEgressPorts,
                                               VLAN_UPDT_VLAN_PORTS);
                }
            }

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            break;

        case VLAN_ACTIVE:
            VLAN_PBB_PERF_MARK_START_TIME ();
            if (pStVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            i4RetVal = VlanAddCurrEntry (pStVlanEntry);

            if (i4RetVal == VLAN_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
            L2PvlanMappingInfo.InVlanId = (tVlanId) u4VlanIndex;
            L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

            VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

            /* Handle the pvlan association also. the port isloation 
             * table updation is handled inside this function */
            VlanUpdatePortIsolation (&L2PvlanMappingInfo);

            pStVlanEntry->u1RowStatus = VLAN_ACTIVE;

            /* 
             * Propagate the static vlan info eventhough the curr vlan entry
             * already exists.         
             */
            if (L2PvlanMappingInfo.u1VlanType == L2IWF_NORMAL_VLAN)
            {
                if (VLAN_MODULE_STATUS () == VLAN_ENABLED)
                {
                    pu1LocalPortList = UtilPlstAllocLocalPortList
                        (sizeof (tLocalPortList));
                    if (pu1LocalPortList == NULL)
                    {
                        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                  "Memory allocation failed for tLocalPortList\n");
                        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        return SNMP_FAILURE;
                    }
                    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                    VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                    VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                                   pStVlanEntry->VlanId,
                                                   pu1LocalPortList,
                                                   gNullPortList);
                    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                    VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                    VlanAttrRPSetVlanForbiddenPorts
                        (VLAN_CURR_CONTEXT_ID (),
                         pStVlanEntry->VlanId, pu1LocalPortList, gNullPortList);
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                }
            }

            if (SNOOP_ENABLED ==
                VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                || SNOOP_ENABLED ==
                VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
            {
                MEMSET (McastAddr, 0, sizeof (tMacAddr));

                /* For MI the port list to the externa modules should be 
                   physical port list so convert and pass it */
                VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pStVlanEntry,
                                                      *pForbiddenPorts);
                VLAN_CONVERT_EGRESS_TO_IFPORTLIST (pStVlanEntry, *pEgressPorts);

                VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                           pStVlanEntry->VlanId, McastAddr,
                                           *pEgressPorts,
                                           *pForbiddenPorts,
                                           VLAN_UPDT_VLAN_PORTS);
            }
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC,
                           VLAN_NAME, "Vlan %d created \n", VlanId);
            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
            VLAN_PBB_PERF_PRINT_TIME (gu4CreateBVLANTimeTaken);
            break;

        case VLAN_DESTROY:

#ifdef NPAPI_WANTED
            pCurrEntry = VlanGetVlanEntry (VlanId);
            if (pCurrEntry != NULL)
            {
                if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
                {
                    if (pCurrEntry->pVlanHwStatsEntry != NULL)
                    {
                        if (pCurrEntry->i4VlanCounterStatus == VLAN_ENABLED)
                        {
                            /* pCurrEntry->pVlanHwStatsEntry hold the statsId and number of entries
                             * values from the h/w while enabling the vlan stats counters.So before 
                             * release this buffer, we need to disable the vlan stats counters.
                             */
                            i4RetVal =
                                VlanFsMiVlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID
                                                            (),
                                                            (tVlanId) VlanId,
                                                            VLAN_DISABLED,
                                                            pCurrEntry->
                                                            pVlanHwStatsEntry);
                            if (i4RetVal == FNP_SUCCESS)
                            {
                                VLAN_RELEASE_BUF (VLAN_HW_STATS_ENTRY,
                                                  (UINT1 *) pCurrEntry->
                                                  pVlanHwStatsEntry);
                                pCurrEntry->i4VlanCounterStatus = VLAN_DISABLED;
                            }
                        }
                    }
                }
            }
#endif
            VLAN_PBB_PERF_MARK_START_TIME ();
            VLAN_PBB_PERF_RESET (gu4DeleteBVLANTimeTaken);
            /* Not to allow default VLAN entry deletion */
            if (pStVlanEntry->VlanId == VLAN_DEFAULT_PORT_VID)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {

                /* if this vlan is associated with pvlan, then
                 * delete that also. Default PVID configuration will taken care
                 * inside the VlanDelStVlanInfo function. So no need configure
                 * explicitly inside the below function. */
                VlanUpdtIsolationTblForVlanDel (pStVlanEntry->VlanId);
                VlanL2IwfDeletePvlanMapping (VLAN_CURR_CONTEXT_ID (),
                                             pStVlanEntry->VlanId);

#ifdef VXLAN_WANTED
                VxlanPortRemoveVniFromVlan (pStVlanEntry->VlanId);
#endif
#ifdef ICCH_WANTED
                VlanUpdtIsolationTblForIccl (pStVlanEntry->VlanId);
#endif /* ICCH_WANTED */
                i4RetVal = VlanDelStVlanInfo (pStVlanEntry);

                if (i4RetVal == VLAN_FAILURE)
                {

                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                /* Indicate GVRP about the VLAN Deletion */

#ifdef ICCH_WANTED
                VlanIcchSyncVlanFlush (pStVlanEntry->VlanId);
#endif

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                VlanAttrRPSetVlanForbiddenPorts (VLAN_CURR_CONTEXT_ID (),
                                                 pStVlanEntry->VlanId,
                                                 gNullPortList,
                                                 pu1LocalPortList);

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId,
                                               gNullPortList, pu1LocalPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }

            VlanDeleteStVlanEntry (pStVlanEntry);
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC,
                           VLAN_NAME, "Vlan %d deleted successfully \n",
                           pStVlanEntry->VlanId);

            VLAN_PBB_PERF_MARK_END_TIME (gu4DeleteBVLANTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4DeleteBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);
            VLAN_PBB_PERF_PRINT_VLANTIME (gu4DelVlanIsidTimeTaken);
            break;

        case VLAN_CREATE_AND_GO:
            pStVlanEntry = (tStaticVlanEntry *) (VOID *)
                VlanCustGetBuf (VlanId, VLAN_ST_VLAN_ENTRY, sizeof
                                (tStaticVlanEntry));

            if (pStVlanEntry == NULL)
            {
                CLI_SET_ERR (CLI_VLAN_MAX_LIMIT_REACHED);
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Static VLAN Entry \n");
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            MEMSET (pStVlanEntry, 0, sizeof (tStaticVlanEntry));

            if (VlanId == VLAN_DEF_VLAN_ID)
            {
                VLAN_SET_ALL_EGRESS_PORTS (pStVlanEntry);
                VLAN_SET_ALL_UNTAGGED_PORTS (pStVlanEntry);
            }

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            pStVlanEntry->VlanId = VlanId;
            pStVlanEntry->pNextNode = gpVlanContextInfo->pStaticVlanTable;
            gpVlanContextInfo->pStaticVlanTable = pStVlanEntry;

            i4RetVal = VlanAddCurrEntry (pStVlanEntry);
            if (i4RetVal == VLAN_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            pStVlanEntry->u1RowStatus = VLAN_ACTIVE;

            /* 
             * Propagate the static vlan info eventhough the curr vlan entry
             * already exists.         
             */
            if (VLAN_MODULE_STATUS () == VLAN_ENABLED)
            {
                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                VlanAttrRPSetVlanForbiddenPorts (VLAN_CURR_CONTEXT_ID (),
                                                 pStVlanEntry->VlanId,
                                                 pu1LocalPortList,
                                                 gNullPortList);

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId,
                                               pu1LocalPortList, gNullPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }

            if (SNOOP_ENABLED ==
                VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                || SNOOP_ENABLED ==
                VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
            {
                MEMSET (McastAddr, 0, sizeof (tMacAddr));

                /* For MI the port list to the externa modules should be 
                   physical port list so convert and pass it */
                VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pStVlanEntry,
                                                      *pForbiddenPorts);
                VLAN_CONVERT_EGRESS_TO_IFPORTLIST (pStVlanEntry, *pEgressPorts);

                VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                           pStVlanEntry->VlanId, McastAddr,
                                           *pEgressPorts, *pForbiddenPorts,
                                           VLAN_UPDT_VLAN_PORTS);
            }
            break;

        default:
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (VLAN_MODULE_STATUS () == VLAN_ENABLED)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qVlanStaticRowStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", VLAN_CURR_CONTEXT_ID (),
                          u4VlanIndex, i4RowStatus));
    }
    VlanSelectContext (u4CurrContextId);

    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qPortVlanTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDot1qPvid
Input       :  The Indices
               Dot1dBasePort

               The Object 
               setValDot1qPvid
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qPvid (INT4 i4Dot1dBasePort, UINT4 u4SetValDot1qPvid)
{
    if (VlanUtilSetPvid (i4Dot1dBasePort, u4SetValDot1qPvid) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qPortAcceptableFrameTypes
Input       :  The Indices
               Dot1dBasePort

               The Object 
               setValDot1qPortAcceptableFrameTypes
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qPortAcceptableFrameTypes (INT4 i4Dot1dBasePort, INT4 i4AccFrameType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pVlanPortEntry != NULL)
    {
        i4RetVal =
            VlanHwSetPortAccFrameType (VLAN_CURR_CONTEXT_ID (),
                                       (UINT2) i4Dot1dBasePort,
                                       (UINT1) i4AccFrameType);

        if (i4RetVal == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u1AccpFrmTypes = (UINT1) i4AccFrameType;

        L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (),
                                     (UINT2) i4Dot1dBasePort,
                                     pVlanPortEntry->u1AccpFrmTypes);

        /* Sending Trigger to MSR */

        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qPortAcceptableFrameTypes,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4Dot1dBasePort), i4AccFrameType));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDot1qPortIngressFiltering
Input       :  The Indices
               Dot1dBasePort

               The Object 
               setValDot1qPortIngressFiltering
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qPortIngressFiltering (INT4 i4Dot1dBasePort, INT4 i4IngFiltering)
{
    if (VlanUtilSetIngressFiltering (i4Dot1dBasePort,
                                     i4IngFiltering) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qLearningConstraintsTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1qConstraintType
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet

                The Object 
                setValDot1qConstraintType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qConstraintType (UINT4 u4Dot1qConstraintVlan,
                           INT4 i4Dot1qConstraintSet,
                           INT4 i4SetValDot1qConstraintType)
{
    UNUSED_PARAM (u4Dot1qConstraintVlan);
    UNUSED_PARAM (i4Dot1qConstraintSet);
    UNUSED_PARAM (i4SetValDot1qConstraintType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qConstraintStatus
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet

                The Object 
                setValDot1qConstraintStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qConstraintStatus (UINT4 u4Dot1qConstraintVlan,
                             INT4 i4Dot1qConstraintSet,
                             INT4 i4SetValDot1qConstraintStatus)
{
    UNUSED_PARAM (u4Dot1qConstraintVlan);
    UNUSED_PARAM (i4Dot1qConstraintSet);
    UNUSED_PARAM (i4SetValDot1qConstraintStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1qConstraintSetDefault
 Input       :  The Indices

                The Object 
                setValDot1qConstraintSetDefault
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qConstraintSetDefault (INT4 i4SetValDot1qConstraintSetDefault)
{
    UNUSED_PARAM (i4SetValDot1qConstraintSetDefault);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qConstraintTypeDefault
 Input       :  The Indices

                The Object 
                setValDot1qConstraintTypeDefault
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qConstraintTypeDefault (INT4 i4SetValDot1qConstraintTypeDefault)
{
    UNUSED_PARAM (i4SetValDot1qConstraintTypeDefault);
    return SNMP_SUCCESS;
}

/************************ P-MIB Set routines *******************************/

/****************************************************************************
 Function    :  nmhSetDot1dTrafficClassesEnabled
 Input       :  The Indices

                The Object 
                setValDot1dTrafficClassesEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dTrafficClassesEnabled (INT4 i4SetValDot1dTrafficClassesEnabled)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValDot1dTrafficClassesEnabled ==
        (INT4) gpVlanContextInfo->VlanInfo.u1TrfClassEnabled)
    {

        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValDot1dTrafficClassesEnabled)
    {

        case VLAN_SNMP_TRUE:

            if (VlanEnablePriorityModule () == VLAN_SUCCESS)
            {

                gpVlanContextInfo->VlanInfo.u1TrfClassEnabled = VLAN_SNMP_TRUE;
                i1RetVal = SNMP_SUCCESS;
            }
            break;

        case VLAN_SNMP_FALSE:

            if (VlanDisablePriorityModule () == VLAN_SUCCESS)
            {

                gpVlanContextInfo->VlanInfo.u1TrfClassEnabled = VLAN_SNMP_FALSE;
                i1RetVal = SNMP_SUCCESS;
            }
            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dTrafficClassesEnabled,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1dTrafficClassesEnabled));
    VlanSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortDefaultUserPriority
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1dPortDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortDefaultUserPriority (INT4 i4Dot1dBasePort, INT4 i4DefPriority)
{

    tVlanPortEntry     *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Default user priority can be set even if the priority module is
     * disabled, since all priorities will be mapping to the same traffic
     * class queue. Hence setting default user priority in the hardware
     * will not cause any problems. Also the VLAN module include priority 
     * in the tag header. So the priority changes should be given to h/w.
     */
    i4RetVal =
        VlanHwSetDefUserPriority (VLAN_CURR_CONTEXT_ID (),
                                  (UINT2) i4Dot1dBasePort, i4DefPriority);

    if (i4RetVal == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry->u1DefUserPriority = (UINT1) i4DefPriority;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortDefaultUserPriority,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1dBasePort), i4DefPriority));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortNumTrafficClasses
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1dPortNumTrafficClasses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortNumTrafficClasses (INT4 i4Dot1dBasePort,
                                  INT4 i4SetValDot1dPortNumTrafficClasses)
{

    tVlanPortEntry     *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT1               u1Index;
    UINT1               u1NumClasses;
    UINT4               u4Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
    {
        INT4                i4RetVal;

        i4RetVal =
            VlanHwSetPortNumTrafClasses (VLAN_CURR_CONTEXT_ID (),
                                         (UINT2) i4Dot1dBasePort,
                                         i4SetValDot1dPortNumTrafficClasses);

        if (i4RetVal == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

#ifndef NPAPI_WANTED
    VlanDrainAllPriQ ();
#endif

    pPortEntry->u1NumTrfClass = (UINT1) i4SetValDot1dPortNumTrafficClasses;

    u1NumClasses = (UINT1) (i4SetValDot1dPortNumTrafficClasses - 1);

    /* Set traffic class map table here */

    for (u1Index = 0; u1Index < VLAN_MAX_PRIORITY; u1Index++)
    {
        if (VlanHwSetTraffClassMap (VLAN_CURR_CONTEXT_ID (),
                                    (UINT2) i4Dot1dBasePort, (INT4) u1Index,
                                    (INT4)
                                    gau1PriTrfClassMap[u1Index][u1NumClasses])
            != VLAN_SUCCESS)
        {
            u4Port = VLAN_GET_IFINDEX (i4Dot1dBasePort);
            VLAN_TRC_ARG3 (VLAN_MOD_TRC,
                           CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                           "Traffic Class Mapping failed. Port = %d"
                           "Priority = %d, Traffic Class = %d\n",
                           u4Port, u1Index,
                           gau1PriTrfClassMap[u1Index][u1NumClasses]);

            return SNMP_FAILURE;
        }

        pPortEntry->au1TrfClassMap[u1Index]
            = gau1PriTrfClassMap[u1Index][u1NumClasses];
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortNumTrafficClasses,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1dPortNumTrafficClasses));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dRegenUserPriority
 Input       :  The Indices
                Dot1dBasePort
                Dot1dUserPriority

                The Object 
                setValDot1dRegenUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dRegenUserPriority (INT4 i4Dot1dBasePort, INT4 i4Dot1dUserPriority,
                              INT4 i4SetValDot1dRegenUserPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if ((i4Dot1dUserPriority < 0)
        || (i4Dot1dUserPriority > VLAN_HIGHEST_PRIORITY))
    {

        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
    {
        INT4                i4RetVal;

        i4RetVal =
            VlanHwSetRegenUserPriority (VLAN_CURR_CONTEXT_ID (),
                                        (UINT2) i4Dot1dBasePort,
                                        i4Dot1dUserPriority,
                                        i4SetValDot1dRegenUserPriority);

        if (i4RetVal == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pPortEntry->au1PriorityRegen[i4Dot1dUserPriority] =
        (UINT1) i4SetValDot1dRegenUserPriority;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dRegenUserPriority, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1dBasePort), i4Dot1dUserPriority,
                      i4SetValDot1dRegenUserPriority));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dTrafficClass
 Input       :  The Indices
                Dot1dBasePort
                Dot1dTrafficClassPriority

                The Object 
                setValDot1dTrafficClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dTrafficClass (INT4 i4Dot1dBasePort,
                         INT4 i4Dot1dTrafficClassPriority,
                         INT4 i4SetValDot1dTrafficClass)
{

    tVlanPortEntry     *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4Dot1dTrafficClassPriority < 0) ||
        (i4Dot1dTrafficClassPriority > VLAN_HIGHEST_PRIORITY))
    {

        return SNMP_FAILURE;
    }

    if (pPortEntry->u1NumTrfClass <= i4SetValDot1dTrafficClass)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_TRAFFIC_CLASS_ERR);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
    {
        INT4                i4RetVal;

        i4RetVal =
            VlanHwSetTraffClassMap (VLAN_CURR_CONTEXT_ID (),
                                    (UINT2) i4Dot1dBasePort,
                                    i4Dot1dTrafficClassPriority,
                                    i4SetValDot1dTrafficClass);

        if (i4RetVal == VLAN_FAILURE)
        {
            CLI_SET_ERR (CLI_VLAN_HW_ERR);
            return SNMP_FAILURE;
        }
    }

    pPortEntry->au1TrfClassMap[i4Dot1dTrafficClassPriority] =
        (UINT1) i4SetValDot1dTrafficClass;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dTrafficClass, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1dBasePort),
                      i4Dot1dTrafficClassPriority, i4SetValDot1dTrafficClass));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanStatus
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanStatus (INT4 i4SetValDot1qFutureVlanStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal;
    UINT4               u4AgeOutInt;
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (i4SetValDot1qFutureVlanStatus == (INT1) VLAN_MODULE_ADMIN_STATUS ())
    {
        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
    {
        VLAN_MODULE_ADMIN_STATUS () = (UINT1) i4SetValDot1qFutureVlanStatus;
        return SNMP_SUCCESS;
    }

    switch (i4SetValDot1qFutureVlanStatus)
    {

        case VLAN_ENABLED:

            if (VlanEnableVlan () == VLAN_SUCCESS)
            {
                VLAN_MODULE_ADMIN_STATUS () =
                    (UINT1) i4SetValDot1qFutureVlanStatus;
                i1RetVal = SNMP_SUCCESS;
            }

            u4AgeOutInt = VlanL2IwfGetAgeoutInt ();

            u4AgeOutInt = VLAN_SPLIT_AGEOUT_TIME (u4AgeOutInt);

            VLAN_START_TIMER (VLAN_AGEOUT_TIMER, u4AgeOutInt);

            break;

        case VLAN_DISABLED:

            if ((i4RetVal =
                 VlanHwVlanDisable (VLAN_CURR_CONTEXT_ID ())) == VLAN_FAILURE)
            {
                break;
            }
            if (i4RetVal == VLAN_DISABLED_BUT_NOT_DELETED)
            {
                VlanHwDelStaticInfo ();
            }

            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);

            if (VlanDisableVlan () == VLAN_SUCCESS)
            {
                i1RetVal = SNMP_SUCCESS;
            }

            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanStatus, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanStatus));
    VlanSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanShutdownStatus
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanShutdownStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1qFutureVlanShutdownStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetDot1qFutureVlanShutdownStatus (INT4 i4SetValDot1qFutureVlanShutdownStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal;
    INT1                i1RetVal = SNMP_SUCCESS;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VLAN_SYSTEM_CONTROL () == (UINT1) i4SetValDot1qFutureVlanShutdownStatus)
    {

        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValDot1qFutureVlanShutdownStatus)
    {
        case VLAN_SNMP_TRUE:

            if ((i4RetVal =
                 VlanHwVlanDisable (VLAN_CURR_CONTEXT_ID ())) == VLAN_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (i4RetVal == VLAN_DISABLED_BUT_NOT_DELETED)
            {
                VlanHwDelStaticInfo ();
            }

            VlanDeInit ();

            VlanL2IwfDeleteAllVlans (VLAN_CURR_CONTEXT_ID ());

            /* Notify MSR with the Vlan oids */
            VlanNotifyProtocolShutdownStatus (VLAN_CURR_CONTEXT_ID ());

            break;

        case VLAN_SNMP_FALSE:

            if (VlanInit () == VLAN_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
            }

            break;

        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanShutdownStatus,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanShutdownStatus));
    VlanSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                setValDot1qFutureVlanPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1qFutureVlanPortType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetDot1qFutureVlanPortType (INT4 i4Dot1qFutureVlanPort,
                               INT4 i4SetValDot1qFutureVlanPortType)
{
    if (VlanUtilHandlePortTypeDependency (i4Dot1qFutureVlanPort,
                                          i4SetValDot1qFutureVlanPortType) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanUtilSetVlanPortType (i4Dot1qFutureVlanPort,
                                 i4SetValDot1qFutureVlanPortType)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanMacBasedOnAllPorts
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanMacBasedOnAllPorts (INT4
                                         i4SetValDot1qFutureVlanMacBasedOnAllPorts)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetStatus = VLAN_SUCCESS;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VLAN_MAC_BASED == (UINT1) i4SetValDot1qFutureVlanMacBasedOnAllPorts)
    {
        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    i4RetStatus =
        VlanSetMacBasedStatusOnAllPorts ((UINT1)
                                         i4SetValDot1qFutureVlanMacBasedOnAllPorts);
    if (i4RetStatus == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanMacBasedOnAllPorts,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanMacBasedOnAllPorts));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts (INT4
                                               i4SetValDot1qFutureVlanPortProtoBasedOnAllPorts)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetStatus = VLAN_SUCCESS;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VLAN_PORT_PROTO_BASED ==
        (UINT1) i4SetValDot1qFutureVlanPortProtoBasedOnAllPorts)
    {
        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    i4RetStatus =
        VlanSetPortProtocolBasedStatusOnAllPorts ((UINT1)
                                                  i4SetValDot1qFutureVlanPortProtoBasedOnAllPorts);

    if (i4RetStatus == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Send the notification to LLDP to set the status for all the ports */
    VlanLldpApiNotifyProtoVlanStatus (0,
                                      (UINT1)
                                      i4SetValDot1qFutureVlanPortProtoBasedOnAllPorts);

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortProtoBasedOnAllPorts, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanPortProtoBasedOnAllPorts));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanBaseBridgeMode
 Input       :  The Indices

                The Object 
                i4SetValDot1qFutureVlanBaseBridgeMode
 Output      :  The Set Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanBaseBridgeMode (INT4 i4SetValDot1qFutureVlanBaseBridgeMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = VLAN_INIT_VAL;
    UINT4               u4SeqNum = VLAN_INIT_VAL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = VLAN_INIT_VAL;
#else
    tVlanCurrEntry     *pVlanEntry = NULL;
#endif

    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));

    if (VLAN_BASE_BRIDGE_MODE () ==
        (UINT4) i4SetValDot1qFutureVlanBaseBridgeMode)
    {
        return SNMP_SUCCESS;
    }

    if ((i4SetValDot1qFutureVlanBaseBridgeMode == DOT_1Q_VLAN_MODE) &&
        (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE))
    {
        /*Delete all static and dynamic Unicast info from Tables */
        VlanDeleteUCastEntry ();

        /*Delete all static and dynamic Multicast info from FDB Tables */
        VlanDeleteMcastEntry ();

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pVlanEntry = VLAN_GET_CURR_ENTRY (VLAN_DEF_VLAN_ID);

            if (pVlanEntry != NULL)
            {
                VlanUpdtVlanPortStatsTbl (pVlanEntry);
            }
        }
#endif
        /*De-initalise VLAN Module */
        VlanDeInit ();
        /* Starting the VLAN Module */
        if (VlanInit () == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /*Post a message to CFA Module to 
         *  (1) Create Default IVR Interface 
         *  (2) Change Default port as switch port. Previously in
         *  Transparent Mode it was changed to Router port for mgmt */
        CfaConfigBaseBridgeMode (CFA_ENABLED);
    }
    else
    {
        VLAN_BASE_BRIDGE_MODE () = DOT_1D_IN_PROGRESS_MODE;

        if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (VLAN_CURR_CONTEXT_ID ()) ==
            VLAN_SNMP_FALSE)
        {
            /*VLAN Module was not shutdown, So Shutting down */
            /*Shutting down the VLAN module */
#ifdef NPAPI_WANTED
            if ((i4RetVal =
                 VlanFsMiVlanHwVlanDisable (VLAN_CURR_CONTEXT_ID ())) ==
                FNP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (i4RetVal == FNP_VLAN_DISABLED_BUT_NOT_DELETED)
            {
                VlanHwDelStaticInfo ();
            }
#endif
            /*De-initalise VLAN Module */
            VlanDeInit ();
            VlanL2IwfDeleteAllVlans (VLAN_CURR_CONTEXT_ID ());

            /* Notify MSR with the Vlan oids */
            VlanNotifyProtocolShutdownStatus (VLAN_CURR_CONTEXT_ID ());

        }

        /* Starting the VLAN Module */
        if (VlanInit () == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
        /*Post a message to CFA Module to 
         *  (1) Default all L3 IVR Interfaces
         *  (2) Change Default port as Router port */
        CfaConfigBaseBridgeMode (CFA_DISABLED);
    }
#ifdef NPAPI_WANTED
    /* Set Base Bridge Mode in Hardware */
    if (VlanHwSetBaseBrgMode (VLAN_CURR_CONTEXT_ID (),
                              i4SetValDot1qFutureVlanBaseBridgeMode)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    VLAN_BASE_BRIDGE_MODE () = i4SetValDot1qFutureVlanBaseBridgeMode;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureBaseBridgeMode,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanBaseBridgeMode));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanSubnetBasedOnAllPorts
 Input       :  The Indices

                The Object
                setValDot1qFutureVlanSubnetBasedOnAllPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanSubnetBasedOnAllPorts (INT4
                                            i4SetValDot1qFutureVlanSubnetBasedOnAllPorts)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = VLAN_INIT_VAL;
    UINT4               u4SeqNum = VLAN_INIT_VAL;

    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));

    if (VLAN_SUBNET_BASED ==
        (UINT1) i4SetValDot1qFutureVlanSubnetBasedOnAllPorts)
    {
        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    if (VlanSetSubnetBasedStatusOnAllPorts ((UINT1)
                                            i4SetValDot1qFutureVlanSubnetBasedOnAllPorts)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanSubnetBasedOnAllPorts, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanSubnetBasedOnAllPorts));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortMacBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                setValDot1qFutureVlanPortMacBasedClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortMacBasedClassification (INT4 i4Dot1qFutureVlanPort,
                                                 INT4
                                                 i4SetValDot1qFutureVlanPortMacBasedClassification)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4Retval;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PORT_MAC_BASED (u2Port)
        == (UINT1) i4SetValDot1qFutureVlanPortMacBasedClassification)
    {
        /* Nothing to do */
        return SNMP_SUCCESS;
    }

    i4Retval =
        VlanHwSetMacBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                       (UINT1)
                                       i4SetValDot1qFutureVlanPortMacBasedClassification);

    if (i4Retval == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    VLAN_PORT_MAC_BASED (u2Port) =
        (UINT1) i4SetValDot1qFutureVlanPortMacBasedClassification;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortMacBasedClassification,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanPortMacBasedClassification));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortPortProtoBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                setValDot1qFutureVlanPortPortProtoBasedClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortPortProtoBasedClassification (INT4
                                                       i4Dot1qFutureVlanPort,
                                                       INT4
                                                       i4SetValDot1qFutureVlanPortPortProtoBasedClassification)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4Retval;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PORT_PORT_PROTOCOL_BASED (u2Port)
        == (UINT1) i4SetValDot1qFutureVlanPortPortProtoBasedClassification)
    {
        /* same as old value */
        return SNMP_SUCCESS;
    }

    i4Retval =
        VlanHwEnableProtoVlanOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                     (UINT1)
                                     i4SetValDot1qFutureVlanPortPortProtoBasedClassification);

    if (i4Retval == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) =
        (UINT1) i4SetValDot1qFutureVlanPortPortProtoBasedClassification;
    /* Send the notification to LLDP to notify the status change on the port. */
    VlanLldpApiNotifyProtoVlanStatus
        (u2Port,
         (UINT1) i4SetValDot1qFutureVlanPortPortProtoBasedClassification);

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortPortProtoBasedClassification,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (u2Port),
                      i4SetValDot1qFutureVlanPortPortProtoBasedClassification));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortMacMapVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                setValDot1qFutureVlanPortMacMapVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1qFutureVlanMacMapVid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetDot1qFutureVlanPortMacMapVid (INT4 i4Dot1qFutureVlanPort,
                                    tMacAddr Dot1qFutureVlanPortMacMapAddr,
                                    INT4 i4SetValDot1qFutureVlanPortMacMapVid)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;
    UINT4               u4Port = 0;
    INT4                i4RetVal;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4Port = (UINT4) i4Dot1qFutureVlanPort;

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           u4Port);

    if (pVlanMacMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* If the vid is being set just after row creation (before row has
     * been made active even once), only then the row status has to be
     * made to Not In Service. Else if vid of an existing entry is 
     * being modified, then do not change the row status */
    if (pVlanMacMapEntry->u1RowStatus != VLAN_ACTIVE)
    {
        pVlanMacMapEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    if (pVlanMacMapEntry->VlanId ==
        (tVlanId) i4SetValDot1qFutureVlanPortMacMapVid)
    {
        return SNMP_SUCCESS;
    }

    pVlanMacMapEntry->VlanId = (tVlanId) i4SetValDot1qFutureVlanPortMacMapVid;

    if ((u4Port <= VLAN_MAX_PORTS) &&
        (VLAN_PORT_MAC_BASED (u4Port) != VLAN_ENABLED))
    {
        return SNMP_SUCCESS;
    }
    if (pVlanMacMapEntry->u1RowStatus == VLAN_ACTIVE)
    {
        /* Update in hardware only if Row Status is already active, and
         * the VLAN ID is being changed for an existing entry. Else, 
         * when the Row Status is made active, the NPAPI will get 
         * invoked then. */
        i4RetVal = VlanHwAddPortMacVlanEntry
            (VLAN_CURR_CONTEXT_ID (),
             (UINT2) (VLAN_GET_IFINDEX ((UINT2) u4Port)),
             Dot1qFutureVlanPortMacMapAddr,
             pVlanMacMapEntry->VlanId, pVlanMacMapEntry->u1McastOption);

        if (i4RetVal != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "Updating MAC VLAN entry failed\n");
            return SNMP_FAILURE;
        }
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanPortMacMapVid,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      Dot1qFutureVlanPortMacMapAddr,
                      i4SetValDot1qFutureVlanPortMacMapVid));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortMacMapName
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                setValDot1qFutureVlanPortMacMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1qFutureVlanMacMapName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetDot1qFutureVlanPortMacMapName (INT4 i4Dot1qFutureVlanPort,
                                     tMacAddr Dot1qFutureVlanPortMacMapAddr,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValDot1qFutureVlanPortMacMapName)
{
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           (UINT4) i4Dot1qFutureVlanPort);

    if (pVlanMacMapEntry != NULL)
    {

        pVlanMacMapEntry->u1VlanNameLen =
            (UINT1) pSetValDot1qFutureVlanPortMacMapName->i4_Length;

        MEMCPY (pVlanMacMapEntry->au1VlanName,
                pSetValDot1qFutureVlanPortMacMapName->pu1_OctetList,
                pVlanMacMapEntry->u1VlanNameLen);

        /* Sending Trigger to MSR */

        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanPortMacMapName,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %s",
                          VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                          Dot1qFutureVlanPortMacMapAddr,
                          pSetValDot1qFutureVlanPortMacMapName));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortMacMapMcastBcastOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object
                setValDot1qFutureVlanPortMacMapMcastBcastOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortMacMapMcastBcastOption (INT4 i4Dot1qFutureVlanPort,
                                                 tMacAddr
                                                 Dot1qFutureVlanPortMacMapAddr,
                                                 INT4
                                                 i4SetValDot1qFutureVlanPortMacMapMcastBcastOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;
    UINT4               u4Port = 0;
    INT4                i4RetVal;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4Port = (UINT4) i4Dot1qFutureVlanPort;

    pVlanMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                           (UINT4) i4Dot1qFutureVlanPort);

    if (pVlanMacMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pVlanMacMapEntry->u1McastOption ==
        (UINT1) i4SetValDot1qFutureVlanPortMacMapMcastBcastOption)
    {
        return SNMP_SUCCESS;
    }
    pVlanMacMapEntry->u1McastOption = (UINT1)
        i4SetValDot1qFutureVlanPortMacMapMcastBcastOption;

    if ((u4Port <= VLAN_MAX_PORTS) &&
        (VLAN_PORT_MAC_BASED (u4Port) != VLAN_ENABLED))
    {
        return SNMP_SUCCESS;
    }

    if (pVlanMacMapEntry->u1RowStatus == VLAN_ACTIVE)
    {
        /* Update in hardware only if Row Status is already active, and
         * the Mcast/Bcast option is being changed for an existing entry. 
         * Else, when the Row Status is made active, the NPAPI will get 
         * invoked then. */
        i4RetVal = VlanHwAddPortMacVlanEntry
            (VLAN_CURR_CONTEXT_ID (),
             (UINT2) (VLAN_GET_IFINDEX ((UINT2) u4Port)),
             Dot1qFutureVlanPortMacMapAddr,
             pVlanMacMapEntry->VlanId, pVlanMacMapEntry->u1McastOption);

        if (i4RetVal != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "Updating MAC VLAN entry failed\n");
            return SNMP_FAILURE;
        }
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortMacMapMcastBcastOption,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      Dot1qFutureVlanPortMacMapAddr,
                      i4SetValDot1qFutureVlanPortMacMapMcastBcastOption));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortMacMapRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                setValDot1qFutureVlanPortMacMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1qFutureVlanMacMapRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetDot1qFutureVlanPortMacMapRowStatus (INT4 i4Dot1qFutureVlanPort,
                                          tMacAddr
                                          Dot1qFutureVlanPortMacMapAddr,
                                          INT4
                                          i4SetValDot1qFutureVlanPortMacMapRowStatus)
{
    tVlanMacMapEntry   *pMacMapEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4Port = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4Port = (UINT4) i4Dot1qFutureVlanPort;
    pMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr, u4Port);

    if (pMacMapEntry != NULL)
    {
        /* Entry Already present */
        if (pMacMapEntry->u1RowStatus ==
            (UINT1) i4SetValDot1qFutureVlanPortMacMapRowStatus)
        {
            return SNMP_SUCCESS;
        }

        /* Return failure  if row status is create and 
         * wait */
        if (i4SetValDot1qFutureVlanPortMacMapRowStatus == VLAN_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* mac map entry is not present */
        if (i4SetValDot1qFutureVlanPortMacMapRowStatus != VLAN_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValDot1qFutureVlanPortMacMapRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pMacMapEntry =
                (tVlanMacMapEntry *) (VOID *) VLAN_GET_BUF (VLAN_MAC_MAP_ENTRY,
                                                            sizeof
                                                            (tVlanMacMapEntry));

            if (pMacMapEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "New MAC Map Entry allocation failed\n");

                return SNMP_FAILURE;
            }

            gu2VlanMacMapCount++;

            MEMSET (pMacMapEntry, 0, sizeof (tVlanMacMapEntry));

            VLAN_CPY_MAC_ADDR (pMacMapEntry->MacAddr,
                               Dot1qFutureVlanPortMacMapAddr);

            pMacMapEntry->u4Port = u4Port;

            pMacMapEntry->VlanId = VLAN_DEF_VLAN_ID;

            pMacMapEntry->u1McastOption = VLAN_MCAST_USE;

            pPortEntry = VLAN_GET_PORT_ENTRY (pMacMapEntry->u4Port);

            (pPortEntry->u2MacVlanEntryCount)++;

            VlanAddMacMapEntry (pMacMapEntry);

            pMacMapEntry->u1RowStatus = VLAN_NOT_READY;

            break;

        case VLAN_NOT_IN_SERVICE:

            pMacMapEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            /* The purpose of deleting from hardware while setting Row status
             * to Not In Service itself, is because, when a row is destroyed
             * on an entry that is in Not In Service, the previous status
             * could have been either Not Ready or Active. If Not Ready, then
             * no entry would have been created in the hardware at all. But if
             * the previous status had been active, then entry would have been
             * created in the hardware. There is no way to distinguish the 
             * previous row status. Hence whenever a row is set to Not In 
             * Service (this is possible only if the previous status was Active, 
             * the entry is destroyed from the hardware. */
            if (VLAN_PORT_MAC_BASED (pMacMapEntry->u4Port) != VLAN_ENABLED)
            {
                break;
            }
            i4RetVal = VlanHwDeletePortMacVlanEntry
                (VLAN_CURR_CONTEXT_ID (),
                 (UINT2) (VLAN_GET_IFINDEX ((UINT2) u4Port)),
                 Dot1qFutureVlanPortMacMapAddr);
            if (i4RetVal != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "Deleting MAC VLAN entry in hardware failed \n");
                return SNMP_FAILURE;
            }
            break;

        case VLAN_ACTIVE:

            pMacMapEntry->u1RowStatus = VLAN_ACTIVE;

            if ((u4Port <= VLAN_MAX_PORTS) &&
                (VLAN_PORT_MAC_BASED (u4Port) != VLAN_ENABLED))
            {
                break;
            }

            i4RetVal = VlanHwAddPortMacVlanEntry
                (VLAN_CURR_CONTEXT_ID (),
                 (UINT2) (VLAN_GET_IFINDEX ((UINT2) u4Port)),
                 Dot1qFutureVlanPortMacMapAddr,
                 pMacMapEntry->VlanId, pMacMapEntry->u1McastOption);

            if (i4RetVal != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "Updating MAC VLAN entry failed\n");
                return SNMP_FAILURE;
            }

            break;

        case VLAN_DESTROY:

            pPortEntry = VLAN_GET_PORT_ENTRY (pMacMapEntry->u4Port);

            (pPortEntry->u2MacVlanEntryCount)--;

            gu2VlanMacMapCount--;

            VlanDeleteMacMapEntry (pMacMapEntry);
            break;

        default:
            return SNMP_FAILURE;

    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortMacMapRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      Dot1qFutureVlanPortMacMapAddr,
                      i4SetValDot1qFutureVlanPortMacMapRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanDebug
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanDebug (INT4 i4SetValDot1qFutureVlanDebug)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    gpVlanContextInfo->u4VlanDbg = (UINT4) i4SetValDot1qFutureVlanDebug;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanDebug, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanDebug));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValDot1qFutureVlanDebug);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanLearningMode
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanLearningMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanLearningMode (INT4 i4SetValDot1qFutureVlanLearningMode)
{
    tVlanId             ScanVlanId;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanCurrEntry     *pScanVlanEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4VlanFid = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4NewFidIndex = 0;
    UINT2               u2VlanIndex;
    UINT2               u2Port;
    UINT1               u1FdbEntryPresent;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (gpVlanContextInfo->VlanInfo.u1VlanLearningType ==
        (UINT1) i4SetValDot1qFutureVlanLearningMode)
    {
        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    if (VlanHwSetVlanLearningType (VLAN_CURR_CONTEXT_ID (),
                                   i4SetValDot1qFutureVlanLearningMode)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        /* Delete ALL fdb entries learnt on this port */
#ifdef NPAPI_WANTED
        VlanHwFlushPort (VLAN_CURR_CONTEXT_ID (), u2Port, VLAN_OPTIMIZE);
#else
        VlanHandleDeleteFdbEntries (u2Port);
#endif
    }

    for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
    {
        u1FdbEntryPresent = VLAN_FALSE;

        pVlanEntry = VlanGetVlanEntry (u2VlanIndex);

        if (((UINT1) i4SetValDot1qFutureVlanLearningMode
             == VLAN_INDEP_LEARNING) ||
            (((UINT1) i4SetValDot1qFutureVlanLearningMode
              == VLAN_HYBRID_LEARNING) &&
             (gpVlanContextInfo->VlanInfo.u1DefConstType ==
              VLAN_INDEP_LEARNING)))
        {
            u4VlanFid = (UINT4) u2VlanIndex;
        }
        else if (((UINT1) i4SetValDot1qFutureVlanLearningMode
                  == VLAN_SHARED_LEARNING) ||
                 (((UINT1) i4SetValDot1qFutureVlanLearningMode
                   == VLAN_HYBRID_LEARNING)
                  && (gpVlanContextInfo->VlanInfo.u1DefConstType ==
                      VLAN_SHARED_LEARNING)))
        {
            u4VlanFid = VLAN_SHARED_DEF_FDBID;
        }

        if (u4VlanFid == VlanL2IwfMiGetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                                                  (tVlanId) u2VlanIndex))
        {
            continue;
        }
        if (pVlanEntry != NULL)
        {
            VLAN_SCAN_VLAN_TABLE (ScanVlanId)
            {
                pScanVlanEntry = VLAN_GET_CURR_ENTRY (ScanVlanId);

                if (pScanVlanEntry != NULL)
                {
                    if (pScanVlanEntry != pVlanEntry)
                    {
                        pFidEntry = VLAN_GET_FID_ENTRY
                            (pScanVlanEntry->u4FidIndex);
                        if (u4VlanFid == pFidEntry->u4Fid)
                        {
                            u1FdbEntryPresent = VLAN_TRUE;
                            u4NewFidIndex = pScanVlanEntry->u4FidIndex;
                            break;
                        }
                    }
                }
            }
            if (VLAN_FALSE == u1FdbEntryPresent)
            {
                u4NewFidIndex = VlanGetFreeFidIndex (u4VlanFid);

                if (VLAN_INVALID_FID_INDEX == u4NewFidIndex)
                {
                    return SNMP_FAILURE;
                }
            }

            if (VLAN_FAILURE == VlanRemapFidIndexForVlan
                (pVlanEntry, u4NewFidIndex))
            {
                /* Free only when this is newly created FidIndex */
                if (VLAN_FALSE == u1FdbEntryPresent)
                {
                    VlanFreeFidIndex (u4NewFidIndex);
                }
                return SNMP_FAILURE;
            }
        }
        VlanL2IwfSetVlanFdbId (VLAN_CURR_CONTEXT_ID (), u2VlanIndex, u4VlanFid);
    }
    gpVlanContextInfo->VlanInfo.u1VlanLearningType =
        (UINT1) i4SetValDot1qFutureVlanLearningMode;

    gpVlanContextInfo->VlanInfo.u1DefConstType = VLAN_INDEP_LEARNING;
    /* Sending Trigger to MSR */

    VlanL2IwfSetVlanLearningType (VLAN_CURR_CONTEXT_ID (),
                                  (UINT1) i4SetValDot1qFutureVlanLearningMode);

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanLearningMode,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanLearningMode));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetDot1qFutureVlanHybridTypeDefault
Input       :  The Indices

The Object 
setValDot1qFutureVlanHybridTypeDefault
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qFutureVlanHybridTypeDefault (INT4
                                        i4SetValDot1qFutureVlanHybridTypeDefault)
{
    tVlanId             ScanVlanId;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanCurrEntry     *pScanVlanEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4VlanFid;
    UINT4               u4NewFidIndex = 0;
    UINT2               u2VlanIndex;
    UINT2               u2Port;
    UINT1               u1VlanLearningMode;
    UINT1               u1FdbEntryPresent;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (gpVlanContextInfo->VlanInfo.u1DefConstType ==
        (UINT1) i4SetValDot1qFutureVlanHybridTypeDefault)
    {
        return SNMP_SUCCESS;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_SUCCESS;
    }

    /* When the switch is restarted in hybrid mode
     * with a config-save-restore of this object
     * then the Fdb Id should be updated according 
     * to the default hybrid type.*/
    u1VlanLearningMode = VlanGetVlanLearningMode ();

    if (u1VlanLearningMode == VLAN_HYBRID_LEARNING)
    {
        /* Dynamic Entries are deleted when the 
         * default hybrid  mode is changed */
        VLAN_SCAN_PORT_TABLE (u2Port)
        {
            /* Delete ALL fdb entries learnt on this port */
#ifdef NPAPI_WANTED
            VlanHwFlushPort (VLAN_CURR_CONTEXT_ID (), u2Port, VLAN_OPTIMIZE);
#else
            VlanHandleDeleteFdbEntries (u2Port);
#endif
        }

        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
        {
            u1FdbEntryPresent = VLAN_FALSE;

            pVlanEntry = VlanGetVlanEntry (u2VlanIndex);

            if ((UINT1) i4SetValDot1qFutureVlanHybridTypeDefault
                == VLAN_INDEP_LEARNING)
            {
                u4VlanFid = (UINT4) u2VlanIndex;
            }
            else
            {
                u4VlanFid = VLAN_SHARED_DEF_FDBID;
            }

            if (u4VlanFid == VlanL2IwfMiGetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                                                      (tVlanId) u2VlanIndex))
            {
                continue;
            }
            if (pVlanEntry != NULL)
            {
                VLAN_SCAN_VLAN_TABLE (ScanVlanId)
                {
                    pScanVlanEntry = VLAN_GET_CURR_ENTRY (ScanVlanId);

                    if (pScanVlanEntry != NULL)
                    {
                        if (pScanVlanEntry != pVlanEntry)
                        {
                            pFidEntry = VLAN_GET_FID_ENTRY
                                (pScanVlanEntry->u4FidIndex);

                            if (u4VlanFid == pFidEntry->u4Fid)
                            {
                                u1FdbEntryPresent = VLAN_TRUE;
                                u4NewFidIndex = pScanVlanEntry->u4FidIndex;
                                break;
                            }
                        }
                    }
                }
                if (VLAN_FALSE == u1FdbEntryPresent)
                {

                    u4NewFidIndex = VlanGetFreeFidIndex (u4VlanFid);

                    if (VLAN_INVALID_FID_INDEX == u4NewFidIndex)
                    {
                        return SNMP_FAILURE;
                    }
                }
                if (VLAN_FAILURE == VlanRemapFidIndexForVlan
                    (pVlanEntry, u4NewFidIndex))
                {
                    /* Free only when this is newly created FidIndex */
                    if (VLAN_FALSE == u1FdbEntryPresent)
                    {
                        VlanFreeFidIndex (u4NewFidIndex);
                    }
                    return SNMP_FAILURE;
                }
            }
            VlanL2IwfSetVlanFdbId (VLAN_CURR_CONTEXT_ID (), u2VlanIndex,
                                   u4VlanFid);
        }
    }
    gpVlanContextInfo->VlanInfo.u1DefConstType =
        (UINT1) i4SetValDot1qFutureVlanHybridTypeDefault;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanHybridTypeDefault,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanHybridTypeDefault));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanGlobalMacLearningStatus
 Input       :  The Indices

                The Object
                setValDot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanGlobalMacLearningStatus (INT4
                                              i4SetValDot1qFutureVlanGlobalMacLearningStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanSetGlobalMacLearnLearningStatus
        (i4SetValDot1qFutureVlanGlobalMacLearningStatus) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanGlobalMacLearningStatus, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureVlanGlobalMacLearningStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria (INT4
                                                     i4SetValDot1qFutureVlanApplyEnhancedFilteringCriteria)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VLAN_CURR_CTX_ENHANCE_FILTERING_STATUS () !=
        i4SetValDot1qFutureVlanApplyEnhancedFilteringCriteria)
    {

        VLAN_CURR_CTX_ENHANCE_FILTERING_STATUS () =
            i4SetValDot1qFutureVlanApplyEnhancedFilteringCriteria;
        /* Sending Trigger to MSR */
        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

        VlanL2IwfUpdateEnhFilterStatus (u4CurrContextId,
                                        (BOOL1)
                                        i4SetValDot1qFutureVlanApplyEnhancedFilteringCriteria);

        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                          i4SetValDot1qFutureVlanApplyEnhancedFilteringCriteria));
        VlanSelectContext (u4CurrContextId);

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanSwStatsEnabled
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanSwStatsEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanSwStatsEnabled (INT4 i4SetValDot1qFutureVlanSwStatsEnabled)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (gu1SwStatsEnabled != i4SetValDot1qFutureVlanSwStatsEnabled)
    {
        gu1SwStatsEnabled = i4SetValDot1qFutureVlanSwStatsEnabled;

        RM_GET_SEQ_NUM (&u4SeqNum);
        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanSwStatsEnabled, u4SeqNum,
                              FALSE, VlanLock, VlanUnLock, 0, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                          i4SetValDot1qFutureVlanSwStatsEnabled));
        VlanSelectContext (u4CurrContextId);

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1vProtocolGroupId
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue

                The Object 
                setValDot1vProtocolGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1vProtocolGroupId (INT4 i4Dot1vProtocolTemplateFrameType,
                            tSNMP_OCTET_STRING_TYPE *
                            pDot1vProtocolTemplateProtocolValue,
                            INT4 i4SetValDot1vProtocolGroupId)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];

    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);
    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;
    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry == NULL)
    {
        /*
         * No Group entry is present in Protocol Group Database 
         */
        return SNMP_FAILURE;
    }

    pVlanProtGrpEntry->u4ProtGrpId = (UINT4) i4SetValDot1vProtocolGroupId;

    if (pVlanProtGrpEntry->u1RowStatus == VLAN_NOT_READY)
    {
        /* Mandatory objects set...Can be moved to ACTIVE by the management
         * station */
        pVlanProtGrpEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1vProtocolGroupId, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i", VLAN_CURR_CONTEXT_ID (),
                      i4Dot1vProtocolTemplateFrameType,
                      pDot1vProtocolTemplateProtocolValue,
                      i4SetValDot1vProtocolGroupId));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1vProtocolGroupRowStatus
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue

                The Object 
                setValDot1vProtocolGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1vProtocolGroupRowStatus (INT4 i4Dot1vProtocolTemplateFrameType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pDot1vProtocolTemplateProtocolValue,
                                   INT4 i4SetValDot1vProtocolGroupRowStatus)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];
    INT4                i4Status = VLAN_SUCCESS;

    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);
    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;

    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry != NULL)
    {
        /* Entry Already present */
        if (pVlanProtGrpEntry->u1RowStatus ==
            (UINT1) i4SetValDot1vProtocolGroupRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValDot1vProtocolGroupRowStatus)
    {

        case VLAN_CREATE_AND_WAIT:

            pVlanProtGrpEntry =
                (tVlanProtGrpEntry *) (VOID *)
                VLAN_GET_BUF (VLAN_PROTO_GROUP_ENTRY,
                              sizeof (tVlanProtGrpEntry));

            if (pVlanProtGrpEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Protocol Group Entry \n");
                CLI_SET_ERR (CLI_VLAN_NO_PROTO_GRP_ERR);
                return SNMP_FAILURE;
            }

            MEMSET (pVlanProtGrpEntry, 0, sizeof (tVlanProtGrpEntry));

            pVlanProtGrpEntry->VlanProtoTemplate.u1TemplateProtoFrameType
                = (UINT1) i4Dot1vProtocolTemplateFrameType;

            pVlanProtGrpEntry->u1RowStatus = VLAN_NOT_READY;
            VlanStoreProtoValue (u1Length, au1Value, pVlanProtGrpEntry);
            VlanAddProtGrpEntry (pVlanProtGrpEntry);

            break;

        case VLAN_NOT_IN_SERVICE:

            if (pVlanProtGrpEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Protocol Group Entry \n");
                CLI_SET_ERR (CLI_VLAN_NO_PROTO_GRP_ERR);
                return SNMP_FAILURE;
            }
            if (pVlanProtGrpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                /* 
                 * This function takes care of giving the deletion
                 * info to hardware and deletion in software too.
                 */
                i4Status = VlanCheckAndDelProtGrpInfo (pVlanProtGrpEntry);

                if (i4Status == VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

            pVlanProtGrpEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            break;

        case VLAN_ACTIVE:

            if (pVlanProtGrpEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Protocol Group Entry \n");
                CLI_SET_ERR (CLI_VLAN_NO_PROTO_GRP_ERR);
                return SNMP_FAILURE;
            }
            /* Check the group ID for all the available ports 
             * And update the H/W table
             */
            VlanAddNewGroupInfoInHw (pVlanProtGrpEntry);

            pVlanProtGrpEntry->u1RowStatus = VLAN_ACTIVE;
            break;

        case VLAN_DESTROY:

            if (pVlanProtGrpEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Protocol Group Entry \n");
                CLI_SET_ERR (CLI_VLAN_NO_PROTO_GRP_ERR);
                return SNMP_FAILURE;
            }
            if (pVlanProtGrpEntry->u1RowStatus == VLAN_ACTIVE)
            {
                /* 
                 * This function takes care of giving the deletion
                 * info to hardware and deletion in software too.
                 */
                i4Status = VlanCheckAndDelProtGrpInfo (pVlanProtGrpEntry);

                if (i4Status == VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

            /* Just delete the entry */
            VlanDeleteProtGrpEntry (pVlanProtGrpEntry);
            break;

        default:
            return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1vProtocolGroupRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i", VLAN_CURR_CONTEXT_ID (),
                      i4Dot1vProtocolTemplateFrameType,
                      pDot1vProtocolTemplateProtocolValue,
                      i4SetValDot1vProtocolGroupRowStatus));
    VlanSelectContext (u4CurrContextId);
    KW_FALSEPOSITIVE_FIX (pVlanProtGrpEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1vProtocolPortGroupVid
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId

                The Object 
                setValDot1vProtocolPortGroupVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1vProtocolPortGroupVid (INT4 i4Dot1dBasePort,
                                 INT4 i4Dot1vProtocolPortGroupId,
                                 INT4 i4SetValDot1vProtocolPortGroupVid)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT2               u2BasePort = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2BasePort = (UINT2) i4Dot1dBasePort;

    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2BasePort,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry == NULL)
    {
        /* Entry not yet created */
        return SNMP_FAILURE;
    }

    if (pVlanPortVidSetEntry->VlanId == (tVlanId)
        i4SetValDot1vProtocolPortGroupVid)
    {
        pVlanPortVidSetEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
        return SNMP_SUCCESS;
    }

    if (pVlanPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
    {
        /* Modifying the existing entry. Hence delete the existing protocol
         * maps in the hardware and add new protocol maps. */
        VlanUpdateGroupVlanMapInfoInHw ((UINT4) i4Dot1dBasePort,
                                        (UINT4) i4Dot1vProtocolPortGroupId,
                                        pVlanPortVidSetEntry->VlanId,
                                        VLAN_DELETE);

        /* Add the new protocol maps with the new VLAN ID */
        VlanUpdateGroupVlanMapInfoInHw ((UINT4) i4Dot1dBasePort,
                                        (UINT4) i4Dot1vProtocolPortGroupId,
                                        (tVlanId)
                                        i4SetValDot1vProtocolPortGroupVid,
                                        VLAN_ADD);
        /* Send the notification to LLDP to modify the existing entry */
        VlanLldpApiNotifyProtoVlanId (u2BasePort, (tVlanId)
                                      i4SetValDot1vProtocolPortGroupVid,
                                      VLAN_UPDATE,
                                      pVlanPortVidSetEntry->VlanId);
    }

    pVlanPortVidSetEntry->VlanId = (UINT2) i4SetValDot1vProtocolPortGroupVid;
    if (pVlanPortVidSetEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pVlanPortVidSetEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1vProtocolPortGroupVid, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1dBasePort),
                      i4Dot1vProtocolPortGroupId,
                      i4SetValDot1vProtocolPortGroupVid));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1vProtocolPortRowStatus
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId

                The Object 
                setValDot1vProtocolPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1vProtocolPortRowStatus (INT4 i4Dot1dBasePort,
                                  INT4 i4Dot1vProtocolPortGroupId,
                                  INT4 i4SetValDot1vProtocolPortRowStatus)
{

    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2BasePort = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2BasePort = (UINT2) i4Dot1dBasePort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2BasePort);

    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2BasePort,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry != NULL)
    {
        /* Entry Already present */

        if (pVlanPortVidSetEntry->u1RowStatus ==
            (UINT1) i4SetValDot1vProtocolPortRowStatus)
        {

            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValDot1vProtocolPortRowStatus)
    {

        case VLAN_CREATE_AND_WAIT:

            pVlanPortVidSetEntry =
                (tVlanPortVidSet *) (VOID *)
                VLAN_GET_BUF (VLAN_PORT_PROTO_ENTRY, sizeof (tVlanPortVidSet));

            if (pVlanPortVidSetEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Port Protocol Entry \n");

                return SNMP_FAILURE;
            }

            MEMSET (pVlanPortVidSetEntry, 0, sizeof (tVlanPortVidSet));

            pVlanPortVidSetEntry->u4ProtGrpId
                = (UINT4) i4Dot1vProtocolPortGroupId;
            pVlanPortVidSetEntry->VlanId = VLAN_DEF_VLAN_ID;

            pVlanPortVidSetEntry->u1RowStatus = VLAN_NOT_READY;

            VlanAddPortProtoVidSetEntry (pVlanPortEntry, pVlanPortVidSetEntry);

            break;

        case VLAN_NOT_IN_SERVICE:

            if (pVlanPortVidSetEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Port Protocol Entry \n");

                return SNMP_FAILURE;
            }
            if (pVlanPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
            {
                /* Need to program the hardware here */
                VlanUpdateGroupVlanMapInfoInHw ((UINT4) i4Dot1dBasePort,
                                                (UINT4)
                                                i4Dot1vProtocolPortGroupId,
                                                pVlanPortVidSetEntry->VlanId,
                                                VLAN_DELETE);
            }
            pVlanPortVidSetEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            /* Existing protocol vlan has been deleted from hardware, hence the
             * delete indication is given to LLDP. This is needed in case of
             * modifiying an existing entry with a new value.
             */
            VlanLldpApiNotifyProtoVlanId (u2BasePort,
                                          pVlanPortVidSetEntry->VlanId,
                                          VLAN_DELETE, 0);
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncProtoVlanSwUpdate (VLAN_CURR_CONTEXT_ID (),
                                          i4Dot1vProtocolPortGroupId,
                                          i4Dot1dBasePort, VLAN_DELETE);
#endif /*L2RED_WANTED */

            break;

        case VLAN_ACTIVE:

            if (pVlanPortVidSetEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Port Protocol Entry \n");

                return SNMP_FAILURE;
            }
            /* Need to program the hardware here */
            VlanUpdateGroupVlanMapInfoInHw ((UINT4) i4Dot1dBasePort,
                                            (UINT4) i4Dot1vProtocolPortGroupId,
                                            pVlanPortVidSetEntry->VlanId,
                                            VLAN_ADD);
            pVlanPortVidSetEntry->u1RowStatus = VLAN_ACTIVE;
            /* This takes care of notifying LLDP about both the new addition 
             * as well as the modication of existing protocol vlan with new
             * value. Indication for deleting the old value has been given 
             * while setting the status as VLAN_NOT_IN_SERVICE.
             */
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncProtoVlanSwUpdate (VLAN_CURR_CONTEXT_ID (),
                                          i4Dot1vProtocolPortGroupId,
                                          i4Dot1dBasePort, VLAN_ADD);
#endif /*L2RED_WANTED */

            VlanLldpApiNotifyProtoVlanId (u2BasePort,
                                          pVlanPortVidSetEntry->VlanId,
                                          VLAN_ADD, 0);

            break;

        case VLAN_DESTROY:
            if (pVlanPortVidSetEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No new Port Protocol Entry \n");

                return SNMP_FAILURE;
            }
            if (pVlanPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
            {
                /* Need to program the hardware here */
                VlanUpdateGroupVlanMapInfoInHw ((UINT4) i4Dot1dBasePort,
                                                (UINT4)
                                                i4Dot1vProtocolPortGroupId,
                                                pVlanPortVidSetEntry->VlanId,
                                                VLAN_DELETE);
            }

            /* Notifies LLDP about the deletion of protocol vlan */
            VlanLldpApiNotifyProtoVlanId (u2BasePort,
                                          pVlanPortVidSetEntry->VlanId,
                                          VLAN_DELETE, 0);

            VLAN_SLL_DEL (&pVlanPortEntry->VlanVidSet,
                          (tTMO_SLL_NODE *) & (pVlanPortVidSetEntry->NextNode));
            VLAN_RELEASE_BUF (VLAN_PORT_PROTO_ENTRY,
                              (UINT1 *) pVlanPortVidSetEntry);

#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncProtoVlanSwUpdate (VLAN_CURR_CONTEXT_ID (),
                                          i4Dot1vProtocolPortGroupId,
                                          i4Dot1dBasePort, VLAN_DELETE);
#endif
            break;

        default:
            return SNMP_FAILURE;

    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1vProtocolPortRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1dBasePort),
                      i4Dot1vProtocolPortGroupId,
                      i4SetValDot1vProtocolPortRowStatus));
    VlanSelectContext (u4CurrContextId);

    KW_FALSEPOSITIVE_FIX (pVlanPortVidSetEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanBridgeMode
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanBridgeMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanBridgeMode (INT4 i4SetValDot1qFutureVlanBridgeMode)
{
    UNUSED_PARAM (i4SetValDot1qFutureVlanBridgeMode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanTunnelBpduPri
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanTunnelBpduPri
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanTunnelBpduPri (INT4 i4SetValDot1qFutureVlanTunnelBpduPri)
{

    UNUSED_PARAM (i4SetValDot1qFutureVlanTunnelBpduPri);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanTunnelStatus
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                setValDot1qFutureVlanTunnelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanTunnelStatus (INT4 i4Dot1qFutureVlanPort,
                                   INT4 i4SetValDot1qFutureVlanTunnelStatus)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValDot1qFutureVlanTunnelStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanTunnelStpPDUs
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                setValDot1qFutureVlanTunnelStpPDUs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanTunnelStpPDUs (INT4 i4Dot1qFutureVlanPort,
                                    INT4 i4SetValDot1qFutureVlanTunnelStpPDUs)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValDot1qFutureVlanTunnelStpPDUs);

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDot1qFutureVlanTunnelGvrpPDUs
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
setValDot1qFutureVlanTunnelGvrpPDUs
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qFutureVlanTunnelGvrpPDUs (INT4 i4Dot1qFutureVlanPort,
                                     INT4 i4SetValDot1qFutureVlanTunnelGvrpPDUs)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValDot1qFutureVlanTunnelGvrpPDUs);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDot1qFutureVlanTunnelIgmpPkts
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
setValDot1qFutureVlanTunnelIgmpPkts
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qFutureVlanTunnelIgmpPkts (INT4 i4Dot1qFutureVlanPort,
                                     INT4 i4SetValDot1qFutureVlanTunnelIgmpPkts)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValDot1qFutureVlanTunnelIgmpPkts);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanCounterStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                setValDot1qFutureVlanCounterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanCounterStatus (UINT4 u4Dot1qFutureVlanIndex,
                                    INT4 i4SetValDot1qFutureVlanCounterStatus)
{
    tVlanCurrEntry     *pCurrEntry;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

#ifdef NPAPI_WANTED
    UINT1               u1RetVal = SNMP_FAILURE;
#endif

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanCounterStatus == i4SetValDot1qFutureVlanCounterStatus)
    {
        return SNMP_SUCCESS;
    }
#ifdef NPAPI_WANTED
    if (i4SetValDot1qFutureVlanCounterStatus == VLAN_ENABLED)
    {
        /* pCurrEntry->pVlanHwStatsEntry hold the statsId and number of entries 
         * values from the h/w while enabling the vlan stats counters and these 
         * values used while disabling the vlan stats counters. 
         */
        pCurrEntry->pVlanHwStatsEntry =
            (VOID *) VLAN_GET_BUF (VLAN_HW_STATS_ENTRY,
                                   VLAN_HW_STATS_ENTRY_LEN);
        MEMSET (pCurrEntry->pVlanHwStatsEntry, 0, VLAN_HW_STATS_ENTRY_LEN);

        u1RetVal = VlanFsMiVlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                                               (tVlanId) u4Dot1qFutureVlanIndex,
                                               i4SetValDot1qFutureVlanCounterStatus,
                                               pCurrEntry->pVlanHwStatsEntry);

        if (u1RetVal == FNP_FAILURE)
        {
            VLAN_RELEASE_BUF (VLAN_HW_STATS_ENTRY,
                              (UINT1 *) pCurrEntry->pVlanHwStatsEntry);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pCurrEntry->pVlanHwStatsEntry != NULL)
        {
            u1RetVal = VlanFsMiVlanHwGetVlanStats (VLAN_CURR_CONTEXT_ID (),
                                                   (tVlanId)
                                                   u4Dot1qFutureVlanIndex,
                                                   i4SetValDot1qFutureVlanCounterStatus,
                                                   pCurrEntry->
                                                   pVlanHwStatsEntry);

            if (u1RetVal == FNP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            VLAN_RELEASE_BUF (VLAN_HW_STATS_ENTRY,
                              (UINT1 *) pCurrEntry->pVlanHwStatsEntry);
        }
    }
#endif

    pCurrEntry->i4VlanCounterStatus = i4SetValDot1qFutureVlanCounterStatus;

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanCounterStatus,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", VLAN_CURR_CONTEXT_ID (),
                      u4Dot1qFutureVlanIndex,
                      i4SetValDot1qFutureVlanCounterStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanFid
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object 
                setValDot1qFutureVlanFid
 Output      :  The Set Low Lev Routine Take the Indices &
 Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1qFutureVlanFid (UINT4 u4Dot1qFutureVlanIndex,
                          UINT4 u4SetValDot1qFutureVlanFid)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanCurrEntry     *pScanVlanEntry = NULL;
    tVlanId             ScanVlanId;
    tVlanFidEntry      *pFidEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4NewFidIndex = VLAN_INVALID_FID_INDEX;
    UINT1               u1FdbEntryPresent = VLAN_FALSE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_FDB_ID_VALID (u4SetValDot1qFutureVlanFid) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (u4SetValDot1qFutureVlanFid ==
        VlanL2IwfMiGetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                                 (tVlanId) u4Dot1qFutureVlanIndex))
    {
        return SNMP_SUCCESS;
    }

    pVlanEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);

    if (pVlanEntry != NULL)
    {
        VLAN_SCAN_VLAN_TABLE (ScanVlanId)
        {
            pScanVlanEntry = VLAN_GET_CURR_ENTRY (ScanVlanId);
            if (pScanVlanEntry != NULL)
            {
                if (pScanVlanEntry != pVlanEntry)
                {
                    pFidEntry = VLAN_GET_FID_ENTRY (pScanVlanEntry->u4FidIndex);

                    if (u4SetValDot1qFutureVlanFid == pFidEntry->u4Fid)
                    {
                        u1FdbEntryPresent = VLAN_TRUE;
                        u4NewFidIndex = pScanVlanEntry->u4FidIndex;
                        break;
                    }
                }
            }
        }

        if (VLAN_FALSE == u1FdbEntryPresent)
        {
            u4NewFidIndex = VlanGetFreeFidIndex (u4SetValDot1qFutureVlanFid);

            if (VLAN_INVALID_FID_INDEX == u4NewFidIndex)
            {
                return SNMP_FAILURE;
            }
        }

        if (VLAN_FAILURE == VlanRemapFidIndexForVlan (pVlanEntry,
                                                      u4NewFidIndex))
        {
            /* Free only when this is newly created FidIndex */
            if (VLAN_FALSE == u1FdbEntryPresent)
            {
                VlanFreeFidIndex (u4NewFidIndex);
            }

            return SNMP_FAILURE;
        }
    }

    VlanL2IwfSetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                           (tVlanId) u4Dot1qFutureVlanIndex,
                           u4SetValDot1qFutureVlanFid);

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanFid, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %u", VLAN_CURR_CONTEXT_ID (),
                      u4Dot1qFutureVlanIndex, u4SetValDot1qFutureVlanFid));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  VlanSetDot1qFutureVlanTunnelStpPDUs
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
setValDot1qFutureVlanTunnelStpPDUs
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
VlanSetDot1qFutureVlanTunnelStpPDUs (INT4 i4Dot1qFutureVlanPort,
                                     INT4 i4SetValDot1qFutureVlanTunnelStpPDUs)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (i4SetValDot1qFutureVlanTunnelStpPDUs == VLAN_ENABLED)
    {
        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_STP,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL);
    }
    else
    {
        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_STP,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  VlanSetDot1qFutureVlanTunnelGvrpPDUs
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
setValDot1qFutureVlanTunnelStpPDUs
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
VlanSetDot1qFutureVlanTunnelGvrpPDUs (INT4 i4Dot1qFutureVlanPort,
                                      INT4
                                      i4SetValDot1qFutureVlanTunnelGvrpPDUs)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (i4SetValDot1qFutureVlanTunnelGvrpPDUs == VLAN_ENABLED)
    {
        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_GVRP,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL);
    }
    else
    {
        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_GVRP,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  VlanSetDot1qFutureVlanTunnelIgmpPDUs
Input       :  The Indices
Dot1qFutureVlanPort

The Object 
setValDot1qFutureVlanTunnelStpPDUs
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
VlanSetDot1qFutureVlanTunnelIgmpPkts (INT4 i4Dot1qFutureVlanPort,
                                      INT4
                                      i4SetValDot1qFutureVlanTunnelIgmpPkts)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (i4SetValDot1qFutureVlanTunnelIgmpPkts == VLAN_ENABLED)
    {
        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_TUNNEL;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_IGMP,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL);
    }
    else
    {
        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_DISCARD;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_IGMP,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  VlanSetDot1qPvid                       
 Input       :  The Indices
                Dot1qFutureVlanPort
                setValDot1qPvid

                The Object 
                setValDot1qPvid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
VlanSetDot1qPvid (UINT2 u2Port, tVlanId VlanId)
{
    INT4                i4RetVal;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanCurrEntry     *pVlanNewEntry = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
#ifdef PB_WANTED
    tHwVlanPortProperty PbPortProperty;
    INT4                i4PortSVlanPriorityType = 0;
    INT4                i4PortSVlanPriority = 0;
    VLAN_MEMSET (&PbPortProperty, 0, sizeof (tHwVlanPortProperty));
#endif
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Vlan id should not be reset if PVID and VlanId to be set are same */
    if (pVlanPortEntry != NULL && (pVlanPortEntry->Pvid != VlanId))
    {
        if (VlanAstIsPvrstStartedInContext (VLAN_CURR_CONTEXT_ID ())
            == AST_TRUE)
        {
            pStVlanEntry = VlanGetStaticVlanEntry (pVlanPortEntry->Pvid);

            /* Resetting the Untagged Member-ship information from the 
             * previous VLAN */

            if ((pStVlanEntry != NULL)
                && (MsrIsMibRestoreInProgress () != MSR_TRUE))
            {
/*Don't reset the untagged vlan membership in case of tagged member for that
 *          *previous pvid. This is applicable when port type is hybrid*/

                VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result);

                if ((pVlanPortEntry->u1PortType == VLAN_HYBRID_PORT) &&
                    (u1Result == VLAN_TRUE))
                {
                    VLAN_RESET_SINGLE_UNTAGGED_PORT (pStVlanEntry, u2Port);
                    VLAN_RESET_SINGLE_EGRESS_PORT (pStVlanEntry, u2Port);
                    pVlanEntry = VLAN_GET_CURR_ENTRY (pVlanPortEntry->Pvid);

                    if (pVlanEntry != NULL)
                    {
                        VLAN_RESET_SINGLE_CURR_EGRESS_PORT (pVlanEntry, u2Port);

                        if (VlanHwResetVlanMemberPort
                            (pVlanPortEntry->Pvid, u2Port) != VLAN_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                            (pVlanEntry->VlanId, u2Port);

                        if (VlanCfaGetIvrStatus () == CFA_ENABLED)
                        {
                            VlanIvrComputeVlanMclagIfOperStatus (pVlanEntry);
                            VlanIvrComputeVlanIfOperStatus (pVlanEntry);
                        }
                    }
                }

            }

            if (pVlanPortEntry->u1PortType == VLAN_ACCESS_PORT)
            {
                /* Resetting the tagged Member-ship information from the 
                 * previous VLAN only when the port-type is Access.*/
                if (pStVlanEntry != NULL)
                {
                    VLAN_RESET_SINGLE_UNTAGGED_PORT (pStVlanEntry, u2Port);
                    VLAN_RESET_SINGLE_EGRESS_PORT (pStVlanEntry, u2Port);
                }
                pVlanEntry = VLAN_GET_CURR_ENTRY (pVlanPortEntry->Pvid);

                if (pVlanEntry != NULL)
                {
                    VLAN_RESET_SINGLE_CURR_EGRESS_PORT (pVlanEntry, u2Port);

                    if (VlanHwResetVlanMemberPort (pVlanPortEntry->Pvid, u2Port)
                        != VLAN_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                        (pVlanEntry->VlanId, u2Port);

                    if (VlanCfaGetIvrStatus () == CFA_ENABLED)
                    {
                        VlanIvrComputeVlanMclagIfOperStatus (pVlanEntry);
                        VlanIvrComputeVlanIfOperStatus (pVlanEntry);
                    }
                }
            }

            pVlanNewEntry = VLAN_GET_CURR_ENTRY (VlanId);

            /* Add the Port in Current entry's Egress-ports as well as in 
             * Untagged-ports of the Static VLAN (PVID) */

            if ((pVlanNewEntry != NULL)
                && (MsrIsMibRestoreInProgress () != MSR_TRUE))
            {

                VLAN_SET_SINGLE_CURR_EGRESS_PORT (pVlanNewEntry, u2Port);

                VLAN_IS_TAGGED_PORT (pVlanNewEntry->pStVlanEntry, u2Port,
                                     u1Result);

                /*For Access port type, the port can be added as untagged member
                 *of the new vlan.
                 *For Hybrid port type,if the port is already a tagged member for 
                 *that vlan (New PVID), there is no needed to make it as untagged port*/
                if ((pVlanPortEntry->u1PortType == VLAN_ACCESS_PORT) ||
                    ((pVlanPortEntry->u1PortType == VLAN_HYBRID_PORT) &&
                     (u1Result != VLAN_TRUE)))
                {
                    VLAN_SET_SINGLE_UNTAGGED_PORT (pVlanNewEntry->pStVlanEntry,
                                                   u2Port);
                }
                VLAN_SET_SINGLE_EGRESS_PORT (pVlanNewEntry->pStVlanEntry,
                                             u2Port);

                if (VlanHwSetVlanMemberPort (VlanId, u2Port, VLAN_FALSE)
                    != VLAN_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                VLAN_GET_TIMESTAMP (&pVlanNewEntry->u4TimeStamp);

                VlanNotifySetVlanMemberToL2IwfAndEnhFltCriteria
                    (pVlanNewEntry->VlanId, u2Port, VLAN_ADD_UNTAGGED_PORT);

                if (VlanCfaGetIvrStatus () == CFA_ENABLED)
                {
                    VlanIvrComputeVlanMclagIfOperStatus (pVlanNewEntry);
                    VlanIvrComputeVlanIfOperStatus (pVlanNewEntry);
                }
            }
        }

        i4RetVal = VlanHwSetPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port, VlanId);

        if (i4RetVal == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
#ifdef PB_WANTED
        if ((VLAN_PB_PORT_TYPE (u2Port) == VLAN_CNP_TAGGED_PORT) ||
            (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CNP_PORTBASED_PORT))
        {
            nmhGetDot1adPortSVlanPriorityType ((INT4) u2Port,
                                               &i4PortSVlanPriorityType);

            nmhGetDot1adPortSVlanPriority ((INT4) u2Port, &i4PortSVlanPriority);

            PbPortProperty.u2OpCode = PB_PORT_PROPERTY_SVLAN_PRIORITY;
            PbPortProperty.u1SVlanPriorityType =
                (UINT1) i4PortSVlanPriorityType;
            PbPortProperty.u1SVlanPriority = (UINT1) i4PortSVlanPriority;

            if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_PHY_PORT (u2Port),
                                       PbPortProperty) != VLAN_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
#endif

        pVlanPortEntry->Pvid = VlanId;
        VlanL2IwfSetVlanPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port, VlanId);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanUnicastMacLimit
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                setValDot1qFutureVlanUnicastMacLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanUnicastMacLimit (UINT4 u4Dot1qFutureVlanIndex,
                                      UINT4
                                      u4SetValDot1qFutureVlanUnicastMacLimit)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4RetValue = 0;
    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    /* In the MSR thread, unicast mac limit for dynamic vlans are also
     * gets restored. As dynamic vlan might not get created here, 
     * directly accessing vlan curr table for this vlan will crash.
     * Hence check for vlan entry. */
    if (VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex) == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_ENTRY_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    if (u4SetValDot1qFutureVlanUnicastMacLimit ==
        VLAN_CONTROL_MAC_LEARNING_LIMIT (u4Dot1qFutureVlanIndex))
    {
        return SNMP_SUCCESS;
    }

    /*If the Limit value is zero then disable the unicast MAC 
     *learning status in that particular vlan. */
    if ((0 == u4SetValDot1qFutureVlanUnicastMacLimit) &&
        (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (u4Dot1qFutureVlanIndex)
         == VLAN_ENABLED))
    {
        if (VlanSetVlanAdminMacLearnStatus (u4Dot1qFutureVlanIndex,
                                            VLAN_LEARNING_LIMIT_ZERO_TRIGGER) ==
            VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }

    }

    /* if the previous limit value is zero and the current limit value is greater
     * than zero then enable the unicast MAC learning status in that pariticular
     * vlan */
    else if ((0 == VLAN_CONTROL_MAC_LEARNING_LIMIT (u4Dot1qFutureVlanIndex)) &&
             (0 != u4SetValDot1qFutureVlanUnicastMacLimit) &&
             (VLAN_LEARNING_LIMIT_ZERO_TRIGGER ==
              VLAN_CONTROL_ADM_MAC_LEARNING_STATUS (u4Dot1qFutureVlanIndex)))
    {
        if (VlanSetVlanAdminMacLearnStatus (u4Dot1qFutureVlanIndex,
                                            VLAN_ENABLED) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    i4RetValue = VlanHwMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                         (tVlanId) u4Dot1qFutureVlanIndex,
                                         u4SetValDot1qFutureVlanUnicastMacLimit);
    if (i4RetValue != VLAN_SUCCESS)
    {
        if (i4RetValue == VLAN_UNSUPPORTED)
        {
            CLI_SET_ERR (CLI_VLAN_NOT_SUPPORTED);
        }
        return SNMP_FAILURE;
    }

    pVlanEntry = VLAN_GET_CURR_ENTRY (u4Dot1qFutureVlanIndex);
    pFidEntry = VLAN_GET_FID_ENTRY (pVlanEntry->u4FidIndex);

    if (pFidEntry->u4DynamicUnicastCount >
        VLAN_CONTROL_MAC_LEARNING_LIMIT (pVlanEntry->VlanId))
    {
        /* If this condition hits then it means count has one value
           greater than the actual value, for Trap generation when
           threshold exceeded condition. So it has to be decremented */
        pFidEntry->u4DynamicUnicastCount--;
    }

    /* If the configurable value of Unicast Mac Limit per vlan is configured 
     * less than the previously configured value then FDB entries are flushed.
     */

    if (u4SetValDot1qFutureVlanUnicastMacLimit <
        VLAN_CONTROL_MAC_LEARNING_LIMIT (u4Dot1qFutureVlanIndex))
    {
#ifndef NPAPI_WANTED
        VlanHandleFlushFdbId (pFidEntry->u4Fid);
#else
        VlanMiFlushFdbId (gpVlanContextInfo->u4ContextId,
                          u4Dot1qFutureVlanIndex);
#endif
        pFidEntry->u4DynamicUnicastCount = 0;
    }

    VLAN_CONTROL_MAC_LEARNING_LIMIT (u4Dot1qFutureVlanIndex) =
        u4SetValDot1qFutureVlanUnicastMacLimit;

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanUnicastMacLimit,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %u", VLAN_CURR_CONTEXT_ID (),
                      u4Dot1qFutureVlanIndex,
                      u4SetValDot1qFutureVlanUnicastMacLimit));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanAdminMacLearningStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                setValDot1qFutureVlanAdminMacLearningStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanAdminMacLearningStatus (UINT4 u4Dot1qFutureVlanIndex,
                                             INT4
                                             i4SetValDot1qFutureVlanAdminMacLearningStatus)
{

    if (VlanSetVlanAdminMacLearnStatus
        (u4Dot1qFutureVlanIndex,
         i4SetValDot1qFutureVlanAdminMacLearningStatus) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortFdbFlush
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object 
                setValDot1qFutureVlanPortFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortFdbFlush (UINT4 u4Dot1qFutureVlanIndex,
                                   INT4 i4SetValDot1qFutureVlanPortFdbFlush)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;

    if (VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pStVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValDot1qFutureVlanPortFdbFlush == VLAN_ENABLED)
    {
        pStVlanEntry->bVlanFdbFlush = VLAN_TRUE;
        if (VlanMiFlushFdbId (gpVlanContextInfo->u4ContextId,
                              u4Dot1qFutureVlanIndex) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pStVlanEntry->bVlanFdbFlush = VLAN_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanFilteringUtilityCriteria
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                setValDot1qFutureVlanFilteringUtilityCriteria
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanFilteringUtilityCriteria (INT4 i4Dot1qFutureVlanPort,
                                               INT4
                                               i4SetValDot1qFutureVlanFilteringUtilityCriteria)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u2FiltUtilityCriteria ==
        i4SetValDot1qFutureVlanFilteringUtilityCriteria)
    {
        return SNMP_SUCCESS;
    }

    pVlanPortEntry->u2FiltUtilityCriteria =
        (UINT2) i4SetValDot1qFutureVlanFilteringUtilityCriteria;

    if (VLAN_IS_ENH_FILTERING_ENABLED () == VLAN_TRUE)
    {

#ifdef NPAPI_WANTED
        /*                                                         
         * If port is present in enhance filtering port list,
         * Enable learning on port, if filtering criteria is enhanced
         * Disable learning on port, if filtering criteria is default 
         */

        VlanUpdatePortLearningStatus
            (i4Dot1qFutureVlanPort,
             i4SetValDot1qFutureVlanFilteringUtilityCriteria);

#endif
    }

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanFilteringUtilityCriteria, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanFilteringUtilityCriteria));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanWildCardEgressPorts
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress

                The Object 
                setValDot1qFutureVlanWildCardEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanWildCardEgressPorts (tMacAddr WildCardMacAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pWildCardEgressPorts)
{
    UINT1              *pAddedPorts = NULL;
    UINT1              *pDeletedPorts = NULL;
    UINT1              *pau1OldWildCardPortList = NULL;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OldWildCardPorts;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort =
        VLAN_INVALID;
    UINT2               u2Port;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pau1OldWildCardPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldWildCardPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qFutureVlanWildCardEgressPorts: "
                  "Error in allocating memory for pau1OldWildCardPortList\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pau1OldWildCardPortList, 0, VLAN_PORT_LIST_SIZE);

    /* Obtain the prev. wild card static port list for the vlan */
    OldWildCardPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldWildCardPorts.pu1_OctetList = pau1OldWildCardPortList;

    nmhGetDot1qFutureVlanWildCardEgressPorts
        (WildCardMacAddress, &OldWildCardPorts);

    MEMCPY (pWildCardEntry->EgressPorts, pWildCardEgressPorts->pu1_OctetList,
            pWildCardEgressPorts->i4_Length);

    /* Sending Trigger to MSR */
    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pAddedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qFutureVlanWildCardEgressPorts: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1OldWildCardPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));

    pDeletedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDeletedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetDot1qFutureVlanWildCardEgressPorts: "
                  "Error in allocating memory for pDeletedPorts\r\n");
        UtilPlstReleaseLocalPortList (pAddedPorts);
        UtilPlstReleaseLocalPortList (pau1OldWildCardPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDeletedPorts, 0, sizeof (tLocalPortList));

    VlanGetAddedAndDeletedPorts (&OldWildCardPorts,
                                 pWildCardEgressPorts,
                                 pAddedPorts, pDeletedPorts);

    UtilPlstReleaseLocalPortList (pau1OldWildCardPortList);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    for (u2Port = 1; u2Port < VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_MEMBER_PORT (pAddedPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort = VLAN_TRUE;
        }

        VLAN_IS_MEMBER_PORT (pDeletedPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort = VLAN_FALSE;
        }

        /* For port that are not added or deleted for this vlan, 
         * i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort
         * will be Non Memeber. */

        if (i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort != VLAN_INVALID)
        {
            /* Sending Trigger to MSR */

            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIDot1qFutureVlanIsWildCardEgressPort,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i %i",
                              VLAN_CURR_CONTEXT_ID (), WildCardMacAddress,
                              VLAN_GET_IFINDEX (u2Port),
                              i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort));
            VlanSelectContext (u4CurrContextId);
        }
        i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort = VLAN_INVALID;
    }
    UtilPlstReleaseLocalPortList (pAddedPorts);
    UtilPlstReleaseLocalPortList (pDeletedPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanWildCardRowStatus
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress

                The Object 
                setValDot1qFutureVlanWildCardRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanWildCardRowStatus (tMacAddr WildCardMacAddress,
                                        INT4 i4RowStatus)
{

    tVlanWildCardEntry *pWildCardEntry = NULL;
    tMacAddr            ConnectionId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4Result;

    MEMSET (ConnectionId, 0, sizeof (tMacAddr));
    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry != NULL)
    {

        if (pWildCardEntry->u1RowStatus == (UINT1) i4RowStatus)
        {

            return SNMP_SUCCESS;
        }

        if (i4RowStatus == VLAN_CREATE_AND_WAIT)
        {

            return SNMP_FAILURE;
        }
    }
    else
    {
        /* entry is not present in the database */
        if (i4RowStatus != VLAN_CREATE_AND_WAIT)
        {

            return SNMP_FAILURE;
        }
    }
    switch (i4RowStatus)
    {

        case VLAN_CREATE_AND_WAIT:

            pWildCardEntry = (tVlanWildCardEntry *) (VOID *)
                VLAN_GET_BUF (VLAN_WILD_CARD_ENTRY,
                              sizeof (tVlanWildCardEntry));

            if (pWildCardEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "No Free Wild card Entry creation failed\n");

                return SNMP_FAILURE;
            }

            MEMSET (pWildCardEntry, 0, sizeof (tVlanWildCardEntry));

            VLAN_SLL_INIT_NODE (&(pWildCardEntry->NextNode));

            MEMCPY (pWildCardEntry->MacAddr, WildCardMacAddress,
                    sizeof (tMacAddr));

            pWildCardEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            VLAN_SLL_ADD (&gpVlanContextInfo->WildCardTable,
                          (tTMO_SLL_NODE *) & (pWildCardEntry->NextNode));

            break;

        case VLAN_NOT_IN_SERVICE:
            if (pWildCardEntry->u1RowStatus == VLAN_ACTIVE)
            {
                VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                     VLAN_WILD_CARD_ID, WildCardMacAddress);
            }
            pWildCardEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;

        case VLAN_ACTIVE:
            if (pWildCardEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
            {
                return VLAN_FAILURE;
            }
            pWildCardEntry->u1RowStatus = VLAN_ACTIVE;

            VlanDeleteMacAddressInfo (WildCardMacAddress);

            if (VlanIsValidUcastAddr (WildCardMacAddress) == VLAN_SUCCESS)
            {
                i4Result = VlanHwAddStaticUcastEntry
                    (VLAN_WILD_CARD_ID, WildCardMacAddress,
                     VLAN_DEF_RECVPORT, pWildCardEntry->EgressPorts,
                     VLAN_PERMANENT, ConnectionId);

                if (i4Result == VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }

            }
            else
            {
                i4Result = VlanHwAddStMcastEntry (VLAN_WILD_CARD_ID,
                                                  WildCardMacAddress,
                                                  VLAN_DEF_RECVPORT,
                                                  pWildCardEntry->EgressPorts);
                if (i4Result == VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }

            }

            break;

        case VLAN_DESTROY:

            if (VlanDeleteWildCardEntry (pWildCardEntry) == VLAN_FAILURE)
            {
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanWildCardRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i", VLAN_CURR_CONTEXT_ID (),
                      WildCardMacAddress, i4RowStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortProtected
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortProtected
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortProtected (INT4 i4Dot1qFutureVlanPort,
                                    INT4 i4SetValDot1qFutureVlanPortProtected)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if ((pVlanPortEntry->u1PortProtected) ==
        i4SetValDot1qFutureVlanPortProtected)
    {
        return SNMP_SUCCESS;
    }

    pVlanPortEntry->u1PortProtected = (UINT1)
        i4SetValDot1qFutureVlanPortProtected;

    VlanHwSetProtectedStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                    u2Port,
                                    i4SetValDot1qFutureVlanPortProtected);

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanPortProtected,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanPortProtected));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortSubnetBasedClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortSubnetBasedClassification (INT4 i4Dot1qFutureVlanPort,
                                                    INT4
                                                    i4SetValDot1qFutureVlanPortSubnetBasedClassification)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = VLAN_INIT_VAL;
    UINT4               u4SeqNum = VLAN_INIT_VAL;
    INT4                i4Retval = VLAN_INIT_VAL;
    UINT2               u2Port = VLAN_INIT_VAL;

    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PORT_SUBNET_BASED (u2Port)
        == (UINT1) i4SetValDot1qFutureVlanPortSubnetBasedClassification)
    {
        /* Nothing to do */
        return SNMP_SUCCESS;
    }

    i4Retval =
        VlanHwSetSubnetBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                          (UINT1)
                                          i4SetValDot1qFutureVlanPortSubnetBasedClassification);

    if (i4Retval == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    VLAN_SET_PORT_SUBNET_BASED (u2Port,
                                (UINT1)
                                i4SetValDot1qFutureVlanPortSubnetBasedClassification);

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortSubnetBasedClassification,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanPortSubnetBasedClassification));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortUnicastMacLearning
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortUnicastMacLearning
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortUnicastMacLearning (INT4 i4Dot1qFutureVlanPort,
                                             INT4
                                             i4SetValDot1qFutureVlanPortUnicastMacLearning)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u1MacLearningStatus !=
        (UINT1) i4SetValDot1qFutureVlanPortUnicastMacLearning)
    {
        if (VlanHwPortMacLearningStatus (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_GET_IFINDEX
                                         (i4Dot1qFutureVlanPort),
                                         (UINT1)
                                         i4SetValDot1qFutureVlanPortUnicastMacLearning)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u1MacLearningStatus
            = (UINT1) i4SetValDot1qFutureVlanPortUnicastMacLearning;
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortUnicastMacLearning,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                          i4SetValDot1qFutureVlanPortUnicastMacLearning));

        /*Flushing out all the existing mac entries in the port while disabling the
           mac learning */
        if (i4SetValDot1qFutureVlanPortUnicastMacLearning == VLAN_DISABLED)
        {
#ifdef NPAPI_WANTED
            if (VlanHwFlushPort
                (u4CurrContextId, (UINT2) i4Dot1qFutureVlanPort,
                 VLAN_NO_OPTIMIZE) == VLAN_FAILURE)
            {
                return SNMP_FAILURE;
            }
#else
            VlanHandleDeleteFdbEntries ((UINT2) i4Dot1qFutureVlanPort);
#endif
        }

        VlanSelectContext (u4CurrContextId);

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortIngressEtherType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortIngressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortIngressEtherType (INT4 i4Dot1qFutureVlanPort,
                                           INT4
                                           i4SetValDot1qFutureVlanPortIngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u2IngressEtherType !=
        (UINT2) i4SetValDot1qFutureVlanPortIngressEtherType)
    {
        if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                           VLAN_GET_IFINDEX
                                           (i4Dot1qFutureVlanPort),
                                           (UINT2)
                                           i4SetValDot1qFutureVlanPortIngressEtherType)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u2IngressEtherType
            = (UINT2) i4SetValDot1qFutureVlanPortIngressEtherType;
#ifdef BCMX_WANTED
        /* When IngressEtherType is set the Allowable TPIDs configured 
         * in BCM are overwritten, hence updating the same in Control
         * plane data-structure. */
        pVlanPortEntry->u2AllowableTPID1 = VLAN_PORT_DEFAULT_ALLOWABLE_TPID;
        pVlanPortEntry->u2AllowableTPID2 = VLAN_PORT_DEFAULT_ALLOWABLE_TPID;
        pVlanPortEntry->u2AllowableTPID3 = VLAN_PORT_DEFAULT_ALLOWABLE_TPID;
#endif
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortIngressEtherType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                          i4SetValDot1qFutureVlanPortIngressEtherType));
        VlanSelectContext (u4CurrContextId);

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortEgressEtherType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortEgressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortEgressEtherType (INT4 i4Dot1qFutureVlanPort,
                                          INT4
                                          i4SetValDot1qFutureVlanPortEgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u2EgressEtherType !=
        (UINT2) i4SetValDot1qFutureVlanPortEgressEtherType)
    {
        if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_IFINDEX
                                          (i4Dot1qFutureVlanPort),
                                          (UINT2)
                                          i4SetValDot1qFutureVlanPortEgressEtherType)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u2EgressEtherType
            = (UINT2) i4SetValDot1qFutureVlanPortEgressEtherType;

        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortEgressEtherType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                          i4SetValDot1qFutureVlanPortEgressEtherType));
        VlanSelectContext (u4CurrContextId);

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortEgressTPIDType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortEgressTPIDType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortEgressTPIDType (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         i4SetValDot1qFutureVlanPortEgressTPIDType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u1EgressTPIDType !=
        (UINT1) i4SetValDot1qFutureVlanPortEgressTPIDType)
    {
        VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_EGRESS_TPID_TYPE;
        VlanPortProperty.u1EgressTPIDType =
            (UINT1) i4SetValDot1qFutureVlanPortEgressTPIDType;

        if (VlanHwSetPortProperty (u4CurrContextId,
                                   VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                                   VlanPortProperty) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u1EgressTPIDType
            = (UINT1) i4SetValDot1qFutureVlanPortEgressTPIDType;

        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortEgressTPIDType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                          i4SetValDot1qFutureVlanPortEgressTPIDType));
        VlanSelectContext (u4CurrContextId);

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortAllowableTPID1
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortAllowableTPID1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortAllowableTPID1 (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         i4SetValDot1qFutureVlanPortAllowableTPID1)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u2AllowableTPID1 ==
        (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID1)
    {
        return SNMP_SUCCESS;
    }

    /*Delete the current TPID set */
    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID1;
    VlanPortProperty.u2Flag = VLAN_DELETE;
    VlanPortProperty.u2AllowableTPID1 = pVlanPortEntry->u2AllowableTPID1;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_ETHER_TYPE_VALID (i4SetValDot1qFutureVlanPortAllowableTPID1))
    {
        VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID1;
        VlanPortProperty.u2Flag = VLAN_ADD;
        VlanPortProperty.u2AllowableTPID1 =
            (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID1;

        if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                   VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                                   VlanPortProperty) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    pVlanPortEntry->u2AllowableTPID1
        = (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID1;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortAllowableTPID1,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanPortAllowableTPID1));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetDot1qFutureVlanPortAllowableTPID2
Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortAllowableTPID2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortAllowableTPID2 (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         i4SetValDot1qFutureVlanPortAllowableTPID2)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u2AllowableTPID2 ==
        (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID2)
    {
        return SNMP_SUCCESS;
    }

    /*Delete the current TPID set */
    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID2;
    VlanPortProperty.u2Flag = VLAN_DELETE;
    VlanPortProperty.u2AllowableTPID2 = pVlanPortEntry->u2AllowableTPID2;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_ETHER_TYPE_VALID (i4SetValDot1qFutureVlanPortAllowableTPID2))
    {
        VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID2;
        VlanPortProperty.u2Flag = VLAN_ADD;
        VlanPortProperty.u2AllowableTPID2 =
            (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID2;

        if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                   VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                                   VlanPortProperty) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    pVlanPortEntry->u2AllowableTPID2
        = (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID2;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortAllowableTPID2,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanPortAllowableTPID2));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortAllowableTPID3
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                setValDot1qFutureVlanPortAllowableTPID3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortAllowableTPID3 (INT4 i4Dot1qFutureVlanPort,
                                         INT4
                                         i4SetValDot1qFutureVlanPortAllowableTPID3)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u2AllowableTPID3 ==
        (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID3)
    {
        return SNMP_SUCCESS;
    }

    /*Delete the current TPID set */
    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID3;
    VlanPortProperty.u2Flag = VLAN_DELETE;
    VlanPortProperty.u2AllowableTPID3 = pVlanPortEntry->u2AllowableTPID3;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_ETHER_TYPE_VALID (i4SetValDot1qFutureVlanPortAllowableTPID3))
    {
        VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID3;
        VlanPortProperty.u2Flag = VLAN_ADD;
        VlanPortProperty.u2AllowableTPID3 =
            (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID3;

        if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                   VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                                   VlanPortProperty) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    pVlanPortEntry->u2AllowableTPID3
        = (UINT2) i4SetValDot1qFutureVlanPortAllowableTPID3;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortAllowableTPID3,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFutureVlanPortAllowableTPID3));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
* Function    :  nmhSetDot1qFutureVlanPortUnicastMacSecType
* Input       :  The Indices
*                 Dot1qFutureVlanPort  The Object
*                 setValDot1qFutureVlanPortUnicastMacSecType
* Output      :  The Set Low Lev Routine Take the Indices &
*                Sets the Value accordingly.
*                Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortUnicastMacSecType (INT4 i4Dot1qFutureVlanPort,
                                            INT4
                                            i4SetValDot1qFutureVlanPortUnicastMacSecType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->i4UnicastMacSecType !=
        i4SetValDot1qFutureVlanPortUnicastMacSecType)
    {
        if (VlanHwPortUnicastMacSecType (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_GET_IFINDEX
                                         (i4Dot1qFutureVlanPort),
                                         i4SetValDot1qFutureVlanPortUnicastMacSecType)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (VlanHwFlushPort (VLAN_CURR_CONTEXT_ID (), i4Dot1qFutureVlanPort,
                             VLAN_OPTIMIZE) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#else
        VlanHandleDeleteFdbEntries ((UINT2) i4Dot1qFutureVlanPort);
#endif

        pVlanPortEntry->i4UnicastMacSecType =
            i4SetValDot1qFutureVlanPortUnicastMacSecType;

        pVlanPortEntry->u4NumLearntMacEntries = 0;

        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              Dot1qFutureVlanPortUnicastMacSecType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                          i4SetValDot1qFutureVlanPortUnicastMacSecType));
        VlanSelectContext (u4CurrContextId);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureUnicastMacLearningLimit
 Input       :  The Indices

                The Object 
                setValDot1qFutureUnicastMacLearningLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureUnicastMacLearningLimit (UINT4
                                          u4SetValDot1qFutureUnicastMacLearningLimit)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (u4SetValDot1qFutureUnicastMacLearningLimit != VLAN_DYNAMIC_UNICAST_SIZE)
    {
        if (VlanHwSwitchMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                          u4SetValDot1qFutureUnicastMacLearningLimit)
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        VLAN_SCAN_PORT_TABLE (u2Port)
        {
            /* Delete ALL fdb entries learnt on this port */
#ifndef NPAPI_WANTED
            VlanHandleDeleteFdbEntries (u2Port);
#endif
        }

        VLAN_DYNAMIC_UNICAST_COUNT = 0;

        VLAN_DYNAMIC_UNICAST_SIZE = u4SetValDot1qFutureUnicastMacLearningLimit;
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureUnicastMacLearningLimit, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", VLAN_CURR_CONTEXT_ID (),
                      u4SetValDot1qFutureUnicastMacLearningLimit));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureStaticConnectionIdentifier
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort

                The Object
                setValDot1qFutureStaticConnectionIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureStaticConnectionIdentifier (UINT4 u4Dot1qFdbId, tMacAddr
                                             Dot1qStaticUnicastAddress,
                                             INT4
                                             i4Dot1qStaticUnicastReceivePort,
                                             tMacAddr
                                             SetValDot1qFutureStaticConnectionIdentifier)
{
    if (VlanSetConnectionIdentifier (u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                                     i4Dot1qStaticUnicastReceivePort,
                                     SetValDot1qFutureStaticConnectionIdentifier)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetMapVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                setValDot1qFutureVlanPortSubnetMapVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortSubnetMapVid (INT4 i4Dot1qFutureVlanPort,
                                       UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
                                       INT4
                                       i4SetValDot1qFutureVlanPortSubnetMapVid)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask = VlanGetDefMaskFromSrcIPAddr
        (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhSetDot1qFutureVlanPortSubnetMapExtVid
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask, i4SetValDot1qFutureVlanPortSubnetMapVid));
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetMapARPOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                setValDot1qFutureVlanPortSubnetMapARPOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetDot1qFutureVlanPortSubnetMapARPOption
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 i4SetValDot1qFutureVlanPortSubnetMapARPOption)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhSetDot1qFutureVlanPortSubnetMapExtARPOption
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask, i4SetValDot1qFutureVlanPortSubnetMapARPOption));
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetMapRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                setValDot1qFutureVlanPortSubnetMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetDot1qFutureVlanPortSubnetMapRowStatus
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 i4SetValDot1qFutureVlanPortSubnetMapRowStatus)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask, i4SetValDot1qFutureVlanPortSubnetMapRowStatus));
}

/****************************************************************************
 Function    :  VlanNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
VlanNotifyProtocolShutdownStatus (INT4 i4ContextId)
{
#ifdef ISS_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = 0;
    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));
    /* The oid list contains the list of Oids that has been registered
     * for the protocol + the oid of the SystemControl object. */

    /* Delete all the VLAN related sub-trees in fsmsbr mib. All other
     * spanning-tree related sub-tree's should not be deleted */
    SNMPGetOidString (fsDot1dTp, (sizeof (fsDot1dTp) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (fsDot1dStatic, (sizeof (fsDot1dStatic) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, i4ContextId);

    SNMPGetOidString (fsPBridgeMIB, (sizeof (fsPBridgeMIB) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (fsmsvl, (sizeof (fsmsvl) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, i4ContextId);

    SNMPGetOidString (fsmpvl, (sizeof (fsmpvl) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (fsmsbe, (sizeof (fsmsbe) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, i4ContextId);

    u2Objects = 1;
#ifdef PB_WANTED
    SNMPGetOidString (fsmpb, (sizeof (fsmpb) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (fsm1ad, (sizeof (fsm1ad) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, i4ContextId);

    SNMPGetOidString (fsmvle, (sizeof (fsmvle) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    u2Objects = 2;
#endif
    SNMPGetOidString
        (FsMIDot1qFutureVlanShutdownStatus,
         (sizeof (FsMIDot1qFutureVlanShutdownStatus) / sizeof (UINT4)),
         au1ObjectOid[u2Objects - 1]);
    /* Send a notification to MSR to process the vlan shutdown, with
     * vlan oids and its system control object */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, u2Objects, i4ContextId);

#ifdef GARP_WANTED
    /* The above vlan protocol shutdown notification makes MSR to delete
     * all VLAN related configurations in the MSR RBTree. So the GARP shutdown
     * configuration will be deleted in the MSR RBTree. While saving, the
     * GARP shutdown configuration will be lost in iss.conf file.
     * On restoration, GARP module will gets started by default, which is wrongg

     * To avoid this, GARP shutdown indication will be send to MSR module
     * here, once again. */

    /*Send the GVRP Status to MSR */
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qGvrpStatus, 0,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4ContextId, GVRP_DISABLED));

    /*Send the GMRP Status to MSR */
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dGmrpStatus, 0,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4ContextId, GMRP_DISABLED));

    /*Send the GARP Shutdown Status to MSR */
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureGarpShutdownStatus, 0,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4ContextId,
                          GARP_SNMP_TRUE));
    VlanSelectContext (i4ContextId);
#else
    UNUSED_PARAM (SnmpNotifyInfo);
#endif
#else
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetMapExtVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                setValDot1qFutureVlanPortSubnetMapExtVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanPortSubnetMapExtVid (INT4 i4Dot1qFutureVlanPort,
                                          UINT4
                                          u4Dot1qFutureVlanPortSubnetMapExtAddr,
                                          UINT4
                                          u4Dot1qFutureVlanPortSubnetMapExtMask,
                                          INT4
                                          i4SetValDot1qFutureVlanPortSubnetMapExtVid)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = VLAN_INIT_VAL;
    UINT4               u4SeqNum = VLAN_INIT_VAL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    tVlanSubnetMapEntry *pVlanSubnetMapEntry = NULL;
    UINT4               u4Port = VLAN_INIT_VAL;
    INT4                i4RetVal = VLAN_INIT_VAL;

    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));

    u4Port = (UINT4) i4Dot1qFutureVlanPort;
    if (gu4SubnetGlobalOption)
    {
        u4IfIndex = u4Port;
    }
    else
    {
        u4IfIndex = VLAN_GET_IFINDEX ((UINT2) u4Port);
    }

    pVlanSubnetMapEntry =
        VlanGetSubnetMapEntry
        (u4Dot1qFutureVlanPortSubnetMapExtAddr, u4Port,
         u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pVlanSubnetMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the vid is being set just after row creation (before row has
     * been made active even once), only then the row status has to be
     * made to Not In Service. Else if vid of an existing entry is
     * being modified, then do not change the row status */
    if (pVlanSubnetMapEntry->u1RowStatus != VLAN_ACTIVE)
    {
        pVlanSubnetMapEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    if (pVlanSubnetMapEntry->VlanId ==
        (tVlanId) i4SetValDot1qFutureVlanPortSubnetMapExtVid)
    {
        return SNMP_SUCCESS;
    }

    pVlanSubnetMapEntry->VlanId =
        (tVlanId) i4SetValDot1qFutureVlanPortSubnetMapExtVid;

    if (VLAN_PORT_SUBNET_BASED (u4Port) == VLAN_ENABLED)
    {
        if (pVlanSubnetMapEntry->u1RowStatus == VLAN_ACTIVE)
        {
            /* Update in hardware only if Row Status is already active, and
             * the VLAN ID is being changed for an existing entry. Else,
             * when the Row Status is made active, the NPAPI will get
             * invoked then. */
            i4RetVal = VlanHwAddPortSubnetVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                                     (UINT2) (u4IfIndex),
                                                     u4Dot1qFutureVlanPortSubnetMapExtAddr,
                                                     u4Dot1qFutureVlanPortSubnetMapExtMask,
                                                     pVlanSubnetMapEntry->
                                                     VlanId,
                                                     pVlanSubnetMapEntry->
                                                     u1ArpOption);

            if (i4RetVal != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "Updating Subnet VLAN entry failed\n");
                return SNMP_FAILURE;
            }
        }
    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO
        (SnmpNotifyInfo, FsMIDot1qFutureVlanPortSubnetMapExtVid, u4SeqNum,
         FALSE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      u4IfIndex,
                      u4Dot1qFutureVlanPortSubnetMapExtAddr,
                      u4Dot1qFutureVlanPortSubnetMapExtMask,
                      i4SetValDot1qFutureVlanPortSubnetMapExtVid));

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetMapExtARPOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                setValDot1qFutureVlanPortSubnetMapExtARPOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetDot1qFutureVlanPortSubnetMapExtARPOption
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4SetValDot1qFutureVlanPortSubnetMapExtARPOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = VLAN_INIT_VAL;
    UINT4               u4SeqNum = VLAN_INIT_VAL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    tVlanSubnetMapEntry *pVlanSubnetMapEntry = NULL;
    UINT4               u4Port = VLAN_INIT_VAL;
    INT4                i4RetVal = VLAN_INIT_VAL;

    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));

    u4Port = (UINT4) i4Dot1qFutureVlanPort;
    if (gu4SubnetGlobalOption)
    {
        u4IfIndex = u4Port;
    }
    else
    {
        u4IfIndex = VLAN_GET_IFINDEX ((UINT2) u4Port);
    }

    pVlanSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               (UINT4) i4Dot1qFutureVlanPort,
                               u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pVlanSubnetMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanSubnetMapEntry->u1ArpOption ==
        (UINT1) i4SetValDot1qFutureVlanPortSubnetMapExtARPOption)
    {
        return SNMP_SUCCESS;
    }
    pVlanSubnetMapEntry->u1ArpOption = (UINT1)
        i4SetValDot1qFutureVlanPortSubnetMapExtARPOption;

    if (VLAN_PORT_SUBNET_BASED (u4Port) == VLAN_ENABLED)
    {
        if (pVlanSubnetMapEntry->u1RowStatus == VLAN_ACTIVE)
        {
            /* Update in hardware only if Row Status is already active, and
             * the ARP option is being changed for an existing entry.
             * Else, when the Row Status is made active, the NPAPI will get
             * invoked then. */
            i4RetVal = VlanHwAddPortSubnetVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                                     (UINT2) (u4IfIndex),
                                                     u4Dot1qFutureVlanPortSubnetMapExtAddr,
                                                     u4Dot1qFutureVlanPortSubnetMapExtMask,
                                                     pVlanSubnetMapEntry->
                                                     VlanId,
                                                     pVlanSubnetMapEntry->
                                                     u1ArpOption);

            if (i4RetVal != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "Updating Subnet VLAN entry failed\n");
                return SNMP_FAILURE;
            }
        }
    }
    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortSubnetMapExtARPOption,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 3,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      u4IfIndex,
                      u4Dot1qFutureVlanPortSubnetMapExtAddr,
                      u4Dot1qFutureVlanPortSubnetMapExtMask,
                      i4SetValDot1qFutureVlanPortSubnetMapExtARPOption));

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                setValDot1qFutureVlanPortSubnetMapExtRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4SetValDot1qFutureVlanPortSubnetMapExtRowStatus)
{
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = VLAN_INIT_VAL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    UINT4               u4Port = VLAN_INIT_VAL;
    UINT4               u4SeqNum = VLAN_INIT_VAL;
    INT4                i4RetVal = VLAN_INIT_VAL;

    MEMSET (&SnmpNotifyInfo, VLAN_INIT_VAL, sizeof (tSnmpNotifyInfo));

    u4Port = (UINT4) i4Dot1qFutureVlanPort;
    if (gu4SubnetGlobalOption)
    {
        u4IfIndex = u4Port;
    }
    else
    {
        u4IfIndex = VLAN_GET_IFINDEX ((UINT2) u4Port);
    }

    pSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               u4Port, u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pSubnetMapEntry != NULL)
    {
        /* Entry Already present */
        if (pSubnetMapEntry->u1RowStatus ==
            (UINT1) i4SetValDot1qFutureVlanPortSubnetMapExtRowStatus)
        {
            return SNMP_SUCCESS;
        }

        /* Return failure  if row status is create and
         * wait */
        if (i4SetValDot1qFutureVlanPortSubnetMapExtRowStatus ==
            VLAN_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Subnet map entry is not present */
        if (i4SetValDot1qFutureVlanPortSubnetMapExtRowStatus !=
            VLAN_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValDot1qFutureVlanPortSubnetMapExtRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pSubnetMapEntry =
                (tVlanSubnetMapEntry *) (VOID *)
                VLAN_GET_BUF (VLAN_SUBNET_MAP_ENTRY,
                              sizeof (tVlanSubnetMapEntry));

            if (pSubnetMapEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "New Subnet Map Entry allocation failed\n");

                return SNMP_FAILURE;
            }

            gu2VlanSubnetMapCount++;

            MEMSET (pSubnetMapEntry, VLAN_INIT_VAL,
                    sizeof (tVlanSubnetMapEntry));

            VLAN_CPY_SUBNET_ADDR (pSubnetMapEntry->SubnetAddr,
                                  u4Dot1qFutureVlanPortSubnetMapExtAddr);

            pSubnetMapEntry->SubnetMask = u4Dot1qFutureVlanPortSubnetMapExtMask;

            pSubnetMapEntry->SrcIPAddr = u4Dot1qFutureVlanPortSubnetMapExtAddr;

            pSubnetMapEntry->u4Port = u4Port;

            pSubnetMapEntry->VlanId = VLAN_DEF_VLAN_ID;
            if (gu4SubnetGlobalOption)
            {
                pSubnetMapEntry->u1ArpOption = VLAN_ARP_SUPPRESS;
            }
            else
            {
                pSubnetMapEntry->u1ArpOption = VLAN_ARP_ALLOW;
            }
            if (!gu4SubnetGlobalOption)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (pSubnetMapEntry->u4Port);

                (pPortEntry->u2SubnetVlanEntryCount)++;
            }

            VlanAddSubnetMapEntry (pSubnetMapEntry);

            pSubnetMapEntry->u1RowStatus = VLAN_NOT_READY;

            break;

        case VLAN_NOT_IN_SERVICE:

            /* The purpose of deleting from hardware while setting Row status
             * to Not In Service itself, is because, when a row is destroyed
             * on an entry that is in Not In Service, the previous status
             * could have been either Not Ready or Active. If Not Ready, then
             * no entry would have been created in the hardware at all. But if
             * the previous status had been active, then entry would have been
             * created in the hardware. There is no way to distinguish the
             * previous row status. Hence whenever a row is set to Not In
             * Service (this is possible only if the previous status was Active,
             * the entry is destroyed from the hardware. */
            if (VLAN_PORT_SUBNET_BASED (pSubnetMapEntry->u4Port) ==
                VLAN_ENABLED)
            {
                if (pSubnetMapEntry->u1RowStatus == VLAN_ACTIVE)
                {
                    i4RetVal = VlanHwDeletePortSubnetVlanEntry
                        (VLAN_CURR_CONTEXT_ID (),
                         (UINT2) (u4IfIndex),
                         u4Dot1qFutureVlanPortSubnetMapExtAddr,
                         pSubnetMapEntry->SubnetMask);

                    if (i4RetVal != VLAN_SUCCESS)
                    {
                        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                                  VLAN_NAME,
                                  "Deleting Subnet VLAN entry in hardware failed \n");
                        return SNMP_FAILURE;
                    }
                }
            }

            pSubnetMapEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;

        case VLAN_ACTIVE:

            pSubnetMapEntry->u1RowStatus = VLAN_ACTIVE;

            if (VLAN_PORT_SUBNET_BASED (u4Port) != VLAN_ENABLED)
            {
                break;
            }

            i4RetVal = VlanHwAddPortSubnetVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                                     (UINT2) (u4IfIndex),
                                                     u4Dot1qFutureVlanPortSubnetMapExtAddr,
                                                     u4Dot1qFutureVlanPortSubnetMapExtMask,
                                                     pSubnetMapEntry->VlanId,
                                                     pSubnetMapEntry->
                                                     u1ArpOption);

            if (i4RetVal != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "Updating Subnet VLAN entry failed\n");
                return SNMP_FAILURE;
            }

            break;

        case VLAN_DESTROY:

            if (!gu4SubnetGlobalOption)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (pSubnetMapEntry->u4Port);

                (pPortEntry->u2SubnetVlanEntryCount)--;
            }
            gu2VlanSubnetMapCount--;

            VlanDeleteSubnetMapEntry (pSubnetMapEntry);
            break;

        default:
            return SNMP_FAILURE;

    }

    /* Sending Trigger to MSR */

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanPortSubnetMapExtRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      u4IfIndex,
                      u4Dot1qFutureVlanPortSubnetMapExtAddr,
                      u4Dot1qFutureVlanPortSubnetMapExtMask,
                      i4SetValDot1qFutureVlanPortSubnetMapExtRowStatus));

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureStVlanEgressEthertype
 Input       :  The Indices
                Dot1qVlanIndex

                The Object
                setValDot1qFutureStVlanEgressEthertype
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureStVlanEgressEthertype (UINT4 u4Dot1qVlanIndex,
                                        INT4
                                        i4SetValDot1qFutureStVlanEgressEthertype)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tHwVlanPortProperty VlanPortProperty;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4IfIndex = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qVlanIndex);
    if (pStVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pStVlanEntry->u2EgressEtherType ==
        (UINT2) i4SetValDot1qFutureStVlanEgressEthertype)
    {
        return SNMP_SUCCESS;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_VLAN_EGRESS_ETHER_TYPE;
    VlanPortProperty.u2EgressEtherType =
        (UINT2) i4SetValDot1qFutureStVlanEgressEthertype;
    VlanPortProperty.VlanId = (tVlanId) u4Dot1qVlanIndex;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               u4IfIndex, VlanPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pStVlanEntry->u2EgressEtherType =
        (UINT2) i4SetValDot1qFutureStVlanEgressEthertype;

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureStVlanEgressEthertype, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                      u4CurrContextId, (u4Dot1qVlanIndex),
                      i4SetValDot1qFutureStVlanEgressEthertype));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanGlobalsFdbFlush
 Input       :  The Indices

                The Object 
                setValDot1qFutureVlanGlobalsFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanGlobalsFdbFlush (INT4
                                      i4SetValDot1qFutureVlanGlobalsFdbFlush)
{
    UINT4               u4CurrContextId;

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    if (i4SetValDot1qFutureVlanGlobalsFdbFlush == VLAN_TRUE)
    {
        gpVlanContextInfo->bGlobalFdbFlush =
            (BOOLEAN) i4SetValDot1qFutureVlanGlobalsFdbFlush;
        if (VlanHwDeleteAllFdbEntries (u4CurrContextId) == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    gpVlanContextInfo->bGlobalFdbFlush = VLAN_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanUserDefinedTPID
 Input       :  The Indices

                The Object
                setValDot1qFutureVlanUserDefinedTPID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanUserDefinedTPID (INT4
                                      i4SetValDot1qFutureVlanUserDefinedTPID)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tHwVlanPortProperty VlanPortProperty;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4CurrContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT2               u2NextLocalPort = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4SeqNum = 0;
    tVlanId             CurrVlanId = 0;
    tVlanId             NextVlanId = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    if (VLAN_CURR_CONTEXT_PTR () == NULL)
    {
        return SNMP_FAILURE;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (VLAN_PORT_USER_DEFINED_TPID == i4SetValDot1qFutureVlanUserDefinedTPID)
    {
        return SNMP_SUCCESS;
    }

    /* User-defined TPIDs must be deleted first from all the ports
     * and new TPID shuold be added */

    while (VlanL2IwfGetNextValidPortForContext (u4CurrContextId,
                                                u2LocalPortId, &u2NextLocalPort,
                                                &u4NextIfIndex) !=
           L2IWF_FAILURE)
    {

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2NextLocalPort);

        if (pVlanPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        /*Delete the current TPID set */
        if (pVlanPortEntry->u2AllowableTPID3 != 0)
        {
            VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID3;
            VlanPortProperty.u2Flag = VLAN_DELETE;
            VlanPortProperty.u2AllowableTPID3 =
                pVlanPortEntry->u2AllowableTPID3;

            if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_IFINDEX (u2NextLocalPort),
                                       VlanPortProperty) != VLAN_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }

        u2LocalPortId = u2NextLocalPort;
    }
    /* Reset the VLAN Egress Ether type if User-defined TPID is configured */
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));
    while (VlanL2IwfGetNextActiveVlan (CurrVlanId, &NextVlanId) !=
           L2IWF_FAILURE)
    {

        pStVlanEntry = VlanGetStaticVlanEntry (NextVlanId);
        if (pStVlanEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        if (pStVlanEntry->u2EgressEtherType == VLAN_PORT_USER_DEFINED_TPID)
        {

            VlanPortProperty.u2OpCode =
                VLAN_PORT_PROPERTY_CONF_VLAN_EGRESS_ETHER_TYPE;
            VlanPortProperty.u2EgressEtherType =
                (UINT2) i4SetValDot1qFutureVlanUserDefinedTPID;
            VlanPortProperty.VlanId = NextVlanId;
            if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                       u4IfIndex,
                                       VlanPortProperty) != VLAN_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pStVlanEntry->u2EgressEtherType =
                (UINT2) i4SetValDot1qFutureVlanUserDefinedTPID;
        }

        CurrVlanId = NextVlanId;
    }

    u2LocalPortId = 0;
    u2NextLocalPort = 0;
    u4NextIfIndex = 0;

    while (VlanL2IwfGetNextValidPortForContext (u4CurrContextId,
                                                u2LocalPortId, &u2NextLocalPort,
                                                &u4NextIfIndex) !=
           L2IWF_FAILURE)
    {

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2NextLocalPort);

        if (pVlanPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        /*Delete the current TPID set */
        if ((pVlanPortEntry->u2AllowableTPID3 != 0) &&
            (i4SetValDot1qFutureVlanUserDefinedTPID != 0))
        {
            VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID3;
            VlanPortProperty.u2Flag = VLAN_ADD;
            VlanPortProperty.u2AllowableTPID3 =
                (UINT2) i4SetValDot1qFutureVlanUserDefinedTPID;

            if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_IFINDEX (u2NextLocalPort),
                                       VlanPortProperty) != VLAN_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pVlanPortEntry->u2AllowableTPID3
                = (UINT2) i4SetValDot1qFutureVlanUserDefinedTPID;
        }

        u2LocalPortId = u2NextLocalPort;
    }

    VLAN_PORT_USER_DEFINED_TPID =
        (UINT2) i4SetValDot1qFutureVlanUserDefinedTPID;

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureVlanUserDefinedTPID, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4CurrContextId, i4SetValDot1qFutureVlanUserDefinedTPID));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanLoopbackStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                setValDot1qFutureVlanLoopbackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureVlanLoopbackStatus (UINT4 u4Dot1qFutureVlanIndex,
                                     INT4 i4SetValDot1qFutureVlanLoopbackStatus)
{
    tVlanCurrEntry     *pCurrEntry;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCurrEntry->i4VlanLoopbackStatus ==
        i4SetValDot1qFutureVlanLoopbackStatus)
    {
        return SNMP_SUCCESS;
    }
    if (VlanHwSetVlanLoopbackStatus (VLAN_CURR_CONTEXT_ID (),
                                     (tVlanId) u4Dot1qFutureVlanIndex,
                                     i4SetValDot1qFutureVlanLoopbackStatus) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry->i4VlanLoopbackStatus = i4SetValDot1qFutureVlanLoopbackStatus;

    /* Sending Trigger to MSR */
    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanLoopbackStatus,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", VLAN_CURR_CONTEXT_ID (),
                      u4Dot1qFutureVlanIndex,
                      i4SetValDot1qFutureVlanLoopbackStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFuturePortPacketReflectionStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                The Object
                  setValDot1qFuturePortPacketReflectionStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                  Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetDot1qFuturePortPacketReflectionStatus (INT4 i4Dot1qFutureVlanPort,
                                             INT4
                                             i4SetValDot1qFuturePortPacketReflectionStatus)
{

#ifdef NPAPI_WANTED
    tFsNpVlanPortReflectEntry FsNpVlanPortReflectEntry;
#endif
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#ifdef NPAPI_WANTED
    VLAN_MEMSET (&FsNpVlanPortReflectEntry, 0,
                 sizeof (tFsNpVlanPortReflectEntry));
#endif

    /*Get the port entry to set status */
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    /*If port enrey is NULL no need to proceed */
    if (NULL == pVlanPortEntry)
    {
        return SNMP_FAILURE;
    }
    else
    {

        u4IfIndex = VLAN_GET_PHY_PORT (i4Dot1qFutureVlanPort);
#ifdef NPAPI_WANTED
        FsNpVlanPortReflectEntry.u4IfIndex = u4IfIndex;
        FsNpVlanPortReflectEntry.u4Status =
            (UINT4) i4SetValDot1qFuturePortPacketReflectionStatus;

        if (VlanFsMiVlanHwPortPktReflectStatus (&FsNpVlanPortReflectEntry) ==
            FNP_SUCCESS)
        {
            pVlanPortEntry->i4ReflectionStatus =
                i4SetValDot1qFuturePortPacketReflectionStatus;
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIDot1qFuturePortPacketReflectionStatus,
                                  u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                                  SNMP_SUCCESS);

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                              i4SetValDot1qFuturePortPacketReflectionStatus));

            return SNMP_SUCCESS;

        }
        else
        {
            return SNMP_FAILURE;
        }
#endif
        pVlanPortEntry->i4ReflectionStatus =
            i4SetValDot1qFuturePortPacketReflectionStatus;
    }

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFuturePortPacketReflectionStatus, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort),
                      i4SetValDot1qFuturePortPacketReflectionStatus));

    VlanSelectContext (u4CurrContextId);
    UNUSED_PARAM (u4IfIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureVlanRemoteFdbFlush
 Input       :  The Indices

                The Object
                setValDot1qFutureVlanRemoteFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetDot1qFutureVlanRemoteFdbFlush (INT4 i4SetValDot1qFutureVlanRemoteFdbFlush)
{

    gpVlanContextInfo->bRemoteFdbFlush =
        (BOOLEAN) i4SetValDot1qFutureVlanRemoteFdbFlush;

    if (i4SetValDot1qFutureVlanRemoteFdbFlush == VLAN_TRUE)
    {
        VlanFlushRemoteFdb (VLAN_TRUE);
        gpVlanContextInfo->bRemoteFdbFlush = VLAN_FALSE;
    }
    return SNMP_SUCCESS;
}
