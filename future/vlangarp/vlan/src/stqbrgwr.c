# include  "lr.h"
# include  "fssnmp.h"
# include  "stqbrglw.h"
# include  "stqbrgwr.h"
# include  "stqbrgdb.h"

INT4
GetNextIndexIeee8021QBridgeTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTQBRG ()
{
    SNMPRegisterMib (&stqbrgOID, &stqbrgEntry);
    SNMPAddSysorEntry (&stqbrgOID, (const UINT1 *) "stdotqbr");
}

VOID
UnRegisterSTQBRG ()
{
    SNMPUnRegisterMib (&stqbrgOID, &stqbrgEntry);
    SNMPDelSysorEntry (&stqbrgOID, (const UINT1 *) "stdotqbr");
}

INT4
Ieee8021QBridgeVlanVersionNumberGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanVersionNumber
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeMaxVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeMaxVlanId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeMaxSupportedVlansGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeMaxSupportedVlans
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeNumVlansGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeNumVlans (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeMvrpEnabledStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeMvrpEnabledStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeMvrpEnabledStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeMvrpEnabledStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Ieee8021QBridgeTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        i4SetValIeee8021QBridgeMvrpEnabledStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeMvrpEnabledStatus (u4Ieee8021QBridgeComponentId,
                                                  i4SetValIeee8021QBridgeMvrpEnabledStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 1, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,1,1,1,6")))
        {
            pMultiIndex->u4No = 1;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue = u4Ieee8021QBridgeComponentId;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeMvrpEnabledStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,1,1,1,6");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeCVlanPortTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeCVlanPortTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeCVlanPortTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeCVlanPortRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeCVlanPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeCVlanPortRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeCVlanPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeCVlanPortRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeCVlanPortRowStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Ieee8021QBridgeCVlanPortTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeCVlanPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeCVlanPortRowStatus (UINT4
                                         u4Ieee8021QBridgeCVlanPortComponentId,
                                         UINT4 u4Ieee8021QBridgeCVlanPortNumber,
                                         INT4
                                         i4SetValIeee8021QBridgeCVlanPortRowStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeCVlanPortRowStatus
        (u4Ieee8021QBridgeCVlanPortComponentId,
         u4Ieee8021QBridgeCVlanPortNumber,
         i4SetValIeee8021QBridgeCVlanPortRowStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,1,2,1,3")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeCVlanPortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeCVlanPortNumber;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeCVlanPortRowStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,1,2,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeFdbTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeFdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeFdbDynamicCountGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeFdbDynamicCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeFdbLearnedEntryDiscardsGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeFdbLearnedEntryDiscards
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgeFdbAgingTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeFdbAgingTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeFdbAgingTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeFdbAgingTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeFdbAgingTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeFdbAgingTime (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeFdbTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeFdbTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                   UINT4 u4Ieee8021QBridgeFdbId,
                                   INT4 i4SetValIeee8021QBridgeFdbAgingTime)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeFdbAgingTime (u4Ieee8021QBridgeFdbComponentId,
                                             u4Ieee8021QBridgeFdbId,
                                             i4SetValIeee8021QBridgeFdbAgingTime);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,2,1,1,5")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeFdbComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021QBridgeFdbId;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue = i4SetValIeee8021QBridgeFdbAgingTime;
            STRCPY (pOid, "1,111,2,802,1,4,1,2,1,1,5");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeTpFdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeTpFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpFdbPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpFdbStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeVlanNumDeletesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIeee8021QBridgeVlanNumDeletes
            (&(pMultiData->u8_Counter64Value)));
}

INT4
GetNextIndexIeee8021QBridgeVlanCurrentTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeVlanCurrentTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeVlanFdbIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanFdbId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeVlanCurrentEgressPortsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanCurrentEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanCurrentUntaggedPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeVlanCreationTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanCreationTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIeee8021QBridgeTpGroupTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeTpGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeTpGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeTpGroupEgressPortsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpGroupEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeTpGroupLearntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpGroupLearnt
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiData->pOctetStrValue));

}

INT4
GetNextIndexIeee8021QBridgeForwardAllTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeForwardAllTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeForwardAllTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeForwardAllPortsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardAllPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllStaticPortsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardAllStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllForbiddenPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardAllForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllStaticPortsSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardAllStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllForbiddenPortsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardAllForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllStaticPortsTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardAllStaticPorts (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllForbiddenPortsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeForwardAllTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeForwardAllStaticPorts (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeForwardAllVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021QBridgeForwardAllStaticPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeForwardAllStaticPorts
        (u4Ieee8021QBridgeVlanCurrentComponentId,
         u4Ieee8021QBridgeForwardAllVlanIndex,
         pSetValIeee8021QBridgeForwardAllStaticPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,2,4,1,3")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeForwardAllVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeForwardAllStaticPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeForwardAllStaticPorts->pu1_OctetList,
                    pSetValIeee8021QBridgeForwardAllStaticPorts->i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,2,4,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValIeee8021QBridgeForwardAllForbiddenPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeForwardAllForbiddenPorts
        (u4Ieee8021QBridgeVlanCurrentComponentId,
         u4Ieee8021QBridgeForwardAllVlanIndex,
         pSetValIeee8021QBridgeForwardAllForbiddenPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,2,4,1,4")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeForwardAllVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeForwardAllForbiddenPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeForwardAllForbiddenPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeForwardAllForbiddenPorts->i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,2,4,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeForwardUnregisteredTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeForwardUnregisteredPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardUnregisteredPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredStaticPortsGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredForbiddenPortsGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredStaticPortsSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredForbiddenPortsSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredStaticPortsTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredForbiddenPortsTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       u4_ULongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [1].
                                                                       u4_ULongValue,
                                                                       pMultiData->
                                                                       pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeForwardUnregisteredTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pSetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeForwardUnregisteredStaticPorts
        (u4Ieee8021QBridgeVlanCurrentComponentId,
         u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pSetValIeee8021QBridgeForwardUnregisteredStaticPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,2,5,1,3")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeForwardUnregisteredVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeForwardUnregisteredStaticPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeForwardUnregisteredStaticPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeForwardUnregisteredStaticPorts->
                    i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,2,5,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeForwardUnregisteredForbiddenPorts
        (u4Ieee8021QBridgeVlanCurrentComponentId,
         u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,2,5,1,4")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeForwardUnregisteredVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts->
                i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts->
                    i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,2,5,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeStaticUnicastTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeStaticUnicastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeStaticUnicastStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastStorageTypeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticUnicastRowStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticUnicastStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastStorageTypeSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastRowStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastStaticEgressPortsTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    (*
                                                                     (tMacAddr
                                                                      *)
                                                                     pMultiIndex->
                                                                     pIndex[2].
                                                                     pOctetStrValue->
                                                                     pu1_OctetList),
                                                                    pMultiIndex->
                                                                    pIndex[3].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastForbiddenEgressPortsTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       u4_ULongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [1].
                                                                       u4_ULongValue,
                                                                       (*
                                                                        (tMacAddr
                                                                         *)
                                                                        pMultiIndex->
                                                                        pIndex
                                                                        [2].
                                                                        pOctetStrValue->
                                                                        pu1_OctetList),
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [3].
                                                                       u4_ULongValue,
                                                                       pMultiData->
                                                                       pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastStorageTypeTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastStorageType (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              (*(tMacAddr *)
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               pOctetStrValue->
                                                               pu1_OctetList),
                                                              pMultiIndex->
                                                              pIndex[3].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastRowStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            (*(tMacAddr *)
                                                             pMultiIndex->
                                                             pIndex[2].
                                                             pOctetStrValue->
                                                             pu1_OctetList),
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeStaticUnicastTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                     u4Ieee8021QBridgeStaticUnicastComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                     tMacAddr
                                                     tIeee8021QBridgeStaticUnicastAddress,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticUnicastStaticEgressPorts
        (u4Ieee8021QBridgeStaticUnicastComponentId,
         u4Ieee8021QBridgeStaticUnicastVlanIndex,
         tIeee8021QBridgeStaticUnicastAddress,
         u4Ieee8021QBridgeStaticUnicastReceivePort,
         pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,1,1,5")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticUnicastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts->
                    i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,3,1,1,5");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        tIeee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticUnicastForbiddenEgressPorts
        (u4Ieee8021QBridgeStaticUnicastComponentId,
         u4Ieee8021QBridgeStaticUnicastVlanIndex,
         tIeee8021QBridgeStaticUnicastAddress,
         u4Ieee8021QBridgeStaticUnicastReceivePort,
         pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,1,1,6")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticUnicastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts->
                i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts->
                    i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,3,1,1,6");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeStaticUnicastStorageType (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               tIeee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticUnicastStorageType)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticUnicastStorageType
        (u4Ieee8021QBridgeStaticUnicastComponentId,
         u4Ieee8021QBridgeStaticUnicastVlanIndex,
         tIeee8021QBridgeStaticUnicastAddress,
         u4Ieee8021QBridgeStaticUnicastReceivePort,
         i4SetValIeee8021QBridgeStaticUnicastStorageType);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,1,1,7")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticUnicastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeStaticUnicastStorageType;
            STRCPY (pOid, "1,111,2,802,1,4,1,3,1,1,7");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                             u4Ieee8021QBridgeStaticUnicastComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                             tMacAddr
                                             tIeee8021QBridgeStaticUnicastAddress,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastReceivePort,
                                             INT4
                                             i4SetValIeee8021QBridgeStaticUnicastRowStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticUnicastRowStatus
        (u4Ieee8021QBridgeStaticUnicastComponentId,
         u4Ieee8021QBridgeStaticUnicastVlanIndex,
         tIeee8021QBridgeStaticUnicastAddress,
         u4Ieee8021QBridgeStaticUnicastReceivePort,
         i4SetValIeee8021QBridgeStaticUnicastRowStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,1,1,8")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticUnicastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticUnicastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeStaticUnicastRowStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,3,1,1,8");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeStaticMulticastTable (tSnmpIndex * pFirstMultiIndex,
                                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeStaticMulticastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeStaticMulticastStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastStorageTypeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticMulticastRowStatusGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticMulticastStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastStorageTypeSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastRowStatusSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastStaticEgressPortsTest (UINT4 *pu4Error,
                                                     tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts (pu4Error,
                                                                      pMultiIndex->
                                                                      pIndex[0].
                                                                      u4_ULongValue,
                                                                      pMultiIndex->
                                                                      pIndex[1].
                                                                      u4_ULongValue,
                                                                      (*
                                                                       (tMacAddr
                                                                        *)
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [2].
                                                                       pOctetStrValue->
                                                                       pu1_OctetList),
                                                                      pMultiIndex->
                                                                      pIndex[3].
                                                                      u4_ULongValue,
                                                                      pMultiData->
                                                                      pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastForbiddenEgressPortsTest (UINT4 *pu4Error,
                                                        tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastStorageTypeTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastStorageType (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                (*(tMacAddr *)
                                                                 pMultiIndex->
                                                                 pIndex[2].
                                                                 pOctetStrValue->
                                                                 pu1_OctetList),
                                                                pMultiIndex->
                                                                pIndex[3].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastRowStatusTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              (*(tMacAddr *)
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               pOctetStrValue->
                                                               pu1_OctetList),
                                                              pMultiIndex->
                                                              pIndex[3].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastTableDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeStaticMulticastTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       tMacAddr
                                                       tIeee8021QBridgeStaticMulticastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticMulticastStaticEgressPorts
        (u4Ieee8021QBridgeVlanCurrentComponentId, u4Ieee8021QBridgeVlanIndex,
         tIeee8021QBridgeStaticMulticastAddress,
         u4Ieee8021QBridgeStaticMulticastReceivePort,
         pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,2,1,3")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021QBridgeVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticMulticastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticMulticastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts->
                i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts->
                    i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,3,2,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          tIeee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticMulticastForbiddenEgressPorts
        (u4Ieee8021QBridgeVlanCurrentComponentId, u4Ieee8021QBridgeVlanIndex,
         tIeee8021QBridgeStaticMulticastAddress,
         u4Ieee8021QBridgeStaticMulticastReceivePort,
         pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,2,1,4")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021QBridgeVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticMulticastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticMulticastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts->
                i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts->
                    i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,3,2,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 tIeee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 i4SetValIeee8021QBridgeStaticMulticastStorageType)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticMulticastStorageType
        (u4Ieee8021QBridgeVlanCurrentComponentId, u4Ieee8021QBridgeVlanIndex,
         tIeee8021QBridgeStaticMulticastAddress,
         u4Ieee8021QBridgeStaticMulticastReceivePort,
         i4SetValIeee8021QBridgeStaticMulticastStorageType);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,2,1,5")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021QBridgeVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticMulticastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticMulticastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeStaticMulticastStorageType;
            STRCPY (pOid, "1,111,2,802,1,4,1,3,2,1,5");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tMacAddr
                                               tIeee8021QBridgeStaticMulticastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticMulticastRowStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeStaticMulticastRowStatus
        (u4Ieee8021QBridgeVlanCurrentComponentId, u4Ieee8021QBridgeVlanIndex,
         tIeee8021QBridgeStaticMulticastAddress,
         u4Ieee8021QBridgeStaticMulticastReceivePort,
         i4SetValIeee8021QBridgeStaticMulticastRowStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 4, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,3,2,1,6")))
        {
            pMultiIndex->u4No = 4;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021QBridgeVlanIndex;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    tIeee8021QBridgeStaticMulticastAddress, 6);
            pMultiIndex->pIndex[3].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[3].u4_ULongValue =
                u4Ieee8021QBridgeStaticMulticastReceivePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeStaticMulticastRowStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,3,2,1,6");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeVlanStaticTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeVlanStaticTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeVlanStaticTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeVlanStaticNameGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticName
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticUntaggedPortsGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticUntaggedPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeVlanStaticNameSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticName
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticUntaggedPortsSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticUntaggedPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeVlanStaticNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticName (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticEgressPortsTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanForbiddenEgressPortsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticUntaggedPortsTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiIndex->
                                                             pIndex[1].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticRowStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Ieee8021QBridgeVlanStaticTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeVlanStaticTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeVlanStaticName (UINT4
                                     u4Ieee8021QBridgeVlanStaticComponentId,
                                     UINT4 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValIeee8021QBridgeVlanStaticName)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeVlanStaticName
        (u4Ieee8021QBridgeVlanStaticComponentId,
         u4Ieee8021QBridgeVlanStaticVlanIndex,
         pSetValIeee8021QBridgeVlanStaticName);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,3,1,3")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeVlanStaticName->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeVlanStaticName->pu1_OctetList,
                    pSetValIeee8021QBridgeVlanStaticName->i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,4,3,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021QBridgeVlanStaticEgressPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeVlanStaticEgressPorts
        (u4Ieee8021QBridgeVlanStaticComponentId,
         u4Ieee8021QBridgeVlanStaticVlanIndex,
         pSetValIeee8021QBridgeVlanStaticEgressPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,3,1,4")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeVlanStaticEgressPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeVlanStaticEgressPorts->pu1_OctetList,
                    pSetValIeee8021QBridgeVlanStaticEgressPorts->i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,4,3,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeVlanForbiddenEgressPorts
        (u4Ieee8021QBridgeVlanStaticComponentId,
         u4Ieee8021QBridgeVlanStaticVlanIndex,
         pSetValIeee8021QBridgeVlanForbiddenEgressPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,3,1,5")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeVlanForbiddenEgressPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeVlanForbiddenEgressPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeVlanForbiddenEgressPorts->i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,4,3,1,5");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeVlanStaticUntaggedPorts
        (u4Ieee8021QBridgeVlanStaticComponentId,
         u4Ieee8021QBridgeVlanStaticVlanIndex,
         pSetValIeee8021QBridgeVlanStaticUntaggedPorts);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,3,1,6")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pSetValIeee8021QBridgeVlanStaticUntaggedPorts->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pSetValIeee8021QBridgeVlanStaticUntaggedPorts->
                    pu1_OctetList,
                    pSetValIeee8021QBridgeVlanStaticUntaggedPorts->i4_Length);
            STRCPY (pOid, "1,111,2,802,1,4,1,4,3,1,6");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeVlanStaticRowStatus (UINT4
                                          u4Ieee8021QBridgeVlanStaticComponentId,
                                          UINT4
                                          u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          INT4
                                          i4SetValIeee8021QBridgeVlanStaticRowStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeVlanStaticRowStatus
        (u4Ieee8021QBridgeVlanStaticComponentId,
         u4Ieee8021QBridgeVlanStaticVlanIndex,
         i4SetValIeee8021QBridgeVlanStaticRowStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,3,1,7")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeVlanStaticVlanIndex;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeVlanStaticRowStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,3,1,7");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeNextFreeLocalVlanTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeNextFreeLocalVlanIndexGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeNextFreeLocalVlanIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIeee8021QBridgePortVlanTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgePortVlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgePortVlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgePvidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePvid (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgePortAcceptableFrameTypesGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortAcceptableFrameTypes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePortIngressFilteringGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortIngressFiltering
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePortMvrpEnabledStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePortMvrpFailedRegistrationsGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortMvrpFailedRegistrations
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgePortMvrpLastPduOriginGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIeee8021QBridgePortMvrpLastPduOrigin
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
Ieee8021QBridgePortRestrictedVlanRegistrationGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortRestrictedVlanRegistration
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePvidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePvid (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Ieee8021QBridgePortAcceptableFrameTypesSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortAcceptableFrameTypes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePortIngressFilteringSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortIngressFiltering
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePortMvrpEnabledStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePortRestrictedVlanRegistrationSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortRestrictedVlanRegistration
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePvidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePvid (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Ieee8021QBridgePortAcceptableFrameTypesTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Ieee8021QBridgePortIngressFilteringTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortIngressFiltering (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021QBridgePortMvrpEnabledStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Ieee8021QBridgePortRestrictedVlanRegistrationTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Ieee8021QBridgePortVlanTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgePortVlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                           UINT4 u4Ieee8021BridgeBasePort,
                           UINT4 u4SetValIeee8021QBridgePvid)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val = nmhSetExIeee8021QBridgePvid (u4Ieee8021BridgeBasePortComponentId,
                                           u4Ieee8021BridgeBasePort,
                                           u4SetValIeee8021QBridgePvid);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,5,1,1")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiData->u4_ULongValue = u4SetValIeee8021QBridgePvid;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,5,1,1");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4SetValIeee8021QBridgePortAcceptableFrameTypes)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgePortAcceptableFrameTypes
        (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortAcceptableFrameTypes);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,5,1,2")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgePortAcceptableFrameTypes;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,5,1,2");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgePortIngressFiltering (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021QBridgePortIngressFiltering)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgePortIngressFiltering
        (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortIngressFiltering);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,5,1,3")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgePortIngressFiltering;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,5,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4SetValIeee8021QBridgePortMvrpEnabledStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgePortMvrpEnabledStatus
        (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortMvrpEnabledStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,5,1,4")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgePortMvrpEnabledStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,5,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     i4SetValIeee8021QBridgePortRestrictedVlanRegistration)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgePortRestrictedVlanRegistration
        (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortRestrictedVlanRegistration);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 2, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,5,1,7")))
        {
            pMultiIndex->u4No = 2;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgePortRestrictedVlanRegistration;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,5,1,7");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgePortVlanStatisticsTable (tSnmpIndex *
                                                    pFirstMultiIndex,
                                                    tSnmpIndex *
                                                    pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeTpVlanPortInFramesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpVlanPortInFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgeTpVlanPortOutFramesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpVlanPortOutFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgeTpVlanPortInDiscardsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpVlanPortInDiscards
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
GetNextIndexIeee8021QBridgeLearningConstraintsTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeLearningConstraintsTypeGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintsType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintsStatusGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintsStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintsTypeSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintsType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsStatusSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintsStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsTypeTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintsType (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiIndex->
                                                             pIndex[1].
                                                             u4_ULongValue,
                                                             pMultiIndex->
                                                             pIndex[2].
                                                             i4_SLongValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsStatusTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintsStatus (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeLearningConstraintsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeLearningConstraintsType (UINT4
                                              u4Ieee8021QBridgeLearningConstraintsComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeLearningConstraintsVlan,
                                              INT4
                                              i4Ieee8021QBridgeLearningConstraintsSet,
                                              INT4
                                              i4SetValIeee8021QBridgeLearningConstraintsType)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeLearningConstraintsType
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet,
         i4SetValIeee8021QBridgeLearningConstraintsType);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 3, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,8,1,4")))
        {
            pMultiIndex->u4No = 3;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeLearningConstraintsComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeLearningConstraintsVlan;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiIndex->pIndex[2].i4_SLongValue =
                i4Ieee8021QBridgeLearningConstraintsSet;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeLearningConstraintsType;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,8,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                i4SetValIeee8021QBridgeLearningConstraintsStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeLearningConstraintsStatus
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet,
         i4SetValIeee8021QBridgeLearningConstraintsStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 3, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,8,1,5")))
        {
            pMultiIndex->u4No = 3;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeLearningConstraintsComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue =
                u4Ieee8021QBridgeLearningConstraintsVlan;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiIndex->pIndex[2].i4_SLongValue =
                i4Ieee8021QBridgeLearningConstraintsSet;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeLearningConstraintsStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,8,1,5");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable (tSnmpIndex *
                                                            pFirstMultiIndex,
                                                            tSnmpIndex *
                                                            pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeLearningConstraintDefaultsSetGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintDefaultsSet
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTypeGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintDefaultsType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsSetSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintDefaultsSet
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTypeSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintDefaultsType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsSetTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTypeTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTableDep (UINT4 *pu4Error,
                                                   tSnmpIndexList *
                                                   pSnmpIndexList,
                                                   tSNMP_VAR_BIND *
                                                   pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                    u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                    INT4
                                                    i4SetValIeee8021QBridgeLearningConstraintDefaultsSet)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeLearningConstraintDefaultsSet
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
         i4SetValIeee8021QBridgeLearningConstraintDefaultsSet);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 1, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,9,1,2")))
        {
            pMultiIndex->u4No = 1;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeLearningConstraintDefaultsComponentId;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeLearningConstraintDefaultsSet;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,9,1,2");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     INT4
                                                     i4SetValIeee8021QBridgeLearningConstraintDefaultsType)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeLearningConstraintDefaultsType
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
         i4SetValIeee8021QBridgeLearningConstraintDefaultsType);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 1, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,4,9,1,3")))
        {
            pMultiIndex->u4No = 1;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeLearningConstraintDefaultsComponentId;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeLearningConstraintDefaultsType;
            STRCPY (pOid, "1,111,2,802,1,4,1,4,9,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeProtocolGroupTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeProtocolGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeProtocolGroupIdGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolGroupRowStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolGroupRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolGroupIdSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupRowStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolGroupRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolGroupId (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupRowStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            pOctetStrValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeProtocolGroupTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeProtocolGroupId (UINT4
                                      u4Ieee8021QBridgeProtocolGroupComponentId,
                                      INT4
                                      i4Ieee8021QBridgeProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIeee8021QBridgeProtocolTemplateProtocolValue,
                                      INT4
                                      i4SetValIeee8021QBridgeProtocolGroupId)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeProtocolGroupId
        (u4Ieee8021QBridgeProtocolGroupComponentId,
         i4Ieee8021QBridgeProtocolTemplateFrameType,
         pIeee8021QBridgeProtocolTemplateProtocolValue,
         i4SetValIeee8021QBridgeProtocolGroupId);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 3, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,5,1,1,4")))
        {
            pMultiIndex->u4No = 3;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeProtocolGroupComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiIndex->pIndex[1].i4_SLongValue =
                i4Ieee8021QBridgeProtocolTemplateFrameType;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length =
                pIeee8021QBridgeProtocolTemplateProtocolValue->i4_Length;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    pIeee8021QBridgeProtocolTemplateProtocolValue->
                    pu1_OctetList,
                    pIeee8021QBridgeProtocolTemplateProtocolValue->i4_Length);
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue = i4SetValIeee8021QBridgeProtocolGroupId;
            STRCPY (pOid, "1,111,2,802,1,4,1,5,1,1,4");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             INT4
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             INT4
                                             i4SetValIeee8021QBridgeProtocolGroupRowStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeProtocolGroupRowStatus
        (u4Ieee8021QBridgeProtocolGroupComponentId,
         i4Ieee8021QBridgeProtocolTemplateFrameType,
         pIeee8021QBridgeProtocolTemplateProtocolValue,
         i4SetValIeee8021QBridgeProtocolGroupRowStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 3, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,5,1,1,5")))
        {
            pMultiIndex->u4No = 3;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021QBridgeProtocolGroupComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiIndex->pIndex[1].i4_SLongValue =
                i4Ieee8021QBridgeProtocolTemplateFrameType;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length =
                pIeee8021QBridgeProtocolTemplateProtocolValue->i4_Length;
            MEMCPY (pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
                    pIeee8021QBridgeProtocolTemplateProtocolValue->
                    pu1_OctetList,
                    pIeee8021QBridgeProtocolTemplateProtocolValue->i4_Length);
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeProtocolGroupRowStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,5,1,1,5");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT4
GetNextIndexIeee8021QBridgeProtocolPortTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeProtocolPortTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeProtocolPortTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeProtocolPortGroupVidGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolPortGroupVid
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolPortRowStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolPortGroupVidSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolPortGroupVid
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortGroupVidTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolPortGroupVid (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolPortRowStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeProtocolPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT1
nmhSetIeee8021QBridgeProtocolPortGroupVid (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021QBridgeProtocolPortGroupId,
                                           INT4
                                           i4SetValIeee8021QBridgeProtocolPortGroupVid)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeProtocolPortGroupVid
        (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
         i4Ieee8021QBridgeProtocolPortGroupId,
         i4SetValIeee8021QBridgeProtocolPortGroupVid);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 3, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,5,2,1,2")))
        {
            pMultiIndex->u4No = 3;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiIndex->pIndex[2].i4_SLongValue =
                i4Ieee8021QBridgeProtocolPortGroupId;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeProtocolPortGroupVid;
            STRCPY (pOid, "1,111,2,802,1,4,1,5,2,1,2");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 0, pMultiData, pOid);
        }
    }
    return (ret_val);
}

INT1
nmhSetIeee8021QBridgeProtocolPortRowStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021QBridgeProtocolPortGroupId,
                                            INT4
                                            i4SetValIeee8021QBridgeProtocolPortRowStatus)
{
    INT1                ret_val;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;

    ret_val =
        nmhSetExIeee8021QBridgeProtocolPortRowStatus
        (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
         i4Ieee8021QBridgeProtocolPortGroupId,
         i4SetValIeee8021QBridgeProtocolPortRowStatus);
    if (ret_val == SNMP_SUCCESS)
    {
        if (SUCCESS ==
            MSRUpdateMemAlloc (&pMultiIndex, 3, &pMultiData, &pOid,
                               STRLEN ("1,111,2,802,1,4,1,5,2,1,3")))
        {
            pMultiIndex->u4No = 3;
            pMultiIndex->pIndex[0].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[0].u4_ULongValue =
                u4Ieee8021BridgeBasePortComponentId;
            pMultiIndex->pIndex[1].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiIndex->pIndex[1].u4_ULongValue = u4Ieee8021BridgeBasePort;
            pMultiIndex->pIndex[2].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiIndex->pIndex[2].i4_SLongValue =
                i4Ieee8021QBridgeProtocolPortGroupId;
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue =
                i4SetValIeee8021QBridgeProtocolPortRowStatus;
            STRCPY (pOid, "1,111,2,802,1,4,1,5,2,1,3");
            /* Sending an Update Trigger to MSR */
            MSRNotifyConfChg (pMultiIndex, 1, pMultiData, pOid);
        }
    }
    return (ret_val);
}
