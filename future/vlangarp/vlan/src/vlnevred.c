/***********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevred.c,v 1.4 2016/07/22 06:46:32 siva Exp $
 *
 * Description     : This file contains VLAN EVB redundancy related routines
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : vlnevred.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 01 Oct 2015                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/
#ifndef __VLNEVRED_C__
#define __VLNEVRED_C__

#include "vlaninc.h"

extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedProcessCdcpTlvSyncUp
 *
 *    DESCRIPTION      : This function process the TLV change sync up 
 *                       message received from the Active node and updates 
 *                       the EVB data base at standby node.
 *
 *    INPUT            : pRmCtrlMsg - RM control message
 *                       pu2Offset  - pointer to number of bytes added in
 *                                    RM message buffer in calling function
 *
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
VlanEvbRedProcessCdcpTlvSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset)
{
    tEvbUapIfEntry      *pUapIfEntry = NULL;
    UINT4               u4UapIfIndex = 0;

    VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
            "VlanEvbRedProcessTlvChgSyncUp:Received Cdcp TLV change "
            "sync up message from RM\r\n");

    /*CFA If Index of Uap.*/
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4UapIfIndex);

    /* CDCP TLV information on this UAP*/
    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);
    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbRedProcessCdcpTlvSyncUp: UAP %d not exists \n", 
                      u4UapIfIndex);
        return VLAN_FAILURE;
    }
	VLAN_RM_GET_N_BYTE (pRmMsg, &(pUapIfEntry->au1UapCdcpTlv[0]),
			pu2Offset, sizeof (pUapIfEntry->au1UapCdcpTlv));
	VLAN_RM_GET_2_BYTE (pRmMsg, pu2Offset, pUapIfEntry->u2UapCdcpTlvLength);

    /* For Updating the CDCP TLV with LLDP Module After Redundancy 
     * Force-Switchover */ 
	if (VlanEvbCdcpPortUpdate (pUapIfEntry) != VLAN_SUCCESS)
	{
		VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
				VLAN_NAME,"VlanEvbRedProcessCdcpTlvSyncUp:"
				"VlanEvbCdcpPortUpdate failed for UAP : %d \n", u4UapIfIndex);
		return VLAN_FAILURE;
	}


    return VLAN_SUCCESS;
}
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedProcessRemoteRoleSyncUp
 *
 *    DESCRIPTION      : This function process the RemoteRole change sync up 
 *                       message received from the Active node and updates 
 *                       the EVB data base at standby node.
 *
 *    INPUT            : pRmCtrlMsg - RM control message
 *                       pu2Offset  - pointer to number of bytes added in
 *                                    RM message buffer in calling function
 *
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
VlanEvbRedProcessRemoteRoleSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset)
{
    tEvbUapIfEntry      *pUapIfEntry = NULL;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4UapIfCdcpOperState = 0;
    UINT4               u4UapIfSchCdcpRemoteRole = 0;

    VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
            "VlanEvbRedProcessRemoteRoleSyncUp:Received RemoteRole change "
            "sync up message from RM\r\n");

    /*CFA If Index of Uap.*/
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4UapIfIndex);
    /* RemoteRole information on this UAP*/
    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);
    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbRedProcessRemoteRoleSyncUp: UAP %d not exists \n",
                u4UapIfIndex);
        return VLAN_FAILURE;
    }
    /* CDCP Oper state of UAP*/
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset,u4UapIfCdcpOperState);


    /* Just to Make sure that nothing has been changes inised the 
     * Macro VLAN_RM_GET_4_BYTE  which in turn calls the OSIX
     * functions */
    pUapIfEntry->i4UapIfCdcpOperState = (INT4)u4UapIfCdcpOperState;
    
    /* Remote Role of UAP */
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4UapIfSchCdcpRemoteRole);

    pUapIfEntry->i4UapIfSchCdcpRemoteRole = (INT4)u4UapIfSchCdcpRemoteRole;

    return VLAN_SUCCESS;
}
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedProcessSbpSyncUp 
 *
 *    DESCRIPTION      : This function process the SBP add sync up 
 *                       message received from the Active node and updates 
 *                       the EVB data base at standby node.
 *
 *    INPUT            : pRmMsg - RM control message
 *                       pu2Offset - pointer to number of bytes added in
 *                                   RM message buffer in calling function
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
VlanEvbRedProcessSbpSyncUp (tRmMsg * pRmMsg, UINT1 u1MsgType, UINT2 *pu2Offset)
{
    tEvbSChIfEntry      *pSChIfEntry = NULL;
    tEvbUapIfEntry      *pUapIfEntry = NULL;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4SChId      = 0;
    UINT4               u4Svid       = 0;
    UINT4               u4SChIfPort = 0;
    UINT4                u4NegoStatus = 0;
    UINT4               u4UapIfCompId = 0;
	UINT1               u1OperStatus = 0;
	UINT1               u1AdminStatus = 0;

    VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC , VLAN_NAME,
            "VlanEvbRedProcessTlvChgSyncUp:Received Cdcp TLV change "
            "sync up message from RM\r\n");
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4UapIfCompId);
    /*CFA If Index of Uap.*/
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4UapIfIndex);
    /* S-Ch Id:Secondary index of EvbSChScidTree*/
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4SChId);
    /* SVID; secondary index of EvbSChIfTree */
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4Svid);
    /* CFA Index of SBP */
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4SChIfPort);
    /* CFA Index of SBP */
    VLAN_RM_GET_4_BYTE (pRmMsg, pu2Offset, u4NegoStatus);
	/* Get S-channel Admin Status */
	VLAN_RM_GET_1_BYTE (pRmMsg, pu2Offset, u1AdminStatus);
	/* Get S-channel Oper Status */
	VLAN_RM_GET_1_BYTE (pRmMsg, pu2Offset, u1OperStatus);

    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);
    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                      VLAN_NAME, "VlanEvbRedProcessSbpSyncUp:UAP %d not exists"
                      " \n", u4UapIfIndex);
        return VLAN_FAILURE;
    }

    if (u1MsgType == VLAN_EVB_RED_SBP_ADD_SYNC_UP)
    {
        if(pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
        {
            if (VlanEvbSChIfAddEntry
                    (u4UapIfIndex, u4Svid,
                     VLAN_EVB_UAP_SCH_MODE_DYNAMIC,u4SChId, &pSChIfEntry)
                    == VLAN_FAILURE)
            {
                VLAN_TRC_ARG4 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        VLAN_NAME,
                        "VlanEvbRedProcessSbpSyncUp:VlanEvbSChIfAddEntry failed "
                        "for UAP %d SVID %d Mode %d SCID %d\n", u4UapIfIndex,
                        u4Svid, pUapIfEntry->i4UapIfSChMode,u4SChId);
                return VLAN_FAILURE;
            }
        }
        /* Getting the pSChIfEntry and check it is a null (uap + SVID). 
         * Filling  the required information other than UAP , SVID and SCID */

        pSChIfEntry = VlanEvbSChIfGetEntry(u4UapIfIndex, u4Svid);

        if(NULL == pSChIfEntry)
        {
            VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    VLAN_NAME,
                    "VlanEvbRedProcessSbpSyncUp: Entry Not found for UAP %d"
                    " SVID %d\n", u4UapIfIndex, u4Svid); 
                return VLAN_FAILURE;
        }
        pSChIfEntry->i4NegoStatus= (INT4)u4NegoStatus;        
        if (pSChIfEntry->i4NegoStatus == VLAN_EVB_SCH_CONFIRMED)
        {
                if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) &&
                                (u4Svid != VLAN_EVB_DEF_SVID))
                {
                        pUapIfEntry->pEvbLldpSChannelIfNotify
                                (u4UapIfIndex,
                                 u4Svid,VLAN_EVB_SVID_UPDATE);
                }

        }
        else if  (pSChIfEntry->i4NegoStatus == VLAN_EVB_SCH_FREE)
        {
                if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) &&
                                (u4Svid != VLAN_EVB_DEF_SVID))
                {
                        pUapIfEntry->pEvbLldpSChannelIfNotify
                                (u4UapIfIndex,
                                 u4Svid,VLAN_EVB_SVID_DELETE);
                }
        }
        pSChIfEntry->u4SChIfPort = u4SChIfPort;
		pSChIfEntry->u1AdminStatus = u1AdminStatus;
		pSChIfEntry->u1OperStatus = u1OperStatus;
    }
    else
    {
        if(pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
        {

            /* Deletes the entry from EvbSchIfTree and EvbSchScidTree
             * and releases the memory of pSChIfNode. */
            VlanEvbSChIfDelEntry (u4UapIfIndex, u4Svid, u4SChId);
        }
    }
    return VLAN_SUCCESS;
}
/*****************************************************************************
 * Function Name      : VlanEvbRedSendDynamicUpdates                         
 *                                                                           
 * Description        : This function sends the dynamic update messages to    
 *                      the standby node whenever an S-Channel is created or 
 *                      deleted dynamically through CDCP, the corresponding  
 *                      S-Channel identifier and SVID are synced.            
 *                                                                           
 * Input(s)           : pSchIfEntry - S-Channel entry.                       
 *                      u1MsgType   - Message Type
 *                                                                           
 * Output(s)          : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Referred           : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Modified           : None.                                                
 *                                                                           
 * Return Value(s)    : None.                                                
 *****************************************************************************/

INT4 VlanEvbRedSendDynamicUpdates ( UINT1 u1MsgType, VOID *pSyncUpMsg)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2MsgLen = 0;
    UINT2               u2Offset = 0;
    UINT4               u4ContextId = 0;

    if (VlanRmGetNodeState () != RM_ACTIVE)
    {
        /*Only Active node will send the SyncUp message */
        return VLAN_SUCCESS;
    }
    if (VLAN_RED_NUM_STANDBY_NODES () == 0)
    {
        /* When there are no standby nodes, no need to send sync up message */
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                "VlanEvbRedSendDynamicUpdates: No need to send sync up message,"
                " since STANDBY node is down\r\n");
        return VLAN_SUCCESS;

    }
    /* Message can't be NULL */
    if (pSyncUpMsg == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbRedSendDynamicUpdates:Message pointer is NULL\r\n");
        return VLAN_FAILURE;;
    }

    /* get message length */
    VlanEvbRedGetMsgLen (u1MsgType, &u2MsgLen);

    /* allocate memory for the sync up message */
    if ((pRmMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                       VLAN_NAME, "VlanEvbRedSendDynamicUpdates: "
                       "RM alloc failed for length %d\n", u2MsgLen);
        return VLAN_FAILURE;
    }

        /* RM message header */
        /* --------------------
         * | MsgType | MsgLen |
         * --------------------*/

    /* MsgType */
    VLAN_RM_PUT_1_BYTE (pRmMsg, &u2Offset, u1MsgType);

    /* MsgLen */
    VLAN_RM_PUT_2_BYTE (pRmMsg, &u2Offset, u2MsgLen);

    /* Context ID */
    VLAN_RM_PUT_4_BYTE (pRmMsg, &u2Offset, u4ContextId);


    /* Form the message */
    switch (u1MsgType)
    {
        case VLAN_EVB_RED_CDCP_TLV_CHANGE:
            VlanEvbRedConstructCdcpTlvSyncUp(pRmMsg,
                    (tEvbUapIfEntry *)pSyncUpMsg, &u2Offset);
            break;
        case VLAN_EVB_RED_SBP_ADD_SYNC_UP:
        case VLAN_EVB_RED_SBP_DEL_SYNC_UP:
            VlanEvbRedConstructSbpSyncUp(pRmMsg, 
                (tEvbSChIfEntry *) pSyncUpMsg, &u2Offset);
            

            break;
        case VLAN_EVB_RED_CDCP_REMOTE_ROLE:
            VlanEvbRedConstructRemoteRoleSyncUp(pRmMsg,
                    (tEvbUapIfEntry *)pSyncUpMsg, &u2Offset);
             break;                                          
        default:
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbRedSendDynamicUpdates:Invalid message type %d\r\n",
                    u1MsgType);
            break;
    }


    /* Send update message to RM */
    if (VlanRedSendMsgToRm (pRmMsg, u2MsgLen) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbRedSendDynamicUpdates: "
                "Vlan EVB message sending is failed for length %d\n", u2MsgLen);
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedGetMsgLen
 *
 *    DESCRIPTION      : This function returns the message length based on
 *                       message type
 *
 *    INPUT            : u1MsgType - Message type
 *                       pu2MsgLen - pointer to message length
 *                       pSyncUpInfo - pointer to sync up info
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
VlanEvbRedGetMsgLen (UINT1 u1MsgType, UINT2 *pu2MsgLen)
{

    tEvbUapIfEntry * pUapPortInfo  = NULL;
    tEvbSChIfEntry *pSchPortInfo   = NULL;
    UINT2           u2TlvLen       = 0;
    UINT2           u2AddDelSBPLen = 0;
    UINT4           u4ContextId    = 0;
    /* Form the message */
    switch (u1MsgType)
    {
        case VLAN_EVB_RED_CDCP_TLV_CHANGE:
            
            *pu2MsgLen = (UINT2) (sizeof(u1MsgType) +
                                  sizeof(u2TlvLen) + 
                                  sizeof(u4ContextId)+
                                  sizeof (pUapPortInfo->u4UapIfIndex) +
                                  sizeof (pUapPortInfo->au1UapCdcpTlv) +
                                  sizeof (pUapPortInfo->u2UapCdcpTlvLength));
            break; 
        case VLAN_EVB_RED_SBP_ADD_SYNC_UP:
        case VLAN_EVB_RED_SBP_DEL_SYNC_UP:
            *pu2MsgLen = (UINT2) (VLAN_RED_CONTEXT_FIELD_SIZE +
                    sizeof(u1MsgType) +
                    sizeof(u2AddDelSBPLen) +
                    sizeof(u4ContextId)+
                    sizeof (pSchPortInfo->u4UapIfIndex) +
                    sizeof (pSchPortInfo->u4SChId) +
                    sizeof (pSchPortInfo->u4SVId) +
                    sizeof (pSchPortInfo->u4SChIfPort) +
                    sizeof (pSchPortInfo->i4NegoStatus) +
					sizeof (pSchPortInfo->u1AdminStatus) +
					sizeof (pSchPortInfo->u1OperStatus));
            break;
        case VLAN_EVB_RED_CDCP_REMOTE_ROLE:
            *pu2MsgLen = (UINT2) (sizeof(u1MsgType) +
                                  sizeof(u2TlvLen) + 
                                  sizeof(u4ContextId)+
                                  sizeof (pUapPortInfo->u4UapIfIndex) +
                                  sizeof (pUapPortInfo->i4UapIfCdcpOperState) +
                                  sizeof (pUapPortInfo->i4UapIfSchCdcpRemoteRole));
            break;
        default:
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                    "VlanEvbRedGetMsgLen: Invalid message type %d\r\n",
                    u1MsgType);
            break;

    }
    UNUSED_PARAM(pUapPortInfo);
    UNUSED_PARAM(pSchPortInfo);
    UNUSED_PARAM(u1MsgType);
    UNUSED_PARAM(u2TlvLen);
    UNUSED_PARAM(u4ContextId);
}

/***************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedConstructSbpSyncUp
 *
 *    DESCRIPTION      : This function forms the SBP sync up
 *                       message
 *
 *    INPUT            : pRmMsgBuf      - pointer to message buffer
 *                       tEvbSChIfEntry - pointer to S-Channel port info structure
 *                     : pu2Offset      - pointer to number of bytes added in 
 *                                        RM message buffer in calling function
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
VlanEvbRedConstructSbpSyncUp (tRmMsg * pRmMsgBuf, tEvbSChIfEntry * pSchPortInfo,
                             UINT2 *pu2Offset)
{
    tEvbUapIfEntry   *pUapPortInfo = NULL;
    pUapPortInfo = VlanEvbUapIfGetEntry(pSchPortInfo->u4UapIfIndex);

    if (pUapPortInfo == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbRedConstructSbpSyncUp: UAP not exists \n");
        return ;
    }
    /*Context Id*/
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pUapPortInfo->u4UapIfCompId);
    /* CFA Index of SBP */
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->u4UapIfIndex);
    /* S-Ch Id:Secondary index of EvbSChScidTree*/
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->u4SChId);
    /* SVID; secondary index of EvbSChIfTree */
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->u4SVId);
    /* CFA Index of SBP */
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->u4SChIfPort);
    /* CFA Index of SBP */
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->i4NegoStatus);
	/* Filling s-channel Admin status */
    VLAN_RM_PUT_1_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->u1AdminStatus);
	/* Filling s-channel oper status */
    VLAN_RM_PUT_1_BYTE (pRmMsgBuf, pu2Offset, pSchPortInfo->u1OperStatus);
    
    return;
}
/***************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedConstructRemoteRoleSyncUp
 *
 *    DESCRIPTION      : This function forms the RemoteRole change sync up
 *
 *    INPUT            : pRmMsgBuf      - pointer to message buffer
 *                       tEvbUapIfEntry - pointer to UAP port info structure
 *                     : pu2Offset      - pointer to number of bytes added in 
 *                                        RM message buffer in calling function
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
VlanEvbRedConstructRemoteRoleSyncUp (tRmMsg * pRmMsgBuf, tEvbUapIfEntry * pUapPortInfo,
                             UINT2 *pu2Offset)
{
    /*CFA If Index of Uap.*/
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pUapPortInfo->u4UapIfIndex);
    /* CDCP OPER STATE OF UAP */
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pUapPortInfo->i4UapIfCdcpOperState);
    /* CDCP REMOTE ROLE OF UAP */
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pUapPortInfo->i4UapIfSchCdcpRemoteRole);
    return;
}
/***************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedConstructCdcpTlvSyncUp
 *
 *    DESCRIPTION      : This function forms the CDCP TLV change sync up
 *                       message
 *
 *    INPUT            : pRmMsgBuf      - pointer to message buffer
 *                       tEvbUapIfEntry - pointer to UAP port info structure
 *                     : pu2Offset      - pointer to number of bytes added in 
 *                                        RM message buffer in calling function
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
VlanEvbRedConstructCdcpTlvSyncUp (tRmMsg * pRmMsgBuf, tEvbUapIfEntry * pUapPortInfo,
                             UINT2 *pu2Offset)
{
    /*Context Id*/
    /*CFA If Index of Uap.*/
    VLAN_RM_PUT_4_BYTE (pRmMsgBuf, pu2Offset, pUapPortInfo->u4UapIfIndex);
    /* CDCP TLV information on this UAP*/
    VLAN_RM_PUT_N_BYTE (pRmMsgBuf, &(pUapPortInfo->au1UapCdcpTlv[0]),
                         pu2Offset, sizeof (pUapPortInfo->au1UapCdcpTlv));
    /*CDCP TLV Length information on this UAP*/
    VLAN_RM_PUT_2_BYTE (pRmMsgBuf, pu2Offset, pUapPortInfo->u2UapCdcpTlvLength);
    return;
}
/*****************************************************************************/
/* Function Name      : VlanEvbRedSendCdcpTlvBulkSyncup                      */
/*                                                                           */
/* Description        : This function sync  up the CDCP TLV change           */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbRedSendCdcpTlvBulkSyncup ()
{
    /* Send Bulk Update messages to the standby. */
    tEvbUapIfEntry   *pUapPortInfo = NULL;
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = VLAN_RED_MAX_MSG_SIZE;
    UINT2               u2TlvMsgSize = VLAN_RED_MAX_MSG_SIZE;
    UINT4               u4PortNo = 0;
    UINT4               u4ContextId = 0;


    /* get message length */
    VlanEvbRedGetMsgLen (VLAN_EVB_RED_CDCP_TLV_CHANGE, &u2TlvMsgSize);

    for (u4ContextId = gu4EvbBulkUpdPrevContxt;
            u4ContextId <= gVlanRedInfo.u4BulkUpdNextContext; u4ContextId++)
    {

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u4ContextId++;
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            /* Vlan module is shutdown */
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (VLAN_MODULE_ADMIN_STATUS () != VLAN_ENABLED)
        {
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (VLAN_EVB_SYSTEM_STATUS (u4ContextId) == VLAN_EVB_SYSTEM_SHUTDOWN)
        {
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (pMsg == NULL)
        {
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                VLAN_TRC_ARG1 (VLAN_EVB_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                        VLAN_NAME, "VlanEvbRedSendCdcpTlvBulkSyncup: "
                        "RM alloc failed for size %d\n", u2BufSize);
                VlanReleaseContext ();
                return;
            }
        }

        /*
         *    PORT Update message
         *
         *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 512 bytes ->|
         *    -----------------------------------------------------
         *    | Msg. Type |   Length    | UAP index  |  CDCP TLV    |
         *    |     (1)   |1 + 2 + 4 + 1|            |              |
         *    |----------------------------------------------------
         *
         */

        for (u4PortNo = gu4EvbBulkUpdPrevPort; u4PortNo < gVlanRedInfo.BulkUpdNextPort;
                u4PortNo++)
        {
            pUapPortInfo =  VlanEvbUapIfGetEntry(u4PortNo);

            if(pUapPortInfo != NULL)
            {
                if ((u2BufSize - u2Offset) < u2TlvMsgSize)
                {
                    /* There is no enough space to fill the information
                     * in to the buffer. Hence send the existing message and
                     * construct new message to fill the information of other
                     * ports.*/
                    /* Send the existing message to RM. */
                    /* Offset of the Length field for update information is
                     * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                     * size of update information so far written. Hence update
                     * length field at offset u4SyncLengthOffset with the value
                     * u2SyncMsgLen. */

                    /* Note u4Offset contains the number of bytes written in to
                     * the buffer. Hence can be used as buffer size here. */
                    if (VlanRedSendMsgToRm (pMsg, u2Offset) == VLAN_FAILURE)
                    {
                        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                            "VlanEvbRedSendCdcpTlvBulkSyncup: Rm send failed "
                            "for length %d\n", u2Offset);
                        VlanReleaseContext ();
                        return;
                    }
                    /* Reset the offset, size fields so that they can be used in the
                     * new buffer. */
                    u2Offset = 0;
                    u2SyncMsgLen = 0;

                    u2BufSize = VLAN_EVB_BULK_SPLIT_MSG_SIZE;

                    /* Allocate memory for data to be sent to RM. This memory
                     * will be freed by RM after Txmitting  */
                    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                    {
                        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                            "VlanEvbRedSendCdcpTlvBulkSyncup: Rm alloc failed "
                            "for size %d\n", u2BufSize);
                        VlanReleaseContext ();
                        return;
                    }
                }
                /* Fill the message type. */
                VLAN_RM_PUT_1_BYTE (pMsg, &u2Offset, VLAN_EVB_RED_CDCP_TLV_CHANGE);

                /* Fill the number of size of  information to be
                 * synced up. Here only zero will be filled. The following
                 * macro is called to move the pointer by 2 bytes.*/
                VLAN_RM_PUT_2_BYTE (pMsg, &u2Offset, u2TlvMsgSize);

                VLAN_RM_PUT_4_BYTE(pMsg, &u2Offset, pUapPortInfo->u4UapIfCompId);

                /*CFA If Index of Uap.*/
                VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pUapPortInfo->u4UapIfIndex);

                /* CDCP TLV information on this UAP*/
                VLAN_RM_PUT_N_BYTE (pMsg, &(pUapPortInfo->au1UapCdcpTlv[0]),
                        &u2Offset, sizeof (pUapPortInfo->au1UapCdcpTlv));
                /*CDCP TLV Length information on this UAP*/ 
                VLAN_RM_PUT_2_BYTE (pMsg, &u2Offset, pUapPortInfo->u2UapCdcpTlvLength); 


                u2SyncMsgLen = (UINT2)(u2SyncMsgLen + u2TlvMsgSize);
            }
            else
            {
                VlanReleaseContext ();
                continue;
            }

        } /* End of for(;;) for Port */

        if (!(VLAN_IS_PORT_VALID (u4PortNo)))
        {
            gu4EvbBulkUpdPrevPort = 0;
            gu4EvbBulkUpdPrevContxt++;
        }
        VlanReleaseContext ();
    }     /* End of for(;;) for context */


    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (VlanRedSendMsgToRm (pMsg, u2Offset) == VLAN_FAILURE)
        {
            VLAN_GBL_TRC ("VlanEvbRedSendCdcpTlvBulkSyncup: Failed to send msg\r\n");
            return;
        }
    }

}
/*****************************************************************************/
/* Function Name      : VlanEvbRedSendRemoteRoleBulkSyncup                   */
/*                                                                           */
/* Description        : This function sync  up the RemoteRole change         */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbRedSendRemoteRoleBulkSyncup ()
{
    /* Send Bulk Update messages to the standby. */
    tEvbUapIfEntry     *pUapPortInfo = NULL;
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = VLAN_EVB_BULK_SPLIT_MSG_SIZE;
    UINT2               u2RemRoleMsgSize = 0;
    UINT4               u4PortNo = 0;
    UINT4               u4ContextId = 0;



    /* get message length */
    VlanEvbRedGetMsgLen (VLAN_EVB_RED_CDCP_REMOTE_ROLE, &u2RemRoleMsgSize);

    for (u4ContextId = gu4EvbBulkUpdPrevContxt;
            u4ContextId <= gVlanRedInfo.u4BulkUpdNextContext; u4ContextId++)
    {

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u4ContextId++;
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            /* Vlan module is shutdown */
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (VLAN_MODULE_ADMIN_STATUS () != VLAN_ENABLED)
        {
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (VLAN_EVB_SYSTEM_STATUS (u4ContextId) == VLAN_EVB_SYSTEM_SHUTDOWN)
        {
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (pMsg == NULL)
        {
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                VLAN_TRC_ARG1 (VLAN_EVB_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                        VLAN_NAME, "VlanEvbRedSendRemoteRoleBulkSyncup: "
                        "RM alloc failed for size %d\r\n", u2BufSize);
                VlanReleaseContext ();
                return;
            }
        }

        /*
         *    PORT Update message
         *
         *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<-- 4 bytes-->| <- 1 byte ->|
         *    ------------------------------------------ -------------------------
         *    | Msg. Type |   Length    | UAP index |Cdcp OperState| RemoteRole  |
         *    |     (1)   |1 + 2 + 4 + 1|           |              |             |
         *    |------------------------------------------------------------------
         *
         */

        for (u4PortNo = gu4EvbBulkUpdPrevPort; u4PortNo < gVlanRedInfo.BulkUpdNextPort;
                u4PortNo++)
        {
            pUapPortInfo =  VlanEvbUapIfGetEntry(u4PortNo);

            if(pUapPortInfo != NULL)
            {
                if ((u2BufSize - u2Offset) < u2RemRoleMsgSize)
                {
                    /* There is no enough space to fill the information
                     * in to the buffer. Hence send the existing message and
                     * construct new message to fill the information of other
                     * ports.*/
                    /* Send the existing message to RM. */
                    /* Offset of the Length field for update information is
                     * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                     * size of update information so far written. Hence update
                     * length field at offset u4SyncLengthOffset with the value
                     * u2SyncMsgLen. */

                    /* Note u4Offset contains the number of bytes written in to
                     * the buffer. Hence can be used as buffer size here. */
                    if (VlanRedSendMsgToRm (pMsg, u2Offset) == VLAN_FAILURE)
                    {
                        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                            "VlanEvbRedSendCdcpTlvBulkSyncup: Rm send failed "
                            " for size %d\n", u2Offset);
                        VlanReleaseContext ();
                        return;
                    }
                    /* Reset the offset, size fields so that they can be used in the
                     * new buffer. */
                    u2Offset = 0;
                    u2SyncMsgLen = 0;

                    u2BufSize = VLAN_EVB_BULK_SPLIT_MSG_SIZE;

                    /* Allocate memory for data to be sent to RM. This memory
                     * will be freed by RM after Txmitting  */
                    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                    {
                        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                            "VlanEvbRedSendCdcpTlvBulkSyncup: Rm alloc failed "
                            " for size %d\n", u2BufSize);
                        VlanReleaseContext ();
                        return;
                    }

                }
                /* Fill the message type. */
                VLAN_RM_PUT_1_BYTE (pMsg, &u2Offset, VLAN_EVB_RED_CDCP_REMOTE_ROLE);
                /* Fill the number of size of  information to be
                 * synced up. Here only zero will be filled. The following
                 * macro is called to move the pointer by 2 bytes.*/
				VLAN_RM_PUT_2_BYTE (pMsg, &u2Offset, u2RemRoleMsgSize);

				VLAN_RM_PUT_4_BYTE(pMsg, &u2Offset, pUapPortInfo->u4UapIfCompId);

				/*CFA If Index of Uap.*/
				VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pUapPortInfo->u4UapIfIndex);
				/* CDCP OPER STATE OF UAP */
				VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pUapPortInfo->i4UapIfCdcpOperState);
				/* CDCP REMOTE ROLE OF UAP */
				VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pUapPortInfo->i4UapIfSchCdcpRemoteRole);

                u2SyncMsgLen = (UINT2)(u2SyncMsgLen+u2RemRoleMsgSize);
            }
            else
            {
                VlanReleaseContext ();
                continue;
            }

        } /* End of for(;;) for Port */

        if (!(VLAN_IS_PORT_VALID (u4PortNo)))
        {
            gu4EvbBulkUpdPrevPort = 0;
            gu4EvbBulkUpdPrevContxt++;
        }
        VlanReleaseContext ();
    }     /* End of for(;;) for context */


    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (VlanRedSendMsgToRm (pMsg, u2Offset) == VLAN_FAILURE)
        {
            VLAN_GBL_TRC ("VlanEvbRedSendRemoteRoleBulkSyncup: Failed to send msg\r\n");
            return;
        }
    }

}
/*****************************************************************************/
/* Function Name      : VlanEvbRedSendSbpBulkSyncup                          */
/*                                                                           */
/* Description        : This function sync  up the SBP                       */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbRedSendSbpBulkSyncup ()
{
    /* Send Bulk Update messages to the standby. */
    tEvbSChIfEntry      *pSchPortInfo = NULL;
    tEvbSChIfEntry      SChIfNode;
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = VLAN_EVB_BULK_SPLIT_MSG_SIZE;
    UINT2               u2SbpMsgSize = 0;
    tEvbUapIfEntry      *pUapPortInfo  = NULL;
    UINT4               u4ContextId = 0;

    /* get message length */
    VlanEvbRedGetMsgLen (VLAN_EVB_RED_SBP_ADD_SYNC_UP, &u2SbpMsgSize);


    /* Fill the number of size of  information to be
     * synced up. Here only zero will be filled. The following
     * macro is called to move the pointer by 2 bytes.*/

    /* Getting the First entry from CAP config table */

    pSchPortInfo = (tEvbSChIfEntry *) (VOID *) RBTreeGetFirst
        (gEvbGlobalInfo.EvbSchIfTree);

    for (u4ContextId = gu4EvbBulkUpdPrevContxt;
            u4ContextId <= gVlanRedInfo.u4BulkUpdNextContext; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u4ContextId ++;
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            /* Vlan module is shutdown */
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (VLAN_MODULE_ADMIN_STATUS () != VLAN_ENABLED)
        {
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (VLAN_EVB_SYSTEM_STATUS (u4ContextId) == VLAN_EVB_SYSTEM_SHUTDOWN)
        {
            VlanReleaseContext ();
            u4ContextId++;
            continue;
        }

        if (pMsg == NULL)
        {
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                VLAN_TRC_ARG1 (VLAN_EVB_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                        VLAN_NAME, "VlanEvbRedSendSbpBulkSyncup: "
                        "RM alloc failed for size %d\n", u2BufSize);
                VlanReleaseContext ();
                return;
            }
        }

        /*
         *    PORT Update message
         *
         *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 512 bytes ->|
         *    -----------------------------------------------------
         *    | Msg. Type |   Length    | UAP index  |  CDCP TLV    |
         *    |     (1)   |1 + 2 + 4 + 1|            |              |
         *    |----------------------------------------------------
         *
         */


        while (pSchPortInfo != NULL)
        {
            if ((u2BufSize - u2Offset) < u2SbpMsgSize)
            {
                /* There is no enough space to fill the information
                 * in to the buffer. Hence send the existing message and
                 * construct new message to fill the information of other
                 * ports.*/
                if (pMsg != NULL)
                {
                    /* Note u4Offset contains the number of bytes written in to
                     * the buffer. Hence can be used as buffer size here. */
                    if (VlanRedSendMsgToRm (pMsg, u2Offset) == VLAN_FAILURE)
                    {
                        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                                "VlanEvbRedSendSbpBulkSyncup: Rm send failed for"
                                " size %d\n", u2Offset);
                        VlanReleaseContext ();
                        return;
                    }
                }
                /* Reset the offset, size fields so that they can be used in the
                 * new buffer. */
                u2Offset = 0;
                u2SyncMsgLen = 0;

                u2BufSize = VLAN_EVB_BULK_SPLIT_MSG_SIZE;

                /* Allocate memory for data to be sent to RM. This memory
                 * will be freed by RM after Txmitting  */
                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {
                    VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME, 
                        "VlanEvbRedSendSbpBulkSyncup: Rm alloc failed for "
                        "size %d\n", u2BufSize);
                    VlanReleaseContext ();
                    return;
                }
            }
            /* Getting UAP entry for filling Context ID */

            pUapPortInfo = VlanEvbUapIfGetEntry(pSchPortInfo->u4UapIfIndex);
			if (pUapPortInfo == NULL)
			{
                continue;
			}
            /* Fill the message type. */

            VLAN_RM_PUT_1_BYTE (pMsg, &u2Offset, VLAN_EVB_RED_SBP_ADD_SYNC_UP);

            /* Fill the Message type */

            VLAN_RM_PUT_2_BYTE (pMsg, &u2Offset, u2SbpMsgSize);

            /*Context Id*/

            VLAN_RM_PUT_4_BYTE(pMsg, &u2Offset, pUapPortInfo->u4UapIfCompId);

            VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pUapPortInfo->u4UapIfCompId);

            /*CFA If Index of Uap.*/

            VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pSchPortInfo->u4UapIfIndex);

            /* S-Ch Id:Secondary index of EvbSChScidTree*/

            VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pSchPortInfo->u4SChId);

            /* SVID; secondary index of EvbSChIfTree */

            VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pSchPortInfo->u4SVId);

            /* HW Index of SBP */

            VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pSchPortInfo->u4SChIfPort);

            /* Hw negotiation status */

            VLAN_RM_PUT_4_BYTE (pMsg, &u2Offset, pSchPortInfo->i4NegoStatus);
			/* S-channel Admin status */
            VLAN_RM_PUT_1_BYTE (pMsg, &u2Offset, pSchPortInfo->u1AdminStatus);
			/*  s-channel Oper status */
            VLAN_RM_PUT_1_BYTE (pMsg, &u2Offset, pSchPortInfo->u1OperStatus);

            /* CFA Index of SBP */

            u2SyncMsgLen = (UINT2)(u2SyncMsgLen+u2SbpMsgSize);

            SChIfNode.u4UapIfIndex  = pSchPortInfo->u4UapIfIndex;
            SChIfNode.u4SVId        = pSchPortInfo->u4SVId;

            pSchPortInfo = (tEvbSChIfEntry *) (VOID *) RBTreeGetNext
                ((gEvbGlobalInfo.EvbSchIfTree), (tRBElem *) (VOID *)&SChIfNode, NULL);

        }
        VlanReleaseContext ();

    } /* End of for(;;) for Context */


    /* Send the last message. */
    if (pMsg != NULL)
    {

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (VlanRedSendMsgToRm (pMsg, u2Offset) == VLAN_FAILURE)
        {
            VLAN_GBL_TRC ("VlanEvbRedSendSbpBulkSyncup: Failed to send msg\r\n");
            return;
        }
    }
    /* Storing the vlan global variable to EVB global variable for 
     * tracking Context ID and Port number */

    gu4EvbBulkUpdPrevPort = gVlanRedInfo.BulkUpdNextPort;
    gu4EvbBulkUpdPrevContxt  = gVlanRedInfo.u4BulkUpdNextContext;

}
#endif /* __VLNEVRED_C__*/

