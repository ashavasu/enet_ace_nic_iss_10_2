/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlansz.c,v 1.3 2013/11/29 11:04:16 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _VLANSZ_C
#include "vlaninc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
VlanSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < VLAN_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsVLANSizingParams[i4SizingId].u4StructSize,
                              FsVLANSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(VLANMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            VlanSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
VlanSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsVLANSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, VLANMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
VlanSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < VLAN_MAX_SIZING_ID; i4SizingId++)
    {
        if (VLANMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (VLANMemPoolIds[i4SizingId]);
            VLANMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
