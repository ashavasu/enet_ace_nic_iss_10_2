/* $Id: stdbriwr.c,v 1.21 2012/01/21 09:25:46 siva Exp $*/
# include  "lr.h"
# include "vlaninc.h"
# include  "fssnmp.h"
#ifdef RSTP_WANTED
# include  "stdrslex.h"
#endif
# include  "stdbriwr.h"
# include  "stdbridb.h"
# include  "mbsm.h"
# include  "fsvlan.h"
# include  "rstp.h"
INT4
Dot1dBaseBridgeAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    pMultiData->pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhGetDot1dBaseBridgeAddress
        ((tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList);

    return i4RetVal;

}

INT4
Dot1dBaseNumPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhGetDot1dBaseNumPorts (&(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
Dot1dBaseTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhGetDot1dBaseType (&(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
GetNextIndexDot1dBasePortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Dot1dBasePort;

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dBasePortTable (&i4Dot1dBasePort)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dBasePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4Dot1dBasePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4Dot1dBasePort;
    return SNMP_SUCCESS;

}

INT4
Dot1dBasePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dBasePortIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dBasePortIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
Dot1dBasePortCircuitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dBasePortCircuit (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOidValue);

    return i4RetVal;

}

INT4
Dot1dBasePortDelayExceededDiscardsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dBasePortDelayExceededDiscards
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue));

    return i4RetVal;

}

INT4
Dot1dBasePortMtuExceededDiscardsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dBasePortMtuExceededDiscards
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue));

    return i4RetVal;

}

INT4
Dot1dStpProtocolSpecificationGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpProtocolSpecification
        (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpPriority (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpTimeSinceTopologyChangeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpTimeSinceTopologyChange
        (&(pMultiData->u4_ULongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpTopChangesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpTopChanges (&(pMultiData->u4_ULongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpDesignatedRootGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpDesignatedRoot (pMultiData->pOctetStrValue);

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpRootCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpRootCost (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpRootPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpRootPort (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpMaxAge (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpHelloTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpHelloTime (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpHoldTime (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpForwardDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpForwardDelay (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpBridgeMaxAge (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeHelloTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpBridgeHelloTime (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeForwardDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhGetDot1dStpBridgeForwardDelay (&(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhSetDot1dStpPriority (pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeMaxAgeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhSetDot1dStpBridgeMaxAge (pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeHelloTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhSetDot1dStpBridgeHelloTime (pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeForwardDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhSetDot1dStpBridgeForwardDelay (pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhTestv2Dot1dStpPriority (pu4Error, pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeMaxAgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhTestv2Dot1dStpBridgeMaxAge
        (pu4Error, pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeHelloTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhTestv2Dot1dStpBridgeHelloTime
        (pu4Error, pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeForwardDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef RSTP_WANTED

    i4RetVal = nmhTestv2Dot1dStpBridgeForwardDelay
        (pu4Error, pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPriorityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhDepv2Dot1dStpPriority (pu4Error, pSnmpIndexList,
                                         pSnmpvarbinds);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeMaxAgeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhDepv2Dot1dStpBridgeMaxAge (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeHelloTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhDepv2Dot1dStpBridgeHelloTime (pu4Error, pSnmpIndexList,
                                                pSnmpvarbinds);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return i4RetVal;

}

INT4
Dot1dStpBridgeForwardDelayDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhDepv2Dot1dStpBridgeForwardDelay (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return i4RetVal;

}

INT4
GetNextIndexDot1dStpPortTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
#ifdef RSTP_WANTED
    INT4                i4Dot1dBasePort;

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dStpPortTable
            (&i4Dot1dBasePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dStpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4Dot1dBasePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4Dot1dBasePort;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFirstMultiIndex);
    UNUSED_PARAM (pNextMultiIndex);
    return SNMP_FAILURE;
#endif

}

INT4
Dot1dStpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return SNMP_SUCCESS;

}

INT4
Dot1dStpPortPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortState (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortPathCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortPathCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortDesignatedRootGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedRoot
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue);

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortDesignatedCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedCost
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortDesignatedBridgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedBridge
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue);

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortDesignatedPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedPort
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue);

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortForwardTransitionsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortForwardTransitions
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortPathCost32Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef RSTP_WANTED

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dStpPortPathCost32 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED
    i4RetVal = nmhGetDot1dStpVersion (&(pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpTxHoldCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED
    i4RetVal = nmhGetDot1dStpTxHoldCount (&(pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortProtocolMigrationGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef RSTP_WANTED
    if (nmhValidateIndexInstanceDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1dStpPortProtocolMigration
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminEdgePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef RSTP_WANTED
    if (nmhValidateIndexInstanceDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1dStpPortAdminEdgePort
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortOperEdgePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef RSTP_WANTED
    if (nmhValidateIndexInstanceDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1dStpPortOperEdgePort
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminPointToPointGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef RSTP_WANTED
    if (nmhValidateIndexInstanceDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1dStpPortAdminPointToPoint
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortOperPointToPointGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef RSTP_WANTED
    if (nmhValidateIndexInstanceDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1dStpPortOperPointToPoint
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminPathCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef RSTP_WANTED
    if (nmhValidateIndexInstanceDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1dStpPortAdminPathCost
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhSetDot1dStpPortPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhSetDot1dStpPortEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortPathCostSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED

    i4RetVal =
        (nmhSetDot1dStpPortPathCost
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortPathCost32Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal =
        (nmhSetDot1dStpPortPathCost32
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED
    i4RetVal = nmhSetDot1dStpVersion (pMultiData->i4_SLongValue);
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpTxHoldCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetDot1dStpTxHoldCount (pMultiData->i4_SLongValue);
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortProtocolMigrationSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhSetDot1dStpPortProtocolMigration
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminEdgePortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhSetDot1dStpPortAdminEdgePort
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminPointToPointSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhSetDot1dStpPortAdminPointToPoint
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminPathCostSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhSetDot1dStpPortAdminPathCost
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhTestv2Dot1dStpPortPriority (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhTestv2Dot1dStpPortEnable (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortPathCostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = (nmhTestv2Dot1dStpPortPathCost (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpPortPathCost32Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = (nmhTestv2Dot1dStpPortPathCost32 (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
Dot1dStpVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED
    i4RetVal = nmhTestv2Dot1dStpVersion (pu4Error, pMultiData->i4_SLongValue);
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpTxHoldCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
#ifdef RSTP_WANTED
    i4RetVal =
        nmhTestv2Dot1dStpTxHoldCount (pu4Error, pMultiData->i4_SLongValue);
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortProtocolMigrationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhTestv2Dot1dStpPortProtocolMigration (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminEdgePortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhTestv2Dot1dStpPortAdminEdgePort (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminPointToPointTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhTestv2Dot1dStpPortAdminPointToPoint (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortAdminPathCostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = (nmhTestv2Dot1dStpPortAdminPathCost (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return (i4RetVal);

}

INT4
Dot1dStpPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED

    i4RetVal = nmhDepv2Dot1dStpPortTable (pu4Error, pSnmpIndexList,
                                          pSnmpvarbinds);

#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return i4RetVal;

}

INT4
Dot1dStpVersionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED
    AST_LOCK ();

    i4RetVal = nmhDepv2Dot1dStpVersion (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds);

    AST_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return (i4RetVal);
}

INT4
Dot1dStpTxHoldCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef RSTP_WANTED
    AST_LOCK ();

    i4RetVal = nmhDepv2Dot1dStpTxHoldCount (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds);

    AST_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return (i4RetVal);
}

INT4
Dot1dTpLearnedEntryDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhGetDot1dTpLearnedEntryDiscards (&(pMultiData->u4_ULongValue));

    return i4RetVal;

}

INT4
Dot1dTpAgingTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhGetDot1dTpAgingTime (&(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
Dot1dTpAgingTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhSetDot1dTpAgingTime (pMultiData->i4_SLongValue);

    return i4RetVal;

}

INT4
Dot1dTpAgingTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhTestv2Dot1dTpAgingTime (pu4Error, pMultiData->i4_SLongValue);

    return i4RetVal;

}

INT4
Dot1dTpAgingTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    i4RetVal = nmhDepv2Dot1dTpAgingTime (pu4Error, pSnmpIndexList,
                                         pSnmpvarbinds);

    return i4RetVal;

}

INT4
GetNextIndexDot1dTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTpFdbTable ((tMacAddr *) pNextMultiIndex->
                                             pIndex[0].pOctetStrValue->
                                             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTpFdbTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Dot1dTpFdbAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpFdbTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dTpFdbTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpFdbPort ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
Dot1dTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dTpFdbTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpFdbStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
GetNextIndexDot1dTpPortTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Dot1dBasePort;

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTpPortTable (&i4Dot1dBasePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4Dot1dBasePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4Dot1dBasePort;
    return SNMP_SUCCESS;

}

INT4
Dot1dTpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dTpPortMaxInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpPortMaxInfo (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
Dot1dTpPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue));

    return i4RetVal;

}

INT4
Dot1dTpPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpPortOutFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue));

    return i4RetVal;

}

INT4
Dot1dTpPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dTpPortInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue));

    return i4RetVal;

}

INT4
GetNextIndexDot1dStaticTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    tMacAddr           *pDot1dStaticAddress;
    INT4                i4Dot1dReceivePort;

    pDot1dStaticAddress =
        (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList;

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dStaticTable
            (pDot1dStaticAddress, &i4Dot1dReceivePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dStaticTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             pDot1dStaticAddress, pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4Dot1dReceivePort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4Dot1dReceivePort;
    return SNMP_SUCCESS;

}

INT4
Dot1dStaticAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dStaticReceivePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dStaticAllowedToGoToGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStaticAllowedToGoTo ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue);

    return i4RetVal;

}

INT4
Dot1dStaticStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStaticStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    return i4RetVal;

}

INT4
Dot1dStaticAllowedToGoToSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    i4RetVal = nmhSetDot1dStaticAllowedToGoTo ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue);

    return i4RetVal;

}

INT4
Dot1dStaticStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    i4RetVal = nmhSetDot1dStaticStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    return i4RetVal;

}

INT4
Dot1dStaticAllowedToGoToTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    i4RetVal = nmhTestv2Dot1dStaticAllowedToGoTo (pu4Error,
                                                  (*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiData->pOctetStrValue);

    return i4RetVal;

}

INT4
Dot1dStaticStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    i4RetVal = nmhTestv2Dot1dStaticStatus (pu4Error,
                                           (*(tMacAddr *) pMultiIndex->
                                            pIndex[0].pOctetStrValue->
                                            pu1_OctetList),
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    return i4RetVal;

}

INT4
Dot1dStaticTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    i4RetVal = nmhDepv2Dot1dStaticTable (pu4Error, pSnmpIndexList,
                                         pSnmpvarbinds);
    return i4RetVal;
}

INT4
Dot1dStpExtPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;
#ifdef RSTP_WANTED
    i4RetVal = nmhDepv2Dot1dStpExtPortTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return (i4RetVal);
}

INT4
GetNextIndexDot1dStpExtPortTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{

#ifdef RSTP_WANTED
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dStpExtPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dStpExtPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFirstMultiIndex);
    UNUSED_PARAM (pNextMultiIndex);
    return (SNMP_FAILURE);
#endif
}

VOID
RegisterSTDBRI ()
{
    SNMPRegisterMibWithLock (&Dot1dBasePortTableOID, &Dot1dBasePortTableEntry,
                             VlanLock, VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dTpFdbTableOID, &Dot1dTpFdbTableEntry,
                             VlanLock, VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dTpPortTableOID, &Dot1dTpPortTableEntry,
                             VlanLock, VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStaticTableOID, &Dot1dStaticTableEntry,
                             VlanLock, VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dBaseBridgeAddressOID,
                             &Dot1dBaseBridgeAddressEntry, VlanLock, VlanUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dBaseNumPortsOID, &Dot1dBaseNumPortsEntry,
                             VlanLock, VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dBaseTypeOID, &Dot1dBaseTypeEntry, VlanLock,
                             VlanUnLock, SNMP_MSR_TGR_FALSE);
#ifdef RSTP_WANTED
    SNMPRegisterMibWithLock (&Dot1dStpPortTableOID, &Dot1dStpPortTableEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpExtPortTableOID,
                             &Dot1dStpExtPortTableEntry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpProtocolSpecificationOID,
                             &Dot1dStpProtocolSpecificationEntry, AstLock,
                             AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpPriorityOID, &Dot1dStpPriorityEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpTimeSinceTopologyChangeOID,
                             &Dot1dStpTimeSinceTopologyChangeEntry, AstLock,
                             AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpTopChangesOID, &Dot1dStpTopChangesEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpDesignatedRootOID,
                             &Dot1dStpDesignatedRootEntry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpRootCostOID, &Dot1dStpRootCostEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpRootPortOID, &Dot1dStpRootPortEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpMaxAgeOID, &Dot1dStpMaxAgeEntry, AstLock,
                             AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpHelloTimeOID, &Dot1dStpHelloTimeEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpHoldTimeOID, &Dot1dStpHoldTimeEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpForwardDelayOID,
                             &Dot1dStpForwardDelayEntry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpBridgeMaxAgeOID,
                             &Dot1dStpBridgeMaxAgeEntry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpBridgeHelloTimeOID,
                             &Dot1dStpBridgeHelloTimeEntry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpBridgeForwardDelayOID,
                             &Dot1dStpBridgeForwardDelayEntry, AstLock,
                             AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpVersionOID, &Dot1dStpVersionEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dStpTxHoldCountOID, &Dot1dStpTxHoldCountEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
#endif
    SNMPRegisterMibWithLock (&Dot1dTpLearnedEntryDiscardsOID,
                             &Dot1dTpLearnedEntryDiscardsEntry, VlanLock,
                             VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&Dot1dTpAgingTimeOID, &Dot1dTpAgingTimeEntry,
                             VlanLock, VlanUnLock, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdbriOID, (const UINT1 *) "stdbridge");
}

VOID
UnRegisterSTDBRI ()
{
    SNMPUnRegisterMib (&Dot1dBasePortTableOID, &Dot1dBasePortTableEntry);
    SNMPUnRegisterMib (&Dot1dStpPortTableOID, &Dot1dStpPortTableEntry);
    SNMPUnRegisterMib (&Dot1dStpExtPortTableOID, &Dot1dStpExtPortTableEntry);
    SNMPUnRegisterMib (&Dot1dTpFdbTableOID, &Dot1dTpFdbTableEntry);
    SNMPUnRegisterMib (&Dot1dTpPortTableOID, &Dot1dTpPortTableEntry);
    SNMPUnRegisterMib (&Dot1dStaticTableOID, &Dot1dStaticTableEntry);
    SNMPUnRegisterMib (&Dot1dBaseBridgeAddressOID,
                       &Dot1dBaseBridgeAddressEntry);
    SNMPUnRegisterMib (&Dot1dBaseNumPortsOID, &Dot1dBaseNumPortsEntry);
    SNMPUnRegisterMib (&Dot1dBaseTypeOID, &Dot1dBaseTypeEntry);
    SNMPUnRegisterMib (&Dot1dStpProtocolSpecificationOID,
                       &Dot1dStpProtocolSpecificationEntry);
    SNMPUnRegisterMib (&Dot1dStpPriorityOID, &Dot1dStpPriorityEntry);
    SNMPUnRegisterMib (&Dot1dStpTimeSinceTopologyChangeOID,
                       &Dot1dStpTimeSinceTopologyChangeEntry);
    SNMPUnRegisterMib (&Dot1dStpTopChangesOID, &Dot1dStpTopChangesEntry);
    SNMPUnRegisterMib (&Dot1dStpDesignatedRootOID,
                       &Dot1dStpDesignatedRootEntry);
    SNMPUnRegisterMib (&Dot1dStpRootCostOID, &Dot1dStpRootCostEntry);
    SNMPUnRegisterMib (&Dot1dStpRootPortOID, &Dot1dStpRootPortEntry);
    SNMPUnRegisterMib (&Dot1dStpMaxAgeOID, &Dot1dStpMaxAgeEntry);
    SNMPUnRegisterMib (&Dot1dStpHelloTimeOID, &Dot1dStpHelloTimeEntry);
    SNMPUnRegisterMib (&Dot1dStpHoldTimeOID, &Dot1dStpHoldTimeEntry);
    SNMPUnRegisterMib (&Dot1dStpForwardDelayOID, &Dot1dStpForwardDelayEntry);
    SNMPUnRegisterMib (&Dot1dStpBridgeMaxAgeOID, &Dot1dStpBridgeMaxAgeEntry);
    SNMPUnRegisterMib (&Dot1dStpBridgeHelloTimeOID,
                       &Dot1dStpBridgeHelloTimeEntry);
    SNMPUnRegisterMib (&Dot1dStpBridgeForwardDelayOID,
                       &Dot1dStpBridgeForwardDelayEntry);
    SNMPUnRegisterMib (&Dot1dStpVersionOID, &Dot1dStpVersionEntry);
    SNMPUnRegisterMib (&Dot1dStpTxHoldCountOID, &Dot1dStpTxHoldCountEntry);
    SNMPUnRegisterMib (&Dot1dTpLearnedEntryDiscardsOID,
                       &Dot1dTpLearnedEntryDiscardsEntry);
    SNMPUnRegisterMib (&Dot1dTpAgingTimeOID, &Dot1dTpAgingTimeEntry);
    SNMPDelSysorEntry (&stdbriOID, (const UINT1 *) "stdbridge");
}
