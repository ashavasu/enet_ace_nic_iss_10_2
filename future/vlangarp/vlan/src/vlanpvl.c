/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanpvl.c,v 1.25.24.1 2018/03/15 13:00:04 siva Exp $
 *
 * Description: This file contains pvlan utility routines used in VLAN module.
 *
 *******************************************************************/

#include "vlaninc.h"

#include "fsmpvlcli.h"
#include "fsmsvlcli.h"
#include "ip.h"
#include "arp.h"
#include "vlancli.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUtilHandlePortTypeDependency                 */
/*                                                                           */
/*    Description         : This function takes care of handling the         */
/*                          dependencies to be set while changing the port   */
/*                          type from host/promiscuous to other and          */
/*                          vice-versa.                                      */
/*                                                                           */
/*    Input(s)            : i4Dot1qFutureVlanPort - Port number.             */
/*                          i4SetValDot1qFutureVlanPortType - Type of the    */
/*                                 Port. Can take the values as              */
/*                                 - VLAN_ACCESS_PORT                        */
/*                                 - VLAN_HYBRID_PORT                        */
/*                                 - VLAN_TRUNK_PORT                         */
/*                                 - VLAN_HOST_PORT                          */
/*                                 - VLAN_PROMISCOUS_PORT                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUtilHandlePortTypeDependency (INT4 i4Dot1qFutureVlanPort,
                                  INT4 i4SetValDot1qFutureVlanPortType)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT1               u1PortType;

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    u1PortType = pPortEntry->u1PortType;

    if (((u1PortType == VLAN_HOST_PORT) ||
         (u1PortType == VLAN_PROMISCOUS_PORT)) &&
        ((i4SetValDot1qFutureVlanPortType == VLAN_HYBRID_PORT) ||
         (i4SetValDot1qFutureVlanPortType == VLAN_ACCESS_PORT) ||
         (i4SetValDot1qFutureVlanPortType == VLAN_TRUNK_PORT)))
    {
        /* Reset the following to default value 
         * - PVID */
        if (VlanUtilSetPvid (i4Dot1qFutureVlanPort,
                             VLAN_DEF_VLAN_ID) == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }

        /* While configuring the pvlan port type to access / hybrid
         * the follwing the steps needs to be taken care.
         * - Remove the port from pvlan
         * - Configure the pvid of that port to default vlan id */
        VlanConfigPvlanPortToNormal ((UINT2) i4Dot1qFutureVlanPort);
        if (VlanUtilSetIngressFiltering (i4Dot1qFutureVlanPort,
                                         VLAN_DISABLED) == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }

    }
    else if (((u1PortType != VLAN_HOST_PORT) ||
              (u1PortType != VLAN_PROMISCOUS_PORT)) &&
             ((i4SetValDot1qFutureVlanPortType == VLAN_HOST_PORT) ||
              (i4SetValDot1qFutureVlanPortType == VLAN_PROMISCOUS_PORT)))
    {
        /* If a hybrid/access/trunck port is configured as a pvlan port type,
         * then just update the port type, enable ingress filtering
         * and return */
        if (VlanUtilSetIngressFiltering (i4Dot1qFutureVlanPort,
                                         VLAN_ENABLED) == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetPvlanMemberPorts                          */
/*                                                                           */
/*    Description         : This function sets the member ports of associated*/
/*                          pvlans of the given vlan.                        */
/*    Input(s)            : VlanId - vlan identifier.                        */
/*                          pAddedPorts - Added ports of the given vlan.     */
/*                          pDeletedPorts - Deleted ports of the given vlan. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : NONE.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanSetPvlanMemberPorts (tVlanId VlanId,
                         tLocalPortList AddedPorts, tLocalPortList DeletedPorts)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2PortInd;
    UINT1               u1Result;

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
    {
        VLAN_SCAN_PORT_TABLE (u2PortInd)
        {
            VLAN_IS_MEMBER_PORT (AddedPorts, u2PortInd, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (u2PortInd);

                if (pPortEntry != NULL)
                {
                    VlanSetPvlanProperties (pPortEntry,
                                            &L2PvlanMappingInfo,
                                            u2PortInd, VLAN_ADD);
                }
            }

            /* The member ports of the pvlan are deleted, then the
             * following function is to handle the behaviour */

            VLAN_IS_MEMBER_PORT (DeletedPorts, u2PortInd, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                VlanSetPvlanProperties (NULL, &L2PvlanMappingInfo,
                                        u2PortInd, VLAN_DELETE);
            }
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : VlanSetPvlanProperties                               */
/*                                                                           */
/* Description        : This routine is called to configure the pvlan        */
/*                      properties when a member ports of a pvlan is         */
/*                      configured.                                          */
/*                                                                           */
/* Input(s)           : pPortEntry - vlan port entry.                        */
/*                      pL2PvlanMappingInfo - pointer to L2PvlanMappingInfo. */
/*                      u2PortInd - Port number.                             */
/*                      u1Action - ADD 0r DELETE. It will take the following */
/*                                 vlaues. VLAN_ADD / VLAN_DELETE.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanSetPvlanProperties (tVlanPortEntry * pPortEntry,
                        tL2PvlanMappingInfo * pL2PvlanMappingInfo,
                        UINT2 u2PortInd, UINT1 u1Action)
{
    tL2PvlanMappingInfo PvlanMapInfo;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (u1Action == VLAN_ADD)
    {
        if (pPortEntry != NULL)
        {
            if ((pPortEntry->u1PortType == VLAN_HYBRID_PORT) ||
                (pPortEntry->u1PortType == VLAN_ACCESS_PORT))
            {
                VlanUtilSetVlanPortType (u2PortInd, VLAN_HOST_PORT);
                VlanUtilSetIngressFiltering (u2PortInd, VLAN_ENABLED);
            }
            PvlanMapInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
            PvlanMapInfo.InVlanId = pL2PvlanMappingInfo->InVlanId;

            VlanL2IwfGetPVlanMappingInfo (&PvlanMapInfo);
            /* (i) Only for the promiscous ports added to primary vlan 
             *     set the Pvid as Primary vlan Id. 
             * (ii)Only for the host ports added to isolated/community vlans
             *     set the Pvid as isolated/community vlan id. 
             */
            if (((pPortEntry->u1PortType == VLAN_HOST_PORT) &&
                 (PvlanMapInfo.u1VlanType != L2IWF_PRIMARY_VLAN)) ||
                ((pPortEntry->u1PortType == VLAN_PROMISCOUS_PORT) &&
                 (PvlanMapInfo.u1VlanType == L2IWF_PRIMARY_VLAN)))
            {
                VlanUtilSetPvid ((INT4) u2PortInd,
                                 pL2PvlanMappingInfo->InVlanId);
            }
            if (pL2PvlanMappingInfo->u1VlanType == L2IWF_ISOLATED_VLAN)
            {
                VlanUpdatePortIsolation (pL2PvlanMappingInfo);
            }

            if ((pL2PvlanMappingInfo->u1VlanType == L2IWF_COMMUNITY_VLAN))
            {
                VlanUpdateCommunityPortIsolation (pL2PvlanMappingInfo);

            }
        }
    }
    else
    {
        VlanDelPortsFromPvlan (pL2PvlanMappingInfo, u2PortInd);
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2PortInd);

        if (pVlanPortEntry == NULL)
        {
            return;
        }

        if (((pVlanPortEntry->u1PortType == VLAN_HOST_PORT) &&
             (pL2PvlanMappingInfo->u1VlanType != L2IWF_PRIMARY_VLAN)) ||
            ((pVlanPortEntry->u1PortType == VLAN_PROMISCOUS_PORT) &&
             (pL2PvlanMappingInfo->u1VlanType == L2IWF_PRIMARY_VLAN)))
        {

            VlanUtilSetPvid ((INT4) u2PortInd, VLAN_DEF_VLAN_ID);
            return;
        }

    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanUpdatePortIsolation                              */
/*                                                                           */
/* Description        : This routine is called to configure the port         */
/*                      isolation table when an isolated vlan is gone active */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier.                    */
/*                      VlanId - Vlan Identifier                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanUpdatePortIsolation (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    UINT1              *pHostPortList = NULL;
    tIssUpdtPortIsolation PortIsolation;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT4               au4UpLinkPorts[ISS_MAX_UPLINK_PORTS];
    UINT4               u4TempPort = 0;
    UINT2               au2EgressPorts[ISS_MAX_UPLINK_PORTS];
    UINT2               u2PortIndex = 0;
    UINT2               u2NumPorts = 0;
    UINT1               u1Result = VLAN_FALSE;

    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (au2EgressPorts, 0, ISS_MAX_UPLINK_PORTS);

    if (VLAN_IS_VLAN_ID_VALID (pL2PvlanMappingInfo->InVlanId) == VLAN_FALSE)
    {
        return VLAN_FAILURE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (pL2PvlanMappingInfo->InVlanId);

    if (pVlanCurrEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    if (pL2PvlanMappingInfo->u1VlanType != L2IWF_ISOLATED_VLAN)
    {
        return VLAN_SUCCESS;
    }
    pHostPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pHostPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanUpdatePortIsolation: Error in allocating memory "
                  "for pHostPortList\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pHostPortList, 0, sizeof (tLocalPortList));
    VlanConstHostAndUpLinkPorts (pVlanCurrEntry, pHostPortList,
                                 au2EgressPorts, &u2NumPorts);

    VlanPgmPortIsoTblForHostPorts (pHostPortList, au2EgressPorts, u2NumPorts,
                                   ISS_PI_ADD);
    UtilPlstReleaseLocalPortList (pHostPortList);
    /* Update the port isolation table only when there exist egress ports 
     * to program. The u2NumPorts equivalent to 1 means, there exists only 
     * one uplink port in the sytem (trunk/promiscuous). 
     * Hence Port Isolation table need not be programmed. 
     */
    if (u2NumPorts <= 1)
    {
        return VLAN_SUCCESS;
    }

    /* Program the port isolation table with trunk port as IngressPort,
     * InVlanId and Other TrunkPortList + PromiscuousPortList as egress port
     * list */
    for (u2PortIndex = 0; u2PortIndex < u2NumPorts; u2PortIndex++)
    {
        VLAN_IS_CURR_TRUNK_PORT (pVlanCurrEntry, au2EgressPorts[u2PortIndex],
                                 u1Result);

        if (u1Result == VLAN_FALSE)
        {
            continue;
        }

        VlanConvertLPortArrayToPhyPorts (au2EgressPorts, au4UpLinkPorts,
                                         u2NumPorts);
        u4TempPort = au4UpLinkPorts[u2PortIndex];

        PortIsolation.u4IngressPort = au4UpLinkPorts[u2PortIndex];
        PortIsolation.InVlanId = pL2PvlanMappingInfo->InVlanId;
        if (VLAN_GET_PHY_PORT (au2EgressPorts[u2PortIndex]) == u4TempPort)
        {
            au4UpLinkPorts[u2PortIndex] = 0;
        }
        PortIsolation.pu4EgressPorts = au4UpLinkPorts;
        PortIsolation.u1Action = ISS_PI_ADD;
        PortIsolation.u2NumEgressPorts = u2NumPorts;

        VlanIssApiUpdtPortIsolationEntry (&PortIsolation);

        au4UpLinkPorts[u2PortIndex] = u4TempPort;
    }
    VlanUpdateCommunityPortIsolation (pL2PvlanMappingInfo);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPgmPortIsoTblForHostPorts                        */
/*                                                                           */
/* Description        : This function programs the port isolation table      */
/*                      entry for the given list of host ports.              */
/*                                                                           */
/* Input(s)           : HostPortList - Host ports that is going to be the    */
/*                      index of each host port, egress ports entry.         */
/*                      pu2EgressPorts - Pointer to the egress ports.        */
/*                      u2NumPorts - Number of egress ports present in the   */
/*                                   pu2EgressPorts                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanPgmPortIsoTblForHostPorts (tLocalPortList HostPortList,
                               UINT2 *pu2EgressPorts,
                               UINT2 u2NumPorts, UINT1 u1Action)
{
    tIssUpdtPortIsolation PortIsolation;
    UINT4               au4UpLinkPorts[ISS_MAX_UPLINK_PORTS];
    UINT2               u2Port = 0;
    BOOL1               b1Result = VLAN_FALSE;

    /* Update the port isolation table only when there exist entries to
     * program.
     */
    VlanConvertLPortArrayToPhyPorts (pu2EgressPorts, au4UpLinkPorts,
                                     u2NumPorts);
    /* Program the port isolation table with the host port as ingress port
     * and PromiscuousPortList + TrunkPortList as EgressPortList */
    for (u2Port = 1; u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_MEMBER_PORT (HostPortList, u2Port, b1Result);

        if (b1Result == VLAN_FALSE)
        {
            continue;
        }
        PortIsolation.u4IngressPort = VLAN_GET_IFINDEX (u2Port);
        PortIsolation.InVlanId = 0;
        PortIsolation.u1Action = u1Action;
        PortIsolation.pu4EgressPorts = au4UpLinkPorts;
        PortIsolation.u2NumEgressPorts = u2NumPorts;

        VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : VlanConstHostAndUpLinkPorts                          */
/*                                                                           */
/* Description        : This functions gets the list of host ports and list  */
/*                      of uplink ports from the given vlan.                 */
/*                                                                           */
/* Input(s)           : pVlanCurrEntry - Current vlan entry.                 */
/*                                                                           */
/* Output(s)          : HostPortList - The list of ports that configured as  */
/*                                      host port.                           */
/*                      pu4UpLinkPorts - Both the trunk and promiscuous      */
/*                                       ports.                              */
/*                      pu2NumPorts - Number of trunk and promiscuous        */
/*                                    ports                                  */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanConstHostAndUpLinkPorts (tVlanCurrEntry * pVlanCurrEntry,
                             tLocalPortList HostPortList,
                             UINT2 *pu2UpLinkPorts, UINT2 *pu2NumPorts)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2Index = 0;
    UINT1               u1PortType;
    UINT1               u1Result = VLAN_FALSE;

    /* Construct the port lists.
     * HostPortList, TrunkPortList, PromiscuousPortList */
    for (u2Port = 1; u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        VLAN_IS_CURR_EGRESS_PORT (pVlanCurrEntry, u2Port, u1Result);

        if (u1Result == VLAN_FALSE)
        {
            continue;
        }

        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }

        u1PortType = pPortEntry->u1PortType;

        if ((u1PortType == VLAN_ACCESS_PORT) ||
            (u1PortType == VLAN_HYBRID_PORT))
        {
            continue;
        }

        if (u1PortType == VLAN_HOST_PORT)
        {
            VLAN_SET_MEMBER_PORT (HostPortList, u2Port);
        }
        else if (u2Index < ISS_MAX_UPLINK_PORTS)
        {
            /* this port is a trunk or promiscuous port. */
            pu2UpLinkPorts[u2Index] = u2Port;
            u2Index++;
        }
        *pu2NumPorts = u2Index;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanUpdtIsolationTblForVlanDel                       */
/*                                                                           */
/* Description        : This routine is called to delete the port isolation  */
/*                      table of the deleted pvlan.                          */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - Pointer to the                 */
/*                                            L2PvlanMappingInfo information.*/
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanUpdtIsolationTblForVlanDel (tVlanId VlanId)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tIssUpdtPortIsolation PortIsolation;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2PortIndex = 0;
    UINT1               u1PortType = 0;
    UINT1               u1Result = VLAN_FALSE;

    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    pVlanCurrEntry = VlanGetVlanEntry (VlanId);

    if (pVlanCurrEntry != NULL)
    {
        for (u2PortIndex = 0; u2PortIndex < VLAN_MAX_PORTS; u2PortIndex++)
        {
            VLAN_IS_CURR_EGRESS_PORT (pVlanCurrEntry, u2PortIndex, u1Result);

            if (u1Result == VLAN_FALSE)
            {
                continue;
            }
            pPortEntry = VLAN_GET_PORT_ENTRY (u2PortIndex);

            if (pPortEntry == NULL)
            {
                continue;
            }

            u1PortType = pPortEntry->u1PortType;

            /* If it is a isolated vlan, delete the port isolation table
             * for that vlan also. */
            if (L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN)
            {
                PortIsolation.u4IngressPort = VLAN_GET_PHY_PORT (u2PortIndex);
                PortIsolation.u1Action = ISS_PI_DELETE;
                if (u1PortType == VLAN_HOST_PORT)
                {
                    PortIsolation.InVlanId = 0;
                }
                else if (u1PortType == VLAN_TRUNK_PORT)
                {
                    PortIsolation.InVlanId = L2PvlanMappingInfo.InVlanId;
                }

                VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanUpdtIsolationTblForIccl                          */
/*                                                                           */
/* Description        : This routine is called to delete the port isolation  */
/*                      table of the deleted VLAN.                           */
/*                                                                           */
/* Input(s)           : VlanId - VLAN ID                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*****************************************************************************/
VOID
VlanUpdtIsolationTblForIccl (tVlanId VlanId)
{
    tIssUpdtPortIsolation PortIsolation;
    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    VlanIcchGetIcclIfIndex (&(PortIsolation.u4IngressPort));
    PortIsolation.InVlanId = VlanId;
    PortIsolation.u1Action = ISS_PI_DELETE;
    VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanDelPortsFromPvlan                                */
/*                                                                           */
/* Description        : This routine is called on deleting a member port from*/
/*                      a pvlan.                                             */
/*                                                                           */
/* Input(s)           : VlanId - Vlan Id                                     */
/*                      u2Port - Deleted port                                */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanDelPortsFromPvlan (tL2PvlanMappingInfo * pL2PvlanMappingInfo, UINT2 u2Port)
{
    tL2PvlanMappingInfo L2SecondaryVlanMappingInfo;
    tIssUpdtPortIsolation PortIsolation;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT4               u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    UINT2              *pu2SecVlan = NULL;
    UINT2               u2VlanIndex;
    UINT1               u1PortType;
    UINT1               u1Result;
    INT4                i4VlanStaticPort = 0;

    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (&L2SecondaryVlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    if (u2Port >= VLAN_MAX_PORTS)
    {
        return;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return;
    }

    u1PortType = pPortEntry->u1PortType;

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        return;
    }
    pL2PvlanMappingInfo->u1RequestType = L2IWF_MAPPED_VLANS;
    pL2PvlanMappingInfo->pMappedVlans = pu2SecVlan;
    pL2PvlanMappingInfo->u4ContextId = u4CurrContextId;
    VlanL2IwfGetPVlanMappingInfo (pL2PvlanMappingInfo);

    switch (pL2PvlanMappingInfo->u1VlanType)
    {
        case L2IWF_ISOLATED_VLAN:
        case L2IWF_COMMUNITY_VLAN:

            if ((u1PortType != VLAN_HYBRID_PORT) ||
                (u1PortType != VLAN_ACCESS_PORT))
            {
                /* Get the primary vlan entry */
                pStVlanEntry =
                    VlanGetStaticVlanEntry
                    (*(pL2PvlanMappingInfo->pMappedVlans));

                if (pStVlanEntry != NULL)
                {
                    /* Get the type of port */
                    VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        i4VlanStaticPort = VLAN_DEL_UNTAGGED_PORT;
                    }
                    else
                    {
                        i4VlanStaticPort = VLAN_DEL_TAGGED_PORT;
                    }
                }
                if (u1PortType == VLAN_PROMISCOUS_PORT)
                {
                    break;
                }
                /* Delete that port from primary vlan */
                VlanUtilMISetVlanStEgressPort
                    (u4CurrContextId,
                     *(pL2PvlanMappingInfo->pMappedVlans), u2Port,
                     i4VlanStaticPort);

                VlanSelectContext (u4CurrContextId);
                if (pL2PvlanMappingInfo->u1VlanType == L2IWF_ISOLATED_VLAN)
                {
                    /* If it is a isolated vlan, delete the port isolation
                     * table for that port also. */
                    PortIsolation.u4IngressPort = VLAN_GET_PHY_PORT (u2Port);
                    if (u1PortType == VLAN_HOST_PORT)
                    {
                        PortIsolation.InVlanId = 0;
                    }
                    else if (u1PortType == VLAN_TRUNK_PORT)
                    {
                        PortIsolation.InVlanId = pL2PvlanMappingInfo->InVlanId;
                    }

                    PortIsolation.u2NumEgressPorts = 0;
                    PortIsolation.u1Action = ISS_PI_DELETE;

                    VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
                }
            }
            break;

        case L2IWF_PRIMARY_VLAN:

            if ((u1PortType == VLAN_PROMISCOUS_PORT) ||
                (u1PortType == VLAN_HOST_PORT))
            {
                for (u2VlanIndex = 0;
                     u2VlanIndex < pL2PvlanMappingInfo->u2NumMappedVlans;
                     u2VlanIndex++)
                {
                    /* Get all the secondary vlan entries associated with the
                     * given primary vlans */
                    pStVlanEntry =
                        VlanGetStaticVlanEntry
                        (pL2PvlanMappingInfo->pMappedVlans[u2VlanIndex]);

                    if (pStVlanEntry != NULL)
                    {
                        /* Get the type of port in all the secondary vlans */
                        VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result);

                        if (u1Result == VLAN_TRUE)
                        {
                            i4VlanStaticPort = VLAN_DEL_UNTAGGED_PORT;
                        }
                        else
                        {
                            i4VlanStaticPort = VLAN_DEL_TAGGED_PORT;
                        }
                        /*  Delete the port from u2VlanIndex
                         * (isolated and community vlans) */
                        VlanUtilMISetVlanStEgressPort
                            (u4CurrContextId,
                             pL2PvlanMappingInfo->pMappedVlans[u2VlanIndex],
                             u2Port, i4VlanStaticPort);
                        VlanSelectContext (u4CurrContextId);

                        L2SecondaryVlanMappingInfo.InVlanId =
                            pL2PvlanMappingInfo->pMappedVlans[u2VlanIndex];
                        L2SecondaryVlanMappingInfo.u1RequestType =
                            L2IWF_VLAN_TYPE;
                        L2SecondaryVlanMappingInfo.u4ContextId =
                            u4CurrContextId;

                        VlanL2IwfGetPVlanMappingInfo
                            (&L2SecondaryVlanMappingInfo);

                        if (L2SecondaryVlanMappingInfo.u1VlanType ==
                            L2IWF_ISOLATED_VLAN)
                        {
                            /* If it is a isolated vlan, delete the port
                             * isolation table for that vlan also. */
                            PortIsolation.u4IngressPort =
                                VLAN_GET_PHY_PORT (u2Port);
                            if (u1PortType == VLAN_HOST_PORT)
                            {
                                PortIsolation.InVlanId = 0;
                            }
                            else if (u1PortType == VLAN_TRUNK_PORT)
                            {
                                PortIsolation.InVlanId =
                                    pL2PvlanMappingInfo->InVlanId;
                            }

                            PortIsolation.u2NumEgressPorts = 0;
                            PortIsolation.u1Action = ISS_PI_DELETE;

                            VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
                        }
                    }
                }
            }
            break;

        default:
            break;

    }
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;
    return;
}

/*****************************************************************************/
/* Function Name      : VlanTestConfigMembersOnPvlan                         */
/*                                                                           */
/* Description        : This function checks the member port eligibility for */
/*                      secondary vlans.                                     */
/*                                                                           */
/* Input(s)           : u4Dot1qVlanIndex - Vlan Id                           */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - Error code has to be return on failure*/
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/ VLAN_FAILURE                           */
/*****************************************************************************/
INT4
VlanTestConfigMembersOnPvlan (UINT4 u4Dot1qVlanIndex, UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pEgressPorts)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2              *pu2SecVlan = NULL;
    INT4                i4RetVal = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1Result;
    UINT2               u2PortInd;
    VLAN_MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        return VLAN_FAILURE;
    }
    MEMSET (pu2SecVlan, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = (tVlanId) u4Dot1qVlanIndex;
    L2PvlanMappingInfo.pMappedVlans = pu2SecVlan;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    /* The following code is to check the members of the secondary vlan
     * is subset of primary vlan. Else dont allow the configuration */
    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if ((L2PvlanMappingInfo.u1VlanType == L2IWF_COMMUNITY_VLAN) ||
        (L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN))
    {
        if (L2PvlanMappingInfo.u2NumMappedVlans != 0)
        {
            pStVlanEntry =
                VlanGetStaticVlanEntry (*(L2PvlanMappingInfo.pMappedVlans));

            if (pStVlanEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;
                return VLAN_FAILURE;
            }
            VLAN_IS_PORTLIST_SUBSET_TO_EGRESS (pStVlanEntry,
                                               pEgressPorts->pu1_OctetList,
                                               u1Result);
            if (u1Result == VLAN_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_VLAN_PVLAN_SUBSET_ERR);

                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;
                return VLAN_FAILURE;
            }
        }

    }
    if (L2PvlanMappingInfo.u1VlanType == L2IWF_PRIMARY_VLAN)
    {
        i4RetVal =
            VlanChkPortsInPrimVlansAreExclusive (u4Dot1qVlanIndex,
                                                 L2IWF_PRIMARY_VLAN,
                                                 pEgressPorts);
        if (i4RetVal == VLAN_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_PVLAN_PRIMARY);
            MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                (UINT1 *) pu2SecVlan);
            pu2SecVlan = NULL;
            return VLAN_FAILURE;

        }
    }
    /* if vlan is normal vlan , always return success */
    if (L2PvlanMappingInfo.u1VlanType == L2IWF_NORMAL_VLAN)
    {
        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                            (UINT1 *) pu2SecVlan);
        pu2SecVlan = NULL;

        return VLAN_SUCCESS;

    }

    VLAN_SCAN_PORT_TABLE (u2PortInd)
    {
        OSIX_BITLIST_IS_BIT_SET (pEgressPorts->pu1_OctetList, u2PortInd,
                                 VLAN_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_TRUE)
        {

            pPortEntry = VLAN_GET_PORT_ENTRY (u2PortInd);

            if (pPortEntry != NULL)
            {
                if (pPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.
                                        VlanPvlanPoolId, (UINT1 *) pu2SecVlan);

                    CLI_SET_ERR (CLI_VLAN_PVLAN_ERR);
                    return VLAN_FAILURE;

                }
                if ((pPortEntry->Pvid != u4Dot1qVlanIndex))
                {

                    if (((pPortEntry->u1PortType == VLAN_HOST_PORT) &&
                         (L2PvlanMappingInfo.u1VlanType != L2IWF_PRIMARY_VLAN))
                        || ((pPortEntry->u1PortType == VLAN_PROMISCOUS_PORT)
                            && (L2PvlanMappingInfo.u1VlanType ==
                                L2IWF_PRIMARY_VLAN)))
                    {

                        if (pPortEntry->Pvid != VLAN_DEF_VLAN_ID)
                        {
                            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                            MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.
                                                VlanPvlanPoolId,
                                                (UINT1 *) pu2SecVlan);
                            pu2SecVlan = NULL;
                            CLI_SET_ERR (CLI_VLAN_SECONDARY_ERR);
                            return VLAN_FAILURE;

                        }
                    }
                }
            }
        }
    }

    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanConfigPvlanPortToNormal                          */
/*                                                                           */
/* Description        : This routine is called to configure the pvlan ports  */
/*                      to access / hybrid / trunk.                          */
/*                                                                           */
/* Input(s)           : u4Dot1qVlanIndex - Vlan Id                           */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - Error code has to be return on failure*/
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanConfigPvlanPortToNormal (UINT2 u2Port)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    UINT1              *pu1VlanList = NULL;
    UINT2               u2VlanIndex;
    BOOL1               b1Result = VLAN_FALSE;
    UINT1               u1Result;
    INT4                i4VlanStaticPort = 0;

    if (u2Port > VLAN_MAX_PORTS)
    {
        return;
    }

    pu1VlanList = UtlShMemAllocVlanList ();
    if (pu1VlanList == NULL)
    {
        return;
    }

    MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);
    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    L2IwfGetPortVlanMemberList (VLAN_GET_IFINDEX (u2Port), pu1VlanList);

    for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u2VlanIndex, VLAN_LIST_SIZE,
                                 b1Result);

        if (b1Result == OSIX_TRUE)
        {
            L2PvlanMappingInfo.u4ContextId = u4CurrContextId;
            L2PvlanMappingInfo.InVlanId = u2VlanIndex;
            L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

            VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

            if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
            {
                pStVlanEntry = VlanGetStaticVlanEntry (u2VlanIndex);
                if (pStVlanEntry != NULL)
                {
                    VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2Port, u1Result);
                    if (u1Result == VLAN_TRUE)
                    {
                        i4VlanStaticPort = VLAN_DEL_UNTAGGED_PORT;
                    }
                    else
                    {
                        i4VlanStaticPort = VLAN_DEL_TAGGED_PORT;
                    }
                }
                VlanUtilMISetVlanStEgressPort (u4CurrContextId,
                                               u2VlanIndex,
                                               u2Port, i4VlanStaticPort);
                VlanSelectContext (u4CurrContextId);
            }
        }
    }
    UtlShMemFreeVlanList (pu1VlanList);

    return;
}

#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
/*****************************************************************************/
/*    Function Name       : VlanPvlanFdbTableAdd                             */
/*                                                                           */
/*    Description         : This function adds the FDB entry for the private */
/*                          vlans to the RB tree. If the give VlanId is      */
/*                          primary then it adds for all the secondary vlans */
/*                          else it adds for primary vlan.                   */
/*                                                                           */
/*    Input(s)            : u2Port - Port on which the entry is learned      */
/*                          MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanPvlanFdbTableAdd (UINT2 u2Port, tMacAddr MacAddr, tVlanId VlanId,
                      UINT1 u1EntryType, tMacAddr ConnectionId)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tLocalPortList      AllowedToGoPortList;
    UINT2               u2VlanIndex = 0;
    UINT2              *pu2SecVlan = NULL;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        return;
    }
    MEMSET (pu2SecVlan, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.pMappedVlans = pu2SecVlan;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
    {
        for (u2VlanIndex = 0; u2VlanIndex < L2PvlanMappingInfo.u2NumMappedVlans;
             u2VlanIndex++)
        {
            VlanFdbTableAddInCtxt (u2Port, MacAddr,
                                   L2PvlanMappingInfo.pMappedVlans[u2VlanIndex],
                                   u1EntryType, ConnectionId, FNP_ZERO,
                                   FNP_ZERO, FNP_ZERO);

            MEMSET (AllowedToGoPortList, 0, sizeof (AllowedToGoPortList));

            /* Program the hardware */
            VLAN_SET_MEMBER_PORT (AllowedToGoPortList, u2Port);

            if (VlanHwAddStaticUcastEntry
                (L2PvlanMappingInfo.pMappedVlans[u2VlanIndex], MacAddr,
                 0, AllowedToGoPortList, VLAN_DELETE_ON_RESET, ConnectionId)
                == VLAN_FAILURE)
            {
                break;
            }
        }
    }
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;

    return;
}

/*****************************************************************************/
/*    Function Name       : VlanPvlanFdbTableRem                             */
/*                                                                           */
/*    Description         : This function removes the FDB entry for the      */
/*                          private vlans to the RB tree. If the give VlanId */
/*                          is primary then it remove for all the secondary  */
/*                          vlans else it remove for primary vlan.           */
/*                                                                           */
/*    Input(s)            : MacAddr - MAC address                            */
/*                          VlanId  - VlanID                                 */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanPvlanFdbTableRem (tVlanId VlanId, tMacAddr MacAddr)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2VlanIndex = 0;
    UINT2              *pu2SecVlan = NULL;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        return;
    }
    MEMSET (pu2SecVlan, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.pMappedVlans = pu2SecVlan;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
    {
        for (u2VlanIndex = 0; u2VlanIndex < L2PvlanMappingInfo.u2NumMappedVlans;
             u2VlanIndex++)
        {
            VlanFdbTableRemoveInCtxt
                (L2PvlanMappingInfo.pMappedVlans[u2VlanIndex], MacAddr,
                 VLAN_LOCAL_FDB);

            /* Program the hardware */
            if (VlanHwDelStaticUcastEntry
                (L2PvlanMappingInfo.pMappedVlans[u2VlanIndex], MacAddr,
                 0) == VLAN_FAILURE)
            {
                break;
            }
        }
    }
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;

    return;
}
#endif
#endif
/*****************************************************************************/
/*    Function Name       : VlanConvertLPortArrayToPhyPorts                  */
/*                                                                           */
/*    Description         : This function converts the local port list array */
/*                          in to physical port list array.                  */
/*                                                                           */
/*    Input(s)            : pu2EgressPorts - Pointer to the local portlist   */
/*                                           array.                          */
/*                          u2NumPorts - Number of ports in the array.       */
/*    Output(s)           : pu4UpLinkPorts - Pointer to the physical port    */
/*                                           list array.                     */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanConvertLPortArrayToPhyPorts (UINT2 *pu2EgressPorts, UINT4 *pu4UpLinkPorts,
                                 UINT2 u2NumPorts)
{
    UINT2               u2PortIndex = 0;

    for (u2PortIndex = 0; u2PortIndex < u2NumPorts; u2PortIndex++)
    {
        pu4UpLinkPorts[u2PortIndex] =
            VLAN_GET_PHY_PORT (pu2EgressPorts[u2PortIndex]);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanSetDefPortTypeForPvlan                           */
/*                                                                           */
/* Description        : This routine is called to set the port type of the   */
/*                      members of the pvlan.                                */
/*                                                                           */
/* Input(s)           : VlanId - Vlan Id as index                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSetDefPortTypeForPvlan (tVlanId VlanId)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT2               u2PortIndex = 0;
    UINT1               u1Result = VLAN_FALSE;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
    {
        return L2IWF_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    VLAN_SCAN_PORT_TABLE (u2PortIndex)
    {
        VLAN_IS_EGRESS_PORT (pStVlanEntry, u2PortIndex, u1Result);

        if (u1Result == VLAN_FALSE)
        {
            continue;
        }

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2PortIndex);

        L2PvlanMappingInfo.InVlanId = VlanId;
        L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
        L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

        if (pVlanPortEntry != NULL)
        {
            VlanSetPvlanProperties (pVlanPortEntry, &L2PvlanMappingInfo,
                                    u2PortIndex, VLAN_ADD);
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPgmPITableForAllVlans                            */
/*                                                                           */
/* Description        : This routine is called to program  the port isolation*/
/*                      table for all the Vlans.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanPgmPITableForAllVlans (VOID)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tL2PvlanMappingInfo PvlanMapInfo;
    tVlanId             VlanId;

    MEMSET (&PvlanMapInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* For all the Vlan Current table entries */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pVlanEntry)
        {
            continue;
        }

        PvlanMapInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
        PvlanMapInfo.InVlanId = VlanId;
        PvlanMapInfo.u1RequestType = L2IWF_VLAN_TYPE;

        VlanL2IwfGetPVlanMappingInfo (&PvlanMapInfo);

        /* Program the port isolation table for isolated vlans alone. */
        if (L2IWF_ISOLATED_VLAN == PvlanMapInfo.u1VlanType)
        {
            VlanUpdatePortIsolation (&PvlanMapInfo);
        }

    }                            /* End of scanning vlan table */

    return;
}                                /* End of VlanPgmPortIsoTableForPvlan */

/*****************************************************************************/
/* Function Name      : VlanRePgmPITableForTrunkPorts                        */
/*                                                                           */
/* Description        : This routine is called to re-program  the port       */
/*                      table for the trunk port type change.                */
/*                                                                           */
/* Input(s)           : u2TrunkPort - TrunkPort interface index.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanRePgmPITableForTrunkPorts (UINT2 u2TrunkPort)
{
    UINT1              *pHostPortList = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tL2PvlanMappingInfo PvlanMapInfo;
    tIssUpdtPortIsolation PortIsolation;
    UINT2               au2EgressPorts[ISS_MAX_UPLINK_PORTS];
    UINT4               u4TrunkIfIndex = VLAN_GET_IFINDEX (u2TrunkPort);
    tVlanId             VlanId = 0;
    UINT2               u2NumPorts = 0;
    UINT2               u2PortIndex = 0;
    UINT1               u1Result = VLAN_FALSE;

    pHostPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pHostPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRePgmPITableForTrunkPorts: Error in allocating memory "
                  "for pHostPortList\r\n");
        return;
    }
    MEMSET (pHostPortList, 0, sizeof (tLocalPortList));

    MEMSET (&PvlanMapInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (au2EgressPorts, 0, (sizeof (UINT2) * ISS_MAX_UPLINK_PORTS));

    /* For all the Vlan Current table entries */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pVlanEntry)
        {
            continue;
        }

        PvlanMapInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
        PvlanMapInfo.InVlanId = VlanId;
        PvlanMapInfo.u1RequestType = L2IWF_VLAN_TYPE;

        VlanL2IwfGetPVlanMappingInfo (&PvlanMapInfo);

        /* Program the port isolation table for isolated vlans alone. */
        if (L2IWF_ISOLATED_VLAN == PvlanMapInfo.u1VlanType)
        {
            /* Get the host ports, promiscuous and trunk ports for this
             * isolated vlan. 
             */
            VlanConstHostAndUpLinkPorts (pVlanEntry, pHostPortList,
                                         au2EgressPorts, &u2NumPorts);

            /* Delete the Port Isolation table entries for this
             * (hostport, trunk port) pair. 
             */
            VlanPgmPortIsoTblForHostPorts (pHostPortList,
                                           &u2TrunkPort, 1, ISS_PI_DELETE);

            /* Delete Port Isolation entries for 
             * (other trunk port, VlanId, this trunk port)  in the
             * system.
             */
            for (u2PortIndex = 0; u2PortIndex < u2NumPorts; u2PortIndex++)
            {
                VLAN_IS_CURR_TRUNK_PORT (pVlanEntry,
                                         au2EgressPorts[u2PortIndex], u1Result);

                if (u1Result == VLAN_FALSE)
                {
                    /* This is a promiscuous port. */
                    continue;
                }

                PortIsolation.u4IngressPort =
                    VLAN_GET_IFINDEX (au2EgressPorts[u2PortIndex]);
                PortIsolation.InVlanId = VlanId;
                PortIsolation.u1Action = ISS_PI_DELETE;
                /* If the ingress port is this trunk port, then remove
                 * the entire rule. To acheive this mark the u4IngressPort 
                 * as 0. 
                 */
                if (PortIsolation.u4IngressPort == u4TrunkIfIndex)
                {
                    PortIsolation.u2NumEgressPorts = 0;
                }
                else
                {
                    PortIsolation.u2NumEgressPorts = 1;
                    PortIsolation.pu4EgressPorts = &u4TrunkIfIndex;
                }

                VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
            }                    /* End of for */
        }

    }                            /* End of scanning vlan table */
    UtilPlstReleaseLocalPortList (pHostPortList);
}                                /* End of VlanRePgmPITableForTrunkPorts */

/*****************************************************************************/
/* Function Name      : VlanUpdateCommunityPortIsolation                     */
/*                                                                           */
/* Description        : This routine is called to program  the port          */
/*                      isolation entry for egress port as promiscous and    */
/*                      community                                            */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - pointer to L2PvlanMappingInfo  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanUpdateCommunityPortIsolation (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{

    tIssUpdtPortIsolation PortIsolation;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    tVlanCurrEntry     *pVlanCurrEntryCom = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfoCom;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tVlanId             VlanId = 0;
    UINT2               au2EgressPorts[ISS_MAX_UPLINK_PORTS];
    UINT2               au2EgressPorts1[ISS_MAX_UPLINK_PORTS];
    UINT4               au4UpLinkPorts[ISS_MAX_UPLINK_PORTS];
    UINT1              *pHostPortList = NULL;
    UINT1              *pHostPortListCom = NULL;
    UINT2              *pu2SecVlan = NULL;
    UINT4               u4TempPort = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2NumPorts = 0;
    UINT2               u2NumPortsCom = 0;
    UINT2               u2NumPortsCount = 0;
    UINT1               u1Result = VLAN_FALSE;
    UINT2               u2Port = 0;
    INT4                i4IsoCount = FALSE;

    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (au2EgressPorts, 0, ISS_MAX_UPLINK_PORTS);
    MEMSET (au2EgressPorts1, 0, ISS_MAX_UPLINK_PORTS);
    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (&L2PvlanMappingInfoCom, 0, sizeof (tL2PvlanMappingInfo));

    if (VLAN_IS_VLAN_ID_VALID (pL2PvlanMappingInfo->InVlanId) == VLAN_FALSE)
    {
        return VLAN_FAILURE;
    }

    pVlanCurrEntry = VlanGetVlanEntry (pL2PvlanMappingInfo->InVlanId);

    if (pVlanCurrEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    pHostPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pHostPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanUpdateCommunityPortIsolation: Error in allocating memory "
                  "for pHostPortList\r\n");
        return VLAN_FAILURE;
    }
    pHostPortListCom = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pHostPortListCom == NULL)
    {
        UtilPlstReleaseLocalPortList (pHostPortList);
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanUpdateCommunityPortIsolation: Error in allocating memory "
                  "for pHostPortListCom\r\n");
        return VLAN_FAILURE;
    }

    MEMSET (pHostPortList, 0, sizeof (tLocalPortList));
    MEMSET (pHostPortListCom, 0, sizeof (tLocalPortList));
    VlanConstHostAndUpLinkPorts (pVlanCurrEntry, pHostPortList,
                                 au2EgressPorts, &u2NumPorts);

    UtilPlstReleaseLocalPortList (pHostPortList);
    /* Update the port isolation table only when there exist egress ports
     * to program. The u2NumPorts equivalent to 1 means, there exists only
     * one uplink port in the sytem (trunk/promiscuous).
     * Hence Port Isolation table need not be programmed.
     */
    if (u2NumPorts <= 1)
    {
        UtilPlstReleaseLocalPortList (pHostPortListCom);
        return VLAN_SUCCESS;
    }

    /* Program the port isolation table with trunk port as IngressPort,
     * InVlanId and Other TrunkPortList + PromiscuousPortList as egress port
     * list */

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        UtilPlstReleaseLocalPortList (pHostPortListCom);
        return VLAN_FAILURE;
    }
    MEMSET (pu2SecVlan, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfoCom.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = pL2PvlanMappingInfo->InVlanId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;
    L2PvlanMappingInfo.pMappedVlans = pu2SecVlan;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u2NumMappedVlans == 0)
    {
        UtilPlstReleaseLocalPortList (pHostPortListCom);

        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                            (UINT1 *) pu2SecVlan);
        pu2SecVlan = NULL;

        return VLAN_SUCCESS;
    }

    L2PvlanMappingInfo.InVlanId = L2PvlanMappingInfo.pMappedVlans[0];
    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /*Add the portlist of all community vlan's to au2EgressPorts */

    u2NumPortsCount = u2NumPorts;

    while (L2PvlanMappingInfo.u2NumMappedVlans > 0)
    {
        L2PvlanMappingInfoCom.InVlanId =
            L2PvlanMappingInfo.pMappedVlans[L2PvlanMappingInfo.
                                            u2NumMappedVlans - 1];
        VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfoCom);
        --L2PvlanMappingInfo.u2NumMappedVlans;
        if (L2PvlanMappingInfoCom.u1VlanType == L2IWF_ISOLATED_VLAN)
        {
            VlanId = L2PvlanMappingInfoCom.InVlanId;
            i4IsoCount = TRUE;
        }
        if (L2PvlanMappingInfoCom.u1VlanType != L2IWF_COMMUNITY_VLAN)
        {
            continue;
        }
        pVlanCurrEntryCom = VlanGetVlanEntry (L2PvlanMappingInfoCom.InVlanId);
        if (pVlanCurrEntryCom == NULL)
        {
            UtilPlstReleaseLocalPortList (pHostPortListCom);
            MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                (UINT1 *) pu2SecVlan);
            pu2SecVlan = NULL;
            return VLAN_FAILURE;
        }
        VlanConstHostAndUpLinkPorts (pVlanCurrEntryCom, pHostPortListCom,
                                     au2EgressPorts1, &u2NumPortsCom);
        for (u2Port = 1; u2Port <= VLAN_MAX_PORTS; u2Port++)
        {

            VLAN_IS_MEMBER_PORT (pHostPortListCom, u2Port, u1Result)
                if (u1Result == VLAN_TRUE)
            {
                au4UpLinkPorts[u2NumPortsCount] = VLAN_GET_PHY_PORT (u2Port);
                ++u2NumPortsCount;
                --u2NumPortsCom;

            }
            if (u2NumPortsCom == 0)
            {
                break;
            }
        }

    }
    if (i4IsoCount == FALSE)
    {
        UtilPlstReleaseLocalPortList (pHostPortListCom);
        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                            (UINT1 *) pu2SecVlan);
        pu2SecVlan = NULL;
        return VLAN_SUCCESS;

    }

    for (u2PortIndex = 0; u2PortIndex < u2NumPorts; u2PortIndex++)
    {
        if (pVlanCurrEntryCom == NULL)
        {
            UtilPlstReleaseLocalPortList (pHostPortListCom);
            MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                (UINT1 *) pu2SecVlan);
            pu2SecVlan = NULL;
            return VLAN_FAILURE;
        }

        VLAN_IS_CURR_TRUNK_PORT (pVlanCurrEntryCom,
                                 au2EgressPorts1[u2PortIndex], u1Result);

        if (u1Result == VLAN_FALSE)
        {
            continue;
        }

        VlanConvertLPortArrayToPhyPorts (au2EgressPorts1, au4UpLinkPorts,
                                         u2NumPorts);
        u4TempPort = au4UpLinkPorts[u2PortIndex];
        PortIsolation.u4IngressPort = au4UpLinkPorts[u2PortIndex];
        if (VLAN_GET_PHY_PORT (au2EgressPorts[u2PortIndex]) == u4TempPort)
        {
            au4UpLinkPorts[u2PortIndex] = 0;
        }
        u2NumPortsCom = u2NumPortsCount;
        PortIsolation.pu4EgressPorts = au4UpLinkPorts;
        PortIsolation.u1Action = ISS_PI_ADD;
        PortIsolation.InVlanId = VlanId;
        PortIsolation.u2NumEgressPorts = u2NumPortsCount;

        VlanIssApiUpdtPortIsolationEntry (&PortIsolation);

        au4UpLinkPorts[u2PortIndex] = u4TempPort;
    }
    UtilPlstReleaseLocalPortList (pHostPortListCom);
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPrimaryPortTable                                 */
/*                                                                           */
/* Description        : This routine is called to test whether same port     */
/*                      exist in the vlan                                    */
/*                      if same port exist :- return failure                 */
/*                      if same port doesnt exist :- program it              */
/*                      isolation entry for egress port as promiscous and    */
/*                      community                                            */
/*                                                                           */
/* Input(s)           : u4Dot1qVlanIndex - secondary vlan ID                 */
/*                      i4SetValDot1qFutureStVlanPrimaryVid                  */
/*                                         - primary Vlan ID                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanPrimaryPortTable (UINT4 u4Dot1qVlanIndex,
                      INT4 i4SetValDot1qFutureStVlanPrimaryVid)
{

    tL2PvlanMappingInfo L2PvlanMappingInfoSecondary;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanCurrEntry     *pSecEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT1              *pInPorts = NULL;
    UINT1              *pInPortsReset = NULL;
    UINT2               u2Port = 0;
    BOOL1               bResult = VLAN_FALSE;
    UINT1               u1PortType = 0;
    UINT2              *pu2SecVlan = NULL;
    UINT2               pSecVlanId = 0;
    UINT1               u1Result = VLAN_FALSE;

    MEMSET (&L2PvlanMappingInfoSecondary, 0, sizeof (tL2PvlanMappingInfo));

    pInPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pInPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanPrimaryPortTable: Error in allocating memory "
                  "for pInPorts\r\n");
        return VLAN_FAILURE;
    }
    pInPortsReset = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pInPortsReset == NULL)
    {
        UtilPlstReleaseLocalPortList (pInPorts);
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanPrimaryPortTable: Error in allocating memory "
                  "for pInPortsReset\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pInPortsReset, 0, sizeof (tLocalPortList));
    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        UtilPlstReleaseLocalPortList (pInPorts);
        UtilPlstReleaseLocalPortList (pInPortsReset);
        return VLAN_FAILURE;
    }

    MEMSET (pu2SecVlan, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qVlanIndex);
    if (pCurrEntry != NULL)
    {
        /* check is to stop ports configuring again */
        L2PvlanMappingInfoSecondary.u4ContextId = VLAN_CURR_CONTEXT_ID ();
        L2PvlanMappingInfoSecondary.InVlanId =
            ((tVlanId) i4SetValDot1qFutureStVlanPrimaryVid);
        L2PvlanMappingInfoSecondary.pMappedVlans = pu2SecVlan;
        L2PvlanMappingInfoSecondary.u1RequestType = L2IWF_MAPPED_VLANS;

        VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfoSecondary);
        while (L2PvlanMappingInfoSecondary.u2NumMappedVlans > 0)
        {
            pSecVlanId =
                L2PvlanMappingInfoSecondary.
                pMappedVlans[L2PvlanMappingInfoSecondary.u2NumMappedVlans - 1];
            pSecEntry = VlanGetVlanEntry ((tVlanId) pSecVlanId);
            if (pSecEntry == NULL)
            {
                break;
            }
            MEMSET (pInPorts, 0, sizeof (tLocalPortList));

            MEMCPY (pInPorts, pSecEntry->EgressPorts, sizeof (tLocalPortList));
            for (u2Port = 1; u2Port <= VLAN_MAX_PORTS; u2Port++)
            {
                OSIX_BITLIST_IS_BIT_SET (pInPorts, u2Port,
                                         CONTEXT_PORT_LIST_SIZE, bResult);

                if (bResult == VLAN_TRUE)
                {
                    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                    if (pPortEntry == NULL)
                    {
                        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.
                                            VlanPvlanPoolId,
                                            (UINT1 *) pu2SecVlan);
                        pu2SecVlan = NULL;

                        UtilPlstReleaseLocalPortList (pInPorts);
                        UtilPlstReleaseLocalPortList (pInPortsReset);
                        return VLAN_FAILURE;
                    }
                    u1PortType = pPortEntry->u1PortType;
                    if ((u1PortType == VLAN_TRUNK_PORT)
                        || (u1PortType == VLAN_PROMISCOUS_PORT))
                    {
                        OSIX_BITLIST_SET_BIT (pInPortsReset, u2Port,
                                              sizeof (tLocalPortList));
                        VLAN_RESET_PORT_LIST (pInPorts, pInPortsReset);
                    }

                }
            }

            VLAN_ARE_PORTS_EXCLUSIVE (pInPorts, pCurrEntry->EgressPorts,
                                      u1Result) if (u1Result == VLAN_FALSE)
            {
                CLI_SET_ERR (CLI_VLAN_PORT_OVERLAP_ERR);
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;

                UtilPlstReleaseLocalPortList (pInPorts);
                UtilPlstReleaseLocalPortList (pInPortsReset);

                return VLAN_FAILURE;

            }
            --L2PvlanMappingInfoSecondary.u2NumMappedVlans;
        }
    }
    UtilPlstReleaseLocalPortList (pInPorts);
    UtilPlstReleaseLocalPortList (pInPortsReset);
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanChkPortsInPrimVlansAreExclusive                  */
/*                                                                           */
/* Description        : This routine is called to test whether same port     */
/*                      exist in the vlan                                    */
/*                      if same port exist :- return failure                 */
/*                      if same port doesnt exist :- program the ports       */
/*                                                                           */
/* Input(s)           : u4Dot1qVlanIndex - secondary vlan ID                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanChkPortsInPrimVlansAreExclusive (UINT4 u4Dot1qVlanIndex,
                                     UINT1 u1CurType,
                                     tSNMP_OCTET_STRING_TYPE * pEgressPorts)
{

    tVlanCurrEntry     *pPrimaryEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4Type = 0;
    UINT4               u4FsDot1qVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    UINT2               u2Port = 0;
    UINT1              *pInPorts = NULL;
    UINT1              *pInPortsReset = NULL;
    UINT1               u1PortType = 0;
    UINT1               u1Result;
    BOOL1               bResult = OSIX_FALSE;

    while (nmhGetNextIndexDot1qVlanStaticTable
           (u4FsDot1qVlanIndex, &u4NextVlanIndex) == SNMP_SUCCESS)
    {

        nmhGetDot1qFutureStVlanType (u4NextVlanIndex, &i4Type);

        if ((u1CurType == i4Type) && (u4Dot1qVlanIndex != u4NextVlanIndex))
        {
            pInPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
            if (pInPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "VlanPrimaryPortTable: Error in allocating memory "
                          "for pInPorts\r\n");
                return VLAN_FAILURE;
            }
            pInPortsReset =
                UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
            if (pInPortsReset == NULL)
            {
                UtilPlstReleaseLocalPortList (pInPorts);
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "VlanPrimaryPortTable: Error in allocating memory "
                          "for pInPortsReset\r\n");
                return VLAN_FAILURE;
            }
            MEMSET (pInPortsReset, 0, sizeof (tLocalPortList));

            pPrimaryEntry = VlanGetVlanEntry ((tVlanId) u4NextVlanIndex);
            if (pPrimaryEntry != NULL)
            {
                MEMSET (pInPorts, 0, sizeof (tLocalPortList));
                MEMCPY (pInPorts, pPrimaryEntry->EgressPorts,
                        sizeof (tLocalPortList));
                for (u2Port = 1; u2Port <= VLAN_MAX_PORTS; u2Port++)
                {

                    OSIX_BITLIST_IS_BIT_SET (pInPorts, u2Port,
                                             CONTEXT_PORT_LIST_SIZE, bResult);

                    if (bResult == VLAN_TRUE)
                    {
                        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                        if (pPortEntry == NULL)
                        {
                            UtilPlstReleaseLocalPortList (pInPorts);
                            UtilPlstReleaseLocalPortList (pInPortsReset);
                            return VLAN_FAILURE;
                        }
                        u1PortType = pPortEntry->u1PortType;
                        if ((u1PortType == VLAN_TRUNK_PORT)
                            || (u1PortType == VLAN_PROMISCOUS_PORT))
                        {
                            OSIX_BITLIST_SET_BIT (pInPortsReset, u2Port,
                                                  sizeof (tLocalPortList));
                            VLAN_RESET_PORT_LIST (pInPorts, pInPortsReset);
                        }

                    }
                }
                VLAN_ARE_PORTS_EXCLUSIVE (pInPorts, pEgressPorts->pu1_OctetList,
                                          u1Result) if (u1Result == VLAN_FALSE)
                {
                    UtilPlstReleaseLocalPortList (pInPorts);
                    UtilPlstReleaseLocalPortList (pInPortsReset);

                    return VLAN_FAILURE;

                }

            }

            UtilPlstReleaseLocalPortList (pInPorts);
            UtilPlstReleaseLocalPortList (pInPortsReset);
        }
        u4FsDot1qVlanIndex = u4NextVlanIndex;
    }
    return VLAN_SUCCESS;

}
