/***********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanred.c,v 1.123 2016/06/20 10:00:08 siva Exp $
 *
 * Description     : This file contains VLAN redundancy related routines
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : vlanred.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/
#ifdef L2RED_WANTED
#include "vlaninc.h"

tVlanRedGlobalInfo  gVlanRedInfo;
tVlanRedTaskInfo    gVlanRedTaskInfo;

PRIVATE VOID VlanRedTrigHigherLayer PROTO ((UINT1));
static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);
/*****************************************************************************/
/* Function Name      : VlanRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers VLAN module with RM to send and receive    */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then VLAN_SUCCESS         */
/*                      Otherwise VLAN_FAILURE                               */
/*****************************************************************************/
INT4
VlanRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_VLANGARP_APP_ID;
    RmRegParams.pFnRcvPkt = VlanRedHandleUpdateEvents;

    if (VlanRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "VlanRegisterWithRM: Registration with RM FAILED\n");

        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleUpdateEvents                            */
/*                                                                           */
/* Description        : This function will be invoked by the RM module to    */
/*                      pass events like GO_ACTIVE, GO_STANDBY etc. to VLAN  */
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - If the message is enq'ed successfully */
/*                      VLAN_FAILURE otherwise                               */
/*****************************************************************************/
INT4
VlanRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to VLAN task. */
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "Message from RM ignored - Buffer missing !!! \n");

        return RM_FAILURE;
    }

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        /* VLAN module is not yet initiailised. Ignoring the 
         * message from RM */
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            VlanRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "Message from RM ignored - VLAN module not initialized !!! \n");

        return RM_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (pVlanQMsg == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                         "No memory for VLAN Q Message !!!!!! \n");
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            VlanRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_RM_MSG;

    pVlanQMsg->RmData.u4Events = (UINT4) u1Event;
    pVlanQMsg->RmData.pRmMsg = pData;
    pVlanQMsg->RmData.u2DataLen = u2DataLen;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "RM Message enqueue FAILED\n");
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            VlanRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, (UINT1 *) pVlanQMsg);

        return RM_FAILURE;
    }

    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "RM Event send FAILED\n");

        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedProcessUpdateMsg                              */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the RM module. This function invokes appropriate*/
/*                      functions to handle the update message.              */
/*                                                                           */
/* Input(s)           : pVlanQMsg - Pointer to the VLAN Queue message.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedProcessUpdateMsg (tVlanQMsg * pVlanQMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    INT4                i4RetVal = VLAN_SUCCESS;
    tVlanNodeStatus     VlanPrevNodeState = VLAN_NODE_IDLE;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pVlanQMsg->RmData.u4Events)
    {
        case GO_ACTIVE:
            VlanPrevNodeState = VLAN_NODE_STATUS ();

            if ((VlanPrevNodeState == VLAN_NODE_ACTIVE) ||
                (VlanPrevNodeState == VLAN_NODE_SWITCHOVER_IN_PROGRESS))
            {
                VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                                 CONTROL_PLANE_TRC,
                                 "Node already ACTIVE or becoming ACTIVE \n");
                return;
            }
            else if (VlanPrevNodeState == VLAN_NODE_IDLE)
            {
                i4RetVal = VlanRedMakeNodeActiveFromIdle ();
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else if (VlanPrevNodeState == VLAN_NODE_STANDBY)
            {
                i4RetVal = VlanRedMakeNodeActiveFromStandby ();
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            if (VLAN_RED_BULK_REQ_RECD () == VLAN_TRUE)
            {
                VLAN_RED_BULK_REQ_RECD () = VLAN_FALSE;
                gVlanRedInfo.BulkUpdNextVlanId = 0;
                gVlanRedInfo.BulkUpdNextPort = 0;
                gVlanRedInfo.u4BulkUpdNextContext = 0;
#ifdef EVB_WANTED
                gu4EvbBulkUpdPrevPort = 0;
                gu4EvbBulkUpdPrevContxt = 0;
#endif 
                VlanRedHandleStandbyUpEvent ();
            }

            VlanRmHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:

            if (VLAN_NODE_STATUS () == VLAN_NODE_STANDBY)
            {
                VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                                 CONTROL_PLANE_TRC, "Node already STANDBY \n");
                return;
            }
            VLAN_RED_BULK_REQ_RECD () = VLAN_FALSE;

            if (VLAN_NODE_STATUS () == VLAN_NODE_IDLE)
            {
                VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                                 CONTROL_PLANE_TRC,
                                 "Ignoring this event, node will become standby "
                                 "once the static configurations are restored \n");

            }
            else if ((VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE) ||
                     (VLAN_NODE_STATUS () == VLAN_NODE_SWITCHOVER_IN_PROGRESS))
            {
                i4RetVal = VlanRedMakeNodeStandbyFromActive ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                VlanRmHandleProtocolEvent (&ProtoEvt);
            }

            break;

        case RM_INIT_HW_AUDIT:
            if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
            {
                /* Called this fn for Audit. 
                 * Since Relearn timer is not started, Audit is called
                 * directly without timer expiry */
                VlanRedHandleRelearnTmrExpiry ();
            }
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pVlanQMsg->RmData.pRmMsg;
            VLAN_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            VlanRmReleaseMemoryForMsg ((UINT1 *) pData);
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC,
                             "STANDBY UP Event received. Num Peers = %d \n",
                             VLAN_RED_NUM_STANDBY_NODES ());

            if (VLAN_RED_BULK_REQ_RECD () == VLAN_TRUE)
            {
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED) && !defined (RSTP_WANTED)
                VLAN_RED_BULK_REQ_RECD () = VLAN_FALSE;
                /* Bulk request msg is received before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                gVlanRedInfo.BulkUpdNextVlanId = 0;
                gVlanRedInfo.BulkUpdNextPort = 0;
                gVlanRedInfo.u4BulkUpdNextContext = 0;
#ifdef EVB_WANTED
                gu4EvbBulkUpdPrevPort = 0;
                gu4EvbBulkUpdPrevContxt = 0;
#endif 
                VlanRedHandleStandbyUpEvent ();
#endif
            }
            break;

        case RM_STANDBY_DOWN:
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC,
                             "STANDBY DOWN Event received \n");
            pData = (tRmNodeInfo *) pVlanQMsg->RmData.pRmMsg;
            VLAN_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            VlanRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pVlanQMsg->RmData.pRmMsg, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pVlanQMsg->RmData.pRmMsg,
                                 pVlanQMsg->RmData.u2DataLen);

            ProtoAck.u4AppId = RM_VLANGARP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            VlanRedHandleUpdates (pVlanQMsg->RmData.pRmMsg,
                                  pVlanQMsg->RmData.u2DataLen);

            RM_FREE (pVlanQMsg->RmData.pRmMsg);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            VlanRedHandleDynSyncAudit ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:

            if (VLAN_NODE_STATUS () == VLAN_NODE_IDLE)
            {
                if (VlanRmGetNodeState () == RM_STANDBY)
                {
                    i4RetVal = VlanRedMakeNodeStandbyFromIdle ();
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                VlanRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case RM_COMPLETE_SYNC_UP:
#if defined (CFA_WANTED) || defined (EOAM_WANTED) || defined (PNAC_WANTED) || defined (LA_WANTED) || defined (RSTP_WANTED)
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC,
                             "Ignoring RM_COMPLETE_SYNC_UP event. Waiting for the"
                             "L2_COMPLETE_SYNC_UP trigger from the lower layer\n");
#else
            /* TODO: VLAN periodic sync up has to be completed before
             * force switchover from Active --> Standby
             */
            VlanRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
#endif
            break;

        case L2_COMPLETE_SYNC_UP:
            VlanRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        case L2_INITIATE_BULK_UPDATES:
            VLAN_GBL_TRC ("[STANDBY]: Received L2Initiate Bulk Update Message \r\n");
            VlanRedSendBulkReqMsg ();
            break;

        default:
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC, "Received unknown event\n");

            break;
    }
    UNUSED_PARAM (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : VlanRedMakeNodeActiveFromIdle                        */
/*                                                                           */
/* Description        : This function makes the node active from the idle    */
/*                      state. This function is called when the node becomes */
/*                      active and there are no other nodes.                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
VlanRedMakeNodeActiveFromIdle (VOID)
{
    INT4                i4RetVal;
    UINT4               u4ContextId, u4AgeOutInt;
    UINT1               u1ProtocolId;
    tMacAddr            MacAddr;

    VLAN_NODE_STATUS () = VLAN_NODE_ACTIVE;

    VLAN_RED_NUM_STANDBY_NODES () = VlanRmGetStandbyNodeCount ();

    VLAN_RED_AUDIT_FLAG () = VLAN_RED_AUDIT_STOP;    /* No need for audit */

    /* Lock is taken alreday before GO_ACTIVE msg is received */
    for (u4ContextId = 0; u4ContextId < VLAN_MAX_CONTEXT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
        {
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
        {
            VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                      "VLAN Node moving from IDLE to ACTIVE State \n");
            /* Starting VLAN ageing timer */
            u4AgeOutInt = VlanL2IwfGetAgeoutInt ();

            if (IssuGetMaintModeOperation() == OSIX_FALSE)
            {
                VlanL2IwfStartHwAgeTimer (VLAN_CURR_CONTEXT_ID (), u4AgeOutInt);
            }

            u4AgeOutInt = VLAN_SPLIT_AGEOUT_TIME (u4AgeOutInt);

            VLAN_START_TIMER (VLAN_AGEOUT_TIMER, u4AgeOutInt);

            /* When the system comes up, VlanInit will be called for
             * default context.
             * At this time the Node status may not be in Active State.
             * So hw programming for the bridge mode might not have done.
             * Hence the node moves from IDLE to Active program the
             * bridge mode for default context. */
            i4RetVal = VlanHwSetBrgMode (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_BRIDGE_MODE ());

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          VLAN_NAME, "Configuration of Bridge Mode failed \n");

            }

            /* Also Set the default VlanId in NP and in hw. */
            VlanHwSetDefaultVlanId (VLAN_CURR_CONTEXT_ID (), VLAN_DEF_VLAN_ID);

            for (u1ProtocolId = 1; u1ProtocolId < VLAN_MAX_PROT_ID;
                 u1ProtocolId++)
            {
                VLAN_MEMSET (MacAddr, 0, sizeof (tMacAddr));

                VlanTunnelProtocolMac (u1ProtocolId, MacAddr);
                /* To program the Non-NULL MAC address to the NPAPI */
                if(0 != MEMCMP (MacAddr, gNullMacAddress, sizeof (tMacAddr)))
                {
                    i4RetVal = VlanHwSetTunnelMacAddress
                        (VLAN_CURR_CONTEXT_ID (), MacAddr, u1ProtocolId);
                    if (i4RetVal == VLAN_FAILURE)
                    {
                        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC | 
                            CONTROL_PLANE_TRC,
                            VLAN_NAME, "Setting Tunnel mode failed \n");
                    }

                }
                else
                {
                    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                          "MAC value is NULL for protocol id:%d \n",
                          u1ProtocolId);
                }
            }

            if (VLAN_MODULE_ADMIN_STATUS () == VLAN_ENABLED)
            {
                i4RetVal = VlanEnableVlan ();
                if (i4RetVal != VLAN_SUCCESS)
                {
                    VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "VLAN Node to ACTIVE state failed when enabling "
                              "VLAN module.\n");
                }

            }
        }
        VlanReleaseContext ();
    }
#ifdef GARP_WANTED
    /* Send GO_ACTIVE trigger to GARP */
    VlanGarpRedSendMsg (GO_ACTIVE);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedMakeNodeStandbyFromIdle                       */
/*                                                                           */
/* Description        : This function makes the node standby from the idle   */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
VlanRedMakeNodeStandbyFromIdle (VOID)
{
    INT4                i4RetVal;
    UINT4               u4ContextId;

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC,
                     "VLAN Node moving from IDLE to STANDBY State \n");

    VLAN_RED_AUDIT_FLAG () = VLAN_RED_AUDIT_STOP;

    VLAN_NODE_STATUS () = VLAN_NODE_STANDBY;

    VLAN_RED_NUM_STANDBY_NODES () = 0;

    for (u4ContextId = 0; u4ContextId < VLAN_MAX_CONTEXT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
        {
            if (VLAN_MODULE_ADMIN_STATUS () == VLAN_ENABLED)
            {
                i4RetVal = VlanEnableVlan ();
                if (i4RetVal != VLAN_SUCCESS)
                {
                    VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "VLAN Node to STANDBY state failed when enabling "
                              "VLAN module.\n");
                    VlanReleaseContext ();
                    continue;
                }
            }
        }
        VlanReleaseContext ();
    }
#ifdef GARP_WANTED
    /* Send GO_STANDBY trigger to GARP */
    VlanGarpRedSendMsg (GO_STANDBY);
#endif /* GARP_WANTED */

#ifdef MBSM_WANTED
#ifdef SW_LEARNING
    VlanUpdateVlanMacTable ();
#endif /* SW_LEARNING */
#endif /* MBSM_WANTED */

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedMakeNodeActiveFromStandby                     */
/*                                                                           */
/* Description        : This function makes the node active frrom the        */
/*                      standby state.                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
VlanRedMakeNodeActiveFromStandby (VOID)
{
    UINT4               u4AgeOutInt, u4ContextId;

    VLAN_NODE_STATUS () = VLAN_NODE_ACTIVE;

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC,
                     "VLAN Node moving from STANDBY to ACTIVE State \n");

    /* 
     * Set node status to Switchover in progress so that the NPAPI
     * will not be invoked when VLAN, GVRP and GMRP are enabled.
     */
    VLAN_RED_NUM_STANDBY_NODES () = VlanRmGetStandbyNodeCount ();

    for (u4ContextId = 0; u4ContextId < VLAN_MAX_CONTEXT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
        {
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
        {

            /* Starting VLAN ageing timer */
            u4AgeOutInt = VlanL2IwfGetAgeoutInt ();

            if (IssuGetMaintModeOperation() == OSIX_FALSE)
            {
                VlanL2IwfStartHwAgeTimer (VLAN_CURR_CONTEXT_ID (), u4AgeOutInt);
            }

            u4AgeOutInt = VLAN_SPLIT_AGEOUT_TIME (u4AgeOutInt);

            VLAN_START_TIMER (VLAN_AGEOUT_TIMER, u4AgeOutInt);
#ifdef NPAPI_WANTED
            VlanRedUpdateNpapiDefaultVlanId (VLAN_CURR_CONTEXT_ID ());
#endif
        }
        VlanReleaseContext ();
    }

    /* Node status will be made active once the dynamic data applied message
     * is received from GARP */

#ifdef GARP_WANTED
    /* Send GO_ACTIVE trigger to GARP */
    VlanGarpRedSendMsg (GO_ACTIVE);
#else
    /* GARP is not there. Treat as if we received the data applied msg
     * from GARP */
#endif /* GARP_WANTED */

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedProcessDataAppliedMsg                         */
/*                                                                           */
/* Description        : This function will be invoked when dynamic data      */
/*                      applied message is received from GARP. This function */
/*                      will start relearn timer and will delete the         */
/*                      GVRP and GMRP learnt ports table.                    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanRedProcessDataAppliedMsg (VOID)
{
    INT4                i4RetVal;

    if (VLAN_NODE_STATUS () != VLAN_NODE_SWITCHOVER_IN_PROGRESS)
    {
        /* node status is not switchover in progress. Can happen in
         * the following scenario:
         * STANDBY becomes ACTIVE and hence VLAN posted GO_ACTIVE trigger
         * to GARP which is in the process of applying the dynamic data.
         * During this period if VLAN gets GO_STANDBY trigger it will
         * deallocate the GVRP and GMRP learnt ports table and move to
         * STANDBY state. So just ignore the data applied msg.
         */
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VLAN Node is not in switchover in progress state."
                  "Ignoring the data applied message from GARP. \n");
        return VLAN_SUCCESS;
    }

    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Received Data applied message from GARP. "
              "Moving Node status to ACTIVE. \n");
    /* 
     * Make node status as ACTIVE so that NPAPIs will get invoked.
     */
    VLAN_NODE_STATUS () = VLAN_NODE_ACTIVE;

    /* Delete all entries in learnt port tables */
    VlanRedDelAllStandbyInfo ();

    VlanRedDeInitStandbyInfo ();

    /* Start Relearn timer */
    i4RetVal = VlanStartTimer (VLAN_RED_RELEARN_TIMER,
                               VLAN_RED_GARP_RELEARN_INT);

    if (i4RetVal != VLAN_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Starting relearning timer failed. Node status "
                  "moved to ACTIVE. \n");
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleRelearnTmrExpiry                        */
/*                                                                           */
/* Description        : This function will be invoked on the expiry of the   */
/*                      relearn timer. This function will invoke the audit   */
/*                      start function to start the audit process.           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedHandleRelearnTmrExpiry (VOID)
{
    INT4                i4RetVal;
    UINT4               u4Context;
    UINT1               u1IsVlanEnable = VLAN_FALSE;

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC,
                     "Relearning Timer expired. Sending event to AUDIT "
                     "task to start the audit. \n");

    gVlanRedInfo.RelearnTimer.u1IsTmrActive = VLAN_FALSE;

    /*
     * Check in all context, if vlan is shutdown remove configuration
     * from Hw. If any context has vlan up Spawn VlanAudit task
     * to do Audit in Hw.
     * */
    for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
    {
        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            continue;
        }
        if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4Context) == VLAN_DISABLED)
        {
            /* 
             * VLAN Is disabled. Hence GVRP and GMRP should have been disabled.
             * No information in software to compare with hardware. Hence DeINIT
             * hardware line cards completely.
             */
            VlanHwDeInit (u4Context);

        }
        else
        {
            u1IsVlanEnable = VLAN_TRUE;
        }

        VlanReleaseContext ();
        continue;
    }

    if (u1IsVlanEnable == VLAN_TRUE)
    {
        /* VLAN Is enabled. Start performing the audit */
        VLAN_RED_AUDIT_FLAG () = VLAN_RED_AUDIT_START;    /* audit started here */

        if (VLAN_AUDIT_TASK_ID == 0)
        {
            /* Doing audit for the first time */
            i4RetVal = VLAN_SPAWN_TASK (VLAN_AUDIT_TASK,
                                        VLAN_AUDIT_TASK_PRIORITY,
                                        OSIX_DEFAULT_STACK_SIZE,
                                        (OsixTskEntry) VlanRedAuditMain,
                                        0, &VLAN_AUDIT_TASK_ID);
        }

        if (VLAN_AUDIT_TASK_ID != 0)
        {
            VLAN_SEND_EVENT (VLAN_AUDIT_TASK_ID, VLAN_RED_AUDIT_START_EVENT);
        }
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC,
                     "Current number of standby node count - %d \n",
                     VLAN_RED_NUM_STANDBY_NODES ());

    if (VLAN_RED_NUM_STANDBY_NODES () != 0)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "Starting Periodic Update timer \n");
    }
    UNUSED_PARAM (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : VlanRedUpdateNpapiDefaultVlanId                      */
/*                                                                           */
/* Description        : This function will be invoked to update the NP global*/
/*                      variable with the default vlan id when the node goes */
/*                      active from standby                                  */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanRedUpdateNpapiDefaultVlanId (UINT4 u4Context)
{
    if (VlanRedHwUpdateDBForDefaultVlanId (u4Context,
                                           VLAN_DEF_VLAN_ID) == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC,
                         "Default Vlan Id configuration failed \n");

        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedMakeNodeStandbyFromActive                     */
/*                                                                           */
/* Description        : This function is called when the node becomes a      */
/*                      Standby node either from ACTIVE or from              */
/*                      SWITCH_OVER_IN_PROGRESS state.                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
VlanRedMakeNodeStandbyFromActive (VOID)
{
    UINT4               u4ContextId;
    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC,
                     "VLAN Node moving from ACTIVE to STANDBY State \n");
    /* Set NUM Peers to zero */
    VLAN_RED_NUM_STANDBY_NODES () = 0;

    for (u4ContextId = 0; u4ContextId < VLAN_MAX_CONTEXT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
        {
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
        {
            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        }
        VlanReleaseContext ();
    }
    /*
     * Set node status to STANDBY here so that NPAPI will not
     * get triggered during module shutdown.
     */
    VLAN_NODE_STATUS () = VLAN_NODE_STANDBY;

    if (VLAN_AUDIT_TASK_ID != 0)
    {
        VLAN_RED_AUDIT_FLAG () = VLAN_RED_AUDIT_STOP;

        /* Send delete task event to Audit task */
        VLAN_SEND_EVENT (VLAN_AUDIT_TASK_ID, VLAN_RED_AUDIT_TASK_DEL_EVENT);
    }

#ifdef KERNEL_WANTED
    VlanRemoveFdbEntryFromKernel ();
#endif
#ifdef GARP_WANTED
    /* Send GO_STANDBY trigger to GARP */
    VlanGarpRedSendMsg (GO_STANDBY);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleStandbyUpEvent                          */
/*                                                                           */
/* Description        : This function handles the standby UP event from the  */
/*                      RM module. This function sends the bulk updates      */
/*                      containing the list of VLAN and Group learnt ports.  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedHandleStandbyUpEvent (VOID)
{
    if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
    {
        VLAN_RED_BULK_REQ_RECD () = VLAN_TRUE;
        return;
    }

    /* TODO
     * Relearning timer running. Standby UPs will be processed
     * only after relearn timer expiry
     * This part of code is commented explicitly for testing IGS without GARP.
     * When Hot standby is implemented for GARP, we should take care of it.
     */
    VlanRedSendBulkUpdates ();
}

/*****************************************************************************/
/* Function Name      : VlanRedSendBulkUpdates                               */
/*                                                                           */
/* Description        : This function sends bulk updates to the standby node.*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedSendBulkUpdates (VOID)
{
    tRmMsg             *pMsg = NULL;
    tVlanPortEntry     *pPortEntry;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2PortUpdLen;
    UINT2               u2BufSize;
    UINT2               u2OffSet = 0;
    UINT4               u4BulkUpdPortCnt;
    UINT4               u4PortNo;
    UINT4               u4ContextId;
    UINT4               u4BulkUnitSize = 0;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        /* VLANGARP completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        VlanRmSetBulkUpdatesStatus (RM_VLANGARP_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        VlanRedSendBulkUpdTailMsg ();

        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Vlan Module not started ! \n");

        return;
    }
    u4BulkUpdPortCnt = VLAN_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u4ContextId = gVlanRedInfo.u4BulkUpdNextContext;
         u4ContextId < VLAN_MAX_CONTEXT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            gVlanRedInfo.u4BulkUpdNextContext++;
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            /* Vlan module is shutdown */
            VlanReleaseContext ();
            gVlanRedInfo.u4BulkUpdNextContext++;
            continue;
        }

        if (VLAN_MODULE_ADMIN_STATUS () != VLAN_ENABLED)
        {
            VlanReleaseContext ();
            gVlanRedInfo.u4BulkUpdNextContext++;
            continue;
        }

        if (gVlanRedInfo.BulkUpdNextPort == 0)
        {
            gVlanRedInfo.BulkUpdNextPort =
                gpVlanContextInfo->u2VlanStartPortInd;
        }

        u2PortUpdLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
            VLAN_RED_CONTEXT_FIELD_SIZE + VLAN_RED_PORT_NO_LEN +
            VLAN_RED_PORT_OPER_STATUS_SIZE;
        u2BufSize = VLAN_RED_MAX_MSG_SIZE;

        if (pMsg == NULL)
        {
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                          VLAN_NAME,
                          "RM alloc failed. Bulk updates not sent\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                VlanRmHandleProtocolEvent (&ProtoEvt);
                VlanReleaseContext ();
                return;
            }
        }

        /*
         *    PORT Update message
         *    
         *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 1 bytes ->|
         *    -----------------------------------------------------
         *    | Msg. Type |   Length    | PortId     | Oper Status |
         *    |     (1)   |1 + 2 + 4 + 1|            |             |
         *    |----------------------------------------------------
         *
         */

        for (u4PortNo = gVlanRedInfo.BulkUpdNextPort;
             (VLAN_IS_PORT_VALID (u4PortNo) == VLAN_TRUE) &&
             (u4BulkUpdPortCnt > 0);
             u4PortNo = VLAN_GET_NEXT_PORT_INDEX (u4PortNo), u4BulkUpdPortCnt--)
        {
            gVlanRedInfo.BulkUpdNextPort = VLAN_GET_NEXT_PORT_INDEX (u4PortNo);

            pPortEntry = VLAN_GET_PORT_ENTRY (u4PortNo);

            if ((pPortEntry == NULL)
                || (pPortEntry->u1OperStatus != VLAN_OPER_UP))
            {
                continue;
            }

            if ((u2BufSize - u2OffSet) < u2PortUpdLen)
            {
                /* no room for the current message */
                VlanRedSendMsgToRm (pMsg, u2OffSet);

                pMsg = RM_ALLOC_TX_BUF (u2BufSize);

                if (pMsg == NULL)
                {
                    VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                              VLAN_NAME,
                              "RM alloc failed. Bulk updates not sent\n");
                    VlanReleaseContext ();
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    VlanRmHandleProtocolEvent (&ProtoEvt);
                    return;
                }

                u2OffSet = 0;
            }

            VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_PORT_OPER_STATUS_MESSAGE);
            VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2PortUpdLen);
            VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, VLAN_CURR_CONTEXT_ID ());
            VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4PortNo);
            VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pPortEntry->u1OperStatus);

        }
        VlanReleaseContext ();

        if (u4BulkUpdPortCnt == 0)
        {
            break;
        }
        else if (!(VLAN_IS_PORT_VALID (u4PortNo)))
        {
            gVlanRedInfo.BulkUpdNextPort = 0;
            gVlanRedInfo.u4BulkUpdNextContext++;
        }
    }
    if ((u1MemEstFlag == 1) && (GARP_HR_STATUS () != GARP_HR_STATUS_DISABLE))
    {
        /* This is to find the memory required for syncing BulkUpdates */
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */

        IssSzUpdateSizingInfoForHR ((CHR1 *) "VLAN",
                                    (CHR1 *) "tVlanContextInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT1);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "VLAN", (CHR1 *) "tVlanPortEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done , so changing the flag to next bulk message  */
        u1MemEstFlag = VLAN_PORT_OPER_STATUS_MESSAGE;
    }

    if (u2OffSet != 0)
    {
        /* Send the buffer out */
        if (VlanRedSendMsgToRm (pMsg, u2OffSet) == VLAN_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            VlanRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    else if (pMsg)
    {
        /* Empty buffer created without any update messages filled */
        RM_FREE (pMsg);
    }

    /*Send the EVB Bulk updates to standby*/
    VlanEvbRedSendCdcpTlvBulkSyncup();
    VlanEvbRedSendRemoteRoleBulkSyncup();
    VlanEvbRedSendSbpBulkSyncup();

    if (gVlanRedInfo.u4BulkUpdNextContext < VLAN_MAX_CONTEXT)
    {
        /* Send an event to start the next sub bulk update. */
        if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_RED_BULK_UPD_EVENT)
            == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            VlanRmHandleProtocolEvent (&ProtoEvt);
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             " Send Event failed for Vlan redundancy"
                             "Bulk updates");
        }
        return;
    }
    else
    {
        /* VLANGARP completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        VlanRmSetBulkUpdatesStatus (RM_VLANGARP_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        VlanRedSendBulkUpdTailMsg ();
    }

}

/*****************************************************************************/
/* Function Name      : VlanRedHandlePeriodicTmrExpiry                       */
/*                                                                           */
/* Description        : This function handles the periodic timer expiry      */
/*                      event. This function invokes a function to send      */
/*                      periodic updates and restarts the periodic update    */
/*                      timer.                                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedHandlePeriodicTmrExpiry (VOID)
{
    INT4                i4RetVal;

    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Periodic Update Timer expired \n");

    /* Start periodic update timer */
    i4RetVal = VlanStartTimer (VLAN_RED_PERIODIC_TIMER,
                               VLAN_RED_PERIODIC_UPD_INT);

    if (i4RetVal != VLAN_SUCCESS)
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "Starting periodic update timer failed \n");
    }

    if ((VLAN_GVRP_STATUS () == VLAN_DISABLED) &&
        (VLAN_GMRP_STATUS () == VLAN_DISABLED))
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "GVRP and GMRP are disabled. Nothing to sync UP\n");

        return;
    }

    if ((VLAN_RED_VLANS_CHANGED () == 0) &&
        (VLAN_RED_GRPS_CHANGED () == 0) &&
        (VLAN_RED_VLAN_DELETES () == 0) && (VLAN_RED_GRP_DELETES () == 0))
    {
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "No updates to be sent. \n");

        return;
    }

    VlanRedSendPeriodicUpdates ();
}

/*****************************************************************************/
/* Function Name      : VlanRedSendPeriodicUpdates                           */
/*                                                                           */
/* Description        : This function sends periodic update messages         */
/*                      containing the list of changed VLAN and Group        */
/*                      learnt ports and sends it to the standby node.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedSendPeriodicUpdates (VOID)
{
    UINT1              *pu1LocalPortList = NULL;
    tRmMsg             *pMsg;
    tVlanCurrEntry     *pVlanEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanRedGvrpDeletedEntry *pGvrpDelEntry;
    tVlanRedGmrpDeletedEntry *pGmrpDelEntry;
    UINT4               u4HashIndex;
    UINT2               u2VlanUpdLen;
    UINT2               u2GrpUpdLen;
    UINT2               u2VlanDelUpdLen;
    UINT2               u2GrpDelUpdLen;
    UINT2               u2BufSize;
    UINT2               u2OffSet;
    tVlanId             VlanId;

    u2VlanUpdLen = (UINT2) (VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
                            VLAN_RED_VLAN_ID_LEN + sizeof (tLocalPortList));

    u2GrpUpdLen = (UINT2) (VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
                           VLAN_RED_VLAN_ID_LEN + ETHERNET_ADDR_SIZE +
                           sizeof (tLocalPortList));

    u2BufSize = VLAN_RED_MAX_MSG_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME,
                  "RM alloc failed. Periodic updates not sent.\n");
        return;
    }

    u2OffSet = 0;

    if (VLAN_RED_VLAN_DELETES () != 0)
    {
        /* Send VLAN delete update messages */
        u2VlanDelUpdLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
            VLAN_RED_VLAN_ID_LEN;

        /* Deleting entries from VLAN deleted table */
        VLAN_RED_LOCK ();

        VLAN_HASH_SCAN_TABLE (gVlanRedInfo.pVlanDeletedTable, u4HashIndex)
        {
            pGvrpDelEntry = (tVlanRedGvrpDeletedEntry *)
                VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pVlanDeletedTable,
                                          u4HashIndex);
            while (pGvrpDelEntry != NULL)
            {
                if ((u2BufSize - u2OffSet) < u2VlanDelUpdLen)
                {
                    VlanRedSendMsgToRm (pMsg, u2OffSet);

                    pMsg = RM_ALLOC_TX_BUF (u2BufSize);

                    if (pMsg == NULL)
                    {
                        VLAN_TRC (VLAN_RED_TRC,
                                  (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                  VLAN_NAME,
                                  "RM alloc failed. Periodic updates not sent.\n");

                        VLAN_RED_UNLOCK ();

                        return;
                    }

                    u2OffSet = 0;
                }

                VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_DEL_UPDATE_MESSAGE);
                VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2VlanDelUpdLen);
                VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pGvrpDelEntry->VlanId);

                VLAN_HASH_DELETE_NODE (gVlanRedInfo.pVlanDeletedTable,
                                       (tTMO_HASH_NODE *) & (pGvrpDelEntry->
                                                             NextDeletedEntry),
                                       (UINT4) u4HashIndex);

                VLAN_RELEASE_BUF (VLAN_RED_GVRP_DEL_BUFF,
                                  (UINT1 *) pGvrpDelEntry);

                pGvrpDelEntry = (tVlanRedGvrpDeletedEntry *)
                    VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pVlanDeletedTable,
                                              u4HashIndex);

                VLAN_RED_DECR_VLAN_DELETES ();
            }                    /* while (pGvrpDelEntry != NULL) */

            if (VLAN_RED_VLAN_DELETES () == 0)
            {
                /* No more updates to be sent */
                break;
            }
        }                        /* VLAN_HASH_SCAN_TABLE - pVlanDeletedTable */

        VLAN_RED_UNLOCK ();

    }                            /* if (VLAN_RED_VLAN_DELETES () != 0) */

    if (VLAN_RED_GRP_DELETES () != 0)
    {
        /* Send GROUP delete update messages */
        u2GrpDelUpdLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
            VLAN_RED_VLAN_ID_LEN + ETHERNET_ADDR_SIZE;

        VLAN_RED_LOCK ();

        VLAN_HASH_SCAN_TABLE (gVlanRedInfo.pGrpDeletedTable, u4HashIndex)
        {
            pGmrpDelEntry = (tVlanRedGmrpDeletedEntry *)
                VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pGrpDeletedTable,
                                          u4HashIndex);
            while (pGmrpDelEntry != NULL)
            {
                if ((u2BufSize - u2OffSet) < u2GrpDelUpdLen)
                {
                    VlanRedSendMsgToRm (pMsg, u2OffSet);

                    pMsg = RM_ALLOC_TX_BUF (u2BufSize);

                    if (pMsg == NULL)
                    {
                        VLAN_TRC (VLAN_RED_TRC,
                                  (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                  VLAN_NAME,
                                  "RM alloc failed. Periodic updates not sent.\n");

                        VLAN_RED_UNLOCK ();

                        return;
                    }

                    u2OffSet = 0;
                }

                VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                                    VLAN_GRP_DEL_UPDATE_MESSAGE);
                VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2GrpDelUpdLen);
                VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pGmrpDelEntry->VlanId);
                VLAN_RM_PUT_N_BYTE (pMsg, pGmrpDelEntry->McastAddr,
                                    &u2OffSet, ETHERNET_ADDR_SIZE);

                VLAN_HASH_DELETE_NODE (gVlanRedInfo.pGrpDeletedTable,
                                       (tTMO_HASH_NODE *) & (pGmrpDelEntry->
                                                             NextDeletedEntry),
                                       (UINT4) u4HashIndex);

                VLAN_RELEASE_BUF (VLAN_RED_GMRP_DEL_BUFF,
                                  (UINT1 *) pGmrpDelEntry);

                pGmrpDelEntry = (tVlanRedGmrpDeletedEntry *)
                    VLAN_HASH_GET_FIRST_NODE (gVlanRedInfo.pGrpDeletedTable,
                                              u4HashIndex);

                VLAN_RED_DECR_GRP_DELETES ();
            }                    /* while (pGmrpDelEntry != NULL) */

            if (VLAN_RED_GRP_DELETES () == 0)
            {
                /* No more updates to be sent */
                break;
            }
        }                        /* VLAN_HASH_SCAN_TABLE -> pGrpDeletedTable */

        VLAN_RED_UNLOCK ();

    }                            /* if (VLAN_RED_GRP_DELETES != 0) */

    /* Scan VLAN and Group deleted table */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry == NULL)
        {
            continue;
        }

        if (VLAN_GVRP_STATUS () == VLAN_ENABLED)
        {
            if (pVlanEntry->u1IsLearntInfoChanged == VLAN_TRUE)
            {
                /* Some member ports present in the learnt port list */
                if ((u2BufSize - u2OffSet) < u2VlanUpdLen)
                {
                    /* no room for the current message */
                    VlanRedSendMsgToRm (pMsg, u2OffSet);

                    pMsg = RM_ALLOC_TX_BUF (u2BufSize);

                    if (pMsg == NULL)
                    {
                        VLAN_TRC (VLAN_RED_TRC,
                                  (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                  VLAN_NAME,
                                  "RM alloc failed. Periodic updates not sent.\n");
                        return;
                    }

                    u2OffSet = 0;
                }

                VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_UPDATE_MESSAGE);
                VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2VlanUpdLen);
                VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pVlanEntry->VlanId);
                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    RM_FREE (pMsg);
                    return;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_CURR_LEARNT_PORTS (pVlanEntry, pu1LocalPortList);

                VLAN_RM_PUT_N_BYTE (pMsg, pu1LocalPortList,
                                    &u2OffSet, sizeof (tLocalPortList));
                UtilPlstReleaseLocalPortList (pu1LocalPortList);

                pVlanEntry->u1IsLearntInfoChanged = VLAN_FALSE;

                VLAN_RED_DECR_VLANS_CHANGED ();
            }                    /* pVlanEntry->u1IsLearntInfoChanged == VLAN_TRUE */
        }                        /* VLAN_GVRP_STATUS () != VLAN_DISABLED */

        if ((VLAN_GMRP_STATUS () == VLAN_ENABLED) &&
            (pVlanEntry->u1IsGrpChanged == VLAN_TRUE))
        {
            for (pGroupEntry = pVlanEntry->pGroupTable;
                 pGroupEntry != NULL; pGroupEntry = pGroupEntry->pNextNode)
            {
                if (pGroupEntry->u1IsLearntInfoChanged == VLAN_TRUE)
                {
                    /* Some member ports present in the learnt port list */
                    if ((u2BufSize - u2OffSet) < u2GrpUpdLen)
                    {
                        /* no room for the current message */
                        VlanRedSendMsgToRm (pMsg, u2OffSet);

                        pMsg = RM_ALLOC_TX_BUF (u2BufSize);

                        if (pMsg == NULL)
                        {
                            VLAN_TRC (VLAN_RED_TRC,
                                      (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                      VLAN_NAME,
                                      "RM alloc failed. Periodic "
                                      "updates not sent.\n");
                            return;
                        }

                        u2OffSet = 0;
                    }

                    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                                        VLAN_GRP_UPDATE_MESSAGE);
                    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2GrpUpdLen);
                    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pVlanEntry->VlanId);
                    VLAN_RM_PUT_N_BYTE (pMsg, pGroupEntry->MacAddr,
                                        &u2OffSet, ETHERNET_ADDR_SIZE);
                    VLAN_RM_PUT_N_BYTE (pMsg, pGroupEntry->LearntPorts,
                                        &u2OffSet, sizeof (tLocalPortList));

                    pGroupEntry->u1IsLearntInfoChanged = VLAN_FALSE;

                    VLAN_RED_DECR_GRPS_CHANGED ();
                }                /* pGroupEntry->u1IsLearntInfoChanged == VLAN_TRUE */
            }                    /* for - pGroupEntry */

            pVlanEntry->u1IsGrpChanged = VLAN_FALSE;
        }                        /* if (VLAN_GMRP_STATUS () == VLAN_ENABLED) */

        if ((VLAN_RED_VLANS_CHANGED () == 0) && (VLAN_RED_GRPS_CHANGED () == 0))
        {
            /* Nothing to send break */
            break;
        }
    }                            /* VLAN_SCAN_VLAN_TABLE */

    if (u2OffSet != 0)
    {
        /* Send the buffer out */
        VlanRedSendMsgToRm (pMsg, u2OffSet);
    }
    else
    {
        /* Empty buffer created without any update messages filled */
        RM_FREE (pMsg);
    }
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleUpdates                                 */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      RM module. This function invokes appropriate fns.    */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedHandleUpdates (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UINT4               u4Port, u4RcvPortId, u4FidIndex = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2Length;
    UINT2               u2RemMsgLen;
    UINT2               u2MinLen;
    UINT2               u2EvbMsgLen = 0;
    tVlanId             u2VlanId;
    UINT1               u1MsgType;
    UINT1               u1PortOperStatus;
    UINT4               u4ContextId = 0;
    tMacAddr            McastAddr, UcastAddr;
    tRmProtoEvt         ProtoEvt;
#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    UINT1               u1BrgModeChgFlag;
#endif
#endif

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MinLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
        VLAN_RED_CONTEXT_FIELD_SIZE;

    VLAN_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    if (u1MsgType == VLAN_BULK_UPD_TAIL_MESSAGE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "Bulk update tail message received, which "
                         "indicates that bulk updates are completed "
                         "in VLAN module. It should be indicated to "
                         "higher layer modules. \n");
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        VLAN_GBL_TRC ("[STANDBY]: Received Bulk Update Tail Message \r\n");
        VlanRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    /* Only bulk req msg shud be received */
    if (u1MsgType == VLAN_BULK_REQ_MESSAGE)
    {
        VLAN_GBL_TRC("[ACTIVE]: Received Bulk Request \r\n");
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "Bulk request message received from the "
                         "standby node. Treating as if STANDBY_UP"
                         " event is received \n");

        if (VLAN_RED_NUM_STANDBY_NODES () == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             ALL_FAILURE_TRC,
                             "Bulk request message received from the "
                             "standby node before RM_STANDBY_UP"
                             " Ignoring the message !! \n");

            VLAN_RED_BULK_REQ_RECD () = VLAN_TRUE;
            return;
        }

        VLAN_RED_BULK_REQ_RECD () = VLAN_FALSE;
        /* On recieving VLAN_BULK_REQ_MESSAGE, Bulk updation process
         * should be restarted.
         */
        gVlanRedInfo.BulkUpdNextVlanId = 0;
        gVlanRedInfo.BulkUpdNextPort = 0;
        gVlanRedInfo.u4BulkUpdNextContext = 0;
#ifdef EVB_WANTED
        gu4EvbBulkUpdPrevPort = 0;
        gu4EvbBulkUpdPrevContxt = 0;
#endif 
        VlanRedHandleStandbyUpEvent ();
        return;
    }

    /* HITLESS RESTART */
    if (u1MsgType == RM_HR_STDY_ST_PKT_REQ)
    {
#ifdef GARP_WANTED
        /* Send RM_MESSAGE trigger to GARP */
        VlanGarpRedSendMsg (RM_HR_STDY_ST_PKT_REQ);
#endif
        return;
    }
    u2OffSet = 0;

    if (VLAN_NODE_STATUS () != VLAN_NODE_STANDBY)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, " Node status not standby !! \n");

        return;
    }

    while ((u2OffSet + u2MinLen) < u2DataLen)
    {
        VLAN_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        VLAN_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* doesnot have room for type and length field itself. 
             * Skip to next attribute */
            u2OffSet += u2Length;
            continue;
        }

        VLAN_RM_GET_4_BYTE (pMsg, &u2OffSet, u4ContextId);

        u2RemMsgLen = u2Length - u2MinLen;

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u2OffSet = u2OffSet + u2RemMsgLen;
            continue;
        }

        if (VLAN_MODULE_ADMIN_STATUS () != VLAN_ENABLED)
        {
            VlanReleaseContext ();
            u2OffSet = u2OffSet + u2RemMsgLen;
            continue;
        }

        switch (u1MsgType)
        {
            case VLAN_PORT_OPER_STATUS_MESSAGE:

                if (u2RemMsgLen != (VLAN_RED_PORT_NO_LEN +
                                    VLAN_RED_PORT_OPER_STATUS_SIZE))
                {
                    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the Port oper status "
                              "update message is incorrect. Skipping the "
                              "update \n");
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    VlanRmHandleProtocolEvent (&ProtoEvt);
                    u2OffSet += u2RemMsgLen;
                    break;
                }

                if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
                {
                    /* room exists completely for this update... */
                    VLAN_RM_GET_4_BYTE (pMsg, &u2OffSet, u4Port);
                    VLAN_RM_GET_1_BYTE (pMsg, &u2OffSet, u1PortOperStatus);

                    if (VlanRedHandlePortOperStatusUpdate (u4Port,
                                                           u1PortOperStatus)
                        == VLAN_FAILURE)
                    {
                        ProtoEvt.u4Error = RM_PROCESS_FAIL;
                        VlanRmHandleProtocolEvent (&ProtoEvt);
                    }
                }
                else
                {
                    u2OffSet = u2DataLen;
                }

                break;

            case VLAN_MCAST_DEL_ONTIMEOUT:
                if (u2RemMsgLen != (VLAN_RED_VLAN_ID_LEN +
                                    VLAN_RED_MAC_ADDR_LEN +
                                    VLAN_RED_PORT_NO_LEN))
                {
                    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the update message is"
                              "incorrect. Skipping the update \n");
                    u2OffSet += u2RemMsgLen;
                    break;
                }
                if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
                {
                    VLAN_RM_GET_2_BYTE (pMsg, &u2OffSet, u2VlanId);
                    VLAN_RM_GET_N_BYTE (pMsg, McastAddr, &u2OffSet,
                                        VLAN_MAC_ADDR_LEN);
                    VLAN_RM_GET_4_BYTE (pMsg, &u2OffSet, u4RcvPortId);
                    VlanRedHandleMcastAddrDelOnTimeOut (u2VlanId, McastAddr,
                                                        u4RcvPortId);
                }
                else
                {
                    u2OffSet = u2DataLen;
                }
                break;

            case VLAN_UCAST_DEL_ONTIMEOUT:
                if (u2RemMsgLen != (VLAN_RED_FDB_ID_LEN +
                                    VLAN_RED_MAC_ADDR_LEN +
                                    VLAN_RED_PORT_NO_LEN))
                {
                    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the update message is"
                              "incorrect. Skipping the update \n");
                    u2OffSet += u2RemMsgLen;
                    break;
                }
                if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
                {
                    VLAN_RM_GET_4_BYTE (pMsg, &u2OffSet, u4FidIndex);
                    VLAN_RM_GET_N_BYTE (pMsg, UcastAddr, &u2OffSet,
                                        VLAN_MAC_ADDR_LEN);
                    VLAN_RM_GET_4_BYTE (pMsg, &u2OffSet, u4RcvPortId);
                    VlanRedHandleUcastAddrDelOnTimeOut (u4FidIndex, UcastAddr,
                                                        u4RcvPortId);
                }
                else
                {
                    u2OffSet = u2DataLen;
                }
                break;

         case VLAN_EVB_RED_CDCP_TLV_CHANGE:
                VlanEvbRedGetMsgLen (u1MsgType, &u2EvbMsgLen);
                if ((u2RemMsgLen + u2MinLen) %u2EvbMsgLen != 0)
                {
                    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the CDCP TLV "
                              "update message is incorrect. Skipping the "
                              "update of length :%d \r\n",u2RemMsgLen);
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    VlanRmHandleProtocolEvent (&ProtoEvt);
                    u2OffSet = (UINT2)(u2OffSet + u2RemMsgLen);
                    break;
                }

                if ( u2RemMsgLen <= u2DataLen)
                {
                    if (VlanEvbRedProcessCdcpTlvSyncUp (pMsg, &u2OffSet)
                        == VLAN_FAILURE)
                    {
                        ProtoEvt.u4Error = RM_PROCESS_FAIL;
                        VlanRmHandleProtocolEvent (&ProtoEvt);
                    }
                }
                else
                {
                    u2OffSet = u2DataLen;
                }



            break; 
        case VLAN_EVB_RED_SBP_ADD_SYNC_UP:
        case VLAN_EVB_RED_SBP_DEL_SYNC_UP:
                VlanEvbRedGetMsgLen (u1MsgType, &u2EvbMsgLen);
                if ((u2RemMsgLen+u2MinLen) % u2EvbMsgLen != 0)
                {
                    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the SBP "
                              "update message is incorrect. Skipping the "
                              "update of length :%d \r\n",u2RemMsgLen);
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    VlanRmHandleProtocolEvent (&ProtoEvt);
                    u2OffSet = (UINT2)(u2OffSet + u2RemMsgLen);
                    break;
                }

                if (u2RemMsgLen <= u2DataLen)
                {
                    if (VlanEvbRedProcessSbpSyncUp (pMsg, u1MsgType, &u2OffSet)
                        == VLAN_FAILURE)
                    {
                        ProtoEvt.u4Error = RM_PROCESS_FAIL;
                        VlanRmHandleProtocolEvent (&ProtoEvt);
                    }
                }
                else
                {
                    u2OffSet = u2DataLen;
                }

            break;
         case VLAN_EVB_RED_CDCP_REMOTE_ROLE:
                VlanEvbRedGetMsgLen (u1MsgType, &u2EvbMsgLen);
                if ((u2RemMsgLen + u2MinLen) %u2EvbMsgLen != 0)
                {
                    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the RemoteRole "
                              "update message is incorrect. Skipping the "
                              "update of length :%d \r\n",u2RemMsgLen);
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    VlanRmHandleProtocolEvent (&ProtoEvt);
                    u2OffSet = (UINT2)(u2OffSet + u2RemMsgLen);
                    break;
                }

                if ( u2RemMsgLen <= u2DataLen)
                {
                    if (VlanEvbRedProcessRemoteRoleSyncUp (pMsg, &u2OffSet)
                        == VLAN_FAILURE)
                    {
                        ProtoEvt.u4Error = RM_PROCESS_FAIL;
                        VlanRmHandleProtocolEvent (&ProtoEvt);
                    }
                }
                else
                {
                    u2OffSet = u2DataLen;
                }

            break; 

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
            case VLAN_HWAUD_BRG_MODE_CHG:
                /* Set the Flag indicating the Bridge Mode change in the 
                 * Active node. Store the ContextId.
                 */
                if (u2RemMsgLen != (VLAN_RED_BRG_MODE_CHG_FLAG_SIZE))
                {
                    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "The length field in the update message is"
                              "incorrect. Skipping the update \n");
                    u2OffSet += u2RemMsgLen;
                    break;
                }
                if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
                {
                    VLAN_RM_GET_1_BYTE (pMsg, &u2OffSet, u1BrgModeChgFlag);

                    VLAN_RED_HW_AUD_BRG_MODE_CONTEXT () = u4ContextId;
                    VLAN_RED_HW_AUD_BRG_MODE_FLAG () = u1BrgModeChgFlag;
                }
                else
                {
                    u2OffSet = u2DataLen;
                }
                break;
            case VLAN_RED_SYNC_CACHE_INFO:
                if (!(gVlanRedInfo.u1ForceFullAudit & VLAN_RED_VLAN_TABLE_MASK))
                {
                    VlanRedProcessCacheInfo (pMsg, u4ContextId, &u2OffSet);
                }
                break;
            case VLAN_RED_SYNC_ST_UCAST_CACHE_INFO:
                if (!
                    (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_UCAST_TABLE_MASK))
                {
                    VlanRedProcessStUcastCacheInfo (pMsg, u4ContextId,
                                                    &u2OffSet);
                }
                break;
            case VLAN_RED_SYNC_MCAST_CACHE_INFO:
                if (!
                    (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_MCAST_TABLE_MASK))
                {
                    VlanRedProcessMcastCacheInfo (pMsg, u4ContextId, &u2OffSet);
                }
                break;
            case VLAN_RED_SYNC_PROTO_CACHE_INFO:
                if (!
                    (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_PROTO_TABLE_MASK))
                {
                    VlanRedProcessProtoVlanCacheInfo (pMsg, u4ContextId,
                                                      &u2OffSet);
                }
                break;

#endif
#endif

            default:
                VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Unknown message Type " "Skipping the attribute \n");

                u2OffSet += u2Length;    /* Skip the attribute */
                break;
        }
        VlanReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : VlanRedSendBulkReqMsg                                */
/*                                                                           */
/* Description        : This function sends bulk request message to the      */
/*                      active node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedSendBulkReqMsg (VOID)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;

    if (VLAN_NODE_STATUS () != VLAN_NODE_STANDBY)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "Node currently not standby. Bulk Request not sent \n");
        return;
    }

    /*
     *    VLAN Bulk Request message           
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (7)   |   3        |
     *    |-------------------------
     *
     */

    if ((pMsg = RM_ALLOC_TX_BUF (VLAN_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "RM alloc failed. Bulk Request not sent \n");
        return;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC, "Sending Bulk request message \n");

    u2OffSet = 0;

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_BULK_REQ_MESSAGE);

    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_RED_BULK_REQ_MSG_SIZE);
    VLAN_GBL_TRC ("[STANDBY]: Send Bulk Request Message \r\n");
    VlanRedSendMsgToRm (pMsg, u2OffSet);
}

/*****************************************************************************/
/* Function Name      : VlanRedSendDelAllDynInfoUpdate                       */
/*                                                                           */
/* Description        : This function sends delete all dynamic information   */
/*                      update message to the standby node. This function    */
/*                      will be invoked whenever GVRP/GMRP is disabled       */
/*                      globally or on a port or if the port becomes         */
/*                      operationally down.                                  */
/*                                                                           */
/* Input(s)           : u1Flag - Indicates whether VLAN or GROUP information.*/
/*                      u4Port - Indicates port number for which information */
/*                               needs to be deleted. Will be 0 for all ports*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedSendDelAllDynInfoUpdate (UINT1 u1Flag, UINT4 u4Port)
{
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (u4Port);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedSendDataAppliedMsg                            */
/*                                                                           */
/* Description        : This function sends data applied message to VLAN     */
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanRedSendDataAppliedMsg (VOID)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC,
                         "Vlan module not initialized !!!!!! \n");

        return VLAN_FAILURE;
    }
    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (pVlanQMsg == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "No memory to send data applied message !!!!!! \n");

        return VLAN_FAILURE;
    }

    MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = VLAN_DYN_DATA_APPLIED_MSG;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC,
                         "Data Applied Message Enqueue FAILED\n");

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, (UINT1 *) pVlanQMsg);

        return VLAN_FAILURE;
    }

    /* Sent the event to LA CTRL Task. */
    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Data applied Event send FAILED\n");

        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedGetNodeStatus                                 */
/*                                                                           */
/* Description        : This function returns the node status after getting  */
/*                      it from the RM module.                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : tVlanNodeStatus                                      */
/*****************************************************************************/
tVlanNodeStatus
VlanRedGetNodeStatus (VOID)
{
    tVlanNodeStatus     NodeStatus;
    UINT4               u4RmNodeState;

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC, "Node status from RM is ");

    if (VLAN_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_NOT_RESTORED)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC, "NOT RESTORED, hence IDLE \n ");
        NodeStatus = VLAN_NODE_IDLE;
        return NodeStatus;
    }

    u4RmNodeState = VlanRmGetNodeState ();

    switch (u4RmNodeState)
    {
        case RM_INIT:
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC, "IDLE \n ");
            NodeStatus = VLAN_NODE_IDLE;
            break;

        case RM_ACTIVE:
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC, "ACTIVE \n ");
            NodeStatus = VLAN_NODE_ACTIVE;
            break;

        case RM_STANDBY:
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             CONTROL_PLANE_TRC, "STANDBY \n ");
            NodeStatus = VLAN_NODE_STANDBY;
            break;

        default:
            NodeStatus = VLAN_NODE_IDLE;    /* Shud not come here */
            break;
    }

    return NodeStatus;
}

/*****************************************************************************/
/* Function Name      : VlanRedInitRedundancyInfo                            */
/*                                                                           */
/* Description        : This function initialises the required information   */
/*                      to support redundancy based on the current node      */
/*                      status. This function will be invoked when the VLAN  */
/*                      module is started afresh.                            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanRedInitRedundancyInfo (VOID)
{
    switch (VLAN_NODE_STATUS ())
    {
        case VLAN_NODE_ACTIVE:

            VLAN_RED_NUM_STANDBY_NODES () = VlanRmGetStandbyNodeCount ();

            break;

        case VLAN_NODE_STANDBY:

            /*Do Nothing */

            break;

        default:
            break;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedDeInitRedundancyInfo                          */
/*                                                                           */
/* Description        : This function deinitialises all the information      */
/*                      allocated to support redundancy based on the node    */
/*                      status. This function will be invoked when the VLAN  */
/*                      module is shutdown.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedDeInitRedundancyInfo (VOID)
{
    switch (VLAN_NODE_STATUS ())
    {
        case VLAN_NODE_ACTIVE:

            VLAN_RED_NUM_STANDBY_NODES () = 0;

            break;

        case VLAN_NODE_STANDBY:
            break;

        default:
            break;
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAuditMain                                        */
/*                                                                            */
/*  Description     : This the main routine for the VLAN Audit submodule.     */
/*                                                                            */
/*  Input(s)        : pi1Param                                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedAuditMain (INT1 *pi1Param)
{
    tOsixTaskId         TempTaskId;
    UINT4               u4Events;
#ifdef NPAPI_WANTED
    UINT4               u4Context;
#endif

    UNUSED_PARAM (pi1Param);

    while (1)
    {
        if ((VLAN_RECEIVE_EVENT (VLAN_AUDIT_TASK_ID,
                                 (VLAN_RED_AUDIT_START_EVENT |
                                  VLAN_RED_AUDIT_TASK_DEL_EVENT |
                                  VLAN_RED_PERFORM_MCAST_AUDIT),
                                 OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {
            if (u4Events & VLAN_RED_AUDIT_TASK_DEL_EVENT)
            {

                VLAN_AUDIT_TASK_ID = 0;

                TempTaskId = OsixGetCurTaskId ();

                /* Deletion of audit task in suicidal manner. */
                VLAN_DELETE_TASK (TempTaskId);

                /* Audit task would have been terminated in the
                 * previous step */
            }

            if (u4Events & VLAN_RED_AUDIT_START_EVENT)
            {
#ifdef NPAPI_WANTED
                VlanRedStartAudit ();
#endif
            }

            if (u4Events & VLAN_RED_PERFORM_MCAST_AUDIT)
            {
#ifdef NPAPI_WANTED
                for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
                {
#if defined (IGS_WANTED)
                    if (VlanSnoopIsIgmpSnoopingEnabled (u4Context) !=
                        SNOOP_ENABLED)
                    {
                        continue;
                    }
                    /* Audit should be done only for Context in which 
                     * Snooping is in MAC Mode only.*/
                    if (SnoopMiGetForwardingType (u4Context)
                        != SNOOP_MCAST_FWD_MODE_MAC)
                    {
                        continue;
                    }
#endif
                    if (gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE)
                    {
                        /* Perform OPTIMIZED Mcast HW Audit */
                        VlanRedSyncPendingMcastEntries (u4Context);
                    }
                    else
                    {
                        /* Perform FULL Mcast HW Audit */
                        VlanRedSyncMcastTable (u4Context);
                    }
                }
#endif
            }
        }
    }
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedStartAudit                                       */
/*                                                                            */
/*  Description     : This function performs audit of the VLAN parameters     */
/*                    between the hardware and the software. This function    */
/*                    will be invoked whenever the AUDIT_START_EVENT is       */
/*                    received from the VLAN module (AUDIT_START_EVENT will   */
/*                    be sent on the expiry of relearn timer when the node    */
/*                    becomes active from standby).                           */
/*                    This function synchronizes the VLAN parameters between  */
/*                    the hardware and the software.                          */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedStartAudit (VOID)
{
    UINT4               u4Context;
    tPbbVlanAuditStatus PbbVlanAuditStatus;

    VLAN_LOCK ();
    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }
    VLAN_UNLOCK ();

    MEMSET (&PbbVlanAuditStatus, VLAN_INIT_VAL, sizeof (tPbbVlanAuditStatus));

    if (VLAN_RED_HW_AUD_BRG_MODE_FLAG () == VLAN_TRUE)
    {
        /* If Brg Mode change Flag is set, it means that switchover has 
         * happened in between Bridge Mode Configuration, hence Audit for that 
         * context alone is enough.
         */

        VLAN_LOCK ();

        if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
        {
            VLAN_UNLOCK ();
            return;
        }

        if (VlanSelectContext (VLAN_RED_HW_AUD_BRG_MODE_CONTEXT ()) !=
            VLAN_SUCCESS)
        {
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             ALL_FAILURE_TRC,
                             "Context Selection failed for Vlan Audit \n ");

            VLAN_UNLOCK ();
            return;
        }

        VlanHwSetBrgMode (VLAN_CURR_CONTEXT_ID (), VLAN_BRIDGE_MODE ());
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        VlanRedContextAudit (VLAN_RED_HW_AUD_BRG_MODE_CONTEXT ());

        /* Indicate the PBB about the completion of Vlan H/w Audit */

        PbbVlanAuditStatus.u4VlanAudStatus = PBB_VLAN_AUDIT_BRG_MODE_CHG;
        PbbVlanAuditStatus.u4ContextId = VLAN_RED_HW_AUD_BRG_MODE_CONTEXT ();

        VlanPbbVlanAuditStatusInd (&PbbVlanAuditStatus);
        if (gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE)
        {
            if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_VLAN_TABLE_MASK)
            {
                /* Reset the VLAN full audit mask */
                gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_VLAN_TABLE_MASK;
            }
            if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_MCAST_TABLE_MASK)
            {
                /* Reset the MCAST full audit mask */
                gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_MCAST_TABLE_MASK;
            }
            if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_UCAST_TABLE_MASK)
            {
                /* Reset the UCAST full audit mask */
                gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_UCAST_TABLE_MASK;
            }
            if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_PROTO_TABLE_MASK)
            {
                /* Reset the Proto VLAN full audit mask */
                gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_PROTO_TABLE_MASK;
            }

        }
        return;
    }

    /*
     * Learning type cannot be determined from the state of the hardware
     * database. Hence it is assumed that the learning type is same across
     * both active and standby node.
     */
    for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
    {
        VlanRedContextAudit (u4Context);
    }

    PbbVlanAuditStatus.u4VlanAudStatus = PBB_VLAN_AUDIT_COMPLETED;
    PbbVlanAuditStatus.u4ContextId = VLAN_INIT_VAL;

    /* Send Indication to PBB that Vlan Audit is finished */
    VlanPbbVlanAuditStatusInd (&PbbVlanAuditStatus);
    if (gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE)
    {
        if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_VLAN_TABLE_MASK)
        {
            /* Reset the VLAN full audit mask */
            gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_VLAN_TABLE_MASK;
        }
        if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_MCAST_TABLE_MASK)
        {
            /* Reset the MCAST full audit mask */
            gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_MCAST_TABLE_MASK;
        }
        if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_UCAST_TABLE_MASK)
        {
            /* Reset the UCAST full audit mask */
            gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_UCAST_TABLE_MASK;
        }
        if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_PROTO_TABLE_MASK)
        {
            /* Reset the UCAST full audit mask */
            gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_PROTO_TABLE_MASK;
        }

    }
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedContextAudit                                     */
/*                                                                            */
/*  Description     : This function performs audit for the entire context     */
/*                    related configurations.                                 */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier.                       */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedContextAudit (UINT4 u4Context)
{
    UINT1               u1GmrpStatus;
    UINT1               u1ProtocolId;
    tMacAddr            MacAddr;
    tMacAddr            SyncMacAddr;

    VLAN_LOCK ();

    /* Check for Audit Flag */
    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        for (u1ProtocolId = 1; u1ProtocolId < VLAN_MAX_PROT_ID; u1ProtocolId++)
        {
            VLAN_MEMSET (MacAddr, 0, sizeof (tMacAddr));
            VLAN_MEMSET (SyncMacAddr, 0, sizeof (tMacAddr));

            VlanTunnelProtocolMac (u1ProtocolId, MacAddr);

            /* if synced Protocol mac address is not same as in software
             * delete filter and reinstall with software mac address*/
            VlanHwGetSyncedTnlProtocolMacAddr (VLAN_CURR_CONTEXT_ID (),
                                               (UINT2) u1ProtocolId,
                                               SyncMacAddr);

            /* Both the function VlanTunnelProtocolMac and 
             * VlanHwGetSyncedTnlProtocolMacAddr will return NULL 
             * in case of IGMP Filter. So the below NP call will 
             * never happen for IGMP Filter.*/

            if (VLAN_MEMCMP (SyncMacAddr, MacAddr, sizeof (tMacAddr)) != 0)
            {
                VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                           MacAddr, u1ProtocolId);
            }
        }
    }

    u1GmrpStatus = VLAN_GMRP_STATUS ();

    VlanRedSyncDefaultVlanId ();

    VlanReleaseContext ();

    VLAN_UNLOCK ();

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE)
    {
        /* Optimized VLAN HW Audit is enabled */
        VlanRedSyncPendingVlanEntries (u4Context);
    }
    else
    {
        /* Perform a full VLAN AUDIT */
        VlanRedSyncVlanTable (u4Context);
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        VlanRedSyncPortParams (u4Context);
    }

    if ((gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE) &&
        (!(gVlanRedInfo.u1ForceFullAudit & VLAN_RED_PROTO_TABLE_MASK)))
    {
        /* Optimized VLAN HW Audit is enabled */
        VlanRedSyncPendingProtoVlanEntries (u4Context);
    }

#if defined (IGS_WANTED) || defined (MLDS_WANTED)    /* SNOOP_APPROACH */
    if ((u1GmrpStatus == VLAN_ENABLED) ||
        ((VlanSnoopIsIgmpSnoopingEnabled (u4Context) != SNOOP_ENABLED) &&
         (VlanSnoopIsMldSnoopingEnabled (0) != SNOOP_ENABLED)) ||
        (((VlanSnoopIsIgmpSnoopingEnabled (u4Context) == SNOOP_ENABLED)
          || (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
         && (SnoopMiGetForwardingType (u4Context) == SNOOP_MCAST_FWD_MODE_IP)))
#endif /* IGS_WANTED */
    {
        /*
         * VLAN will perform multicast audit in following cases:
         * 1. GMRP is enabled (meaning IGS is disabled. Performs audit for
         * static and dynamic entries learnt throught GMRP.)
         * 2. GMRP and IGS are disabled (perform audit for static mcast
         * entries.
         * 3. GMRP is disabled and IGS is enabled and IGS forwarding table
         * mode is IP based. In this case VLAN table will only have static
         * multicast entries and hence it will immediately start audit.
         * 4. GMRP is disabled and IGS is enabled and IGS forwarding table
         * mode is MAC based. In this case the audit will be performed only
         * after the completion of relearning by IGS module. IGS module 
         * will invoke VLAN API to start the mcast audit.
         */
        if (gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE)
        {
            /* Perform OPTIMIZED Mcast HW Audit */
            VlanRedSyncPendingMcastEntries (u4Context);
        }
        else
        {
            /* Perform FULL Mcast HW Audit */
            VlanRedSyncMcastTable (u4Context);
        }
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_TRUE)
    {
        /* Perform OPTIMIZED Static FDB HW Audit */
        VlanRedSyncPendingFdbs (u4Context);
    }
    else
    {
        /* Perform FULL Static FDB HW Audit */
        VlanRedSyncFdb (u4Context);
    }

    VlanRedSyncWildCardTbl (u4Context);

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncVlanTable                                    */
/*                                                                            */
/*  Description     : This function performs audit of the VLAN current table  */
/*                    and VLAN memberships.                                   */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncVlanTable (UINT4 u4Context)
{
    tVlanCurrEntry     *pVlanEntry;
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pu1TempLocalPortList = NULL;
    tHwMiVlanEntry      HwEntry;
    tVlanId             VlanId;
    INT4                i4AuditFlag;
    INT4                i4RetVal;
    UINT1               u1Status = OSIX_FALSE;

    VlanL2IwfGetAllToOneBndlStatus (u4Context, &u1Status);

    /* All to one bundling status is set, so no need to do audit for the
       Vlan Table. This is because if PISID is configured on any CNP, then we
       assume no VLAN are not configured for that component */
    if (u1Status == OSIX_TRUE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "All to one bundling status set"
                         "No audit for Vlan Table \n ");
        return;
    }

    /* Scan thru VLAN table */
    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        VLAN_LOCK ();

        if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
        {
            VLAN_UNLOCK ();
            break;
        }

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            VlanId = VLAN_MAX_VLAN_ID;
            VLAN_UNLOCK ();
            break;
        }

        pVlanEntry = VlanGetVlanEntry (VlanId);

        MEMSET (&HwEntry, 0, sizeof (tHwMiVlanEntry));

        i4RetVal = VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId,
                                      &HwEntry);

        i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;

        if ((i4RetVal == VLAN_SUCCESS) && (pVlanEntry != NULL))
        {
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_BOTH;
        }
        else if (i4RetVal == VLAN_SUCCESS)
        {
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY;
        }
        else if (pVlanEntry != NULL)
        {
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY;
        }

        switch (i4AuditFlag)
        {
            case VLAN_RED_ENTRY_PRESENT_IN_BOTH:
                /* Check and sync up the VLAN member ports */
                VlanRedSyncUpVlanMemberPorts (pVlanEntry, &HwEntry);
                break;

            case VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY:
                /* Delete the VLAN from the hardware */
                /* it is assumed that VlanHwDelVlanEntry will 
                 * disassociate the VLAN to FID mappings and delete the
                 * FDB itself.
                 */
                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : VLAN %d Present only in Hardware. "
                               " \n", VlanId);
                VlanHwDelVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId);

                VlanVcmSispUpdatePortVlanMappingInHw (VLAN_CURR_CONTEXT_ID (),
                                                      VlanId,
                                                      HwEntry.HwMemberPbmp,
                                                      SISP_DELETE);
                break;

            case VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY:
                /* Add the VLAN to the H/W */
                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : VLAN %d Present only in Software. "
                               " \n", VlanId);
                pu1TempLocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1TempLocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    break;
                }
                MEMSET (pu1TempLocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pu1TempLocalPortList);

                if (pVlanEntry->pStVlanEntry != NULL)
                {
                    pu1LocalPortList = UtilPlstAllocLocalPortList
                        (sizeof (tLocalPortList));
                    if (pu1LocalPortList == NULL)
                    {
                        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                  "Memory allocation failed for tLocalPortList\n");
                        UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
                        break;
                    }
                    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                    VLAN_GET_UNTAGGED_PORTS (pVlanEntry->pStVlanEntry,
                                             pu1LocalPortList);

                    VlanHwAddVlanEntry (pVlanEntry->VlanId,
                                        pu1TempLocalPortList, pu1LocalPortList);
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                }
                else
                {
                    VlanHwAddVlanEntry (pVlanEntry->VlanId,
                                        pu1TempLocalPortList, gNullPortList);
                }

                VlanVcmSispUpdatePortVlanMappingInHw (VLAN_CURR_CONTEXT_ID (),
                                                      VlanId,
                                                      pu1TempLocalPortList,
                                                      SISP_ADD);
                UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
                break;

            case VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH:
                break;

            default:
                break;
        }                        /* Switch -> AuditFlag */

        VlanReleaseContext ();
        VLAN_UNLOCK ();
    }                            /* For each VLAN */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncUpVlanMemberPorts                            */
/*                                                                            */
/*  Description     : This function will be invoked to synchronize the VLAN   */
/*                    and untagged memberships between the hardware and the   */
/*                    software.                                               */
/*                                                                            */
/*  Input(s)        : pVlanEntry - Pointer to the VLAN current entry          */
/*                    pHwEntry   - Pointer to the hardware VLAN info.         */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncUpVlanMemberPorts (tVlanCurrEntry * pVlanEntry,
                              tHwMiVlanEntry * pHwEntry)
{
    UINT1              *pu1LocalPortList = NULL;
    tLocalPortList      UntaggedPorts;
    UINT1               u1RetVal1;
    INT4                i4RetVal2;

    MEMSET (UntaggedPorts, 0, sizeof (tLocalPortList));

    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_GET_UNTAGGED_PORTS (pVlanEntry->pStVlanEntry, UntaggedPorts);
    }

    VLAN_IS_ALL_MATCHING_FOR_CURR_EGRESS (pVlanEntry, pHwEntry->HwMemberPbmp,
                                          u1RetVal1);

    i4RetVal2 = MEMCMP (UntaggedPorts, pHwEntry->HwUntagPbmp,
                        sizeof (tLocalPortList));

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return;
    }
    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
    VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pu1LocalPortList);
    if ((u1RetVal1 != 0) || (i4RetVal2 != 0))
    {
        /* Difference in member ports or untagged ports */
        /*
         * VlanHwAddVlanEntry is supposed to overwrite the VLAN member
         * ports (if VLAN entry already exists) without affecting the 
         * data traffic.
         */
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT : VLAN %d member/untagged ports different. "
                       "Overwriting software values \n", pVlanEntry->VlanId);
        VlanHwAddVlanEntry (pVlanEntry->VlanId, pu1LocalPortList,
                            UntaggedPorts);
    }

    if (u1RetVal1 != 0)
    {
        VlanVcmSispUpdatePortVlanMappingInHw (VLAN_CURR_CONTEXT_ID (),
                                              pVlanEntry->VlanId,
                                              pu1LocalPortList, SISP_ADD);
    }
    UtilPlstReleaseLocalPortList (pu1LocalPortList);
    /* */
#ifdef VLAN_EXTENDED_FILTER
    /* Program FwdALL and FwdUnReg membership */
    /* Program fwd all ports */
    if (MEMCMP (pHwEntry->HwFwdAllPbmp, pVlanEntry->AllGrps.Ports,
                sizeof (tLocalPortList)) != 0)
    {
        VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId,
                               VLAN_ALL_GROUPS, pVlanEntry->AllGrps.Ports);
    }

    if (MEMCMP (pHwEntry->HwFwdUnregPbmp, pVlanEntry->UnRegGrps.Ports,
                sizeof (tLocalPortList)) != 0)
    {
        /* Program fwd unreg ports */
        VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId,
                               VLAN_UNREG_GROUPS, pVlanEntry->UnRegGrps.Ports);
    }
#endif /* VLAN_EXTENDED_FILTER */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAuditProtoVLanTable                             */
/*                                                                            */
/*  Description     : This function synchronizes the port protocol parameters */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : u2Port - Port number to be synced.                      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedAuditProtoVlanTable (tVlanProtoTemplate * pVlanProtoTemplate,
                            UINT4 u4Context, UINT4 u4GroupId, UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSet = NULL;
    tVlanProtGrpEntry  *pGroupEntry = NULL;
    INT4                i4RetVal = 0;
    INT4                i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;
    tVlanId             VlanId = 0;
    UINT4               u4Port = 0;
    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Selection failed!! \n ");

        VLAN_UNLOCK ();
        return;
    }

    i4RetVal =
        VlanHwGetVlanProtocolMap (u4Context,
                                  u2Port,
                                  u4GroupId, pVlanProtoTemplate, &VlanId);

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        /* Now scan all the entries in the software and add those
         * entries which are not present in hardware */
        VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSet,
                       tVlanPortVidSet *)
        {
            if (pVlanPortVidSet->u1RowStatus == VLAN_ACTIVE)
            {
                if (u4GroupId == pVlanPortVidSet->u4ProtGrpId)
                {
                    /* Get group entry and get it from h/w */
                    pGroupEntry = VlanGetActiveProtoGrpEntry (u4GroupId);
                    break;
                }
            }
        }
    }

    if ((i4RetVal == VLAN_SUCCESS) && (pGroupEntry != NULL))
    {
        i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_BOTH;
    }
    else if (i4RetVal == VLAN_SUCCESS)
    {
        i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY;
    }
    else if (pGroupEntry != NULL)
    {
        i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY;
    }

    if (i4AuditFlag == VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT Protocol VLAN Mapping for port %d "
                       "and VLAN %d not present in Hardware \n",
                       u4Port, pVlanPortVidSet->VlanId);

        /* Add protocol vlan entry to the hardware */
        VlanHwAddVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  pGroupEntry->u4ProtGrpId,
                                  pVlanProtoTemplate,
                                  pVlanPortVidSet->u4ProtGrpId);
    }                            /* (i4RetVal != VLAN_SUCCESS) */

    if (i4AuditFlag == VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY)
    {
        u4Port = VLAN_GET_IFINDEX (u2Port);
        VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT Protocol VLAN Mapping for port %d "
                       "and VLAN %d not present in Hardware \n",
                       u4Port, pVlanPortVidSet->VlanId);

        /* Add protocol vlan entry to the hardware */
        VlanHwDelVlanProtocolMap (u4Context, u2Port,
                                  u4GroupId, pVlanProtoTemplate);

    }                            /* if (pGroupEntry != NULL) */
    if (i4AuditFlag == VLAN_RED_ENTRY_PRESENT_IN_BOTH)
    {
        /* Vlan id for the protocol group is different.
         * modify */
        VlanHwDelVlanProtocolMap (u4Context, u2Port,
                                  u4GroupId, pVlanProtoTemplate);
        VlanHwAddVlanProtocolMap (u4Context, u2Port,
                                  u4GroupId, pVlanProtoTemplate,
                                  pVlanPortVidSet->VlanId);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncMcastEntry                                   */
/*                                                                            */
/*  Description     : This function synchronizes the multicast entries        */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : u4Context - Virtual ContextId                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncMcastEntry (UINT4 u4Context,
                       tVlanId VlanId, tMacAddr McastAddr, UINT4 u4RcvPort)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tLocalPortList      HwPortList;
    tLocalPortList      TempPortList;
    UINT4               u4Port = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    MEMSET (HwPortList, 0, sizeof (tLocalPortList));
    MEMSET (TempPortList, 0, sizeof (tLocalPortList));
    /* Scan all the entries in the hardware and delete those
     * entries which are not present in the software */
    if (u4RcvPort != 0)
    {
        if (VlanGetContextInfoFromIfIndex (u4RcvPort, &u4ContextId,
                                           &u2LocalPort) != VLAN_SUCCESS)
        {
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             ALL_FAILURE_TRC,
                             "Failed to get local port from Ifindex \n");
            return;
        }
    }

    if ((VlanHwGetStMcastEntry
         (u4Context, VlanId, McastAddr, u4RcvPort, HwPortList) == VLAN_SUCCESS)
        || (VlanHwGetMcastEntry (u4Context, VlanId, McastAddr, HwPortList) ==
            VLAN_SUCCESS))
    {
        VlanRedVlanMcastCb (VlanId, McastAddr, u2LocalPort, HwPortList);
    }
    else
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry == NULL)
        {
            return;
        }
        pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort (McastAddr,
                                                             u4RcvPort,
                                                             pVlanEntry);

        if (pStMcastEntry != NULL)
        {
            /* Static mcast with receive port == 0 will be audited thru 
             * group entries */
            if (pStMcastEntry->u2RcvPort != 0)
            {
                MEMSET (HwPortList, 0, sizeof (tLocalPortList));
                u4Port = VLAN_GET_IFINDEX (pStMcastEntry->u2RcvPort);
                VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : Adding static mcast that is not "
                               "present in HW - Recv Port (%d), VLAN (%d) "
                               "Mcast Addr - ", u4Port, pVlanEntry->VlanId);
                VlanPrintMacAddr (VLAN_RED_TRC, pStMcastEntry->MacAddr);

                VlanHwAddStMcastEntry (pVlanEntry->VlanId,
                                       pStMcastEntry->MacAddr,
                                       pStMcastEntry->u2RcvPort,
                                       pStMcastEntry->EgressPorts);
            }                    /* if u2RcvPort != 0 */
            else
            {
                /* dynamic info can be there */
                pGroupEntry = pStMcastEntry->pGroupEntry;

                MEMSET (HwPortList, 0, sizeof (tLocalPortList));
                MEMSET (TempPortList, 0, sizeof (tLocalPortList));

                VLAN_ADD_PORT_LIST (TempPortList, pStMcastEntry->EgressPorts);

                VLAN_ADD_PORT_LIST (TempPortList, pGroupEntry->LearntPorts);

                /* Entry not present in the hardware */
                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                               VLAN_NAME, "AUDIT : Adding entry to HW - "
                               "VLAN (%d) Mcast Addr - ", pVlanEntry->VlanId);
                VlanPrintMacAddr (VLAN_RED_TRC, pGroupEntry->MacAddr);

                /* Entry not present in hardware */
#if  (defined IGS_WANTED || defined MLDS_WANTED)    /* SNOOP_APPROACH */
                if ((VlanSnoopIsIgmpSnoopingEnabled (u4Context)
                     == SNOOP_ENABLED) ||
                    (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
                {
                    /* 
                     * Add static mcast entry to hardware. Inform IGS 
                     * and reset learnt ports to all 0's.
                     */
                    VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                         pVlanEntry->VlanId,
                                         pStMcastEntry->
                                         MacAddr, pStMcastEntry->EgressPorts);

                    /* Post the message to IGS */

                    /* if the static mcast entry present only in SW,
                     * then create entry in HW and reset the
                     * Learnt port list as 0's in SW Group Tables.
                     */

                    SnoopRedMcastAuditSyncMsg
                        (u4Context,
                         SNOOP_RED_MCAST_PORTS_ONLY_IN_SW,
                         pVlanEntry->VlanId, pGroupEntry->MacAddr,
                         pGroupEntry->LearntPorts);

                    MEMSET (pGroupEntry->LearntPorts, 0,
                            sizeof (tLocalPortList));
                }
                else
#endif /* IGS_WANTED */
                {
                    /* 
                     * Add the mcast to hardware if IGS is disabled.
                     * In case of GMRP, s/w db is superset.
                     */
                    VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                         pVlanEntry->VlanId,
                                         pGroupEntry->MacAddr, TempPortList);
                }
            }
        }

        pGroupEntry = VlanGetGroupEntry (McastAddr, pVlanEntry);
        if (pGroupEntry != NULL)
        {
            /* Perform audit only for the pending entries.
             * Skip rest of the entries.
             */
            if (pGroupEntry->u1StRefCount == 0)
            {
                /* do audit only for the dynamic entries */
                MEMSET (HwPortList, 0, sizeof (tLocalPortList));

                /* Entry not present in hardware */
#if  (defined IGS_WANTED || defined MLDS_WANTED)
                if ((VlanSnoopIsIgmpSnoopingEnabled (u4Context)
                     == SNOOP_ENABLED) ||
                    (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
                {
                    SnoopRedMcastAuditSyncMsg
                        (u4Context,
                         SNOOP_RED_MCAST_ENTRY_ONLY_IN_SW,
                         pVlanEntry->VlanId, pGroupEntry->MacAddr, HwPortList);

                    VlanDeleteGroupEntry (pVlanEntry, pGroupEntry);

                    VlanPbUpdateCurrentMulticastMacCount (VLAN_DELETE);

                }
                else
#endif /* IGS_WANTED */
                {
                    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                                   VLAN_NAME, "AUDIT : Adding entry to "
                                   "H/W - VLAN (%d) Mcast Addr - ",
                                   pVlanEntry->VlanId);
                    VlanPrintMacAddr (VLAN_RED_TRC, pGroupEntry->MacAddr);

                    VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                         pVlanEntry->VlanId,
                                         pGroupEntry->MacAddr,
                                         pGroupEntry->LearntPorts);
                }
            }                    /*if (pGroupEntry->u1StRefCount == 0)  */
        }                        /* if pGroupEntry != NULL */
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedAuditVlanTable                                   */
/*                                                                            */
/*  Description     : This function performs audit of the VLAN current table  */
/*                    and VLAN memberships.                                   */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/

VOID
VlanRedAuditVlanTable (UINT4 u4Context, tVlanId VlanId)
{
    tHwMiVlanEntry      HwEntry;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1              *pu1TempLocalPortList = NULL;
    UINT1              *pu1LocalPortList = NULL;
    INT4                i4AuditFlag;
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    MEMSET (&HwEntry, 0, sizeof (tHwMiVlanEntry));

    i4RetVal = VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId, &HwEntry);

    i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;

    if ((i4RetVal == VLAN_SUCCESS) && (pVlanEntry != NULL))
    {
        i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_BOTH;
    }
    else if (i4RetVal == VLAN_SUCCESS)
    {
        i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY;
    }
    else if (pVlanEntry != NULL)
    {
        i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY;
    }

    switch (i4AuditFlag)
    {
        case VLAN_RED_ENTRY_PRESENT_IN_BOTH:
            /* Check and sync up the VLAN member ports */
            VlanRedSyncUpVlanMemberPorts (pVlanEntry, &HwEntry);
            break;

        case VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY:
            /* Delete the VLAN from the hardware */
            /* it is assumed that VlanHwDelVlanEntry will 
             * disassociate the VLAN to FID mappings and delete the
             * FDB itself.
             */
            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "AUDIT : VLAN %d Present only in Hardware. "
                           " \n", VlanId);
            VlanHwDelVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId);

            VlanVcmSispUpdatePortVlanMappingInHw (VLAN_CURR_CONTEXT_ID (),
                                                  VlanId,
                                                  HwEntry.HwMemberPbmp,
                                                  SISP_DELETE);
            break;

        case VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY:
            /* Add the VLAN to the H/W */
            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "AUDIT : VLAN %d Present only in Software. "
                           " \n", VlanId);
            pu1TempLocalPortList = UtilPlstAllocLocalPortList
                (sizeof (tLocalPortList));
            if (pu1TempLocalPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Memory allocation failed for tLocalPortList\n");
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return;
            }
            MEMSET (pu1TempLocalPortList, 0, sizeof (tLocalPortList));
            VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pu1TempLocalPortList);

            if (pVlanEntry->pStVlanEntry != NULL)
            {
                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_UNTAGGED_PORTS (pVlanEntry->pStVlanEntry,
                                         pu1LocalPortList);

                VlanHwAddVlanEntry (pVlanEntry->VlanId,
                                    pu1TempLocalPortList, pu1LocalPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }
            else
            {
                VlanHwAddVlanEntry (pVlanEntry->VlanId,
                                    pu1TempLocalPortList, gNullPortList);
            }

            VlanVcmSispUpdatePortVlanMappingInHw (VLAN_CURR_CONTEXT_ID (),
                                                  VlanId,
                                                  pu1TempLocalPortList,
                                                  SISP_ADD);
            UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
            break;

        case VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH:
            break;

        default:
            break;
    }                            /* Switch -> AuditFlag */

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncPortParams                                   */
/*                                                                            */
/*  Description     : This function synchronizes the port parameters between  */
/*                    the software and the hardware.                          */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncPortParams (UINT4 u4Context)
{
    tVlanPortEntry     *pVlanPortEntry;
    UINT4               u4Mode;
    INT4                i4RetVal;
    UINT2               u2Port;
    UINT2               u2CurrPort;
    UINT2               u2PriIndex;
    UINT4               u4Port;
    u2CurrPort = 0;

    while (1)
    {
        VLAN_LOCK ();

        if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
        {
            VLAN_UNLOCK ();
            return;
        }

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            VLAN_UNLOCK ();
            break;
        }

        i4RetVal = VlanGetNextPort (u2CurrPort, &u2Port);

        if (i4RetVal != VLAN_SUCCESS)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            break;
        }

        u2CurrPort = u2Port;

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry == NULL)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            continue;
        }

        u4Port = VLAN_GET_IFINDEX (u2Port);

        if (VlanHwSetPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port,
                               pVlanPortEntry->Pvid) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting PVID to %d failed for Port - %d \n",
                           pVlanPortEntry->Pvid, u4Port);
        }

        if (VlanHwSetPortAccFrameType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                       pVlanPortEntry->u1AccpFrmTypes) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Acceptable frame types failed "
                           "for Port %d \n", u4Port);
        }

        if (VlanHwSetPortIngFiltering (VLAN_CURR_CONTEXT_ID (), u2Port,
                                       pVlanPortEntry->u1IngFiltering) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Ingress filtering failed "
                           "for Port %d \n", u4Port);
        }

        if (VlanHwSetDefUserPriority (VLAN_CURR_CONTEXT_ID (), u2Port,
                                      pVlanPortEntry->u1DefUserPriority) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Default User Priority to %d for "
                           "Port %d failed \n",
                           pVlanPortEntry->u1DefUserPriority, u4Port);
        }

        if (VlanHwSetPortNumTrafClasses (VLAN_CURR_CONTEXT_ID (), u2Port,
                                         pVlanPortEntry->u1NumTrfClass) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting number of traffic classes to %d "
                           "for Port %d failed \n",
                           pVlanPortEntry->u1NumTrfClass, u4Port);
        }

        for (u2PriIndex = 0; u2PriIndex < VLAN_MAX_PRIORITY; u2PriIndex++)
        {
            if ((pVlanPortEntry->u1IfType != VLAN_ETHERNET_INTERFACE_TYPE) &&
                (pVlanPortEntry->u1IfType != VLAN_LAGG_INTERFACE_TYPE))
            {
                /* Priority regen table valid only for non-ethernet interfaces.
                 * Since LAGG interfaces are also formed only over ethernet      
                 * interfaces, priority regen table not valid for LAGG interfaces
                 * also. */
                if (VlanHwSetRegenUserPriority (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                (INT4) u2PriIndex,
                                                (INT4) pVlanPortEntry->
                                                au1PriorityRegen[u2PriIndex]) !=
                    VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG3 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "AUDIT Setting Regen user priority to %d for "
                                   "Priority %d on Port %d failed \n",
                                   pVlanPortEntry->au1PriorityRegen[u2PriIndex],
                                   u2PriIndex, u4Port);
                }
            }

            if (VlanHwSetTraffClassMap (VLAN_CURR_CONTEXT_ID (), u2Port,
                                        (INT4) u2PriIndex,
                                        (INT4) pVlanPortEntry->
                                        au1TrfClassMap[u2PriIndex]) !=
                VLAN_SUCCESS)
            {
                VLAN_TRC_ARG3 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "AUDIT Setting traffic class map to %d for "
                               "Priority %d on Port %d failed \n",
                               pVlanPortEntry->au1TrfClassMap[u2PriIndex],
                               u2PriIndex, u4Port);
            }
        }

        if (VlanHwSetMacBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                           VLAN_PORT_MAC_BASED (u2Port)) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting MAC based status on a port failed "
                           "for Port %d \n", u4Port);
        }

        if (VlanHwEnableProtoVlanOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                         VLAN_PORT_PORT_PROTOCOL_BASED (u2Port))
            != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Protocol VLAN based status on a "
                           "port failed for Port %d \n", u4Port);
        }

        if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
        {
            u4Mode = VLAN_NO_TUNNEL_PORT;
        }
        else
        {
            if (VLAN_TUNNEL_STATUS (pVlanPortEntry) == VLAN_ENABLED)
            {
                u4Mode = VLAN_TUNNEL_EXTERNAL;
            }
            else
            {
                /* Bridge is running in Provider network. 
                 * By default tunnelling is disabled
                 * on all ports. */
                u4Mode = VLAN_TUNNEL_INTERNAL;    /* Indicates this port is 
                                                 * connected to another provider 
                                                 * bridge */
            }
        }

        if (VlanHwSetPortTunnelMode (VLAN_CURR_CONTEXT_ID (), u2Port,
                                     u4Mode) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Port tunnel mode on port failed "
                           "for Port %d \n", u4Port);
        }

        if (VlanUpdatePortLearningStatus
            (u2Port, pVlanPortEntry->u2FiltUtilityCriteria) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting filtering utility criteria failed "
                           "for Port %d \n", u4Port);

        }

        VlanReleaseContext ();
        VLAN_UNLOCK ();

        if ((gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE) ||
            (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_PROTO_TABLE_MASK))
        {
            VlanRedSyncPortProtocolParams (u4Context, u2Port);
        }

        VlanPbRedSyncPbPortProperties (u4Context, u2Port);
    }                            /* for each port */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncPortProtocolParams                           */
/*                                                                            */
/*  Description     : This function synchronizes the port protocol parameters */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : u2Port - Port number to be synced.                      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncPortProtocolParams (UINT4 u4Context, UINT2 u2Port)
{
    tVlanPortEntry     *pVlanPortEntry;
    tVlanPortVidSet    *pVlanPortVidSet = NULL;
    tVlanProtGrpEntry  *pGroupEntry;
    INT4                i4RetVal;
    tVlanId             VlanId;
    UINT4               u4Port;
    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Selection failed!! \n ");

        VLAN_UNLOCK ();
        return;
    }

    VlanHwScanProtocolVlanTbl (VLAN_CURR_CONTEXT_ID (), u2Port,
                               VlanRedProtocolVlanCb);

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Vlan Port Entry is Null!! \n ");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    /* Now scan all the entries in the software and add those
     * entries which are not present in hardware */
    VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSet,
                   tVlanPortVidSet *)
    {
        if (pVlanPortVidSet->u1RowStatus == VLAN_ACTIVE)
        {
            /* Get group entry and get it from h/w */
            pGroupEntry =
                VlanGetActiveProtoGrpEntry (pVlanPortVidSet->u4ProtGrpId);

            if (pGroupEntry != NULL)
            {
                /* GET Entry from H/W */
                i4RetVal =
                    VlanHwGetVlanProtocolMap (VLAN_CURR_CONTEXT_ID (),
                                              u2Port,
                                              pGroupEntry->u4ProtGrpId,
                                              &pGroupEntry->
                                              VlanProtoTemplate, &VlanId);

                /* 
                 * NO need to check whether VLAN id in the software is
                 * same as in the hardware since we had already checked that
                 * in VlanRedProtocolVlanCb
                 */
                if (i4RetVal != VLAN_SUCCESS)
                {
                    u4Port = VLAN_GET_IFINDEX (u2Port);
                    VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                                   "AUDIT Protocol VLAN Mapping for port %d "
                                   "and VLAN %d not present in Hardware \n",
                                   u4Port, pVlanPortVidSet->VlanId);

                    /* Add protocol vlan entry to the hardware */
                    VlanHwAddVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2Port,
                                              pGroupEntry->u4ProtGrpId,
                                              &pGroupEntry->VlanProtoTemplate,
                                              pVlanPortVidSet->VlanId);
                }                /* (i4RetVal != VLAN_SUCCESS) */
            }                    /* if (pGroupEntry != NULL) */
        }                        /* VID Set is active */
    }                            /* for each vid set in the software */

    VlanReleaseContext ();
    VLAN_UNLOCK ();
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedProtocolVlanCb                                   */
/*                                                                            */
/*  Description     : This function will be invoked for each protocol VLAN    */
/*                    entry that is present in the hardware. This function    */
/*                    checks whether the entry is present in the software.    */
/*                    If not present, this fn. will delete the entry from the */
/*                    hardware.                                               */
/*                                                                            */
/*  Input(s)        : u4IfIndex - Port number on which the entry is present in*/
/*                    the hardware.                                           */
/*                    u4HwGroupId - Group Id in hardware.                     */
/*                    pProtoTemplate - Protocol template of the entry.        */
/*                    VlanId - VLAN Id in the hardware.                       */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedProtocolVlanCb (UINT4 u4IfIndex, UINT4 u4HwGroupId,
                       tVlanProtoTemplate * pProtoTemplate, tVlanId VlanId)
{
    tVlanProtoTemplate  ProtoTemplate;
    tVlanPortVidSet    *pVidSet;
    UINT4               u4GroupId;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    INT4                i4RetVal;

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        return;
    }

    /* Get the local port from the physical index */
    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPort) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC,
                         "Failed to get local port from physical index \n ");

        return;
    }

    /* 
     * Check whether the entry is present in the software.
     * If present check the parameters.
     * Else delete the entry from the hardware.
     */
    MEMSET (&ProtoTemplate, 0, sizeof (tVlanProtoTemplate));
    ProtoTemplate.u1Length = pProtoTemplate->u1Length;
    MEMCPY (ProtoTemplate.au1ProtoValue, pProtoTemplate->au1ProtoValue,
            ProtoTemplate.u1Length);
    ProtoTemplate.u1TemplateProtoFrameType =
        pProtoTemplate->u1TemplateProtoFrameType;

    i4RetVal = VlanGetGroupIdForProtoTemplate (ProtoTemplate, &u4GroupId);

    if (i4RetVal != VLAN_SUCCESS)
    {
        /* Entry not present in software */
        VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT Deleting Protocol VLAN Mapping for port %d "
                       "and VLAN %d since no group entry is present. \n",
                       u4IfIndex, VlanId);

        VlanHwDelVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2LocalPort,
                                  u4HwGroupId, &ProtoTemplate);

        return;
    }

    pVidSet = VlanGetPortProtoVidSetEntry (u2LocalPort, u4GroupId);

    if (pVidSet == NULL)
    {
        /* Entry not present in software */
        VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT Deleting Protocol VLAN Mapping for port %d "
                       "and VLAN %d since no VID set is present. \n",
                       u4IfIndex, VlanId);

        VlanHwDelVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2LocalPort,
                                  u4HwGroupId, &ProtoTemplate);
        return;
    }

    if (pVidSet->u1RowStatus != VLAN_ACTIVE)
    {
        /* Entry present but not active. delete the entry from the h/w */
        VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT Deleting Protocol VLAN Mapping for port %d "
                       "and VLAN %d since the VID set is not active. \n",
                       u4IfIndex, VlanId);

        VlanHwDelVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2LocalPort,
                                  u4HwGroupId, &ProtoTemplate);
        return;
    }

    if (pVidSet->VlanId != VlanId)
    {
        VLAN_TRC_ARG3 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT Changing Protocol VLAN Mapping for port %d "
                       "and VLAN %d to VLAN %d in HW \n",
                       u4IfIndex, VlanId, pVidSet->VlanId);
        /* Vlan id for the protocol group is different.
         * modify */
        VlanHwDelVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2LocalPort,
                                  u4HwGroupId, &ProtoTemplate);
        VlanHwAddVlanProtocolMap (VLAN_CURR_CONTEXT_ID (), u2LocalPort,
                                  u4GroupId, &ProtoTemplate, pVidSet->VlanId);
        return;
    }

    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncPendingMcastEntries                          */
/*                                                                            */
/*  Description     : This function synchronizes the unicast table            */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncPendingMcastEntries (UINT4 u4Context)
{
    tVlanRedMcastCacheNode *pVlanRedMcastCache = NULL;
    tVlanRedMcastCacheNode VlanRedMcastCache;

    MEMSET (&VlanRedMcastCache, 0, sizeof (tVlanRedMcastCacheNode));

    /* The full audit flag is set on for VLAN Multicast table. 
     * Hence perform complete audit for VLAN Multicast Table. 
     */
    if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_MCAST_TABLE_MASK)
    {
        VlanRedSyncMcastTable (u4Context);
        gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_MCAST_TABLE_MASK;
        return;
    }

    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Switching failed \n ");

        VLAN_UNLOCK ();
        return;
    }

    VlanRedMcastCache.u4ContextId = u4Context;
    while ((NULL !=
            (pVlanRedMcastCache =
             RBTreeGetNext (gVlanRedInfo.VlanRedMcastCacheTable,
                            &VlanRedMcastCache, NULL))))
    {
        if (pVlanRedMcastCache->u4ContextId != u4Context)
        {
            break;
        }
        VlanRedSyncMcastEntry (pVlanRedMcastCache->u4ContextId,
                               pVlanRedMcastCache->VlanId,
                               pVlanRedMcastCache->MacAddr,
                               pVlanRedMcastCache->u4RcvPort);

        VlanRedDelMcastCacheEntry (pVlanRedMcastCache->u4ContextId,
                                   pVlanRedMcastCache->MacAddr,
                                   pVlanRedMcastCache->VlanId,
                                   pVlanRedMcastCache->u4RcvPort);

    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncPendingFdbs                                  */
/*                                                                            */
/*  Description     : This function synchronizes the unicast table            */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncPendingFdbs (UINT4 u4Context)
{
    tVlanRedStUcastCacheNode *pVlanRedStUcastCache = NULL;
    tVlanRedStUcastCacheNode VlanRedStUcastCache;

    if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_UCAST_TABLE_MASK)
    {
        /* The full audit flag is set on for VLAN FDB table. 
         * Hence perform complete audit for VLAN FDB Table. 
         */
        VlanRedSyncFdb (u4Context);
        gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_UCAST_TABLE_MASK;
        return;
    }

    MEMSET (&VlanRedStUcastCache, 0, sizeof (tVlanRedMcastCacheNode));

    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Switching failed \n ");

        VLAN_UNLOCK ();
        return;
    }

    VlanRedStUcastCache.u4ContextId = u4Context;
    while ((NULL !=
            (pVlanRedStUcastCache =
             RBTreeGetNext (gVlanRedInfo.VlanRedStUcastCacheTable,
                            &VlanRedStUcastCache, NULL))))
    {
        if (pVlanRedStUcastCache->u4ContextId != u4Context)
        {
            break;
        }
        VlanRedSyncFdbs (pVlanRedStUcastCache->u4ContextId,
                         pVlanRedStUcastCache->u4FdbId,
                         pVlanRedStUcastCache->MacAddr,
                         pVlanRedStUcastCache->u4RcvPort);

        VlanRedDelStUcastCacheEntry (pVlanRedStUcastCache->u4ContextId,
                                     pVlanRedStUcastCache->MacAddr,
                                     pVlanRedStUcastCache->u4FdbId,
                                     pVlanRedStUcastCache->u4RcvPort);

    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncFdbs                                         */
/*                                                                            */
/*  Description     : This function synchronizes the unicast table            */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/

VOID
VlanRedSyncFdbs (UINT4 u4ContextId,
                 UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort)
{
    tLocalPortList      AllowedToGo;
    tMacAddr            ConnectionId;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4FidIndex = 0;
    UINT2               u2LocalPortId = 0;
    UINT1               u1Status = 0;

    MEMSET (ConnectionId, 0, sizeof (tMacAddr));
    MEMSET (AllowedToGo, 0, sizeof (tLocalPortList));

    if (u4RcvPort != 0)
    {
        if (VLAN_FAILURE == VlanGetContextInfoFromIfIndex (u4RcvPort,
                                                           &u4CurrContextId,
                                                           &u2LocalPortId))
        {
            return;
        }
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);
    if (u4FidIndex != VLAN_INVALID_FID_INDEX)
    {
        pStUcastEntry =
            VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex,
                                                     MacAddr, u2LocalPortId);
    }
    if (VLAN_SUCCESS ==
        VlanHwGetStaticUcastEntry (u4ContextId, u4FdbId, MacAddr,
                                   u2LocalPortId, AllowedToGo, &u1Status,
                                   ConnectionId))
    {

        /* Case 1 : Entry present in hardware and missing in software.
         *          Action - Delete from hardware
         */
        if (pStUcastEntry == NULL)
        {
            VlanHwDelStaticUcastEntry (u4FdbId, MacAddr, u2LocalPortId);
        }
        else
        {
            /* Case 2 : Entry present in both h/w and s/w, 
             *          But with PortList mismatch. 
             *          Action - Re-Program the port list in hardware, 
             *                   to remain in sync with software. 
             */
            if ((MEMCMP (AllowedToGo,
                         pStUcastEntry->AllowedToGo,
                         sizeof (tLocalPortList)) != 0) ||
                (u1Status != pStUcastEntry->u1Status))
            {
                /* Allowed bitmap difference. Overwrite new one */
                VlanHwAddStaticUcastEntry (u4FdbId,
                                           MacAddr, u2LocalPortId,
                                           pStUcastEntry->AllowedToGo,
                                           pStUcastEntry->u1Status,
                                           pStUcastEntry->ConnectionId);
            }
        }
    }
    else                        /* Entry present in software and not present in hardware */
    {
        /* Case 3:  Entry present in software and not present in hardware.
         *          Action -> Add the entry to hardware*/

        if (pStUcastEntry != NULL)
        {
            VlanHwAddStaticUcastEntry (u4FdbId,
                                       pStUcastEntry->MacAddr,
                                       pStUcastEntry->u2RcvPort,
                                       pStUcastEntry->AllowedToGo,
                                       pStUcastEntry->u1Status,
                                       pStUcastEntry->ConnectionId);
        }
        /* Case 4 : Entry present not in h/w or s/w.
         *          Action -> do nothing. 
         */
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncMcastTable                                   */
/*                                                                            */
/*  Description     : This function synchronizes the multicast entries        */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : u4Context - Virtual ContextId                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncMcastTable (UINT4 u4Context)
{
    tVlanCurrEntry     *pVlanEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanGroupEntry    *pGroupEntry;
    tLocalPortList      HwPortList;
    tLocalPortList      TempPortList;
    INT4                i4RetVal;
    UINT4               u4VlanId;
    tVlanId             CurrVlanId;
#if defined (IGS_WANTED) || defined (MLDS_WANTED)    /* SNOOP_APPROACH */
    tVlanGroupEntry    *pNextGroupEntry;
    tLocalPortList      SwPortList;
    UINT1               u1Result;
#endif /* IGS_WANTED */
    UINT4               u4Port;
    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) == VLAN_FAILURE)
    {

        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Selection Failed \n ");

        VLAN_UNLOCK ();
        return;
    }
    /* Scan all the entries in the hardware and delete those
     * entries which are not present in the software */
    VlanHwScanMulticastTbl (VLAN_CURR_CONTEXT_ID (), VlanWrRedVlanMcastCb);

    VlanReleaseContext ();

    VLAN_UNLOCK ();

    /* Scan software group table */
    CurrVlanId = 0;

    while (1)
    {
        VLAN_LOCK ();

        if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
        {
            VLAN_UNLOCK ();
            return;
        }

        if (VlanSelectContext (u4Context) == VLAN_FAILURE)
        {
            VLAN_UNLOCK ();
            break;
        }

        i4RetVal = VlanGetNextVlanId (CurrVlanId, &u4VlanId);

        CurrVlanId = (tVlanId) u4VlanId;

        if (i4RetVal != SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            break;
        }

        pVlanEntry = VlanGetVlanEntry (u4VlanId);

        if (pVlanEntry == NULL)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            continue;
        }

        pStMcastEntry = pVlanEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            /* Static mcast with receive port == 0 will be audited thru 
             * group entries */
            if (pStMcastEntry->u2RcvPort != 0)
            {
                MEMSET (HwPortList, 0, sizeof (tLocalPortList));

                i4RetVal = VlanHwGetStMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                                  pVlanEntry->VlanId,
                                                  pStMcastEntry->MacAddr,
                                                  pStMcastEntry->u2RcvPort,
                                                  HwPortList);
                u4Port = VLAN_GET_IFINDEX (pStMcastEntry->u2RcvPort);
                if (i4RetVal != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                                   "AUDIT : Adding static mcast that is not "
                                   "present in HW - Recv Port (%d), VLAN (%d) "
                                   "Mcast Addr - ", u4Port, pVlanEntry->VlanId);
                    VlanPrintMacAddr (VLAN_RED_TRC, pStMcastEntry->MacAddr);

                    VlanHwAddStMcastEntry (pVlanEntry->VlanId,
                                           pStMcastEntry->MacAddr,
                                           pStMcastEntry->u2RcvPort,
                                           pStMcastEntry->EgressPorts);
                }
                else
                {
                    /* Entry present in h/w. Compare port list */
                    if (MEMCMP (pStMcastEntry->EgressPorts, HwPortList,
                                sizeof (tLocalPortList)) != 0)
                    {
                        VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                                       VLAN_NAME, "AUDIT : Overwriting st. "
                                       "egress ports to HW - Receive Port (%d), "
                                       "VLAN (%d) Mcast Address - ",
                                       u4Port, pVlanEntry->VlanId);

                        VlanPrintMacAddr (VLAN_RED_TRC, pStMcastEntry->MacAddr);
                        /* Overwrite static mcast membership
                         * in hardware. */
                        VlanHwAddStMcastEntry (pVlanEntry->VlanId,
                                               pStMcastEntry->MacAddr,
                                               pStMcastEntry->u2RcvPort,
                                               pStMcastEntry->EgressPorts);
                    }
                }
            }                    /* if u2RcvPort != 0 */
            else
            {
                /* dynamic info can be there */
                pGroupEntry = pStMcastEntry->pGroupEntry;

                MEMSET (HwPortList, 0, sizeof (tLocalPortList));
                MEMSET (TempPortList, 0, sizeof (tLocalPortList));

                VLAN_ADD_PORT_LIST (TempPortList, pStMcastEntry->EgressPorts);

                VLAN_ADD_PORT_LIST (TempPortList, pGroupEntry->LearntPorts);

                i4RetVal =
                    VlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                         pVlanEntry->VlanId,
                                         pGroupEntry->MacAddr, HwPortList);

                if (i4RetVal != VLAN_SUCCESS)
                {
                    /* Entry not present in the hardware */
                    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                                   VLAN_NAME, "AUDIT : Adding entry to HW - "
                                   "VLAN (%d) Mcast Address - ",
                                   pVlanEntry->VlanId);
                    VlanPrintMacAddr (VLAN_RED_TRC, pGroupEntry->MacAddr);

                    /* Entry not present in hardware */
#if (defined IGS_WANTED || defined MLDS_WANTED)    /* SNOOP_APPROACH */
                    if ((VlanSnoopIsIgmpSnoopingEnabled (u4Context)
                         == SNOOP_ENABLED) ||
                        (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
                    {
                        /* 
                         * Add static mcast entry to hardware. Inform IGS 
                         * and reset learnt ports to all 0's.
                         */
                        VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             pVlanEntry->VlanId,
                                             pStMcastEntry->
                                             MacAddr,
                                             pStMcastEntry->EgressPorts);

                        /* Post the message to IGS */

                        /* if the static mcast entry present only in SW,
                         * then create entry in HW and reset the
                         * Learnt port list as 0's in SW Group Tables.
                         */

                        SnoopRedMcastAuditSyncMsg
                            (u4Context,
                             SNOOP_RED_MCAST_PORTS_ONLY_IN_SW,
                             pVlanEntry->VlanId, pGroupEntry->MacAddr,
                             pGroupEntry->LearntPorts);

                        MEMSET (pGroupEntry->LearntPorts, 0,
                                sizeof (tLocalPortList));
                    }
                    else
#endif /* IGS_WANTED */
                    {
                        /* 
                         * Add the mcast to hardware if IGS is disabled.
                         * In case of GMRP, s/w db is superset.
                         */
                        VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             pVlanEntry->VlanId,
                                             pGroupEntry->MacAddr,
                                             TempPortList);
                    }
                }
                else
                {
                    /* Entry present in h/w. Compare port list */
                    if (MEMCMP (TempPortList, HwPortList,
                                sizeof (tLocalPortList)) != 0)
                    {
#if (defined IGS_WANTED || defined MLDS_WANTED)
                        if ((VlanSnoopIsIgmpSnoopingEnabled (u4Context)
                             == SNOOP_ENABLED) ||
                            (VlanSnoopIsMldSnoopingEnabled (0) ==
                             SNOOP_ENABLED))
                        {
                            VLAN_IS_PORT_LIST_A_SUBSET (HwPortList,
                                                        pStMcastEntry->
                                                        EgressPorts, u1Result);

                            if (u1Result == VLAN_FALSE)
                            {
                                /* Static ports are not present in h/w */
                                VLAN_ADD_PORT_LIST (HwPortList,
                                                    pStMcastEntry->EgressPorts);

                                /* add st egress ports + old hw port list to 
                                 * hardware */
                                VlanHwAddMcastEntry
                                    (VLAN_CURR_CONTEXT_ID (),
                                     pVlanEntry->VlanId, pGroupEntry->MacAddr,
                                     HwPortList);

                            }

                            /* need to check if the port lists are equal now */
                            if (MEMCMP (TempPortList, HwPortList,
                                        sizeof (tLocalPortList)) != 0)
                            {
                                /* all static ports present in h/w */
                                MEMCPY (SwPortList, TempPortList,
                                        sizeof (tLocalPortList));
                                VLAN_RESET_PORT_LIST (SwPortList, HwPortList);
                                /* Now SwPortList contains ports that are 
                                 * present only in S/W and not in H/w */
                                VLAN_RESET_PORT_LIST (HwPortList, TempPortList);
                                /* Now HwPortList contains ports thar are
                                 * present only in H/W and not in S/w */
                                SnoopRedMcastAuditSyncMsg
                                    (u4Context,
                                     SNOOP_RED_MCAST_PORTS_ONLY_IN_SW,
                                     pVlanEntry->VlanId, pGroupEntry->MacAddr,
                                     SwPortList);

                                VLAN_RESET_PORT_LIST (pGroupEntry->LearntPorts,
                                                      SwPortList);

                                SnoopRedMcastAuditSyncMsg
                                    (u4Context,
                                     SNOOP_RED_MCAST_PORTS_ONLY_IN_HW,
                                     pVlanEntry->VlanId, pGroupEntry->MacAddr,
                                     HwPortList);
                            }
                        }
                        else
#endif /* IGS_WANTED */
                        {
                            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                                           VLAN_NAME,
                                           "AUDIT : Overwriting portlist to "
                                           "Hw - VLAN (%d) Mcast Address - ",
                                           pVlanEntry->VlanId);

                            VlanPrintMacAddr (VLAN_RED_TRC,
                                              pGroupEntry->MacAddr);

                            VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                                 pVlanEntry->VlanId,
                                                 pGroupEntry->MacAddr,
                                                 TempPortList);
                        }
                    }
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }                        /* while pStMcastEntry != NULL */

        pGroupEntry = pVlanEntry->pGroupTable;

        while (pGroupEntry != NULL)
        {
            if (pGroupEntry->u1StRefCount == 0)
            {
                /* do audit only for the dynamic entries */
                MEMSET (HwPortList, 0, sizeof (tLocalPortList));

                i4RetVal =
                    VlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                         pVlanEntry->VlanId,
                                         pGroupEntry->MacAddr, HwPortList);

                if (i4RetVal != VLAN_SUCCESS)
                {
                    /* Entry not present in hardware */
#if (defined IGS_WANTED || defined MLDS_WANTED)
                    if ((VlanSnoopIsIgmpSnoopingEnabled (u4Context)
                         == SNOOP_ENABLED) ||
                        (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
                    {
                        SnoopRedMcastAuditSyncMsg
                            (u4Context,
                             SNOOP_RED_MCAST_ENTRY_ONLY_IN_SW,
                             pVlanEntry->VlanId, pGroupEntry->MacAddr,
                             HwPortList);

                        pNextGroupEntry = pGroupEntry->pNextNode;

                        VlanDeleteGroupEntry (pVlanEntry, pGroupEntry);

                        VlanPbUpdateCurrentMulticastMacCount (VLAN_DELETE);

                        pGroupEntry = pNextGroupEntry;

                        continue;
                    }
                    else
#endif /* IGS_WANTED */
                    {
                        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                                       VLAN_NAME, "AUDIT : Adding entry to "
                                       "H/W - VLAN (%d) Mcast Address - ",
                                       pVlanEntry->VlanId);
                        VlanPrintMacAddr (VLAN_RED_TRC, pGroupEntry->MacAddr);

                        VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             pVlanEntry->VlanId,
                                             pGroupEntry->MacAddr,
                                             pGroupEntry->LearntPorts);
                    }
                }
                else
                {
                    if (MEMCMP (pGroupEntry->LearntPorts, HwPortList,
                                sizeof (tLocalPortList)) != 0)
                    {
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
                        if ((VlanSnoopIsIgmpSnoopingEnabled (u4Context)
                             == SNOOP_ENABLED) ||
                            (VlanSnoopIsMldSnoopingEnabled (0) ==
                             SNOOP_ENABLED))
                        {
                            MEMCPY (SwPortList, pGroupEntry->LearntPorts,
                                    sizeof (tLocalPortList));
                            VLAN_RESET_PORT_LIST (SwPortList, HwPortList);
                            /* Now SwPortList holds the extra ports that are
                             * present in software and not in hardware */
                            VLAN_RESET_PORT_LIST (HwPortList,
                                                  pGroupEntry->LearntPorts);
                            /* Now HwPortList holds the extra ports in 
                             * hardware that are not present in software */

                            SnoopRedMcastAuditSyncMsg
                                (u4Context,
                                 SNOOP_RED_MCAST_PORTS_ONLY_IN_SW,
                                 pVlanEntry->VlanId, pGroupEntry->MacAddr,
                                 SwPortList);

                            VLAN_RESET_PORT_LIST (pGroupEntry->LearntPorts,
                                                  SwPortList);

                            SnoopRedMcastAuditSyncMsg
                                (u4Context,
                                 SNOOP_RED_MCAST_PORTS_ONLY_IN_HW,
                                 pVlanEntry->VlanId, pGroupEntry->MacAddr,
                                 HwPortList);
                        }
                        else
#endif /* IGS_WANTED */
                        {
                            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC,
                                           VLAN_NAME,
                                           "AUDIT : Overwriting portlist to "
                                           "H/W - VLAN (%d) Mcast Address - ",
                                           pVlanEntry->VlanId);

                            VlanPrintMacAddr (VLAN_RED_TRC,
                                              pGroupEntry->MacAddr);

                            VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                                 pVlanEntry->VlanId,
                                                 pGroupEntry->MacAddr,
                                                 pGroupEntry->LearntPorts);
                        }
                    }
                }
            }

            pGroupEntry = pGroupEntry->pNextNode;
        }                        /* While pGroupEntry != NULL */
        VlanReleaseContext ();
        VLAN_UNLOCK ();
    }                            /* for each VLAN */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncDefaultVlanId                                */
/*                                                                            */
/*  Description     : This function synchronizes the default vlan id          */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncDefaultVlanId (VOID)
{
    /* If default Vlan Id is different in sw and in hw,
     * set the proper default vlan id in hw. 
     * Scenario to simulate this difference: 
     *  - Both standby and active nodes are coming up together.
     *  - In Active node, before VLAN could program default vlan-id in 
     *    hardware active node crashes. */

    VlanHwSyncDefaultVlanId (VLAN_CURR_CONTEXT_ID (), VLAN_DEF_VLAN_ID);

    /* VlanRedSyncVlanTable will sync up the member ports for these
     * affected vlans. */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedVlanMcastCb                                      */
/*                                                                            */
/*  Description     : This function will be invoked for each multicast entry  */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same mcast entry in the software. If        */
/*                    present this fn. corrects the mcast membership. If not  */
/*                    present this fn. intimates GARP to add the mcast entry  */
/*                    to its software database.                               */
/*                                                                            */
/*  Input(s)        : VlanId - VLAN Id of the group                           */
/*                    McastAddr - Multicast address                           */
/*                    u2RcvPort - Receive port parameter                      */
/*                    HwPortList - Mcast egress ports in hardware.            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedVlanMcastCb (tVlanId VlanId, tMacAddr McastAddr, UINT2 u2RcvPort,
                    tLocalPortList HwPortList)
{
    tVlanCurrEntry     *pVlanEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tLocalPortList      TempPortList;
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    tLocalPortList      SwPortList;
    UINT1               u1Result;
#endif /* IGS_WANTED */

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        return;
    }
    if (VlanId == VLAN_WILD_CARD_ID)
    {
        VlanRedWildCardCb (McastAddr);
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "AUDIT : Deleting multicast entry - VLAN (%d) "
                       "Mcast Address - ", VlanId);
        VlanPrintMacAddr (VLAN_RED_TRC, McastAddr);

        /* Delete the mcast entry */
        if (u2RcvPort == 0)
        {
            VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, McastAddr);
        }
        else
        {
            VlanHwDelStMcastEntry (VlanId, McastAddr, u2RcvPort);
        }
        return;
    }

    if (u2RcvPort == 0)
    {
        pGroupEntry = VlanGetGroupEntry (McastAddr, pVlanEntry);

        if (pGroupEntry == NULL)
        {
            /* Group entry not present in s/w */
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
            if ((VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                 == SNOOP_ENABLED) ||
                (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
            {
                /* Post the message to IGS */
                SnoopRedMcastAuditSyncMsg (VLAN_CURR_CONTEXT_ID (),
                                           SNOOP_RED_MCAST_ENTRY_ONLY_IN_HW,
                                           VlanId, McastAddr, HwPortList);
            }
            else
#endif /* IGS_WANTED */
            {
                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : Deleting extra multicast entry from "
                               "HW - VLAN (%d) Mcast Address - ", VlanId);
                VlanPrintMacAddr (VLAN_RED_TRC, McastAddr);

                /* GMRP is disabled and the entry is not present in the 
                 * s/w. Delete the entry from the hardware */
                VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                     McastAddr);
            }
            return;
        }

        MEMSET (TempPortList, 0, sizeof (tLocalPortList));

        VLAN_ADD_PORT_LIST (TempPortList, pGroupEntry->LearntPorts);

        if (pGroupEntry->u1StRefCount != 0)
        {
            pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort (McastAddr,
                                                                 u2RcvPort,
                                                                 pVlanEntry);

            if (pStMcastEntry != NULL)
            {
                VLAN_ADD_PORT_LIST (TempPortList, pStMcastEntry->EgressPorts);
            }
        }

        if (MEMCMP (TempPortList, HwPortList, sizeof (tLocalPortList)) != 0)
        {
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
            if ((VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                 == SNOOP_ENABLED) ||
                (VlanSnoopIsMldSnoopingEnabled (0) == SNOOP_ENABLED))
            {
                if (pStMcastEntry != NULL)
                {
                    VLAN_IS_PORT_LIST_A_SUBSET (HwPortList,
                                                pStMcastEntry->EgressPorts,
                                                u1Result);

                    if (u1Result != VLAN_TRUE)
                    {
                        /* Static ports not present in hardware */
                        VLAN_ADD_PORT_LIST (HwPortList,
                                            pStMcastEntry->EgressPorts);

                        VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             VlanId, McastAddr, HwPortList);
                    }
                }

                if (MEMCMP (TempPortList, HwPortList, sizeof (tLocalPortList))
                    != 0)
                {
                    /* dynamic ports differ */
                    MEMCPY (SwPortList, TempPortList, sizeof (tLocalPortList));
                    VLAN_RESET_PORT_LIST (SwPortList, HwPortList);
                    /* Now SwPortList contains ports that are 
                     * present only in S/W and not in H/w */
                    VLAN_RESET_PORT_LIST (HwPortList, TempPortList);
                    /* Now HwPortList contains ports thar are
                     * present only in H/W and not in S/w */
                    SnoopRedMcastAuditSyncMsg (VLAN_CURR_CONTEXT_ID (),
                                               SNOOP_RED_MCAST_PORTS_ONLY_IN_SW,
                                               pVlanEntry->VlanId,
                                               pGroupEntry->MacAddr,
                                               SwPortList);
                    VLAN_RESET_PORT_LIST (pGroupEntry->LearntPorts, SwPortList);

                    SnoopRedMcastAuditSyncMsg (VLAN_CURR_CONTEXT_ID (),
                                               SNOOP_RED_MCAST_PORTS_ONLY_IN_HW,
                                               pVlanEntry->VlanId,
                                               pGroupEntry->MacAddr,
                                               HwPortList);
                }
            }
            else
#endif /* IGS_WANTED */
            {
                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : Overwriting member ports for the MCAST "
                               "Entry - VLAN (%d) Mcast Address - ", VlanId);
                VlanPrintMacAddr (VLAN_RED_TRC, McastAddr);
                /* 
                 * VlanHwAddMcastEntry is supposed to overwrite the
                 * multicast membership in the hardware without affecting the 
                 * data traffic.
                 */
                VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                     McastAddr, TempPortList);
            }
        }
    }
    else
    {
        pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort (McastAddr,
                                                             u2RcvPort,
                                                             pVlanEntry);

        if (pStMcastEntry == NULL)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "AUDIT : Deleting the static mcast entry from "
                           "the HW - VLAN (%d) Mcast Address - ", VlanId);
            VlanPrintMacAddr (VLAN_RED_TRC, McastAddr);

            /* St. mcast not present in software */
            VlanHwDelStMcastEntry (VlanId, McastAddr, u2RcvPort);
            return;
        }

        if (MEMCMP (pStMcastEntry->EgressPorts, HwPortList,
                    sizeof (tLocalPortList)) != 0)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "AUDIT : Overwriting static member ports for the "
                           "Entry - VLAN (%d) Mcast Address - ", VlanId);
            VlanPrintMacAddr (VLAN_RED_TRC, McastAddr);

            /* Overwrite membership in h/w */
            VlanHwAddStMcastEntry (VlanId, McastAddr, u2RcvPort,
                                   pStMcastEntry->EgressPorts);
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedWildCardCb                                       */
/*                                                                            */
/*  Description     : This function will be invoked for each multicast entry  */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same wildcard entry in the software.        */
/*                    if entry not present in software delete it from hardware*/
/*                    also.                                                   */
/*                                                                            */
/*  Input             McastAddr - Multicast address                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedWildCardCb (tMacAddr MacAddr)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;

    pWildCardEntry = VlanGetWildCardEntry (MacAddr);

    if (pWildCardEntry == NULL)
    {
        if (VLAN_IS_MCASTADDR (pWildCardEntry->MacAddr) == VLAN_TRUE)
        {
            VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_WILD_CARD_ID, MacAddr);
        }
        else
        {
            VlanHwDelStaticUcastEntry (VLAN_WILD_CARD_ID, MacAddr,
                                       VLAN_DEF_RECVPORT);
        }
    }

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncFdbs                                         */
/*                                                                            */
/*  Description     : This function synchronizes the unicast table            */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncFdb (UINT4 u4Context)
{
    UINT4               u4FidIndex;
    tVlanFidEntry      *pFidEntry;
    tVlanStUcastEntry  *pStUcastEntry;
    tMacAddr            ConnectionId;
    INT4                i4RetVal;
    tLocalPortList      AllowedToGo;
    UINT1               u1Status;
    UINT4               u4Port;
    MEMSET (ConnectionId, 0, sizeof (tMacAddr));
    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Switching failed \n ");

        VLAN_UNLOCK ();
        return;
    }
    VlanHwScanUnicastTbl (VLAN_CURR_CONTEXT_ID (), VlanWrRedVlanUcastCb);

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    /* Scan all static unicast entries in the software */
    for (u4FidIndex = 0; u4FidIndex < VLAN_MAX_FID_ENTRIES; u4FidIndex++)
    {
        VLAN_LOCK ();

        if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
        {
            VLAN_UNLOCK ();
            return;
        }

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            u4FidIndex = VLAN_MAX_FID_ENTRIES;
            VLAN_UNLOCK ();
            break;
        }

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {
            for (pStUcastEntry = pFidEntry->pStUcastTbl;
                 pStUcastEntry; pStUcastEntry = pStUcastEntry->pNextNode)
            {
                if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
                {
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return;
                }

                MEMSET (AllowedToGo, 0, sizeof (tLocalPortList));

                i4RetVal =
                    VlanHwGetStaticUcastEntry (VLAN_CURR_CONTEXT_ID (),
                                               pFidEntry->u4Fid,
                                               pStUcastEntry->MacAddr,
                                               pStUcastEntry->u2RcvPort,
                                               AllowedToGo, &u1Status,
                                               ConnectionId);
                if (i4RetVal != VLAN_SUCCESS)
                {
                    u4Port = VLAN_GET_IFINDEX (pStUcastEntry->u2RcvPort);
                    VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                                   "AUDIT : Adding the static Ucast entry from "
                                   "the HW - FDB (%d) Recv Port (%d) Ucast Address - ",
                                   pFidEntry->u4Fid, u4Port);
                    VlanPrintMacAddr (VLAN_RED_TRC, pStUcastEntry->MacAddr);

                    /* Static unicast not present in hardware */
                    VlanHwAddStaticUcastEntry (pFidEntry->u4Fid,
                                               pStUcastEntry->MacAddr,
                                               pStUcastEntry->u2RcvPort,
                                               pStUcastEntry->AllowedToGo,
                                               pStUcastEntry->u1Status,
                                               pStUcastEntry->ConnectionId);
                }
                else
                {
                    /* Entry present in h/w and s/w */
                    if ((MEMCMP (AllowedToGo, pStUcastEntry->AllowedToGo,
                                 sizeof (tLocalPortList)) != 0) ||
                        (pStUcastEntry->u1Status != u1Status))
                    {
                        /* Overwrite the entry in the hardware */
                        VlanHwAddStaticUcastEntry (pFidEntry->u4Fid,
                                                   pStUcastEntry->MacAddr,
                                                   pStUcastEntry->u2RcvPort,
                                                   pStUcastEntry->AllowedToGo,
                                                   pStUcastEntry->u1Status,
                                                   pStUcastEntry->ConnectionId);
                    }
                }
            }                    /* for (pStUcastEntry) */
        }                        /* valid FID entry */
        VlanReleaseContext ();
        VLAN_UNLOCK ();
    }                            /* for each FID entry */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncWildCardTbl                                  */
/*                                                                            */
/*  Description     : This function performs audit of the VLAN WildCard table */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncWildCardTbl (UINT4 u4Context)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tMacAddr            ConnectionId;
    tLocalPortList      HwPortList;
    INT4                i4RetVal;
    UINT1               u1Status;

    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Context Switching failed \n ");

        VLAN_UNLOCK ();
        return;
    }

    VLAN_SLL_SCAN (&gpVlanContextInfo->WildCardTable, pWildCardEntry,
                   tVlanWildCardEntry *)
    {
        MEMSET (HwPortList, 0, sizeof (tLocalPortList));
        MEMSET (ConnectionId, 0, sizeof (tMacAddr));

        if (VLAN_IS_MCASTADDR (pWildCardEntry->MacAddr) == VLAN_TRUE)
        {
            i4RetVal = VlanHwGetStMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                              VLAN_WILD_CARD_ID,
                                              pWildCardEntry->MacAddr,
                                              VLAN_DEF_RECVPORT, HwPortList);

            if (i4RetVal == VLAN_SUCCESS)
            {
                if ((MEMCMP (HwPortList, pWildCardEntry->EgressPorts,
                             sizeof (tLocalPortList)) != 0))
                {
                    VlanHwAddStMcastEntry (VLAN_WILD_CARD_ID,
                                           pWildCardEntry->MacAddr,
                                           VLAN_DEF_RECVPORT,
                                           pWildCardEntry->EgressPorts);
                }
            }
            else
            {
                VlanHwAddStMcastEntry (VLAN_WILD_CARD_ID,
                                       pWildCardEntry->MacAddr,
                                       VLAN_DEF_RECVPORT,
                                       pWildCardEntry->EgressPorts);
            }
        }
        else
        {
            i4RetVal =
                VlanHwGetStaticUcastEntry (VLAN_CURR_CONTEXT_ID (),
                                           VLAN_WILD_CARD_ID,
                                           pWildCardEntry->MacAddr,
                                           VLAN_DEF_RECVPORT,
                                           HwPortList, &u1Status, ConnectionId);

            if (i4RetVal == VLAN_SUCCESS)
            {
                if ((MEMCMP (HwPortList, pWildCardEntry->EgressPorts,
                             sizeof (tLocalPortList)) != 0))
                {
                    VlanHwAddStaticUcastEntry
                        (VLAN_WILD_CARD_ID, pWildCardEntry->MacAddr,
                         VLAN_DEF_RECVPORT, pWildCardEntry->EgressPorts,
                         VLAN_PERMANENT, ConnectionId);

                }

            }
            else
            {
                VlanHwAddStaticUcastEntry
                    (VLAN_WILD_CARD_ID, pWildCardEntry->MacAddr,
                     VLAN_DEF_RECVPORT, pWildCardEntry->EgressPorts,
                     VLAN_PERMANENT, ConnectionId);

            }

        }

    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedVlanUcastCb                                      */
/*                                                                            */
/*  Description     : This function will be invoked for each unicast entry    */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same  st. ucast entry in the software. If   */
/*                    present this fn. corrects the ucast membership. If not  */
/*                    present this fn. deletes the static ucast entry from    */
/*                    the hardware. If S/w learning is enabled, this fn.      */
/*                    invokes fns. to add dynamic FDB into the table.         */
/*                                                                            */
/*  Input(s)        : u4FdbId - Filtering database identifier                 */
/*                    MacAddr - Unicast mac address                           */
/*                    u2RcvPort - Receive port parameter                      */
/*                    pUcastEntry - Pointer to the unicast entry.             */
/*                    AllowedList - AllowedToGo bitmap in H/W. Valid only for */
/*                    static unicast entries.                                 */
/*                    u1Status - Indicates the status of the entry. Valid     */
/*                    only for static entries.                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedVlanUcastCb (UINT4 u4FdbId, tMacAddr MacAddr, UINT2 u2RcvPort,
                    tHwUnicastMacEntry * pUcastEntry,
                    tLocalPortList AllowedList, UINT1 u1Status)
{
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    UINT4               u4FidIndex;
    UINT4               u4Port;

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        return;
    }

    if (u4FdbId == VLAN_WILD_CARD_ID)
    {
        VlanRedWildCardCb (MacAddr);
    }

    u4Port = VLAN_GET_IFINDEX (u2RcvPort);
    if (pUcastEntry->u1EntryType == VLAN_FDB_MGMT)
    {
        /* Static unicast entry */
        u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

        if (u4FidIndex != VLAN_INVALID_FID_INDEX)
        {
            pStUcastEntry =
                VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex,
                                                         MacAddr, u2RcvPort);

            if (pStUcastEntry == NULL)
            {
                /* Entry not present in s/w. Delete from h/w */
                VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : Deleting the static Ucast entry from "
                               "the HW - FDB (%d) Receive Port (%d) Ucast Address - ",
                               u4FdbId, u4Port);
                VlanPrintMacAddr (VLAN_RED_TRC, MacAddr);

                VlanHwDelStaticUcastEntry (u4FdbId, MacAddr, u2RcvPort);
            }
            else
            {
                /* Entry present check pbmp */
                if ((MEMCMP (AllowedList, pStUcastEntry->AllowedToGo,
                             sizeof (tLocalPortList)) != 0) ||
                    (u1Status != pStUcastEntry->u1Status))
                {
                    /* Allowed bitmap difference. Overwrite new one */
                    VLAN_TRC_ARG2 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                                   "AUDIT : Difference in Allowed portlist or "
                                   "status for the Ucast - FDB (%d) Receive "
                                   "Port (%d) Ucast Address - ", u4FdbId,
                                   u4Port);
                    VlanPrintMacAddr (VLAN_RED_TRC, MacAddr);

                    VlanHwAddStaticUcastEntry (u4FdbId, MacAddr, u2RcvPort,
                                               pStUcastEntry->AllowedToGo,
                                               pStUcastEntry->u1Status,
                                               pStUcastEntry->ConnectionId);
                }
            }
        }
    }
#ifdef SW_LEARNING
    else
    {
        /* add the dynamic unicast entry to the software database */
        /* ideally VlanFdbTableAddInCtxt should take FDB Id instead of VLAN Id.
         * This will work only for IVL switches only */

        VlanFdbTableAddInCtxt ((UINT2) pUcastEntry->u4Port, MacAddr,
                               (tVlanId) u4FdbId, pUcastEntry->u1EntryType,
                               pUcastEntry->ConnectionId, VLAN_LOCAL_FDB,
                               FNP_ZERO, FNP_ZERO);
    }
#endif /* SW_LEARNING */
}
#endif /* NPAPI_WANTED */

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedIsRelearningInProgress                           */
/*                                                                            */
/*  Description     : This function will be invoked to check if relearning    */
/*                    is completed.                                           */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
INT4
VlanRedIsRelearningInProgress (VOID)
{
    return VLAN_FALSE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedIsMcastRelrningInProgress                        */
/*                                                                            */
/*  Description     : This function will be invoked to check if multicast     */
/*                    relearning is completed.                                */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
INT4
VlanRedIsMcastRelrningInProgress (VOID)
{

    return VLAN_FALSE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSetChangedFlag                                   */
/*                                                                            */
/*  Description     : This function will be invoked when standby nodes exist  */
/*                    when the relearning timer expires. This function sets   */
/*                    changed flag for the VLAN and multicast entries.        */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSetChangedFlag (VOID)
{
    tVlanCurrEntry     *pVlanEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);
        if (pVlanEntry == NULL)
        {
            continue;
        }

        VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT (pVlanEntry, u1Result);
        if (u1Result != VLAN_TRUE)
        {
            VlanRedSetVlanChangedFlag (pVlanEntry);
        }

        for (pGroupEntry = pVlanEntry->pGroupTable;
             pGroupEntry != NULL; pGroupEntry = pGroupEntry->pNextNode)
        {
            if (MEMCMP (pGroupEntry->LearntPorts, gNullPortList,
                        sizeof (tLocalPortList)) != 0)
            {
                VlanRedSetGroupChangedFlag (pVlanEntry, pGroupEntry);
            }
        }
    }
}

/******************************************************************************/
/*  Function Name   : VlanRedIsVlanPresentInLrntTable                         */
/*                                                                            */
/*  Description     : This function will be checks whether the given VLAN     */
/*                    is present in GVRP learnt ports table or not.           */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS or VLAN_FAILURE                            */
/******************************************************************************/
INT4
VlanRedIsVlanPresentInLrntTable (tVlanId VlanId)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;

    pGvrpEntry = VlanRedGetGvrpLearntEntry (VlanId);

    if (pGvrpEntry != NULL)
    {
        return VLAN_SUCCESS;
    }
    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     ALL_FAILURE_TRC,
                     "GVRP learnt entry for the VLAN Id %d from"
                     "the GVRP learnt ports table is NULL.\n", VlanId);

    return VLAN_FAILURE;
}

/******************************************************************************/
/*  Function Name   : VlanRedSendMsgToRm                                      */
/*                                                                            */
/*  Description     : This function enqueues the update messages to the RM    */
/*                    module.                                                 */
/*                                                                            */
/*  Input(s)        : pMsg  - Message to be sent to RM module                 */
/*                    u2Len - Length of the Message                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCESS/VLAN_FAILURE                                */
/******************************************************************************/
INT4
VlanRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal;

    u4RetVal = VlanRmEnqMsgToRmFromAppl (pMsg, u2Len, RM_VLANGARP_APP_ID,
                                         RM_VLANGARP_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC,
                         "En queuing of Messages from Appl to Rm failed \n");

        RM_FREE (pMsg);
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : VlanRedStartMcastAudit                                  */
/*                                                                            */
/*  Description     : This function will be invoked by IGS module when the    */
/*                    node becomes active from the standby state. The IGS     */
/*                    module will invoke this function after the completion   */
/*                    of relearning timer expiry and when the forwarding table*/
/*                    mode is mac based.                                      */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedStartMcastAudit (VOID)
{
#ifdef NPAPI_WANTED
    /* 
     * Since VLAN module would have moved from standby to active state
     * VlanAuditTaskId will never be 0.
     */
    UINT4               i4RetVal;
#endif

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Vlan module not started yet\n");

        return;
    }
#ifdef NPAPI_WANTED
    VLAN_LOCK ();

    VLAN_RED_AUDIT_FLAG () = VLAN_RED_AUDIT_START;

    if (gVlanRedTaskInfo.VlanAuditTaskId == 0)
    {
        i4RetVal = VLAN_SPAWN_TASK (VLAN_AUDIT_TASK,
                                    VLAN_AUDIT_TASK_PRIORITY,
                                    OSIX_DEFAULT_STACK_SIZE,
                                    (OsixTskEntry) VlanRedAuditMain, 0,
                                    &VLAN_AUDIT_TASK_ID);
    }

    if (gVlanRedTaskInfo.VlanAuditTaskId != 0)
    {
        VLAN_SEND_EVENT (VLAN_AUDIT_TASK_ID, VLAN_RED_PERFORM_MCAST_AUDIT);
    }

    VLAN_UNLOCK ();
    UNUSED_PARAM(i4RetVal);
#endif /* NPAPI_WANTED */
}

/******************************************************************************/
/*  Function Name   : VlanRedSyncBulkUpdNextVlanId                            */
/*                                                                            */
/*  Description     : This function updates the BulkUpdNextVlanId, when the   */
/*                    vlan is removed`.                                       */
/*                                                                            */
/*  Input(s)        : VlanId, u1UpdType                                       */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :gVlanRedInfo                                   */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncBulkUpdNextVlanId (tVlanId VlanId, UINT1 u1UpdType)
{
    switch (u1UpdType)
    {
        case VLAN_RED_BULK_UPD_REMOVE_VLAN:
            if (gVlanRedInfo.BulkUpdNextVlanId == VlanId)
            {
                gVlanRedInfo.BulkUpdNextVlanId =
                    VLAN_GET_NEXT_VLAN_INDEX (VlanId);
            }
            break;
    }
}

/******************************************************************************/
/*  Function Name   : VlanRedHandleBulkUpdateEvent                            */
/*                                                                            */
/* Description      : It Handles the bulk update event. This event is used    */
/*                    to start the next sub bulk update. So                   */
/*                    VlanRedSendBulkUpdates is triggered.                    */
/*                                                                            */
/*  Input(s)        : VlanId, u1UpdType                                       */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :gVlanRedInfo                                   */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedHandleBulkUpdateEvent (VOID)
{
    if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
    {
        VlanRedSendBulkUpdates ();
    }
}

/*****************************************************************************/
/* Function Name      : VlanRedSendBulkUpdTailMsg                            */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message. 

     *        <------------1 Byte----------><--2 Byte-->
     ***************************************************
     *        *                            *           *
     * RM Hdr * VLAN_BULK_UPD_TAIL_MESSAGE *Msg Length *
     *        *                            *           *
     ***************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (VLAN_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         "RM alloc failed. Bulk Update Tail message not sent \n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        VlanRmHandleProtocolEvent (&ProtoEvt);
        return VLAN_FAILURE;
    }

    VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                     CONTROL_PLANE_TRC, "Sending Bulk Update tail message \n");
    u2OffSet = 0;

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_BULK_UPD_TAIL_MESSAGE);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, VLAN_RED_BULK_UPD_TAIL_MSG_SIZE);
    VLAN_GBL_TRC ("[ACTIVE]: Send Bulk Update Tail Message \r\n");

    if (VlanRedSendMsgToRm (pMsg, u2OffSet) == VLAN_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        VlanRmHandleProtocolEvent (&ProtoEvt);
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedSyncUcastAddressDeletion                      */
/*                                                                           */
/* Description        : This function will send unicast address deletion     */
/*                      syncup message on time out to standby node.          */
/*                                                                           */
/* Input(s)           : u4Context   - Virtual ContextId                      */
/*                      u4FidIndex  - Fid Index                              */
/*                      UcastAddr   - Unicast address which has been deleted */
/*                      u4RcvPortId - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSyncUcastAddressDeletion (UINT4 u4Context, UINT4 u4FidIndex,
                                 tMacAddr UcastAddr, UINT4 u4RcvPortId)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgLen;

    if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        return VLAN_SUCCESS;
    }

    if (VLAN_RED_NUM_STANDBY_NODES () == 0)
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN: Red sync up messages can not be sent if"
                  " the standby node is down.\n");
        return VLAN_SUCCESS;
    }

    /* Form a unicast address deletion sync message

     * 
     * |--------------------------------------------------------------------|
     * |Msg. Type |   Length  | ContextId | FidIndex | MacAddr  | RcvPortId |
     * | (1 byte) | (2 bytes) | (4 bytes) | (4 bytes)| (6 bytes)| (4 bytes) |
     * |--------------------------------------------------------------------|

     * The RM Hdr shall be included by RM.
     */

    u2MsgLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
        VLAN_RED_CONTEXT_FIELD_SIZE + VLAN_RED_FDB_ID_LEN +
        VLAN_RED_MAC_ADDR_LEN + VLAN_RED_PORT_NO_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME,
                  "RM alloc failed. Mac address deletion sync up message"
                  "not sent \n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pMsg, 0, sizeof (u2MsgLen));
    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Sending mac address deletion sync up message\n");

    u2OffSet = 0;

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_UCAST_DEL_ONTIMEOUT);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Context);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4FidIndex);
    VLAN_RM_PUT_N_BYTE (pMsg, UcastAddr, &u2OffSet, VLAN_MAC_ADDR_LEN);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4RcvPortId);

    VlanRedSendMsgToRm (pMsg, u2OffSet);

    return VLAN_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanRedSyncMcastAddressDeletion                      */
/*                                                                           */
/* Description        : This function will send multicast address deletion   */
/*                      sync up message on time out to standby node.         */
/*                                                                           */
/* Input(s)           : u4Context - Virtual ContextId                        */
/*                      u2VlanId  - Vlan Id.                                 */
/*                      McastAddr - Multicast address which has been deleted */
/*                      u4RcvPortId - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSyncMcastAddressDeletion (UINT4 u4Context, UINT2 u2VlanId,
                                 tMacAddr McastAddr, UINT4 u4RcvPortId)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgLen;

    if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        return VLAN_SUCCESS;
    }

    if (VLAN_RED_NUM_STANDBY_NODES () == 0)
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN: Red sync up messages can not be sent if"
                  " the standby node is down.\n");
        return VLAN_SUCCESS;
    }

    /* Form a multicast address deletion sync message

     * 
     * |--------------------------------------------------------------------|
     * |Msg. Type |   Length  | ContextId |  VlanId  | MacAddr  | RcvPortId |
     * | (1 byte) | (2 bytes) | (4 bytes) | (2 bytes)| (6 bytes)| (4 bytes) |
     * |--------------------------------------------------------------------|

     * The RM Hdr shall be included by RM.
     */

    u2MsgLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
        VLAN_RED_CONTEXT_FIELD_SIZE + VLAN_RED_VLAN_ID_LEN +
        VLAN_RED_MAC_ADDR_LEN + VLAN_RED_PORT_NO_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME,
                  "RM alloc failed. Mac address deletion sync up message"
                  "not sent \n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pMsg, 0, sizeof (u2MsgLen));
    VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Sending mac address deletion sync up message\n");

    u2OffSet = 0;

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_MCAST_DEL_ONTIMEOUT);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Context);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2VlanId);
    VLAN_RM_PUT_N_BYTE (pMsg, McastAddr, &u2OffSet, VLAN_MAC_ADDR_LEN);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4RcvPortId);

    VlanRedSendMsgToRm (pMsg, u2OffSet);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedSyncPortOperStatus                            */
/*                                                                           */
/* Description        : This function will send port oper status change to   */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : u4Context - Virtual ContextId                        */
/*                      u4Port   - Port number.                              */
/*                    : u1Status - Port Oper status.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSyncPortOperStatus (UINT4 u4Context, UINT4 u4Port, UINT1 u1Status)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgLen;

    if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        return VLAN_SUCCESS;
    }

    if (VLAN_RED_NUM_STANDBY_NODES () == 0)
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VLAN: Red sync up messages can not be sent if"
                  " the standby node is down.\n");
        return VLAN_SUCCESS;
    }

    /* Form a port oper status sync message

     *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 1 bytes ->|
     *    -----------------------------------------------------
     *    | Msg. Type |   Length    | PortId     | Oper Status |
     *    |     (1)   |1 + 2 + 4 + 1|            |             |
     *    |----------------------------------------------------

     * The RM Hdr shall be included by RM.
     */

    u2MsgLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
        VLAN_RED_CONTEXT_FIELD_SIZE + VLAN_RED_PORT_NO_LEN +
        VLAN_RED_PORT_OPER_STATUS_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                       VLAN_NAME,
                       "RM alloc failed. Oper status message for port %d "
                       "not sent \n", u4Port);
        return VLAN_FAILURE;
    }

    VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "Sending port oper status sync message for port %d\n",
                   u4Port);

    u2OffSet = 0;

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_PORT_OPER_STATUS_MESSAGE);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Context);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Port);
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, u1Status);

    VlanRedSendMsgToRm (pMsg, u2OffSet);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRestartModule                                    */
/*                                                                           */
/* Description        : This function will restart VLAN module               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRestartModule (VOID)
{
    UINT4               u4Context;

    VLAN_LOCK ();

    for (u4Context = 0; u4Context < VLAN_MAX_CONTEXT; u4Context++)
    {
        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            continue;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VLAN_TRC (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                      "VLAN Module is shutdown. So no need th Restart Module\n");
            VlanReleaseContext ();
            continue;
        }

        /* Shutdown VLAN module so that the configurations are lost. */
        VlanDeInit ();

        /* Start VLAN module and kept it in IDLE state */
        if (VlanInit () != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VLAN start failed during restart for applying "
                      "configuration database\n");
            VlanReleaseContext ();
            continue;
        }
        VlanReleaseContext ();
    }

    VLAN_UNLOCK ();

    VLAN_NODE_STATUS () = VLAN_NODE_IDLE;

    /* Redundancy information will be reinitialized, when the
     * node moves from IDLE to STANDBY state on RM_CONFIG_RESTORE_COMPLETE
     */

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedTrigHigherLayer                               */
/*                                                                           */
/* Description        : This function will send the given event to the next  */
/*                      higher layer (can be ECFM or ELMI.                   */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type. L2_INITIATE_BULK_UPDATES  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE VOID
VlanRedTrigHigherLayer (UINT1 u1TrigType)
{
#if defined (ECFM_WANTED)
    VlanEcfmRedRcvPktFromRm (u1TrigType, NULL, 0);
#elif defined (ELMI_WANTED)
    VlanElmRedRcvPktFromRm (u1TrigType, NULL, 0);
#elif defined (IGS_WANTED) || defined (MLDS_WANTED)    /* SNOOP_APPROACH */
    VlanSnoopRedRcvPktFromRm (u1TrigType, NULL, 0);
#elif defined (LLDP_WANTED)
    VlanLldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    if ((VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))

    {
        /* Send an event to RM to notify that sync up is completed */

        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                             "VLAN Node send event L2_SYNC_UP_COMPLETED "
                             "to RM failed\n");

        }

    }
#endif
}

/*****************************************************************************/
/* Function Name      : VlanRedHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedHandleDynSyncAudit (VOID)
{
/*On receiving this event, VLAN should execute show cmd and calculate checksum*/
    VlanExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : VlanExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_VLANGARP_APP_ID;
    UINT2               u2ChkSum = 0;

    VLAN_UNLOCK ();
    if (VlanGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == VLAN_FAILURE)
    {
        VLAN_LOCK ();
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Checksum of calculation failed for VLAN\n");
        return;
    }

    if (VlanRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == VLAN_FAILURE)
    {
        VLAN_LOCK ();
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Sending checkum to RM failed\n");
        return;
    }
    VLAN_LOCK ();
    return;
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*  Function Name   : VlanRedNotifyMcastAddFailure                            */
/*                                                                            */
/*  Description     : This function will be invoked by GMRP module when       */
/*                    there is a failure in the mcast add message which is    */
/*                    found as a result of audit.                             */
/*                                                                            */
/*  Input(s)        : VlanId - VLAN Identifier                                */
/*                    MacAddr - Mcast address.                                */
/*                    LearntLocalPortList - Port list in the hardware detected     */
/*                    during audit.                                           */
/*                    FailedPortList - Port list failed to apply.             */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedNotifyMcastAddFailure (tVlanId VlanId, tMacAddr MacAddr,
                              tLocalPortList LearntLocalPortList,
                              tLocalPortList FailedPortList)
{
    if (MEMCMP (LearntLocalPortList, FailedPortList, sizeof (tLocalPortList)) ==
        0)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME,
                       "AUDIT : Deleting the mcast entry which "
                       "is failed while adding - VLAN (%d) " "Mcast Address - ",
                       VlanId);
        VlanPrintMacAddr (VLAN_RED_TRC, MacAddr);

        /* all ports failed. delete teh entry */
        VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr);
    }
    else
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME,
                       "AUDIT : Adding the mcast entry which "
                       "is failed while adding - VLAN (%d) " "Mcast Address - ",
                       VlanId);
        VlanPrintMacAddr (VLAN_RED_TRC, MacAddr);

        /* Overwrite with port list that are successfully applied */
        VLAN_RESET_PORT_LIST (LearntLocalPortList, FailedPortList);
        VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                             LearntLocalPortList);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanSetOrSyncBrgModeFlag                                */
/*                                                                            */
/*  Description     : This function will be invoked when the Bridge Mode is   */
/*                    Changed in Active Node and when the corresponding nmh   */
/*                    sync-up is received at the standby node. In Standby node*/
/*                    it resets the flag while the active node sends the      */
/*                    sync-up which would have actually set the same flag.    */
/*                    This Flag is used during H/w Audit                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier                        */
/*                    u1Flag - Flag indicating if Failover happened when      */
/*                             setting the Bridge Mode.                       */
/*                             VLAN_TRUE/VLAN_FALSE                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanSetOrSyncBrgModeFlag (UINT4 u4ContextId, UINT1 u1Flag)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgLen;

    if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
    {
        if (VLAN_RED_NUM_STANDBY_NODES () != VLAN_INIT_VAL)
        {
            /* Form a Bridge Mode H/w Audit sync message

             *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 1 Byte ->|
             *    ---------------------------------------------------|
             *    | Msg. Type |   Length    | ContextID |u1Flag      |
             *    |     (1)   |1 + 2 + 4 + 1|           |            |
             *    |---------------------------------------------------

             * The RM Hdr shall be included by RM.
             */

            u2MsgLen = VLAN_RED_TYPE_FIELD_SIZE + VLAN_RED_LEN_FIELD_SIZE +
                VLAN_RED_CONTEXT_FIELD_SIZE + VLAN_RED_BRG_MODE_CHG_FLAG_SIZE;

            if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
            {
                VLAN_TRC_ARG1 (VLAN_RED_TRC,
                               (OS_RESOURCE_TRC | ALL_FAILURE_TRC), VLAN_NAME,
                               "RM alloc failed. HwAudit Bridge Mode for "
                               "Context %d " "not sent \n", u4ContextId);
                return;
            }

            VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Sending HwAudit Bridge Mode Indication for "
                           "Context%d\n", u4ContextId);

            u2OffSet = 0;

            VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_HWAUD_BRG_MODE_CHG);
            VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
            VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4ContextId);
            VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, u1Flag);

            VlanRedSendMsgToRm (pMsg, u2OffSet);
        }
    }
    else if (VLAN_NODE_STATUS () == VLAN_NODE_STANDBY)
    {
        VLAN_RED_HW_AUD_BRG_MODE_CONTEXT () = VLAN_INIT_VAL;
        VLAN_RED_HW_AUD_BRG_MODE_FLAG () = VLAN_FALSE;
    }
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedProcessSyncInfo                                  */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier                        */
/*                    u1Flag - VLAN_NP_NOT_UPDATED - NP is not updated.       */
/*                             VLAN_NP_FAILED      - NP call has failed.      */
/*                             VLAN_NP_UPDATED     - NP update successful.    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedProcessSyncInfo (tVlanRedCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedCacheNode  *pVlanRedCacheNode = NULL;

    if (NULL !=
        (pVlanRedCacheNode =
         VlanRedGetCacheEntry (pUpdtVlanRedCacheNode->u4ContextId,
                               pUpdtVlanRedCacheNode->VlanId)))
    {
        /* Case 1. This node is already observed to have a mismatch in s/w and h/w
         *         programming, which requires a mandate audit. Any further updates 
         *         are ignored.  
         */
        if ((pVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_FAILED))
        {
            pVlanRedCacheNode->i1RedSyncState = VLAN_RED_SYNC_FAILED;
            return VLAN_FAILURE;
        }
        else
        {
            pVlanRedCacheNode->i1RedSyncState =
                pUpdtVlanRedCacheNode->i1RedSyncState;

            if (VLAN_IS_NULL_PORTLIST
                (pUpdtVlanRedCacheNode->HwPortList.HwMemberPortList) !=
                VLAN_TRUE)
            {
                if (pVlanRedCacheNode->i1RedSyncState ==
                    VLAN_RED_SYNC_HW_UPDATED)
                {

                    MEMCPY (&(pVlanRedCacheNode->HwPortList.HwMemberPortList),
                            pUpdtVlanRedCacheNode->HwPortList.HwMemberPortList,
                            sizeof (tLocalPortList));
                    MEMCPY (&(pVlanRedCacheNode->HwPortList.HwUntagPortList),
                            pUpdtVlanRedCacheNode->HwPortList.HwUntagPortList,
                            sizeof (tLocalPortList));
                    MEMCPY (&(pVlanRedCacheNode->HwPortList.HwFwdAllPortList),
                            pUpdtVlanRedCacheNode->HwPortList.HwFwdAllPortList,
                            sizeof (tLocalPortList));
                    MEMCPY (&(pVlanRedCacheNode->HwPortList.HwFwdUnregPortList),
                            pUpdtVlanRedCacheNode->HwPortList.
                            HwFwdUnregPortList, sizeof (tLocalPortList));
                    VlanRedCheckAndClearCache (pVlanRedCacheNode);
                }
            }
        }
    }
    else
    {
        VlanRedAddCacheEntry (pUpdtVlanRedCacheNode);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSendCacheInfo                                    */
/*                                                                            */
/*  Description     : This function will be invoked during the following steps*/
/*                    1) Before VLAN table update in NP.                      */
/*                    2) After successful NP update of VLAN table.            */
/*                    3) Successful SW update of VLAN table.                  */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedSendCacheInfo (tVlanRedCacheNode * pVlanRedCacheNode)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgAllocSize = 0;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgAllocSize = VLAN_RED_CACHE_INFO_SIZE;
    u2MsgAllocSize = (u2MsgAllocSize < VLAN_RED_MAX_MSG_SIZE) ?
        u2MsgAllocSize : VLAN_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    VLAN Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 2B->|<- 1B ->|<-sizeof(tHwVlanPortList)->|
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|VLAN Id|Syncstate|    tHwVlanPortList       |
     *    |           |         |          |       |         |                          |
     *    ------------------------------------------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | OS_RESOURCE_TRC, VLAN_NAME,
                  "VlanRedSendCacheInfo: RM Memory allocation" " failed\r\n");
        return VLAN_FAILURE;
    }

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_RED_SYNC_CACHE_INFO);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgAllocSize);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4ContextId);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->VlanId);
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->i1RedSyncState);
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->HwPortList),
                        &u2OffSet, sizeof (tHwVlanPortList));

    if (VlanRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedSendCacheInfo:Send message to RM failed \r\n");
        return VLAN_FAILURE;
    }

    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedSendCacheInfo\r \n");
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSendMcastCacheInfo                             */
/*                                                                            */
/*  Description     : This function will be invoked during the following steps*/
/*                    1) Before VLAN table update in NP.                      */
/*                    2) After successful NP update of VLAN table.            */
/*                    3) Successful SW update of VLAN table.                  */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedSendMcastCacheInfo (tVlanRedMcastCacheNode * pVlanRedCacheNode)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgAllocSize = 0;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgAllocSize = VLAN_RED_MCAST_CACHE_INFO_SIZE;
    u2MsgAllocSize = (u2MsgAllocSize < VLAN_RED_MAX_MSG_SIZE) ?
        u2MsgAllocSize : VLAN_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    VLAN Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 2B->|<-  4B   ->|<-szof(tLocalPortList)->|     *
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|Vlan Id |<-RcvPort->|      AllowedPorts     |
     *    |           |         |          |       |           |                       |
     *    ------------------------------------------------------------------------------
     *
     *    ->|<-sizeof(MacAddress)->|<-  1B  ->|
     *    -------------------------------------
     *    | MacAddress             |SyncState |
     *    |                        |          |
     *    -------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | OS_RESOURCE_TRC, VLAN_NAME,
                  "VlanRedSendStUcastCacheInfo: RM Memory allocation"
                  " failed\r\n");
        return VLAN_FAILURE;
    }

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_RED_SYNC_MCAST_CACHE_INFO);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgAllocSize);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4ContextId);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->VlanId);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4RcvPort);
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->HwPortList),
                        &u2OffSet, sizeof (tLocalPortList));
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->MacAddr),
                        &u2OffSet, sizeof (tMacAddr));
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->i1RedSyncState);

    if (VlanRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedSendMcastCacheInfo:Send message to RM failed \r\n");
        return VLAN_FAILURE;
    }

    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedSendMcastCacheInfo\r \n");
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedHwUpdatePortVlanSync                     */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                      i1RedSyncState                                  */
/*                                                                      */
/*                        VLAN_RED_SYNC_HW_NOT_UPDATED     0            */
/*                       VLAN_RED_SYNC_HW_UPDATED          1            */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/*  Returns         : VLAN_SUCCESS, if successful                       */
/*                    VLAN_FAILURE, otherwise                           */
/************************************************************************/

INT4
VlanRedHwUpdateProtoVlanSync (tVlanProtoTemplate * pVlanProtoTemplate,
                              UINT4 u4ContextId,
                              UINT4 u4GroupId,
                              UINT4 u4IfIndex,
                              INT1 i1RedSyncState, INT1 i1Action)
{
    tVlanRedProtoVlanCacheNode VlanRedCacheNode;
    tVlanId             VlanId = 0;
    UINT2               u2Port = 0;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedProtoVlanCacheNode));
    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2Port)
        != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedProtoVlanCacheNode));

    if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
    {
        VlanRedCacheNode.u4ContextId = u4ContextId;
        VlanRedCacheNode.u4GroupId = u4GroupId;
        VlanRedCacheNode.u2Port = u2Port;
        VlanRedCacheNode.i1RedSyncState = i1RedSyncState;
        VlanRedCacheNode.i1Action = i1Action;
        MEMCPY (&VlanRedCacheNode.VlanProtoTemplate, pVlanProtoTemplate,
                sizeof (tVlanProtoTemplate));

        if (VLAN_RED_SYNC_HW_UPDATED == i1RedSyncState)
        {
            if (VLAN_SUCCESS == VlanHwGetVlanProtocolMap (u4ContextId, u2Port,
                                                          u4GroupId,
                                                          pVlanProtoTemplate,
                                                          &VlanId))
            {
                VlanRedCacheNode.VlanId = VlanId;
            }
        }
        if (VLAN_FAILURE == VlanRedSendProtoVlanCacheInfo (&VlanRedCacheNode))
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedHwUpdateStUcastSync                      */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                      i1RedSyncState                                  */
/*                                                                      */
/*                        VLAN_RED_SYNC_HW_NOT_UPDATED     0            */
/*                       VLAN_RED_SYNC_HW_UPDATED          1            */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/*  Returns         : VLAN_SUCCESS, if successful                       */
/*                    VLAN_FAILURE, otherwise                           */
/************************************************************************/

INT4
VlanRedHwUpdateStUcastSync (UINT4 u4ContextId,
                            UINT4 u4FdbId,
                            tMacAddr MacAddress,
                            UINT4 u4RcvPort, INT1 i1RedSyncState, INT1 i1Action)
{
    tVlanRedStUcastCacheNode VlanRedCacheNode;
    tLocalPortList      AllowedToGoToPorts;
    tMacAddr            ConnectionId;
    UINT4               u4CurrContext = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1Status = 0;

    if (VLAN_RED_NUM_STANDBY_NODES () == 0)
    {
	return VLAN_SUCCESS;
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }

    if (u4RcvPort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            (u4RcvPort, &u4CurrContext, &u2LocalPort) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedStUcastCacheNode));
    MEMSET (&AllowedToGoToPorts, 0, sizeof (tLocalPortList));
    MEMSET (&ConnectionId, 0, sizeof (tMacAddr));

    if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
    {
        VlanRedCacheNode.u4FdbId = u4FdbId;
        VlanRedCacheNode.u4ContextId = u4ContextId;
        VlanRedCacheNode.i1RedSyncState = i1RedSyncState;
        VlanRedCacheNode.u4RcvPort = u4RcvPort;
        VlanRedCacheNode.i1Action = i1Action;
        MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

        if (VLAN_RED_SYNC_HW_UPDATED == i1RedSyncState)
        {
            if (VLAN_SUCCESS == VlanHwGetStaticUcastEntry (u4ContextId, u4FdbId,
                                                           MacAddress,
                                                           u2LocalPort,
                                                           AllowedToGoToPorts,
                                                           &u1Status,
                                                           ConnectionId))
            {
                MEMCPY (&VlanRedCacheNode.AllowedToGoToPorts,
                        &AllowedToGoToPorts, sizeof (tLocalPortList));
                MEMCPY (&VlanRedCacheNode.ConnectionId,
                        &ConnectionId, sizeof (tMacAddr));
                VlanRedCacheNode.u1Status = u1Status;
            }
        }
        if (VLAN_FAILURE == VlanRedSendStUcastCacheInfo (&VlanRedCacheNode))
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedHwUpdateMcastSync                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                      i1RedSyncState                                  */
/*                                                                      */
/*                        VLAN_RED_SYNC_HW_NOT_UPDATED     0            */
/*                       VLAN_RED_SYNC_HW_UPDATED          1            */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/*  Returns         : VLAN_SUCCESS, if successful                       */
/*                    VLAN_FAILURE, otherwise                           */
/************************************************************************/

INT4
VlanRedHwUpdateMcastSync (UINT4 u4ContextId,
                          tVlanId VlanId,
                          tMacAddr MacAddress,
                          UINT4 u4RcvPort, INT1 i1RedSyncState)
{
    tVlanRedMcastCacheNode VlanRedCacheNode;
    tLocalPortList      HwPortList;

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedMcastCacheNode));
    MEMSET (&HwPortList, 0, sizeof (tLocalPortList));

    if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
    {
        VlanRedCacheNode.VlanId = VlanId;
        VlanRedCacheNode.u4ContextId = u4ContextId;
        VlanRedCacheNode.i1RedSyncState = i1RedSyncState;
        VlanRedCacheNode.u4RcvPort = u4RcvPort;
        MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));
        if (VLAN_RED_SYNC_HW_UPDATED == i1RedSyncState)
        {

            if (VLAN_SUCCESS ==
                VlanHwGetMcastEntry (u4ContextId, VlanId, MacAddress,
                                     HwPortList))
            {
                MEMCPY (&VlanRedCacheNode.HwPortList,
                        &HwPortList, sizeof (tLocalPortList));
            }
        }
        if (VLAN_FAILURE == VlanRedSendMcastCacheInfo (&VlanRedCacheNode))
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedSyncProtoVlanSwUpdate                    */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedSyncProtoVlanSwUpdate (UINT4 u4ContextId,
                              UINT4 u4GroupId, UINT2 u2Port, UINT1 u1Action)
{
    tVlanRedProtoVlanCacheNode VlanRedCacheNode;
    tVlanRedProtoVlanCacheNode *pVlanRedCacheNode = NULL;

    if (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS)
    {
        /* Code change to prevent updation of cache entries during bulk syncup */
        return VLAN_SUCCESS;
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }
    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedProtoVlanCacheNode));

    if (((u1Action != VLAN_ADD) && (VLAN_DELETE != u1Action)) ||
        (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_PROTO_TABLE_MASK))
    {
        return VLAN_FAILURE;
    }

    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4GroupId = u4GroupId;
    VlanRedCacheNode.u2Port = u2Port;

    if (VlanRedGetNodeStatus () == VLAN_NODE_STANDBY)
    {
        pVlanRedCacheNode =
            VlanRedGetProtoVlanCacheEntry (u4ContextId, u4GroupId, u2Port);

        if ((NULL != pVlanRedCacheNode))
        {
            if ((pVlanRedCacheNode->i1Action == VLAN_DELETE) &&
                (u1Action == VLAN_DELETE))
            {
                VlanRedDelProtoVlanCache (pVlanRedCacheNode->u4ContextId,
                                          pVlanRedCacheNode->u4GroupId,
                                          pVlanRedCacheNode->u2Port);

            }
            else
            {
                VlanRedProtoCheckAndClearCache (pVlanRedCacheNode);
            }
        }
        else
        {
            VlanRedAddProtoVlanCacheEntry (&VlanRedCacheNode);
            return VLAN_SUCCESS;
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedSyncMcastSwUpdate                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedSyncMcastSwUpdate (UINT4 u4ContextId,
                          tVlanId VlanId,
                          tMacAddr MacAddress, UINT4 u4RcvPort, UINT1 u1Action)
{
    tVlanRedMcastCacheNode *pVlanRedCacheNode = NULL;
    tVlanStMcastEntry  *pVlanStMcastEntry = NULL;
    tVlanGroupEntry    *pVlanGroupEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanRedMcastCacheNode VlanRedCacheNode;
    UINT2               u2LocalPortId = 0;
    UINT4               u4CurrContext = 0;

    if (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS)
    {
        /* Code change to prevent updation of cache entries during bulk syncup */
        return VLAN_SUCCESS;
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }
    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedMcastCacheNode));

    if (((u1Action != VLAN_ADD) && (VLAN_DELETE != u1Action)) ||
        (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_MCAST_TABLE_MASK))
    {
        return VLAN_FAILURE;
    }

    VlanRedCacheNode.VlanId = VlanId;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4RcvPort = u4RcvPort;
    MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

    if (VlanRedGetNodeStatus () == VLAN_NODE_STANDBY)
    {
        pVlanRedCacheNode =
            VlanRedGetMcastCacheEntry (u4ContextId, MacAddress, VlanId,
                                       u4RcvPort);

        /* This block will hit when the static multicast entry is deleted */
        if ((NULL != pVlanRedCacheNode))
        {
            if (u1Action == VLAN_DELETE)
            {
                VlanGetContextInfoFromIfIndex (pVlanRedCacheNode->u4RcvPort,
                                               &u4CurrContext, &u2LocalPortId);
                pVlanEntry = VlanGetVlanEntry (pVlanRedCacheNode->VlanId);
                if (NULL != pVlanEntry)
                {
                    pVlanStMcastEntry =
                        VlanGetStMcastEntryWithExactRcvPort (pVlanRedCacheNode->
                                                             MacAddr,
                                                             u2LocalPortId,
                                                             pVlanEntry);

                    /* for Static multicast entries */
                    if ((NULL == pVlanStMcastEntry) &&
                        ((0 ==
                          MEMCMP (pVlanRedCacheNode->HwPortList, gNullPortList,
                                  sizeof (tLocalPortList)))))
                    {
                        if (VLAN_SUCCESS ==
                            VlanRedDelMcastCacheEntry (pVlanRedCacheNode->
                                                       u4ContextId,
                                                       pVlanRedCacheNode->
                                                       MacAddr,
                                                       pVlanRedCacheNode->
                                                       VlanId,
                                                       pVlanRedCacheNode->
                                                       u4RcvPort))
                        {
                            return VLAN_FAILURE;
                        }
                        return VLAN_SUCCESS;
                    }
                    /* For Dynamic multicast entries */
                    pVlanGroupEntry =
                        VlanGetGroupEntry (pVlanRedCacheNode->MacAddr,
                                           pVlanEntry);
                    if ((NULL == pVlanGroupEntry)
                        &&
                        ((0 ==
                          MEMCMP (pVlanRedCacheNode->HwPortList, gNullPortList,
                                  sizeof (tLocalPortList)))))
                    {
                        if (VLAN_SUCCESS ==
                            VlanRedDelMcastCacheEntry (pVlanRedCacheNode->
                                                       u4ContextId,
                                                       pVlanRedCacheNode->
                                                       MacAddr,
                                                       pVlanRedCacheNode->
                                                       VlanId,
                                                       pVlanRedCacheNode->
                                                       u4RcvPort))
                        {
                            return VLAN_FAILURE;
                        }
                        return VLAN_SUCCESS;
                    }

                }
            }
            else
            {
                VlanRedMcastCheckAndClearCache (pVlanRedCacheNode);
            }
        }
    }
    else
    {
        VlanRedAddMcastCacheEntry (&VlanRedCacheNode);
        return VLAN_SUCCESS;
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedProtoCheckAndClearCache                  */
/*                                                                      */
/* Description        : This function checks and clear the cache node   */
/*                      if the software configurations matches that of  */
/*                      NP programming.                                 */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedProtoCheckAndClearCache (tVlanRedProtoVlanCacheNode * pVlanRedCacheNode)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanProtGrpEntry  *pGroupEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSet = NULL;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (pVlanRedCacheNode->u2Port);

    if (pVlanPortEntry == NULL)
    {
        VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Vlan Port Entry is Null!! \n ");
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSet,
                   tVlanPortVidSet *)
    {
        if ((pVlanPortVidSet->VlanId == pVlanRedCacheNode->VlanId) &&
            (pVlanPortVidSet->u1RowStatus == VLAN_ACTIVE))
        {
            pGroupEntry =
                VlanGetActiveProtoGrpEntry (pVlanPortVidSet->u4ProtGrpId);
            break;
        }
    }
    if ((VLAN_RED_SYNC_FAILED != pVlanRedCacheNode->i1RedSyncState) &&
        (NULL != pGroupEntry))
    {
        if (0 == MEMCMP (&(pVlanRedCacheNode->VlanProtoTemplate),
                         &(pGroupEntry->VlanProtoTemplate),
                         sizeof (tVlanProtoTemplate)))
        {
            if (VLAN_SUCCESS ==
                VlanRedDelProtoVlanCache (pVlanRedCacheNode->u4ContextId,
                                          pVlanRedCacheNode->u4GroupId,
                                          pVlanRedCacheNode->u2Port))
            {
                return VLAN_SUCCESS;
            }
        }
    }

    return VLAN_FAILURE;
}

/************************************************************************/
/* Function Name      : VlanRedMcastCheckAndClearCache                */
/*                                                                      */
/* Description        : This function checks and clear the cache node   */
/*                      if the software configurations matches that of  */
/*                      NP programming.                                 */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedMcastCheckAndClearCache (tVlanRedMcastCacheNode * pVlanRedCacheNode)
{
    tVlanStMcastEntry  *pVlanStMcastEntry = NULL;
    tVlanGroupEntry    *pVlanGroupEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT2               u2LocalPortId = 0;
    UINT4               u4CurrContext = 0;

    /* This routine will be invoked when SW/update event is notifie                  * By this time, the pending entry should be in h/w updated state. 
     * Else, the entry has to be marked as sync failed entry. 
     */

    if (0 != pVlanRedCacheNode->u4RcvPort)
    {
        if (VLAN_FAILURE ==
            VlanGetContextInfoFromIfIndex (pVlanRedCacheNode->u4RcvPort,
                                           &u4CurrContext, &u2LocalPortId))
        {
            return VLAN_FAILURE;
        }
    }
    pVlanEntry = VlanGetVlanEntry (pVlanRedCacheNode->VlanId);
    if (NULL == pVlanEntry)
    {
        return VLAN_FAILURE;
    }

    pVlanStMcastEntry =
        VlanGetStMcastEntryWithExactRcvPort (pVlanRedCacheNode->MacAddr,
                                             u2LocalPortId, pVlanEntry);

    /* Avoid clearing of the pending entry when it is already marked as
     * VLAN_RED_SYNC_FAILED.
     */
    if ((VLAN_RED_SYNC_FAILED != pVlanRedCacheNode->i1RedSyncState) &&
        (NULL != pVlanStMcastEntry) &&
        (0 ==
         MEMCMP (pVlanRedCacheNode->HwPortList, pVlanStMcastEntry->EgressPorts,
                 sizeof (tLocalPortList))))
    {
        if (VLAN_SUCCESS ==
            VlanRedDelMcastCacheEntry (pVlanRedCacheNode->u4ContextId,
                                       pVlanRedCacheNode->MacAddr,
                                       pVlanRedCacheNode->VlanId,
                                       pVlanRedCacheNode->u4RcvPort))
        {
            return VLAN_SUCCESS;
        }
    }

    pVlanGroupEntry =
        VlanGetGroupEntry (pVlanRedCacheNode->MacAddr, pVlanEntry);
    if ((NULL != pVlanGroupEntry)
        && (0 ==
            MEMCMP (pVlanRedCacheNode->HwPortList, pVlanGroupEntry->LearntPorts,
                    sizeof (tLocalPortList))))
    {
        if (VLAN_SUCCESS ==
            VlanRedDelMcastCacheEntry (pVlanRedCacheNode->u4ContextId,
                                       pVlanRedCacheNode->MacAddr,
                                       pVlanRedCacheNode->VlanId,
                                       pVlanRedCacheNode->u4RcvPort))
        {
            return VLAN_SUCCESS;
        }
    }

    return VLAN_FAILURE;
}

/************************************************************************/
/* Function Name      : VlanRedProcessProtoVlanCacheInfo                */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
VlanRedProcessProtoVlanCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId,
                                  UINT2 *pu2OffSet)
{
    tVlanRedProtoVlanCacheNode VlanRedCacheNode;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedProtoVlanCacheNode));
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Entering VlanRedProcessProtoVlanCacheInfo\r \n");

    /*
     *    Protocol VLAN Dynamic Update message
     *
     * <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 4B->|<-  2B   ->|<-(tVlanProtoTemplate)->|  *
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|Grp Id |<-  Port ->|      VlanProtTemplate  |
     *    |           |         |          |       |           |                        |
     *    ------------------------------------------------------------------------------
     *                                                                                        *    ->|<-tVlanId->|<- 1B ->|<-  1B  ->|
     *    -----------------------------------
     *      | VlanId    |SyncState|Action   |
     *      |           |         |         |          
     *    -----------------------------------
     */

    VLAN_RM_GET_4_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.u4GroupId);
    VLAN_RM_GET_2_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.u2Port);
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.VlanProtoTemplate),
                        pu2OffSet, sizeof (tVlanProtoTemplate));
    VLAN_RM_GET_2_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.VlanId);
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.i1RedSyncState);
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.i1Action);

    VlanRedCacheNode.u4ContextId = u4ContextId;
    if (VLAN_FAILURE == VlanRedProcessProtoSyncInfo (&VlanRedCacheNode))
    {
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC, VLAN_NAME,
                  "Processing the pending entries for VLAN Hardware Audit Failed. \r \n");
        return;

    }
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedProcessProtoVlanCacheInfo\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VlanRedProcessMcastCacheInfo                  */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
VlanRedProcessMcastCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId,
                              UINT2 *pu2OffSet)
{
    tVlanRedMcastCacheNode VlanRedCacheNode;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedMcastCacheNode));
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Entering VlanRedProcessMcastCacheInfo\r \n");

    VLAN_RM_GET_2_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.VlanId);
    VLAN_RM_GET_4_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.u4RcvPort);
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.HwPortList),
                        pu2OffSet, sizeof (tLocalPortList));
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.MacAddr),
                        pu2OffSet, sizeof (tMacAddr));
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.i1RedSyncState);

    VlanRedCacheNode.u4ContextId = u4ContextId;
    if (VLAN_FAILURE == VlanRedProcessMcastSyncInfo (&VlanRedCacheNode))
    {
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC, VLAN_NAME,
                  "Processing the pending entries for VLAN Hardware Audit Failed. \r \n");
        return;

    }
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedProcessMcastCacheInfo\r \n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedProcessProtoSyncInfo                            */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier                        */
/*                    u1Flag - VLAN_NP_NOT_UPDATED - NP is not updated.       */
/*                             VLAN_NP_FAILED      - NP call has failed.      */
/*                             VLAN_NP_UPDATED     - NP update successful.    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedProcessProtoSyncInfo (tVlanRedProtoVlanCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedProtoVlanCacheNode *pVlanRedCacheNode = NULL;

    if (NULL !=
        (pVlanRedCacheNode =
         VlanRedGetProtoVlanCacheEntry (pUpdtVlanRedCacheNode->u4ContextId,
                                        pUpdtVlanRedCacheNode->u4GroupId,
                                        pUpdtVlanRedCacheNode->u2Port)))
    {
        /* Case 1. This node is already observed to have a mismatch in s/w and h/w
         *         programming, which requires a mandate audit. Any further updates 
         *         are ignored.  
         */
        if (pVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_FAILED)
        {
            pVlanRedCacheNode->i1RedSyncState = VLAN_RED_SYNC_FAILED;
            return VLAN_FAILURE;
        }

        if (pUpdtVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_HW_UPDATED)
        {
            pVlanRedCacheNode->VlanId = pUpdtVlanRedCacheNode->VlanId;
            VlanRedProtoCheckAndClearCache (pVlanRedCacheNode);
        }
    }
    else
    {
        VlanRedAddProtoVlanCacheEntry (pUpdtVlanRedCacheNode);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedProcessMcastSyncInfo                           */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier                        */
/*                    u1Flag - VLAN_NP_NOT_UPDATED - NP is not updated.       */
/*                             VLAN_NP_FAILED      - NP call has failed.      */
/*                             VLAN_NP_UPDATED     - NP update successful.    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedProcessMcastSyncInfo (tVlanRedMcastCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedMcastCacheNode *pVlanRedCacheNode = NULL;

    if (NULL !=
        (pVlanRedCacheNode =
         VlanRedGetMcastCacheEntry (pUpdtVlanRedCacheNode->u4ContextId,
                                    pUpdtVlanRedCacheNode->MacAddr,
                                    pUpdtVlanRedCacheNode->VlanId,
                                    pUpdtVlanRedCacheNode->u4RcvPort)))
    {
        /* Case 1. This node is already observed to have a mismatch in s/w and h/w
         *         programming, which requires a mandate audit. Any further updates 
         *         are ignored.  
         */
        if (pVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_FAILED)
        {
            pVlanRedCacheNode->i1RedSyncState = VLAN_RED_SYNC_FAILED;
            return VLAN_FAILURE;
        }
        pVlanRedCacheNode->i1RedSyncState =
            pUpdtVlanRedCacheNode->i1RedSyncState;

        if (pVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_HW_UPDATED)
        {
            MEMCPY (&(pVlanRedCacheNode->HwPortList),
                    &(pUpdtVlanRedCacheNode->HwPortList),
                    sizeof (tLocalPortList));
            VlanRedMcastCheckAndClearCache (pVlanRedCacheNode);
        }
    }
    else
    {
        VlanRedAddMcastCacheEntry (pUpdtVlanRedCacheNode);
    }
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSendProtoVlanCacheInfo                           */
/*                                                                            */
/*  Description     : This function will be invoked during the following steps*/
/*                    1) Before VLAN table update in NP.                      */
/*                    2) After successful NP update of VLAN table.            */
/*                    3) Successful SW update of VLAN table.                  */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedSendProtoVlanCacheInfo (tVlanRedProtoVlanCacheNode * pVlanRedCacheNode)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgAllocSize = 0;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgAllocSize = VLAN_RED_PROTO_VLAN_CACHE_INFO_SIZE;
    u2MsgAllocSize = (u2MsgAllocSize < VLAN_RED_MAX_MSG_SIZE) ?
        u2MsgAllocSize : VLAN_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    VLAN Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 4B->|<-  4B   ->|<-szof(tLocalPortList)->|     *
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|FDB Id |<-RcvPort->|      AllowedPorts     |
     *    |           |         |          |       |           |                       |
     *    ------------------------------------------------------------------------------
     *
     *    ->|<-sizeof(MacAddress)->|<-sizeof(MacAddress)->|<- 1B ->|<-  1B  ->|
     *    --------------------------------------------------------------------
     *    | MacAddress             | ConnectionId         |u1Status|SyncState |
     *    |                        |                      |        |          |
     *    ---------------------------------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | OS_RESOURCE_TRC, VLAN_NAME,
                  "VlanRedSendProtoVlanCacheInfo: RM Memory allocation"
                  " failed\r\n");
        return VLAN_FAILURE;
    }

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_RED_SYNC_PROTO_CACHE_INFO);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgAllocSize);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4ContextId);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4GroupId);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u2Port);
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->VlanProtoTemplate),
                        &u2OffSet, sizeof (tVlanProtoTemplate));
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->VlanId);
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->i1RedSyncState);
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->i1Action);

    if (VlanRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedSendProtoVlanCacheInfo:Send message to RM failed \r\n");
        return VLAN_FAILURE;
    }

    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedSendProtoVlanCacheInfo\r \n");
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedSwUpdateStUcastSync                      */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedSyncStUcastSwUpdate (UINT4 u4ContextId,
                            UINT4 u4FdbId,
                            tMacAddr MacAddress,
                            UINT4 u4RcvPort, UINT1 u1Action)
{
    tVlanRedStUcastCacheNode *pVlanRedCacheNode = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    tVlanRedStUcastCacheNode VlanRedCacheNode;
    UINT4               u4CurrContextId = 0;
    UINT4               u4FidIndex = 0;
    UINT2               u2LocalPortId = 0;

    if (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS)
    {
        /* Code change to prevent updation of cache entries during bulk syncup */
        return VLAN_SUCCESS;
    }

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }
    if (((u1Action != VLAN_ADD) && (VLAN_DELETE != u1Action)) ||
        (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_UCAST_TABLE_MASK))
    {
        return VLAN_FAILURE;
    }
    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedStUcastCacheNode));

    VlanRedCacheNode.u4FdbId = u4FdbId;
    VlanRedCacheNode.u4ContextId = u4ContextId;
    VlanRedCacheNode.u4RcvPort = u4RcvPort;
    MEMCPY (VlanRedCacheNode.MacAddr, MacAddress, sizeof (tMacAddr));

    if (VlanRedGetNodeStatus () == VLAN_NODE_STANDBY)
    {
        pVlanRedCacheNode =
            VlanRedGetStUcastCacheEntry (u4ContextId, MacAddress, u4FdbId,
                                         u4RcvPort);

        /* This block will hit when the static unicast entry is deleted */
        if ((pVlanRedCacheNode != NULL))
        {
            if ((u1Action == VLAN_DELETE))
            {
                VlanGetContextInfoFromIfIndex (pVlanRedCacheNode->u4RcvPort,
                                               &u4CurrContextId,
                                               &u2LocalPortId);
                u4FidIndex =
                    VlanGetFidEntryFromFdbId (pVlanRedCacheNode->u4FdbId);

                pVlanStUcastEntry =
                    VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex,
                                                             pVlanRedCacheNode->
                                                             MacAddr,
                                                             u2LocalPortId);
                if (((NULL == pVlanStUcastEntry)
                     &&
                     ((0 ==
                       MEMCMP (pVlanRedCacheNode->AllowedToGoToPorts,
                               gNullPortList, sizeof (tLocalPortList)))
                      && (0 ==
                          MEMCMP (&(pVlanRedCacheNode->ConnectionId),
                                  gNullMacAddress, sizeof (tMacAddr))))))
                {
                    if (VLAN_FAILURE ==
                        VlanRedDelStUcastCacheEntry (pVlanRedCacheNode->
                                                     u4ContextId,
                                                     pVlanRedCacheNode->MacAddr,
                                                     pVlanRedCacheNode->u4FdbId,
                                                     pVlanRedCacheNode->
                                                     u4RcvPort))
                    {
                        return VLAN_FAILURE;
                    }
                }
            }
            else
            {
                VlanRedStUcastCheckAndClearCache (pVlanRedCacheNode);
            }
        }
        else
        {
            VlanRedAddStUcastCacheEntry (&VlanRedCacheNode);
            return VLAN_SUCCESS;
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedHwUpdateSync                             */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                      i1RedSyncState                                  */
/*                                                                      */
/*                        VLAN_RED_SYNC_HW_NOT_UPDATED     0            */
/*                       VLAN_RED_SYNC_HW_UPDATED          1            */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/*  Returns         : VLAN_SUCCESS, if successful                       */
/*                    VLAN_FAILURE, otherwise                           */
/************************************************************************/

INT4
VlanRedSyncHwUpdate (UINT4 u4ContextId, tVlanId VlanId, INT1 i1RedSyncState)
{
    tVlanRedCacheNode   VlanRedCacheNode;
    tHwMiVlanEntry      HwEntry;

    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }
    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedCacheNode));
    MEMSET (&HwEntry, 0, sizeof (tHwMiVlanEntry));

    if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
    {
        VlanRedCacheNode.VlanId = VlanId;
        VlanRedCacheNode.u4ContextId = u4ContextId;
        VlanRedCacheNode.i1RedSyncState = i1RedSyncState;

        if (VLAN_RED_SYNC_HW_UPDATED == i1RedSyncState)
        {
            if (VLAN_SUCCESS ==
                VlanHwGetVlanInfo (u4ContextId, VlanId, &HwEntry))
            {
                MEMCPY (&VlanRedCacheNode.HwPortList.HwMemberPortList,
                        HwEntry.HwMemberPbmp, sizeof (tLocalPortList));
                MEMCPY (&VlanRedCacheNode.HwPortList.HwUntagPortList,
                        HwEntry.HwUntagPbmp, sizeof (tLocalPortList));
                MEMCPY (&VlanRedCacheNode.HwPortList.HwFwdAllPortList,
                        HwEntry.HwFwdAllPbmp, sizeof (tLocalPortList));
                MEMCPY (&VlanRedCacheNode.HwPortList.HwFwdUnregPortList,
                        HwEntry.HwFwdUnregPbmp, sizeof (tLocalPortList));
            }
        }
        if (VLAN_FAILURE == VlanRedSendCacheInfo (&VlanRedCacheNode))
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedSwUpdateSync                             */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedSyncSwUpdate (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1Action)
{
    tVlanRedCacheNode  *pVlanRedCacheNode = NULL;
    tVlanRedCacheNode   VlanRedCacheNode;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    if (gVlanRedInfo.u1OptimizedAudit == VLAN_FALSE)
    {
        /* Optimized HW Audit Not Enabled, hence no need to 
         * send sync for storing cache information 
         */
        return VLAN_SUCCESS;
    }

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedCacheNode));
    if (((u1Action != VLAN_ADD) && (VLAN_DELETE != u1Action)) ||
        (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_VLAN_TABLE_MASK))
    {
        return VLAN_FAILURE;
    }

    if (VlanRedGetNodeStatus () == VLAN_NODE_STANDBY)
    {
        pVlanCurrEntry = VlanGetVlanEntry (VlanId);
        pVlanRedCacheNode = VlanRedGetCacheEntry (u4ContextId, VlanId);
        if ((pVlanRedCacheNode != NULL))
        {

            if ((u1Action == VLAN_DELETE) &&
                (pVlanCurrEntry == NULL) &&
                (0 == MEMCMP (pVlanRedCacheNode->HwPortList.HwMemberPortList,
                              gNullPortList, sizeof (tLocalPortList))) &&
                (0 ==
                 MEMCMP (pVlanRedCacheNode->HwPortList.HwUntagPortList,
                         gNullPortList, sizeof (tLocalPortList)))
                && (0 ==
                    MEMCMP (pVlanRedCacheNode->HwPortList.HwFwdAllPortList,
                            gNullPortList, sizeof (tLocalPortList)))
                && (0 ==
                    MEMCMP (pVlanRedCacheNode->HwPortList.HwFwdUnregPortList,
                            gNullPortList, sizeof (tLocalPortList))))
            {
                if (VLAN_SUCCESS ==
                    VlanRedDelCacheEntry (pVlanRedCacheNode->u4ContextId,
                                          pVlanRedCacheNode->VlanId))
                {
                    return VLAN_SUCCESS;
                }

            }
            else
            {
                VlanRedCheckAndClearCache (pVlanRedCacheNode);
            }
        }
        else
        {
            VlanRedCacheNode.VlanId = VlanId;
            VlanRedCacheNode.u4ContextId = u4ContextId;

            if (NULL == pVlanRedCacheNode)
            {
                if (pVlanCurrEntry != NULL)
                {
                    if (pVlanCurrEntry->pStVlanEntry != NULL)
                    {
                        MEMCPY (&(VlanRedCacheNode.HwPortList.HwMemberPortList),
                                pVlanCurrEntry->pStVlanEntry->EgressPorts,
                                sizeof (tLocalPortList));
                        MEMCPY (&(VlanRedCacheNode.HwPortList.HwUntagPortList),
                                pVlanCurrEntry->pStVlanEntry->UnTagPorts,
                                sizeof (tLocalPortList));
                    }
                    MEMCPY (&(VlanRedCacheNode.HwPortList.HwFwdAllPortList),
                            pVlanCurrEntry->AllGrps.StaticPorts,
                            sizeof (tLocalPortList));
                    MEMCPY (&(VlanRedCacheNode.HwPortList.HwFwdUnregPortList),
                            pVlanCurrEntry->UnRegGrps.StaticPorts,
                            sizeof (tLocalPortList));
                }
                VlanRedAddCacheEntry (&VlanRedCacheNode);
                return VLAN_SUCCESS;
            }
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedStUcastCheckAndClearCache                */
/*                                                                      */
/* Description        : This function checks and clear the cache node   */
/*                      if the software configurations matches that of  */
/*                      NP programming.                                 */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedStUcastCheckAndClearCache (tVlanRedStUcastCacheNode * pVlanRedCacheNode)
{
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4FidIndex = 0;
    UINT2               u2LocalPortId = 0;

    /* This routine will be invoked when SW/update event is notified. 
     * By this time, the pending entry should be in h/w updated state. 
     * Else, the entry has to be marked as sync failed entry. 
     */

    if (0 != pVlanRedCacheNode->u4RcvPort)
    {

        if (VLAN_FAILURE ==
            VlanGetContextInfoFromIfIndex (pVlanRedCacheNode->u4RcvPort,
                                           &u4CurrContextId, &u2LocalPortId))
        {
            return VLAN_FAILURE;
        }

    }

    u4FidIndex = VlanGetFidEntryFromFdbId (pVlanRedCacheNode->u4FdbId);

    pVlanStUcastEntry =
        VlanGetStaticUcastEntry (u4FidIndex,
                                 pVlanRedCacheNode->MacAddr, u2LocalPortId);

    /* Avoid clearing of the pending entry when it is already marked as
     * VLAN_RED_SYNC_FAILED.
     */
    if (((VLAN_RED_SYNC_FAILED != pVlanRedCacheNode->i1RedSyncState) &&
         (NULL != pVlanStUcastEntry) &&
         (0 ==
          MEMCMP (pVlanRedCacheNode->AllowedToGoToPorts,
                  pVlanStUcastEntry->AllowedToGo, sizeof (tLocalPortList)))
         && (0 ==
             MEMCMP (&(pVlanRedCacheNode->ConnectionId),
                     pVlanStUcastEntry->ConnectionId, sizeof (tMacAddr))))
        || ((pVlanStUcastEntry == NULL)
            && (VLAN_RED_SYNC_HW_UPDATED == pVlanRedCacheNode->i1RedSyncState)
            && (VLAN_DELETE == pVlanRedCacheNode->i1Action)))
    {
        if (VLAN_SUCCESS ==
            VlanRedDelStUcastCacheEntry (pVlanRedCacheNode->u4ContextId,
                                         pVlanRedCacheNode->MacAddr,
                                         pVlanRedCacheNode->u4FdbId,
                                         pVlanRedCacheNode->u4RcvPort))
        {
            return VLAN_SUCCESS;
        }
    }

    return VLAN_FAILURE;
}

/************************************************************************/
/* Function Name      : VlanRedProcessStUcastCacheInfo                  */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
VlanRedProcessStUcastCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId,
                                UINT2 *pu2OffSet)
{
    tVlanRedStUcastCacheNode VlanRedCacheNode;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedStUcastCacheNode));
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Entering VlanRedProcessStUcastCacheInfo\r \n");

    VLAN_RM_GET_4_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.u4FdbId);
    VLAN_RM_GET_4_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.u4RcvPort);
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.AllowedToGoToPorts),
                        pu2OffSet, sizeof (tLocalPortList));
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.MacAddr),
                        pu2OffSet, sizeof (tMacAddr));
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.ConnectionId),
                        pu2OffSet, sizeof (tMacAddr));
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.u1Status);
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.i1RedSyncState);
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.i1Action);

    VlanRedCacheNode.u4ContextId = u4ContextId;
    if (VLAN_FAILURE == VlanRedProcessStUcastSyncInfo (&VlanRedCacheNode))
    {
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC, VLAN_NAME,
                  "Processing the pending entries for VLAN Hardware Audit Failed. \r \n");
        return;

    }
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedProcessCacheInfo\r \n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedProcessStUcastSyncInfo                           */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    VlanRedCacheTable.                                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier                        */
/*                    u1Flag - VLAN_NP_NOT_UPDATED - NP is not updated.       */
/*                             VLAN_NP_FAILED      - NP call has failed.      */
/*                             VLAN_NP_UPDATED     - NP update successful.    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedProcessStUcastSyncInfo (tVlanRedStUcastCacheNode * pUpdtVlanRedCacheNode)
{
    tVlanRedStUcastCacheNode *pVlanRedCacheNode = NULL;

    if (NULL !=
        (pVlanRedCacheNode =
         VlanRedGetStUcastCacheEntry (pUpdtVlanRedCacheNode->u4ContextId,
                                      pUpdtVlanRedCacheNode->MacAddr,
                                      pUpdtVlanRedCacheNode->u4FdbId,
                                      pUpdtVlanRedCacheNode->u4RcvPort)))
    {
        /* Case 1. This node is already observed to have a mismatch in s/w and h/w
         *         programming, which requires a mandate audit. Any further updates 
         *         are ignored.  
         */
        if (pVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_FAILED)
        {
            pVlanRedCacheNode->i1RedSyncState = VLAN_RED_SYNC_FAILED;
            return VLAN_FAILURE;
        }

        pVlanRedCacheNode->i1RedSyncState =
            pUpdtVlanRedCacheNode->i1RedSyncState;
        pVlanRedCacheNode->i1Action = pUpdtVlanRedCacheNode->i1Action;

        if (pVlanRedCacheNode->i1RedSyncState == VLAN_RED_SYNC_HW_UPDATED)
        {
            MEMCPY (&(pVlanRedCacheNode->AllowedToGoToPorts),
                    &(pUpdtVlanRedCacheNode->AllowedToGoToPorts),
                    sizeof (tLocalPortList));
            MEMCPY (&(pVlanRedCacheNode->ConnectionId),
                    &(pUpdtVlanRedCacheNode->ConnectionId), sizeof (tMacAddr));
            pVlanRedCacheNode->u1Status = pUpdtVlanRedCacheNode->u1Status;

            VlanRedStUcastCheckAndClearCache (pVlanRedCacheNode);
        }

    }
    else
    {
        VlanRedAddStUcastCacheEntry (pUpdtVlanRedCacheNode);
    }
    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name      : VlanRedCheckAndClearCache                       */
/*                                                                      */
/* Description        : This function checks and clear the cache node   */
/*                      if the software configurations matches that of  */
/*                      NP programming.                                 */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
VlanRedCheckAndClearCache (tVlanRedCacheNode * pVlanRedCacheNode)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    /* Avoid clearing of the pending entry when it is already marked as
     * VLAN_RED_SYNC_FAILED.
     */
    pVlanCurrEntry = VlanGetVlanEntry (pVlanRedCacheNode->VlanId);

    if ((VLAN_RED_SYNC_FAILED != pVlanRedCacheNode->i1RedSyncState) &&
        ((pVlanCurrEntry != NULL) && (pVlanCurrEntry->pStVlanEntry != NULL) &&
         ((0 ==
           MEMCMP (pVlanRedCacheNode->HwPortList.HwMemberPortList,
                   pVlanCurrEntry->pStVlanEntry->EgressPorts,
                   sizeof (tLocalPortList)))
          && (0 ==
              MEMCMP (pVlanRedCacheNode->HwPortList.HwUntagPortList,
                      pVlanCurrEntry->pStVlanEntry->UnTagPorts,
                      sizeof (tLocalPortList))))))
    {
        if (VLAN_SUCCESS ==
            VlanRedDelCacheEntry (pVlanRedCacheNode->u4ContextId,
                                  pVlanRedCacheNode->VlanId))
        {
            return VLAN_SUCCESS;
        }
    }

    return VLAN_FAILURE;
}

/************************************************************************/
/* Function Name      : VlanRedProcessCacheInfo                         */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Exceptions or Operating System Error Handling : None                */
/*                                                                      */
/*  Use of Recursion : None                                             */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
VlanRedProcessCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId, UINT2 *pu2OffSet)
{
    tVlanRedCacheNode   VlanRedCacheNode;

    MEMSET (&VlanRedCacheNode, 0, sizeof (tVlanRedCacheNode));
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Entering VlanRedProcessCacheInfo\r \n");

    VLAN_RM_GET_2_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.VlanId);
    VLAN_RM_GET_1_BYTE (pMsg, pu2OffSet, VlanRedCacheNode.i1RedSyncState);
    VLAN_RM_GET_N_BYTE (pMsg, &(VlanRedCacheNode.HwPortList),
                        pu2OffSet, sizeof (tHwVlanPortList));

    VlanRedCacheNode.u4ContextId = u4ContextId;
    if (VLAN_FAILURE == VlanRedProcessSyncInfo (&VlanRedCacheNode))
    {
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC, VLAN_NAME,
                  "Processing the pending entries for VLAN Hardware Audit Failed. \r \n");
        return;

    }
    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedProcessCacheInfo\r \n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSendStUcastCacheInfo                             */
/*                                                                            */
/*  Description     : This function will be invoked during the following steps*/
/*                    1) Before VLAN table update in NP.                      */
/*                    2) After successful NP update of VLAN table.            */
/*                    3) Successful SW update of VLAN table.                  */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanRedCacheNode      */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS, if successful                             */
/*                    VLAN_FAILURE, otherwise                                 */
/******************************************************************************/
INT4
VlanRedSendStUcastCacheInfo (tVlanRedStUcastCacheNode * pVlanRedCacheNode)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgAllocSize = 0;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_VLANGARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgAllocSize = VLAN_RED_ST_UCAST_CACHE_INFO_SIZE;
    u2MsgAllocSize = (u2MsgAllocSize < VLAN_RED_MAX_MSG_SIZE) ?
        u2MsgAllocSize : VLAN_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    VLAN Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 4B->|<-  4B   ->|<-szof(tLocalPortList)->|     *
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|FDB Id |<-RcvPort->|      AllowedPorts     |
     *    |           |         |          |       |           |                       |
     *    ------------------------------------------------------------------------------
     *
     *    ->|<-sizeof(MacAddress)->|<-sizeof(MacAddress)->|<- 1B ->|<-  1B  ->|
     *    --------------------------------------------------------------------
     *    | MacAddress             | ConnectionId         |u1Status|SyncState |
     *    |                        |                      |        |          |
     *    ---------------------------------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | OS_RESOURCE_TRC, VLAN_NAME,
                  "VlanRedSendStUcastCacheInfo: RM Memory allocation"
                  " failed\r\n");
        return VLAN_FAILURE;
    }

    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, VLAN_RED_SYNC_ST_UCAST_CACHE_INFO);
    VLAN_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgAllocSize);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4ContextId);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4FdbId);
    VLAN_RM_PUT_4_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u4RcvPort);
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->AllowedToGoToPorts),
                        &u2OffSet, sizeof (tLocalPortList));
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->MacAddr),
                        &u2OffSet, sizeof (tMacAddr));
    VLAN_RM_PUT_N_BYTE (pMsg, &(pVlanRedCacheNode->ConnectionId),
                        &u2OffSet, sizeof (tMacAddr));
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->u1Status);
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->i1RedSyncState);
    VLAN_RM_PUT_1_BYTE (pMsg, &u2OffSet, pVlanRedCacheNode->i1Action);

    if (VlanRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedSendStUcastCacheInfo:Send message to RM failed \r\n");
        return VLAN_FAILURE;
    }

    VLAN_TRC (VLAN_RED_TRC, VLAN_MOD_TRC |
              CONTROL_PLANE_TRC, VLAN_NAME,
              "Exiting VlanRedSendStUcastCacheInfo\r\n");
    return VLAN_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncPendingProtoVlanEntries                      */
/*                                                                            */
/*  Description     : This function performs audit of the VLAN current table  */
/*                    and VLAN memberships.                                   */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncPendingProtoVlanEntries (UINT4 u4Context)
{
    tVlanRedProtoVlanCacheNode VlanRedCacheInfo;
    tVlanRedProtoVlanCacheNode *pVlanRedCacheInfo = NULL;

    MEMSET (&VlanRedCacheInfo, 0, sizeof (tVlanRedProtoVlanCacheNode));

    VlanRedCacheInfo.u4ContextId = u4Context;
    while ((NULL !=
            (pVlanRedCacheInfo =
             RBTreeGetNext (gVlanRedInfo.VlanRedProtoVlanCacheTable,
                            &VlanRedCacheInfo, NULL))))

    {
        if (pVlanRedCacheInfo->u4ContextId != u4Context)
        {
            break;
        }
        VlanRedAuditProtoVlanTable (&(pVlanRedCacheInfo->VlanProtoTemplate),
                                    u4Context, pVlanRedCacheInfo->u4GroupId,
                                    pVlanRedCacheInfo->u2Port);
        VlanRedDelProtoVlanCache (pVlanRedCacheInfo->u4ContextId,
                                  pVlanRedCacheInfo->u4GroupId,
                                  pVlanRedCacheInfo->u2Port);
    }
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedSyncPendingVlanEntries                                    */
/*                                                                            */
/*  Description     : This function performs audit of the VLAN current table  */
/*                    and VLAN memberships.                                   */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedSyncPendingVlanEntries (UINT4 u4Context)
{
    tVlanRedCacheNode   VlanRedCacheInfo;
    tVlanRedCacheNode  *pVlanRedCacheInfo = NULL;
    UINT1               u1Status = OSIX_FALSE;

    MEMSET (&VlanRedCacheInfo, 0, sizeof (tVlanRedCacheNode));

    /* The full audit flag is set on for VLAN Port Membership table. 
     * Hence perform complete audit for VLAN Table. 
     */
    if (gVlanRedInfo.u1ForceFullAudit & VLAN_RED_VLAN_TABLE_MASK)
    {
        VlanRedSyncVlanTable (u4Context);
        gVlanRedInfo.u1ForceFullAudit = ~VLAN_RED_VLAN_TABLE_MASK;
        return;
    }

    VlanL2IwfGetAllToOneBndlStatus (u4Context, &u1Status);

    /* All to one bundling status is set, so no need to do audit for the
       Vlan Table. This is because if PISID is configured on any CNP, then we
       assume no VLAN are not configured for that component */
    if (u1Status == OSIX_TRUE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         CONTROL_PLANE_TRC,
                         "All to one bundling status set"
                         "No audit for Vlan Table \n ");
        RBTreeDrain (gVlanRedInfo.VlanRedCacheTable, VlanRedCacheFreeNodes, 0);
        return;
    }

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        return;
    }

    VlanRedCacheInfo.u4ContextId = u4Context;
    while ((NULL !=
            (pVlanRedCacheInfo = RBTreeGetNext (gVlanRedInfo.VlanRedCacheTable,
                                                &VlanRedCacheInfo, NULL))))

    {
        if (pVlanRedCacheInfo->u4ContextId != u4Context)
        {
            break;
        }
        VlanRedAuditVlanTable (u4Context, pVlanRedCacheInfo->VlanId);
        VlanRedDelCacheEntry (pVlanRedCacheInfo->u4ContextId,
                              pVlanRedCacheInfo->VlanId);
    }
    return;
}

#ifdef NP_BACKWD_COMPATIBILITY
/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanWrRedVlanMcastCb                                    */
/*                                                                            */
/*  Description     : This function will be invoked for each multicast entry  */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same mcast entry in the software. If        */
/*                    present this fn. corrects the mcast membership. If not  */
/*                    present this fn. intimates GARP to add the mcast entry  */
/*                    to its software database.                               */
/*                                                                            */
/*  Input(s)        : VlanId - VLAN Id of the group                           */
/*                    McastAddr - Multicast address                           */
/*                    u2RcvPort - Receive port parameter                      */
/*                    HwPortList - Mcast egress ports in hardware.            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanWrRedVlanMcastCb (tVlanId VlanId, tMacAddr McastAddr, UINT2 u2RcvPort,
                      tPortList HwPortList)
{
    VlanRedVlanMcastCb (VlanId, McastAddr, u2RcvPort, HwPortList);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanWrRedVlanUcastCb                                    */
/*                                                                            */
/*  Description     : This function will be invoked for each unicast entry    */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same  st. ucast entry in the software. If   */
/*                    present this fn. corrects the ucast membership. If not  */
/*                    present this fn. deletes the static ucast entry from    */
/*                    the hardware. If S/w learning is enabled, this fn.      */
/*                    invokes fns. to add dynamic FDB into the table.         */
/*                                                                            */
/*  Input(s)        : u4FdbId - Filtering database identifier                 */
/*                    MacAddr - Unicast mac address                           */
/*                    u2RcvPort - Receive port parameter                      */
/*                    pUcastEntry - Pointer to the unicast entry.             */
/*                    AllowedList - AllowedToGo bitmap in H/W. Valid only for */
/*                    static unicast entries.                                 */
/*                    u1Status - Indicates the status of the entry. Valid     */
/*                    only for static entries.                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanWrRedVlanUcastCb (UINT4 u4FdbId, tMacAddr MacAddr, UINT2 u2RcvPort,
                      tHwUnicastMacEntry * pUcastEntry,
                      tPortList AllowedList, UINT1 u1Status)
{
    VlanRedVlanUcastCb (u4FdbId, MacAddr, u2RcvPort,
                        pUcastEntry, AllowedList, u1Status);
}

#else
/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanWrRedVlanMcastCb                                    */
/*                                                                            */
/*  Description     : This function will be invoked for each multicast entry  */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same mcast entry in the software. If        */
/*                    present this fn. corrects the mcast membership. If not  */
/*                    present this fn. intimates GARP to add the mcast entry  */
/*                    to its software database.                               */
/*                                                                            */
/*  Input(s)        : VlanId - VLAN Id of the group                           */
/*                    McastAddr - Multicast address                           */
/*                    u2RcvPort - Receive port parameter                      */
/*                    pHwPortArray - Mcast egress ports in hardware.          */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanWrRedVlanMcastCb (tVlanId VlanId, tMacAddr McastAddr, UINT4 u4RcvPort,
                      tHwPortArray * pHwPortArray)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPort = 0;
    tLocalPortList      HwPortList;

    VLAN_MEMSET (HwPortList, 0, sizeof (tLocalPortList));

    if (VlanConvertPortArrayToLocalPortList
        (pHwPortArray->pu4PortArray, pHwPortArray->i4Length,
         HwPortList) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC,
                         "Conversion of ports tp local port failed \n");

        return;
    }

    if (u4RcvPort != 0)
    {
        if (VlanGetContextInfoFromIfIndex (u4RcvPort, &u4ContextId,
                                           &u2LocalPort) != VLAN_SUCCESS)
        {
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             ALL_FAILURE_TRC,
                             "Failed to get local port from Ifindex \n");
            return;
        }
    }

    VlanRedVlanMcastCb (VlanId, McastAddr, u2LocalPort, HwPortList);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanWrRedVlanUcastCb                                    */
/*                                                                            */
/*  Description     : This function will be invoked for each unicast entry    */
/*                    present in the hardware. This function checks for the   */
/*                    presence of same  st. ucast entry in the software. If   */
/*                    present this fn. corrects the ucast membership. If not  */
/*                    present this fn. deletes the static ucast entry from    */
/*                    the hardware. If S/w learning is enabled, this fn.      */
/*                    invokes fns. to add dynamic FDB into the table.         */
/*                                                                            */
/*  Input(s)        : u4FdbId - Filtering database identifier                 */
/*                    MacAddr - Unicast mac address                           */
/*                    u2RcvPort - Receive port parameter                      */
/*                    pUcastEntry - Pointer to the unicast entry.             */
/*                    pHwPortArray- AllowedToGo bitmap in H/W. Valid only for */
/*                    static unicast entries.                                 */
/*                    u1Status - Indicates the status of the entry. Valid     */
/*                    only for static entries.                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanWrRedVlanUcastCb (UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort,
                      tHwUnicastMacEntry * pUcastEntry,
                      tHwPortArray * pHwPortArray, UINT1 u1Status)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPort = 0;
    tLocalPortList      HwPortList;

    VLAN_MEMSET (HwPortList, 0, sizeof (tLocalPortList));

    if (VlanConvertPortArrayToLocalPortList
        (pHwPortArray->pu4PortArray, pHwPortArray->i4Length,
         HwPortList) != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                         ALL_FAILURE_TRC, "Conversion of ports to"
                         " local port failed \n");

        return;
    }

    if (u4RcvPort != 0)
    {
        if (VlanGetContextInfoFromIfIndex (u4RcvPort, &u4ContextId,
                                           &u2LocalPort) != VLAN_SUCCESS)
        {
            VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_RED_TRC,
                             ALL_FAILURE_TRC,
                             "Failed to get local port from Ifindex \n");

            return;
        }
    }
    VlanRedVlanUcastCb (u4FdbId, MacAddr, u2LocalPort,
                        pUcastEntry, HwPortList, u1Status);

    return;
}
#endif /* NP_BACKWD_COMPATIBILITY */
#endif /* NPAPI_WANTED */
#endif /* L2RED_WANTED */
