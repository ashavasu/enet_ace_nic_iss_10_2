/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: fsm1adwr.c,v 1.8 2013/12/05 12:44:53 siva Exp $
* Description: This file contains the wrapper routines in VLAN Modules.
****************************************************************************/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsm1adwr.h"
# include  "fsm1addb.h"

INT4
GetNextIndexDot1adMIPortTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMIPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMIPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSM1AD ()
{
    SNMPRegisterMibWithLock (&fsm1adOID, &fsm1adEntry, VlanLock, VlanUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsm1adOID, (const UINT1 *) "fsm1ad");
}

VOID
UnRegisterFSM1AD ()
{
    SNMPUnRegisterMib (&fsm1adOID, &fsm1adEntry);
    SNMPDelSysorEntry (&fsm1adOID, (const UINT1 *) "fsm1ad");
}

INT4
Dot1adMIPortPcpSelectionRowGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPortPcpSelectionRow
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPortUseDeiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPortUseDei (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationRelayCVidGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationRelayCVid
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPortReqDropEncodingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPortReqDropEncoding
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPortSVlanPriorityTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPortSVlanPriorityType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPortSVlanPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPortSVlanPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPortPcpSelectionRowSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPortPcpSelectionRow
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortUseDeiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPortUseDei (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortReqDropEncodingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPortReqDropEncoding
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortSVlanPriorityTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPortSVlanPriorityType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortSVlanPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPortSVlanPriority
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortPcpSelectionRowTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPortPcpSelectionRow (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortUseDeiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPortUseDei (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortReqDropEncodingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPortReqDropEncoding (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortSVlanPriorityTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPortSVlanPriorityType (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortSVlanPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPortSVlanPriority (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMIPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Dot1adMICVidRegistrationRelayCVidTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationRelayCVid (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
GetNextIndexDot1adMIVidTranslationTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMIVidTranslationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMIVidTranslationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1adMIVidTranslationRelayVidGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIVidTranslationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIVidTranslationRelayVid
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIVidTranslationRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIVidTranslationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIVidTranslationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIVidTranslationRelayVidSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetDot1adMIVidTranslationRelayVid
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIVidTranslationRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetDot1adMIVidTranslationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIVidTranslationRelayVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIVidTranslationRelayVid (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Dot1adMIVidTranslationRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIVidTranslationRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Dot1adMIVidTranslationTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMIVidTranslationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1adMICVidRegistrationTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMICVidRegistrationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMICVidRegistrationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1adMICVidRegistrationSVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationSVid
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationUntaggedPepGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationUntaggedPep
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationUntaggedCepGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationUntaggedCep
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationSVlanPriorityTypeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationSVlanPriorityType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationSVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationSVlanPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMICVidRegistrationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMICVidRegistrationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMICVidRegistrationSVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationSVid
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationUntaggedPepSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationUntaggedPep
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationUntaggedCepSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationUntaggedCep
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationSVlanPriorityTypeSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationSVlanPriorityType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationSVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationSVlanPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationSVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationSVid (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationUntaggedPepTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationUntaggedPep (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationUntaggedCepTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationUntaggedCep (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationSVlanPriorityTypeTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationSVlanPriorityType (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                i4_SLongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationSVlanPriorityTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationSVlanPriority (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMICVidRegistrationRowStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Dot1adMICVidRegistrationRelayCVidSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetDot1adMICVidRegistrationRelayCVid
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
Dot1adMICVidRegistrationTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMICVidRegistrationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1adMIPepTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMIPepTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMIPepTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1adMIPepPvidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPepTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPepPvid (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPepDefaultUserPriorityGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPepTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPepDefaultUserPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPepAccptableFrameTypesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPepTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPepAccptableFrameTypes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPepIngressFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPepTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPepIngressFiltering
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPepPvidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPepPvid (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPepDefaultUserPrioritySet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPepDefaultUserPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPepAccptableFrameTypesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPepAccptableFrameTypes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPepIngressFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPepIngressFiltering
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPepPvidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPepPvid (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPepDefaultUserPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPepDefaultUserPriority (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Dot1adMIPepAccptableFrameTypesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPepAccptableFrameTypes (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Dot1adMIPepIngressFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPepIngressFiltering (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPepTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMIPepTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1adMIServicePriorityRegenerationTable (tSnmpIndex *
                                                      pFirstMultiIndex,
                                                      tSnmpIndex *
                                                      pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMIServicePriorityRegenerationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMIServicePriorityRegenerationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1adMIServicePriorityRegenRegeneratedPriorityGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIServicePriorityRegenerationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIServicePriorityRegenRegeneratedPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIServicePriorityRegenRegeneratedPrioritySet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhSetDot1adMIServicePriorityRegenRegeneratedPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIServicePriorityRegenRegeneratedPriorityTest (UINT4 *pu4Error,
                                                     tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIServicePriorityRegenRegeneratedPriority (pu4Error,
                                                                      pMultiIndex->
                                                                      pIndex[0].
                                                                      i4_SLongValue,
                                                                      pMultiIndex->
                                                                      pIndex[1].
                                                                      i4_SLongValue,
                                                                      pMultiIndex->
                                                                      pIndex[2].
                                                                      i4_SLongValue,
                                                                      pMultiData->
                                                                      i4_SLongValue));

}

INT4
Dot1adMIServicePriorityRegenerationTableDep (UINT4 *pu4Error,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMIServicePriorityRegenerationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1adMIPcpDecodingTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMIPcpDecodingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMIPcpDecodingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1adMIPcpDecodingPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPcpDecodingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPcpDecodingPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPcpDecodingDropEligibleGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPcpDecodingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPcpDecodingDropEligible
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPcpDecodingPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPcpDecodingPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPcpDecodingDropEligibleSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPcpDecodingDropEligible
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPcpDecodingPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPcpDecodingPriority (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPcpDecodingDropEligibleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPcpDecodingDropEligible (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Dot1adMIPcpDecodingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMIPcpDecodingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1adMIPcpEncodingTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1adMIPcpEncodingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1adMIPcpEncodingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1adMIPcpEncodingPcpValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1adMIPcpEncodingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1adMIPcpEncodingPcpValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1adMIPcpEncodingPcpValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1adMIPcpEncodingPcpValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPcpEncodingPcpValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Dot1adMIPcpEncodingPcpValue (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[3].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Dot1adMIPcpEncodingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1adMIPcpEncodingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
