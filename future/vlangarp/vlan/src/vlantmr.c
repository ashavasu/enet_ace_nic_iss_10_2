/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlantmr.c,v 1.45 2016/02/21 09:55:33 siva Exp $
 *
 * Description     : This file contains util routines for VLAN Timer 
 *
 *******************************************************************/

/*                                                                           */
/*  FILE NAME             : vlantmr.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN timer realted routines.  */
/*****************************************************************************/

#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAgeoutStMcastEntries                         */
/*                                                                           */
/*    Description         : This function ageout the entries in Static       */
/*                          Multicast table when timer expires.              */
/*                                                                           */
/*    Input(s)            : pVlanEntry  - Pointer to the Vlan Entry          */
/*                          u4CurrTime  - Current Time                       */
/*                          u2AgeoutInt - AgeOut Time                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanAgeoutStMcastEntries (tVlanCurrEntry * pVlanEntry, UINT4 u4CurrTime,
                          UINT2 u2AgeoutInt)
{
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStMcastEntry  *pNextStMcastEntry = NULL;
    UINT1              *pAddedPorts = NULL;
    tPortList          *pEgressPorts = NULL;
    tPortList          *pForbiddenPorts = NULL;
    UINT4               u4TimeDiff;

    pStMcastEntry = pVlanEntry->pStMcastTable;
    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanAgeoutStMcastEntries: "
                  "Error in allocating memory for pEgressPorts\r\n");
        return;
    }
    MEMSET (*pEgressPorts, 0, sizeof (tPortList));

    pForbiddenPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbiddenPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanAgeoutStMcastEntries: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return;
    }
    MEMSET (*pForbiddenPorts, 0, sizeof (tPortList));
    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddedPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRedHandleMcastAddrDelOnTimeOut: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
        return;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));

    while (pStMcastEntry != NULL)
    {

        pNextStMcastEntry = pStMcastEntry->pNextNode;

        if (pStMcastEntry->u1Status == VLAN_DELETE_ON_TIMEOUT)
        {

            u4TimeDiff = u4CurrTime - pStMcastEntry->u4TimeStamp;

            if (u4TimeDiff >= u2AgeoutInt)
            {
                /* Propagate Mcast information to other modules only when 
                 * receive port is zero. */
                if (VLAN_DEF_RECVPORT == pStMcastEntry->u2RcvPort)
                {
                    if ((VlanGmrpIsGmrpEnabledInContext
                         (VLAN_CURR_CONTEXT_ID ()) == GMRP_TRUE) ||
                        (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ())
                         == OSIX_TRUE))
                    {
                        VlanAttrRPSetMcastForbiddPorts (VLAN_CURR_CONTEXT_ID (),
                                                        pStMcastEntry->MacAddr,
                                                        pVlanEntry->VlanId,
                                                        pAddedPorts,
                                                        pStMcastEntry->
                                                        ForbiddenPorts);

                        VlanPropagateMacInfoToAttrRP
                            (VLAN_CURR_CONTEXT_ID (), pStMcastEntry->MacAddr,
                             pVlanEntry->VlanId, pAddedPorts,
                             pStMcastEntry->EgressPorts);
                    }

                    if (SNOOP_ENABLED == VlanSnoopIsIgmpSnoopingEnabled
                        (VLAN_CURR_CONTEXT_ID ()) ||
                        SNOOP_ENABLED == VlanSnoopIsMldSnoopingEnabled
                        (VLAN_CURR_CONTEXT_ID ()))
                    {
                        /* Mcast Forbid Ports needs to be added now since entry 
                         * is deleted */
                        /* For MI the port list to the externa modules should be 
                           physical port list so convert and pass it */
                        MEMSET (*pEgressPorts, 0, sizeof (tPortList));
                        MEMSET (*pForbiddenPorts, 0, sizeof (tPortList));

                        VlanConvertToIfPortList (pStMcastEntry->ForbiddenPorts,
                                                 *pForbiddenPorts);
                        VlanConvertToIfPortList (pAddedPorts, *pEgressPorts);

                        VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                                   pVlanEntry->VlanId,
                                                   pStMcastEntry->MacAddr,
                                                   *pForbiddenPorts,
                                                   *pEgressPorts,
                                                   VLAN_UPDT_MCAST_PORTS);
                    }

                }
                VlanRedSyncMcastAddressDeletion (VLAN_CURR_CONTEXT_ID (),
                                                 pVlanEntry->VlanId,
                                                 pStMcastEntry->MacAddr,
                                                 pStMcastEntry->u2RcvPort);
                VlanDeleteStMcastEntry (pVlanEntry, pStMcastEntry, VLAN_TRUE);
            }
        }

        pStMcastEntry = pNextStMcastEntry;
    }
    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
    UtilPlstReleaseLocalPortList (pAddedPorts);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAgeoutStUcastEntries                         */
/*                                                                           */
/*    Description         : This function ageout the entries in the Static   */
/*                          Unicast table when timer value expires.          */
/*                                                                           */
/*    Input(s)            : u4FidIndex  - FidIndex                           */
/*                          u4CurrTime  - Current Time                       */
/*                          u2AgeoutInt - AgeOut Time                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanAgeoutStUcastEntries (UINT4 u4FidIndex, UINT4 u4CurrTime, UINT2 u2AgeoutInt)
{
    UINT4               u4TimeDiff;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanStUcastEntry  *pNextStUcastEntry = NULL;

    pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

    pStUcastEntry = pFidEntry->pStUcastTbl;

    while (pStUcastEntry != NULL)
    {

        pNextStUcastEntry = pStUcastEntry->pNextNode;
        if (pStUcastEntry->u1Status == VLAN_DELETE_ON_TIMEOUT)
        {

            u4TimeDiff = u4CurrTime - pStUcastEntry->u4TimeStamp;

            if (u4TimeDiff >= u2AgeoutInt)
            {
                /* Notify MRP module about Static Unicast Entry deletion.  
                 */
                if ((VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ())
                     == OSIX_TRUE) &&
                    (VLAN_DEF_RECVPORT == pStUcastEntry->u2RcvPort))
                {
                    VlanPropagateMacInfoToAttrRP
                        (VLAN_CURR_CONTEXT_ID (), pStUcastEntry->MacAddr,
                         pStUcastEntry->pFdbEntry->VlanId, gNullPortList,
                         pStUcastEntry->AllowedToGo);
                }

#ifdef NPAPI_WANTED
                /* Delete the Static Entry added in H/W */
                VlanHwDelStaticUcastEntry (pFidEntry->u4Fid,
                                           pStUcastEntry->MacAddr,
                                           pStUcastEntry->u2RcvPort);

#else
                if (pStUcastEntry->pFdbEntry->u1StRefCount-- == 1)
                {
                    pStUcastEntry->pFdbEntry->u1Status = VLAN_FDB_LEARNT;
                }

#endif

                /* 
                 * The corresponding FDB entry in the FDB database will be deleted 
                 * once we age out the FDB entries.
                 */
                VlanRedSyncUcastAddressDeletion (VLAN_CURR_CONTEXT_ID (),
                                                 u4FidIndex,
                                                 pStUcastEntry->MacAddr,
                                                 pStUcastEntry->u2RcvPort);

                VlanDeleteStUcastEntry (pFidEntry, pStUcastEntry);
            }
        }

        pStUcastEntry = pNextStUcastEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAgeoutFdbEntries                             */
/*                                                                           */
/*    Description         : This function ageout the Fdb entires in Fdb      */
/*                          table when timer value expires.                  */
/*                                                                           */
/*    Input(s)            : u4CurrTime  - Current Time                       */
/*                          u2AgeoutInt - AgeOut Time                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanAgeoutFdbEntries (UINT4 u4CurrTime, UINT2 u2AgeoutInt)
{
    UINT2               u2HashIndex;
    UINT4               u4FidIndex;
    UINT4               u4TimeDiff;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;
#ifdef NPAPI_WANTED
    tHwUnicastMacEntry  HwUnicastMacEntry;
#endif

    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry == NULL)
        {
            if (VLAN_LEARNING_MODE () == VLAN_SHARED_LEARNING)
            {
                return;
            }

            continue;
        }
#ifdef SW_AGE_TIMER
        if (gpVlanContextInfo->VlanInfo.bUseShortAgingTime != VLAN_TRUE)
        {
#endif
            pStUcastEntry = pFidEntry->pStUcastTbl;

            if (pStUcastEntry != NULL)
            {
                VlanAgeoutStUcastEntries (u4FidIndex, u4CurrTime, u2AgeoutInt);
            }
#ifdef SW_AGE_TIMER
        }
#endif
#if defined (SW_AGE_TIMER) && defined (NPAPI_WANTED)
        VlanFsMiVlanNpHwRunMacAgeing (VLAN_CURR_CONTEXT_ID ());
#endif
        for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
        {

            pFdbEntry = VLAN_GET_FDB_ENTRY (u4FidIndex, u2HashIndex);

            while (pFdbEntry != NULL)
            {

                pNextFdbEntry = pFdbEntry->pNextHashNode;

                u4TimeDiff = u4CurrTime - pFdbEntry->u4TimeStamp;

                if (u4TimeDiff >= u2AgeoutInt)
                {
#ifdef NPAPI_WANTED
                    if (IssCPUControlledLearning (pFdbEntry->u2Port)
                        == OSIX_SUCCESS)
                    {
                        VlanHwGetFdbEntry (VLAN_CURR_CONTEXT_ID (),
                                           pFidEntry->u4Fid,
                                           pFdbEntry->MacAddr,
                                           &HwUnicastMacEntry);

                        if (HwUnicastMacEntry.u1HitStatus == 0)
                        {
                            VlanHwDelStaticUcastEntry (pFidEntry->u4Fid,
                                                       pFdbEntry->MacAddr,
                                                       VLAN_DEF_RECVPORT);
                        }
                    }
#endif
                    if ((pFdbEntry->u1StRefCount == 0) &&
                        (pFdbEntry->u1Status == VLAN_FDB_LEARNT))
                    {
                        /*No Static entries present */
                        VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
                    }
                    else
                    {
                        pFdbEntry->u2Port = VLAN_INVALID_PORT;
                    }
                }
                pFdbEntry = pNextFdbEntry;
            }
        }
        if (VLAN_LEARNING_MODE () == VLAN_SHARED_LEARNING)
        {
            return;
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessAgeoutTimerExpiry                     */
/*                                                                           */
/*    Description         : This function process the ageout timer and call  */
/*                          table ageout entires.                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanProcessAgeoutTimerExpiry (VOID)
{
    UINT4               u4AgeoutInt;
    UINT4               u4CurrTime;
    UINT4               u4AgeoutInTicks;
    tVlanId             VlanId;
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        return;
    }

    VLAN_GET_TIMESTAMP (&u4CurrTime);

#ifdef SW_AGE_TIMER
    if (gpVlanContextInfo->VlanInfo.bUseShortAgingTime == VLAN_TRUE)
    {
        u4AgeoutInt = (UINT4) gpVlanContextInfo->VlanInfo.i4ShortAgingTime;
        u4AgeoutInTicks = (u4AgeoutInt * SYS_TIME_TICKS_IN_A_SEC);
    }
    else
#endif
    {
        /* Do not use the short ageout timer for aging out static multicast
         * entries */

        u4AgeoutInt = VlanL2IwfGetAgeoutInt ();

        u4AgeoutInTicks = (u4AgeoutInt * SYS_TIME_TICKS_IN_A_SEC);

        u4AgeoutInt = VLAN_SPLIT_AGEOUT_TIME (u4AgeoutInt);

        /* when ISSU is in MM  donot flush the Mac entries */
        if (IssuGetMaintModeOperation() == OSIX_FALSE)
        {
            VLAN_SCAN_VLAN_TABLE (VlanId)
            {
                pVlanEntry = VlanGetVlanEntry (VlanId);
                if (NULL == pVlanEntry)
                {
                    continue;
                }

                if (pVlanEntry->pStMcastTable != NULL)
                {
                    VlanAgeoutStMcastEntries (pVlanEntry, u4CurrTime,
                            (UINT2) u4AgeoutInTicks);
                }
            }
        }
    }

    /* When ISSU is in MM  donot Ageout FDB entries */
    if (IssuGetMaintModeOperation() == OSIX_FALSE)
    {
        VlanAgeoutFdbEntries (u4CurrTime, (UINT2) u4AgeoutInTicks);
    }
    else
    {
        /* when ISSU maintenance mode is enabled 
         * restart the vlan ageing timer with ISSU fixed interval */
        u4AgeoutInt = u4AgeoutInt + ISSU_TIMER_VALUE;
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VlanProcessAgeoutTimerExpiry: "
                  "ISSU is in progress, restarting Vlan Ageing Timer\r\n");
    }

    /*
     * VLAN Runs only one timer for all dynamic and static entries including
     * FDB entries, static unicast and multicast entries.
     * VLAN compares the current time and the time difference to ageout the 
     * entries in the VLAN database.
     */

    VLAN_START_TIMER (VLAN_AGEOUT_TIMER, u4AgeoutInt);
}

/************************************************************************/
/* Function Name    : VlanStartTimer ()                                 */
/*                                                                      */
/* Description      : Starts the Vlan timer of the given duration       */
/*                                                                      */
/* Input(s)         : u4TimeOut - The duration of the timer in seconds  */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpVlanContextInfo->VlanAgeOutTimer                                  */
/*                    gVlanTaskInfo                                     */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpVlanContextInfo->VlanAgeOutTimer                                  */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VLAN_SUCCESS, if timer is started successfully    */
/*                    VLAN_FAILURE otherwise                            */
/************************************************************************/
INT4
VlanStartTimer (UINT1 u1TimerType, UINT4 u4TimeOut)
{
    UINT4               u4RetVal;
    tVlanTimer         *pVlanTimer;

    if ((VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
        && (u1TimerType != VLAN_MBSM_TIMER))
    {
        /*Start the timers only in Active Node */
        return VLAN_SUCCESS;
    }

    switch (u1TimerType)
    {

        case VLAN_AGEOUT_TIMER:

            gpVlanContextInfo->VlanAgeOutTimer.u1TmrType = VLAN_AGEOUT_TIMER;

            u4TimeOut = (u4TimeOut * SYS_TIME_TICKS_IN_A_SEC);

            pVlanTimer = &(gpVlanContextInfo->VlanAgeOutTimer);
            pVlanTimer->pVlanContextInfo = VLAN_CURR_CONTEXT_PTR ();

            break;
#ifndef NPAPI_WANTED
        case VLAN_PRIORITY_TIMER:

            gpVlanContextInfo->VlanPriTimer.u1TmrType = VLAN_PRIORITY_TIMER;

            pVlanTimer = &(gpVlanContextInfo->VlanPriTimer);
            pVlanTimer->pVlanContextInfo = VLAN_CURR_CONTEXT_PTR ();

            break;
#endif

#ifdef L2RED_WANTED
        case VLAN_RED_RELEARN_TIMER:
            gVlanRedInfo.RelearnTimer.u1TmrType = VLAN_RED_RELEARN_TIMER;

            pVlanTimer = &(gVlanRedInfo.RelearnTimer);
            pVlanTimer->pVlanContextInfo = VLAN_CURR_CONTEXT_PTR ();

            u4TimeOut = (u4TimeOut * SYS_TIME_TICKS_IN_A_SEC);
            break;

        case VLAN_RED_PERIODIC_TIMER:
            gVlanRedInfo.PeriodicTimer.u1TmrType = VLAN_RED_PERIODIC_TIMER;

            pVlanTimer = &(gVlanRedInfo.PeriodicTimer);
            pVlanTimer->pVlanContextInfo = VLAN_CURR_CONTEXT_PTR ();

            u4TimeOut = (u4TimeOut * SYS_TIME_TICKS_IN_A_SEC);
            break;
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
        case VLAN_MBSM_TIMER:
            gpVlanContextInfo->VlanMbsmTimer.u1TmrType = VLAN_MBSM_TIMER;

            pVlanTimer = &(gpVlanContextInfo->VlanMbsmTimer);
            pVlanTimer->pVlanContextInfo = VLAN_CURR_CONTEXT_PTR ();
            break;

#endif
        default:
            return VLAN_FAILURE;
    }

    u4RetVal = VLAN_TMR_START (gVlanTaskInfo.VlanTmrListId,
                               &(pVlanTimer->TmrNode), u4TimeOut);

    if (u4RetVal == TMR_SUCCESS)
    {
        pVlanTimer->u1IsTmrActive = VLAN_TRUE;
        return VLAN_SUCCESS;
    }

    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
              "VLAN Timer Start Failed.\n");

    pVlanTimer->u1IsTmrActive = VLAN_FALSE;

    return VLAN_FAILURE;
}

/************************************************************************/
/* Function Name    : VlanStopTimer ()                                  */
/*                                                                      */
/* Description      : Stops the Vlan Timer                              */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpVlanContextInfo->VlanAgeOutTimer                                  */
/*                    gVlanTaskInfod                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpVlanContextInfo->VlanAgeOutTimer                                  */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VLAN_SUCCESS if timer is stopped successfully     */
/*                    VLAN_FAILURE otherwise                            */
/************************************************************************/
INT4
VlanStopTimer (UINT1 u1TimerType)
{
    UINT4               u4RetVal;
    tVlanTimer         *pVlanTimer;

    switch (u1TimerType)
    {

        case VLAN_AGEOUT_TIMER:

            pVlanTimer = &(gpVlanContextInfo->VlanAgeOutTimer);

            break;

#ifndef NPAPI_WANTED
        case VLAN_PRIORITY_TIMER:

            pVlanTimer = &(gpVlanContextInfo->VlanPriTimer);

            break;
#endif

#ifdef L2RED_WANTED
        case VLAN_RED_RELEARN_TIMER:

            pVlanTimer = &(gVlanRedInfo.RelearnTimer);
            break;

        case VLAN_RED_PERIODIC_TIMER:

            pVlanTimer = &(gVlanRedInfo.PeriodicTimer);
            break;
#endif /* L2RED_WANTED */

        default:

            return VLAN_FAILURE;
    }

    if (pVlanTimer->u1IsTmrActive == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    u4RetVal = VLAN_TMR_STOP (gVlanTaskInfo.VlanTmrListId,
                              &(pVlanTimer->TmrNode));

    if (u4RetVal == TMR_SUCCESS)
    {
        pVlanTimer->u1IsTmrActive = VLAN_FALSE;
        return VLAN_SUCCESS;
    }

    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
              "VLAN Timer Stop Failed.\n");

    return VLAN_FAILURE;
}

/************************************************************************/
/* Function Name    : VlanProcessTimerEvent ()                          */
/*                                                                      */
/* Description      : Process the timer expiry event                    */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gVlanTaskInfo                                     */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
VlanProcessTimerEvent (VOID)
{
    tTmrAppTimer       *pTmrBlock;
    tVlanTimer         *pVlanTimer;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return;
    }
    pTmrBlock = TmrGetNextExpiredTimer (gVlanTaskInfo.VlanTmrListId);

    while (pTmrBlock != NULL)
    {
        pVlanTimer = (tVlanTimer *) pTmrBlock;

        if (VlanSelectContext (pVlanTimer->pVlanContextInfo->u4ContextId) ==
            VLAN_FAILURE)
        {
            pTmrBlock = TmrGetNextExpiredTimer (gVlanTaskInfo.VlanTmrListId);
            continue;
        }
        switch (pVlanTimer->u1TmrType)
        {
            case VLAN_AGEOUT_TIMER:

                VlanProcessAgeoutTimerExpiry ();

                break;
#ifndef NPAPI_WANTED
            case VLAN_PRIORITY_TIMER:

                VlanDrainAllPriQ ();

                break;
#endif

#ifdef L2RED_WANTED
            case VLAN_RED_RELEARN_TIMER:

                VlanRedHandleRelearnTmrExpiry ();
                break;

            case VLAN_RED_PERIODIC_TIMER:

                VlanRedHandlePeriodicTmrExpiry ();
                break;
#endif /* L2RED_WANTED */

#ifdef MBSM_WANTED
            case VLAN_MBSM_TIMER:
                VlanMbsmUpdtPortPropertiesToHw (&gMbsmPortInfo, &gMbsmSlotInfo);
                break;
#endif

            default:

                break;
        }
        VlanReleaseContext ();
        pTmrBlock = TmrGetNextExpiredTimer (gVlanTaskInfo.VlanTmrListId);
    }
}
