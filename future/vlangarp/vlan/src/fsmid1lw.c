/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmid1lw.c,v 1.10 2016/07/22 06:46:31 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
#include   "vlaninc.h"

/* LOW LEVEL Routines for Table : FsMIEvbSystemTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEvbSystemTable
 Input       :  The Indices
                FsMIEvbSystemContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEvbSystemTable (INT4 i4FsMIEvbSystemContextId)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */
    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        return SNMP_FAILURE;
    }   

    /* Getting Context information for the given context */
    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEvbSystemTable
 Input       :  The Indices
                FsMIEvbSystemContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEvbSystemTable (INT4 *pi4FsMIEvbSystemContextId)
{  
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Getting Context information for the given context */
    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[0];

    if (NULL == pEvbContextInfo)
    {
        return SNMP_FAILURE;
    }
    *pi4FsMIEvbSystemContextId = (INT4) 
            (gEvbGlobalInfo.apEvbContextInfo[0])->u4EvbSysCxtId;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEvbSystemTable
 Input       :  The Indices
                FsMIEvbSystemContextId
                nextFsMIEvbSystemContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEvbSystemTable (INT4 i4FsMIEvbSystemContextId,
              INT4 *pi4NextFsMIEvbSystemContextId )
{
    tEvbContextInfo  *pEvbContextInfo = NULL;
    INT4              i4ContextId = i4FsMIEvbSystemContextId + 1;

    /* Validating the Context ID */
    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        return SNMP_FAILURE;
    }

    while (i4ContextId < MAX_VLAN_EVB_CONTEXT_ENTRIES)
    {
        /* Getting Context information for the given context */
        pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4ContextId];

        if (NULL == pEvbContextInfo)
        {
            i4ContextId++;
            continue;
        }
        else
        {
            *pi4NextFsMIEvbSystemContextId = (INT4)
                gEvbGlobalInfo.apEvbContextInfo[i4ContextId]->u4EvbSysCxtId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEvbSystemControl
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSystemControl (INT4 i4FsMIEvbSystemContextId,
              INT4 *pi4RetValFsMIEvbSystemControl)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */
    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIEvbSystemControl = pEvbContextInfo->i4EvbSysCtrlStatus;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSystemModuleStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSystemModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSystemModuleStatus (INT4 i4FsMIEvbSystemContextId,
              INT4 *pi4RetValFsMIEvbSystemModuleStatus)
{

    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    
    *pi4RetValFsMIEvbSystemModuleStatus = pEvbContextInfo->i4EvbSysModStatus;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSystemTraceLevel
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSystemTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSystemTraceLevel (INT4 i4FsMIEvbSystemContextId,
              UINT4 *pu4RetValFsMIEvbSystemTraceLevel)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {   
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIEvbSystemTraceLevel = pEvbContextInfo->u4EvbSysTrcLevel;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSystemTrapStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSystemTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSystemTrapStatus (INT4 i4FsMIEvbSystemContextId,
              INT4 *pi4RetValFsMIEvbSystemTrapStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    { 
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIEvbSystemTrapStatus = pEvbContextInfo->i4EvbSysTrapStatus;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSystemStatsClear
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSystemStatsClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSystemStatsClear (INT4 i4FsMIEvbSystemContextId,
    INT4 *pi4RetValFsMIEvbSystemStatsClear)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
   
    /* By default System Status Clear is Set to VLAN_FALSE*/ 
    *pi4RetValFsMIEvbSystemStatsClear = VLAN_FALSE;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSchannelIdMode
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSchannelIdMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSchannelIdMode (INT4 i4FsMIEvbSystemContextId,
    INT4 *pi4RetValFsMIEvbSchannelIdMode)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIEvbSchannelIdMode =  pEvbContextInfo->i4EvbSysSChMode;

	return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSystemRowStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                retValFsMIEvbSystemRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSystemRowStatus (INT4 i4FsMIEvbSystemContextId,
    INT4 *pi4RetValFsMIEvbSystemRowStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIEvbSystemRowStatus = pEvbContextInfo->i4EvbSysRowStatus;

	return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEvbSystemControl
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSystemControl (INT4 i4FsMIEvbSystemContextId,
    INT4 i4SetValFsMIEvbSystemControl)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];
    if ( i4SetValFsMIEvbSystemControl == VLAN_EVB_SYSTEM_START)
    {
        if ( pEvbContextInfo != NULL)
        {
            pEvbContextInfo->i4EvbSysCtrlStatus = i4SetValFsMIEvbSystemControl;
        }
        else
        {
            if(VLAN_FAILURE != VlanEvbStart ((UINT4)i4FsMIEvbSystemContextId))
            {
                pEvbContextInfo =
                        gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];
            pEvbContextInfo->i4EvbSysCtrlStatus = i4SetValFsMIEvbSystemControl;
        }
     }
    }
     else if (i4SetValFsMIEvbSystemControl == VLAN_EVB_SYSTEM_SHUTDOWN)
     {
        if ( pEvbContextInfo == NULL)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        else
        {
            VlanEvbShutdown ((UINT4)i4FsMIEvbSystemContextId);
        }
     }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSystemModuleStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSystemModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSystemModuleStatus (INT4 i4FsMIEvbSystemContextId,
              INT4 i4SetValFsMIEvbSystemModuleStatus)
{

    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME, 
                       "nmhSetFsMIEvbSystemModuleStatus:"
                       "Context Info not present at Context Id %d" 
                        "and Module Status is %d \r\n",i4FsMIEvbSystemContextId ,
                        i4SetValFsMIEvbSystemModuleStatus );
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if(VLAN_EVB_MODULE_ENABLE == i4SetValFsMIEvbSystemModuleStatus)
    {
        /* Calling EVB enable util */
        VlanEvbEnable ((UINT4)i4FsMIEvbSystemContextId);
    }
    else
    {
        /* Calling EVB Disable util */
        VlanEvbDisable ((UINT4)i4FsMIEvbSystemContextId);
    }
    pEvbContextInfo->i4EvbSysModStatus = i4SetValFsMIEvbSystemModuleStatus;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSystemTraceLevel
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSystemTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSystemTraceLevel (INT4 i4FsMIEvbSystemContextId,
              UINT4 u4SetValFsMIEvbSystemTraceLevel)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhSetFsMIEvbSystemTraceLevel:"
                       "Context Info not present at Context Id :%d Trace Level:"
                       "%d\r\n",i4FsMIEvbSystemContextId, 
                       u4SetValFsMIEvbSystemTraceLevel);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbContextInfo->u4EvbSysTrcLevel = u4SetValFsMIEvbSystemTraceLevel;

    if (u4SetValFsMIEvbSystemTraceLevel == 0)
    {
        /* None of the EVB trace is wanted. Clear all traces for EVB */
        /* Buggy:- after setting and resetting the EVB trace, if VLAN trace is 
         * set already then it is also reset. */
        VLAN_DBG_FLAG &= (UINT4)(~VLAN_EVB_TRC);
        VLAN_DBG_FLAG &= (UINT4)(~VLAN_TRC_TYPE_ALL);
    }
    else
    {
        /* Enable the module level and also append the trace level */
        VLAN_DBG_FLAG = u4SetValFsMIEvbSystemTraceLevel;
        VLAN_DBG_FLAG |= VLAN_EVB_TRC; 
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSystemTrapStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSystemTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSystemTrapStatus (INT4 i4FsMIEvbSystemContextId,
              INT4 i4SetValFsMIEvbSystemTrapStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhSetFsMIEvbSystemTrapStatus:"
                       "Context Info not present at Context Id :%d"
                       "TrapStatus:%d \r\n",i4FsMIEvbSystemContextId,
                        i4SetValFsMIEvbSystemTrapStatus );
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pEvbContextInfo->i4EvbSysTrapStatus = i4SetValFsMIEvbSystemTrapStatus;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSystemStatsClear
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSystemStatsClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSystemStatsClear (INT4 i4FsMIEvbSystemContextId,
              INT4 i4SetValFsMIEvbSystemStatsClear)
{

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Calling VlanEvbClearStats utility to clear the EVB stats */

    if(VLAN_TRUE == i4SetValFsMIEvbSystemStatsClear)
    {
        VlanEvbClearStats ((UINT4)i4FsMIEvbSystemContextId);
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSchannelIdMode
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSchannelIdMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSchannelIdMode (INT4 i4FsMIEvbSystemContextId,
              INT4 i4SetValFsMIEvbSchannelIdMode)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME, 
                       "nmhSetFsMIEvbSchannelIdMode:"
                       "Context info for %d is not present,SchannelIdMode:"
                       "%d\r\n", i4FsMIEvbSystemContextId,
                       i4SetValFsMIEvbSchannelIdMode);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pEvbContextInfo->i4EvbSysSChMode = i4SetValFsMIEvbSchannelIdMode;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
 
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSystemRowStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                setValFsMIEvbSystemRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSystemRowStatus (INT4 i4FsMIEvbSystemContextId,
              INT4 i4SetValFsMIEvbSystemRowStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */
    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    /* Regarding OID sync up issue, CREATE_AND_WAIT is validated here */
    if ((pEvbContextInfo != NULL) &&
        ((i4SetValFsMIEvbSystemRowStatus == CREATE_AND_GO) ||
         (i4SetValFsMIEvbSystemRowStatus == ACTIVE) ||
        (i4SetValFsMIEvbSystemRowStatus == CREATE_AND_WAIT)))

    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    switch(i4SetValFsMIEvbSystemRowStatus)
    {
        case CREATE_AND_WAIT: /* For MSR, it is added */
        case CREATE_AND_GO:
        case ACTIVE:
            if(VLAN_FAILURE == VlanEvbStart ((UINT4)i4FsMIEvbSystemContextId))
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, 
                               VLAN_NAME,"nmhSetFsMIEvbSystemRowStatus"
                                "Could not Start EVB with Context Id %d\r\n"
                                "EvbSystemRowStatus: %d",
                                i4FsMIEvbSystemContextId,
                                i4SetValFsMIEvbSystemRowStatus);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            break;

        case DESTROY: 
            VlanEvbShutdown((UINT4)i4FsMIEvbSystemContextId);    
            break;

            /* CREATE_AND_WAIT , NOT_IN_SERVICE are not supported */
        default:
            VlanReleaseContext ();
            return SNMP_FAILURE;

    } 
    VlanReleaseContext ();
	return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSystemControl
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSystemControl (UINT4 *pu4ErrorCode,
              INT4 i4FsMIEvbSystemContextId , 
              INT4 i4TestValFsMIEvbSystemControl)
{

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
         *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if(VLAN_EVB_SYSTEM_START != i4TestValFsMIEvbSystemControl && 
        VLAN_EVB_SYSTEM_SHUTDOWN != i4TestValFsMIEvbSystemControl)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    } 
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSystemModuleStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSystemModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSystemModuleStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIEvbSystemContextId,
                                    INT4 i4TestValFsMIEvbSystemModuleStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
         CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (i4FsMIEvbSystemContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if((VLAN_EVB_MODULE_ENABLE != i4TestValFsMIEvbSystemModuleStatus) &&
      (VLAN_EVB_MODULE_DISABLE != i4TestValFsMIEvbSystemModuleStatus))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSystemTraceLevel
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSystemTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSystemTraceLevel (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIEvbSystemContextId,
                                  UINT4 u4TestValFsMIEvbSystemTraceLevel)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS (i4FsMIEvbSystemContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
 
    if(u4TestValFsMIEvbSystemTraceLevel > VLAN_EVB_MAX_TRACE_LEVEL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSystemTrapStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSystemTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSystemTrapStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIEvbSystemContextId,
                                  INT4 i4TestValFsMIEvbSystemTrapStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS(i4FsMIEvbSystemContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if((VLAN_EVB_TRAP_ENABLE != i4TestValFsMIEvbSystemTrapStatus) && 
        (VLAN_EVB_TRAP_DISABLE!= i4TestValFsMIEvbSystemTrapStatus))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSystemStatsClear
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSystemStatsClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSystemStatsClear (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIEvbSystemContextId,
                                  INT4 i4TestValFsMIEvbSystemStatsClear)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
       *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */
    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS(i4FsMIEvbSystemContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((VLAN_TRUE != i4TestValFsMIEvbSystemStatsClear) && 
        (VLAN_FALSE != i4TestValFsMIEvbSystemStatsClear))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSchannelIdMode
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSchannelIdMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSchannelIdMode (UINT4 *pu4ErrorCode,
              INT4 i4FsMIEvbSystemContextId , 
              INT4 i4TestValFsMIEvbSchannelIdMode)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;
    tEvbSChIfEntry  *pEvbSChIfEntry   = NULL;
    /* Physical interface index starts with 1 */
    UINT4            u4UapIfIndex     = 1;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */

    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    
    /* Verifying the EVB System status */
    if(VLAN_EVB_SYSTEM_START != 
        VLAN_EVB_SYSTEM_STATUS(i4FsMIEvbSystemContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
         return SNMP_FAILURE;
    }

    if((VLAN_EVB_SCH_MODE_AUTO != i4TestValFsMIEvbSchannelIdMode) && 
        (i4TestValFsMIEvbSchannelIdMode != VLAN_EVB_SCH_MODE_MANUAL)) 
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIEvbSystemContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* If any S-Channel entries other than default entries are 
     * present, then the changing the SCID mode is not allowed. */
    while (u4UapIfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        pEvbSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, 
                                    VLAN_EVB_DEF_SVID);
        if ((pEvbSChIfEntry != NULL) &&
            (pEvbSChIfEntry->u4SChId != VLAN_EVB_DEF_SCID) &&
            (pEvbSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID))
        {
            CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
             VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                            "nmhTestv2FsMIEvbSchannelIdMode:"
                            "Default values  not present at UAP Index %d \r\n",
                            u4UapIfIndex);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        u4UapIfIndex += 1;
    }
    VlanReleaseContext ();
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSystemRowStatus
 Input       :  The Indices
                FsMIEvbSystemContextId

                The Object 
                testValFsMIEvbSystemRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSystemRowStatus (UINT4 *pu4ErrorCode,
              INT4 i4FsMIEvbSystemContextId , 
              INT4 i4TestValFsMIEvbSystemRowStatus)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;

    /* Validating the Context ID */

    if(i4FsMIEvbSystemContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */
    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[i4FsMIEvbSystemContextId];

    if (NULL == pEvbContextInfo)
    {
        if ((i4TestValFsMIEvbSystemRowStatus == CREATE_AND_GO) ||
            (i4TestValFsMIEvbSystemRowStatus == ACTIVE))
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsMIEvbSystemRowStatus == DESTROY)
        {
            return SNMP_SUCCESS;
        }
    }

    if ((i4TestValFsMIEvbSystemRowStatus == CREATE_AND_WAIT) ||
        (i4TestValFsMIEvbSystemRowStatus == NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    else
    {
        
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    return SNMP_FAILURE;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEvbSystemTable
 Input       :  The Indices
                FsMIEvbSystemContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEvbSystemTable (UINT4 *pu4ErrorCode,
              tSnmpIndexList *pSnmpIndexList, 
              tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsMIEvbCAPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMIEvbCAPConfigTable
    (UINT4 u4Ieee8021BridgePhyPort, UINT4 u4Ieee8021BridgeEvbSchID)
{
    tEvbSChIfEntry *pEvbSChIfEntry= NULL;
    UINT4          u4ContextId     = 0;
    UINT2          u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                           u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChIfEntry)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhValidateIndexInstanceFsMIEvbCAPConfigTable"
                     "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
         VlanReleaseContext ();
	    return SNMP_FAILURE;
    }
    VlanReleaseContext ();
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsMIEvbCAPConfigTable
    (UINT4 *pu4Ieee8021BridgePhyPort, UINT4 *pu4Ieee8021BridgeEvbSchID)
{
    tEvbSChIfEntry  *pEvbSChIfEntry= NULL;

    /* Getting the First entry from CAP config table */
    pEvbSChIfEntry = (tEvbSChIfEntry *) (VOID *) RBTreeGetFirst
                     (gEvbGlobalInfo.EvbSchIfTree);

    if(NULL == pEvbSChIfEntry)
    {
        return SNMP_FAILURE;
    }
    *pu4Ieee8021BridgePhyPort = pEvbSChIfEntry->u4UapIfIndex;
    *pu4Ieee8021BridgeEvbSchID = pEvbSChIfEntry->u4SVId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                nextIeee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
                nextIeee8021BridgeEvbSchID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEvbCAPConfigTable (UINT4 u4Ieee8021BridgePhyPort,
                                      UINT4 *pu4NextIeee8021BridgePhyPort,
                                      UINT4 u4Ieee8021BridgeEvbSchID,
                                      UINT4 *pu4NextIeee8021BridgeEvbSchID)
{
    tEvbSChIfEntry  *pEvbSChIfEntry  = NULL;
    tEvbSChIfEntry   SChIfNode;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
        
    MEMSET (&SChIfNode, 0, sizeof (tEvbSChIfEntry));
    SChIfNode.u4UapIfIndex = u4Ieee8021BridgePhyPort;
    SChIfNode.u4SVId = u4Ieee8021BridgeEvbSchID;
  
    /* Getting the next entry from CAP config table */
    pEvbSChIfEntry = (tEvbSChIfEntry *) (VOID *) RBTreeGetNext
          ((gEvbGlobalInfo.EvbSchIfTree), (tRBElem *) (VOID *)&SChIfNode, NULL);

    if(NULL == pEvbSChIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pu4NextIeee8021BridgePhyPort = pEvbSChIfEntry->u4UapIfIndex;
    *pu4NextIeee8021BridgeEvbSchID = pEvbSChIfEntry->u4SVId;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEvbCAPSChannelID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValFsMIEvbCAPSChannelID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbCAPSChannelID (UINT4 u4Ieee8021BridgePhyPort,
                            UINT4 u4Ieee8021BridgeEvbSchID,
                            UINT4 *pu4RetValFsMIEvbCAPSChannelID)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChScidIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetFsMIEvbCAPSChannelID : "
                      "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbCAPSChannelID = pEvbSChScidIfEntry->u4SChId; 
    VlanReleaseContext ();
    return SNMP_SUCCESS;
    
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbCAPSChannelIfIndex
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValFsMIEvbCAPSChannelIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbCAPSChannelIfIndex (UINT4 u4Ieee8021BridgePhyPort,
                                 UINT4 u4Ieee8021BridgeEvbSchID,
                                 INT4 *pi4RetValFsMIEvbCAPSChannelIfIndex)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChScidIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetFsMIEvbCAPSChannelIfIndex: "
                      "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIEvbCAPSChannelIfIndex = pEvbSChScidIfEntry->i4SChIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbCAPSChNegoStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                retValFsMIEvbCAPSChNegoStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbCAPSChNegoStatus (UINT4 u4Ieee8021BridgePhyPort,
                               UINT4 u4Ieee8021BridgeEvbSchID,
                               INT4 *pi4RetValFsMIEvbCAPSChNegoStatus)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChScidIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhGetFsMIEvbCAPSChNegoStatus: "
                      "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIEvbCAPSChNegoStatus = pEvbSChScidIfEntry->i4NegoStatus;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbPhyPort
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object
                retValFsMIEvbPhyPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbPhyPort (UINT4 u4Ieee8021BridgePhyPort,
                      UINT4 u4Ieee8021BridgeEvbSchID,
                      UINT4 *pu4RetValFsMIEvbPhyPort)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChScidIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetFsMIEvbPhyPort: "
                      "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbPhyPort = pEvbSChScidIfEntry->u4UapIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSchID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object
                retValFsMIEvbSchID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSchID (UINT4 u4Ieee8021BridgePhyPort,
                    UINT4 u4Ieee8021BridgeEvbSchID,
                    UINT4 *pu4RetValFsMIEvbSchID)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChScidIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetFsMIEvbSchID: "
                      "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbSchID = pEvbSChScidIfEntry->u4SVId;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSChannelFilterStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object
                retValFsMIEvbSChannelFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSChannelFilterStatus (UINT4 u4Ieee8021BridgePhyPort,
                                   UINT4 u4Ieee8021BridgeEvbSchID,
                                   INT4 *pi4RetValFsMIEvbSChannelFilterStatus)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;
    
    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
     if(NULL == pEvbSChScidIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhGetFsMIEvbSchID: "
                      "Failed to get S-channel Entry for the UAP index %d"
                       " and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIEvbSChannelFilterStatus =
        pEvbSChScidIfEntry->i4SChFilterStatus;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEvbCAPSChannelID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                setValFsMIEvbCAPSChannelID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbCAPSChannelID (UINT4 u4Ieee8021BridgePhyPort,
                            UINT4 u4Ieee8021BridgeEvbSchID,
                            UINT4 u4SetValFsMIEvbCAPSChannelID)
{

    tEvbSChIfEntry *pEvbSChIfEntry = NULL;
    tEvbContextInfo *pEvbContextInfo = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[(INT4) u4ContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                           u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME, 
                       "nmhSetFsMIEvbCAPSChannelID: "
                        "S-channel entry not present at UAP Index %d \r\n",
                       u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if (pEvbContextInfo->i4EvbSysSChMode == VLAN_EVB_SCH_MODE_MANUAL)
    {
        /* For auto mode - the entry addition in EvbSchScidTree will be taken 
         * care inside the function VlanEvbSChIfAddEntry */ 
        pEvbSChIfEntry->u4SChId = u4SetValFsMIEvbCAPSChannelID;
        /* Adding in RBTree 1 - keys UAP port + SCID */
        if (RBTreeAdd (gEvbGlobalInfo.EvbSchScidTree, (tRBElem *) (VOID *)
                    (pEvbSChIfEntry)) == RB_FAILURE)
        {
            VLAN_TRC (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME, 
                       "nmhSetFsMIEvbCAPSChannelID: "
                       "Failed to add info in RBTree \r\n");
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    /* put 3 byte  in UAP entry*/
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbCAPSChannelIfIndex
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                setValFsMIEvbCAPSChannelIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbCAPSChannelIfIndex (UINT4 u4Ieee8021BridgePhyPort,
                                 UINT4 u4Ieee8021BridgeEvbSchID,
                                 INT4 i4SetValFsMIEvbCAPSChannelIfIndex)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChScidIfEntry)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME, 
                    "nmhSetFsMIEvbCAPSChannelIfIndex: "
                    "S-channel entry not present for UAP %d SVID %d\r\n",
                       u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID);
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbSChScidIfEntry->i4SChIfIndex = i4SetValFsMIEvbCAPSChannelIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIEvbSChannelFilterStatus
 Input       :  i4SetValFsMIEvbSChannelFilterStatus
                u4Ieee8021BridgePhyPort 
                u4Ieee8021BridgeEvbSchID

                The Object
                setValFsMIEvbSChannelFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbSChannelFilterStatus (UINT4 u4Ieee8021BridgePhyPort,
                                   UINT4 u4Ieee8021BridgeEvbSchID,
                                   INT4 i4SetValFsMIEvbSChannelFilterStatus)
{
    tEvbSChIfEntry *pEvbSChScidIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbSChScidIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                               u4Ieee8021BridgeEvbSchID);

    if(NULL == pEvbSChScidIfEntry)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhSetFsMIEvbCAPSChannelIfIndex: "
                "S-channel entry not present for UAP %d SVID %d\r\n",
                       u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID);
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VlanSetSChFilterStatus
        (u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID,
                i4SetValFsMIEvbSChannelFilterStatus) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhSetFsMIEvbSChannelFilterStatus: "
                "Setting Filter Status for S-channel failed for U AP %d SVID %d\r\n",
                       u4Ieee8021BridgePhyPort, u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEvbCAPSChannelID
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                testValFsMIEvbCAPSChannelID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbCAPSChannelID (UINT4 *pu4ErrorCode,
                               UINT4 u4Ieee8021BridgePhyPort,
                               UINT4 u4Ieee8021BridgeEvbSchID,
    UINT4 u4TestValFsMIEvbCAPSChannelID)
{
    tEvbContextInfo  *pEvbContextInfo = NULL;
    tEvbUapIfEntry   *pEvbUapIfEntry  = NULL;
    tEvbSChIfEntry   *pEvbSChIfEntry  = NULL;
    tEvbSChIfEntry   *pSChIfNode = NULL;
    INT4             i4SchannelIdMode = VLAN_EVB_ZERO;
    UINT4          u4ContextId =  VLAN_EVB_ZERO;   
    INT4                i4Count = VLAN_EVB_ZERO;
    UINT4            u4SVID           = VLAN_EVB_ZERO;
    UINT2          u2LocalPort    = VLAN_EVB_ZERO;  /* Local Port ID */
    /* Getting the Context Id for the given Physical port */

    if(VCM_SUCCESS != VlanVcmGetContextInfoFromIfIndex
        (u4Ieee8021BridgePhyPort, &u4ContextId, &u2LocalPort))
    {
        CLI_SET_ERR (CLI_VLAN_EVB_GET_CONTEXT_FROM_IF_ERR);
        return SNMP_FAILURE;
    }

    /* Getting Context information for the given context */
    pEvbContextInfo = gEvbGlobalInfo.apEvbContextInfo[(INT4) u4ContextId];

    if (NULL == pEvbContextInfo)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }
    /* Verifying the EVB System status */
    if (VLAN_EVB_SYSTEM_START != VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i4SchannelIdMode = pEvbContextInfo->i4EvbSysSChMode;

    /* If mode is auto, then SVID should be 0 
       else if mode is manual SVID should not be 0 */
    if((VLAN_EVB_SCH_MODE_AUTO == i4SchannelIdMode)&&
        (VLAN_EVB_ZERO != u4TestValFsMIEvbCAPSChannelID)) 
    {
        CLI_SET_ERR(CLI_VLAN_EVB_SCID_AUTO_ERR);
         VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhTestv2FsMIEvbCAPSChannelID: Auto mode-SVID non zero-"
                 "Wrong Value:SchannelIdMode:%d,EvbCAPSChannelID: %d \r\n",
                 i4SchannelIdMode,u4TestValFsMIEvbCAPSChannelID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((u4TestValFsMIEvbCAPSChannelID == VLAN_EVB_DEF_SCID) &&
        (VLAN_EVB_SCH_MODE_MANUAL == i4SchannelIdMode)) 
    {
        CLI_SET_ERR (CLI_VLAN_EVB_DEF_SCID_CREATION_NOT_ALLOWED);
         VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhTestv2FsMIEvbCAPSChannelID: Manual mode-Def SVID-"
                 "Wrong Value:SchannelIdMode:%d,EvbCAPSChannelID: %d \r\n",
                 i4SchannelIdMode,u4TestValFsMIEvbCAPSChannelID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
  	    return SNMP_FAILURE;
    }
    
    if((VLAN_EVB_SCH_MODE_MANUAL == i4SchannelIdMode) &&
        ((VLAN_EVB_ZERO == u4Ieee8021BridgeEvbSchID) ||
        (VLAN_EVB_ZERO == u4TestValFsMIEvbCAPSChannelID)))
    {
        CLI_SET_ERR(CLI_VLAN_EVB_SVID_SCID_MANUAL_ERR);
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhTestv2FsMIEvbCAPSChannelID:-Manual mode SCID and SVID-"
                 "Wrong Value:BridgeEvbSchID: %d,EvbCAPSChannelID: %d \r\n",
                       u4Ieee8021BridgeEvbSchID, u4TestValFsMIEvbCAPSChannelID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2FsMIEvbCAPSChannelID:"
                       "UAP Entry %d not present \n", u4Ieee8021BridgePhyPort);
       *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                           u4Ieee8021BridgeEvbSchID);
    if(NULL == pEvbSChIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2FsMIEvbCAPSChannelID:"
                       "S-channel Entry %d not present \n",
                       u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    /* If Mode is Manual, check if S-channel ID is available for the 
        given S-Vid. */
    if((VLAN_EVB_SCH_MODE_MANUAL == i4SchannelIdMode) &&
           (VLAN_FALSE == VlanEvbIsSChannelIdAvailable (pEvbUapIfEntry,
                                                     u4Ieee8021BridgeEvbSchID,
                                                     u4TestValFsMIEvbCAPSChannelID)))
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCH_ENTRY_ALREADY_PRESENT_ERR);
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2FsMIEvbCAPSChannelID:"
                        "S-channel Id %d is already present for the S-Vid %d"
                       "at Uap Index %d\n", u4Ieee8021BridgeEvbSchID,
                         u4TestValFsMIEvbCAPSChannelID,
                         pEvbUapIfEntry->u4UapIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
   
    if((VLAN_EVB_SCH_MODE_MANUAL == i4SchannelIdMode) &&
       ((VLAN_EVB_MAX_SCID < u4TestValFsMIEvbCAPSChannelID) ||
       (VLAN_EVB_MIN_CDCP_ADMIN_CHAN_CAP > u4TestValFsMIEvbCAPSChannelID)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                        "nmhTestv2FsMIEvbCAPSChannelID: Manual mode"
                        "SCID is > than VLAN_EVB_MAX_SCID %d and SVID is < "
                        "than VLAN_EVB_MIN_CDCP_ADMIN_CHAN_CAP %d\r\n",
                        VLAN_EVB_SCH_MODE_MANUAL,VLAN_EVB_MAX_SCID,
                          VLAN_EVB_MIN_CDCP_ADMIN_CHAN_CAP);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    while ((pSChIfNode = VlanEvbSChIfGetNextEntry
            (u4Ieee8021BridgePhyPort, u4SVID)) != NULL)
    {
        if (u4Ieee8021BridgePhyPort != pSChIfNode->u4UapIfIndex)
        {
            break;
        }
        i4Count += 1;
        u4SVID = pSChIfNode->u4SVId;
        pSChIfNode = NULL;
    }

    if (i4Count > pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_MAX_SCHANNEL_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelID:"
                      "Maximum value %d Reached \n",
                      pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if (pEvbSChIfEntry->i4SChIfRowStatus == ACTIVE) 
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelID:"
                      "Wrong Row Status value %d \n",
                      pEvbSChIfEntry->i4SChIfRowStatus);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbCAPSChannelIfIndex
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object 
                testValFsMIEvbCAPSChannelIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbCAPSChannelIfIndex (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021BridgePhyPort,
                                    UINT4 u4Ieee8021BridgeEvbSchID,
    INT4 i4TestValFsMIEvbCAPSChannelIfIndex)
{
    tEvbSChIfEntry   *pEvbSChIfEntry = NULL;
    INT4                i4IndexMin = 0;
    INT4                i4IndexMax = 0;
    UINT4            u4SVID          = VLAN_EVB_DEF_SVID;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    /* Verifying the EVB System status */
    if (VLAN_EVB_SYSTEM_START != VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /*____________________________________________
     *|             |              |              |
     *| UAP1        | UAP2         | UAP3         |
     *|_____________|______________|______________|
     *|             |              |              |
     *|SBP1,SBP2,etc| SBP1,SBP2,etc|SBP1,SBP2,etc |
     *|_____________|______________|______________|
     */
    i4IndexMin = (INT4)((CFA_MIN_EVB_SBP_INDEX + (VLAN_EVB_MAX_SBP_PER_UAP *
                                           (u4Ieee8021BridgePhyPort - 1))));

    i4IndexMax = i4IndexMin + VLAN_EVB_MAX_SBP_PER_UAP - 1;

    if (i4TestValFsMIEvbCAPSChannelIfIndex == i4IndexMin)
    {
        /* must not match with default S-Channel's IfIndex */
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelIfIndex:"
                      "Error:Matched with default S-Channel IfIndex %d \n",
                       i4TestValFsMIEvbCAPSChannelIfIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (!((i4TestValFsMIEvbCAPSChannelIfIndex >= i4IndexMin) &&
          (i4TestValFsMIEvbCAPSChannelIfIndex <= i4IndexMax)))
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelIfIndex:"
                      "Wrong Test value - Not in range %d \n",
                       i4TestValFsMIEvbCAPSChannelIfIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                                u4Ieee8021BridgeEvbSchID)) ==
        NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelIfIndex:"
                      "Failed to get S-channel Entry for the UAP index %d"
                       "and SVID %d \r\n", u4Ieee8021BridgePhyPort,
                       u4Ieee8021BridgeEvbSchID);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if (pEvbSChIfEntry->i4SChIfRowStatus != NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelIfIndex:"
                       "Wrong Status Value %d",
                       pEvbSChIfEntry->i4SChIfRowStatus);
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    while ((pEvbSChIfEntry = VlanEvbSChIfGetNextEntry
            (u4Ieee8021BridgePhyPort, u4SVID)) != NULL)
    {
        /* S-Channel If index is defined per UAP specific.
         * If it is not per UAP specific, then the following check
         * to be removed. */
        if (pEvbSChIfEntry->u4UapIfIndex != u4Ieee8021BridgePhyPort)
        {
            /* scaning on this UAP had done and it is found no UAP + SVID
             * pair contains this ifindex. */
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        
        if ((pEvbSChIfEntry->u4SVId == u4Ieee8021BridgeEvbSchID) &&
            (pEvbSChIfEntry->i4SChIfIndex != 0))
        {
            /* This entry is already configured with the SChIfIndex and 
             * hence modifying the ifindex is not allowed. */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelIfIndex:"
                      "Svid %d already configured with SchIfIndex %d",
                           pEvbSChIfEntry->u4SVId,
                           pEvbSChIfEntry->i4SChIfIndex);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        if (pEvbSChIfEntry->i4SChIfIndex == i4TestValFsMIEvbCAPSChannelIfIndex)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
             VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                      "nmhTestv2FsMIEvbCAPSChannelIfIndex: Wrong Value:SchIfEntry"
                           "%d equals SChannelIfIndex %d",
                           pEvbSChIfEntry->i4SChIfIndex,
                      i4TestValFsMIEvbCAPSChannelIfIndex);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        u4SVID = pEvbSChIfEntry->u4SVId;
    }
    /* Returning success since there are no more UAP+SVID holds this ifindex*/
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIEvbSChannelFilterStatus
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID

                The Object
                testValFsMIEvbSChannelFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbSChannelFilterStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021BridgePhyPort,
                                      UINT4 u4Ieee8021BridgeEvbSchID,
                                      INT4 i4TestValFsMIEvbSChannelFilterStatus)
{
    UINT4   u4ContextId   = 0;
    UINT2   u2LocalPortId = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    UNUSED_PARAM (u4Ieee8021BridgeEvbSchID);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Verifying the EVB System status */
    if (VLAN_EVB_SYSTEM_START != VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIEvbSChannelFilterStatus !=
         VLAN_EVB_SCH_L2_FILTER_STATUS_ENABLE)
        && (i4TestValFsMIEvbSChannelFilterStatus !=
            VLAN_EVB_SCH_L2_FILTER_STATUS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEvbCAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                Ieee8021BridgeEvbSchID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEvbCAPConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsMIEvbUAPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEvbUAPConfigTable (UINT4 u4Ieee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);


    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEvbUAPConfigTable (UINT4 *pu4Ieee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;

    pEvbUapIfEntry = VlanEvbUapIfGetFirstEntry();

    if(NULL == pEvbUapIfEntry)
    {
        return SNMP_FAILURE;
    }
    *pu4Ieee8021BridgePhyPort = pEvbUapIfEntry->u4UapIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                nextIeee8021BridgePhyPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEvbUAPConfigTable (UINT4 u4Ieee8021BridgePhyPort,
                                      UINT4 *pu4NextIeee8021BridgePhyPort)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetNextEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4NextIeee8021BridgePhyPort = pEvbUapIfEntry->u4UapIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEvbUAPSchCdcpMode
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbUAPSchCdcpMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbUAPSchCdcpMode (UINT4 u4Ieee8021BridgePhyPort,
    INT4 *pi4RetValFsMIEvbUAPSchCdcpMode)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    
    *pi4RetValFsMIEvbUAPSchCdcpMode = pEvbUapIfEntry->i4UapIfSChMode;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIEvbUAPSchCdcpMode
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                setValFsMIEvbUAPSchCdcpMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIEvbUAPSchCdcpMode (UINT4 u4Ieee8021BridgePhyPort,
    INT4 i4SetValFsMIEvbUAPSchCdcpMode)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    tEvbSChIfEntry *pEvbSChIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;
    UINT1            u1OperStatus    = CFA_IF_DOWN;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                    "nmhSetFsMIEvbUAPSchCdcpMode: UAP Entry %d not present \n",
                       u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry->i4UapIfSChMode = i4SetValFsMIEvbUAPSchCdcpMode;
    /* Get default S-Channel Entry, check the UAP oper status,
     * If it is UP, then make it as Confirmed **/

    pEvbSChIfEntry = VlanEvbSChIfGetEntry (u4Ieee8021BridgePhyPort,
                                             VLAN_EVB_DEF_SVID);
    if (NULL == pEvbSChIfEntry)
    {
        CLI_SET_ERR (CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhSetFsMIEvbCAPSChannelID: Default "
                        "S-channel entry not present for UAP Index %d \r\n",
                       u4Ieee8021BridgePhyPort);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanEvbUapGetOperStatus (pEvbUapIfEntry->u4UapIfIndex, &u1OperStatus);
    if (u1OperStatus == CFA_IF_UP)
    {
        pEvbSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_CONFIRMED;
          
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIEvbUAPSchCdcpMode
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                testValFsMIEvbUAPSchCdcpMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIEvbUAPSchCdcpMode (UINT4 *pu4ErrorCode,
                                UINT4 u4Ieee8021BridgePhyPort,
                                INT4 i4TestValFsMIEvbUAPSchCdcpMode)
{
    tEvbUapIfEntry   *pEvbUapIfEntry = NULL;
    tEvbSChIfEntry   *pEvbSChIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_GET_CONTEXT_FROM_IF_ERR);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    /* Verifying the EVB System status */
    if (VLAN_EVB_SYSTEM_START != VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        CLI_SET_ERR(CLI_VLAN_EVB_SYSTEM_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIEvbUAPSchCdcpMode != VLAN_EVB_UAP_SCH_MODE_DYNAMIC) &&
        (i4TestValFsMIEvbUAPSchCdcpMode != VLAN_EVB_UAP_SCH_MODE_HYBRID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2FsMIEvbUAPSchCdcpMode: "
                       "UAP Entry %d not present: \n", u4Ieee8021BridgePhyPort);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pEvbSChIfEntry = VlanEvbSChIfGetNextEntry (u4Ieee8021BridgePhyPort,
                                                VLAN_EVB_DEF_SVID);

    if ((NULL != pEvbSChIfEntry) && 
        (pEvbSChIfEntry->u4SChId != VLAN_EVB_DEF_SCID) &&
        (pEvbSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID) &&
        (VLAN_EVB_UAP_SCH_MODE_DYNAMIC == i4TestValFsMIEvbUAPSchCdcpMode))
    {
        /* Checking if entry is present already in S-channel interface,
           and returning failure if entry is present */
        CLI_SET_ERR(CLI_VLAN_EVB_SCHANNEL_MODE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME, 
                       "S-Channel Entry SChId %d SVID %d Mode %d already "
                       "present \n",
                        pEvbSChIfEntry->u4SChId , pEvbSChIfEntry->u4SVId ,
                        i4TestValFsMIEvbUAPSchCdcpMode);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIEvbUAPConfigTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIEvbUAPConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsMIEvbUAPStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIEvbUAPStatsTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIEvbUAPStatsTable (UINT4 u4Ieee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if(NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIEvbUAPStatsTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIEvbUAPStatsTable (UINT4 *pu4Ieee8021BridgePhyPort)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    pEvbUapIfEntry = VlanEvbUapIfGetFirstEntry();

    if(NULL == pEvbUapIfEntry)
    {
        return SNMP_FAILURE;
    }
    *pu4Ieee8021BridgePhyPort = pEvbUapIfEntry->u4UapIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIEvbUAPStatsTable
 Input       :  The Indices
                Ieee8021BridgePhyPort
                nextIeee8021BridgePhyPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIEvbUAPStatsTable (UINT4 u4Ieee8021BridgePhyPort,
                                     UINT4 *pu4NextIeee8021BridgePhyPort)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetNextEntry (u4Ieee8021BridgePhyPort);

    if (NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4NextIeee8021BridgePhyPort = pEvbUapIfEntry->u4UapIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIEvbTxCdcpCount
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbTxCdcpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbTxCdcpCount (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbTxCdcpCount)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
 
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Getting UAP entry for the given port */

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);

    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbTxCdcpCount = pEvbUapIfEntry->UapStats.u4EvbTxCdcpCount;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbRxCdcpCount
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbRxCdcpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbRxCdcpCount (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbRxCdcpCount)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbRxCdcpCount = pEvbUapIfEntry->UapStats.u4EvbRxCdcpCount;
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSChAllocFailCount
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbSChAllocFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSChAllocFailCount (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbSChAllocFailCount)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbSChAllocFailCount =
        pEvbUapIfEntry->UapStats.u4EvbSchAllocFailCount;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSChActiveFailCount
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbSChActiveFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSChActiveFailCount (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbSChActiveFailCount)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbSChActiveFailCount =
        pEvbUapIfEntry->UapStats.u4EvbSchActiveFailCount;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbSVIDPoolExceedsCount
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbSVIDPoolExceedsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbSVIDPoolExceedsCount (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbSVIDPoolExceedsCount)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbSVIDPoolExceedsCount =
        pEvbUapIfEntry->UapStats.u4SVIdPoolExcdCount;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbCdcpRejectStationReq
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbCdcpRejectStationReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbCdcpRejectStationReq (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbCdcpRejectStationReq)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbCdcpRejectStationReq =
        pEvbUapIfEntry->UapStats.u4EvbCdcpRejectStationReq;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMIEvbCdcpOtherDropCount
 Input       :  The Indices
                Ieee8021BridgePhyPort

                The Object 
                retValFsMIEvbCdcpOtherDropCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIEvbCdcpOtherDropCount (UINT4 u4Ieee8021BridgePhyPort,
    UINT4 *pu4RetValFsMIEvbCdcpOtherDropCount)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4Ieee8021BridgePhyPort,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4Ieee8021BridgePhyPort);
    if(pEvbUapIfEntry == NULL)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIEvbCdcpOtherDropCount =
        pEvbUapIfEntry->UapStats.u4EvbCdcpOtherDropCount;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}


