
/* $Id: stdbrgwr.c,v 1.11 2011/10/25 10:39:08 siva Exp $*/
#include "lr.h"
#include "vlaninc.h"
#include "fssnmp.h"
#include "stdbrgwr.h"
#include "stdbrgdb.h"

VOID
RegisterSTDBRG ()
{
    SNMPRegisterMib (&stdbrgOID, &stdbrgEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdbrgOID, (const UINT1 *) "pBridgeMIB");
}

INT4
Dot1dDeviceCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1dDeviceCapabilities (pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;;
}

INT4
Dot1dTrafficClassesEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1dTrafficClassesEnabled
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassesEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1dTrafficClassesEnabled (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassesEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1dTrafficClassesEnabled (&pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dGmrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1dGmrpStatus (pu4Error, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassesEnabledDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1dTrafficClassesEnabled (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dGmrpStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhDepv2Dot1dGmrpStatus (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dGmrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    GARP_LOCK ();

    i4RetVal = nmhSetDot1dGmrpStatus (pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dGmrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    GARP_LOCK ();

    i4RetVal = nmhGetDot1dGmrpStatus (&pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortCapabilitiesTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dPortCapabilities (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortDefaultUserPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortDefaultUserPriority (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortDefaultUserPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1dPortDefaultUserPriority
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortDefaultUserPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortDefaultUserPriority
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortNumTrafficClassesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortNumTrafficClasses (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortPriorityTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1dPortPriorityTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortNumTrafficClassesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1dPortNumTrafficClasses
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortNumTrafficClassesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortNumTrafficClasses
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dRegenUserPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1dRegenUserPriority (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dUserPriorityRegenTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1dUserPriorityRegenTable (pu4Error, pSnmpIndexList,
                                                    pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dRegenUserPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1dRegenUserPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dRegenUserPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dUserPriorityRegenTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dRegenUserPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1dTrafficClass (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1dTrafficClassTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1dTrafficClass (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTrafficClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTrafficClassTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTrafficClass (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortOutboundAccessPriorityGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortOutboundAccessPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortOutboundAccessPriority
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpJoinTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortGarpJoinTime (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpJoinTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetDot1dPortGarpJoinTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpJoinTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGarpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dPortGarpJoinTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpLeaveTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortGarpLeaveTime (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpLeaveTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetDot1dPortGarpLeaveTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpLeaveTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGarpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dPortGarpLeaveTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpLeaveAllTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortGarpLeaveAllTime (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhDepv2Dot1dPortGarpTable (pu4Error, pSnmpIndexList,
                                           pSnmpvarbinds);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpLeaveAllTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetDot1dPortGarpLeaveAllTime
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGarpLeaveAllTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGarpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortGarpLeaveAllTime
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGmrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortGmrpStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGmrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetDot1dPortGmrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGmrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortGmrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGmrpFailedRegistrationsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortGmrpFailedRegistrations
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortGmrpLastPduOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (nmhGetDot1dPortGmrpLastPduOrigin (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList)
        == SNMP_SUCCESS)
    {
        pMultiData->pOctetStrValue->i4_Length = 6;
        GARP_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
}

INT4
Dot1dPortRestrictedGroupRegistrationTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1dPortRestrictedGroupRegistration (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1dPortGmrpTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhDepv2Dot1dPortGmrpTable (pu4Error, pSnmpIndexList,
                                           pSnmpvarbinds);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dPortRestrictedGroupRegistrationSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetDot1dPortRestrictedGroupRegistration
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1dPortRestrictedGroupRegistrationGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dPortRestrictedGroupRegistration
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTpHCPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTpHCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dTpHCPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->u8_Counter64Value);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTpHCPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTpHCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dTpHCPortOutFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->u8_Counter64Value);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTpHCPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTpHCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1dTpHCPortInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &pMultiData->u8_Counter64Value);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTpPortInOverflowFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTpPortOverflowTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpPortInOverflowFrames
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTpPortOutOverflowFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTpPortOverflowTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpPortOutOverflowFrames
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1dTpPortInOverflowDiscardsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1dTpPortOverflowTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dTpPortInOverflowDiscards
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1dTpPortOverflowTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTpPortOverflowTable (&i4dot1dTpPort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTpPortOverflowTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dTpPort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dTpHCPortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTpHCPortTable (&i4dot1dTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTpHCPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dTpPort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dPortGmrpTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;

    GARP_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dPortGmrpTable (&i4dot1dBasePort)
            == SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dPortGmrpTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dBasePort) == SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    GARP_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dPortGarpTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;

    GARP_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dPortGarpTable (&i4dot1dBasePort)
            == SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dPortGarpTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dBasePort) == SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    GARP_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dPortOutboundAccessPriorityTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;
    INT4                i4dot1dRegenUserPriority;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dPortOutboundAccessPriorityTable
            (&i4dot1dBasePort, &i4dot1dRegenUserPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dPortOutboundAccessPriorityTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4dot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4dot1dRegenUserPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4dot1dRegenUserPriority;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dTrafficClassTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;
    INT4                i4dot1dTrafficClassPriority;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTrafficClassTable (&i4dot1dBasePort,
                                                    &i4dot1dTrafficClassPriority)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTrafficClassTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4dot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4dot1dTrafficClassPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4dot1dTrafficClassPriority;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dUserPriorityRegenTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;
    INT4                i4dot1dUserPriority;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dUserPriorityRegenTable (&i4dot1dBasePort,
                                                         &i4dot1dUserPriority)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dUserPriorityRegenTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4dot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4dot1dUserPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4dot1dUserPriority;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dPortPriorityTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dPortPriorityTable (&i4dot1dBasePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dPortPriorityTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1dPortCapabilitiesTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dPortCapabilitiesTable (&i4dot1dBasePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dPortCapabilitiesTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
