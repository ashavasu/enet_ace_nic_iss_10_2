/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanutl2.c,v 1.3 2014/01/24 12:26:20 siva Exp $
 *
 * Description: This file contains utility routines used in VLAN module.
 *
 *******************************************************************/

#include "vlaninc.h"
 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanAddSinglePortInDB                               */
 /*                                                                           */
 /* Description         : This function creates a node with u2VlanId and      */
 /*                       u2Port and then sets the u2Flag if the node is not  */
 /*                       created already.                                    */
 /*                       It just sets the u2Flag for the respective          */
 /*                       u2Vlan + u2Port Tuple if the node exists.           */
 /*                                                                           */
 /* Input(s)            : u2VlanId  - Vlan Identifier                         */
 /*                       u2Port    - Local Port identifier                   */
 /*                       u2Flag    - Port property flag                      */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
 /*****************************************************************************/
INT1
VlanAddSinglePortInDB (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;

    if (VlanCreatePortVlanMapEntry (u2VlanId, u2Port, &pPortVlanMapEntry)
        == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }
    pPortVlanMapEntry->u2BitMask |= u2Flag;
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanAddPortsInDB                                    */
 /*                                                                           */
 /* Description         : This function creates nodes with u2VlanId and       */
 /*                       the ports that are set in the ocalPortList and      */
 /*                       then sets the u2Flag for each new node.Saying       */
 /*                       that the node is not created already.               */
 /*                       It just sets the u2Flag for the respective          */
 /*                       (u2Vlan + port from LocalPortList) Tuple            */
 /*                       if the nodes are already present.                   */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
 /*****************************************************************************/
INT1
VlanAddPortsInDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteInd = 0;
    UINT1               u1BitIndex = 0;
    UINT1               u1PortFlag = 0;

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (LocalPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = LocalPortList[u2ByteInd];

        for (u1BitIndex = 0; ((u1BitIndex < VLAN_PORTS_PER_BYTE) &&
                              (u1PortFlag != 0)); u1BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                u1PortFlag = u1PortFlag << 1;
                continue;
            }
            u2Port = (u2ByteInd * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1;

            if (VlanCreatePortVlanMapEntry
                (u2VlanId, u2Port, &pPortVlanMapEntry) == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }
            pPortVlanMapEntry->u2BitMask |= u2Flag;

            u1PortFlag = u1PortFlag << 1;
            continue;
        }
    }
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanIsMemberPort                                    */
 /*                                                                           */
 /* Description         :  This function checks the particular port property  */
 /*                        is set for the given (u2VlanId  + u2Port) tuple.   */
 /*                        The result VLAN_TRUE/VLAN_FALSE is the output.     */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Port        - Port Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : pu1Result     - Pointer that holds the output       */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanIsMemberPort (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;

    *pu1Result = VLAN_FALSE;

    if ((pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port))
        != NULL)
    {
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            *pu1Result = VLAN_TRUE;
        }
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanResetSinglePortInDB                             */
 /*                                                                           */
 /* Description         : This function resets  a node with u2VlanId          */
 /*                       and u2Port  provided with the given u2Flag.         */
 /*                       If the u2BitMask for that particular (u2Vlanid +    */
 /*                       u2Port) is zero, then the node is deleted.          */
 /*                                                                           */
 /* Input(s)            : u2VlanId  - Vlan Identifier                         */
 /*                       u2Port    - Local Port identifier                   */
 /*                       u2Flag    - Port property flag                      */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
 /*****************************************************************************/
INT1
VlanResetSinglePortInDB (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;

    if ((pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port))
        == NULL)
    {
        return VLAN_FAILURE;
    }

    pPortVlanMapEntry->u2BitMask &= (~u2Flag);

    if (pPortVlanMapEntry->u2BitMask == 0)
    {
        if (VlanDeletePortVlanMapEntry (pPortVlanMapEntry) == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanResetPortsInDB                                  */
 /*                                                                           */
 /* Description         : This function resets in the data base local         */
 /*                       port list from the local port list providing        */
 /*                       with (u2Vlanid + u2Flag) combination.               */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
 /*****************************************************************************/
INT1
VlanResetPortsInDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteInd = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1BitIndex = 0;

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (LocalPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = LocalPortList[u2ByteInd];

        for (u1BitIndex = 0; ((u1BitIndex < VLAN_PORTS_PER_BYTE) &&
                              (u1PortFlag != 0)); u1BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                u1PortFlag = u1PortFlag << 1;
                continue;
            }
            u2Port = (u2ByteInd * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1;

            if ((pPortVlanMapEntry = VlanGetPortVlanMapEntry
                 (u2VlanId, u2Port)) == NULL)
            {
                u1PortFlag = u1PortFlag << 1;
                continue;
            }

            if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
            {
                pPortVlanMapEntry->u2BitMask &= (~u2Flag);
            }

            if (pPortVlanMapEntry->u2BitMask == 0)
            {
                if (VlanDeletePortVlanMapEntry (pPortVlanMapEntry)
                    == VLAN_FAILURE)
                {
                    return VLAN_FAILURE;
                }
            }

            u1PortFlag = u1PortFlag << 1;
            continue;
        }
    }
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanGetAddedDeletedPorts                            */
 /*                                                                           */
 /* Description         : It gives the new ports added in AddedPortList       */
 /*                       and existing ports deleted in DeletedPortList       */
 /*                       from the DB to InputPortList.                       */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       InputPortList - InputPortList                       */
 /*                                                                           */
 /* Output (s)          : AddedPortList - AddedPortList                       */
 /*                       DeletedPortList - Deleted PortList                  */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanGetAddedDeletedPorts (UINT2 u2VlanId, UINT2 u2Flag,
                          tLocalPortList InputPortList,
                          tLocalPortList AddedPortList,
                          tLocalPortList DeletedPortList)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteInd = 0;
    UINT1               u1BitIndex = 0;
    UINT1               u1PortFlag = 0;

    /* the following for loops are important.
     * they will affect the performance.
     * have to think alternate approach.
     * in worst case, the below description can be followed.
     */
    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        u1PortFlag = InputPortList[u2ByteInd];

        for (u1BitIndex = 0; u1BitIndex < VLAN_PORTS_PER_BYTE; u1BitIndex++)
        {
            u2Port = (u2ByteInd * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1;

            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port);
                if (pPortVlanMapEntry == NULL)
                {
                    /* Bit is not set in the InputPortList.
                     * And pPortVlanMapEntry is also not present.
                     * Hence continue. */
                    u1PortFlag = u1PortFlag << 1;
                    continue;
                }
                else if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
                {
                    /* Bit is not set in the InputPortList.
                     * But it is set in the pPortVlanMapEntry.
                     * Hence it should be added in DeletedPortList. */
                    VLAN_SET_MEMBER_PORT (DeletedPortList, u2Port);
                }
                u1PortFlag = u1PortFlag << 1;
                continue;
            }

            if ((pPortVlanMapEntry = VlanGetPortVlanMapEntry
                 (u2VlanId, u2Port)) == NULL)
            {
                /* Bit is set in the InputPortList.
                 * Getting the pPortVlanMapEntry is NULL
                 * Hence it should be added in AddedPortList. */
                VLAN_SET_MEMBER_PORT (AddedPortList, u2Port);
            }
            else if ((pPortVlanMapEntry->u2BitMask & u2Flag) != u2Flag)
            {
                /* Bit is set in the InputPortList.
                 * But it is not set in the pPortVlanEntry.
                 * Hence it should be added in Added PortList */

                VLAN_SET_MEMBER_PORT (AddedPortList, u2Port);
            }
            u1PortFlag = u1PortFlag << 1;
        }
    }
    return;
}

/* Added for tStaticVlanEntry->EgressPorts */
 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanIsNoPortsPresent                                */
 /*                                                                           */
 /* Description         : It checks that there are no ports present for the   */
 /*                       givent u2VlanId + Port property combination.        */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output (s)          : pu1Result     - VLAN_TURE if no ports present       */
 /*                                     - VLAN_FALSE if any port present.     */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanIsNoPortsPresent (UINT2 u2VlanId, UINT2 u2Flag, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    *pu1Result = VLAN_FALSE;

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry
            (u2VlanId, u2Port)) != NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            *pu1Result = VLAN_FALSE;
            return;
        }
        u2Port = pPortVlanMapEntry->u2Port;
    }
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanWrSetAllPorts                                   */
 /*                                                                           */
 /* Description         : tPortVlanMapEntry will be created for each          */
 /*                       tVlanPortEntry providing with the u2VlanId.         */
 /*                       tPortVlanMapEntry's bitmask will be set based       */
 /*                       on the u2Flag.                                      */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_SUCCESS/VLAN_FAILURE                           */
 /*****************************************************************************/
INT1
VlanWrSetAllPorts (UINT2 u2VlanId, UINT2 u2Flag)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port = 0;

    for (u2Port = 1; u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry != NULL)
        {
            if (VlanCreatePortVlanMapEntry
                (u2VlanId, u2Port, &pPortVlanMapEntry) == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }
            pPortVlanMapEntry->u2BitMask |= u2Flag;
        }
    }
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanResetPortsInLocalPortList                       */
 /*                                                                           */
 /* Description         : This function resets in the local port list         */
 /*                       of local variable from the data base RBTree         */
 /*                       providing with (u2Vlanid + u2Flag) combination.     */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
 /*****************************************************************************/
VOID
VlanResetPortsInLocalPortList (UINT2 u2VlanId, UINT2 u2Flag,
                               tLocalPortList LocalPortList)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry
            (u2VlanId, u2Port)) != NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            VLAN_RESET_MEMBER_PORT (LocalPortList, pPortVlanMapEntry->u2Port);
        }
        u2Port = pPortVlanMapEntry->u2Port;
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanGetPortsFromDB                                  */
 /*                                                                           */
 /* Description         : This function reads the (u2VlanId + u2Port)         */
 /*                       tuple. And if the u2Flag for this tuple is          */
 /*                       set in the data base, then it will be set in the    */
 /*                       LocalPortList.                                      */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Returns             :  NONE.                                              */
 /*****************************************************************************/
VOID
VlanGetPortsFromDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    /* Do not memset LocalPortList since, it may have some ports already set */

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry
            (u2VlanId, u2Port)) != NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            VLAN_SET_MEMBER_PORT (LocalPortList, pPortVlanMapEntry->u2Port);
        }
        u2Port = pPortVlanMapEntry->u2Port;
    }
    return;
}

 /*****************************************************************************/
 /* Function Name       : VlanIsPortListSubsetToDB                            */
 /*                                                                           */
 /* Description         : The ports that are set in the given LocalPortList   */
 /*                       should be present in the data base too with         */
 /*                       the flag u2Flag enabled on the data base.           */
 /*                                                                           */
 /* Input(s)            : u2VlanId     - Vlan Identifier for Data Base        */
 /*                       u1Flag       - Flag used to define LocalPortList    */
 /*                       LocalPortList- Local port list if u1Flag = 0,       */
 /*                                       DB port list if u1Flag != 0.        */
 /*                                                                           */
 /* Output (s)          : pu1Result     - VLAN_TRUE/VLAN_FALSE                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanIsPortListSubsetToDB (UINT2 u2VlanId, UINT2 u2Flag,
                          tLocalPortList LocalPortList, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteInd = 0;
    UINT1               u1BitIndex = 0;
    UINT1               u1PortFlag = 0;
    *pu1Result = VLAN_FALSE;

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        u1PortFlag = LocalPortList[u2ByteInd];

        if (u1PortFlag == 0)
        {
            continue;
        }

        for (u1BitIndex = 0; u1BitIndex < VLAN_PORTS_PER_BYTE; u1BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }
            /* Port is set in the LocalPortList.
             * It should present in the Database. */
            u2Port =
                (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1);

            if ((pPortVlanMapEntry = VlanGetPortVlanMapEntry
                 (u2VlanId, u2Port)) == NULL)
            {
                /* Port that is set in the LocalPortList is not
                 * present in the Data base. Hence setting VLAN_FALSE.*/
                *pu1Result = VLAN_FALSE;
                return;
            }
            if ((pPortVlanMapEntry->u2BitMask & u2Flag) != u2Flag)
            {
                /* Port that is set in the LocalPortList is present
                 * in Database. But it is not having the property 
                 * that is given by u2Flag. Hence setting VLAN_FALSE.*/
                *pu1Result = VLAN_FALSE;
                return;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
            continue;
        }
    }
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /* Function Name       : VlanIsDB2SubsetToDB1                                */
 /*                                                                           */
 /* Description         : All the ports that are set with (Flag2 + VLanId2)   */
 /*                       should be subset of the ports that can be defined   */
 /*                       from the (flag1 + VlanId1).                         */
 /*                                                                           */
 /* Input(s)            : u2VlanId1     - Vlan Identifier for Data Base 1     */
 /*                       u1Flag1       - Flag used to define LocalPortList1  */
 /*                       u2VlanId2     - VlanIdentifier for Data base 2.     */
 /*                       u1Flag2       - Flag used to define LocalPortList2  */
 /*                                                                           */
 /* Output (s)          : pu1Result     - VLAN_TRUE/VLAN_FALSE                */
 /*                                                                           */
 /* Returns             : None                                                */
 /*****************************************************************************/
VOID
VlanIsDB2SubsetToDB1 (UINT2 u2VlanId1, UINT2 u2Flag1, UINT2 u2VlanId2,
                      UINT2 u2Flag2, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry1 = NULL;
    tPortVlanMapEntry  *pPortVlanMapEntry2 = NULL;
    UINT2               u2Port = 0;

    *pu1Result = VLAN_FALSE;

    while ((pPortVlanMapEntry2 = VlanPortVlanMapTblGetNextEntry
            (u2VlanId2, u2Port)) != NULL)
    {
        if (u2VlanId2 != pPortVlanMapEntry2->u2VlanId)
        {
            break;
        }
        if ((pPortVlanMapEntry2->u2BitMask & u2Flag2) == u2Flag2)
        {
            /* DB entry 2 is set with port property u2Flag2.
             * It is expected that the same should be present for
             * data base entry 1. */
            if ((pPortVlanMapEntry1 = VlanGetPortVlanMapEntry
                 (u2VlanId1, u2Port)) == NULL)
            {
                /* data base entry 1 is not present. but data base entry 2 is 
                 * present. That is DB2 is not subset of DB1. Hence, setting
                 * VLAN_FALSE. */
                *pu1Result = VLAN_FALSE;
                return;
            }
            if ((pPortVlanMapEntry1->u2BitMask & u2Flag1) != u2Flag1)
            {
                /* data base entry 2 is present. data base entry 1 is also
                 * present. But port property is not set in data base entry 1.
                 * Hence, setting VLAN_FALSE. */
                *pu1Result = VLAN_FALSE;
                return;
            }
        }
        u2Port = pPortVlanMapEntry2->u2Port;
    }
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /* Function Name       : VlanIsDBPortsSubsetToPortList                       */
 /*                                                                           */
 /* Description         : All ports that are present in the data base (VlanId */
 /*                       + u2Flag) should be set in the LocalPortList.       */
 /*                                                                           */
 /* Input(s)            : u2VlanId     - Vlan Identifier for Data Base        */
 /*                       u1Flag       - Flag used to define LocalPortList    */
 /*                       LocalPortList- Local port list if u1Flag = 0,       */
 /*                                       DB port list if u1Flag != 0.        */
 /*                                                                           */
 /* Output (s)          : pu1Result     - VLAN_TRUE/VLAN_FALSE                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanIsDBPortsSubsetToPortList (UINT2 u2VlanId, UINT2 u2Flag,
                               tLocalPortList LocalPortList, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;

    *pu1Result = VLAN_FALSE;

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry (u2VlanId,
                                                                u2Port)) !=
           NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            VLAN_IS_MEMBER_PORT (LocalPortList, pPortVlanMapEntry->u2Port,
                                 *pu1Result);
            /* Data base port property for u2Port is present.
             * But the u2Port is not set in the LocalPortList.
             * Hence returning with VLAN_FALSE. */
            if (*pu1Result == VLAN_FALSE)
            {
                return;
            }
        }
        u2Port = pPortVlanMapEntry->u2Port;
    }
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanIsAllPortsMatching                              */
 /*                                                                           */
 /* Description         : This function checks with bits that are set in      */
 /*                       the LocalPortList of local variable to the          */
 /*                       tPortVlanMapEntry's bitmask.                        */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Output(s)           : pu1Result     - VLAN_TRUE/VLAN_FALSE                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanIsAllPortsMatching (UINT2 u2VlanId, UINT2 u2Flag,
                        tLocalPortList LocalPortList, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT1               u1BitIndex = 0;
    UINT1               u1PortFlag = 0;

    *pu1Result = VLAN_FALSE;

    for (u2ByteIndex = 0; u2ByteIndex < VLAN_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = LocalPortList[u2ByteIndex];
        for (u1BitIndex = 0; u1BitIndex < VLAN_PORTS_PER_BYTE; u1BitIndex++)
        {
            u2Port =
                (UINT2) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1);

            pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port);

            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                if ((pPortVlanMapEntry != NULL) &&
                    ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag))
                {
                    /* u2Port is not set in the LocalPortList.
                     * u2Port with port property is present in data base.
                     * Hence LocalPortList and Data base are not matching.*/
                    *pu1Result = VLAN_FALSE;
                    return;
                }
            }
            else
            {
                if (pPortVlanMapEntry == NULL)
                {
                    /* u2Port is set in the LocalPortList.
                     * u2Port is not present in the data base.
                     * Hence LocalPortList and Data base are not matching. */
                    *pu1Result = VLAN_FALSE;
                    return;
                }
                if ((pPortVlanMapEntry->u2BitMask & u2Flag) != u2Flag)
                {
                    /* u2Port is set in the LocalPortList.
                     * u2Port property in data base is not set.
                     * Hence LocalPortList and Data base are not matching. */
                    *pu1Result = VLAN_FALSE;
                    return;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
            continue;
        }
    }
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanArePortsExclusive                               */
 /*                                                                           */
 /* Description         : This function checks that the ports that are set in */
 /*                       LocalPortList are not set in the Data base ports    */
 /*                       with (vlan + flag) in tPortVlanMapEntry.            */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Output(s)           : pu1Result     - VLAN_TRUE/VLAN_FALSE                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanArePortsExclusive (UINT2 u2VlanId, UINT2 u2Flag,
                       tLocalPortList LocalPortList, UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT1               u1BitIndex = 0;
    UINT1               u1PortFlag = 0;

    *pu1Result = VLAN_FALSE;

    for (u2ByteIndex = 0; u2ByteIndex < VLAN_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = LocalPortList[u2ByteIndex];

        if (u1PortFlag == 0)
        {
            continue;
        }

        for (u1BitIndex = 0; ((u1BitIndex < VLAN_PORTS_PER_BYTE) &&
                              (u1PortFlag != 0)); u1BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }

            u2Port =
                (UINT2) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1);

            pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port);

            if ((pPortVlanMapEntry != NULL) &&
                ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag))
            {
                /* u2Port is present in LocalPortList.
                 * u2Port with its property is also present in the data base.
                 * Hence LocalPortList and Data base for u2Flag are not
                 * mutually exclusive. */
                *pu1Result = VLAN_FALSE;
                return;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
            continue;
        }
    }
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanConvertDBPortsToIfPortList                      */
 /*                                                                           */
 /* Description         : This function reads the port from the (u2Vlanid +   */
 /*                       u2Flag) in tPortVlanMapEntry and sets the IfIndex   */
 /*                       of LocalPort into IfPortList.                       */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       PortList      - IfPortList                          */
 /*                                                                           */
 /* Output(s)           : None.                                               */
 /*                                                                           */
 /* Returns             : None.                                               */
 /*****************************************************************************/
VOID
VlanConvertDBPortsToIfPortList (UINT2 u2VlanId, UINT2 u2Flag,
                                tPortList PortList)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry (u2VlanId,
                                                                u2Port)) !=
           NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (pPortVlanMapEntry->u2Port);

            if (pPortEntry != NULL)
            {
                VLAN_SET_MEMBER_PORT_LIST (PortList, pPortEntry->u4IfIndex);
            }
        }
        u2Port = pPortVlanMapEntry->u2Port;
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanAddDelPortsInDB                                 */
 /*                                                                           */
 /* Description         : This function creates nodes with u2VlanId and       */
 /*                       the ports that are set in the ocalPortList and      */
 /*                       then sets the u2Flag for each new node.Saying       */
 /*                       that the node is not created already.               */
 /*                       It just sets the u2Flag for the respective          */
 /*                       (u2Vlan + port from LocalPortList) Tuple            */
 /*                       if the nodes are already present.                   */
 /*                       It removes the flag if the node is previously for   */
 /*                       for the u2Flag. If no more port property for that   */
 /*                       u2Vlan + u2Port is present, then tat entry is deltd. */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                       LocalPortList - LocalPortList                       */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
 /*****************************************************************************/
INT1
VlanAddDelPortsInDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteInd = 0;
    UINT1               u1BitIndex = 0;
    UINT1               u1PortFlag = 0;

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        u1PortFlag = LocalPortList[u2ByteInd];

        for (u1BitIndex = 0; (u1BitIndex < VLAN_PORTS_PER_BYTE); u1BitIndex++)
        {
            u2Port =
                (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) + u1BitIndex + 1);

            pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port);

            if ((pPortVlanMapEntry != NULL) &&
                ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag))
            {
                if ((u1PortFlag & VLAN_BIT8) == 0)
                {
                    /* u2Port is not set in the LocalPortList.
                     * Hence u2Port's property in data base is reset.*/
                    pPortVlanMapEntry->u2BitMask =
                        (UINT2) ((pPortVlanMapEntry->u2BitMask) & (~u2Flag));
                }
                if (pPortVlanMapEntry->u2BitMask == 0)

                {
                    /* (u2Vlan + u2Port) does not have any other property.
                     * Hence that RB node is deleted. */
                    if (VlanDeletePortVlanMapEntry (pPortVlanMapEntry)
                        == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }
                }
            }

            if ((u1PortFlag & VLAN_BIT8) == 0)
            {
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }
            /* u2Port is set in the LocalPortList. (u2Vlan + u2Port)
             * is not present in the data base. Hence it is newly created. */
            if (VlanCreatePortVlanMapEntry
                (u2VlanId, u2Port, &pPortVlanMapEntry) == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }
            pPortVlanMapEntry->u2BitMask |= u2Flag;

            u1PortFlag = (UINT1) (u1PortFlag << 1);
            continue;
        }
    }
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanIsPortOnlyMember                                */
 /*                                                                           */
 /* Description         :  This function checks the particular port property  */
 /*                        is set only for the given (u2VlanId + u2Port) tuple */
 /*                        The result VLAN_TRUE/VLAN_FALSE is the output.     */
 /*                                                                           */
 /* Input(s)            : u2VlanId      - Vlan Identifier                     */
 /*                       u2Port        - Port Identifier                     */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : pu1Result     - Pointer that holds the output       */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanIsPortOnlyMember (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag,
                      UINT1 *pu1Result)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2DBPort = 0;

    *pu1Result = VLAN_FALSE;

    if ((pPortVlanMapEntry = VlanGetPortVlanMapEntry (u2VlanId, u2Port))
        != NULL)
    {
        if ((pPortVlanMapEntry->u2BitMask & u2Flag) != u2Flag)
        {
            /*The given port is not even present in DB */
            VlanAddSinglePortInDB (u2VlanId, u2Port, u2Flag);
            *pu1Result = VLAN_FALSE;
            return;
        }
    }

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry
            (u2VlanId, u2DBPort)) != NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }

        if ((u2DBPort == u2Port)
            && ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag))
        {
            /*The given port is there in DB so continues and check if any other port is present */
            continue;
        }
        else
        {
            /*some other port is present than the given port */
            VlanAddSinglePortInDB (u2VlanId, u2Port, u2Flag);
            *pu1Result = VLAN_FALSE;
            return;
        }
    }

    /*Only the given port is present in DB */
    *pu1Result = VLAN_TRUE;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : VlanResetAllPortInDB                                */
 /*                                                                           */
 /* Description         : This function resets  all the node with u2VlanId    */
 /*                       provided with the given u2Flag.                     */
 /*                       If the u2BitMask for a particular (u2Vlanid +       */
 /*                       u2Port) is zero, then that node is deleted.         */
 /*                                                                           */
 /* Input(s)            : u2VlanId  - Vlan Identifier                         */
 /*                       u2Flag    - Port property flag                      */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
VlanResetAllPortInDB (UINT2 u2VlanId, UINT2 u2Flag)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    UINT2               u2Port = 0;

    while ((pPortVlanMapEntry = VlanPortVlanMapTblGetNextEntry
            (u2VlanId, u2Port)) != NULL)
    {
        if (u2VlanId != pPortVlanMapEntry->u2VlanId)
        {
            break;
        }

        if ((pPortVlanMapEntry->u2BitMask & u2Flag) == u2Flag)
        {
            pPortVlanMapEntry->u2BitMask =
                (UINT2) ((pPortVlanMapEntry->u2BitMask) & (~u2Flag));
        }

        if (pPortVlanMapEntry->u2BitMask == 0)
        {
            if (VlanDeletePortVlanMapEntry (pPortVlanMapEntry) == VLAN_FAILURE)
            {
                return;
            }
        }
        u2Port = pPortVlanMapEntry->u2Port;
    }
    return;
}
