/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbplw.c,v 1.28 2016/01/11 12:56:23 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"

/* LOW LEVEL Routines for Table : FsMIPbContextInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbContextInfoTable
 Input       :  The Indices
                FsMIPbContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
extern UINT4 FsMIPbPortCVlanCounterStatus [12];

INT1
nmhValidateIndexInstanceFsMIPbContextInfoTable (INT4 i4FsMIPbContextId)
{

    if (VLAN_IS_CONTEXT_VALID (i4FsMIPbContextId) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    VlanReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbContextInfoTable
 Input       :  The Indices
                FsMIPbContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMIPbContextInfoTable (INT4 *pi4FsMIPbContextId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIPbContextInfoTable (-1, pi4FsMIPbContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbContextInfoTable
 Input       :  The Indices
                FsMIPbContextId
                nextFsMIPbContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbContextInfoTable (INT4 i4FsMIPbContextId,
                                       INT4 *pi4NextFsMIPbContextId)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VLAN_IS_CONTEXT_VALID (i4FsMIPbContextId) == VLAN_FALSE)
    {
        if (i4FsMIPbContextId < 0)
        {
            i4FsMIPbContextId = -1;    /* We will give the first entry */
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    for (u4ContextId = (UINT4) (i4FsMIPbContextId + 1); u4ContextId <=
         VLAN_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
            {
                *pi4NextFsMIPbContextId = (INT4) u4ContextId;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }

            VlanReleaseContext ();
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbMulticastMacLimit
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                retValFsMIPbMulticastMacLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbMulticastMacLimit (INT4 i4FsMIPbContextId,
                               UINT4 *pu4RetValFsMIPbMulticastMacLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbMulticastMacLimit (pu4RetValFsMIPbMulticastMacLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelStpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                retValFsMIPbTunnelStpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelStpAddress (INT4 i4FsMIPbContextId,
                              tMacAddr * pRetValFsMIPbTunnelStpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (pRetValFsMIPbTunnelStpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelLacpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                retValFsMIPbTunnelLacpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelLacpAddress (INT4 i4FsMIPbContextId,
                               tMacAddr * pRetValFsMIPbTunnelLacpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (pRetValFsMIPbTunnelLacpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelDot1xAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                retValFsMIPbTunnelDot1xAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelDot1xAddress (INT4 i4FsMIPbContextId,
                                tMacAddr * pRetValFsMIPbTunnelDot1xAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (pRetValFsMIPbTunnelDot1xAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelGvrpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                retValFsMIPbTunnelGvrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelGvrpAddress (INT4 i4FsMIPbContextId,
                               tMacAddr * pRetValFsMIPbTunnelGvrpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (pRetValFsMIPbTunnelGvrpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelGmrpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                retValFsMIPbTunnelGmrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelGmrpAddress (INT4 i4FsMIPbContextId,
                               tMacAddr * pRetValFsMIPbTunnelGmrpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (pRetValFsMIPbTunnelGmrpAddress);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbMulticastMacLimit
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                setValFsMIPbMulticastMacLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbMulticastMacLimit (INT4 i4FsMIPbContextId,
                               UINT4 u4SetValFsMIPbMulticastMacLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbMulticastMacLimit (u4SetValFsMIPbMulticastMacLimit);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelStpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                setValFsMIPbTunnelStpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelStpAddress (INT4 i4FsMIPbContextId,
                              tMacAddr SetValFsMIPbTunnelStpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (SetValFsMIPbTunnelStpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelLacpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                setValFsMIPbTunnelLacpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelLacpAddress (INT4 i4FsMIPbContextId,
                               tMacAddr SetValFsMIPbTunnelLacpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (SetValFsMIPbTunnelLacpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelDot1xAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                setValFsMIPbTunnelDot1xAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelDot1xAddress (INT4 i4FsMIPbContextId,
                                tMacAddr SetValFsMIPbTunnelDot1xAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (SetValFsMIPbTunnelDot1xAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelGvrpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                setValFsMIPbTunnelGvrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelGvrpAddress (INT4 i4FsMIPbContextId,
                               tMacAddr SetValFsMIPbTunnelGvrpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (SetValFsMIPbTunnelGvrpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelGmrpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                setValFsMIPbTunnelGmrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelGmrpAddress (INT4 i4FsMIPbContextId,
                               tMacAddr SetValFsMIPbTunnelGmrpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (SetValFsMIPbTunnelGmrpAddress);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbMulticastMacLimit
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                testValFsMIPbMulticastMacLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbMulticastMacLimit (UINT4 *pu4ErrorCode, INT4 i4FsMIPbContextId,
                                  UINT4 u4TestValFsMIPbMulticastMacLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VLAN_IS_CONTEXT_VALID (i4FsMIPbContextId) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbMulticastMacLimit (pu4ErrorCode,
                                        u4TestValFsMIPbMulticastMacLimit);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelStpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                testValFsMIPbTunnelStpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelStpAddress (UINT4 *pu4ErrorCode, INT4 i4FsMIPbContextId,
                                 tMacAddr TestValFsMIPbTunnelStpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (TestValFsMIPbTunnelStpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelLacpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                testValFsMIPbTunnelLacpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelLacpAddress (UINT4 *pu4ErrorCode, INT4 i4FsMIPbContextId,
                                  tMacAddr TestValFsMIPbTunnelLacpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (TestValFsMIPbTunnelLacpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelDot1xAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                testValFsMIPbTunnelDot1xAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelDot1xAddress (UINT4 *pu4ErrorCode, INT4 i4FsMIPbContextId,
                                   tMacAddr TestValFsMIPbTunnelDot1xAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (TestValFsMIPbTunnelDot1xAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelGvrpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                testValFsMIPbTunnelGvrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelGvrpAddress (UINT4 *pu4ErrorCode, INT4 i4FsMIPbContextId,
                                  tMacAddr TestValFsMIPbTunnelGvrpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (TestValFsMIPbTunnelGvrpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelGmrpAddress
 Input       :  The Indices
                FsMIPbContextId

                The Object 
                testValFsMIPbTunnelGmrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelGmrpAddress (UINT4 *pu4ErrorCode, INT4 i4FsMIPbContextId,
                                  tMacAddr TestValFsMIPbTunnelGmrpAddress)
{
    UNUSED_PARAM (i4FsMIPbContextId);
    UNUSED_PARAM (TestValFsMIPbTunnelGmrpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbContextInfoTable
 Input       :  The Indices
                FsMIPbContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbContextInfoTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbPortInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbPortInfoTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbPortInfoTable (INT4 i4FsMIPbPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsPbPortInfoTable (u2LocalPort);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbPortInfoTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMIPbPortInfoTable (INT4 *pi4FsMIPbPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIPbPortInfoTable (0, pi4FsMIPbPort);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbPortInfoTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbPortInfoTable (INT4 i4FsMIPbPort, INT4 *pi4NextFsMIPbPort)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    while (VlanGetNextPortInSystem (i4FsMIPbPort, &u4IfIndex) == VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE &&
                    VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_FALSE)
                {
                    *pi4NextFsMIPbPort = (INT4) u4IfIndex;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }

                VlanReleaseContext ();
            }
        }

        i4FsMIPbPort = u4IfIndex;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbPortSVlanClassificationMethod
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortSVlanClassificationMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortSVlanClassificationMethod (INT4 i4FsMIPbPort,
                                           INT4 *pi4SVlanClassificationMethod)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortSVlanClassificationMethod ((INT4) u2LocalPort,
                                                 pi4SVlanClassificationMethod);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortSVlanIngressEtherType
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortSVlanIngressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortSVlanIngressEtherType (INT4 i4FsMIPbPort,
                                       INT4 *pi4SVlanIngressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortSVlanIngressEtherType ((INT4) u2LocalPort,
                                             pi4SVlanIngressEtherType);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortSVlanEgressEtherType
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortSVlanEgressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortSVlanEgressEtherType (INT4 i4FsMIPbPort,
                                      INT4 *pi4SVlanEgressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortSVlanEgressEtherType ((INT4) u2LocalPort,
                                            pi4SVlanEgressEtherType);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortSVlanEtherTypeSwapStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortSVlanEtherTypeSwapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortSVlanEtherTypeSwapStatus (INT4 i4FsMIPbPort,
                                          INT4 *pi4SVlanEtherTypeSwapStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortSVlanEtherTypeSwapStatus ((INT4) u2LocalPort,
                                                pi4SVlanEtherTypeSwapStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortSVlanTranslationStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortSVlanTranslationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortSVlanTranslationStatus (INT4 i4FsMIPbPort,
                                        INT4 *pi4SVlanTranslationStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortSVlanTranslationStatus ((INT4) u2LocalPort,
                                              pi4SVlanTranslationStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortUnicastMacLearning
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortUnicastMacLearning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortUnicastMacLearning (INT4 i4FsMIPbPort,
                                    INT4 *pi4PortUnicastMacLearning)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortUnicastMacLearning ((INT4) u2LocalPort,
                                          pi4PortUnicastMacLearning);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortUnicastMacLimit
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortUnicastMacLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortUnicastMacLimit (INT4 i4FsMIPbPort,
                                 UINT4 *pu4PortUnicastMacLimit)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortUnicastMacLimit ((INT4) u2LocalPort,
                                       pu4PortUnicastMacLimit);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortBundleStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                retValFsMIPbPortBundleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortBundleStatus (INT4 i4FsMIPbPort,
                              INT4 *pi4RetValFsMIPbPortBundleStatus)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortBundleStatus ((INT4) u2LocalPort,
                                    pi4RetValFsMIPbPortBundleStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortMultiplexStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                retValFsMIPbPortMultiplexStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortMultiplexStatus (INT4 i4FsMIPbPort,
                                 INT4 *pi4RetValFsMIPbPortMultiplexStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortMultiplexStatus ((INT4) u2LocalPort,
                                       pi4RetValFsMIPbPortMultiplexStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbPortSVlanClassificationMethod
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortSVlanClassificationMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortSVlanClassificationMethod (INT4 i4FsMIPbPort,
                                           INT4 i4SVlanClassificationMethod)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortSVlanClassificationMethod ((INT4) u2LocalPort,
                                                 i4SVlanClassificationMethod);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortSVlanIngressEtherType
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortSVlanIngressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortSVlanIngressEtherType (INT4 i4FsMIPbPort,
                                       INT4 i4SVlanIngressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortSVlanIngressEtherType ((INT4) u2LocalPort,
                                             i4SVlanIngressEtherType);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortSVlanEgressEtherType
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortSVlanEgressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortSVlanEgressEtherType (INT4 i4FsMIPbPort,
                                      INT4 i4SVlanEgressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortSVlanEgressEtherType ((INT4) u2LocalPort,
                                            i4SVlanEgressEtherType);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortSVlanEtherTypeSwapStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortSVlanEtherTypeSwapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortSVlanEtherTypeSwapStatus (INT4 i4FsMIPbPort,
                                          INT4 i4SVlanEtherTypeSwapStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortSVlanEtherTypeSwapStatus ((INT4) u2LocalPort,
                                                i4SVlanEtherTypeSwapStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortSVlanTranslationStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortSVlanTranslationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortSVlanTranslationStatus (INT4 i4FsMIPbPort,
                                        INT4 i4SVlanTranslationStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortSVlanTranslationStatus ((INT4) u2LocalPort,
                                              i4SVlanTranslationStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortUnicastMacLearning
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortUnicastMacLearning
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortUnicastMacLearning (INT4 i4FsMIPbPort,
                                    INT4 i4PortUnicastMacLearning)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortUnicastMacLearning ((INT4) u2LocalPort,
                                          i4PortUnicastMacLearning);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortUnicastMacLimit
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortUnicastMacLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortUnicastMacLimit (INT4 i4FsMIPbPort, UINT4 u4PortUnicastMacLimit)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortUnicastMacLimit ((INT4) u2LocalPort,
                                       u4PortUnicastMacLimit);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortBundleStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                setValFsMIPbPortBundleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortBundleStatus (INT4 i4FsMIPbPort,
                              INT4 i4SetValFsMIPbPortBundleStatus)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortBundleStatus ((INT4) u2LocalPort,
                                    i4SetValFsMIPbPortBundleStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortMultiplexStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                setValFsMIPbPortMultiplexStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortMultiplexStatus (INT4 i4FsMIPbPort,
                                 INT4 i4SetValFsMIPbPortMultiplexStatus)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortMultiplexStatus ((INT4) u2LocalPort,
                                       i4SetValFsMIPbPortMultiplexStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortSVlanClassificationMethod
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortSVlanClassificationMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortSVlanClassificationMethod (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIPbPort,
                                              INT4 i4SVlanClassificationMethod)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortSVlanClassificationMethod (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4SVlanClassificationMethod);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortSVlanIngressEtherType
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortSVlanIngressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortSVlanIngressEtherType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIPbPort,
                                          INT4 i4SVlanIngressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortSVlanIngressEtherType (pu4ErrorCode,
                                                (INT4) u2LocalPort,
                                                i4SVlanIngressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortSVlanEgressEtherType
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortSVlanEgressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortSVlanEgressEtherType (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIPbPort,
                                         INT4 i4SVlanEgressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortSVlanEgressEtherType (pu4ErrorCode,
                                               (INT4) u2LocalPort,
                                               i4SVlanEgressEtherType);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortSVlanEtherTypeSwapStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortSVlanEtherTypeSwapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortSVlanEtherTypeSwapStatus (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIPbPort,
                                             INT4 i4SVlanEtherTypeSwapStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortSVlanEtherTypeSwapStatus (pu4ErrorCode,
                                                   (INT4) u2LocalPort,
                                                   i4SVlanEtherTypeSwapStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortSVlanTranslationStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortSVlanTranslationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortSVlanTranslationStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIPbPort,
                                           INT4 i4SVlanTranslationStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortSVlanTranslationStatus (pu4ErrorCode,
                                                 (INT4) u2LocalPort,
                                                 i4SVlanTranslationStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortUnicastMacLearning
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortUnicastMacLearning
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortUnicastMacLearning (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIPbPort,
                                       INT4 i4UnicastMacLearning)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortUnicastMacLearning (pu4ErrorCode,
                                             (INT4) u2LocalPort,
                                             i4UnicastMacLearning);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortUnicastMacLimit
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortUnicastMacLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortUnicastMacLimit (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIPbPort, UINT4 u4UnicastMacLimit)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortUnicastMacLimit (pu4ErrorCode,
                                          (INT4) u2LocalPort,
                                          u4UnicastMacLimit);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortBundleStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                testValFsMIPbPortBundleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortBundleStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                 INT4 i4TestValFsMIPbPortBundleStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortBundleStatus (pu4ErrorCode,
                                       (INT4) u2LocalPort,
                                       i4TestValFsMIPbPortBundleStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortMultiplexStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                testValFsMIPbPortMultiplexStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortMultiplexStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                    INT4 i4TestValFsMIPbPortMultiplexStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortMultiplexStatus (pu4ErrorCode,
                                          (INT4) u2LocalPort,
                                          i4TestValFsMIPbPortMultiplexStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbPortInfoTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbPortInfoTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbSrcMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIPbSrcMacSVlanTable (INT4 i4FsMIPbPort,
                                                tMacAddr SrcMacAddress)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbSrcMacSVlanTable ((INT4) u2LocalPort,
                                                      SrcMacAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbSrcMacSVlanTable (INT4 *pi4FsMIPbPort,
                                        tMacAddr * pFsMIPbSrcMacAddress)
{
    tMacAddr            SrcMacAddress;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;

    MEMSET (SrcMacAddress, 0, sizeof (tMacAddr));

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbSrcMacSVlanTable ((INT4) u4IfIndex,
                                                   pi4FsMIPbPort,
                                                   SrcMacAddress,
                                                   pFsMIPbSrcMacAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbSrcMacAddress
                nextFsMIPbSrcMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbSrcMacSVlanTable (INT4 i4FsMIPbPort,
                                       INT4 *pi4NextFsMIPbPort,
                                       tMacAddr FsMIPbSrcMacAddress,
                                       tMacAddr * pNextFsMIPbSrcMac)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbSrcMacSVlanTable ((INT4) u2LocalPort,
                                                         pi4NextFsMIPbPort,
                                                         FsMIPbSrcMacAddress,
                                                         pNextFsMIPbSrcMac) ==
                    SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/

        MEMSET (FsMIPbSrcMacAddress, 0, sizeof (tMacAddr));

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbSrcMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
                
                The Object 
                retValFsMIPbSrcMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSrcMacSVlan (INT4 i4FsMIPbPort, tMacAddr FsMIPbSrcMacAddress,
                         INT4 *pi4RetValFsMIPbSrcMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsPbSrcMacSVlan ((INT4) u2LocalPort,
                                      FsMIPbSrcMacAddress,
                                      pi4RetValFsMIPbSrcMacSVlan);
    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbSrcMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
                
                The Object 
                retValFsMIPbSrcMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSrcMacRowStatus (INT4 i4FsMIPbPort, tMacAddr FsMIPbSrcMacAddress,
                             INT4 *pi4RetValFsMIPbSrcMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbSrcMacRowStatus ((INT4) u2LocalPort,
                                          FsMIPbSrcMacAddress,
                                          pi4RetValFsMIPbSrcMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbSrcMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
                
                The Object 
                setValFsMIPbSrcMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSrcMacSVlan (INT4 i4FsMIPbPort, tMacAddr FsMIPbSrcMacAddress,
                         INT4 i4SetValFsMIPbSrcMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbSrcMacSVlan ((INT4) u2LocalPort, FsMIPbSrcMacAddress,
                                      i4SetValFsMIPbSrcMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbSrcMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
                
                The Object 
                setValFsMIPbSrcMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSrcMacRowStatus (INT4 i4FsMIPbPort, tMacAddr FsMIPbSrcMacAddress,
                             INT4 i4SetValFsMIPbSrcMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbSrcMacRowStatus ((INT4) u2LocalPort,
                                          FsMIPbSrcMacAddress,
                                          i4SetValFsMIPbSrcMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSrcMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
                
                The Object 
                testValFsMIPbSrcMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSrcMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                            tMacAddr FsMIPbSrcMacAddress,
                            INT4 i4TestValFsMIPbSrcMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSrcMacSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                         FsMIPbSrcMacAddress,
                                         i4TestValFsMIPbSrcMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSrcMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
                
                The Object 
                testValFsMIPbSrcMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSrcMacRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIPbPort,
                                tMacAddr FsMIPbSrcMacAddress,
                                INT4 i4TestValFsMIPbSrcMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSrcMacRowStatus (pu4ErrorCode,
                                             (INT4) u2LocalPort,
                                             FsMIPbSrcMacAddress,
                                             i4TestValFsMIPbSrcMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbSrcMacSVlanTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbDstMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIPbDstMacSVlanTable (INT4 i4FsMIPbPort,
                                                tMacAddr DstMacAddress)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbDstMacSVlanTable ((INT4) u2LocalPort,
                                                      DstMacAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbDstMacSVlanTable (INT4 *pi4FsMIPbPort,
                                        tMacAddr * pFsMIPbDstMacAddress)
{
    tMacAddr            DstMacAddress;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;

    MEMSET (DstMacAddress, 0, sizeof (tMacAddr));

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbDstMacSVlanTable ((INT4) u4IfIndex,
                                                   pi4FsMIPbPort,
                                                   DstMacAddress,
                                                   pFsMIPbDstMacAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbDstMacAddress
                nextFsMIPbDstMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbDstMacSVlanTable (INT4 i4FsMIPbPort,
                                       INT4 *pi4NextFsMIPbPort,
                                       tMacAddr FsMIPbDstMac,
                                       tMacAddr * pNextFsMIPbDstMac)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbDstMacSVlanTable ((INT4) u2LocalPort,
                                                         pi4NextFsMIPbPort,
                                                         FsMIPbDstMac,
                                                         pNextFsMIPbDstMac) ==
                    SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/

        MEMSET (FsMIPbDstMac, 0, sizeof (tMacAddr));
    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbDstMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
                
                The Object 
                retValFsMIPbDstMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbDstMacSVlan (INT4 i4FsMIPbPort, tMacAddr FsMIPbDstMacAddress,
                         INT4 *pi4RetValFsMIPbDstMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsPbDstMacSVlan ((INT4) u2LocalPort,
                                      FsMIPbDstMacAddress,
                                      pi4RetValFsMIPbDstMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbDstMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
                
                The Object 
                retValFsMIPbDstMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbDstMacRowStatus (INT4 i4FsMIPbPort, tMacAddr FsMIPbDstMacAddress,
                             INT4 *pi4RetValFsMIPbDstMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsPbDstMacRowStatus ((INT4) u2LocalPort,
                                          FsMIPbDstMacAddress,
                                          pi4RetValFsMIPbDstMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbDstMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
                
                The Object 
                setValFsMIPbDstMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbDstMacSVlan (INT4 i4FsMIPbPort, tMacAddr FsMIPbDstMacAddress,
                         INT4 i4SetValFsMIPbDstMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsPbDstMacSVlan ((INT4) u2LocalPort,
                                      FsMIPbDstMacAddress,
                                      i4SetValFsMIPbDstMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbDstMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
                
                The Object 
                setValFsMIPbDstMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbDstMacRowStatus (INT4 i4FsMIPbPort, tMacAddr FsMIPbDstMacAddress,
                             INT4 i4SetValFsMIPbDstMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbDstMacRowStatus ((INT4) u2LocalPort,
                                          FsMIPbDstMacAddress,
                                          i4SetValFsMIPbDstMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbDstMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
                
                The Object 
                testValFsMIPbDstMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbDstMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                            tMacAddr FsMIPbDstMacAddress,
                            INT4 i4TestValFsMIPbDstMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbDstMacSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                         FsMIPbDstMacAddress,
                                         i4TestValFsMIPbDstMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbDstMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
                
                The Object 
                testValFsMIPbDstMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbDstMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                tMacAddr FsMIPbDstMacAddress,
                                INT4 i4TestValFsMIPbDstMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbDstMacRowStatus (pu4ErrorCode,
                                             (INT4) u2LocalPort,
                                             FsMIPbDstMacAddress,
                                             i4TestValFsMIPbDstMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIPbDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbDstMacSVlanTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbCVlanSrcMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbCVlanSrcMacSVlanTable (INT4 i4FsMIPbPort,
                                                     INT4 i4CVlanSrcMacCVlan,
                                                     tMacAddr CVlanSrcMacAddr)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbCVlanSrcMacSVlanTable ((INT4) u2LocalPort,
                                                           i4CVlanSrcMacCVlan,
                                                           CVlanSrcMacAddr);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMIPbCVlanSrcMacSVlanTable (INT4 *pi4FsMIPbPort,
                                             INT4 *pi4CVlanSrcMacCVlan,
                                             tMacAddr * pCVlanSrcMacAddr)
{
    tMacAddr            SrcMacAddr;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CVlan = 0;

    MEMSET (SrcMacAddr, 0, sizeof (tMacAddr));

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbCVlanSrcMacSVlanTable ((INT4) u4IfIndex,
                                                        pi4FsMIPbPort,
                                                        u4CVlan,
                                                        pi4CVlanSrcMacCVlan,
                                                        SrcMacAddr,
                                                        pCVlanSrcMacAddr));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                nextFsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                nextFsMIPbCVlanSrcMacAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbCVlanSrcMacSVlanTable (INT4 i4FsMIPbPort,
                                            INT4 *pi4NextFsMIPbPort,
                                            INT4 i4SrcMacCVlan,
                                            INT4 *pi4NextCVlan,
                                            tMacAddr SrcMacAddr,
                                            tMacAddr * pNextSrcMacAddr)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbCVlanSrcMacSVlanTable
                    ((INT4) u2LocalPort, pi4NextFsMIPbPort, i4SrcMacCVlan,
                     pi4NextCVlan, SrcMacAddr, pNextSrcMacAddr) == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        i4SrcMacCVlan = 0;
        MEMSET (SrcMacAddr, 0, sizeof (tMacAddr));

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanSrcMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                
                The Object 
                retValFsMIPbCVlanSrcMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanSrcMacSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanSrcMacCVlan,
                              tMacAddr FsMIPbCVlanSrcMacAddr,
                              INT4 *pi4RetValFsMIPbCVlanSrcMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanSrcMacSVlan ((INT4) u2LocalPort,
                                           i4FsMIPbCVlanSrcMacCVlan,
                                           FsMIPbCVlanSrcMacAddr,
                                           pi4RetValFsMIPbCVlanSrcMacSVlan);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanSrcMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                
                The Object 
                retValFsMIPbCVlanSrcMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanSrcMacRowStatus (INT4 i4FsMIPbPort,
                                  INT4 i4FsMIPbCVlanSrcMacCVlan,
                                  tMacAddr FsMIPbCVlanSrcMacAddr,
                                  INT4 *pi4RetValCVlanSrcMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsPbCVlanSrcMacRowStatus ((INT4) u2LocalPort,
                                               i4FsMIPbCVlanSrcMacCVlan,
                                               FsMIPbCVlanSrcMacAddr,
                                               pi4RetValCVlanSrcMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanSrcMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                
                The Object 
                setValFsMIPbCVlanSrcMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanSrcMacSVlan (INT4 i4FsMIPbPort,
                              INT4 i4FsMIPbCVlanSrcMacCVlan,
                              tMacAddr FsMIPbCVlanSrcMacAddr,
                              INT4 i4SetValFsMIPbCVlanSrcMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanSrcMacSVlan ((INT4) u2LocalPort,
                                           i4FsMIPbCVlanSrcMacCVlan,
                                           FsMIPbCVlanSrcMacAddr,
                                           i4SetValFsMIPbCVlanSrcMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanSrcMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                
                The Object 
                setValFsMIPbCVlanSrcMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanSrcMacRowStatus (INT4 i4FsMIPbPort,
                                  INT4 i4FsMIPbCVlanSrcMacCVlan,
                                  tMacAddr FsMIPbCVlanSrcMacAddr,
                                  INT4 i4SetValCVlanSrcMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return i1RetVal;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsPbCVlanSrcMacRowStatus ((INT4) u2LocalPort,
                                               i4FsMIPbCVlanSrcMacCVlan,
                                               FsMIPbCVlanSrcMacAddr,
                                               i4SetValCVlanSrcMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanSrcMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                
                The Object 
                testValFsMIPbCVlanSrcMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanSrcMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                 INT4 i4FsMIPbCVlanSrcMacCVlan,
                                 tMacAddr FsMIPbCVlanSrcMacAddr,
                                 INT4 i4TestValFsMIPbCVlanSrcMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbCVlanSrcMacSVlan (pu4ErrorCode,
                                              (INT4) u2LocalPort,
                                              i4FsMIPbCVlanSrcMacCVlan,
                                              FsMIPbCVlanSrcMacAddr,
                                              i4TestValFsMIPbCVlanSrcMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanSrcMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
                
                The Object 
                testValFsMIPbCVlanSrcMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanSrcMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                     INT4 i4FsMIPbCVlanSrcMacCVlan,
                                     tMacAddr FsMIPbCVlanSrcMacAddr,
                                     INT4 i4TestValCVlanSrcMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbCVlanSrcMacRowStatus (pu4ErrorCode,
                                                  (INT4) u2LocalPort,
                                                  i4FsMIPbCVlanSrcMacCVlan,
                                                  FsMIPbCVlanSrcMacAddr,
                                                  i4TestValCVlanSrcMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanSrcMacCVlan
                FsMIPbCVlanSrcMacAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbCVlanSrcMacSVlanTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbCVlanDstMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbCVlanDstMacSVlanTable (INT4 i4FsMIPbPort,
                                                     INT4 i4CVlanDstMacCVlan,
                                                     tMacAddr CVlanDstMacAddr)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbCVlanDstMacSVlanTable ((INT4) u2LocalPort,
                                                           i4CVlanDstMacCVlan,
                                                           CVlanDstMacAddr);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbCVlanDstMacSVlanTable (INT4 *pi4FsMIPbPort,
                                             INT4 *pi4CVlanDstMacCVlan,
                                             tMacAddr * pCVlanDstMacAddr)
{
    tMacAddr            DstMacAddr;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CVlan = 0;

    MEMSET (DstMacAddr, 0, sizeof (tMacAddr));

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbCVlanDstMacSVlanTable ((INT4) u4IfIndex,
                                                        pi4FsMIPbPort,
                                                        u4CVlan,
                                                        pi4CVlanDstMacCVlan,
                                                        DstMacAddr,
                                                        pCVlanDstMacAddr));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbCVlanDstMacCVlan
                nextFsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                nextFsMIPbCVlanDstMacAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbCVlanDstMacSVlanTable (INT4 i4FsMIPbPort,
                                            INT4 *pi4NextFsMIPbPort,
                                            INT4 i4DstMacCVlan,
                                            INT4 *pi4NextCVlan,
                                            tMacAddr DstMacAddr,
                                            tMacAddr * pNextDstMacAddr)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbCVlanDstMacSVlanTable
                    ((INT4) u2LocalPort, pi4NextFsMIPbPort, i4DstMacCVlan,
                     pi4NextCVlan, DstMacAddr, pNextDstMacAddr) == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        i4DstMacCVlan = 0;
        MEMSET (DstMacAddr, 0, sizeof (tMacAddr));

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanDstMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                
                The Object 
                retValFsMIPbCVlanDstMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanDstMacSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanDstMacCVlan,
                              tMacAddr FsMIPbCVlanDstMacAddr,
                              INT4 *pi4RetValFsMIPbCVlanDstMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanDstMacSVlan ((INT4) u2LocalPort,
                                           i4FsMIPbCVlanDstMacCVlan,
                                           FsMIPbCVlanDstMacAddr,
                                           pi4RetValFsMIPbCVlanDstMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanDstMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                
                The Object 
                retValFsMIPbCVlanDstMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanDstMacRowStatus (INT4 i4FsMIPbPort,
                                  INT4 i4FsMIPbCVlanDstMacCVlan,
                                  tMacAddr FsMIPbCVlanDstMacAddr,
                                  INT4 *pi4RetValCVlanDstMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanDstMacRowStatus ((INT4) u2LocalPort,
                                               i4FsMIPbCVlanDstMacCVlan,
                                               FsMIPbCVlanDstMacAddr,
                                               pi4RetValCVlanDstMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanDstMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                
                The Object 
                setValFsMIPbCVlanDstMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanDstMacSVlan (INT4 i4FsMIPbPort,
                              INT4 i4FsMIPbCVlanDstMacCVlan,
                              tMacAddr FsMIPbCVlanDstMacAddr,
                              INT4 i4SetValFsMIPbCVlanDstMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanDstMacSVlan ((INT4) u2LocalPort,
                                           i4FsMIPbCVlanDstMacCVlan,
                                           FsMIPbCVlanDstMacAddr,
                                           i4SetValFsMIPbCVlanDstMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanDstMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                
                The Object 
                setValFsMIPbCVlanDstMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanDstMacRowStatus (INT4 i4FsMIPbPort,
                                  INT4 i4FsMIPbCVlanDstMacCVlan,
                                  tMacAddr FsMIPbCVlanDstMacAddr,
                                  INT4 i4SetValCVlanDstMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanDstMacRowStatus ((INT4) u2LocalPort,
                                               i4FsMIPbCVlanDstMacCVlan,
                                               FsMIPbCVlanDstMacAddr,
                                               i4SetValCVlanDstMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanDstMacSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                
                The Object 
                testValFsMIPbCVlanDstMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanDstMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                 INT4 i4FsMIPbCVlanDstMacCVlan,
                                 tMacAddr FsMIPbCVlanDstMacAddr,
                                 INT4 i4TestValFsMIPbCVlanDstMacSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbCVlanDstMacSVlan (pu4ErrorCode,
                                              (INT4) u2LocalPort,
                                              i4FsMIPbCVlanDstMacCVlan,
                                              FsMIPbCVlanDstMacAddr,
                                              i4TestValFsMIPbCVlanDstMacSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanDstMacRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
                
                The Object 
                testValFsMIPbCVlanDstMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanDstMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                     INT4 i4FsMIPbCVlanDstMacCVlan,
                                     tMacAddr FsMIPbCVlanDstMacAddr,
                                     INT4 i4TestValCVlanDstMacRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbCVlanDstMacRowStatus (pu4ErrorCode,
                                                  (INT4) u2LocalPort,
                                                  i4FsMIPbCVlanDstMacCVlan,
                                                  FsMIPbCVlanDstMacAddr,
                                                  i4TestValCVlanDstMacRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstMacCVlan
                FsMIPbCVlanDstMacAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbCVlanDstMacSVlanTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbDscpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIPbDscpSVlanTable (INT4 i4FsMIPbPort,
                                              INT4 i4FsMIPbDscp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbDscpSVlanTable ((INT4) u2LocalPort,
                                                    i4FsMIPbDscp);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbDscpSVlanTable (INT4 *pi4FsMIPbPort, INT4 *pi4FsMIPbDscp)
{
    INT4                i4Dscp = 0;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbDscpSVlanTable ((INT4) u4IfIndex,
                                                 pi4FsMIPbPort,
                                                 i4Dscp, pi4FsMIPbDscp));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbDscp
                nextFsMIPbDscp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbDscpSVlanTable (INT4 i4FsMIPbPort, INT4 *pi4NextFsMIPbPort,
                                     INT4 i4FsMIPbDscp, INT4 *pi4NextFsMIPbDscp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbDscpSVlanTable ((INT4) u2LocalPort,
                                                       pi4NextFsMIPbPort,
                                                       i4FsMIPbDscp,
                                                       pi4NextFsMIPbDscp) ==
                    SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        i4FsMIPbDscp = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbDscpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp

                The Object 
                retValFsMIPbDscpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbDscpSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbDscp,
                       INT4 *pi4RetValFsMIPbDscpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbDscpSVlan ((INT4) u2LocalPort,
                                    i4FsMIPbDscp, pi4RetValFsMIPbDscpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbDscpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp

                The Object 
                retValFsMIPbDscpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbDscpRowStatus (INT4 i4FsMIPbPort, INT4 i4FsMIPbDscp,
                           INT4 *pi4RetValFsMIPbDscpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbDscpRowStatus ((INT4) u2LocalPort,
                                        i4FsMIPbDscp,
                                        pi4RetValFsMIPbDscpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbDscpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp

                The Object 
                setValFsMIPbDscpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbDscpSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbDscp,
                       INT4 i4SetValFsMIPbDscpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbDscpSVlan ((INT4) u2LocalPort,
                                    i4FsMIPbDscp, i4SetValFsMIPbDscpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbDscpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp

                The Object 
                setValFsMIPbDscpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbDscpRowStatus (INT4 i4FsMIPbPort, INT4 i4FsMIPbDscp,
                           INT4 i4SetValFsMIPbDscpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbDscpRowStatus ((INT4) u2LocalPort,
                                        i4FsMIPbDscp,
                                        i4SetValFsMIPbDscpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbDscpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp

                The Object 
                testValFsMIPbDscpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbDscpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                          INT4 i4FsMIPbDscp, INT4 i4TestValFsMIPbDscpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbDscpSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                       i4FsMIPbDscp, i4TestValFsMIPbDscpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbDscpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp

                The Object 
                testValFsMIPbDscpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbDscpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                              INT4 i4FsMIPbDscp,
                              INT4 i4TestValFsMIPbDscpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbDscpRowStatus (pu4ErrorCode,
                                           (INT4) u2LocalPort,
                                           i4FsMIPbDscp,
                                           i4TestValFsMIPbDscpRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDscp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbDscpSVlanTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbCVlanDscpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbCVlanDscpSVlanTable (INT4 i4FsMIPbPort,
                                                   INT4 i4CVlanDscpCVlan,
                                                   INT4 i4CVlanDscp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbCVlanDscpSVlanTable ((INT4) u2LocalPort,
                                                         i4CVlanDscpCVlan,
                                                         i4CVlanDscp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbCVlanDscpSVlanTable (INT4 *pi4FsMIPbPort,
                                           INT4 *pi4FsMIPbCVlanDscpCVlan,
                                           INT4 *pi4FsMIPbCVlanDscp)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CVlan = 0;
    INT4                i4Dscp = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbCVlanDscpSVlanTable ((INT4) u4IfIndex,
                                                      pi4FsMIPbPort,
                                                      u4CVlan,
                                                      pi4FsMIPbCVlanDscpCVlan,
                                                      i4Dscp,
                                                      pi4FsMIPbCVlanDscp));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbCVlanDscpCVlan
                nextFsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp
                nextFsMIPbCVlanDscp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbCVlanDscpSVlanTable (INT4 i4FsMIPbPort,
                                          INT4 *pi4NextFsMIPbPort,
                                          INT4 i4CVlan,
                                          INT4 *pi4NextCVlan,
                                          INT4 i4Dscp, INT4 *pi4NextDscp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbCVlanDscpSVlanTable ((INT4) u2LocalPort,
                                                            pi4NextFsMIPbPort,
                                                            i4CVlan,
                                                            pi4NextCVlan,
                                                            i4Dscp,
                                                            pi4NextDscp)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        i4CVlan = 0;
        i4Dscp = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanDscpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp

                The Object 
                retValFsMIPbCVlanDscpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanDscpSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanDscpCVlan,
                            INT4 i4FsMIPbCVlanDscp,
                            INT4 *pi4RetValFsMIPbCVlanDscpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanDscpSVlan ((INT4) u2LocalPort,
                                         i4FsMIPbCVlanDscpCVlan,
                                         i4FsMIPbCVlanDscp,
                                         pi4RetValFsMIPbCVlanDscpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanDscpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp

                The Object 
                retValFsMIPbCVlanDscpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanDscpRowStatus (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanDscpCVlan,
                                INT4 i4FsMIPbCVlanDscp,
                                INT4 *pi4RetValFsMIPbCVlanDscpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanDscpRowStatus ((INT4) u2LocalPort,
                                             i4FsMIPbCVlanDscpCVlan,
                                             i4FsMIPbCVlanDscp,
                                             pi4RetValFsMIPbCVlanDscpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanDscpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp

                The Object 
                setValFsMIPbCVlanDscpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanDscpSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanDscpCVlan,
                            INT4 i4FsMIPbCVlanDscp,
                            INT4 i4SetValFsMIPbCVlanDscpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanDscpSVlan ((INT4) u2LocalPort,
                                         i4FsMIPbCVlanDscpCVlan,
                                         i4FsMIPbCVlanDscp,
                                         i4SetValFsMIPbCVlanDscpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanDscpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp

                The Object 
                setValFsMIPbCVlanDscpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanDscpRowStatus (INT4 i4FsMIPbPort,
                                INT4 i4FsMIPbCVlanDscpCVlan,
                                INT4 i4FsMIPbCVlanDscp,
                                INT4 i4SetValFsMIPbCVlanDscpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanDscpRowStatus ((INT4) u2LocalPort,
                                             i4FsMIPbCVlanDscpCVlan,
                                             i4FsMIPbCVlanDscp,
                                             i4SetValFsMIPbCVlanDscpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanDscpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp

                The Object 
                testValFsMIPbCVlanDscpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanDscpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                               INT4 i4FsMIPbCVlanDscpCVlan,
                               INT4 i4FsMIPbCVlanDscp,
                               INT4 i4TestValFsMIPbCVlanDscpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbCVlanDscpSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                            i4FsMIPbCVlanDscpCVlan,
                                            i4FsMIPbCVlanDscp,
                                            i4TestValFsMIPbCVlanDscpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanDscpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp

                The Object 
                testValFsMIPbCVlanDscpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanDscpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                   INT4 i4FsMIPbCVlanDscpCVlan,
                                   INT4 i4FsMIPbCVlanDscp,
                                   INT4 i4TestValCVlanDscpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (nmhTestv2FsPbCVlanDscpRowStatus (pu4ErrorCode,
                                             (INT4) u2LocalPort,
                                             i4FsMIPbCVlanDscpCVlan,
                                             i4FsMIPbCVlanDscp,
                                             i4TestValCVlanDscpRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDscpCVlan
                FsMIPbCVlanDscp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbCVlanDscpSVlanTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbSrcIpAddrSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbSrcIpAddrSVlanTable (INT4 i4FsMIPbPort,
                                                   UINT4 u4FsMIPbSrcIpAddr)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbSrcIpAddrSVlanTable ((INT4) u2LocalPort,
                                                         u4FsMIPbSrcIpAddr);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbSrcIpAddrSVlanTable (INT4 *pi4FsMIPbPort,
                                           UINT4 *pu4FsMIPbSrcIpAddr)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4SrcIp = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbSrcIpAddrSVlanTable ((INT4) u4IfIndex,
                                                      pi4FsMIPbPort,
                                                      u4SrcIp,
                                                      pu4FsMIPbSrcIpAddr));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbSrcIpAddr
                nextFsMIPbSrcIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbSrcIpAddrSVlanTable (INT4 i4FsMIPbPort,
                                          INT4 *pi4NextFsMIPbPort,
                                          UINT4 u4SrcIpAddr,
                                          UINT4 *pu4NextSrcIpAddr)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbSrcIpAddrSVlanTable ((INT4) u2LocalPort,
                                                            pi4NextFsMIPbPort,
                                                            u4SrcIpAddr,
                                                            pu4NextSrcIpAddr)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        u4SrcIpAddr = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbSrcIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr

                The Object 
                retValFsMIPbSrcIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSrcIpSVlan (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcIpAddr,
                        INT4 *pi4RetValFsMIPbSrcIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbSrcIpSVlan ((INT4) u2LocalPort, u4FsMIPbSrcIpAddr,
                                     pi4RetValFsMIPbSrcIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbSrcIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr

                The Object 
                retValFsMIPbSrcIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSrcIpRowStatus (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcIpAddr,
                            INT4 *pi4RetValFsMIPbSrcIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbSrcIpRowStatus ((INT4) u2LocalPort, u4FsMIPbSrcIpAddr,
                                         pi4RetValFsMIPbSrcIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbSrcIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr

                The Object 
                setValFsMIPbSrcIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSrcIpSVlan (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcIpAddr,
                        INT4 i4SetValFsMIPbSrcIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbSrcIpSVlan ((INT4) u2LocalPort,
                                     u4FsMIPbSrcIpAddr,
                                     i4SetValFsMIPbSrcIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbSrcIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr

                The Object 
                setValFsMIPbSrcIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSrcIpRowStatus (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcIpAddr,
                            INT4 i4SetValFsMIPbSrcIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbSrcIpRowStatus ((INT4) u2LocalPort, u4FsMIPbSrcIpAddr,
                                  i4SetValFsMIPbSrcIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSrcIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr

                The Object 
                testValFsMIPbSrcIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSrcIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                           UINT4 u4FsMIPbSrcIpAddr,
                           INT4 i4TestValFsMIPbSrcIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSrcIpSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                        u4FsMIPbSrcIpAddr,
                                        i4TestValFsMIPbSrcIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSrcIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr

                The Object 
                testValFsMIPbSrcIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSrcIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                               UINT4 u4FsMIPbSrcIpAddr,
                               INT4 i4TestValFsMIPbSrcIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSrcIpRowStatus (pu4ErrorCode, (INT4) u2LocalPort,
                                            u4FsMIPbSrcIpAddr,
                                            i4TestValFsMIPbSrcIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbSrcIpAddrSVlanTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbDstIpAddrSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbDstIpAddrSVlanTable (INT4 i4FsMIPbPort,
                                                   UINT4 u4FsMIPbDstIpAddr)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbDstIpAddrSVlanTable ((INT4) u2LocalPort,
                                                         u4FsMIPbDstIpAddr);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbDstIpAddrSVlanTable (INT4 *pi4FsMIPbPort,
                                           UINT4 *pu4FsMIPbDstIpAddr)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4DstIp = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbDstIpAddrSVlanTable ((INT4) u4IfIndex,
                                                      pi4FsMIPbPort,
                                                      u4DstIp,
                                                      pu4FsMIPbDstIpAddr));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbDstIpAddr
                nextFsMIPbDstIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbDstIpAddrSVlanTable (INT4 i4FsMIPbPort,
                                          INT4 *pi4NextFsMIPbPort,
                                          UINT4 u4DstIpAddr,
                                          UINT4 *pu4NextDstIpAddr)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbDstIpAddrSVlanTable ((INT4) u2LocalPort,
                                                            pi4NextFsMIPbPort,
                                                            u4DstIpAddr,
                                                            pu4NextDstIpAddr)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        u4DstIpAddr = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr

                The Object 
                retValFsMIPbDstIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbDstIpSVlan (INT4 i4FsMIPbPort, UINT4 u4FsMIPbDstIpAddr,
                        INT4 *pi4RetValFsMIPbDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbDstIpSVlan ((INT4) u2LocalPort,
                                     u4FsMIPbDstIpAddr,
                                     pi4RetValFsMIPbDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr

                The Object 
                retValFsMIPbDstIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbDstIpRowStatus (INT4 i4FsMIPbPort, UINT4 u4FsMIPbDstIpAddr,
                            INT4 *pi4RetValFsMIPbDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbDstIpRowStatus ((INT4) u2LocalPort,
                                         u4FsMIPbDstIpAddr,
                                         pi4RetValFsMIPbDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr

                The Object 
                setValFsMIPbDstIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbDstIpSVlan (INT4 i4FsMIPbPort, UINT4 u4FsMIPbDstIpAddr,
                        INT4 i4SetValFsMIPbDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbDstIpSVlan ((INT4) u2LocalPort,
                                     u4FsMIPbDstIpAddr,
                                     i4SetValFsMIPbDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr

                The Object 
                setValFsMIPbDstIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbDstIpRowStatus (INT4 i4FsMIPbPort, UINT4 u4FsMIPbDstIpAddr,
                            INT4 i4SetValFsMIPbDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbDstIpRowStatus ((INT4) u2LocalPort, u4FsMIPbDstIpAddr,
                                  i4SetValFsMIPbDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr

                The Object 
                testValFsMIPbDstIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbDstIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                           UINT4 u4FsMIPbDstIpAddr,
                           INT4 i4TestValFsMIPbDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbDstIpSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                        u4FsMIPbDstIpAddr,
                                        i4TestValFsMIPbDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr

                The Object 
                testValFsMIPbDstIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbDstIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                               UINT4 u4FsMIPbDstIpAddr,
                               INT4 i4TestValFsMIPbDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbDstIpRowStatus (pu4ErrorCode, (INT4) u2LocalPort,
                                            u4FsMIPbDstIpAddr,
                                            i4TestValFsMIPbDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbDstIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbDstIpAddrSVlanTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbSrcDstIpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbSrcDstIpSVlanTable (INT4 i4FsMIPbPort,
                                                  UINT4 u4SrcDstSrcIpAddr,
                                                  UINT4 u4SrcDstDstIpAddr)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbSrcDstIpSVlanTable ((INT4) u2LocalPort,
                                                        u4SrcDstSrcIpAddr,
                                                        u4SrcDstDstIpAddr);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbSrcDstIpSVlanTable (INT4 *pi4FsMIPbPort,
                                          UINT4 *pu4FsMIPbSrcDstSrcIpAddr,
                                          UINT4 *pu4FsMIPbSrcDstDstIpAddr)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4SrcIp = 0;
    UINT4               u4DstIp = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbSrcDstIpSVlanTable ((INT4) u4IfIndex,
                                                     pi4FsMIPbPort,
                                                     u4SrcIp,
                                                     pu4FsMIPbSrcDstSrcIpAddr,
                                                     u4DstIp,
                                                     pu4FsMIPbSrcDstDstIpAddr));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                nextFsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr
                nextFsMIPbSrcDstDstIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbSrcDstIpSVlanTable (INT4 i4FsMIPbPort,
                                         INT4 *pi4NextFsMIPbPort,
                                         UINT4 u4SrcIpAddr,
                                         UINT4 *pu4NextSrcIpAddr,
                                         UINT4 u4DstIpAddr,
                                         UINT4 *pu4NextDstIpAddr)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbSrcDstIpSVlanTable ((INT4) u2LocalPort,
                                                           pi4NextFsMIPbPort,
                                                           u4SrcIpAddr,
                                                           pu4NextSrcIpAddr,
                                                           u4DstIpAddr,
                                                           pu4NextDstIpAddr)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        u4SrcIpAddr = 0;
        u4DstIpAddr = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbSrcDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr

                The Object 
                retValFsMIPbSrcDstIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSrcDstIpSVlan (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcDstSrcIpAddr,
                           UINT4 u4FsMIPbSrcDstDstIpAddr,
                           INT4 *pi4RetValFsMIPbSrcDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbSrcDstIpSVlan ((INT4) u2LocalPort,
                                        u4FsMIPbSrcDstSrcIpAddr,
                                        u4FsMIPbSrcDstDstIpAddr,
                                        pi4RetValFsMIPbSrcDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbSrcDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr

                The Object 
                retValFsMIPbSrcDstIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSrcDstIpRowStatus (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcDstSrcIpAddr,
                               UINT4 u4FsMIPbSrcDstDstIpAddr,
                               INT4 *pi4RetValFsMIPbSrcDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbSrcDstIpRowStatus ((INT4) u2LocalPort,
                                            u4FsMIPbSrcDstSrcIpAddr,
                                            u4FsMIPbSrcDstDstIpAddr,
                                            pi4RetValFsMIPbSrcDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbSrcDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr

                The Object 
                setValFsMIPbSrcDstIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSrcDstIpSVlan (INT4 i4FsMIPbPort, UINT4 u4FsMIPbSrcDstSrcIpAddr,
                           UINT4 u4FsMIPbSrcDstDstIpAddr,
                           INT4 i4SetValFsMIPbSrcDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbSrcDstIpSVlan ((INT4) u2LocalPort,
                                        u4FsMIPbSrcDstSrcIpAddr,
                                        u4FsMIPbSrcDstDstIpAddr,
                                        i4SetValFsMIPbSrcDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbSrcDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr

                The Object 
                setValFsMIPbSrcDstIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSrcDstIpRowStatus (INT4 i4FsMIPbPort,
                               UINT4 u4FsMIPbSrcDstSrcIpAddr,
                               UINT4 u4FsMIPbSrcDstDstIpAddr,
                               INT4 i4SetValFsMIPbSrcDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbSrcDstIpRowStatus ((INT4) u2LocalPort,
                                            u4FsMIPbSrcDstSrcIpAddr,
                                            u4FsMIPbSrcDstDstIpAddr,
                                            i4SetValFsMIPbSrcDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSrcDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr

                The Object 
                testValFsMIPbSrcDstIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSrcDstIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                              UINT4 u4FsMIPbSrcDstSrcIpAddr,
                              UINT4 u4FsMIPbSrcDstDstIpAddr,
                              INT4 i4TestValFsMIPbSrcDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSrcDstIpSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                           u4FsMIPbSrcDstSrcIpAddr,
                                           u4FsMIPbSrcDstDstIpAddr,
                                           i4TestValFsMIPbSrcDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSrcDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr

                The Object 
                testValFsMIPbSrcDstIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSrcDstIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                  UINT4 u4FsMIPbSrcDstSrcIpAddr,
                                  UINT4 u4FsMIPbSrcDstDstIpAddr,
                                  INT4 i4TestValFsMIPbSrcDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSrcDstIpRowStatus (pu4ErrorCode,
                                               (INT4) u2LocalPort,
                                               u4FsMIPbSrcDstSrcIpAddr,
                                               u4FsMIPbSrcDstDstIpAddr,
                                               i4TestValFsMIPbSrcDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbSrcDstSrcIpAddr
                FsMIPbSrcDstDstIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbSrcDstIpSVlanTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbCVlanDstIpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbCVlanDstIpSVlanTable (INT4 i4FsMIPbPort,
                                                    INT4 i4CVlanDstIpCVlan,
                                                    UINT4 u4CVlanDstIp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbCVlanDstIpSVlanTable ((INT4) u2LocalPort,
                                                          i4CVlanDstIpCVlan,
                                                          u4CVlanDstIp);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbCVlanDstIpSVlanTable (INT4 *pi4FsMIPbPort,
                                            INT4 *pi4FsMIPbCVlanDstIpCVlan,
                                            UINT4 *pu4FsMIPbCVlanDstIp)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CVlan = 0;
    UINT4               u4DstIp = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbCVlanDstIpSVlanTable ((INT4) u4IfIndex,
                                                       pi4FsMIPbPort,
                                                       u4CVlan,
                                                       pi4FsMIPbCVlanDstIpCVlan,
                                                       u4DstIp,
                                                       pu4FsMIPbCVlanDstIp));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbCVlanDstIpCVlan
                nextFsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp
                nextFsMIPbCVlanDstIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbCVlanDstIpSVlanTable (INT4 i4FsMIPbPort,
                                           INT4 *pi4NextFsMIPbPort,
                                           INT4 i4CVlan,
                                           INT4 *pi4NextCVlan,
                                           UINT4 u4DstIp, UINT4 *pu4NextDstIp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbCVlanDstIpSVlanTable ((INT4) u2LocalPort,
                                                             pi4NextFsMIPbPort,
                                                             i4CVlan,
                                                             pi4NextCVlan,
                                                             u4DstIp,
                                                             pu4NextDstIp)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        i4CVlan = 0;
        u4DstIp = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp

                The Object 
                retValFsMIPbCVlanDstIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanDstIpSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanDstIpCVlan,
                             UINT4 u4FsMIPbCVlanDstIp,
                             INT4 *pi4RetValFsMIPbCVlanDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanDstIpSVlan ((INT4) u2LocalPort,
                                          i4FsMIPbCVlanDstIpCVlan,
                                          u4FsMIPbCVlanDstIp,
                                          pi4RetValFsMIPbCVlanDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbCVlanDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp

                The Object 
                retValFsMIPbCVlanDstIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbCVlanDstIpRowStatus (INT4 i4FsMIPbPort,
                                 INT4 i4FsMIPbCVlanDstIpCVlan,
                                 UINT4 u4FsMIPbCVlanDstIp,
                                 INT4 *pi4RetValFsMIPbCVlanDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbCVlanDstIpRowStatus ((INT4) u2LocalPort,
                                              i4FsMIPbCVlanDstIpCVlan,
                                              u4FsMIPbCVlanDstIp,
                                              pi4RetValFsMIPbCVlanDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp

                The Object 
                setValFsMIPbCVlanDstIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanDstIpSVlan (INT4 i4FsMIPbPort, INT4 i4FsMIPbCVlanDstIpCVlan,
                             UINT4 u4FsMIPbCVlanDstIp,
                             INT4 i4SetValFsMIPbCVlanDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanDstIpSVlan ((INT4) u2LocalPort,
                                          i4FsMIPbCVlanDstIpCVlan,
                                          u4FsMIPbCVlanDstIp,
                                          i4SetValFsMIPbCVlanDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbCVlanDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp

                The Object 
                setValFsMIPbCVlanDstIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbCVlanDstIpRowStatus (INT4 i4FsMIPbPort,
                                 INT4 i4FsMIPbCVlanDstIpCVlan,
                                 UINT4 u4FsMIPbCVlanDstIp,
                                 INT4 i4SetValFsMIPbCVlanDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbCVlanDstIpRowStatus ((INT4) u2LocalPort,
                                              i4FsMIPbCVlanDstIpCVlan,
                                              u4FsMIPbCVlanDstIp,
                                              i4SetValFsMIPbCVlanDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanDstIpSVlan
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp

                The Object 
                testValFsMIPbCVlanDstIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanDstIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                INT4 i4FsMIPbCVlanDstIpCVlan,
                                UINT4 u4FsMIPbCVlanDstIp,
                                INT4 i4TestValFsMIPbCVlanDstIpSVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbCVlanDstIpSVlan (pu4ErrorCode, (INT4) u2LocalPort,
                                             i4FsMIPbCVlanDstIpCVlan,
                                             u4FsMIPbCVlanDstIp,
                                             i4TestValFsMIPbCVlanDstIpSVlan);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbCVlanDstIpRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp

                The Object 
                testValFsMIPbCVlanDstIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbCVlanDstIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                    INT4 i4FsMIPbCVlanDstIpCVlan,
                                    UINT4 u4FsMIPbCVlanDstIp,
                                    INT4 i4TestValCVlanDstIpRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbCVlanDstIpRowStatus (pu4ErrorCode, (INT4) u2LocalPort,
                                          i4FsMIPbCVlanDstIpCVlan,
                                          u4FsMIPbCVlanDstIp,
                                          i4TestValCVlanDstIpRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbCVlanDstIpCVlan
                FsMIPbCVlanDstIp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbCVlanDstIpSVlanTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbPortBasedCVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbPortBasedCVlanTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIPbPortBasedCVlanTable (INT4 i4FsMIPbPort)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbPortBasedCVlanTable ((INT4) u2LocalPort);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbPortBasedCVlanTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbPortBasedCVlanTable (INT4 *pi4FsMIPbPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIPbPortBasedCVlanTable (0, pi4FsMIPbPort);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbPortBasedCVlanTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbPortBasedCVlanTable (INT4 i4FsMIPbPort,
                                          INT4 *pi4NextFsMIPbPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    while (VlanGetNextPortInSystem (u4CurrIfIndex, &u4IfIndex) == VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE &&
                    VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_FALSE)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);
                    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;

                    if (pVlanPbPortEntry == NULL)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }

                    if ((pVlanPbPortEntry->u1PbPortType !=
                         VLAN_CNP_TAGGED_PORT) &&
                        (pVlanPbPortEntry->u1PbPortType !=
                         VLAN_CNP_PORTBASED_PORT) &&
                        (pVlanPbPortEntry->u1PbPortType !=
                         VLAN_PROP_PROVIDER_NETWORK_PORT) &&
                        (pVlanPbPortEntry->u1PbPortType !=
                         VLAN_PROVIDER_NETWORK_PORT))
                    {
                        *pi4NextFsMIPbPort = (INT4) u4IfIndex;

                        VlanReleaseContext ();
                        return SNMP_SUCCESS;

                    }
                }

                VlanReleaseContext ();
            }
        }

        u4CurrIfIndex = u4IfIndex;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlan
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortCVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortCVlan (INT4 i4FsMIPbPort, INT4 *pi4RetValFsMIPbPortCVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortCVlan ((INT4) u2LocalPort, pi4RetValFsMIPbPortCVlan);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlanClassifyStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbPortCVlanClassifyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPortCVlanClassifyStatus (INT4 i4FsMIPbPort,
                                     INT4 *pi4RetValPortCVlanClassifyStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortCVlanClassifyStatus ((INT4) u2LocalPort,
                                           pi4RetValPortCVlanClassifyStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortEgressUntaggedStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                retValFsMIPbPortEgressUntaggedStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIPbPortEgressUntaggedStatus(INT4 i4FsMIPbPort , INT4 *pi4RetValFsMIPbPortEgressUntaggedStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPbPortEgressUntaggedStatus ((INT4) u2LocalPort,
                                           pi4RetValFsMIPbPortEgressUntaggedStatus);

    VlanReleaseContext ();

    return i1RetVal;
}


/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbPortCVlan
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortCVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortCVlan (INT4 i4FsMIPbPort, INT4 i4SetValFsMIPbPortCVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbPortCVlan ((INT4) u2LocalPort,
                                    i4SetValFsMIPbPortCVlan);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortCVlanClassifyStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbPortCVlanClassifyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPortCVlanClassifyStatus (INT4 i4FsMIPbPort,
                                     INT4 i4SetValFsMIPbPortCVlanClassifyStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortCVlanClassifyStatus ((INT4) u2LocalPort,
                                           i4SetValFsMIPbPortCVlanClassifyStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortEgressUntaggedStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                setValFsMIPbPortEgressUntaggedStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIPbPortEgressUntaggedStatus(INT4 i4FsMIPbPort , INT4 i4SetValFsMIPbPortEgressUntaggedStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPbPortEgressUntaggedStatus ((INT4) u2LocalPort,
                                           i4SetValFsMIPbPortEgressUntaggedStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortCVlan
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortCVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortCVlan (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                          INT4 i4TestValFsMIPbPortCVlan)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortCVlan (pu4ErrorCode,
                                (INT4) u2LocalPort, i4TestValFsMIPbPortCVlan);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortCVlanClassifyStatus
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbPortCVlanClassifyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPortCVlanClassifyStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                        INT4 i4TestValPortCVlanClassifyStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortCVlanClassifyStatus (pu4ErrorCode,
                                              (INT4) u2LocalPort,
                                              i4TestValPortCVlanClassifyStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortEgressUntaggedStatus
 Input       :  The Indices
                FsMIPbPort

                The Object
                testValFsMIPbPortEgressUntaggedStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIPbPortEgressUntaggedStatus(UINT4 *pu4ErrorCode , INT4 i4FsMIPbPort , 
                                             INT4 i4TestValFsMIPbPortEgressUntaggedStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbPortEgressUntaggedStatus (pu4ErrorCode,
                                              (INT4) u2LocalPort,
                                              i4TestValFsMIPbPortEgressUntaggedStatus);

    VlanReleaseContext ();

    return i1RetVal;

}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbPortBasedCVlanTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbPortBasedCVlanTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbEtherTypeSwapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbEtherTypeSwapTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbEtherTypeSwapTable (INT4 i4FsMIPbPort,
                                                  INT4 i4LocalEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPbEtherTypeSwapTable ((INT4) u2LocalPort,
                                                        i4LocalEtherType);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbEtherTypeSwapTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMIPbEtherTypeSwapTable (INT4 *pi4FsMIPbPort,
                                          INT4 *pi4FsMIPbLocalEtherType)
{
    INT4                i4EtherType = 0;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsMIPbEtherTypeSwapTable ((INT4) u4IfIndex,
                                                     pi4FsMIPbPort,
                                                     i4EtherType,
                                                     pi4FsMIPbLocalEtherType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbEtherTypeSwapTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
                FsMIPbLocalEtherType
                nextFsMIPbLocalEtherType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbEtherTypeSwapTable (INT4 i4FsMIPbPort,
                                         INT4 *pi4NextFsMIPbPort,
                                         INT4 i4LocalEtherType,
                                         INT4 *pi4NextEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4FsMIPbPort < 0)
    {
        i4FsMIPbPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPort;

    /* GetNext indices based on the incoming physical port, C-VLAN/SrcMac/
     * DstMac/Dscp/SrcIp/DstIp*/

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexFsPbEtherTypeSwapTable ((INT4) u2LocalPort,
                                                           pi4NextFsMIPbPort,
                                                           i4LocalEtherType,
                                                           pi4NextEtherType)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        u4IfIndex = u4CurrIfIndex;
        /* If the first iteration has failed then the second indices
         * are not relevant , hence making them zero*/
        i4LocalEtherType = 0;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbRelayEtherType
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType

                The Object 
                retValFsMIPbRelayEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRelayEtherType (INT4 i4FsMIPbPort, INT4 i4FsMIPbLocalEtherType,
                            INT4 *pi4RetValFsMIPbRelayEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRelayEtherType ((INT4) u2LocalPort,
                                         i4FsMIPbLocalEtherType,
                                         pi4RetValFsMIPbRelayEtherType);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbEtherTypeSwapRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType

                The Object 
                retValFsMIPbEtherTypeSwapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbEtherTypeSwapRowStatus (INT4 i4FsMIPbPort,
                                    INT4 i4FsMIPbLocalEtherType,
                                    INT4 *pi4RetValEtherTypeSwapRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbEtherTypeSwapRowStatus ((INT4) u2LocalPort,
                                                 i4FsMIPbLocalEtherType,
                                                 pi4RetValEtherTypeSwapRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbRelayEtherType
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType

                The Object 
                setValFsMIPbRelayEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbRelayEtherType (INT4 i4FsMIPbPort, INT4 i4FsMIPbLocalEtherType,
                            INT4 i4SetValFsMIPbRelayEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbRelayEtherType ((INT4) u2LocalPort,
                                         i4FsMIPbLocalEtherType,
                                         i4SetValFsMIPbRelayEtherType);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbEtherTypeSwapRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType

                The Object 
                setValFsMIPbEtherTypeSwapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbEtherTypeSwapRowStatus (INT4 i4FsMIPbPort,
                                    INT4 i4FsMIPbLocalEtherType,
                                    INT4 i4SetValEtherTypeSwapRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT2) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbEtherTypeSwapRowStatus ((INT4) u2LocalPort,
                                                 i4FsMIPbLocalEtherType,
                                                 i4SetValEtherTypeSwapRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbRelayEtherType
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType

                The Object 
                testValFsMIPbRelayEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbRelayEtherType (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                               INT4 i4FsMIPbLocalEtherType,
                               INT4 i4TestValFsMIPbRelayEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbRelayEtherType (pu4ErrorCode, (INT4) u2LocalPort,
                                            i4FsMIPbLocalEtherType,
                                            i4TestValFsMIPbRelayEtherType);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbEtherTypeSwapRowStatus
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType

                The Object 
                testValFsMIPbEtherTypeSwapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbEtherTypeSwapRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                       INT4 i4FsMIPbLocalEtherType,
                                       INT4 i4EtherTypeSwapRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIPbPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbEtherTypeSwapRowStatus (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4FsMIPbLocalEtherType,
                                                    i4EtherTypeSwapRowStatus);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbEtherTypeSwapTable
 Input       :  The Indices
                FsMIPbPort
                FsMIPbLocalEtherType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbEtherTypeSwapTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbSVlanConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbSVlanConfigTable
 Input       :  The Indices
                FsMIPbContextId
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbSVlanConfigTable (INT4 i4FsMIPbContextId,
                                                UINT4 u4Dot1qVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VLAN_IS_CONTEXT_VALID (i4FsMIPbContextId) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsPbSVlanConfigTable (u4Dot1qVlanIndex);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbSVlanConfigTable
 Input       :  The Indices
                FsMIPbContextId
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbSVlanConfigTable (INT4 *pi4FsMIPbContextId,
                                        UINT4 *pu4Dot1qVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIPbSVlanConfigTable (0, pi4FsMIPbContextId,
                                                      0, pu4Dot1qVlanIndex);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbSVlanConfigTable
 Input       :  The Indices
                FsMIPbContextId
                nextFsMIPbContextId
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbSVlanConfigTable (INT4 i4FsMIPbContextId,
                                       INT4 *pi4NextFsMIPbContextId,
                                       UINT4 u4Dot1qVlanIndex,
                                       UINT4 *pu4NextDot1qVlanIndex)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VLAN_IS_CONTEXT_VALID (i4FsMIPbContextId) == VLAN_FALSE)
    {
        if (i4FsMIPbContextId < 0)
        {
            i4FsMIPbContextId = 0;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    for (u4ContextId = (UINT4) i4FsMIPbContextId; u4ContextId <=
         VLAN_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (nmhGetNextIndexFsPbSVlanConfigTable (u4Dot1qVlanIndex,
                                                     pu4NextDot1qVlanIndex)
                == SNMP_SUCCESS)
            {
                *pi4NextFsMIPbContextId = (INT4) u4ContextId;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }

            VlanReleaseContext ();
        }
        u4Dot1qVlanIndex = 0;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbSVlanConfigServiceType
 Input       :  The Indices
                FsMIPbContextId
                Dot1qVlanIndex

                The Object 
                retValFsMIPbSVlanConfigServiceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbSVlanConfigServiceType (INT4 i4FsMIPbContextId,
                                    UINT4 u4Dot1qVlanIndex,
                                    INT4 *pi4RetValSVlanConfigServiceType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbSVlanConfigServiceType
        (u4Dot1qVlanIndex, pi4RetValSVlanConfigServiceType);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbSVlanConfigServiceType
 Input       :  The Indices
                FsMIPbContextId
                Dot1qVlanIndex

                The Object 
                setValFsMIPbSVlanConfigServiceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbSVlanConfigServiceType (INT4 i4FsMIPbContextId,
                                    UINT4 u4Dot1qVlanIndex,
                                    INT4 i4SetSVlanConfigServiceType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbSVlanConfigServiceType
        (u4Dot1qVlanIndex, i4SetSVlanConfigServiceType);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbSVlanConfigServiceType
 Input       :  The Indices
                FsMIPbContextId
                Dot1qVlanIndex

                The Object 
                testValFsMIPbSVlanConfigServiceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbSVlanConfigServiceType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIPbContextId,
                                       UINT4 u4Dot1qVlanIndex,
                                       INT4 i4TestSVlanConfigServiceType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VLAN_IS_CONTEXT_VALID (i4FsMIPbContextId) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIPbContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbSVlanConfigServiceType (pu4ErrorCode,
                                                    u4Dot1qVlanIndex,
                                                    i4TestSVlanConfigServiceType);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbSVlanConfigTable
 Input       :  The Indices
                FsMIPbContextId
                Dot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbSVlanConfigTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbTunnelProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbTunnelProtocolTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbTunnelProtocolTable (INT4 i4FsMIPbPort)
{
    UNUSED_PARAM (i4FsMIPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbTunnelProtocolTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbTunnelProtocolTable (INT4 *pi4FsMIPbPort)
{
    UNUSED_PARAM (pi4FsMIPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbTunnelProtocolTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbTunnelProtocolTable (INT4 i4FsMIPbPort,
                                          INT4 *pi4NextFsMIPbPort)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4NextFsMIPbPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolDot1x
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolDot1x
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolDot1x (INT4 i4FsMIPbPort,
                                 INT4 *pi4RetValTunnelProtocolDot1x)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4RetValTunnelProtocolDot1x);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolLacp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolLacp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolLacp (INT4 i4FsMIPbPort,
                                INT4 *pi4RetValTunnelProtocolLacp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4RetValTunnelProtocolLacp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolStp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolStp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolStp (INT4 i4FsMIPbPort,
                               INT4 *pi4RetValTunnelProtocolStp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4RetValTunnelProtocolStp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolGvrp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolGvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolGvrp (INT4 i4FsMIPbPort,
                                INT4 *pi4RetValTunnelProtocolGvrp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4RetValTunnelProtocolGvrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolGmrp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolGmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolGmrp (INT4 i4FsMIPbPort,
                                INT4 *pi4RetValTunnelProtocolGmrp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4RetValTunnelProtocolGmrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolIgmp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolIgmp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolIgmp (INT4 i4FsMIPbPort,
                                INT4 *pi4RetValTunnelProtocolIgmp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4RetValTunnelProtocolIgmp);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelProtocolDot1x
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbTunnelProtocolDot1x
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelProtocolDot1x (INT4 i4FsMIPbPort,
                                 INT4 i4SetTunnelProtocolDot1x)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4SetTunnelProtocolDot1x);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelProtocolLacp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbTunnelProtocolLacp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelProtocolLacp (INT4 i4FsMIPbPort, INT4 i4SetTunnelProtocolLacp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4SetTunnelProtocolLacp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelProtocolStp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbTunnelProtocolStp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelProtocolStp (INT4 i4FsMIPbPort, INT4 i4SetTunnelProtocolStp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4SetTunnelProtocolStp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelProtocolGvrp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbTunnelProtocolGvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelProtocolGvrp (INT4 i4FsMIPbPort, INT4 i4SetTunnelProtocolGvrp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4SetTunnelProtocolGvrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelProtocolGmrp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbTunnelProtocolGmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelProtocolGmrp (INT4 i4FsMIPbPort, INT4 i4SetTunnelProtocolGmrp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4SetTunnelProtocolGmrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbTunnelProtocolIgmp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                setValFsMIPbTunnelProtocolIgmp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbTunnelProtocolIgmp (INT4 i4FsMIPbPort, INT4 i4SetTunnelProtocolIgmp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4SetTunnelProtocolIgmp);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelProtocolDot1x
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbTunnelProtocolDot1x
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelProtocolDot1x (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                    INT4 i4TunnelProtocolDot1x)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4TunnelProtocolDot1x);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelProtocolLacp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbTunnelProtocolLacp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelProtocolLacp (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                   INT4 i4TunnelProtocolLacp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4TunnelProtocolLacp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelProtocolStp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbTunnelProtocolStp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelProtocolStp (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                  INT4 i4TunnelProtocolStp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4TunnelProtocolStp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelProtocolGvrp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbTunnelProtocolGvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelProtocolGvrp (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                   INT4 i4TunnelProtocolGvrp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4TunnelProtocolGvrp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelProtocolGmrp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbTunnelProtocolGmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelProtocolGmrp (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                   INT4 i4TunnelProtocolGmrp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4TunnelProtocolGmrp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbTunnelProtocolIgmp
 Input       :  The Indices
                FsMIPbPort

                The Object 
                testValFsMIPbTunnelProtocolIgmp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbTunnelProtocolIgmp (UINT4 *pu4ErrorCode, INT4 i4FsMIPbPort,
                                   INT4 i4TunnelProtocolIgmp)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (i4TunnelProtocolIgmp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbTunnelProtocolTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbTunnelProtocolStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbTunnelProtocolStatsTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbTunnelProtocolStatsTable (INT4 i4FsMIPbPort)
{
    UNUSED_PARAM (i4FsMIPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbTunnelProtocolStatsTable
 Input       :  The Indices
                FsMIPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbTunnelProtocolStatsTable (INT4 *pi4FsMIPbPort)
{
    UNUSED_PARAM (pi4FsMIPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbTunnelProtocolStatsTable
 Input       :  The Indices
                FsMIPbPort
                nextFsMIPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbTunnelProtocolStatsTable (INT4 i4FsMIPbPort,
                                               INT4 *pi4NextFsMIPbPort)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pi4NextFsMIPbPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolDot1xPktsRecvd
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolDot1xPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolDot1xPktsRecvd (INT4 i4FsMIPbPort,
                                          UINT4 *pu4ProtocolDot1xPktsRecvd)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolDot1xPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolDot1xPktsSent
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolDot1xPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolDot1xPktsSent (INT4 i4FsMIPbPort,
                                         UINT4 *pu4ProtocolDot1xPktsSent)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolDot1xPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolLacpPktsRecvd
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolLacpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolLacpPktsRecvd (INT4 i4FsMIPbPort,
                                         UINT4 *pu4ProtocolLacpPktsRecvd)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolLacpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolLacpPktsSent
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolLacpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolLacpPktsSent (INT4 i4FsMIPbPort,
                                        UINT4 *pu4ProtocolLacpPktsSent)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolLacpPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolStpPDUsRecvd
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolStpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolStpPDUsRecvd (INT4 i4FsMIPbPort,
                                        UINT4 *pu4ProtocolStpPDUsRecvd)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolStpPDUsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolStpPDUsSent
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolStpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolStpPDUsSent (INT4 i4FsMIPbPort,
                                       UINT4 *pu4ProtocolStpPDUsSent)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolStpPDUsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolGvrpPDUsRecvd
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolGvrpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolGvrpPDUsRecvd (INT4 i4FsMIPbPort,
                                         UINT4 *pu4ProtocolGvrpPDUsRecvd)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolGvrpPDUsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolGvrpPDUsSent
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolGvrpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolGvrpPDUsSent (INT4 i4FsMIPbPort,
                                        UINT4 *pu4ProtocolGvrpPDUsSent)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolGvrpPDUsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolGmrpPktsRecvd
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolGmrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolGmrpPktsRecvd (INT4 i4FsMIPbPort,
                                         UINT4 *pu4ProtocolGmrpPktsRecvd)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolGmrpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolGmrpPktsSent
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolGmrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolGmrpPktsSent (INT4 i4FsMIPbPort,
                                        UINT4 *pu4ProtocolGmrpPktsSent)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolGmrpPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolIgmpPktsRecvd
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolIgmpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolIgmpPktsRecvd (INT4 i4FsMIPbPort,
                                         UINT4 *pu4ProtocolIgmpPktsRecvd)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolIgmpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIPbTunnelProtocolIgmpPktsSent
 Input       :  The Indices
                FsMIPbPort

                The Object 
                retValFsMIPbTunnelProtocolIgmpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbTunnelProtocolIgmpPktsSent (INT4 i4FsMIPbPort,
                                        UINT4 *pu4ProtocolIgmpPktsSent)
{
    UNUSED_PARAM (i4FsMIPbPort);
    UNUSED_PARAM (pu4ProtocolIgmpPktsSent);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMIPbPepExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbPepExtTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbPepExtTable (INT4 i4Dot1adMIPortNum,
                                           INT4 i4Dot1adMICVidRegistrationSVid)
{
    return (nmhValidateIndexInstanceDot1adMIPepTable
            (i4Dot1adMIPortNum, i4Dot1adMICVidRegistrationSVid));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbPepExtTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbPepExtTable (INT4 *pi4Dot1adMIPortNum,
                                   INT4 *pi4Dot1adMICVidRegistrationSVid)
{
    return (nmhGetFirstIndexDot1adMIPepTable (pi4Dot1adMIPortNum,
                                              pi4Dot1adMICVidRegistrationSVid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbPepExtTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                nextDot1adMICVidRegistrationSVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbPepExtTable (INT4 i4Dot1adMIPortNum,
                                  INT4 *pi4NextDot1adMIPortNum,
                                  INT4 i4Dot1adMICVidRegistrationSVid,
                                  INT4 *pi4NextDot1adMICVidRegistrationSVid)
{
    return (nmhGetNextIndexDot1adMIPepTable
            (i4Dot1adMIPortNum, pi4NextDot1adMIPortNum,
             i4Dot1adMICVidRegistrationSVid,
             pi4NextDot1adMICVidRegistrationSVid));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbPepExtCosPreservation
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                retValFsMIPbPepExtCosPreservation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbPepExtCosPreservation (INT4 i4Dot1adMIPortNum,
                                   INT4 i4Dot1adMICVidRegistrationSVid,
                                   INT4 *pi4RetValFsMIPbPepExtCosPreservation)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsPbPepExtCosPreservation
        (u2LocalPort, i4Dot1adMICVidRegistrationSVid,
         pi4RetValFsMIPbPepExtCosPreservation);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbPepExtCosPreservation
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                setValFsMIPbPepExtCosPreservation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbPepExtCosPreservation (INT4 i4Dot1adMIPortNum,
                                   INT4 i4Dot1adMICVidRegistrationSVid,
                                   INT4 i4SetValFsMIPbPepExtCosPreservation)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsPbPepExtCosPreservation
        (u2LocalPort, i4Dot1adMICVidRegistrationSVid,
         i4SetValFsMIPbPepExtCosPreservation);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPepExtCosPreservation
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                testValFsMIPbPepExtCosPreservation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbPepExtCosPreservation (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1adMIPortNum,
                                      INT4 i4Dot1adMICVidRegistrationSVid,
                                      INT4 i4TestValFsMIPbPepExtCosPreservation)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPbPepExtCosPreservation
        (pu4ErrorCode, (INT4) u2LocalPort, i4Dot1adMICVidRegistrationSVid,
         i4TestValFsMIPbPepExtCosPreservation);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbPepExtTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbPepExtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}




/* LOW LEVEL Routines for Table : FsMIPbPortCVlanCounterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbPortCVlanCounterTable
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsMIPbPortCVlanCounterTable(INT4 i4FsMIPbPortCVlanContextId ,
                                                        INT4 i4FsMIPbPortCVlanPort ,
                                                        UINT4 u4FsMIPbPortCVlanIndex)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMIPbPortCVlanContextId);
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adCVidRegistrationTable (u2LocalPort,
                                                             u4FsMIPbPortCVlanIndex);

    VlanReleaseContext ();
    return  i1RetVal;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbPortCVlanCounterTable
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsMIPbPortCVlanCounterTable(INT4 *pi4FsMIPbPortCVlanContextId ,
                                                INT4 *pi4FsMIPbPortCVlanPort ,
                                                UINT4 *pu4FsMIPbPortCVlanIndex)
{

    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4CVidRegistrationCVid = 0;
   UNUSED_PARAM(pi4FsMIPbPortCVlanContextId);

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMICVidRegistrationTable (u4IfIndex,
                                                          pi4FsMIPbPortCVlanPort,
                                                          i4CVidRegistrationCVid,
                                                          (INT4 *)pu4FsMIPbPortCVlanIndex));
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbPortCVlanCounterTable
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                nextFsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                nextFsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex
                nextFsMIPbPortCVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsMIPbPortCVlanCounterTable(INT4 i4FsMIPbPortCVlanContextId ,
                                               INT4 *pi4NextFsMIPbPortCVlanContextId  ,
                                               INT4 i4FsMIPbPortCVlanPort ,
                                               INT4 *pi4NextFsMIPbPortCVlanPort  ,
                                               UINT4 u4FsMIPbPortCVlanIndex ,
                                               UINT4 *pu4NextFsMIPbPortCVlanIndex )
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

   UNUSED_PARAM(i4FsMIPbPortCVlanContextId);
   UNUSED_PARAM(pi4NextFsMIPbPortCVlanContextId);
    if (i4FsMIPbPortCVlanPort < 0)
    {
         i4FsMIPbPortCVlanPort = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4FsMIPbPortCVlanPort;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adCVidRegistrationTable
                    ((INT4) u2LocalPort, pi4NextFsMIPbPortCVlanPort, u4FsMIPbPortCVlanIndex,
                     (INT4 *)pu4NextFsMIPbPortCVlanIndex) == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextFsMIPbPortCVlanPort);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextFsMIPbPortCVlanPort = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }
           }

        }
        u4FsMIPbPortCVlanIndex = 0;
        u4IfIndex = u4CurrIfIndex;
    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlanCounterRxUcast
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                retValFsMIPbPortCVlanCounterRxUcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIPbPortCVlanCounterRxUcast(INT4 i4FsMIPbPortCVlanContextId ,
                                        INT4 i4FsMIPbPortCVlanPort ,
                                        UINT4 u4FsMIPbPortCVlanIndex ,
                                        UINT4 *pu4RetValFsMIPbPortCVlanCounterRxUcast)
{
    if (VlanHwGetCVlanStats (i4FsMIPbPortCVlanContextId,
                            i4FsMIPbPortCVlanPort,
                            u4FsMIPbPortCVlanIndex,
                            VLAN_STAT_UCAST_IN_FRAMES,
                            pu4RetValFsMIPbPortCVlanCounterRxUcast)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }


    return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlanCounterRxFrames
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                retValFsMIPbPortCVlanCounterRxFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIPbPortCVlanCounterRxFrames(INT4 i4FsMIPbPortCVlanContextId ,
                                         INT4 i4FsMIPbPortCVlanPort ,
                                         UINT4 u4FsMIPbPortCVlanIndex ,
                                         UINT4 *pu4RetValFsMIPbPortCVlanCounterRxFrames)
{
    if (VlanHwGetCVlanStats (i4FsMIPbPortCVlanContextId,
                            i4FsMIPbPortCVlanPort,
                            u4FsMIPbPortCVlanIndex,
                            VLAN_STAT_VLAN_IN_FRAMES,
                            pu4RetValFsMIPbPortCVlanCounterRxFrames)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlanCounterRxBytes
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                retValFsMIPbPortCVlanCounterRxBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIPbPortCVlanCounterRxBytes(INT4 i4FsMIPbPortCVlanContextId ,
                                        INT4 i4FsMIPbPortCVlanPort ,
                                        UINT4 u4FsMIPbPortCVlanIndex ,
                                        UINT4 *pu4RetValFsMIPbPortCVlanCounterRxBytes)
{
    if (VlanHwGetCVlanStats (i4FsMIPbPortCVlanContextId,
                            i4FsMIPbPortCVlanPort,
                            u4FsMIPbPortCVlanIndex,
                            VLAN_STAT_VLAN_IN_BYTES,
                            pu4RetValFsMIPbPortCVlanCounterRxBytes)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlanCounterStatus
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                retValFsMIPbPortCVlanCounterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIPbPortCVlanCounterStatus(INT4 i4FsMIPbPortCVlanContextId ,
                                       INT4 i4FsMIPbPortCVlanPort ,
                                       UINT4 u4FsMIPbPortCVlanIndex ,
                                       INT4 *pi4RetValFsMIPbPortCVlanCounterStatus)
{
	tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;
	UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
	UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    UNUSED_PARAM(i4FsMIPbPortCVlanContextId);
	if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort,
				&u4ContextId, &u2LocalPort)
			== VLAN_FAILURE)
	{
		return SNMP_FAILURE;
	}

	if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
	{
		return SNMP_FAILURE;
	}
	u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
	pCVlanSVlanEntry =
		VlanPbGetCVlanSVlanEntry (u4TblIndex,
				(UINT2) u2LocalPort,
				(tVlanId)u4FsMIPbPortCVlanIndex);

	if ((pCVlanSVlanEntry == NULL))
	{
	    VlanReleaseContext ();
		return SNMP_FAILURE;
	}
     
	*pi4RetValFsMIPbPortCVlanCounterStatus = pCVlanSVlanEntry->u1CvlanStatus;
	VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbPortCVlanClearCounter
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                retValFsMIPbPortCVlanClearCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIPbPortCVlanClearCounter(INT4 i4FsMIPbPortCVlanContextId ,
                                      INT4 i4FsMIPbPortCVlanPort ,
                                      UINT4 u4FsMIPbPortCVlanIndex ,
                                      INT4 *pi4RetValFsMIPbPortCVlanClearCounter)
{
	tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;
	UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
	UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    UNUSED_PARAM(i4FsMIPbPortCVlanContextId);
	if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort,
				&u4ContextId, &u2LocalPort)
			== VLAN_FAILURE)
	{
		return SNMP_FAILURE;
	}

	if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
	{
		return SNMP_FAILURE;
	}
	u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
	pCVlanSVlanEntry =
		VlanPbGetCVlanSVlanEntry (u4TblIndex,
				(UINT2) u2LocalPort,
				(tVlanId)u4FsMIPbPortCVlanIndex);

	if ((pCVlanSVlanEntry == NULL))
	{
	    VlanReleaseContext ();
		return SNMP_FAILURE;
	}
     
	*pi4RetValFsMIPbPortCVlanClearCounter = (INT4)pCVlanSVlanEntry->bCvlanStatFlush;
	VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */


/****************************************************************************
 Function    :  nmhSetFsMIPbPortCVlanCounterStatus
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                setValFsMIPbPortCVlanCounterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIPbPortCVlanCounterStatus(INT4 i4FsMIPbPortCVlanContextId ,
                                       INT4 i4FsMIPbPortCVlanPort ,
                                       UINT4 u4FsMIPbPortCVlanIndex ,
                                       INT4 i4SetValFsMIPbPortCVlanCounterStatus)
{
    tVlanPbLogicalPortEntry *pVlanLogicalPortEntry = NULL;
	tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
#ifdef NPAPI_WANTED
	tHwVlanCVlanStat     VlanStat;
#endif
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
	UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
	UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4SeqNum = 0;

    UNUSED_PARAM(i4FsMIPbPortCVlanContextId);

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

	if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort,
				&u4ContextId, &u2LocalPort)
			== VLAN_FAILURE)
	{
		return SNMP_FAILURE;
	}


	if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) u2LocalPort,
                                  (tVlanId)u4FsMIPbPortCVlanIndex);

    if ((pCVlanSVlanEntry == NULL))
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if (i4SetValFsMIPbPortCVlanCounterStatus == pCVlanSVlanEntry->u1CvlanStatus)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    
    pVlanLogicalPortEntry =
        VlanPbGetLogicalPortEntry (u2LocalPort,
                                   pCVlanSVlanEntry->SVlanId);

    if (pVlanLogicalPortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
	    VlanStat.CVlanId  =  u4FsMIPbPortCVlanIndex;
        VlanStat.u2Port   =  i4FsMIPbPortCVlanPort;    
		VlanStat.u1Status =  (INT4)i4SetValFsMIPbPortCVlanCounterStatus;
        if (pVlanLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
        {
            if (VlanFsMiVlanHwSetCVlanStat(i4FsMIPbPortCVlanContextId,VlanStat)!= SNMP_SUCCESS)
            {
                VlanReleaseContext ();
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbPortCVlanCounterStatus, u4SeqNum,
                                      FALSE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);

                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i",i4FsMIPbPortCVlanContextId,
                                  i4FsMIPbPortCVlanPort,u4FsMIPbPortCVlanIndex,
                                  i4SetValFsMIPbPortCVlanCounterStatus));
                return SNMP_FAILURE;
            }
        }
#endif

    pCVlanSVlanEntry->u1CvlanStatus  = (UINT1) i4SetValFsMIPbPortCVlanCounterStatus;  
    
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                         FsMIPbPortCVlanCounterStatus, u4SeqNum,
                         FALSE, VlanLock, VlanUnLock, 3,
                         SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i",i4FsMIPbPortCVlanContextId,
                      i4FsMIPbPortCVlanPort,u4FsMIPbPortCVlanIndex,
                      i4SetValFsMIPbPortCVlanCounterStatus));


	VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIPbPortCVlanClearCounter
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                setValFsMIPbPortCVlanClearCounter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIPbPortCVlanClearCounter(INT4 i4FsMIPbPortCVlanContextId ,
                                      INT4 i4FsMIPbPortCVlanPort ,
                                      UINT4 u4FsMIPbPortCVlanIndex ,
                                      INT4 i4SetValFsMIPbPortCVlanClearCounter)
{

    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    UNUSED_PARAM(i4FsMIPbPortCVlanContextId);
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                  (UINT2) u2LocalPort,
                                  (tVlanId)u4FsMIPbPortCVlanIndex);
 
    if ((pCVlanSVlanEntry == NULL))
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if( (i4SetValFsMIPbPortCVlanClearCounter  == VLAN_TRUE))
    {      
         pCVlanSVlanEntry->bCvlanStatFlush  = (BOOLEAN) i4SetValFsMIPbPortCVlanClearCounter;
        if (VlanHwClearCVlanStats (i4FsMIPbPortCVlanContextId,
                               i4FsMIPbPortCVlanPort,
                               u4FsMIPbPortCVlanIndex)
          != VLAN_SUCCESS)
        {
    		VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
#endif
    if( (i4SetValFsMIPbPortCVlanClearCounter  == VLAN_TRUE))
    {
    	pCVlanSVlanEntry->bCvlanStatFlush = VLAN_FALSE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortCVlanCounterStatus
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                testValFsMIPbPortCVlanCounterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIPbPortCVlanCounterStatus(UINT4 *pu4ErrorCode ,
                                          INT4 i4FsMIPbPortCVlanContextId ,
                                          INT4 i4FsMIPbPortCVlanPort ,
                                          UINT4 u4FsMIPbPortCVlanIndex ,
                                          INT4 i4TestValFsMIPbPortCVlanCounterStatus)
{

    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
   UNUSED_PARAM(i4FsMIPbPortCVlanContextId);


    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
   
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }


    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_PORT_VALID ((UINT2) i4FsMIPbPortCVlanPort) != VLAN_TRUE) ||
        (VLAN_IS_CUSTOMER_VLAN_ID_VALID(u4FsMIPbPortCVlanIndex) == VLAN_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIPbPortCVlanCounterStatus != VLAN_ENABLED) &&
        (i4TestValFsMIPbPortCVlanCounterStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }
        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPortId);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
    pCVlanSVlanEntry =
        VlanPbGetCVlanSVlanEntry (u4TblIndex,
                i4FsMIPbPortCVlanPort,
                (tVlanId)u4FsMIPbPortCVlanIndex);
    if ((pCVlanSVlanEntry == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_CVID_REG_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

   VlanReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPbPortCVlanClearCounter
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex

                The Object
                testValFsMIPbPortCVlanClearCounter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIPbPortCVlanClearCounter(UINT4 *pu4ErrorCode ,
                                         INT4 i4FsMIPbPortCVlanContextId ,
                                         INT4 i4FsMIPbPortCVlanPort ,
                                         UINT4 u4FsMIPbPortCVlanIndex ,
                                         INT4 i4TestValFsMIPbPortCVlanClearCounter)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIPbPortCVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsMIPbPortCVlanContextId)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsMIPbPortCVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_PORT_VALID ((UINT2) i4FsMIPbPortCVlanPort) != VLAN_TRUE) ||
       ((VLAN_IS_CUSTOMER_VLAN_ID_VALID(u4FsMIPbPortCVlanIndex) ==VLAN_FALSE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIPbPortCVlanClearCounter != TRUE) &&
        (i4TestValFsMIPbPortCVlanClearCounter != FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
         VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2LocalPortId);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    pVlanPbPortEntry = pVlanPortEntry->pVlanPbPortEntry;
    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FsMIPbPortCVlanCounterTable
 Input       :  The Indices
                FsMIPbPortCVlanContextId
                FsMIPbPortCVlanPort
                FsMIPbPortCVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIPbPortCVlanCounterTable(UINT4 *pu4ErrorCode,
                                         tSnmpIndexList *pSnmpIndexList,
                                        tSNMP_VAR_BIND *pSnmpVarBind)
{
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pSnmpIndexList);
   UNUSED_PARAM(pSnmpVarBind);
   return SNMP_SUCCESS;
}


