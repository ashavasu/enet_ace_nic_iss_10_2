
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevuap.c,v 1.9 2016/07/22 06:46:32 siva Exp $
 *
 * Description: This file contains EVB UAP sub module routines.
 *
 *******************************************************************/
#ifndef __VLNEVUAP_C__
#define __VLNEVUAP_C__

#include "vlaninc.h"

PRIVATE tEvbUapIfEntry    gUapIfEntry;
/*****************************************************************************/
/* Function Name      : VlanEvbUapIfTblCreate                                */
/*                                                                           */
/* Description        : This routine creates gEvbGlobalInfo.EvbUapIfTree.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapIfTblCreate (VOID)
{
    /* This function is called from VlanEvbStart */
    gEvbGlobalInfo.EvbUapIfTree = RBTreeCreateEmbedded (0, VlanEvbUapIfTblCmp);

    if (gEvbGlobalInfo.EvbUapIfTree == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbUAPConfigTblCreate:"
             "RB Tree creation for gEvbGlobalInfo.EvbUapIfTree has failed\r\n");
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfTblDelete                                */
/*                                                                           */
/* Description        : This routine deletes gEvbGlobalInfo.EvbUapIfTree.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanEvbUapIfTblDelete (VOID)
{
    /* This function is called from VlanEvbShutdown */

    /* Delete all the UAP entries before deleting the RBTree.*/
    VlanEvbUapIfDelAllEntries ();

    if (gEvbGlobalInfo.EvbUapIfTree != NULL)
    {
        RBTreeDelete (gEvbGlobalInfo.EvbUapIfTree);
        gEvbGlobalInfo.EvbUapIfTree = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfTblCmp                                   */
/*                                                                           */
/* Description        : This routine is used in gEvbGlobalInfo.EvbUapIfTree  */
/*                      for comparing the key used in RBTree functionality.  */
/*                                                                           */
/* Input(s)           : pNode        - Node from RBTree                      */
/*                      pNodeIn      - Input node                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
VlanEvbUapIfTblCmp (tRBElem *pNode,
                    tRBElem *pNodeIn)
{
    tEvbUapIfEntry     *pUapNode = (tEvbUapIfEntry *)pNode;
    tEvbUapIfEntry     *pUapNodeIn = (tEvbUapIfEntry *)pNodeIn; 

    /* Key for EvbUapIfTree is u4UapIfIndex */
    if (pUapNode->u4UapIfIndex > pUapNodeIn->u4UapIfIndex)
    {
        return (VLAN_RBTREE_KEY_GREATER);
    }
    else if (pUapNode->u4UapIfIndex < pUapNodeIn->u4UapIfIndex)
    {
        return (VLAN_RBTREE_KEY_LESSER);
    }
    return (VLAN_RBTREE_KEY_EQUAL); 
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfWrAddEntry                               */
/*                                                                           */
/* Description        : This routine adds a UAP interface entry in           */
/*                      gEvbGlobalInfo.EvbUapIfTree if EVB is enabled.       */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                                                                           */
/* Output(s)          : ppEvbUapIfEntry - pointer to pEvbUapIfEntry          */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapIfWrAddEntry (UINT4 u4UapIfIndex)
{
    tEvbUapIfEntry *pUapIfEntry    = NULL;

    VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
            "VlanEvbUapIfWrAddEntry: "
            "UAP entry creation hit for UAP IfIndex %d\r\n",
            u4UapIfIndex);

    return  (VlanEvbUapIfAddEntry (u4UapIfIndex, &pUapIfEntry));
}
/*****************************************************************************/
/* Function Name      : VlanEvbUapIfAddEntry                                 */
/*                                                                           */
/* Description        : This routine adds a UAP interface entry in           */
/*                      gEvbGlobalInfo.EvbUapIfTree.                         */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                                                                           */
/* Output(s)          : ppEvbUapIfEntry - pointer to pEvbUapIfEntry          */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapIfAddEntry (UINT4 u4UapIfIndex, tEvbUapIfEntry **ppUapIfEntry)
{
    /* This function is called from VlanHandleCreatePort. */
    tEvbSChIfEntry  *pSChIfEntry    =   NULL;
    UINT4           u4ContextId     =   0;
    UINT2           u2LocalPort     =   0;

    (*ppUapIfEntry) = NULL;

    if (((*ppUapIfEntry) = VlanEvbUapIfGetEntry(u4UapIfIndex)) != NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "VlanEvbUapIfAddEntry: "
                       "UAP entry is already present for UAP IfIndex %d\r\n",
                        u4UapIfIndex);
        return VLAN_SUCCESS;
    }

    (*ppUapIfEntry) = (tEvbUapIfEntry *) (VOID *) VLAN_EVB_GET_BUF
                      (VLAN_EVB_UAP_ENTRY, sizeof (tEvbUapIfEntry));

    if((*ppUapIfEntry) == NULL) 
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanEvbUapIfAddEntry: Memory allocation for "
                        "UAP Index %d to create tEvbUapIfEntry failed\r\n",
                        u4UapIfIndex);
        return VLAN_FAILURE; 
    }

    MEMSET ((*ppUapIfEntry), 0, sizeof (tEvbUapIfEntry));

    /* Setting RBTree Index - UAP If Index */
    (*ppUapIfEntry)->u4UapIfIndex = u4UapIfIndex;

    if (VlanVcmGetContextInfoFromIfIndex (u4UapIfIndex,
                            &u4ContextId, &u2LocalPort) != VCM_FAILURE)
    {
    /* The context id and local port for UAP is retrieved from VCM module.*/
            (*ppUapIfEntry)->u4UapIfCompId = u4ContextId; 
            (*ppUapIfEntry)->u4UapIfPort = ((UINT4) u2LocalPort);
    }
    /* Setting default values for other variables in UAP entry*/
    (*ppUapIfEntry)->
        i4UapIfStorageType = VLAN_EVB_STORAGE_TYPE_NON_VOLATILE;
    (*ppUapIfEntry)->
        i4UapIfSchCdcpAdminEnable = VLAN_EVB_UAP_CDCP_DISABLE;
    (*ppUapIfEntry)->
        i4UapIfSchAdminCdcpChanCap = VLAN_EVB_MAX_SBP_PER_UAP;
    (*ppUapIfEntry)->
        u4UapIfSchAdminCdcpSvidPoolLow = VLAN_EVB_DEF_CDCP_SVID_POOL_LOW;
    (*ppUapIfEntry)->
        u4UapIfSchAdminCdcpSvidPoolHigh = VLAN_EVB_DEF_CDCP_SVID_POOL_HIGH;
    (*ppUapIfEntry)->
        i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
    (*ppUapIfEntry)->
        i4UapIfRowStatus = NOT_IN_SERVICE;
    (*ppUapIfEntry)->
        i4UapIfSchCdcpRemoteEnabled = VLAN_EVB_UAP_CDCP_DISABLE;
    (*ppUapIfEntry)->
        i4UapIfSChMode = VLAN_EVB_UAP_SCH_MODE_DYNAMIC;
    (*ppUapIfEntry)->
        i4EvbSysEvbLldpTxEnable = VLAN_EVB_CDCP_LLDP_DISABLE;
    (*ppUapIfEntry)->
        i4UapIfSchCdcpRemoteRole = VLAN_EVB_ROLE_STATION; 
    /* Adding the UAP If Entry in its RBTree */ 
    if (RBTreeAdd (gEvbGlobalInfo.EvbUapIfTree, (tRBElem *) (VOID *)
                    (*ppUapIfEntry)) == RB_FAILURE)
    {
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_UAP_ENTRY, (VOID *)(*ppUapIfEntry));
        (*ppUapIfEntry) = NULL;
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanEvbUapIfAddEntry: EvbUapIfTree additon for "
                        "UAP Index %d failed\r\n", u4UapIfIndex);
        return VLAN_FAILURE;
    }

    (*ppUapIfEntry)->pEvbLldpSChannelIfNotify =
                 VlanPortLldpApiUpdateSvidOnUap;

    /* Creating default SCID-SVID pair */
    if (VlanEvbSChIfAddEntry (u4UapIfIndex, VLAN_EVB_DEF_SVID,
                VLAN_EVB_UAP_SCH_MODE_STATIC, VLAN_EVB_DEF_SCID,
                &pSChIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbUapIfAddEntry: "
                "Default SCID-SVID pair creation is failed for UAP "
                "port %d\r\n", u4UapIfIndex);
        /* NOTE: do we need to delete UAP Entry ? */
        return VLAN_FAILURE;

    }
    OSIX_BITLIST_SET_BIT ((*ppUapIfEntry)->UapSvidList, VLAN_EVB_DEF_SVID,
                          sizeof ((*ppUapIfEntry)->UapSvidList)); 
    (*ppUapIfEntry)->i4UapIfRowStatus = ACTIVE;
    return VLAN_SUCCESS;
}                          

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfGetEntry                                 */
/*                                                                           */
/* Description        : This routine gets a tEvbUapIfEntry for the           */
/*                      given u4UapIfIndex.                                  */ 
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Interface Index.                  */
/*                                                                           */
/* Output(s)          : pUapIfEntry - pointer to tEvbUapIfEntry if entry     */
/*                                       is present, otherwise NULL          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tEvbUapIfEntry *
VlanEvbUapIfGetEntry (UINT4 u4UapIfIndex)
{
    tEvbUapIfEntry   *pUapIfEntry    = NULL;

    MEMSET(&gUapIfEntry, 0, sizeof (tEvbUapIfEntry));

    gUapIfEntry.u4UapIfIndex = u4UapIfIndex;

    pUapIfEntry = (tEvbUapIfEntry *) (VOID *) RBTreeGet 
                 (gEvbGlobalInfo.EvbUapIfTree, (tRBElem *)(VOID *)&gUapIfEntry);
    return pUapIfEntry;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfGetFirstEntry                            */
/*                                                                           */
/* Description        : This routine is used to get first                    */
/*                      tEvbUapIfEntry from gEvbGlobalInfo.EvbUapIfTree.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pUapIfEntry - pointer to tEvbUapIfEntry              */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tEvbUapIfEntry *
VlanEvbUapIfGetFirstEntry (VOID)
{
    tEvbUapIfEntry  *pUapIfEntry    = NULL;

    pUapIfEntry = (tEvbUapIfEntry *) (VOID *) RBTreeGetFirst
                     (gEvbGlobalInfo.EvbUapIfTree);
    return pUapIfEntry;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfGetNextEntry                             */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tEvbUapIfEntry from gEvbGlobalInfo.EvbUapIfTree for  */
/*                      the given UAP IfIndex.                               */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP If Index.                         */
/*                                                                           */
/* Output(s)          : pUapIfEntry - pointer to tEvbUapIfEntry              */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tEvbUapIfEntry *
VlanEvbUapIfGetNextEntry (UINT4 u4UapIfIndex)
{
    tEvbUapIfEntry    *pUapIfEntry     = NULL;

    MEMSET(&gUapIfEntry, 0, sizeof (tEvbUapIfEntry));

    gUapIfEntry.u4UapIfIndex = u4UapIfIndex;
     
    pUapIfEntry = (tEvbUapIfEntry *) (VOID *) RBTreeGetNext
                    ((gEvbGlobalInfo.EvbUapIfTree), (tRBElem *) (VOID *)
                       &gUapIfEntry, NULL);
    return pUapIfEntry;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfDelEntry                                 */
/*                                                                           */
/* Description        : This routine deletes a UAP If Entry and also deletes */
/*                      all the underlying S-Channel entries.                */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP If Index.                         */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapIfDelEntry (UINT4 u4UapIfIndex)
{
    tEvbUapIfEntry *pUapIfEntry = NULL;
 
    VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME, 
            "VlanEvbUapIfDelEntry: Hit for UAP Index %d\r\n ",
            u4UapIfIndex);

    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);

    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME, 
                       "VlanEvbUapIfDelEntry: "
                       "Entry for UAP Index %d not exists\r\n", u4UapIfIndex);
        return VLAN_SUCCESS;
    }
    /* De Registering the API provided to LLDP moudule by EVB */
    pUapIfEntry->pEvbLldpSChannelIfNotify = (VOID *)NULL;
   
    /* The following function deletes all the S-Channel 
     * entries from control plane as well from data plane. */ 
    VlanEvbSChIfDelAllEntriesOnUap (u4UapIfIndex);
    VlanEvbSChIfDelEntry (u4UapIfIndex, VLAN_EVB_DEF_SVID, VLAN_EVB_DEF_SCID);

    if(RBTreeRemove ((gEvbGlobalInfo.EvbUapIfTree),
                      (tRBElem *)(VOID* )pUapIfEntry) == RB_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanEvbUapIfDelEntry: "
                       "UAP entry for Index %d is not able to be removed "
                       "from EvbUapIfTree\r\n", u4UapIfIndex);
        return VLAN_FAILURE;
    }

    VLAN_EVB_RELEASE_BUF (VLAN_EVB_UAP_ENTRY, (VOID *)pUapIfEntry);
    pUapIfEntry = NULL;
    UNUSED_PARAM(pUapIfEntry);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfDelAllEntries                            */
/*                                                                           */
/* Description        : This function deletes all the  UAP entries from      */
/*                      gEvbGlobalInfo.EvbUapIfTree.                         */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanEvbUapIfDelAllEntries (VOID)
{
    tEvbUapIfEntry  *pUapIfEntry = NULL;

    pUapIfEntry = VlanEvbUapIfGetFirstEntry ();

    while (pUapIfEntry != NULL)
    {
        /* Delete the UAP entry and underlying S-Channel 
         * entries are also be deleted.*/
        VlanEvbUapIfDelEntry (pUapIfEntry->u4UapIfIndex);
        pUapIfEntry = NULL;
        pUapIfEntry = VlanEvbUapIfGetFirstEntry ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapGetOperStatus                              */
/*                                                                           */
/* Description        : This function gets the operational status            */
/*                      of the UAP interface.                                */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Interface Index                   */
/*                                                                           */
/* Output(s)          : pu1OperStatus - Operational status                   */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 
VlanEvbUapGetOperStatus (UINT4 u4UapIfIndex, UINT1 *pu1OperStatus)
{
    if (VlanCfaGetIfOperStatus (u4UapIfIndex, pu1OperStatus) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanEvbUapGetOperStatus: "
                       "Oper status get for UAP Index %d failed\r\n",
                        u4UapIfIndex);
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapSetOperStatusUp                            */
/*                                                                           */
/* Description        : This function updates the S-Channel Handler and      */
/*                      CDCP handler about the operational status - UP.      */
/*                      It sets the UAP port's CDCP running status.          */
/*                      The trigger to S-Channel handler is given so that    */
/*                      it provisions the S-Channel interfaces in the        */
/*                      hardware in case of CDCP is disabled on the UAP.     */
/*                      When CDCP is enabled on UAP and the CDCP mode is     */
/*                      dynamic, the trigger to CDCP handler is given so     */
/*                      that default CDCP TLV pair is transmitted over the   */
/*                      UAP (whereas static S-Channel interfaces must not    */
/*                      be present).                                         */
/*                                                                           */
/*                      When CDCP is enabled on UAP and the CDCP mode is     */
/*                      hybrid, the trigger to CDCP handler is given so      */
/*                      that default CDCP TLV pair is transmitted over the   */
/*                      UAP (provided that static S-Channel entries are      */
/*                      present). The trigger is not given to the S-Channel  */
/*                      Handler for provisioning the entries in the hardware.*/
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapSetOperStatusUp (tEvbUapIfEntry *pUapIfEntry)
{
    INT4 i4RetVal = VLAN_FAILURE;
    /* This function does the following tasks.
     * 1. Underlying S-Channel entries of UAP are programmed in hardware
     *    if their admin status is also UP.
     * 2. Those S-Channel entries oper status are set as UP.
     * 3. In case of dynamic/hybrid, program only default pair. */
    
    /* Making i4RetVal as UNUSED_PARAM due to coverity and again using it below */
    UNUSED_PARAM(i4RetVal);
    i4RetVal = VlanEvbSChSetAllEntriesOperStatus (pUapIfEntry, CFA_IF_UP);

    if ((pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE) &&
     (pUapIfEntry->i4EvbSysEvbLldpTxEnable == VLAN_EVB_CDCP_LLDP_ENABLE))
    {
        /* This function does the following tasks.
         * 1. If i4EvbSysEvbLldpTxEnable is enabled, Tx default (1,1) pair 
         *    since registration is already done with LLDP.
         * 2. Sets i4UapIfCdcpOperState as RUNNING. */
	    VlanEvbCdcpConstructTlv (pUapIfEntry);
        if ((VlanEvbPortPostMsgToCdcp (pUapIfEntry, VLAN_EVB_CDCP_LLDP_PORT_UPDATE)) 
	                                                 	== VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbUapSetOperStatusUp: "
                    "Setting UAP oper status UP failed for UAP port %d\r\n",
                    pUapIfEntry->u4UapIfIndex);
            return VLAN_FAILURE;
        }
        pUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_RUNNING;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapSetOperStatusDown                          */
/*                                                                           */
/* Description        : This function updates the S-Channel Handler and      */
/*                      CDCP handler about the operational status change.    */
/*                      It sets the UAP port's CDCP running status and       */
/*                      remote UAP port's CDCP status.                       */
/*                                                                           */
/*                      When the operational status comes DOWN,              */
/*                      The trigger to S-Channel handler is given so that it */
/*                      deletes the S-Channel interfaces from the hardware   */
/*                      in case of CDCP is disabled/enabled on the UAP.      */
/*                      On the successful deletion of S-Channels, the SVLAN  */
/*                      Pool manager bit list is reset for the UAP port.     */
/*                      When CDCP is enabled on UAP and the CDCP mode is     */
/*                      dynamic/hybrid, the trigger to CDCP handler is given */
/*                      so that CDCP TLVs transmission is stopped over the   */
/*                      UAP.                                                 */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapSetOperStatusDown (tEvbUapIfEntry *pUapIfEntry)
{
    /* This function does the following tasks.
     * 1. S/D/H: Underlying S-Channel entries of UAP are deleted from hardware.
     * 2. S/D/H: Those S-Channel entries oper status are set as DOWN.
     * 3. D: S-Channels are deleted in control plane.
     * 3. D/H: SVLAN Pool Manager of UAP is reset.
     * 4. D/H: EVB Local TLV of UAP entry is reset. */
    if (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE)
    {
        /* The UAP port down indication is already given to
         * LLDP from L2IWF where the tx/rx will be stopped.
         * At that time, it is necessary to clear the Local and remote
         * CDCP TLV data base from LLDP data structure.
         * When the UAP port UP indication comes, the VlanEvbCdcpPortUpdate
         * will take care of txing the TLV to LLDP. */
	if (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
	{
            VlanEvbSChIfDelAllEntriesOnUap (pUapIfEntry->u4UapIfIndex);
	}
	else   /* HYBRID */
	{
	    VlanEvbSChSetStaticEntriesOperStatus (pUapIfEntry, CFA_IF_DOWN);
	}
        /* NOTE: do we have to set default pair and its bit ? */
        MEMSET (pUapIfEntry->UapSvidList, 0, sizeof (tVlanList));
        MEMSET (pUapIfEntry->au1UapCdcpTlv, 0, 
                sizeof (pUapIfEntry->au1UapCdcpTlv)); 
    }
    else /* VLAN_EVB_UAP_CDCP_DISABLE */
    {
        if ((VlanEvbSChSetAllEntriesOperStatus (pUapIfEntry, CFA_IF_DOWN)) 
                == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbUapSetOperStatusDown: "
                    "Setting UAP oper status DOWN failed for UAP port %d\r\n",
                    pUapIfEntry->u4UapIfIndex);
            return VLAN_FAILURE;
        }
    }
    /* For Dynamic Mode , i4UapIfCdcpOperState will be updated in 
     * VlanEvbSchIfDelDynamicEntry */
    if (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
    {
        pUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
    }
    pUapIfEntry->i4UapIfSchCdcpRemoteEnabled = VLAN_EVB_UAP_CDCP_DISABLE;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapSetOperStatus                              */
/*                                                                           */
/* Description        : This function updates the S-Channel Handler and      */
/*                      CDCP handler about the operational status change.    */
/*                      It sets the UAP port's CDCP running status and       */
/*                      remote UAP port's CDCP status.                       */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Interface Index                   */
/*                      u1OperStatus - CFA_IF_UP/CFA_IF_DOWN.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 
VlanEvbUapSetOperStatus (UINT4 u4UapIfIndex, UINT1 u1OperStatus)
{
    /* This fun is called from VlanHandlePortOperStatusChg */
    tEvbUapIfEntry *pUapIfEntry = NULL;

    VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, 
            VLAN_NAME,"VlanEvbUapSetOperStatus: Hit for "
            "UAP Index %d Status %d\r\n", u4UapIfIndex, u1OperStatus);

    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);

    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanEvbUapSetOperStatus: "
                       "Entry for UAP Index %d not exists\r\n", u4UapIfIndex);
        return VLAN_FAILURE;
    }
    if (u1OperStatus == CFA_IF_UP)
    {
        /* This fun is called from VlanEvbEnable */
        if (VlanEvbUapSetOperStatusUp (pUapIfEntry) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                     "VlanEvbUapSetOperStatus: "
                     "Setting UAP oper status UP failed for UAP port %d\r\n",
                     u4UapIfIndex);
            return VLAN_FAILURE;
        }
    }
    else /* u1OperStatus == CFA_IF_DOWN */
    {
        /* This fun is called from VlanEvbDisable */
        if (VlanEvbUapSetOperStatusDown (pUapIfEntry) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbUapSetOperStatus: "
                    "Setting UAP oper status DOWN failed for UAP port %d\r\n",
                     u4UapIfIndex);
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapEnableCdcpStatus                           */
/*                                                                           */
/* Description        : The CDCP status for the UAP is Enable. It calls the  */
/*                      register function to trigger the CDCP handler to     */
/*                      send the default CDCP TLV.                           */
/*                      CDCP enabling is not allowed when static             */
/*                      S-Channel interface entries are present in dynamic.  */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapEnableCdcpStatus (tEvbUapIfEntry *pUapIfEntry)
{
    tEvbSChIfEntry *pSChIfEntry = NULL;

    /* Default S-Channel entry must not be used for the below
     * validations. Hence getting the next entry other than 
     * default S-Channel entry on a UAP port. */
    pSChIfEntry = VlanEvbSChIfGetNextEntry (pUapIfEntry->u4UapIfIndex, 
                                            VLAN_EVB_DEF_SVID);

    if ((pSChIfEntry != NULL) &&
        (pSChIfEntry->u4SChId != VLAN_EVB_DEF_SCID) &&
        (pSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID) &&
        (pUapIfEntry->i4UapIfSChMode == 
         VLAN_EVB_UAP_SCH_MODE_DYNAMIC))
    {
        /* Static entries are present and so can't enable CDCP */
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbUapEnableCdcpStatus: "
                "CDCP can't be enabled on UAP port %d since static "
                "S-Channel entries are present\r\n", 
                pUapIfEntry->u4UapIfIndex);
        return VLAN_FAILURE;
    }

    if ((pSChIfEntry == NULL) &&
        (pUapIfEntry->i4UapIfSChMode ==
         VLAN_EVB_UAP_SCH_MODE_HYBRID))
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                "VlanEvbUapEnableCdcpStatus: "
                "Static S-Channel entries other than default entry "
                "is not present for UAP port %d in hybrid mode\r\n", 
                pUapIfEntry->u4UapIfIndex);
        /* Do not return here. */
    }
    /* The tasks done are:-
     * 1. Register with LLDP irrespective of port status.
     * 2. UAP port status is UP, place the TLV to LLDP. */
    return (VlanEvbPortPostMsgToCdcp (pUapIfEntry,VLAN_EVB_CDCP_LLDP_PORT_REG));
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapDisableCdcpStatus                          */
/*                                                                           */
/* Description        : The CDCP status for the UAP is disable. It calls the */
/*                      deregister function of CDCP handler  to stop txing   */
/*                      the CDCP TLV.                                        */
/*                                                                           */ 
/*                      In dynamic mode:-                                    */
/*                      ------------------                                   */
/*                      1. S-Channel entries are deleted in hw and sw.       */
/*                      2. SVLAN pool manager and Local TLV on UAP are reset.*/
/*                                                                           */ 
/*                      In hybrid mode:-                                     */
/*                      ----------------                                     */
/*                      1. S-Channel entries are deleted in hw.              */
/*                      2. Nego status for S-Channel entries are set as free.*/
/*                      3. SVLAN pool manager and Local TLV on UAP are reset.*/ 
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapDisableCdcpStatus (tEvbUapIfEntry *pUapIfEntry)
{
    if (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
    {
        /* This function does the following tasks.
         * 1. Underlying S-Channel entries of UAP are deleted from hardware
         * 2. Underlying S-Channel entries of UAP are deleted from software
         * 3. SVLAN Pool Manager of UAP is reset in dynamic/hybrid.
         * 4. EVB Local TLV of UAP entry is reset in dynamic/hybrid. */
        VlanEvbSChIfDelAllEntriesOnUap (pUapIfEntry->u4UapIfIndex);
    }
    else /* VLAN_EVB_UAP_SCH_MODE_HYBRID */
    {
        /* This function does the following tasks.
         * 1. Underlying S-Channel entries of UAP are deleted from hardware
         * 2. Underlying S-Channel entries i4NegoStatus = freed.
         * 3. SVLAN Pool Manager of UAP is reset in dynamic/hybrid.
         * 4. EVB Local TLV of UAP entry is reset in dynamic/hybrid. */
        if ((VlanEvbSChSetStaticEntriesOperDown (pUapIfEntry, OSIX_TRUE)) 
                    == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbUapDisableCdcpStatus: "
                    "Deleting S-Channel entries from HW on the UAP port "
                    "%d is failed\r\n", pUapIfEntry->u4UapIfIndex);
            return VLAN_FAILURE;
        }
    }
    /* NOTE: do we have to set default pair and its bit ? */
    MEMSET (pUapIfEntry->UapSvidList, 0, sizeof (tVlanList));
    MEMSET (pUapIfEntry->au1UapCdcpTlv, 0, 
            sizeof (pUapIfEntry->au1UapCdcpTlv)); 
    /* The tasks done are:-
     * 1. Clears the LLDP local and remote TLV info
     * 2. De-Register with LLDP irrespective of port status. */
    return (VlanEvbCdcpPortDeReg (pUapIfEntry));
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapSetCdcpStatus                              */
/*                                                                           */
/* Description        : The CDCP status for the UAP is set in this function  */
/*                      and respective actions for Enable/Disable are taken  */
/*                      care.                                                */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Interface Index.                  */
/*                      i4Status - VLAN_EVB_UAP_CDCP_ENABLE/                 */
/*                                 VLAN_EVB_UAP_CDCP_DISABLE                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 
VlanEvbUapSetCdcpStatus (UINT4 u4UapIfIndex, INT4 i4Status) 
{
    tEvbUapIfEntry *pUapIfEntry = NULL;

    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);

    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbUapSetCdcpStatus: "
                       "Entry for UAP Index %d not exists\r\n", u4UapIfIndex);
        return VLAN_FAILURE;
    }

    if (pUapIfEntry->i4UapIfSchCdcpAdminEnable == i4Status)
    {
        if (i4Status == VLAN_EVB_UAP_CDCP_ENABLE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                    "VlanEvbUapSetCdcpStatus: "
                    "CDCP is already enabled on this UAP Port %d\r\n", 
                    u4UapIfIndex);
        }
        else /* VLAN_EVB_UAP_CDCP_DISABLE */
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                    "VlanEvbUapSetCdcpStatus: "
                    "CDCP is already disabled on this UAP Port %d\r\n", 
                    u4UapIfIndex);
        }
        return VLAN_SUCCESS;
    }
    if (i4Status == VLAN_EVB_UAP_CDCP_ENABLE)
    {
        if (VlanEvbUapEnableCdcpStatus (pUapIfEntry) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbUapSetCdcpStatus: "
                    "Enabling CDCP on UAP port %d is failed \r\n", 
                    u4UapIfIndex);
            return VLAN_FAILURE;
        }
    }
    else /* VLAN_EVB_UAP_CDCP_DISABLE */
    {
        if (VlanEvbUapDisableCdcpStatus (pUapIfEntry) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbUapSetCdcpStatus: "
                    "Disabling CDCP on UAP port %d is failed \r\n", 
                    u4UapIfIndex);
            return VLAN_FAILURE;
        }
        pUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
        pUapIfEntry->i4UapIfSchCdcpRemoteEnabled = VLAN_EVB_UAP_CDCP_DISABLE;
    }
    pUapIfEntry->i4UapIfSchCdcpAdminEnable = i4Status;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfSetDefProperties                         */
/*                                                                           */
/* Description        : This function sets the following port properties for */
/*                      the given UAP port in tVlanPortEntry.                */ 
/*                      -Admit all frames.                                   */
/*                      -PVID parameter equal to the default S-chan SVID-1   */
/*                      -Should be included in the member set and un-tagged  */
/*                      set for the default S-channel S-VID-1. This          */ 
/*                      operation helps to transmit out default S-Channel    */
/*                      packets out of UAP without S-tag.                    */
/*                      -Default TPID for the port as 0x88a8                 */
/*                                                                           */
/* Input(s)           : pPortEntry - VLAN port entry                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID 
VlanEvbUapIfSetDefProperties (tVlanPortEntry *pVlanPortEntry)
{
    /* The following items are aleady taken care in VlanHandleCreatePort.
     * 1. PVID = default SVID (1) ie. VLAN_DEFAULT_PORT_VID.
     * 2. Inclusion in member set and untagged set for default SVID (1)
     */
    pVlanPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
    pVlanPortEntry->u2IngressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPortEntry->u2EgressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPortEntry->u2AllowableTPID1 = VLAN_PROTOCOL_ID;
    pVlanPortEntry->u2AllowableTPID2 = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPortEntry->u2AllowableTPID3 = VLAN_PROVIDER_PROTOCOL_ID;
    pVlanPortEntry->u2FiltUtilityCriteria = VLAN_ENHANCE_FILTERING_CRITERIA;
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSbpCreateIndication                           */
/*                                                                           */
/* Description        : This function indicates to create SBP ports for each */
/*                      S-Channel entries present in this UAP port.          */ 
/*                                                                           */
/* Input(s)           : pUapIfEntry - UAP Interface Entry.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbSbpCreateIndication (tEvbUapIfEntry *pUapIfEntry)
{
    tEvbSChIfEntry      *pSChIfEntry    = NULL;
    UINT4                u4UapIfIndex   = pUapIfEntry->u4UapIfIndex;
    UINT4                u4SVId         = VLAN_EVB_DEF_SVID;
    INT4                 i4RetVal       = VLAN_SUCCESS;
    UINT1                u1Status       = 0;

    /* IMPORTANT:  This func should be called only in VlanEvbEnable */

    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                != NULL)
    {
        if (pUapIfEntry->u4UapIfIndex != pSChIfEntry->u4UapIfIndex)
        {
            /* Indicated all the SBP ports creation under this UAP */
            break;
        }
        if (VlanPortCfaCreateSChannelInterface (pUapIfEntry->u4UapIfIndex,
                    (UINT4)pSChIfEntry->i4SChIfIndex , u1Status) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSbpCreateIndication: SBP port "
                    "%d creation failed \r\n", pSChIfEntry->i4SChIfIndex);
            /* Do not return here , so as to continue to create other SBPs */
            i4RetVal = VLAN_FAILURE;
        }
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
    }
    return i4RetVal;
}


/*****************************************************************************/
/* Function Name      : VlanEvbSbpDeleteIndication                           */
/*                                                                           */
/* Description        : This function indicates to delete SBP ports for each */
/*                      S-Channel entries present in this UAP port.          */ 
/*                                                                           */
/* Input(s)           : pUapIfEntry - UAP Interface Entry.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbSbpDeleteIndication (tEvbUapIfEntry *pUapIfEntry)
{
    tEvbSChIfEntry      *pSChIfEntry    = NULL;
    UINT4                u4UapIfIndex   = pUapIfEntry->u4UapIfIndex;
    UINT4                u4SVId         = VLAN_EVB_DEF_SVID;
    INT4                 i4RetVal       = VLAN_SUCCESS;

    /* IMPORTANT:  This func should be called only in VlanEvbDisable */
    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                != NULL)
    {
        if (pUapIfEntry->u4UapIfIndex != pSChIfEntry->u4UapIfIndex)
        {
            /* Indicated all the SBP ports deletion under this UAP */
            break;
        }
        if (VlanPortCfaDeleteSChannelInterface ((UINT4)pSChIfEntry->i4SChIfIndex)
                == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSbpDeleteIndication: SBP port "
                    "%d deletion failed \r\n", pSChIfEntry->i4SChIfIndex);
            /* Do not return here , so as to continue to delete other SBPs */
            i4RetVal = VLAN_FAILURE;
        }
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
    }
    return i4RetVal;
}

#endif /* __VLNEVUAP_C__ */


