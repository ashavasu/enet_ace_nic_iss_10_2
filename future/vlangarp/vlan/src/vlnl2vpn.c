/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnl2vpn.c,v 1.33 2017/06/15 13:37:33 siva Exp $
 *
 * Description: This file contains utility routines used in VLANmodule
 *              for MPLS Interactions. 
 *
 *******************************************************************/
#ifndef _VLNL2VPN_C
#define _VLNL2VPN_C
#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnInit                                    */
/*                                                                           */
/*    Description         : This function initialises the Data Structures    */
/*                          in VLAN modules required for MPLS-VLAN Interaction*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnInit (VOID)
{
    gpVlanL2VpnPortMapTbl =
        RBTreeCreateEmbedded (0, VlanL2VpnCmpPortBasedMapTbl);

    if (gpVlanL2VpnPortMapTbl == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "VlanL2VpnInit :RBTree Creation of Port Map "
                  "Table failed !!!!!!!\r\n");
        return VLAN_FAILURE;
    }

    gpVlanL2VpnPortVlanMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanL2VpnMap, L2VpnPortVlanBased),
                              VlanL2VpnCmpPortVlanBasedMapTbl);

    if (gpVlanL2VpnPortVlanMapTbl == NULL)
    {
        RBTreeDestroy (gpVlanL2VpnPortMapTbl, VlanL2VpnFreeTreeNode,
                       VLAN_L2VPN_MAP_ENTRY);

        gpVlanL2VpnPortMapTbl = NULL;

        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "VlanL2VpnInit :RBTree Creation of Port Vlan Map "
                  "Table failed !!!!!!!\r\n");
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnDeInit                                  */
/*                                                                           */
/*    Description         : This function destroys the Data Structures and   */
/*                          Mempools in VLAN module required for MPLS-VLAN   */
/*                          Interaction                                      */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanL2VpnDeInit (VOID)
{
    if (gpVlanL2VpnPortMapTbl != NULL)
    {
        RBTreeDestroy (gpVlanL2VpnPortMapTbl, VlanL2VpnFreeTreeNode,
                       VLAN_L2VPN_MAP_ENTRY);
        gpVlanL2VpnPortMapTbl = NULL;
    }

    if (gpVlanL2VpnPortVlanMapTbl != NULL)
    {
        RBTreeDestroy (gpVlanL2VpnPortVlanMapTbl, VlanL2VpnFreeTreeNode,
                       VLAN_L2VPN_MAP_ENTRY);

        gpVlanL2VpnPortVlanMapTbl = NULL;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnCmpPortBasedMapTbl                      */
/*                                                                           */
/*    Description         : This function is the compare function used by    */
/*                          FSAP RBTree Application when there is an access  */
/*                          to the RBTree                                    */
/*                                                                           */
/*    Input(s)            : pNodeA - Element1 of RBTree (tVlanL2VpnMap)      */
/*                          pNodeB - Element2 of RBTree (tVlanL2VpnMap)      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 /-1 / 0                                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnCmpPortBasedMapTbl (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tVlanL2VpnMap      *pPortL2VpnMapA = (tVlanL2VpnMap *) pNodeA;
    tVlanL2VpnMap      *pPortL2VpnMapB = (tVlanL2VpnMap *) pNodeB;

    if (pPortL2VpnMapA->u4ContextId != pPortL2VpnMapB->u4ContextId)
    {
        if (pPortL2VpnMapA->u4ContextId > pPortL2VpnMapB->u4ContextId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    if (pPortL2VpnMapA->u4IfIndex != pPortL2VpnMapB->u4IfIndex)
    {
        if (pPortL2VpnMapA->u4IfIndex > pPortL2VpnMapB->u4IfIndex)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnCmpPortVlanBasedMapTbl                  */
/*                                                                           */
/*    Description         : This function is the compare function used by    */
/*                          FSAP RBTree Application when there is an access  */
/*                          to the RBTree                                    */
/*                                                                           */
/*    Input(s)            : pNodeA - Element1 of RBTree (tVlanL2VpnMap)      */
/*                          pNodeB - Element2 of RBTree (tVlanL2VpnMap)      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 /-1 / 0                                        */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnCmpPortVlanBasedMapTbl (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tVlanL2VpnMap      *pPortL2VpnMapA = (tVlanL2VpnMap *) pNodeA;
    tVlanL2VpnMap      *pPortL2VpnMapB = (tVlanL2VpnMap *) pNodeB;

    if (pPortL2VpnMapA->u4ContextId != pPortL2VpnMapB->u4ContextId)
    {
        if (pPortL2VpnMapA->u4ContextId > pPortL2VpnMapB->u4ContextId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    if (pPortL2VpnMapA->VlanId != pPortL2VpnMapB->VlanId)
    {
        if (pPortL2VpnMapA->VlanId > pPortL2VpnMapB->VlanId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    else if ((pPortL2VpnMapA->VlanId == pPortL2VpnMapB->VlanId)
             && (pPortL2VpnMapB->u4IfIndex != 0)
             && (pPortL2VpnMapA->u4IfIndex != 0))
    {
        if (pPortL2VpnMapA->u4IfIndex != pPortL2VpnMapB->u4IfIndex)
        {
            if (pPortL2VpnMapA->u4IfIndex > pPortL2VpnMapB->u4IfIndex)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }

    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnGetMemBlock                             */
/*                                                                           */
/*    Description         : This function is an utility routine to allocate  */
/*                          Memory from the denoted Mempool.                 */
/*                                                                           */
/*    Input(s)            : i4BufType - Identifies a Mempool Id              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pu1Buf - Allocated MemBlock                      */
/*                                                                           */
/*****************************************************************************/

UINT1              *
VlanL2VpnGetMemBlock (INT4 i4BufType)
{
    UINT1              *pu1Buf = NULL;

    switch (i4BufType)
    {
        case VLAN_L2VPN_MAP_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanL2VpnMapPoolId,
                                  pu1Buf);
            break;

            /*For Future Enhancements */

        default:
            break;
    }

    return pu1Buf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnReleaseMemBlock                         */
/*                                                                           */
/*    Description         : This function is an utility routine to reelease  */
/*                          Memory from the denoted Mempool.                 */
/*                                                                           */
/*    Input(s)            : i4BufType - Identifies a Mempool Id              */
/*                          pu1Buf  - MemBlock to be released                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnReleaseMemBlock (INT4 i4BufType, UINT1 *pu1Buf)
{

    switch (i4BufType)
    {
        case VLAN_L2VPN_MAP_ENTRY:
            VLAN_RELEASE_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.
                                    VlanL2VpnMapPoolId, pu1Buf);
            break;

            /* For Future Enhancements */
        default:
            break;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnFreeTreeNode                            */
/*                                                                           */
/*    Description         : This function is an utility routine used by RBTree*/
/*                          Application to free the Deleted Node from the    */
/*                          RBTree.                                          */
/*                                                                           */
/*    Input(s)            : pNode - RBTree Node to be released               */
/*                          u4NodeType - Argument identifes the Node Type    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : RB_SUCCESS/RB_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnFreeTreeNode (tRBElem * pNode, UINT4 u4NodeType)
{
    VlanL2VpnReleaseMemBlock (VLAN_L2VPN_MAP_ENTRY, pNode);

    UNUSED_PARAM (u4NodeType);    /* For Future Enhancements it can be used */

    return RB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnHandleAddL2VpnInfo                      */
/*                                                                           */
/*    Description         : This function process the message for creating   */
/*                          L2Vpn Map Entry based on Service type. This msg  */
/*                          was posted by MPLS Module                        */
/*                                                                           */
/*    Input(s)            : pL2VpnData- Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnHandleAddL2VpnInfo (tVplsInfo * pL2VpnData)
{
    tVlanL2VpnMap      *pL2VpnMapInfo = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Add L2VPN map Failed as module is shutdown\n");
        return;
    }

    pL2VpnMapInfo =
        (tVlanL2VpnMap *) (VOID *) VlanL2VpnGetMemBlock (VLAN_L2VPN_MAP_ENTRY);

    if (pL2VpnMapInfo == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for L2Vpn Map Info\n");
        return;
    }

    pL2VpnMapInfo->u4ContextId = pL2VpnData->u4ContextId;
    pL2VpnMapInfo->u4IfIndex = pL2VpnData->u4IfIndex;
    pL2VpnMapInfo->VlanId = pL2VpnData->VlanId;
    pL2VpnMapInfo->u4VplsIndex = pL2VpnData->u4VplsIndex;

    pL2VpnMapInfo->u1PwVcMode = pL2VpnData->u1PwVcMode;

    if (pL2VpnData->u2MplsServiceType == VLAN_L2VPN_PORT_BASED)
    {
        /*Since service type is port based in rcvd message, This Information
         * is meant for adding the VpnInfo in the Port Based RBTree only*/
        if (RBTreeAdd (gpVlanL2VpnPortMapTbl, (tRBElem *) pL2VpnMapInfo)
            != RB_SUCCESS)
        {
            VlanL2VpnReleaseMemBlock (VLAN_L2VPN_MAP_ENTRY,
                                      (UINT1 *) pL2VpnMapInfo);

            return;
        }
    }
    else
    {
        /*For Port + VLAN OR VLAN Based */
        /*Valid VLAN ID is present, this information should be added to 
         * VLAN Based RBTree */
        if (RBTreeAdd (gpVlanL2VpnPortVlanMapTbl, (tRBElem *) pL2VpnMapInfo)
            != RB_SUCCESS)
        {
            VlanL2VpnReleaseMemBlock (VLAN_L2VPN_MAP_ENTRY,
                                      (UINT1 *) pL2VpnMapInfo);

            return;
        }

        /*Disable Mac-Learning on the Hardware */
        if (pL2VpnData->u1PwVcMode == VLAN_L2VPN_VPWS)
        {
            if (VLAN_GET_CURR_ENTRY ((tVlanId) pL2VpnMapInfo->VlanId) == NULL)
            {

                return;

            }
            if (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS
                ((tVlanId) pL2VpnMapInfo->VlanId) == VLAN_ENABLED)
            {
                pu1LocalPortList =
                    UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    return;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));

                pStVlanEntry =
                    VlanGetStaticVlanEntry ((tVlanId) pL2VpnMapInfo->VlanId);
                if (pStVlanEntry == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Static vlan entry not found\n");
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                    return;
                }

                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                /* Directly calling NPAPI to disable mac-learning */

                if (VlanHwMacLearningStatus
                    (VLAN_CURR_CONTEXT_ID (), (tVlanId) pL2VpnMapInfo->VlanId,
                     pu1LocalPortList, VLAN_DISABLED) != VLAN_SUCCESS)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "MAC-Learning Disabling Failed\n");
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                    return;

                }
                /*Setting the oper-status to disabled */
                VLAN_CONTROL_OPER_MAC_LEARNING_STATUS ((tVlanId) pL2VpnMapInfo->
                                                       VlanId) = VLAN_DISABLED;
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }
        }

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnHandleDelL2VpnInfo                      */
/*                                                                           */
/*    Description         : This function process the message for deleting   */
/*                          L2Vpn Map Entry based on Service type. This msg  */
/*                          was posted by MPLS Module                        */
/*                                                                           */
/*    Input(s)            : pL2VpnData- Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnHandleDelL2VpnInfo (tVplsInfo * pL2VpnData)
{
    tVlanL2VpnMap       L2VpnMapInfo;
    tVlanL2VpnMap      *pTempL2VpnMapInfo = NULL;

    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Delete L2VPN map Failed as module is shutdown\n");
        return;
    }

    VLAN_MEMSET (&L2VpnMapInfo, VLAN_INIT_VAL, sizeof (tVlanL2VpnMap));

    L2VpnMapInfo.u4ContextId = pL2VpnData->u4ContextId;
    L2VpnMapInfo.u4IfIndex = pL2VpnData->u4IfIndex;
    L2VpnMapInfo.VlanId = pL2VpnData->VlanId;

    if (pL2VpnData->u2MplsServiceType == VLAN_L2VPN_PORT_BASED)
    {
        /*Since service type is port based in the received message, This 
         * Information is meant for deleting the VpnInfo in the Port 
         * Based RBTree only*/
        pTempL2VpnMapInfo = (tVlanL2VpnMap *) RBTreeRem (gpVlanL2VpnPortMapTbl,
                                                         (tRBElem *) &
                                                         L2VpnMapInfo);
        if (pTempL2VpnMapInfo != NULL)
        {
            VlanL2VpnReleaseMemBlock (VLAN_L2VPN_MAP_ENTRY,
                                      (UINT1 *) pTempL2VpnMapInfo);

            return;
        }
    }
    else
    {

        if (pL2VpnData->u1PwVcMode == VLAN_L2VPN_VPWS)
        {
            if (VLAN_GET_CURR_ENTRY ((tVlanId) L2VpnMapInfo.VlanId) == NULL)
            {
                return;
            }
            /*Enable the mac-learning only if user has enabled it and the oper is disabled */
            if (((VLAN_CONTROL_ADM_MAC_LEARNING_STATUS (L2VpnMapInfo.VlanId) ==
                  VLAN_ENABLED)
                 ||
                 ((VLAN_CONTROL_ADM_MAC_LEARNING_STATUS (L2VpnMapInfo.VlanId) ==
                   VLAN_DEFAULT)))
                && (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (L2VpnMapInfo.VlanId)
                    == VLAN_DISABLED))
            {
                pu1LocalPortList =
                    UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    return;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));

                pStVlanEntry =
                    VlanGetStaticVlanEntry ((tVlanId) L2VpnMapInfo.VlanId);
                if (pStVlanEntry == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Static vlan entry not found\n");
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                    return;
                }

                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                /* Directly calling NPAPI to disable mac-learning */

                if (VlanHwMacLearningStatus
                    (VLAN_CURR_CONTEXT_ID (), (tVlanId) L2VpnMapInfo.VlanId,
                     pu1LocalPortList, VLAN_ENABLED) != VLAN_SUCCESS)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "MAC-Learning Enabling Failed\n");
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                    return;
                }

                VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (L2VpnMapInfo.VlanId) =
                    VLAN_ENABLED;
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }
        }

        /*For Port + VLAN OR VLAN Based */
        /*Valid VLAN ID is present, this information should be deleted from 
         * VLAN Based RBTree */
        pTempL2VpnMapInfo = (tVlanL2VpnMap *) RBTreeRem
            (gpVlanL2VpnPortVlanMapTbl, (tRBElem *) & L2VpnMapInfo);
        if (pTempL2VpnMapInfo != NULL)
        {
            VlanL2VpnReleaseMemBlock (VLAN_L2VPN_MAP_ENTRY,
                                      (UINT1 *) pTempL2VpnMapInfo);

            return;
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnHandleMplsPktFwdOnPorts                 */
/*                                                                           */
/*    Description         : This function process the packet recvd from the  */
/*                          message from MPLS Module. This Packet was recvd  */
/*                          on MPLS interface and to be fwd on ethernet ports*/
/*                                                                           */
/*    Input(s)            : pBuf - Packet received on MPLS Interface         */
/*                          pVlanIf - Message containing the information for */
/*                          processing the packet                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanL2VpnHandleMplsPktFwdOnPorts (tCRU_BUF_CHAIN_DESC * pFrame,
                                  tVlanIfMsg * pVlanIf)
{
    UINT1              *pu1LocalPortList = NULL;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    tVlanTag            VlanTag;
    INT4                i4RetVal = VLAN_FORWARD;
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    VLAN_MEMSET (&VlanTag, VLAN_INIT_VAL, sizeof (tVlanTag));
    if (VlanSelectContext (pVlanIf->u4ContextId) != VLAN_SUCCESS)
    {
        return;
    }
    /* Vlan entry is retrieved only if a valid Vlan ID is 
     * present other than Default Port Vlan ID for PW */
    if (pVlanIf->SVlanId != VLAN_L2VPN_DEF_PORT_VID)
    {
        pVlanCurrEntry = VlanGetVlanEntry (pVlanIf->SVlanId);

        if (pVlanCurrEntry == NULL)
        {
            return;
        }
    }
    VlanTag.OuterVlanTag.u2VlanId = pVlanIf->SVlanId;
    /*Handling of Priority tagged packets from the MPLS interface 
     * is not supported*/
    if (pVlanIf->u1MplsMode == VLAN_L2VPN_TAGGED_MODE)
    {
        VlanTag.OuterVlanTag.u1TagType = VLAN_TAGGED;
    }
    else
    {
        VlanTag.OuterVlanTag.u1TagType = VLAN_UNTAGGED;
    }
    VlanTag.OuterVlanTag.u1Priority = pVlanIf->u1Priority;
    VlanTag.OuterVlanTag.u1DropEligible = VLAN_FALSE;

    /* When PW is operating in RAW (Ethernet) mode, ethernet Port can be used
     * as AC. In this case, Egress filtering is done to verify the membership
     * of an ethernet port in a vlan only if a valid Vlan ID and ethernet port
     * is present. If Vlan Id or Port is Invalid, Egress filtering is not
     * done. */
    if ((pVlanIf->SVlanId != VLAN_L2VPN_DEF_PORT_VID) &&
        (pVlanIf->u2LocalPort != 0))
    {
        i4RetVal = VlanEgressFiltering (pVlanIf->u2LocalPort, pVlanCurrEntry,
                                        pVlanIf->DestAddr);
    }
    else if (pVlanIf->u2LocalPort != 0)
    {
        /*Verifying the OperStatus of Port */
        pPortEntry = VLAN_GET_PORT_ENTRY (pVlanIf->u2LocalPort);

        if (pPortEntry == NULL)
        {
            i4RetVal = VLAN_NO_FORWARD;
            return;
        }
        if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
        {
            i4RetVal = VLAN_NO_FORWARD;
        }
        else
        {
            i4RetVal = VLAN_FORWARD;
        }

    }

    if (i4RetVal != VLAN_FORWARD)
    {
        return;
    }

    if ((pVlanIf->SVlanId == VLAN_L2VPN_DEF_PORT_VID) ||
        (pVlanIf->u2LocalPort != 0))
    {
        /* In this case Attachment Circuit, can be Port (or) Port + Vlan
         * 
         * For both unicast and multicast traffic,
         *
         * 1. Port alone  - Traffic is forwarded to that port only
         * 2. Port + Vlan - Traffic is forwarded to that port in that Vlan */
        pDupBuf = VlanDupBuf (pFrame);

        if (pDupBuf == NULL)
        {
            return;
        }

        if (VLAN_IS_MCASTADDR (pVlanIf->DestAddr) == VLAN_TRUE)
        {
            VlanFwdOnSinglePort (pDupBuf, pVlanIf, pVlanIf->u2LocalPort,
                                 pVlanCurrEntry, &VlanTag, VLAN_MCAST_FRAME);
        }
        else
        {
            VlanFwdOnSinglePort (pDupBuf, pVlanIf, pVlanIf->u2LocalPort,
                                 pVlanCurrEntry, &VlanTag, VLAN_DATA_FRAME);

        }
#ifdef HVPLS_WANTED
        if (VLAN_IS_MCASTADDR (pVlanIf->DestAddr) == VLAN_TRUE)
        {
            VlanL2VpnPktFloodOnMplsPort (pFrame, pVlanIf, pVlanCurrEntry,
                                         &VlanTag);
        }
        else
        {
            VlanFwdUnicastDataPacket (pFrame, pVlanIf, pVlanCurrEntry,
                                      &VlanTag);
        }
#endif
    }
    else
    {
        /* Here, Attachment Circuit is Vlan alone. */
        if (VLAN_IS_MCASTADDR (pVlanIf->DestAddr) == VLAN_TRUE)
        {
            /* For multicast traffic, it is forwarded to all its egress
             * ports. */
            pu1LocalPortList = UtilPlstAllocLocalPortList
                (sizeof (tLocalPortList));
            if (pu1LocalPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Memory allocation failed for tLocalPortList\n");
                return;
            }
            MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
            VLAN_GET_CURR_EGRESS_PORTS (pVlanCurrEntry, pu1LocalPortList);

            VlanFwdOnPorts (pFrame, pVlanIf, pu1LocalPortList,
                            pVlanCurrEntry, &VlanTag, VLAN_MCAST_FRAME);
            UtilPlstReleaseLocalPortList (pu1LocalPortList);
#ifdef HVPLS_WANTED
            VlanL2VpnPktFloodOnMplsPort (pFrame, pVlanIf, pVlanCurrEntry,
                                         &VlanTag);
#endif
        }
        else
        {
            /* For unicast traffic, it is forwarded to the port in which
             * the MAC is learnt. */
            VlanFwdUnicastDataPacket (pFrame, pVlanIf, pVlanCurrEntry,
                                      &VlanTag);
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnPktFloodOnMplsPort                      */
/*                                                                           */
/*    Description         : This function is called from VLAN module to flood*/
/*                          a unknown unicast pkt/ mcast pkt on MPLS         */
/*                          interface. This function calls MPLS API to flood.*/
/*                                                                           */
/*    Input(s)            : pFrame- Packet received on MPLS Interface        */
/*                          pVlanIf - Message containing the information for */
/*                          processing the packet                            */
/*                          pVlanEntry - VLAN Curr Entry                     */
/*                          pInVlanTag - Contains pkt tag information        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanL2VpnPktFloodOnMplsPort (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf,
                             tVlanCurrEntry * pVlanEntry, tVlanTag * pInVlanTag)
{

    tVlanL2VpnMap      *pVlanL2VpnInfo = NULL;
    tVlanL2VpnMap       TempVlanL2VpnInfo;
    tVplsInfo           TempVplsInfo;
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
    tCfaIfInfo          IfInfo;
#ifdef HVPLS_WANTED
    UINT1               u1MplsPkt = FALSE;
#endif
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    if (VLAN_IS_PKT_RCVD_FROM_MPLS (pVlanIf) == VLAN_TRUE)
    {
        /*There is no support to switch the Pkt received from MPLS interface
         * back to same MPLS Interface */
#ifdef HVPLS_WANTED
        u1MplsPkt = TRUE;
        if (pVlanIf->u4PwVcIndex == 0)
#endif
        {
            return VLAN_NO_FORWARD;
        }
    }

    VlanCfaGetIfInfo (pVlanIf->u4IfIndex, &IfInfo);
    if ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
        (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))
    {
        /* Incoming interface is visible as Attachment circuit interface 
         * to the CFA So this packet should follow normal VLAN forwarding
         * logic should not send it to MPLS. */
        return VLAN_NO_FORWARD;
    }
    VLAN_MEMSET (&TempVlanL2VpnInfo, VLAN_INIT_VAL, sizeof (tVlanL2VpnMap));
    VLAN_MEMSET (&TempVplsInfo, VLAN_INIT_VAL, sizeof (tVplsInfo));

    TempVlanL2VpnInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    TempVlanL2VpnInfo.u4IfIndex = VLAN_GET_IFINDEX (pVlanIf->u2LocalPort);
    /*Searching Port Based l2VPN Map table */

    pVlanL2VpnInfo = RBTreeGet (gpVlanL2VpnPortMapTbl, &TempVlanL2VpnInfo);

    if (pVlanL2VpnInfo == NULL)
    {
        /*No Entry for in Port Based L2VPN Map Table */
        /* Searching VLAN Based L2VPN Map Table */

        TempVlanL2VpnInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
        TempVlanL2VpnInfo.VlanId = pVlanEntry->VlanId;
        TempVlanL2VpnInfo.u4IfIndex = pVlanIf->u4IfIndex;

        pVlanL2VpnInfo =
            RBTreeGet (gpVlanL2VpnPortVlanMapTbl, &TempVlanL2VpnInfo);

        if (pVlanL2VpnInfo == NULL)
        {
            /*No Entry for in Vlan Based L2VPN Map Table */
            return VLAN_NO_FORWARD;
        }

        /* If RBTree port is Non NULL, and the incoming port is same 
         * as the RBTree Port, then forward the packet else drop*/
        if ((pVlanL2VpnInfo->u4IfIndex != 0) &&
            (pVlanL2VpnInfo->u4IfIndex !=
             VLAN_GET_IFINDEX (pVlanIf->u2LocalPort)))
        {
            return VLAN_NO_FORWARD;
        }

    }

    /* Since NO MPLS Port is created in VLAN Module, Priority and DE Bit 
     * are set to default in the VLAN Tag added to the packet*/
    if (pVlanEntry != NULL)
    {
        if (pInVlanTag->OuterVlanTag.u1TagType == VLAN_UNTAGGED)
        {
            VlanAddTagToFrame (pFrame, pVlanEntry->VlanId,
                               VLAN_DEF_USER_PRIORITY, VLAN_DE_FALSE, NULL);
        }
        else if (pInVlanTag->OuterVlanTag.u1TagType == VLAN_PRIORITY_TAGGED)
        {
            VlanUpdateVlanTag (pFrame, pVlanEntry->VlanId,
                               VLAN_DEF_USER_PRIORITY, VLAN_DE_FALSE, NULL);
        }
    }
    /*pVlanTag->OuterVlanTag.u1TagType == VLAN_TAGGED --Already Tagged Packet
     * No need to update that Packet*/

    /*There is an Entry in Port / Vlan / Port + Vlan Based Map Table.
     * So Fwd the Packet to MPLS Module for further processing*/

    pDupBuf = VlanDupBuf (pFrame);

    if (pDupBuf == NULL)
    {
        return VLAN_NO_FORWARD;
    }
#ifdef HVPLS_WANTED
    if (u1MplsPkt == FALSE)
#endif
    {
        VlanMplsProcessL2Pkt (pDupBuf, pVlanL2VpnInfo->u4IfIndex,
                              pVlanL2VpnInfo->u4VplsIndex, CFA_LINK_BCAST);
    }
#ifdef HVPLS_WANTED
    else
    {
        VlanMplsProcessMplsPkt (pDupBuf, pVlanL2VpnInfo->u4IfIndex,
                                pVlanL2VpnInfo->u4VplsIndex, CFA_LINK_BCAST,
                                pVlanIf->u4PwVcIndex);
    }
#endif
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnFwdLearntPktOnMplsPort                  */
/*                                                                           */
/*    Description         : This function is called from VLAN module to switch*/
/*                          a known unicast pkt on MPLS interface. This      */
/*                          function calls MPLS API to fwd pkt on specific   */
/*                          MPLS Port.                                       */
/*                                                                           */
/*    Input(s)            : pFrame- Packet received on MPLS Interface        */
/*                          u4VplsIndex - VPLS Interface Identifier(MPLS Port)*/
/*                          processing the packet                            */
/*                          pVlanEntry - VLAN Curr Entry                     */
/*                          pInVlanTag - Contains pkt tag information        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanL2VpnFwdLearntPktOnMplsPort (tCRU_BUF_CHAIN_DESC * pFrame,
                                 tVlanIfMsg * pVlanIf,
                                 tVlanCurrEntry * pVlanEntry,
                                 tVlanTag * pInVlanTag, UINT4 u4PwVcIndex)
{
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
#ifndef HVPLS_WANTED
    if (VLAN_IS_PKT_RCVD_FROM_MPLS (pVlanIf) == VLAN_TRUE)
    {
        /*If HVPLS Flag is not enable then There is no support to switch the Pkt received from MPLS interface
         * back to same MPLS Interface */
        return;
    }
#endif
    VlanCfaGetIfInfo (pVlanIf->u4IfIndex, &IfInfo);
    if ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
        (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))
    {
        /* Incoming interface is visible as Attachment circuit interface
         * to the CFA So this packet should follow normal VLAN forwarding
         * logic should not send it to MPLS. */
        return;
    }

    /* Since NO MPLS Port is created in VLAN Module, Priority and DE Bit 
     * are set to default in the VLAN Tag added to the packet*/
    if (pInVlanTag->OuterVlanTag.u1TagType == VLAN_UNTAGGED)
    {
        VlanAddTagToFrame (pFrame, pVlanEntry->VlanId, VLAN_DEF_USER_PRIORITY,
                           VLAN_DE_FALSE, NULL);
    }
    else if (pInVlanTag->OuterVlanTag.u1TagType == VLAN_PRIORITY_TAGGED)
    {
        VlanUpdateVlanTag (pFrame, pVlanEntry->VlanId, VLAN_DEF_USER_PRIORITY,
                           VLAN_DE_FALSE, NULL);
    }
    /*pVlanTag->OuterVlanTag.u1TagType == VLAN_TAGGED --Already Tagged Packet
     * No need to update that Packet*/

    pDupBuf = VlanDupBuf (pFrame);

    if (pDupBuf == NULL)
    {
        return;
    }

    VlanMplsProcessL2Pkt (pDupBuf, VLAN_GET_IFINDEX (pVlanIf->u2LocalPort),
                          u4PwVcIndex, CFA_LINK_UCAST);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnIsMplsIfPresent                         */
/*                                                                           */
/*    Description         : This function is called from VLAN module to      */
/*                          decide whether the configuration of ELINE service*/
/*                          type is valid for the VLAN OR whether the VLAN   */
/*                          can be deleted in the system                     */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan Identifier                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE/VLAN_FALSE                             */
/*                                                                           */
/*****************************************************************************/

INT4
VlanL2VpnIsMplsIfPresent (tVlanId VlanId)
{
    tVlanL2VpnMap      *pVlanL2VpnInfo = NULL;
    tVlanL2VpnMap       TempVlanL2VpnInfo;

    VLAN_MEMSET (&TempVlanL2VpnInfo, VLAN_INIT_VAL, sizeof (tVlanL2VpnMap));

    TempVlanL2VpnInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    TempVlanL2VpnInfo.VlanId = VlanId;

    pVlanL2VpnInfo = RBTreeGet (gpVlanL2VpnPortVlanMapTbl, &TempVlanL2VpnInfo);

    if (pVlanL2VpnInfo == NULL)
    {
        /*E-line can be configured for this VLAN */
        /* VLAN can be deleted */
        return VLAN_TRUE;
    }

    /* Entry Exists in VLAN Map Table, So E-line cannot be configured for this
     * VLAN */
    /* VLAN cannot be deleted */
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnGetVplsIndex                            */
/*                                                                           */
/*    Description         : This function is called from VLAN module to      */
/*                          get the VPLS Index for the VLAN ID               */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context ID                         */
/*                          u4VlanId    - Vlan Identifier                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_ZERO/Valid VPLS Index                       */
/*                                                                           */
/*****************************************************************************/

UINT4
VlanL2VpnGetVplsIndex (UINT4 u4ContextId, UINT4 u4VlanId)
{
    tVlanL2VpnMap      *pVlanL2VpnInfo = NULL;
    tVlanL2VpnMap       TempVlanL2VpnInfo;

    VLAN_MEMSET (&TempVlanL2VpnInfo, VLAN_INIT_VAL, sizeof (tVlanL2VpnMap));

    TempVlanL2VpnInfo.u4ContextId = u4ContextId;
    TempVlanL2VpnInfo.VlanId = (tVlanId) u4VlanId;

    pVlanL2VpnInfo = RBTreeGet (gpVlanL2VpnPortVlanMapTbl, &TempVlanL2VpnInfo);

    if (pVlanL2VpnInfo == NULL)
    {
        return VLAN_ZERO;
    }

    return pVlanL2VpnInfo->u4VplsIndex;
}

#ifdef MPLS_WANTED
#ifndef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanVplsDelPwFdbEntries                          */
/*                                                                           */
/*    Description         : This function flushes the FDB entries in FDB Tbl */
/*                          learned on this PW.                              */
/*                          This function will be called                     */
/*                          1) If the pseudowire oper status made down       */
/*                          2) If the PW is deleted                          */
/*                                                                           */
/*    Input(s)            : u4PwVcIndex - PW index                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanVplsDelPwFdbEntries (UINT4 u4PwVcIndex)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    UINT4               u4FidIndex = 0;
    UINT2               u2HashIndex = 0;

    VLAN_LOCK ();
    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }
    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        VlanReleaseContext ();
        VLAN_UNLOCK ();
        return;
    }

    /* Irrespective of the learned entries in the different 
     * FDB table, flushing is done based on the port applicable
     * for all learning types
     */
    for (u4FidIndex = 0; u4FidIndex < VLAN_MAX_FID_ENTRIES; u4FidIndex++)
    {
        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry == NULL)
        {
            continue;
        }

        if (pFidEntry->u4Count == 0)
        {
            continue;
        }

        for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
        {
            pFdbEntry = VLAN_GET_FDB_ENTRY (u4FidIndex, u2HashIndex);

            while (pFdbEntry != NULL)
            {
                pNextFdbEntry = pFdbEntry->pNextHashNode;

                /* Delete the FDB entry only for port
                 * where it is learned
                 */
                if (pFdbEntry->u4PwVcIndex == u4PwVcIndex)
                {
                    VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
                }

                pFdbEntry = pNextFdbEntry;
            }

        }
    }
    VlanReleaseContext ();
    VLAN_UNLOCK ();
}

#endif
#ifdef HVPLS_WANTED
VOID
VlanCheckAndRaiseVplsAlarm (tVlanFdbEntry * pFdbEntry)
{
    UINT4               u4VplsFDBCounter = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4VplsFDBLowThreshold = 0;

    u4VplsFDBCounter = L2VpnVlanDecFDBCounter (pFdbEntry->u4PwVcIndex);
    L2VpnVlanGetFDBLowThreshold (pFdbEntry->u4PwVcIndex,
                                 &u4VplsFDBLowThreshold);
    L2VpnVlanGetVpnIdFromPwIndex (pFdbEntry->u4PwVcIndex, &u4VpnId);
    if (u4VpnId == 0)
    {
        return;
    }
    if (u4VplsFDBCounter < u4VplsFDBLowThreshold)
    {
        if (L2VpnSendVplsFDBAlarmCleared (u4VpnId) == L2VPN_FAILURE)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Sending Trap message failed\n");

        }
    }

}
#endif
#endif /* MPLS_WANTED */

#endif /*__VLNL2VPN_C*/
