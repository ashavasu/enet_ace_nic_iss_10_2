/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: std1evwr.c,v 1.3 2016/07/16 11:15:04 siva Exp $
* Description: This header file contains all prototype of the wrapper
*              routines in VLAN Modules.
****************************************************************************/

# include  "lr.h" 
# include  "fssnmp.h" 
# include  "vlaninc.h" 
# include  "std1evdb.h"


VOID RegisterSTD1EV ()
{
	SNMPRegisterMibWithLock (&std1evOID, &std1evEntry, VlanLock, VlanUnLock,
                             SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&std1evOID, (const UINT1 *) "std1evb");
}



VOID UnRegisterSTD1EV ()
{
	SNMPUnRegisterMib (&std1evOID, &std1evEntry);
	SNMPDelSysorEntry (&std1evOID, (const UINT1 *) "std1evb");
}

INT4 Ieee8021BridgeEvbSysTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysType(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysNumExternalPortsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysNumExternalPorts(&(pMultiData->u4_ULongValue)));
}
INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysEvbLldpTxEnable(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysEvbLldpManualGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysEvbLldpManual(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysEvbLldpGidCapable(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysEcpAckTimerGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysEcpAckTimer(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysEcpMaxRetriesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysEcpMaxRetries(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive(&(pMultiData->i4_SLongValue)));
}
INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysEvbLldpTxEnable(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEvbLldpManualSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysEvbLldpManual(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysEvbLldpGidCapable(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEcpAckTimerSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysEcpAckTimer(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEcpMaxRetriesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysEcpMaxRetries(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelaySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive(pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysEvbLldpTxEnable(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEvbLldpManualTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysEvbLldpManual(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysEvbLldpGidCapable(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEcpAckTimerTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysEcpAckTimer(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEcpMaxRetriesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysEcpMaxRetries(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysEvbLldpTxEnable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Ieee8021BridgeEvbSysEvbLldpManualDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysEvbLldpManual(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysEvbLldpGidCapable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Ieee8021BridgeEvbSysEcpAckTimerDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysEcpAckTimer(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Ieee8021BridgeEvbSysEcpMaxRetriesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysEcpMaxRetries(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexIeee8021BridgeEvbSbpTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbSbpTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbSbpTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbSbpLldpManualGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbSbpLldpManual(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbSbpVdpOperRsrcWaitDelayGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbSbpVdpOperReinitKeepAliveGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbSbpVdpOperReinitKeepAlive(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbSbpVdpOperToutKeepAliveGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbSbpVdpOperToutKeepAlive(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbSbpLldpManualSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbSbpLldpManual(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbSbpLldpManualTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbSbpLldpManual(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbSbpTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbSbpTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexIeee8021BridgeEvbVSIDBTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbVSIDBTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbVSIDBTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].pOctetStrValue,
			pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbVSITimeSinceCreateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVSITimeSinceCreate(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbVsiVdpOperCmdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVsiVdpOperCmd(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVsiOperRevertGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVsiOperRevert(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVsiOperHardGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVsiOperHard(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVsiOperReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVsiOperReason(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 Ieee8021BridgeEvbVSIMgrIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVSIMgrID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 Ieee8021BridgeEvbVSITypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVSIType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVSITypeVersionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVSITypeVersion(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 Ieee8021BridgeEvbVSIMvFormatGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVSIMvFormat(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVSINumMACsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVSINumMACs(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVDPMachineStateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVDPMachineState(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbVDPCommandsSucceededGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVDPCommandsSucceeded(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbVDPCommandsFailedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVDPCommandsFailed(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbVDPCommandRevertsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVDPCommandReverts(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbVDPCounterDiscontinuityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbVDPCounterDiscontinuity(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexIeee8021BridgeEvbVSIDBMacTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbVSIDBMacTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pNextMultiIndex->pIndex[3].pOctetStrValue,
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			(tMacAddr *)pNextMultiIndex->pIndex[5].pOctetStrValue->pu1_OctetList,
			&(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbVSIDBMacTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].pOctetStrValue,
			pNextMultiIndex->pIndex[3].pOctetStrValue,
			pFirstMultiIndex->pIndex[4].u4_ULongValue,
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			*(tMacAddr *)pFirstMultiIndex->pIndex[5].pOctetStrValue->pu1_OctetList,
			(tMacAddr *)pNextMultiIndex->pIndex[5].pOctetStrValue->pu1_OctetList,
			pFirstMultiIndex->pIndex[6].u4_ULongValue,
			&(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
		pNextMultiIndex->pIndex[5].pOctetStrValue->i4_Length = 6;
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbVSIVlanIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBMacTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[5].pOctetStrValue->pu1_OctetList),
		pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	pMultiData->u4_ULongValue =pMultiIndex->pIndex[6].u4_ULongValue;

	return SNMP_SUCCESS;

}

INT4 GetNextIndexIeee8021BridgeEvbUAPConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbUAPConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbUAPConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbUAPComponentIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPComponentId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbUAPPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbUapConfigIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUapConfigIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPRoleGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchAdminCDCPRole(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCapGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchOperCDCPChanCapGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchOperCDCPChanCap(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchOperStateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPSchOperState(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbSchCdcpRemoteEnabledGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbSchCdcpRemoteEnabled(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbSchCdcpRemoteRoleGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbSchCdcpRemoteRole(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPConfigStorageTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPConfigStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPConfigRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbUAPConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPRoleSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCapSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbUAPConfigStorageTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPConfigStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPConfigRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbUAPConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPRoleTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPRole(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCapTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbUAPConfigStorageTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPConfigRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbUAPConfigRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbUAPConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbUAPConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexIeee8021BridgeEvbCAPConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbCAPConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbCAPConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbCAPComponentIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCAPComponentId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbCapConfigIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCapConfigIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbCAPPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCAPPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbCAPSChannelIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCAPSChannelID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbCAPRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbCAPRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbCAPRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbCAPRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbCAPRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbCAPRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbCAPConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbCAPConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexIeee8021BridgeEvbURPTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbURPTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbURPTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbURPIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbURPTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbURPIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbURPBindToISSPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbURPTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbURPBindToISSPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbURPLldpManualGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbURPTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbURPLldpManual(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Ieee8021BridgeEvbURPVdpOperRsrcWaitDelayGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbURPTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbURPVdpOperRsrcWaitDelay(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelayGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbURPTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbURPVdpOperRespWaitDelay(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAliveGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbURPTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbURPVdpOperReinitKeepAlive(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbURPIfIndexSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbURPIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbURPBindToISSPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbURPBindToISSPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbURPLldpManualSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbURPLldpManual(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelaySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbURPVdpOperRespWaitDelay(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAliveSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetIeee8021BridgeEvbURPVdpOperReinitKeepAlive(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbURPIfIndexTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbURPIfIndex(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbURPBindToISSPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbURPBindToISSPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbURPLldpManualTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbURPLldpManual(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelayTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbURPVdpOperRespWaitDelay(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAliveTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Ieee8021BridgeEvbURPVdpOperReinitKeepAlive(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 Ieee8021BridgeEvbURPTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Ieee8021BridgeEvbURPTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexIeee8021BridgeEvbEcpTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexIeee8021BridgeEvbEcpTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexIeee8021BridgeEvbEcpTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Ieee8021BridgeEvbEcpOperAckTimerInitGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbEcpOperAckTimerInit(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbEcpOperMaxRetriesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbEcpOperMaxRetries(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbEcpTxFrameCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbEcpTxFrameCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbEcpTxRetryCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbEcpTxRetryCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbEcpTxFailuresGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbEcpTxFailures(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Ieee8021BridgeEvbEcpRxFrameCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetIeee8021BridgeEvbEcpRxFrameCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}


