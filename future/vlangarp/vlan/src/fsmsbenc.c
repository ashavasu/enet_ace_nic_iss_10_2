
/* $Id: fsmsbenc.c,v 1.1 2015/12/30 13:35:04 siva Exp $
   ISS Wrapper module
   module ARICENTP-BRIDGE-MIB

*/
# include "lr.h"
# include "vlaninc.h"
# include "fssnmp.h"
# include "garp.h"
# include "fsmsbenc.h"

/********************************************************************
 * FUNCTION NcFsDot1dTpHCPortInFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpHCPortInFramesGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu8fsDot1dTpHCPortInFrames )
{

		INT1 i1RetVal;
		UNUSED_PARAM(pu8fsDot1dTpHCPortInFrames);
		tSNMP_COUNTER64_TYPE fsDot1dTpHCPortInFrames;
		MEMSET(&fsDot1dTpHCPortInFrames,0,sizeof(tSNMP_COUNTER64_TYPE));
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpHCPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpHCPortInFrames(
						i4fsDot1dTpPort,
						&fsDot1dTpHCPortInFrames );
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dTpHCPortInFramesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpHCPortOutFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpHCPortOutFramesGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu8fsDot1dTpHCPortOutFrames )
{

		INT1 i1RetVal;
		tSNMP_COUNTER64_TYPE fsDot1dTpHCPortOutFrames;
		MEMSET(&fsDot1dTpHCPortOutFrames,0,sizeof(tSNMP_COUNTER64_TYPE));
		UNUSED_PARAM(pu8fsDot1dTpHCPortOutFrames);
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpHCPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpHCPortOutFrames(
						i4fsDot1dTpPort,
						&fsDot1dTpHCPortOutFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpHCPortOutFramesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpHCPortInDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpHCPortInDiscardsGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu8fsDot1dTpHCPortInDiscards )
{

		INT1 i1RetVal;
		tSNMP_COUNTER64_TYPE fsDot1dTpHCPortInDiscards;
		UNUSED_PARAM(pu8fsDot1dTpHCPortInDiscards);
		MEMSET(&fsDot1dTpHCPortInDiscards,0,sizeof(tSNMP_COUNTER64_TYPE));
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpHCPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpHCPortInDiscards(
						i4fsDot1dTpPort,
						&fsDot1dTpHCPortInDiscards );
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dTpHCPortInDiscardsGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortInOverflowFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortInOverflowFramesGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu4fsDot1dTpPortInOverflowFrames )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortOverflowTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortInOverflowFrames(
						i4fsDot1dTpPort,
						pu4fsDot1dTpPortInOverflowFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortInOverflowFramesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortOutOverflowFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortOutOverflowFramesGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu4fsDot1dTpPortOutOverflowFrames )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortOverflowTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortOutOverflowFrames(
						i4fsDot1dTpPort,
						pu4fsDot1dTpPortOutOverflowFrames );
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortOutOverflowFramesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortInOverflowDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortInOverflowDiscardsGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu4fsDot1dTpPortInOverflowDiscards )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortOverflowTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortInOverflowDiscards(
						i4fsDot1dTpPort,
						pu4fsDot1dTpPortInOverflowDiscards );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortInOverflowDiscardsGet */

/********************************************************************
 * FUNCTION NcFsDot1dDeviceCapabilitiesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*Commented due to open issue of ncx_list_t (bits)
 * INT1 NcFsDot1dDeviceCapabilitiesGet (
 INT4 i4fsDot1dBridgeContextId,
 ncx_list_t *pncx_list_tfsDot1dDeviceCapabilities )
 {

 INT1 i1RetVal;

 if (nmhValidateIndexInstanceFsDot1dExtBaseTable(
 i4fsDot1dBridgeContextId) == SNMP_FAILURE)

 {
 return SNMP_FAILURE;
 }

 i1RetVal = nmhGetFsDot1dDeviceCapabilities(
 i4fsDot1dBridgeContextId,
 ncx_list_tfsDot1dDeviceCapabilities );

 return i1RetVal;


 } */
/* NcFsDot1dDeviceCapabilitiesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTrafficClassesEnabledSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTrafficClassesEnabledSet (
				INT4 i4fsDot1dBridgeContextId,
				INT4 i4fsDot1dTrafficClassesEnabled )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetFsDot1dTrafficClassesEnabled(
						i4fsDot1dBridgeContextId,
						i4fsDot1dTrafficClassesEnabled);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dTrafficClassesEnabledSet */

/********************************************************************
 * FUNCTION NcFsDot1dTrafficClassesEnabledTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTrafficClassesEnabledTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBridgeContextId,
				INT4 i4fsDot1dTrafficClassesEnabled )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2FsDot1dTrafficClassesEnabled(pu4Error,
						i4fsDot1dBridgeContextId,
						i4fsDot1dTrafficClassesEnabled);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTrafficClassesEnabledTest */

/********************************************************************
 * FUNCTION NcFsDot1dGmrpStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dGmrpStatusSet (
				INT4 i4fsDot1dBridgeContextId,
				INT4 i4fsDot1dGmrpStatus )
{

		INT1 i1RetVal;
		GARP_LOCK ();
		i1RetVal = nmhSetFsDot1dGmrpStatus(
						i4fsDot1dBridgeContextId,
						i4fsDot1dGmrpStatus);

		GARP_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dGmrpStatusSet */

/********************************************************************
 * FUNCTION NcFsDot1dGmrpStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dGmrpStatusTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBridgeContextId,
				INT4 i4fsDot1dGmrpStatus )
{

		INT1 i1RetVal;
		GARP_LOCK ();
		i1RetVal = nmhTestv2FsDot1dGmrpStatus(pu4Error,
						i4fsDot1dBridgeContextId,
						i4fsDot1dGmrpStatus);
		GARP_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dGmrpStatusTest */

/********************************************************************
 * FUNCTION NcFsDot1dRegenUserPrioritySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dRegenUserPrioritySet (
				INT4 i4fsDot1dBasePort,
				INT4 i4fsDot1dUserPriority,
				INT4 i4fsDot1dRegenUserPriority )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetFsDot1dRegenUserPriority(
						i4fsDot1dBasePort,
						i4fsDot1dUserPriority,
						i4fsDot1dRegenUserPriority);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dRegenUserPrioritySet */

/********************************************************************
 * FUNCTION NcFsDot1dRegenUserPriorityTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dRegenUserPriorityTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBasePort,
				INT4 i4fsDot1dUserPriority,
				INT4 i4fsDot1dRegenUserPriority )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2FsDot1dRegenUserPriority(pu4Error,
						i4fsDot1dBasePort,
						i4fsDot1dUserPriority,
						i4fsDot1dRegenUserPriority);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dRegenUserPriorityTest */

/********************************************************************
 * FUNCTION NcFsDot1dTrafficClassSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTrafficClassSet (
				INT4 i4fsDot1dBasePort,
				INT4 i4fsDot1dTrafficClassPriority,
				INT4 i4fsDot1dTrafficClass )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetFsDot1dTrafficClass(
						i4fsDot1dBasePort,
						i4fsDot1dTrafficClassPriority,
						i4fsDot1dTrafficClass);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dTrafficClassSet */

/********************************************************************
 * FUNCTION NcFsDot1dTrafficClassTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTrafficClassTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBasePort,
				INT4 i4fsDot1dTrafficClassPriority,
				INT4 i4fsDot1dTrafficClass )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2FsDot1dTrafficClass(pu4Error,
						i4fsDot1dBasePort,
						i4fsDot1dTrafficClassPriority,
						i4fsDot1dTrafficClass);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dTrafficClassTest */

/********************************************************************
 * FUNCTION NcFsDot1dPortOutboundAccessPriorityGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dPortOutboundAccessPriorityGet (
				INT4 i4fsDot1dBasePort,
				INT4 i4fsDot1dRegenUserPriority,
				INT4 *pi4fsDot1dPortOutboundAccessPriority )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dPortOutboundAccessPriorityTable(
								i4fsDot1dBasePort,
								i4fsDot1dRegenUserPriority) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dPortOutboundAccessPriority(
						i4fsDot1dBasePort,
						i4fsDot1dRegenUserPriority,
						pi4fsDot1dPortOutboundAccessPriority );
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dPortOutboundAccessPriorityGet */

/* END i_ARICENTP_BRIDGE_MIB.c */
