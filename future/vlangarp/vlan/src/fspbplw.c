/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbplw.c,v 1.70 2016/01/11 12:56:23 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"
# include  "fsmpbcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbMulticastMacLimit
 Input       :  The Indices

                The Object 
                retValFsPbMulticastMacLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbMulticastMacLimit (UINT4 *pu4RetValFsPbMulticastMacLimit)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4RetValFsPbMulticastMacLimit = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsPbMulticastMacLimit = VLAN_PB_MULTICAST_TABLE_LIMIT;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelStpAddress
 Input       :  The Indices

                The Object 
                retValFsPbTunnelStpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelStpAddress (tMacAddr * pRetValFsPbTunnelStpAddress)
{
    VLAN_MEMSET (pRetValFsPbTunnelStpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelLacpAddress
 Input       :  The Indices

                The Object 
                retValFsPbTunnelLacpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelLacpAddress (tMacAddr * pRetValFsPbTunnelLacpAddress)
{
    VLAN_MEMSET (pRetValFsPbTunnelLacpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelDot1xAddress
 Input       :  The Indices

                The Object 
                retValFsPbTunnelDot1xAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelDot1xAddress (tMacAddr * pRetValFsPbTunnelDot1xAddress)
{
    VLAN_MEMSET (pRetValFsPbTunnelDot1xAddress, 0, VLAN_MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelGvrpAddress
 Input       :  The Indices

                The Object 
                retValFsPbTunnelGvrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelGvrpAddress (tMacAddr * pRetValFsPbTunnelGvrpAddress)
{
    VLAN_MEMSET (pRetValFsPbTunnelGvrpAddress, 0, VLAN_MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelGmrpAddress
 Input       :  The Indices

                The Object 
                retValFsPbTunnelGmrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelGmrpAddress (tMacAddr * pRetValFsPbTunnelGmrpAddress)
{
    VLAN_MEMSET (pRetValFsPbTunnelGmrpAddress, 0, VLAN_MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbMulticastMacLimit
 Input       :  The Indices

                The Object 
                setValFsPbMulticastMacLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbMulticastMacLimit (UINT4 u4SetValFsPbMulticastMacLimit)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (VlanHwMulticastMacTableLimit (VLAN_CURR_CONTEXT_ID (),
                                      u4SetValFsPbMulticastMacLimit)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbMulticastMacLimit, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    VLAN_PB_MULTICAST_TABLE_LIMIT = u4SetValFsPbMulticastMacLimit;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", VLAN_CURR_CONTEXT_ID (),
                      u4SetValFsPbMulticastMacLimit));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelStpAddress
 Input       :  The Indices

                The Object 
                setValFsPbTunnelStpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelStpAddress (tMacAddr SetValFsPbTunnelStpAddress)
{
    UNUSED_PARAM (SetValFsPbTunnelStpAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelLacpAddress
 Input       :  The Indices

                The Object 
                setValFsPbTunnelLacpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelLacpAddress (tMacAddr SetValFsPbTunnelLacpAddress)
{
    UNUSED_PARAM (SetValFsPbTunnelLacpAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelDot1xAddress
 Input       :  The Indices

                The Object 
                setValFsPbTunnelDot1xAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelDot1xAddress (tMacAddr SetValFsPbTunnelDot1xAddress)
{
    UNUSED_PARAM (SetValFsPbTunnelDot1xAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelGvrpAddress
 Input       :  The Indices

                The Object 
                setValFsPbTunnelGvrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelGvrpAddress (tMacAddr SetValFsPbTunnelGvrpAddress)
{
    UNUSED_PARAM (SetValFsPbTunnelGvrpAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelGmrpAddress
 Input       :  The Indices

                The Object 
                setValFsPbTunnelGmrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelGmrpAddress (tMacAddr SetValFsPbTunnelGmrpAddress)
{
    UNUSED_PARAM (SetValFsPbTunnelGmrpAddress);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbMulticastMacLimit
 Input       :  The Indices

                The Object 
                testValFsPbMulticastMacLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbMulticastMacLimit (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsPbMulticastMacLimit)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (u4TestValFsPbMulticastMacLimit > VLAN_DEV_MAX_MCAST_TABLE_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_MULTICAST_MAC_MAX_ERR);
        return SNMP_FAILURE;

    }

    if (VLAN_PB_MULTICAST_TABLE_COUNT > u4TestValFsPbMulticastMacLimit)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_INVALID_LIMIT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelStpAddress
 Input       :  The Indices

                The Object 
                testValFsPbTunnelStpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelStpAddress (UINT4 *pu4ErrorCode,
                               tMacAddr TestValFsPbTunnelStpAddress)
{
    UNUSED_PARAM (TestValFsPbTunnelStpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelLacpAddress
 Input       :  The Indices

                The Object 
                testValFsPbTunnelLacpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelLacpAddress (UINT4 *pu4ErrorCode,
                                tMacAddr TestValFsPbTunnelLacpAddress)
{
    UNUSED_PARAM (TestValFsPbTunnelLacpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelDot1xAddress
 Input       :  The Indices

                The Object 
                testValFsPbTunnelDot1xAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelDot1xAddress (UINT4 *pu4ErrorCode,
                                 tMacAddr TestValFsPbTunnelDot1xAddress)
{
    UNUSED_PARAM (TestValFsPbTunnelDot1xAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelGvrpAddress
 Input       :  The Indices

                The Object 
                testValFsPbTunnelGvrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelGvrpAddress (UINT4 *pu4ErrorCode,
                                tMacAddr TestValFsPbTunnelGvrpAddress)
{
    UNUSED_PARAM (TestValFsPbTunnelGvrpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelGmrpAddress
 Input       :  The Indices

                The Object 
                testValFsPbTunnelGmrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelGmrpAddress (UINT4 *pu4ErrorCode,
                                tMacAddr TestValFsPbTunnelGmrpAddress)
{
    UNUSED_PARAM (TestValFsPbTunnelGmrpAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbMulticastMacLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbMulticastMacLimit (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbTunnelStpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbTunnelStpAddress (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbTunnelLacpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbTunnelLacpAddress (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbTunnelDot1xAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbTunnelDot1xAddress (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbTunnelGvrpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbTunnelGvrpAddress (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbTunnelGmrpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbTunnelGmrpAddress (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbPortInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbPortInfoTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbPortInfoTable (INT4 i4FsPbPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbPortInfoTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbPortInfoTable (INT4 *pi4FsPbPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsPbPortInfoTable (0, pi4FsPbPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbPortInfoTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbPortInfoTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4FsPbPort + 1); u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry == NULL)
        {
            continue;
        }

        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

        if (pVlanPbPortEntry != NULL)
        {
            *pi4NextFsPbPort = (INT4) u2Port;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbPortSVlanClassificationMethod
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortSVlanClassificationMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortSVlanClassificationMethod (INT4 i4FsPbPort,
                                         INT4
                                         *pi4RetValFsPbPortSVlanClassificationMethod)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortSVlanClassificationMethod =
        (INT4) pVlanPbPortEntry->u1SVlanTableType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortSVlanIngressEtherType
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortSVlanIngressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortSVlanIngressEtherType (INT4 i4FsPbPort,
                                     INT4
                                     *pi4RetValFsPbPortSVlanIngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortSVlanIngressEtherType =
        (INT4) pVlanPortEntry->u2IngressEtherType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortSVlanEgressEtherType
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortSVlanEgressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortSVlanEgressEtherType (INT4 i4FsPbPort,
                                    INT4 *pi4RetValFsPbPortSVlanEgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortSVlanEgressEtherType =
        (INT4) pVlanPortEntry->u2EgressEtherType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortSVlanEtherTypeSwapStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortSVlanEtherTypeSwapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortSVlanEtherTypeSwapStatus (INT4 i4FsPbPort,
                                        INT4
                                        *pi4RetValFsPbPortSVlanEtherTypeSwapStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortSVlanEtherTypeSwapStatus =
        (INT4) (pVlanPbPortEntry->u1EtherTypeSwapStatus);
    /* (INT4) VLAN_PB_PORT_ETHER_TYPE_SWAP_STATUS (i4FsPbPort); */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortSVlanTranslationStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortSVlanTranslationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortSVlanTranslationStatus (INT4 i4FsPbPort,
                                      INT4
                                      *pi4RetValFsPbPortSVlanTranslationStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1TranslationStatus = OSIX_DISABLED;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfPbGetVidTransStatus
        (VLAN_CURR_CONTEXT_ID (), (UINT2) i4FsPbPort,
         &u1TranslationStatus) == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u1TranslationStatus == OSIX_ENABLED)
    {
        *pi4RetValFsPbPortSVlanTranslationStatus = VLAN_ENABLED;
    }
    else
    {
        *pi4RetValFsPbPortSVlanTranslationStatus = VLAN_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortUnicastMacLearning
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortUnicastMacLearning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortUnicastMacLearning (INT4 i4FsPbPort,
                                  INT4 *pi4RetValFsPbPortUnicastMacLearning)
{

    return (nmhGetDot1qFutureVlanPortUnicastMacLearning
            (i4FsPbPort, pi4RetValFsPbPortUnicastMacLearning));
}

/****************************************************************************
 Function    :  nmhGetFsPbPortUnicastMacLimit
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortUnicastMacLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortUnicastMacLimit (INT4 i4FsPbPort,
                               UINT4 *pu4RetValFsPbPortUnicastMacLimit)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsPbPortUnicastMacLimit =
             pVlanPbPortEntry->u4MacLearningLimit;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortBundleStatus
 Input       :  The Indices
                FsPbPort

                The Object
                retValFsPbPortBundleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortBundleStatus (INT4 i4FsPbPort,
                            INT4 *pi4RetValFsPbPortBundleStatus)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortBundleStatus = (INT4) pVlanPbPortEntry->u1BundleStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortMultiplexStatus
 Input       :  The Indices
                FsPbPort

                The Object
                retValFsPbPortMultiplexStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortMultiplexStatus (INT4 i4FsPbPort,
                               INT4 *pi4RetValFsPbPortMultiplexStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortMultiplexStatus =
        (INT4) pVlanPbPortEntry->u1MultiplexStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbPortSVlanClassificationMethod
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortSVlanClassificationMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortSVlanClassificationMethod (INT4 i4FsPbPort,
                                         INT4
                                         i4SetValFsPbPortSVlanClassificationMethod)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1SVlanTableType !=
        (UINT1) i4SetValFsPbPortSVlanClassificationMethod)
    {
        if (VlanHwSetPortSVlanClassifyMethod (VLAN_CURR_CONTEXT_ID (),
                                              VLAN_GET_IFINDEX
                                              (i4FsPbPort),
                                              (UINT1)
                                              i4SetValFsPbPortSVlanClassificationMethod)
            == VLAN_SUCCESS)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIPbPortSVlanClassificationMethod, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

            pVlanPbPortEntry->u1SVlanTableType
                = (UINT1) i4SetValFsPbPortSVlanClassificationMethod;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4SetValFsPbPortSVlanClassificationMethod));
            VlanSelectContext (u4CurrContextId);
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortSVlanIngressEtherType
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortSVlanIngressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortSVlanIngressEtherType (INT4 i4FsPbPort,
                                     INT4 i4SetValFsPbPortSVlanIngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = VLAN_GET_IFINDEX (i4FsPbPort);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVlanPortEntry->u2IngressEtherType !=
        (UINT2) i4SetValFsPbPortSVlanIngressEtherType)
    {
        if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                           VLAN_GET_IFINDEX (i4FsPbPort),
                                           (UINT2)
                                           i4SetValFsPbPortSVlanIngressEtherType)
            != VLAN_SUCCESS)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIPbPortSVlanIngressEtherType, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4SetValFsPbPortSVlanIngressEtherType));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u2IngressEtherType
            = (UINT2) i4SetValFsPbPortSVlanIngressEtherType;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortSVlanIngressEtherType,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (i4FsPbPort),
                      i4SetValFsPbPortSVlanIngressEtherType));

    if (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_TRUE)
    {
        /* Before calling this function, Context should not be selected */
        VlanSispCopyIngressEtherTypeToSispPorts (u4IfIndex,
                                                 (UINT2)
                                                 i4SetValFsPbPortSVlanIngressEtherType);
    }

    VlanSelectContext (u4CurrContextId);
    /* Notify ECFM about the change in Ingress Ether Type */
    VlanEcfmEtherTypeChngInd (VLAN_CURR_CONTEXT_ID (),
                              pVlanPortEntry->u4IfIndex,
                              ECFM_VLAN_INGRESS_ETHER_TYPE,
                              i4SetValFsPbPortSVlanIngressEtherType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortSVlanEgressEtherType
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortSVlanEgressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortSVlanEgressEtherType (INT4 i4FsPbPort,
                                    INT4 i4SetValFsPbPortSVlanEgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = VLAN_GET_IFINDEX (i4FsPbPort);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVlanPortEntry->u2EgressEtherType !=
        (UINT2) i4SetValFsPbPortSVlanEgressEtherType)
    {
        if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                          u4IfIndex,
                                          (UINT2)
                                          i4SetValFsPbPortSVlanEgressEtherType)
            != VLAN_SUCCESS)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIPbPortSVlanEgressEtherType, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex,
                              i4SetValFsPbPortSVlanEgressEtherType));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }

        pVlanPortEntry->u2EgressEtherType
            = (UINT2) i4SetValFsPbPortSVlanEgressEtherType;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortSVlanEgressEtherType,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex,
                      i4SetValFsPbPortSVlanEgressEtherType));

    if (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_TRUE)
    {
        /* Before calling this function, Context should not be selected */
        VlanSispCopyEgressEtherTypeToSispPorts (u4IfIndex,
                                                (UINT2)
                                                i4SetValFsPbPortSVlanEgressEtherType);
    }

    VlanSelectContext (u4CurrContextId);
    /* Notify ECFM about the change in Egress Ether Type */
    VlanEcfmEtherTypeChngInd (VLAN_CURR_CONTEXT_ID (),
                              pVlanPortEntry->u4IfIndex,
                              ECFM_VLAN_EGRESS_ETHER_TYPE,
                              i4SetValFsPbPortSVlanEgressEtherType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortSVlanEtherTypeSwapStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortSVlanEtherTypeSwapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortSVlanEtherTypeSwapStatus (INT4 i4FsPbPort,
                                        INT4
                                        i4SetValFsPbPortSVlanEtherTypeSwapStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    /* if (VLAN_PB_PORT_ETHER_TYPE_SWAP_STATUS (i4FsPbPort) != */
    if (pVlanPbPortEntry->u1EtherTypeSwapStatus !=
        (UINT1) i4SetValFsPbPortSVlanEtherTypeSwapStatus)
    {

        if (VlanHwSetPortEtherTypeSwapStatus (VLAN_CURR_CONTEXT_ID (),
                                              VLAN_GET_IFINDEX
                                              (i4FsPbPort),
                                              (UINT1)
                                              i4SetValFsPbPortSVlanEtherTypeSwapStatus)
            != VLAN_SUCCESS)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIPbPortSVlanEtherTypeSwapStatus, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4SetValFsPbPortSVlanEtherTypeSwapStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }

        /*VLAN_PB_PORT_ETHER_TYPE_SWAP_STATUS (i4FsPbPort) */
        pVlanPbPortEntry->u1EtherTypeSwapStatus
            = (UINT1) i4SetValFsPbPortSVlanEtherTypeSwapStatus;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortSVlanEtherTypeSwapStatus,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (i4FsPbPort),
                      i4SetValFsPbPortSVlanEtherTypeSwapStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortSVlanTranslationStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortSVlanTranslationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortSVlanTranslationStatus (INT4 i4FsPbPort,
                                      INT4
                                      i4SetValFsPbPortSVlanTranslationStatus)
{
    UINT1               u1VidTransStatus;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsPbPortSVlanTranslationStatus == VLAN_ENABLED)
    {
        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
            (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE))
        {
            if (VLAN_IS_VID_TRANS_VALID_ONPORT ((UINT2) i4FsPbPort) == VLAN_FALSE)
            {
                return SNMP_FAILURE;
            }
        }

        if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
        {
            if ((VLAN_PB_PORT_TYPE ((UINT2) i4FsPbPort) ==
                 VLAN_PROVIDER_INSTANCE_PORT) ||
                (VLAN_PB_PORT_TYPE ((UINT2) i4FsPbPort) == VLAN_CUSTOMER_BACKBONE_PORT))

            {
                return SNMP_FAILURE;
            }
        }
    }

    if (VlanL2IwfPbGetVidTransStatus (VLAN_CURR_CONTEXT_ID (),
                                      (UINT2) i4FsPbPort, &u1VidTransStatus)
        == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (((u1VidTransStatus == OSIX_ENABLED) &&
         (i4SetValFsPbPortSVlanTranslationStatus == VLAN_ENABLED)) ||
        ((u1VidTransStatus == OSIX_DISABLED) &&
         (i4SetValFsPbPortSVlanTranslationStatus == VLAN_DISABLED)))
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFsPbPortSVlanTranslationStatus == VLAN_ENABLED)
    {
        if (VlanL2IwfPbSetVidTransStatus
            (VLAN_CURR_CONTEXT_ID (), (UINT2) i4FsPbPort,
             OSIX_ENABLED) == L2IWF_FAILURE)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIPbPortSVlanTranslationStatus, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4SetValFsPbPortSVlanTranslationStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (VlanL2IwfPbSetVidTransStatus
            (VLAN_CURR_CONTEXT_ID (), (UINT2) i4FsPbPort,
             OSIX_DISABLED) == L2IWF_FAILURE)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  FsMIPbPortSVlanTranslationStatus, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4SetValFsPbPortSVlanTranslationStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }
    }

    if (VlanHwSetPortSVlanTranslationStatus (VLAN_CURR_CONTEXT_ID (),
                                             VLAN_GET_PHY_PORT (i4FsPbPort),
                                             (UINT1)
                                             i4SetValFsPbPortSVlanTranslationStatus)
        != VLAN_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortSVlanTranslationStatus,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsPbPort),
                          i4SetValFsPbPortSVlanTranslationStatus));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortSVlanTranslationStatus,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (i4FsPbPort),
                      i4SetValFsPbPortSVlanTranslationStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortUnicastMacLearning
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortUnicastMacLearning
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortUnicastMacLearning (INT4 i4FsPbPort,
                                  INT4 i4SetValFsPbPortUnicastMacLearning)
{

    return (nmhSetDot1qFutureVlanPortUnicastMacLearning
            (i4FsPbPort, i4SetValFsPbPortUnicastMacLearning));
}

/****************************************************************************
 Function    :  nmhSetFsPbPortUnicastMacLimit
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortUnicastMacLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortUnicastMacLimit (INT4 i4FsPbPort,
                               UINT4 u4SetValFsPbPortUnicastMacLimit)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u4MacLearningLimit != u4SetValFsPbPortUnicastMacLimit)
    {
        if (VlanHwPortMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                        VLAN_GET_IFINDEX
                                        (i4FsPbPort),
                                        (UINT4)
                                        u4SetValFsPbPortUnicastMacLimit) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (VlanHwFlushPort (VLAN_CURR_CONTEXT_ID (), i4FsPbPort,
                             VLAN_OPTIMIZE) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#else
        VlanHandleDeleteFdbEntries ((UINT2) i4FsPbPort);
#endif
        RM_GET_SEQ_NUM (&u4SeqNum);

        pVlanPbPortEntry->u4MacLearningLimit = u4SetValFsPbPortUnicastMacLimit;

        pVlanPbPortEntry->u4NumLearntMacEntries = 0;

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortUnicastMacLimit,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                          VLAN_GET_IFINDEX (i4FsPbPort),
                          u4SetValFsPbPortUnicastMacLimit));
        VlanSelectContext (u4CurrContextId);

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortBundleStatus
 Input       :  The Indices
                FsPbPort

                The Object
                setValFsPbPortBundleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortBundleStatus (INT4 i4FsPbPort, INT4 i4SetValFsPbPortBundleStatus)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1BundleStatus !=
        (UINT1) i4SetValFsPbPortBundleStatus)
    {

        RM_GET_SEQ_NUM (&u4SeqNum);

        pVlanPbPortEntry->u1BundleStatus = (UINT1) i4SetValFsPbPortBundleStatus;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortBundleStatus,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsPbPort),
                          i4SetValFsPbPortBundleStatus));
        VlanSelectContext (u4CurrContextId);

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortMultiplexStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortMultiplexStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortMultiplexStatus (INT4 i4FsPbPort,
                               INT4 i4SetValFsPbPortMultiplexStatus)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1MultiplexStatus !=
        (UINT1) i4SetValFsPbPortMultiplexStatus)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pVlanPbPortEntry->u1MultiplexStatus
            = (UINT1) i4SetValFsPbPortMultiplexStatus;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortMultiplexStatus,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsPbPort),
                          i4SetValFsPbPortMultiplexStatus));
        VlanSelectContext (u4CurrContextId);

    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbPortSVlanClassificationMethod
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortSVlanClassificationMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortSVlanClassificationMethod (UINT4 *pu4ErrorCode,
                                            INT4 i4FsPbPort,
                                            INT4
                                            i4TestValFsPbPortSVlanClassificationMethod)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbPortSVlanClassificationMethod <
         VLAN_SVLAN_PORT_SRCMAC_TYPE)
        || (i4TestValFsPbPortSVlanClassificationMethod >
            VLAN_SVLAN_PORT_PVID_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* The CVID registration table can be configured only for 
     * Customer Edge Port
     * */
    if (i4TestValFsPbPortSVlanClassificationMethod ==
        VLAN_SVLAN_PORT_CVLAN_TYPE &&
        pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_PB_CONF_NOT_APPLICABLE_ERR);
        return SNMP_FAILURE;
    }

    /* For CEP only C-VID registration table can be configured  */
    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) &&
        (i4TestValFsPbPortSVlanClassificationMethod !=
         VLAN_SVLAN_PORT_CVLAN_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_PB_CONF_NOT_APPLICABLE_ERR);
        return SNMP_FAILURE;
    }

    /* On PNP/PPNP/CNP only pvid type can be set as the S-Vlan classification
     * method. */
    if (((pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT) ||
         (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT) ||
         (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
         (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT)) &&
        (i4TestValFsPbPortSVlanClassificationMethod !=
         VLAN_SVLAN_PORT_PVID_TYPE))
    {
        CLI_SET_ERR (CLI_PB_CONF_NOT_APPLICABLE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortSVlanIngressEtherType
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortSVlanIngressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortSVlanIngressEtherType (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                        INT4
                                        i4TestValFsPbPortSVlanIngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ethertype configurations are not allowed on SISP ports */
    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4FsPbPort)) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_ETHER_TYPE_VALID (i4TestValFsPbPortSVlanIngressEtherType) !=
        VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortSVlanEgressEtherType
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortSVlanEgressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortSVlanEgressEtherType (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                       INT4
                                       i4TestValFsPbPortSVlanEgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ethertype configurations are not allowed on SISP ports */
    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4FsPbPort)) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_ETHER_TYPE_VALID (i4TestValFsPbPortSVlanEgressEtherType) !=
        VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortSVlanEtherTypeSwapStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortSVlanEtherTypeSwapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortSVlanEtherTypeSwapStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                           INT4
                                           i4TestValFsPbPortSVlanEtherTypeSwapStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbPortSVlanEtherTypeSwapStatus != VLAN_ENABLED) &&
        (i4TestValFsPbPortSVlanEtherTypeSwapStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_PORT_TYPE ((UINT2) i4FsPbPort) == VLAN_CUSTOMER_EDGE_PORT)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4FsPbPort)) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortSVlanTranslationStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortSVlanTranslationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortSVlanTranslationStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                         INT4
                                         i4TestValFsPbPortSVlanTranslationStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbPortSVlanTranslationStatus != VLAN_ENABLED) &&
        (i4TestValFsPbPortSVlanTranslationStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbPortSVlanTranslationStatus == VLAN_ENABLED)
    {
        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
            (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE))
        {
            if (VLAN_IS_VID_TRANS_VALID_ONPORT ((UINT2) i4FsPbPort) == VLAN_FALSE)
            {
                CLI_SET_ERR (CLI_PB_SVLAN_TRANS_CNP_PNP_PCNP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
        {
            if ((VLAN_PB_PORT_TYPE ((UINT2) i4FsPbPort) ==
                 VLAN_PROVIDER_INSTANCE_PORT) ||
                (VLAN_PB_PORT_TYPE ((UINT2) i4FsPbPort) == VLAN_CUSTOMER_BACKBONE_PORT))
            {
                CLI_SET_ERR (CLI_PB_SVLAN_TRANS_PIP_CBP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortUnicastMacLearning
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortUnicastMacLearning
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortUnicastMacLearning (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                     INT4 i4TestValFsPbPortUnicastMacLearning)
{

    return (nmhTestv2Dot1qFutureVlanPortUnicastMacLearning
            (pu4ErrorCode, i4FsPbPort, i4TestValFsPbPortUnicastMacLearning));
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortUnicastMacLimit
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortUnicastMacLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortUnicastMacLimit (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                  UINT4 u4TestValFsPbPortUnicastMacLimit)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (u4TestValFsPbPortUnicastMacLimit > VLAN_PB_DYNAMIC_UNICAST_SIZE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_LIMIT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortBundleStatus
 Input       :  The Indices
                FsPbPort

                The Object
                testValFsPbPortBundleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortBundleStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                               INT4 i4TestValFsPbPortBundleStatus)
{

    INT4                i4NextFsPbPort = VLAN_ZERO;
    UINT4               u4FirstSvid = VLAN_NULL_VLAN_ID;
    UINT4               u4NextSVlanId = VLAN_ZERO;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanPbLogicalNextPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (i4FsPbPort);
    if (pVlanPbPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pVlanPbPortEntry->u1BundleStatus == i4TestValFsPbPortBundleStatus)
    {
        return SNMP_SUCCESS;
    }

    if ((i4TestValFsPbPortBundleStatus != VLAN_ENABLED) &&
        (i4TestValFsPbPortBundleStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT))
    {
        CLI_SET_ERR (CLI_PB_BUNDLE_MULTIPLEX_CEP_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    do
    {
        pVlanPbLogicalNextPortEntry =
            VlanPbGetNextLogicalPortEntry (i4FsPbPort, u4FirstSvid,
                                           &i4NextFsPbPort, &u4NextSVlanId);

        if ((pVlanPbLogicalNextPortEntry != NULL)
            && (i4FsPbPort == i4NextFsPbPort))
        {
            u4FirstSvid = u4NextSVlanId;

            pVlanPbLogicalPortEntry =
                VlanPbGetLogicalPortEntry (i4FsPbPort, u4NextSVlanId);

            if (pVlanPbLogicalPortEntry != NULL)
            {
                if (pVlanPbLogicalPortEntry->u2NoOfCvidEntries > 1)
                {
                    CLI_SET_ERR (CLI_PB_BUNDLE_STATUS_CONFIG_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
        else
        {
            i4NextFsPbPort = VLAN_ZERO;
        }
    }
    while (i4FsPbPort == i4NextFsPbPort);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortMultiplexStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortMultiplexStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortMultiplexStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                  INT4 i4TestValFsPbPortMultiplexStatus)
{

    UINT4               u4NextSvid = VLAN_ZERO;
    UINT4               u4FirstSvid = VLAN_NULL_VLAN_ID;
    INT4                i4NextPort = VLAN_ZERO;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (i4FsPbPort);
    if (pVlanPbPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pVlanPbPortEntry->u1MultiplexStatus == i4TestValFsPbPortMultiplexStatus)
    {
        return SNMP_SUCCESS;
    }

    if ((i4TestValFsPbPortMultiplexStatus != VLAN_ENABLED) &&
        (i4TestValFsPbPortMultiplexStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT))

    {
        CLI_SET_ERR (CLI_PB_BUNDLE_MULTIPLEX_CEP_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry (i4FsPbPort,
                                                             VLAN_NULL_VLAN_ID,
                                                             &i4NextPort,
                                                             &u4FirstSvid);

    if ((pVlanPbLogicalPortEntry != NULL) && (i4NextPort == i4FsPbPort))

    {
        pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry
            (i4FsPbPort, (tVlanId) u4FirstSvid, &i4NextPort, &u4NextSvid);

        if ((pVlanPbLogicalPortEntry != NULL) && (i4NextPort == i4FsPbPort))
        {
            CLI_SET_ERR (CLI_PB_MULTIPLEX_STATUS_CONFIG_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbPortInfoTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbPortInfoTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbSrcMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsPbSrcMacSVlanTable (INT4 i4FsPbPort,
                                              tMacAddr FsPbSrcMacAddress)
{
    UINT2               u2Port;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbSrcMacAddress, VLAN_MAC_ADDR_LEN)))
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_BCASTADDR (FsPbSrcMacAddress) == VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (FsPbSrcMacAddress) == VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbSrcMacSVlanTable (INT4 *pi4FsPbPort,
                                      tMacAddr * pFsPbSrcMacAddress)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    i4Result = VlanPbGetFirstSrcMacSVlanEntry (u4TblIndex,
                                               pi4FsPbPort,
                                               *pFsPbSrcMacAddress);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbSrcMacAddress
                nextFsPbSrcMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbSrcMacSVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                     tMacAddr FsPbSrcMacAddress,
                                     tMacAddr * pNextFsPbSrcMacAddress)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    i4Result = VlanPbGetNextSrcMacSVlanEntry (u4TblIndex,
                                              i4FsPbPort,
                                              FsPbSrcMacAddress,
                                              pi4NextFsPbPort,
                                              *pNextFsPbSrcMacAddress);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbSrcMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress

                The Object 
                retValFsPbSrcMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSrcMacSVlan (INT4 i4FsPbPort, tMacAddr FsPbSrcMacAddress,
                       INT4 *pi4RetValFsPbSrcMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    pSrcMacSVlanEntry = VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       FsPbSrcMacAddress);

    if (pSrcMacSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbSrcMacSVlan = (UINT4) pSrcMacSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbSrcMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress

                The Object 
                retValFsPbSrcMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSrcMacRowStatus (INT4 i4FsPbPort, tMacAddr FsPbSrcMacAddress,
                           INT4 *pi4RetValFsPbSrcMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    pSrcMacSVlanEntry = VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       FsPbSrcMacAddress);

    if (pSrcMacSVlanEntry != NULL)
    {
        *pi4RetValFsPbSrcMacRowStatus = (INT4) pSrcMacSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbSrcMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress

                The Object 
                setValFsPbSrcMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSrcMacSVlan (INT4 i4FsPbPort, tMacAddr FsPbSrcMacAddress,
                       INT4 i4SetValFsPbSrcMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    pSrcMacSVlanEntry = VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       FsPbSrcMacAddress);

    if (NULL == pSrcMacSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), FsPbSrcMacAddress,
                          i4SetValFsPbSrcMacSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pSrcMacSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbSrcMacSVlan;

    if (pSrcMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {

        pSrcMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), FsPbSrcMacAddress,
                      i4SetValFsPbSrcMacSVlan));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbSrcMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress

                The Object 
                setValFsPbSrcMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSrcMacRowStatus (INT4 i4FsPbPort, tMacAddr FsPbSrcMacAddress,
                           INT4 i4SetValFsPbSrcMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanSVlanMap       VlanSVlanMap;
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    pSrcMacSVlanEntry =
        VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort, FsPbSrcMacAddress);

    if (pSrcMacSVlanEntry != NULL)
    {
        if (pSrcMacSVlanEntry->u1RowStatus == i4SetValFsPbSrcMacRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbSrcMacRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pSrcMacSVlanEntry =
                (tVlanSrcMacSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_SRCMAC_TYPE);

            if (pSrcMacSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, Src MAC based S-VLAN"
                          "Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));

                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pSrcMacSVlanEntry, 0, sizeof (tVlanSrcMacSVlanEntry));

            pSrcMacSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pSrcMacSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            VLAN_CPY_MAC_ADDR (pSrcMacSVlanEntry->SrcMac, FsPbSrcMacAddress);
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCMAC_TYPE,
                                       (UINT1 *) pSrcMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd
                (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
                 (tRBElem *) pSrcMacSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCMAC_TYPE,
                                       (UINT1 *) pSrcMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcMacSVlanEntry == NULL) ||
                (pSrcMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcMacSVlanEntry->u2Port));

            VlanSVlanMap.SVlanId = pSrcMacSVlanEntry->SVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.SrcMac, pSrcMacSVlanEntry->SrcMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pSrcMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcMacSVlanEntry == NULL) ||
                (pSrcMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcMacSVlanEntry->SVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.SrcMac, pSrcMacSVlanEntry->SrcMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pSrcMacSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcMacSVlanEntry == NULL) ||
                (pSrcMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcMacSVlanEntry->SVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.SrcMac, pSrcMacSVlanEntry->SrcMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pSrcMacSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbSrcMacAddress,
                                  i4SetValFsPbSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCMAC_TYPE,
                                   (UINT1 *) pSrcMacSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                              VLAN_GET_IFINDEX (i4FsPbPort), FsPbSrcMacAddress,
                              i4SetValFsPbSrcMacRowStatus));
            VlanSelectContext (u4CurrContextId);

            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcMacRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), FsPbSrcMacAddress,
                      i4SetValFsPbSrcMacRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbSrcMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress

                The Object 
                testValFsPbSrcMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbSrcMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                          tMacAddr FsPbSrcMacAddress,
                          INT4 i4TestValFsPbSrcMacSVlan)
{
    UINT2               u2Port;
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbSrcMacAddress, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_BCASTADDR (FsPbSrcMacAddress) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (FsPbSrcMacAddress) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbSrcMacSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    pSrcMacSVlanEntry =
        VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort, FsPbSrcMacAddress);

    if (pSrcMacSVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        return SNMP_FAILURE;
    }

    if (pSrcMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPbSrcMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress

                The Object 
                testValFsPbSrcMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbSrcMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                              tMacAddr FsPbSrcMacAddress,
                              INT4 i4TestValFsPbSrcMacRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanSrcMacSVlanEntry *pSrcMacSVlanEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbSrcMacAddress, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (FsPbSrcMacAddress) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_BCASTADDR (FsPbSrcMacAddress) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSrcMacRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbSrcMacRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCMAC_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pSrcMacSVlanEntry =
        VlanPbGetSrcMacAddrSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort, FsPbSrcMacAddress);

    if (pSrcMacSVlanEntry == NULL &&
        i4TestValFsPbSrcMacRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pSrcMacSVlanEntry != NULL &&
        i4TestValFsPbSrcMacRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSrcMacRowStatus == VLAN_NOT_IN_SERVICE) &&
        pSrcMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbSrcMacRowStatus == VLAN_ACTIVE &&
        pSrcMacSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbSrcMacSVlanTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbDstMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbDstMacSVlanTable (INT4 i4FsPbPort,
                                              tMacAddr FsPbDstMacAddress)
{
    UINT2               u2Port;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbDstMacAddress, VLAN_MAC_ADDR_LEN)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbDstMacSVlanTable (INT4 *pi4FsPbPort,
                                      tMacAddr * pFsPbDstMacAddress)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    i4Result = VlanPbGetFirstDstMacSVlanEntry (u4TblIndex,
                                               pi4FsPbPort,
                                               *pFsPbDstMacAddress);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbDstMacAddress
                nextFsPbDstMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbDstMacSVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                     tMacAddr FsPbDstMacAddress,
                                     tMacAddr * pNextFsPbDstMacAddress)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    i4Result = VlanPbGetNextDstMacSVlanEntry (u4TblIndex,
                                              i4FsPbPort,
                                              FsPbDstMacAddress,
                                              pi4NextFsPbPort,
                                              *pNextFsPbDstMacAddress);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbDstMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress

                The Object 
                retValFsPbDstMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbDstMacSVlan (INT4 i4FsPbPort, tMacAddr FsPbDstMacAddress,
                       INT4 *pi4RetValFsPbDstMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    pDstMacSVlanEntry = VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       FsPbDstMacAddress);

    if (pDstMacSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbDstMacSVlan = (UINT4) pDstMacSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbDstMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress

                The Object 
                retValFsPbDstMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbDstMacRowStatus (INT4 i4FsPbPort, tMacAddr FsPbDstMacAddress,
                           INT4 *pi4RetValFsPbDstMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    pDstMacSVlanEntry = VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       FsPbDstMacAddress);

    if (pDstMacSVlanEntry != NULL)
    {
        *pi4RetValFsPbDstMacRowStatus = (INT4) pDstMacSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbDstMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress

                The Object 
                setValFsPbDstMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbDstMacSVlan (INT4 i4FsPbPort, tMacAddr FsPbDstMacAddress,
                       INT4 i4SetValFsPbDstMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    pDstMacSVlanEntry = VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       FsPbDstMacAddress);

    if (NULL == pDstMacSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), FsPbDstMacAddress,
                          i4SetValFsPbDstMacSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pDstMacSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbDstMacSVlan;

    if (pDstMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pDstMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), FsPbDstMacAddress,
                      i4SetValFsPbDstMacSVlan));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbDstMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress

                The Object 
                setValFsPbDstMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbDstMacRowStatus (INT4 i4FsPbPort, tMacAddr FsPbDstMacAddress,
                           INT4 i4SetValFsPbDstMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    pDstMacSVlanEntry =
        VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort, FsPbDstMacAddress);

    if (pDstMacSVlanEntry != NULL)
    {
        if (pDstMacSVlanEntry->u1RowStatus == i4SetValFsPbDstMacRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbDstMacRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pDstMacSVlanEntry =
                (tVlanDstMacSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_DSTMAC_TYPE);

            if (pDstMacSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, Dst MAC based S-VLAN"
                          "Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pDstMacSVlanEntry, 0, sizeof (tVlanDstMacSVlanEntry));

            pDstMacSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pDstMacSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            VLAN_CPY_MAC_ADDR (pDstMacSVlanEntry->DstMac, FsPbDstMacAddress);
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSTMAC_TYPE,
                                       (UINT1 *) pDstMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pDstMacSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSTMAC_TYPE,
                                       (UINT1 *) pDstMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDstMacSVlanEntry == NULL) ||
                (pDstMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstMacSVlanEntry->SVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.DstMac, pDstMacSVlanEntry->DstMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pDstMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDstMacSVlanEntry == NULL) ||
                pDstMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstMacSVlanEntry->SVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.DstMac, pDstMacSVlanEntry->DstMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pDstMacSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDstMacSVlanEntry == NULL) ||
                (pDstMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstMacSVlanEntry->SVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.DstMac, pDstMacSVlanEntry->DstMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pDstMacSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  FsPbDstMacAddress,
                                  i4SetValFsPbDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSTMAC_TYPE,
                                   (UINT1 *) pDstMacSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                              VLAN_GET_IFINDEX (i4FsPbPort), FsPbDstMacAddress,
                              i4SetValFsPbDstMacRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstMacRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), FsPbDstMacAddress,
                      i4SetValFsPbDstMacRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbDstMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress

                The Object 
                testValFsPbDstMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbDstMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                          tMacAddr FsPbDstMacAddress,
                          INT4 i4TestValFsPbDstMacSVlan)
{
    UINT4               u4TblIndex;
    UINT2               u2Port;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbDstMacAddress, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbDstMacSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    pDstMacSVlanEntry =
        VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort, FsPbDstMacAddress);

    if (pDstMacSVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        return SNMP_FAILURE;
    }

    if (pDstMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPbDstMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress

                The Object 
                testValFsPbDstMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbDstMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                              tMacAddr FsPbDstMacAddress,
                              INT4 i4TestValFsPbDstMacRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanDstMacSVlanEntry *pDstMacSVlanEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbDstMacAddress, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDstMacRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbDstMacRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTMAC_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pDstMacSVlanEntry =
        VlanPbGetDstMacAddrSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort, FsPbDstMacAddress);
    if ((pDstMacSVlanEntry == NULL) &&
        (i4TestValFsPbDstMacRowStatus != VLAN_CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pDstMacSVlanEntry != NULL &&
        i4TestValFsPbDstMacRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDstMacRowStatus == VLAN_NOT_IN_SERVICE) &&
        pDstMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDstMacRowStatus == VLAN_ACTIVE) &&
        pDstMacSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDstMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbDstMacSVlanTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbCVlanSrcMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbCVlanSrcMacSVlanTable (INT4 i4FsPbPort,
                                                   INT4 i4FsPbCVlanSrcMacCVlan,
                                                   tMacAddr FsPbCVlanSrcMacAddr)
{
    UINT2               u2Port;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanSrcMacCVlan) == VLAN_FALSE)
    {
        return SNMP_FAILURE;

    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbCVlanSrcMacAddr, VLAN_MAC_ADDR_LEN)))
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (FsPbCVlanSrcMacAddr) == VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_BCASTADDR (FsPbCVlanSrcMacAddr) == VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbCVlanSrcMacSVlanTable (INT4 *pi4FsPbPort,
                                           INT4 *pi4FsPbCVlanSrcMacCVlan,
                                           tMacAddr * pFsPbCVlanSrcMacAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    i4Result = VlanPbGetFirstCVlanSrcMacSVlanEntry (u4TblIndex,
                                                    pi4FsPbPort,
                                                    (UINT4 *)
                                                    pi4FsPbCVlanSrcMacCVlan,
                                                    *pFsPbCVlanSrcMacAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbCVlanSrcMacCVlan
                nextFsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr
                nextFsPbCVlanSrcMacAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbCVlanSrcMacSVlanTable (INT4 i4FsPbPort,
                                          INT4 *pi4NextFsPbPort,
                                          INT4 i4FsPbCVlanSrcMacCVlan,
                                          INT4 *pi4NextFsPbCVlanSrcMacCVlan,
                                          tMacAddr FsPbCVlanSrcMacAddr,
                                          tMacAddr * pNextFsPbCVlanSrcMacAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    i4Result = VlanPbGetNextCVlanSrcMacSVlanEntry (u4TblIndex,
                                                   i4FsPbPort,
                                                   (UINT4)
                                                   i4FsPbCVlanSrcMacCVlan,
                                                   FsPbCVlanSrcMacAddr,
                                                   pi4NextFsPbPort,
                                                   (UINT4 *)
                                                   pi4NextFsPbCVlanSrcMacCVlan,
                                                   *pNextFsPbCVlanSrcMacAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbCVlanSrcMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr

                The Object 
                retValFsPbCVlanSrcMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanSrcMacSVlan (INT4 i4FsPbPort, INT4 i4FsPbCVlanSrcMacCVlan,
                            tMacAddr FsPbCVlanSrcMacAddr,
                            INT4 *pi4RetValFsPbCVlanSrcMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    pCVlanSrcMacSVlanEntry =
        VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanSrcMacCVlan,
                                        FsPbCVlanSrcMacAddr);

    if (pCVlanSrcMacSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbCVlanSrcMacSVlan = (UINT4) pCVlanSrcMacSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbCVlanSrcMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr

                The Object 
                retValFsPbCVlanSrcMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanSrcMacRowStatus (INT4 i4FsPbPort,
                                INT4 i4FsPbCVlanSrcMacCVlan,
                                tMacAddr FsPbCVlanSrcMacAddr,
                                INT4 *pi4RetValFsPbCVlanSrcMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    pCVlanSrcMacSVlanEntry =
        VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanSrcMacCVlan,
                                        FsPbCVlanSrcMacAddr);

    if (pCVlanSrcMacSVlanEntry != NULL)
    {
        *pi4RetValFsPbCVlanSrcMacRowStatus =
            (INT4) pCVlanSrcMacSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbCVlanSrcMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr

                The Object 
                setValFsPbCVlanSrcMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanSrcMacSVlan (INT4 i4FsPbPort, INT4 i4FsPbCVlanSrcMacCVlan,
                            tMacAddr FsPbCVlanSrcMacAddr,
                            INT4 i4SetValFsPbCVlanSrcMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    pCVlanSrcMacSVlanEntry =
        VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanSrcMacCVlan,
                                        FsPbCVlanSrcMacAddr);

    if (NULL == pCVlanSrcMacSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanSrcMacSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 3, SNMP_FAILURE);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanSrcMacCVlan,
                          FsPbCVlanSrcMacAddr, i4SetValFsPbCVlanSrcMacSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pCVlanSrcMacSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbCVlanSrcMacSVlan;

    if (pCVlanSrcMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pCVlanSrcMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanSrcMacSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanSrcMacCVlan,
                      FsPbCVlanSrcMacAddr, i4SetValFsPbCVlanSrcMacSVlan));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbCVlanSrcMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr

                The Object 
                setValFsPbCVlanSrcMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanSrcMacRowStatus (INT4 i4FsPbPort, INT4 i4FsPbCVlanSrcMacCVlan,
                                tMacAddr FsPbCVlanSrcMacAddr,
                                INT4 i4SetValFsPbCVlanSrcMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    pCVlanSrcMacSVlanEntry =
        VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanSrcMacCVlan,
                                        FsPbCVlanSrcMacAddr);

    if (pCVlanSrcMacSVlanEntry != NULL)
    {
        if (pCVlanSrcMacSVlanEntry->u1RowStatus ==
            (UINT1) i4SetValFsPbCVlanSrcMacRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbCVlanSrcMacRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pCVlanSrcMacSVlanEntry =
                (tVlanCVlanSrcMacSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

            if (pCVlanSrcMacSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, C-VLAN, Src MAC based"
                          "S-VLAN Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pCVlanSrcMacSVlanEntry, 0,
                         sizeof (tVlanCVlanSrcMacSVlanEntry));

            pCVlanSrcMacSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pCVlanSrcMacSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pCVlanSrcMacSVlanEntry->CVlanId = (tVlanId) i4FsPbCVlanSrcMacCVlan;

            VLAN_CPY_MAC_ADDR (pCVlanSrcMacSVlanEntry->SrcMac,
                               FsPbCVlanSrcMacAddr);

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE,
                                       (UINT1 *) pCVlanSrcMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pCVlanSrcMacSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE,
                                       (UINT1 *) pCVlanSrcMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanSrcMacSVlanEntry == NULL) ||
                (pCVlanSrcMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanSrcMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanSrcMacSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanSrcMacSVlanEntry->CVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.SrcMac,
                               pCVlanSrcMacSVlanEntry->SrcMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pCVlanSrcMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanSrcMacSVlanEntry == NULL) ||
                (pCVlanSrcMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanSrcMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanSrcMacSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanSrcMacSVlanEntry->CVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.SrcMac,
                               pCVlanSrcMacSVlanEntry->SrcMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pCVlanSrcMacSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanSrcMacSVlanEntry == NULL) ||
                (pCVlanSrcMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanSrcMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanSrcMacSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanSrcMacSVlanEntry->CVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.SrcMac,
                               pCVlanSrcMacSVlanEntry->SrcMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pCVlanSrcMacSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                                  i4SetValFsPbCVlanSrcMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE,
                                   (UINT1 *) pCVlanSrcMacSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanSrcMacRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4FsPbCVlanSrcMacCVlan, FsPbCVlanSrcMacAddr,
                              i4SetValFsPbCVlanSrcMacRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanSrcMacRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanSrcMacCVlan,
                      FsPbCVlanSrcMacAddr, i4SetValFsPbCVlanSrcMacRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanSrcMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr

                The Object 
                testValFsPbCVlanSrcMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanSrcMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                               INT4 i4FsPbCVlanSrcMacCVlan,
                               tMacAddr FsPbCVlanSrcMacAddr,
                               INT4 i4TestValFsPbCVlanSrcMacSVlan)
{
    UINT4               u4TblIndex;
    UINT2               u2Port;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanSrcMacCVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbCVlanSrcMacAddr, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (FsPbCVlanSrcMacAddr) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_BCASTADDR (FsPbCVlanSrcMacAddr) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbCVlanSrcMacSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    pCVlanSrcMacSVlanEntry =
        VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                        u2Port,
                                        (tVlanId) i4FsPbCVlanSrcMacCVlan,
                                        FsPbCVlanSrcMacAddr);

    if (pCVlanSrcMacSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pCVlanSrcMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanSrcMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr

                The Object 
                testValFsPbCVlanSrcMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanSrcMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                   INT4 i4FsPbCVlanSrcMacCVlan,
                                   tMacAddr FsPbCVlanSrcMacAddr,
                                   INT4 i4TestValFsPbCVlanSrcMacRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanCVlanSrcMacSVlanEntry *pCVlanSrcMacSVlanEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4FsPbCVlanSrcMacCVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbCVlanSrcMacAddr, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_BCASTADDR (FsPbCVlanSrcMacAddr) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (FsPbCVlanSrcMacAddr) == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbCVlanSrcMacRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbCVlanSrcMacRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pCVlanSrcMacSVlanEntry =
        VlanPbGetCVlanSrcMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanSrcMacCVlan,
                                        FsPbCVlanSrcMacAddr);

    if (pCVlanSrcMacSVlanEntry == NULL &&
        i4TestValFsPbCVlanSrcMacRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanSrcMacSVlanEntry != NULL &&
        i4TestValFsPbCVlanSrcMacRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanSrcMacRowStatus == VLAN_NOT_IN_SERVICE &&
        pCVlanSrcMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanSrcMacRowStatus == VLAN_ACTIVE &&
        pCVlanSrcMacSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbCVlanSrcMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanSrcMacCVlan
                FsPbCVlanSrcMacAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbCVlanSrcMacSVlanTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbCVlanDstMacSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbCVlanDstMacSVlanTable (INT4 i4FsPbPort,
                                                   INT4 i4FsPbCVlanDstMacCVlan,
                                                   tMacAddr FsPbCVlanDstMacAddr)
{
    UINT2               u2Port;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanDstMacCVlan) == VLAN_FALSE)
    {
        return SNMP_FAILURE;

    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbCVlanDstMacAddr, VLAN_MAC_ADDR_LEN)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbCVlanDstMacSVlanTable (INT4 *pi4FsPbPort,
                                           INT4 *pi4FsPbCVlanDstMacCVlan,
                                           tMacAddr * pFsPbCVlanDstMacAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    i4Result = VlanPbGetFirstCVlanDstMacSVlanEntry (u4TblIndex,
                                                    pi4FsPbPort,
                                                    (UINT4 *)
                                                    pi4FsPbCVlanDstMacCVlan,
                                                    *pFsPbCVlanDstMacAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbCVlanDstMacCVlan
                nextFsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr
                nextFsPbCVlanDstMacAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbCVlanDstMacSVlanTable (INT4 i4FsPbPort,
                                          INT4 *pi4NextFsPbPort,
                                          INT4 i4FsPbCVlanDstMacCVlan,
                                          INT4 *pi4NextFsPbCVlanDstMacCVlan,
                                          tMacAddr FsPbCVlanDstMacAddr,
                                          tMacAddr * pNextFsPbCVlanDstMacAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    i4Result = VlanPbGetNextCVlanDstMacSVlanEntry (u4TblIndex,
                                                   i4FsPbPort,
                                                   (UINT4)
                                                   i4FsPbCVlanDstMacCVlan,
                                                   FsPbCVlanDstMacAddr,
                                                   pi4NextFsPbPort,
                                                   (UINT4 *)
                                                   pi4NextFsPbCVlanDstMacCVlan,
                                                   *pNextFsPbCVlanDstMacAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbCVlanDstMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr

                The Object 
                retValFsPbCVlanDstMacSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanDstMacSVlan (INT4 i4FsPbPort,
                            INT4 i4FsPbCVlanDstMacCVlan,
                            tMacAddr FsPbCVlanDstMacAddr,
                            INT4 *pi4RetValFsPbCVlanDstMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    pCVlanDstMacSVlanEntry =
        VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanDstMacCVlan,
                                        FsPbCVlanDstMacAddr);

    if (pCVlanDstMacSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbCVlanDstMacSVlan = (UINT4) pCVlanDstMacSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbCVlanDstMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr

                The Object 
                retValFsPbCVlanDstMacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanDstMacRowStatus (INT4 i4FsPbPort, INT4 i4FsPbCVlanDstMacCVlan,
                                tMacAddr FsPbCVlanDstMacAddr,
                                INT4 *pi4RetValFsPbCVlanDstMacRowStatus)
{
    UINT4               u4TblIndex;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    pCVlanDstMacSVlanEntry =
        VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanDstMacCVlan,
                                        FsPbCVlanDstMacAddr);

    if (pCVlanDstMacSVlanEntry != NULL)
    {
        *pi4RetValFsPbCVlanDstMacRowStatus =
            (INT4) pCVlanDstMacSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbCVlanDstMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr

                The Object 
                setValFsPbCVlanDstMacSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanDstMacSVlan (INT4 i4FsPbPort, INT4 i4FsPbCVlanDstMacCVlan,
                            tMacAddr FsPbCVlanDstMacAddr,
                            INT4 i4SetValFsPbCVlanDstMacSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    pCVlanDstMacSVlanEntry =
        VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanDstMacCVlan,
                                        FsPbCVlanDstMacAddr);

    if (NULL == pCVlanDstMacSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstMacSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 3, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDstMacCVlan,
                          FsPbCVlanDstMacAddr, i4SetValFsPbCVlanDstMacSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pCVlanDstMacSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbCVlanDstMacSVlan;

    if (pCVlanDstMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pCVlanDstMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstMacSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDstMacCVlan,
                      FsPbCVlanDstMacAddr, i4SetValFsPbCVlanDstMacSVlan));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbCVlanDstMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr

                The Object 
                setValFsPbCVlanDstMacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanDstMacRowStatus (INT4 i4FsPbPort,
                                INT4 i4FsPbCVlanDstMacCVlan,
                                tMacAddr FsPbCVlanDstMacAddr,
                                INT4 i4SetValFsPbCVlanDstMacRowStatus)
{
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    pCVlanDstMacSVlanEntry =
        VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanDstMacCVlan,
                                        FsPbCVlanDstMacAddr);

    if (pCVlanDstMacSVlanEntry != NULL)
    {
        if (pCVlanDstMacSVlanEntry->u1RowStatus ==
            i4SetValFsPbCVlanDstMacRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbCVlanDstMacRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pCVlanDstMacSVlanEntry =
                (tVlanCVlanDstMacSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

            if (pCVlanDstMacSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "No Free Port, C-VLAN, Destination MAC based"
                          "S-VLAN Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pCVlanDstMacSVlanEntry, 0,
                         sizeof (tVlanCVlanDstMacSVlanEntry));

            pCVlanDstMacSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pCVlanDstMacSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pCVlanDstMacSVlanEntry->CVlanId = (tVlanId) i4FsPbCVlanDstMacCVlan;

            VLAN_CPY_MAC_ADDR (pCVlanDstMacSVlanEntry->DstMac,
                               FsPbCVlanDstMacAddr);

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE,
                                       (UINT1 *) pCVlanDstMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pCVlanDstMacSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE,
                                       (UINT1 *) pCVlanDstMacSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanDstMacSVlanEntry == NULL) ||
                (pCVlanDstMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDstMacSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDstMacSVlanEntry->CVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.DstMac,
                               pCVlanDstMacSVlanEntry->DstMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pCVlanDstMacSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanDstMacSVlanEntry == NULL) ||
                (pCVlanDstMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDstMacSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDstMacSVlanEntry->CVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.DstMac,
                               pCVlanDstMacSVlanEntry->DstMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pCVlanDstMacSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanDstMacSVlanEntry == NULL) ||
                (pCVlanDstMacSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstMacSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDstMacSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDstMacSVlanEntry->CVlanId;

            VLAN_CPY_MAC_ADDR (VlanSVlanMap.DstMac,
                               pCVlanDstMacSVlanEntry->DstMac);

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pCVlanDstMacSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                                  i4SetValFsPbCVlanDstMacRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE,
                                   (UINT1 *) pCVlanDstMacSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstMacRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4FsPbCVlanDstMacCVlan, FsPbCVlanDstMacAddr,
                              i4SetValFsPbCVlanDstMacRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstMacRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %m %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDstMacCVlan,
                      FsPbCVlanDstMacAddr, i4SetValFsPbCVlanDstMacRowStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanDstMacSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr

                The Object 
                testValFsPbCVlanDstMacSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanDstMacSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                               INT4 i4FsPbCVlanDstMacCVlan,
                               tMacAddr FsPbCVlanDstMacAddr,
                               INT4 i4TestValFsPbCVlanDstMacSVlan)
{
    UINT2               u2Port;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if ((VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4FsPbCVlanDstMacCVlan) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbCVlanDstMacAddr, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbCVlanDstMacSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    pCVlanDstMacSVlanEntry =
        VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                        u2Port,
                                        (tVlanId) i4FsPbCVlanDstMacCVlan,
                                        FsPbCVlanDstMacAddr);

    if (pCVlanDstMacSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pCVlanDstMacSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanDstMacRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr

                The Object 
                testValFsPbCVlanDstMacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanDstMacRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                   INT4 i4FsPbCVlanDstMacCVlan,
                                   tMacAddr FsPbCVlanDstMacAddr,
                                   INT4 i4TestValFsPbCVlanDstMacRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanCVlanDstMacSVlanEntry *pCVlanDstMacSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4FsPbCVlanDstMacCVlan) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    VLAN_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (!(VLAN_MEMCMP (au1ZeroMacAddr, FsPbCVlanDstMacAddr, VLAN_MAC_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbCVlanDstMacRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbCVlanDstMacRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pCVlanDstMacSVlanEntry =
        VlanPbGetCVlanDstMacSVlanEntry (u4TblIndex,
                                        (UINT2) i4FsPbPort,
                                        (tVlanId) i4FsPbCVlanDstMacCVlan,
                                        FsPbCVlanDstMacAddr);

    if (pCVlanDstMacSVlanEntry == NULL &&
        i4TestValFsPbCVlanDstMacRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanDstMacSVlanEntry != NULL &&
        i4TestValFsPbCVlanDstMacRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanDstMacRowStatus == VLAN_NOT_IN_SERVICE &&
        pCVlanDstMacSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanDstMacRowStatus == VLAN_ACTIVE &&
        pCVlanDstMacSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbCVlanDstMacSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstMacCVlan
                FsPbCVlanDstMacAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbCVlanDstMacSVlanTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbDscpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDscp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbDscpSVlanTable (INT4 i4FsPbPort, INT4 i4FsPbDscp)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_DSCP_VALID (i4FsPbDscp) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDscp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbDscpSVlanTable (INT4 *pi4FsPbPort, INT4 *pi4FsPbDscp)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    i4Result = VlanPbGetFirstDscpSVlanEntry (u4TblIndex,
                                             pi4FsPbPort, pi4FsPbDscp);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbDscp
                nextFsPbDscp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbDscpSVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                   INT4 i4FsPbDscp, INT4 *pi4NextFsPbDscp)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    i4Result = VlanPbGetNextDscpSVlanEntry (u4TblIndex,
                                            i4FsPbPort,
                                            i4FsPbDscp,
                                            pi4NextFsPbPort, pi4NextFsPbDscp);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbDscpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDscp

                The Object 
                retValFsPbDscpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbDscpSVlan (INT4 i4FsPbPort, INT4 i4FsPbDscp,
                     INT4 *pi4RetValFsPbDscpSVlan)
{
    UINT4               u4TblIndex;
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    pDscpSVlanEntry = VlanPbGetDscpSVlanEntry (u4TblIndex,
                                               (UINT2) i4FsPbPort,
                                               (UINT4) i4FsPbDscp);

    if (pDscpSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbDscpSVlan = (UINT4) pDscpSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbDscpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDscp

                The Object 
                retValFsPbDscpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbDscpRowStatus (INT4 i4FsPbPort, INT4 i4FsPbDscp,
                         INT4 *pi4RetValFsPbDscpRowStatus)
{
    UINT4               u4TblIndex;
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    pDscpSVlanEntry = VlanPbGetDscpSVlanEntry (u4TblIndex,
                                               (UINT2) i4FsPbPort,
                                               (UINT4) i4FsPbDscp);

    if (pDscpSVlanEntry != NULL)
    {
        *pi4RetValFsPbDscpRowStatus = (INT4) pDscpSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbDscpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDscp

                The Object 
                setValFsPbDscpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbDscpSVlan (INT4 i4FsPbPort, INT4 i4FsPbDscp,
                     INT4 i4SetValFsPbDscpSVlan)
{
    UINT4               u4TblIndex;
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    pDscpSVlanEntry = VlanPbGetDscpSVlanEntry (u4TblIndex,
                                               (UINT2) i4FsPbPort,
                                               (UINT4) i4FsPbDscp);

    if (NULL == pDscpSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                          i4SetValFsPbDscpSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pDscpSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbDscpSVlan;

    if (pDscpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pDscpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                      i4SetValFsPbDscpSVlan));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbDscpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDscp

                The Object 
                setValFsPbDscpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbDscpRowStatus (INT4 i4FsPbPort, INT4 i4FsPbDscp,
                         INT4 i4SetValFsPbDscpRowStatus)
{
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4TblIndex;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    pDscpSVlanEntry =
        VlanPbGetDscpSVlanEntry (u4TblIndex,
                                 (UINT2) i4FsPbPort, (UINT4) i4FsPbDscp);

    if (pDscpSVlanEntry != NULL)
    {
        if (pDscpSVlanEntry->u1RowStatus == i4SetValFsPbDscpRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbDscpRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pDscpSVlanEntry =
                (tVlanDscpSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_DSCP_TYPE);

            if (pDscpSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, DSCP based S-VLAN"
                          "Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pDscpSVlanEntry, 0, sizeof (tVlanDscpSVlanEntry));

            pDscpSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pDscpSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pDscpSVlanEntry->u4Dscp = (UINT4) i4FsPbDscp;

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSCP_TYPE,
                                       (UINT1 *) pDscpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pDscpSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSCP_TYPE,
                                       (UINT1 *) pDscpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDscpSVlanEntry == NULL) ||
                (pDscpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDscpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDscpSVlanEntry->SVlanId;
            VlanSVlanMap.u4Dscp = pDscpSVlanEntry->u4Dscp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSCP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pDscpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDscpSVlanEntry == NULL) ||
                (pDscpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDscpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDscpSVlanEntry->SVlanId;
            VlanSVlanMap.u4Dscp = pDscpSVlanEntry->u4Dscp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSCP_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pDscpSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDscpSVlanEntry == NULL) ||
                (pDscpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDscpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDscpSVlanEntry->SVlanId;
            VlanSVlanMap.u4Dscp = pDscpSVlanEntry->u4Dscp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSCP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pDscpSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                                  i4SetValFsPbDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSCP_TYPE,
                                   (UINT1 *) pDscpSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, 0,
                                  TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                              i4SetValFsPbDscpRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDscpRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbDscp,
                      i4SetValFsPbDscpRowStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbDscpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDscp

                The Object 
                testValFsPbDscpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbDscpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                        INT4 i4FsPbDscp, INT4 i4TestValFsPbDscpSVlan)
{
    UINT2               u2Port;
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_DSCP_VALID (i4FsPbDscp) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbDscpSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    pDscpSVlanEntry =
        VlanPbGetDscpSVlanEntry (u4TblIndex,
                                 (UINT2) i4FsPbPort, (UINT4) i4FsPbDscp);

    if (pDscpSVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pDscpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPbDscpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDscp

                The Object 
                testValFsPbDscpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbDscpRowStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsPbPort,
                            INT4 i4FsPbDscp, INT4 i4TestValFsPbDscpRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanDscpSVlanEntry *pDscpSVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_DSCP_VALID (i4FsPbDscp) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDscpRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbDscpRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSCP_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pDscpSVlanEntry =
        VlanPbGetDscpSVlanEntry (u4TblIndex,
                                 (UINT2) i4FsPbPort, (UINT4) i4FsPbDscp);

    if (pDscpSVlanEntry == NULL &&
        i4TestValFsPbDscpRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pDscpSVlanEntry != NULL &&
        i4TestValFsPbDscpRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDscpRowStatus == VLAN_NOT_IN_SERVICE) &&
        pDscpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDscpRowStatus == VLAN_ACTIVE) &&
        pDscpSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDscp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbDscpSVlanTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbCVlanDscpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbCVlanDscpSVlanTable (INT4 i4FsPbPort,
                                                 INT4 i4FsPbCVlanDscpCVlan,
                                                 INT4 i4FsPbCVlanDscp)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanDscpCVlan) == VLAN_FALSE)
    {
        return SNMP_FAILURE;

    }

    if (VLAN_IS_DSCP_VALID (i4FsPbCVlanDscp) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbCVlanDscpSVlanTable (INT4 *pi4FsPbPort,
                                         INT4 *pi4FsPbCVlanDscpCVlan,
                                         INT4 *pi4FsPbCVlanDscp)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    i4Result = VlanPbGetFirstCVlanDscpSVlanEntry (u4TblIndex,
                                                  pi4FsPbPort,
                                                  (UINT4 *)
                                                  pi4FsPbCVlanDscpCVlan,
                                                  (UINT4 *) pi4FsPbCVlanDscp);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbCVlanDscpCVlan
                nextFsPbCVlanDscpCVlan
                FsPbCVlanDscp
                nextFsPbCVlanDscp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbCVlanDscpSVlanTable (INT4 i4FsPbPort,
                                        INT4 *pi4NextFsPbPort,
                                        INT4 i4FsPbCVlanDscpCVlan,
                                        INT4 *pi4NextFsPbCVlanDscpCVlan,
                                        INT4 i4FsPbCVlanDscp,
                                        INT4 *pi4NextFsPbCVlanDscp)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    i4Result = VlanPbGetNextCVlanDscpSVlanEntry (u4TblIndex,
                                                 i4FsPbPort,
                                                 (UINT4) i4FsPbCVlanDscpCVlan,
                                                 i4FsPbCVlanDscp,
                                                 pi4NextFsPbPort,
                                                 (UINT4 *)
                                                 pi4NextFsPbCVlanDscpCVlan,
                                                 pi4NextFsPbCVlanDscp);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbCVlanDscpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp

                The Object 
                retValFsPbCVlanDscpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanDscpSVlan (INT4 i4FsPbPort,
                          INT4 i4FsPbCVlanDscpCVlan,
                          INT4 i4FsPbCVlanDscp,
                          INT4 *pi4RetValFsPbCVlanDscpSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    pCVlanDscpSVlanEntry =
        VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                      (UINT2) i4FsPbPort,
                                      (tVlanId) i4FsPbCVlanDscpCVlan,
                                      (UINT4) i4FsPbCVlanDscp);

    if (pCVlanDscpSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbCVlanDscpSVlan = (UINT4) pCVlanDscpSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbCVlanDscpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp

                The Object 
                retValFsPbCVlanDscpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanDscpRowStatus (INT4 i4FsPbPort,
                              INT4 i4FsPbCVlanDscpCVlan,
                              INT4 i4FsPbCVlanDscp,
                              INT4 *pi4RetValFsPbCVlanDscpRowStatus)
{
    UINT4               u4TblIndex;
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    pCVlanDscpSVlanEntry =
        VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                      (UINT2) i4FsPbPort,
                                      (tVlanId) i4FsPbCVlanDscpCVlan,
                                      (UINT4) i4FsPbCVlanDscp);

    if (pCVlanDscpSVlanEntry != NULL)
    {
        *pi4RetValFsPbCVlanDscpRowStatus =
            (INT4) pCVlanDscpSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbCVlanDscpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp

                The Object 
                setValFsPbCVlanDscpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanDscpSVlan (INT4 i4FsPbPort,
                          INT4 i4FsPbCVlanDscpCVlan,
                          INT4 i4FsPbCVlanDscp, INT4 i4SetValFsPbCVlanDscpSVlan)
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4TblIndex;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    pCVlanDscpSVlanEntry =
        VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                      (UINT2) i4FsPbPort,
                                      (tVlanId) i4FsPbCVlanDscpCVlan,
                                      (UINT4) i4FsPbCVlanDscp);

    if (NULL == pCVlanDscpSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 3, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDscpCVlan,
                          i4FsPbCVlanDscp, i4SetValFsPbCVlanDscpSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pCVlanDscpSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbCVlanDscpSVlan;

    if (pCVlanDscpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pCVlanDscpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDscpCVlan,
                      i4FsPbCVlanDscp, i4SetValFsPbCVlanDscpSVlan));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsPbCVlanDscpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp

                The Object 
                setValFsPbCVlanDscpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanDscpRowStatus (INT4 i4FsPbPort,
                              INT4 i4FsPbCVlanDscpCVlan,
                              INT4 i4FsPbCVlanDscp,
                              INT4 i4SetValFsPbCVlanDscpRowStatus)
{
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    pCVlanDscpSVlanEntry =
        VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                      (UINT2) i4FsPbPort,
                                      (tVlanId) i4FsPbCVlanDscpCVlan,
                                      (UINT4) i4FsPbCVlanDscp);

    if (pCVlanDscpSVlanEntry != NULL)
    {
        if (pCVlanDscpSVlanEntry->u1RowStatus == i4SetValFsPbCVlanDscpRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbCVlanDscpRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pCVlanDscpSVlanEntry =
                (tVlanCVlanDscpSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

            if (pCVlanDscpSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, C-VLAN, Dscp based"
                          "S-VLAN Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pCVlanDscpSVlanEntry, 0,
                         sizeof (tVlanCVlanDscpSVlanEntry));

            pCVlanDscpSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pCVlanDscpSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pCVlanDscpSVlanEntry->CVlanId = (tVlanId) i4FsPbCVlanDscpCVlan;

            pCVlanDscpSVlanEntry->u4Dscp = (UINT4) i4FsPbCVlanDscp;

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE,
                                       (UINT1 *) pCVlanDscpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pCVlanDscpSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE,
                                       (UINT1 *) pCVlanDscpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanDscpSVlanEntry == NULL) ||
                (pCVlanDscpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDscpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDscpSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDscpSVlanEntry->CVlanId;
            VlanSVlanMap.u4Dscp = pCVlanDscpSVlanEntry->u4Dscp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pCVlanDscpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanDscpSVlanEntry == NULL) ||
                (pCVlanDscpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDscpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDscpSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDscpSVlanEntry->CVlanId;
            VlanSVlanMap.u4Dscp = pCVlanDscpSVlanEntry->u4Dscp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pCVlanDscpSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pCVlanDscpSVlanEntry == NULL) ||
                (pCVlanDscpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDscpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDscpSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDscpSVlanEntry->CVlanId;
            VlanSVlanMap.u4Dscp = pCVlanDscpSVlanEntry->u4Dscp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pCVlanDscpSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                                  i4SetValFsPbCVlanDscpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE,
                                   (UINT1 *) pCVlanDscpSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4FsPbCVlanDscpCVlan, i4FsPbCVlanDscp,
                              i4SetValFsPbCVlanDscpRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDscpRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDscpCVlan,
                      i4FsPbCVlanDscp, i4SetValFsPbCVlanDscpRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanDscpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp

                The Object 
                testValFsPbCVlanDscpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanDscpSVlan (UINT4 *pu4ErrorCode,
                             INT4 i4FsPbPort, INT4 i4FsPbCVlanDscpCVlan,
                             INT4 i4FsPbCVlanDscp,
                             INT4 i4TestValFsPbCVlanDscpSVlan)
{
    UINT4               u4TblIndex;
    UINT2               u2Port;
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4FsPbCVlanDscpCVlan) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_DSCP_VALID (i4FsPbCVlanDscp) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbCVlanDscpSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    pCVlanDscpSVlanEntry =
        VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                      u2Port,
                                      (tVlanId) i4FsPbCVlanDscpCVlan,
                                      (UINT4) i4FsPbCVlanDscp);

    if (pCVlanDscpSVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pCVlanDscpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanDscpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp

                The Object 
                testValFsPbCVlanDscpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanDscpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                 INT4 i4FsPbCVlanDscpCVlan,
                                 INT4 i4FsPbCVlanDscp,
                                 INT4 i4TestValFsPbCVlanDscpRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanCVlanDscpSVlanEntry *pCVlanDscpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4FsPbCVlanDscpCVlan) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_DSCP_VALID (i4FsPbCVlanDscp) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbCVlanDscpRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbCVlanDscpRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pCVlanDscpSVlanEntry =
        VlanPbGetCVlanDscpSVlanEntry (u4TblIndex,
                                      (UINT2) i4FsPbPort,
                                      (tVlanId) i4FsPbCVlanDscpCVlan,
                                      (UINT4) i4FsPbCVlanDscp);

    if (pCVlanDscpSVlanEntry == NULL &&
        i4TestValFsPbCVlanDscpRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanDscpSVlanEntry != NULL &&
        i4TestValFsPbCVlanDscpRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanDscpRowStatus == VLAN_NOT_IN_SERVICE &&
        pCVlanDscpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanDscpRowStatus == VLAN_ACTIVE &&
        pCVlanDscpSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbCVlanDscpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDscpCVlan
                FsPbCVlanDscp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbCVlanDscpSVlanTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbSrcIpAddrSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbSrcIpAddrSVlanTable (INT4 i4FsPbPort,
                                                 UINT4 u4FsPbSrcIpAddr)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcIpAddr)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbSrcIpAddrSVlanTable (INT4 *pi4FsPbPort,
                                         UINT4 *pu4FsPbSrcIpAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    i4Result = VlanPbGetFirstSrcIpSVlanEntry (u4TblIndex,
                                              pi4FsPbPort, pu4FsPbSrcIpAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbSrcIpAddr
                nextFsPbSrcIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbSrcIpAddrSVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                        UINT4 u4FsPbSrcIpAddr,
                                        UINT4 *pu4NextFsPbSrcIpAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    i4Result = VlanPbGetNextSrcIpSVlanEntry (u4TblIndex,
                                             i4FsPbPort,
                                             u4FsPbSrcIpAddr,
                                             pi4NextFsPbPort,
                                             pu4NextFsPbSrcIpAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbSrcIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr

                The Object 
                retValFsPbSrcIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSrcIpSVlan (INT4 i4FsPbPort, UINT4 u4FsPbSrcIpAddr,
                      INT4 *pi4RetValFsPbSrcIpSVlan)
{
    UINT4               u4TblIndex;
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    pSrcIpSVlanEntry = VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                                 (UINT2) i4FsPbPort,
                                                 u4FsPbSrcIpAddr);

    if (pSrcIpSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbSrcIpSVlan = (UINT4) pSrcIpSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbSrcIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr

                The Object 
                retValFsPbSrcIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSrcIpRowStatus (INT4 i4FsPbPort, UINT4 u4FsPbSrcIpAddr,
                          INT4 *pi4RetValFsPbSrcIpRowStatus)
{
    UINT4               u4TblIndex;
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    pSrcIpSVlanEntry = VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                                 (UINT2) i4FsPbPort,
                                                 u4FsPbSrcIpAddr);

    if (pSrcIpSVlanEntry != NULL)
    {
        *pi4RetValFsPbSrcIpRowStatus = (INT4) pSrcIpSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbSrcIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr

                The Object 
                setValFsPbSrcIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSrcIpSVlan (INT4 i4FsPbPort,
                      UINT4 u4FsPbSrcIpAddr, INT4 i4SetValFsPbSrcIpSVlan)
{
    UINT4               u4TblIndex;
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    pSrcIpSVlanEntry = VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                                 (UINT2) i4FsPbPort,
                                                 u4FsPbSrcIpAddr);

    if (NULL == pSrcIpSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcIpAddr,
                          i4SetValFsPbSrcIpSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }
    pSrcIpSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbSrcIpSVlan;

    if (pSrcIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {

        pSrcIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcIpAddr,
                      i4SetValFsPbSrcIpSVlan));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbSrcIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr

                The Object 
                setValFsPbSrcIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSrcIpRowStatus (INT4 i4FsPbPort,
                          UINT4 u4FsPbSrcIpAddr,
                          INT4 i4SetValFsPbSrcIpRowStatus)
{
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    pSrcIpSVlanEntry =
        VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                  (UINT2) i4FsPbPort, u4FsPbSrcIpAddr);

    if (pSrcIpSVlanEntry != NULL)
    {
        if (pSrcIpSVlanEntry->u1RowStatus == i4SetValFsPbSrcIpRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbSrcIpRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pSrcIpSVlanEntry =
                (tVlanSrcIpSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_SRCIP_TYPE);

            if (pSrcIpSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, SRC IP based S-VLAN"
                          "Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pSrcIpSVlanEntry, 0, sizeof (tVlanSrcIpSVlanEntry));

            pSrcIpSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pSrcIpSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pSrcIpSVlanEntry->u4SrcIp = u4FsPbSrcIpAddr;

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCIP_TYPE,
                                       (UINT1 *) pSrcIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pSrcIpSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCIP_TYPE,
                                       (UINT1 *) pSrcIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcIpSVlanEntry == NULL) ||
                (pSrcIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcIpSVlanEntry->u4SrcIp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }
            pSrcIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcIpSVlanEntry == NULL) ||
                (pSrcIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcIpSVlanEntry->u4SrcIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pSrcIpSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcIpSVlanEntry == NULL) ||
                (pSrcIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcIpSVlanEntry->u4SrcIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pSrcIpSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcIpAddr, i4SetValFsPbSrcIpRowStatus));

                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCIP_TYPE,
                                   (UINT1 *) pSrcIpSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                              VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcIpAddr,
                              i4SetValFsPbSrcIpRowStatus));

            VlanSelectContext (u4CurrContextId);

            return SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcIpRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcIpAddr,
                      i4SetValFsPbSrcIpRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbSrcIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr

                The Object 
                testValFsPbSrcIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbSrcIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                         UINT4 u4FsPbSrcIpAddr, INT4 i4TestValFsPbSrcIpSVlan)
{
    UINT2               u2Port;
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;
    UINT4               u4TblIndex;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbSrcIpSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pSrcIpSVlanEntry =
        VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                  (UINT2) i4FsPbPort, u4FsPbSrcIpAddr);

    if (pSrcIpSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pSrcIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPbSrcIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr

                The Object 
                testValFsPbSrcIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbSrcIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                             UINT4 u4FsPbSrcIpAddr,
                             INT4 i4TestValFsPbSrcIpRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanSrcIpSVlanEntry *pSrcIpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSrcIpRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbSrcIpRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCIP_TYPE);

    pSrcIpSVlanEntry =
        VlanPbGetSrcIpSVlanEntry (u4TblIndex,
                                  (UINT2) i4FsPbPort, u4FsPbSrcIpAddr);

    if (pSrcIpSVlanEntry == NULL &&
        i4TestValFsPbSrcIpRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pSrcIpSVlanEntry != NULL &&
        i4TestValFsPbSrcIpRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSrcIpRowStatus == VLAN_NOT_IN_SERVICE) &&
        pSrcIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSrcIpRowStatus == VLAN_ACTIVE) &&
        pSrcIpSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbSrcIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbSrcIpAddrSVlanTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbDstIpAddrSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbDstIpAddrSVlanTable (INT4 i4FsPbPort,
                                                 UINT4 u4FsPbDstIpAddr)
{
    UINT2               u2Port;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbDstIpAddr)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbDstIpAddrSVlanTable (INT4 *pi4FsPbPort,
                                         UINT4 *pu4FsPbDstIpAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    i4Result = VlanPbGetFirstDstIpSVlanEntry (u4TblIndex,
                                              pi4FsPbPort, pu4FsPbDstIpAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbDstIpAddr
                nextFsPbDstIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbDstIpAddrSVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                        UINT4 u4FsPbDstIpAddr,
                                        UINT4 *pu4NextFsPbDstIpAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    i4Result = VlanPbGetNextDstIpSVlanEntry (u4TblIndex,
                                             i4FsPbPort,
                                             u4FsPbDstIpAddr,
                                             pi4NextFsPbPort,
                                             pu4NextFsPbDstIpAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr

                The Object 
                retValFsPbDstIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbDstIpSVlan (INT4 i4FsPbPort, UINT4 u4FsPbDstIpAddr,
                      INT4 *pi4RetValFsPbDstIpSVlan)
{
    UINT4               u4TblIndex;
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    pDstIpSVlanEntry = VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                                 (UINT2) i4FsPbPort,
                                                 u4FsPbDstIpAddr);

    if (pDstIpSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbDstIpSVlan = (UINT4) pDstIpSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr

                The Object 
                retValFsPbDstIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbDstIpRowStatus (INT4 i4FsPbPort, UINT4 u4FsPbDstIpAddr,
                          INT4 *pi4RetValFsPbDstIpRowStatus)
{
    UINT4               u4TblIndex;
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    pDstIpSVlanEntry = VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                                 (UINT2) i4FsPbPort,
                                                 u4FsPbDstIpAddr);

    if (pDstIpSVlanEntry != NULL)
    {
        *pi4RetValFsPbDstIpRowStatus = (INT4) pDstIpSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr

                The Object 
                setValFsPbDstIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbDstIpSVlan (INT4 i4FsPbPort, UINT4 u4FsPbDstIpAddr,
                      INT4 i4SetValFsPbDstIpSVlan)
{
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4TblIndex;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    pDstIpSVlanEntry = VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                                 (UINT2) i4FsPbPort,
                                                 u4FsPbDstIpAddr);
    if (NULL == pDstIpSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbDstIpAddr,
                          i4SetValFsPbDstIpSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }

    pDstIpSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbDstIpSVlan;

    if (pDstIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pDstIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbDstIpAddr,
                      i4SetValFsPbDstIpSVlan));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr

                The Object 
                setValFsPbDstIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbDstIpRowStatus (INT4 i4FsPbPort,
                          UINT4 u4FsPbDstIpAddr,
                          INT4 i4SetValFsPbDstIpRowStatus)
{
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    pDstIpSVlanEntry =
        VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                  (UINT2) i4FsPbPort, u4FsPbDstIpAddr);

    if (pDstIpSVlanEntry != NULL)
    {
        if (pDstIpSVlanEntry->u1RowStatus == i4SetValFsPbDstIpRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbDstIpRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pDstIpSVlanEntry =
                (tVlanDstIpSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_DSTIP_TYPE);

            if (pDstIpSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "No Free Port, Destination MAC based S-VLAN"
                          "Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pDstIpSVlanEntry, 0, sizeof (tVlanDstIpSVlanEntry));

            pDstIpSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pDstIpSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pDstIpSVlanEntry->u4DstIp = u4FsPbDstIpAddr;

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSTIP_TYPE,
                                       (UINT1 *) pDstIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pDstIpSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSTIP_TYPE,
                                       (UINT1 *) pDstIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDstIpSVlanEntry == NULL) ||
                (pDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pDstIpSVlanEntry->u4DstIp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pDstIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDstIpSVlanEntry == NULL) ||
                (pDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pDstIpSVlanEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pDstIpSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pDstIpSVlanEntry == NULL) ||
                (pDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4DstIp = pDstIpSVlanEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_DSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pDstIpSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbDstIpAddr, i4SetValFsPbDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_DSTIP_TYPE,
                                   (UINT1 *) pDstIpSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                              VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbDstIpAddr,
                              i4SetValFsPbDstIpRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbDstIpRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbDstIpAddr,
                      i4SetValFsPbDstIpRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr

                The Object 
                testValFsPbDstIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbDstIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                         UINT4 u4FsPbDstIpAddr, INT4 i4TestValFsPbDstIpSVlan)
{
    UINT2               u2Port;
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbDstIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbDstIpSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    pDstIpSVlanEntry =
        VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                  (UINT2) i4FsPbPort, u4FsPbDstIpAddr);

    if (pDstIpSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPbDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr

                The Object 
                testValFsPbDstIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbDstIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                             UINT4 u4FsPbDstIpAddr,
                             INT4 i4TestValFsPbDstIpRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanDstIpSVlanEntry *pDstIpSVlanEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbDstIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDstIpRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbDstIpRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_DSTIP_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pDstIpSVlanEntry =
        VlanPbGetDstIpSVlanEntry (u4TblIndex,
                                  (UINT2) i4FsPbPort, u4FsPbDstIpAddr);

    if (pDstIpSVlanEntry == NULL &&
        i4TestValFsPbDstIpRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pDstIpSVlanEntry != NULL &&
        i4TestValFsPbDstIpRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDstIpRowStatus == VLAN_NOT_IN_SERVICE) &&
        pDstIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbDstIpRowStatus == VLAN_ACTIVE) &&
        pDstIpSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbDstIpAddrSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbDstIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbDstIpAddrSVlanTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbSrcDstIpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbSrcDstIpSVlanTable (INT4 i4FsPbPort,
                                                UINT4 u4FsPbSrcDstSrcIpAddr,
                                                UINT4 u4FsPbSrcDstDstIpAddr)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcDstSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcDstSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcDstSrcIpAddr)))
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbSrcDstDstIpAddr)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbSrcDstIpSVlanTable (INT4 *pi4FsPbPort,
                                        UINT4 *pu4FsPbSrcDstSrcIpAddr,
                                        UINT4 *pu4FsPbSrcDstDstIpAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    i4Result = VlanPbGetFirstSrcDstIpSVlanEntry (u4TblIndex,
                                                 pi4FsPbPort,
                                                 pu4FsPbSrcDstSrcIpAddr,
                                                 pu4FsPbSrcDstDstIpAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbSrcDstSrcIpAddr
                nextFsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr
                nextFsPbSrcDstDstIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbSrcDstIpSVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                       UINT4 u4FsPbSrcDstSrcIpAddr,
                                       UINT4 *pu4NextFsPbSrcDstSrcIpAddr,
                                       UINT4 u4FsPbSrcDstDstIpAddr,
                                       UINT4 *pu4NextFsPbSrcDstDstIpAddr)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    i4Result = VlanPbGetNextSrcDstIpSVlanEntry (u4TblIndex,
                                                i4FsPbPort,
                                                u4FsPbSrcDstSrcIpAddr,
                                                u4FsPbSrcDstDstIpAddr,
                                                pi4NextFsPbPort,
                                                pu4NextFsPbSrcDstSrcIpAddr,
                                                pu4NextFsPbSrcDstDstIpAddr);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbSrcDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr

                The Object 
                retValFsPbSrcDstIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSrcDstIpSVlan (INT4 i4FsPbPort, UINT4 u4FsPbSrcDstSrcIpAddr,
                         UINT4 u4FsPbSrcDstDstIpAddr,
                         INT4 *pi4RetValFsPbSrcDstIpSVlan)
{
    UINT4               u4TblIndex;
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    pSrcDstIpSVlanEntry = VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       u4FsPbSrcDstSrcIpAddr,
                                                       u4FsPbSrcDstDstIpAddr);

    if (pSrcDstIpSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbSrcDstIpSVlan = (UINT4) pSrcDstIpSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbSrcDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr

                The Object 
                retValFsPbSrcDstIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSrcDstIpRowStatus (INT4 i4FsPbPort, UINT4 u4FsPbSrcDstSrcIpAddr,
                             UINT4 u4FsPbSrcDstDstIpAddr,
                             INT4 *pi4RetValFsPbSrcDstIpRowStatus)
{
    UINT4               u4TblIndex;
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    pSrcDstIpSVlanEntry = VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       u4FsPbSrcDstSrcIpAddr,
                                                       u4FsPbSrcDstDstIpAddr);

    if (pSrcDstIpSVlanEntry != NULL)
    {
        *pi4RetValFsPbSrcDstIpRowStatus =
            (INT4) pSrcDstIpSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbSrcDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr

                The Object 
                setValFsPbSrcDstIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSrcDstIpSVlan (INT4 i4FsPbPort, UINT4 u4FsPbSrcDstSrcIpAddr,
                         UINT4 u4FsPbSrcDstDstIpAddr,
                         INT4 i4SetValFsPbSrcDstIpSVlan)
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    pSrcDstIpSVlanEntry = VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                                       (UINT2) i4FsPbPort,
                                                       u4FsPbSrcDstSrcIpAddr,
                                                       u4FsPbSrcDstDstIpAddr);
    if (NULL == pSrcDstIpSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpSVlan, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 3, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcDstSrcIpAddr,
                          u4FsPbSrcDstDstIpAddr, i4SetValFsPbSrcDstIpSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }

    pSrcDstIpSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbSrcDstIpSVlan;

    if (pSrcDstIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pSrcDstIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpSVlan, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcDstSrcIpAddr,
                      u4FsPbSrcDstDstIpAddr, i4SetValFsPbSrcDstIpSVlan));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbSrcDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr

                The Object 
                setValFsPbSrcDstIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSrcDstIpRowStatus (INT4 i4FsPbPort, UINT4 u4FsPbSrcDstSrcIpAddr,
                             UINT4 u4FsPbSrcDstDstIpAddr,
                             INT4 i4SetValFsPbSrcDstIpRowStatus)
{
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;
    tVlanSVlanMap       VlanSVlanMap;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TblIndex;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    pSrcDstIpSVlanEntry =
        VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                     (UINT2) i4FsPbPort,
                                     u4FsPbSrcDstSrcIpAddr,
                                     u4FsPbSrcDstDstIpAddr);

    if (pSrcDstIpSVlanEntry != NULL)
    {
        if (pSrcDstIpSVlanEntry->u1RowStatus == i4SetValFsPbSrcDstIpRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbSrcDstIpRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pSrcDstIpSVlanEntry =
                (tVlanSrcDstIpSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

            if (pSrcDstIpSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Port, SRC, DST IP based S-VLAN"
                          "Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pSrcDstIpSVlanEntry, 0,
                         sizeof (tVlanSrcDstIpSVlanEntry));

            pSrcDstIpSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pSrcDstIpSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pSrcDstIpSVlanEntry->u4SrcIp = u4FsPbSrcDstSrcIpAddr;

            pSrcDstIpSVlanEntry->u4DstIp = u4FsPbSrcDstDstIpAddr;
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCDSTIP_TYPE,
                                       (UINT1 *) pSrcDstIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pSrcDstIpSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCDSTIP_TYPE,
                                       (UINT1 *) pSrcDstIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));
            if ((pSrcDstIpSVlanEntry == NULL) ||
                (pSrcDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcDstIpSVlanEntry->u4SrcIp;
            VlanSVlanMap.u4DstIp = pSrcDstIpSVlanEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pSrcDstIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcDstIpSVlanEntry == NULL) ||
                (pSrcDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcDstIpSVlanEntry->u4SrcIp;
            VlanSVlanMap.u4DstIp = pSrcDstIpSVlanEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pSrcDstIpSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

            if ((pSrcDstIpSVlanEntry == NULL) ||
                (pSrcDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pSrcDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pSrcDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.u4SrcIp = pSrcDstIpSVlanEntry->u4SrcIp;
            VlanSVlanMap.u4DstIp = pSrcDstIpSVlanEntry->u4DstIp;
            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_SRCDSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pSrcDstIpSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                                  i4SetValFsPbSrcDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_SRCDSTIP_TYPE,
                                   (UINT1 *) pSrcDstIpSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              u4FsPbSrcDstSrcIpAddr, u4FsPbSrcDstDstIpAddr,
                              i4SetValFsPbSrcDstIpRowStatus));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbSrcDstIpRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), u4FsPbSrcDstSrcIpAddr,
                      u4FsPbSrcDstDstIpAddr, i4SetValFsPbSrcDstIpRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbSrcDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr

                The Object 
                testValFsPbSrcDstIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbSrcDstIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                            UINT4 u4FsPbSrcDstSrcIpAddr,
                            UINT4 u4FsPbSrcDstDstIpAddr,
                            INT4 i4TestValFsPbSrcDstIpSVlan)
{
    UINT2               u2Port;
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcDstSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcDstSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcDstSrcIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbSrcDstDstIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbSrcDstIpSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    pSrcDstIpSVlanEntry =
        VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                     (UINT2) i4FsPbPort,
                                     u4FsPbSrcDstSrcIpAddr,
                                     u4FsPbSrcDstDstIpAddr);

    if (pSrcDstIpSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pSrcDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbSrcDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr

                The Object 
                testValFsPbSrcDstIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbSrcDstIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                UINT4 u4FsPbSrcDstSrcIpAddr,
                                UINT4 u4FsPbSrcDstDstIpAddr,
                                INT4 i4TestValFsPbSrcDstIpRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanSrcDstIpSVlanEntry *pSrcDstIpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcDstSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcDstSrcIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcDstSrcIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbSrcDstDstIpAddr) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbSrcDstDstIpAddr)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSrcDstIpRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbSrcDstIpRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_SRCDSTIP_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pSrcDstIpSVlanEntry =
        VlanPbGetSrcDstIpSVlanEntry (u4TblIndex,
                                     (UINT2) i4FsPbPort,
                                     u4FsPbSrcDstSrcIpAddr,
                                     u4FsPbSrcDstDstIpAddr);

    if (pSrcDstIpSVlanEntry == NULL &&
        i4TestValFsPbSrcDstIpRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbSrcDstIpRowStatus == VLAN_NOT_IN_SERVICE &&
        pSrcDstIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbSrcDstIpRowStatus == VLAN_ACTIVE &&
        pSrcDstIpSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbSrcDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbSrcDstSrcIpAddr
                FsPbSrcDstDstIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbSrcDstIpSVlanTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbCVlanDstIpSVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbCVlanDstIpSVlanTable (INT4 i4FsPbPort,
                                                  INT4 i4FsPbCVlanDstIpCVlan,
                                                  UINT4 u4FsPbCVlanDstIp)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanDstIpCVlan) == VLAN_FALSE)
    {
        return SNMP_FAILURE;

    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbCVlanDstIp)))
    {
        return SNMP_FAILURE;
    }

    if (u4FsPbCVlanDstIp == 0)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbCVlanDstIpSVlanTable (INT4 *pi4FsPbPort,
                                          INT4 *pi4FsPbCVlanDstIpCVlan,
                                          UINT4 *pu4FsPbCVlanDstIp)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    i4Result = VlanPbGetFirstCVlanDstIpSVlanEntry (u4TblIndex,
                                                   pi4FsPbPort,
                                                   (UINT4 *)
                                                   pi4FsPbCVlanDstIpCVlan,
                                                   pu4FsPbCVlanDstIp);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbCVlanDstIpCVlan
                nextFsPbCVlanDstIpCVlan
                FsPbCVlanDstIp
                nextFsPbCVlanDstIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbCVlanDstIpSVlanTable (INT4 i4FsPbPort,
                                         INT4 *pi4NextFsPbPort,
                                         INT4 i4FsPbCVlanDstIpCVlan,
                                         INT4 *pi4NextFsPbCVlanDstIpCVlan,
                                         UINT4 u4FsPbCVlanDstIp,
                                         UINT4 *pu4NextFsPbCVlanDstIp)
{
    INT4                i4Result;
    UINT4               u4TblIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    i4Result = VlanPbGetNextCVlanDstIpSVlanEntry (u4TblIndex,
                                                  i4FsPbPort,
                                                  (UINT4) i4FsPbCVlanDstIpCVlan,
                                                  u4FsPbCVlanDstIp,
                                                  pi4NextFsPbPort,
                                                  (UINT4 *)
                                                  pi4NextFsPbCVlanDstIpCVlan,
                                                  pu4NextFsPbCVlanDstIp);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbCVlanDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp

                The Object 
                retValFsPbCVlanDstIpSVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanDstIpSVlan (INT4 i4FsPbPort,
                           INT4 i4FsPbCVlanDstIpCVlan,
                           UINT4 u4FsPbCVlanDstIp,
                           INT4 *pi4RetValFsPbCVlanDstIpSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    pCVlanDstIpSVlanEntry =
        VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort,
                                       (tVlanId) i4FsPbCVlanDstIpCVlan,
                                       u4FsPbCVlanDstIp);

    if (pCVlanDstIpSVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbCVlanDstIpSVlan = (UINT4) pCVlanDstIpSVlanEntry->SVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbCVlanDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp

                The Object 
                retValFsPbCVlanDstIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbCVlanDstIpRowStatus (INT4 i4FsPbPort,
                               INT4 i4FsPbCVlanDstIpCVlan,
                               UINT4 u4FsPbCVlanDstIp,
                               INT4 *pi4RetValFsPbCVlanDstIpRowStatus)
{
    UINT4               u4TblIndex;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    pCVlanDstIpSVlanEntry =
        VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort,
                                       (tVlanId) i4FsPbCVlanDstIpCVlan,
                                       u4FsPbCVlanDstIp);

    if (pCVlanDstIpSVlanEntry != NULL)
    {
        *pi4RetValFsPbCVlanDstIpRowStatus =
            (INT4) pCVlanDstIpSVlanEntry->u1RowStatus;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbCVlanDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp

                The Object 
                setValFsPbCVlanDstIpSVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanDstIpSVlan (INT4 i4FsPbPort,
                           INT4 i4FsPbCVlanDstIpCVlan,
                           UINT4 u4FsPbCVlanDstIp,
                           INT4 i4SetValFsPbCVlanDstIpSVlan)
{
    UINT4               u4TblIndex;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    pCVlanDstIpSVlanEntry =
        VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort,
                                       (tVlanId) i4FsPbCVlanDstIpCVlan,
                                       u4FsPbCVlanDstIp);
    if (NULL == pCVlanDstIpSVlanEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpSVlan, 0,
                              TRUE, VlanLock, VlanUnLock, 3, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDstIpCVlan,
                          u4FsPbCVlanDstIp, i4SetValFsPbCVlanDstIpSVlan));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }

    pCVlanDstIpSVlanEntry->SVlanId = (tVlanId) i4SetValFsPbCVlanDstIpSVlan;

    if (pCVlanDstIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pCVlanDstIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpSVlan, 0,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDstIpCVlan,
                      u4FsPbCVlanDstIp, i4SetValFsPbCVlanDstIpSVlan));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsPbCVlanDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp

                The Object 
                setValFsPbCVlanDstIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbCVlanDstIpRowStatus (INT4 i4FsPbPort,
                               INT4 i4FsPbCVlanDstIpCVlan,
                               UINT4 u4FsPbCVlanDstIp,
                               INT4 i4SetValFsPbCVlanDstIpRowStatus)
{
    tVlanSVlanMap       VlanSVlanMap;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4TblIndex;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    pCVlanDstIpSVlanEntry =
        VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort,
                                       (tVlanId) i4FsPbCVlanDstIpCVlan,
                                       u4FsPbCVlanDstIp);

    if (pCVlanDstIpSVlanEntry != NULL)
    {
        if (pCVlanDstIpSVlanEntry->u1RowStatus ==
            i4SetValFsPbCVlanDstIpRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbCVlanDstIpRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            pCVlanDstIpSVlanEntry =
                (tVlanCVlanDstIpSVlanEntry *) (VOID *)
                VlanPbGetMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

            if (pCVlanDstIpSVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "No Free Port, C-VLAN, Destination IP based"
                          "S-VLAN Classification Entry \n");
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (pCVlanDstIpSVlanEntry, 0,
                         sizeof (tVlanCVlanDstIpSVlanEntry));

            pCVlanDstIpSVlanEntry->u1RowStatus = VLAN_NOT_READY;

            pCVlanDstIpSVlanEntry->u2Port = (UINT2) i4FsPbPort;

            pCVlanDstIpSVlanEntry->CVlanId = (tVlanId) i4FsPbCVlanDstIpCVlan;

            pCVlanDstIpSVlanEntry->u4DstIp = u4FsPbCVlanDstIp;

            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE,
                                       (UINT1 *) pCVlanDstIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->
                           apVlanSVlanTable[u4TblIndex],
                           (tRBElem *) pCVlanDstIpSVlanEntry) == RB_FAILURE)
            {
                VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE,
                                       (UINT1 *) pCVlanDstIpSVlanEntry);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }

            break;
        case VLAN_NOT_IN_SERVICE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));
            if ((pCVlanDstIpSVlanEntry == NULL) ||
                (pCVlanDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDstIpSVlanEntry->CVlanId;
            VlanSVlanMap.u4DstIp = pCVlanDstIpSVlanEntry->u4DstIp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }
            pCVlanDstIpSVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            break;
        case VLAN_ACTIVE:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));
            if ((pCVlanDstIpSVlanEntry == NULL) ||
                (pCVlanDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDstIpSVlanEntry->CVlanId;
            VlanSVlanMap.u4DstIp = pCVlanDstIpSVlanEntry->u4DstIp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;

            if (VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                   VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }
            pCVlanDstIpSVlanEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_DESTROY:

            VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));
            if ((pCVlanDstIpSVlanEntry == NULL) ||
                (pCVlanDstIpSVlanEntry->u2Port >= VLAN_MAX_PORTS + 1))
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }

            VlanSVlanMap.u2Port =
                (UINT2) (VLAN_GET_IFINDEX (pCVlanDstIpSVlanEntry->u2Port));
            VlanSVlanMap.SVlanId = pCVlanDstIpSVlanEntry->SVlanId;
            VlanSVlanMap.CVlanId = pCVlanDstIpSVlanEntry->CVlanId;
            VlanSVlanMap.u4DstIp = pCVlanDstIpSVlanEntry->u4DstIp;

            VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE;

            if (VlanHwDeleteSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                      VlanSVlanMap) == VLAN_FAILURE)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }
            if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            if (RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                              apVlanSVlanTable[u4TblIndex],
                              (tRBElem *) pCVlanDstIpSVlanEntry) != RB_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                      u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                                  i4SetValFsPbCVlanDstIpRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }

            VlanPbReleaseMemBlock (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE,
                                   (UINT1 *) pCVlanDstIpSVlanEntry);

            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 3,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4FsPbCVlanDstIpCVlan, u4FsPbCVlanDstIp,
                              i4SetValFsPbCVlanDstIpRowStatus));
            VlanSelectContext (u4CurrContextId);

            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbCVlanDstIpRowStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbCVlanDstIpCVlan,
                      u4FsPbCVlanDstIp, i4SetValFsPbCVlanDstIpRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanDstIpSVlan
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp

                The Object 
                testValFsPbCVlanDstIpSVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanDstIpSVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                              INT4 i4FsPbCVlanDstIpCVlan,
                              UINT4 u4FsPbCVlanDstIp,
                              INT4 i4TestValFsPbCVlanDstIpSVlan)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanDstIpCVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbCVlanDstIp)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsPbCVlanDstIpSVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    pCVlanDstIpSVlanEntry =
        VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                       u2Port,
                                       (tVlanId) i4FsPbCVlanDstIpCVlan,
                                       u4FsPbCVlanDstIp);

    if (pCVlanDstIpSVlanEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pCVlanDstIpSVlanEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbCVlanDstIpRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp

                The Object 
                testValFsPbCVlanDstIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbCVlanDstIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                  INT4 i4FsPbCVlanDstIpCVlan,
                                  UINT4 u4FsPbCVlanDstIp,
                                  INT4 i4TestValFsPbCVlanDstIpRowStatus)
{
    UINT2               u2Port;
    UINT4               u4TblIndex;
    tVlanCVlanDstIpSVlanEntry *pCVlanDstIpSVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4FsPbCVlanDstIpCVlan) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!(VLAN_IP_IS_ADDR_CLASS_A (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_B (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_C (u4FsPbCVlanDstIp) ||
          VLAN_IP_IS_ADDR_CLASS_D (u4FsPbCVlanDstIp)))
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbCVlanDstIpRowStatus < VLAN_ACTIVE) ||
        (i4TestValFsPbCVlanDstIpRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE);

    if (u4TblIndex >= VLAN_NUM_SVLAN_CLASSIFY_TABLES)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pCVlanDstIpSVlanEntry =
        VlanPbGetCVlanDstIpSVlanEntry (u4TblIndex,
                                       (UINT2) i4FsPbPort,
                                       (tVlanId) i4FsPbCVlanDstIpCVlan,
                                       u4FsPbCVlanDstIp);

    if (pCVlanDstIpSVlanEntry == NULL &&
        i4TestValFsPbCVlanDstIpRowStatus != VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanDstIpSVlanEntry != NULL &&
        i4TestValFsPbCVlanDstIpRowStatus == VLAN_CREATE_AND_WAIT)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanDstIpRowStatus == VLAN_NOT_IN_SERVICE &&
        pCVlanDstIpSVlanEntry->u1RowStatus == VLAN_NOT_READY)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbCVlanDstIpRowStatus == VLAN_ACTIVE &&
        pCVlanDstIpSVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbCVlanDstIpSVlanTable
 Input       :  The Indices
                FsPbPort
                FsPbCVlanDstIpCVlan
                FsPbCVlanDstIp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbCVlanDstIpSVlanTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbPortBasedCVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbPortBasedCVlanTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbPortBasedCVlanTable (INT4 i4FsPbPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsPbPort;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbPortBasedCVlanTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbPortBasedCVlanTable (INT4 *pi4FsPbPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsPbPortBasedCVlanTable (0, pi4FsPbPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbPortBasedCVlanTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbPortBasedCVlanTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsPbPort < 0) || (i4FsPbPort > VLAN_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4FsPbPort + 1); u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }

        pVlanPbPortEntry = pPortEntry->pVlanPbPortEntry;

        if ((pVlanPbPortEntry != NULL) &&
            (pVlanPbPortEntry->u1PbPortType != VLAN_CNP_PORTBASED_PORT) &&
            (pVlanPbPortEntry->u1PbPortType != VLAN_CNP_TAGGED_PORT) &&
            (pVlanPbPortEntry->u1PbPortType != VLAN_PROP_PROVIDER_NETWORK_PORT)
            && (pVlanPbPortEntry->u1PbPortType != VLAN_PROVIDER_NETWORK_PORT))
        {
            *pi4NextFsPbPort = (INT4) u2Port;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbPortCVlan
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortCVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortCVlan (INT4 i4FsPbPort, INT4 *pi4RetValFsPbPortCVlan)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT))
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortCVlan = pVlanPbPortEntry->CVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortCVlanClassifyStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbPortCVlanClassifyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPortCVlanClassifyStatus (INT4 i4FsPbPort,
                                   INT4 *pi4RetValFsPbPortCVlanClassifyStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT))
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortCVlanClassifyStatus =
        pVlanPbPortEntry->u1DefCVlanClassify;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbPortEgressUntaggedStatus
 Input       :  The Indices
                FsPbPort

                The Object
                retValFsPbPortEgressUntaggedStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPbPortEgressUntaggedStatus(INT4 i4FsPbPort , INT4 *pi4RetValFsPbPortEgressUntaggedStatus)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT) 
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPortEgressUntaggedStatus =
        (INT4) pVlanPbPortEntry->u1UntagFrameOnCEP;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbPortCVlan
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortCVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortCVlan (INT4 i4FsPbPort, INT4 i4SetValFsPbPortCVlan)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->CVlanId == (tVlanId) i4SetValFsPbPortCVlan)
    {
        return SNMP_SUCCESS;
    }

    if(pVlanPortEntry->u1OperStatus == VLAN_OPER_UP)
    {
        if (VlanHwSetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                       (UINT2)
                                       (VLAN_GET_IFINDEX (i4FsPbPort)),
                                       (tVlanId) i4SetValFsPbPortCVlan) !=
            VLAN_SUCCESS)
        {

            return SNMP_FAILURE;

        }
    }
    
    RM_GET_SEQ_NUM (&u4SeqNum);

    pVlanPbPortEntry->CVlanId = (UINT2) i4SetValFsPbPortCVlan;
    pVlanPbPortEntry->u1EncapType = VLAN_PB_PORT_AS_DOT1Q_ENA;

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortCVlan, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (i4FsPbPort),
                      i4SetValFsPbPortCVlan));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortCVlanClassifyStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbPortCVlanClassifyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPortCVlanClassifyStatus (INT4 i4FsPbPort,
                                   INT4 i4SetValFsPbPortCVlanClassifyStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1DefCVlanClassify
        == (UINT1) i4SetValFsPbPortCVlanClassifyStatus)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValFsPbPortCVlanClassifyStatus == VLAN_ENABLED)
    {
        if (VlanHwSetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                       (UINT2)
                                       (VLAN_GET_IFINDEX (i4FsPbPort)),
                                       pVlanPbPortEntry->CVlanId) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (VlanHwResetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                         (UINT2)
                                         (VLAN_GET_IFINDEX (i4FsPbPort)))
            != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    pVlanPbPortEntry->u1DefCVlanClassify =
        (UINT1) i4SetValFsPbPortCVlanClassifyStatus;
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPortCVlanClassifyStatus,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (i4FsPbPort),
                      i4SetValFsPbPortCVlanClassifyStatus));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbPortEgressUntaggedStatus
 Input       :  The Indices
                FsPbPort

                The Object
                setValFsPbPortEgressUntaggedStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsPbPortEgressUntaggedStatus(INT4 i4FsPbPort , INT4 i4SetValFsPbPortEgressUntaggedStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4CurrContextId = 0;

    VLAN_MEMSET (&VlanPortProperty, VLAN_INIT_VAL, sizeof(tHwVlanPortProperty));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1UntagFrameOnCEP == 
        (UINT1) i4SetValFsPbPortEgressUntaggedStatus)
    {
        return SNMP_SUCCESS;
    }

    /* The setting to allow/deny untagged frames on CEP is never used in 
       control plane. So irrespective of the mib value/set value
       the control plane will transmit untagged frames (based on CVID 
       registration table) towards CEP port on Customer Network.
       This is only applicable for H/w (BCMX) where we can restrict 
       the CEP port to allow/deny untagged frames being transmitted from
       it towards Customer Bridges. This needs to be ported accordingly
       for various platforms based on the requirement.
     */

    VlanPortProperty.u2OpCode = PB_PORT_PROPERTY_CEP_UNTAG_EGRESS_STATUS;
    VlanPortProperty.u1UntagFrameOnCEP = (UINT1) i4SetValFsPbPortEgressUntaggedStatus;

    /* Set the status to allow/deny untagged frames on CEP */
    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_PHY_PORT ((UINT2) i4FsPbPort),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry->u1UntagFrameOnCEP = 
        (UINT1) i4SetValFsPbPortEgressUntaggedStatus;

    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbPortCVlan
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortCVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortCVlan (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                        INT4 i4TestValFsPbPortCVlan)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_CUSTOMER_VLAN_ID_VALID(i4TestValFsPbPortCVlan) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT))
    {
        CLI_SET_ERR (CLI_PB_CNP_PNP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbPortCVlanClassifyStatus
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbPortCVlanClassifyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPortCVlanClassifyStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                      INT4 i4TestValFsPbPortCVlanClassifyStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbPortCVlanClassifyStatus != VLAN_ENABLED) &&
        (i4TestValFsPbPortCVlanClassifyStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pVlanPbPortEntry->u1PbPortType == VLAN_CNP_TAGGED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_CNP_PORTBASED_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT) ||
        (pVlanPbPortEntry->u1PbPortType == VLAN_PROVIDER_NETWORK_PORT))
    {
        CLI_SET_ERR (CLI_PB_CNP_PNP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbPortEgressUntaggedStatus
Input       :  The Indices
FsPbPort

The Object
testValFsPbPortEgressUntaggedStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsPbPortEgressUntaggedStatus(UINT4 *pu4ErrorCode , INT4 i4FsPbPort , 
                                           INT4 i4TestValFsPbPortEgressUntaggedStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbPortEgressUntaggedStatus != VLAN_ENABLED) &&
        (i4TestValFsPbPortEgressUntaggedStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pVlanPbPortEntry->u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
    {
        CLI_SET_ERR (CLI_PB_CEP_UNTAG_EGRESS_CONF_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbPortBasedCVlanTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbPortBasedCVlanTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbEtherTypeSwapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbEtherTypeSwapTable
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbEtherTypeSwapTable (INT4 i4FsPbPort,
                                                INT4 i4FsPbLocalEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_ETHER_TYPE_VALID (i4FsPbLocalEtherType) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbEtherTypeSwapTable
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbEtherTypeSwapTable (INT4 *pi4FsPbPort,
                                        INT4 *pi4FsPbLocalEtherType)
{

    return (nmhGetNextIndexFsPbEtherTypeSwapTable (0, pi4FsPbPort, 0,
                                                   pi4FsPbLocalEtherType));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbEtherTypeSwapTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
                FsPbLocalEtherType
                nextFsPbLocalEtherType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbEtherTypeSwapTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort,
                                       INT4 i4FsPbLocalEtherType,
                                       INT4 *pi4NextFsPbLocalEtherType)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;
    tEtherTypeSwapEntry TempEtherTypeSwapEntry;

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    VLAN_MEMSET (&TempEtherTypeSwapEntry, 0, sizeof (TempEtherTypeSwapEntry));

    if ((i4FsPbPort == 0) && (i4FsPbLocalEtherType == 0))
    {
        pEtherTypeSwapEntry = RBTreeGetFirst (VLAN_CURR_CONTEXT_PTR ()->
                                              pVlanEtherTypeSwapTable);

        if (pEtherTypeSwapEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4NextFsPbPort = (INT4) pEtherTypeSwapEntry->u2Port;
        *pi4NextFsPbLocalEtherType
            = (INT4) pEtherTypeSwapEntry->u2LocalEtherType;

        return SNMP_SUCCESS;
    }

    TempEtherTypeSwapEntry.u2Port = (UINT2) i4FsPbPort;
    TempEtherTypeSwapEntry.u2LocalEtherType = (UINT2) i4FsPbLocalEtherType;

    pEtherTypeSwapEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                         pVlanEtherTypeSwapTable,
                                         &TempEtherTypeSwapEntry,
                                         VlanPbCmpEtherTypeSwapEntry);

    if (pEtherTypeSwapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsPbPort = (INT4) pEtherTypeSwapEntry->u2Port;
    *pi4NextFsPbLocalEtherType = (INT4) pEtherTypeSwapEntry->u2LocalEtherType;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbRelayEtherType
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType

                The Object 
                retValFsPbRelayEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRelayEtherType (INT4 i4FsPbPort, INT4 i4FsPbLocalEtherType,
                          INT4 *pi4RetValFsPbRelayEtherType)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    pEtherTypeSwapEntry = VlanPbGetEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                                       (UINT2)
                                                       i4FsPbLocalEtherType);

    if (pEtherTypeSwapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbRelayEtherType = (INT4) pEtherTypeSwapEntry->u2RelayEtherType;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPbEtherTypeSwapRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType

                The Object 
                retValFsPbEtherTypeSwapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbEtherTypeSwapRowStatus (INT4 i4FsPbPort, INT4 i4FsPbLocalEtherType,
                                  INT4 *pi4RetValFsPbEtherTypeSwapRowStatus)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    pEtherTypeSwapEntry = VlanPbGetEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                                       (UINT2)
                                                       i4FsPbLocalEtherType);

    if (pEtherTypeSwapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbEtherTypeSwapRowStatus
        = (INT4) pEtherTypeSwapEntry->u1RowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbRelayEtherType
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType

                The Object 
                setValFsPbRelayEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbRelayEtherType (INT4 i4FsPbPort, INT4 i4FsPbLocalEtherType,
                          INT4 i4SetValFsPbRelayEtherType)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    pEtherTypeSwapEntry = VlanPbGetEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                                       (UINT2)
                                                       i4FsPbLocalEtherType);
    if (NULL == pEtherTypeSwapEntry)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbRelayEtherType, u4SeqNum,
                              TRUE, VlanLock, VlanUnLock, 2, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                          VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbLocalEtherType,
                          i4SetValFsPbRelayEtherType));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }

    pEtherTypeSwapEntry->u2RelayEtherType = (UINT2) i4SetValFsPbRelayEtherType;

    if (pEtherTypeSwapEntry->u1RowStatus == VLAN_NOT_READY)
    {
        pEtherTypeSwapEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbRelayEtherType, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbLocalEtherType,
                      i4SetValFsPbRelayEtherType));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsPbEtherTypeSwapRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType

                The Object 
                setValFsPbEtherTypeSwapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbEtherTypeSwapRowStatus (INT4 i4FsPbPort, INT4 i4FsPbLocalEtherType,
                                  INT4 i4SetValFsPbEtherTypeSwapRowStatus)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pEtherTypeSwapEntry = VlanPbGetEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                                       (UINT2)
                                                       i4FsPbLocalEtherType);

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsPbEtherTypeSwapRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
            pEtherTypeSwapEntry
                = VlanPbAddEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                               (UINT2) i4FsPbLocalEtherType);

            if (pEtherTypeSwapEntry == NULL)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbEtherTypeSwapRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbLocalEtherType,
                                  i4SetValFsPbEtherTypeSwapRowStatus));
                VlanSelectContext (u4CurrContextId);
                return SNMP_FAILURE;
            }
            pEtherTypeSwapEntry->u1RowStatus = VLAN_NOT_READY;
            break;

        case VLAN_ACTIVE:
            if (VlanHwAddEtherTypeSwapEntry (VLAN_CURR_CONTEXT_ID (),
                                             VLAN_GET_IFINDEX
                                             (i4FsPbPort),
                                             (UINT2)
                                             pEtherTypeSwapEntry->
                                             u2LocalEtherType,
                                             (UINT2)
                                             pEtherTypeSwapEntry->
                                             u2RelayEtherType) != VLAN_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbEtherTypeSwapRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbLocalEtherType,
                                  i4SetValFsPbEtherTypeSwapRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }

            pEtherTypeSwapEntry->u1RowStatus = VLAN_ACTIVE;

            break;
        case VLAN_NOT_IN_SERVICE:

            if (VlanHwDelEtherTypeSwapEntry (VLAN_CURR_CONTEXT_ID (),
                                             VLAN_GET_IFINDEX
                                             (i4FsPbPort),
                                             pEtherTypeSwapEntry->
                                             u2LocalEtherType,
                                             pEtherTypeSwapEntry->
                                             u2RelayEtherType) != VLAN_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbEtherTypeSwapRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbLocalEtherType,
                                  i4SetValFsPbEtherTypeSwapRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }
            pEtherTypeSwapEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            break;

        case VLAN_DESTROY:

            if (VlanHwDelEtherTypeSwapEntry (VLAN_CURR_CONTEXT_ID (),
                                             VLAN_GET_IFINDEX
                                             (i4FsPbPort),
                                             pEtherTypeSwapEntry->
                                             u2LocalEtherType,
                                             pEtherTypeSwapEntry->
                                             u2RelayEtherType) != VLAN_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbEtherTypeSwapRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbLocalEtherType,
                                  i4SetValFsPbEtherTypeSwapRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }

            if (VlanPbDelEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                             (UINT2) i4FsPbLocalEtherType) !=
                VLAN_SUCCESS)
            {
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIPbEtherTypeSwapRowStatus, u4SeqNum,
                                      TRUE, VlanLock, VlanUnLock, 2,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  VLAN_GET_IFINDEX (i4FsPbPort),
                                  i4FsPbLocalEtherType,
                                  i4SetValFsPbEtherTypeSwapRowStatus));
                VlanSelectContext (u4CurrContextId);

                return SNMP_FAILURE;
            }
            break;
        default:
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbEtherTypeSwapRowStatus,
                                  u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                                  SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                              VLAN_GET_IFINDEX (i4FsPbPort),
                              i4FsPbLocalEtherType,
                              i4SetValFsPbEtherTypeSwapRowStatus));
            VlanSelectContext (u4CurrContextId);

            return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbEtherTypeSwapRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4FsPbPort), i4FsPbLocalEtherType,
                      i4SetValFsPbEtherTypeSwapRowStatus));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbRelayEtherType
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType

                The Object 
                testValFsPbRelayEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbRelayEtherType (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                             INT4 i4FsPbLocalEtherType,
                             INT4 i4TestValFsPbRelayEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_ETHER_TYPE_VALID (i4FsPbLocalEtherType) != VLAN_TRUE) ||
        (VLAN_IS_ETHER_TYPE_VALID (i4TestValFsPbRelayEtherType) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    pEtherTypeSwapEntry
        = VlanPbGetEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                       (UINT2) i4FsPbLocalEtherType);

    if (pEtherTypeSwapEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pEtherTypeSwapEntry->u1RowStatus == VLAN_ACTIVE)
    {
        CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPbEtherTypeSwapRowStatus
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType

                The Object 
                testValFsPbEtherTypeSwapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbEtherTypeSwapRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                     INT4 i4FsPbLocalEtherType,
                                     INT4 i4TestValFsPbEtherTypeSwapRowStatus)
{
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((VLAN_IS_PORT_VALID ((UINT2) i4FsPbPort) != VLAN_TRUE) ||
        (VLAN_IS_ETHER_TYPE_VALID (i4FsPbLocalEtherType) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4FsPbPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4FsPbPort);

    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pEtherTypeSwapEntry
        = VlanPbGetEtherTypeSwapEntry ((UINT2) i4FsPbPort,
                                       (UINT2) i4FsPbLocalEtherType);

    switch (i4TestValFsPbEtherTypeSwapRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
            if (pEtherTypeSwapEntry != NULL)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* Ethertype Swap configurations are not allowed on SISP ports */
            if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4FsPbPort)) ==
                VCM_TRUE)
            {
                CLI_SET_ERR (CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case VLAN_ACTIVE:
            if (pEtherTypeSwapEntry == NULL)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (pEtherTypeSwapEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case VLAN_DESTROY:
            if (pEtherTypeSwapEntry == NULL)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case VLAN_NOT_IN_SERVICE:
            if (pEtherTypeSwapEntry == NULL)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (pEtherTypeSwapEntry->u1RowStatus != VLAN_ACTIVE)
            {
                CLI_SET_ERR (CLI_PB_CONF_FAIL_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbEtherTypeSwapTable
 Input       :  The Indices
                FsPbPort
                FsPbLocalEtherType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbEtherTypeSwapTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbSVlanConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbSVlanConfigTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbSVlanConfigTable (UINT4 u4Dot1qVlanIndex)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qVlanIndex) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4Dot1qVlanIndex);

    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbSVlanConfigTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbSVlanConfigTable (UINT4 *pu4Dot1qVlanIndex)
{
    return (nmhGetNextIndexFsPbSVlanConfigTable (0, pu4Dot1qVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbSVlanConfigTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbSVlanConfigTable (UINT4 u4Dot1qVlanIndex,
                                     UINT4 *pu4NextDot1qVlanIndex)
{
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId             VlanId = 0;
    tVlanId             VlanStartId = 0;
    UINT1               u1EntryFound = VLAN_FALSE;
    UINT4               u4NextVlanIndex = 0;

    *pu4NextDot1qVlanIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qVlanIndex > VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qVlanIndex == 0)
    {
        VlanStartId = VLAN_START_VLAN_INDEX ();

        pCurrEntry = VlanGetVlanEntry (VlanStartId);

        if (pCurrEntry == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        VlanStartId = (tVlanId) u4Dot1qVlanIndex;

        pCurrEntry = VlanGetVlanEntry (VlanStartId);

        if (pCurrEntry == NULL)
        {
            VlanStartId = VLAN_START_VLAN_INDEX ();
            pCurrEntry = VlanGetVlanEntry (VlanStartId);

            if (pCurrEntry == NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }

    /* Search the Next valid  VLAN from the given VLAN */
    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (VlanId <= u4Dot1qVlanIndex)
        {
            continue;
        }
        if (pCurrEntry != NULL)
        {
            u4NextVlanIndex = pCurrEntry->VlanId;
            u1EntryFound = VLAN_TRUE;
            break;
        }
    }

    /* No greater entry found, return failure */
    if (u1EntryFound == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    *pu4NextDot1qVlanIndex = u4NextVlanIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbSVlanConfigServiceType
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                retValFsPbSVlanConfigServiceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbSVlanConfigServiceType (UINT4 u4Dot1qVlanIndex,
                                  INT4 *pi4RetValFsPbSVlanConfigServiceType)
{
    UINT1               u1ServiceType;

    if (VlanL2IwfPbGetVlanServiceType (VLAN_CURR_CONTEXT_ID (),
                                       (tVlanId) u4Dot1qVlanIndex,
                                       &u1ServiceType) == L2IWF_SUCCESS)
    {
        *pi4RetValFsPbSVlanConfigServiceType = (INT4) u1ServiceType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbSVlanConfigServiceType
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                setValFsPbSVlanConfigServiceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbSVlanConfigServiceType (UINT4 u4Dot1qVlanIndex,
                                  INT4 i4SetValFsPbSVlanConfigServiceType)
{
    return (VlanPbSetSVlanConfigServiceType
            (u4Dot1qVlanIndex, i4SetValFsPbSVlanConfigServiceType));
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPbSVlanConfigServiceType
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValFsPbSVlanConfigServiceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPbSVlanConfigServiceType
    (UINT4 *pu4ErrorCode, UINT4 u4Dot1qVlanIndex,
     INT4 i4TestValFsPbSVlanConfigServiceType)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!(VLAN_PB_802_1AD_AH_BRIDGE ()))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qVlanIndex) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbSVlanConfigServiceType != VLAN_E_LAN) &&
        (i4TestValFsPbSVlanConfigServiceType != VLAN_E_LINE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4Dot1qVlanIndex);

    if (pVlanCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        return SNMP_FAILURE;
    }

    /* 
     * If the number of member ports is more than two, then 
     * then don't allow E-Line service type configuration..
     */
    if (VlanPbIsServiceTypeValid ((tVlanId) u4Dot1qVlanIndex,
                                  (UINT1) i4TestValFsPbSVlanConfigServiceType)
        == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PB_SERVICE_TYPE_CONF_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbSVlanConfigServiceType == VLAN_E_LINE)
    {
        if (VlanL2VpnIsMplsIfPresent ((tVlanId) u4Dot1qVlanIndex) != VLAN_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PB_L2VPN_INVALID_VLAN_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbSVlanConfigTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbSVlanConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbTunnelProtocolTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbTunnelProtocolTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsPbTunnelProtocolTable (INT4 i4FsPbPort)
{
    UNUSED_PARAM (i4FsPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbTunnelProtocolTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbTunnelProtocolTable (INT4 *pi4FsPbPort)
{
    UNUSED_PARAM (pi4FsPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbTunnelProtocolTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbTunnelProtocolTable (INT4 i4FsPbPort, INT4 *pi4NextFsPbPort)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4NextFsPbPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolDot1x
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolDot1x
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolDot1x (INT4 i4FsPbPort,
                               INT4 *pi4RetValFsPbTunnelProtocolDot1x)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4RetValFsPbTunnelProtocolDot1x);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolLacp
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolLacp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolLacp (INT4 i4FsPbPort,
                              INT4 *pi4RetValFsPbTunnelProtocolLacp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4RetValFsPbTunnelProtocolLacp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolStp
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolStp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolStp (INT4 i4FsPbPort,
                             INT4 *pi4RetValFsPbTunnelProtocolStp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4RetValFsPbTunnelProtocolStp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolGvrp
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolGvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolGvrp (INT4 i4FsPbPort,
                              INT4 *pi4RetValFsPbTunnelProtocolGvrp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4RetValFsPbTunnelProtocolGvrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolGmrp
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolGmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolGmrp (INT4 i4FsPbPort,
                              INT4 *pi4RetValFsPbTunnelProtocolGmrp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4RetValFsPbTunnelProtocolGmrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolIgmp
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolIgmp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolIgmp (INT4 i4FsPbPort,
                              INT4 *pi4RetValFsPbTunnelProtocolIgmp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4RetValFsPbTunnelProtocolIgmp);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPbTunnelProtocolDot1x
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbTunnelProtocolDot1x
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelProtocolDot1x (INT4 i4FsPbPort,
                               INT4 i4SetValFsPbTunnelProtocolDot1x)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4SetValFsPbTunnelProtocolDot1x);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelProtocolLacp
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbTunnelProtocolLacp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelProtocolLacp (INT4 i4FsPbPort,
                              INT4 i4SetValFsPbTunnelProtocolLacp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4SetValFsPbTunnelProtocolLacp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelProtocolStp
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbTunnelProtocolStp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelProtocolStp (INT4 i4FsPbPort,
                             INT4 i4SetValFsPbTunnelProtocolStp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4SetValFsPbTunnelProtocolStp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelProtocolGvrp
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbTunnelProtocolGvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelProtocolGvrp (INT4 i4FsPbPort,
                              INT4 i4SetValFsPbTunnelProtocolGvrp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4SetValFsPbTunnelProtocolGvrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelProtocolGmrp
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbTunnelProtocolGmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelProtocolGmrp (INT4 i4FsPbPort,
                              INT4 i4SetValFsPbTunnelProtocolGmrp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4SetValFsPbTunnelProtocolGmrp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPbTunnelProtocolIgmp
 Input       :  The Indices
                FsPbPort

                The Object 
                setValFsPbTunnelProtocolIgmp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbTunnelProtocolIgmp (INT4 i4FsPbPort,
                              INT4 i4SetValFsPbTunnelProtocolIgmp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4SetValFsPbTunnelProtocolIgmp);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelProtocolDot1x
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbTunnelProtocolDot1x
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelProtocolDot1x (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                  INT4 i4TestValFsPbTunnelProtocolDot1x)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4TestValFsPbTunnelProtocolDot1x);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelProtocolLacp
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbTunnelProtocolLacp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelProtocolLacp (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                 INT4 i4TestValFsPbTunnelProtocolLacp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4TestValFsPbTunnelProtocolLacp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelProtocolStp
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbTunnelProtocolStp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelProtocolStp (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                INT4 i4TestValFsPbTunnelProtocolStp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4TestValFsPbTunnelProtocolStp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelProtocolGvrp
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbTunnelProtocolGvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelProtocolGvrp (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                 INT4 i4TestValFsPbTunnelProtocolGvrp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4TestValFsPbTunnelProtocolGvrp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelProtocolGmrp
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbTunnelProtocolGmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelProtocolGmrp (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                 INT4 i4TestValFsPbTunnelProtocolGmrp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4TestValFsPbTunnelProtocolGmrp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbTunnelProtocolIgmp
 Input       :  The Indices
                FsPbPort

                The Object 
                testValFsPbTunnelProtocolIgmp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbTunnelProtocolIgmp (UINT4 *pu4ErrorCode, INT4 i4FsPbPort,
                                 INT4 i4TestValFsPbTunnelProtocolIgmp)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (i4TestValFsPbTunnelProtocolIgmp);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbTunnelProtocolTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbTunnelProtocolStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbTunnelProtocolStatsTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbTunnelProtocolStatsTable (INT4 i4FsPbPort)
{
    UNUSED_PARAM (i4FsPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbTunnelProtocolStatsTable
 Input       :  The Indices
                FsPbPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbTunnelProtocolStatsTable (INT4 *pi4FsPbPort)
{
    UNUSED_PARAM (pi4FsPbPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbTunnelProtocolStatsTable
 Input       :  The Indices
                FsPbPort
                nextFsPbPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbTunnelProtocolStatsTable (INT4 i4FsPbPort,
                                             INT4 *pi4NextFsPbPort)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pi4NextFsPbPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolDot1xPktsRecvd
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolDot1xPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolDot1xPktsRecvd (INT4 i4FsPbPort,
                                        UINT4
                                        *pu4RetValFsPbTunnelProtocolDot1xPktsRecvd)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolDot1xPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolDot1xPktsSent
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolDot1xPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolDot1xPktsSent (INT4 i4FsPbPort,
                                       UINT4
                                       *pu4RetValFsPbTunnelProtocolDot1xPktsSent)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolDot1xPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolLacpPktsRecvd
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolLacpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolLacpPktsRecvd (INT4 i4FsPbPort,
                                       UINT4
                                       *pu4RetValFsPbTunnelProtocolLacpPktsRecvd)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolLacpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolLacpPktsSent
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolLacpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolLacpPktsSent (INT4 i4FsPbPort,
                                      UINT4
                                      *pu4RetValFsPbTunnelProtocolLacpPktsSent)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolLacpPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolStpPDUsRecvd
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolStpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolStpPDUsRecvd (INT4 i4FsPbPort,
                                      UINT4
                                      *pu4RetValFsPbTunnelProtocolStpPDUsRecvd)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolStpPDUsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolStpPDUsSent
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolStpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolStpPDUsSent (INT4 i4FsPbPort,
                                     UINT4
                                     *pu4RetValFsPbTunnelProtocolStpPDUsSent)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolStpPDUsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolGvrpPDUsRecvd
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolGvrpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolGvrpPDUsRecvd (INT4 i4FsPbPort,
                                       UINT4
                                       *pu4RetValFsPbTunnelProtocolGvrpPDUsRecvd)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolGvrpPDUsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolGvrpPDUsSent
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolGvrpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolGvrpPDUsSent (INT4 i4FsPbPort,
                                      UINT4
                                      *pu4RetValFsPbTunnelProtocolGvrpPDUsSent)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolGvrpPDUsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolGmrpPktsRecvd
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolGmrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolGmrpPktsRecvd (INT4 i4FsPbPort,
                                       UINT4
                                       *pu4RetValFsPbTunnelProtocolGmrpPktsRecvd)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolGmrpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolGmrpPktsSent
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolGmrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolGmrpPktsSent (INT4 i4FsPbPort,
                                      UINT4
                                      *pu4RetValFsPbTunnelProtocolGmrpPktsSent)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolGmrpPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolIgmpPktsRecvd
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolIgmpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolIgmpPktsRecvd (INT4 i4FsPbPort,
                                       UINT4
                                       *pu4RetValFsPbTunnelProtocolIgmpPktsRecvd)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolIgmpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPbTunnelProtocolIgmpPktsSent
 Input       :  The Indices
                FsPbPort

                The Object 
                retValFsPbTunnelProtocolIgmpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbTunnelProtocolIgmpPktsSent (INT4 i4FsPbPort,
                                      UINT4
                                      *pu4RetValFsPbTunnelProtocolIgmpPktsSent)
{
    UNUSED_PARAM (i4FsPbPort);
    UNUSED_PARAM (pu4RetValFsPbTunnelProtocolIgmpPktsSent);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsPbPepExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbPepExtTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationSVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbPepExtTable (INT4 i4Dot1adPortNum,
                                         INT4 i4Dot1adCVidRegistrationSVid)
{
    return (nmhValidateIndexInstanceDot1adPepTable
            (i4Dot1adPortNum, i4Dot1adCVidRegistrationSVid));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbPepExtTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationSVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbPepExtTable (INT4 *pi4Dot1adPortNum,
                                 INT4 *pi4Dot1adCVidRegistrationSVid)
{
    return (nmhGetFirstIndexDot1adPepTable
            (pi4Dot1adPortNum, pi4Dot1adCVidRegistrationSVid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbPepExtTable
 Input       :  The Indices
                Dot1adPortNum
                nextDot1adPortNum
                Dot1adCVidRegistrationSVid
                nextDot1adCVidRegistrationSVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbPepExtTable (INT4 i4Dot1adPortNum,
                                INT4 *pi4NextDot1adPortNum,
                                INT4 i4Dot1adCVidRegistrationSVid,
                                INT4 *pi4NextDot1adCVidRegistrationSVid)
{
    return (nmhGetNextIndexDot1adPepTable
            (i4Dot1adPortNum, pi4NextDot1adPortNum,
             i4Dot1adCVidRegistrationSVid, pi4NextDot1adCVidRegistrationSVid));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbPepExtCosPreservation
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationSVid

                The Object 
                retValFsPbPepExtCosPreservation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbPepExtCosPreservation (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adCVidRegistrationSVid,
                                 INT4 *pi4RetValFsPbPepExtCosPreservation)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);

    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbPepExtCosPreservation =
        pVlanPbLogicalPortEntry->u1CosPreservation;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbPepExtCosPreservation
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationSVid

                The Object 
                setValFsPbPepExtCosPreservation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbPepExtCosPreservation (INT4 i4Dot1adPortNum,
                                 INT4 i4Dot1adCVidRegistrationSVid,
                                 INT4 i4SetValFsPbPepExtCosPreservation)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    pVlanPbLogicalPortEntry->u1CosPreservation =
        (UINT1) i4SetValFsPbPepExtCosPreservation;
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbPepExtCosPreservation, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      VLAN_GET_IFINDEX (i4Dot1adPortNum),
                      i4Dot1adCVidRegistrationSVid,
                      i4SetValFsPbPepExtCosPreservation));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbPepExtCosPreservation
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationSVid

                The Object 
                testValFsPbPepExtCosPreservation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbPepExtCosPreservation (UINT4 *pu4ErrorCode, INT4 i4Dot1adPortNum,
                                    INT4 i4Dot1adCVidRegistrationSVid,
                                    INT4 i4TestValFsPbPepExtCosPreservation)
{
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_PROVIDEREDGEBRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((VLAN_IS_PORT_VALID ((UINT2) i4Dot1adPortNum) != VLAN_TRUE) ||
        (VLAN_IS_VLAN_ID_VALID (i4Dot1adCVidRegistrationSVid) != VLAN_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbPepExtCosPreservation != VLAN_ENABLED) &&
        (i4TestValFsPbPepExtCosPreservation != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1adPortNum);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1adPortNum);
    if (pVlanPbPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbLogicalPortEntry =
        VlanPbGetLogicalPortEntry ((UINT2) i4Dot1adPortNum,
                                   (tVlanId) i4Dot1adCVidRegistrationSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_NO_PEP_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pVlanPbLogicalPortEntry->u1CosPreservation ==
        i4TestValFsPbPepExtCosPreservation)
    {
        return SNMP_SUCCESS;
    }

    /* Enabling COS preservation will not be allowed when Untag-PEP is 
     * set to VLAN_TRUE */
    if (pVlanPbLogicalPortEntry->u1UnTagPepSet == VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_PB_COS_PRESERVATION_UNTAGPEP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Enabling COS preservation will not be allowed when Untag-CEP is 
     * set to VLAN_TRUE */
    if (VlanPbGetUntaggedCepStatus ((UINT2) i4Dot1adPortNum,
                                    (tVlanId) i4Dot1adCVidRegistrationSVid)
        == VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_PB_COS_PRESERVATION_UNTAGPEP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbPepExtTable
 Input       :  The Indices
                Dot1adPortNum
                Dot1adCVidRegistrationSVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbPepExtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
