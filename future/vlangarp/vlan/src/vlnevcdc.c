/*************************************************************************
 *
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevcdc.c,v 1.8 2016/07/22 06:46:31 siva Exp $
 *
 * Description: This file contains CDCP API which are exported
 *               to other modules.
 *
 ****************************************************************************/
#ifndef __VLNEVCDC_C__
#define __VLNEVCDC_C__

#include "vlaninc.h"

static UINT1    gau1TxBuf[VLAN_EVB_CDCP_TLV_LEN] = {0};

/******************************************************************************/
/* Function Name      : VlanEvbCdcpPortReg                                    */
/*                                                                            */
/* Description        : This function fills the following information         */
/*                      Provides CDCP TLV information to LLDP to transmit out */
/*                      Registers call back function "VlanApiEvbCallBackFunc  */
/*                      (tLldpAppTlv * pLldpAppTlv)" with LLDP so that on     */
/*                      hearing upon the CDCP TLV from LLDP,to process them   */
/*                                                                            */
/* Input(s)           : pEvbUapIfEntry - UAP interface entry                  */
/*                                                                            */
/* Output(s)          : NONE                                                  */
/*                                                                            */
/* Return Value(s)    : VLAN_SUCCESS if the port registration is successful   */
/*                      VLAN_FAILURE otherwise                                */
/******************************************************************************/
INT4
VlanEvbCdcpPortReg (tEvbUapIfEntry * pEvbUapIfEntry)
{
    tLldpAppPortMsg    LldpAppPortMsg;
    UINT1               au1LldpNonTPMRMac[MAC_ADDR_LEN] =
    { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03 };

    MEMSET (&LldpAppPortMsg, 0, sizeof (tLldpAppPortMsg));
    MEMCPY (LldpAppPortMsg.au1LldpApplSpecMac, au1LldpNonTPMRMac,
	    MAC_ADDR_LEN);
    VLAN_EVB_CDCP_FILL_APPL_ID (LldpAppPortMsg.LldpAppId);

    LldpAppPortMsg.pAppCallBackFn     = VlanApiEvbCallBackFunc;
    LldpAppPortMsg.pu1TxAppTlv        = pEvbUapIfEntry->au1UapCdcpTlv;
    LldpAppPortMsg.u2TxAppTlvLen      = pEvbUapIfEntry->u2UapCdcpTlvLength;
    LldpAppPortMsg.bAppTlvTxStatus    = OSIX_TRUE;
    LldpAppPortMsg.u1AppTlvRxStatus   = OSIX_TRUE;
    LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;

    if (pEvbUapIfEntry->i4EvbSysEvbLldpTxEnable == VLAN_EVB_CDCP_LLDP_ENABLE)
    {
        VLAN_TRC (VLAN_EVB_TRC,CONTROL_PLANE_TRC, VLAN_NAME,
                "VlanEvbCdcpPortReg: Forming and sending CDCP tlv   \n");

        if (VlanPortEvbL2IwfApplPortRequest
                (pEvbUapIfEntry->u4UapIfIndex, &LldpAppPortMsg,
                 L2IWF_LLDP_APPL_PORT_REGISTER) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                      VLAN_NAME,"VlanEvbCdcpPortReg: "
                      "VlanPortEvbL2IwfApplPortRequest failed  for UAP %d\n",
                      pEvbUapIfEntry->u4UapIfIndex);
            return VLAN_FAILURE;
        }
        pEvbUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_RUNNING;
        VLAN_DUMP_TRC (VLAN_EVB_TRC, DUMP_TRC, VLAN_NAME,
                       pEvbUapIfEntry->au1UapCdcpTlv, 
                       pEvbUapIfEntry->u2UapCdcpTlvLength,
                       "VlanEvbCdcpPortReg: Packet dump: Port ID - %d\r\n",
                       pEvbUapIfEntry->u4UapIfIndex);
    }

    /* Update TLV Tx counters */
    pEvbUapIfEntry->UapStats.u4EvbTxCdcpCount =
        pEvbUapIfEntry->UapStats.u4EvbTxCdcpCount + VLAN_EVB_CDCP_ONE;

    return VLAN_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpPortDeReg
 *
 *  DESCRIPTION   : This function is used to De Register Appl from the
 *                  LLDP.
 *
 *  INPUT         : u4IfIndex - Port Number.
 *                  pEvbQueMsg - Structure to store the CDCP Appl Reg
 *                  Info.
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE
 *
 * **************************************************************************/
INT4
VlanEvbCdcpPortDeReg (tEvbUapIfEntry * pEvbUapIfEntry)
{
    tLldpAppPortMsg    LldpAppPortMsg;
    UINT1              au1LldpNonTPMRMac[MAC_ADDR_LEN] =
    { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03 };

    MEMSET (&LldpAppPortMsg, 0, sizeof (tLldpAppPortMsg));
    MEMCPY (LldpAppPortMsg.au1LldpApplSpecMac, au1LldpNonTPMRMac,
	    MAC_ADDR_LEN);

    /* If CDCP is already DeRegistered with LLDP , then 
     * Instance ID will be deleted , so to prevent 
     * that and also the event need not be sent to LLDP again
     * CDCP is Disabled */
    if(pEvbUapIfEntry->i4UapIfCdcpOperState ==
                             VLAN_EVB_UAP_CDCP_NOT_RUNNING)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC,
                VLAN_NAME,"VlanEvbCdcpPortDeReg:"
                "Already LLDP is deregistered and "
                "CDCP is not running on UAP - %d.\r\n",
                pEvbUapIfEntry->u4UapIfIndex);
        return  VLAN_SUCCESS;
    }

    if (VLAN_FAILURE == VlanPortLldpApiGetInstanceId (au1LldpNonTPMRMac,
                        &(LldpAppPortMsg.u4LldpInstSelector)))
    {
		VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
				VLAN_NAME,"VlanEvbCdcpPortDeReg:"
				"VlanPortLldpApiGetInstanceId failed  \n");
        return VLAN_FAILURE;
    }

    VLAN_EVB_CDCP_FILL_APPL_ID (LldpAppPortMsg.LldpAppId);

    LldpAppPortMsg.pAppCallBackFn     = NULL;
    LldpAppPortMsg.u1AppPDURecv       = OSIX_FALSE;
    LldpAppPortMsg.u1AppTlvRxStatus   = OSIX_FALSE;
    LldpAppPortMsg.bAppTlvTxStatus    = OSIX_FALSE;
    LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_FALSE;
  
    /* When cdcp is disable or Tlv-Select is given no form command,
     * the S-channels will still be in confirmed state in hybrd mode, 
     * Created S-channels will not be deleted in Dynamic Mode */
	if(VlanEvbCdcpProcessAgeout (pEvbUapIfEntry->u4UapIfIndex) == VLAN_FAILURE)
	{
		VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
				VLAN_NAME,"VlanEvbCdcpPortDeReg:"
				"VlanEvbCdcpProcessAgeout failed  \n");
		return  VLAN_FAILURE;
	}

    if(VlanPortEvbL2IwfApplPortRequest (pEvbUapIfEntry->u4UapIfIndex,
            &LldpAppPortMsg,
            L2IWF_LLDP_APPL_PORT_DEREGISTER) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                VLAN_NAME,"VlanEvbCdcpPortDeReg: "
                "VlanPortEvbL2IwfApplPortRequest failed for UAP %d\n",
                pEvbUapIfEntry->u4UapIfIndex);
        return VLAN_FAILURE;
    }

    /* For Dynamic Mode , i4UapIfCdcpOperState will be updated in
     * VlanEvbSchIfDelDynamicEntry */
    if (pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
    {
        pEvbUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
    }
    return VLAN_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpPortUpdate
 *
 *  DESCRIPTION   : This function is used to update the CDCP TLV information 
                    that has to be transmitted out on the UAP.
 *  PRE_REQUISITE   New (Scid,SVID) pair is added or(SCID, SVID)pair is deleted.
 *
 *  INPUT         : pEvbUapIfEntry . UAP interface entry
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS if the update is successful
                    VLAN_FAILURE otherwise
 *
 * **************************************************************************/
INT4
VlanEvbCdcpPortUpdate (tEvbUapIfEntry * pEvbUapIfEntry)
{
    tLldpAppPortMsg    LldpAppPortMsg;
    UINT1              au1LldpNonTPMRMac[MAC_ADDR_LEN] =
    { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03 };

    MEMSET (&LldpAppPortMsg, 0, sizeof (tLldpAppPortMsg));

    if (VLAN_FAILURE == VlanPortLldpApiGetInstanceId (au1LldpNonTPMRMac,
                        &(LldpAppPortMsg.u4LldpInstSelector)))
    {
		VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
				VLAN_NAME,"VlanEvbCdcpPortUpdate :"
				"VlanPortLldpApiGetInstanceId failed  \n");
        return VLAN_FAILURE;
    }
    
    VLAN_EVB_CDCP_FILL_APPL_ID (LldpAppPortMsg.LldpAppId);

    LldpAppPortMsg.pAppCallBackFn     = VlanApiEvbCallBackFunc;
    LldpAppPortMsg.pu1TxAppTlv        = pEvbUapIfEntry->au1UapCdcpTlv;
    LldpAppPortMsg.u2TxAppTlvLen      = pEvbUapIfEntry->u2UapCdcpTlvLength;
    LldpAppPortMsg.bAppTlvTxStatus    = OSIX_TRUE;
    LldpAppPortMsg.u1AppTlvRxStatus   = OSIX_TRUE;
    LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;

    if(VLAN_FAILURE == VlanPortEvbL2IwfApplPortRequest 
                        (pEvbUapIfEntry->u4UapIfIndex,
                         &LldpAppPortMsg,
                         L2IWF_LLDP_APPL_PORT_UPDATE))
    {
       VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                VLAN_NAME,"VlanEvbCdcpPortUpdate: "
                "VlanPortEvbL2IwfApplPortRequest failed for UAP %d\n",
                pEvbUapIfEntry->u4UapIfIndex);
       VLAN_DUMP_TRC(VLAN_EVB_TRC, DUMP_TRC, VLAN_NAME, pEvbUapIfEntry->au1UapCdcpTlv,
                          pEvbUapIfEntry->u2UapCdcpTlvLength, 
                          "Packet Dump: Port ID: %d",
                          pEvbUapIfEntry->u4UapIfIndex);
        return VLAN_FAILURE;
    } 
    VLAN_DUMP_TRC (VLAN_EVB_TRC, DUMP_TRC, VLAN_NAME,
                   pEvbUapIfEntry->au1UapCdcpTlv, 
                   pEvbUapIfEntry->u2UapCdcpTlvLength,
                   "VlanEvbCdcpPortUpdate: Packet dump: Port ID: %d\r\n",
                   pEvbUapIfEntry->u4UapIfIndex);

    /* Update TLV Tx counters */
    pEvbUapIfEntry->UapStats.u4EvbTxCdcpCount =
        pEvbUapIfEntry->UapStats.u4EvbTxCdcpCount + VLAN_EVB_CDCP_ONE;

    return VLAN_SUCCESS;

}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpProcessTlv
 *
 *  DESCRIPTION   : This function is used to process CDCP TLV received on the 
 *                  UAP and take actions as following.
 *                  -For the accepted (Scid, SVID) pair, provision the S-Channel
 *                  interface in the hardware.
 *                  -Replies with new (Scid, SVID) pair if the server requests 
 *                   for (SCID,0)
 *                  -Deletes the S-Channel interface entry if it is missed in 
 *                   the received TLV and updates the pair list.
 *                  -Replies with new (Scid, SVID) pair if the server requests
 *                   for (SCID, X)where X is not available and hence the next 
 *                   available SVID is taken from the pool. This is applicable 
 *                   only for dynamic mode.In Hybrid mode, the i4NegoStatus in 
 *                   tEvbSChIfEntry to be updated with 'negotiating' when an 
 *                   S-Channel entry request is received from the station
 *                   and it has to be updated with 'confirmed' after the station
 *                   confirmed and the S-Channel entry is provisioned in the 
 *                   hardware. 
 *                   The au1UapCdcpTlv in tEvbUapIfEntry is updated whenever the
 *                   requests accepted/rejected.
 *
 *
 *  INPUT         : pMsg - Queue message received from VLAN queue.
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE
 *
 * **************************************************************************/
INT4
VlanEvbCdcpProcessTlv (tEvbQueMsg * pMsg)
{
    tEvbUapIfEntry  *pEvbUapIfEntry = NULL;
    UINT1            *pu1PktBuf     = pMsg->EvbTlvParam.au1EvbTlv;
    UINT1            au1OUI[VLAN_EVB_CDCP_THREE] = { 0 };
    UINT4            u4ChnCapCount       = 0; 
    UINT2            u2TlvHdr            = 0;
    UINT2            u2Length            = 0;
    UINT1            u1SubType           = 0;
    UINT4            u4RRSRC             = 0;

    VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
            "VlanEvbCdcpProcessTlv:Hit for type %d on UAP %d\r\n",
            pMsg->u4MsgType, pMsg->u4IfIndex);

    switch (pMsg->u4MsgType)
   {
        case L2IWF_LLDP_APPL_TLV_AGED:
             if(VlanEvbCdcpProcessAgeout (pMsg->u4IfIndex) == VLAN_FAILURE)

             {
                 VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         VLAN_NAME,"VlanEvbCdcpProcessTlv:"
                         "VlanEvbCdcpProcessAgeout failed  \n");
                 VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
                 return  VLAN_FAILURE;
             }

            VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
            return VLAN_SUCCESS;

        case L2IWF_LLDP_APPL_RE_REG:
            if(VlanEvbCdcpPortReReg () == VLAN_FAILURE)

            {
                VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        VLAN_NAME,"VlanEvbCdcpProcessTlv:"
                        "VlanEvbCdcpPortReReg failed  \n");
                VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
                return  VLAN_FAILURE;
            }

            VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
            return VLAN_SUCCESS;

        case L2IWF_LLDP_APPL_TLV_RECV:
            /* Packet processing will be done in this function */
            break;

        case L2IWF_LLDP_APPL_PDU_RECV:
        default:
            VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
            return VLAN_SUCCESS;
    }
 
    VLAN_DUMP_TRC (VLAN_EVB_TRC, DUMP_TRC, VLAN_NAME,
                   pMsg->EvbTlvParam.au1EvbTlv, 
                   pMsg->EvbTlvParam.u2RxTlvLen,
                   "VlanEvbCdcpProcessTlv: received packet dump: Port ID: %d\r\n",
                   pMsg->u4IfIndex);

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (pMsg->u4IfIndex);

    if (pEvbUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbCdcpProcessTlv:Pkt dropped - UAP %d not exists \n",
                pMsg->u4IfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }

    if (pEvbUapIfEntry->i4UapIfSchCdcpAdminEnable != VLAN_EVB_UAP_CDCP_ENABLE) 
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
             "VlanEvbCdcpProcessTlv:Pkt dropped - CDCP is disabled on UAP %d\n",
             pEvbUapIfEntry->u4UapIfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }

    /* Update TLV Rx counters */
    pEvbUapIfEntry->UapStats.u4EvbRxCdcpCount += 1;

    VLAN_EVB_GET_2BYTE (pu1PktBuf, u2TlvHdr);

    if (VLAN_EVB_CDCP_VALIDATE_TYPE (u2TlvHdr) != VLAN_EVB_LLDP_ORG_SPEC_TLV)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
            "VlanEvbCdcpProcessTlv:Pkt dropped - Invalid CDCP Type for UAP %d \n",
            pEvbUapIfEntry->u4UapIfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }
    
    u2Length = VLAN_EVB_CDCP_GET_LENGTH (u2TlvHdr);

    if ((u2Length > VLAN_EVB_CDCP_TLV_INFO_STR_LEN) ||
        (u2Length < VLAN_EVB_CDCP_MANDATORY))
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbCdcpProcessTlv:Pkt dropped - Invalid length pkt "
                "received on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }

    VLAN_EVB_GET_3BYTE (pu1PktBuf, au1OUI);

    if (VLAN_EVB_CDCP_VALIDATE_OUI (au1OUI) != VLAN_TRUE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbCdcpProcessTlv:Pkt dropped - Invalid OUI received"
                " on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }

    VLAN_EVB_GET_1BYTE (pu1PktBuf, u1SubType);

    if (VLAN_EVB_CDCP_VALIDATE_SUBTYPE (u1SubType) != VLAN_TRUE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbCdcpProcessTlv:Pkt dropped - Invalid subtype received "
                "on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }

    VLAN_EVB_GET_4BYTE (pu1PktBuf, u4RRSRC);

	if (VLAN_EVB_CDCP_VALIDATE_ROLE (u4RRSRC) != VLAN_TRUE)
	{
		/* Important: If peer is bridge, then Initiate Ageout.
		 * When peer is bridge , Only the CDCP state is moved 
		 * to Not_RUNNING, CDCP is Transmitted Unconditionally 
		 * with Default Pair */ 
		if (pEvbUapIfEntry->i4UapIfSchCdcpRemoteRole == VLAN_EVB_ROLE_BRIDGE)
		{
        /* If the Remote Role is already Bridge , sync should not be sent to standby*/
			VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
					"VlanEvbCdcpProcessTlv:Pkt dropped - Peer not station received "
					"on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
			VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
			return VLAN_FAILURE;

		}
		else
		{ 
         /* If the Remote Role is station , sync is ent to standby  and Role is changed 
          * to Bridge and CDCP state as NOT-RUNNING */ 
			pEvbUapIfEntry->i4UapIfSchCdcpRemoteRole = VLAN_EVB_ROLE_BRIDGE;
			pEvbUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
            if (VlanEvbCdcpProcessAgeout(pEvbUapIfEntry->u4UapIfIndex) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                                "VlanEvbCdcpProcessTlv: VlanEvbCdcpProcessAgeout Failed for UAP "
                                "on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
            }
#ifdef L2RED_WANTED
			/* RemoteRole is changed, so sync up the
			 * TLV change with STANDBY node */
			if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_CDCP_REMOTE_ROLE,
						(VOID *) pEvbUapIfEntry) == VLAN_FAILURE)
			{
				VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
						"VlanEvbCdcpProcessTLV: RemoteRole change sync up failed on the standby\n");
			}
#endif
			VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
					"VlanEvbCdcpProcessTlv:Pkt dropped - Peer not station received "
					"on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
			VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
			return VLAN_FAILURE;
		}
	}
	else
	{
		if (pEvbUapIfEntry->i4UapIfSchCdcpRemoteRole == VLAN_EVB_ROLE_STATION)
		{
            /* If the RemoteRole is already station sync is not needed */
			/* Peer role is station and hence the packet is accepted. */
			pEvbUapIfEntry->i4UapIfSchCdcpRemoteRole = VLAN_EVB_ROLE_STATION;
			pEvbUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_RUNNING;
		}
		else
		{
            /* If the Previous remote role is bridge and station packet comes,
             * sync is sent to standby */
			pEvbUapIfEntry->i4UapIfSchCdcpRemoteRole = VLAN_EVB_ROLE_STATION;
			pEvbUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_RUNNING;
#ifdef L2RED_WANTED
			/* RemoteRole is changed, so sync up the
			 * TLV change with STANDBY node */
			if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_CDCP_REMOTE_ROLE,
						(VOID *) pEvbUapIfEntry) == VLAN_FAILURE)
			{
				VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
						"VlanEvbCdcpProcessTLV: RemoteRole change sync up failed on the standby\n");
			}
#endif
		}
	}
    
    if (VLAN_EVB_CDCP_VALIDATE_SCOMP (u4RRSRC) != VLAN_TRUE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbCdcpProcessTlv:Pkt dropped - SComp is not set for pkt "
                  "received on UAP %d\n", pEvbUapIfEntry->u4UapIfIndex);
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
        return VLAN_FAILURE;
    }
    /* what is the value to be maintained */
    if (VLAN_EVB_CDCP_VALIDATE_CHANCAP (u4RRSRC, u4ChnCapCount, 
            (UINT4)pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap) != VLAN_TRUE)
    {
    }
    if (pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap < (INT4)u4ChnCapCount)
    {
        u4ChnCapCount = (UINT4)pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap;
        UNUSED_PARAM(u4ChnCapCount);
    }
    
    u2Length = (UINT2)(u2Length - VLAN_EVB_CDCP_MANDATORY);

    if (u2Length > 0)
    {
        if (pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
        {
            VlanEvbCdcpProcessHybridPairs (pu1PktBuf, pEvbUapIfEntry, u2Length);
        }
        else if(pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
        {
            VlanEvbCdcpProcessDynamicPairs (pu1PktBuf, pEvbUapIfEntry, u2Length);
        }
    }
    VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY, pMsg);
    return VLAN_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpProcessHybridPairs
 *
 *  DESCRIPTION   : This function processes the TLV pairs in hybrid mode and
 *                  takes the necessary action.
 *
 *  INPUT         : pu1RxBuf - Liner buffer pointer starting at SCID-SVID 
 *                           pair position.
 *                  pUapIfEntry - UAP interface entry.
 *                  u2Length - SCID-SVID pair length
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 * **************************************************************************/
VOID
VlanEvbCdcpProcessHybridPairs (UINT1 *pu1RxBuf, tEvbUapIfEntry *pUapIfEntry, 
                               UINT2 u2Length)
{
    tCfaIfInfo      CfaIfInfo;
    tEvbSChIfEntry  *pSChIfEntry   = NULL;
    UINT1           *pu1TxBuf      = NULL;
    UINT1           *pu1TypeHdr    = gau1TxBuf;
    UINT4           u4RxPair       = 0;     
    UINT4           u4TxPair       = 0;
    UINT2           u2TlvHeader    = 0;
    UINT2           u2InnerLoop    = 0;
    UINT2           u2OuterLoop    = 0;
    UINT2           u2OffSet       = 0;
    UINT2           u2TxLen        = 0; 
    UINT2           u2RxSVID       = 0;
    UINT2           u2RxSCID       = 0;
    UINT2           u2TxSVID       = 0;
    UINT2           u2TxSCID       = 0;
    UINT1           u1NewPair      = VLAN_TRUE;

    MEMSET (gau1TxBuf, 0, VLAN_EVB_CDCP_TLV_LEN); 
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* Get the start of Tx TLV after the default pair presence */
    VLAN_EVB_CDCP_GET_TLV_START_PTR (pUapIfEntry->au1UapCdcpTlv, pu1TxBuf);
    
    /* Move the default pair in Rx TLV */
    VLAN_EVB_GET_3BYTE (pu1RxBuf, (UINT1 *)(VOID *)&u4RxPair);
    VLAN_EVB_PARSE_PAIR (u4RxPair, u2RxSCID, u2RxSVID);

    if ((u2RxSCID != VLAN_EVB_DEF_SCID) || (u2RxSVID != VLAN_EVB_DEF_SVID))
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC,  VLAN_NAME,
                  "VlanEvbCdcpProcessHybridPairs:Pkt dropped - "
                  "Default pair not present\n");
        return;
    }

    /* gau1TxBuf is used to construct the updated TLV */
    MEMCPY (gau1TxBuf, pUapIfEntry->au1UapCdcpTlv,
            VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
    u2TxLen = VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR;
    u2OffSet = VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR;

    /* Scanning through the SCID-SVID pairs in the incoming packet */
    for (u2OuterLoop = VLAN_EVB_CDCP_THREE; u2OuterLoop < (u2Length - 1); 
         u2OuterLoop = (UINT2)(u2OuterLoop + VLAN_EVB_CDCP_THREE),
          u2OffSet = (UINT2) (u2OffSet + VLAN_EVB_CDCP_THREE))
    {
        /* It is expected that the pair is considered as new always as
         * initialized at declaration */
        u1NewPair = VLAN_TRUE;
        /* This offset is used when adding TLVs in TxBuf. It has to be
         * decremented by 3 when the TLV from RxBuf is discarded. */

        VLAN_EVB_GET_3BYTE (pu1RxBuf, (UINT1 *)(VOID *)&u4RxPair);
        VLAN_EVB_PARSE_PAIR (u4RxPair, u2RxSCID, u2RxSVID);

        if ((u2RxSCID == 0) || (u2RxSCID == VLAN_EVB_DEF_SCID) ||
            (u2RxSCID > VLAN_EVB_MAX_SCID))
        {
            /* SCID must not be zero hence dropping this request or 
             * default SCID-SVID pair is not required to be proceessed.
             * Start processing the next pair.
             * Note: default pair validation is added here if somewhere
             * in middle of TLV, default pair might present which requires
             * to be discarded.*/
            u2OffSet = (UINT2)(u2OffSet - VLAN_EVB_CDCP_THREE); 
            continue;
        }
        /* New request has received for this u2SCID. second loop index
         * is reduced by three bytes since default pair is not accounted.
         */
        
        for (u2InnerLoop = 0; u2InnerLoop < 
            (pUapIfEntry->u2UapCdcpTlvLength - VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
             u2InnerLoop =(UINT2) (u2InnerLoop + VLAN_EVB_CDCP_THREE))
        {
            MEMCPY (((UINT1 *)(VOID *)&u4TxPair), (pu1TxBuf + u2InnerLoop), 
                    VLAN_EVB_CDCP_THREE);

            VLAN_EVB_PARSE_PAIR (u4TxPair, u2TxSCID, u2TxSVID);
           
            if (u2RxSCID == u2TxSCID)
            {
                /* Pair is already present in Tx TLV. 
                 * In Tx pair, u2TxSVID will never be zero.
                 * Do nothing */
                
                u1NewPair = VLAN_FALSE;
                break;
            }
        }

        if (u2RxSVID == 0)
        {
            pSChIfEntry = VlanEvbSChScidIfGetEntry 
                (pUapIfEntry->u4UapIfIndex, (UINT4) u2RxSCID);
            if (pSChIfEntry == NULL)
            {
                /* No static entries present.Discard the pair*/
                u2OffSet = (UINT2)(u2OffSet - VLAN_EVB_CDCP_THREE); 
                /* Update TLV S-Channel allocation failed count */
                pUapIfEntry->UapStats.u4EvbCdcpRejectStationReq +=1;
                continue;
            }
            /* The check is added as != CONFIRMED, since same (SCID,0)
             * request might come multiple times. In each case, we need
             * to give the proper SVID and its status to be set as
             * NEGOTIATING. */
            if (pSChIfEntry->i4NegoStatus != VLAN_EVB_SCH_CONFIRMED)
            {
                VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, u2OffSet, 
                               (UINT4)u2RxSCID, (UINT4)pSChIfEntry->u4SVId);
                u2TxLen = (UINT2) (u2TxLen + VLAN_EVB_CDCP_THREE);
                pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_NEGOTIATING;
            }
        }
        else /* u2RxSVID is non zero */
        {
            pSChIfEntry = VlanEvbSChScidIfGetEntry 
                (pUapIfEntry->u4UapIfIndex, (UINT4) u2RxSCID);
            if (pSChIfEntry == NULL)
            {
                /* No static entries present.Discard the pair*/
                u2OffSet =(UINT2) (u2OffSet - VLAN_EVB_CDCP_THREE);
                /* Update TLV S-Channel allocation failed count */
                pUapIfEntry->UapStats.u4EvbCdcpRejectStationReq +=1; 
                continue;
            }
            if (u1NewPair == VLAN_TRUE)
            {
                if (pSChIfEntry->u4SVId != u2RxSVID)
                {
                    /* Update TLV S-Channel allocation failed count */
                    pUapIfEntry->UapStats.u4EvbCdcpRejectStationReq +=1;  
                        u2OffSet =(UINT2) (u2OffSet - VLAN_EVB_CDCP_THREE);
                    continue;
                }
              
                else
                {
                    
                    VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, u2OffSet, 
                            (UINT4)u2RxSCID, (UINT4)pSChIfEntry->u4SVId);
                    u2TxLen =(UINT2)(u2TxLen + VLAN_EVB_CDCP_THREE);
                    pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_NEGOTIATING;
                }
            }
         

            else /* Existing pair */
            {
                if (u2RxSVID != u2TxSVID)
                {
                    pSChIfEntry = VlanEvbSChIfGetEntry
                        (pUapIfEntry->u4UapIfIndex, (UINT4) u2TxSVID);
                    if (pSChIfEntry == NULL)
                    {  
                       
                        u2OffSet =(UINT2)(u2OffSet - VLAN_EVB_CDCP_THREE);
                        /*Update TLV S-Channel allocation failed count */
                        pUapIfEntry->UapStats.u4EvbCdcpRejectStationReq +=1;
                    }
                    else
                    {
                        VlanPortCfaUpdateSChannelOperStatus 
                                ((UINT4) pSChIfEntry->i4SChIfIndex,
                                CFA_IF_DOWN);
                        pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_FREE;
                        pSChIfEntry->u1OperStatus = CFA_IF_DOWN;

                        if (VlanEvbHwConfigSChInterface
                              (pSChIfEntry, VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK) ==
                                VLAN_FAILURE)
                        {
                            /* Critical place */
                          VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | 
                                  ALL_FAILURE_TRC, VLAN_NAME, 
                                 "VlanEvbCdcpProcessHybridPairs:"
                                  "Hw Block failed: UAP %d, SVID %d, "
                                  "SChIfIndex %d \n",pSChIfEntry->u4UapIfIndex,
                                   pSChIfEntry->u4SVId, 
                                    pSChIfEntry->i4SChIfIndex);
                          pUapIfEntry->UapStats.u4EvbSchActiveFailCount +=1;
                       }
#ifdef SYSLOG_WANTED
                        SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                        "SBP oper Status is [UP] for SCID-SVID (%d - %d) on the " 
                        "UAP port %d", pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex));
#endif
#ifdef L2RED_WANTED
                    /* Updating the S-Channel state information
                     * to STANDBY node since moving from Confirmed to free  */
                      if (VlanEvbRedSendDynamicUpdates 
                         ((UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                                (VOID *) pSChIfEntry) == VLAN_FAILURE)
                      {
                        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                "VlanEvbCdcpProcessHybridPairs: S-Channel"
                                " Negotiation status sync up "
                                "failed on the standby\n");
                      }

#endif
                       u2OffSet = (UINT2)(u2OffSet - VLAN_EVB_CDCP_THREE);
                     /* Update TLV S-Channel allocation failed count */
                       pUapIfEntry->UapStats.u4EvbCdcpRejectStationReq +=1;
                    }
                         continue;
                }

                if (pSChIfEntry->i4NegoStatus == VLAN_EVB_SCH_NEGOTIATING)
                {
                    pSChIfEntry->u1OperStatus = CFA_IF_UP;
                    VlanCfaGetIfInfo ((UINT4)pSChIfEntry->i4SChIfIndex, 
                                       &CfaIfInfo);
                    pSChIfEntry->u1AdminStatus = CfaIfInfo.u1IfAdminStatus;

                    if ( pSChIfEntry->u1AdminStatus == CFA_IF_UP)
                    {
                        /* Indicate to other Modules the Oper Up indication
                         * for SBP Port */
                        VlanPortCfaUpdateSChannelOperStatus ((UINT4)
                                pSChIfEntry->i4SChIfIndex,
                                pSChIfEntry->u1OperStatus);

                        /* Add in Hardware */
                        /* Allow Traffic To Forward Now*/
                                               
                        if (VlanEvbHwConfigSChInterface
                                (pSChIfEntry, VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD) == 
                                VLAN_FAILURE)
                        {
                            /* Critical place */
                            VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                                    VLAN_NAME, "VlanEvbCdcpProcessHybridPairs:"
                                    "Hw Addition failed: UAP %d, SVID %d, "
                                    "SChIfIndex %d \n",pSChIfEntry->u4UapIfIndex,
                                    pSChIfEntry->u4SVId, pSChIfEntry->i4SChIfIndex);
                                u2OffSet = (UINT2)(u2OffSet - VLAN_EVB_CDCP_THREE);
                           /* Update TLV S-Channel activation failed count */
                           pUapIfEntry->UapStats.u4EvbSchActiveFailCount +=1;
                       }
#ifdef SYSLOG_WANTED
           SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL, (UINT4)gi4VlanSysLogId,
                        "SBP oper Status is [UP] for SCID-SVID (%d - %d) on the UAP port %d",
                        pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex));
#endif
                   }
                    pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_CONFIRMED;
                    if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) && 
                            (pSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID))
                    {
                        pUapIfEntry->pEvbLldpSChannelIfNotify
                            (pSChIfEntry->u4UapIfIndex,
                             pSChIfEntry->u4SVId,VLAN_EVB_SVID_UPDATE);
                    }
#ifdef L2RED_WANTED
                    /* Updating the S-Channel state information
                     * to STANDBY node  */
                    if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                                (VOID *) pSChIfEntry) == VLAN_FAILURE)
                    {
                        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                "VlanEvbCdcpProcessHybridPairs: S-Channel"
                                " Negotiation status sync up "
                                "failed on the standby\n");
                    }
#endif
                }
                VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, u2OffSet, 
                        (UINT4)u2RxSCID, (UINT4)pSChIfEntry->u4SVId);
                u2TxLen =(UINT2)(u2TxLen + VLAN_EVB_CDCP_THREE);
            } /* End of Else - existing pair */
        } /* End of if - else for u2RxSVID validation */
    } /* End of for - all pairs processed */

    /* Remove the deleted pairs */
    VlanEvbCdcpDeleteHybridPairs (pUapIfEntry, u2TxLen);
   
    /* Put proper length in CDCP TLV. */
    /* This macro will fill TLV type into first 7 bits of variable +
     * Length into next 9 bits of the variable */
    VLAN_EVB_CDCP_CONSTRUCT_TLV_HEADER (VLAN_EVB_LLDP_ORG_SPEC_TLV,
        (UINT2)(u2TxLen - VLAN_EVB_CDCP_TLV_HDR_LEN), &u2TlvHeader);
    VLAN_EVB_CDCP_PUT_2BYTE (pu1TypeHdr, u2TlvHeader);

    /* copy to transmit the modified buffer and length */
    MEMSET (pUapIfEntry->au1UapCdcpTlv, 0, VLAN_EVB_CDCP_TLV_LEN);
    MEMCPY (pUapIfEntry->au1UapCdcpTlv, gau1TxBuf, u2TxLen);
    pUapIfEntry->u2UapCdcpTlvLength = u2TxLen;

#ifdef L2RED_WANTED
    /* CDCP TLV is changed, so sync up the
     * TLV change with STANDBY node */
    if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_CDCP_TLV_CHANGE,
                             (VOID *) pUapIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
           "VlanEvbCdcpProcessiHybridPairs:TLV change sync up failed on the standby\n");
    }
#endif

    /* Transmit the updated TLVs through LLDP */
    VlanEvbPortPostMsgToCdcp (pUapIfEntry, VLAN_EVB_CDCP_LLDP_PORT_UPDATE);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpDeleteHybridPairs 
 *
 *  DESCRIPTION   : This function compares the gau1TxBuf to be transmitted now
 *                  and the old pair from pUapIfEntry->au1UapCdcpTlv and finds
 *                  the deleted pairs and remove them from the hardware.
 *
 *  INPUT         : pUapIfEntry - UAP Interface entry.
 *                  u2NewTxLen  - No of bytes filled in gau1TxBuf
 *                  gau1TxBuf is referred.
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 ****************************************************************************/
VOID
VlanEvbCdcpDeleteHybridPairs (tEvbUapIfEntry *pUapIfEntry, UINT2 u2NewTxLen)
{
    tEvbSChIfEntry  *pSChIfEntry = NULL;
    UINT1           *pu1OldBuf  = NULL;
    UINT1           *pu1NewBuf  = NULL;
    UINT4           u4OldPair   = 0;
    UINT4           u4NewPair   = 0;
    UINT2           u2OldSCID   = 0;
    UINT2           u2OldSVID   = 0;
    UINT2           u2NewSCID   = 0;
    UINT2           u2NewSVID   = 0;
    UINT2           u2OuterLoop = 0;
    UINT2           u2InnerLoop = 0;
    UINT1           u1DeleteFlag= VLAN_TRUE;

    /* Get the start of Tx TLV after the default pair presence */
    VLAN_EVB_CDCP_GET_TLV_START_PTR (pUapIfEntry->au1UapCdcpTlv, pu1OldBuf);

    for (u2OuterLoop = 0; u2OuterLoop < 
         (pUapIfEntry->u2UapCdcpTlvLength - VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
          u2OuterLoop =(UINT2)(u2OuterLoop + VLAN_EVB_CDCP_THREE))
    {
        VLAN_EVB_CDCP_GET_TLV_START_PTR (gau1TxBuf, pu1NewBuf);
        VLAN_EVB_GET_3BYTE (pu1OldBuf, (UINT1 *)(VOID *)&u4OldPair);
        VLAN_EVB_PARSE_PAIR (u4OldPair, u2OldSCID, u2OldSVID);
        UNUSED_PARAM(u2OldSVID);
        for (u2InnerLoop = 0; u2InnerLoop < 
             (u2NewTxLen - VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
             u2InnerLoop =((UINT2)(u2InnerLoop + VLAN_EVB_CDCP_THREE)))
        {
            VLAN_EVB_GET_3BYTE (pu1NewBuf, (UINT1 *)(VOID *)&u4NewPair);
            VLAN_EVB_PARSE_PAIR (u4NewPair, u2NewSCID, u2NewSVID);
            UNUSED_PARAM(u2NewSVID);
            if (u2OldSCID == u2NewSCID)
            {
                u1DeleteFlag = VLAN_FALSE;
                break;
            }
        }
        if (u1DeleteFlag == VLAN_TRUE)
        {
            pSChIfEntry = VlanEvbSChScidIfGetEntry (pUapIfEntry->u4UapIfIndex,
                                                    (UINT4) u2OldSCID);
            if (pSChIfEntry != NULL)
            {
                /* Entry would have programmed only in CONFIRMED state. 
                 * So delete the entry if its state is CONFIRMED. Otherwise
                 * simply setting its state as FREE */
                /* When the status changes from CONFIRMED State,the Traffic
                 * should not be forwarded*/ 
                if ((pSChIfEntry->i4NegoStatus == VLAN_EVB_SCH_CONFIRMED) &&
                        (VlanEvbHwConfigSChInterface (pSChIfEntry,
                           VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK) == VLAN_FAILURE))
                {
                    /* Critical place */
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbCdcpDeleteHybridPairs:"
                            "Hw traffic block failed: UAP %d, SVID %d, "
                            "SChIfIndex %d \n",pSChIfEntry->u4UapIfIndex,
                            pSChIfEntry->u4SVId, pSChIfEntry->i4SChIfIndex);
                    pUapIfEntry->UapStats.u4EvbSchActiveFailCount +=1;
                }

                VlanPortCfaUpdateSChannelOperStatus ((UINT4)
                                                     pSChIfEntry->i4SChIfIndex,
                                                     CFA_IF_DOWN);
                pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_FREE;
                pSChIfEntry->u1OperStatus = CFA_IF_DOWN;
                if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) &&
                                (pSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID))
                {
                    pUapIfEntry->pEvbLldpSChannelIfNotify
                            (pSChIfEntry->u4UapIfIndex,
                             pSChIfEntry->u4SVId,VLAN_EVB_SVID_DELETE);
                }
#ifdef SYSLOG_WANTED
           SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL, (UINT4)gi4VlanSysLogId,
                        "SBP oper Status is [DOWN] for SCID-SVID (%d - %d) on the UAP port %d",
                        pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex));
#endif
#ifdef L2RED_WANTED
                /* SBP entry is deleted, so sync up the
                 * SBP change with STANDBY node */
                if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_SBP_DEL_SYNC_UP,
                        (VOID *) pUapIfEntry) == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                            "VlanEvbCdcpDeleteHybridPairs: S-Channel"
                            "deletion sync up failed on the standby\n");
                }


              if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                      (VOID *) pSChIfEntry) == VLAN_FAILURE)
              {
                  VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "VlanEvbCdcpProcessHybridPairs: New S-Channel entry"
                          "creation sync up failed on the standby\n");
              }

#endif
            }
        }
        /* For the next iteration , setting the flag as VLAN_TRUE */
        u1DeleteFlag = VLAN_TRUE;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpProcessDynamicPairs
 *
 *  DESCRIPTION   : This function processes the TLV pairs in dynamic mode and
 *                  takes the necessary action.
 *
 *  INPUT         : pu1RxBuf - Liner buffer pointer starting at SCID-SVID 
 *                           pair position.
 *                  pUapIfEntry - UAP interface entry.
 *                  u2Length - SCID-SVID pair length
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 * **************************************************************************/
VOID
VlanEvbCdcpProcessDynamicPairs (UINT1 *pu1RxBuf, tEvbUapIfEntry *pUapIfEntry, 
                               UINT2 u2Length)
{
    tEvbSChIfEntry  *pSChIfEntry   = NULL;
    UINT1           *pu1TxBuf      = NULL;
    UINT1           *pu1TypeHdr    = gau1TxBuf;
    UINT4           u4RxPair       = 0;     
    UINT4           u4TxPair       = 0;
    UINT2           u2TlvHeader    = 0;
    UINT2           u2InnerLoop    = 0;
    UINT2           u2OuterLoop    = 0;
    UINT2           u2OffSet       = 0;
    UINT2           u2RxSVID       = 0;
    UINT2           u2RxSCID       = 0;
    UINT2           u2TxSVID       = 0;
    UINT2           u2TxSCID       = 0;
    UINT1           u1NewPair      = VLAN_TRUE;
    UINT1           u1FailStats    = VLAN_TRUE;
    UINT1           u1PairCount    = 1; /* Since Default Pair is Present Always*/
    BOOL1           bFlag          = OSIX_FALSE;

    MEMSET (gau1TxBuf, 0, VLAN_EVB_CDCP_TLV_LEN); 
    /* Get the start of Tx TLV after the default pair presence */
    VLAN_EVB_CDCP_GET_TLV_START_PTR (pUapIfEntry->au1UapCdcpTlv, pu1TxBuf);
    
    /* Move the default pair in Rx TLV */
    VLAN_EVB_GET_3BYTE (pu1RxBuf, (UINT1 *)(VOID *)&u4RxPair);
    VLAN_EVB_PARSE_PAIR (u4RxPair, u2RxSCID, u2RxSVID);

    if ((u2RxSCID != VLAN_EVB_DEF_SCID) || (u2RxSVID != VLAN_EVB_DEF_SVID))
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbCdcpProcessDynamicPairs:Pkt dropped - "
                  "Default pair not present\n");
        return;
    }

    /* gau1TxBuf is used to construct the updated TLV */
    MEMCPY (gau1TxBuf, pUapIfEntry->au1UapCdcpTlv,
            VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
    u2OffSet = VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR;

    /* Scanning through the SCID-SVID pairs in the incoming packet */
    /* TODO add check for ChnCap in this for loop*/
    for (u2OuterLoop = VLAN_EVB_CDCP_THREE; u2OuterLoop < (u2Length - 1); 
         u2OuterLoop =(UINT2)(u2OuterLoop + VLAN_EVB_CDCP_THREE))
    {
        /* It is expected that the pair is considered as new always as
         * initialized at declaration */
        u1NewPair = VLAN_TRUE;
        /* This offset is used when adding TLVs in TxBuf. It has to be
         * decremented by 3 when the TLV from RxBuf is discarded. */

        VLAN_EVB_GET_3BYTE (pu1RxBuf, (UINT1 *)(VOID *)&u4RxPair);
        VLAN_EVB_PARSE_PAIR (u4RxPair, u2RxSCID, u2RxSVID);

        if ((u2RxSCID == 0) || (u2RxSCID == VLAN_EVB_DEF_SCID) ||
            (u2RxSCID > VLAN_EVB_MAX_SCID))
        {
            /* SCID must not be zero hence dropping this request or 
             * default SCID-SVID pair is not required to be proceessed.
             * Start processing the next pair.
             * Note: default pair validation is added here if somewhere
             * in middle of TLV, default pair might present which requires
             * to be discarded.*/
            continue;
        }
        /* New request has received for this u2SCID. second loop index
         * is reduced by three bytes since default pair is not accounted.
         */
        for (u2InnerLoop = 0; u2InnerLoop < 
            (pUapIfEntry->u2UapCdcpTlvLength - VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
             u2InnerLoop = (UINT2)(u2InnerLoop + VLAN_EVB_CDCP_THREE))
        {
            MEMCPY (((UINT1 *)(VOID *)&u4TxPair), (pu1TxBuf + u2InnerLoop), 
                    VLAN_EVB_CDCP_THREE);

            VLAN_EVB_PARSE_PAIR (u4TxPair, u2TxSCID, u2TxSVID);

            if (u2RxSCID == u2TxSCID)
            {
                /* Pair is already present in Tx TLV. 
                 * In Tx pair, u2TxSVID will never be zero.
                 * Do nothing */
                u1NewPair = VLAN_FALSE;
                break;
            }
        }

        if (u2RxSVID == 0)
        {
            if (u1NewPair == VLAN_TRUE)
            {
                /* Get the available SVID for the new request */
                for (u2InnerLoop = (UINT2)pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow;
                     u2InnerLoop <((UINT2) pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh);
                     u2InnerLoop++)
                {
                    OSIX_BITLIST_IS_BIT_SET (pUapIfEntry->UapSvidList, 
                        u2InnerLoop, sizeof (pUapIfEntry->UapSvidList), bFlag);

                    if (bFlag == OSIX_FALSE)
                    {
                       /* To Check whether Allocated S-channels are less then 
                        * Channel Capacity */
                       if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                       {
                        
                        /* Got the available SVID in u2InnerLoop variable.
                         * Fist time-hence no data base addition required.*/
                        OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                u2InnerLoop, sizeof (pUapIfEntry->UapSvidList));

                        VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, u2OffSet,
                                (UINT4)u2RxSCID, (UINT4) u2InnerLoop);
                        u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                        u1FailStats =(UINT1) VLAN_FALSE;
                        u1PairCount =(UINT1) (u1PairCount + 1);
                        break;
                       }
                    }
                } /* Inner for loop */
                if ((u1FailStats == VLAN_TRUE) && (u2InnerLoop ==
                       ((UINT2)pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh)))
                {
                    /* Update TLV  SVID Pool exceeded count */
                    pUapIfEntry->UapStats.u4SVIdPoolExcdCount +=1; 
                }
                u1FailStats = VLAN_TRUE;
            }
            else /* Existing pair */
            {
                /* Eariler Tx contains the SCID with SVID value.
                 * Station is still not accepted the request. We are not going
                 * to allocate new SVID. Instead we are sending the same previous
                 * SVID.*/
                if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                {
                    OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                            u2TxSVID, sizeof (pUapIfEntry->UapSvidList)); 

                    VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, u2OffSet,
                            (UINT4)u2RxSCID, (UINT4) u2TxSVID);
                    u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                    u1PairCount =(UINT1)( u1PairCount + 1);
                }
            }
        }
        else /* u2RxSVID is non zero */
        {
            if ((u2RxSVID >= ((UINT2) pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow)) &&
                    (u2RxSVID <= ((UINT2)pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh)))
            {
                /*Checking whether the RxSVID is within the PoolRange value*/
                if (u1NewPair == VLAN_TRUE)
                {
                    OSIX_BITLIST_IS_BIT_SET (pUapIfEntry->UapSvidList,
                            u2RxSVID, sizeof (pUapIfEntry->UapSvidList), bFlag);

                    if (bFlag == OSIX_FALSE)
                    {
                        if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                        {
                            OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                    u2RxSVID, sizeof (pUapIfEntry->UapSvidList)); 

                            VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, u2OffSet,
                                    (UINT4)u2RxSCID, (UINT4) u2RxSVID);
                            u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                            u1PairCount =(UINT1)(u1PairCount + 1);
                        }
                    }
                    else /* Old Pair:u2TxSCID == u2RxSCID but u2RxSVID != 0*/ 
                    {
                        if (u2RxSVID == u2TxSVID)
                        {
                            /* The pair is previously allocated and transmitted.
                             * Now this is treated as ACK and entry in control
                             * plane and hardware are created. */
                            pSChIfEntry = NULL;
                            pSChIfEntry = VlanEvbSChScidIfGetEntry 
                                (pUapIfEntry->u4UapIfIndex, (UINT4) u2RxSCID);

                            if (pSChIfEntry == NULL)
                            {
                                /* The following function internally calls 
                                 * VlanEvbHwConfigSChInterface with 
                                 * VLAN_EVB_HW_SCH_IF_CREATE*/
                                if (VlanEvbSChIfAddEntry (pUapIfEntry->u4UapIfIndex,
                                            (UINT4) u2RxSVID, VLAN_EVB_UAP_SCH_MODE_DYNAMIC,
                                            u2RxSCID, &pSChIfEntry) == VLAN_FAILURE)
                                {
                                    VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC |
                                            ALL_FAILURE_TRC, VLAN_NAME, 
                                            "VlanEvbCdcpProcessDynamicPairs: "
                                            "New S-Channel entry creation failed\n");
                                    /*Update TLV S-Channel activation failed count*/
                                    pUapIfEntry->UapStats.u4EvbSchActiveFailCount +=1;
                                }
#ifdef L2RED_WANTED
                                else
                                {
                                    /* SBP entry is added, so sync up the
                                     * SBP entry with STANDBY node */
                                    if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                                                (VOID *) pSChIfEntry) == VLAN_FAILURE)
                                    {
                                        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                                "VlanEvbCdcpProcessDynamicPairs: New S-Channel entry"
                                                "creation sync up failed on the standby\n");
                                    }
                                }
#endif
                            }
                            if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                            {
                                OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                        u2RxSVID, sizeof (pUapIfEntry->UapSvidList)); 

                                VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf,
                                        u2OffSet, (UINT4)u2RxSCID, (UINT4) u2RxSVID);
                                u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                                u1PairCount =(UINT1)(u1PairCount + 1);
                            }
                        }
                        else /* u2RxSVID != previous u2TxSVID */
                        {
                            /* Already this u2RxSVID is allocated to some other
                             * u2TxSCID. So trying to suggest a new u2TxSVID. */

                            /* Get the available SVID for the new request */
                            for (u2InnerLoop =(UINT2)pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolLow;
                                    u2InnerLoop <((UINT2) pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh);
                                    u2InnerLoop++)
                            {
                                OSIX_BITLIST_IS_BIT_SET (pUapIfEntry->UapSvidList, 
                                        u2InnerLoop, sizeof (pUapIfEntry->UapSvidList), 
                                        bFlag);

                                if (bFlag == OSIX_FALSE)
                                {
                                    /* Got the available SVID in u2InnerLoop.
                                     * Fist time-hence no data base addition rqrd.*/
                                    if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                                    {
                                        OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                                u2InnerLoop, 
                                                sizeof (pUapIfEntry->UapSvidList));

                                        VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf, 
                                                u2OffSet, (UINT4) u2RxSCID, 
                                                (UINT4) u2InnerLoop);
                                        u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                                        u1FailStats =(UINT1) VLAN_FALSE;
                                        u1PairCount =(UINT1) (u1PairCount + 1);
                                        break;
                                    }
                                }
                            } /* Inner for loop */
                            if ((u1FailStats == VLAN_TRUE) && (u2InnerLoop ==
                                        ((UINT2)pUapIfEntry->u4UapIfSchAdminCdcpSvidPoolHigh)))
                            {
                                /* Update TLV  SVID Pool exceeded count */
                                pUapIfEntry->UapStats.u4SVIdPoolExcdCount += 1; 
                            }
                            u1FailStats = VLAN_TRUE;
                        } /* If - Else for "u2RxSVID == u2TxSVID" */
                    } /* If - Else for bFlag == OSIX_FALSE */
                }
                else /* u1NewPair == VLAN_FALSE */
                {
                    /* u2TxSCID == u2RxSCID, u2RxSVID != 0 */
                    if (u2RxSVID == u2TxSVID)
                    {
                        /* The pair is previously allocated and transmitted.
                         * Now this is treated as ACK and entry in control
                         * plane and hardware are created. */
                        pSChIfEntry = NULL;
                        pSChIfEntry = VlanEvbSChScidIfGetEntry 
                            (pUapIfEntry->u4UapIfIndex, (UINT4) u2RxSCID);

                        if (pSChIfEntry == NULL)
                        {
                            /* The following function internally calls 
                             * VlanEvbHwConfigSChInterface with 
                             * VLAN_EVB_HW_SCH_IF_CREATE*/
                            if (VlanEvbSChIfAddEntry (pUapIfEntry->u4UapIfIndex,
                                        (UINT4) u2RxSVID, VLAN_EVB_UAP_SCH_MODE_DYNAMIC,
                                        u2RxSCID, &pSChIfEntry) == VLAN_FAILURE)
                            {
                                VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC |
                                        ALL_FAILURE_TRC, VLAN_NAME, 
                                        "VlanEvbCdcpProcessDynamicPairs: "
                                        "New S-Channel entry creation failed\n");
                                /*Update TLV S-Channel activation failed count*/
                                pUapIfEntry->UapStats.u4EvbSchActiveFailCount += 1;
                            }
#ifdef L2RED_WANTED
                            else
                            {
                                /* SBP entry is added, so sync up the
                                 * SBP entry with STANDBY node */
                                if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                                            (VOID *) pSChIfEntry) == VLAN_FAILURE)
                                {
                                    VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                            "VlanEvbCdcpProcessDynamicPairs:"
                                            "New S-Channel entry creation sync up failed on the standby\n");
                                }
                            }
#endif
                        }
                        if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                        {
                            OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                    u2RxSVID, sizeof (pUapIfEntry->UapSvidList)); 

                            VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf,
                                    u2OffSet, (UINT4)u2RxSCID, (UINT4) u2RxSVID);
                            u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                            u1PairCount =(UINT1)(u1PairCount + 1);
                        }
                    }
                    else /* u2RxSVID != previous u2TxSVID */
                    {
                        /* Earlier pair is (SCIDx, SVIDx) where
                         * new pair is (SCIDx, SVIDy).
                         * Actions to be taken:
                         * 1) Delete (SCIDx, SVIDx) if present in control+hw.
                         * 2) Check for SVIDy availability. If exists,
                         *    provide that. else go for new one.
                         */
                        pSChIfEntry = NULL;
                        pSChIfEntry = VlanEvbSChScidIfGetEntry 
                            (pUapIfEntry->u4UapIfIndex, (UINT4) u2TxSCID);

                        if (pSChIfEntry != NULL)
                        {
                            /* The following function internally calls 
                             * VlanEvbHwConfigSChInterface with 
                             * VLAN_EVB_HW_SCH_IF_DELETE*/
                            if (VlanEvbSChIfDelEntry (pSChIfEntry->u4UapIfIndex,
                                        (UINT4) u2TxSVID, (UINT4) u2TxSCID) 
                                    == VLAN_FAILURE)
                            {
                                /* Critical place */
                                VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC |
                                        ALL_FAILURE_TRC, VLAN_NAME, 
                                        "VlanEvbCdcpProcessDynamicPairs: S-Channel"
                                        "Deletion failed \n");
                                pUapIfEntry->UapStats.u4EvbSchActiveFailCount +=1;
                            }
                            else
                            {
                                /* Have to reset the old u2TxSVID first. */
                                OSIX_BITLIST_RESET_BIT (pUapIfEntry->UapSvidList,
                                        u2TxSVID, sizeof (pUapIfEntry->UapSvidList));
#ifdef L2RED_WANTED
                                /* SBP entry is deleted, so sync up the
                                 * SBP change with STANDBY node */
                                if (VlanEvbRedSendDynamicUpdates ((UINT1) 
                                            VLAN_EVB_RED_SBP_DEL_SYNC_UP,
                                            (VOID *) pUapIfEntry) == VLAN_FAILURE)
                                {
                                    VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                            "VlanEvbCdcpProcessDynamicPairs: S-Channel"
                                            "deletion sync up failed on the standby\n");
                                }
#endif
                            }
                        }
                        OSIX_BITLIST_IS_BIT_SET (pUapIfEntry->UapSvidList,
                                u2RxSVID, sizeof (pUapIfEntry->UapSvidList), bFlag);

                        if (bFlag == OSIX_FALSE)
                        {
                            if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                            {
                                /* Set the new u2RxSVID bit */
                                OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                        u2RxSVID, sizeof (pUapIfEntry->UapSvidList));

                                VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf,
                                        u2OffSet, (UINT4)u2RxSCID, (UINT4) u2RxSVID);
                                u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                                u1PairCount =(UINT1)(u1PairCount + 1);
                            }
                        }
                        else 
                        {
                            /* In case of new requested u2RxSVID is not available
                             * send the old u2TxSVID again.
                             * NOTE: Just before we deleted and so it is available*/
                            if (u1PairCount < pUapIfEntry->i4UapIfSchAdminCdcpChanCap ) 
                            {

                                OSIX_BITLIST_SET_BIT (pUapIfEntry->UapSvidList,
                                        u2TxSVID, sizeof (pUapIfEntry->UapSvidList));

                                VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET (gau1TxBuf,
                                        u2OffSet, (UINT4)u2RxSCID, (UINT4) u2TxSVID);
                                u2OffSet =(UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE);
                                u1PairCount =(UINT1)(u1PairCount + 1);
                            }
                        }
                    } /* If - Else for "u2RxSVID == u2TxSVID" */
                }
            }/* - To Check whether u2Rxsvid is within the pool Range */
        } /* If - Else for u2RxSVID == 0 */
    } /* End of for - all pairs processed */

    /* Remove the deleted pairs */
    VlanEvbCdcpDeleteDynamicPairs (pUapIfEntry, u2OffSet);
   
    /* Put proper length in CDCP TLV. */
    /* This macro will fill TLV type into first 7 bits of variable +
     * Length into next 9 bits of the variable */
    VLAN_EVB_CDCP_CONSTRUCT_TLV_HEADER (VLAN_EVB_LLDP_ORG_SPEC_TLV,
        (UINT2)(u2OffSet - VLAN_EVB_CDCP_TLV_HDR_LEN), &u2TlvHeader);
    VLAN_EVB_CDCP_PUT_2BYTE (pu1TypeHdr, u2TlvHeader);

    /* copy to transmit the modified buffer and length */
    MEMSET (pUapIfEntry->au1UapCdcpTlv, 0, VLAN_EVB_CDCP_TLV_LEN);
    MEMCPY (pUapIfEntry->au1UapCdcpTlv, gau1TxBuf, u2OffSet);
    pUapIfEntry->u2UapCdcpTlvLength = u2OffSet;

#ifdef L2RED_WANTED
    /* CDCP TLV is changed, so sync up the
     * TLV change with STANDBY node */
    if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_CDCP_TLV_CHANGE,
                             (VOID *) pUapIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
           "VlanEvbCdcpProcessDynamicPairs:TLV change sync up failed on the standby\n");
    }
#endif
    /* Transmit the updated TLVs through LLDP */
    VlanEvbPortPostMsgToCdcp (pUapIfEntry, VLAN_EVB_CDCP_LLDP_PORT_UPDATE);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpDeleteDynamicPairs 
 *
 *  DESCRIPTION   : This function compares the gau1TxBuf to be transmitted now
 *                  and the old pair from pUapIfEntry->au1UapCdcpTlv and finds
 *                  the deleted pairs and remove them from the control plane
 *                  and from the hardware.
 *
 *  INPUT         : pUapIfEntry - UAP Interface entry.
 *                  u2NewTxLen  - No of bytes filled in gau1TxBuf
 *                  gau1TxBuf is referred.
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 ****************************************************************************/
VOID
VlanEvbCdcpDeleteDynamicPairs (tEvbUapIfEntry *pUapIfEntry, UINT2 u2NewTxLen)
{
    tEvbSChIfEntry  *pSChIfEntry = NULL;
    UINT1           *pu1OldBuf  = NULL;
    UINT1           *pu1NewBuf  = NULL;
    UINT4           u4OldPair   = 0;
    UINT4           u4NewPair   = 0;
    UINT2           u2OldSCID   = 0;
    UINT2           u2OldSVID   = 0;
    UINT2           u2NewSCID   = 0;
    UINT2           u2NewSVID   = 0;
    UINT2           u2OuterLoop = 0;
    UINT2           u2InnerLoop = 0;
    UINT1           u1DeleteFlag= VLAN_TRUE;

    /* Get the start of Tx TLV after the default pair presence */
    VLAN_EVB_CDCP_GET_TLV_START_PTR (pUapIfEntry->au1UapCdcpTlv, pu1OldBuf);

    for (u2OuterLoop = 0; u2OuterLoop < 
         (pUapIfEntry->u2UapCdcpTlvLength - VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
          u2OuterLoop =(UINT2) (u2OuterLoop + VLAN_EVB_CDCP_THREE))
    {
        VLAN_EVB_GET_3BYTE (pu1OldBuf, (UINT1 *)(VOID *)&u4OldPair);
        VLAN_EVB_PARSE_PAIR (u4OldPair, u2OldSCID, u2OldSVID);
        VLAN_EVB_CDCP_GET_TLV_START_PTR (gau1TxBuf, pu1NewBuf);

        for (u2InnerLoop = 0; u2InnerLoop < 
             (u2NewTxLen - VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR);
             u2InnerLoop = (UINT2)(u2InnerLoop + VLAN_EVB_CDCP_THREE))
        {
            VLAN_EVB_GET_3BYTE (pu1NewBuf, (UINT1 *)(VOID *)&u4NewPair);
            VLAN_EVB_PARSE_PAIR (u4NewPair, u2NewSCID, u2NewSVID);
            UNUSED_PARAM(u2NewSVID);
            if (u2OldSCID == u2NewSCID)
            {
                u1DeleteFlag = VLAN_FALSE;
                break;
            }
        }
        if (u1DeleteFlag == VLAN_TRUE)
        {
            pSChIfEntry = VlanEvbSChScidIfGetEntry (pUapIfEntry->u4UapIfIndex,
                                                    (UINT4) u2OldSCID);
            if (pSChIfEntry != NULL)
            {
                /* The following function internally calls 
                 * VlanEvbHwConfigSChInterface with VLAN_EVB_HW_SCH_IF_DELETE*/
                if (VlanEvbSChIfDelEntry (pSChIfEntry->u4UapIfIndex,
                       (UINT4) u2OldSVID, (UINT4)u2OldSCID) == VLAN_FAILURE)
                {
                    /* Critical place */
                    VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME, "VlanEvbCdcpDeleteDynamicPairs:"
                              "Deletion failed \n");
                    pUapIfEntry->UapStats.u4EvbSchActiveFailCount +=1;
                }
                else
                {
                    OSIX_BITLIST_RESET_BIT (pUapIfEntry->UapSvidList, u2OldSVID,
                            sizeof (pUapIfEntry->UapSvidList));
#ifdef L2RED_WANTED
                    /* SBP entry is deleted, so sync up the
                     * SBP change with STANDBY node */
                    if (VlanEvbRedSendDynamicUpdates ((UINT1) 
                                VLAN_EVB_RED_SBP_DEL_SYNC_UP,
                                (VOID *) pUapIfEntry) == VLAN_FAILURE)
                    {
                        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                "VlanEvbCdcpDeleteDynamicPairs: S-Channel"
                                "deletion sync up failed on the standby\n");
                    }
#endif
                }
            } /*pSChIfEntry != NULL*/
        }
        /* For the next iteration , setting the flag as VLAN_TRUE */
        u1DeleteFlag = VLAN_TRUE;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpPortReReg
 *
 *  DESCRIPTION   : This function is used to process the Reregistration message 
 *                  posted in VLAN queue
 *                  from LLDP. CDCP TLV and call back function has to be 
 *                  registered with LLDP again for
 *                  the UAP ports that are enabled with CDCP.
 *
 *  INPUT         : None
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbCdcpPortReReg (VOID)
{
    tEvbUapIfEntry     *pEvbUapIfEntry = NULL;
    INT4                i4RetVal       = VLAN_SUCCESS;

    pEvbUapIfEntry = (tEvbUapIfEntry *) VlanEvbUapIfGetFirstEntry ();
    while (pEvbUapIfEntry != NULL)
    {
        if (pEvbUapIfEntry->i4UapIfSchCdcpAdminEnable == 
                VLAN_EVB_UAP_CDCP_ENABLE)
        {
            if (VlanEvbPortPostMsgToCdcp (pEvbUapIfEntry, 
                        VLAN_EVB_CDCP_LLDP_PORT_REG) == VLAN_FAILURE)
            {
                /* one or some of CDCP enabled UAP ports registration failed 
                 * with LLDP */
                i4RetVal = VLAN_FAILURE;
            }
        }
        pEvbUapIfEntry = (tEvbUapIfEntry *) VlanEvbUapIfGetNextEntry
            (pEvbUapIfEntry->u4UapIfIndex);
    }
    return i4RetVal;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpConstructTlv
 *
 *  DESCRIPTION   : This function is used to construct the CDCP TLV information
 *                  for the available S-Channel entries present in the UAP.
 *                  
 *
 *  INPUT         : pEvbUapIfEntry - UAP interface entry
 *
 *  OUTPUT        : pEvbUapIfEntry->au1UapCdcpTlv, 
 *                  pEvbUapIfEntry->u2UapCdcpTlvLength
 *
 *  RETURNS       : None.
 *
 * **************************************************************************/
VOID
VlanEvbCdcpConstructTlv (tEvbUapIfEntry * pEvbUapIfEntry)
{
    tEvbSChIfEntry      *pEvbSChIfEntry      = NULL;
    UINT1               *pu1Temp             = NULL;
    UINT1               *pu1TypeHdr          = NULL;
    UINT4               u4UapIfIndex         = pEvbUapIfEntry->u4UapIfIndex;
    UINT4               u4SVId               = 0;
    UINT2               u2TlvHeader          = VLAN_ZERO;
    UINT2               u2TlvLength          = VLAN_ZERO;
    UINT1               u1EvbCdcpSubtype     = VLAN_EVB_CDCP_SUBTYPE;
    UINT1               u1PairCount          = 0;

   MEMSET (pEvbUapIfEntry->au1UapCdcpTlv, 0, 
           sizeof(pEvbUapIfEntry->au1UapCdcpTlv));
   pu1Temp = pu1TypeHdr = pEvbUapIfEntry->au1UapCdcpTlv;

    /* |<----- Packet field name ------------------>|<---Size--->| Octet
     *  _________________________________________________________  0  _    
     * |                                            |            |     |    
     * | TLV Type = 127                             |    7 bits  |     |
     * |____________________________________________|____________|      > TLV 
     * |                                            |            |     |  Header
     * | TLV information string length = 8 + 3N     |    9 bits  |     |
     * |____________________________________________|____________| 2  _|
     * |                                            |            |    _
     * | OUI = 00-80-C2                             |    3 bytes |     |  
     * |____________________________________________|____________| 5   |
     * |                                            |            |     |
     * | Subtype = 0x0e                             |    1 byte  |     |
     * |____________________________________________|____________| 6   |
     * |                                            |            |     |
     * | Role = 0 (bridge)                          |    1 bit   |      > TLV
     * |____________________________________________|____________|     |  Info
     * |                                            |            |     |  String
     * | Reserved1 = 0                              |    3 bits  |     |
     * |____________________________________________|____________|     |
     * |                                            |            |     |
     * | SComp = 1 (presence of S-VLAN Comp)        |    1 bit   |     |
     * |____________________________________________|____________|     |
     * |                                            |            |     |
     * | Reserved2 = 0                              |    15 bits |     |
     * |____________________________________________|____________|     |
     * |                                            |            |     |
     * | ChnCap = total S-Ch's (assigned + avail)   |    12 bits |     |
     * |____________________________________________|____________| 10  |
     * |                                            |            |     |
     * | SCID(12 bits)/SVID (12 bits) pairs         |  N*3 bytes |     |
     * |____________________________________________|____________|    _|
     */

     /* SCID and SVID calculation:- 
      * Total size for CDCP TLV     =   512  bytes
      * Other than SCID/SVID pair   =    10  bytes
      * Remaining bytes             =   502  bytes
      * Each pair size              =     3  bytes
      * Space for SCID/SVID pair    =   502/3 = 167 pairs (inc. default pair)
      */

    /* Type and Length are filled at last since u1PairCount is required.
     * Having reference pointer as pu1TypeHdr. Give space for type & length
     * to be filled at last (i.e) 2 bytes. */
    pu1Temp = pu1Temp + VLAN_EVB_CDCP_TYPE_LEN_SIZE;
     
    /* Fill the OUI field in to buffer */
    VLAN_EVB_CDCP_PUT_OUI (pu1Temp, gau1EvbCdcpOUI);
    
    /* Fill the sub type field in to buffer */
    VLAN_EVB_CDCP_PUT_1BYTE(pu1Temp, u1EvbCdcpSubtype);

     /* Note: Reserved2 (15 bits) is split into following method for filling.
      * 3 bits along with SComp
      * 1 byte 
      * 4 bits along with Chncap 
      */
    /* Fill the following fields in next byte */
    /* 1. Role      (1 bit)  = 0 (bridge)
     * 2. Reserved1 (3 bits) = 0
     * 3. SComp     (1 bit)  = 1
     * 4. Reserved2 (3 bits) = 0 (partial)
     */
    VLAN_EVB_CDCP_PUT_1BYTE (pu1Temp, 
                             (VLAN_EVB_CDCP_ONE << VLAN_EVB_CDCP_THREE));

    /* Fill the Reserved2's second split up */
    VLAN_EVB_CDCP_PUT_1BYTE (pu1Temp, 0);

    /* Fill the Reserved2's 3rd split up + Chncap */
    VLAN_EVB_CDCP_PUT_CHNCAP (pu1Temp, 
                              pEvbUapIfEntry->i4UapIfSchAdminCdcpChanCap);

    while ((pEvbSChIfEntry = VlanEvbSChIfGetNextEntry 
                (u4UapIfIndex, u4SVId))  != NULL)
    {
        if ((pEvbSChIfEntry->u4UapIfIndex) == (pEvbUapIfEntry->u4UapIfIndex))
        {
            if (((pEvbSChIfEntry->i4NegoStatus != VLAN_EVB_SCH_FREE) &&
                (pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID))||
                 (pEvbSChIfEntry->u4SVId == VLAN_EVB_DEF_SVID))
            { 
                VLAN_EVB_CDCP_PUT_3BYTE (pu1Temp, pEvbSChIfEntry->u4SVId,
                                         pEvbSChIfEntry->u4SChId);
                /* In Dynamic Mode Default SVID should not be allocated to another
                 * SCID other than Default SCID , so DEF_SVID  is set in UAP_SVID_LIST*/ 
                if ((pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC) &&
                        (pEvbUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE))
                {
                    OSIX_BITLIST_SET_BIT (pEvbUapIfEntry->UapSvidList,
                            VLAN_EVB_DEF_SVID, sizeof (pEvbUapIfEntry->UapSvidList));
                }
                u1PairCount =(UINT1)(u1PairCount + 1);
            }
        }
        else
        {
            break;
        }
        u4UapIfIndex = pEvbSChIfEntry->u4UapIfIndex;
        u4SVId = pEvbSChIfEntry->u4SVId;
    }

    /* Filling Type & Len fields */ 
    u2TlvLength = (UINT2)(VLAN_EVB_CDCP_TLV_OUI_LEN + VLAN_EVB_CDCP_TLV_SUBTYPE_LEN + 
                  VLAN_EVB_CDCP_TLV_RRSRC_LEN + 
                  (u1PairCount * VLAN_EVB_CDCP_SINGLE_PAIR_LEN)); 
      
    /* This macro will fill TLV type into first 7 bits of variable +
     * Length into next 9 bits of the variable */
    VLAN_EVB_CDCP_CONSTRUCT_TLV_HEADER (VLAN_EVB_LLDP_ORG_SPEC_TLV,
                                        u2TlvLength, &u2TlvHeader);
    /* Fill the TLV header in to buffer */
    VLAN_EVB_CDCP_PUT_2BYTE (pu1TypeHdr, u2TlvHeader);

    pEvbUapIfEntry->u2UapCdcpTlvLength = (UINT2) (pu1Temp - 
                                          pEvbUapIfEntry->au1UapCdcpTlv);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbPortPostMsgToCdcp
 *
 *  DESCRIPTION   : This utility function is used to post the TLV/Sem
 *                  Update message to Cdcp.
 *
 *  INPUT         : pEvbUapIfEntry - UAP interface entry
 *                  u1MsgType - CDCP LLDP port mesg type
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE
 *
 * **************************************************************************/
INT4
VlanEvbPortPostMsgToCdcp (tEvbUapIfEntry *pEvbUapIfEntry, UINT1 u1MsgType)
{

    switch (u1MsgType)
    {
        case VLAN_EVB_CDCP_LLDP_PORT_UPDATE:
            if (VlanEvbCdcpPortUpdate (pEvbUapIfEntry) != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,"VlanEvbPortPostMsgToCdcp:"
                                     "VlanEvbCdcpPortUpdate failed  \n");
                return VLAN_FAILURE;
            }
            break;

        case VLAN_EVB_CDCP_LLDP_PORT_REG:
            /* Used to Register the application to the LDDP using
             * CDCP function */
            VlanEvbCdcpConstructTlv (pEvbUapIfEntry);
            if (VlanEvbCdcpPortReg (pEvbUapIfEntry) != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                          VLAN_NAME,"VlanEvbPortPostMsgToCdcp:"
                                    " CDCP Port Registration Failed !!!\r\n");
                return VLAN_FAILURE;

            }
            break;

        case VLAN_EVB_CDCP_LLDP_PORT_DEREG:
            /* Used to De-Register the application from the LLDP using
             * CDCP function */
            if (VlanEvbCdcpPortDeReg (pEvbUapIfEntry) != VLAN_SUCCESS)
            {
                VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                          VLAN_NAME,"VlanEvbPortPostMsgToCdcp:CDCP Port"
                          "DeRegistration Failed!!!\r\n");
                return VLAN_FAILURE;
            }
            break;

        default:
            VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    VLAN_NAME,"VlanEvbPortPostMsgToCdcp:Invalid Message"
                    "Type Received!!!\r\n");
            break;
    }
    return VLAN_SUCCESS;
}
/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpUtlTlvStatusChange
 *
 *  DESCRIPTION   : This utility is used to handle the Cdcp TLV Tx status
 *                  change.
 *
 *  INPUT         : pEvbUapIfEntry - Uap Port Entry.
 *                  u1NewTlvStatus - TLV Tx Status.
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE
 *
 * **************************************************************************/
INT4
VlanEvbCdcpUtlTlvStatusChange (tEvbUapIfEntry * pEvbUapIfEntry,
                               INT4 i4NewTlvStatus)
{
    if (pEvbUapIfEntry->i4UapIfSchCdcpAdminEnable ==
            VLAN_EVB_UAP_CDCP_DISABLE)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                VLAN_NAME,"VlanEvbCdcpUtlTlvStatusChange:"
                "CDCP must be enabled on the UAP \n");
        return VLAN_FAILURE;
    }
    if (i4NewTlvStatus == VLAN_EVB_CDCP_LLDP_ENABLE)
    {
        /* If the TLV TX new status is enabled then update
         * the data structure and send the Registration request
         * with CDCP since Registration will happen only if
         * TX status is enabled */
        pEvbUapIfEntry->i4EvbSysEvbLldpTxEnable = i4NewTlvStatus;
        if (VlanEvbPortPostMsgToCdcp (pEvbUapIfEntry,
                    VLAN_EVB_CDCP_LLDP_PORT_REG) == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,"VlanEvbCdcpUtlTlvStatusChange:"
                      "VlanEvbPortPostMsgToCdcp failed in reg\n");
            return VLAN_FAILURE;
        }
    }
    else /* VLAN_EVB_CDCP_LLDP_DISABLE */
    {
        /* Deregister the application with CDCP and change the
         * TLV Tx status since deregistration happnes only if
         * the Tx status is already enabled.*/
        pEvbUapIfEntry->i4EvbSysEvbLldpTxEnable = i4NewTlvStatus;
        if (VlanEvbPortPostMsgToCdcp (pEvbUapIfEntry,
                           VLAN_EVB_CDCP_LLDP_PORT_DEREG) == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,"VlanEvbCdcpUtlTlvStatusChange:"
                      "VlanEvbPortPostMsgToCdcp failed in dereg \n");
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpProcessAgeout
 *
 *  DESCRIPTION   : This utility is used to handle the Age Out Message from LLDP.
 *                  change.
 *
 *  INPUT         : u4IfIndex - UAP Interface index
 *                   
 *  OUTPUT        : None
 *
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE
 *
 * **************************************************************************/
INT4
VlanEvbCdcpProcessAgeout (UINT4 u4UapIfIndex)
{
	tEvbUapIfEntry      *pEvbUapIfEntry      = NULL;
	tEvbSChIfEntry      *pSChIfNode          = NULL;

    pEvbUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);
   
    if (pEvbUapIfEntry == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbCdcpProcessAgeout:Pkt dropped - UAP not exists \n");
        return VLAN_FAILURE;
    }
    if (pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
    {
        if(VlanEvbSChSetStaticEntriesOperDown(pEvbUapIfEntry, OSIX_TRUE) 
                    == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    VLAN_NAME,"VlanEvbCdcpProcessAgeout:"
                    "VlanEvbSChSetStaticEntriesOperDown failed  \n");
        }
    }
        
    if (pEvbUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
    {
		VlanEvbSChIfDelAllEntriesOnUap(u4UapIfIndex);
        /* When Non-Default S-Channels are not Present and EVB is disabled then
         * i4UapIfCdcpOperState for UAP is not changed so taking care here
         * This is commoni place for CDCP -DISABLING and no evb tlv-select */  
		pSChIfNode = VlanEvbSChIfGetNextEntry (u4UapIfIndex, VLAN_EVB_DEF_SVID);
		if ((pSChIfNode == NULL) ||
				((pSChIfNode != NULL ) &&
				 (pSChIfNode->u4UapIfIndex != u4UapIfIndex) &&
				 (pSChIfNode->u4SVId == VLAN_EVB_DEF_SVID)))
		{
			pEvbUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
			VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC , VLAN_NAME,
					"VlanEvbCdcpProcessAgeout:i4UapIfCdcpOperState is "
					"set for UAP : %d \r\n",u4UapIfIndex);
		}
        MEMSET (pEvbUapIfEntry->UapSvidList, 0, 
                sizeof(pEvbUapIfEntry->UapSvidList));
        
    }
   
	VlanEvbCdcpConstructTlv (pEvbUapIfEntry);
    if (VlanEvbPortPostMsgToCdcp (pEvbUapIfEntry,
                VLAN_EVB_CDCP_LLDP_PORT_UPDATE) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                VLAN_NAME,"VlanEvbCdcpProcessAgeout:"
                "VlanEvbPortPostMsgToCdcp failed in reg \n");
        return VLAN_FAILURE;
    }
#ifdef L2RED_WANTED
    /* CDCP TLV is changed, so sync up the
     * TLV change with STANDBY node */
    if (VlanEvbRedSendDynamicUpdates ((UINT1) VLAN_EVB_RED_CDCP_TLV_CHANGE,
                             (VOID *) pEvbUapIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
           "VlanEvbCdcpProcessAgeout:TLV change sync up failed on the standby\n");
    }
#endif
    return VLAN_SUCCESS;
}
#endif
