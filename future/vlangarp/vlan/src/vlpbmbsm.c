/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlpbmbsm.c,v 1.11 2013/12/18 12:00:00 siva Exp $
 *
 * Description: This file contains mbsm related changes used in VLANmodule
 *              for provider bridges
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/*                                                                           */
/*  FILE NAME             : vlpbmbsm.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN MBSM related routines.   */
/*                                                                           */
/*****************************************************************************/

#ifndef _VLPBMBSM_C
#define _VLPBMBSM_C
#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdtPbPortPropertiesToHw                     */
/*                                                                           */
/* Description        : This function Updates Pb port properties for         */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)          :  pSlotInfo - Information about inserted slot          */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtPbPortPropertiesToHw (tMbsmSlotInfo * pSlotInfo, UINT2 u2PortNum)
{
    tHwVlanPbPcpInfo    HwVlanPbPcpInfo;
    tVlanPbPortEntry   *pPbPortEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Pcp = 0;
    UINT1               u1Priority = 0;
    UINT1               u1DropEligible = 0;
    UINT1               u1SVlanTranslationStatus = VLAN_DISABLED;
    UINT1               u1InterfaceType = VLAN_S_INTERFACE_TYPE;
    UINT1               u1PbPortType = 0;

    MEMSET (&HwVlanPbPcpInfo, 0, sizeof (tHwVlanPbPcpInfo));

    pPortEntry = VLAN_GET_PORT_ENTRY (u2PortNum);
    pPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2PortNum);

    if (pPortEntry == NULL)
    {
        return MBSM_SUCCESS;
    }

    VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (), &u1InterfaceType);

    u1PbPortType = pPbPortEntry->u1PbPortType;

    if (u1PbPortType == VLAN_CNP_TAGGED_PORT)
    {
        if (u1InterfaceType == VLAN_C_INTERFACE_TYPE)
        {
            /* In NP set the Port_Type as CNP_CTagged */
            u1PbPortType = CFA_CNP_CTAGGED_PORT;
        }
    }

    if (VlanFsMiVlanMbsmHwSetProviderBridgePortType
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         u1PbPortType, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetProviderBridgePortType Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortIngressEtherType
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPortEntry->u2IngressEtherType, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortIngressEtherType Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortEgressEtherType
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPortEntry->u2EgressEtherType, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortEgressEtherType Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanL2IwfPbGetVidTransStatus
        (VLAN_CURR_CONTEXT_ID (), u2PortNum,
         &u1SVlanTranslationStatus) == L2IWF_FAILURE)
    {

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortSVlanTranslationStatus
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         u1SVlanTranslationStatus, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortSVlanTranslationStatus Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortEtherTypeSwapStatus
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u1EtherTypeSwapStatus, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortEtherTypeSwapStatus Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortCustomerVlan
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->CVlanId, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortCustomerVlan Failed \n");
        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortSVlanClassifyMethod
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u1SVlanTableType, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortSVlanClassifyMethod Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwPortMacLearningStatus
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u1MacLearningStatus, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwPortMacLearningStatus Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwPortMacLearningLimit
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u4MacLearningLimit, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwPortMacLearningLimit Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortPcpSelection
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u1PcpSelRow, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortPcpSelection Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortReqDropEncoding
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u1ReqDropEncoding, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortReqDropEncoding Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanFsMiVlanMbsmHwSetPortUseDei
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
         pPbPortEntry->u1UseDei, pSlotInfo) != FNP_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanFsMiVlanMbsmHwSetPortUseDei Failed \n");

        return MBSM_FAILURE;
    }

    HwVlanPbPcpInfo.u2PcpSelRow = pPbPortEntry->u1PcpSelRow;

    for (u2Pcp = 0; u2Pcp < VLAN_PB_MAX_PCP; u2Pcp++)
    {
        HwVlanPbPcpInfo.u2PcpValue = u2Pcp;
        HwVlanPbPcpInfo.u2Priority = VLAN_PB_PCP_DECODE_PRIORITY
            (pPbPortEntry, pPbPortEntry->u1PcpSelRow, u2Pcp);
        HwVlanPbPcpInfo.u1DropEligible =
            VLAN_PB_PCP_DECODE_DROP_ELIGIBLE
            (pPbPortEntry, pPbPortEntry->u1PcpSelRow, u2Pcp);

        if (VlanFsMiVlanMbsmHwSetPcpDecodTbl
            (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2PortNum),
             HwVlanPbPcpInfo, pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB,
                      "Card Insertion: "
                      "VlanFsMiVlanMbsmHwSetPcpDecodTbl Failed \n");

            return MBSM_FAILURE;
        }
    }

    for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
    {
        for (u1DropEligible = 0;
             u1DropEligible < VLAN_PB_MAX_DROP_ELIGIBLE; u1DropEligible++)
        {
            HwVlanPbPcpInfo.u2Priority = u1Priority;
            HwVlanPbPcpInfo.u1DropEligible = u1DropEligible;
            HwVlanPbPcpInfo.u2PcpValue =
                VLAN_PB_PCP_ENCODE_PCP
                (pPbPortEntry, pPbPortEntry->u1PcpSelRow,
                 u1Priority, u1DropEligible);

            if (VlanFsMiVlanMbsmHwSetPcpEncodTbl
                (VLAN_CURR_CONTEXT_ID (),
                 VLAN_GET_PHY_PORT (u2PortNum), HwVlanPbPcpInfo,
                 pSlotInfo) != FNP_SUCCESS)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB,
                          "Card Insertion: "
                          "VlanFsMiVlanMbsmHwSetPcpEncodTbl Failed \n");

                return MBSM_FAILURE;
            }
        }
    }
    if (pPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT)
    {
        if (VlanMbsmUpdtPepStatusToHw (u2PortNum, pSlotInfo) != MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB,
                      "Card Insertion: " "VlanMbsmUpdtPepStatusToHw Failed \n");

            return MBSM_FAILURE;
        }
    }

    if (VlanMbsmUpdateVlanSwapTableToHw (u2PortNum, pSlotInfo) != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanMbsmUpdateVlanSwapTableToHw Failed \n");

        return MBSM_FAILURE;
    }

    if (VlanAddMbsmAddEtherSwapTableToHw (u2PortNum, pSlotInfo) != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN_PB, "Card Insertion: "
                  "VlanAddMbsmAddEtherSwapTableToHw Failed \n");

        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdtPepStatusToHw                            */
/*                                                                           */
/* Description        : This function Updates Peb port status for            */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtPepStatusToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo)
{
    tCfaIfInfo          CfaIfInfo;
    tHwVlanPbPepInfo    HwVlanPbPepInfo;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    UINT1               u1Priority;
    INT4                i4FirstPort = (INT4) u2Port;
    INT4                i4NextPort;
    INT4                i4RegenPriority;
    UINT4               u4NextSVid;
    tVlanId             FirstSVid = 0;

    VlanCfaGetIfInfo (VLAN_GET_PHY_PORT (u2Port), &CfaIfInfo);

    pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry (i4FirstPort,
                                                             FirstSVid,
                                                             &i4NextPort,
                                                             &u4NextSVid);
    if (pVlanPbLogicalPortEntry == NULL)
    {
        return MBSM_SUCCESS;
    }

    do
    {
        if (i4NextPort == u2Port)
        {

            if (pVlanPbLogicalPortEntry->u1OperStatus == VLAN_OPER_UP)
            {

                if (VlanMbsmUpdateCvidEntriesToHw ((UINT2) i4NextPort,
                                                   (tVlanId) u4NextSVid,
                                                   pSlotInfo) != MBSM_SUCCESS)
                {
                    return MBSM_FAILURE;
                }

            }

            HwVlanPbPepInfo.i4DefUserPri =
                pVlanPbLogicalPortEntry->u1DefUserPriority;
            HwVlanPbPepInfo.Cpvid = pVlanPbLogicalPortEntry->Cpvid;
            HwVlanPbPepInfo.u1AccptFrameType =
                pVlanPbLogicalPortEntry->u1AccptFrameType;
            HwVlanPbPepInfo.u1IngFiltering =
                pVlanPbLogicalPortEntry->u1IngressFiltering;

            if (VlanFsMiVlanMbsmHwCreateProviderEdgePort
                (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (i4NextPort),
                 u4NextSVid, HwVlanPbPepInfo, pSlotInfo) != FNP_SUCCESS)
            {
                return MBSM_FAILURE;
            }

            for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY;
                 u1Priority++)
            {
                i4RegenPriority =
                    pVlanPbLogicalPortEntry->au1RegenPriority[u1Priority];

                if (VlanFsMiVlanMbsmHwSetServicePriRegenEntry
                    (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (i4NextPort),
                     (tVlanId) u4NextSVid, (INT4) u1Priority, i4RegenPriority,
                     pSlotInfo) != FNP_SUCCESS)
                {
                    return MBSM_FAILURE;
                }
            }

        }

        i4FirstPort = i4NextPort;
        FirstSVid = (tVlanId) u4NextSVid;
    }
    while ((pVlanPbLogicalPortEntry = VlanPbGetNextLogicalPortEntry
            (i4FirstPort, FirstSVid, &i4NextPort, &u4NextSVid)) != NULL);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdateCvidEntriesToHw                        */
/*                                                                           */
/* Description        : This function Updates Cvid Entried for               */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u4SVlanId - Service vlan Ip for port                 */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdateCvidEntriesToHw (UINT2 u2Port, UINT4 u4SVlanId,
                               tMbsmSlotInfo * pSlotInfo)
{
    tVlanSVlanMap       VlanSVlanMap;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    UINT4               u4TblIndex;
    UINT4               u4NextCVid;
    INT4                i4NextPort;
    UINT2               u2CurrPort;
    tVlanId             CVid = 0;

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    if (VlanPbGetNextCVlanSVlanEntry (u4TblIndex, u2Port,
                                      CVid, &i4NextPort,
                                      &u4NextCVid) == VLAN_FAILURE)
    {
        /* 
         * There is no entry available for this CEP in CVID
         * registration table. Hence return Failure.
         */
        return MBSM_SUCCESS;
    }

    do
    {
        pCVlanSVlanEntry = VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                                     (UINT2) i4NextPort,
                                                     (tVlanId) u4NextCVid);
        if (pCVlanSVlanEntry != NULL)
        {
            if ((i4NextPort == (INT4) u2Port) &&
                (pCVlanSVlanEntry->SVlanId == u4SVlanId)
                && (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE))
            {
                VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

                VlanSVlanMap.u2Port =
                    VLAN_GET_PHY_PORT (pCVlanSVlanEntry->u2Port);
                VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
                VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
                VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
                VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;

                VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;

                if (VlanFsMiVlanMbsmHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                                   VlanSVlanMap, pSlotInfo)
                    != FNP_SUCCESS)
                {
                    return MBSM_FAILURE;
                }
            }
        }
        u2CurrPort = (UINT2) i4NextPort;
        CVid = (tVlanId) u4NextCVid;
    }
    while (VlanPbGetNextCVlanSVlanEntry (u4TblIndex, u2CurrPort,
                                         CVid, &i4NextPort,
                                         &u4NextCVid) == VLAN_SUCCESS);
    return MBSM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdateVlanSwapTableToHw                      */
/*                                                                           */
/* Description        : This function Updates Vlan Swap Table for            */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdateVlanSwapTableToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    i4RetVal = VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                   u2Port, 0,
                                                   &OutVidTransEntryInfo);
    while (i4RetVal == L2IWF_SUCCESS)
    {
        if ((u2Port == OutVidTransEntryInfo.u2Port) &&
            (OutVidTransEntryInfo.u1RowStatus == VLAN_ACTIVE))
        {
            if (VlanMbsmAddVidTransEntryToHw (OutVidTransEntryInfo.u2Port,
                                              OutVidTransEntryInfo.LocalVid,
                                              OutVidTransEntryInfo.RelayVid,
                                              pSlotInfo) != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }
        }
        i4RetVal =
            VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                OutVidTransEntryInfo.u2Port,
                                                OutVidTransEntryInfo.LocalVid,
                                                &OutVidTransEntryInfo);
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMbsmAddVidTransEntryToHw                         */
/*                                                                           */
/* Description        : This function Updates Vlan Swap Table for            */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmAddVidTransEntryToHw (UINT2 u2Port, tVlanId LocalVid, tVlanId RelayVid,
                              tMbsmSlotInfo * pSlotInfo)
{
    tVidTransEntryInfo  OutVidTransEntryInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    if (VlanFsMiVlanMbsmHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                                    VLAN_GET_PHY_PORT (u2Port),
                                                    LocalVid, RelayVid,
                                                    pSlotInfo) != FNP_SUCCESS)
    {
        return MBSM_FAILURE;
    }

    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, RelayVid,
                                            OSIX_TRUE, &OutVidTransEntryInfo);
    if ((i4RetVal == L2IWF_FAILURE) ||
        (OutVidTransEntryInfo.u1RowStatus != VLAN_ACTIVE))
    {
        if (VlanFsMiVlanMbsmHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                                        VLAN_GET_PHY_PORT
                                                        (u2Port), RelayVid,
                                                        VLAN_NULL_VLAN_ID,
                                                        pSlotInfo) !=
            FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }

    i4RetVal = VlanL2IwfPbGetVidTransEntry (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, LocalVid,
                                            OSIX_FALSE, &OutVidTransEntryInfo);
    if ((i4RetVal == L2IWF_FAILURE) ||
        (OutVidTransEntryInfo.u1RowStatus != VLAN_ACTIVE))
    {
        if (VlanFsMiVlanMbsmHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                                        VLAN_GET_PHY_PORT
                                                        (u2Port),
                                                        VLAN_NULL_VLAN_ID,
                                                        LocalVid,
                                                        pSlotInfo) !=
            FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }

    }

    return MBSM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanAddMbsmAddEtherSwapTableToHw                     */
/*                                                                           */
/* Description        : This function Updates Ether Swap Table for           */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanAddMbsmAddEtherSwapTableToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo)
{
    tEtherTypeSwapEntry TempEtherTypeSwapEntry;
    tEtherTypeSwapEntry *pEtherTypeSwapEntry = NULL;

    VLAN_MEMSET (&TempEtherTypeSwapEntry, 0, sizeof (TempEtherTypeSwapEntry));

    TempEtherTypeSwapEntry.u2Port = u2Port;
    TempEtherTypeSwapEntry.u2LocalEtherType = 0;

    pEtherTypeSwapEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                         pVlanEtherTypeSwapTable,
                                         &TempEtherTypeSwapEntry, NULL);

    if (pEtherTypeSwapEntry == NULL)
    {
        return MBSM_SUCCESS;
    }

    do
    {

        if (pEtherTypeSwapEntry->u2Port == u2Port)
        {
            if (VlanFsMiVlanMbsmHwAddEtherTypeSwapEntry
                (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2Port),
                 (UINT2) pEtherTypeSwapEntry->u2LocalEtherType,
                 (UINT2) pEtherTypeSwapEntry->u2RelayEtherType,
                 pSlotInfo) != FNP_SUCCESS)
            {
                return MBSM_FAILURE;
            }
        }

        TempEtherTypeSwapEntry.u2Port = pEtherTypeSwapEntry->u2Port;
        TempEtherTypeSwapEntry.u2LocalEtherType =
            pEtherTypeSwapEntry->u2LocalEtherType;
        TempEtherTypeSwapEntry.u2RelayEtherType =
            pEtherTypeSwapEntry->u2RelayEtherType;

        pEtherTypeSwapEntry = RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->
                                             pVlanEtherTypeSwapTable,
                                             &TempEtherTypeSwapEntry, NULL);

    }
    while (pEtherTypeSwapEntry != NULL);

    return MBSM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdtMcastMacLimitToHw                        */
/*                                                                           */
/* Description        : This function Updates Pb Multicast Mac limit for     */
/*                      instrted slots                                       */
/*                                                                           */
/* Input(s)          :  pSlotInfo - Information about inserted slot          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMbsmUpdtMcastMacLimitToHw (tMbsmSlotInfo * pSlotInfo)
{
    if (VlanFsMiVlanMbsmHwMulticastMacTableLimit (VLAN_CURR_CONTEXT_ID (),
                                                  VLAN_PB_MULTICAST_TABLE_LIMIT,
                                                  pSlotInfo) != FNP_SUCCESS)
    {
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}
#endif
