
/* $Id: fsmsbrnc.c,v 1.1 2015/12/30 13:35:04 siva Exp $
   ISS Wrapper module
   module ARICENT-MIStdBRIDGE-MIB

*/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsmsbrlw.h"
# include  "garp.h"
# include  "fsmsbrnc.h"


/********************************************************************
 * FUNCTION NcFsDot1dBaseBridgeAddressGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBaseBridgeAddressGet (
				INT4 i4fsDot1dBaseContextId,
				UINT1 *ppfsDot1dBaseBridgeAddress )
{

		INT1 i1RetVal;
		tMacAddr pfsDot1dBaseBridgeAddress;
		MEMSET(&pfsDot1dBaseBridgeAddress,0,sizeof(tMacAddr));
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dBaseTable(
								i4fsDot1dBaseContextId) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBaseBridgeAddress(
						i4fsDot1dBaseContextId,
						&pfsDot1dBaseBridgeAddress );
		STRCPY(ppfsDot1dBaseBridgeAddress,pfsDot1dBaseBridgeAddress);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dBaseBridgeAddressGet */

/********************************************************************
 * FUNCTION NcFsDot1dBaseNumPortsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBaseNumPortsGet (
				INT4 i4fsDot1dBaseContextId,
				INT4 *pi4fsDot1dBaseNumPorts )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dBaseTable(
								i4fsDot1dBaseContextId) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBaseNumPorts(
						i4fsDot1dBaseContextId,
						pi4fsDot1dBaseNumPorts );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dBaseNumPortsGet */

/********************************************************************
 * FUNCTION NcFsDot1dBaseTypeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBaseTypeGet (
				INT4 i4fsDot1dBaseContextId,
				INT4 *pi4fsDot1dBaseType )
{

		INT1 i1RetVal;
		VLAN_LOCK();
		if (nmhValidateIndexInstanceFsDot1dBaseTable(
								i4fsDot1dBaseContextId) == SNMP_FAILURE)

		{
				VLAN_UNLOCK();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBaseType(
						i4fsDot1dBaseContextId,
						pi4fsDot1dBaseType );
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcFsDot1dBaseTypeGet */

/********************************************************************
 * FUNCTION NcFsDot1dBasePortIfIndexGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBasePortIfIndexGet (
				INT4 i4fsDot1dBasePort,
				INT4 *pi4fsDot1dBasePortIfIndex )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dBasePortTable(
								i4fsDot1dBasePort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBasePortIfIndex(
						i4fsDot1dBasePort,
						pi4fsDot1dBasePortIfIndex );
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dBasePortIfIndexGet */

/********************************************************************
 * FUNCTION NcFsDot1dBasePortCircuitGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBasePortCircuitGet (
				INT4 i4fsDot1dBasePort,
				UINT1 *ppfsDot1dBasePortCircuit )
{

		INT1 i1RetVal;
		tSNMP_OID_TYPE *pRetValFsDot1dBasePortCircuit;
		UNUSED_PARAM(ppfsDot1dBasePortCircuit); 
		MEMSET (&pRetValFsDot1dBasePortCircuit, 0, sizeof(tSNMP_OID_TYPE));
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dBasePortTable(
								i4fsDot1dBasePort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBasePortCircuit(
						i4fsDot1dBasePort,
						pRetValFsDot1dBasePortCircuit);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dBasePortCircuitGet */

/********************************************************************
 * FUNCTION NcFsDot1dBasePortDelayExceededDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBasePortDelayExceededDiscardsGet (
				INT4 i4fsDot1dBasePort,
				UINT4 *pu4fsDot1dBasePortDelayExceededDiscards )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dBasePortTable(
								i4fsDot1dBasePort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBasePortDelayExceededDiscards(
						i4fsDot1dBasePort,
						pu4fsDot1dBasePortDelayExceededDiscards );

		return i1RetVal;


} /* NcFsDot1dBasePortDelayExceededDiscardsGet */

/********************************************************************
 * FUNCTION NcFsDot1dBasePortMtuExceededDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dBasePortMtuExceededDiscardsGet (
				INT4 i4fsDot1dBasePort,
				UINT4 *pu4fsDot1dBasePortMtuExceededDiscards )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dBasePortTable(
								i4fsDot1dBasePort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dBasePortMtuExceededDiscards(
						i4fsDot1dBasePort,
						pu4fsDot1dBasePortMtuExceededDiscards );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dBasePortMtuExceededDiscardsGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpProtocolSpecificationGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpProtocolSpecificationGet (
				INT4 i4fsDot1dStpContextId,
				INT4 pi4fsDot1dStpProtocolSpecification )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpProtocolSpecification(
						i4fsDot1dStpContextId,
						&pi4fsDot1dStpProtocolSpecification );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpProtocolSpecificationGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPrioritySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPrioritySet (
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpPriority )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpPriority(
						i4fsDot1dStpContextId,
						i4fsDot1dStpPriority);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPrioritySet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPriorityTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPriorityTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpPriority )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpPriority(pu4Error,
						i4fsDot1dStpContextId,
						i4fsDot1dStpPriority);
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpPriorityTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpTimeSinceTopologyChangeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpTimeSinceTopologyChangeGet (
				INT4 i4fsDot1dStpContextId,
				UINT4 *pu4fsDot1dStpTimeSinceTopologyChange )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpTimeSinceTopologyChange(
						i4fsDot1dStpContextId,
						pu4fsDot1dStpTimeSinceTopologyChange );
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpTimeSinceTopologyChangeGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpTopChangesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpTopChangesGet (
				INT4 i4fsDot1dStpContextId,
				UINT4 *pu4fsDot1dStpTopChanges )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpTopChanges(
						i4fsDot1dStpContextId,
						pu4fsDot1dStpTopChanges );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpTopChangesGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpDesignatedRootGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpDesignatedRootGet (
				INT4 i4fsDot1dStpContextId,
				UINT1 *ppfsDot1dStpDesignatedRoot )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE v_pfsDot1dStpDesignatedRoot;
		memset (&v_pfsDot1dStpDesignatedRoot, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}
		v_pfsDot1dStpDesignatedRoot.pu1_OctetList=ppfsDot1dStpDesignatedRoot;
		i1RetVal = nmhGetFsDot1dStpDesignatedRoot(
						i4fsDot1dStpContextId,
						&v_pfsDot1dStpDesignatedRoot );

		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpDesignatedRootGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpRootCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpRootCostGet (
				INT4 i4fsDot1dStpContextId,
				INT4 *pi4fsDot1dStpRootCost )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpRootCost(
						i4fsDot1dStpContextId,
						pi4fsDot1dStpRootCost );
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpRootCostGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpRootPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpRootPortGet (
				INT4 i4fsDot1dStpContextId,
				INT4 *pi4fsDot1dStpRootPort )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpRootPort(
						i4fsDot1dStpContextId,
						pi4fsDot1dStpRootPort );
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpRootPortGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpMaxAgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpMaxAgeGet (
				INT4 i4fsDot1dStpContextId,
				INT4 *pi4fsDot1dStpMaxAge )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpMaxAge(
						i4fsDot1dStpContextId,
						pi4fsDot1dStpMaxAge );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpMaxAgeGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpHelloTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpHelloTimeGet (
				INT4 i4fsDot1dStpContextId,
				INT4 *pi4fsDot1dStpHelloTime )
{

		INT1 i1RetVal;

		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpHelloTime(
						i4fsDot1dStpContextId,
						pi4fsDot1dStpHelloTime );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpHelloTimeGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpHoldTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpHoldTimeGet (
				INT4 i4fsDot1dStpContextId,
				INT4 *pi4fsDot1dStpHoldTime )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpHoldTime(
						i4fsDot1dStpContextId,
						pi4fsDot1dStpHoldTime );
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpHoldTimeGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpForwardDelayGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpForwardDelayGet (
				INT4 i4fsDot1dStpContextId,
				INT4 *pi4fsDot1dStpForwardDelay )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpTable(
								i4fsDot1dStpContextId) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpForwardDelay(
						i4fsDot1dStpContextId,
						pi4fsDot1dStpForwardDelay );
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpForwardDelayGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpBridgeMaxAgeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpBridgeMaxAgeSet (
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpBridgeMaxAge )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpBridgeMaxAge(
						i4fsDot1dStpContextId,
						i4fsDot1dStpBridgeMaxAge);
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpBridgeMaxAgeSet */

/********************************************************************
 * FUNCTION NcFsDot1dStpBridgeMaxAgeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpBridgeMaxAgeTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpBridgeMaxAge )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpBridgeMaxAge(pu4Error,
						i4fsDot1dStpContextId,
						i4fsDot1dStpBridgeMaxAge);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpBridgeMaxAgeTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpBridgeHelloTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpBridgeHelloTimeSet (
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpBridgeHelloTime )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpBridgeHelloTime(
						i4fsDot1dStpContextId,
						i4fsDot1dStpBridgeHelloTime);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpBridgeHelloTimeSet */

/********************************************************************
 * FUNCTION NcFsDot1dStpBridgeHelloTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpBridgeHelloTimeTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpBridgeHelloTime )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpBridgeHelloTime(pu4Error,
						i4fsDot1dStpContextId,
						i4fsDot1dStpBridgeHelloTime);
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpBridgeHelloTimeTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpBridgeForwardDelaySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpBridgeForwardDelaySet (
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpBridgeForwardDelay )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpBridgeForwardDelay(
						i4fsDot1dStpContextId,
						i4fsDot1dStpBridgeForwardDelay);
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpBridgeForwardDelaySet */

/********************************************************************
 * FUNCTION NcFsDot1dStpBridgeForwardDelayTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpBridgeForwardDelayTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpContextId,
				INT4 i4fsDot1dStpBridgeForwardDelay )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpBridgeForwardDelay(pu4Error,
						i4fsDot1dStpContextId,
						i4fsDot1dStpBridgeForwardDelay);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpBridgeForwardDelayTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortPrioritySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortPrioritySet (
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortPriority )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpPortPriority(
						i4fsDot1dStpPort,
						i4fsDot1dStpPortPriority);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortPrioritySet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortPriorityTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortPriorityTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortPriority )
{
#ifdef RSTP_WANTED
		INT1 i1RetVal;
		AST_LOCK ();
		/* if (AST_IS_RST_STARTED ())*/
		/*{*/
		i1RetVal = nmhTestv2FsDot1dStpPortPriority(pu4Error,
						i4fsDot1dStpPort,
						i4fsDot1dStpPortPriority);
		/*}*/
		AST_UNLOCK ();
		return i1RetVal;
#else
		UNUSED_PARAM (pu4Error);
		UNUSED_PARAM (i4fsDot1dStpPort);
		UNUSED_PARAM (i4fsDot1dStpPortPriority);
		return SNMP_FAILURE;
#endif

} /* NcFsDot1dStpPortPriorityTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortStateGet (
				INT4 i4fsDot1dStpPort,
				INT4 pi4fsDot1dStpPortState )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpPortTable(
								i4fsDot1dStpPort) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpPortState(
						i4fsDot1dStpPort,
						&pi4fsDot1dStpPortState );

		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpPortStateGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortEnableSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortEnableSet (
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortEnable )
{

		INT1 i1RetVal;

		i1RetVal = nmhSetFsDot1dStpPortEnable(
						i4fsDot1dStpPort,
						i4fsDot1dStpPortEnable);

		return i1RetVal;


} /* NcFsDot1dStpPortEnableSet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortEnableTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortEnableTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpPort,
				INT4	i4fsDot1dStpPortEnable )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpPortEnable(pu4Error,
						i4fsDot1dStpPort,
						i4fsDot1dStpPortEnable);

		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpPortEnableTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortPathCostSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortPathCostSet (
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortPathCost )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpPortPathCost(
						i4fsDot1dStpPort,
						i4fsDot1dStpPortPathCost);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortPathCostSet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortPathCostTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortPathCostTest (UINT4 *pu4Error,
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortPathCost )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpPortPathCost(pu4Error,
						i4fsDot1dStpPort,
						i4fsDot1dStpPortPathCost);
		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpPortPathCostTest */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortDesignatedRootGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortDesignatedRootGet (
				INT4 i4fsDot1dStpPort,
				UINT1 *ppfsDot1dStpPortDesignatedRoot )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE FsDot1dStpPortDesignatedRoot;
		MEMSET (&FsDot1dStpPortDesignatedRoot,0,sizeof(tSNMP_OCTET_STRING_TYPE));
		FsDot1dStpPortDesignatedRoot.pu1_OctetList = (unsigned char *)ppfsDot1dStpPortDesignatedRoot;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpPortTable(
								i4fsDot1dStpPort) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpPortDesignatedRoot(
						i4fsDot1dStpPort,
						&FsDot1dStpPortDesignatedRoot );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortDesignatedRootGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortDesignatedCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortDesignatedCostGet (
				INT4 i4fsDot1dStpPort,
				INT4 *pi4fsDot1dStpPortDesignatedCost )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpPortTable(
								i4fsDot1dStpPort) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpPortDesignatedCost(
						i4fsDot1dStpPort,
						pi4fsDot1dStpPortDesignatedCost );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortDesignatedCostGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortDesignatedBridgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortDesignatedBridgeGet (
				INT4 i4fsDot1dStpPort,
				UINT1 *ppfsDot1dStpPortDesignatedBridge )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE FsDot1dStpPortDesignatedBridge;
		memset (&FsDot1dStpPortDesignatedBridge, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
		FsDot1dStpPortDesignatedBridge.pu1_OctetList =ppfsDot1dStpPortDesignatedBridge;
		AST_LOCK ();

		if (nmhValidateIndexInstanceFsDot1dStpPortTable(
								i4fsDot1dStpPort) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpPortDesignatedBridge(
						i4fsDot1dStpPort,
						&FsDot1dStpPortDesignatedBridge );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortDesignatedBridgeGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortDesignatedPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortDesignatedPortGet (
				INT4 i4fsDot1dStpPort,
				UINT1 *ppfsDot1dStpPortDesignatedPort )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE  FsDot1dStpPortDesignatedPort;
		MEMSET (&FsDot1dStpPortDesignatedPort,0,sizeof(tSNMP_OCTET_STRING_TYPE));
		FsDot1dStpPortDesignatedPort.pu1_OctetList = (unsigned char *)ppfsDot1dStpPortDesignatedPort;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpPortTable(
								i4fsDot1dStpPort) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpPortDesignatedPort(
						i4fsDot1dStpPort,
						&FsDot1dStpPortDesignatedPort );
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortDesignatedPortGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortForwardTransitionsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortForwardTransitionsGet (
				INT4 i4fsDot1dStpPort,
				UINT4 *pu4fsDot1dStpPortForwardTransitions )
{

		INT1 i1RetVal;
		AST_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dStpPortTable(
								i4fsDot1dStpPort) == SNMP_FAILURE)

		{
				AST_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dStpPortForwardTransitions(
						i4fsDot1dStpPort,
						pu4fsDot1dStpPortForwardTransitions );

		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpPortForwardTransitionsGet */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortPathCost32Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortPathCost32Set (
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortPathCost32 )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhSetFsDot1dStpPortPathCost32(
						i4fsDot1dStpPort,
						i4fsDot1dStpPortPathCost32);
		AST_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStpPortPathCost32Set */

/********************************************************************
 * FUNCTION NcFsDot1dStpPortPathCost32Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStpPortPathCost32Test (UINT4 *pu4Error,
				INT4 i4fsDot1dStpPort,
				INT4 i4fsDot1dStpPortPathCost32 )
{

		INT1 i1RetVal;
		AST_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStpPortPathCost32(pu4Error,
						i4fsDot1dStpPort,
						i4fsDot1dStpPortPathCost32);

		AST_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStpPortPathCost32Test */

/********************************************************************
 * FUNCTION NcFsDot1dTpLearnedEntryDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpLearnedEntryDiscardsGet (
				INT4 i4fsDot1dBaseContextId,
				UINT4 *pu4fsDot1dTpLearnedEntryDiscards )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpTable(
								i4fsDot1dBaseContextId) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpLearnedEntryDiscards(
						i4fsDot1dBaseContextId,
						pu4fsDot1dTpLearnedEntryDiscards );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpLearnedEntryDiscardsGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpAgingTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpAgingTimeSet (
				INT4 i4fsDot1dBaseContextId,
				INT4 i4fsDot1dTpAgingTime )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetFsDot1dTpAgingTime(
						i4fsDot1dBaseContextId,
						i4fsDot1dTpAgingTime);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpAgingTimeSet */

/********************************************************************
 * FUNCTION NcFsDot1dTpAgingTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpAgingTimeTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBaseContextId,
				INT4 i4fsDot1dTpAgingTime )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2FsDot1dTpAgingTime(pu4Error,
						i4fsDot1dBaseContextId,
						i4fsDot1dTpAgingTime);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpAgingTimeTest */

/********************************************************************
 * FUNCTION NcFsDot1dTpFdbPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpFdbPortGet (
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *pfsDot1dTpFdbAddress,
				INT4 *pi4fsDot1dTpFdbPort )
{

		INT1 i1RetVal;
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(pfsDot1dTpFdbAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, pfsDot1dTpFdbAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpFdbTable(
								i4fsDot1dBaseContextId,
								au1MacAddr) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpFdbPort(
						i4fsDot1dBaseContextId,
						au1MacAddr,
						pi4fsDot1dTpFdbPort );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpFdbPortGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpFdbStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpFdbStatusGet (
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *pfsDot1dTpFdbAddress,
				INT4 i4fsDot1dTpFdbStatus )
{

		INT1 i1RetVal;
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(pfsDot1dTpFdbAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, pfsDot1dTpFdbAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);

		if (nmhValidateIndexInstanceFsDot1dTpFdbTable(
								i4fsDot1dBaseContextId,
								au1MacAddr) == SNMP_FAILURE)

		{
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpFdbStatus(
						i4fsDot1dBaseContextId,
						au1MacAddr,
						&i4fsDot1dTpFdbStatus );

		return i1RetVal;


} /* NcFsDot1dTpFdbStatusGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortMaxInfoGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortMaxInfoGet (
				INT4 i4fsDot1dTpPort,
				INT4 *pi4fsDot1dTpPortMaxInfo )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortMaxInfo(
						i4fsDot1dTpPort,
						pi4fsDot1dTpPortMaxInfo );
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortMaxInfoGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortInFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortInFramesGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu4fsDot1dTpPortInFrames )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortInFrames(
						i4fsDot1dTpPort,
						pu4fsDot1dTpPortInFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortInFramesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortOutFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortOutFramesGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu4fsDot1dTpPortOutFrames )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortOutFrames(
						i4fsDot1dTpPort,
						pu4fsDot1dTpPortOutFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortOutFramesGet */

/********************************************************************
 * FUNCTION NcFsDot1dTpPortInDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dTpPortInDiscardsGet (
				INT4 i4fsDot1dTpPort,
				UINT4 *pu4fsDot1dTpPortInDiscards )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceFsDot1dTpPortTable(
								i4fsDot1dTpPort) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetFsDot1dTpPortInDiscards(
						i4fsDot1dTpPort,
						pu4fsDot1dTpPortInDiscards );
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dTpPortInDiscardsGet */

/********************************************************************
 * FUNCTION NcFsDot1dStaticRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStaticRowStatusSet (
				INT4 i4fsDot1dBaseContextId,
				const    UINT1 *pfsDot1dStaticAddress,
				INT4 i4fsDot1dStaticReceivePort,
				INT4 i4fsDot1dStaticRowStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(pfsDot1dStaticAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, pfsDot1dStaticAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);


		i1RetVal = nmhSetFsDot1dStaticRowStatus(
						i4fsDot1dBaseContextId,
						au1MacAddr,
						i4fsDot1dStaticReceivePort,
						i4fsDot1dStaticRowStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStaticRowStatusSet */

/********************************************************************
 * FUNCTION NcFsDot1dStaticRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStaticRowStatusTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *pfsDot1dStaticAddress,
				INT4 i4fsDot1dStaticReceivePort,
				INT4 i4fsDot1dStaticRowStatus )
{

		INT1 i1RetVal;
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(pfsDot1dStaticAddress)+ 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, pfsDot1dStaticAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);


		VLAN_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStaticRowStatus(pu4Error,
						i4fsDot1dBaseContextId,
						au1MacAddr,
						i4fsDot1dStaticReceivePort,
						i4fsDot1dStaticRowStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStaticRowStatusTest */

/********************************************************************
 * FUNCTION NcFsDot1dStaticStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStaticStatusSet (
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *pfsDot1dStaticAddress,
				INT4 i4fsDot1dStaticReceivePort,
				INT4 i4fsDot1dStaticStatus )
{

		INT1 i1RetVal;
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(pfsDot1dStaticAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, pfsDot1dStaticAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);


		VLAN_LOCK ();
		i1RetVal = nmhSetFsDot1dStaticStatus(
						i4fsDot1dBaseContextId,
						au1MacAddr,
						i4fsDot1dStaticReceivePort,
						i4fsDot1dStaticStatus);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStaticStatusSet */

/********************************************************************
 * FUNCTION NcFsDot1dStaticStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStaticStatusTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *pfsDot1dStaticAddress,
				INT4 i4fsDot1dStaticReceivePort,
				INT4 i4fsDot1dStaticStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(pfsDot1dStaticAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, pfsDot1dStaticAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);

		i1RetVal = nmhTestv2FsDot1dStaticStatus(pu4Error,
						i4fsDot1dBaseContextId,
						au1MacAddr,
						i4fsDot1dStaticReceivePort,
						i4fsDot1dStaticStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcFsDot1dStaticStatusTest */

/********************************************************************
 * FUNCTION NcFsDot1dStaticAllowedIsMemberSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStaticAllowedIsMemberSet (
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *fsDot1dStaticAddress,
				INT4 i4fsDot1dStaticReceivePort,
				INT4 i4fsDot1dTpPort,
				INT4 i4fsDot1dStaticAllowedIsMember )
{

		INT1 i1RetVal;
		VLAN_LOCK (); 
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(fsDot1dStaticAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, fsDot1dStaticAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);

		i1RetVal = nmhSetFsDot1dStaticAllowedIsMember(
						i4fsDot1dBaseContextId,
						au1MacAddr,
						i4fsDot1dStaticReceivePort,
						i4fsDot1dTpPort,
						i4fsDot1dStaticAllowedIsMember);
		VLAN_UNLOCK (); 
		return i1RetVal;


} /* NcFsDot1dStaticAllowedIsMemberSet */

/********************************************************************
 * FUNCTION NcFsDot1dStaticAllowedIsMemberTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsDot1dStaticAllowedIsMemberTest (UINT4 *pu4Error,
				INT4 i4fsDot1dBaseContextId,
				const UINT1 *IfsDot1dStaticAddress,
				INT4 i4fsDot1dStaticReceivePort,
				INT4 i4fsDot1dTpPort,
				INT4 i4fsDot1dStaticAllowedIsMember )
{


		INT1 i1RetVal;
		unsigned char au1MacAddr[MAC_ADDR_LEN];
		INT4    i4Size;

		i4Size = STRLEN(IfsDot1dStaticAddress) + 1;

		UINT1   au1FdbAddress[i4Size];

		MEMSET (au1FdbAddress, '\0', i4Size);
		MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

		MEMCPY (au1FdbAddress, IfsDot1dStaticAddress, i4Size);

		CliDotStrToMac (au1FdbAddress, au1MacAddr);


		VLAN_LOCK ();
		i1RetVal = nmhTestv2FsDot1dStaticAllowedIsMember(pu4Error,
						i4fsDot1dBaseContextId,
						au1MacAddr,
						i4fsDot1dStaticReceivePort,
						i4fsDot1dTpPort,
						i4fsDot1dStaticAllowedIsMember);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcFsDot1dStaticAllowedIsMemberTest */

/* END i_ARICENT_MIStdBRIDGE_MIB.c */
