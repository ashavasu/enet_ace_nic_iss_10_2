/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlancli.c,v 1.410.2.1 2018/03/15 13:00:00 siva Exp $
 *
 * Description     : This file contains util routines for VLAN objects
 *
 *******************************************************************/
#ifndef __VLANCLI_C__
#define __VLANCLI_C__

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : vlancli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : Vlan                                             |
 * |                                                                           |
 * |  MODULE NAME           : Vlan configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           | 
 * |                          fsmpvlan.mib, fsmsvlan.mib and fsmsbext.mib      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#include "vlaninc.h"
#include "fsmsbrlw.h"
#include "mplscli.h"
#define VLAN_IFPORT_LIST_SIZE  BRG_PORT_LIST_SIZE
PRIVATE INT4        VlanLockAndSelCliContext (VOID);
UINT1               gau1VlanFrmTypeName[6][20] =
    { "", "Enet-v2", "Snap", "Snap8021H", "SnapOther", "LlcOther" };
UINT1               gau1VlanProtocolName[8][20] =
    { "", "IP ", "Novell   ", "Novell   ", "Arp      ", "Rarp     ",
    "Netbios  ", "AppleTalk "
};
UINT4               gu4VlanTrcLvl;
INT4
cli_process_vlan_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UINT1              *pau1LocalMemberPorts = NULL;
    UINT1              *pau1LocalUntaggedPorts = NULL;
    UINT1              *pau1LocalForbiddenPorts = NULL;
    UINT1              *pau1TempPortList = NULL;
    va_list             ap;
    UINT4              *args[VLAN_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = 0;
    UINT4               u4ActionFlag;
    UINT4               u4EntryStatus = 0;
    UINT4               u4ProtoFrameType;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = 0;
    UINT4               u4Index = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4VlanId = 0;
    INT4                i4RetStatus = 0;
    INT4                i4AgingTime = 0;
    INT4                i4Val = 0;
    INT4                i4PortType = 0;
    INT4                i4Status = SNMP_FAILURE;
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pau1LocalStaticPorts = NULL;
    tPortList          *pStaticPorts = NULL;
#endif /* VLAN_EXTENDED_FILTER */
    tPortList          *pMemberPorts = NULL;
    tPortList          *pUntaggedPorts = NULL;
    tPortList          *pForbiddenPorts = NULL;
    UINT1               au1ProtoValue[VLAN_CLI_MAX_PROTO_SIZE];
    UINT1               u1Variable;
    UINT1               u1Action;
    UINT1               u1LearningMode;
    UINT1              *pu1VlanList = NULL;
    UINT1              *pu1ConnectionId = NULL;
    UINT1               u1PbPortType = 0;
    tMacAddr            au1InMacAddr;
    tMacAddr            au1ConnId;
    tVlanId             VlanId;
    UINT4               u4Fid;
    UINT4               u4EgressFlag = VLAN_FALSE;
    UINT4               u4ForbiddenFlag = VLAN_FALSE;
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    UINT4               u4Flag = 0;
    UINT4               u4DelAll = 0;
    UINT2               u2LocalPortId = 0;
    UINT2               u2RcvLocalPortId;
    UINT4               u4SrcIPAddr = VLAN_INIT_VAL;
    UINT4               u4SubnetMask = VLAN_INIT_VAL;
    UINT4               u4L3IfVlanStartIndex = 0;
    UINT4               u4L3IfVlanEndIndex = 0;
    UINT4               u4Ethertype = 0;
    UINT4               u4MacAddrType = 0;
    tSNMP_OCTET_STRING_TYPE TempPorts;
    BOOL1               bIsOFVlan = OSIX_FALSE;
#ifdef TRACE_WANTED
    INT4                i4Args = 0;
#endif

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);

    VLAN_LOCK ();

    if (VlanCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    if (pu1Inst != 0)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
        if (VcmGetContextInfoFromIfIndex
            ((UINT4) u4IfIndex, &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Port not mapped to any of the context \r\n");
            i4RetStatus = CLI_FAILURE;
        }
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_VLAN_ACTIVE:

            i4RetStatus = VlanActive (CliHandle);
            break;

        case CLI_VLAN_SHUT:

            i4RetStatus = VlanShutdown (CliHandle, VLAN_SNMP_TRUE);
            break;

        case CLI_VLAN_NO_SHUT:

            i4RetStatus = VlanShutdown (CliHandle, VLAN_SNMP_FALSE);
            break;

        case CLI_VLAN_ENABLE:
            /* args[0] contains enable/disable status */

            i4RetStatus = VlanEnable (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_CONF_SW_STATS:

            i4RetStatus = VlanConfSwStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_CONF_FILT_CRITERIA:

            i4RetStatus =
                VlanConfApplyFilteringCriteria (CliHandle,
                                                CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_CREATE:
            /* args[0] - Vlan identifier to create/enter into its mode */

            i4RetStatus = VlanCreate (CliHandle, *(args[0]));
            break;

        case CLI_VLAN_NO_CREATE:
            /* args[0] - Vlan identifier to delete */

            i4RetStatus = VlanDelete (CliHandle, *(args[0]));
            break;

        case CLI_GLOBAL_MAC_LEARNING_STATUS:
            /* args[0] - contains enable/disable status */

            i4RetStatus = VlanGlobalMacLearningStatus (CliHandle,
                                                       CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_MAC_GBL:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanMacbasedOnAllPorts (CliHandle, u4ActionFlag);
            break;

        case CLI_VLAN_NO_MAC_GBL:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanMacbasedOnAllPorts (CliHandle, u4ActionFlag);
            break;

        case CLI_VLAN_SUBNET_GBL:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanSubnetbasedOnAllPorts (CliHandle, u4ActionFlag);
            break;

        case CLI_VLAN_NO_SUBNET_GBL:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanSubnetbasedOnAllPorts (CliHandle, u4ActionFlag);
            break;

        case CLI_VLAN_PROTOCOL_VLAN_GBL:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanPortProtocolBasedOnAllPorts (CliHandle,
                                                           u4ActionFlag);
            break;

        case CLI_VLAN_NO_PROTOCOL_VLAN_GBL:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanPortProtocolBasedOnAllPorts (CliHandle,
                                                           u4ActionFlag);
            break;

        case CLI_VLAN_PROTO_GROUP:
            /* fall through */
        case CLI_VLAN_NO_PROTO_GROUP:
            /* args[0] - frame type */
            /* args[1] - protocol value */
            /* args[2] - Group identifier */
            /* args[3] - Other protocol */

            /* NOTE: args[2] will be a Dummy parameter for */
            /*       case CLI_VLAN_NO_PROTO_GROUP          */

            u4ProtoFrameType = CLI_PTR_TO_U4 (args[0]);
            if (CLI_PTR_TO_U4 (args[1]) == VLAN_PROTO_OTHER)
            {
                u1Variable = (UINT1) OctetLen ((UINT1 *) args[3]);
                if (u4ProtoFrameType == VLAN_PORT_PROTO_SNAPOTHER)
                {
                    if (u1Variable != VLAN_5BYTE_PROTO_TEMP_SIZE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Protocol value should be "
                                   "in 5 bytes for snapOther frame types\r\n");
                        break;
                    }
                }
                else
                {
                    if (u1Variable != VLAN_2BYTE_PROTO_TEMP_SIZE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Protocol value should be "
                                   "in 2 bytes for enet-v2, llcOther, "
                                   "snap and snap8021H frame types\r\n");
                        break;
                    }
                }

                MEMSET (au1ProtoValue, 0, u1Variable);
                CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) args[3], au1ProtoValue);
            }
            else
            {
                MEMSET (au1ProtoValue, 0, sizeof (au1ProtoValue));

                if (VlanUtilGetProtocolValue (CLI_PTR_TO_U4 (args[1]),
                                              au1ProtoValue,
                                              &u1Variable) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%%Invalid Protocol \r\n");
                    break;
                }
            }

            if (u4Command == CLI_VLAN_PROTO_GROUP)
            {
                u1Action = VLAN_SET_CMD;
                u4GroupId = *(args[2]);
            }
            else
            {
                u1Action = VLAN_NO_CMD;
            }

            i4RetStatus = VlanUpdateProtocolGroup (CliHandle,
                                                   (UINT1) u4ProtoFrameType,
                                                   u4GroupId, au1ProtoValue,
                                                   u1Variable, u1Action);
            break;

        case CLI_VLAN_TRAFFIC_CLASS:
            /* args[0] - VLAN_SNMP_TRUE/FALSE to enable/disable */
            i4RetStatus = VlanSetTrafficClasses (CliHandle,
                                                 CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_VLAN_INTERFACE_RANGE:
            /* args[0] - interface type for member ports */
            /* args[1] - interface identifier for member ports */
            /* args[2] - vlan token */
            /* args[3] - start index of vlan */
            /* args[4] - end index of vlan */

            if (args[1] != NULL)
            {
                if (STRSTR (args[1], ",") != NULL)
                {
                    CliPrintf (CliHandle,
                               "\r%% Comma is not allowed in Range command. Use Hyphen to give range\r\n");
                    break;
                }
            }
            if (args[0] != NULL)
            {
                if (STRSTR (args[0], "ppp") != NULL)
                {
                    CliPrintf (CliHandle,
                               "\r%% PPP interface is not allowed in Range command\r\n");
                    break;
                }
            }
            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            /* build member port list */

            if (args[1] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }
            if (VlanConvertIntfRangeToLocalPortList (*pMemberPorts,
                                                     pau1LocalMemberPorts) !=
                VLAN_SUCCESS)
            {
                if (VlanConvertToOpenflowPortList (*pMemberPorts,
                                                   pau1LocalMemberPorts) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Some ports in this portlist are "
                               "not mapped to this context or part of "
                               "port-channel.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }
            }

            if (args[3] != NULL)
            {

                MEMCPY (&u4L3IfVlanStartIndex, args[3], sizeof (UINT4));

                if (args[4] != NULL)
                {
                    MEMCPY (&u4L3IfVlanEndIndex, args[4], sizeof (UINT4));
                }
                else
                {
                    u4L3IfVlanEndIndex = u4L3IfVlanStartIndex;
                }

                if (u4L3IfVlanStartIndex > u4L3IfVlanEndIndex)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid interface vlan range\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                if ((u4L3IfVlanEndIndex - u4L3IfVlanStartIndex) >
                    IP_DEV_MAX_L3VLAN_INTF)
                {
                    CliPrintf (CliHandle,
                               "\r%% Range exceeds maximum allowed vlan interfaces\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }

            if (CliChangePath (CLI_INT_RANGE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to enter into range configuration mode\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;

            }

            /* if Everything is successful */
            /* Set the CliContext PortList */
            /* Set the Clicontext If Type */
            /* Set the Clicontext If Type */

            if (args[0] != NULL)
            {
                CliSetIfRangeType ((UINT1 *) args[0]);
            }

            if (args[1] != NULL)
            {
                CliSetRangeList ((UINT1 *) args[1]);
            }

            CliSetRangeIndex (u4L3IfVlanStartIndex, u4L3IfVlanEndIndex);
            CliSetIfRangeMode ();
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            break;

        case CLI_VLAN_PORTS:
            /* args[0] - interface type for member ports */
            /* args[1] - interface identifier for member ports */
            /* args[2] - interface type for member ports */
            /* args[3] - interface identifier for member ports */
            /* args[13] - interface type for Lagg port */
            /* args[14] - interface identifier for Lagg port */
            /* args[24] - interface type for pseudo wire interface */
            /* args[25] - interface identifier for pseudo wire interface */
            /* Added for Attachment Circuit interface */
            /* args[30] - interface type for attachment circuit interface */
            /* args[31] - interface identifier for attachment circuit interface */
            /* args[20] - flag to indicate addition or deletion of ports */
            /* args[23] -  deleting all portlist in case of resetting ports
             * It is used only in case of no ports command*/
            /* args */

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pUntaggedPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pUntaggedPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pForbiddenPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pForbiddenPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1LocalUntaggedPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalUntaggedPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalUntaggedPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                break;
            }
            MEMSET (pau1LocalUntaggedPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1LocalForbiddenPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalForbiddenPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalForbiddenPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                break;
            }
            MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1TempPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1TempPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1TempPortList\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                break;
            }
            MEMSET (pau1TempPortList, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            MEMSET (pUntaggedPorts, 0, sizeof (tPortList));
            MEMSET (pForbiddenPorts, 0, sizeof (tPortList));
            TempPorts.i4_Length = VLAN_PORT_LIST_SIZE;
            TempPorts.pu1_OctetList = pau1TempPortList;

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1TempPortList);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            u4VlanId = (UINT4) i4Val;

            u4Flag = (CLI_PTR_TO_U4 (args[20]));
            u4DelAll = (CLI_PTR_TO_U4 (args[23]));
            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[1] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[3] != NULL)
            {
                /* Currently all interfaces are represented as ethernet */
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }

            /* Add Lagg Port also as member if given */
            if (args[14] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[13], (INT1 *) args[14],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }
/* Added for pseudo wire visibility */
            if (args[25] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[24], (INT1 *) args[25],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Pseudo wire"
                               " Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }
            /* Added for Attachment Circuit Interface */
            if (args[31] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[30], (INT1 *) args[31],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                               " Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }

            if (VlanCfaIsThisOpenflowVlan (u4VlanId) == OSIX_SUCCESS)
            {
                bIsOFVlan = OSIX_TRUE;
            }

            if (bIsOFVlan == OSIX_FALSE)
            {
                if (VlanConvertToLocalPortList (*pMemberPorts,
                                                pau1LocalMemberPorts) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Some ports in this portlist are "
                               "not mapped to this context or part of "
                               "port-channel.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }
                /* This is done only in the case of no ports command
                 * args[21] -  resetting all ports in member port list*/
                if ((args[21] != NULL) || (u4DelAll == 1))
                {
                    nmhGetDot1qVlanStaticEgressPorts (u4VlanId, &TempPorts);
                    MEMCPY (pau1LocalMemberPorts, TempPorts.pu1_OctetList,
                            VLAN_PORT_LIST_SIZE);
                    MEMSET (TempPorts.pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);
                }
            }
            /* args[4] - interface type for untagged ports */
            /* args[5] - interface identifier for untagged ports */
            /* args[6] - interface type for untagged ports */
            /* args[7] - interface identifier for untagged ports */
            /* args[15] - interface type for Lagg port */
            /* args[16] - interface identifier for Lagg port */
            /* args[26] - interface type for pseudo wire interface */
            /* args[27] - interface identifier for pseudo wire interface */
            /* Added for Attachment Circuit interface */
            /* args[32] - interface type for attachment circuit interface */
            /* args[33] - interface identifier for attachment circuit interface */

            /* build untagged port list */

            if (args[19] != NULL || (u4DelAll == 1))
            {
                /* Untagged ports are set as ALL */
                if (u4Flag == VLAN_ADD || u4Flag == VLAN_UPDATE)
                {
                    MEMCPY (pUntaggedPorts, pMemberPorts, sizeof (tPortList));
                }
                else if (bIsOFVlan == OSIX_FALSE)
                {
                    {
                        nmhGetDot1qVlanStaticUntaggedPorts (u4VlanId,
                                                            &TempPorts);
                        MEMCPY (pau1LocalUntaggedPorts, TempPorts.pu1_OctetList,
                                VLAN_PORT_LIST_SIZE);
                        MEMSET (TempPorts.pu1_OctetList, 0,
                                VLAN_PORT_LIST_SIZE);

                    }

                }
            }
            else
            {
                /* Check if fastethernet ports are given */
                if (args[5] != NULL)
                {
                    i4RetStatus =
                        VlanCfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                             *pUntaggedPorts,
                                             sizeof (tPortList));
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Untagged"
                                   " Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }

                }

                /* Check if gigabitethernet ports are given */
                if (args[7] != NULL)
                {
                    i4RetStatus =
                        VlanCfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                             *pUntaggedPorts,
                                             sizeof (tPortList));
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Untagged "
                                   "Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }

                }

                /* Add Lagg Port also as member if given */
                if (args[16] != NULL)
                {
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[15],
                                                       (INT1 *) args[16],
                                                       *pUntaggedPorts,
                                                       sizeof (tPortList));
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Port Channel "
                                   "Untagged Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }

                }
/* Added for pseudo wire visibility */
                /* Add pseudo wire also as member if given */
                if (args[27] != NULL)
                {
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[26],
                                                       (INT1 *) args[27],
                                                       *pUntaggedPorts,
                                                       sizeof (tPortList));
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Pseudo wire "
                                   "Untagged Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }

                }
                /* Added for Attachment Circuit Interface */
                if (args[33] != NULL)
                {
                    i4RetStatus =
                        VlanCfaCliGetIfList ((INT1 *) args[32],
                                             (INT1 *) args[33], *pUntaggedPorts,
                                             sizeof (tPortList));
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                                   " Member Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }
                }
            }
            /*  local portlist this conversion is done only when the user provides the portlist 
             *  while resetting ports */

            if (!
                ((u4Flag == VLAN_DELETE && args[19] != NULL)
                 || (u4Flag == VLAN_DELETE && u4DelAll == 1)))
            {
                if (bIsOFVlan == OSIX_FALSE)
                {
                    if (VlanConvertToLocalPortList (*pUntaggedPorts,
                                                    pau1LocalUntaggedPorts) !=
                        VLAN_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Some ports in this portlist are "
                                   "not mapped to this context or part of "
                                   "port-channel.\r\n");
                        i4RetStatus = CLI_FAILURE;
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }
                }
            }

            /* args[8]  - interface type for forbidden ports */
            /* args[9]  - interface identifier for forbidden ports */
            /* args[10] - interface type for forbidden ports */
            /* args[11] - interface identifier for forbidden ports */
            /* args[17] - interface type for Lagg port */
            /* args[18] - interface identifier for Lagg port */
            /* args[28] - interface type for pseudo wire interface */
            /* args[29] - interface identifier for pseudo wire interface */
            /* Added for Attachment Circuit interface */
            /* args[34] - interface type for attachment circuit interface */
            /* args[35] - interface identifier for attachment circuit interface */

            /* build forbidden port list */
            /* Check if fastethernet ports are given */
            if (args[9] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[8], (INT1 *) args[9],
                                         *pForbiddenPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[11] != NULL)
            {
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[10],
                                                   (INT1 *) args[11],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }

            /* Add Lagg Port also as member if given */
            if (args[18] != NULL)
            {
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[17],
                                                   (INT1 *) args[18],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port Channel Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }
/* Added for pseudo wire visibility */
            /* Add pseudo wire  also as member if given */
            if (args[29] != NULL)
            {
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[28],
                                                   (INT1 *) args[29],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Pseudo wire Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }

            }

            /* Added for Attachment Circuit Interface */
            if (args[35] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[34], (INT1 *) args[35],
                                         *pForbiddenPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                               " Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }
            }

            if (bIsOFVlan == OSIX_FALSE)
            {
                if (VlanConvertToLocalPortList (*pForbiddenPorts,
                                                pau1LocalForbiddenPorts) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Some ports in this portlist are "
                               "not mapped to this context or part of "
                               "port-channel.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1TempPortList);
                    break;
                }
                /* This is done only in the case of no ports command
                 * args[22] -  resetting all ports in forbidden port list*/
                if ((args[22] != NULL) || (u4DelAll == 1))
                {
                    nmhGetDot1qVlanForbiddenEgressPorts (u4VlanId, &TempPorts);
                    MEMCPY (pau1LocalForbiddenPorts, TempPorts.pu1_OctetList,
                            VLAN_PORT_LIST_SIZE);
                    MEMSET (TempPorts.pu1_OctetList, 0, VLAN_PORT_LIST_SIZE);
                }
                if (u4Flag == VLAN_DELETE)
                {
                    i4RetStatus =
                        VlanFormPortList (CliHandle, pau1LocalMemberPorts,
                                          pau1LocalUntaggedPorts,
                                          pau1LocalForbiddenPorts);
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Portlist\r\n");
                        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1TempPortList);
                        break;
                    }
                }
                i4RetStatus = VlanSetPorts (CliHandle, pau1LocalMemberPorts,
                                            pau1LocalUntaggedPorts,
                                            pau1LocalForbiddenPorts, u4Flag);

            }
            else
            {
                /* Delete the Vlan as it has been created when you entered the Vlan Mode */
                VlanDelete (CliHandle, u4VlanId);
                if ((i4RetStatus =
                     VlanOfcVlanSetPorts (u4VlanId, (UINT1 *) pMemberPorts,
                                          (UINT1 *) pUntaggedPorts,
                                          u4Flag)) == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to update portlist for openflowvlan %d\r\n",
                               u4VlanId);
                    CLI_SET_ERR (0);
                }
            }

            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
            UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
            UtilPlstReleaseLocalPortList (pau1TempPortList);
            break;

        case CLI_VLAN_MAC_STATIC_UNICAST:
            /* args[0] - MAC Address */

            /* args[1] - interface type for member ports       */
            /* args[2] - interface identifier for member ports */
            /* args[3] - interface type for member ports       */
            /* args[4] - interface identifier for member ports */

            /* args[5] - Status of the configured MAC entry */
            /* args[6] - interface type for member ports */
            /* args[7] - interface identifier for member ports */
            /* args[8] - vlan id */
            /* args[9] - Connection-identifier */

            /* args[10] - interface type for member ports       */
            /* args[11] - interface identifier for member ports */
            /* Added for Attachment Circuit Inteface */
            /* args[12] - interface type for member ports       */
            /* args[13] - interface identifier for member ports */

            /* build member port list */
            /* Check if fastethernet ports are given */

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = 0;
            }

            u4EntryStatus = CLI_PTR_TO_U4 (args[5]);

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            MEMSET (au1ConnId, 0, sizeof (tMacAddr));

            if (args[9] != NULL)
            {
                StrToMac ((UINT1 *) args[9], au1ConnId);
                pu1ConnectionId = au1ConnId;
            }

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pMemberPorts, 0, sizeof (tPortList));

            if (args[2] != NULL)
            {
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[1],
                                                   (INT1 *) args[2],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[4] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[7] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }
            /* Added for pseudo wire visibility */
            /* Check if pseudo wire interfaces are given.
             * IMPORTANT: the pseudo wire interfaces should be already 
             * mapped to a VLAN.
             */
            if (args[11] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[10], (INT1 *) args[11],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Pseudo"
                               " Wire Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Added for Attachment Circuit Inteface */
            /* IMPORTANT: the Attachment Circuit interfaces should be already 
             * mapped to a VLAN.
             */
            if (args[13] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[12], (INT1 *) args[13],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment "
                               " Circuit Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            if (VlanConvertToLocalPortList (*pMemberPorts,
                                            pau1LocalMemberPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                i4RetStatus = CLI_FAILURE;
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                break;
            }

            i4RetStatus = VlanSetUnicastAdd (CliHandle,
                                             au1InMacAddr,
                                             (UINT4) u2RcvLocalPortId,
                                             pau1LocalMemberPorts,
                                             u4EntryStatus, *args[8],
                                             pu1ConnectionId);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            break;

        case CLI_VLAN_MAC_STATIC_MULTICAST:
            /* args[0] - MAC Address */

            /* args[1] - interface type for member ports       */
            /* args[2] - interface identifier for member ports */
            /* args[3] - interface type for member ports       */
            /* args[4] - interface identifier for member ports */

            /* args[5] - interface type for forbidden ports        */
            /* args[6] - interface identifier for forbidden ports  */
            /* args[7] - interface type for forbidden ports        */
            /* args[8] - interface identifier for forbidden ports */

            /* args[9] - Status of the configured MAC entry */
            /* args[10] - interface type for member ports       */
            /* args[11] - interface identifier for member ports */
            /* args[12] - interface type for forbidden ports        */
            /* args[13] - interface identifier for forbidden ports */

            /* args[14] - Vlan Id */

            /* args[15] - interface type for member ports       */
            /* args[16] - interface identifier for member ports */
            /* args[17] - interface type for forbidden ports        */
            /* args[18] - interface identifier for forbidden ports */

            /* Added for Attachment Circuit Interface */
            /* args[19] - interface type for member ports       */
            /* args[20] - interface identifier for member ports */
            /* args[21] - interface type for forbidden ports        */
            /* args[22] - interface identifier for forbidden ports */

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = 0;
            }

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pForbiddenPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pForbiddenPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1LocalForbiddenPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalForbiddenPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalForbiddenPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                break;
            }
            MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            MEMSET (pForbiddenPorts, 0, sizeof (tPortList));

            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[2] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[1], (INT1 *) args[2],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[4] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if Portchannel ports are given */
            if (args[11] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[10],
                                                   (INT1 *) args[11],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }
            /* Added for pseudo wire visibility */
            /* Check if Pseudo wire interfaces are given.
             * IMPORTANT: this pseudo wire interfaces should be already
             * mapped to a vlan.
             */
            if (args[16] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[15],
                                                   (INT1 *) args[16],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Psuedo Wire"
                               "  Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Added for Attachment Circuit Interface */
            /* IMPORTANT: this Attachment Circuit Interfaces should be already
             * mapped to a vlan.
             */
            if (args[20] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[19],
                                                   (INT1 *) args[20],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                               "  Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }

            if (VlanConvertToLocalPortList (*pMemberPorts,
                                            pau1LocalMemberPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (args[6] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[5], (INT1 *) args[6],
                                         *pForbiddenPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[8] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[7], (INT1 *) args[8],
                                         *pForbiddenPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[13] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[12],
                                                   (INT1 *) args[13],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Forbidden Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }
            /* Added for pseudo wire visibility */
            /* Check if Pseudo wire interfaces are given.
             * IMPORTANT: this pseudo wire interfaces should be already
             * mapped to a vlan.
             */

            if (args[18] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[17],
                                                   (INT1 *) args[18],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Pseudo Wire"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }
            /* Added for Attachment Circuit Interface */
            /* IMPORTANT: this Attachment Circuit Interfaces should be already
             * mapped to a vlan.
             */
            if (args[22] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[21],
                                                   (INT1 *) args[22],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                               "  Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }

            if (VlanConvertToLocalPortList (*pForbiddenPorts,
                                            pau1LocalForbiddenPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            u4EntryStatus = CLI_PTR_TO_U4 (args[9]);

            i4RetStatus
                = VlanMulticastEntryAdd (CliHandle, au1InMacAddr,
                                         (UINT4) u2RcvLocalPortId,
                                         pau1LocalMemberPorts,
                                         pau1LocalForbiddenPorts,
                                         u4EntryStatus, u4EgressFlag,
                                         u4ForbiddenFlag, *(args[14]));

            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
            break;

        case CLI_VLAN_DOT1D_MAC_STATIC_MULTICAST:
            /* args[0] - MAC Address */

            /* args[1] - interface type for member ports       */
            /* args[2] - interface identifier for member ports */
            /* args[3] - interface type for member ports       */
            /* args[4] - interface identifier for member ports */

            /* args[5] - interface type for forbidden ports        */
            /* args[6] - interface identifier for forbidden ports  */
            /* args[7] - interface type for forbidden ports        */
            /* args[8] - interface identifier for forbidden ports */

            /* args[9] - Status of the configured MAC entry */
            /* args[10] - interface type for member ports       */
            /* args[11] - interface identifier for member ports */
            /* args[12] - interface type for forbidden ports        */
            /* args[13] - interface identifier for forbidden ports */

            /* args[14] - Vlan Id */

            if (u4IfIndex != VLAN_INIT_VAL)
            {
                if (VlanGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = VLAN_INIT_VAL;
            }

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pForbiddenPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pForbiddenPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1LocalForbiddenPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalForbiddenPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalForbiddenPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                break;
            }
            MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            MEMSET (pForbiddenPorts, 0, sizeof (tPortList));

            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[2] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[1], (INT1 *) args[2],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[4] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if Portchannel ports are given */
            if (args[11] != NULL)
            {
                u4EgressFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[10],
                                                   (INT1 *) args[11],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }
            if (VlanConvertToLocalPortList (*pMemberPorts,
                                            pau1LocalMemberPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (args[6] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[5], (INT1 *) args[6],
                                         *pForbiddenPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[8] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[7], (INT1 *) args[8],
                                         *pForbiddenPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[13] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[12],
                                                   (INT1 *) args[13],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Forbidden Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }

            }
            if (VlanConvertToLocalPortList (*pForbiddenPorts,
                                            pau1LocalForbiddenPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            u4EntryStatus = CLI_PTR_TO_U4 (args[9]);

            i4RetStatus
                = VlanDot1dMulticastEntryAdd (CliHandle, au1InMacAddr,
                                              (UINT4) u2RcvLocalPortId,
                                              pau1LocalMemberPorts,
                                              u4EntryStatus, u4EgressFlag);

            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
            break;

        case CLI_VLAN_DOT1D_MAC_STATIC_UNICAST:
            /* args[0] - MAC Address */

            /* args[1] - interface type for member ports       */
            /* args[2] - interface identifier for member ports */
            /* args[3] - interface type for member ports       */

            /* args[4] - interface identifier for member ports */

            /* args[5] - Status of the configured MAC entry */
            /* args[6] - interface type for member ports */
            /* args[7] - interface identifier for member ports */
            /* args[8] - vlan id */
            /* args[9] - Connection-identifier */

            /* build member port list */
            /* Check if fastethernet ports are given */

            if (u4IfIndex != VLAN_INIT_VAL)
            {
                if (VlanGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = VLAN_INIT_VAL;
            }
            u4EntryStatus = CLI_PTR_TO_U4 (args[5]);

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                break;
            }

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            if (args[2] != NULL)
            {
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[1],
                                                   (INT1 *) args[2],
                                                   *pMemberPorts,
                                                   sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[4] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[7] != NULL)
            {
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                         *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            if (VlanConvertToLocalPortList (*pMemberPorts,
                                            pau1LocalMemberPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanSetDot1dUnicastAdd (CliHandle,
                                                  au1InMacAddr,
                                                  (UINT4) u2RcvLocalPortId,
                                                  pau1LocalMemberPorts,
                                                  u4EntryStatus);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            break;

        case CLI_VLAN_NO_MAC_STATIC_UNICAST:
            /* args[0] - MAC Address */
            /* args[1] - Vlan Id */

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = 0;
            }

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            i4RetStatus =
                VlanSetUnicastDel (CliHandle, au1InMacAddr,
                                   (UINT4) u2RcvLocalPortId, *(args[1]));
            break;

        case CLI_VLAN_DOT1D_NO_MAC_STATIC_UNICAST:
            /* args[0] - MAC Address */
            /* args[1] - Vlan Id */

            if (u4IfIndex != VLAN_INIT_VAL)
            {
                if (VlanGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = VLAN_INIT_VAL;
            }

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            i4RetStatus =
                VlanDot1dSetUnicastDel (CliHandle, au1InMacAddr,
                                        (UINT4) u2RcvLocalPortId,
                                        VLAN_INIT_VAL);
            break;

        case CLI_VLAN_NO_MAC_STATIC_MULTICAST:
            /* args[0] - MAC Address */
            /* args[1] - Vlan Id */

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = 0;
            }

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            i4RetStatus
                = VlanMulticastEntryDel (CliHandle, au1InMacAddr,
                                         (UINT4) u2RcvLocalPortId, *args[1]);
            break;

        case CLI_VLAN_DOT1D_NO_MAC_STATIC_MULTICAST:
            /* args[0] - MAC Address */
            /* args[1] - Vlan Id */

            if (u4IfIndex != VLAN_INIT_VAL)
            {
                if (VlanGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                   &u4RcvContextId,
                                                   &u2RcvLocalPortId) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4RcvContextId != u4ContextId)
                {
                    CliPrintf (CliHandle, "\r%% Receive Port is not mapped "
                               "with this context.\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u2RcvLocalPortId = VLAN_INIT_VAL;
            }

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            i4RetStatus
                = VlanDot1dMulticastEntryDel (CliHandle, au1InMacAddr,
                                              (UINT4) u2RcvLocalPortId,
                                              VLAN_INIT_VAL);
            break;

        case CLI_VLAN_MAC_MAP_ADD:
            /* args[0] - MAC Address */
            /* args[1] - VlanId */
            /* args[2] - m-cast/b-cast option */

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            i4RetStatus
                = VlanUpdateMacMapEntry (CliHandle, au1InMacAddr, (UINT4)
                                         u2LocalPortId,
                                         *args[1],
                                         CLI_PTR_TO_U4 (args[2]),
                                         (UINT1) u4Command);

            if (i4RetStatus == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Mac Map Addition Failed\r\n");
            }
            break;

        case CLI_VLAN_MAC_MAP_DEL:
            /* args[0] - MAC Address */
            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus =
                VlanUpdateMacMapEntry (CliHandle, au1InMacAddr, (UINT4)
                                       u2LocalPortId, 0, 0, (UINT1) u4Command);

            if (i4RetStatus == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Mac Map Deletion Failed\r\n");
            }

            break;

        case CLI_VLAN_SUBNET_MAP_ADD:
            /* args[0] - IP Subnet Address */
            /* args[1] - VlanId */
            /* args[2] - ARP option */
            /* args[3] - Subnet Mask */
            /* args[4] - Subnet option - global/interface */

            u4SrcIPAddr = *(UINT4 *) args[0];
            if (args[3] != NULL)
            {
                u4SubnetMask = *(UINT4 *) args[3];
            }
            else
            {
                u4SubnetMask = VlanGetDefMaskFromSrcIPAddr (u4SrcIPAddr);
            }

            gu4SubnetGlobalOption = CLI_PTR_TO_U4 (args[4]);

            if (gu4SubnetGlobalOption)
            {
                u2LocalPortId = VLAN_INIT_VAL;
            }

            i4RetStatus
                = VlanUpdateSubnetMapEntry (CliHandle, u4SrcIPAddr,
                                            u4SubnetMask,
                                            (UINT4) u2LocalPortId, *args[1],
                                            (INT4) CLI_PTR_TO_U4 (args[2]),
                                            (UINT1) u4Command);

            if (i4RetStatus == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Subnet Map Addition Failed\r\n");
            }
            gu4SubnetGlobalOption = VLAN_INIT_VAL;
            break;

        case CLI_VLAN_SUBNET_MAP_DEL:
            /* args[0] - IP Subnet Address */

            u4SrcIPAddr = *(UINT4 *) args[0];
            if (args[1] != NULL)
            {
                u4SubnetMask = *(UINT4 *) args[1];
            }
            else
            {
                u4SubnetMask = VlanGetDefMaskFromSrcIPAddr (u4SrcIPAddr);
            }

            gu4SubnetGlobalOption = CLI_PTR_TO_U4 (args[2]);

            if (gu4SubnetGlobalOption)
            {
                u2LocalPortId = VLAN_INIT_VAL;
            }
            i4RetStatus =
                VlanUpdateSubnetMapEntry (CliHandle, u4SrcIPAddr, u4SubnetMask,
                                          (UINT4) u2LocalPortId, 0, 0,
                                          (UINT1) u4Command);

            if (i4RetStatus == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Subnet Map Deletion Failed\r\n");
            }
            gu4SubnetGlobalOption = VLAN_INIT_VAL;

            break;

        case CLI_VLAN_FLT_CRITERIA:
            /* args[0] - MAC Address */

            i4RetStatus
                = VlanSetPortFilteringCriteria (CliHandle,
                                                (UINT4) u2LocalPortId,
                                                (UINT2)
                                                CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_PORT_PROTECTED:

            i4RetStatus = VlanSetPortProtected (CliHandle,
                                                (UINT4) u2LocalPortId,
                                                VLAN_SNMP_TRUE);
            break;

        case CLI_VLAN_NO_PORT_PROTECTED:

            i4RetStatus = VlanSetPortProtected (CliHandle,
                                                (UINT4) u2LocalPortId,
                                                VLAN_SNMP_FALSE);
            break;

        case CLI_VLAN_PORT_LEARNING_STATUS:
            i4RetStatus = VlanSetPortUnicastMacLearningStatus (CliHandle,
                                                               (INT4)
                                                               u2LocalPortId,
                                                               CLI_PTR_TO_U4
                                                               (args[0]));
            break;
        case CLI_VLAN_PORT_SEC_LEVEL:
            i4RetStatus =
                VlanSetPortUnicastSecType (CliHandle, (INT4) u2LocalPortId,
                                           CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_PORT_ADD_UNICAST_MAC_ADDR:
            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            u4MacAddrType = VLAN_CLI_UNICAST_MAC_ADDR_TYPE_LOCKED;
            i4RetStatus =
                VlanSetPortUnicastMacAddr (CliHandle, (INT4) u2LocalPortId,
                                           au1InMacAddr, u4MacAddrType,
                                           *(args[1]));
            break;

        case CLI_VLAN_SEC_LEVEL:
            i4RetStatus =
                VlanSetUnicastSecType (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_SWITCH_MAC_LIMIT:

            i4RetStatus = VlanSetSwUnicastMacLimit (CliHandle, *(args[0]));
            break;

        case CLI_VLAN_NO_SWITCH_MAC_LIMIT:

            i4RetStatus = VlanSetSwUnicastMacLimit (CliHandle,
                                                    VLAN_DEF_UNICAST_LEARNING_LIMIT);
            break;

        case CLI_VLAN_USER_DEFINED_TPID:
            i4RetStatus =
                VlanSetUserDefinedTPID (CliHandle, *((UINT4 *) args[0]));
            break;

        case CLI_VLAN_NO_USER_DEFINED_TPID:
            i4RetStatus = VlanSetUserDefinedTPID (CliHandle, 0);
            break;

        case CLI_VLAN_PORT_ALLOWABLE_TPID:
            i4RetStatus =
                VlanSetPortTPID (CliHandle, (INT4) u2LocalPortId,
                                 CLI_PTR_TO_U4 (args[0]),
                                 CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_VLAN_PORT_ETHERTYPE:
            i4RetStatus =
                VlanSetPortEtherType (CliHandle, (INT4) u2LocalPortId,
                                      CLI_PTR_TO_U4 (args[0]),
                                      *(UINT4 *) args[1]);
            break;

        case CLI_VLAN_NO_PORT_ETHERTYPE:
            /* Default Ether Type will be selected based on the port type */
            i4RetStatus =
                VlanGetDefaultPortEtherType (u2LocalPortId,
                                             CLI_PTR_TO_U4 (args[0]),
                                             &u4Ethertype);
            i4RetStatus =
                VlanSetPortEtherType (CliHandle, (INT4) u2LocalPortId,
                                      CLI_PTR_TO_U4 (args[0]), u4Ethertype);
            break;

        case CLI_VLAN_EGRESS_ETHER_TYPE:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            i4RetStatus =
                VlanSetVlanEgressEthertype (CliHandle, VlanId,
                                            CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VLAN_NO_EGRESS_ETHER_TYPE:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            /* Default Ether Type will be selected based on the port type */

            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                u4Ethertype = VLAN_PROVIDER_PROTOCOL_ID;
            }

            else
            {
                u4Ethertype = VLAN_PROTOCOL_ID;
            }

            i4RetStatus =
                VlanSetVlanEgressEthertype (CliHandle, VlanId, u4Ethertype);
            break;

        case CLI_VLAN_PORT_EGRESS_TPID_TYPE:
            i4RetStatus =
                VlanSetPortEgressTPIDType (CliHandle, (INT4) u2LocalPortId,
                                           CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VLAN_PORT_NO_EGRESS_TPID_TYPE:
            i4RetStatus =
                VlanSetPortEgressTPIDType (CliHandle, (INT4) u2LocalPortId,
                                           CLI_VLAN_PORT_EGRESS_PORTBASED);
            break;

#ifdef VLAN_EXTENDED_FILTER
        case CLI_VLAN_FORWARD_ALL:
            pStaticPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pStaticPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pForbiddenPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pForbiddenPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalStaticPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalStaticPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalStaticPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                break;
            }
            MEMSET (pau1LocalStaticPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1LocalForbiddenPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalForbiddenPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalForbiddenPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                break;
            }
            MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pStaticPorts, 0, sizeof (tPortList));
            MEMSET (pForbiddenPorts, 0, sizeof (tPortList));

            /* args[0] - interface type for Static Forward All ports */
            /* args[1] - interface identifier for Static Forward All ports */
            /* args[2] - interface type for Static Forward All ports */
            /* args[3] - interface identifier for Static Forward All ports */
            /* args[8] - interface identifier for Static Forward All ports */
            /* args[9] - interface identifier for Static Forward All ports */
            /* args[12] - !NULL if member ports is given as NONE, */
            /*            NULL otherwise */
            /* args[13] - interface identifier for Static Forward All ports */
            /* args[14] - interface identifier for Static Forward All ports */
            /* Added for Attachment Circuit interface */
            /* args[17] - interface identifier for Static Forward All ports */
            /* args[18] - interface identifier for Static Forward All ports */

            if (args[12] != NULL)
            {
                /* Here Egress flag to be set. since its special 
                 * operation of giving NONE as forward-all ports
                 */
                u4EgressFlag = VLAN_TRUE;
            }
            else
            {
                /* build static port list */
                /* Check if fastethernet ports are given */
                if (args[1] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[0],
                                                       (INT1 *) args[1],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Member" " Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }

                /* Check if gigabitethernet ports are given */
                if (args[3] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[2],
                                                       (INT1 *) args[3],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Member " "Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }

                /* Check if portchannel ports are given */
                if (args[9] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[8],
                                                       (INT1 *) args[9],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                                   " Member Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }
                /* Added for pseudo wire visibility  */
                /* Check if pseudo wire interfaces are given.
                 * IMPORTANT: this pseudo wire interfaces should be already
                 * mapped to a vlan.
                 */
                if (args[14] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[13],
                                                       (INT1 *) args[14],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Pseudo Wire"
                                   " Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }
                /* Added for Attachment Circuit interface  */
                /* IMPORTANT: this attachment circuit interfaces should be already
                 * mapped to a vlan.
                 */
                if (args[18] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[17],
                                                       (INT1 *) args[18],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                                   " Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }
                if (VlanConvertToLocalPortList (*pStaticPorts,
                                                pau1LocalStaticPorts) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Some ports in this portlist "
                               "are not mapped to this context or part of "
                               "port-channel.\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            /* args[4] - interface type for Forbidden Forward All ports */
            /* args[5] - interface identifier for Forbidden Forward All ports */
            /* args[6] - interface type for Forbidden Forward All ports */
            /* args[7] - interface identifier for Forbidden Forward All ports */
            /* args[10] - interface type for Forbidden Forward All ports */
            /* args[11] - interface identifier for Forbidden ForwardAll ports */
            /* args[15] - interface type for Forbidden Forward All ports */
            /* args[16] - interface identifier for Forbidden ForwardAll ports */
            /* Added for Attachment Circuit interface */
            /* args[19] - interface type for Forbidden Forward All ports */
            /* args[20] - interface identifier for Forbidden ForwardAll ports */

            /* build forbidden port list */
            /* Check if fastethernet ports are given */
            if (args[5] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                         *pForbiddenPorts, sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }

            /* Check if gigabitethernet ports are given */
            if (args[7] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus =
                    VlanCfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                         *pForbiddenPorts, sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }

            /* Check if portchannel ports are given */
            if (args[11] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[10],
                                                   (INT1 *) args[11],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Forbidden Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }
/* Added for pseudo wire visibility */
            /* Check if pseudo wire interfaces are given.
             * IMPORTANT: this pseudo wire interfaces should be already
             * mapped to a vlan
             */
            if (args[16] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[15],
                                                   (INT1 *) args[16],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Pseudo Wire"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }
            /* Added for Attachment Circuit interface */
            /* IMPORTANT: this attachment circuit interfaces should be already
             * mapped to a vlan
             */
            if (args[20] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[19],
                                                   (INT1 *) args[20],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }
            if (VlanConvertToLocalPortList (*pForbiddenPorts,
                                            pau1LocalForbiddenPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanSetFwdEntry (CliHandle, pau1LocalStaticPorts,
                                           pau1LocalForbiddenPorts,
                                           u4EgressFlag,
                                           u4ForbiddenFlag, VLAN_ALL_GROUPS);

            FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
            UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
            break;

        case CLI_VLAN_NO_FORWARD_ALL:
            i4RetStatus = VlanReSetFwdEntry (CliHandle, VLAN_ALL_GROUPS);
            break;

        case CLI_VLAN_FORWARD_UNREG:
            pStaticPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pStaticPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pForbiddenPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pForbiddenPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalStaticPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalStaticPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalStaticPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                break;
            }
            MEMSET (pau1LocalStaticPorts, 0, VLAN_PORT_LIST_SIZE);

            pau1LocalForbiddenPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalForbiddenPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalForbiddenPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                break;
            }
            MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pStaticPorts, 0, sizeof (tPortList));
            MEMSET (pForbiddenPorts, 0, sizeof (tPortList));

            /* args[0] - interface type for Static Forward Unreg ports */
            /* args[1] - interface identifier for Static Forward Unreg ports */
            /* args[2] - interface type for Static Forward Unreg ports */
            /* args[3] - interface identifier for Static Forward Unreg ports */
            /* args[8] - interface type for Static Forward Unreg ports */
            /* args[9] - interface identifier for Static Forward Unreg ports */
            /* args[12] - !NULL if member ports is given as NONE, */
            /*            NULL otherwise */
            /* args[13] - interface type for Static Forward Unreg ports */
            /* args[14] - interface identifier for Static Forward Unreg ports */
            /* Added for Attachment Circuit interface */
            /* args[17] - interface type for Static Forward Unreg ports */
            /* args[18] - interface identifier for Static Forward Unreg ports */

            if (args[12] != NULL)
            {
                /* Here Egress flag to be set. since its special 
                 * operation of giving NONE as forward-unreg ports
                 */
                u4EgressFlag = VLAN_TRUE;
            }
            else
            {
                /* build static port list */
                /* Check if fastethernet ports are given */
                if (args[1] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[0],
                                                       (INT1 *) args[1],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Member " "Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }

                /* Check if gigabitethernet ports are given */
                if (args[3] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[2],
                                                       (INT1 *) args[3],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Member " "Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }

                /* Check if portchannel ports are given */
                if (args[9] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[8],
                                                       (INT1 *) args[9],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                                   " Member Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }
                /* Added for pseudo wire visibility */
                /* Check if pseudo wire interfaces are given.
                 * IMPORTANT: this pseudo wire interfaces should be already 
                 * mapped to a vlan.
                 */
                if (args[14] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[13],
                                                       (INT1 *) args[14],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Pseudo Wire"
                                   " Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }
                /* Added for Attachment Circuit interface */
                /* IMPORTANT: this attachment circuit interfaces should be already 
                 * mapped to a vlan.
                 */
                if (args[18] != NULL)
                {
                    u4EgressFlag = VLAN_TRUE;
                    i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[17],
                                                       (INT1 *) args[18],
                                                       *pStaticPorts,
                                                       sizeof (tPortList));

                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                                   " Port(s) \r\n");
                        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                        break;
                    }
                }
                if (VlanConvertToLocalPortList (*pStaticPorts,
                                                pau1LocalStaticPorts) !=
                    VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Some ports in this portlist "
                               "are not mapped to this context or part of "
                               "port-channel.\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            /* args[4] - interface type for Forbidden Fwd Unreg ports */
            /* args[5] - interface identifier for Forbidden Fwd Unreg ports */
            /* args[6] - interface type for Forbidden Fwd Unreg ports */
            /* args[7] - interface identifier for Forbidden Fwd Unreg ports */
            /* args[10] - interface type for Forbidden Fwd Unreg ports */
            /* args[11] - interface identifier for Forbidden Fwd Unreg ports */
            /* args[15] - interface type for Forbidden Fwd Unreg ports */
            /* args[16] - interface identifier for Forbidden Fwd Unreg ports */
            /* Added for Attachment Circuit interface */
            /* args[19] - interface type for Forbidden Fwd Unreg ports */
            /* args[20] - interface identifier for Forbidden Fwd Unreg ports */

            /* build forbidden port list */
            /* Check if fastethernet ports are given */
            if (args[5] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[4],
                                                   (INT1 *) args[5],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }

            /* Check if gigabitethernet ports are given */
            if (args[7] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[6],
                                                   (INT1 *) args[7],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Forbidden"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }

            /* Check if portchannel ports are given */
            if (args[11] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[10],
                                                   (INT1 *) args[11],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port Channel"
                               " Forbidden Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }
/* Added for pseudo wire visibility */
            /* Check if pseudo wire interfaces are given.
             * IMPORTANT: this pseudo  wire interfaces should be already
             * mapped to a vlan.
             */
            if (args[16] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[15],
                                                   (INT1 *) args[16],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Pseudo Wire"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }
            /* Added for Attachment Circuit interface */
            /* IMPORTANT: this attachment circuit interfaces should be already
             * mapped to a vlan.
             */
            if (args[20] != NULL)
            {
                u4ForbiddenFlag = VLAN_TRUE;
                i4RetStatus = VlanCfaCliGetIfList ((INT1 *) args[19],
                                                   (INT1 *) args[20],
                                                   *pForbiddenPorts,
                                                   sizeof (tPortList));

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment Circuit "
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                    break;
                }
            }
            if (VlanConvertToLocalPortList (*pForbiddenPorts,
                                            pau1LocalForbiddenPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
                UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanSetFwdEntry (CliHandle, pau1LocalStaticPorts,
                                           pau1LocalForbiddenPorts,
                                           u4EgressFlag,
                                           u4ForbiddenFlag, VLAN_UNREG_GROUPS);

            FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            UtilPlstReleaseLocalPortList (pau1LocalStaticPorts);
            UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
            break;

        case CLI_VLAN_NO_FORWARD_UNREG:
            i4RetStatus = VlanReSetFwdEntry (CliHandle, VLAN_UNREG_GROUPS);
            break;
#else
        case CLI_VLAN_FORWARD_ALL:
            /* fall through */
        case CLI_VLAN_NO_FORWARD_ALL:
            /* fall through */
        case CLI_VLAN_FORWARD_UNREG:
            /* fall through */
        case CLI_VLAN_NO_FORWARD_UNREG:
            CliPrintf (CliHandle, "\r%% Command not supported \r\n");
            i4RetStatus = CLI_FAILURE;
            break;
#endif

        case CLI_VLAN_PORT_PVID:
            /* args[0] - Vlan Identifier */

            i4RetStatus = VlanSetPortPvid (CliHandle, *(args[0]),
                                           (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_PORT_PVID:

            i4RetStatus = VlanSetPortPvid (CliHandle, VLAN_DEFAULT_PORT_VID,
                                           (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_PORT_MODE:

            i4RetStatus = VlanSetPortMode (CliHandle, (UINT4) u2LocalPortId,
                                           CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VLAN_NO_PORT_MODE:

            VlanL2IwfGetPbPortType ((UINT4) u2LocalPortId, &u1PbPortType);

            if ((u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) ||
                (u1PbPortType == VLAN_CNP_PORTBASED_PORT) ||
                (u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))
            {
                i4RetStatus = VlanSetPortMode (CliHandle, (UINT4) u2LocalPortId,
                                               VLAN_ACCESS_PORT);
            }
            else
            {
                i4RetStatus = VlanSetPortMode (CliHandle, (UINT4) u2LocalPortId,
                                               VLAN_DEFAULT_PORT);
            }

            break;

        case CLI_VLAN_FRAME_TYPE:
            /* args[0] - Frame type */

            i4RetStatus = VlanSetPortFrameType (CliHandle,
                                                CLI_PTR_TO_I4 (args[0]),
                                                (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_FRAME_TYPE:

            i4Status =
                nmhGetDot1qFutureVlanPortType ((INT4) u2LocalPortId,
                                               &i4PortType);

            if (i4Status == SNMP_FAILURE)
            {
                return CLI_SUCCESS;
            }

            if (i4PortType == VLAN_ACCESS_PORT)
            {
                i4RetStatus = VlanSetPortFrameType (CliHandle,
                                                    VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES,
                                                    (UINT4) u2LocalPortId);
            }
            else
            {
                i4RetStatus = VlanSetPortFrameType (CliHandle,
                                                    VLAN_ADMIT_ALL_FRAMES,
                                                    (UINT4) u2LocalPortId);
            }
            break;

        case CLI_VLAN_FILTERING:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanSetPortFiltering (CliHandle, u4ActionFlag,
                                                (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_FILTERING:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanSetPortFiltering (CliHandle, u4ActionFlag,
                                                (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_MAC_PORT:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanSetPortMacbased (CliHandle, u4ActionFlag,
                                               (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_MAC_PORT:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanSetPortMacbased (CliHandle, u4ActionFlag,
                                               (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_SUBNET_PORT:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanSetPortSubnetbased (CliHandle, u4ActionFlag,
                                                  (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_SUBNET_PORT:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanSetPortSubnetbased (CliHandle, u4ActionFlag,
                                                  (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_PORT_PROTOCOL:

            u4ActionFlag = VLAN_SNMP_TRUE;
            i4RetStatus = VlanSetPortProtocolbased (CliHandle, u4ActionFlag,
                                                    (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_PORT_PROTOCOL:

            u4ActionFlag = VLAN_SNMP_FALSE;
            i4RetStatus = VlanSetPortProtocolbased (CliHandle, u4ActionFlag,
                                                    (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_MAP_PROTOCOL:
            /* args[0] - Group identifier */
            /* args[1] - Vlan identifier */

            i4RetStatus = VlanSetPortProtoGroup (CliHandle,
                                                 *(args[0]),
                                                 *(args[1]),
                                                 (UINT4) u2LocalPortId,
                                                 VLAN_SET_CMD);
            break;

        case CLI_VLAN_NO_MAP_PROTOCOL:
            /* args[0] - Group identifier */

            i4RetStatus = VlanSetPortProtoGroup (CliHandle,
                                                 *(args[0]), 0,
                                                 (UINT4) u2LocalPortId,
                                                 VLAN_NO_CMD);
            break;

        case CLI_VLAN_PORT_PRIO:
            /* args[0] - Priority value */

            i4RetStatus = VlanSetPortDefaultPriority (CliHandle,
                                                      *(args[0]),
                                                      (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_PORT_PRIO:

            i4RetStatus = VlanSetPortDefaultPriority (CliHandle,
                                                      VLAN_DEF_USER_PRIORITY,
                                                      (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_MAX_TRCLASS:
            /* args[0] - Traffic Class value */

            i4RetStatus = VlanSetMaxTrafficClass (CliHandle, *args[0],
                                                  (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_NO_MAX_TRCLASS:

            i4RetStatus = VlanSetMaxTrafficClass (CliHandle,
                                                  VLAN_MAX_TRAFF_CLASS,
                                                  (UINT4) u2LocalPortId);
            break;

        case CLI_VLAN_PRIO_TRCLASS:
            /* args[0] - Priority value */
            /* args[1] - Traffic Class value */

            i4RetStatus = VlanSetPortPrioAndTrafficClass (CliHandle,
                                                          *(args[0]),
                                                          *(args[1]),
                                                          (UINT4) u2LocalPortId,
                                                          VLAN_SET_CMD);
            break;

        case CLI_VLAN_NO_PRIO_TRCLASS:
            /* args[0] - Priority value */

            i4RetStatus = VlanSetPortPrioAndTrafficClass (CliHandle,
                                                          *(args[0]),
                                                          0,
                                                          (UINT4) u2LocalPortId,
                                                          VLAN_NO_CMD);
            break;

        case CLI_VLAN_SET_MAC_AGING_TIME:
            /* fall through */
        case CLI_VLAN_NO_MAC_AGING_TIME:

            if (u4Command == CLI_VLAN_SET_MAC_AGING_TIME)
            {
                /* Set the Aging time as entered by the User */
                i4AgingTime = *(args[0]);
            }
            else
            {
                /* Set the Aging time with the Default value (300 secs) */
                i4AgingTime = NORMAL_AGEOUT_TIME;
            }

            if (nmhTestv2Dot1dTpAgingTime (&u4ErrCode, i4AgingTime)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Cannot Set Aging Time. Aging Time value"
                           "should be between 10 & 1000000 \r\n");

                VLAN_UNLOCK ();

                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }
            if (nmhSetDot1dTpAgingTime (i4AgingTime) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Failed to Set Aging Time \r\n");

                VLAN_UNLOCK ();

                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = CLI_SUCCESS;
            break;

        case CLI_VLAN_CLR_VLAN_COUNTERS:

            if (args[0] != NULL)
            {
                i4RetStatus = VlanResetVlanStats (CliHandle,
                                                  (tVlanId) (*args[0]));

            }
            else
            {
                i4RetStatus = VlanResetVlanStats (CliHandle, 0);
            }

            break;

        case CLI_VLAN_LEARNING_MODE:

            /* args[0] contains learning mode */
            i4RetStatus = VlanSetLearningMode (CliHandle,
                                               CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_VLAN_DEFAULT_HYBRIDTYPE:

            i4RetStatus = VlanSetHybridTypeDefault (CliHandle,
                                                    CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VLAN_WILDCARD_ENTRY:

            if (args[0] != NULL)
            {
                StrToMac ((UINT1 *) args[0], au1InMacAddr);
            }
            else
            {
                /*If specfic mac address is not given, set it as
                 * broadcast address.
                 */
                MEMSET (au1InMacAddr, 0xff, sizeof (tMacAddr));
            }

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            pau1LocalMemberPorts =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1LocalMemberPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "cli_process_vlan_cmd: Error in allocating memory "
                          "for pau1LocalMemberPorts\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                break;
            }
            MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

            MEMSET (pMemberPorts, 0, sizeof (tPortList));

            if (args[2] != NULL)
            {
                i4RetStatus = CfaCliGetIfList ((INT1 *) args[1],
                                               (INT1 *) args[2],
                                               *pMemberPorts,
                                               sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[4] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                     *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[6] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[5], (INT1 *) args[6],
                                     *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }
/* Added for pseudo wire visibility */
            /* Check if pseudo wire ports are given.
             * IMPORTANT: the pseudo wire interfaces should be already 
             * mapped to a vlan.
             */
            if (args[8] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[7], (INT1 *) args[8],
                                     *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Pseudo "
                               " Wire Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }
            /* Added for Attachment Circuit Interface */
            /* IMPORTANT: the attachment circuit interfaces should be already 
             * mapped to a vlan.
             */
            if (args[10] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[9], (INT1 *) args[10],
                                     *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Attachment "
                               " Circuit Port(s)\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                    break;
                }

            }
            if (VlanConvertToLocalPortList (*pMemberPorts,
                                            pau1LocalMemberPorts) !=
                VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                           "not mapped to this context or part of "
                           "port-channel.\r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanAddWildCardEntry (CliHandle, au1InMacAddr,
                                                pau1LocalMemberPorts);

            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
            break;

        case CLI_VLAN_NO_WILDCARD_ENTRY:

            if (args[0] != NULL)
            {
                StrToMac ((UINT1 *) args[0], au1InMacAddr);
            }
            else
            {
                /*If specfic mac address is not given, set it as
                 * broadcast address.
                 */
                MEMSET (au1InMacAddr, 0xff, sizeof (tMacAddr));
            }

            i4RetStatus = VlanDelWildCardEntry (CliHandle, au1InMacAddr);

            break;

        case CLI_VLAN_MAPFID_VLAN:

            u1LearningMode = VlanGetVlanLearningMode ();

            if (u1LearningMode != VLAN_HYBRID_LEARNING)
            {
                CliPrintf (CliHandle, "\r%% Command Invalid for the learning"
                           "mode operational on this switch.\r\n");
                VLAN_UNLOCK ();

                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            u1Action = VLAN_SET_CMD;

            u4Fid = CLI_PTR_TO_U4 (args[0]);

            pu1VlanList = UtlShMemAllocVlanList ();

            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                           " failed.\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[1], pu1VlanList,
                                  VLAN_LIST_SIZE, CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1VlanList);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = VlanCliFidVlanMapping (CliHandle, u4Fid,
                                                 pu1VlanList, u1Action);
            UtlShMemFreeVlanList (pu1VlanList);

            break;

        case CLI_VLAN_NOMAPFID_VLAN:

            u1LearningMode = VlanGetVlanLearningMode ();

            if (u1LearningMode != VLAN_HYBRID_LEARNING)
            {
                CliPrintf (CliHandle, "\r%% Command Invalid for the learning"
                           "mode operational on this switch.\r\n");
                VLAN_UNLOCK ();

                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            u1Action = VLAN_NO_CMD;

            u4Fid = 0;

            pu1VlanList = UtlShMemAllocVlanList ();

            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                           " failed.\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[0], pu1VlanList,
                                  VLAN_LIST_SIZE, CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1VlanList);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = VlanCliFidVlanMapping (CliHandle, u4Fid,
                                                 pu1VlanList, u1Action);
            UtlShMemFreeVlanList (pu1VlanList);

            break;

        case CLI_VLAN_MODE_OF:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            /* Give delete vlan to vlan module and
             * Create vlan to openflow module*/

            i4RetStatus = VlanCreateOpenflowVlan (CliHandle, VlanId);

            break;

        case CLI_VLAN_NO_MODE_OF:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            i4RetStatus = VlanDeleteOpenflowVlan (CliHandle, VlanId);
            break;
        case CLI_VLAN_MAC_LEARNING_STATUS:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            i4RetStatus =
                VlanMacLearningStatus (CliHandle, VlanId,
                                       CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VLAN_MAC_LIMIT:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            i4RetStatus = VlanUnicastMacLimit (CliHandle, VlanId, *(args[0]));
            break;

        case CLI_VLAN_NO_MAC_LIMIT:

            u1LearningMode = VlanGetVlanLearningMode ();

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
                i4RetStatus = CLI_FAILURE;
                VlanReleaseContext ();
                break;
            }

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            if (u1LearningMode == VLAN_SHARED_LEARNING)

            {

                i4RetStatus = VlanUnicastMacLimit (CliHandle, VlanId,
                                                   VLAN_DYNAMIC_UNICAST_SIZE);
            }

            else
            {
                i4RetStatus = VlanUnicastMacLimit (CliHandle, VlanId,
                                                   VLAN_DEF_DYN_UCAST_ENTRIES_PER_VLAN);
            }

            break;

        case CLI_VLAN_COUNTER_STATUS:

            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            u4VlanId = (UINT4) i4Val;

            i4RetStatus = VlanSetCounterStatus (CliHandle, u4VlanId,
                                                CLI_PTR_TO_I4 (args[0]));
            break;

#ifdef PBB_WANTED
        case CLI_VLAN_ISID_VLAN_MAP:
            pu1VlanList = UtlShMemAllocVlanList ();

            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                           " failed.\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);
            if (args[0] != NULL)
            {
                if (CliStrToPortList ((UINT1 *) args[0], pu1VlanList,
                                      VLAN_LIST_SIZE,
                                      CFA_L2VLAN) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                    i4RetStatus = CLI_FAILURE;
                    UtlShMemFreeVlanList (pu1VlanList);
                    break;
                }
            }

            i4RetStatus = VlanSetVlanMapForIsid (CliHandle,
                                                 pu1VlanList,
                                                 CLI_PTR_TO_I4 (args[1]),
                                                 u2LocalPortId);
            UtlShMemFreeVlanList (pu1VlanList);
            break;
#endif
        case CLI_VLAN_BASE_BRIDGE_MODE:
            i4RetStatus =
                VlanSetBaseBridgeMode (CliHandle, CLI_PTR_TO_I4 (args[0]));

            break;

#ifdef TRACE_WANTED
        case CLI_VLAN_DEBUGS:
            /* args[0] - Context Name */
            /* args[1] - Debug Module */
            /* args[2] - Debug Type */
            if (args[3] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[3]);
                VlanCliSetDebugLevel (CliHandle, i4Args);
            }
            if (args[0] != NULL)
            {
                if (VlanVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = VLAN_DEF_CONTEXT_ID;
            }

            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanSetDebugs (CliHandle, CLI_PTR_TO_U4 (args[1]),
                                         CLI_PTR_TO_U4 (args[2]), VLAN_SET_CMD);
            break;

        case CLI_VLAN_NO_DEBUGS:
            /* args[0] - Context Name */
            /* args[1] - Debug Value */
            /* args[2] - Debug Module */

            if (args[0] != NULL)
            {
                if (VlanVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = VLAN_DEF_CONTEXT_ID;
            }

            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanSetDebugs (CliHandle, CLI_PTR_TO_U4 (args[1]),
                                         CLI_PTR_TO_U4 (args[2]), VLAN_NO_CMD);
            break;

        case CLI_VLAN_GBL_DEBUG:
            if (args[0] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[0]);
                VlanCliSetDebugLevel (CliHandle, i4Args);
            }

            i4RetStatus = VlanSetGlobalDebug (CliHandle, VLAN_ENABLED);
            break;

        case CLI_VLAN_NO_GBL_DEBUG:

            i4RetStatus = VlanSetGlobalDebug (CliHandle, VLAN_DISABLED);
            break;

#else
        case CLI_VLAN_DEBUGS:
        case CLI_VLAN_NO_DEBUGS:
        case CLI_VLAN_GBL_DEBUG:
            /* fall through */
        case CLI_VLAN_NO_GBL_DEBUG:
            CliPrintf (CliHandle, "\r%% Debug not supported \r\n");
            i4RetStatus = CLI_FAILURE;
            break;

#endif /* TRACE_WANTED */
        case CLI_ADD_UNTAG_PORT:

            MEMCPY (&u4VlanId, args[0], sizeof (UINT4));

            if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                            u2LocalPortId,
                                            &u4Index) != VCM_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (VlanL2IwfIsPortInPortChannel (u4Index) == VLAN_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Port is already a member of port-channel."
                           "Configuration not allowed.\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = VlanMapToPort (CliHandle, u4VlanId, u2LocalPortId);
            break;
        case CLI_REMOVE_UNTAG_PORT:
            VlanRemoveUntaggedPortFromVlans (CliHandle, VLAN_DEF_VLAN_ID,
                                             u2LocalPortId, (UINT1) VLAN_TRUE);
            VlanAddPortToDefaultVlan (CliHandle, (UINT4) u2LocalPortId);
            i4RetStatus = VlanSetPortPvid (CliHandle, VLAN_DEFAULT_PORT_VID,
                                           (UINT4) u2LocalPortId);
            break;
        case CLI_VLAN_FLUSH_FDB:
            if (CLI_PTR_TO_U4 (args[2]) == VLAN_REMOTE_FDB)
            {
                i4RetStatus = VlanCliFlushRemoteFdb (CliHandle);
                break;
            }
            if (args[1] != NULL)
            {
                MEMCPY (&u4VlanId, args[1], sizeof (UINT4));
            }
            i4RetStatus = VlanCliFlushFdb (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                           u4VlanId);

            break;
        case CLI_VLAN_NAME:
            if (args[0] != NULL)
                i4RetStatus = VlanSetName (CliHandle, (UINT1 *) args[0]);
            else
                i4RetStatus = VlanSetName (CliHandle, NULL);

            break;
        case CLI_VLAN_LOOPBACK_STATUS:
            u4VlanId = (UINT4) CLI_GET_VLANID ();
            i4RetStatus = VlanSetLoopbackStatus (CliHandle, u4VlanId,
                                                 (CLI_PTR_TO_I4 (args[0])));

            break;
        case CLI_PORT_REFLECTION_STATUS:
            i4RetStatus = VlanCliSetPortPacketReflectionStatus (CliHandle,
                                                                u4ContextId,
                                                                u2LocalPortId,
                                                                CLI_PTR_TO_I4
                                                                (args[0]));

            break;

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    VlanReleaseContext ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VLAN) &&
            (u4ErrCode < CLI_VLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       VlanCliErrString[CLI_ERR_OFFSET_VLAN (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

INT4
cli_process_vlan_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[VLAN_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = 0;
    INT4                i4Inst = 0;
    INT4                i4AgingTime = 0;
    tMacAddr            au1InMacAddr;
    UINT4               u4Fid;
    UINT1              *pu1MacAddr;
    UINT4               u4ContextId;
    UINT4               u4TempContextId = VLAN_CLI_INVALID_CONTEXT;
    UINT4               u4PagingStatus = 0;
    UINT2               u2LocalPortId = 0;
    UINT1              *pu1ContextName = NULL;
    tVlanId             StartVlanId = 0;
    tVlanId             LastVlanId = 0;
    UINT4               u4Showwildcardflag = 0;
    UINT4               u4VlanId = 0;
    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);

    VLAN_LOCK ();
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing 
     * IfIndex as the first argument in variable argument list. Like that 
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    while (VlanCliGetContextForShowCmd
           (CliHandle, pu1ContextName, u4IfIndex,
            u4TempContextId, &u4ContextId, &u2LocalPortId) == VLAN_SUCCESS)
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            continue;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            /* Allow only the following commands in Transparent Bridging Mode */
            if ((u4Command != CLI_VLAN_SHOW_GLOBALS) &&
                (u4Command != CLI_VLAN_DOT1D_SHOW_MAC_TABLE) &&
                (u4Command != CLI_VLAN_SHOW_DOT1D_MAC_STATIC_UCAST_TABLE) &&
                (u4Command != CLI_VLAN_SHOW_DOT1D_MAC_STATIC_MCAST_TABLE) &&
                (u4Command != CLI_VLAN_SHOW_MAC_AGING_TIME) &&
                (u4Command != CLI_VLAN_SHOW_STATS) &&
                (u4Command != CLI_VLAN_SHOW_STATISTICS))
            {
                break;
            }
        }
        switch (u4Command)
        {
            case CLI_VLAN_SHOW_VLAN_ID:

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                ShowVlanDatabase (CliHandle, u4ContextId, u4Command,
                                  StartVlanId, LastVlanId);

                break;

            case CLI_VLAN_SHOW_VLAN_BRIEF:
                /* fall through */
            case CLI_VLAN_SHOW_VLAN_SUMMARY:
                /* fall through */
            case CLI_VLAN_SHOW_VLAN_ALL:
                i4RetStatus = ShowVlanDatabase (CliHandle, u4ContextId,
                                                u4Command, 0, 0);
                break;

            case CLI_VLAN_SHOW_VLAN_ALL_ASCENDING:
                i4RetStatus = ShowVlanDatabase (CliHandle, u4ContextId,
                                                u4Command, 0, 0);
                break;

            case CLI_VLAN_RED_SHOW_VLAN_ALL:
#ifdef L2RED_WANTED
                if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
                {
                    ShowVlanGvrpLrntPortsDb (CliHandle);
                }
#endif /* L2RED_WANTED */
                break;

            case CLI_VLAN_SHOW_GLOBALS:
                i4RetStatus = ShowVlanGlobalInfo (CliHandle, u4ContextId);
                break;

            case CLI_VLAN_SHOW_FWD_ALL:
                i4RetStatus = ShowVlanForwardAll (CliHandle, u4ContextId);
                break;

            case CLI_VLAN_SHOW_FWD_UNREG:
                i4RetStatus = ShowVlanForwardUnreg (CliHandle, u4ContextId);
                break;

            case CLI_VLAN_SHOW_TRCLASS:
                i4RetStatus = ShowVlanTrafficClasses (CliHandle,
                                                      u4ContextId, u4IfIndex);
                break;

            case CLI_VLAN_SHOW_PROTO_GROUP:
                i4RetStatus = ShowVlanProtocolGroup (CliHandle, u4ContextId);
                break;

            case CLI_VLAN_SHOW_PROTOCOLS:
                i4RetStatus = ShowVlanProtocolDatabase (CliHandle, u4ContextId);
                break;

            case CLI_VLAN_SHOW_MAC:
                if (u4IfIndex != 0)
                {
                    i4RetStatus =
                        ShowVlanMacDatabaseOnPort (CliHandle, u4IfIndex);
                }
                else
                {
                    i4RetStatus = ShowVlanMacDatabase (CliHandle, u4ContextId);
                }
                break;

            case CLI_VLAN_SHOW_SUBNET:
                if (u4IfIndex != VLAN_INIT_VAL)
                {
                    i4RetStatus =
                        ShowVlanSubnetDatabaseOnPort (CliHandle, u4IfIndex);
                }
                else
                {
                    i4RetStatus =
                        ShowVlanSubnetDatabase (CliHandle, u4ContextId);
                }
                break;

            case CLI_VLAN_SHOW_STATS:

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                ShowVlanGlobalStats (CliHandle, u4ContextId,
                                     StartVlanId, LastVlanId);

                break;

            case CLI_VLAN_SHOW_STATISTICS:

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                ShowVlanStatistics (CliHandle, u4ContextId, StartVlanId,
                                    LastVlanId);

                break;

            case CLI_VLAN_SHOW_LEARNING_PARAMS:

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                ShowVlanMacLearningParams (CliHandle, u4ContextId,
                                           StartVlanId, LastVlanId);

                break;

            case CLI_VLAN_SHOW_MAC_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_PORT_SEC_UCAST_MAC_ADDR:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanStUcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_PORT_SEC_MAC:
                /* args [0] - Vlan Id */
                i4RetStatus = ShowVlanUcastMacPortSecurity
                    (CliHandle, u4ContextId, u4IfIndex);
                break;

            case CLI_VLAN_DOT1D_SHOW_MAC_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanDot1dMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, u4IfIndex);

                break;

            case CLI_VLAN_RED_SHOW_MAC_TABLE:
#ifdef L2RED_WANTED
                if (VLAN_NODE_STATUS () != VLAN_NODE_ACTIVE)
                {
                    ShowVlanGmrpLrntPortsDb (CliHandle);
                }
#endif /* L2RED_WANTED */
                break;

            case CLI_VLAN_SHOW_MAC_COUNT:
                /* args [0] - Vlan Id */
                if (args[0] == NULL)
                {
                    i4RetStatus = ShowVlanMacAddrCount (CliHandle, u4ContextId);
                }
                else
                {
                    MEMCPY (&u4VlanId, args[0], sizeof (UINT4));
                    i4RetStatus = ShowVlanMacAddrCountVlan (CliHandle,
                                                            u4ContextId,
                                                            u4VlanId,
                                                            &u4PagingStatus);
                }
                break;

            case CLI_VLAN_SHOW_MAC_STATIC_UCAST_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanStUcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_DOT1D_MAC_STATIC_UCAST_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */

                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanDot1dStUcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_MAC_STATIC_MCAST_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */
                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanStMcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_DOT1D_MAC_STATIC_MCAST_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */
                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanDot1dStMcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_MAC_DYNAMIC_UCAST_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */
                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanDynamicUcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_MAC_DYNAMIC_MCAST_TABLE:
                /* args [0] - Vlan Id */
                /* args [1] - Mac Address */
                if (VlanGetStartAndEndVlanIdFromRangeStr ((CHR1 *) args[0],
                                                          &StartVlanId,
                                                          &LastVlanId) ==
                    CLI_FAILURE)

                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");

                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                if (args[1] == NULL)
                {
                    pu1MacAddr = NULL;
                }
                else
                {
                    StrToMac (args[1], au1InMacAddr);
                    pu1MacAddr = au1InMacAddr;
                }

                i4RetStatus = ShowVlanDynamicMcastMacAddrTable
                    (CliHandle, u4ContextId, pu1MacAddr, StartVlanId,
                     LastVlanId, u4IfIndex);

                break;

            case CLI_VLAN_SHOW_MAC_AGING_TIME:
                nmhGetFsDot1dTpAgingTime (u4ContextId, &i4AgingTime);

                CliPrintf (CliHandle, "\r\nMac Address Aging Time: %d\r\n\r\n",
                           i4AgingTime);

                i4RetStatus = CLI_SUCCESS;
                break;

            case CLI_VLAN_SHOW_PORT_CONFIG:
                i4RetStatus = ShowVlanPortConfig (CliHandle, u4ContextId,
                                                  u4IfIndex);
                break;

            case CLI_VLAN_SHOW_VLAN_FID_ID:

                u4Fid = *(args[0]);

                if (VLAN_IS_FDB_ID_VALID (u4Fid) == VLAN_FALSE)
                {
                    VLAN_UNLOCK ();

                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }
                i4RetStatus
                    = VlanCliShowFidVlanMapping (CliHandle, u4ContextId,
                                                 u4Command, u4Fid);
                break;

            case CLI_VLAN_SHOW_VLAN_FID_DETAIL:
                i4RetStatus = VlanCliShowFidVlanMapping (CliHandle,
                                                         u4ContextId,
                                                         u4Command, 0);
                break;

            case CLI_VLAN_SHOW_CAPABILITIES:
                i4RetStatus = VlanCliShowDeviceCapabilities (CliHandle,
                                                             u4ContextId);
                break;

            case CLI_VLAN_SHOW_WILDCARD_ENTRY:
                if (args[0] != NULL)
                {
                    StrToMac (args[0], au1InMacAddr);
                }
                else
                {
                    /*If specfic mac address is not given, set it as
                     * broadcast address.
                     */
                    MEMSET (au1InMacAddr, 0xff, sizeof (tMacAddr));
                }
                u4Showwildcardflag = VLAN_SHOW_WILD_CARD_ENTRY;
                i4RetStatus = VlanMICliShowWildCardTable (CliHandle,
                                                          u4ContextId,
                                                          au1InMacAddr,
                                                          u4Showwildcardflag);
                break;
            case CLI_VLAN_SHOW_WILDCARD_ALL:

                /*If specfic mac address is not given, set it as
                 * broadcast address.
                 */
                MEMSET (au1InMacAddr, 0, sizeof (tMacAddr));
                u4Showwildcardflag = VLAN_SHOW_WILD_CARD_ENTRY_ALL;
                i4RetStatus = VlanMICliShowWildCardTable (CliHandle,
                                                          u4ContextId,
                                                          au1InMacAddr,
                                                          u4Showwildcardflag);
                break;
#ifdef PBB_WANTED
            case CLI_VLAN_SHOW_DEF_VID:

                i4RetStatus = VlanMICliShowDefaultVid (CliHandle,
                                                       u4ContextId, *(args[0]));
                break;
#endif
            case CLI_VLAN_SHOW_USER_DEFINED_TPID:

                i4RetStatus = VlanShowUserDefinedTPID (CliHandle, u4ContextId);
                break;

            default:
                /* Given command does not match with any of the SHOW commands */
                CliPrintf (CliHandle, "\r%% Unknown command \r\n");

                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
        }
        /* If SwitchName or Interface is given as input for show command 
         * then we have to come out of the Loop */
        if ((pu1ContextName != NULL) || (u4IfIndex != 0))
        {
            break;
        }
        u4TempContextId = u4ContextId;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VLAN) &&
            (u4ErrCode < CLI_VLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       VlanCliErrString[CLI_ERR_OFFSET_VLAN (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanShutdown                                      */
/*                                                                          */
/*     DESCRIPTION      : This function will start/shut Vlan module         */
/*                                                                          */
/*     INPUT            : i4Status - Vlan Module status                     */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanShutdown (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2Dot1qFutureVlanShutdownStatus
        (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanShutdownStatus (i4Status) == SNMP_FAILURE)
        /* if i4Status == VLAN_SNMP_FALSE, then the VLAN module will
         * be started and enabled */
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/****************************************************************************
* Function    :  VlanCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VlanCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    INT4                i4RetVal = 0;
    gu4VlanTrcLvl = 0;

    UNUSED_PARAM (CliHandle);

    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        /* Warning message to enable the all VLAN trace messages */
        i4RetVal = CliDisplayMessageAndUserPromptResponse
            ("This will enable all VLAN traces ", 1, VlanLock, VlanUnLock);
        if (i4RetVal != CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        gu4VlanTrcLvl =
            MGMT_TRC | BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC |
            OS_RESOURCE_TRC | CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        gu4VlanTrcLvl = CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC
            | INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        gu4VlanTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        gu4VlanTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        gu4VlanTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        gu4VlanTrcLvl = INIT_SHUT_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanEnable                                        */
/*                                                                          */
/*     DESCRIPTION      : This function will enable/disable Vlan module     */
/*                                                                          */
/*     INPUT            : i4Status - Vlan Module status                     */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanEnable (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode;
    INT4                i4ShutdownStatus;

    if (i4Status == VLAN_ENABLED)
    {
        i4ShutdownStatus = VLAN_SNMP_FALSE;
    }
    else
    {
        i4ShutdownStatus = VLAN_SNMP_TRUE;
    }

    if ((VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        && (i4Status == VLAN_DISABLED))
        /* If module is shutdown and the user intends to disable the module
         * return success*/
    {
        return (CLI_SUCCESS);
    }
    else
    {

        if ((VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            && (i4Status == VLAN_ENABLED))
            /* If VLAN module is shutdown and the user intends to enable the 
             * module, first start the module and then enable it */

        {
            if (nmhTestv2Dot1qFutureVlanShutdownStatus
                (&u4ErrorCode, i4ShutdownStatus) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1qFutureVlanShutdownStatus (i4ShutdownStatus) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
            /* The VLAN module is started , only enable /disable of
             * status needs to be performed */
        {
            if (nmhTestv2Dot1qFutureVlanStatus (&u4ErrorCode, i4Status) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Vlan cannot be disabled since GVRP/GMRP is enabled\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetDot1qFutureVlanStatus (i4Status) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }
    return (CLI_SUCCESS);

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanConfSwStatus                                  */
/*                                                                          */
/*     DESCRIPTION      : This function will enable/disable software        */
/*                        statistics in the VLAN module                     */
/*                                                                          */
/*     INPUT            : i4Status - software statistics status             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanConfSwStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot1qFutureVlanSwStatsEnabled (&u4ErrorCode, i4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetDot1qFutureVlanSwStatsEnabled (i4Status);

    UNUSED_PARAM (CliHandle);

    return (CLI_SUCCESS);
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanConfApplyFilteringCriteria                    */
/*                                                                          */
/*     DESCRIPTION      : This function will enable/disable application of  */
/*                        filtering utility criteria inthe VLAN module      */
/*                                                                          */
/*     INPUT            : i4Status - Filter utility criteria status         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanConfApplyFilteringCriteria (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria
        (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria (i4Status);

    UNUSED_PARAM (CliHandle);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCreateOpenflowVlan                         */
/*                                                                           */
/*     DESCRIPTION      : This function deletes existing vlan configuration  */
/*                        in vlan module and creates vlan  in openflow       */
/*                        module                                             */
/*                                                                           */
/*     INPUT            : u4VlanId   - Vlan Identifier to create             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCreateOpenflowVlan (tCliHandle CliHandle, UINT4 u4VlanId)
{

    /* Default vlan can not be configured as openflow vlan. */
    if (u4VlanId == VLAN_DEF_VLAN_ID)
    {
        CliPrintf (CliHandle,
                   "\r%% Default Vlan cannot be configured as Openflow Vlan\r\n");
        return CLI_FAILURE;
    }

    VlanDelete (CliHandle, u4VlanId);

    if (VlanOfcCreateOpenflowVlan (u4VlanId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to Create Openflow Vlan\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanDeleteOpenflowVlan                         */
/*                                                                           */
/*     DESCRIPTION      : This function deletes existing vlan configuration  */
/*                        in vlan module and creates vlan  in openflow       */
/*                        module                                             */
/*                                                                           */
/*     INPUT            : u4VlanId   - Vlan Identifier to create             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeleteOpenflowVlan (tCliHandle CliHandle, UINT4 u4VlanId)
{
    INT4                i4Dot1qVlanStaticRowStatus = 0;
    UINT4               u4ErrCode = 0;
    if (VlanOfcDeleteOpenflowVlan (u4VlanId) != OSIX_SUCCESS)
    {

        CliPrintf (CliHandle, "\r%% Unable to delete Openflow Vlan\r\n");
        return CLI_FAILURE;
    }

    /* VLAN might be created when entering Vlan mode */
    /* Check if the VLAN is already present */
    if ((nmhGetDot1qVlanStaticRowStatus (u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        != SNMP_SUCCESS)
    {
        /* VLAN NOT present - CREATE */

        if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, u4VlanId,
                                               VLAN_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* CREATE VLAN */
        if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle,
                   "%% This configuration is not allowed for Transparent Bridges\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCreate                                         */
/*                                                                           */
/*     DESCRIPTION      : This function will enter into vlan database config */
/*                        mode. New VLAN will be created when identifier is  */
/*                        not present in the database.                       */
/*                                                                           */
/*     INPUT            : u4VlanId   - Vlan Identifier to create             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCreate (tCliHandle CliHandle, UINT4 u4VlanId)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    INT4                i4Dot1qVlanStaticRowStatus;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    /* Check if the VLAN is ICCL VLAN */
    if (VlanIcchIsIcclVlan ((UINT2) u4VlanId) == OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_VLAN_BLOCK_ICCL_VLAN);
        return CLI_FAILURE;
    }

    /* Check if the VLAN is already present */
    if ((nmhGetDot1qVlanStaticRowStatus (u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        != SNMP_SUCCESS)
    {
        /* VLAN NOT present - CREATE */

        if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, u4VlanId,
                                               VLAN_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* CREATE VLAN */
        if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle,
                   "%% This configuration is not allowed for Transparent Bridges\r\n");
        return CLI_FAILURE;
    }

    /* ENTER VLAN Mode */
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_VLAN_MODE, u4VlanId);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into Vlan configuration mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanDelete                                         */
/*                                                                           */
/*     DESCRIPTION      : This function deletes existing vlan configuration  */
/*                                                                           */
/*     INPUT            : u4VlanId   - Vlan Identifier to delete             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDelete (tCliHandle CliHandle, UINT4 u4VlanId)
{
    UINT4               u4ErrCode;

    if (VlanCfaIsL3SubIfVlanId (u4VlanId) == VLAN_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% WARNING!! VLAN associated with L3Subinterface \r\n");
    }

    if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, u4VlanId, VLAN_DESTROY)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanGlobalMacLearningStatus                        */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables Global Mac learning */
/*                        status                                             */
/*                                                                           */
/*     INPUT            : i4Status - Status either enable or disable         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGlobalMacLearningStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus (&u4ErrCode, i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanGlobalMacLearningStatus (i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanMacbasedOnAllPorts                             */
/*                                                                           */
/*     DESCRIPTION      : This function will enables MAC based VLAN learning */
/*                        on all ports of the VLAN                           */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable MAC based learning      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMacbasedOnAllPorts (tCliHandle CliHandle, UINT4 u4Action)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts (&u4ErrCode,
                                                    (INT4) u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetDot1qFutureVlanMacBasedOnAllPorts ((INT4) u4Action);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSubnetbasedOnAllPorts                          */
/*                                                                           */
/*     DESCRIPTION      : This function will enables Subnet based VLAN       */
/*                        learning on all ports of the VLAN                  */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable subnet based learning   */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSubnetbasedOnAllPorts (tCliHandle CliHandle, UINT4 u4Action)
{
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts (&u4ErrCode,
                                                       (INT4) u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetDot1qFutureVlanSubnetBasedOnAllPorts ((INT4) u4Action);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPortProtocolBasedOnAllPorts                    */
/*                                                                           */
/*     DESCRIPTION      : This function will enables Protocol based VLAN     */
/*                        learning on all ports of the VLAN                  */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable Protocl Based Learning  */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortProtocolBasedOnAllPorts (tCliHandle CliHandle, UINT4 u4Action)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts (&u4ErrCode,
                                                          (INT4) u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts (u4Action);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanSetPortUnicastMacLearningStatus                */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Unicast Mac Learning status*/
/*                        for Port                                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Status - Enable/Disable status                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanSetPortUnicastMacLearningStatus (tCliHandle CliHandle, INT4 i4PortId,
                                     UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanPortUnicastMacLearning
        (&u4ErrCode, i4PortId, u4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1qFutureVlanPortUnicastMacLearning (i4PortId, u4Status) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanSetPortUnicastSecType                         */
/*                                                                          */
/*     DESCRIPTION      : Set the port security violation level                      */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        i4PortMacSecType - Mac security type              */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanSetPortUnicastSecType (tCliHandle CliHandle, INT4 i4PortId,
                           INT4 i4PortMacSecType)
{
    UINT4               u4ErrCode;
    if (nmhTestv2Dot1qFutureVlanPortUnicastMacSecType (&u4ErrCode,
                                                       i4PortId,
                                                       i4PortMacSecType) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1qFutureVlanPortUnicastMacSecType (i4PortId, i4PortMacSecType)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/************************************************************************/
/*     FUNCTION NAME    : VlanSetPortUnicastMacAddr                   */
/*                                                                      */
/*     DESCRIPTION      : Set the port security violation level         */
/*                                                                      */
/*     INPUT            : i4PortId  - Port Index                        */
/*                        i4PortMacSecType - Mac security type          */
/*                                                                      */
/*     OUTPUT           : CliHandle - Contains error messages           */
/*                                                                      */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                       */
/*                                                                      */
/************************************************************************/
INT4
VlanSetPortUnicastMacAddr (tCliHandle CliHandle, INT4 i4PortId,
                           UINT1 *pu1MacAddr, UINT4 u4MacAddrType,
                           UINT4 u4VlanId)
{
    UINT4               u4RcvLocalPortId = 0;
    UINT4               u4EntryStatus = VLAN_PERMANENT;
    UINT4               i4RetStatus = CLI_SUCCESS;
    UINT1              *pu1ConnectionId = NULL;
    UINT1              *pau1LocalMemberPorts = NULL;
    tPortList          *pMemberPorts = NULL;
    tMacAddr            au1InMacAddr;

    INT1                ai1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1NameStr = ai1NameStr;
    INT1               *pi1IfName = ai1IfName;

    UNUSED_PARAM (u4MacAddrType);

    MEMSET (ai1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    pau1LocalMemberPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

    if (pau1LocalMemberPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME,
                  "cli_process_vlan_cmd: Error in allocating memory "
                  "for pau1LocalMemberPorts\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

    pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pMemberPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
        return CLI_FAILURE;
    }

    MEMSET (pMemberPorts, 0, sizeof (tPortList));
    if (VlanCfaCliGetIfName ((UINT4) i4PortId, ai1NameStr) == CLI_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
        return CLI_FAILURE;
    }
    pi1NameStr = ai1NameStr + 2;

    VlanCfaCliConfGetIfName ((UINT4) i4PortId, ai1IfName);
    pi1IfName = (signed char *) STRTOK ((char *) ai1IfName, " ,");
    i4RetStatus = VlanCfaCliGetIfList (pi1IfName, pi1NameStr,
                                       *pMemberPorts, sizeof (tPortList));
    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
        return CLI_FAILURE;
    }

    if (VlanConvertToLocalPortList (*pMemberPorts,
                                    pau1LocalMemberPorts) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Some ports in this portlist are "
                   "not mapped to this context or part of "
                   "port-channel.\r\n");
        i4RetStatus = CLI_FAILURE;
        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
    }

    StrToMac ((UINT1 *) pu1MacAddr, au1InMacAddr);
    i4RetStatus = VlanSetUnicastAdd (CliHandle,
                                     pu1MacAddr, u4RcvLocalPortId,
                                     pau1LocalMemberPorts, u4EntryStatus,
                                     u4VlanId, pu1ConnectionId);

    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
    UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanSetUnicastSecType                         */
/*                                                                          */
/*     DESCRIPTION      : Set the port security violation level                */
/*                                                                          */
/*     INPUT            : i4PortMacSecType - Mac security type              */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanSetUnicastSecType (tCliHandle CliHandle, INT4 i4PortMacSecType)
{
    UINT4               u4ErrCode;
    UINT4               i4PortId = 1;
    UINT4               u4MaxFrontPanelPorts = 0;

    u4MaxFrontPanelPorts = IssGetFrontPanelPortCountFromNvRam ();

    /*  set the status for all the ports in the switch  */
    for (i4PortId = 1; i4PortId <= u4MaxFrontPanelPorts; i4PortId++)
    {
        if (nmhTestv2Dot1qFutureVlanPortUnicastMacSecType (&u4ErrCode,
                                                           i4PortId,
                                                           i4PortMacSecType) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetDot1qFutureVlanPortUnicastMacSecType (i4PortId,
                                                        i4PortMacSecType) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanUpdateProtocolGroup                            */
/*                                                                           */
/*     DESCRIPTION      : This function will adds/removes entries from       */
/*                        protocol group table                               */
/*                                                                           */
/*     INPUT            : u1FrameType  - Frame encapsulation type            */
/*                        u4GroupId    - Protocol group identifier           */
/*                        pu1ProtocolValue - pointer to other protocol value */
/*                        u1Length     - Length of the other protocol value  */
/*                        u1Action     - Action to be performed (Add/Remove) */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateProtocolGroup (tCliHandle CliHandle, UINT1 u1FrameType,
                         UINT4 u4GroupId, UINT1 *pu1ProtocolValue,
                         UINT1 u1Length, UINT1 u1Action)
{
    INT4                i4GroupIdentifier;
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE ProtocolGroup;
    UINT1               au1ProtoValue[VLAN_MAX_PROTO_SIZE];

    MEMSET (au1ProtoValue, 0, sizeof (au1ProtoValue));
    ProtocolGroup.i4_Length = u1Length;

    ProtocolGroup.pu1_OctetList = au1ProtoValue;

    MEMCPY (ProtocolGroup.pu1_OctetList,
            pu1ProtocolValue, ProtocolGroup.i4_Length);

    switch (u1Action)
    {
        case VLAN_SET_CMD:
            if (nmhGetDot1vProtocolGroupId (u1FrameType,
                                            &ProtocolGroup,
                                            &i4GroupIdentifier) == SNMP_FAILURE)
            {
                if (nmhTestv2Dot1vProtocolGroupRowStatus (&u4ErrCode,
                                                          u1FrameType,
                                                          &ProtocolGroup,
                                                          VLAN_CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot1vProtocolGroupId (&u4ErrCode, u1FrameType,
                                                   &ProtocolGroup,
                                                   u4GroupId) == SNMP_FAILURE)
                {
                    nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_DESTROY);
                    return CLI_FAILURE;
                }

                if (nmhSetDot1vProtocolGroupId (u1FrameType, &ProtocolGroup,
                                                u4GroupId) == SNMP_FAILURE)
                {
                    nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_DESTROY);
                    return CLI_FAILURE;
                }

                if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_ACTIVE) ==
                    SNMP_FAILURE)
                {
                    if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                           &ProtocolGroup,
                                                           VLAN_DESTROY) ==
                        SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    return CLI_FAILURE;
                }
            }
            else
            {
                if ((UINT4) i4GroupIdentifier != u4GroupId)
                {
                    if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                           &ProtocolGroup,
                                                           VLAN_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% FrameType is already in Active state,"
                                   "The Protocol Group Entry updation is possible only "
                                   "when  the FrameType is not in active state \r\n");
                        return CLI_FAILURE;
                    }

                    if (nmhTestv2Dot1vProtocolGroupId (&u4ErrCode, u1FrameType,
                                                       &ProtocolGroup,
                                                       u4GroupId)
                        == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }

                    if (nmhSetDot1vProtocolGroupId (u1FrameType, &ProtocolGroup,
                                                    u4GroupId) == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }

                    nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_ACTIVE);
                }
            }

            break;

        case VLAN_NO_CMD:

            if (nmhTestv2Dot1vProtocolGroupRowStatus (&u4ErrCode, u1FrameType,
                                                      &ProtocolGroup,
                                                      VLAN_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType, &ProtocolGroup,
                                                   VLAN_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        default:
            CliPrintf (CliHandle, "\r%% Not a valid command.\r\n");
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetTrafficClasses                              */
/*                                                                           */
/*     DESCRIPTION      : This function will enable/disable VLAN traffic     */
/*                        classes on the device.                             */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable Traffic classes         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetTrafficClasses (tCliHandle CliHandle, UINT4 u4Action)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Dot1dTrafficClassesEnabled (&u4ErrCode, u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetDot1dTrafficClassesEnabled (u4Action);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanFormPortList                                   */
/*                                                                           */
/*     DESCRIPTION      : This function will form the  egress ,untagged      */
/*                        and forbidden portlist                             */
/*                        ports on a VLAN.                                   */
/*                                                                           */
/*     INPUT            : pu1MemberPorts    - Contains member ports          */
/*                        pu1UntaggedPorts  - Contains untagged ports        */
/*                        pu1ForbiddenPorts - Contains forbidden ports       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFormPortList (tCliHandle CliHandle, UINT1 *pu1MemberPorts,
                  UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts)
{
    UINT1              *pDelEgressPortList = NULL;
    UINT1              *pDelForbiddenPortList = NULL;
    UINT1              *pDelUntaggedPortList = NULL;
    UINT1              *pau1OldEgressPortList = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    UINT1              *pau1OldForbiddenPortList = NULL;
    UINT1              *pau1EgressPortList = NULL;
    UINT1              *pau1UntaggedPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    UINT4               u4VlanId = 0;
    UINT1               u1Result = VLAN_FALSE;
    INT4                i4Val = 0;
    tSNMP_OCTET_STRING_TYPE OldEgressPorts;
    tSNMP_OCTET_STRING_TYPE OldForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
    i4Val = CLI_GET_VLANID ();

    if (i4Val == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    u4VlanId = (UINT4) i4Val;

    pau1EgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1EgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: "
                  "Error in allocating memory for pau1EgressPortList\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1EgressPortList, 0, VLAN_PORT_LIST_SIZE);

    OldEgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldEgressPorts.pu1_OctetList = pau1EgressPortList;

    pau1ForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1ForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: "
                  "Error in allocating memory for pau1ForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    OldForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldForbiddenPorts.pu1_OctetList = pau1ForbiddenPortList;

    pau1UntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1UntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: "
                  "Error in allocating memory for pau1UntaggedPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1UntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    OldUntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldUntaggedPorts.pu1_OctetList = pau1UntaggedPortList;

    if ((nmhGetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts)) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    if ((nmhGetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts)) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    if ((nmhGetDot1qVlanForbiddenEgressPorts (u4VlanId, &OldForbiddenPorts)) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    pDelEgressPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDelEgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pDelEgressPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pDelEgressPortList, 0, sizeof (tLocalPortList));

    pau1OldEgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldEgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pau1OldEgressPortList\r\n");
        UtilPlstReleaseLocalPortList (pDelEgressPortList);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1OldEgressPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1OldEgressPortList, OldEgressPorts.pu1_OctetList,
            VLAN_PORT_LIST_SIZE);

    MEMCPY (pDelEgressPortList, pu1MemberPorts, VLAN_PORT_LIST_SIZE);
    if (VlanIsPortListPortTypeValid (pDelEgressPortList, VLAN_PORT_LIST_SIZE,
                                     VLAN_TRUNK_PORT) == VLAN_FALSE)
    {
        UtilPlstReleaseLocalPortList (pDelEgressPortList);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        CLI_SET_ERR (CLI_VLAN_TRUNK_UNTAGGED_PORT_ERR);
        return CLI_FAILURE;
    }
    VLAN_RESET_PORT_LIST (pau1OldEgressPortList, pDelEgressPortList);
    VLAN_ARE_PORTS_INCLUSIVE (pau1OldEgressPortList, pDelEgressPortList,
                              pau1EgressPortList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Some Egress ports are not a member of given vlan \r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pDelEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMCPY (pu1MemberPorts, pau1OldEgressPortList, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pau1EgressPortList);
    UtilPlstReleaseLocalPortList (pDelEgressPortList);
    UtilPlstReleaseLocalPortList (pau1OldEgressPortList);

    pDelUntaggedPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDelUntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for DelUntaggedPortList \r\n");
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pDelUntaggedPortList, 0, sizeof (tLocalPortList));

    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldUntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pau1OldUntaggedPortList\r\n");
        UtilPlstReleaseLocalPortList (pDelUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1OldUntaggedPortList, OldUntaggedPorts.pu1_OctetList,
            VLAN_PORT_LIST_SIZE);

    MEMCPY (pDelUntaggedPortList, pu1UntaggedPorts, VLAN_PORT_LIST_SIZE);
    VLAN_RESET_PORT_LIST (pau1OldUntaggedPortList, pDelUntaggedPortList);
    VLAN_ARE_PORTS_INCLUSIVE (pau1OldUntaggedPortList, pDelUntaggedPortList,
                              pau1UntaggedPortList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Some Untagged ports are not a member of given vlan \r\n");
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pDelUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return CLI_FAILURE;
    }

    MEMCPY (pu1UntaggedPorts, pau1OldUntaggedPortList, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
    UtilPlstReleaseLocalPortList (pDelUntaggedPortList);
    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);

    pDelForbiddenPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDelForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pDelForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pDelForbiddenPortList, 0, sizeof (tLocalPortList));

    pau1OldForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pau1OldForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pDelForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1OldForbiddenPortList, OldForbiddenPorts.pu1_OctetList,
            VLAN_PORT_LIST_SIZE);

    MEMCPY (pDelForbiddenPortList, pu1ForbiddenPorts, VLAN_PORT_LIST_SIZE);
    VLAN_RESET_PORT_LIST (pau1OldForbiddenPortList, pDelForbiddenPortList);
    VLAN_ARE_PORTS_INCLUSIVE (pau1OldForbiddenPortList, pDelForbiddenPortList,
                              pau1ForbiddenPortList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Some Forbidden ports are not a member of given vlan \r\n");
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        UtilPlstReleaseLocalPortList (pDelForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }

    MEMCPY (pu1ForbiddenPorts, pau1OldForbiddenPortList, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
    UtilPlstReleaseLocalPortList (pDelForbiddenPortList);
    UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetName                                        */
/*                                                                           */
/*     DESCRIPTION      : This function will configures the name for         */
/*                        the VLAN.                                          */
/*                                                                           */
/*     INPUT            : pau1Name          - Vlan name identifier           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetName (tCliHandle CliHandle, UINT1 *pau1Name)
{
    UINT4               u4VlanId = 0;
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
    INT4                i4Val = 0;
    tSNMP_OCTET_STRING_TYPE Name;

    i4Val = CLI_GET_VLANID ();

    if (i4Val == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    u4VlanId = (UINT4) i4Val;

    if (pau1Name != NULL)
    {
        Name.i4_Length = STRLEN (pau1Name);
        Name.pu1_OctetList = pau1Name;

        if (Name.i4_Length > VLAN_STATIC_MAX_NAME_LEN)
        {
            Name.i4_Length = VLAN_STATIC_MAX_NAME_LEN;
        }

        if (nmhTestv2Dot1qVlanStaticName (&u4ErrCode, u4VlanId, &Name)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1qVlanStaticName (u4VlanId, &Name) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        MEMSET (au1VlanName, 0, sizeof (au1VlanName));
        Name.i4_Length = 0;
        Name.pu1_OctetList = au1VlanName;

        if (nmhSetDot1qVlanStaticName (u4VlanId, &Name) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPorts                                       */
/*                                                                           */
/*     DESCRIPTION      : This function will configure egress and forbidden  */
/*                        ports on a VLAN.                                   */
/*                                                                           */
/*     INPUT            : pu1MemberPorts    - Contains member ports          */
/*                        pu1UntaggedPorts  - Contains untagged ports        */
/*                        pu1ForbiddenPorts - Contains forbidden ports       */
/*                        pau1Name          - Vlan name identifier           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPorts (tCliHandle CliHandle, UINT1 *pu1MemberPorts,
              UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts, UINT4 u4Flag)
{
    UINT1              *pau1OldEgressPortList = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    UINT1              *pau1OldForbiddenPortList = NULL;
    UINT4               u4VlanId;
    INT4                i4Dot1qVlanStaticRowStatus;
    INT4                i4Val = 0;
    tSNMP_OCTET_STRING_TYPE OldEgressPorts;
    tSNMP_OCTET_STRING_TYPE OldForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE ForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE UntaggedPorts;

    i4Val = CLI_GET_VLANID ();

    if (i4Val == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    u4VlanId = (UINT4) i4Val;

    /* Check whether the Vlan entry is already present */
    if ((nmhGetDot1qVlanStaticRowStatus (u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        == SNMP_FAILURE)
    {
        /* A row in the Vlan table must have been created */
        CliPrintf (CliHandle, "\r%% Vlan %d is not created \r\n", u4VlanId);
        return CLI_FAILURE;
    }
    pau1OldEgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldEgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetPorts: Error in allocating memory "
                  "for pau1OldEgressPortList\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldEgressPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldUntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetPorts: Error in allocating memory "
                  "for pau1OldUntaggedPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1OldForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetPorts: Error in allocating memory "
                  "for pau1OldForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    EgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    EgressPorts.pu1_OctetList = pu1MemberPorts;

    ForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    ForbiddenPorts.pu1_OctetList = pu1ForbiddenPorts;

    UntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    UntaggedPorts.pu1_OctetList = pu1UntaggedPorts;

    /* OldxxxPorts are used to restore the objects in case of any failure. */
    OldEgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldEgressPorts.pu1_OctetList = pau1OldEgressPortList;

    OldForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldForbiddenPorts.pu1_OctetList = pau1OldForbiddenPortList;

    OldUntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldUntaggedPorts.pu1_OctetList = pau1OldUntaggedPortList;

/*
 * Obtain the previous value, in order to restore the object
 * in case of any failure.
 */
    nmhGetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
    nmhGetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);
    nmhGetDot1qVlanForbiddenEgressPorts (u4VlanId, &OldForbiddenPorts);

    if (u4Flag == VLAN_UPDATE)
    {
        /* Add new member ports to existing member portlist
         * EgressPorts = OldEgressPorts + EgressPorts
         * UntaggedPorts = OldUntaggedports + UntaggedPorts
         * ForbiddenPorts = OldForbiddenPorts + ForbiddenPorts
         * */
        VLAN_ADD_PORT_LIST (EgressPorts.pu1_OctetList,
                            OldEgressPorts.pu1_OctetList);
        VLAN_ADD_PORT_LIST (UntaggedPorts.pu1_OctetList,
                            OldUntaggedPorts.pu1_OctetList);
        VLAN_ADD_PORT_LIST (ForbiddenPorts.pu1_OctetList,
                            OldForbiddenPorts.pu1_OctetList);
    }
    /*
     * If Untagged ports and Forbidden ports are not provided by the user, 
     * then they will take the default value of empty portlist. So need
     * not check if Untagged ports and Forbiddne ports are provided.
     *
     * VlanTestModificationInfo () checks if the Member ports, Untagged ports
     * and Forbidden ports satisfy all the dependencies. Once this is done
     * we need not call the specific Test routines.
     */
    if ((VlanTestModificationInfo (CliHandle, u4VlanId, &EgressPorts,
                                   &UntaggedPorts, &ForbiddenPorts))
        == CLI_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return CLI_FAILURE;
    }

    if (nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &EgressPorts)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return CLI_FAILURE;
    }

    if (nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &UntaggedPorts)
        == SNMP_FAILURE)
    {
        nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);

        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return CLI_FAILURE;
    }

    if (nmhSetDot1qVlanForbiddenEgressPorts (u4VlanId, &ForbiddenPorts)
        == SNMP_FAILURE)
    {
        nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
        nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);

        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        return CLI_FAILURE;
    }

    if (i4Dot1qVlanStaticRowStatus != VLAN_ACTIVE)
    {
        if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_ACTIVE)
            == SNMP_FAILURE)
        {
            nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
            nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);
            nmhSetDot1qVlanForbiddenEgressPorts (u4VlanId, &OldForbiddenPorts);

            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
            return CLI_FAILURE;
        }
    }
    VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC,
                   VLAN_NAME, "Portlist updation for VLAN %d successful  \n",
                   u4VlanId);

    UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
    UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetDot1dUnicastAdd                             */
/*                                                                           */
/*     DESCRIPTION      : This function adds a static Unicast entry in Dot1d */
/*                        Bridge mibs                                        */
/*                                                                           */
/*     INPUT            : pu1MacAddress     - MAC Address                    */
/*                        u4ReceivePort     - Reception port                 */
/*                        pu1Ports          - Member ports                   */
/*                        u4Status          - Status of the entry            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetDot1dUnicastAdd (tCliHandle CliHandle, UINT1 *pu1MacAddress,
                        UINT4 u4ReceivePort, UINT1 *pu1Ports, UINT4 u4Status)
{
    UINT1              *pau1StaticPorts = NULL;
    UINT1              *pau1OldStaticPorts = NULL;
    INT4                i4GetOperationStatus = VLAN_INIT_VAL;
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE *pPortList = &PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;

    pau1StaticPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetDot1dUnicastAdd: "
                  "Error in allocating memory for pau1StaticPorts\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1StaticPorts, 0, VLAN_PORT_LIST_SIZE);

    MEMSET (pau1StaticPorts, VLAN_INIT_VAL, VLAN_PORT_LIST_SIZE);
    PortList.i4_Length = VLAN_PORT_LIST_SIZE;
    PortList.pu1_OctetList = pau1StaticPorts;
    MEMCPY (PortList.pu1_OctetList, pu1Ports, PortList.i4_Length);

    if (nmhTestv2Dot1dStaticAllowedToGoTo (&u4ErrCode,
                                           pu1MacAddress,
                                           u4ReceivePort,
                                           pPortList) == SNMP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        return CLI_FAILURE;
    }

    if (nmhTestv2Dot1dStaticStatus (&u4ErrCode,
                                    pu1MacAddress,
                                    (INT4) u4ReceivePort,
                                    u4Status) == SNMP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        return CLI_FAILURE;
    }
    pau1OldStaticPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldStaticPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetDot1dUnicastAdd: "
                  "Error in allocating memory for pau1OldStaticPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        return CLI_FAILURE;
    }
    MEMSET (pau1OldStaticPorts, 0, VLAN_PORT_LIST_SIZE);

    OldPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    OldPortList.pu1_OctetList = pau1OldStaticPorts;

    i4GetOperationStatus = nmhGetDot1dStaticAllowedToGoTo (pu1MacAddress,
                                                           u4ReceivePort,
                                                           &OldPortList);
    if (nmhSetDot1dStaticAllowedToGoTo (pu1MacAddress,
                                        u4ReceivePort,
                                        &PortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1StaticPorts);
    if (nmhSetDot1dStaticStatus (pu1MacAddress,
                                 u4ReceivePort, u4Status) == SNMP_FAILURE)
    {
        if (i4GetOperationStatus == SNMP_SUCCESS)
        {
            /* Entry already exists, so restore the old value */
            nmhSetDot1dStaticAllowedToGoTo (pu1MacAddress,
                                            u4ReceivePort, &OldPortList);
        }
        else
        {
            /* Entry was not present earlier, so delete the entry */
            nmhSetDot1dStaticStatus (pu1MacAddress,
                                     u4ReceivePort, VLAN_MGMT_INVALID);
        }

        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetUnicastAdd                                  */
/*                                                                           */
/*     DESCRIPTION      : This function adds a static Unicast entry          */
/*                                                                           */
/*     INPUT            : pu1MacAddress     - MAC Address                    */
/*                        u4ReceivePort     - Reception port                 */
/*                        pu1Ports          - Member ports                   */
/*                        u4Status          - Status of the entry
 *                        pu1ConnectionId   - Back Bone Mac Address          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetUnicastAdd (tCliHandle CliHandle, UINT1 *pu1MacAddress,
                   UINT4 u4ReceivePort, UINT1 *pu1Ports, UINT4 u4Status,
                   UINT4 u4VlanId, UINT1 *pu1ConnectionId)
{
    UINT1              *pau1StaticPorts = NULL;
    UINT1              *pau1OldStaticPorts = NULL;
    INT4                i4GetOperationStatus;
    UINT4               u4ErrCode;
    UINT4               u4FdbId;
    tMacAddr            au1OldConnectionId;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE *pPortList = &PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;

    if (VlanGetFdbIdFromVlanId ((tVlanId) u4VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Vlan is not Active\r\n");

        return CLI_FAILURE;
    }
    pau1StaticPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetUnicastAdd: "
                  "Error in allocating memory for pau1StaticPorts\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1StaticPorts, 0, VLAN_PORT_LIST_SIZE);

    MEMSET (au1OldConnectionId, 0, sizeof (tMacAddr));
    PortList.i4_Length = VLAN_PORT_LIST_SIZE;
    PortList.pu1_OctetList = pau1StaticPorts;
    MEMCPY (PortList.pu1_OctetList, pu1Ports, PortList.i4_Length);

    if (nmhTestv2Dot1qStaticUnicastAllowedToGoTo (&u4ErrCode,
                                                  u4FdbId,
                                                  pu1MacAddress,
                                                  u4ReceivePort,
                                                  pPortList) == SNMP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        return CLI_FAILURE;
    }

    if (nmhTestv2Dot1qStaticUnicastStatus (&u4ErrCode,
                                           u4FdbId,
                                           pu1MacAddress,
                                           u4ReceivePort,
                                           u4Status) == SNMP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        return CLI_FAILURE;
    }
    pau1OldStaticPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldStaticPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetUnicastAdd: "
                  "Error in allocating memory for pau1OldStaticPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        return CLI_FAILURE;
    }
    MEMSET (pau1OldStaticPorts, 0, VLAN_PORT_LIST_SIZE);

    OldPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    OldPortList.pu1_OctetList = pau1OldStaticPorts;

    i4GetOperationStatus = nmhGetDot1qStaticUnicastAllowedToGoTo (u4FdbId,
                                                                  pu1MacAddress,
                                                                  u4ReceivePort,
                                                                  &OldPortList);
    nmhGetDot1qFutureStaticConnectionIdentifier (u4FdbId,
                                                 pu1MacAddress,
                                                 u4ReceivePort,
                                                 &au1OldConnectionId);

    if (nmhSetDot1qStaticUnicastAllowedToGoTo (u4FdbId,
                                               pu1MacAddress,
                                               (INT4) u4ReceivePort,
                                               pPortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1StaticPorts);
        UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1StaticPorts);
    if (pu1ConnectionId != NULL)
    {
        if (nmhTestv2Dot1qFutureStaticConnectionIdentifier (&u4ErrCode,
                                                            u4FdbId,
                                                            pu1MacAddress,
                                                            u4ReceivePort,
                                                            pu1ConnectionId) ==
            SNMP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
            return CLI_FAILURE;
        }
        if (nmhSetDot1qFutureStaticConnectionIdentifier (u4FdbId,
                                                         pu1MacAddress,
                                                         u4ReceivePort,
                                                         pu1ConnectionId) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
            return CLI_FAILURE;
        }
    }

    if (nmhSetDot1qStaticUnicastStatus (u4FdbId, pu1MacAddress,
                                        u4ReceivePort,
                                        u4Status) == SNMP_FAILURE)
    {
        if (i4GetOperationStatus == SNMP_SUCCESS)
        {
            /* Entry already exists, so restore the old value */
            nmhSetDot1qStaticUnicastAllowedToGoTo (u4FdbId,
                                                   pu1MacAddress,
                                                   (INT4) u4ReceivePort,
                                                   &OldPortList);

            nmhSetDot1qFutureStaticConnectionIdentifier (u4FdbId,
                                                         pu1MacAddress,
                                                         u4ReceivePort,
                                                         au1OldConnectionId);
        }
        else
        {
            /* Entry was not present earlier, so delete the entry */
            nmhSetDot1qStaticUnicastStatus (u4FdbId, pu1MacAddress,
                                            u4ReceivePort, VLAN_MGMT_INVALID);
        }

        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
        return CLI_FAILURE;
    }

    UtilPlstReleaseLocalPortList (pau1OldStaticPorts);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetUnicastDel                                  */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the static Unicast entry     */
/*                                                                           */
/*     INPUT            : pu1MacAddr        - MAC Address                    */
/*                        u4ReceivePort     - Reception port                 */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetUnicastDel (tCliHandle CliHandle, UINT1 *pu1MacAddr, UINT4 u4ReceivePort,
                   UINT4 u4VlanId)
{
    UINT4               u4ErrCode;
    UINT4               u4FdbId;
    INT4                i4RetVal;

    if (VlanGetFdbIdFromVlanId ((tVlanId) u4VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Vlan is not Active\r\n");

        return CLI_FAILURE;
    }

    if (nmhGetDot1qStaticUnicastStatus
        (u4FdbId, pu1MacAddr, u4ReceivePort, &i4RetVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Such Static Unicast entry \r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1qStaticUnicastStatus (&u4ErrCode, u4FdbId, pu1MacAddr,
                                           u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qStaticUnicastStatus (u4FdbId, pu1MacAddr,
                                        u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanDot1dSetUnicastDel                             */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the static Unicast entry     */
/*                        in Transparent Bridging Mode.                      */
/*                                                                           */
/*     INPUT            : pu1MacAddr        - MAC Address                    */
/*                        u4ReceivePort     - Reception port                 */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDot1dSetUnicastDel (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                        UINT4 u4ReceivePort, UINT4 u4VlanId)
{
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    INT4                i4RetVal = VLAN_INIT_VAL;

    UNUSED_PARAM (u4VlanId);

    if (nmhTestv2Dot1dStaticStatus (&u4ErrCode, pu1MacAddr,
                                    (INT4) u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetDot1dStaticStatus
        (pu1MacAddr, u4ReceivePort, &i4RetVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Such Static Unicast entry \r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetDot1dStaticStatus (pu1MacAddr, u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : VlanMulticastEntryAdd ()                               */
/*                                                                           */
/* DESCRIPTION      : This function adds a Static Multicast entry            */
/*                                                                           */
/* INPUT            : CliHandle            - Cli Context Handle              */
/*                    pu1MacAddr           - Multicast Mac address           */
/*                    u4ReceivePort        - Receive Port                    */
/*                    pu1EgressPorts       - Multicast Member Ports          */
/*                    pu1ForbiddenPorts    - Multicast Forbidden Ports       */
/*                    u4Status             - Multicast entry Status          */
/*                    u4EgressFlag         - Flag to indicate if Egress      */
/*                                           Ports are present               */
/*                    u4ForbiddenFlag      - Flag to indicate if Forbidden   */
/*                                           Ports are present               */
/*                                                                           */
/* OUTPUT           : CliHandle - Contains error messages                    */
/*                                                                           */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMulticastEntryAdd (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                       UINT4 u4ReceivePort, UINT1 *pu1EgressPorts,
                       UINT1 *pu1ForbiddenPorts, UINT4 u4Status,
                       UINT4 u4EgressFlag, UINT4 u4ForbiddenFlag,
                       UINT4 u4VlanId)
{
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbbidenPortList = NULL;
    UINT1              *pau1OldStaticPortList = NULL;
    UINT1              *pau1OldForbbidenPortList = NULL;
    UINT4               u4ErrCode;
    INT4                i4OldStatus;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1               u1Modify = VLAN_TRUE;
    UINT1               u1Result = VLAN_FALSE;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE *pStaticPortList = &StaticPortList;
    tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
    tSNMP_OCTET_STRING_TYPE *pForbiddenPortList = &ForbiddenPortList;
    tSNMP_OCTET_STRING_TYPE OldStaticPortList;
    tSNMP_OCTET_STRING_TYPE OldForbiddenPortList;

    /* 
     * If the egress ports are not given in the input ,we set all 
     * members of the VLAN in egress ports 
     */
    pau1ForbbidenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1ForbbidenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanMulticastEntryAdd: "
                  "Error in allocating memory for pau1ForbbidenPortList\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1ForbbidenPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1StaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanMulticastEntryAdd: "
                  "Error in allocating memory for pau1StaticPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1StaticPortList, pu1EgressPorts, VLAN_PORT_LIST_SIZE);
    StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    StaticPortList.pu1_OctetList = pau1StaticPortList;

    if (u4EgressFlag == VLAN_FALSE)
    {
        pVlanEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);

        if (pVlanEntry != NULL)
        {
            VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pau1StaticPortList);

            if (u4ForbiddenFlag == VLAN_TRUE)
            {
                /*
                 * Only Forbidden Ports are provided. So remove the
                 * Forbidden Ports from the Egress Ports.
                 */

                VLAN_RESET_PORT_LIST (pau1StaticPortList,
                                      pau1ForbbidenPortList);
            }
        }
    }

    /* Check if the multicast entry is already available in the data base */
    if (nmhGetDot1qStaticMulticastStatus (u4VlanId, pu1MacAddr, u4ReceivePort,
                                          &i4OldStatus) == SNMP_FAILURE)
    {
        u1Modify = VLAN_FALSE;
    }

    /* 
     * If we are going to modify the existing entry we call a separate 
     * test function to validate the inputs, since the normal nmh Validation
     * routines check for mutual exclusiveness with the existing egress or 
     * forbidden ports 
     */

    pau1OldStaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldStaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanMulticastEntryAdd: "
                  "Error in allocating memory for pau1OldStaticPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldStaticPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1OldForbbidenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldForbbidenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanMulticastEntryAdd: "
                  "Error in allocating memory for pau1OldForbbidenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
        UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldForbbidenPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1ForbbidenPortList, pu1ForbiddenPorts, VLAN_PORT_LIST_SIZE);
    ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    ForbiddenPortList.pu1_OctetList = pau1ForbbidenPortList;

    if (u1Modify == VLAN_TRUE)
    {
        if ((VlanTestStaticMcastModificationInfo (CliHandle, u4VlanId,
                                                  pu1MacAddr, u4ReceivePort,
                                                  &StaticPortList,
                                                  &ForbiddenPortList))
            == CLI_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return CLI_FAILURE;
        }

        OldStaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
        OldStaticPortList.pu1_OctetList = pau1OldStaticPortList;

        nmhGetDot1qStaticMulticastStaticEgressPorts (u4VlanId, pu1MacAddr,
                                                     u4ReceivePort,
                                                     &OldStaticPortList);

        OldForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
        OldForbiddenPortList.pu1_OctetList = pau1OldForbbidenPortList;

        nmhGetDot1qStaticMulticastForbiddenEgressPorts (u4VlanId, pu1MacAddr,
                                                        u4ReceivePort,
                                                        &OldForbiddenPortList);
    }
    else
    {
        /* 
         * We are creating a new static multicast entry ,checking if 
         * egress and forbidden ports are exclusive 
         */
        VLAN_ARE_PORTS_EXCLUSIVE (StaticPortList.pu1_OctetList,
                                  ForbiddenPortList.pu1_OctetList, u1Result);

        if (u1Result == VLAN_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Egress Ports and Forbidden Ports "
                       "are overlapping\r\n");
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return CLI_FAILURE;
        }

        /* 
         * Here we are validating the egress ports and forbidden ports 
         * when we are creating a new entry 
         */

        if (nmhTestv2Dot1qStaticMulticastStaticEgressPorts (&u4ErrCode,
                                                            u4VlanId,
                                                            pu1MacAddr,
                                                            u4ReceivePort,
                                                            pStaticPortList)
            == SNMP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return CLI_FAILURE;
        }

        if (nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts (&u4ErrCode,
                                                               u4VlanId,
                                                               pu1MacAddr,
                                                               u4ReceivePort,
                                                               &ForbiddenPortList)
            == SNMP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return CLI_FAILURE;
        }
    }
    if (u4Status != VLAN_NOT_PRESENT)
    {
        if (nmhTestv2Dot1qStaticMulticastStatus (&u4ErrCode, u4VlanId,
                                                 pu1MacAddr, u4ReceivePort,
                                                 u4Status) == SNMP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            return CLI_FAILURE;
        }
    }

    /* 
     * From here we go ahead setting the egress and forbidden 
     * ports since validation has passed 
     */
    if (nmhSetDot1qStaticMulticastStaticEgressPorts (u4VlanId, pu1MacAddr,
                                                     u4ReceivePort,
                                                     pStaticPortList)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
        UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
        return CLI_FAILURE;
    }

    if (nmhSetDot1qStaticMulticastForbiddenEgressPorts (u4VlanId, pu1MacAddr,
                                                        u4ReceivePort,
                                                        pForbiddenPortList)
        == SNMP_FAILURE)
    {
        if (u1Modify == VLAN_TRUE)
        {
            nmhSetDot1qStaticMulticastStaticEgressPorts (u4VlanId, pu1MacAddr,
                                                         u4ReceivePort,
                                                         &OldStaticPortList);
        }
        else
        {
            nmhSetDot1qStaticMulticastStatus (u4VlanId, pu1MacAddr,
                                              u4ReceivePort, VLAN_MGMT_INVALID);
        }

        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
        UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
        return CLI_FAILURE;
    }

    /* We go ahead and set the Status if it is given through CLI */
    if (u4Status != VLAN_NOT_PRESENT)
    {
        if (nmhSetDot1qStaticMulticastStatus (u4VlanId, pu1MacAddr,
                                              u4ReceivePort, u4Status)
            == SNMP_FAILURE)
        {
            if (u1Modify == VLAN_TRUE)
            {
                nmhSetDot1qStaticMulticastStaticEgressPorts (u4VlanId,
                                                             pu1MacAddr,
                                                             u4ReceivePort,
                                                             &OldStaticPortList);

                nmhSetDot1qStaticMulticastForbiddenEgressPorts (u4VlanId,
                                                                pu1MacAddr,
                                                                u4ReceivePort,
                                                                &OldForbiddenPortList);
            }
            else
            {
                nmhSetDot1qStaticMulticastStatus (u4VlanId, pu1MacAddr,
                                                  u4ReceivePort,
                                                  VLAN_MGMT_INVALID);
            }

            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            return CLI_FAILURE;
        }
    }
    UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
    UtilPlstReleaseLocalPortList (pau1OldForbbidenPortList);
    UtilPlstReleaseLocalPortList (pau1StaticPortList);
    UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : VlanDot1dMulticastEntryAdd ()                          */
/*                                                                           */
/* DESCRIPTION      : This function adds a Static Multicast entry for Dot1d  */
/*                    Bridge                                                 */
/*                                                                           */
/* INPUT            : CliHandle            - Cli Context Handle              */
/*                    pu1MacAddr           - Multicast Mac address           */
/*                    u4ReceivePort        - Receive Port                    */
/*                    pu1EgressPorts       - Multicast Member Ports          */
/*                    u4Status             - Multicast entry Status          */
/*                    u4EgressFlag         - Flag to indicate if Egress      */
/*                                           Ports are present               */
/*                                                                           */
/* OUTPUT           : CliHandle - Contains error messages                    */
/*                                                                           */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDot1dMulticastEntryAdd (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                            UINT4 u4ReceivePort, UINT1 *pu1EgressPorts,
                            UINT4 u4Status, UINT4 u4EgressFlag)
{
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    INT4                i4OldStatus = VLAN_INIT_VAL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1               u1Modify = VLAN_TRUE;
    UINT1               u1Result = VLAN_INIT_VAL;
    UINT4               u4VlanId = VLAN_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE *pStaticPortList = &StaticPortList;
    tSNMP_OCTET_STRING_TYPE OldStaticPortList;
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbbidenPortList = NULL;
    UINT1              *pau1OldStaticPortList = NULL;

    u4VlanId = VLAN_DEF_VLAN_ID;
    pau1StaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanDot1dMulticastEntryAdd: "
                  "Error in allocating memory for pau1StaticPortList\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1ForbbidenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1ForbbidenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanDot1dMulticastEntryAdd: "
                  "Error in allocating memory for pau1ForbbidenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1ForbbidenPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1OldStaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldStaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanDot1dMulticastEntryAdd: "
                  "Error in allocating memory for pau1OldStaticPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldStaticPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1StaticPortList, pu1EgressPorts, VLAN_PORT_LIST_SIZE);
    StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    StaticPortList.pu1_OctetList = pau1StaticPortList;

    MEMSET (pau1ForbbidenPortList, VLAN_INIT_VAL, VLAN_PORT_LIST_SIZE);
    ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    ForbiddenPortList.pu1_OctetList = pau1ForbbidenPortList;
    /* 
     * If the egress ports are not given in the input ,we set all 
     * members of the VLAN in egress ports 
     */

    if (u4EgressFlag == VLAN_FALSE)
    {
        pVlanEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);

        if (pVlanEntry != NULL)
        {
            VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pau1StaticPortList);
        }
    }

    /* Check if the multicast entry is already available in the data base */
    if (nmhGetDot1dStaticStatus (pu1MacAddr, u4ReceivePort,
                                 &i4OldStatus) == SNMP_FAILURE)
    {
        u1Modify = VLAN_FALSE;
    }

    /* 
     * If we are going to modify the existing entry we call a separate 
     * test function to validate the inputs, since the normal nmh Validation
     * routines check for mutual exclusiveness with the existing egress or 
     * forbidden ports 
     */

    if (u1Modify == VLAN_TRUE)
    {
        if ((VlanTestStaticMcastModificationInfo (CliHandle, u4VlanId,
                                                  pu1MacAddr, u4ReceivePort,
                                                  &StaticPortList,
                                                  &ForbiddenPortList))
            == CLI_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            return CLI_FAILURE;
        }

        OldStaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
        OldStaticPortList.pu1_OctetList = pau1OldStaticPortList;

        nmhGetDot1dStaticAllowedToGoTo (pu1MacAddr,
                                        u4ReceivePort, &OldStaticPortList);

    }
    else
    {
        /* 
         * We are creating a new static multicast entry ,checking if 
         * egress and forbidden ports are exclusive 
         */
        VLAN_ARE_PORTS_EXCLUSIVE (StaticPortList.pu1_OctetList,
                                  ForbiddenPortList.pu1_OctetList, u1Result);

        if (u1Result == VLAN_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Egress Ports and Forbidden Ports "
                       "are overlapping\r\n");
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            return CLI_FAILURE;
        }

        /* 
         * Here we are validating the egress ports and forbidden ports 
         * when we are creating a new entry 
         */

        if (nmhTestv2Dot1dStaticAllowedToGoTo (&u4ErrCode,
                                               pu1MacAddr,
                                               u4ReceivePort,
                                               pStaticPortList) == SNMP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            return CLI_FAILURE;
        }
    }
    UtilPlstReleaseLocalPortList (pau1ForbbidenPortList);
    if (u4Status != VLAN_NOT_PRESENT)
    {
        if (nmhTestv2Dot1dStaticStatus (&u4ErrCode,
                                        pu1MacAddr, (INT4) u4ReceivePort,
                                        u4Status) == SNMP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return CLI_FAILURE;
        }
    }

    /* 
     * From here we go ahead setting the egress and forbidden 
     * ports since validation has passed 
     */
    if (nmhSetDot1dStaticAllowedToGoTo (pu1MacAddr,
                                        u4ReceivePort,
                                        pStaticPortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1StaticPortList);

    /* We go ahead and set the Status if it is given through CLI */
    if (u4Status != VLAN_NOT_PRESENT)
    {
        if (nmhSetDot1dStaticStatus (pu1MacAddr,
                                     u4ReceivePort, u4Status) == SNMP_FAILURE)
        {
            if (u1Modify == VLAN_TRUE)
            {
                nmhSetDot1dStaticAllowedToGoTo (pu1MacAddr,
                                                u4ReceivePort,
                                                &OldStaticPortList);

            }
            else
            {
                nmhSetDot1dStaticStatus (pu1MacAddr,
                                         u4ReceivePort, VLAN_MGMT_INVALID);
            }

            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            return CLI_FAILURE;
        }
    }

    UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : VlanMulticastEntryDel ()                               */
/*                                                                           */
/* DESCRIPTION      : This function deletes the Static Multicast entry       */
/*                                                                           */
/* INPUT            : CliHandle            - Cli Context Handle              */
/*                    pu1MacAddr           - Multicast Mac address           */
/*                    u4ReceivePort        - Receive Port                    */
/*                                                                           */
/* OUTPUT           : CliHandle - Contains error messages                    */
/*                                                                           */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMulticastEntryDel (tCliHandle CliHandle,
                       UINT1 *pu1MacAddr, UINT4 u4ReceivePort, UINT4 u4VlanId)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal;

    if (nmhGetDot1qStaticMulticastStatus
        (u4VlanId, pu1MacAddr, u4ReceivePort, &i4RetVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Such Static Multicast entry \r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1qStaticMulticastStatus (&u4ErrCode, u4VlanId,
                                             pu1MacAddr, u4ReceivePort,
                                             VLAN_MGMT_INVALID) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qStaticMulticastStatus (u4VlanId, pu1MacAddr,
                                          u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : VlanDot1dMulticastEntryDel ()                          */
/*                                                                           */
/* DESCRIPTION      : This function deletes the Static Multicast entry       */
/*                    in Dot1d Static table                                  */
/*                                                                           */
/* INPUT            : CliHandle            - Cli Context Handle              */
/*                    pu1MacAddr           - Multicast Mac address           */
/*                    u4ReceivePort        - Receive Port                    */
/*                                                                           */
/* OUTPUT           : CliHandle - Contains error messages                    */
/*                                                                           */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDot1dMulticastEntryDel (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                            UINT4 u4ReceivePort, UINT4 u4VlanId)
{
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    INT4                i4RetVal = VLAN_INIT_VAL;

    UNUSED_PARAM (u4VlanId);

    if (nmhTestv2Dot1dStaticStatus (&u4ErrCode, pu1MacAddr,
                                    (INT4) u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetDot1dStaticStatus (pu1MacAddr, u4ReceivePort, &i4RetVal)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Such Static Multicast entry \r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetDot1dStaticStatus (pu1MacAddr, u4ReceivePort, VLAN_MGMT_INVALID)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef VLAN_EXTENDED_FILTER
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetFwdEntry                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets Forwarding information          */
/*                                                                           */
/*     INPUT            : CliHandle         - CLI Context Handle             */
/*                        pu1StaticPorts    - Contains static ports          */
/*                        pu1ForbiddenPorts - Contains forbidden ports       */
/*                        u4EgressFlag      - Flag to indicate if Egress     */
/*                                            Ports are present              */
/*                        u4ForbiddenFlag   - Flag to indicate if Forbidden  */
/*                                            Ports are present              */
/*                        u4Type            - Flag to indicate FWD_ALL/UNREG */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetFwdEntry (tCliHandle CliHandle, UINT1 *pu1EgressPorts,
                 UINT1 *pu1ForbiddenPorts, UINT4 u4EgressFlag,
                 UINT4 u4ForbiddenFlag, UINT4 u4Type)
{
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    UINT1              *pau1OldStaticPortList = NULL;
    UINT4               u4VlanId;
    INT4                i4Val = 0;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE *pStaticPortList = &StaticPortList;
    tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
    tSNMP_OCTET_STRING_TYPE OldStaticPortList;

    UNUSED_PARAM (u4ForbiddenFlag);

    i4Val = CLI_GET_VLANID ();

    if (i4Val == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    u4VlanId = (UINT4) i4Val;

    pau1ForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1ForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetFwdEntry: "
                  "Error in allocating memory for pau1ForbiddenPortList\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1StaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetFwdEntry: "
                  "Error in allocating memory for pau1StaticPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1StaticPortList, pu1EgressPorts, VLAN_PORT_LIST_SIZE);
    StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    StaticPortList.pu1_OctetList = pau1StaticPortList;

    MEMCPY (pau1ForbiddenPortList, pu1ForbiddenPorts, VLAN_PORT_LIST_SIZE);
    ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    ForbiddenPortList.pu1_OctetList = pau1ForbiddenPortList;

    /*    
     * we call a separate test function to validate the inputs,
     * since the normal nmh Validation routines check for mutual
     * exclusiveness with the existing egress or forbidden ports
     */

    if (VlanTestStaticDefGroupModificationInfo (CliHandle, u4VlanId,
                                                &StaticPortList,
                                                &ForbiddenPortList,
                                                u4Type) == CLI_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return CLI_FAILURE;
    }
    pau1OldStaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldStaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSetFwdEntry: "
                  "Error in allocating memory for pau1OldStaticPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1OldStaticPortList, 0, VLAN_PORT_LIST_SIZE);

    OldStaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
    OldStaticPortList.pu1_OctetList = pau1OldStaticPortList;

    /* 
     * From here we go ahead setting the egress and forbidden 
     * ports since validation has passed 
     */
    if (u4Type == VLAN_ALL_GROUPS)
    {
        if (u4EgressFlag == VLAN_TRUE)
        {
            nmhGetDot1qForwardAllStaticPorts (u4VlanId, &OldStaticPortList);

            if (nmhSetDot1qForwardAllStaticPorts (u4VlanId,
                                                  pStaticPortList)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
                UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
                return CLI_FAILURE;
            }
        }

        if (nmhSetDot1qForwardAllForbiddenPorts (u4VlanId,
                                                 &ForbiddenPortList)
            == SNMP_FAILURE)
        {
            if (u4EgressFlag == VLAN_TRUE)
            {
                nmhSetDot1qForwardAllStaticPorts (u4VlanId, &OldStaticPortList);
            }

            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            return CLI_FAILURE;
        }
    }
    else if (u4Type == VLAN_UNREG_GROUPS)
    {
        if (u4EgressFlag == VLAN_TRUE)
        {
            nmhGetDot1qForwardUnregisteredPorts (u4VlanId, &OldStaticPortList);

            if (nmhSetDot1qForwardUnregisteredStaticPorts (u4VlanId,
                                                           pStaticPortList)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
                UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
                return CLI_FAILURE;
            }
        }

        if (nmhSetDot1qForwardUnregisteredForbiddenPorts (u4VlanId,
                                                          &ForbiddenPortList)
            == SNMP_FAILURE)
        {
            if (u4EgressFlag == VLAN_TRUE)
            {
                nmhSetDot1qForwardUnregisteredStaticPorts (u4VlanId,
                                                           &OldStaticPortList);
            }

            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
            return CLI_FAILURE;
        }
    }

    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
    UtilPlstReleaseLocalPortList (pau1StaticPortList);
    UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanReSetFwdEntry                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Re-sets Forward All information      */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanReSetFwdEntry (tCliHandle CliHandle, UINT4 u4Type)
{
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    tVlanCurrEntry     *pCurrEntry;
    UINT4               u4VlanId;
    INT4                i4RetVal;
    INT4                i4Val = 0;
    UINT2               u2Port;

    i4Val = CLI_GET_VLANID ();

    if (i4Val == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    u4VlanId = (UINT4) i4Val;

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);

    if (pCurrEntry == NULL)
    {
        /* 
         * pCurrEntry must be created in Current Vlan entry for
         * the configuration of Forward All Groups.
         */
        CliPrintf (CliHandle, "\r%% Vlan is not active \r\n");
        return CLI_FAILURE;
    }
    pau1StaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1StaticPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanReSetFwdEntry: "
                  "Error in allocating memory for pau1StaticPortList\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1ForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1ForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanReSetFwdEntry: "
                  "Error in allocating memory for pau1ForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1StaticPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

    if (u4Type != VLAN_ALL_GROUPS)
    {
        VLAN_SCAN_PORT_TABLE (u2Port)
        {
            VLAN_SET_MEMBER_PORT (pau1StaticPortList, u2Port);
        }
    }

    MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    i4RetVal = VlanSetFwdEntry (CliHandle, pau1StaticPortList,
                                pau1ForbiddenPortList, VLAN_TRUE,
                                VLAN_TRUE, u4Type);
    UtilPlstReleaseLocalPortList (pau1StaticPortList);
    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
    return (i4RetVal);
}
#endif /* GARP_EXTENDED_FILTER */

#ifdef TRACE_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetDebugs                                      */
/*                                                                           */
/*     DESCRIPTION      : This function configures/deconfigures debug level  */
/*                                                                           */
/*     INPUT            : u4DbgMod   - Submodule for which Trace is modified */
/*                        u4DbgValue - Type of the Trace that is modified    */
/*                        u1Action   - Set/Re-set action                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetDebugs (tCliHandle CliHandle, UINT4 u4DbgMod, UINT4 u4DbgValue,
               UINT1 u1Action)
{
    UINT4               u4CurDbgValue;
    UINT4               u4NewDbgValue = 0;
    UINT4               u4ErrCode;

    nmhGetDot1qFutureVlanDebug ((INT4 *) &u4CurDbgValue);
    u4NewDbgValue = u4DbgMod | u4DbgValue;

    if (u1Action == VLAN_SET_CMD)
    {
        u4NewDbgValue = u4NewDbgValue | u4CurDbgValue;
    }
    else
    {
        /* No debug command */
        if ((u4NewDbgValue & u4CurDbgValue) == 0)
        {
            /* Debug for the specific module was not enabled */
            return CLI_SUCCESS;
        }

        /* Resetting the bit corresponding to the specific module */
        u4NewDbgValue = u4CurDbgValue & (~u4DbgMod);
        u4NewDbgValue = u4NewDbgValue & (~u4DbgValue);
    }

    if (nmhTestv2Dot1qFutureVlanDebug (&u4ErrCode, (INT4) u4NewDbgValue)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanDebug ((INT4) u4NewDbgValue) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetGlobalDebug                                 */
/*                                                                           */
/*     DESCRIPTION      : This function configures/deconfigures debug level  */
/*                                                                           */
/*     INPUT            : u1Action   - Set/Re-set action                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetGlobalDebug (tCliHandle CliHandle, UINT1 u1Action)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsMIDot1qFutureVlanGlobalTrace (&u4ErrCode, u1Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsMIDot1qFutureVlanGlobalTrace (u1Action);
    return CLI_SUCCESS;
}

#endif /* TRACE_WANTED */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortPvid                                    */
/*                                                                           */
/*     DESCRIPTION      : This function configures/re-sets port's PVID       */
/*                                                                           */
/*     INPUT            : u4VlanId  - Port VLAN identifier                   */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortPvid (tCliHandle CliHandle, UINT4 u4VlanId, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qPvid (&u4ErrCode, u4PortId, u4VlanId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qPvid (u4PortId, u4VlanId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortMode                                    */
/*                                                                           */
/*     DESCRIPTION      : This function configures/re-sets port  Mode.       */
/*                                                                           */
/*     INPUT            : u4PortMode- Access / Trunk / Hybrid                */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortMode (tCliHandle CliHandle, UINT4 u4PortId, UINT4 u4PortMode)
{
    UINT4               u4ErrCode;
    UINT4               u4VlanId = VLAN_DEF_VLAN_ID;
    INT4                i4RetStatus = CLI_FAILURE;

    if (nmhTestv2Dot1qFutureVlanPortType (&u4ErrCode, (INT4) u4PortId,
                                          (INT4) u4PortMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (u4PortMode == VLAN_TRUNK_PORT)
    {
        /* Remove the port if it is an access port in any of the VLANs */
        i4RetStatus =
            VlanRemoveUntaggedPortFromVlans (CliHandle, u4VlanId,
                                             u4PortId, (UINT1) VLAN_FALSE);

        if (i4RetStatus == CLI_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhSetDot1qFutureVlanPortType (u4PortId, u4PortMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortFrameType                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures/re-sets port's frame      */
/*                        handling type                                      */
/*                                                                           */
/*     INPUT            : u1FrameType  - Port VLAN identifier                */
/*                        u4PortId     - Port identifier                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortFrameType (tCliHandle CliHandle, INT4 i4FrameType, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qPortAcceptableFrameTypes (&u4ErrCode, u4PortId,
                                                i4FrameType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qPortAcceptableFrameTypes (u4PortId, i4FrameType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortFiltering                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures/re-sets port's filtering  */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : u4Action  - Action to perform Set/Re-set filtering */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortFiltering (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qPortIngressFiltering (&u4ErrCode, u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qPortIngressFiltering (u4PortId, u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortMacbased                                */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables MAC based learning  */
/*                        on the port.                                       */
/*                                                                           */
/*     INPUT            : u4Action  - Action to perform Set/Re-set mac based */
/*                                    learning                               */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortMacbased (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanPortMacBasedClassification (&u4ErrCode,
                                                            u4PortId,
                                                            u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortMacBasedClassification (u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortSubnetbased                             */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables Subnet based VLAN   */
/*                        classification on the port.                        */
/*                                                                           */
/*     INPUT            : u4Action  - Action to perform Set/Re-set Subnet    */
/*                                    based VLAN classification              */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortSubnetbased (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode = VLAN_INIT_VAL;

    if (nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification (&u4ErrCode,
                                                               u4PortId,
                                                               u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortSubnetBasedClassification (u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortProtocolbased                           */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables protocol based      */
/*                        learning on the port.                              */
/*                                                                           */
/*     INPUT            : u4Action  - Action to perform Set/Re-set port to   */
/*                                    perform protocol based learning        */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortProtocolbased (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification
        (&u4ErrCode, u4PortId, u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortPortProtoBasedClassification
        (u4PortId, u4Action) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VLAN_PROTO_PORT_ERR);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortProtoGroup                              */
/*                                                                           */
/*     DESCRIPTION      : This function set/re-sets port to protocol mapping */
/*                        on the port.                                       */
/*                                                                           */
/*     INPUT            : u4GroupId - Group identifier                       */
/*                        u4VlanId  - Vlan identifier                        */
/*                        u4PortId  - Port identifier                        */
/*                        u1Action  - Action to be performed Set/Re-set      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortProtoGroup (tCliHandle CliHandle, UINT4 u4GroupId, UINT4 u4VlanId,
                       UINT4 u4PortId, UINT1 u1Action)
{
    UINT4               u4ErrCode;
    UINT1               u1IsEntryExists = VLAN_TRUE;    /* Indicates whether the
                                                           command is for modify-
                                                           ing the entry or for 
                                                           creating new one.
                                                         */
    INT1                i1Result;
    INT4                i4PortProtocolRowStatus;

    switch (u1Action)
    {
        case VLAN_SET_CMD:

            /* Check Whether the Entry Exists  or Not */
            i1Result = nmhGetDot1vProtocolPortRowStatus (u4PortId, u4GroupId,
                                                         &i4PortProtocolRowStatus);

            if (i1Result == SNMP_FAILURE)
            {
                /* new entry */
                i4PortProtocolRowStatus = VLAN_CREATE_AND_WAIT;
                u1IsEntryExists = VLAN_FALSE;
            }
            else
            {                    /* entry available . start modifying */
                i4PortProtocolRowStatus = VLAN_NOT_IN_SERVICE;
            }

            if (nmhTestv2Dot1vProtocolPortRowStatus (&u4ErrCode,
                                                     u4PortId, u4GroupId,
                                                     i4PortProtocolRowStatus) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1vProtocolPortRowStatus
                ((INT4) u4PortId, (INT4) u4GroupId, i4PortProtocolRowStatus)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhTestv2Dot1vProtocolPortGroupVid (&u4ErrCode, u4PortId,
                                                    u4GroupId,
                                                    u4VlanId) == SNMP_FAILURE)
            {
                if (u1IsEntryExists == VLAN_FALSE)
                {
                    nmhSetDot1vProtocolPortRowStatus (u4PortId, u4GroupId,
                                                      VLAN_DESTROY);
                }
                return CLI_FAILURE;
            }

            if (nmhSetDot1vProtocolPortGroupVid (u4PortId, u4GroupId,
                                                 u4VlanId) == SNMP_FAILURE)
            {

                if (u1IsEntryExists == VLAN_FALSE)
                {
                    nmhSetDot1vProtocolPortRowStatus (u4PortId, u4GroupId,
                                                      VLAN_DESTROY);
                }
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetDot1vProtocolPortRowStatus (u4PortId, u4GroupId,
                                                  VLAN_ACTIVE) == SNMP_FAILURE)
            {
                nmhSetDot1vProtocolPortRowStatus
                    (u4PortId, u4GroupId, VLAN_DESTROY);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;

        case VLAN_NO_CMD:

            if (nmhTestv2Dot1vProtocolPortRowStatus (&u4ErrCode,
                                                     u4PortId, u4GroupId,
                                                     VLAN_DESTROY) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1vProtocolPortRowStatus
                (u4PortId, u4GroupId, VLAN_DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;

        default:
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortDefaultPriority                         */
/*                                                                           */
/*     DESCRIPTION      : This function set/re-sets port's default priority  */
/*                        value.                                             */
/*                                                                           */
/*     INPUT            : u1DefPriority - Priority to be assigned to port as */
/*                                        default priority                   */
/*                        u4PortId      - Port identifier                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortDefaultPriority (tCliHandle CliHandle, INT4 i4DefPriority,
                            UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1dPortDefaultUserPriority (&u4ErrCode, u4PortId,
                                               i4DefPriority) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1dPortDefaultUserPriority (u4PortId, i4DefPriority)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetMaxTrafficClass                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets/re-sets traffic class value to  */
/*                        a port.                                            */
/*                                                                           */
/*     INPUT            : u4MaxTrafficClass - Traffic class value            */
/*                        u4PortId          - Port identifier                */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetMaxTrafficClass (tCliHandle CliHandle, UINT4 u4MaxTrafficClass,
                        UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1dPortNumTrafficClasses (&u4ErrCode, u4PortId,
                                             u4MaxTrafficClass) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1dPortNumTrafficClasses (u4PortId, u4MaxTrafficClass)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortPrioAndTrafficClass                     */
/*                                                                           */
/*     DESCRIPTION      : This function maps priority and traffic classes on */
/*                        a port.                                            */
/*                                                                           */
/*     INPUT            : u4Priority     - Priority value                    */
/*                        u4TrafficClass - Traffic class value               */
/*                        u4PortId       - Port identifier                   */
/*                        u1Action       - Action to be performed Set/Re-set */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortPrioAndTrafficClass (tCliHandle CliHandle, UINT4 u4Priority,
                                UINT4 u4TrafficClass, UINT4 u4PortId,
                                UINT1 u1Action)
{
    UINT4               u4ErrCode;
    UINT4               u4DefTrafficClass;

    UNUSED_PARAM (CliHandle);

    switch (u1Action)
    {
        case VLAN_SET_CMD:
            if (nmhTestv2Dot1dTrafficClass (&u4ErrCode, u4PortId, u4Priority,
                                            u4TrafficClass) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1dTrafficClass (u4PortId, u4Priority, u4TrafficClass)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case VLAN_NO_CMD:
            u4DefTrafficClass =
                gau1PriTrfClassMap[u4Priority][VLAN_MAX_TRAFF_CLASS - 1];

            if (nmhTestv2Dot1dTrafficClass (&u4ErrCode, u4PortId, u4Priority,
                                            u4DefTrafficClass) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1dTrafficClass (u4PortId, u4Priority,
                                         u4DefTrafficClass) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        default:
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanUpdateMacMapEntry                              */
/*                                                                           */
/*     DESCRIPTION      : This function will Add/Delete Mac Map Entry        */
/*                                                                           */
/*     INPUT            : pu1MacAddr - Mac address of the entry              */
/*                        VlanId     - Vlan Id to be associated              */
/*                        u1Flag     - Add/Delete operation of the entry     */
/*                        u4Port     - Port number for the Entry             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateMacMapEntry (tCliHandle CliHandle, UINT1 *pu1MacAddr, UINT4 u4Port,
                       UINT4 u4VlanId, INT4 i4McastOption, UINT1 u1Flag)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;
    INT4                i4Status = VLAN_ACTIVE;
    INT4                i4OldVlanId = 0;
    INT4                i4OldMcastOption = 0;

    if (nmhGetDot1qFutureVlanPortMacMapRowStatus
        ((INT4) u4Port, pu1MacAddr, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* Entry available, correspondingly change the row status */
        if (u1Flag == CLI_VLAN_MAC_MAP_ADD)
        {
            i4RowStatus = VLAN_NOT_IN_SERVICE;
            nmhGetDot1qFutureVlanPortMacMapVid ((INT4) u4Port,
                                                pu1MacAddr, &i4OldVlanId);
            nmhGetDot1qFutureVlanPortMacMapMcastBcastOption ((INT4) u4Port,
                                                             pu1MacAddr,
                                                             &i4OldMcastOption);
        }
        else if (u1Flag == CLI_VLAN_MAC_MAP_DEL)
        {
            i4RowStatus = VLAN_DESTROY;
        }
    }
    else
    {
        if (u1Flag == CLI_VLAN_MAC_MAP_ADD)
        {
            /* Entry not available */
            i4RowStatus = VLAN_CREATE_AND_WAIT;
        }
        else if (u1Flag == CLI_VLAN_MAC_MAP_DEL)
        {
            /* Trying to delete a entry which is not there.
             * ignore the command */
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2Dot1qFutureVlanPortMacMapRowStatus (&u4ErrCode,
                                                     (INT4) u4Port, pu1MacAddr,
                                                     i4RowStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortMacMapRowStatus ((INT4) u4Port, pu1MacAddr,
                                                  i4RowStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1Flag == CLI_VLAN_MAC_MAP_ADD)
    {
        if (nmhTestv2Dot1qFutureVlanPortMacMapVid (&u4ErrCode,
                                                   (INT4) u4Port,
                                                   pu1MacAddr,
                                                   (INT4) u4VlanId)
            == SNMP_FAILURE)
        {
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }

            nmhSetDot1qFutureVlanPortMacMapRowStatus ((INT4) u4Port,
                                                      pu1MacAddr, i4Status);
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureVlanPortMacMapVid ((INT4) u4Port, pu1MacAddr,
                                                (INT4) u4VlanId) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }
            nmhSetDot1qFutureVlanPortMacMapRowStatus ((INT4) u4Port,
                                                      pu1MacAddr, i4Status);
            return CLI_FAILURE;
        }

        if (VlanSetMcastBcastTrafficStatus (u4Port,
                                            pu1MacAddr,
                                            i4McastOption) == CLI_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }
            nmhSetDot1qFutureVlanPortMacMapRowStatus ((INT4) u4Port,
                                                      pu1MacAddr, i4Status);
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureVlanPortMacMapRowStatus ((INT4) u4Port,
                                                      pu1MacAddr,
                                                      VLAN_ACTIVE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }
            else
            {
                nmhSetDot1qFutureVlanPortMacMapVid ((INT4) u4Port,
                                                    pu1MacAddr, i4OldVlanId);
                nmhSetDot1qFutureVlanPortMacMapMcastBcastOption
                    ((INT4) u4Port, pu1MacAddr, i4OldMcastOption);
            }
            nmhSetDot1qFutureVlanPortMacMapRowStatus ((INT4) u4Port,
                                                      pu1MacAddr, i4Status);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanUpdateSubnetMapEntry                           */
/*                                                                           */
/*     DESCRIPTION      : This function will Add/Delete Subnet Map Entry     */
/*                                                                           */
/*     INPUT            : u4SrcIPAddr - Source IP of the entry               */
/*                        u4SubnetMask - Subnet mask                         */
/*                        VlanId     - Vlan Id to be associated              */
/*                        u1Flag     - Add/Delete operation of the entry     */
/*                        u4Port     - Port number for the Entry             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateSubnetMapEntry (tCliHandle CliHandle, UINT4 u4SrcIPAddr,
                          UINT4 u4SubnetMask, UINT4 u4Port, UINT4 u4VlanId,
                          INT4 i4ArpOption, UINT1 u1Flag)
{
    UINT4               u4ErrCode = VLAN_INIT_VAL;
    INT4                i4RowStatus = VLAN_INIT_VAL;
    INT4                i4Status = VLAN_ACTIVE;
    INT4                i4OldVlanId = VLAN_INIT_VAL;
    INT4                i4OldArpOption = VLAN_INIT_VAL;

    if (nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
        ((INT4) u4Port, u4SrcIPAddr, u4SubnetMask, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        /* Entry available, correspondingly change the row status */
        if (u1Flag == CLI_VLAN_SUBNET_MAP_ADD)
        {
            i4RowStatus = VLAN_NOT_IN_SERVICE;
            nmhGetDot1qFutureVlanPortSubnetMapExtVid ((INT4) u4Port,
                                                      u4SrcIPAddr,
                                                      u4SubnetMask,
                                                      &i4OldVlanId);

            nmhGetDot1qFutureVlanPortSubnetMapExtARPOption ((INT4) u4Port,
                                                            u4SrcIPAddr,
                                                            u4SubnetMask,
                                                            &i4OldArpOption);
        }
        else if (u1Flag == CLI_VLAN_SUBNET_MAP_DEL)
        {
            i4RowStatus = VLAN_DESTROY;
        }
    }
    else
    {
        if (u1Flag == CLI_VLAN_SUBNET_MAP_ADD)
        {
            /* Entry not available */
            i4RowStatus = VLAN_CREATE_AND_WAIT;
        }
        else if (u1Flag == CLI_VLAN_SUBNET_MAP_DEL)
        {
            /* Trying to delete a entry which is not there.
             * ignore the command */
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus (&u4ErrCode,
                                                           (INT4) u4Port,
                                                           u4SrcIPAddr,
                                                           u4SubnetMask,
                                                           i4RowStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus
        ((INT4) u4Port, u4SrcIPAddr, u4SubnetMask, i4RowStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1Flag == CLI_VLAN_SUBNET_MAP_ADD)
    {
        if (nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid (&u4ErrCode,
                                                         (INT4) u4Port,
                                                         u4SrcIPAddr,
                                                         u4SubnetMask,
                                                         (INT4) u4VlanId)
            == SNMP_FAILURE)
        {
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }

            nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus ((INT4) u4Port,
                                                            u4SrcIPAddr,
                                                            u4SubnetMask,
                                                            i4Status);
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapExtVid ((INT4) u4Port,
                                                      u4SrcIPAddr,
                                                      u4SubnetMask,
                                                      (INT4) u4VlanId)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }
            nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus ((INT4) u4Port,
                                                            u4SrcIPAddr,
                                                            u4SubnetMask,
                                                            i4Status);
            return CLI_FAILURE;
        }

        if (VlanSetARPOption (u4Port, u4SrcIPAddr, u4SubnetMask,
                              i4ArpOption) == CLI_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }
            nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus ((INT4) u4Port,
                                                            u4SrcIPAddr,
                                                            u4SubnetMask,
                                                            i4Status);
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus ((INT4) u4Port,
                                                            u4SrcIPAddr,
                                                            u4SubnetMask,
                                                            VLAN_ACTIVE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                i4Status = VLAN_DESTROY;
            }
            else
            {
                nmhSetDot1qFutureVlanPortSubnetMapExtVid ((INT4) u4Port,
                                                          u4SrcIPAddr,
                                                          u4SubnetMask,
                                                          i4OldVlanId);
                nmhSetDot1qFutureVlanPortSubnetMapExtARPOption ((INT4) u4Port,
                                                                u4SrcIPAddr,
                                                                u4SubnetMask,
                                                                i4OldArpOption);
            }
            nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus ((INT4) u4Port,
                                                            u4SrcIPAddr,
                                                            u4SubnetMask,
                                                            i4Status);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetLearningMode                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the VLAN learning mode          */
/*                                                                           */
/*     INPUT            : CliHandle - Context in which the CLI command is    */
/*                                    processed.                             */
/*                                                                           */
/*                        u4VlanLearningMode - Vlan learning mode IVL/SVL/   */
/*                                             Hybrid.                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - If the command is executed           */
/*                                       successfully.                       */
/*                        CLI_FAILURE - If the command is not executed       */
/*                                       successfully.                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetLearningMode (tCliHandle CliHandle, UINT4 u4VlanLearningMode)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanLearningMode (&u4ErrCode,
                                              (INT4) u4VlanLearningMode) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanLearningMode ((INT4) u4VlanLearningMode) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetHybridTypeDefault                           */
/*                                                                           */
/*     DESCRIPTION      : This function deletes constraint vlan entry        */
/*                                                                           */
/*     INPUT            : CliHandle    - Context in which the CLI            */
/*                                       command is processed                */
/*                                                                           */
/*                        u4ConstraintType - Constraint Type - IVL/SVL       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetHybridTypeDefault (tCliHandle CliHandle, UINT4 u4ConstraintType)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal;

    i4RetVal = nmhTestv2Dot1qFutureVlanHybridTypeDefault (&u4ErrCode,
                                                          (INT4)
                                                          u4ConstraintType);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhSetDot1qFutureVlanHybridTypeDefault ((INT4) u4ConstraintType);

    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanMacLearningStatus                             */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Enable/Disable Learning    */
/*                        status for the particular vlan                    */
/*                                                                          */
/*     INPUT            : VlanId  - Vlan Identifier                         */
/*                        u4Status     - Enable/Disable state               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanMacLearningStatus (tCliHandle CliHandle, tVlanId VlanId, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanAdminMacLearningStatus
        (&u4ErrCode, VlanId, u4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1qFutureVlanAdminMacLearningStatus (VlanId,
                                                     u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanUnicastMacLimit                               */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Unicast Mac Limit for the  */
/*                        particular vlan                                   */
/*                                                                          */
/*     INPUT            : VlanId  - Vlan Identifier                         */
/*                        u4VlanMacLimit - Mac Limit Size                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanUnicastMacLimit (tCliHandle CliHandle, tVlanId VlanId, UINT4 u4VlanMacLimit)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Dot1qFutureVlanUnicastMacLimit
        (&u4ErrCode, VlanId, u4VlanMacLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanUnicastMacLimit (VlanId, u4VlanMacLimit)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanDatabase                                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN database               */
/*                                                                           */
/*     INPUT            : u4DisplayType - Display type (brief/vlan/summary   */
/*                        u4StartVlanId      - If starting Vlan identifier   */
/*                        u4LastVlanId       - If Last Vlan identifier       */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanDatabase (tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT4 u4DisplayType, UINT4 u4StartVlanId, UINT4 u4LastVlanId)
{
    tPortList          *pStaticPorts = NULL;
    tPortList          *pForbidPorts = NULL;
    tPortList          *pUntagPorts = NULL;
    INT4                i4RetVal = 0;
    INT4                i4VlanLoopbackStatus = 0;
    INT4                i4CurrentContextId;
    INT4                i4NextContextId;
    INT4                i4EgressEthertype = 0;
    UINT4               u4CurrentVlanId = 0;
    UINT4               u4NextVlanId;
    UINT4               u4CurrentTimeMark;
    UINT4               u4NextTimeMark;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    INT4                i4PortType;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1PbPortType;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];

    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE ForbiddenEgressPorts;
    tSNMP_OCTET_STRING_TYPE UntaggedPorts;

    if (u4DisplayType == CLI_VLAN_SHOW_VLAN_SUMMARY)
    {
        /* display vlan summary */
        nmhGetFsDot1qNumVlans (u4ContextId, (UINT4 *) &i4RetVal);
        CliPrintf (CliHandle, "\r\nNumber of vlans : %d\r\n\r\n", i4RetVal);
        return CLI_SUCCESS;
    }

    /* 
     * Since Lock has been taken before the switch construct in 
     * cli_process_vlan_cmd(), we are releasing the Lock here.
     */

    CliPrintf (CliHandle, "\r\nVlan database \r\n");
    CliPrintf (CliHandle, "------------- \r\n");

    i4CurrentContextId = (INT4) u4ContextId;

    if (u4StartVlanId != 0)
    {
        u4CurrentVlanId = u4StartVlanId - 1;
    }

    if (u4DisplayType == CLI_VLAN_SHOW_VLAN_ALL_ASCENDING)
    {
        if (VlanGetNextIndexAscendingFsDot1qVlanCurrentTable
            (i4CurrentContextId, &i4NextContextId, 0,
             &u4NextTimeMark, u4CurrentVlanId, &u4NextVlanId) == VLAN_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qVlanCurrentTable
            (i4CurrentContextId, &i4NextContextId, 0,
             &u4NextTimeMark, u4CurrentVlanId, &u4NextVlanId) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    if (i4CurrentContextId != i4NextContextId)
    {
        return CLI_SUCCESS;
    }

    pStaticPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pStaticPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pUntagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUntagPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
        return CLI_FAILURE;
    }

    pForbidPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbidPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
        FsUtilReleaseBitList ((UINT1 *) pUntagPorts);
        return CLI_FAILURE;
    }

    do
    {
        if (((u4LastVlanId != 0) &&
             ((u4NextVlanId >= u4StartVlanId)
              && (u4NextVlanId <= u4LastVlanId))) || (u4LastVlanId == 0))
        {
            /* Primary Index of VlanCurrentTable is TimeMark.
             * So walk on the entire table and print the entries
             * with vlan id in the required range. */

            MEMSET (*pStaticPorts, 0, sizeof (tPortList));
            MEMSET (*pUntagPorts, 0, sizeof (tPortList));
            MEMSET (*pForbidPorts, 0, sizeof (tPortList));
            if (VlanGetFirstPortInContext (i4NextContextId,
                                           &u4IfIndex) == VLAN_SUCCESS)
            {
                do
                {
                    nmhGetFsDot1qVlanCurrentEgressPort
                        ((UINT4) i4NextContextId, u4NextTimeMark, u4NextVlanId,
                         u4IfIndex, &i4PortType);
                    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
                    {

                        if (i4PortType == VLAN_ADD_UNTAGGED_PORT)
                        {
                            if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
                            {
                                VlanL2IwfGetPbPortType (u4IfIndex,
                                                        &u1PbPortType);

                                VLAN_SET_MEMBER_PORT_LIST ((*pUntagPorts),
                                                           u4IfIndex);
                                VLAN_SET_MEMBER_PORT_LIST ((*pStaticPorts),
                                                           u4IfIndex);
                            }
                            else
                            {
                                VLAN_SET_MEMBER_PORT_LIST ((*pUntagPorts),
                                                           u4IfIndex);
                                VLAN_SET_MEMBER_PORT_LIST ((*pStaticPorts),
                                                           u4IfIndex);
                            }
                        }
                        else if (i4PortType == VLAN_ADD_TAGGED_PORT)
                        {
                            VLAN_SET_MEMBER_PORT_LIST ((*pStaticPorts),
                                                       u4IfIndex);
                        }
                        VlanReleaseContext ();
                    }
                    nmhGetFsDot1qVlanStaticPort ((UINT4) i4NextContextId,
                                                 u4NextVlanId, u4IfIndex,
                                                 &i4PortType);
                    if (i4PortType == VLAN_ADD_ST_FORBIDDEN_PORT)
                    {
                        VLAN_SET_MEMBER_PORT_LIST ((*pForbidPorts), u4IfIndex);
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInContext
                       ((UINT4) i4NextContextId, u4PrvIfIndex,
                        &u4IfIndex) == VLAN_SUCCESS);
            }

            MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
            Name.pu1_OctetList = au1VlanName;
            Name.i4_Length = VLAN_STATIC_MAX_NAME_LEN;
            nmhGetFsDot1qVlanStaticName ((UINT4) i4NextContextId, u4NextVlanId,
                                         &Name);

            nmhGetFsDot1qVlanStatus ((UINT4) i4NextContextId, u4NextTimeMark,
                                     u4NextVlanId, &i4RetVal);

            CliPrintf (CliHandle, "Vlan ID             : %d\r\n", u4NextVlanId);

            if (FsUtilBitListIsAllZeros (*pStaticPorts,
                                         sizeof (tPortList)) == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "Member Ports        : None \r\n");
            }
            else
            {
                EgressPorts.pu1_OctetList = *pStaticPorts;
                EgressPorts.i4_Length = sizeof (tPortList);

                if (CliOctetToIfName
                    (CliHandle, "Member Ports        :",
                     &EgressPorts, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                     sizeof (tPortList), 0, &u4PagingStatus, 6) == CLI_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntagPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                    return CLI_FAILURE;
                }
            }

            if (FsUtilBitListIsAllZeros (*pUntagPorts,
                                         sizeof (tPortList)) == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "Untagged Ports      : None \r\n");
            }
            else
            {
                UntaggedPorts.pu1_OctetList = *pUntagPorts;
                UntaggedPorts.i4_Length = sizeof (tPortList);

                if (CliOctetToIfName (CliHandle,
                                      "Untagged Ports      :",
                                      &UntaggedPorts,
                                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                      sizeof (tPortList),
                                      0, &u4PagingStatus, 6) == CLI_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntagPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                    return CLI_FAILURE;
                }
            }

            if (FsUtilBitListIsAllZeros (*pForbidPorts,
                                         sizeof (tPortList)) == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "Forbidden Ports     : None \r\n");
            }
            else
            {
                ForbiddenEgressPorts.pu1_OctetList = *pForbidPorts;
                ForbiddenEgressPorts.i4_Length = sizeof (tPortList);

                if (CliOctetToIfName (CliHandle,
                                      "Forbidden Ports     :",
                                      &ForbiddenEgressPorts,
                                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                      sizeof (tPortList),
                                      0, &u4PagingStatus, 6) == CLI_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntagPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                    return CLI_FAILURE;
                }
            }

            CliPrintf (CliHandle, "Name                : %s\r\n",
                       Name.pu1_OctetList);

            if (i4RetVal == VLAN_CURR_ENTRY_PERMANENT)
            {
                CliPrintf (CliHandle, "Status              : Permanent\r\n");
            }
            else if (i4RetVal == VLAN_CURR_ENTRY_DYNAMIC)
            {
                if (VlanMrpIsMvrpEnabled (i4NextContextId) == OSIX_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Status              : Dynamic Mvrp\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Status              : Dynamic Gvrp\r\n");
                }
            }
            else
            {
                CliPrintf (CliHandle, "Status              : Other\r\n");
            }
            nmhGetFsMIDot1qFutureStVlanEgressEthertype (i4NextContextId,
                                                        u4NextVlanId,
                                                        &i4EgressEthertype);
            CliPrintf (CliHandle, "Egress Ethertype    : 0x%x\r\n",
                       i4EgressEthertype);
            VlanPbCliVlanDisplay (CliHandle, i4NextContextId, u4NextVlanId);

            /* Display the loopback status */
            nmhGetFsMIDot1qFutureVlanLoopbackStatus (i4NextContextId,
                                                     u4NextVlanId,
                                                     &i4VlanLoopbackStatus);
            if (i4VlanLoopbackStatus == VLAN_ENABLED)
            {
                CliPrintf (CliHandle,
                           "Service Loopback Status   : Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Service Loopback Status   : Disabled \r\n");
            }

            u4PagingStatus = CliPrintf (CliHandle,
                                        "----------------------------------------------------\r\n");

        }

        i4CurrentContextId = i4NextContextId;
        u4CurrentTimeMark = u4NextTimeMark;
        u4CurrentVlanId = u4NextVlanId;

        if (u4DisplayType == CLI_VLAN_SHOW_VLAN_ALL_ASCENDING)
        {
            if (VlanGetNextIndexAscendingFsDot1qVlanCurrentTable
                (i4CurrentContextId, &i4NextContextId, u4CurrentTimeMark,
                 &u4NextTimeMark, u4CurrentVlanId, &u4NextVlanId)
                == VLAN_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        else
        {
            if (nmhGetNextIndexFsDot1qVlanCurrentTable
                (i4CurrentContextId, &i4NextContextId,
                 u4CurrentTimeMark,
                 &u4NextTimeMark, u4CurrentVlanId, &u4NextVlanId)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
    FsUtilReleaseBitList ((UINT1 *) pUntagPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanGlobalInfo                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN global status          */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4VlanStatus = 0;
    INT4                i4VlanOperStatus = 0;
#ifdef GARP_WANTED
    INT4                i4GvrpStatus = 0;
    INT4                i4GmrpStatus = 0;
    INT4                i4GvrpOperStatus = 0;
    INT4                i4GmrpOperStatus = 0;
#endif
    INT4                i4MacBased = 0;
    INT4                i4SubnetBased = VLAN_INIT_VAL;
    INT4                i4PortProtoBased = 0;
    INT4                i4VlanBridgeMode = 0;
    INT4                i4TrafficClasses = 0;
    INT4                i4VlanLearnMode = 0;
    INT4                i4VlanVersion = 0;
    INT4                i4MaxVlanId = 0;
    INT4                i4MaxSupportedVlans = 0;
    UINT4               u4SwitchMacLimit = 0;
    INT4                i4BaseBridgeMode = DOT_1Q_VLAN_MODE;
    INT4                i4GlobMacLearnStat = 0;
    INT4                i4Status = 0;
    INT4                i4DefType = 0;

    nmhGetFsMIDot1qFutureVlanStatus (u4ContextId, &i4VlanStatus);
    nmhGetFsMIDot1qFutureVlanOperStatus (u4ContextId, &i4VlanOperStatus);

    VLAN_UNLOCK ();
    CliUnRegisterLock (CliHandle);

#ifdef GARP_WANTED
    GARP_LOCK ();

    nmhGetFsDot1qGvrpStatus ((INT4) u4ContextId, &i4GvrpStatus);
    nmhGetFsDot1dGmrpStatus ((INT4) u4ContextId, &i4GmrpStatus);
    nmhGetFsMIDot1qFutureGvrpOperStatus (u4ContextId, &i4GvrpOperStatus);
    nmhGetFsMIDot1qFutureGmrpOperStatus (u4ContextId, &i4GmrpOperStatus);

    GARP_UNLOCK ();
#endif

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    nmhGetFsMIDot1qFutureVlanMacBasedOnAllPorts (u4ContextId, &i4MacBased);
    nmhGetFsMIDot1qFutureVlanSubnetBasedOnAllPorts (u4ContextId,
                                                    &i4SubnetBased);
    nmhGetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts (u4ContextId,
                                                       &i4PortProtoBased);
    VlanPbGetBridgeMode ((INT4) u4ContextId, &i4VlanBridgeMode);
    nmhGetFsDot1dTrafficClassesEnabled (u4ContextId, &i4TrafficClasses);
    nmhGetFsDot1qVlanVersionNumber ((INT4) u4ContextId, &i4VlanVersion);
    nmhGetFsDot1qMaxVlanId ((INT4) u4ContextId, &i4MaxVlanId);
    nmhGetFsDot1qMaxSupportedVlans ((INT4) u4ContextId,
                                    (UINT4 *) &i4MaxSupportedVlans);
    nmhGetFsMIDot1qFutureVlanLearningMode (u4ContextId, &i4VlanLearnMode);
    nmhGetFsMIDot1qFutureVlanHybridTypeDefault (u4ContextId, &i4DefType);
    nmhGetFsMIDot1qFutureUnicastMacLearningLimit ((INT4) u4ContextId,
                                                  &u4SwitchMacLimit);
    nmhGetFsMIDot1qFutureBaseBridgeMode ((INT4) u4ContextId, &i4BaseBridgeMode);
    nmhGetFsMIDot1qFutureVlanGlobalMacLearningStatus ((INT4) u4ContextId,
                                                      &i4GlobMacLearnStat);
    nmhGetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria ((INT4) u4ContextId,
                                                             &i4Status);

    CliPrintf (CliHandle, "\r\nVlan device configurations\r\n");

    CliPrintf (CliHandle, "--------------------------\r\n");

    if (i4VlanStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Vlan Status");
    }
    else if (i4VlanStatus == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Vlan Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", "Vlan Status");
    }

    if (i4VlanOperStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Vlan Oper status");
    }
    else if (i4VlanOperStatus == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Vlan Oper status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", "Vlan Oper status");
    }

#ifdef GARP_WANTED
    if (i4GvrpStatus == GVRP_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Gvrp status");
    }
    else if (i4GvrpStatus == GVRP_DISABLED)
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Gvrp status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", "Gvrp status");
    }

    if (i4GmrpStatus == GMRP_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Gmrp status");
    }
    else if (i4GmrpStatus == GMRP_DISABLED)
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Gmrp status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", "Gmrp status");
    }

    if (i4GvrpOperStatus == GVRP_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Gvrp Oper status");
    }
    else if (i4GvrpOperStatus == GVRP_DISABLED)
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Gvrp Oper status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", "Gvrp Oper status");
    }

    if (i4GmrpOperStatus == GMRP_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Gmrp Oper status");
    }
    else if (i4GmrpOperStatus == GMRP_DISABLED)
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Gmrp Oper status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", "Gmrp Oper status");
    }
#endif

    /* When u1HwUnicastMacLearningLimitSup is supported, 
     * Display unicast mac learning limit related display 
     */
    if (ISS_HW_SUPPORTED ==
        IssGetHwCapabilities (ISS_HW_UNICAST_MAC_LEARNING_LIMIT))
    {
        if (i4MacBased == VLAN_ENABLED)
        {
            CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Mac-Vlan Status");
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Mac-Vlan Status");
        }
    }

    if (i4SubnetBased == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Subnet-Vlan Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Subnet-Vlan Status");
    }

    if (i4PortProtoBased == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled \r\n", "Protocol-Vlan Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Protocol-Vlan Status");
    }

    switch (i4VlanBridgeMode)
    {
        case VLAN_PROVIDER_BRIDGE_MODE:
            CliPrintf (CliHandle, "%-33s : Provider Bridge \r\n",
                       "Bridge Mode");
            break;
        case VLAN_PROVIDER_CORE_BRIDGE_MODE:
            CliPrintf (CliHandle, "%-33s : Provider Core Bridge \r\n",
                       "Bridge Mode");
            break;
        case VLAN_PROVIDER_EDGE_BRIDGE_MODE:
            CliPrintf (CliHandle, "%-33s : Provider Edge Bridge \r\n",
                       "Bridge Mode");
            break;
        case VLAN_CUSTOMER_BRIDGE_MODE:
            CliPrintf (CliHandle, "%-33s : Customer Bridge \r\n",
                       "Bridge Mode");
            break;
        case VLAN_PBB_ICOMPONENT_BRIDGE_MODE:
            CliPrintf (CliHandle, "%-33s : Provider Backbone I component\r\n",
                       "Bridge Mode");
            break;
        case VLAN_PBB_BCOMPONENT_BRIDGE_MODE:
            CliPrintf (CliHandle, "%-33s : Provider Backbone B component\r\n",
                       "Bridge Mode");
            break;
        default:
            CliPrintf (CliHandle, "%-33s : Not Initialized \r\n",
                       "Bridge Mode");
            break;
    }

    if (i4BaseBridgeMode == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle, "%-33s : Transparent Bridge \r\n",
                   "Base-Bridge Mode");
    }
    else if (i4BaseBridgeMode == DOT_1Q_VLAN_MODE)
    {
        CliPrintf (CliHandle, "%-33s : Vlan Aware Bridge \r\n",
                   "Base-Bridge Mode");
    }

    if (i4TrafficClasses == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s : Enabled \r\n", "Traffic Classes");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled \r\n", "Traffic Classes");
    }

    if (i4VlanLearnMode == VLAN_INDEP_LEARNING)
    {
        CliPrintf (CliHandle,
                   "%-33s : IVL \r\n", "Vlan Operational Learning Mode");
    }
    else if (i4VlanLearnMode == VLAN_SHARED_LEARNING)
    {
        CliPrintf (CliHandle,
                   "%-33s : SVL \r\n", "Vlan Operational Learning Mode");
    }
    else if (i4VlanLearnMode == VLAN_HYBRID_LEARNING)
    {
        CliPrintf (CliHandle,
                   "%-33s : Hybrid \r\n", "Vlan Operational Learning Mode");
    }
    else
    {
        CliPrintf (CliHandle,
                   "%-33s : Unknown \r\n", "Vlan Operational Learning Mode");
    }

    if (i4DefType == VLAN_INDEP_LEARNING)
    {
        CliPrintf (CliHandle,
                   "%-33s : IVL \r\n", "Hybrid Default Learning Mode");
    }
    else if (i4DefType == VLAN_SHARED_LEARNING)
    {
        CliPrintf (CliHandle,
                   "%-33s : SVL \r\n", "Hybrid Default Learning Mode");
    }
    else
    {
        CliPrintf (CliHandle,
                   "%-33s : Unknown \r\n", "Hybrid Default Learning Mode");
    }
    CliPrintf (CliHandle, "%-33s : %d\r\n", "Version number", i4VlanVersion);

    /* In Transparent Bridge mode, VLAN configuration is not supported */

    if (i4BaseBridgeMode != DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle, "%-33s : %d\r\n", "Max Vlan id", i4MaxVlanId);
    }

    CliPrintf (CliHandle,
               "%-33s : %d\r\n", "Max supported vlans", i4MaxSupportedVlans);

    if (i4GlobMacLearnStat == VLAN_ENABLED)
    {
        CliPrintf (CliHandle,
                   "%-33s : Enabled\r\n", "Global mac learning status");
    }
    else if (i4GlobMacLearnStat == VLAN_DISABLED)
    {
        CliPrintf (CliHandle,
                   "%-33s : Disabled\r\n", "Global mac learning status");
    }

    if (i4Status == VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "%-33s : Enabled\r\n", "Filtering Utility Criteria");
    }
    else if (i4Status == VLAN_SNMP_FALSE)
    {
        CliPrintf (CliHandle,
                   "%-33s : Disabled\r\n", "Filtering Utility Criteria");
    }

    /* When u1HwUnicastMacLearningLimitSup is supported,
     * Display unicast mac learning limit
     */
    if (ISS_HW_SUPPORTED ==
        IssGetHwCapabilities (ISS_HW_UNICAST_MAC_LEARNING_LIMIT))
    {
        CliPrintf (CliHandle,
                   "%-33s : %d\r\n", "Unicast mac learning limit",
                   u4SwitchMacLimit);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanForwardAll                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN forwarding information */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanForwardAll (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tPortList          *pAllPorts = NULL;
    tPortList          *pStaticPorts = NULL;
    tPortList          *pForbidPorts = NULL;
    INT4                i4NextContextId = 0;
    INT4                i4CurrentContextId = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PrvIfIndex = 0;
    INT4                i4PortType;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    tSNMP_OCTET_STRING_TYPE FwdAllPorts;
    tSNMP_OCTET_STRING_TYPE FwdAllStaticPorts;
    tSNMP_OCTET_STRING_TYPE FwdAllForbiddenPorts;

    CliPrintf (CliHandle, "\r\nVlan Forward All Table \r\n");

    CliPrintf (CliHandle, "------------------------\r\n\r\n");

    i4CurrentContextId = (INT4) u4ContextId;

    if (nmhGetNextIndexFsDot1qForwardAllStatusTable
        (i4CurrentContextId, &i4NextContextId, 0, &u4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if ((UINT4) i4NextContextId != u4ContextId)
    {
        return CLI_SUCCESS;
    }

    pAllPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pAllPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pStaticPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pStaticPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pAllPorts);
        return CLI_FAILURE;
    }

    pForbidPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbidPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pAllPorts);
        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
        return CLI_FAILURE;
    }

    do
    {
        MEMSET (*pAllPorts, 0, sizeof (tPortList));
        MEMSET (*pStaticPorts, 0, sizeof (tPortList));
        MEMSET (*pForbidPorts, 0, sizeof (tPortList));

        if (VlanGetFirstPortInContext (i4NextContextId,
                                       &u4IfIndex) == VLAN_SUCCESS)
        {
            do
            {
                nmhGetFsDot1qForwardAllPort ((UINT4) i4NextContextId,
                                             u4NextIndex, u4IfIndex,
                                             &i4PortType);

                if (i4PortType == VLAN_ADD_MEMBER_PORT)
                {
                    /* Added for pseudo wire visibility */
                    VLAN_SET_MEMBER_PORT_LIST ((*pAllPorts), u4IfIndex);
                    VLAN_SET_MEMBER_PORT_LIST ((*pStaticPorts), u4IfIndex);
                }
                else if (i4PortType == VLAN_ADD_FORBIDDEN_PORT)
                {
                    /* Added for pseudo wire visibility */
                    VLAN_SET_MEMBER_PORT_LIST ((*pForbidPorts), u4IfIndex);
                }

                nmhGetFsDot1qForwardAllIsLearnt ((UINT4) i4NextContextId,
                                                 u4NextIndex,
                                                 u4IfIndex, &i4PortType);
                if (i4PortType == VLAN_SNMP_TRUE)
                {
                    /* Added for pseudo wire visibility */
                    VLAN_RESET_MEMBER_PORT_LIST ((*pStaticPorts), u4IfIndex);
                }

                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInContext
                   ((UINT4) i4NextContextId, u4PrvIfIndex,
                    &u4IfIndex) == VLAN_SUCCESS);
        }

        CliPrintf (CliHandle, "Vlan ID : %d \r\n", u4NextIndex);

        if (FsUtilBitListIsAllZeros (*pAllPorts,
                                     sizeof (tPortList)) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "ForwardAll Ports          : None \r\n");
        }
        else
        {
            FwdAllPorts.pu1_OctetList = *pAllPorts;
            FwdAllPorts.i4_Length = sizeof (tPortList);

            if (CliOctetToIfName (CliHandle,
                                  "ForwardAll Ports          :",
                                  &FwdAllPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList),
                                  0, &u4PagingStatus, 6) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pAllPorts);
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                return CLI_FAILURE;
            }
        }

        if (FsUtilBitListIsAllZeros (*pStaticPorts,
                                     sizeof (tPortList)) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "ForwardAll Static Ports   : None \r\n");
        }
        else
        {
            FwdAllStaticPorts.pu1_OctetList = *pStaticPorts;
            FwdAllStaticPorts.i4_Length = sizeof (tPortList);

            if (CliOctetToIfName (CliHandle,
                                  "ForwardAll Static Ports   :",
                                  &FwdAllStaticPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList),
                                  0, &u4PagingStatus, 6) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pAllPorts);
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                return CLI_FAILURE;
            }
        }

        if (FsUtilBitListIsAllZeros (*pForbidPorts,
                                     sizeof (tPortList)) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "ForwardAll ForbiddenPorts : None \r\n");
        }
        else
        {
            FwdAllForbiddenPorts.pu1_OctetList = *pForbidPorts;
            FwdAllForbiddenPorts.i4_Length = sizeof (tPortList);

            if (CliOctetToIfName (CliHandle,
                                  "ForwardAll ForbiddenPorts :",
                                  &FwdAllForbiddenPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList),
                                  0, &u4PagingStatus, 6) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pAllPorts);
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                return CLI_FAILURE;
            }
        }

        u4PagingStatus = CliPrintf (CliHandle,
                                    "----------------------------------------------------------\r\n");
        i4CurrentContextId = i4NextContextId;
        u4CurrentIndex = u4NextIndex;

        if (nmhGetNextIndexFsDot1qForwardAllStatusTable
            (i4CurrentContextId, &i4NextContextId,
             u4CurrentIndex, &u4NextIndex) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pAllPorts);
    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanForwardUnreg                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN forwarding un-reg data */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanForwardUnreg (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tPortList          *pAllPorts = NULL;
    tPortList          *pStaticPorts = NULL;
    tPortList          *pForbidPorts = NULL;
    INT4                i4NextContextId = 0;
    INT4                i4CurrentContextId = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PrvIfIndex = 0;
    INT4                i4PortType;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    tSNMP_OCTET_STRING_TYPE FwdUnregPorts;
    tSNMP_OCTET_STRING_TYPE FwdUnregStaticPorts;
    tSNMP_OCTET_STRING_TYPE FwdUnregForbiddenPorts;

    CliPrintf (CliHandle, "\r\nVlan Forward Unregistered Table \r\n");

    CliPrintf (CliHandle, "---------------------------------\r\n\r\n");

    if (nmhGetNextIndexFsDot1qForwardUnregStatusTable
        ((INT4) u4ContextId, &i4NextContextId, 0, &u4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if ((UINT4) i4NextContextId != u4ContextId)
    {
        return CLI_SUCCESS;
    }

    pAllPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pAllPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pStaticPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pStaticPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pAllPorts);
        return CLI_FAILURE;
    }

    pForbidPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbidPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pAllPorts);
        FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
        return CLI_FAILURE;
    }

    do
    {
        MEMSET (*pAllPorts, 0, sizeof (tPortList));
        MEMSET (*pStaticPorts, 0, sizeof (tPortList));
        MEMSET (*pForbidPorts, 0, sizeof (tPortList));

        if (VlanGetFirstPortInContext ((UINT4) i4NextContextId,
                                       &u4IfIndex) == VLAN_SUCCESS)
        {
            do
            {
                nmhGetFsDot1qForwardUnregPort ((UINT4) i4NextContextId,
                                               u4NextIndex,
                                               u4IfIndex, &i4PortType);

                if (i4PortType == VLAN_ADD_MEMBER_PORT)
                {
                    /* Added for pseudo wire visibility */
                    VLAN_SET_MEMBER_PORT_LIST ((*pAllPorts), u4IfIndex);
                    VLAN_SET_MEMBER_PORT_LIST ((*pStaticPorts), u4IfIndex);
                }
                else if (i4PortType == VLAN_ADD_FORBIDDEN_PORT)
                {
                    /* Added for pseudo wire visibility */
                    VLAN_SET_MEMBER_PORT_LIST ((*pForbidPorts), u4IfIndex);
                }

                nmhGetFsDot1qForwardUnregIsLearnt ((UINT4) i4NextContextId,
                                                   u4NextIndex, u4IfIndex,
                                                   &i4PortType);
                if (i4PortType == VLAN_SNMP_TRUE)
                {
                    VLAN_RESET_MEMBER_PORT_LIST ((*pStaticPorts), u4IfIndex);
                }

                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInContext
                   ((UINT4) i4NextContextId, u4PrvIfIndex,
                    &u4IfIndex) == VLAN_SUCCESS);
        }
        CliPrintf (CliHandle, "Vlan ID : %d \r\n", u4NextIndex);

        if (FsUtilBitListIsAllZeros (*pAllPorts,
                                     sizeof (tPortList)) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "Unreg ports           : None \r\n");
        }
        else
        {
            FwdUnregPorts.pu1_OctetList = *pAllPorts;
            FwdUnregPorts.i4_Length = sizeof (tPortList);

            if (CliOctetToIfName (CliHandle,
                                  "Unreg ports           :",
                                  &FwdUnregPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList),
                                  0, &u4PagingStatus, 6) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pAllPorts);
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                return CLI_FAILURE;
            }
        }

        if (FsUtilBitListIsAllZeros (*pStaticPorts,
                                     sizeof (tPortList)) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "Unreg Static Ports    : None \r\n");
        }
        else
        {
            FwdUnregStaticPorts.pu1_OctetList = *pStaticPorts;
            FwdUnregStaticPorts.i4_Length = sizeof (tPortList);

            if (CliOctetToIfName (CliHandle,
                                  "Unreg Static Ports    :",
                                  &FwdUnregStaticPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList),
                                  0, &u4PagingStatus, 6) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pAllPorts);
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                return CLI_FAILURE;
            }
        }

        if (FsUtilBitListIsAllZeros (*pForbidPorts,
                                     sizeof (tPortList)) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "Unreg Forbidden Ports : None \r\n");
        }
        else
        {
            FwdUnregForbiddenPorts.pu1_OctetList = *pForbidPorts;
            FwdUnregForbiddenPorts.i4_Length = sizeof (tPortList);

            if (CliOctetToIfName (CliHandle,
                                  "Unreg Forbidden Ports :",
                                  &FwdUnregForbiddenPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList),
                                  0, &u4PagingStatus, 6) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pAllPorts);
                FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                return CLI_FAILURE;
            }
        }

        u4PagingStatus = CliPrintf (CliHandle,
                                    "------------------------------------------------------\r\n");
        i4CurrentContextId = i4NextContextId;
        u4CurrentIndex = u4NextIndex;

        if (nmhGetNextIndexFsDot1qForwardUnregStatusTable
            (i4CurrentContextId, &i4NextContextId,
             u4CurrentIndex, &u4NextIndex) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pAllPorts);
    FsUtilReleaseBitList ((UINT1 *) pStaticPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanTrafficClasses                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN traffic class          */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : u4PortId - Port identifier                         */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanTrafficClasses (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4NextPort;
    INT4                i4CurrentPort;
    UINT4               u4CurrentPriority;
    INT4                i4NextPriority;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetVal;
    INT4                i4TraffClass;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    VLAN_MEMSET (&CfaIfInfo, VLAN_INIT_VAL, sizeof (tCfaIfInfo));

    ShowVlanMaxTrafficClasses (CliHandle, u4ContextId, u4PortId);

    CliPrintf (CliHandle, "\r\nTraffic Class table \r\n");

    CliPrintf (CliHandle, "--------------------- \r\n");

    CliPrintf (CliHandle, "Port     Priority    Traffic Class \r\n");

    CliPrintf (CliHandle, "-----    ---------   -------------\r\n");

    if (u4PortId == 0)
    {
        /* Show for all the Ports */
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            CliPrintf (CliHandle, " Invalid Virtual Switch \r\n");
            return CLI_FAILURE;
        }
        i4RetVal
            = VlanGetNextTrafficClassTableIndex (0, &i4NextPort,
                                                 0, &i4NextPriority);
        VlanReleaseContext ();
    }
    else
    {
        /* 
         * Show for a specific Port. 
         *
         * Setting the Current indices to the values to the previous 
         * element such that the Get Next operation will fetch the next 
         * element in the lexicographic order.
         */
        i4CurrentPort = u4PortId;
        u4CurrentPriority = 0;
        if (nmhGetFsDot1dTrafficClass (i4CurrentPort, u4CurrentPriority,
                                       &i4TraffClass) == SNMP_SUCCESS)
        {

            VlanCfaCliGetIfName (i4CurrentPort, (INT1 *) au1NameStr);

            CliPrintf (CliHandle, "%-4s    ", au1NameStr);

            CliPrintf (CliHandle, "%-8d    ", u4CurrentPriority);

            CliPrintf (CliHandle, "%d\r\n", i4TraffClass);
        }

        i4RetVal = nmhGetNextIndexFsDot1dTrafficClassTable
            (i4CurrentPort, &i4NextPort, u4CurrentPriority, &i4NextPriority);

        /* to Convert SNMP_SUCCESS/SNMP_FAILURE to VLAN_SUCCESS/VLAN_FAILURE */
        i4RetVal = (i4RetVal == SNMP_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE;
    }

    if (i4RetVal == VLAN_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4PortId != 0)
        {
            /* Show for a specific Port */
            if ((UINT4) i4NextPort > u4PortId)
            {
                /* 
                 * Done with the show of the required port and hence
                 * terminating the loop.
                 */
                break;
            }
        }

        if (VlanCfaGetIfInfo (i4NextPort, &CfaIfInfo) == CFA_SUCCESS)
        {
            VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
            nmhGetFsDot1dTrafficClass (i4NextPort, (UINT4) i4NextPriority,
                                       &i4TraffClass);

            CliPrintf (CliHandle, "%-4s    ", au1NameStr);

            CliPrintf (CliHandle, "%-8d    ", i4NextPriority);

            u4PagingStatus = CliPrintf (CliHandle, "%d\r\n", i4TraffClass);
        }

        i4CurrentPort = i4NextPort;
        u4CurrentPriority = (UINT4) i4NextPriority;

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u1isShowAll = FALSE;
        }
        else
        {
            if (VlanGetNextTrafficClassTableIndex
                (i4CurrentPort, &i4NextPort,
                 u4CurrentPriority, &i4NextPriority) == VLAN_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        VlanReleaseContext ();

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, exit */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanMaxTrafficClasses                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN Max traffic class      */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : u4PortId - Port identifier                         */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanMaxTrafficClasses (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4PortId)
{
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4Dot1dPortNumTrafficClasses = 0;
    INT4                i4NextPriority = 0;
    INT4                i4RetVal = 0;
    UINT4               u4LocalPortId = 0;
    UINT4               u4CurrentPriority = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT2               u2LocalPortId = 0;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    CLI_MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (u4PortId != 0)
    {
        VcmGetContextInfoFromIfIndex (u4PortId, &u4ContextId, &u2LocalPortId);
        u4LocalPortId = (UINT4) u2LocalPortId;
    }
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, " Invalid Virtual Switch \r\n");
        return CLI_FAILURE;
    }

    /* Show for all the Ports */
    CliPrintf (CliHandle, "\r\nMax Vlan Traffic Class table \r\n");

    CliPrintf (CliHandle, "-------------------------- \r\n");

    CliPrintf (CliHandle, "Port     Max Traffic Class \r\n");

    CliPrintf (CliHandle, "-----    ------------------\r\n");

    if (u4PortId != 0)
    {
        if (nmhValidateIndexInstanceDot1qPortVlanTable (u4LocalPortId) ==
            SNMP_SUCCESS)
        {
            nmhGetDot1dPortNumTrafficClasses (u4LocalPortId,
                                              &i4Dot1dPortNumTrafficClasses);
            VlanCfaCliGetIfName (u4PortId, (INT1 *) au1NameStr);

            CliPrintf (CliHandle, "%-4s    ", au1NameStr);

            CliPrintf (CliHandle, "%d\r\n", i4Dot1dPortNumTrafficClasses);

            VlanReleaseContext ();
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Invalid Interface.\r\n");
            VlanReleaseContext ();
            return CLI_FAILURE;
        }
    }
    else
    {
        /* 
         * Setting the Current indices to the values to the previous 
         * element such that the Get Next operation will fetch the next 
         * element in the lexicographic order.
         */
        i4RetVal
            = VlanGetNextTrafficClassTableIndex (0, &i4NextPort,
                                                 0, &i4NextPriority);
        VlanReleaseContext ();
    }

    if (i4RetVal == VLAN_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u1isShowAll = FALSE;
        }
        else
        {
            if (i4CurrentPort != i4NextPort && i4NextPort != 0)
            {
                VcmGetContextInfoFromIfIndex
                    (i4NextPort, &u4ContextId, &u2LocalPortId);
                u4LocalPortId = (UINT4) u2LocalPortId;
                i4Dot1dPortNumTrafficClasses = 0;
                nmhGetDot1dPortNumTrafficClasses (u4LocalPortId,
                                                  &i4Dot1dPortNumTrafficClasses);

                VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);

                CliPrintf (CliHandle, "%-4s    ", au1NameStr);

                CliPrintf (CliHandle, "%d\r\n", i4Dot1dPortNumTrafficClasses);
            }

            i4CurrentPort = i4NextPort;
            u4CurrentPriority = (UINT4) i4NextPriority;

            if (VlanGetNextTrafficClassTableIndex
                (i4CurrentPort, &i4NextPort,
                 u4CurrentPriority, &i4NextPriority) == VLAN_FAILURE)
            {
                u1isShowAll = FALSE;
            }
            VlanReleaseContext ();

        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, exit */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanPortConfig                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN PORT config information*/
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                        u4PortId - Port identifier                         */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanPortConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId)
{
    INT4                i4Pvid = 0;
    INT4                i4PortAcceptableFrame = 0;
    INT4                i4IngressFiltering = 0;
    INT4                i4MacLearningStatus = 0;
    INT4                i4VlanPortType = 0;
#ifdef GARP_WANTED
    INT4                i4GvrpStatus = 0;
    INT4                i4GmrpStatus = 0;
    INT4                i4GvrpFailedReg = 0;
    INT4                i4RestrictedVlanReg = 0;
    INT4                i4RestrictedGroupReg = 0;
    tMacAddr            MacAddress;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
#endif
    INT4                i4PortProtected = 0;
    INT4                i4MacBasedClassification = 0;
    INT4                i4SubnetBasedClassification = VLAN_INIT_VAL;
    INT4                i4PortProtoBasedClassification = 0;
    INT4                i4FilteringCriteria = 0;
    INT4                i4UserPriority = 0;
    INT4                i4IngressEhterType = 0;
    INT4                i4EgressEhterType = 0;
    INT4                i4EgressTPIDType = 0;
    INT4                i4AllowableTPID1 = 0;
    INT4                i4AllowableTPID2 = 0;
    INT4                i4AllowableTPID3 = 0;
    INT4                i4ReflectionStatus = 0;
    UINT1               u1isShowAll = TRUE;
    INT4                i4NextIndex = 0;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IfBrgPortType[CFA_MAX_BRG_PORT_TYPE_LEN];

    MEMSET (au1IfBrgPortType, 0, CFA_MAX_BRG_PORT_TYPE_LEN);

    if (VlanL2IwfIsPortInPortChannel (u4PortId) == L2IWF_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Port is part of port channel\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nVlan Port configuration table\r\n");

    CliPrintf (CliHandle, "-------------------------------\r\n");

    if (VlanGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextIndex)
        == VLAN_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4PortId != 0)
        {
            if (nmhValidateIndexInstanceFsDot1qPortVlanTable
                ((INT4) u4PortId) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4NextIndex = (INT4) u4PortId;
            u1isShowAll = FALSE;
        }
        VlanCfaGetInterfaceBrgPortTypeString ((UINT4) i4NextIndex,
                                              au1IfBrgPortType);
        VlanCfaCliGetIfName (i4NextIndex, (INT1 *) au1NameStr);
        nmhGetFsDot1qPvid (i4NextIndex, (UINT4 *) &i4Pvid);
        nmhGetFsDot1qPortAcceptableFrameTypes (i4NextIndex,
                                               &i4PortAcceptableFrame);
        nmhGetFsMIDot1qFutureVlanPortUnicastMacLearning (i4NextIndex,
                                                         &i4MacLearningStatus);
        nmhGetFsDot1qPortIngressFiltering (i4NextIndex, &i4IngressFiltering);
        nmhGetFsMIDot1qFutureVlanPortType (i4NextIndex, &i4VlanPortType);
        nmhGetFsMIDot1qFutureVlanPortMacBasedClassification
            (i4NextIndex, &i4MacBasedClassification);
        nmhGetFsMIDot1qFutureVlanPortSubnetBasedClassification
            (i4NextIndex, &i4SubnetBasedClassification);
        nmhGetFsMIDot1qFutureVlanPortPortProtoBasedClassification
            (i4NextIndex, &i4PortProtoBasedClassification);
        nmhGetFsMIDot1qFutureVlanFilteringUtilityCriteria
            (i4NextIndex, &i4FilteringCriteria);
        nmhGetFsDot1dPortDefaultUserPriority (i4NextIndex, &i4UserPriority);
        nmhGetFsMIDot1qFutureVlanPortProtected (i4NextIndex, &i4PortProtected);
        nmhGetFsMIDot1qFutureVlanPortIngressEtherType (i4NextIndex,
                                                       &i4IngressEhterType);
        nmhGetFsMIDot1qFutureVlanPortEgressEtherType (i4NextIndex,
                                                      &i4EgressEhterType);
        nmhGetFsMIDot1qFutureVlanPortEgressTPIDType (i4NextIndex,
                                                     &i4EgressTPIDType);
        nmhGetFsMIDot1qFutureVlanPortAllowableTPID1 (i4NextIndex,
                                                     &i4AllowableTPID1);
        nmhGetFsMIDot1qFutureVlanPortAllowableTPID2 (i4NextIndex,
                                                     &i4AllowableTPID2);
        nmhGetFsMIDot1qFutureVlanPortAllowableTPID3 (i4NextIndex,
                                                     &i4AllowableTPID3);
        nmhGetFsMIDot1qFuturePortPacketReflectionStatus (i4NextIndex,
                                                         &i4ReflectionStatus);
        /*
         * There should not be any nested locks taken. Hence
         * release the VLAN Lock before taking ISS Lock.
         */
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);

#ifdef GARP_WANTED
        GARP_LOCK ();

        nmhGetFsDot1qPortGvrpStatus (i4NextIndex, &i4GvrpStatus);
        nmhGetFsDot1dPortGmrpStatus (i4NextIndex, &i4GmrpStatus);
        nmhGetFsDot1qPortGvrpFailedRegistrations
            (i4NextIndex, (UINT4 *) &i4GvrpFailedReg);
        nmhGetFsDot1qPortGvrpLastPduOrigin (i4NextIndex, &MacAddress);
        nmhGetFsDot1qPortRestrictedVlanRegistration
            (i4NextIndex, &i4RestrictedVlanReg);
        nmhGetFsDot1dPortRestrictedGroupRegistration
            (i4NextIndex, &i4RestrictedGroupReg);

        GARP_UNLOCK ();
#endif

        CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
        VLAN_LOCK ();
        CliPrintf (CliHandle, "Port %s\r\n", au1NameStr);

        CliPrintf (CliHandle, " %-35s : %s\r\n", "Bridge Port Type",
                   au1IfBrgPortType);
        CliPrintf (CliHandle, " %-35s : %d\r\n", "Port Vlan ID", i4Pvid);

        if (i4PortAcceptableFrame == VLAN_ADMIT_ALL_FRAMES)
        {
            CliPrintf (CliHandle,
                       " %-35s : Admit All\r\n", "Port Acceptable Frame Type");
        }
        else if (i4PortAcceptableFrame == VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
        {
            CliPrintf (CliHandle,
                       " %-35s : Admit Only Vlan Tagged\r\n",
                       "Port Acceptable Frame Type");
        }
        else if (i4PortAcceptableFrame ==
                 VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            CliPrintf (CliHandle,
                       " %-35s : Admit Only Untagged and Priority Tagged\r\n",
                       "Port Acceptable Frame Type");
        }
        else
        {
            CliPrintf (CliHandle,
                       " %-35s : Unknown\r\n", "Port Acceptable Frame Type");
        }
        if (i4MacLearningStatus == VLAN_PORT_MAC_LEARNING_ENABLED)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n",
                       "Port Mac Learning Status");
        }
        else if (i4MacLearningStatus == VLAN_PORT_MAC_LEARNING_DISABLED)
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n",
                       "Port Mac Learning Status");
        }

        if (i4IngressFiltering == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n",
                       "Port Ingress Filtering");
        }
        else if (i4IngressFiltering == VLAN_SNMP_FALSE)
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n",
                       "Port Ingress Filtering");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Unknown\r\n",
                       "Port Ingress Filtering");
        }

        if (i4VlanPortType == VLAN_ACCESS_PORT)
        {
            CliPrintf (CliHandle, " %-35s : Access\r\n",
                       "Port Mode             ");
        }
        else if (i4VlanPortType == VLAN_HYBRID_PORT)
        {
            CliPrintf (CliHandle, " %-35s : Hybrid\r\n",
                       "Port Mode             ");
        }
        else if (i4VlanPortType == VLAN_TRUNK_PORT)
        {
            CliPrintf (CliHandle, " %-35s : Trunk\r\n",
                       "Port Mode             ");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Unknown\r\n",
                       "Port Mode             ");
        }

#ifdef GARP_WANTED
        if (i4GvrpStatus == GVRP_ENABLED)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n", "Port Gvrp Status");
        }
        else if (i4GvrpStatus == GVRP_DISABLED)
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n", "Port Gvrp Status");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Unknown\r\n", "Port Gvrp Status");
        }

        if (i4GmrpStatus == GMRP_ENABLED)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n", "Port Gmrp Status");
        }
        else if (i4GmrpStatus == GMRP_DISABLED)
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n", "Port Gmrp Status");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Unknown\r\n", "Port Gmrp Status");
        }

        CliPrintf (CliHandle, " %-35s : %d\r\n",
                   "Port Gvrp Failed Registrations", i4GvrpFailedReg);

        PrintMacAddress (MacAddress, au1String);

        CliPrintf (CliHandle, " %-35s : %-17s\r\n", "Gvrp last pdu origin",
                   au1String);

        if (i4RestrictedVlanReg == GARP_ENABLED)
        {
            CliPrintf (CliHandle,
                       " %-35s : Enabled\r\n",
                       "Port Restricted Vlan Registration");
        }
        else if (i4RestrictedVlanReg == GARP_DISABLED)
        {
            CliPrintf (CliHandle,
                       " %-35s : Disabled\r\n",
                       "Port Restricted Vlan Registration");
        }
        else
        {
            CliPrintf (CliHandle,
                       " %-35s : Unknown\r\n",
                       "Port Restricted Vlan Registration");
        }

        if (i4RestrictedGroupReg == GARP_ENABLED)
        {
            CliPrintf (CliHandle,
                       " %-35s : Enabled\r\n",
                       "Port Restricted Group Registration");
        }
        else if (i4RestrictedGroupReg == GARP_DISABLED)
        {
            CliPrintf (CliHandle,
                       " %-35s : Disabled\r\n",
                       "Port Restricted Group Registration");
        }
        else
        {
            CliPrintf (CliHandle,
                       " %-35s : Unknown\r\n",
                       "Port Restricted Group Registration");
        }
#endif

        if (i4MacBasedClassification == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n", "Mac Based Support");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n", "Mac Based Support");
        }

        if (i4SubnetBasedClassification == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n",
                       "Subnet Based Support");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n",
                       "Subnet Based Support");
        }

        if (i4PortProtoBasedClassification == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n",
                       "Port-and-Protocol Based Support");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n",
                       "Port-and-Protocol Based Support");
        }

        CliPrintf (CliHandle, " %-35s : %d\r\n", "Default Priority",
                   i4UserPriority);

        VlanShowProtocolTunnelStatus (CliHandle, i4NextIndex);

        if (i4FilteringCriteria == VLAN_DEFAULT_FILTERING_CRITERIA)
        {
            CliPrintf (CliHandle, " %-35s : Default\r\n",
                       "Filtering Utility Criteria");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Enhanced\r\n",
                       "Filtering Utility Criteria");
        }

        if (i4PortProtected == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n",
                       "Port Protected Status");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n",
                       "Port Protected Status");
        }
        CliPrintf (CliHandle, " %-35s : 0x%x\r\n", "Ingress EtherType",
                   i4IngressEhterType);
        CliPrintf (CliHandle, " %-35s : 0x%x\r\n", "Egress EtherType",
                   i4EgressEhterType);
        if (i4EgressTPIDType == VLAN_PORT_EGRESS_PORTBASED)
        {
            CliPrintf (CliHandle, " %-35s : %s\r\n", "Egress TPID Type",
                       "Portbased");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : %s\r\n", "Egress TPID Type",
                       "Vlanbased");
        }
        CliPrintf (CliHandle, " %-35s : 0x%x\r\n", "Allowable TPID 1",
                   i4AllowableTPID1);
        CliPrintf (CliHandle, " %-35s : 0x%x\r\n", "Allowable TPID 2",
                   i4AllowableTPID2);
        CliPrintf (CliHandle, " %-35s : 0x%x\r\n", "Allowable TPID 3",
                   i4AllowableTPID3);

        if (i4ReflectionStatus == VLAN_ENABLED)
        {

            CliPrintf (CliHandle, " %-35s : Enabled\r\n",
                       "Reflection Status             ");

        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n",
                       "Reflection Status             ");
        }

        u4PagingStatus = CliPrintf (CliHandle,
                                    " -------------------------------------------------------\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }

        u4CurrentIndex = i4NextIndex;

        if (VlanGetNextPortInContext (u4ContextId, u4CurrentIndex,
                                      (UINT4 *) &i4NextIndex) == VLAN_FAILURE)
        {
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanProtocolGroup                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from VLAN protocol  */
/*                        group database                                     */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier.                  */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanProtocolGroup (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4RetVal = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    tSNMP_OCTET_STRING_TYPE *pNextOctetStringType;
    tSNMP_OCTET_STRING_TYPE *pCurrentOctetStringType;
    INT4                i4CurrentContextId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextFrameType = 0;
    INT4                i4CurrentFrameType = 0;
    UINT1               u1Protocol;
    tSNMP_OCTET_STRING_TYPE CurrSnmpOctetStr;
    UINT1               au1CurrOctetList[VLAN_5BYTE_PROTO_TEMP_SIZE];
    tSNMP_OCTET_STRING_TYPE NextSnmpOctetStr;
    UINT1               au1NextOctetList[VLAN_5BYTE_PROTO_TEMP_SIZE];
    UINT1               au1ProtoOctetList[VLAN_CLI_MAX_PROTO_SIZE];

    CLI_MEMSET (au1CurrOctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);
    CLI_MEMSET (au1NextOctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);

    NextSnmpOctetStr.pu1_OctetList = au1NextOctetList;
    CurrSnmpOctetStr.pu1_OctetList = au1CurrOctetList;

    pNextOctetStringType = &NextSnmpOctetStr;
    pNextOctetStringType->i4_Length = VLAN_5BYTE_PROTO_TEMP_SIZE;
    CLI_MEMSET (pNextOctetStringType->pu1_OctetList, 0,
                VLAN_5BYTE_PROTO_TEMP_SIZE);

    pCurrentOctetStringType = &CurrSnmpOctetStr;
    pCurrentOctetStringType->i4_Length = VLAN_5BYTE_PROTO_TEMP_SIZE;
    CLI_MEMSET (pCurrentOctetStringType->pu1_OctetList, 0,
                VLAN_5BYTE_PROTO_TEMP_SIZE);

    CliPrintf (CliHandle, "\r\nProtocol Group Table \r\n");

    CliPrintf (CliHandle, "--------------------\r\n");
    CliPrintf (CliHandle, "------------------------------------------\r\n");

    CliPrintf (CliHandle, "Frame Type      ");
    CliPrintf (CliHandle, "Protocol             ");
    CliPrintf (CliHandle, "Group\r\n");
    CliPrintf (CliHandle, "------------------------------------------\r\n");

    i4CurrentContextId = (INT4) u4ContextId;

    if (nmhGetNextIndexFsDot1vProtocolGroupTable
        ((INT4) u4ContextId, &i4NextContextId, 0, &i4NextFrameType,
         pCurrentOctetStringType, pNextOctetStringType) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if (i4CurrentContextId != i4NextContextId)
    {
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsDot1vProtocolGroupId (i4NextContextId, i4NextFrameType,
                                      pNextOctetStringType, &i4RetVal);

        CliPrintf (CliHandle, "%-13s", gau1VlanFrmTypeName[i4NextFrameType]);

        if (VlanUtilGetProtocol (&u1Protocol,
                                 pNextOctetStringType->pu1_OctetList,
                                 (UINT1) pNextOctetStringType->i4_Length) !=
            CLI_SUCCESS)
        {
            CLI_MEMSET (au1ProtoOctetList, 0, VLAN_CLI_MAX_PROTO_SIZE);
            CliOctetToStr (pNextOctetStringType->pu1_OctetList,
                           pNextOctetStringType->i4_Length,
                           au1ProtoOctetList, VLAN_CLI_MAX_PROTO_SIZE);
            if (pNextOctetStringType->i4_Length == VLAN_2BYTE_PROTO_TEMP_SIZE)
            {
                CliPrintf (CliHandle, "   %-19s", au1ProtoOctetList);
            }
            else
            {
                CliPrintf (CliHandle, "   %-19s", au1ProtoOctetList);
            }
        }
        else
        {
            CliPrintf (CliHandle, "   %-19s", gau1VlanProtocolName[u1Protocol]);
        }

        u4PagingStatus = CliPrintf (CliHandle, "%-10d\r\n", i4RetVal);

        pCurrentOctetStringType = &CurrSnmpOctetStr;
        pCurrentOctetStringType->i4_Length = pNextOctetStringType->i4_Length;

        CLI_MEMSET (pCurrentOctetStringType->pu1_OctetList, 0,
                    VLAN_5BYTE_PROTO_TEMP_SIZE);

        CLI_MEMCPY (pCurrentOctetStringType->pu1_OctetList,
                    pNextOctetStringType->pu1_OctetList,
                    pNextOctetStringType->i4_Length);

        i4CurrentContextId = i4NextContextId;
        i4CurrentFrameType = i4NextFrameType;

        if (nmhGetNextIndexFsDot1vProtocolGroupTable
            (i4CurrentContextId, &i4NextContextId,
             i4CurrentFrameType, &i4NextFrameType,
             pCurrentOctetStringType, pNextOctetStringType) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
            CliPrintf (CliHandle,
                       "------------------------------------------\r\n");
        }

        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanProtocolDatabase                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from protocol VLAN  */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanProtocolDatabase (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4RetVal = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4NextGroupId = 0;
    INT4                i4CurrentGroupId = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    CliPrintf (CliHandle, "\r\nPort Protocol Table \r\n");

    CliPrintf (CliHandle, "--------------------------------------\r\n");

    CliPrintf (CliHandle, "Port           ");
    CliPrintf (CliHandle, "Group          ");
    CliPrintf (CliHandle, "Vlan ID        \r\n");
    CliPrintf (CliHandle, "--------------------------------------\r\n");

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    if (VlanGetNextProtocolPortTableIndex (0, &i4NextPort, 0, &i4NextGroupId)
        == VLAN_FAILURE)
    {
        VlanReleaseContext ();
        return CLI_SUCCESS;
    }
    VlanReleaseContext ();

    do
    {
        VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);

        nmhGetFsDot1vProtocolPortGroupVid (i4NextPort, i4NextGroupId,
                                           &i4RetVal);

        CliPrintf (CliHandle, "%-15s", au1NameStr);

        CliPrintf (CliHandle, "%-15d", i4NextGroupId);

        u4PagingStatus = CliPrintf (CliHandle, "%-10d\r\n", i4RetVal);

        i4CurrentPort = i4NextPort;
        i4CurrentGroupId = i4NextGroupId;

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            u1isShowAll = FALSE;
        }
        else
        {
            if (VlanGetNextProtocolPortTableIndex
                (i4CurrentPort, &i4NextPort, i4CurrentGroupId, &i4NextGroupId)
                == VLAN_FAILURE)
            {
                u1isShowAll = FALSE;
                u4PagingStatus =
                    CliPrintf (CliHandle,
                               "--------------------------------------\r\n");
            }
        }
        VlanReleaseContext ();

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanSetMcastBcastTrafficStatus                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the option for discarding       */
/*                        the multicast and broadcast packets                */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSetMcastBcastTrafficStatus (UINT4 u4Port, UINT1 *pu1MacAddr,
                                INT4 i4DiscardOption)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption
        (&u4ErrorCode, (INT4) u4Port, pu1MacAddr,
         i4DiscardOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortMacMapMcastBcastOption
        ((INT4) u4Port, pu1MacAddr, i4DiscardOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanSetARPOption                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the option for discarding       */
/*                        the ARP packets                                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSetARPOption (UINT4 u4Port, UINT4 u4SubnetAddr, UINT4 u4SubnetMask,
                  INT4 i4Option)
{

    UINT4               u4ErrorCode = VLAN_INIT_VAL;

    if (nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption (&u4ErrorCode,
                                                           (INT4) u4Port,
                                                           u4SubnetAddr,
                                                           u4SubnetMask,
                                                           i4Option)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortSubnetMapExtARPOption ((INT4) u4Port,
                                                        u4SubnetAddr,
                                                        u4SubnetMask,
                                                        i4Option)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanMacDatabase                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from MAC VLAN       */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanMacDatabase (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT4               u4IfIndex = 0;
    UINT4               u4PrvIfIndex = 0;

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            if (ShowVlanMacDatabaseOnPort (CliHandle, u4IfIndex) == CLI_FAILURE)
            {
                break;
            }
            u4PrvIfIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext (u4ContextId, u4PrvIfIndex, &u4IfIndex)
               == VLAN_SUCCESS);
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanMacDatabaseOnPort                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from MAC VLAN       */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4IfIndex - Physical interface for which the       */
/*                                    entries are to be displayed            */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanMacDatabaseOnPort (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT4                i4NextIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tMacAddr            MacMapAddr;
    tMacAddr            NextMacMapAddr;
    INT4                i4MacMapVlanId = 0;
    INT4                i4MacVlanStatus = 0;
    INT4                i4MacMapBcastOption = 0;
    INT4                i4MacMapRowStatus = 0;
    CHR1               *au1Status[] = { "allow", "discard" };

    MEMSET (MacMapAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacMapAddr, 0, sizeof (tMacAddr));

    if (nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable
        ((INT4) u4IfIndex, &i4NextIndex, MacMapAddr,
         (tMacAddr *) & NextMacMapAddr) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (u4IfIndex != (UINT4) i4NextIndex)
    {
        return CLI_SUCCESS;
    }

    nmhGetFsMIDot1qFutureVlanPortMacBasedClassification (u4IfIndex,
                                                         &i4MacVlanStatus);
    if (i4MacVlanStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nMac Map Table For Port %ld--Mac Vlan Enabled\r\n",
                   u4IfIndex);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nMac Map Table For Port %ld--Mac Vlan Disabled\r\n",
                   u4IfIndex);
    }

    CliPrintf (CliHandle, "-------------------------- \r\n\n");
    CliPrintf (CliHandle, "Mac Address       Vlan ID          MCast/Bcast\r\n");
    CliPrintf (CliHandle, "-----------       -------          -----------\r\n");

    do
    {

        nmhGetFsMIDot1qFutureVlanPortMacMapRowStatus (i4NextIndex,
                                                      NextMacMapAddr,
                                                      &i4MacMapRowStatus);
        if (i4MacMapRowStatus == VLAN_ACTIVE)
        {
            nmhGetFsMIDot1qFutureVlanPortMacMapVid (i4NextIndex, NextMacMapAddr,
                                                    &i4MacMapVlanId);

            nmhGetFsMIDot1qFutureVlanPortMacMapMcastBcastOption (i4NextIndex,
                                                                 NextMacMapAddr,
                                                                 &i4MacMapBcastOption);
            PrintMacAddress (NextMacMapAddr, au1String);
            CliPrintf (CliHandle, "%-20s", au1String);

            CliPrintf (CliHandle, "%-15ld", i4MacMapVlanId);
            u4PagingStatus = CliPrintf (CliHandle, "%-15s\r\n",
                                        au1Status[i4MacMapBcastOption - 1]);
        }

        MEMCPY (MacMapAddr, NextMacMapAddr, sizeof (tMacAddr));
        u4IfIndex = (UINT4) i4NextIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            return CLI_FAILURE;
        }
        if (nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable ((INT4) u4IfIndex,
                                                               &i4NextIndex,
                                                               MacMapAddr,
                                                               (tMacAddr *) &
                                                               NextMacMapAddr)
            == SNMP_FAILURE)
        {
            break;
        }
    }
    while (u4IfIndex == (UINT4) i4NextIndex);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanSubnetDatabase                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from Subnet VLAN    */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanSubnetDatabase (tCliHandle CliHandle, UINT4 u4ContextId)
{

#ifndef BCMX_WANTED
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    UINT4               u4PrvIfIndex = VLAN_INIT_VAL;
#endif

#ifdef BCMX_WANTED
    UNUSED_PARAM (u4ContextId);

    if (ShowAllVlanSubnetDatabase (CliHandle) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
#else
    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            if (ShowVlanSubnetDatabaseOnPort (CliHandle, u4IfIndex) ==
                CLI_FAILURE)
            {
                break;
            }
            u4PrvIfIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext (u4ContextId, u4PrvIfIndex, &u4IfIndex)
               == VLAN_SUCCESS);
    }
#endif
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanSubnetDatabaseOnPort                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from Subnet VLAN    */
/*                        database for a particular port                     */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4IfIndex - Physical interface for which the       */
/*                                    entries are to be displayed            */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanSubnetDatabaseOnPort (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT4                i4NextIndex = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pString = NULL;
    UINT4               u4SubnetMapAddr = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMapAddr = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMapMask = VLAN_INIT_VAL;
    UINT4               u4SubnetMapMask = VLAN_INIT_VAL;
    INT4                i4SubnetMapVlanId = VLAN_INIT_VAL;
    INT4                i4SubnetVlanStatus = VLAN_INIT_VAL;
    INT4                i4SubnetMapArpOption = VLAN_INIT_VAL;
    INT4                i4SubnetMapRowStatus = VLAN_INIT_VAL;
    CHR1               *au1Status[] = { "allow", "suppress" };

    if (nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
        ((INT4) u4IfIndex, &i4NextIndex, u4SubnetMapAddr,
         &u4NextSubnetMapAddr, u4SubnetMapMask, &u4NextSubnetMapMask)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (u4IfIndex != (UINT4) i4NextIndex)
    {
        return CLI_SUCCESS;
    }

    nmhGetFsMIDot1qFutureVlanPortSubnetBasedClassification (u4IfIndex,
                                                            &i4SubnetVlanStatus);
    if (i4SubnetVlanStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nSubnet Map Table For Port %ld--Subnet Vlan Enabled\r\n",
                   u4IfIndex);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nSubnet Map Table For Port %ld--Subnet Vlan Disabled\r\n",
                   u4IfIndex);
    }

    CliPrintf (CliHandle,
               "-------------------------------------------------\r\n");
    CliPrintf (CliHandle,
               "Source IP      Subnet Mask      Vlan ID        ARP Traffic\r\n");
    CliPrintf (CliHandle,
               "----------------------------------------------------------\r\n");

    do
    {
        nmhGetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus (i4NextIndex,
                                                            u4NextSubnetMapAddr,
                                                            u4NextSubnetMapMask,
                                                            &i4SubnetMapRowStatus);
        if (i4SubnetMapRowStatus == VLAN_ACTIVE)
        {
            nmhGetFsMIDot1qFutureVlanPortSubnetMapExtVid (i4NextIndex,
                                                          u4NextSubnetMapAddr,
                                                          u4NextSubnetMapMask,
                                                          &i4SubnetMapVlanId);

            nmhGetFsMIDot1qFutureVlanPortSubnetMapExtARPOption (i4NextIndex,
                                                                u4NextSubnetMapAddr,
                                                                u4NextSubnetMapMask,
                                                                &i4SubnetMapArpOption);

            CLI_CONVERT_IPADDR_TO_STR (pString, u4NextSubnetMapAddr);
            CliPrintf (CliHandle, "%-15s", pString);

            CLI_CONVERT_IPADDR_TO_STR (pString, u4NextSubnetMapMask);
            CliPrintf (CliHandle, "%-20s", pString);

            CliPrintf (CliHandle, "%-15ld", i4SubnetMapVlanId);
            u4PagingStatus = CliPrintf (CliHandle, "%-15s\r\n",
                                        au1Status[i4SubnetMapArpOption - 1]);
        }

        u4SubnetMapAddr = u4NextSubnetMapAddr;
        u4SubnetMapMask = u4NextSubnetMapMask;

        u4IfIndex = (UINT4) i4NextIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            return CLI_FAILURE;
        }
        if (nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
            ((INT4) u4IfIndex, &i4NextIndex, u4SubnetMapAddr,
             &u4NextSubnetMapAddr, u4SubnetMapMask, &u4NextSubnetMapMask)
            == SNMP_FAILURE)
        {
            break;
        }
    }
    while (u4IfIndex == (UINT4) i4NextIndex);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowAllVlanSubnetDatabase                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays entries from Subnet VLAN    */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowAllVlanSubnetDatabase (tCliHandle CliHandle)
{
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    INT4                i4NextIndex = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pString = NULL;
    UINT4               u4SubnetMapAddr = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMapAddr = VLAN_INIT_VAL;
    UINT4               u4SubnetMapMask = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMapMask = VLAN_INIT_VAL;
    INT4                i4SubnetMapVlanId = VLAN_INIT_VAL;
    INT4                i4SubnetVlanStatus = VLAN_INIT_VAL;
    INT4                i4SubnetMapArpOption = VLAN_INIT_VAL;
    INT4                i4SubnetMapRowStatus = VLAN_INIT_VAL;
    CHR1               *au1Status[] = { "allow", "suppress" };

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
        ((INT4) u4IfIndex, &i4NextIndex,
         u4SubnetMapAddr, &u4NextSubnetMapAddr,
         u4SubnetMapMask, &u4NextSubnetMapMask) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (u4IfIndex != (UINT4) i4NextIndex)
    {
        return CLI_SUCCESS;
    }

    nmhGetDot1qFutureVlanSubnetBasedOnAllPorts (&i4SubnetVlanStatus);

    if (i4SubnetVlanStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nSubnet Map Table -- Subnet Vlan Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nSubnet Map Table -- Subnet Vlan Disabled\n");
    }

    CliPrintf (CliHandle,
               "-------------------------------------------------\r\n");
    CliPrintf (CliHandle,
               "Source IP     Subnet Mask      Vlan ID        ARP Traffic\r\n");
    CliPrintf (CliHandle,
               "-------------------------------------------------------\r\n");

    do
    {
        nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus (i4NextIndex,
                                                        u4NextSubnetMapAddr,
                                                        u4NextSubnetMapMask,
                                                        &i4SubnetMapRowStatus);
        if (i4SubnetMapRowStatus == VLAN_ACTIVE)
        {
            nmhGetDot1qFutureVlanPortSubnetMapExtVid (i4NextIndex,
                                                      u4NextSubnetMapAddr,
                                                      u4NextSubnetMapMask,
                                                      &i4SubnetMapVlanId);

            nmhGetDot1qFutureVlanPortSubnetMapExtARPOption (i4NextIndex,
                                                            u4NextSubnetMapAddr,
                                                            u4NextSubnetMapMask,
                                                            &i4SubnetMapArpOption);

            CLI_CONVERT_IPADDR_TO_STR (pString, u4NextSubnetMapAddr);
            CliPrintf (CliHandle, "%-15s", pString);

            CLI_CONVERT_IPADDR_TO_STR (pString, u4NextSubnetMapMask);
            CliPrintf (CliHandle, "%-15s", pString);

            CliPrintf (CliHandle, "%-15ld", i4SubnetMapVlanId);
            u4PagingStatus = CliPrintf (CliHandle, "%-15s\r\n",
                                        au1Status[i4SubnetMapArpOption - 1]);
        }

        u4SubnetMapAddr = u4NextSubnetMapAddr;
        u4SubnetMapMask = u4NextSubnetMapMask;

        u4IfIndex = (UINT4) i4NextIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            return CLI_FAILURE;
        }
        if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
            ((INT4) u4IfIndex, &i4NextIndex, u4SubnetMapAddr,
             &u4NextSubnetMapAddr, u4SubnetMapMask, &u4NextSubnetMapMask)
            == SNMP_FAILURE)
        {
            break;
        }
    }
    while (u4IfIndex == (UINT4) i4NextIndex);

    VlanReleaseContext ();

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanGlobalStats                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics information for  */
/*                        VLAN's                                             */
/*                                                                           */
/*     INPUT            : u4VlanId - Vlan identifier                         */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanGlobalStats (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT4 u4StartVlanId, UINT4 u4LastVlanId)
{
    UINT4               u4InFrames = 0;
    UINT4               u4OutFrames = 0;
    UINT4               u4InDiscards = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    CliPrintf (CliHandle, "\r\nPort Vlan statistics \r\n");

    CliPrintf (CliHandle, "-------------------------- \r\n");

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    if (u4StartVlanId != 0)
    {
        u4CurrentIndex = u4StartVlanId - 1;
    }

    while (VlanGetNextPortStatisticsTableIndex
           (i4CurrentPort, &i4NextPort, u4CurrentIndex,
            &u4NextIndex) != VLAN_FAILURE)
    {
        if (u4StartVlanId != 0)
        {
            if (u4NextIndex < u4StartVlanId)
            {
                i4CurrentPort = i4NextPort;
                u4CurrentIndex = u4StartVlanId - 1;
                continue;
            }
            if (u4NextIndex > u4LastVlanId)
            {
                i4CurrentPort = i4NextPort;
                u4CurrentIndex = VLAN_MAX_VLAN_ID;
                continue;
            }

        }

        VlanReleaseContext ();

        VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);

        nmhGetFsDot1qTpVlanPortInFrames (i4NextPort, u4NextIndex, &u4InFrames);

        nmhGetFsDot1qTpVlanPortOutFrames (i4NextPort, u4NextIndex,
                                          &u4OutFrames);

        nmhGetFsDot1qTpVlanPortInDiscards (i4NextPort, u4NextIndex,
                                           &u4InDiscards);

        CliPrintf (CliHandle, "Port %s\r\n", au1NameStr);

        CliPrintf (CliHandle, " Vlan ID    : %d\r\n", u4NextIndex);

        CliPrintf (CliHandle, " In frames  : %d\r\n", u4InFrames);

        CliPrintf (CliHandle, " Out frames : %d\r\n", u4OutFrames);

        CliPrintf (CliHandle, " Discards   : %d\r\n", u4InDiscards);

        u4PagingStatus =
            CliPrintf (CliHandle, "--------------------------\r\n");

        i4CurrentPort = i4NextPort;
        u4CurrentIndex = u4NextIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }

    VlanReleaseContext ();

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanStatistics                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics information for  */
/*                        VLAN's like number of unicast, broadcast and       */
/*                        unknown unicast packets flooded in the VLAN        */
/*                                                                           */
/*     INPUT            : VlanId - Vlan identifier                           */
/*                        u4ContextId - Context Identifie                    */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                    tVlanId StartVlanId, tVlanId LastVlanId)
{
    UINT4               u4Frames = 0;
    INT4                i4NextContextId = 0;
    INT4                i4SwStats = 0;
    UINT4               u4CurrentVlanId = 0;
    UINT4               u4NextVlanId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    INT4                i4CounterStatus = VLAN_DISABLED;

    if (StartVlanId != 0)
    {
        u4CurrentVlanId = StartVlanId - 1;
    }

    if (nmhGetNextIndexFsMIDot1qFutureVlanCounterTable
        ((INT4) u4ContextId, &i4NextContextId,
         u4CurrentVlanId, &u4NextVlanId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, " Vlan Does not Exist\r\n");
        return CLI_FAILURE;
    }
    else if ((UINT4) i4NextContextId != u4ContextId)
    {
        CliPrintf (CliHandle, " Vlan Does not Exist in context\r\n");
        return CLI_FAILURE;
    }

    nmhGetDot1qFutureVlanSwStatsEnabled (&i4SwStats);

    if (i4SwStats == VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Software Statistics Enabled           \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Software Statistics Disabled          \r\n");
    }

    CliPrintf (CliHandle, "\r\nUnicast/broadcast Vlan statistics \r\n");

    CliPrintf (CliHandle, "------------------------------------- \r\n");

    do
    {
        if (LastVlanId != 0)
        {
            if (u4NextVlanId > LastVlanId)
            {
                break;
            }
        }
        nmhGetFsMIDot1qFutureVlanCounterStatus ((INT4) u4ContextId,
                                                u4NextVlanId, &i4CounterStatus);

        if (i4CounterStatus == VLAN_ENABLED)
        {
            CliPrintf (CliHandle, " Vlan Statistics Collection is Enabled\r\n");
            CliPrintf (CliHandle, " Vlan Id                          : %d\r\n",
                       u4NextVlanId);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterRxUcast (u4ContextId,
                                                     u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " Unicast frames received          : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterRxMcastBcast (u4ContextId,
                                                          u4NextVlanId,
                                                          &u4Frames);
            CliPrintf (CliHandle, " Mcast/Bcast frames received      : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterTxUnknUcast (u4ContextId,
                                                         u4NextVlanId,
                                                         &u4Frames);
            CliPrintf (CliHandle, " Unknown Unicast frames flooded   : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterTxUcast (u4ContextId,
                                                     u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " Unicast frames transmitted       : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterTxBcast (u4ContextId,
                                                     u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " Broadcast frames transmitted     : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterRxFrames (u4ContextId,
                                                      u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " In Frames                        : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterRxBytes (u4ContextId,
                                                     u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " In Bytes                         : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterTxFrames (u4ContextId,
                                                      u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " Out Frames                       : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterTxBytes (u4ContextId,
                                                     u4NextVlanId, &u4Frames);
            CliPrintf (CliHandle, " Out Bytes                        : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterDiscardFrames (u4ContextId,
                                                           u4NextVlanId,
                                                           &u4Frames);
            CliPrintf (CliHandle, " Discard Frames                   : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIDot1qFutureVlanCounterDiscardBytes (u4ContextId,
                                                          u4NextVlanId,
                                                          &u4Frames);
            CliPrintf (CliHandle, " Discard Bytes                    : %u\r\n",
                       u4Frames);

        }
        else
        {
            CliPrintf (CliHandle,
                       " Vlan Statistics Collection is Disabled\r\n");
        }

        u4PagingStatus =
            CliPrintf (CliHandle, "------------------------------------- \r\n");

        u4CurrentVlanId = u4NextVlanId;

        if (nmhGetNextIndexFsMIDot1qFutureVlanCounterTable
            ((INT4) u4ContextId, &i4NextContextId,
             u4CurrentVlanId, &u4NextVlanId) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if ((UINT4) i4NextContextId != u4ContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : ShowVlanMacLearningParams                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Unicast Mac Learning   */
/*                        Status and Learning Limit for the vlan in the     */
/*                        given context.                                    */
/*                                                                          */
/*     INPUT            : VlanId  - Vlan Id                                 */
/*                        u4ContextId - Virtual context Id                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
ShowVlanMacLearningParams (tCliHandle CliHandle, UINT4 u4ContextId,
                           tVlanId StartVlanId, tVlanId LastVlanId)
{
    INT4                i4AdminStatus = VLAN_INIT_VAL;
    INT4                i4OperStatus = VLAN_INIT_VAL;
    UINT4               u4LearningLimit = VLAN_INIT_VAL;
    INT4                i4NextContextId = VLAN_INIT_VAL;
    UINT4               u4CurrentVlanId = VLAN_INIT_VAL;
    UINT4               u4NextVlanId = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;

    if (StartVlanId != 0)
    {
        u4CurrentVlanId = StartVlanId - 1;
    }

    if (nmhGetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable
        ((INT4) u4ContextId, &i4NextContextId,
         u4CurrentVlanId, &u4NextVlanId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    else if ((UINT4) i4NextContextId != u4ContextId)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nUnicast MAC Learning Parameters  \r\n");

    CliPrintf (CliHandle, "------------------------------------- \r\n");

    do
    {
        if (LastVlanId != 0)
        {
            if (u4NextVlanId > LastVlanId)
            {
                break;
            }
        }
        /* print if all entries are required or
         * required one is exactly matched
         */
        CliPrintf (CliHandle, "Vlan Id                  : %d\r\n",
                   u4NextVlanId);

        nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus (u4ContextId,
                                                         u4NextVlanId,
                                                         &i4AdminStatus);

        CliPrintf (CliHandle, "Mac Learning Admin-Status : ");
        if (i4AdminStatus == VLAN_ENABLED)
        {
            CliPrintf (CliHandle, "Enable\r\n");
        }
        else if ((i4AdminStatus == VLAN_DISABLED) ||
                 (i4AdminStatus == VLAN_LEARNING_LIMIT_ZERO_TRIGGER))
        {
            CliPrintf (CliHandle, "Disable\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Default\r\n");
        }

        nmhGetFsMIDot1qFutureVlanOperMacLearningStatus (u4ContextId,
                                                        u4NextVlanId,
                                                        &i4OperStatus);

        CliPrintf (CliHandle, "Mac Learning Oper-Status  : ");
        if (i4OperStatus == VLAN_ENABLED)
        {
            CliPrintf (CliHandle, "Enable\r\n");
        }
        else if (i4OperStatus == VLAN_DISABLED)
        {
            CliPrintf (CliHandle, "Disable\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Default\r\n");
        }

        /* When u1HwUnicastMacLearningLimitSup is supported.
         * Get unicast mac learing limit Values*/
        if (ISS_HW_SUPPORTED ==
            IssGetHwCapabilities (ISS_HW_UNICAST_MAC_LEARNING_LIMIT))
        {
            nmhGetFsMIDot1qFutureVlanUnicastMacLimit ((INT4) u4ContextId,
                                                      u4NextVlanId,
                                                      &u4LearningLimit);
            CliPrintf (CliHandle, "Mac Learning Limit        : %d\r\n",
                       u4LearningLimit);
        }

        u4PagingStatus =
            CliPrintf (CliHandle, "------------------------------------- \r\n");

        u4CurrentVlanId = u4NextVlanId;

        if (nmhGetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable
            ((INT4) u4ContextId, &i4NextContextId,
             u4CurrentVlanId, &u4NextVlanId) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if ((UINT4) i4NextContextId != u4ContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanResetVlanStats                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the VLAN statistics to zero     */
/*                                                                           */
/*     INPUT            : VlanId - Vlan identifier                           */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanResetVlanStats (tCliHandle CliHandle, tVlanId VlanId)
{
    UINT4               u4CurrentVlanId = 0;
    UINT4               u4NextVlanId = 0;
    UINT1               u1isResetAll = TRUE;

    if (VlanId != 0)
    {
        if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
            (VlanId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nInvalid Vlan ID\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetFirstIndexDot1qFutureVlanCounterTable
        (&u4NextVlanId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        /* print if all entries are required or
         * required one is exactly matched
         */
        if (((VlanId != 0) && (u4NextVlanId == VlanId)) || (VlanId == 0))
        {
            VlanCounterReset ((tVlanId) u4NextVlanId);
        }

        u4CurrentVlanId = u4NextVlanId;

        if (nmhGetNextIndexDot1qFutureVlanCounterTable
            (u4CurrentVlanId, &u4NextVlanId) == SNMP_FAILURE)
        {
            u1isResetAll = FALSE;
        }
    }
    while (u1isResetAll);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanMacAddrTable ()                            */
/*                                                                       */
/* DESCRIPTION      : This function displays unicast and multicast       */
/*                    tables. If the arguments MacAddr, VlanId and       */
/*                    IfIndex are provided, then entries matching these  */
/*                    arguments alone will be displayed.                 */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Mac Address (unicast or multicast)    */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT1 *pu1MacAddr, tVlanId StartVlanId,
                      tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4FdbId = 0;
    INT4                i4NextContextId = 0;
    UINT4               u4Mask = 0;
    INT4                i4Count = 0;
    UINT2               u2Index = 0;
    tVlanCliArgs        CliArgs;
    UINT1               u1VlanLearningType;
    tTMO_SLL            CliFidList;

    VLAN_SLL_INIT (&CliFidList);

    CliPrintf (CliHandle, "\r\nVlan    Mac Address         Type");
#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "     ConnectionId        PwIndex  Ports");
#else
    CliPrintf (CliHandle, "     ConnectionId       Ports");
#endif
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "----    -----------         ----");
#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "     ------------        -------  -----");
#else
    CliPrintf (CliHandle, "     -----------        -----");
#endif
    CliPrintf (CliHandle, "\r\n");

    if (u4IfIndex != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_FDB_ID_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;

        if (VLAN_IS_MCASTADDR (pu1MacAddr) == VLAN_TRUE)
        {
            /* Multicast or Broadcast Mac address */
            if (StartVlanId != 0)
            {
                /* 
                 * VlanId and Mac Address are provided, so we can obtain
                 * an unique entry. So need not scan the entire table.
                 */
                for (u2Index = StartVlanId; u2Index <= LastVlanId; u2Index++)
                {
                    ShowVlanMcastEntryVlanMacAddr (CliHandle, u4ContextId,
                                                   u2Index, pu1MacAddr,
                                                   u4IfIndex, VLAN_FALSE,
                                                   &i4Count);
                }

                if (i4Count != -1)
                {
                    CliPrintf (CliHandle, "\r\nTotal Mac Addresses "
                               "displayed: %d\r\n\r\n", i4Count);
                }

                return CLI_SUCCESS;
            }
        }
        else
        {
            /* Unicast Mac address */
            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            u1VlanLearningType = VlanGetVlanLearningMode ();
            VlanReleaseContext ();

            if (u1VlanLearningType == VLAN_SHARED_LEARNING)
            {
                /*
                 * Mac Address is provided and since Vlan Learning is SVL,
                 * there will be an unique entry in the Table. So need not
                 * scan the entire table.
                 */

                if (StartVlanId == 0)
                {
                    /*
                     * StartVlanId is not provided. So obtain the first FdbId 
                     * (in fact there will be only one FdbId, since Vlan 
                     * Learning is SVL).
                     */
                    if (nmhValidateIndexInstanceFsDot1qFdbTable
                        (u4ContextId, 0) != SNMP_SUCCESS)
                    {
                        if (nmhGetNextIndexFsDot1qFdbTable
                            ((INT4) u4ContextId, &i4NextContextId,
                             0, &u4FdbId) != SNMP_SUCCESS)
                        {
                            return CLI_FAILURE;
                        }
                        if (u4ContextId != (UINT4) i4NextContextId)
                        {
                            return CLI_FAILURE;
                        }
                    }
                    else
                    {
                        u4FdbId = 0;
                    }
                }

                ShowVlanFdbEntryVlanMacAddr (CliHandle, u4ContextId,
                                             u4FdbId, StartVlanId, pu1MacAddr,
                                             u4IfIndex, VLAN_FALSE, &i4Count);

                CliPrintf (CliHandle,
                           "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
                           i4Count);

                return CLI_SUCCESS;
            }
            else
            {
                /* Vlan Learning is IVL */
                if (StartVlanId != 0)
                {
                    /* 
                     * VlanId and Mac Address are provided, so we can obtain
                     * an unique entry. So need not scan the entire table.
                     */

                    for (u2Index = StartVlanId; u2Index <= LastVlanId;
                         u2Index++)
                    {
                        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\rInvalid Virtual Context\r\n");
                            return CLI_FAILURE;
                        }

                        if (VlanGetFdbIdFromVlanId (u2Index, &u4FdbId) ==
                            VLAN_FAILURE)
                        {
                            VlanReleaseContext ();
                            continue;
                        }
                        VlanReleaseContext ();

                        ShowVlanFdbEntryVlanMacAddr (CliHandle, u4ContextId,
                                                     u4FdbId, u2Index,
                                                     pu1MacAddr, u4IfIndex,
                                                     VLAN_FALSE, &i4Count);
                    }

                    CliPrintf (CliHandle, "\r\nTotal Mac Addresses "
                               "displayed: %d\r\n\r\n", i4Count);
                    return CLI_SUCCESS;
                }
            }
        }
    }
    if (u4Mask == 0)
    {
        /* 
         * Displaying the entire Unicast and Multicast tables, hence
         * initialising tha fields of CliArgs to 0.
         */
        CliArgs.u4VlanIdFdbId1 = 0;
        CliArgs.u4Port1 = 0;
        CliArgs.u4Status1 = 0;
        CliArgs.pu1MacAddr1 = NULL;
        CliArgs.pu1Ports = NULL;

        /* 
         * None of the arguments is present. So all the entries must be
         * displayed. The function pointer (comparison function) that is 
         * passed to ScanAndPrint routine, determines if the entry needs 
         * to be displayed or not. So in this case, we pass NULL as the 
         * comparison function.
         */
        VlanCliScanAndPrintFdbTable (CliHandle, u4ContextId, NULL,
                                     u4Mask, &CliArgs, VLAN_TRUE, &i4Count);

        if (i4Count == -1)
        {
            /* User Termination is provided */
            return CLI_SUCCESS;
        }

    }
    else
    {
        /* Displaying specific entries of Unicast and Multicast tables */
        CliArgs.u4VlanIdFdbId1 = 0;
        CliArgs.u4Port1 = u4IfIndex;
        CliArgs.u4Status1 = 0;
        CliArgs.pu1MacAddr1 = pu1MacAddr;
        CliArgs.pu1Ports = NULL;

        if (StartVlanId != 0)
        {
            if (VlanCliAddFidListScanAndPrint (CliHandle, u4ContextId,
                                               StartVlanId, LastVlanId,
                                               u4Mask, &CliArgs, &i4Count,
                                               VLAN_SCAN_PRINT_FDB_TABLE) ==
                CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            VlanCliScanAndPrintFdbTable (CliHandle, u4ContextId,
                                         VlanCliCompareArgs, u4Mask,
                                         &CliArgs, VLAN_TRUE, &i4Count);

        }
        /*Reset the bits */
        u4Mask = u4Mask & (~VLAN_CLI_VLAN_ID_FDB_ID_PRESENT);

        if (i4Count == -1)
        {
            /* User Termination is provided */
            return CLI_SUCCESS;
        }

        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_PRESENT;
        /* Comparison should be made based on VlanId */
        CliArgs.u4VlanId1 = (UINT4) StartVlanId;
        CliArgs.u4VlanId2 = (UINT4) LastVlanId;

        VlanCliScanAndPrintGroupTable (CliHandle, u4ContextId,
                                       VlanCliCompareArgs, u4Mask,
                                       &CliArgs, VLAN_FALSE,
                                       VLAN_TRUE, &i4Count);

        if (i4Count == -1)
        {
            /* User Termination is provided */
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDot1dMacAddrTable ()                       */
/*                                                                       */
/* DESCRIPTION      : This function displays unicast and multicast       */
/*                    tables for 802.1d Bridge. If the arguments MacAddr,*/
/*                    IfIndex are provided, then entries matching these  */
/*                    arguments alone will be displayed.                 */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Mac Address (unicast or multicast)    */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDot1dMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT1 *pu1MacAddr, UINT4 u4IfIndex)
{
    UINT4               u4FdbId = VLAN_INIT_VAL;
    UINT4               u4Mask = VLAN_INIT_VAL;
    INT4                i4Count = VLAN_INIT_VAL;
    tVlanCliArgs        CliArgs;
    tTMO_SLL            CliFidList;
    UINT2               u2VlanId = VLAN_INIT_VAL;

    VLAN_SLL_INIT (&CliFidList);

    u2VlanId = VLAN_DEF_VLAN_ID;

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\rInvalid Virtual Context\r\n");
        return CLI_FAILURE;
    }

    if (VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        VlanReleaseContext ();
        return CLI_FAILURE;
    }
    VlanReleaseContext ();

    CliPrintf (CliHandle, "\r\nMac Address         Type");
    CliPrintf (CliHandle, "     Ports");

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "-----------         ----");
    CliPrintf (CliHandle, "     -----");
    CliPrintf (CliHandle, "\r\n");

    if (u4IfIndex != VLAN_INIT_VAL)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;

        if (VLAN_IS_MCASTADDR (pu1MacAddr) == VLAN_TRUE)
        {
            /* Multicast or Broadcast Mac address */
            /* 
             * VlanId and Mac Address are provided, so we can obtain
             * an unique entry. So need not scan the entire table.
             */
            ShowVlanDot1dMcastEntryVlanMacAddr (CliHandle, u4ContextId,
                                                u2VlanId, pu1MacAddr,
                                                u4IfIndex, VLAN_FALSE,
                                                &i4Count);

            if (i4Count != -1)
            {
                CliPrintf (CliHandle, "\r\nTotal Mac Addresses "
                           "displayed: %d\r\n\r\n", i4Count);
            }
            return CLI_SUCCESS;
        }
        else
        {
            /* Unicast Mac address */
            /* 
             * VlanId and Mac Address are provided, so we can obtain
             * an unique entry. So need not scan the entire table.
             */
            ShowVlanDot1dFdbEntryVlanMacAddr (CliHandle, u4ContextId,
                                              u4FdbId, u2VlanId,
                                              pu1MacAddr, u4IfIndex,
                                              VLAN_FALSE, &i4Count);

            CliPrintf (CliHandle, "\r\nTotal Mac Addresses "
                       "displayed: %d\r\n\r\n", i4Count);
            return CLI_SUCCESS;
        }
    }
    if (u4Mask == VLAN_INIT_VAL)
    {
        /* 
         * Displaying the entire Unicast and Multicast tables, hence
         * initialising tha fields of CliArgs to 0.
         */
        CliArgs.u4VlanIdFdbId1 = VLAN_INIT_VAL;
        CliArgs.u4Port1 = VLAN_INIT_VAL;
        CliArgs.u4Status1 = VLAN_INIT_VAL;
        CliArgs.pu1MacAddr1 = NULL;
        CliArgs.pu1Ports = NULL;

        /* 
         * None of the arguments is present. So all the entries must be
         * displayed. The function pointer (comparison function) that is 
         * passed to ScanAndPrint routine, determines if the entry needs 
         * to be displayed or not. So in this case, we pass NULL as the 
         * comparison function.
         */
        VlanCliScanAndPrintDot1dFdbTable (CliHandle, u4ContextId, NULL,
                                          u4Mask, &CliArgs, VLAN_TRUE,
                                          &i4Count);

        if (i4Count == -1)
        {
            /* User Termination is provided */
            return CLI_SUCCESS;
        }
    }
    else
    {
        /* Displaying specific entries of Unicast and Multicast tables */
        CliArgs.u4VlanIdFdbId1 = VLAN_INIT_VAL;
        CliArgs.u4Port1 = u4IfIndex;
        CliArgs.u4Status1 = VLAN_INIT_VAL;
        CliArgs.pu1MacAddr1 = pu1MacAddr;
        CliArgs.pu1Ports = NULL;

        VlanCliScanAndPrintDot1dFdbTable (CliHandle, u4ContextId,
                                          VlanCliCompareArgs, u4Mask,
                                          &CliArgs, VLAN_TRUE, &i4Count);
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanMacAddrCount ()                            */
/*                                                                       */
/* DESCRIPTION      : This function displays the total number of dynamic */
/*                    and static entries for all the Vlans.              */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanMacAddrCount (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4RetVal;
    tVlanCurrEntry     *pCurrEntry;
    INT4                i4CurrentContextId;
    UINT4               u4CurrentVlanId = 0;
    UINT4               u4CurrentTimeMark = 0;
    INT4                i4NextContextId;
    UINT4               u4NextVlanId;
    UINT4               u4NextTimeMark;
    UINT4               u4PagingStatus = 0;

    i4CurrentContextId = (INT4) u4ContextId;

    while (nmhGetNextIndexFsDot1qVlanCurrentTable
           (i4CurrentContextId, &i4NextContextId,
            u4CurrentTimeMark, &u4NextTimeMark, u4CurrentVlanId,
            &u4NextVlanId) != SNMP_FAILURE)
    {
        if (i4CurrentContextId != i4NextContextId)
        {
            break;
        }
        if (VlanSelectContext ((UINT4) i4CurrentContextId) != VLAN_SUCCESS)
        {
            break;
        }
        pCurrEntry = VlanGetVlanEntry ((tVlanId) u4NextVlanId);
        if (NULL == pCurrEntry)
        {
            break;
        }
        VlanReleaseContext ();

        i4RetVal = ShowVlanMacAddrCountVlan (CliHandle,
                                             (UINT4) i4CurrentContextId,
                                             pCurrEntry->VlanId,
                                             &u4PagingStatus);

        if (i4RetVal == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            return CLI_SUCCESS;
        }

        i4CurrentContextId = i4NextContextId;
        u4CurrentVlanId = u4NextVlanId;
        u4CurrentTimeMark = u4NextTimeMark;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanMacAddrCountVlan ()                        */
/*                                                                       */
/* DESCRIPTION      : This function displays the total number of dynamic */
/*                    and static entries.                                */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    VlanId     - Vlan Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanMacAddrCountVlan (tCliHandle CliHandle, UINT4 u4ContextId,
                          tVlanId VlanId, UINT4 *pi4PagingStatus)
{
    UINT4               u4FdbId = 0;
#ifdef SW_LEARNING
    tVlanCurrEntry     *pVlanEntry = NULL;
#else

    UINT4               u4Mask = 0;
    INT4                i4DynUcastCount = 0;
    INT4                i4StUcastCount = 0;
    INT4                i4DynMcastCount = 0;
    INT4                i4StMcastCount = 0;
    tVlanCliArgs        CliArgs;
#endif

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\rInvalid Virtual Switch\r\n");
        return CLI_FAILURE;
    }
    if (VlanGetFdbIdFromVlanId (VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Vlan is not Active\r\n");
        VlanReleaseContext ();
        return CLI_FAILURE;
    }

#ifdef SW_LEARNING
    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nMac Entries for Vlan %d:\r\n", VlanId);
    CliPrintf (CliHandle, "--------------------------\r\n");

    CliPrintf (CliHandle, "Dynamic Unicast Address Count    : %d\r\n",
               pVlanEntry->u4UnicastDynamicCount);
    CliPrintf (CliHandle, "Dynamic Multicast Address Count  : %d\r\n",
               pVlanEntry->u4MulticastDynamicCount);
    CliPrintf (CliHandle, "Static Unicast Address Count     : %d\r\n",
               pVlanEntry->u4UnicastStaticCount);
    CliPrintf (CliHandle, "Static Multicast Address Count   : %d\r\n",
               pVlanEntry->u4MulticastStaticCount);
    *pi4PagingStatus =
        CliPrintf (CliHandle, "----------------------------------------\r\n");

#else

    u4Mask = VLAN_CLI_VLAN_ID_FDB_ID_PRESENT;

    /* Displaying specific entries of Unicast and Multicast tables */
    CliArgs.u4Port1 = 0;
    CliArgs.pu1MacAddr1 = NULL;
    CliArgs.pu1Ports = NULL;

    /* 
     * To count only the Dynamic Fdb entries set the bit in the CliArgs
     * corresponding to the u4Status1 field.
     */
    u4Mask = u4Mask | VLAN_CLI_FDB_STATUS_PRESENT;
    CliArgs.u4Status1 = VLAN_FDB_LEARNT;

    /* Comparison should be made based on VlanId */
    CliArgs.u4VlanIdFdbId1 = u4FdbId;
    VlanCliScanAndPrintFdbTable (CliHandle, u4ContextId,
                                 VlanCliCompareArgs, u4Mask, &CliArgs,
                                 VLAN_FALSE, &i4DynUcastCount);

    /* Reset the bit corresponding to the u4Status1 field */
    u4Mask = u4Mask & (~VLAN_CLI_FDB_STATUS_PRESENT);
    VlanCliScanAndPrintStUcastTable (CliHandle, u4ContextId,
                                     VlanCliCompareArgs, u4Mask,
                                     &CliArgs, VLAN_FALSE, &i4StUcastCount);

    /*Reset the bits */
    u4Mask = u4Mask & (~VLAN_CLI_VLAN_ID_FDB_ID_PRESENT);
    u4Mask = u4Mask | VLAN_CLI_VLAN_ID_PRESENT;

    /* Comparison should be made based on VlanId */
    CliArgs.u4VlanId1 = (UINT4) VlanId;
    CliArgs.u4VlanId2 = (UINT4) VlanId;

    VlanCliScanAndPrintGroupTable (CliHandle, u4ContextId, VlanCliCompareArgs,
                                   u4Mask, &CliArgs, VLAN_TRUE,
                                   VLAN_FALSE, &i4DynMcastCount);

    VlanCliScanAndPrintStMcastTable (CliHandle, u4ContextId,
                                     VlanCliCompareArgs, u4Mask,
                                     &CliArgs, VLAN_FALSE, &i4StMcastCount);

    CliPrintf (CliHandle, "\r\nMac Entries for Vlan %d:\r\n", VlanId);
    CliPrintf (CliHandle, "--------------------------\r\n");

    CliPrintf (CliHandle, "Dynamic Unicast Address Count    : %d\r\n",
               i4DynUcastCount);
    CliPrintf (CliHandle, "Dynamic Multicast Address Count  : %d\r\n",
               i4DynMcastCount);
    CliPrintf (CliHandle, "Static Unicast Address Count     : %d\r\n",
               i4StUcastCount);
    CliPrintf (CliHandle, "Static Multicast Address Count   : %d\r\n",
               i4StMcastCount);
    *pi4PagingStatus =
        CliPrintf (CliHandle, "----------------------------------------\r\n");
#endif

    VlanReleaseContext ();

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanUcastMacPortSecurity                       */
/*                                                                       */
/* DESCRIPTION      : This function displays Static Unicast table. If    */
/*                    the arguments MacAddr, VlanId and IfIndex are      */
/*                    provided, then entries matching these arguments    */
/*                    alone will be displayed.                           */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Unicast Mac Address                   */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanUcastMacPortSecurity (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4IfIndex)
{
    UINT4               u4PortId = 0;
    UINT4               u4MaxFrontPanelPorts = 0;

    u4MaxFrontPanelPorts = IssGetFrontPanelPortCountFromNvRam ();

    if (u4IfIndex != 0)
    {
        VlanCliUnicastMacSecScanAndPrint (CliHandle, u4ContextId, u4IfIndex);
    }
    else
    {
        for (u4PortId = 1; u4PortId <= u4MaxFrontPanelPorts; u4PortId++)
        {
            VlanCliUnicastMacSecScanAndPrint (CliHandle, u4ContextId, u4PortId);
        }
    }

    VlanReleaseContext ();
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanStUcastMacAddrTable ()                     */
/*                                                                       */
/* DESCRIPTION      : This function displays Static Unicast table. If    */
/*                    the arguments MacAddr, VlanId and IfIndex are      */
/*                    provided, then entries matching these arguments    */
/*                    alone will be displayed.                           */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Unicast Mac Address                   */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanStUcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT1 *pu1MacAddr, tVlanId StartVlanId,
                             tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4Mask = 0;
    INT4                i4Count = 0;
    tVlanCliArgs        CliArgs;
    tTMO_SLL            CliFidList;

    VLAN_SLL_INIT (&CliFidList);
#ifdef PBB_WANTED
    CliPrintf (CliHandle,
               "\r\nVlan  Mac Address       SrvInst/  Status        ConnectionId         Ports\r\n");
    CliPrintf (CliHandle,
               "\r\n                        RecvPort                                               \r\n");
    CliPrintf (CliHandle,
               "----  -----------       --------  ------        ------------         -----\r\n");
#else
    CliPrintf (CliHandle,
               "\r\nVlan  Mac Address        RecvPort Status        ConnectionId         Ports\r\n");
    CliPrintf (CliHandle,
               "----  -----------        -------- ------        ------------         -----\r\n");
#endif

    if (u4IfIndex != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_FDB_ID_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;
    }

    /* Displaying specific entries of Unicast and Multicast tables */
    CliArgs.u4VlanIdFdbId1 = 0;
    CliArgs.u4Port1 = u4IfIndex;
    CliArgs.u4Status1 = 0;
    CliArgs.pu1MacAddr1 = pu1MacAddr;
    CliArgs.pu1Ports = NULL;

    if (StartVlanId != 0)
    {
        if (VlanCliAddFidListScanAndPrint (CliHandle, u4ContextId,
                                           StartVlanId, LastVlanId,
                                           u4Mask, &CliArgs, &i4Count,
                                           VLAN_SCAN_PRINT_STUCAST_TABLE) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        VlanCliScanAndPrintStUcastTable (CliHandle, u4ContextId,
                                         VlanCliCompareArgs, u4Mask, &CliArgs,
                                         VLAN_TRUE, &i4Count);

    }

    if (i4Count == -1)
    {
        /* User Termination is provided */
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDot1dStUcastMacAddrTable ()                */
/*                                                                       */
/* DESCRIPTION      : This function displays Static Unicast entries in   */
/*                    Static Table for Transparent Bridging.             */
/*                    If the arguments MacAddr, VlanId and IfIndex are   */
/*                    provided, then entries matching these arguments    */
/*                    alone will be displayed.                           */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Unicast Mac Address                   */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDot1dStUcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                  tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4Mask = VLAN_INIT_VAL;
    INT4                i4Count = VLAN_INIT_VAL;
    tVlanCliArgs        CliArgs;
    tTMO_SLL            CliFidList;

    VLAN_SLL_INIT (&CliFidList);

    CliPrintf (CliHandle,
               "\r\nMac Address        RecvPort  Status         Ports\r\n");
    CliPrintf (CliHandle,
               "-----------        --------  ------         -----\r\n");

    if (u4IfIndex != VLAN_INIT_VAL)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != VLAN_INIT_VAL)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_FDB_ID_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;
    }

    /* Displaying specific entries of Unicast and Multicast tables */
    CliArgs.u4VlanIdFdbId1 = VLAN_INIT_VAL;
    CliArgs.u4Port1 = u4IfIndex;
    CliArgs.u4Status1 = VLAN_INIT_VAL;
    CliArgs.pu1MacAddr1 = pu1MacAddr;
    CliArgs.pu1Ports = NULL;

    if (StartVlanId != VLAN_INIT_VAL)
    {
        if (VlanCliAddFidListScanAndPrint (CliHandle, u4ContextId,
                                           StartVlanId, LastVlanId,
                                           u4Mask, &CliArgs, &i4Count,
                                           VLAN_DOT1D_SCAN_PRINT_STUCAST_TABLE)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        VlanCliDot1dScanAndPrintStUcastTable (CliHandle, u4ContextId,
                                              VlanCliCompareArgs, u4Mask,
                                              &CliArgs, VLAN_TRUE, &i4Count);

    }

    if (i4Count == -1)
    {
        /* User Termination is provided */
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDynamicUcastMacAddrTable ()                */
/*                                                                       */
/* DESCRIPTION      : This function displays learnt Fdb entries. If the  */
/*                    arguments MacAddr, VlanId and IfIndex are provided */
/*                    then entries matching these arguments alone will   */
/*                    be displayed.                                      */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Mac Address (unicast or multicast)    */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDynamicUcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                  tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4FdbId = 0;
    INT4                i4NextContextId = 0;
    UINT1               u1VlanLearningType;
    UINT4               u4Mask = VLAN_CLI_FDB_STATUS_PRESENT;
    INT4                i4Count = 0;
    UINT2               u2Index = 0;
    tVlanCliArgs        CliArgs;
    tTMO_SLL            CliFidList;

    VLAN_SLL_INIT (&CliFidList);

    CliPrintf (CliHandle, "\r\nVlan    Mac Address         Type");
#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "     ConnectionId        PwIndex  Ports");
#else
    CliPrintf (CliHandle, "     ConnectionId        Ports");
#endif
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "----    -----------         ----");
#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "     ------------        -------  -----");
#else
    CliPrintf (CliHandle, "     -------------        -----");
#endif
    CliPrintf (CliHandle, "\r\n");

    if (u4IfIndex != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_FDB_ID_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;

        /* Unicast Mac address */
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return VLAN_SUCCESS;
        }
        u1VlanLearningType = VlanGetVlanLearningMode ();
        VlanReleaseContext ();

        if (u1VlanLearningType == VLAN_SHARED_LEARNING)
        {
            /*
             * Mac Address is provided and since Vlan Learning is SVL,
             * there will be an unique entry in the Table. So need not
             * scan the entire table.
             */

            if (StartVlanId == 0)
            {
                /*
                 * VlanId is not provided. So obtain the first FdbId 
                 * (in fact there will be only one FdbId, since Vlan 
                 * Learning is SVL).
                 */
                if (nmhValidateIndexInstanceFsDot1qFdbTable
                    (u4ContextId, 0) != SNMP_SUCCESS)
                {
                    if (nmhGetNextIndexFsDot1qFdbTable
                        ((INT4) u4ContextId, &i4NextContextId,
                         0, &u4FdbId) != SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    if (u4ContextId != (UINT4) i4NextContextId)
                    {
                        return CLI_FAILURE;
                    }
                }
                else
                {
                    u4FdbId = 0;
                }
            }

            ShowVlanFdbEntryVlanMacAddr (CliHandle, u4ContextId, u4FdbId,
                                         StartVlanId, pu1MacAddr, u4IfIndex,
                                         VLAN_TRUE, &i4Count);

            CliPrintf (CliHandle,
                       "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
                       i4Count);

            return CLI_SUCCESS;

        }
        else
        {
            /* Vlan Learning is IVL */
            if (StartVlanId != 0)
            {
                /* 
                 * VlanId and Mac Address are provided, so we can obtain
                 * an unique entry. So need not scan the entire table.
                 */
                for (u2Index = StartVlanId; u2Index <= LastVlanId; u2Index++)
                {
                    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\rInvalid Virtual Context\r\n");
                        return CLI_FAILURE;
                    }

                    if (VlanGetFdbIdFromVlanId (u2Index, &u4FdbId) ==
                        VLAN_FAILURE)
                    {
                        VlanReleaseContext ();
                        continue;
                    }
                    VlanReleaseContext ();

                    ShowVlanFdbEntryVlanMacAddr (CliHandle, u4ContextId,
                                                 u4FdbId, StartVlanId,
                                                 pu1MacAddr, u4IfIndex,
                                                 VLAN_TRUE, &i4Count);

                }
                CliPrintf (CliHandle,
                           "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
                           i4Count);

                return CLI_SUCCESS;
            }
        }
    }

    CliArgs.u4VlanIdFdbId1 = 0;
    CliArgs.u4Port1 = u4IfIndex;
    CliArgs.u4Status1 = VLAN_FDB_LEARNT;
    CliArgs.pu1MacAddr1 = pu1MacAddr;
    CliArgs.pu1Ports = NULL;

    if (StartVlanId != 0)
    {
        if (VlanCliAddFidListScanAndPrint (CliHandle, u4ContextId,
                                           StartVlanId, LastVlanId,
                                           u4Mask, &CliArgs, &i4Count,
                                           VLAN_SCAN_PRINT_FDB_TABLE) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        VlanCliScanAndPrintFdbTable (CliHandle, u4ContextId,
                                     VlanCliCompareArgs, u4Mask,
                                     &CliArgs, VLAN_TRUE, &i4Count);

    }
    if (i4Count == -1)
    {
        /* User Termination is provided */
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanStMcastMacAddrTable ()                     */
/*                                                                       */
/* DESCRIPTION      : This function displays Static Multicast table. If  */
/*                    the arguments MacAddr, VlanId and IfIndex are      */
/*                    provided, then entries matching these arguments    */
/*                    alone will be displayed.                           */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Unicast Mac Address                   */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanStMcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT1 *pu1MacAddr, tVlanId StartVlanId,
                             tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4Mask = 0;
    INT4                i4Count = 0;
    tVlanCliArgs        CliArgs;

    if (u4IfIndex != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;
    }

    /* Displaying specific entries of Unicast and Multicast tables */
    CliArgs.u4VlanId1 = (UINT4) StartVlanId;
    CliArgs.u4Port1 = u4IfIndex;
    CliArgs.u4Status1 = 0;
    CliArgs.pu1MacAddr1 = pu1MacAddr;
    CliArgs.pu1Ports = NULL;
    CliArgs.u4VlanId2 = (UINT4) LastVlanId;

    CliPrintf (CliHandle, "\r\nStatic Multicast Table \r\n");
    CliPrintf (CliHandle, "\r---------------------- \r\n");

    VlanCliScanAndPrintStMcastTable (CliHandle, u4ContextId,
                                     VlanCliCompareArgs, u4Mask, &CliArgs,
                                     VLAN_TRUE, &i4Count);

    if (i4Count == -1)
    {
        /* User Termination is provided */
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDot1dStMcastMacAddrTable ()                */
/*                                                                       */
/* DESCRIPTION      : This function displays Static Multicast entries in */
/*                    Static Table for Transparent Bridging.             */
/*                    If the arguments MacAddr, VlanId and IfIndex are   */
/*                    provided, then entries matching these arguments    */
/*                    alone will be displayed.                           */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Unicast Mac Address                   */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDot1dStMcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                  tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4Mask = VLAN_INIT_VAL;
    INT4                i4Count = VLAN_INIT_VAL;
    tVlanCliArgs        CliArgs;
    tTMO_SLL            CliFidList;

    VLAN_SLL_INIT (&CliFidList);

    CliPrintf (CliHandle,
               "\r\nMac Address        RecvPort  Status         Ports\r\n");
    CliPrintf (CliHandle,
               "-----------        --------  ------         -----\r\n");

    if (u4IfIndex != VLAN_INIT_VAL)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != VLAN_INIT_VAL)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_FDB_ID_PRESENT;
    }

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;
    }

    /* Displaying specific entries of Unicast and Multicast tables */
    CliArgs.u4VlanIdFdbId1 = VLAN_INIT_VAL;
    CliArgs.u4Port1 = u4IfIndex;
    CliArgs.u4Status1 = VLAN_INIT_VAL;
    CliArgs.pu1MacAddr1 = pu1MacAddr;
    CliArgs.pu1Ports = NULL;

    if (StartVlanId != VLAN_INIT_VAL)
    {
        if (VlanCliAddFidListScanAndPrint (CliHandle, u4ContextId,
                                           StartVlanId, LastVlanId,
                                           u4Mask, &CliArgs, &i4Count,
                                           VLAN_DOT1D_SCAN_PRINT_STMCAST_TABLE)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        VlanCliDot1dScanAndPrintStMcastTable (CliHandle, u4ContextId,
                                              VlanCliCompareArgs, u4Mask,
                                              &CliArgs, VLAN_TRUE, &i4Count);

    }

    if (i4Count == -1)
    {
        /* User Termination is provided */
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\r\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDynamicMcastMacAddrTable ()                */
/*                                                                       */
/* DESCRIPTION      : This function displays learnt Group entries. If    */
/*                    the arguments MacAddr, VlanId and IfIndex are      */
/*                    provided then entries matching these arguments     */
/*                    alone will be displayed.                           */
/*                                                                       */
/* INPUT            : CliHandle  - Cli Context Handle                    */
/*                    u4ContextId - Context Identifier                   */
/*                    pu1MacAddr - Mac Address (unicast or multicast)    */
/*                    VlanId     - Vlan Identifier                       */
/*                    u4IfIndex  - Port Identifier                       */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDynamicMcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                  tVlanId LastVlanId, UINT4 u4IfIndex)
{
    UINT4               u4Mask = 0;
    INT4                i4Count = 0;
    UINT4               u4IsMcastScanRequired = VLAN_TRUE;
    UINT2               u2Index = 0;
    tVlanCliArgs        CliArgs;
    tVlanMacCmpFn       pMacCmpFn;

    if (u4IfIndex != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_PORT_PRESENT;
    }

    if (StartVlanId != 0)
    {
        u4Mask = u4Mask | VLAN_CLI_VLAN_ID_PRESENT;
    }

    CliPrintf (CliHandle, "\r\nVlan    Mac Address         Type");
#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "     ConnectionId        PwIndex  Ports");
#else
    CliPrintf (CliHandle, "     ConnectionId       Ports");
#endif
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "----    -----------         ----");
#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "     ------------        -------  -----");
#else
    CliPrintf (CliHandle, "     -----------        -----");
#endif
    CliPrintf (CliHandle, "\r\n");

    if (pu1MacAddr != NULL)
    {
        u4Mask = u4Mask | VLAN_CLI_MAC_ADDR_PRESENT;

        if ((VLAN_IS_BCASTADDR (pu1MacAddr) == VLAN_TRUE)
            || (VLAN_IS_MCASTADDR (pu1MacAddr) == VLAN_TRUE))
        {
            /* Multicast or Broadcast Mac address */
            if (StartVlanId != 0)
            {
                /* 
                 * VlanId and Mac Address are provided, so we can obtain
                 * an unique entry. So need not scan the entire table.
                 */
                u4IsMcastScanRequired = VLAN_FALSE;
            }
        }
    }
    if (u4IsMcastScanRequired == VLAN_TRUE)
    {
        if (u4Mask == 0)
        {
            /* 
             * Displaying the entire Unicast and Multicast tables, hence
             * initialising tha fields of CliArgs to 0.
             */
            CliArgs.u4VlanIdFdbId1 = 0;
            CliArgs.u4Port1 = 0;
            CliArgs.u4Status1 = 0;
            CliArgs.pu1MacAddr1 = NULL;
            CliArgs.pu1Ports = NULL;

            /* 
             * None of the arguments is present. So all the entries must be
             * displayed. The function pointer (comparison function) that is 
             * passed to ScanAndPrint routine, determines if the entry needs 
             * to be displayed or not. So in this case, we pass NULL as the 
             * comparison function.
             */
            pMacCmpFn = NULL;
        }
        else
        {
            /* Displaying specific entries of Unicast and Multicast tables */
            CliArgs.u4VlanId1 = (UINT4) StartVlanId;
            CliArgs.u4Port1 = u4IfIndex;
            CliArgs.u4Status1 = 0;
            CliArgs.pu1MacAddr1 = pu1MacAddr;
            CliArgs.pu1Ports = NULL;
            CliArgs.u4VlanId2 = (UINT4) LastVlanId;

            pMacCmpFn = VlanCliCompareArgs;
        }

        VlanCliScanAndPrintGroupTable (CliHandle, u4ContextId, pMacCmpFn,
                                       u4Mask, &CliArgs, VLAN_TRUE,
                                       VLAN_TRUE, &i4Count);
    }
    else
    {
        for (u2Index = StartVlanId; u2Index <= LastVlanId; u2Index++)
        {

            ShowVlanMcastEntryVlanMacAddr (CliHandle, u4ContextId, u2Index,
                                           pu1MacAddr, u4IfIndex,
                                           VLAN_TRUE, &i4Count);
        }
    }

    if (i4Count == -1)
    {
        /* User Termination is provided */
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTotal Mac Addresses displayed: %d\r\n\n",
               i4Count);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanFdbEntryVlanMacAddr ()                     */
/*                                                                       */
/* DESCRIPTION      : This function displays Fdb entry associated with   */
/*                    the given Fdb Id and Mac Addr. If u4Port is not 0  */
/*                    and if Mac Addr is not learnt on this port, then   */
/*                    the entry is not displayed.                        */
/*                                                                       */
/* INPUT            : CliHandle         - Cli Context Handle             */
/*                    u4ContextId       - Context Identifier             */
/*                    u4FdbId           - Fdb Identifier                 */
/*                    VlanId              Vlan Identifier                */
/*                    MacAddr           - Mac Address                    */
/*                    u4Port            - Port Identifier                */
/*                    u4DispDynamicOnly - If set to VLAN_TRUE, dynamic   */
/*                                        entries alone will be set      */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                    pi4Count   - Count of the number of entries        */
/*                                 displayed by this function. This      */
/*                                 must NOT be assigned. It must be      */
/*                                 incremented. If display termination   */
/*                                 occurs (through page break) then this */
/*                                 counter is set to -1.                 */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanFdbEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4FdbId, tVlanId VlanId, tMacAddr MacAddr,
                             UINT4 u4Port, UINT4 u4DispDynamicOnly,
                             INT4 *pi4Count)
{
    UINT4               u4FdbPort = 0;
#ifdef MPLS_WANTED
    UINT4               u4FdbPwIndex = 0;
#endif
    UINT1               u1VlanLearningType;
    UINT4               u4Status = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1ConnIdString[VLAN_CLI_MAX_MAC_STRING_SIZE];
    MEMSET (au1ConnIdString, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);

    UNUSED_PARAM (VlanId);

    if (nmhGetFsDot1qTpFdbPort (u4ContextId, u4FdbId,
                                MacAddr, (INT4 *) &u4FdbPort) == SNMP_SUCCESS)
    {
        if (u4Port != 0)
        {
            if (u4FdbPort != u4Port)
            {
                return CLI_SUCCESS;
            }
        }
        nmhGetFsDot1qTpFdbStatus (u4ContextId, u4FdbId,
                                  MacAddr, (INT4 *) &u4Status);

        if (u4DispDynamicOnly == VLAN_TRUE)
        {
            /* Display the entry only, if it is purely learnt */
            if (u4Status != VLAN_FDB_LEARNT)
            {
                return CLI_SUCCESS;
            }
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        u1VlanLearningType = VlanGetVlanLearningMode ();
        VlanReleaseContext ();

        if (u1VlanLearningType == VLAN_SHARED_LEARNING)
        {
            CliPrintf (CliHandle, "\rAll     ");
        }
        else
        {
            CliPrintf (CliHandle, "%-8d", u4FdbId);
        }

        PrintMacAddress (MacAddr, au1String);
        CliPrintf (CliHandle, "%s", au1String);

        switch (u4Status)
        {
            case VLAN_FDB_OTHER:
                CliPrintf (CliHandle, "Other    ");
                break;
            case VLAN_FDB_LEARNT:
                CliPrintf (CliHandle, "Learnt   ");
                break;
            case VLAN_FDB_SELF:
                CliPrintf (CliHandle, "Self     ");
                break;
            case VLAN_FDB_MGMT:
                CliPrintf (CliHandle, "Static   ");
                break;
            default:
                CliPrintf (CliHandle, "Unknown  ");
                break;
        }

        (*pi4Count)++;

        MEMSET (au1ConnIdString, VLAN_CLI_SPACE_ASCII, VLAN_CLI_MAC_ADDR_LEN);
        CliPrintf (CliHandle, "%s", au1ConnIdString);

        MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
        VlanCfaCliGetIfName (u4FdbPort, (INT1 *) au1NameStr);
#ifdef MPLS_WANTED
        nmhGetFsDot1qTpFdbPw (u4ContextId, u4FdbId, MacAddr, &u4FdbPwIndex);
        if (u4FdbPwIndex == 0)
        {
            CliPrintf (CliHandle, "%-9s", "-");
            CliPrintf (CliHandle, "%5s", au1NameStr);
        }
        else if (u4FdbPort == 0)
        {
            CliPrintf (CliHandle, "%-9d", u4FdbPwIndex);
            CliPrintf (CliHandle, "%5s", "-");
        }
#else
        CliPrintf (CliHandle, "%s", au1NameStr);
#endif
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDot1dFdbEntryVlanMacAddr ()                */
/*                                                                       */
/* DESCRIPTION      : This function displays Fdb entry associated with   */
/*                    the given Fdb Id and Mac Addr. If u4Port is not 0  */
/*                    and if Mac Addr is not learnt on this port, then   */
/*                    the entry is not displayed.                        */
/*                                                                       */
/* INPUT            : CliHandle         - Cli Context Handle             */
/*                    u4ContextId       - Context Identifier             */
/*                    u4FdbId           - Fdb Identifier                 */
/*                    VlanId              Vlan Identifier                */
/*                    MacAddr           - Mac Address                    */
/*                    u4Port            - Port Identifier                */
/*                    u4DispDynamicOnly - If set to VLAN_TRUE, dynamic   */
/*                                        entries alone will be set      */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                    pi4Count   - Count of the number of entries        */
/*                                 displayed by this function. This      */
/*                                 must NOT be assigned. It must be      */
/*                                 incremented. If display termination   */
/*                                 occurs (through page break) then this */
/*                                 counter is set to -1.                 */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDot1dFdbEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT4 u4FdbId, tVlanId VlanId,
                                  tMacAddr MacAddr,
                                  UINT4 u4Port, UINT4 u4DispDynamicOnly,
                                  INT4 *pi4Count)
{
    UINT4               u4FdbPort = VLAN_INIT_VAL;
    UINT4               u4Status = VLAN_INIT_VAL;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    if (nmhGetDot1dTpFdbPort (MacAddr, (INT4 *) &u4FdbPort) == SNMP_SUCCESS)
    {
        nmhGetDot1dTpFdbStatus (MacAddr, (INT4 *) &u4Status);

        /* Check whether the port fetched from db matches - only for 
         * learnt ports*/
        if (u4Status == VLAN_FDB_LEARNT)
        {
            if ((u4Port != VLAN_INIT_VAL) && (u4Port != u4FdbPort))
            {
                VlanReleaseContext ();
                return CLI_SUCCESS;
            }
        }

        if (u4DispDynamicOnly == VLAN_TRUE)
        {
            /* Display the entry only, if it is purely learnt */
            if (u4Status != VLAN_FDB_LEARNT)
            {
                VlanReleaseContext ();
                return CLI_SUCCESS;
            }
        }

        PrintMacAddress (MacAddr, au1String);
        CliPrintf (CliHandle, "%s\t", au1String);

        switch (u4Status)
        {
            case VLAN_FDB_OTHER:
                CliPrintf (CliHandle, "Other    ");
                break;
            case VLAN_FDB_LEARNT:
                CliPrintf (CliHandle, "Learnt   ");
                break;
            case VLAN_FDB_SELF:
                CliPrintf (CliHandle, "Self     ");
                break;
            case VLAN_FDB_MGMT:
                CliPrintf (CliHandle, "Static   ");
                break;
            default:
                CliPrintf (CliHandle, "Unknown  ");
                break;
        }

        (*pi4Count)++;

        MEMSET (au1NameStr, VLAN_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
        VlanCfaCliGetIfName (u4FdbPort, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "%s", au1NameStr);
        CliPrintf (CliHandle, "\r\n");
    }
    VlanReleaseContext ();
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanMcastEntryVlanMacAddr ()                   */
/*                                                                       */
/* DESCRIPTION      : This function displays Group entry associated      */
/*                    with the given Vlan Id and Mac Address. If u4Port  */
/*                    is not 0 and if that port is not a member of the   */
/*                    Multicast Group, then the entry is not displayed.  */
/*                                                                       */
/* INPUT            : CliHandle         - Cli Context Handle             */
/*                    u4ContextId       - Context Identifier             */
/*                    VlanId            - Vlan Identifier                */
/*                    pu1MacAddr        - Mac Address                    */
/*                    u4Port            - Port Identifier                */
/*                    u4DispDynamicOnly - If set to VLAN_TRUE, dynamic   */
/*                                        entries alone will be set      */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                    pi4Count   - Count of the number of entries        */
/*                                 displayed by this function. This      */
/*                                 must NOT be assigned. It must be      */
/*                                 incremented. If display termination   */
/*                                 occurs (through page break) then this */
/*                                 counter is set to -1.                 */
/*                                                                       */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanMcastEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4VlanId, UINT1 *pu1MacAddr,
                               UINT4 u4Port, UINT4 u4DispDynamicOnly,
                               INT4 *pi4Count)
{
    INT4                i4RetVal = VLAN_FALSE;
    INT4                i4PortType;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tPortList          *pEgressPorts = NULL;
    tVlanCurrEntry     *pCurrEntry;
    tVlanGroupEntry    *pGroupEntry;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;
    UINT1               u1StRefCount;
    UINT1               au1ConnIdString[VLAN_CLI_MAX_MAC_STRING_SIZE];

    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pEgressPorts, 0, sizeof (tPortList));

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            u4PrvIfIndex = u4IfIndex;

            if (nmhGetFsDot1qTpGroupIsLearnt (u4ContextId, u4VlanId,
                                              pu1MacAddr, u4IfIndex,
                                              &i4PortType) == SNMP_FAILURE)
            {
                if (VlanGetNextPortInContext
                    (u4ContextId, u4PrvIfIndex, &u4IfIndex) != VLAN_SUCCESS)
                {
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    return CLI_FAILURE;
                }

                continue;
            }

            if (i4PortType == VLAN_SNMP_TRUE)
            {
                VLAN_SET_MEMBER_PORT ((*pEgressPorts), u4IfIndex);
            }

        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrvIfIndex, &u4IfIndex) == VLAN_SUCCESS);
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_SUCCESS;
    }
    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);
    if (pCurrEntry == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_FAILURE;
    }
    pGroupEntry = VlanGetGroupEntry (pu1MacAddr, pCurrEntry);
    if (pGroupEntry == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_FAILURE;
    }
    u1StRefCount = pGroupEntry->u1StRefCount;

    VlanReleaseContext ();

    if ((u4DispDynamicOnly == VLAN_TRUE) && (u1StRefCount != 0))
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_SUCCESS;
    }

    /* Do the check only if Port is given by user */
    if (u4Port != 0)
    {
        VLAN_IS_MEMBER_PORT ((*pEgressPorts), u4Port, i4RetVal);

        if (i4RetVal == VLAN_FALSE)
        {
            /* 
             * u4Port is not a member of Multicast Group, the criterion
             * is not satisfied and hence do not display anything.
             */
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%-8d", u4VlanId);

    PrintMacAddress (pu1MacAddr, au1String);
    CliPrintf (CliHandle, "%s", au1String);

    if (u1StRefCount != 0)
    {
        CliPrintf (CliHandle, "Static   ");
    }
    else
    {
        CliPrintf (CliHandle, "Learnt   ");
    }

    MEMSET (au1ConnIdString, VLAN_CLI_SPACE_ASCII, VLAN_CLI_MAC_ADDR_LEN);
    CliPrintf (CliHandle, "%s", au1ConnIdString);

    SnmpEgressPorts.pu1_OctetList = *pEgressPorts;
    SnmpEgressPorts.i4_Length = sizeof (tPortList);

#ifdef MPLS_WANTED
    CliPrintf (CliHandle, "%-8s", "-");
#endif
    CliOctetToIfName (CliHandle,
                      NULL,
                      &SnmpEgressPorts,
                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                      sizeof (tPortList), 45, &u4PagingStatus, 3);
    (*pi4Count)++;

    if (u4PagingStatus == CLI_FAILURE)
    {
        /* 
         * User pressed 'q' at the "more" prompt, no more print required.
         * 
         * Setting the Count to -1 will result in not displaying the
         * count of the entries.
         */
        *pi4Count = -1;
    }

    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME    : ShowVlanDot1dMcastEntryVlanMacAddr ()              */
/*                                                                       */
/* DESCRIPTION      : This function displays Group entry associated      */
/*                    with the given Vlan Id and Mac Address. If u4Port  */
/*                    is not 0 and if that port is not a member of the   */
/*                    Multicast Group, then the entry is not displayed.  */
/*                                                                       */
/* INPUT            : CliHandle         - Cli Context Handle             */
/*                    u4ContextId       - Context Identifier             */
/*                    VlanId            - Vlan Identifier                */
/*                    pu1MacAddr        - Mac Address                    */
/*                    u4Port            - Port Identifier                */
/*                    u4DispDynamicOnly - If set to VLAN_TRUE, dynamic   */
/*                                        entries alone will be set      */
/*                                                                       */
/* OUTPUT           : CliHandle  - Contains error messages               */
/*                    pi4Count   - Count of the number of entries        */
/*                                 displayed by this function. This      */
/*                                 must NOT be assigned. It must be      */
/*                                 incremented. If display termination   */
/*                                 occurs (through page break) then this */
/*                                 counter is set to -1.                 */
/*                                                                       */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*************************************************************************/
INT4
ShowVlanDot1dMcastEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                                    UINT4 u4VlanId, UINT1 *pu1MacAddr,
                                    UINT4 u4Port, UINT4 u4DispDynamicOnly,
                                    INT4 *pi4Count)
{
    INT4                i4RetVal = VLAN_INIT_VAL;
    INT4                i4PortType = VLAN_INIT_VAL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    UINT4               u4PrvIfIndex = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tPortList          *pEgressPorts = NULL;
    tVlanCurrEntry     *pCurrEntry;
    tVlanGroupEntry    *pGroupEntry;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;
    UINT1               u1StRefCount = VLAN_INIT_VAL;

    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pEgressPorts, VLAN_INIT_VAL, sizeof (tPortList));

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            u4PrvIfIndex = u4IfIndex;

            if (nmhGetFsDot1qTpGroupIsLearnt (u4ContextId, u4VlanId,
                                              pu1MacAddr, u4IfIndex,
                                              &i4PortType) == SNMP_FAILURE)
            {
                if (VlanGetNextPortInContext
                    (u4ContextId, u4PrvIfIndex, &u4IfIndex) != VLAN_SUCCESS)
                {
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    return CLI_FAILURE;
                }

                continue;
            }

            if (i4PortType == VLAN_SNMP_TRUE)
            {
                VLAN_SET_MEMBER_PORT ((*pEgressPorts), u4IfIndex);
            }

        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrvIfIndex, &u4IfIndex) == VLAN_SUCCESS);
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_SUCCESS;
    }
    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);
    if (pCurrEntry == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_FAILURE;
    }
    pGroupEntry = VlanGetGroupEntry (pu1MacAddr, pCurrEntry);
    if (pGroupEntry == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_FAILURE;
    }
    u1StRefCount = pGroupEntry->u1StRefCount;

    VlanReleaseContext ();

    if ((u4DispDynamicOnly == VLAN_TRUE) && (u1StRefCount != VLAN_INIT_VAL))
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return CLI_SUCCESS;
    }

    /* Do the check only if Port is given by user */
    if (u4Port != VLAN_INIT_VAL)
    {
        VLAN_IS_MEMBER_PORT ((*pEgressPorts), u4Port, i4RetVal);

        if (i4RetVal == VLAN_FALSE)
        {
            /* 
             * u4Port is not a member of Multicast Group, the criterion
             * is not satisfied and hence do not display anything.
             */
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            return CLI_SUCCESS;
        }
    }
    PrintMacAddress (pu1MacAddr, au1String);
    CliPrintf (CliHandle, "%s", au1String);

    if (u1StRefCount != VLAN_INIT_VAL)
    {
        CliPrintf (CliHandle, "Static   ");
    }
    else
    {
        CliPrintf (CliHandle, "Learnt   ");
    }

    SnmpEgressPorts.pu1_OctetList = *pEgressPorts;
    SnmpEgressPorts.i4_Length = sizeof (tPortList);

    CliOctetToIfName (CliHandle,
                      NULL,
                      &SnmpEgressPorts,
                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                      sizeof (tPortList), 45, &u4PagingStatus, 3);
    (*pi4Count)++;

    if (u4PagingStatus == CLI_FAILURE)
    {
        /* 
         * User pressed 'q' at the "more" prompt, no more print required.
         * 
         * Setting the Count to -1 will result in not displaying the
         * count of the entries.
         */
        *pi4Count = -1;
    }

    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    return CLI_SUCCESS;
}

PRIVATE VOID
VlanConvertVlanListToVlan (tCliHandle CliHandle, UINT1 *au1VlanList)
{
    UINT4               u4ByteInd;
    UINT4               u4BitCount;
    UINT4               u4VlanCount = 0;
    UINT1               u1VlanFlag = 0;
    tVlanId             VlanId;

    for (u4ByteInd = 0; u4ByteInd < VLAN_LIST_LEN; u4ByteInd++)
    {
        if (au1VlanList[u4ByteInd] != 0)
        {
            u1VlanFlag = au1VlanList[u4ByteInd];

            for (u4BitCount = 0; (u4BitCount < VLAN_VLANS_PER_BYTE &&
                                  u1VlanFlag != 0); u4BitCount++)
            {
                if (u1VlanFlag & VLAN_BIT8)
                {
                    u4VlanCount++;
                    VlanId = (tVlanId) ((u4ByteInd * VLAN_VLANS_PER_BYTE)
                                        + u4BitCount + 1);

                    if (!(u4VlanCount % 5))
                    {

                        CliPrintf (CliHandle, "\r\n          %d,", VlanId);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%d,", VlanId);
                    }

                }

                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
    }
    return;
}

/*************************************************************************/
/* FUNCTION NAME  : VlanCliScanAndPrintFdbTable ()                       */
/*                                                                       */
/* DESCRIPTION    : This function displays Fdb entries that match        */
/*                  required criteria.                                   */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                  pMacCmpFn     - Entry comparison function which      */
/*                                  returns CLI_SUCCESS if the required  */
/*                                  criteria is satisfied, CLI_FAILURE   */
/*                                  otherwise                            */
/*                  u4ArgsMask    - Bit mask indicating the presence     */
/*                                  the arguments (FdbId, MacAddr,       */
/*                                  Port and Status)                     */
/*                  pCliArgs      - Structure containing FdbId, MacAddr, */
/*                                  Port and Status which determine if   */
/*                                  the entry needs to be displayed.     */
/*                  u4DisplayFlag - Entries will be displayed only if    */
/*                                  this flag is set to VLAN_TRUE        */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
VlanCliScanAndPrintFdbTable (tCliHandle CliHandle, UINT4 u4ContextId,
                             tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                             tVlanCliArgs * pCliArgs,
                             UINT4 u4DisplayFlag, INT4 *pi4Count)
{
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4CurrentContextId;
    INT4                i4NextContextId;
    UINT4               u4CurrFdbId = 0;
    UINT4               u4NextFdbId = 0;
    UINT4               u4FdbPort = 0;
    UINT4               u4Status = 0;
#ifdef MPLS_WANTED
    UINT4               u4FdbPwIndex = 0;
    UINT1               u1IfType = VLAN_ZERO;
#endif
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1VlanLearningType;
    UINT1               au1ConnIdString[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tVlanId             VlanId;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tMacAddr            au1ConnectionId;
    tMacAddr            BaseMacAddr;

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1ConnectionId, 0, sizeof (tMacAddr));
    MEMSET (au1ConnIdString, VLAN_CLI_SPACE_ASCII, VLAN_CLI_MAC_ADDR_LEN);

    if (pMacCmpFn != NULL)
    {
        /*start From the given index */
        u4CurrFdbId = (pCliArgs->u4VlanIdFdbId1);
    }

    if (nmhGetNextIndexFsDot1qTpFdbTable
        ((INT4) u4ContextId, &i4NextContextId, u4CurrFdbId, &u4NextFdbId,
         CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
    {
#ifdef SW_LEARNING
#ifdef ICCH_WANTED
        if (u4DisplayFlag == VLAN_TRUE)
        {
            if (CLI_FAILURE ==
                VlanCliPrintRemoteFdbTable (CliHandle, u4ContextId, pi4Count))
            {
                return CLI_FAILURE;
            }
        }
#endif
#endif

        return CLI_SUCCESS;
    }

    if (u4ContextId != (UINT4) i4NextContextId)
    {
        return CLI_SUCCESS;
    }

    i4NextContextId = (INT4) u4ContextId;

    do
    {
        u4Status = 0;
        u4FdbPort = 0;
        MEMSET (au1ConnectionId, 0, sizeof (tMacAddr));

        nmhGetFsDot1qTpFdbPort (u4ContextId, u4NextFdbId,
                                NextMacAddr, (INT4 *) &u4FdbPort);
        nmhGetFsDot1qTpFdbStatus (u4ContextId, u4NextFdbId,
                                  NextMacAddr, (INT4 *) &u4Status);
        nmhGetFsMIDot1qFutureConnectionIdentifier (u4ContextId,
                                                   u4NextFdbId,
                                                   NextMacAddr,
                                                   &au1ConnectionId);

        if (pMacCmpFn != NULL)
        {
            pCliArgs->u4VlanIdFdbId2 = u4NextFdbId;
            pCliArgs->u4Port2 = u4FdbPort;
            pCliArgs->u4Status2 = u4Status;
            pCliArgs->pu1MacAddr2 = NextMacAddr;
            pCliArgs->pu1Ports = NULL;

            i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
        }

        if (i4RetVal == CLI_SUCCESS)
        {
            if (u4DisplayFlag == VLAN_TRUE)
            {
                if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
                u1VlanLearningType = VlanGetVlanLearningMode ();
                VlanReleaseContext ();

                if (u1VlanLearningType != VLAN_SHARED_LEARNING)
                {
                    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                    VlanGetVlanIdFromFdbId (u4NextFdbId, &VlanId);
                    VlanReleaseContext ();
                }

                if (u1VlanLearningType == VLAN_SHARED_LEARNING)
                {
                    CliPrintf (CliHandle, "All     ");
                }
                else
                {
                    CliPrintf (CliHandle, "%-8d", u4NextFdbId);
                }

                PrintMacAddress (NextMacAddr, au1String);
                CliPrintf (CliHandle, "%s", au1String);

                /* When HwBaseMacInMacTabSup is supported, 
                 * update "SELF" in show mac-address table for switch mac address 
                 */
                if (ISS_HW_SUPPORTED ==
                    IssGetHwCapabilities (ISS_HW_SELF_MAC_IN_MAC_ADDR_TAB))
                {
                    CfaGetSysMacAddress (BaseMacAddr);

                    if ((u4FdbPort == 0) &&
                        (MEMCMP (NextMacAddr, BaseMacAddr, CFA_ENET_ADDR_LEN) ==
                         0))
                    {
                        u4Status = VLAN_FDB_SELF;
                    }
                }
                else
                {
                    UNUSED_PARAM (BaseMacAddr);
                }

                switch (u4Status)
                {
                    case VLAN_FDB_OTHER:
                        CliPrintf (CliHandle, "Other    ");
                        break;
                    case VLAN_FDB_LEARNT:
                        CliPrintf (CliHandle, "Learnt   ");
                        break;
                    case VLAN_FDB_SELF:
                        CliPrintf (CliHandle, "Self     ");
                        break;
                    case VLAN_FDB_MGMT:
                        CliPrintf (CliHandle, "Static   ");
                        break;
                    default:
                        CliPrintf (CliHandle, "Unknown  ");
                        break;
                }

                PrintMacAddress (au1ConnectionId, au1ConnIdString);

                if (VLAN_ARE_MAC_ADDR_EQUAL (au1ConnectionId,
                                             gNullMacAddress) == VLAN_TRUE)
                {
                    MEMSET (au1ConnIdString, VLAN_CLI_SPACE_ASCII,
                            VLAN_CLI_MAC_ADDR_LEN);
                }
                CliPrintf (CliHandle, "%s", au1ConnIdString);

                MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
                VlanCfaCliGetIfName (u4FdbPort, (INT1 *) au1NameStr);
#ifdef MPLS_WANTED
                VlanCfaGetIfType (u4FdbPort, &u1IfType);
                nmhGetFsDot1qTpFdbPw (u4ContextId, u4NextFdbId,
                                      NextMacAddr, &u4FdbPwIndex);
                if (u4FdbPwIndex == 0)
                {
                    CliPrintf (CliHandle, "%-9s", "-");
                    u4PagingStatus = CliPrintf (CliHandle, "%5s", au1NameStr);
                }
                else if ((u4FdbPort == 0) || (u1IfType == CFA_PSEUDO_WIRE))
                {
                    CliPrintf (CliHandle, "%-9d", u4FdbPwIndex);
                    CliPrintf (CliHandle, "%5s", "-");
                }
#ifdef NPAPI_WANTED
                /* For BCM NPAPI, the Fdb port will be non-zero always */
                else if (u4FdbPwIndex != FNP_ZERO)
                {
                    if (!
                        ((u4FdbPwIndex < CFA_MIN_PSW_IF_INDEX)
                         || (u4FdbPwIndex > CFA_MAX_PSW_IF_INDEX)))

                    {
                        VlanCfaCliGetIfName (u4FdbPwIndex, (INT1 *) au1NameStr);
                        CliPrintf (CliHandle, "%-9s", au1NameStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-9d", u4FdbPwIndex);

                    }

                    CliPrintf (CliHandle, "%5s", "-");
                }
#endif
#else
                /* When HwUnicastMacLearningLimitSup is supported, 
                 * Display paging status as au1NameStr 
                 */
                if (ISS_HW_SUPPORTED ==
                    IssGetHwCapabilities (ISS_HW_SELF_MAC_IN_MAC_ADDR_TAB))
                {
                    if (u4FdbPort == 0)
                    {
                        u4PagingStatus = CliPrintf (CliHandle, "%-9s", "-");
                    }
                    else
                    {
                        u4PagingStatus =
                            CliPrintf (CliHandle, "%-9s", au1NameStr);
                    }
                }
                else
                {
                    u4PagingStatus = CliPrintf (CliHandle, "%-9s", au1NameStr);
                }
#endif
                CliPrintf (CliHandle, "\r\n");
            }

            (*pi4Count)++;
        }
        u4CurrFdbId = u4NextFdbId;
        i4CurrentContextId = i4NextContextId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));

        if (nmhGetNextIndexFsDot1qTpFdbTable
            (i4CurrentContextId, &i4NextContextId,
             u4CurrFdbId, &u4NextFdbId,
             CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit.
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);
#ifdef SW_LEARNING
#ifdef ICCH_WANTED
    if (u4DisplayFlag == VLAN_TRUE)
    {
        if (CLI_FAILURE == VlanCliPrintRemoteFdbTable (CliHandle, u4ContextId,
                                                       pi4Count))
        {
            return CLI_FAILURE;
        }
    }
#endif
#endif
    return CLI_SUCCESS;
}

#ifdef ICCH_WANTED
/*************************************************************************/
/* FUNCTION NAME  : VlanCliPrintRemoteFdbTable ()                        */
/*                                                                       */
/* DESCRIPTION    : This function displays  the remote Fdb entries       */
/*                  the are synced from the remote Node.                 */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
VlanCliPrintRemoteFdbTable (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 *pi4Count)
{
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ConnIdString[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tVlanFdbInfo        VlanFdbTable;
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4DefPort = 1;
    UINT4               u4Count = 0;
    UINT2               u2PortChannel = 0;

    MEMSET (&VlanFdbTable, 0, sizeof (tVlanFdbInfo));
    MEMSET (au1String, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);
    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1ConnIdString, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);

    MEMSET (&VlanFdbTable, 0, sizeof (tVlanFdbInfo));
    VlanFdbTable.u2RemoteId = VLAN_REMOTE_FDB;

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    pVlanFdbInfo =
        RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                       (tRBElem *) (&VlanFdbTable), NULL);
    VlanReleaseContext ();
    while (NULL != pVlanFdbInfo)
    {
        MEMSET (au1ConnIdString, VLAN_CLI_SPACE_ASCII, VLAN_CLI_MAC_ADDR_LEN);
        MEMSET (au1String, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, "%-8d", pVlanFdbInfo->u4FdbId);

        PrintMacAddress (pVlanFdbInfo->MacAddr, au1String);
        CliPrintf (CliHandle, "%s", au1String);

        CliPrintf (CliHandle, "Remote   ");

        PrintMacAddress (pVlanFdbInfo->ConnectionId, au1ConnIdString);

        if (VLAN_ARE_MAC_ADDR_EQUAL (pVlanFdbInfo->ConnectionId,
                                     gNullMacAddress) == VLAN_TRUE)
        {
            MEMSET (au1ConnIdString, VLAN_CLI_SPACE_ASCII,
                    VLAN_CLI_MAC_ADDR_LEN);
        }
        CliPrintf (CliHandle, "%s", au1ConnIdString);
        MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

#ifdef MPLS_WANTED
        if (pVlanFdbInfo->u4PwVcIndex != L2VPN_ZERO)
        {
            CliPrintf (CliHandle, "%d", pVlanFdbInfo->u4PwVcIndex);
            CliPrintf (CliHandle, "%13s", "-");
        }
        else
        {
            CliPrintf (CliHandle, "%-9s", "-");
        }
#endif
        if (pVlanFdbInfo->u4PwVcIndex == L2VPN_ZERO)
        {
            /* pVlanFdbInfo->au1Pad (2 Bytes) will be populated with the 
             * port-channel number if it is a port-channel else for normal
             * ports the value would be zero.
             */
            MEMCPY (&u2PortChannel, pVlanFdbInfo->au1Pad, sizeof (UINT2));

            if (0 == u2PortChannel)
            {
                u4Count = 0;
                CfaCliGetIfName (u4DefPort, (INT1 *) au1IfName);
                while (au1IfName[u4Count] != '\0')
                {
                    if (au1IfName[u4Count] == '/')
                    {
                        au1IfName[u4Count] = '\0';
                        break;
                    }
                    u4Count++;
                }
                STRNCPY (au1NameStr, au1IfName, STRLEN (au1IfName));
                SPRINTF ((CHR1 *) au1NameStr + STRLEN (au1NameStr), "/%d",
                         pVlanFdbInfo->u2Port);
            }
            else
            {
                SPRINTF ((CHR1 *) au1NameStr, "po%d ", u2PortChannel);
            }
        }

        u4PagingStatus = CliPrintf (CliHandle, "%5s", au1NameStr);
        CliPrintf (CliHandle, "\r\n");
        (*pi4Count)++;

        VlanFdbTable.u2RemoteId = VLAN_REMOTE_FDB;
        VlanFdbTable.u4FdbId = pVlanFdbInfo->u4FdbId;
        MEMCPY (VlanFdbTable.MacAddr, pVlanFdbInfo->MacAddr, sizeof (tMacAddr));

        pVlanFdbInfo =
            RBTreeGetNext (VLAN_CURR_CONTEXT_PTR ()->VlanFdbInfo,
                           (tRBElem *) (&VlanFdbTable), NULL);

        if (pVlanFdbInfo != NULL)
        {
            if (pVlanFdbInfo->u2RemoteId != VLAN_REMOTE_FDB)
            {
                break;
            }
        }
        VlanReleaseContext ();
        if (u4PagingStatus == CLI_FAILURE)
        {
            /*
             * User pressed 'q' at more prompt, no more print required, exit.
             *
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            break;
        }
    }
    return CLI_SUCCESS;
}
#endif

/*************************************************************************/
/* FUNCTION NAME  : VlanCliScanAndPrintDot1dFdbTable ()                  */
/*                                                                       */
/* DESCRIPTION    : This function displays Fdb entries that match        */
/*                  required criteria.                                   */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                  pMacCmpFn     - Entry comparison function which      */
/*                                  returns CLI_SUCCESS if the required  */
/*                                  criteria is satisfied, CLI_FAILURE   */
/*                                  otherwise                            */
/*                  u4ArgsMask    - Bit mask indicating the presence     */
/*                                  the arguments (FdbId, MacAddr,       */
/*                                  Port and Status)                     */
/*                  pCliArgs      - Structure containing FdbId, MacAddr, */
/*                                  Port and Status which determine if   */
/*                                  the entry needs to be displayed.     */
/*                  u4DisplayFlag - Entries will be displayed only if    */
/*                                  this flag is set to VLAN_TRUE        */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
VlanCliScanAndPrintDot1dFdbTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                  tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                                  tVlanCliArgs * pCliArgs,
                                  UINT4 u4DisplayFlag, INT4 *pi4Count)
{
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4FdbPort = VLAN_INIT_VAL;
    UINT4               u4Status = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;

    MEMSET (CurrMacAddr, VLAN_INIT_VAL, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetNextIndexDot1dTpFdbTable
        (CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetDot1dTpFdbPort (NextMacAddr, (INT4 *) &u4FdbPort);
        nmhGetDot1dTpFdbStatus (NextMacAddr, (INT4 *) &u4Status);

        if (pMacCmpFn != NULL)
        {
            pCliArgs->u4VlanIdFdbId2 = VLAN_DEF_VLAN_ID;
            pCliArgs->u4Port2 = u4FdbPort;
            pCliArgs->u4Status2 = u4Status;
            pCliArgs->pu1MacAddr2 = NextMacAddr;
            pCliArgs->pu1Ports = NULL;

            i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
        }

        if (i4RetVal == CLI_SUCCESS)
        {
            if (u4DisplayFlag == VLAN_TRUE)
            {
                PrintMacAddress (NextMacAddr, au1String);
                CliPrintf (CliHandle, "%s", au1String);

                switch (u4Status)
                {
                    case VLAN_FDB_OTHER:
                        CliPrintf (CliHandle, "Other    ");
                        break;
                    case VLAN_FDB_LEARNT:
                        CliPrintf (CliHandle, "Learnt   ");
                        break;
                    case VLAN_FDB_SELF:
                        CliPrintf (CliHandle, "Self     ");
                        break;
                    case VLAN_FDB_MGMT:
                        CliPrintf (CliHandle, "Static   ");
                        break;
                    default:
                        CliPrintf (CliHandle, "Unknown  ");
                        break;
                }

                MEMSET (au1NameStr, VLAN_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
                VlanCfaCliGetIfName (u4FdbPort, (INT1 *) au1NameStr);
                u4PagingStatus = CliPrintf (CliHandle, "%-9s", au1NameStr);
                CliPrintf (CliHandle, "\r\n");
            }

            (*pi4Count)++;
        }

        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));

        if (nmhGetNextIndexDot1dTpFdbTable
            (CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit.
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    VlanReleaseContext ();
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME  : VlanCliScanAndPrintStUcastTable ()                   */
/*                                                                       */
/* DESCRIPTION    : This function displays Static Unicast table          */
/*                  entries that match the required criteria.            */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                  pMacCmpFn     - Entry comparison function which      */
/*                                  returns CLI_SUCCESS if the required  */
/*                                  criteria is satisfied, CLI_FAILURE   */
/*                                  otherwise                            */
/*                  u4ArgsMask    - Bit mask indicating the presence     */
/*                                  the arguments (FdbId, MacAddr,       */
/*                                  Port and Status)                     */
/*                  pCliArgs      - Structure containing FdbId, MacAddr, */
/*                                  Port and Status which determine if   */
/*                                  the entry needs to be displayed.     */
/*                  u4DisplayFlag - Entries will be displayed only if    */
/*                                  this flag is set to VLAN_TRUE        */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
VlanCliScanAndPrintStUcastTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                 tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                                 tVlanCliArgs * pCliArgs, UINT4 u4DisplayFlag,
                                 INT4 *pi4Count)
{
    INT4                i4Status;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4CurrRcvPort = 0;
    INT4                i4NextRcvPort;
    INT4                i4PortType;
    INT4                i4CurrentContextId;
    INT4                i4NextContextId;
    UINT4               u4CurrFdbId = 0;
    UINT4               u4NextFdbId;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ConnIdStr[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1VlanLearningType;
    tVlanId             VlanId;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tMacAddr            au1ConnectionId;
    tPortList          *pPorts = NULL;
    INT4                i4RowStatus = VLAN_NOT_READY;
    UINT1               u1PbPortType;
    UINT4               u4Isid = 0;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1ConnectionId, 0, sizeof (tMacAddr));
    MEMSET (au1ConnIdStr, VLAN_CLI_SPACE_ASCII, VLAN_CLI_MAC_ADDR_LEN);

    i4CurrentContextId = (INT4) u4ContextId;

    if (pMacCmpFn != NULL)
    {
        /*start From the given index */
        u4CurrFdbId = (pCliArgs->u4VlanIdFdbId1);
    }

    if (nmhGetNextIndexFsDot1qStaticUnicastTable
        (i4CurrentContextId, &i4NextContextId, u4CurrFdbId, &u4NextFdbId,
         CurrMacAddr, &NextMacAddr, 0, &i4NextRcvPort) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if (i4CurrentContextId != i4NextContextId)
    {
        return CLI_SUCCESS;
    }

    pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    do
    {
        MEMSET (*pPorts, 0, sizeof (tPortList));

        nmhGetFsDot1qStaticUnicastRowStatus ((UINT4) i4NextContextId,
                                             u4NextFdbId, NextMacAddr,
                                             i4NextRcvPort, &i4RowStatus);

        if (i4RowStatus == VLAN_ACTIVE)
        {
            if (VlanGetFirstPortInContext ((UINT4) i4NextContextId,
                                           &u4IfIndex) == VLAN_SUCCESS)
            {
                do
                {
                    nmhGetFsDot1qStaticAllowedIsMember
                        ((UINT4) i4NextContextId, u4NextFdbId, NextMacAddr,
                         i4NextRcvPort, u4IfIndex, &i4PortType);

                    if (i4PortType == VLAN_SNMP_TRUE)
                    {
                        /* Added for pseudo wire visibility */
                        VLAN_SET_MEMBER_PORT_LIST ((*pPorts), u4IfIndex);
                    }

                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInContext
                       ((UINT4) i4NextContextId, u4PrvIfIndex,
                        &u4IfIndex) == VLAN_SUCCESS);
            }

            if (pMacCmpFn != NULL)
            {
                pCliArgs->u4VlanIdFdbId2 = u4NextFdbId;
                pCliArgs->u4Port2 = 0;
                pCliArgs->u4Status2 = 0;
                pCliArgs->pu1MacAddr2 = NextMacAddr;
                pCliArgs->pu1Ports = *pPorts;

                i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
            }

            if (i4RetVal == CLI_SUCCESS)
            {
                if (u4DisplayFlag == VLAN_TRUE)
                {
                    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pPorts);
                        return CLI_SUCCESS;
                    }
                    u1VlanLearningType = VlanGetVlanLearningMode ();
                    VlanReleaseContext ();

                    if (u1VlanLearningType != VLAN_SHARED_LEARNING)
                    {
                        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
                        {
                            FsUtilReleaseBitList ((UINT1 *) pPorts);
                            return CLI_SUCCESS;
                        }
                        VlanGetVlanIdFromFdbId (u4NextFdbId, &VlanId);
                        VlanReleaseContext ();
                    }

                    nmhGetFsDot1qStaticUnicastStatus (u4ContextId, u4NextFdbId,
                                                      NextMacAddr,
                                                      i4NextRcvPort, &i4Status);

                    nmhGetFsMIDot1qFutureStaticConnectionIdentifier
                        (u4ContextId, u4NextFdbId, NextMacAddr, i4NextRcvPort,
                         &au1ConnectionId);

                    if (u1VlanLearningType == VLAN_SHARED_LEARNING)
                    {
                        CliPrintf (CliHandle, "All   ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-6d", u4NextFdbId);
                    }

                    PrintMacAddress (NextMacAddr, au1String);
                    CliPrintf (CliHandle, "%s", au1String);

                    if (i4NextRcvPort == 0)
                    {
                        CliPrintf (CliHandle, "%-8s", " ");
                    }
                    else
                    {
                        VlanL2IwfGetPbPortType (i4NextRcvPort, &u1PbPortType);
                        if (u1PbPortType == VLAN_VIRTUAL_INSTANCE_PORT)
                        {
                            VlanPbbGetIsidOfVip (&u4Isid, i4NextRcvPort,
                                                 u4ContextId);
                            CliPrintf (CliHandle, "%-8d", u4Isid);

                        }
                        else
                        {
                            VlanCfaCliGetIfName ((UINT4) i4NextRcvPort,
                                                 (INT1 *) au1NameStr);
                            CliPrintf (CliHandle, "%-8s", au1NameStr);
                        }

                    }

                    switch (i4Status)
                    {
                        case VLAN_OTHER:
                            CliPrintf (CliHandle, "Other         ");
                            break;
                        case VLAN_PERMANENT:
                            CliPrintf (CliHandle, "Permanent     ");
                            break;
                        case VLAN_DELETE_ON_RESET:
                            CliPrintf (CliHandle, "DeleteOnReset ");
                            break;
                        case VLAN_DELETE_ON_TIMEOUT:
                            CliPrintf (CliHandle, "Del-OnTimeout ");
                            break;
                        default:
                            CliPrintf (CliHandle, "Unknown       ");
                            break;
                    }

                    SnmpPorts.pu1_OctetList = *pPorts;
                    SnmpPorts.i4_Length = sizeof (tPortList);

                    PrintMacAddress (au1ConnectionId, au1ConnIdStr);

                    if (VLAN_ARE_MAC_ADDR_EQUAL (au1ConnectionId,
                                                 gNullMacAddress) == VLAN_TRUE)
                    {
                        MEMSET (au1ConnIdStr, VLAN_CLI_SPACE_ASCII,
                                VLAN_CLI_MAC_ADDR_LEN);
                    }
                    CliPrintf (CliHandle, "%s", au1ConnIdStr);

                    CliOctetToIfName (CliHandle,
                                      NULL,
                                      &SnmpPorts,
                                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                      sizeof (tPortList), 48,
                                      &u4PagingStatus, 2);

                }

                (*pi4Count)++;
            }
        }

        i4CurrentContextId = i4NextContextId;
        u4CurrFdbId = u4NextFdbId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
        i4CurrRcvPort = i4NextRcvPort;

        if (nmhGetNextIndexFsDot1qStaticUnicastTable
            (i4CurrentContextId, &i4NextContextId, u4CurrFdbId,
             &u4NextFdbId, CurrMacAddr, &NextMacAddr, i4CurrRcvPort,
             &i4NextRcvPort) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit 
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    FsUtilReleaseBitList ((UINT1 *) pPorts);
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME  : VlanCliDot1dScanAndPrintStUcastTable ()              */
/*                                                                       */
/* DESCRIPTION    : This function displays Static Unicast table          */
/*                  entries in transparent birdging                      */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                  pMacCmpFn     - Entry comparison function which      */
/*                                  returns CLI_SUCCESS if the required  */
/*                                  criteria is satisfied, CLI_FAILURE   */
/*                                  otherwise                            */
/*                  u4ArgsMask    - Bit mask indicating the presence     */
/*                                  the arguments (FdbId, MacAddr,       */
/*                                  Port and Status)                     */
/*                  pCliArgs      - Structure containing FdbId, MacAddr, */
/*                                  Port and Status which determine if   */
/*                                  the entry needs to be displayed.     */
/*                  u4DisplayFlag - Entries will be displayed only if    */
/*                                  this flag is set to VLAN_TRUE        */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
VlanCliDot1dScanAndPrintStUcastTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                      tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                                      tVlanCliArgs * pCliArgs,
                                      UINT4 u4DisplayFlag, INT4 *pi4Count)
{
    INT4                i4Status = VLAN_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4CurrRcvPort = VLAN_INIT_VAL;
    INT4                i4NextRcvPort = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tPortList          *pPorts = NULL;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;

    UNUSED_PARAM (u4ContextId);

    pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (CurrMacAddr, VLAN_INIT_VAL, sizeof (tMacAddr));
    MEMSET (NextMacAddr, VLAN_INIT_VAL, sizeof (tMacAddr));
    MEMSET (pPorts, VLAN_INIT_VAL, sizeof (tPortList));

    SnmpPorts.pu1_OctetList = *pPorts;
    SnmpPorts.i4_Length = sizeof (tPortList);

    if (nmhGetNextIndexDot1dStaticTable
        (CurrMacAddr, &NextMacAddr, VLAN_INIT_VAL,
         &i4NextRcvPort) == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        return CLI_SUCCESS;
    }
    do
    {
        nmhGetDot1dStaticAllowedToGoTo (NextMacAddr, i4NextRcvPort, &SnmpPorts);

        if (pMacCmpFn != NULL)
        {
            pCliArgs->u4Port2 = VLAN_INIT_VAL;
            pCliArgs->u4Status2 = VLAN_INIT_VAL;
            pCliArgs->pu1MacAddr2 = NextMacAddr;
            pCliArgs->pu1Ports = SnmpPorts.pu1_OctetList;

            i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
        }

        if ((i4RetVal == CLI_SUCCESS) && !(VLAN_IS_MCASTADDR (NextMacAddr)))
        {
            if (u4DisplayFlag == VLAN_TRUE)
            {
                nmhGetDot1dStaticStatus (NextMacAddr, i4NextRcvPort, &i4Status);

                PrintMacAddress (NextMacAddr, au1String);
                CliPrintf (CliHandle, "%s", au1String);

                if (i4NextRcvPort == VLAN_INIT_VAL)
                {
                    CliPrintf (CliHandle, "%-8s", " ");
                }
                else
                {
                    VlanCfaCliGetIfName (i4NextRcvPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-8s", au1NameStr);
                }

                switch (i4Status)
                {
                    case VLAN_OTHER:
                        CliPrintf (CliHandle, "Other         ");
                        break;
                    case VLAN_PERMANENT:
                        CliPrintf (CliHandle, "Permanent     ");
                        break;
                    case VLAN_DELETE_ON_RESET:
                        CliPrintf (CliHandle, "DeleteOnReset ");
                        break;
                    case VLAN_DELETE_ON_TIMEOUT:
                        CliPrintf (CliHandle, "Del-OnTimeout ");
                        break;
                    default:
                        CliPrintf (CliHandle, "Unknown       ");
                        break;
                }

                CliOctetToIfName (CliHandle,
                                  NULL,
                                  &SnmpPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList), 48, &u4PagingStatus, 2);

            }
            (*pi4Count)++;
        }

        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
        i4CurrRcvPort = i4NextRcvPort;

        if (nmhGetNextIndexDot1dStaticTable (CurrMacAddr, &NextMacAddr,
                                             i4CurrRcvPort, &i4NextRcvPort)
            == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit 
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    FsUtilReleaseBitList ((UINT1 *) pPorts);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME  : VlanCliScanAndPrintGroupTable                            */
/*                                                                           */
/* DESCRIPTION    : This function displays Group entries that match          */
/*                  required criteria.                                       */
/*                                                                           */
/* INPUT          : CliHandle         - Cli Context Handle                   */
/*                  u4ContextId       - Context Identifier                   */
/*                  pMacCmpFn         - Entry comparison function which      */
/*                                      returns CLI_SUCCESS if the required  */
/*                                      criteria is satisfied, CLI_FAILURE   */
/*                                      otherwise                            */
/*                  u4ArgsMask        - Bit mask indicating the presence     */
/*                                      the arguments (FdbId, MacAddr,       */
/*                                      Port and Status)                     */
/*                  pCliArgs          - Structure containing FdbId, MacAddr, */
/*                                      Port and Status which determine if   */
/*                                      the entry needs to be displayed.     */
/*                  u4DynamicOnly     - If set to VLAN_TRUE, dynamic entries */
/*                                      alone will be chosen.                */
/*                  u4DisplayFlag     - Entries will be displayed only if    */
/*                                      this flag is set to VLAN_TRUE        */
/*                                                                           */
/* OUTPUT         : CliHandle   - Contains error messages                    */
/*                  pi4Count    - Count of the number of entries displayed   */
/*                                by this function. This must NOT be         */
/*                                assigned. It must be incremented. If       */
/*                                display termination occurs (through page   */
/*                                break) then this counter is set to -1.     */
/*                                                                           */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                                  */
/*****************************************************************************/
INT4
VlanCliScanAndPrintGroupTable (tCliHandle CliHandle, UINT4 u4ContextId,
                               tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                               tVlanCliArgs * pCliArgs, UINT4 u4DynamicOnly,
                               UINT4 u4DisplayFlag, INT4 *pi4Count)
{
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4CurrentContextId;
    INT4                i4NextContextId;
    UINT4               u4CurrVlanId = 0;
    UINT4               u4NextVlanId;
    INT4                i4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1ConnId[VLAN_CLI_MAX_MAC_STRING_SIZE + 1];
    UINT1               u1isShowAll = TRUE;
    UINT1               u1Learnt = VLAN_FALSE;
    UINT1               u1StRefCount;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tPortList          *pEgressPorts = NULL;
    tVlanCurrEntry     *pCurrEntry;
    tVlanGroupEntry    *pGroupEntry;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    i4CurrentContextId = (INT4) u4ContextId;

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1ConnId, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);
    MEMSET (au1ConnId, VLAN_CLI_SPACE_ASCII, VLAN_CLI_MAX_MAC_STRING_SIZE - 1);

    if (pMacCmpFn != NULL)
    {
        /*start From the given index */
        u4CurrVlanId = (pCliArgs->u4VlanId1);
    }

    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    while ((nmhGetNextIndexDot1qTpGroupTable (u4CurrVlanId, &u4NextVlanId,
                                              CurrMacAddr, &NextMacAddr)
            != SNMP_FAILURE) && (u1isShowAll == TRUE))

    {
        VlanReleaseContext ();
        u4CurrVlanId = u4NextVlanId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));

        MEMSET (*pEgressPorts, 0, sizeof (tPortList));

        if (nmhGetNextIndexFsDot1qTpGroupTable
            (i4CurrentContextId, &i4NextContextId,
             u4CurrVlanId, &u4NextVlanId, CurrMacAddr, &NextMacAddr, 0,
             &i4IfIndex) != SNMP_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            return CLI_SUCCESS;
        }
        do
        {
            if ((i4CurrentContextId != i4NextContextId) ||
                (u4CurrVlanId != u4NextVlanId) ||
                (VLAN_ARE_MAC_ADDR_EQUAL (CurrMacAddr, NextMacAddr)
                 != VLAN_TRUE))
            {
                break;
            }
            VLAN_SET_MEMBER_PORT_LIST ((*pEgressPorts), (UINT4) i4IfIndex);
            u4PrvIfIndex = (UINT4) i4IfIndex;
        }
        while (nmhGetNextIndexFsDot1qTpGroupTable
               (i4CurrentContextId, &i4NextContextId, u4CurrVlanId,
                &u4NextVlanId, CurrMacAddr, &NextMacAddr, (INT4) u4PrvIfIndex,
                &i4IfIndex) == SNMP_SUCCESS);

        if (pMacCmpFn != NULL)
        {
            pCliArgs->u4VlanId1 = u4CurrVlanId;
            pCliArgs->u4Port2 = 0;
            pCliArgs->u4Status2 = 0;
            /* u4Port2 will not be used, instead pu1Ports will be used */
            pCliArgs->pu1MacAddr2 = CurrMacAddr;
            pCliArgs->pu1Ports = *pEgressPorts;

            i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
        }

        if (i4RetVal == CLI_SUCCESS)
        {

            if (VlanSelectContext ((UINT4) i4CurrentContextId) != VLAN_SUCCESS)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                return CLI_SUCCESS;
            }
            pCurrEntry = VlanGetVlanEntry ((tVlanId) u4CurrVlanId);
            if (NULL == pCurrEntry)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                return CLI_SUCCESS;
            }
            pGroupEntry = VlanGetGroupEntry (CurrMacAddr, pCurrEntry);
            if (NULL == pGroupEntry)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                return CLI_SUCCESS;
            }

            if (MEMCMP (pGroupEntry->LearntPorts, &gNullPortList,
                        sizeof (tLocalPortList)) != 0)
            {
                u1Learnt = VLAN_TRUE;
            }

            u1StRefCount = pGroupEntry->u1StRefCount;
            VlanReleaseContext ();

            if ((u4DynamicOnly == VLAN_FALSE) ||
                ((u4DynamicOnly == VLAN_TRUE) && (u1Learnt == VLAN_TRUE)))
            {
                if (u4DisplayFlag == VLAN_TRUE)
                {

                    CliPrintf (CliHandle, "%-8d", u4CurrVlanId);

                    PrintMacAddress (CurrMacAddr, au1String);
                    CliPrintf (CliHandle, "%s", au1String);

                    if (u1StRefCount != 0)
                    {
                        CliPrintf (CliHandle, "Static  ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "Learnt  ");
                    }
                    CliPrintf (CliHandle, "%s", au1ConnId);

                    SnmpEgressPorts.pu1_OctetList = *pEgressPorts;
                    SnmpEgressPorts.i4_Length = sizeof (tPortList);
#ifdef MPLS_WANTED
                    CliPrintf (CliHandle, "%-7s", " -");
#endif
                    CliOctetToIfName (CliHandle,
                                      NULL,
                                      &SnmpEgressPorts,
                                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                      sizeof (tPortList),
                                      63, &u4PagingStatus, 2);

                }

                (*pi4Count)++;
            }

        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            return CLI_SUCCESS;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, exit.
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
        u1Learnt = VLAN_FALSE;
    }
    VlanReleaseContext ();

    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    return CLI_SUCCESS;
}

/**************************************************************************/
/* FUNCTION NAME  : VlanCliScanAndPrintStMcastTable ()                    */
/*                                                                        */
/* DESCRIPTION    : This function displays Static Multicast table         */
/*                  entries that match the required criteria.             */
/*                                                                        */
/* INPUT          : CliHandle     - Cli Context Handle                    */
/*                  u4ContextId   - Context Identifier                    */
/*                  pMacCmpFn     - Entry comparison function which       */
/*                                  returns CLI_SUCCESS if the required   */
/*                                  criteria is satisfied, CLI_FAILURE    */
/*                                  otherwise                             */
/*                  u4ArgsMask    - Bit mask indicating the presence      */
/*                                  the arguments (VlanId, MacAddr,       */
/*                                  Port and Status)                      */
/*                  pCliArgs      - Structure containing VlanId, MacAddr, */
/*                                  Port and Status which determine if    */
/*                                  the entry needs to be displayed.      */
/*                  u4DisplayFlag - Entries will be displayed only if     */
/*                                  this flag is set to VLAN_TRUE         */
/*                                                                        */
/* OUTPUT         : CliHandle     - Contains error messages               */
/*                  pi4Count      - Count of the number of entries        */
/*                                  displayed by this function. This      */
/*                                  must NOT be assigned. It must be      */
/*                                  incremented. If display termination   */
/*                                  occurs (throug page break) then this  */
/*                                  counter is set to -1.                 */
/*                                                                        */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                               */
/**************************************************************************/
INT4
VlanCliScanAndPrintStMcastTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                 tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                                 tVlanCliArgs * pCliArgs,
                                 UINT4 u4DisplayFlag, INT4 *pi4Count)
{
    INT4                i4Status;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4CurrRcvPort = 0;
    INT4                i4NextRcvPort;
    INT4                i4PortType;
    INT4                i4CurrentContextId;
    INT4                i4NextContextId;
    INT4                i4RowStatus = VLAN_NOT_READY;
    UINT4               u4CurrVlanId = 0;
    UINT4               u4NextVlanId;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               u1PbPortType;
    UINT4               u4Isid = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tPortList          *pPortList1 = NULL;
    tPortList          *pPortList2 = NULL;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE ForbiddenPorts;

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    i4CurrentContextId = (INT4) u4ContextId;

    if (pMacCmpFn != NULL)
    {
        /*start From the given index */
        u4CurrVlanId = (pCliArgs->u4VlanId1);
    }

    if (nmhGetNextIndexFsDot1qStaticMulticastTable
        (i4CurrentContextId, &i4NextContextId, u4CurrVlanId, &u4NextVlanId,
         CurrMacAddr, &NextMacAddr, 0, &i4NextRcvPort) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if (i4CurrentContextId != i4NextContextId)
    {
        return CLI_SUCCESS;
    }

    pPortList1 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList1 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pPortList2 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList2 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pPortList1);
        return CLI_FAILURE;
    }

    do
    {
        MEMSET (*pPortList1, 0, sizeof (tPortList));
        MEMSET (*pPortList2, 0, sizeof (tPortList));

        nmhGetFsDot1qStaticMulticastRowStatus (i4NextContextId,
                                               u4NextVlanId, NextMacAddr,
                                               i4NextRcvPort, &i4RowStatus);
        if (i4RowStatus == VLAN_ACTIVE)
        {
            if (VlanGetFirstPortInContext ((UINT4) i4NextContextId,
                                           &u4IfIndex) == VLAN_SUCCESS)
            {

                do
                {
                    nmhGetFsDot1qStaticMcastPort ((UINT4) i4NextContextId,
                                                  u4NextVlanId, NextMacAddr,
                                                  i4NextRcvPort, u4IfIndex,
                                                  &i4PortType);

                    if (i4PortType == VLAN_ADD_MEMBER_PORT)
                    {
                        /* Added for pseudo wire visibility */
                        VLAN_SET_MEMBER_PORT_LIST ((*pPortList1), u4IfIndex);
                    }
                    else if (i4PortType == VLAN_ADD_FORBIDDEN_PORT)
                    {
                        /* Added for pseudo wire visibility */
                        VLAN_SET_MEMBER_PORT_LIST ((*pPortList2), u4IfIndex);
                    }

                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInContext
                       ((UINT4) i4NextContextId, u4PrvIfIndex,
                        &u4IfIndex) == VLAN_SUCCESS);
            }

            if (pMacCmpFn != NULL)
            {
                pCliArgs->u4VlanId1 = u4NextVlanId;
                pCliArgs->u4Port2 = 0;
                pCliArgs->u4Status2 = 0;
                pCliArgs->pu1MacAddr2 = NextMacAddr;
                pCliArgs->pu1Ports = *pPortList1;

                i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
            }

            if (i4RetVal == CLI_SUCCESS)
            {
                nmhGetFsDot1qStaticMulticastStatus (u4ContextId, u4NextVlanId,
                                                    NextMacAddr, i4NextRcvPort,
                                                    &i4Status);

                if (u4DisplayFlag == VLAN_TRUE)
                {

                    CliPrintf (CliHandle, "Vlan            : %d\r\n",
                               u4NextVlanId);

                    PrintMacAddress (NextMacAddr, au1String);
                    CliPrintf (CliHandle, "Mac Address     : %s\r\n",
                               au1String);
                    if (i4NextRcvPort == 0)
                    {
                        CliPrintf (CliHandle, "Receive Port    : \r\n");
                    }

                    else
                    {
                        VlanL2IwfGetPbPortType (i4NextRcvPort, &u1PbPortType);
                        if (u1PbPortType == VLAN_VIRTUAL_INSTANCE_PORT)
                        {
                            VlanPbbGetIsidOfVip (&u4Isid, i4NextRcvPort,
                                                 u4ContextId);
                            CliPrintf (CliHandle, "Service-instance: %-6d\r\n",
                                       u4Isid);

                        }
                        else
                        {
                            VlanCfaCliGetIfName (i4NextRcvPort,
                                                 (INT1 *) au1NameStr);
                            CliPrintf (CliHandle, "Receive Port    : %s\r\n",
                                       au1NameStr);
                        }
                    }
                    EgressPorts.pu1_OctetList = *pPortList1;
                    EgressPorts.i4_Length = sizeof (tPortList);

                    i4RetVal = CliOctetToIfName (CliHandle,
                                                 "Member Ports    :",
                                                 &EgressPorts,
                                                 BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                                 sizeof (tPortList),
                                                 0, &u4PagingStatus, 6);

                    ForbiddenPorts.pu1_OctetList = *pPortList2;
                    ForbiddenPorts.i4_Length = sizeof (tPortList);

                    i4RetVal = CliOctetToIfName (CliHandle,
                                                 "Forbidden Ports :",
                                                 &ForbiddenPorts,
                                                 BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                                 sizeof (tPortList),
                                                 0, &u4PagingStatus, 6);

                    if (i4RetVal == CLI_FAILURE)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pPortList1);
                        FsUtilReleaseBitList ((UINT1 *) pPortList2);
                        return CLI_FAILURE;
                    }

                    switch (i4Status)
                    {
                        case VLAN_OTHER:
                            CliPrintf (CliHandle,
                                       "Status          : Other\r\n");
                            break;
                        case VLAN_PERMANENT:
                            CliPrintf (CliHandle,
                                       "Status          : Permanent\r\n");
                            break;
                        case VLAN_DELETE_ON_RESET:
                            CliPrintf (CliHandle,
                                       "Status          : DeleteOnReset\r\n");
                            break;
                        case VLAN_DELETE_ON_TIMEOUT:
                            CliPrintf (CliHandle,
                                       "Status          : DeleteOnTimeout \r\n");
                            break;
                        default:
                            CliPrintf (CliHandle,
                                       "Status          : Unknown\r\n");
                            break;
                    }

                    CliPrintf (CliHandle,
                               "\r------------------------------------------------\r\n");
                }

                (*pi4Count)++;
            }
        }
        i4CurrentContextId = i4NextContextId;
        u4CurrVlanId = u4NextVlanId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
        i4CurrRcvPort = i4NextRcvPort;

        if (nmhGetNextIndexFsDot1qStaticMulticastTable
            (i4CurrentContextId, &i4NextContextId, u4CurrVlanId,
             &u4NextVlanId, CurrMacAddr, &NextMacAddr, i4CurrRcvPort,
             &i4NextRcvPort) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit 
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    FsUtilReleaseBitList ((UINT1 *) pPortList1);
    FsUtilReleaseBitList ((UINT1 *) pPortList2);
    return CLI_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME  : VlanCliDot1dScanAndPrintStMcastTable ()              */
/*                                                                       */
/* DESCRIPTION    : This function displays Static Multicast table        */
/*                  entries in transparent birdging                      */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                  pMacCmpFn     - Entry comparison function which      */
/*                                  returns CLI_SUCCESS if the required  */
/*                                  criteria is satisfied, CLI_FAILURE   */
/*                                  otherwise                            */
/*                  u4ArgsMask    - Bit mask indicating the presence     */
/*                                  the arguments (FdbId, MacAddr,       */
/*                                  Port and Status)                     */
/*                  pCliArgs      - Structure containing FdbId, MacAddr, */
/*                                  Port and Status which determine if   */
/*                                  the entry needs to be displayed.     */
/*                  u4DisplayFlag - Entries will be displayed only if    */
/*                                  this flag is set to VLAN_TRUE        */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
VlanCliDot1dScanAndPrintStMcastTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                      tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                                      tVlanCliArgs * pCliArgs,
                                      UINT4 u4DisplayFlag, INT4 *pi4Count)
{
    INT4                i4Status = VLAN_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4CurrRcvPort = VLAN_INIT_VAL;
    INT4                i4NextRcvPort = VLAN_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tPortList          *pPorts = NULL;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;

    UNUSED_PARAM (u4ContextId);

    pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (CurrMacAddr, VLAN_INIT_VAL, sizeof (tMacAddr));
    MEMSET (NextMacAddr, VLAN_INIT_VAL, sizeof (tMacAddr));
    MEMSET (pPorts, VLAN_INIT_VAL, sizeof (tPortList));

    SnmpPorts.pu1_OctetList = *pPorts;
    SnmpPorts.i4_Length = sizeof (tPortList);

    if (nmhGetNextIndexDot1dStaticTable
        (CurrMacAddr, &NextMacAddr, VLAN_INIT_VAL,
         &i4NextRcvPort) == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        return CLI_SUCCESS;
    }
    do
    {
        nmhGetDot1dStaticAllowedToGoTo (NextMacAddr, i4NextRcvPort, &SnmpPorts);

        if (pMacCmpFn != NULL)
        {
            pCliArgs->u4Port2 = VLAN_INIT_VAL;
            pCliArgs->u4Status2 = VLAN_INIT_VAL;
            pCliArgs->pu1MacAddr2 = NextMacAddr;
            pCliArgs->pu1Ports = SnmpPorts.pu1_OctetList;

            i4RetVal = pMacCmpFn (u4ArgsMask, pCliArgs);
        }

        if ((i4RetVal == CLI_SUCCESS) && VLAN_IS_MCASTADDR (NextMacAddr))
        {
            if (u4DisplayFlag == VLAN_TRUE)
            {
                nmhGetDot1dStaticStatus (NextMacAddr, i4NextRcvPort, &i4Status);

                PrintMacAddress (NextMacAddr, au1String);
                CliPrintf (CliHandle, "%s", au1String);

                if (i4NextRcvPort == VLAN_INIT_VAL)
                {
                    CliPrintf (CliHandle, "%-8s", " ");
                }
                else
                {
                    VlanCfaCliGetIfName (i4NextRcvPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-8s", au1NameStr);
                }

                switch (i4Status)
                {
                    case VLAN_OTHER:
                        CliPrintf (CliHandle, "Other         ");
                        break;
                    case VLAN_PERMANENT:
                        CliPrintf (CliHandle, "Permanent     ");
                        break;
                    case VLAN_DELETE_ON_RESET:
                        CliPrintf (CliHandle, "DeleteOnReset ");
                        break;
                    case VLAN_DELETE_ON_TIMEOUT:
                        CliPrintf (CliHandle, "Del-OnTimeout ");
                        break;
                    default:
                        CliPrintf (CliHandle, "Unknown       ");
                        break;
                }

                CliOctetToIfName (CliHandle,
                                  NULL,
                                  &SnmpPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList), 48, &u4PagingStatus, 2);

            }
            (*pi4Count)++;
        }

        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
        i4CurrRcvPort = i4NextRcvPort;

        if (nmhGetNextIndexDot1dStaticTable (CurrMacAddr, &NextMacAddr,
                                             i4CurrRcvPort, &i4NextRcvPort)
            == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit 
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            *pi4Count = -1;
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    FsUtilReleaseBitList ((UINT1 *) pPorts);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME  : VlanCliCompareArgs ()                                    */
/*                                                                           */
/* DESCRIPTION    : This function determines if the entry corresponding      */
/*                  to the supplied arguments, need to be displayed.         */
/*                                                                           */
/* INPUT          : u4Mask - Bit mask specifying the valid fields of pArgs.  */
/*                  pArgs  - Structure containing the fields FdbId, MacAddr, */
/*                           FdbStatus, Port. The comparison is made between */
/*                           Field1 and Field2 of pArgs structure. If the    */
/*                           bit corresponding to Port is set and if         */
/*                           pu1Ports field is not null, then u4Port1 (and   */
/*                           NOT u4Port2) will be checked if it is a member  */
/*                           of pu1Ports.                                    */
/* OUTPUT         : None.                                                    */
/*                                                                           */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                                  */
/*****************************************************************************/
INT4
VlanCliCompareArgs (UINT4 u4Mask, tVlanCliArgs * pArgs)
{
    UINT4               u4Result = VLAN_FALSE;

    if ((u4Mask & VLAN_CLI_VLAN_ID_FDB_ID_PRESENT) != 0)
    {
        if (pArgs->u4VlanIdFdbId1 != pArgs->u4VlanIdFdbId2)
        {
            return CLI_FAILURE;
        }
    }

    if (((u4Mask & VLAN_CLI_MAC_ADDR_PRESENT) != 0) &&
        (pArgs->pu1MacAddr1 != NULL && pArgs->pu1MacAddr2 != NULL))
    {
        if (VlanCmpMacAddr (pArgs->pu1MacAddr1,
                            pArgs->pu1MacAddr2) != VLAN_EQUAL)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4Mask & VLAN_CLI_PORT_PRESENT) != 0)
    {
        if (pArgs->pu1Ports != NULL)
        {
            VLAN_IS_MEMBER_PORT (pArgs->pu1Ports, pArgs->u4Port1, u4Result);

            if (u4Result == VLAN_FALSE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            if (pArgs->u4Port1 != pArgs->u4Port2)
            {
                return CLI_FAILURE;
            }
        }
    }

    if ((u4Mask & VLAN_CLI_FDB_STATUS_PRESENT) != 0)
    {
        if (pArgs->u4Status1 != pArgs->u4Status2)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4Mask & VLAN_CLI_VLAN_ID_PRESENT) != 0)
    {
        /*start index should always be less than last index */
        if ((pArgs->u4VlanId2 != 0) && (pArgs->u4VlanId1 > pArgs->u4VlanId2))
        {
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanUtilGetProtocol                                */
/*                                                                           */
/*     DESCRIPTION      : This function returns the protocol type.           */
/*                                                                           */
/*     INPUT            : pu1ProtoVal, u1Len.                                */
/*                                                                           */
/*     OUTPUT           : pu1Proto - Output pointer which stores the         */
/*                                   protocol type.                          */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUtilGetProtocol (UINT1 *pu1Proto, UINT1 *pu1ProtoVal, UINT1 u1Len)
{
    UINT2               u2EtherType;

    if (u1Len != VLAN_TYPE_OR_LEN_SIZE)
    {
        return CLI_FAILURE;
    }

    MEMCPY (&u2EtherType, pu1ProtoVal, u1Len);

    u2EtherType = OSIX_NTOHS (u2EtherType);

    switch (u2EtherType)
    {
        case VLAN_IP_PROTO_ID:
            *pu1Proto = VLAN_IP;
            break;

        case VLAN_IPX_PROTO_ID:
            /* fall through */
        case VLAN_RAW_IPX_PROTO_ID:
            *pu1Proto = VLAN_IPX;
            break;

        case VLAN_NETBIOS_PROTO_ID:
            *pu1Proto = VLAN_NETBIOS;
            break;

        case VLAN_APPLETALK_PROTO_ID:
            *pu1Proto = VLAN_APPLETALK;
            break;

        default:
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCliShowDeviceCapabilities                      */
/*                                                                           */
/*     DESCRIPTION      : This function show the Vlan Device Capabilities    */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliShowDeviceCapabilities (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1DevCap[VLAN_CAPABILITIES_MASK_LENGTH];
    tSNMP_OCTET_STRING_TYPE OctetStringReturnVal;

    OctetStringReturnVal.i4_Length = sizeof (au1DevCap);
    OctetStringReturnVal.pu1_OctetList = au1DevCap;

    nmhGetFsDot1dDeviceCapabilities (u4ContextId, &OctetStringReturnVal);

    CliPrintf (CliHandle, "%-33s \r\n", "Vlan device capabilities");

    CliPrintf (CliHandle, "--------------------------\r\n\r\n");

    if ((au1DevCap[0] & VLAN_EXTEND_FILTERING_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "Extended filtering services");
    }

    if ((au1DevCap[0] & VLAN_TRAFFIC_CLASSES_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "Traffic classes");
    }

    if ((au1DevCap[0] & VLAN_STATIC_INDIVIDUAL_PORT_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "Static Entry Individual port");
    }

    if ((au1DevCap[0] & VLAN_IVL_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "IVL capable");
    }

    if ((au1DevCap[0] & VLAN_SVL_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "SVL capable");
    }

    if ((au1DevCap[0] & VLAN_HYBRID_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "Hybrid capable");
    }

    if ((au1DevCap[0] & VLAN_CONFIG_PVID_TAGGING_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "Configurable Pvid Tagging");
    }

    if ((au1DevCap[0] & VLAN_LOCAL_VLAN_CAPABILITY_MASK) != 0)
    {
        CliPrintf (CliHandle, "%-33s\r\n", "Local VLAN Capable");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCliShowFidVlanMapping                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the forwarding database     */
/*                        identifier used by vlans                           */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        u4ContextId - Context Identifier                   */
/*                        u4Command            - To display fid info for a   */
/*                                               particular fid or in detail */
/*                        u4Fid                - FID id                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliShowFidVlanMapping (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4Command, UINT4 u4Fid)
{
    UINT1              *pu1VlanList = NULL;
    UINT4               u4VlanIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4HybridTypeDefault;

    /* Allocating memory for Vlan List Size */
    pu1VlanList = UtlShMemAllocVlanList ();
    if (pu1VlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

    if (u4Fid == 0)
    {
        u4Fid++;
    }

    nmhGetFsMIDot1qFutureVlanHybridTypeDefault (u4ContextId,
                                                &i4HybridTypeDefault);

    if (i4HybridTypeDefault == VLAN_INDEP_LEARNING)
    {
        CliPrintf (CliHandle, "Default Learning Type    : IVL\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Default Learning Type    : SVL\r\n");
    }

    CliPrintf (CliHandle, "\r\nFid Vlan mapping information\r\n");

    CliPrintf (CliHandle, "---------------------------- \r\n");
    do
    {
        for (u4VlanIndex = 1; u4VlanIndex <= VLAN_MAX_VLAN_ID; u4VlanIndex++)
        {
            if (VlanL2IwfMiGetVlanFdbId (u4ContextId,
                                         (tVlanId) u4VlanIndex) == u4Fid)
            {
                OSIX_BITLIST_SET_BIT (pu1VlanList, u4VlanIndex, VLAN_LIST_SIZE);
            }
        }
        CliPrintf (CliHandle, "Fid     : %d\r\n", u4Fid);

        CliPrintf (CliHandle, "Vlan's  : ");

        VlanConvertVlanListToVlan (CliHandle, pu1VlanList);

        u4PagingStatus =
            CliPrintf (CliHandle, "\r\n----------------------------\r\n");

        if (u4Command == CLI_VLAN_SHOW_VLAN_FID_ID)
        {
            break;
        }

        MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

        u4Fid++;

    }
    while ((VLAN_IS_FDB_ID_VALID (u4Fid) == VLAN_TRUE) &&
           (u4PagingStatus == CLI_SUCCESS));
    /* Releasing memory for Vlan List Size */
    UtlShMemFreeVlanList (pu1VlanList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCliFidVlanMapping                              */
/*                                                                           */
/*     DESCRIPTION      : This function maps vlans to a FID.                 */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                                command is processed.      */
/*                        u4Fid                 - Filtering database         */
/*                                                identifier.                */
/*                        pau1VlanList          - Vlan list                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliFidVlanMapping (tCliHandle CliHandle, UINT4 u4Fid, UINT1 *pau1VlanList,
                       UINT1 u1Action)
{
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    UINT4               u4ByteIndex;
    UINT4               u4VlanIndex;
    UINT4               u4BitIndex;
    UINT2               u2VlanFlag;
    tVlanId             VlanId;
    UINT2               u2MstInst;
    INT4                i4DefType;
    INT4                i4RetVal;
    UINT1              *pu1TmpAll = NULL;

    pu1TmpAll = UtlShMemAllocVlanList ();
    if (pu1TmpAll == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pu1TmpAll, 0, VLAN_LIST_SIZE);
    VlanListAll.pu1_OctetList = pu1TmpAll;
    VlanListAll.i4_Length = VLAN_LIST_SIZE;

    CLI_MEMCPY (VlanListAll.pu1_OctetList, pau1VlanList, VlanListAll.i4_Length);

    if (u1Action == VLAN_SET_CMD)
    {
        i4RetVal = VlanCheckMstpInconsistency (u4Fid, &VlanListAll);

        if (i4RetVal == VLAN_FAILURE)
        {
            UtlShMemFreeVlanList (pu1TmpAll);
            return CLI_FAILURE;
        }
    }

    for (u4ByteIndex = 0; u4ByteIndex < VLAN_LIST_SIZE; u4ByteIndex++)
    {
        if (VlanListAll.pu1_OctetList[u4ByteIndex] != 0)
        {
            u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];

            for (u4BitIndex = 0;
                 ((u4BitIndex < VLAN_PORTS_PER_BYTE) && (u2VlanFlag != 0));
                 u4BitIndex++)
            {
                if ((u2VlanFlag & VLAN_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u4ByteIndex * VLAN_PORTS_PER_BYTE) +
                                 u4BitIndex + 1);

                    if (u1Action == VLAN_NO_CMD)
                    {
                        nmhGetDot1qFutureVlanHybridTypeDefault (&i4DefType);

                        if (i4DefType == VLAN_INDEP_LEARNING)
                        {

                            u4Fid = (UINT4) VlanId;
                        }
                        else
                        {
                            u4Fid = VLAN_SHARED_DEF_FDBID;
                        }

                        u2MstInst =
                            VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID
                                                           (), VlanId);

                        for (u4VlanIndex = 1; u4VlanIndex <=
                             VLAN_MAX_VLAN_ID; u4VlanIndex++)
                        {
                            if (VlanId != u4VlanIndex)
                            {
                                if (VlanL2IwfMiGetVlanFdbId
                                    (VLAN_CURR_CONTEXT_ID (),
                                     (tVlanId) u4VlanIndex) == u4Fid
                                    &&
                                    VlanL2IwfMiGetVlanInstMapping
                                    (VLAN_CURR_CONTEXT_ID (),
                                     (tVlanId) u4VlanIndex) != u2MstInst)
                                {
                                    CliPrintf (CliHandle, "\r%%Cannot"
                                               "remap Vlan %d to "
                                               "default Fid.Conflicts"
                                               "with MST Instance of"
                                               "Vlan %d\r\n", VlanId,
                                               u4VlanIndex);
                                    UtlShMemFreeVlanList (pu1TmpAll);

                                    return CLI_FAILURE;
                                }
                            }
                        }
                    }

                    nmhSetDot1qFutureVlanFid ((UINT4) VlanId, u4Fid);

                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    UtlShMemFreeVlanList (pu1TmpAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCheckMstpInconsistency                         */
/*                                                                           */
/*     DESCRIPTION      : This function checks if there are any              */
/*                        inconsistencies with MST Instance Vlan mapping     */
/*                        for the given FID  and Vlan list.                  */
/*                                                                           */
/*     INPUT            : u4Fid                 - Filtering database         */
/*                                                identifier.                */
/*                        pVlanList          - Vlan list                     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckMstpInconsistency (UINT4 u4Fid, tSNMP_OCTET_STRING_TYPE * pVlanList)
{
    UINT4               u4ByteIndex;
    UINT4               u4VlanIndex;
    UINT2               u2VlanFlag;
    UINT4               u4BitIndex;
    UINT2               u2MstInst;
    tVlanId             VlanId;
    BOOL1               bResult = OSIX_FALSE;

    for (u4ByteIndex = 0; u4ByteIndex < VLAN_LIST_SIZE; u4ByteIndex++)
    {
        if (pVlanList->pu1_OctetList[u4ByteIndex] != 0)
        {
            u2VlanFlag = pVlanList->pu1_OctetList[u4ByteIndex];

            for (u4BitIndex = 0;
                 ((u4BitIndex < VLAN_PORTS_PER_BYTE) && (u2VlanFlag != 0));
                 u4BitIndex++)
            {
                if ((u2VlanFlag & VLAN_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u4ByteIndex * VLAN_PORTS_PER_BYTE) +
                                 u4BitIndex + 1);

                    u2MstInst =
                        VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                                       VlanId);

                    for (u4VlanIndex = 1; u4VlanIndex <= VLAN_MAX_VLAN_ID;
                         u4VlanIndex++)
                    {
                        if (u4VlanIndex != (UINT4) VlanId)
                        {
                            OSIX_BITLIST_IS_BIT_SET (pVlanList->
                                                     pu1_OctetList,
                                                     u4VlanIndex,
                                                     VLAN_LIST_SIZE, bResult);

                            if (bResult == OSIX_TRUE)
                            {
                                if (VlanL2IwfMiGetVlanInstMapping
                                    (VLAN_CURR_CONTEXT_ID (),
                                     (tVlanId) u4VlanIndex) != u2MstInst)
                                {
                                    mmi_printf ("\r%%Configuration failed"
                                                " All Vlan's in the list"
                                                " being mapped to the same FID"
                                                " should have been mapped"
                                                " to the same MST instance.\r\n");

                                    return VLAN_FAILURE;
                                }
                                continue;
                            }

                            if (VlanL2IwfMiGetVlanFdbId
                                (VLAN_CURR_CONTEXT_ID (),
                                 (UINT2) u4VlanIndex) == u4Fid
                                &&
                                VlanL2IwfMiGetVlanInstMapping
                                (VLAN_CURR_CONTEXT_ID (),
                                 (tVlanId) u4VlanIndex) != u2MstInst)
                            {
                                mmi_printf ("\r%%Vlan %d mapped to FdbId %d"
                                            "is mapped to a different MST"
                                            "instance from the Vlan's in the"
                                            "list. Configuration failed.\r\n",
                                            u4VlanIndex, u4Fid);

                                return VLAN_FAILURE;
                            }

                        }
                    }
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanGetVlanCfgPrompt                               */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for vlan validity and returns */
/*                        the prompt to be displayed.                        */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/

INT1
VlanGetVlanCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4Len = STRLEN (CLI_VLAN_MODE);
    INT4                i4Dot1qVlanStaticRowStatus;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_VLAN_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Index = CLI_ATOI (pi1ModeName);

    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_vlan_cmd.
     */
    if ((nmhGetDot1qVlanStaticRowStatus (u4Index,
                                         &i4Dot1qVlanStaticRowStatus))
        != SNMP_SUCCESS)
    {
        return FALSE;
    }

    CLI_SET_VLANID (u4Index);
    CLI_SET_CXT_ID (0);
    STRNCPY (pi1DispStr, "(config-vlan)#", STRLEN ("(config-vlan)#"));
    pi1DispStr[STRLEN ("(config-vlan)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanGetVcmVlanCfgPrompt                            */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for vlan validity and returns */
/*                        the prompt to be displayed.                        */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/

INT1
VlanGetVcmVlanCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4Len = STRLEN (CLI_VLAN_MODE);
    INT4                i4Dot1qVlanStaticRowStatus;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_VLAN_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Index = CLI_ATOI (pi1ModeName);

    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_vlan_cmd.
     */
    if ((nmhGetFsDot1qVlanStaticRowStatus (CLI_GET_CXT_ID (), u4Index,
                                           &i4Dot1qVlanStaticRowStatus))
        != SNMP_SUCCESS)
    {
        return FALSE;
    }

    CLI_SET_VLANID (u4Index);

    STRNCPY (pi1DispStr, "(config-switch-vlan)#",
             STRLEN ("(config-switch-vlan)#"));
    pi1DispStr[STRLEN ("(config-switch-vlan)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanTestModificationInfo                           */
/*                                                                           */
/*     DESCRIPTION      : This function tests the values specified as        */
/*                        modification data                                  */
/*                                                                           */
/*     INPUT            : pEgressPorts ,pForbiddenPorts,pUntaggedPorts       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanTestModificationInfo (tCliHandle CliHandle,
                          UINT4 u4VlanId,
                          tSNMP_OCTET_STRING_TYPE * pEgressPorts,
                          tSNMP_OCTET_STRING_TYPE * pUntaggedPorts,
                          tSNMP_OCTET_STRING_TYPE * pForbiddenPorts)
{
    UINT1              *pau1TaggedPortList = NULL;
    UINT1               bu1RetVal = VLAN_FALSE;
    UINT1               u1PortType;
    UINT2               u2PortNumber = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4ErrorCode = 0;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    u4ContextId = CLI_GET_CXT_ID ();

    /* Added for pseudo wire visibility */
    /* Check if the command is a switch mode command */
    if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        /* Non Switch-mode Command */
        u4ContextId = VLAN_DEF_CONTEXT_ID;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle,
                   "\r%% This configuration is not allowed for Transparent Bridges.\r\n");
        return CLI_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4VlanId) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Vlan ID %d\r\n", u4VlanId);
        return CLI_FAILURE;
    }

    if ((pEgressPorts->i4_Length <= 0) ||
        (pEgressPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Member Ports\r\n");
        return CLI_FAILURE;
    }

    if ((pForbiddenPorts->i4_Length <= 0) ||
        (pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Forbidden Ports\r\n");
        return CLI_FAILURE;
    }

    if ((pUntaggedPorts->i4_Length <= 0) ||
        (pUntaggedPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Untagged Ports\r\n");
        return CLI_FAILURE;
    }
/* Added for pseudo wire visibility */
    if (VlanUtilIsPortListValid (u4ContextId, pEgressPorts->pu1_OctetList,
                                 pEgressPorts->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Member Port(s) present\r\n");
        return CLI_FAILURE;
    }

    if (VlanUtilIsPortListValid (u4ContextId, pForbiddenPorts->pu1_OctetList,
                                 pForbiddenPorts->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Forbidden Port(s) present\r\n");
        return CLI_FAILURE;
    }

    if (VlanUtilIsPortListValid (u4ContextId, pUntaggedPorts->pu1_OctetList,
                                 pUntaggedPorts->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Untagged Port(s) present\r\n");
        return CLI_FAILURE;
    }

    /* 
     * Check if the number of egress ports is more than the     
     * the Max number of ports in the syatem
     */

    VLAN_IS_EXCEED_MAX_PORTS (pEgressPorts->pu1_OctetList,
                              pEgressPorts->i4_Length, bu1RetVal);
    if (bu1RetVal == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Member Port(s) present\r\n");
        return CLI_FAILURE;
    }

    /* 
     * Check if the number of untagged ports is more than the     
     * the Max number of ports in the syatem
     */

    VLAN_IS_EXCEED_MAX_PORTS (pUntaggedPorts->pu1_OctetList,
                              pUntaggedPorts->i4_Length, bu1RetVal);

    if (bu1RetVal == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Untagged Port(s) present\r\n");
        return CLI_FAILURE;
    }

    /* 
     * Check if the number of forbidden ports is more than the     
     * the Max number of ports in the syatem
     */

    VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
                              pForbiddenPorts->i4_Length, bu1RetVal);

    if (bu1RetVal == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Forbidden Port(s) present\r\n");
        return CLI_FAILURE;
    }

    /* 
     * Check if any of ports in the forbidden port list are
     * members of the egress port list 
     */
    VLAN_ARE_PORTS_EXCLUSIVE (pEgressPorts->pu1_OctetList,
                              pForbiddenPorts->pu1_OctetList, bu1RetVal);

    if (bu1RetVal == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Member Ports and Forbidden Ports "
                   "are overlapping\r\n");
        return CLI_FAILURE;
    }

    /* 
     * The size of the Portlist array passed as arguments to the macro 
     * VLAN_IS_PORT_LIST_A_SUBSET () must always be VLAN_PORT_LIST_SIZE
     */
    VLAN_IS_PORT_LIST_A_SUBSET (pEgressPorts->pu1_OctetList,
                                pUntaggedPorts->pu1_OctetList, bu1RetVal);

    if (bu1RetVal == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Untagged Ports is not a subset of "
                   "Member Ports\r\n");
        return CLI_FAILURE;
    }

    /* Valid for Both PB and PBB */
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {

        if (VlanPbIsPortTypePresentInPortList (pUntaggedPorts->pu1_OctetList,
                                               pUntaggedPorts->i4_Length,
                                               VLAN_PROVIDER_NETWORK_PORT)
            == VLAN_TRUE)
        {
            CLI_SET_ERR (CLI_VLAN_PB_PNP_UNTAG_ERR);
            return CLI_FAILURE;
        }

        if (VlanIsThisInterfaceVlan ((tVlanId) u4VlanId) == VLAN_TRUE)
        {
            /* An IVR exists for this vlan. */
            if (VlanPbIsAnyCustomerPortPresent (pEgressPorts->pu1_OctetList,
                                                pEgressPorts->i4_Length)
                == VLAN_TRUE)
            {
                /* Some customer port (CEP/CNP/PCEP/PCNP) is present in
                 * the egress port list. So don't allow this configuration.
                 * If allowed, customer will be able to configure the
                 * system and this may lead to serious security issues. */
                CLI_SET_ERR (CLI_VLAN_IVR_CONFLICT_ERR);
                return CLI_FAILURE;
            }
        }

        pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanId);

        if (pStVlanEntry != NULL)
        {
            if ((pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
                || (pStVlanEntry->u1RowStatus == VLAN_NOT_IN_SERVICE))
            {
                if (VlanPbCheckVlanServiceType ((tVlanId) u4VlanId,
                                                pEgressPorts->pu1_OctetList)
                    == VLAN_FAILURE)
                {
                    CLI_SET_ERR (CLI_VLAN_PB_SERV_TYPE_ERR);
                    return CLI_FAILURE;
                }
            }
        }

    }

    /* PBB Changes */
    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
    {
        if (VlanIsPbbPortTypeValid (pEgressPorts->pu1_OctetList,
                                    pEgressPorts->i4_Length) == VLAN_FALSE)
        {
            /* Validate that in I-Comp its CNP and 
             * if B-Comp then CBP, PNP.
             */
            CLI_SET_ERR (CLI_VLAN_PBB_PORT_TYPE_ERR);
            return CLI_FAILURE;
        }

        if (VLAN_BRIDGE_MODE () == VLAN_PBB_BCOMPONENT_BRIDGE_MODE)
        {

            if (VlanIsPbbPortTypeInPortList (pEgressPorts->pu1_OctetList,
                                             pUntaggedPorts->pu1_OctetList,
                                             pEgressPorts->i4_Length,
                                             VLAN_CUSTOMER_BACKBONE_PORT)
                == VLAN_FALSE)
            {
                CLI_SET_ERR (CLI_VLAN_PBB_CBP_UNTAG_ERR);
                return CLI_FAILURE;
            }
        }
    }

    u1PortType = VLAN_TRUNK_PORT;
    if (!(AstIsPvrstStartedInContext (VLAN_DEF_CONTEXT_ID)) || (u4VlanId != 1))
    {
        if (VlanIsPortListPortTypeValid (pUntaggedPorts->pu1_OctetList,
                                         pUntaggedPorts->i4_Length,
                                         u1PortType) == VLAN_FALSE)
        {
            CLI_SET_ERR (CLI_VLAN_TRUNK_UNTAGGED_PORT_ERR);
            return CLI_FAILURE;
        }
    }
    u1PortType = VLAN_ACCESS_PORT;
    pau1TaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1TaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanTestModificationInfo: "
                  "Error in allocating memory for pau1TaggedPortList\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1TaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMSET (pau1TaggedPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMCPY (pau1TaggedPortList, pEgressPorts->pu1_OctetList,
            pEgressPorts->i4_Length);

    VLAN_RESET_PORT_LIST (pau1TaggedPortList, pUntaggedPorts->pu1_OctetList);

    if (ISS_HW_SUPPORTED ==
        IssGetHwCapabilities (ISS_HW_UNTAGGED_PORTS_FOR_VLANS))
    {
        if (VlanIsPortListPortTypeValid (pau1TaggedPortList,
                                         VLAN_PORT_LIST_SIZE,
                                         u1PortType) == VLAN_FALSE)
        {
            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                CLI_SET_ERR (CLI_VLAN_PB_PORT_TAG_ERR);
            }
            else
            {
                CLI_SET_ERR (CLI_VLAN_ACCESS_TAGGED_PORT_ERR);
            }
            UtilPlstReleaseLocalPortList (pau1TaggedPortList);
            return CLI_FAILURE;
        }
    }
    UtilPlstReleaseLocalPortList (pau1TaggedPortList);

    if (VlanVcmSispIsPortVlanMappingValid (u4ContextId, (tVlanId) u4VlanId,
                                           pEgressPorts->pu1_OctetList)
        == VCM_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Configurations not allowed\r\n");
        return CLI_FAILURE;
    }

    for (u2PortNumber = 1; u2PortNumber < VLAN_MAX_PORTS; u2PortNumber++)
    {
        OSIX_BITLIST_IS_BIT_SET (pEgressPorts->pu1_OctetList, u2PortNumber,
                                 VLAN_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            /* Added for pseudo wire visibility */
            pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2PortNumber);
            if (pVlanPortEntry == NULL)
            {
                continue;
            }
            if (VlanMstSispValidateInstRestriction (u4ContextId,
                                                    pVlanPortEntry->u4IfIndex,
                                                    (tVlanId) u4VlanId)
                != MST_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% SISP Enabled port cannot be member of same "
                           "instance in different contexts\r\n");

                return CLI_FAILURE;
            }
        }
    }                            /* For all ports present in port list */
    if (VlanTestConfigMembersOnPvlan (u4VlanId, &u4ErrorCode,
                                      pEgressPorts) != VLAN_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanUtilGetProtocolValue                           */
/*                                                                           */
/*     DESCRIPTION      : Function returns the protocol identifier value and */
/*                        length                                             */
/*                                                                           */
/*     INPUT            : u4Protocol  - Protocol identifier                  */
/*                                                                           */
/*     OUTPUT           : pu1ProtoVal - Holds protocol identifier value      */
/*                        pu1Len      - Holds length                         */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUtilGetProtocolValue (UINT4 u4Protocol, UINT1 *pu1ProtoVal, UINT1 *pu1Len)
{
    UINT2               u2EtherType;

    switch (u4Protocol)
    {
        case VLAN_IP:
            u2EtherType = OSIX_HTONS (VLAN_IP_PROTO_ID);
            break;

        case VLAN_IPX:
            u2EtherType = OSIX_HTONS (VLAN_IPX_PROTO_ID);
            break;

        case VLAN_RAW_IPX:
            u2EtherType = OSIX_HTONS (VLAN_RAW_IPX_PROTO_ID);
            break;

        case VLAN_NETBIOS:
            u2EtherType = OSIX_HTONS (VLAN_NETBIOS_PROTO_ID);
            break;

        case VLAN_APPLETALK:
            u2EtherType = OSIX_HTONS (VLAN_APPLETALK_PROTO_ID);
            break;

        default:
            return CLI_FAILURE;
    }

    *pu1Len = VLAN_TYPE_OR_LEN_SIZE;

    MEMCPY (pu1ProtoVal, &u2EtherType, *pu1Len);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanTestStaticMcastModificationInfo                */
/*                                                                           */
/*     DESCRIPTION      : This function tests the values specified as        */
/*                        modification data                                  */
/*                                                                           */
/*     INPUT            : u4VlanId           - Vlan identifier               */
/*                        pu1MacAddr         - Multicast MAC address         */
/*                        u4ReceivePort      - Port where MAC is learnt      */
/*                        pStaticPortList    - Static port list              */
/*                        pForbiddenPortList - Forbidden port list           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanTestStaticMcastModificationInfo (tCliHandle CliHandle, UINT4 u4VlanIndex,
                                     UINT1 *pu1MacAddr,
                                     INT4 i4Dot1qStaticMulticastReceivePort,
                                     tSNMP_OCTET_STRING_TYPE * pStaticPortList,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pForbiddenPortList)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanIsValidMcastAddr (pu1MacAddr) == VLAN_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Multicast Address\r\n");

        return CLI_FAILURE;
    }
    if ((pStaticPortList->i4_Length <= 0) ||
        (pStaticPortList->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Egress Ports\r\n");
        return CLI_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pStaticPortList->pu1_OctetList,
                              pStaticPortList->i4_Length, u1Result);
    if (u1Result == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Egress Ports \r\n");
        return CLI_FAILURE;
    }

    /* Here we check if the ports are Valid in gpVlanContextInfo->pVlanPortTable */

    if (VlanIsPortListValid (pStaticPortList->pu1_OctetList,
                             pStaticPortList->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Egress Ports present\r\n");
        return CLI_FAILURE;
    }

    if ((pForbiddenPortList->i4_Length <= 0) ||
        (pForbiddenPortList->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Forbidden Ports\r\n");
        return CLI_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPortList->pu1_OctetList,
                              pForbiddenPortList->i4_Length, u1Result);
    if (u1Result == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Forbidden Ports present\r\n");
        return CLI_FAILURE;
    }

    /* Here we check if the ports are Valid in gpVlanContextInfo->pVlanPortTable */

    if (VlanIsPortListValid (pForbiddenPortList->pu1_OctetList,
                             pForbiddenPortList->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Forbidden Ports present\r\n");
        return CLI_FAILURE;
    }

    VLAN_ARE_PORTS_EXCLUSIVE (pStaticPortList->pu1_OctetList,
                              pForbiddenPortList->pu1_OctetList, u1Result);
    if (u1Result == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Egress Ports and Forbidden Ports "
                   "are overlapping \r\n");
        return CLI_FAILURE;
    }

    VlanId = (tVlanId) u4VlanIndex;

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    /* If there is no Static VLAN entry we return failure */
    if (pStVlanEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Vlan is not created\r\n");
        return CLI_FAILURE;
    }

    /* Return Failure if the egress ports are not members of VLAN */
    VLAN_IS_PORTLIST_SUBSET_TO_EGRESS (pStVlanEntry,
                                       pStaticPortList->pu1_OctetList,
                                       u1Result);

    if (u1Result == VLAN_FALSE)
    {
        CliPrintf (CliHandle,
                   "\r%% Egress Ports must be members of the VLAN\r\n");
        return CLI_FAILURE;
    }

    /* Forbidden ports must be members of VLAN */
    VLAN_IS_PORTLIST_SUBSET_TO_EGRESS (pStVlanEntry,
                                       pForbiddenPortList->pu1_OctetList,
                                       u1Result);

    if (u1Result == VLAN_FALSE)
    {
        CliPrintf (CliHandle,
                   "\r%% Forbidden Ports must be members of the VLAN\r\n");
        return CLI_FAILURE;
    }

    if ((i4Dot1qStaticMulticastReceivePort > VLAN_MAX_PORTS) ||
        (i4Dot1qStaticMulticastReceivePort < 0))
    {
        CliPrintf (CliHandle, "\r%% Invalid Receive Port\r\n");
        return CLI_FAILURE;
    }

    /* Here we check if the receive port is Valid in gpVlanContextInfo->pVlanPortTable */
    if (i4Dot1qStaticMulticastReceivePort != 0)
    {
        if ((VLAN_GET_PORT_ENTRY (i4Dot1qStaticMulticastReceivePort)) == NULL)
        {
            CliPrintf (CliHandle, "\r%% Unknown Receive Port \r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

#ifdef VLAN_EXTENDED_FILTER
/**************************************************************************/
/* FUNCTION NAME : VlanTestStaticDefGroupModificationInfo                 */
/*                                                                        */
/* DESCRIPTION   : This function tests if the dependencies are satisfied  */
/*                 Default Group Static and Forbidden ports are set.      */
/*                                                                        */
/* INPUT         : CliHandle            - Cli Context Identifier          */
/*                 u4VlanId             - Vlan identifier                 */
/*                 pStaticPorts         - Default Group's Static Ports    */
/*                 pForbiddenPorts      - Default Group's Forbidden Ports */
/*                 u4Type               - Type of forward Group           */
/*                                                                        */
/* OUTPUT        : None.                                                  */
/*                                                                        */
/* RETURNS       : CLI_SUCCESS/CLI_FAILURE                                */
/**************************************************************************/
INT4
VlanTestStaticDefGroupModificationInfo (tCliHandle CliHandle,
                                        UINT4 u4VlanId,
                                        tSNMP_OCTET_STRING_TYPE * pStaticPorts,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pForbiddenPorts, UINT4 u4Type)
{
    UINT1              *pPortList1 = NULL;
    UINT1              *pPortList2 = NULL;
    tVlanCurrEntry     *pCurrEntry;
    UINT1               u1Result = VLAN_TRUE;
    UINT1               bu1RetVal = VLAN_FALSE;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle,
                   "\r%% This configuration is not allowed for Transparent Bridges.\r\n");
        return CLI_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);

    if (pCurrEntry == NULL)
    {
        /* 
         * pCurrEntry must be created in Current Vlan entry for
         * the configuration of Forward All Groups.
         */
        CliPrintf (CliHandle, "\r%% Vlan is not active \r\n");
        return CLI_FAILURE;
    }

    if ((pStaticPorts->i4_Length <= 0) ||
        (pStaticPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Static Ports\r\n");
        return CLI_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pStaticPorts->pu1_OctetList,
                              pStaticPorts->i4_Length, bu1RetVal);

    if (bu1RetVal == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Static Port(s) present\r\n");
        return CLI_FAILURE;
    }

    if (VlanIsPortListValid (pStaticPorts->pu1_OctetList,
                             pStaticPorts->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Static Port(s) present\r\n");
        return CLI_FAILURE;
    }

    if ((pForbiddenPorts->i4_Length <= 0) ||
        (pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Forbidden Ports\r\n");
        return CLI_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
                              pForbiddenPorts->i4_Length, bu1RetVal);

    if (bu1RetVal == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Forbidden Port(s) present\r\n");
        return CLI_FAILURE;
    }

    if (VlanIsPortListValid (pForbiddenPorts->pu1_OctetList,
                             pForbiddenPorts->i4_Length) == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Unknown Forbidden Port(s) present\r\n");
        return CLI_FAILURE;
    }
    pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList1 == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanTestStaticDefGroupModificationInfo: Error in allocating memory "
                  "for pPortList1\r\n");
        return CLI_FAILURE;
    }
    pPortList2 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList2 == NULL)
    {
        UtilPlstReleaseLocalPortList (pPortList1);
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanTestStaticDefGroupModificationInfo: Error in allocating memory "
                  "for pPortList2\r\n");
        return CLI_FAILURE;
    }

    VLAN_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
    VLAN_MEMSET (pPortList2, 0, sizeof (tLocalPortList));
    if (VLAN_MEMCMP (pStaticPorts->pu1_OctetList, gNullPortList,
                     VLAN_PORT_LIST_SIZE) == 0)
    {
        if (u4Type == VLAN_ALL_GROUPS)
        {
            VLAN_MEMCPY (pPortList1,
                         pCurrEntry->AllGrps.StaticPorts, VLAN_PORT_LIST_SIZE);
        }
        else
        {
            VLAN_MEMCPY (pPortList1,
                         pCurrEntry->UnRegGrps.StaticPorts,
                         VLAN_PORT_LIST_SIZE);
        }
    }
    else
    {
        VLAN_MEMCPY (pPortList1,
                     pStaticPorts->pu1_OctetList, pStaticPorts->i4_Length);
    }

    if (VLAN_MEMCMP (pForbiddenPorts->pu1_OctetList, gNullPortList,
                     VLAN_PORT_LIST_SIZE) == 0)
    {
        if (u4Type == VLAN_ALL_GROUPS)
        {
            VLAN_MEMCPY (pPortList2,
                         pCurrEntry->AllGrps.ForbiddenPorts,
                         VLAN_PORT_LIST_SIZE);
        }
        else
        {
            VLAN_MEMCPY (pPortList2,
                         pCurrEntry->UnRegGrps.ForbiddenPorts,
                         VLAN_PORT_LIST_SIZE);
        }
    }
    else
    {
        VLAN_MEMCPY (pPortList2,
                     pForbiddenPorts->pu1_OctetList,
                     pForbiddenPorts->i4_Length);
    }

    VLAN_ARE_PORTS_EXCLUSIVE (pPortList1, pPortList2, u1Result);

    UtilPlstReleaseLocalPortList (pPortList1);
    UtilPlstReleaseLocalPortList (pPortList2);
    if (u1Result == VLAN_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Static Ports and Forbidden Ports "
                   "are overlapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
#endif /* EXTENDED_FILTERING_CHANGE */
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssVlanShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the VLAN debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssVlanShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4GblDbgLevel = 0;

    VLAN_LOCK ();

    if (VlanSelectContext (L2IWF_DEFAULT_CONTEXT) != VLAN_SUCCESS)
    {
        UNUSED_PARAM (CliHandle);

        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        UNUSED_PARAM (CliHandle);
        VlanReleaseContext ();

        VLAN_UNLOCK ();
        return;
    }

    nmhGetDot1qFutureVlanDebug (&i4DbgLevel);
    nmhGetFsMIDot1qFutureVlanGlobalTrace (&i4GblDbgLevel);

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    if ((i4DbgLevel == 0) && (i4GblDbgLevel == VLAN_DISABLED))
    {
        return;
    }
    CliPrintf (CliHandle, "\rVLAN :");

    if (i4GblDbgLevel == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n  VLAN global debugging is on");
    }
    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN init and shutdown debugging is on");
    }
    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN management debugging is on");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN data path debugging is on");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN control path debugging is on");
    }
    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN packet dump debugging is on");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN resources debugging is on");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN all failure debugging is on");
    }
    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN buffer debugging is on");
    }
    if ((i4DbgLevel & VLAN_MOD_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN Forward debugging is on");
    }
    if ((i4DbgLevel & VLAN_PRI_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN priority debugging is on");
    }
    if ((i4DbgLevel & VLAN_RED_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  VLAN redundancy is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanGvrpLrntPortsDb                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the entries in the GVRP     */
/*                        learnt ports table. This function will be invoked  */
/*                        when the node is not active.                       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Context.                           */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
ShowVlanGvrpLrntPortsDb (tCliHandle CliHandle)
{
    UINT1              *pLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE LrntPortList;
    tSNMP_OCTET_STRING_TYPE LocalLrntPortList;
    tVlanRedGvrpLearntEntry *pGvrpEntry;
    tPortList          *pPortList = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    CliPrintf (CliHandle, "\r\n GVRP Learnt ports table");
    CliPrintf (CliHandle, "\r\n ----------------------- \r\n");
    pLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "ShowVlanGvrpLrntPortsDb: Error in allocating memory "
                  "for pLocalPortList\r\n");
        return;
    }

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        UtilPlstReleaseLocalPortList (pLocalPortList);
        return;
    }

    VLAN_RED_LOCK ();

    LocalLrntPortList.pu1_OctetList = pLocalPortList;
    LocalLrntPortList.i4_Length = sizeof (tLocalPortList);

    VLAN_SLL_SCAN (&VLAN_GVRP_LRNT_PORT_LIST (),
                   pGvrpEntry, tVlanRedGvrpLearntEntry *)
    {
        CliPrintf (CliHandle, "Vlan ID             : %d\r\n",
                   pGvrpEntry->VlanId);

        MEMCPY (LocalLrntPortList.pu1_OctetList, pGvrpEntry->LearntPortList,
                sizeof (tLocalPortList));

        MEMSET (pPortList, 0, sizeof (tPortList));
        LrntPortList.pu1_OctetList = *pPortList;
        LrntPortList.i4_Length = sizeof (tPortList);
        VlanConvertToIfPortList (LocalLrntPortList.pu1_OctetList,
                                 LrntPortList.pu1_OctetList);

        if (CliOctetToIfName (CliHandle,
                              "Member Ports        :",
                              &LrntPortList,
                              BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                              sizeof (tPortList), 0, &u4PagingStatus,
                              6) == CLI_FAILURE)
        {
            break;
        }
        CliPrintf (CliHandle,
                   "----------------------------------------------------\r\n");

    }

    VLAN_RED_UNLOCK ();
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    UtilPlstReleaseLocalPortList (pLocalPortList);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanGmrpLrntPortsDb                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the entries in the GMRP     */
/*                        learnt ports table. This function will be invoked  */
/*                        when the node is not active.                       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Context.                           */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
ShowVlanGmrpLrntPortsDb (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "\r\n GMRP Learnt ports table \r\n\r\n");

    VLAN_RED_LOCK ();

    CliPrintf (CliHandle, "\r\nVlan    Mac Address         Type     Ports\r\n");
    CliPrintf (CliHandle, "----    -----------         ----     -----\r\n");

    RBTreeWalk (VLAN_GMRP_LRNT_PORTS_TBL (),
                ShowVlanGmrpLrntEntry, (VOID *) (FS_ULONG) CliHandle, NULL);

    VLAN_RED_UNLOCK ();
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowVlanGmrpLrntEntry                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the given GMRP learnt entry.*/
/*                                                                           */
/*     INPUT            : pRBElem - GMRP Learnt entry                        */
/*                        visit - post order or preorder etc                 */
/*                        u4Level - Level in the tree                        */
/*                        pArg - carries CliHandle                           */
/*                        pOut - Output arg. currently NULL                  */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowVlanGmrpLrntEntry (tRBElem * pRBElem, eRBVisit visit,
                       UINT4 u4Level, VOID *pArg, VOID *pOut)
{
    UINT1              *pLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE LrntPortList;
    tSNMP_OCTET_STRING_TYPE LocalLrntPortList;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tCliHandle          CliHandle = (tCliHandle) CLI_PTR_TO_I4 (pArg);
    tVlanRedGmrpLearntEntry *pGmrpEntry;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tPortList          *pPortList = NULL;

    UNUSED_PARAM (pOut);
    UNUSED_PARAM (u4Level);

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return RB_WALK_BREAK;
    }

    pGmrpEntry = (tVlanRedGmrpLearntEntry *) pRBElem;

    if (visit == leaf || visit == postorder)
    {
        CliPrintf (CliHandle, "\r%-8d", pGmrpEntry->VlanId);

        PrintMacAddress (pGmrpEntry->McastAddr, au1String);
        CliPrintf (CliHandle, "%s", au1String);

        CliPrintf (CliHandle, "Learnt   ");

        LocalLrntPortList.i4_Length = sizeof (tLocalPortList);
        pLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pLocalPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "ShowVlanGvrpLrntPortsDb: Error in allocating memory "
                      "for pLocalPortList\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            return RB_WALK_BREAK;
        }

        MEMSET (pLocalPortList, 0, sizeof (tLocalPortList));
        LocalLrntPortList.pu1_OctetList = pLocalPortList;

        MEMCPY (LocalLrntPortList.pu1_OctetList, pGmrpEntry->LearntPortList,
                sizeof (tLocalPortList));

        MEMSET (pPortList, 0, sizeof (tPortList));
        LrntPortList.pu1_OctetList = *pPortList;
        LrntPortList.i4_Length = sizeof (tPortList);
        VlanConvertToIfPortList (LocalLrntPortList.pu1_OctetList,
                                 LrntPortList.pu1_OctetList);

        if (CliOctetToIfName (CliHandle,
                              NULL,
                              &LrntPortList,
                              BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                              sizeof (tPortList), 0, &u4PagingStatus,
                              6) == CLI_FAILURE)
        {
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            UtilPlstReleaseLocalPortList (pLocalPortList);
            return RB_WALK_BREAK;
        }
        UtilPlstReleaseLocalPortList (pLocalPortList);
    }

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    return RB_WALK_CONT;
}
#endif /* L2RED_WANTED */

/*****************************************************************************/
/*     FUNCTION NAME    : VlanGlobalShowRunningConfig                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configuration  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                        u4VlanId - Specified vlan id, for configuration    */
/*                        u4Module - Specified Module (vlan/all)             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGlobalShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4Status = 0;

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    nmhGetDot1qFutureVlanSwStatsEnabled (&i4Status);

    VLAN_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    if (i4Status == VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r set sw-stats enable \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r set sw-stats disable \r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanMplsShowRunningConfig                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configuration  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                        u4VlanId - Specified vlan id, for configuration    */
/*                        u4Module - Specified Module (vlan/all)             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMplsShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4VlanId, UINT4 u4Module)
{
    INT4                i4RetVal = 0;
    UINT1               u1HeadFlag = VLAN_FALSE;
    /* If it is a ICCH default vlan skip configurations from SRC */
    if (VlanIcchIsIcclVlan ((UINT2) u4VlanId) == OSIX_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    if (u4VlanId == 0)
    {
        i4RetVal =
            VlanShowRunningConfigScalars (CliHandle, u4ContextId, &u1HeadFlag);
        VlanPbShowRunningConfigScalars (CliHandle, u4ContextId, &u1HeadFlag);
    }
    VlanShowRunningConfigVlanInfo (CliHandle, u4ContextId, u4VlanId, u4Module,
                                   &u1HeadFlag);
    if ((u4VlanId == 0) && (i4RetVal == CLI_SUCCESS))
    {
        VlanShowRunningConfigTables (CliHandle, u4ContextId, &u1HeadFlag);
    }

    if (u1HeadFlag == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "! \r\n");
    }

    VLAN_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configuration  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                        u4VlanId - Specified vlan id, for configuration    */
/*                        u4Module - Specified Module (vlan/all)             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4VlanId, UINT4 u4Module)
{
    UINT4               u4SysMode;
    INT4                i4RetVal = 0;
    UINT1               u1HeadFlag = VLAN_FALSE;

    /* If it is a ICCH default vlan skip configurations from SRC */
    if (VlanIcchIsIcclVlan ((UINT2) u4VlanId) == OSIX_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    u4SysMode = VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID);

    if (u4VlanId == 0)
    {
        i4RetVal =
            VlanShowRunningConfigScalars (CliHandle, u4ContextId, &u1HeadFlag);
        VlanPbShowRunningConfigScalars (CliHandle, u4ContextId, &u1HeadFlag);
    }
    VlanShowRunningConfigVlanInfo (CliHandle, u4ContextId, u4VlanId, u4Module,
                                   &u1HeadFlag);

    if ((u4VlanId == 0) && (i4RetVal == CLI_SUCCESS))
    {
        VlanShowRunningConfigTables (CliHandle, u4ContextId, &u1HeadFlag);
        if (u1HeadFlag == VLAN_TRUE)
        {
            CliPrintf (CliHandle, "! \r\n");
            u1HeadFlag = VLAN_FALSE;
        }

        VlanShowRunningConfigInterface (CliHandle, u4ContextId);
    }
    else
    {
        if ((u4SysMode == VCM_MI_MODE) &&
            (u4Module != ISS_MPLS_SHOW_RUNNING_CONFIG)
            && (u1HeadFlag == VLAN_TRUE))
        {
            CliPrintf (CliHandle, "! \r\n");
            u1HeadFlag = VLAN_FALSE;
        }
    }

    VLAN_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanShowRunningConfigScalars                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configuration  */
/*                        for scalars objects                                */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT1 *pu1HeadFlag)
{
    UINT1               au1ContextName[VLAN_SWITCH_ALIAS_LEN];
    UINT4               u4SysMode;
    INT4                i4ShutStatus = VLAN_TRUE;
    INT4                i4ModStatus = 0;
    INT4                i4Macbased = 0;
    INT4                i4Subnetbased = VLAN_INIT_VAL;
    INT4                i4PortBased = 0;
    INT4                i4LearningMode = 0;
    INT4                i4TrfClass = 0;
    INT4                i4Age = 0;
    INT4                i4HybridType = 0;
    UINT4               u4MacLimit = 0;
    INT4                i4BaseBridgeMode = VLAN_INIT_VAL;
    INT4                i4GlobMacLearnStat = 0;
    INT4                i4FilteringStatus = 0;
    INT4                i4UserDefinedTPID = 0;
    UINT1               u1VlanShutFlag = VLAN_FALSE;
    u4SysMode = VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID);
    nmhGetFsMIDot1qFutureVlanShutdownStatus (u4ContextId, &i4ShutStatus);
    nmhGetFsMIDot1qFutureVlanStatus (u4ContextId, &i4ModStatus);
    nmhGetFsMIDot1qFutureVlanMacBasedOnAllPorts (u4ContextId, &i4Macbased);
    nmhGetFsMIDot1qFutureVlanSubnetBasedOnAllPorts (u4ContextId,
                                                    &i4Subnetbased);
    nmhGetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts (u4ContextId,
                                                       &i4PortBased);
    nmhGetFsMIDot1qFutureVlanLearningMode (u4ContextId, &i4LearningMode);
    nmhGetFsDot1dTrafficClassesEnabled (u4ContextId, &i4TrfClass);
    nmhGetFsDot1dTpAgingTime (u4ContextId, &i4Age);
    nmhGetFsMIDot1qFutureVlanHybridTypeDefault (u4ContextId, &i4HybridType);
    nmhGetFsMIDot1qFutureUnicastMacLearningLimit ((INT4) u4ContextId,
                                                  &u4MacLimit);
    nmhGetFsMIDot1qFutureBaseBridgeMode ((INT4) u4ContextId, &i4BaseBridgeMode);
    nmhGetFsMIDot1qFutureVlanGlobalMacLearningStatus ((INT4) u4ContextId,
                                                      &i4GlobMacLearnStat);
    nmhGetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria ((INT4) u4ContextId,
                                                             &i4FilteringStatus);

    nmhGetFsMIDot1qFutureVlanUserDefinedTPID ((INT4) u4ContextId,
                                              &i4UserDefinedTPID);

    if (i4ShutStatus == VLAN_SNMP_TRUE)
    {
#ifndef NPAPI_WANTED
        u1VlanShutFlag = VLAN_TRUE;
#endif
    }
    if (u4SysMode == VCM_MI_MODE)
    {
        if ((i4BaseBridgeMode != DOT_1Q_VLAN_MODE)
            || (u1VlanShutFlag == VLAN_TRUE) || (i4ModStatus == VLAN_DISABLED)
            || (i4GlobMacLearnStat == VLAN_DISABLED)
            || (i4Macbased == VLAN_ENABLED) || (i4Subnetbased == VLAN_ENABLED)
            || (i4PortBased == VLAN_DISABLED)
            || (i4LearningMode == VLAN_HYBRID_LEARNING)
            || (i4LearningMode == VLAN_SHARED_LEARNING)
            || (i4TrfClass == VLAN_DISABLED) || (i4Age != NORMAL_AGEOUT_TIME)
            || (i4HybridType != VLAN_INDEP_LEARNING)
            || (u4MacLimit != VLAN_DEF_UNICAST_LEARNING_LIMIT)
            || (i4FilteringStatus != VLAN_SNMP_TRUE)
            || (i4UserDefinedTPID != 0))
        {
            VLAN_MEMSET (au1ContextName, 0, VLAN_SWITCH_ALIAS_LEN);

            VlanVcmGetAliasName (u4ContextId, au1ContextName);
            if (STRLEN (au1ContextName) != 0)
            {
                CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
                *pu1HeadFlag = VLAN_TRUE;
            }
        }
        else
        {
            return CLI_SUCCESS;
        }
    }

    if (i4BaseBridgeMode != DOT_1Q_VLAN_MODE)
    {
        CliPrintf (CliHandle, "base bridge-mode dot1d-bridge\r\n");
    }

    if (i4ShutStatus == VLAN_SNMP_TRUE)
    {
#ifndef NPAPI_WANTED
        CliPrintf (CliHandle, "shutdown vlan\r\n");
#endif
        if (i4BaseBridgeMode == DOT_1D_BRIDGE_MODE)
        {
            return CLI_SUCCESS;
        }
        return CLI_FAILURE;
    }

    if (i4ModStatus == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "set vlan disable\r\n");
    }

    if (i4GlobMacLearnStat == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "set mac-learning disable\r\n");
    }

    if (i4Macbased == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "mac-vlan\r\n");
    }

    if (i4Subnetbased == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "subnet-vlan\r\n");
    }

    if (i4PortBased == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "no protocol-vlan\r\n");
    }

    if (i4LearningMode == VLAN_HYBRID_LEARNING)
    {
        CliPrintf (CliHandle, "vlan learning mode hybrid\r\n");
    }
    if (i4LearningMode == VLAN_SHARED_LEARNING)
    {
        CliPrintf (CliHandle, "vlan learning mode svl\r\n");
    }

    if (i4TrfClass == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "set vlan traffic-classes disable\r\n");
    }

    if (i4Age != NORMAL_AGEOUT_TIME)
    {
        CliPrintf (CliHandle, "mac-address-table aging-time %d\r\n", i4Age);
    }

    if (i4HybridType != VLAN_INDEP_LEARNING)
    {
        CliPrintf (CliHandle, "vlan default hybrid type svl\r\n");
    }

    if (u4MacLimit != VLAN_DEF_UNICAST_LEARNING_LIMIT)
    {
        CliPrintf (CliHandle, "unicast-mac learning limit %d\r\n", u4MacLimit);
    }

    if (i4FilteringStatus != VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "set filtering-utility-criteria disable \r\n");
    }

    if (i4UserDefinedTPID != 0)
    {
        CliPrintf (CliHandle, "user-defined TPID %d\r\n", i4UserDefinedTPID);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanShowRunningConfigInterface                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configuration  */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4CurrentIfIndex = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IcchIfIndex = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1HeadFlag = VLAN_FALSE;
    VlanIcchGetIcclIfIndex (&u4IcchIfIndex);

    /* Loop to get all the mapped interface(s) of a context */
    while (VlanGetNextPortInContext (u4ContextId, u4CurrentIfIndex,
                                     &u4NextIfIndex) == VLAN_SUCCESS)
    {
        VlanCfaCliConfGetIfName (u4NextIfIndex, (INT1 *) au1NameStr);

        if (u4IcchIfIndex == u4NextIfIndex)
        {
            u4CurrentIfIndex = u4NextIfIndex;
            continue;
        }

        /* Relinquish the control to the task which waits for vlan lock */
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        VlanGarpShowRunningConfigInterface (CliHandle, u4NextIfIndex,
                                            &u1HeadFlag);
        CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
        VLAN_LOCK ();
        if (u1HeadFlag == VLAN_TRUE)
        {
            u4PagingStatus = CliPrintf (CliHandle, "! \r\n");
            u1HeadFlag = VLAN_FALSE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        u4CurrentIfIndex = u4NextIfIndex;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanGarpShowRunningConfigInterface                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN and GARP       */
/*                        configuration for interfaces                       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4CurrentIfIndex - Global interface index          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGarpShowRunningConfigInterface (tCliHandle CliHandle,
                                    UINT4 u4CurrentIfIndex, UINT1 *pu1HeadFlag)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT1               u1PbPortType = 0;

    /* Accessing MI nmhGet and nmhGetNext routines slows down the 
     * SRC output. Hence it is better to access the SI nmhGet and nmhGetNext
     * routines. So from the given IfIndex do the following:
     *            - get the Context id and Local port number.
     *            - Select the context
     *            - call SI nmh routines with local port number.
     * 
     * If we register VlanLock and VlanUnLock only with CliPrintf, then we
     * may face the following issue:
     *         - if the SRC output crosses a page, 
     *                 - the CLI will call VlanUnlock.
     *                 - if the user presses any key other than 'q', the CLI
     *                   will call VlanLock function.
     *                   As VlanLock selects the default context, and this
     *                   may be same as the context selected in this 
     *                   show running config function. This will result in
     *                   wrong SRC out put.
     * To over come the above problem,  the following are added:
     *                - A global variable (gu4VlanCliContext) - to store the 
     *                  Context required by SRC function.
     *                - and the following 2 functions:
     *                         - VlanLockAndSelCliContext and
     * For SRC functions that uses SI nmh routines the following to be done:
     *        - register with CLI the above mentioned function and VlanUnLock. 
     *        - After selecting the context, set the gu4VlanCliContext to
     *          the required context.
     * By this way, once the paging happens the CLI will call 
     * VlanLockAndSelCliContext, when the user want to see more output.
     * And this VlanLockAndSelCliContext will take the vlan sem and will
     * also select the proper context.
     *
     */
    CliRegisterLock (CliHandle, VlanLockAndSelCliContext, VlanUnLock);
    VLAN_LOCK ();
    if (VlanGetContextInfoFromIfIndex (u4CurrentIfIndex, &u4ContextId,
                                       &u2LocalPortId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    /* Select the context here as we call SI nmh routines below */
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    /* Store the selected virtual context in a global variable so that the same
     * can be restored after doing VLAN-LOCK in CliPrintf
     * */

    gu4VlanCliContext = u4ContextId;

    /* Get the Bridge Port-type */
    VlanL2IwfGetPbPortType (u4CurrentIfIndex, &u1PbPortType);

    if (VlanShowRunningConfigInterfaceDetails
        (CliHandle, u4ContextId, u4CurrentIfIndex, (INT4) u2LocalPortId,
         u1PbPortType, pu1HeadFlag) == CLI_FAILURE)
    {
        /* Relinquish the control to the task which waits for vlan lock */
        VlanReleaseContext ();
        gu4VlanCliContext = 0;
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        /* Break the loop here if the user press 'q' in the CLI prompt */
        return CLI_FAILURE;
    }
    gu4VlanCliContext = 0;
    /* GarpShowRunningConfigInterfaceDetails is taking GARP_LOCK, and should be
     * called after releasing VLAN_LOCK, otherwise it will lead to deadlock in 
     * some senarios.*/
    VLAN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
#ifdef GARP_WANTED
    GarpShowRunningConfigInterfaceDetails (CliHandle, u4ContextId,
                                           (INT4) u2LocalPortId, pu1HeadFlag);
#endif
    CliRegisterLock (CliHandle, VlanLockAndSelCliContext, VlanUnLock);
    VLAN_LOCK ();

    /* Select the context here as we call SI nmh routines below */
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        gu4VlanCliContext = 0;
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    /* Store the selected virtual context in a global variable so that the same
     * can be restored after doing VLAN-LOCK in CliPrintf
     * */

    gu4VlanCliContext = u4ContextId;

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        gu4VlanCliContext = 0;
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    if (VlanPbShowRunningConfigInterfaceDetails (CliHandle,
                                                 (INT4) u2LocalPortId,
                                                 u1PbPortType,
                                                 pu1HeadFlag) == CLI_FAILURE)
    {
        VlanReleaseContext ();
        gu4VlanCliContext = 0;
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        /* Break the loop here if the user press 'q' in the CLI prompt */
        return CLI_FAILURE;
    }

    /* Relinquish the control to the task which waits for vlan lock */
    VlanReleaseContext ();
    gu4VlanCliContext = 0;
    VLAN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for a particular interface                         */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4IfIndex - Global interface index                 */
/*                        i4LocalPort - Local port of particular context     */
/*                        u1PbPortType - Port type                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanShowRunningConfigInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                                       UINT4 u4IfIndex, INT4 i4LocalPort,
                                       UINT1 u1PbPortType, UINT1 *pu1HeadFlag)
{
    INT4                i4Status;
    INT4                i4RetVal = 0;
    INT4                i4MaxTrafficClass = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrPort = 0;
    INT4                i4NextGroupId = 0;
    INT4                i4CurrentGroupId = 0;
    INT4                i4RowStatus;
    INT4                i4PortProtoBased = 0;
    INT4                i4SubnetBased = 0;
    INT4                i4MacBased = 0;
    INT4                i4BridgeMode = VLAN_CUSTOMER_BRIDGE_MODE;
    INT4                i4CurrentPriority = VLAN_MIN_PRIORITY - 1;
    INT4                i4NextPriority = VLAN_MIN_PRIORITY - 1;
    INT4                i4AllowableTPID1 = 0;
    INT4                i4AllowableTPID2 = 0;
    INT4                i4AllowableTPID3 = 0;
    INT4                i4EgressTPIDType = 0;
    INT4                i4UnicastMacSecType = 0;
    INT4                i4PacketReflectionStatus = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IcchIfIndex = 0;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
#ifndef BCMX_WANTED
    UINT4               u4CurrSrcIPAddr = VLAN_INIT_VAL;
    UINT4               u4NextSrcIPAddr = VLAN_INIT_VAL;
    UINT4               u4CurrSubnetMask = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMask = VLAN_INIT_VAL;
#endif

    VlanIcchGetIcclIfIndex (&u4IcchIfIndex);
    if (u4IcchIfIndex == u4IfIndex)
    {
        return CLI_SUCCESS;
    }

    if (nmhValidateIndexInstanceDot1qPortVlanTable (i4LocalPort) ==
        SNMP_SUCCESS)
    {
        /* Display Port Pvid */
        if (VlanSrcDisplayPortPvid (CliHandle, u4ContextId,
                                    u4IfIndex, i4LocalPort,
                                    pu1HeadFlag) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4Status = nmhGetDot1qPortAcceptableFrameTypes (i4LocalPort, &i4RetVal);

        if (u1PbPortType == VLAN_CNP_PORTBASED_PORT ||
            u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT ||
            u1PbPortType == VLAN_CUSTOMER_EDGE_PORT)
        {
            if ((i4Status == SNMP_SUCCESS) && i4RetVal == VLAN_ADMIT_ALL_FRAMES)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport acceptable-frame-type all\r\n");
            }
            else if ((i4Status == SNMP_SUCCESS) &&
                     i4RetVal == VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport acceptable-frame-type tagged\r\n");
            }
        }
        else
        {
            /* For SISP logical interfaces, the default value of acceptable
             * frame type will be Admit only tagged. Hence, it should not be
             * displayed in SRC
             * */
            if ((i4Status == SNMP_SUCCESS) &&
                (i4RetVal == VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES) &&
                (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_TRUE))
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport acceptable-frame-type tagged\r\n");
            }
            else if ((i4Status == SNMP_SUCCESS) &&
                     i4RetVal
                     == VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
            {

                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport acceptable-frame-type "
                           "untaggedAndPriorityTagged\r\n");
            }
        }
        i4Status = nmhGetDot1qPortIngressFiltering (i4LocalPort, &i4RetVal);

        if (u1PbPortType == VLAN_CNP_TAGGED_PORT)
        {
            if ((i4Status == SNMP_SUCCESS) && i4RetVal == VLAN_SNMP_FALSE)
            {

                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, "no switchport ingress-filter\r\n");
            }
        }
        else
        {
            if ((i4Status == SNMP_SUCCESS) && i4RetVal == VLAN_SNMP_TRUE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

                CliPrintf (CliHandle, "switchport ingress-filter\r\n");
            }
        }

        nmhGetDot1qFuturePortPacketReflectionStatus (i4LocalPort,
                                                     &i4PacketReflectionStatus);

        if (i4PacketReflectionStatus == VLAN_ENABLED)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle, "set packet-reflection enable\r\n");
        }

        VlanSrcDisplayPortType (CliHandle, i4LocalPort, u1PbPortType,
                                pu1HeadFlag);

        i4Status =
            nmhGetDot1qFutureVlanPortMacBasedClassification (i4LocalPort,
                                                             &i4RetVal);

        if (i4Status == SNMP_SUCCESS)
        {
            i4Status = nmhGetDot1qFutureVlanMacBasedOnAllPorts (&i4MacBased);
            if (i4Status == SNMP_SUCCESS)
            {
                if ((i4MacBased != VLAN_ENABLED) &&
                    (i4RetVal != VLAN_SNMP_FALSE))
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "port mac-vlan\r\n");
                }
                else if ((i4MacBased == VLAN_ENABLED) &&
                         (i4RetVal == VLAN_SNMP_FALSE))
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "no port mac-vlan\r\n");
                }
            }
        }
        i4Status =
            nmhGetDot1qFutureVlanPortSubnetBasedClassification (i4LocalPort,
                                                                &i4RetVal);

        if (i4Status == SNMP_SUCCESS)
        {
            i4Status =
                nmhGetDot1qFutureVlanSubnetBasedOnAllPorts (&i4SubnetBased);
            if (i4Status == SNMP_SUCCESS)
            {
                if ((i4SubnetBased == VLAN_DISABLED) &&
                    (i4RetVal == VLAN_SNMP_TRUE))
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "port subnet-vlan\r\n");
                }
                else if ((i4SubnetBased == VLAN_ENABLED) &&
                         (i4RetVal == VLAN_SNMP_FALSE))
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);

                    CliPrintf (CliHandle, "no port subnet-vlan\r\n");
                }
            }
        }
        i4Status =
            nmhGetDot1qFutureVlanPortPortProtoBasedClassification (i4LocalPort,
                                                                   &i4RetVal);
        if (i4Status == SNMP_SUCCESS)
        {

            i4Status =
                nmhGetDot1qFutureVlanPortProtoBasedOnAllPorts
                (&i4PortProtoBased);
            if ((i4Status == SNMP_SUCCESS)
                && (i4PortProtoBased != VLAN_DISABLED)
                && (i4RetVal == VLAN_SNMP_FALSE))
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

                CliPrintf (CliHandle, "no port protocol-vlan\r\n");
            }
        }
        i4Status = nmhGetDot1dPortDefaultUserPriority (i4LocalPort, &i4RetVal);
        if ((i4Status == SNMP_SUCCESS) && i4RetVal != VLAN_DEF_USER_PRIORITY)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle, "switchport priority default %d\r\n",
                       i4RetVal);
        }

        i4Status = nmhGetDot1dPortNumTrafficClasses (i4LocalPort, &i4RetVal);
        if ((i4Status == SNMP_SUCCESS) && i4RetVal != VLAN_MAX_TRAFF_CLASS)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle, "vlan max-traffic-class %d\r\n", i4RetVal);
        }
        i4Status = nmhGetDot1qFutureVlanBridgeMode (&i4BridgeMode);

        i4Status = nmhGetDot1qFutureVlanFilteringUtilityCriteria
            (i4LocalPort, &i4RetVal);

        if ((i4Status == SNMP_SUCCESS) &&
            i4RetVal != VLAN_DEFAULT_FILTERING_CRITERIA)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle,
                       "switchport filtering-utility-criteria enhanced\r\n");
        }
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
        {

            i4Status =
                nmhGetDot1qFutureVlanPortUnicastMacLearning (i4LocalPort,
                                                             &i4RetVal);

            if ((i4Status == SNMP_SUCCESS) && (i4RetVal != VLAN_ENABLED))
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

                CliPrintf (CliHandle,
                           "switchport unicast-mac learning disable\r\n");
            }
        }

        i4Status = nmhGetDot1qFutureVlanPortUnicastMacSecType (i4LocalPort,
                                                               &i4UnicastMacSecType);

        if (i4UnicastMacSecType == VLAN_CLI_UNICAST_MAC_SEC_SAV)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle,
                       "switchport port-security violation protect\r\n");
        }
        else if (i4UnicastMacSecType == VLAN_CLI_UNICAST_MAC_SEC_SHV)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle,
                       "switchport port-security violation restrict\r\n");
        }

        i4Status =
            nmhGetDot1qFutureVlanPortEgressTPIDType (i4LocalPort,
                                                     &i4EgressTPIDType);
        if ((i4Status == SNMP_SUCCESS) && (i4EgressTPIDType != 0))
        {
            if (i4EgressTPIDType != VLAN_PORT_EGRESS_PORTBASED)    /*Default value */
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

                CliPrintf (CliHandle,
                           "switchport egress TPID-type vlanbased\r\n");
            }
        }
        i4Status =
            nmhGetDot1qFutureVlanPortAllowableTPID1 (i4LocalPort,
                                                     &i4AllowableTPID1);
        if ((i4Status == SNMP_SUCCESS) && (i4AllowableTPID1 != 0))
        {
            if (i4AllowableTPID1 == VLAN_PROTOCOL_ID)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

                CliPrintf (CliHandle,
                           "switchport encapsulation dot1ad vlan-type tpid1 CTAG\r\n");
            }
            if (i4AllowableTPID1 == VLAN_PROVIDER_PROTOCOL_ID)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

                CliPrintf (CliHandle,
                           "switchport encapsulation dot1ad vlan-type tpid1 STAG\r\n");
            }

        }
        i4Status =
            nmhGetDot1qFutureVlanPortAllowableTPID2 (i4LocalPort,
                                                     &i4AllowableTPID2);
        if ((i4Status == SNMP_SUCCESS) && (i4AllowableTPID2 != 0))
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle,
                       "switchport encapsulation dot1ad vlan-type tpid2 \r\n");
        }
        i4Status =
            nmhGetDot1qFutureVlanPortAllowableTPID3 (i4LocalPort,
                                                     &i4AllowableTPID3);
        if ((i4Status == SNMP_SUCCESS) && (i4AllowableTPID3 != 0))
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle,
                       "switchport encapsulation dot1ad vlan-type tpid3 \r\n");
        }
    }
    i4CurrPort = i4LocalPort;
    while (nmhGetNextIndexDot1vProtocolPortTable
           (i4CurrPort, &i4NextPort, i4CurrentGroupId, &i4NextGroupId)
           == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            /* Break the loop here, if the local port is changed */
            break;
        }
        nmhGetDot1vProtocolPortRowStatus (i4NextPort, i4NextGroupId,
                                          &i4RowStatus);
        if (i4RowStatus == VLAN_ACTIVE)
        {
            nmhGetDot1vProtocolPortGroupVid (i4NextPort,
                                             i4NextGroupId, &i4RetVal);

            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, "switchport map protocols-group");
            CliPrintf (CliHandle, " %d ", i4NextGroupId);
            CliPrintf (CliHandle, "vlan");
            u4PagingStatus = CliPrintf (CliHandle, " %d\r\n", i4RetVal);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, 
             * exit
             */
            return CLI_FAILURE;
        }
        i4CurrentGroupId = i4NextGroupId;
        i4CurrPort = i4NextPort;
    }

    i4CurrPort = i4LocalPort;
    i4NextPort = 0;
    while (nmhGetNextIndexDot1dTrafficClassTable
           (i4CurrPort, &i4NextPort,
            i4CurrentPriority, &i4NextPriority) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            /* Break the loop here, if the local port is changed */
            break;
        }
        nmhGetDot1dTrafficClass (i4NextPort, i4NextPriority, &i4RetVal);
        nmhGetDot1dPortNumTrafficClasses (i4NextPort, &i4MaxTrafficClass);

        if (i4RetVal !=
            gau1PriTrfClassMap[i4NextPriority][i4MaxTrafficClass - 1])
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle, "vlan map-priority");
            CliPrintf (CliHandle, " %d", i4NextPriority);

            CliPrintf (CliHandle, " traffic-class");
            u4PagingStatus = CliPrintf (CliHandle, " %d\r\n", i4RetVal);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, 
             * exit
             */
            return CLI_FAILURE;
        }
        i4CurrentPriority = i4NextPriority;
        i4CurrPort = i4NextPort;
    }

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
    i4CurrPort = i4LocalPort;
    while (nmhGetNextIndexDot1qFutureVlanPortMacMapTable
           (i4CurrPort, &i4NextPort, CurrMacAddr, &NextMacAddr) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            /* Break the loop here, if the local port is changed */
            break;
        }
        if (nmhGetDot1qFutureVlanPortMacMapRowStatus
            (i4NextPort, NextMacAddr, &i4RetVal) == SNMP_FAILURE)
        {
            break;
        }

        if (i4RetVal == VLAN_ACTIVE)
        {
            DisplayDot1qVlanMacMapTableInfo (CliHandle, NextMacAddr,
                                             i4NextPort, &u4PagingStatus);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, 
             * exit
             */
            return CLI_FAILURE;
        }
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));
        i4CurrPort = i4NextPort;
    }
#ifndef BCMX_WANTED
    i4CurrPort = u4IfIndex;
    while (nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
           (i4CurrPort, &i4NextPort, u4CurrSrcIPAddr,
            &u4NextSrcIPAddr, u4CurrSubnetMask, &u4NextSubnetMask)
           == SNMP_SUCCESS)
    {
        if (i4CurrPort != i4NextPort)
        {
            /* Break the loop here, if the local port is changed */
            break;
        }
        if (nmhGetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
            (i4NextPort, u4NextSrcIPAddr, u4NextSubnetMask, &i4RetVal)
            == SNMP_FAILURE)
        {
            break;
        }

        if (i4RetVal == VLAN_ACTIVE)
        {
            DisplayDot1qVlanSubnetMapTableInfo (CliHandle,
                                                u4NextSrcIPAddr,
                                                i4NextPort, u4NextSubnetMask,
                                                &u4PagingStatus);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            return CLI_FAILURE;
        }
        u4CurrSrcIPAddr = u4NextSrcIPAddr;
        u4CurrSubnetMask = u4NextSubnetMask;
        i4CurrPort = i4NextPort;
    }
#endif
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DisplayDot1vProtocolGroupInfo                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configuration  */
/*                        for ProtocolGroup table                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4FrameType - Specified Frame-Type                 */
/*                        ProtocolValue - Specified ProtocolValue            */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DisplayDot1vProtocolGroupInfo (tCliHandle CliHandle,
                               UINT4 u4ContextId,
                               UINT4 u4FrameType,
                               tSNMP_OCTET_STRING_TYPE ProtocolValue,
                               UINT4 *pu4PagingStatus)
{
    INT4                i4RetVal = VLAN_INIT_VAL;
    UINT1               u1Protocol = VLAN_INIT_VAL;
    UINT1               au1ProtoOctetList[VLAN_CLI_MAX_PROTO_SIZE];

    nmhGetFsDot1vProtocolGroupId (u4ContextId, (INT4) u4FrameType,
                                  &ProtocolValue, &i4RetVal);
    CliPrintf (CliHandle, "map protocol");

    if (VlanUtilGetProtocol (&u1Protocol,
                             ProtocolValue.pu1_OctetList,
                             (UINT1) ProtocolValue.i4_Length) != CLI_SUCCESS)
    {
        CLI_MEMSET (au1ProtoOctetList, 0, VLAN_CLI_MAX_PROTO_SIZE);
        CliOctetToStr (ProtocolValue.pu1_OctetList,
                       ProtocolValue.i4_Length,
                       au1ProtoOctetList, VLAN_CLI_MAX_PROTO_SIZE);
        CliPrintf (CliHandle, " other");
        CliPrintf (CliHandle, " %s", au1ProtoOctetList);
    }
    else
    {
        CliPrintf (CliHandle, " %s", gau1VlanProtocolName[u1Protocol]);
    }

    CliPrintf (CliHandle, "%s", gau1VlanFrmTypeName[u4FrameType]);
    CliPrintf (CliHandle, " protocols-group");
    *pu4PagingStatus = CliPrintf (CliHandle, " %d\r\n", i4RetVal);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DisplayDot1qVlanMacMapTableInfo                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays current vlan configuration  */
/*                        for MacMapTable                                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        NextMacAddr - Specified Mac-address                */
/*                        i4LocalPort - Local port number                    */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayDot1qVlanMacMapTableInfo (tCliHandle CliHandle,
                                 tMacAddr NextMacAddr, INT4 i4LocalPort,
                                 UINT4 *pu4PagingStatus)
{
    INT4                i4RetVal = 0;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    nmhGetDot1qFutureVlanPortMacMapVid (i4LocalPort, NextMacAddr, &i4RetVal);

    PrintMacAddress (NextMacAddr, au1String);
    CliPrintf (CliHandle, "mac-map");
    CliPrintf (CliHandle, " %s", au1String);

    CliPrintf (CliHandle, " vlan");
    CliPrintf (CliHandle, " %d", i4RetVal);

    nmhGetDot1qFutureVlanPortMacMapMcastBcastOption (i4LocalPort,
                                                     NextMacAddr, &i4RetVal);
    if (i4RetVal == VLAN_MCAST_DROP)
    {

        CliPrintf (CliHandle, " mcast-bcast discard");
    }
    *pu4PagingStatus = CliPrintf (CliHandle, " \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DisplayDot1qVlanSubnetMapTableInfo                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays current vlan configuration  */
/*                        for SubnetMapTable                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        NextSubnetAddr - Specified Subnet-address          */
/*                        i4LocalPort - Local port number                    */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayDot1qVlanSubnetMapTableInfo (tCliHandle CliHandle,
                                    UINT4 u4NextSrcIPAddr, INT4 i4LocalPort,
                                    UINT4 u4NextSubnetMask,
                                    UINT4 *pu4PagingStatus)
{
    INT4                i4Vlan = VLAN_INIT_VAL;
    INT4                i4ArpOption = VLAN_INIT_VAL;
    CHR1               *pString = NULL;
    nmhGetFsMIDot1qFutureVlanPortSubnetMapExtVid (i4LocalPort, u4NextSrcIPAddr,
                                                  u4NextSubnetMask, &i4Vlan);
    nmhGetFsMIDot1qFutureVlanPortSubnetMapExtARPOption (i4LocalPort,
                                                        u4NextSrcIPAddr,
                                                        u4NextSubnetMask,
                                                        &i4ArpOption);
    CliPrintf (CliHandle, "map subnet");
    CLI_CONVERT_IPADDR_TO_STR (pString, u4NextSrcIPAddr);
    CliPrintf (CliHandle, " %s", pString);

    CliPrintf (CliHandle, " vlan");
    CliPrintf (CliHandle, " %d", i4Vlan);

    if (i4ArpOption == VLAN_ARP_SUPPRESS)
    {
        CliPrintf (CliHandle, " arp suppress");
    }

    CliPrintf (CliHandle, " mask");
    CLI_CONVERT_IPADDR_TO_STR (pString, u4NextSubnetMask);
    CliPrintf (CliHandle, " %s", pString);

    *pu4PagingStatus = CliPrintf (CliHandle, " \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DisplayDot1qStaticUnicastTableInfo                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays current vlan configuration  */
/*                        for StaticUnicastTable                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Fid - Specified fid                              */
/*                        NextMacAddr - Specified Mac-address                */
/*                        i4RcvPort  - Specified Ports                       */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayDot1qStaticUnicastTableInfo (tCliHandle CliHandle,
                                    UINT4 u4ContextId,
                                    UINT4 u4Fid,
                                    tMacAddr NextMacAddr,
                                    UINT4 i4RcvPort, UINT4 *pu4PagingStatus)
{
    INT4                i4Status = 0;
    INT4                i4PortType = 0;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1ConnIdStr[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tVlanId             VlanId;
    tPortList          *pPortList1 = NULL;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tMacAddr            au1ConnectionId;
    UINT4               u4IfIndex;
    UINT4               u4PrevIndex;
    INT4                i4RowStatus = VLAN_NOT_READY;
    UINT4               u4Isid = 0;
    UINT1               u1PbPortType;

    MEMSET (au1ConnectionId, 0, sizeof (tMacAddr));

    if (nmhGetFsDot1qStaticUnicastRowStatus
        (u4ContextId, u4Fid, NextMacAddr, i4RcvPort,
         &i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4RowStatus != VLAN_ACTIVE)
    {
        return CLI_SUCCESS;
    }

    pPortList1 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList1 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pPortList1, 0, sizeof (tPortList));
    SnmpPorts.pu1_OctetList = *pPortList1;
    SnmpPorts.i4_Length = sizeof (tPortList);

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            nmhGetFsDot1qStaticAllowedIsMember (u4ContextId, u4Fid,
                                                NextMacAddr, i4RcvPort,
                                                u4IfIndex, &i4PortType);
            if (i4PortType == VLAN_SNMP_TRUE)
            {
                VLAN_SET_MEMBER_PORT ((*pPortList1), u4IfIndex);
            }

            u4PrevIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrevIndex, &u4IfIndex) == VLAN_SUCCESS);
    }

    nmhGetFsDot1qStaticUnicastStatus (u4ContextId, u4Fid,
                                      NextMacAddr, i4RcvPort, &i4Status);

    nmhGetFsMIDot1qFutureStaticConnectionIdentifier (u4ContextId,
                                                     u4Fid,
                                                     NextMacAddr,
                                                     i4RcvPort,
                                                     &au1ConnectionId);

    CliPrintf (CliHandle, "mac-address-table static unicast");

    PrintMacAddress (NextMacAddr, au1String);
    CliPrintf (CliHandle, " %.17s", au1String);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pPortList1);
        return CLI_FAILURE;
    }

    VlanGetVlanIdFromFdbId (u4Fid, &VlanId);

    VlanReleaseContext ();

    CliPrintf (CliHandle, " vlan ");
    CliPrintf (CliHandle, "%d", (UINT4) VlanId);
    VlanL2IwfGetPbPortType (i4RcvPort, &u1PbPortType);
    if ((u1PbPortType == VLAN_VIRTUAL_INSTANCE_PORT) &&
        (L2IwfGetPbbShutdownStatus () == VLAN_FALSE))
    {
        VlanPbbGetIsidOfVip (&u4Isid, i4RcvPort, u4ContextId);
        CliPrintf (CliHandle, " service-instance ");
        CliPrintf (CliHandle, "%d", u4Isid);

    }
    else if (i4RcvPort != 0)
    {
        VlanCfaCliConfGetIfName (i4RcvPort, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, " recv-port ");
        CliPrintf (CliHandle, "%s", au1NameStr);
    }
    CliConfOctetToIfName (CliHandle,
                          " interface ", NULL, &SnmpPorts, pu4PagingStatus);

    switch (i4Status)
    {
        case VLAN_OTHER:
            *pu4PagingStatus = CliPrintf (CliHandle, " status other");
            break;
        case VLAN_PERMANENT:
            break;
        case VLAN_DELETE_ON_RESET:
            *pu4PagingStatus = CliPrintf (CliHandle, " status deleteOnReset");
            break;
        case VLAN_DELETE_ON_TIMEOUT:
            *pu4PagingStatus = CliPrintf (CliHandle, " status deleteOnTimeout");
            break;
    }

    if (VLAN_ARE_MAC_ADDR_EQUAL (au1ConnectionId, gNullMacAddress) != VLAN_TRUE)
    {
        CliPrintf (CliHandle, " connection-Id ");
        PrintMacAddress (au1ConnectionId, au1ConnIdStr);
        CliPrintf (CliHandle, "%s\r\n", au1ConnIdStr);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }

    FsUtilReleaseBitList ((UINT1 *) pPortList1);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DisplayDot1qStaticMulticastTableInfo               */
/*                                                                           */
/*     DESCRIPTION      : This function displays current vlan configuration  */
/*                        for StaticMulticastTable                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanIndex - Specified Vlan id                    */
/*                        NextMacAddr - Specified Mac-address                */
/*                        i4RcvPort  - Specified Ports                       */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DisplayDot1qStaticMulticastTableInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                                      UINT4 u4VlanIndex,
                                      tMacAddr NextMacAddr,
                                      UINT4 i4RcvPort, UINT4 *pu4PagingStatus)
{
    INT4                i4Status = 0;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tPortList          *pPortList1 = NULL;
    tPortList          *pPortList2 = NULL;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE ForbiddenPorts;
    UINT4               u4IfIndex;
    UINT4               u4PrevIndex;
    INT4                i4PortType;
    UINT4               u4Isid = 0;
    UINT1               u1PbPortType;

    pPortList1 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList1 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pPortList2 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList2 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pPortList1);
        return CLI_FAILURE;
    }

    MEMSET (*pPortList1, 0, sizeof (tPortList));
    EgressPorts.pu1_OctetList = *pPortList1;
    EgressPorts.i4_Length = sizeof (tPortList);

    MEMSET (*pPortList2, 0, sizeof (tPortList));
    ForbiddenPorts.pu1_OctetList = *pPortList2;
    ForbiddenPorts.i4_Length = sizeof (tPortList);

    nmhGetFsDot1qStaticMulticastStatus (u4ContextId, u4VlanIndex, NextMacAddr,
                                        i4RcvPort, &i4Status);

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            nmhGetFsDot1qStaticMcastPort (u4ContextId, u4VlanIndex,
                                          NextMacAddr, i4RcvPort,
                                          u4IfIndex, &i4PortType);

            if (i4PortType == VLAN_ADD_MEMBER_PORT)
            {
                VLAN_SET_MEMBER_PORT ((*pPortList1), (UINT2) u4IfIndex);
            }
            else if (i4PortType == VLAN_ADD_FORBIDDEN_PORT)
            {
                VLAN_SET_MEMBER_PORT ((*pPortList2), u4IfIndex);
            }

            u4PrevIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrevIndex, &u4IfIndex) == VLAN_SUCCESS);
    }
    CliPrintf (CliHandle, "mac-address-table static multicast");

    PrintMacAddress (NextMacAddr, au1String);
    CliPrintf (CliHandle, " %.17s ", au1String);
    CliPrintf (CliHandle, " vlan ");
    CliPrintf (CliHandle, "%d", u4VlanIndex);
    VlanL2IwfGetPbPortType (i4RcvPort, &u1PbPortType);
    if (u1PbPortType == VLAN_VIRTUAL_INSTANCE_PORT)
    {
        VlanPbbGetIsidOfVip (&u4Isid, i4RcvPort, u4ContextId);
        CliPrintf (CliHandle, " service-instance ");
        CliPrintf (CliHandle, "%d", u4Isid);

    }
    else if (i4RcvPort != 0)
    {
        VlanCfaCliConfGetIfName (i4RcvPort, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, " recv-port ");
        CliPrintf (CliHandle, "%s", au1NameStr);
    }
    CliConfOctetToIfName (CliHandle,
                          " interface ", NULL, &EgressPorts, pu4PagingStatus);

    CliConfOctetToIfName (CliHandle,
                          " forbidden-ports ", NULL,
                          &ForbiddenPorts, pu4PagingStatus);

    switch (i4Status)
    {
        case VLAN_OTHER:
            *pu4PagingStatus = CliPrintf (CliHandle, " status other\r\n");
            break;
        case VLAN_PERMANENT:
            *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
            break;
        case VLAN_DELETE_ON_RESET:
            *pu4PagingStatus =
                CliPrintf (CliHandle, " status deleteOnReset\r\n");
            break;
        case VLAN_DELETE_ON_TIMEOUT:
            *pu4PagingStatus =
                CliPrintf (CliHandle, " status deleteOnTimeout\r\n");
            break;
    }

    FsUtilReleaseBitList ((UINT1 *) pPortList1);
    FsUtilReleaseBitList ((UINT1 *) pPortList2);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DisplayDot1dStaticTableInfo                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                        for Dot1dStaticTable                               */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Fid - Specified fid                              */
/*                        NextMacAddr - Specified Mac-address                */
/*                        i4RcvPort  - Specified Ports                       */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayDot1dStaticTableInfo (tCliHandle CliHandle,
                             tMacAddr NextMacAddr,
                             UINT4 i4RcvPort, UINT4 *pu4PagingStatus)
{
    INT4                i4Status = VLAN_INIT_VAL;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tPortList          *pPortList1 = NULL;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;

    pPortList1 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList1 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (pPortList1, VLAN_INIT_VAL, sizeof (tPortList));
    SnmpPorts.pu1_OctetList = *pPortList1;
    SnmpPorts.i4_Length = sizeof (tPortList);

    if (VLAN_IS_MCASTADDR (NextMacAddr))
    {
        CliPrintf (CliHandle, "mac-address-table static multicast");
    }
    else
    {
        CliPrintf (CliHandle, "mac-address-table static unicast");
    }

    PrintMacAddress (NextMacAddr, au1String);
    CliPrintf (CliHandle, " %.17s", au1String);

    if (i4RcvPort != VLAN_INIT_VAL)
    {
        VlanCfaCliConfGetIfName (i4RcvPort, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, " recv-port ");
        CliPrintf (CliHandle, "%s", au1NameStr);
    }

    nmhGetDot1dStaticAllowedToGoTo (NextMacAddr, i4RcvPort, &SnmpPorts);

    CliConfOctetToIfName (CliHandle,
                          " interface ", NULL, &SnmpPorts, pu4PagingStatus);

    nmhGetDot1dStaticStatus (NextMacAddr, i4RcvPort, &i4Status);

    switch (i4Status)
    {
        case VLAN_OTHER:
            *pu4PagingStatus = CliPrintf (CliHandle, " status other");
            break;
        case VLAN_PERMANENT:
            break;
        case VLAN_DELETE_ON_RESET:
            *pu4PagingStatus = CliPrintf (CliHandle, " status deleteOnReset");
            break;
        case VLAN_DELETE_ON_TIMEOUT:
            *pu4PagingStatus = CliPrintf (CliHandle, " status deleteOnTimeout");
            break;
    }

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pPortList1);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanDisplayExtFilteringInfo                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays current vlan configuration  */
/*                        for VlanCurrentTable                               */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanIndex - Specified Vlan id                    */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                        pu1isActiveVlan - Flag to verify 'vlan' display    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDisplayExtFilteringInfo (tCliHandle CliHandle,
                             UINT4 u4ContextId,
                             UINT4 u4VlanIndex,
                             UINT4 *pu4PagingStatus, UINT1 *pu1isActiveVlan)
{

#if ! defined ( NPAPI_WANTED ) || defined (NPSIM_WANTED) || defined (SWC) || defined (XCAT) ||  defined (XCAT3)
    UINT1              *pau1LocalVlanPortList = NULL;
    UINT1              *pau1LocalFwdUnregPorts = NULL;
#endif
    tPortList          *pPorts = NULL;
    tPortList          *pPorts2 = NULL;
    tPortList          *pPorts3 = NULL;
    INT4                i4RetVal = 0;
    INT4                i4RetVal2 = 0;
    INT4                i4PortType;
    CHR1                au1DefaultConf[VLAN_STATIC_MAX_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE Ports;
    tSNMP_OCTET_STRING_TYPE Ports2;
    UINT4               u4IfIndex;
    UINT4               u4PrevIndex;
#if ! defined ( NPAPI_WANTED ) || defined (NPSIM_WANTED) || defined (SWC) || defined (XCAT) ||  defined (XCAT3)
    UINT2               u2LocalPort;
#endif
    UINT1               u1VlanModeCheck = *pu1isActiveVlan;

    MEMSET (au1DefaultConf, 0, VLAN_STATIC_MAX_NAME_LEN);

    SPRINTF (au1DefaultConf, "gigabitethernet 0/1-%d", VLAN_MAX_PORTS);

    pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pPorts2 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts2 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        return CLI_FAILURE;
    }

    pPorts3 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts3 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        FsUtilReleaseBitList ((UINT1 *) pPorts2);
        return CLI_FAILURE;
    }

    MEMSET (*pPorts, 0, sizeof (tPortList));
    Ports.pu1_OctetList = *pPorts;
    Ports.i4_Length = sizeof (tPortList);

    MEMSET (*pPorts2, 0, sizeof (tPortList));
    MEMSET (*pPorts3, 0, sizeof (tPortList));

    Ports2.pu1_OctetList = *pPorts2;
    Ports2.i4_Length = sizeof (tPortList);
    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            nmhGetFsDot1qForwardAllPort (u4ContextId, u4VlanIndex,
                                         u4IfIndex, &i4PortType);

            if (i4PortType == VLAN_ADD_MEMBER_PORT)
            {
                VLAN_SET_MEMBER_PORT ((*pPorts), u4IfIndex);
            }
            else if (i4PortType == VLAN_ADD_FORBIDDEN_PORT)
            {
                VLAN_SET_MEMBER_PORT ((*pPorts2), u4IfIndex);
            }

            nmhGetFsDot1qForwardAllIsLearnt (u4ContextId, u4VlanIndex,
                                             u4IfIndex, &i4PortType);

            if (i4PortType == VLAN_SNMP_TRUE)
            {
                VLAN_RESET_MEMBER_PORT ((*pPorts), u4IfIndex);
            }

            u4PrevIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrevIndex, &u4IfIndex) == VLAN_SUCCESS);
    }

    if ((*pu1isActiveVlan == FALSE) &&
        ((FsUtilBitListIsAllZeros (Ports.pu1_OctetList,
                                   Ports.i4_Length) == OSIX_FALSE) ||
         (FsUtilBitListIsAllZeros (Ports2.pu1_OctetList,
                                   Ports2.i4_Length) == OSIX_FALSE)))
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "Vlan %d\r\n", u4VlanIndex);
        *pu1isActiveVlan = TRUE;
    }
    if ((u4VlanIndex == VLAN_DEF_VLAN_ID) && (*pu1isActiveVlan == FALSE)
        && (*Ports.pu1_OctetList != 0))
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "vlan %d\r\n", u4VlanIndex);
        *pu1isActiveVlan = TRUE;
    }

    i4RetVal =
        CliConfOctetToIfName (CliHandle, " forward-all static-ports ", NULL,
                              &Ports, pu4PagingStatus);

    if ((FsUtilBitListIsAllZeros (Ports.pu1_OctetList,
                                  Ports.i4_Length) == OSIX_TRUE) &&
        (FsUtilBitListIsAllZeros (Ports2.pu1_OctetList,
                                  Ports2.i4_Length) == OSIX_FALSE))
    {
        CliPrintf (CliHandle, " forward-all");
    }
    i4RetVal2 = CliConfOctetToIfName (CliHandle, " forbidden-ports ", NULL,
                                      &Ports2, pu4PagingStatus);

    if ((i4RetVal == CLI_SUCCESS) || (i4RetVal2 == CLI_SUCCESS))
    {
        CliPrintf (CliHandle, "\r\n");
    }

#if ! defined ( NPAPI_WANTED ) || defined (NPSIM_WANTED) || defined (SWC) || defined (XCAT) ||  defined (XCAT3)
    MEMSET (*pPorts, 0, sizeof (tPortList));
    MEMSET (*pPorts2, 0, sizeof (tPortList));
    MEMSET (*pPorts3, 0, sizeof (tPortList));

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            nmhGetFsDot1qForwardUnregPort (u4ContextId, u4VlanIndex,
                                           u4IfIndex, &i4PortType);

            if (i4PortType == VLAN_ADD_MEMBER_PORT)
            {
                VLAN_SET_MEMBER_PORT ((*pPorts), u4IfIndex);
            }
            else if (i4PortType == VLAN_ADD_FORBIDDEN_PORT)
            {
                VLAN_SET_MEMBER_PORT ((*pPorts2), u4IfIndex);
            }

            nmhGetFsDot1qForwardUnregIsLearnt (u4ContextId,
                                               u4VlanIndex, u4IfIndex,
                                               &i4PortType);
            if (i4PortType == VLAN_SNMP_TRUE)
            {
                VLAN_RESET_MEMBER_PORT ((*pPorts), u4IfIndex);
            }

            nmhGetFsDot1qVlanStaticPort (u4ContextId, u4VlanIndex,
                                         u4IfIndex, &i4PortType);

            if ((i4PortType == VLAN_ADD_UNTAGGED_PORT) ||
                (i4PortType == VLAN_ADD_TAGGED_PORT))
            {
                VLAN_SET_MEMBER_PORT ((*pPorts3), u4IfIndex);
            }

            u4PrevIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrevIndex, &u4IfIndex) == VLAN_SUCCESS);
    }
    pau1LocalVlanPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pau1LocalVlanPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanDisplayExtFilteringInfo: "
                  "Error in allocating memory for pau1LocalVlanPortList\r\n");
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        FsUtilReleaseBitList ((UINT1 *) pPorts2);
        FsUtilReleaseBitList ((UINT1 *) pPorts3);
        return CLI_FAILURE;
    }
    MEMSET (pau1LocalVlanPortList, 0, VLAN_PORT_LIST_SIZE);

    pau1LocalFwdUnregPorts =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pau1LocalFwdUnregPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanDisplayExtFilteringInfo: "
                  "Error in allocating memory for pau1LocalFwdUnregPorts\r\n");
        UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
        UtilPlstReleaseLocalPortList (pau1LocalFwdUnregPorts);
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        FsUtilReleaseBitList ((UINT1 *) pPorts2);
        FsUtilReleaseBitList ((UINT1 *) pPorts3);
        return CLI_FAILURE;
    }
    MEMSET (pau1LocalFwdUnregPorts, 0, VLAN_PORT_LIST_SIZE);

    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        VLAN_SCAN_PORT_TABLE (u2LocalPort)
        {
            OSIX_BITLIST_SET_BIT (pau1LocalVlanPortList, u2LocalPort,
                                  VLAN_PORT_LIST_SIZE);
        }

        if (VlanConvertToLocalPortList (*pPorts, pau1LocalFwdUnregPorts) !=
            VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
            UtilPlstReleaseLocalPortList (pau1LocalFwdUnregPorts);
            FsUtilReleaseBitList ((UINT1 *) pPorts);
            FsUtilReleaseBitList ((UINT1 *) pPorts2);
            FsUtilReleaseBitList ((UINT1 *) pPorts3);
            return CLI_FAILURE;
        }
        VlanReleaseContext ();
    }

    if ((*pu1isActiveVlan == FALSE) &&
        ((MEMCMP
          (pau1LocalFwdUnregPorts, pau1LocalVlanPortList,
           VLAN_PORT_LIST_SIZE) != 0) ||
         (FsUtilBitListIsAllZeros (Ports2.pu1_OctetList, Ports2.i4_Length) ==
          OSIX_FALSE)))
    {

        CliPrintf (CliHandle, "Vlan %d\r\n", u4VlanIndex);
        *pu1isActiveVlan = TRUE;
    }
    UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
    UtilPlstReleaseLocalPortList (pau1LocalFwdUnregPorts);
    if (*pu1isActiveVlan == TRUE)
    {
        i4RetVal =
            CliConfOctetToIfName (CliHandle,
                                  " forward-unregistered static-ports ",
                                  (CHR1 *) au1DefaultConf, &Ports,
                                  pu4PagingStatus);
    }
    if (FsUtilBitListIsAllZeros (Ports.pu1_OctetList,
                                 Ports.i4_Length) == OSIX_TRUE)
    {
        if (FsUtilBitListIsAllZeros (Ports2.pu1_OctetList,
                                     Ports2.i4_Length) == OSIX_FALSE)
        {
            if ((u4VlanIndex == VLAN_DEF_VLAN_ID)
                && (*pu1isActiveVlan == FALSE))
            {
                CliPrintf (CliHandle, "vlan %d\r\n", u4VlanIndex);
                *pu1isActiveVlan = TRUE;
            }
            CliPrintf (CliHandle, " forward-unregistered");
        }
    }

    i4RetVal2 = CliConfOctetToIfName (CliHandle, " forbidden-ports ", NULL,
                                      &Ports2, pu4PagingStatus);

    if ((i4RetVal == CLI_SUCCESS) || (i4RetVal2 == CLI_SUCCESS))
    {
        CliPrintf (CliHandle, "\r\n");
    }
#endif
    FsUtilReleaseBitList ((UINT1 *) pPorts);
    FsUtilReleaseBitList ((UINT1 *) pPorts2);
    FsUtilReleaseBitList ((UINT1 *) pPorts3);

    if ((u1VlanModeCheck == FALSE) && (*pu1isActiveVlan == TRUE))
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DisplayDot1qVlanStaticTableInfo                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays current vlan configuration  */
/*                        for VlanStaticTable                                */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanIndex - Specified Vlan id                    */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                        pu1isActiveVlan - Flag to verify 'vlan' display    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayDot1qVlanStaticTableInfo (tCliHandle CliHandle,
                                 UINT4 u4ContextId,
                                 UINT4 u4VlanIndex,
                                 UINT4 *pu4PagingStatus,
                                 UINT1 *pu1isActiveVlan, UINT4 *pu4Module,
                                 UINT1 *pu1HeadFlag)
{
    UINT1              *pau1LocalVlanPortList = NULL;
    UINT1              *pau1LocalEgressPorts = NULL;
    UINT1              *pau1LocalUntaggedPorts = NULL;
    tPortList          *pPorts = NULL;
    tPortList          *pUntaggedPorts = NULL;
    tPortList          *pForbidPorts = NULL;
    UINT1               u1PbPortType;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE Ports;
    tSNMP_OCTET_STRING_TYPE UntaggedPorts;
    tSNMP_OCTET_STRING_TYPE ForbidPorts;
    tSNMP_OCTET_STRING_TYPE Name;
    UINT4               u4IfIndex;
    UINT4               u4PrevIndex;
    UINT4               u4Ports = 0;
    UINT4               u4NonMemberPorts = 0;
    INT4                i4PortType;
    BOOL1               bDefault = VLAN_FALSE;
    UINT2               u2LocalPort;
    INT4                i4Type;
    INT4                i4EgressEthertype = 0;
    UINT4               u4IcchIndex = 0;
    pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    pUntaggedPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUntaggedPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        return CLI_FAILURE;
    }

    pForbidPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbidPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
        return CLI_FAILURE;
    }

    MEMSET (*pPorts, 0, sizeof (tPortList));
    Ports.pu1_OctetList = *pPorts;
    Ports.i4_Length = sizeof (tPortList);

    MEMSET (*pUntaggedPorts, 0, sizeof (tPortList));
    UntaggedPorts.pu1_OctetList = *pUntaggedPorts;
    UntaggedPorts.i4_Length = sizeof (tPortList);

    MEMSET (*pForbidPorts, 0, sizeof (tPortList));
    ForbidPorts.pu1_OctetList = *pForbidPorts;
    ForbidPorts.i4_Length = sizeof (tPortList);

    nmhGetFsMIDot1qFutureStVlanType ((INT4) u4ContextId, u4VlanIndex, &i4Type);

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {

        do
        {
            nmhGetFsDot1qVlanStaticPort (u4ContextId, u4VlanIndex,
                                         u4IfIndex, &i4PortType);
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if ((u4IfIndex >= CFA_MIN_SISP_IF_INDEX) &&
                    (u4IfIndex <= CFA_MAX_SISP_IF_INDEX))
                {
                    if (i4PortType == VLAN_ADD_UNTAGGED_PORT)
                    {
                        if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
                        {

                            VlanL2IwfGetPbPortType (u4IfIndex, &u1PbPortType);
                            CliSetMemberPort (UntaggedPorts.pu1_OctetList,
                                              UntaggedPorts.i4_Length,
                                              u4IfIndex);
                            CliSetMemberPort (Ports.pu1_OctetList,
                                              Ports.i4_Length, u4IfIndex);
                        }
                        else
                        {
                            CliSetMemberPort (UntaggedPorts.pu1_OctetList,
                                              UntaggedPorts.i4_Length,
                                              u4IfIndex);
                            CliSetMemberPort (Ports.pu1_OctetList,
                                              Ports.i4_Length, u4IfIndex);
                        }
                    }
                    else if (i4PortType == VLAN_ADD_TAGGED_PORT)
                    {
                        CliSetMemberPort (Ports.pu1_OctetList, Ports.i4_Length,
                                          u4IfIndex);
                    }
                    else if (i4PortType == VLAN_ADD_ST_FORBIDDEN_PORT)
                    {
                        CliSetMemberPort (ForbidPorts.pu1_OctetList,
                                          ForbidPorts.i4_Length, u4IfIndex);
                    }
                }
                else
                {
                    if (i4PortType == VLAN_ADD_UNTAGGED_PORT)
                    {
                        if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
                        {

                            VlanL2IwfGetPbPortType (u4IfIndex, &u1PbPortType);

                            CliSetMemberPort (UntaggedPorts.pu1_OctetList,
                                              UntaggedPorts.i4_Length,
                                              u4IfIndex);
                            CliSetMemberPort (Ports.pu1_OctetList,
                                              Ports.i4_Length, u4IfIndex);
                        }
                        else
                        {
                            CliSetMemberPort (UntaggedPorts.pu1_OctetList,
                                              UntaggedPorts.i4_Length,
                                              u4IfIndex);
                            CliSetMemberPort (Ports.pu1_OctetList,
                                              Ports.i4_Length, u4IfIndex);
                        }
                    }
                    else if (i4PortType == VLAN_ADD_TAGGED_PORT)
                    {

                        CliSetMemberPort (Ports.pu1_OctetList,
                                          Ports.i4_Length, u4IfIndex);
                    }
                    else if (i4PortType == VLAN_ADD_ST_FORBIDDEN_PORT)
                    {
                        CliSetMemberPort (ForbidPorts.pu1_OctetList,
                                          ForbidPorts.i4_Length, u4IfIndex);
                    }
                    else
                    {
                        u4NonMemberPorts++;
                    }
                }

                VlanReleaseContext ();
            }
            u4PrevIndex = u4IfIndex;
            u4Ports++;

        }
        while (VlanGetNextPortInContext (u4ContextId, u4PrevIndex,
                                         &u4IfIndex) == VLAN_SUCCESS);

    }

    MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
    Name.pu1_OctetList = au1VlanName;
    nmhGetFsDot1qVlanStaticName (u4ContextId, u4VlanIndex, &Name);
    if (u4VlanIndex == VLAN_DEF_VLAN_ID)
    {
        pau1LocalUntaggedPorts =
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pau1LocalUntaggedPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "DisplayDot1qVlanStaticTableInfo: Error in allocating memory "
                      "for pau1LocalUntaggedPorts\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalUntaggedPorts, 0, VLAN_PORT_LIST_SIZE);

        pau1LocalVlanPortList =
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pau1LocalVlanPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "DisplayDot1qVlanStaticTableInfo: Error in allocating memory "
                      "for pau1LocalVlanPortList\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
            UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalVlanPortList, 0, VLAN_PORT_LIST_SIZE);

        pau1LocalEgressPorts =
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pau1LocalEgressPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "DisplayDot1qVlanStaticTableInfo: Error in allocating memory "
                      "for pau1LocalEgressPorts\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
            UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
            UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalEgressPorts, 0, VLAN_PORT_LIST_SIZE);

        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            VLAN_SCAN_PORT_TABLE (u2LocalPort)
            {
                OSIX_BITLIST_SET_BIT (pau1LocalVlanPortList, u2LocalPort,
                                      VLAN_PORT_LIST_SIZE);
            }

            if (VlanConvertToLocalPortList (*pUntaggedPorts,
                                            pau1LocalUntaggedPorts) !=
                VLAN_SUCCESS)
            {
                FsUtilReleaseBitList ((UINT1 *) pPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
                UtilPlstReleaseLocalPortList (pau1LocalEgressPorts);
                VlanReleaseContext ();
                return CLI_FAILURE;
            }
            if (VlanConvertToLocalPortList (*pPorts, pau1LocalEgressPorts) !=
                VLAN_SUCCESS)
            {
                FsUtilReleaseBitList ((UINT1 *) pPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
                UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
                UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
                UtilPlstReleaseLocalPortList (pau1LocalEgressPorts);
                VlanReleaseContext ();
                return CLI_FAILURE;
            }
            VlanReleaseContext ();
        }

        /*For default vlan avoid displaying vlan for default conf */
        if ((MEMCMP (pau1LocalEgressPorts, pau1LocalVlanPortList,
                     VLAN_PORT_LIST_SIZE) != 0) ||
            (MEMCMP (pau1LocalUntaggedPorts,
                     pau1LocalVlanPortList, VLAN_PORT_LIST_SIZE) != 0) ||
            (Name.pu1_OctetList[0] != '\0'))
        {
            VlanCliPrintCtxt (CliHandle, u4ContextId, pu1HeadFlag);
            CliPrintf (CliHandle, "vlan %d\r\n", u4VlanIndex);
            *pu1isActiveVlan = TRUE;
        }
        else
        {
            bDefault = VLAN_TRUE;
        }
        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
        UtilPlstReleaseLocalPortList (pau1LocalVlanPortList);
        UtilPlstReleaseLocalPortList (pau1LocalEgressPorts);
    }
    else
    {
        VlanCliPrintCtxt (CliHandle, u4ContextId, pu1HeadFlag);
        CliPrintf (CliHandle, "vlan %d\r\n", u4VlanIndex);
        *pu1isActiveVlan = VLAN_TRUE;
    }

    if (*pu4Module == ISS_MPLS_SHOW_RUNNING_CONFIG)
    {
        FsUtilReleaseBitList ((UINT1 *) pPorts);
        FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
        FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
        return CLI_SUCCESS;
    }

    /*if member port is default but untagged ports changed */

    if (u4VlanIndex != VLAN_DEF_VLAN_ID)
    {
        CliPrintf (CliHandle, "vlan active\r\n");
    }

    VlanIcchGetIcclIfIndex (&u4IcchIndex);
    /* Reset the ICCH interface from the vlan member ports */
    VLAN_RESET_MEMBER_PORT_LIST ((*pPorts), u4IcchIndex);

    if (bDefault == VLAN_FALSE)
    {
        CliConfOctetToIfName (CliHandle, " ports ", NULL,
                              &Ports, pu4PagingStatus);

        if (FsUtilBitListIsAllZeros (UntaggedPorts.pu1_OctetList,
                                     UntaggedPorts.i4_Length) == OSIX_FALSE)
        {
            CliConfOctetToIfName (CliHandle, " untagged ",
                                  NULL, &UntaggedPorts, pu4PagingStatus);
        }

        if (FsUtilBitListIsAllZeros (ForbidPorts.pu1_OctetList,
                                     ForbidPorts.i4_Length) == OSIX_FALSE)
        {
            CliConfOctetToIfName (CliHandle, " forbidden ",
                                  NULL, &ForbidPorts, pu4PagingStatus);
        }
#ifndef ICCH_WANTED
        else if ((FsUtilBitListIsAllZeros
                  (Ports.pu1_OctetList, (UINT4) Ports.i4_Length) != OSIX_FALSE)
                 &&
                 (FsUtilBitListIsAllZeros
                  (UntaggedPorts.pu1_OctetList,
                   (UINT4) UntaggedPorts.i4_Length) != OSIX_FALSE))
        {
            CliPrintf (CliHandle, " no ports ");
        }
#endif
    }
    if ((Name.pu1_OctetList[0] != '\0') &&
        (FsUtilBitListIsAllZeros (Ports.pu1_OctetList, (UINT4) Ports.i4_Length)
         == OSIX_FALSE))
    {
        VlanCliPrintDefVlan (CliHandle, u4ContextId, u4VlanIndex,
                             pu1HeadFlag, pu1isActiveVlan);
        *pu4PagingStatus =
            CliPrintf (CliHandle, "\r\n name %s\r\n", Name.pu1_OctetList);
    }
    else
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    }
    nmhGetFsMIDot1qFutureStVlanEgressEthertype ((INT4) u4ContextId, u4VlanIndex,
                                                &i4EgressEthertype);
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {

    }
    if ((u4VlanIndex == VLAN_DEF_VLAN_ID) && (*pu1isActiveVlan == VLAN_FALSE))
    {

        if (((i4EgressEthertype == VLAN_PROTOCOL_ID)
             && (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE))
            || ((i4EgressEthertype == VLAN_PROVIDER_PROTOCOL_ID)
                && (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE))
            || (i4EgressEthertype == VLAN_QINQ_PROTOCOL_ID)
            || ((i4EgressEthertype == VLAN_PORT_USER_DEFINED_TPID)))
        {
            VlanCliPrintDefVlan (CliHandle, u4ContextId, u4VlanIndex,
                                 pu1HeadFlag, pu1isActiveVlan);
        }
    }
    if (i4EgressEthertype == VLAN_PROTOCOL_ID)
    {
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {
            CliPrintf (CliHandle, "vlan egress ether-type CTAG \r\n");
        }
    }
    else if (i4EgressEthertype == VLAN_PROVIDER_PROTOCOL_ID)
    {
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_FALSE)
        {
            CliPrintf (CliHandle, "vlan egress ether-type STAG \r\n");
        }
    }
    else if (i4EgressEthertype == VLAN_QINQ_PROTOCOL_ID)
    {
        CliPrintf (CliHandle, "vlan egress ether-type QINQ \r\n");
    }
    else if (i4EgressEthertype == VLAN_PORT_USER_DEFINED_TPID)
    {
        CliPrintf (CliHandle, "vlan egress ether-type user-defined \r\n");
    }

    FsUtilReleaseBitList ((UINT1 *) pPorts);
    FsUtilReleaseBitList ((UINT1 *) pUntaggedPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbidPorts);
    VlanReleaseContext ();
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : DisplayDot1qMacLearningParams                     */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Unicast Mac Learning   */
/*                        Status and Learning Limit for the vlan.           */
/*                                                                          */
/*     INPUT            : VlanId  - Vlan Id                                 */
/*                        u4ContextId - Virtual context Id                  */
/*                        pu4PagingStatus - return CLI_FAILURE, if          */
/*                        'q' is pressed, while printing output             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
DisplayDot1qMacLearningParams (tCliHandle CliHandle,
                               UINT4 u4ContextId,
                               tVlanId VlanId, UINT4 *pu4PagingStatus)
{
    INT4                i4AdminStatus = VLAN_INIT_VAL;
    INT4                i4LearningMode = VLAN_INIT_VAL;
    UINT4               u4LearningLimit = VLAN_INIT_VAL;

    nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus (u4ContextId,
                                                     VlanId, &i4AdminStatus);

    if ((i4AdminStatus == VLAN_DISABLED) ||
        (i4AdminStatus == VLAN_LEARNING_LIMIT_ZERO_TRIGGER))
    {
        CliPrintf (CliHandle, "\r\nset unicast-mac learning ");
        CliPrintf (CliHandle, "disable\n");
    }
    else if (i4AdminStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nset unicast-mac learning ");
        CliPrintf (CliHandle, "enable\n");
    }

    nmhGetFsMIDot1qFutureVlanLearningMode (u4ContextId, &i4LearningMode);
    nmhGetFsMIDot1qFutureVlanUnicastMacLimit (u4ContextId, VlanId,
                                              &u4LearningLimit);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (i4LearningMode == VLAN_SHARED_LEARNING)
    {
        if (u4LearningLimit != VLAN_DYNAMIC_UNICAST_SIZE)
        {

            CliPrintf (CliHandle, "vlan unicast-mac learning limit %d\r\n",
                       u4LearningLimit);
        }
    }
    else if (u4LearningLimit != VLAN_DEF_DYN_UCAST_ENTRIES_PER_VLAN)
    {
        CliPrintf (CliHandle, "vlan unicast-mac learning limit %d\r\n",
                   u4LearningLimit);
    }

    VlanReleaseContext ();

    if (*pu4PagingStatus != CLI_SUCCESS)
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "! \r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanShowRunningConfigVlanInfo                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current VLAN            */
/*                        configuration for tables                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                        u4VlanId - Specified Vlan id                       */
/*                                   for configuration                       */
/*                        u4Module   - Module for which the configuration to */
/*                                     be displayed                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanShowRunningConfigVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4VlanId, UINT4 u4Module,
                               UINT1 *pu1HeadFlag)
{
    INT4                i4RetVal = 0;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1isActiveVlan = FALSE;
    UINT4               u4NextVal1 = 0;
    UINT4               u4CurrentVal1 = 0;
    UINT4               u4NextVal2 = 0;
    UINT4               u4CurrentVal2 = 0;
    INT4                i4CurrentContextId;
    INT4                i4NextContextId;
    UINT4               u4PagingStatus = CLI_SUCCESS;
#ifdef MPLS_WANTED
    UINT4               u4VplsIndex = 0;
    UINT1               u1Display = OSIX_FALSE;
#endif
    UINT1               u1ShowConfig = OSIX_TRUE;

    i4CurrentContextId = (INT4) u4ContextId;

    /* If it is a ICCH default vlan skip configurations from SRC */
    if (VlanIcchIsIcclVlan ((UINT2) u4VlanId) == OSIX_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    if (VlanGetNextIndexAscendingFsDot1qVlanCurrentTable
        (i4CurrentContextId, &i4NextContextId, u4CurrentVal2,
         &u4NextVal2, u4CurrentVal1, &u4NextVal1) == VLAN_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if (i4CurrentContextId != i4NextContextId)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG)
        {
            u1ShowConfig = OSIX_FALSE;
        }

        if ((u4NextVal1 < VLAN_MIN_VLAN_ID) || (u4NextVal1 > VLAN_MAX_VLAN_ID))
        {
            /* VLAN ID is not valid */
            return CLI_FAILURE;
        }

#ifdef MPLS_WANTED
        u4VplsIndex = VlanL2VpnGetVplsIndex (u4ContextId, u4NextVal1);
        VLAN_UNLOCK ();
        u1Display = OSIX_FALSE;
        if (u4VplsIndex != 0)
        {
            MplsVpwsShowRunningConfig (CliHandle, 0, u4NextVal1,
                                       MPLS_PWVC_TYPE_ETH_VLAN, u4VplsIndex,
                                       u4ContextId, &u1Display, &u1ShowConfig);
            MplsPwXConnectShowRunningConfig (CliHandle, 0, u4NextVal1,
                                             MPLS_PWVC_TYPE_ETH_VLAN,
                                             &u1Display, &u1ShowConfig);
        }
        VLAN_LOCK ();
#endif

        if ((u1ShowConfig == OSIX_TRUE) &&
            (((u4VlanId != 0) && (u4NextVal1 == u4VlanId)) || (u4VlanId == 0))
            && (VlanIcchIsIcclVlan ((UINT2) u4NextVal1) != OSIX_SUCCESS))
        {
            u1isActiveVlan = FALSE;

            nmhGetFsDot1qVlanStatus ((UINT4) i4NextContextId, u4NextVal2,
                                     u4NextVal1, &i4RetVal);

            if (i4RetVal == VLAN_CURR_ENTRY_PERMANENT)
            {
                nmhGetFsDot1qVlanStaticRowStatus ((UINT4) i4NextContextId,
                                                  u4NextVal1, &i4RetVal);

                if (i4RetVal == VLAN_ACTIVE)
                {
                    DisplayDot1qVlanStaticTableInfo (CliHandle, u4ContextId,
                                                     u4NextVal1,
                                                     &u4PagingStatus,
                                                     &u1isActiveVlan,
                                                     &u4Module, pu1HeadFlag);
                    DisplayDot1qMacLearningParams (CliHandle, u4ContextId,
                                                   (tVlanId) u4NextVal1,
                                                   &u4PagingStatus);
                }
            }

            DisplayVlanCounterStatus (CliHandle, u4ContextId, u4NextVal1,
                                      &u1isActiveVlan);

            DisplayVlanLoopbackStatus (CliHandle, u4ContextId, u4NextVal1);

#ifndef NPAPI_WANTED
            VlanDisplayExtFilteringInfo (CliHandle, u4ContextId, u4NextVal1,
                                         &u4PagingStatus, &u1isActiveVlan);
#endif

            DisplayPbServiceVlanInfo (CliHandle, u4ContextId, u4NextVal1,
                                      &u1isActiveVlan);

            VlanPbShowRunningConfigPerVlanTable (CliHandle, u4ContextId,
                                                 u4NextVal1, pu1HeadFlag);

#ifdef MPLS_WANTED
            VLAN_UNLOCK ();
            u1Display = OSIX_TRUE;
            if (u4VplsIndex != 0)
            {
                MplsVpwsShowRunningConfig (CliHandle, 0, u4NextVal1,
                                           MPLS_PWVC_TYPE_ETH_VLAN, u4VplsIndex,
                                           u4ContextId, &u1Display,
                                           &u1ShowConfig);
                MplsPwXConnectShowRunningConfig (CliHandle, 0, u4NextVal1,
                                                 MPLS_PWVC_TYPE_ETH_VLAN,
                                                 &u1Display, &u1ShowConfig);
            }
            VLAN_LOCK ();
#endif

            if (u1isActiveVlan == TRUE)
            {
                CliPrintf (CliHandle, "!\r\n");
            }

            u1isActiveVlan = FALSE;

            if (u4VlanId != 0)
            {
                break;
            }
        }
        i4CurrentContextId = i4NextContextId;
        u4CurrentVal2 = u4NextVal2;
        u4CurrentVal1 = u4NextVal1;
        if (VlanGetNextIndexAscendingFsDot1qVlanCurrentTable
            (i4CurrentContextId, &i4NextContextId, u4CurrentVal2,
             &u4NextVal2, u4CurrentVal1, &u4NextVal1) == VLAN_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanShowRunningConfigTables                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current VLAN            */
/*                        configuration for tables                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT1 *u1HeadFlag)
{
    INT4                i4RetVal = 0;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1CurrOctetList[VLAN_5BYTE_PROTO_TEMP_SIZE];
    UINT1               au1NextOctetList[VLAN_5BYTE_PROTO_TEMP_SIZE];
    UINT4               u4NextVal1 = 0;
    UINT4               u4CurrentVal1 = 0;
    UINT4               u4NextVal2 = 0;
    UINT4               u4CurrentVal2 = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tSNMP_OCTET_STRING_TYPE CurrSnmpOctetStr;
    tSNMP_OCTET_STRING_TYPE NextSnmpOctetStr;
#ifdef BCMX_WANTED
    UINT4               u4CurrIfIndex = VLAN_INIT_VAL;
    INT4                i4NextIfIndex = VLAN_INIT_VAL;
    UINT4               u4CurrSrcIPAddr = VLAN_INIT_VAL;
    UINT4               u4NextSrcIPAddr = VLAN_INIT_VAL;
    UINT4               u4CurrSubnetMask = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMask = VLAN_INIT_VAL;
#endif

    CLI_MEMSET (au1CurrOctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);
    CLI_MEMSET (au1NextOctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);

    NextSnmpOctetStr.pu1_OctetList = au1NextOctetList;
    CurrSnmpOctetStr.pu1_OctetList = au1CurrOctetList;

    CurrSnmpOctetStr.i4_Length = VLAN_5BYTE_PROTO_TEMP_SIZE;

    CLI_MEMSET (CurrSnmpOctetStr.pu1_OctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);

    CLI_MEMSET (NextSnmpOctetStr.pu1_OctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);

    if (nmhGetNextIndexFsDot1vProtocolGroupTable ((INT4) u4ContextId,
                                                  &i4NextContextId,
                                                  0, (INT4 *) &u4NextVal1,
                                                  &CurrSnmpOctetStr,
                                                  &NextSnmpOctetStr) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (u4ContextId != (UINT4) i4NextContextId)
            {
                break;
            }

            nmhGetFsDot1vProtocolGroupRowStatus (u4ContextId, (INT4) u4NextVal1,
                                                 &NextSnmpOctetStr, &i4RetVal);
            if (i4RetVal == VLAN_ACTIVE)
            {
                VlanCliPrintCtxt (CliHandle, u4ContextId, u1HeadFlag);
                DisplayDot1vProtocolGroupInfo (CliHandle, u4ContextId,
                                               u4NextVal1,
                                               NextSnmpOctetStr,
                                               &u4PagingStatus);
            }
            u4CurrentVal1 = u4NextVal1;

            CurrSnmpOctetStr.i4_Length = NextSnmpOctetStr.i4_Length;

            CLI_MEMSET (CurrSnmpOctetStr.pu1_OctetList, 0,
                        VLAN_5BYTE_PROTO_TEMP_SIZE);

            CLI_MEMCPY (CurrSnmpOctetStr.pu1_OctetList,
                        NextSnmpOctetStr.pu1_OctetList,
                        NextSnmpOctetStr.i4_Length);

            if (nmhGetNextIndexFsDot1vProtocolGroupTable
                ((INT4) u4ContextId, &i4NextContextId,
                 (INT4) u4CurrentVal1, (INT4 *) &u4NextVal1,
                 &CurrSnmpOctetStr, &NextSnmpOctetStr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

    }

    u1isShowAll = TRUE;

#ifdef BCMX_WANTED
    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
            ((INT4) u4CurrIfIndex, &i4NextIfIndex, u4CurrSrcIPAddr,
             &u4NextSrcIPAddr, u4CurrSubnetMask, &u4NextSubnetMask)
            == SNMP_SUCCESS)
        {
            do
            {
                if (nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
                    ((UINT4) i4NextIfIndex, u4NextSrcIPAddr,
                     u4NextSubnetMask, &i4RetVal) == SNMP_FAILURE)
                {
                    break;
                }

                if (i4RetVal == VLAN_ACTIVE)
                {
                    VlanCliPrintCtxt (CliHandle, u4ContextId, u1HeadFlag);
                    DisplayDot1qVlanSubnetMapTableInfo (CliHandle,
                                                        u4NextSrcIPAddr,
                                                        (UINT4) i4NextIfIndex,
                                                        u4NextSubnetMask,
                                                        &u4PagingStatus);
                }
                else
                {
                    u1isShowAll = FALSE;
                }

                u4CurrSrcIPAddr = u4NextSrcIPAddr;
                u4CurrSubnetMask = u4NextSubnetMask;
                u4CurrIfIndex = (UINT4) i4NextIfIndex;

                if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
                    (u4CurrIfIndex, &i4NextIfIndex, u4CurrSrcIPAddr,
                     &u4NextSrcIPAddr, u4CurrSubnetMask, &u4NextSubnetMask)
                    == SNMP_FAILURE)
                {
                    u1isShowAll = FALSE;
                }

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User pressed 'q' at more prompt,
                     * no more print required, exit
                     */
                    u1isShowAll = FALSE;
                }
            }
            while (u1isShowAll);
        }
        VlanReleaseContext ();
    }
#endif

    u1isShowAll = TRUE;

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetNextIndexFsDot1qStaticUnicastTable
        ((INT4) u4ContextId, &i4NextContextId, 0, &u4NextVal1,
         CurrMacAddr, &NextMacAddr, 0, (INT4 *) &u4NextVal2) == SNMP_SUCCESS)
    {
        do
        {
            if (u4ContextId != (UINT4) i4NextContextId)
            {
                break;
            }
            VlanCliPrintCtxt (CliHandle, u4ContextId, u1HeadFlag);
            DisplayDot1qStaticUnicastTableInfo (CliHandle, u4ContextId,
                                                u4NextVal1, NextMacAddr,
                                                u4NextVal2, &u4PagingStatus);

            u4CurrentVal1 = u4NextVal1;
            MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
            u4CurrentVal2 = u4NextVal2;

            if (nmhGetNextIndexFsDot1qStaticUnicastTable
                ((INT4) u4ContextId, &i4NextContextId, u4CurrentVal1,
                 &u4NextVal1, CurrMacAddr, &NextMacAddr, (INT4) u4CurrentVal2,
                 (INT4 *) &u4NextVal2) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);

    }

    u1isShowAll = TRUE;
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetNextIndexFsDot1qStaticMulticastTable ((INT4) u4ContextId,
                                                    &i4NextContextId, 0,
                                                    &u4NextVal1, CurrMacAddr,
                                                    &NextMacAddr, 0,
                                                    (INT4 *) &u4NextVal2) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (u4ContextId != (UINT4) i4NextContextId)
            {
                break;
            }

            DisplayDot1qStaticMulticastTableInfo (CliHandle, u4ContextId,
                                                  u4NextVal1, NextMacAddr,
                                                  u4NextVal2, &u4PagingStatus);
            u4CurrentVal1 = u4NextVal1;
            MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
            u4CurrentVal2 = u4NextVal2;

            if (nmhGetNextIndexFsDot1qStaticMulticastTable ((INT4) u4ContextId,
                                                            &i4NextContextId,
                                                            u4CurrentVal1,
                                                            &u4NextVal1,
                                                            CurrMacAddr,
                                                            &NextMacAddr,
                                                            (INT4)
                                                            u4CurrentVal2,
                                                            (INT4 *)
                                                            &u4NextVal2) ==
                SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);

    }

    u1isShowAll = TRUE;
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
    {
        if (nmhGetNextIndexDot1dStaticTable (CurrMacAddr, &NextMacAddr,
                                             0, (INT4 *) &u4NextVal2)
            == SNMP_SUCCESS)
        {
            do
            {
                DisplayDot1dStaticTableInfo (CliHandle, NextMacAddr,
                                             (INT4) u4NextVal2,
                                             &u4PagingStatus);

                MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));
                u4CurrentVal2 = u4NextVal2;

                if (nmhGetNextIndexDot1dStaticTable (CurrMacAddr,
                                                     &NextMacAddr,
                                                     (INT4) u4CurrentVal2,
                                                     (INT4 *) &u4NextVal2)
                    == SNMP_FAILURE)
                {
                    u1isShowAll = FALSE;
                }

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* 
                     * User pressed 'q' at more prompt, no more print required, exit
                     * Setting the Count to -1 will result in not displaying the
                     * count of the entries.
                     */
                    u1isShowAll = FALSE;
                }
            }
            while (u1isShowAll == TRUE);
        }
        VlanReleaseContext ();
    }

    u1isShowAll = TRUE;
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable ((INT4) u4ContextId,
                                                         &i4NextContextId,
                                                         CurrMacAddr,
                                                         &NextMacAddr)
        == SNMP_SUCCESS)
    {

        do
        {
            if (u4ContextId != (UINT4) i4NextContextId)
            {
                break;
            }

            DisplayWildCardTableInfo (CliHandle, u4ContextId,
                                      NextMacAddr, &u4PagingStatus);

            MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));

            if (nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable
                ((INT4) u4ContextId, &i4NextContextId,
                 CurrMacAddr, &NextMacAddr) != SNMP_SUCCESS)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DisplayWildCardTableInfo                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays wild card  configuration    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        NextMacAddr - Specified Mac-address                */
/*                        pu4PagingStatus - return CLI_FAILURE, if           */
/*                        'q' is pressed, while printing output              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayWildCardTableInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                          tMacAddr NextMacAddr, UINT4 *pu4PagingStatus)
{
    INT4                i4PortType = 0;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    tPortList          *pPortList1 = NULL;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UINT4               u4IfIndex;
    UINT4               u4PrevIndex;
    INT4                i4RowStatus = VLAN_NOT_READY;

    if (nmhGetFsMIDot1qFutureVlanWildCardRowStatus
        (u4ContextId, NextMacAddr, &i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4RowStatus != VLAN_ACTIVE)
    {
        return CLI_SUCCESS;
    }

    pPortList1 = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList1 == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pPortList1, 0, sizeof (tPortList));
    SnmpPorts.pu1_OctetList = *pPortList1;
    SnmpPorts.i4_Length = sizeof (tPortList);

    if (VlanGetFirstPortInContext (u4ContextId, &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            nmhGetFsMIDot1qFutureVlanIsWildCardEgressPort
                (u4ContextId, NextMacAddr, u4IfIndex, &i4PortType);
            if (i4PortType == VLAN_SNMP_TRUE)
            {
                VLAN_SET_MEMBER_PORT ((*pPortList1), u4IfIndex);
            }

            u4PrevIndex = u4IfIndex;
        }
        while (VlanGetNextPortInContext
               (u4ContextId, u4PrevIndex, &u4IfIndex) == VLAN_SUCCESS);
    }

    CliPrintf (CliHandle, "wildcard mac-address");

    PrintMacAddress (NextMacAddr, au1String);
    CliPrintf (CliHandle, " %s", au1String);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pPortList1);
        return CLI_FAILURE;
    }

    VlanReleaseContext ();

    CliConfOctetToIfName (CliHandle,
                          " interface ", NULL, &SnmpPorts, pu4PagingStatus);

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pPortList1);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : DisplayVlanCounterStatus                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Display the statistics         */
/*                        collection status of the vlan                     */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the cli context             */
/*                        VlanId  - Vlan Id                                 */
/*                        u4ContextId - Virtual context Id                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
DisplayVlanCounterStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4VlanId, UINT1 *pu1isActiveVlan)
{
    INT4                i4CounterStatus = VLAN_DISABLED;

    nmhGetFsMIDot1qFutureVlanCounterStatus ((INT4) u4ContextId,
                                            u4VlanId, &i4CounterStatus);

    if (i4CounterStatus == VLAN_ENABLED)
    {
        if (*pu1isActiveVlan == FALSE)
        {
            *pu1isActiveVlan = TRUE;
        }
        CliPrintf (CliHandle, " set vlan counter enable\r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : DisplayVlanLoopbackStatus                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Display the statistics         */
/*                        collection status of the vlan                     */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the cli context             */
/*                        VlanId  - Vlan Id                                 */
/*                        u4ContextId - Virtual context Id                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : None                                              */
/*                                                                          */
/****************************************************************************/
VOID
DisplayVlanLoopbackStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4VlanId)
{
    INT4                i4LoopbackStatus = VLAN_DISABLED;

    nmhGetFsMIDot1qFutureVlanLoopbackStatus ((INT4) u4ContextId,
                                             u4VlanId, &i4LoopbackStatus);

    if (i4LoopbackStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "vlan %d\r\n", u4VlanId);
        CliPrintf (CliHandle, " vlan loopback enable\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetNextTrafficClassTableIndex                */
/*                                                                           */
/*    Description         : This function is used to get the index of the    */
/*                          Dot1dTrafficClassTable.                          */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                          i4Priority - Current Priority.                   */
/*                                                                           */
/*    Output(s)           : pi4NextIfIndex - Next IfIndex.                   */
/*                          pi4NextPriority - Next Priority.                 */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->VlanPortList            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetNextTrafficClassTableIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                   INT4 i4Priority, INT4 *pi4NextPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPortEntry      Dummy;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId = 0;

    if ((i4IfIndex < 0) || (i4Priority < 0))
    {
        return VLAN_FAILURE;
    }

    /* Get Global ifindex based RBTree and get the context ID and loal port
       from the port entry else return failure */

    if (i4IfIndex == 0)
    {
        pPortEntry =
            (tVlanPortEntry *) RBTreeGetFirst (VLAN_GLOBAL_PORT_RBTREE ());

        if (pPortEntry == NULL)
        {
            return VLAN_FAILURE;
        }
        else
        {
            u4TempContextId = pPortEntry->u4ContextId;
            u2LocalPortId = pPortEntry->u2Port;
        }
    }
    else if (VlanGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4TempContextId,
                                            &u2LocalPortId) != VLAN_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    pPortEntry = NULL;
    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pPortEntry != NULL)
    {
        if (i4Priority < VLAN_MAX_PRIORITY - 1)
        {
            *pi4NextIfIndex = i4IfIndex;

            *pi4NextPriority = i4Priority + 1;
            return VLAN_SUCCESS;
        }
    }

    Dummy.u4IfIndex = (UINT4) i4IfIndex;

    pPortEntry = RBTreeGetNext (VLAN_LOCAL_PORT_RBTREE (), &Dummy, NULL);

    if (pPortEntry != NULL)
    {
        *pi4NextIfIndex = pPortEntry->u4IfIndex;
        *pi4NextPriority = 0;

        return VLAN_SUCCESS;
    }

    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetNextPortStatisticsTableIndex              */
/*                                                                           */
/*    Description         : This function is used to get the index of the    */
/*                          Dot1dPortStatisticsTable.                        */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                          u4VlanId - Current VLAN Id.                      */
/*                                                                           */
/*    Output(s)           : pi4NextIfIndex - Next IfIndex.                   */
/*                          pu4NextVlanId - Next VLAN Id.                    */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->VlanPortList            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
VlanGetNextPortStatisticsTableIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4VlanId, UINT4 *pu4NextVlanId)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPortEntry      Dummy;

    if (i4IfIndex < 0 || i4IfIndex > VLAN_MAX_PORTS)
    {
        return VLAN_FAILURE;
    }

    Dummy.u4IfIndex = (UINT4) i4IfIndex;

    pPortEntry = RBTreeGet (VLAN_LOCAL_PORT_RBTREE (), &Dummy);

    if (pPortEntry != NULL)
    {
        pCurrEntry = VlanGetNextCurrVlanEntry (u4VlanId, pPortEntry->u2Port);
        if (pCurrEntry != NULL)
        {
            *pi4NextIfIndex = pPortEntry->u4IfIndex;
            *pu4NextVlanId = (UINT4) pCurrEntry->VlanId;

            return VLAN_SUCCESS;
        }
    }

    pPortEntry = RBTreeGetNext (VLAN_LOCAL_PORT_RBTREE (), &Dummy, NULL);

    while (pPortEntry != NULL)
    {
        u4VlanId = 0;
        pCurrEntry = VlanGetNextCurrVlanEntry (u4VlanId, pPortEntry->u2Port);
        if (pCurrEntry != NULL)
        {
            *pi4NextIfIndex = pPortEntry->u4IfIndex;
            *pu4NextVlanId = (UINT4) pCurrEntry->VlanId;

            return VLAN_SUCCESS;
        }
        Dummy.u4IfIndex = pPortEntry->u4IfIndex;
        pPortEntry = RBTreeGetNext (VLAN_LOCAL_PORT_RBTREE (), &Dummy, NULL);
    }

    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetNextProtocolPortTableIndex                */
/*                                                                           */
/*    Description         : This function is used to get the index of the    */
/*                          Dot1vProtocolPortTable.                          */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                          i4GroupId - Current Group Id.                    */
/*                                                                           */
/*    Output(s)           : pi4NextIfIndex - Next IfIndex.                   */
/*                          pi4NextGroupId - Next Group Id.                  */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->VlanPortList            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
VlanGetNextProtocolPortTableIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                   INT4 i4GroupId, INT4 *pi4NextGroupId)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPortEntry     *pNextPortEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT1               u1IsEntryFound = VLAN_FALSE;
    UINT2               u2Port = 0;
    UINT2               u2FirstPort = 0;

    if ((i4IfIndex < 0) || (i4GroupId < 0))
    {
        return VLAN_FAILURE;
    }

    u2FirstPort = (UINT2) i4IfIndex;

    RB_OFFSET_SCAN (VLAN_LOCAL_PORT_RBTREE (), pVlanPortEntry,
                    pNextPortEntry, tVlanPortEntry *)
    {
        if (pVlanPortEntry->u4IfIndex >= u2FirstPort)
        {
            u2Port = (UINT2) pVlanPortEntry->u4IfIndex;

            VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSetEntry,
                           tVlanPortVidSet *)
            {
                if ((u2FirstPort < u2Port) || ((u2FirstPort == u2Port) &&
                                               (i4GroupId <
                                                (INT4) pVlanPortVidSetEntry->
                                                u4ProtGrpId)))
                {
                    if (u1IsEntryFound == VLAN_FALSE)
                    {

                        *pi4NextIfIndex = (INT4) u2Port;
                        *pi4NextGroupId = pVlanPortVidSetEntry->u4ProtGrpId;
                        u1IsEntryFound = VLAN_TRUE;
                    }
                    else
                    {

                        if ((*pi4NextIfIndex > (INT4) u2Port) ||
                            ((*pi4NextIfIndex == (INT4) u2Port) &&
                             (*pi4NextGroupId >
                              (INT4) pVlanPortVidSetEntry->u4ProtGrpId)))
                        {

                            *pi4NextIfIndex = (INT4) u2Port;
                            *pi4NextGroupId = pVlanPortVidSetEntry->u4ProtGrpId;
                        }
                    }
                }
            }
        }
    }

    if (u1IsEntryFound == VLAN_TRUE)
    {
        return VLAN_SUCCESS;
    }
    else
    {
        return VLAN_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanActive                                         */
/*                                                                           */
/*     DESCRIPTION      : This function will enter into vlan database config */
/*                        mode. The VLAN will be made active                 */
/*                                                                           */
/*     INPUT            : u4VlanId   - Vlan Identifier to made active        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanActive (tCliHandle CliHandle)
{
    INT4                i4Dot1qVlanStaticRowStatus;
    INT4                i4Val = 0;
    UINT4               u4VlanId;

    i4Val = CLI_GET_VLANID ();

    if (i4Val == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    u4VlanId = (UINT4) i4Val;

    if ((nmhGetDot1qVlanStaticRowStatus (u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        == SNMP_FAILURE)
    {
        /* A row in the Vlan table must have been created */
        CliPrintf (CliHandle, "\r%% Vlan %d is not created \r\n", u4VlanId);
        return CLI_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle,
                   "\r%% This configuration is not allowed for Transparent Bridges.\r\n");
        return CLI_FAILURE;
    }

    if (i4Dot1qVlanStaticRowStatus != VLAN_ACTIVE)
    {
        if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCliSelectContextOnMode                       */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu2LocalPort - Local Port Number.                */
/*                          pu2Flag      - Show command or Not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                            UINT4 *pu4Context, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    INT4                i4PortId = 0;
    UINT1               u1IntfCmdFlag = VLAN_FALSE;    /* This flag is used in MI 
                                                       case, to know whether 
                                                       the command is a 
                                                       interface mode command 
                                                       or vlan mode command. */

    /* For debug commands the context-name will be present in args[0], so
     * the select context will be done seperately within the
     * switch statement*/
    if ((u4Cmd == CLI_VLAN_GBL_DEBUG) || (u4Cmd == CLI_VLAN_NO_GBL_DEBUG) ||
        (u4Cmd == CLI_VLAN_DEBUGS) || (u4Cmd == CLI_VLAN_NO_DEBUGS))
    {
        return VLAN_SUCCESS;
    }

    *pu4Context = VLAN_DEF_CONTEXT_ID;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = (UINT4) CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != VLAN_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI. 
         * If the command is a vlan mode command then the context-id won't be
         * VLAN_CLI_INVALID_CONTEXT, So this is an interface mode command.
         * Now by refering this flag we have to get the context-id and 
         * local port number from the IfIndex (CLI_GET_IFINDEX). */
        u1IntfCmdFlag = VLAN_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     * VlanGetContextInfoFromIfIndex. */
    *pu2LocalPort = (UINT2) i4PortId;

    if (VlanVcmGetSystemMode (VLANMOD_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == VLAN_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (VlanVcmGetContextInfoFromIfIndex
                    ((UINT4) i4PortId, pu4Context, pu2LocalPort)
                    != VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return VLAN_FAILURE;
                }
            }
        }
    }

    /* Since we are calling SI nmh routine for Configuration commands
     * we have to do SelectContext for Config commands*/
    if (VlanSelectContext (*pu4Context) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return VLAN_FAILURE;
    }

    if ((VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE) &&
        (u4Cmd != CLI_VLAN_NO_SHUT))
    {
        CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
        VlanReleaseContext ();
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCliGetContextForShowCmd                      */
/*                                                                           */
/*    Description         : This function is called only when the show       */
/*                          command is given. It handles 3 different         */
/*                          ways of show commands.                           */
/*                            1. If switch name is given, we have to get     */
/*                               the context Id from it.                     */
/*                            2. If Interface Index is present, from that    */
/*                               we have to get the context Id.              */
/*                            3. Else we have to go for each and every       */
/*                               context.                                    */
/*                          For the above all thing we have to check for     */
/*                          the Module status of the context.                */
/*                                                                           */
/*    Input(s)            : u4CurrContext    - Context-Id.                   */
/*                          pu1Name          - Switch-Name.                  */
/*                          u4IfIndex        - Interface Index.              */
/*                                                                           */
/*    Output(s)           : CliHandle        - Contains error messages.      */
/*                          pu4NextContextId - Context Id.                   */
/*                          pu2LocalPort     - Local Port Number.            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name,
                             UINT4 u4IfIndex, UINT4 u4CurrContext,
                             UINT4 *pu4NextContext, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    UINT1               au1ContextName[VLAN_SWITCH_ALIAS_LEN];

    *pu2LocalPort = 0;

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1Name != NULL)
    {
        if (VlanVcmIsSwitchExist (pu1Name, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1Name);
            return VLAN_FAILURE;
        }
    }
    /* If IfIndex is given then get the Context-Id from it */
    else if (u4IfIndex != 0)
    {
        if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                              pu2LocalPort) != VLAN_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Interface not mapped to "
                       "any of the context.\r\n ");
            return VLAN_FAILURE;
        }
    }
    else
    {
        /* Case 1: At first entry for this funtion, If Switch-name is not given, 
           then get the first active Context. For this if we give 
           0xffffffff as current context for VlanGetNextActiveContext 
           function it will return the First Active context. 
           Case 2: At the Next frequent entries it will as per we want. */
        if (VlanGetNextActiveContext (u4CurrContext,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    do
    {
        u4CurrContext = u4ContextId;

        /* To avoid "Switch <switch-name>" print in SI */
        if (VlanVcmGetSystemMode (VLANMOD_PROTOCOL_ID) == VCM_MI_MODE)
        {
            /* if switch name is not given we have to get the switch-name 
             * from context-id for Display purpose */
            VLAN_MEMSET (au1ContextName, 0, VLAN_SWITCH_ALIAS_LEN);

            if (pu1Name == NULL)
            {
                VlanVcmGetAliasName (u4ContextId, au1ContextName);
            }
            else
            {
                VLAN_MEMCPY (au1ContextName, pu1Name, STRLEN (pu1Name));
            }
            CliPrintf (CliHandle, "\r\n\rSwitch %s\r\n", au1ContextName);
        }

        if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "\r\n\r%% VLAN switching is shutdown\r\n");
            if ((pu1Name != NULL) || (u4IfIndex != 0))
            {
                /* We have to come out of the loop when the switch is 
                 * shutdown, if the command is for specific switch else
                 * continue the Loop. */
                break;
            }
            continue;
        }
        *pu4NextContext = u4ContextId;
        return VLAN_SUCCESS;
    }
    while (VlanGetNextActiveContext (u4CurrContext,
                                     &u4ContextId) == VLAN_SUCCESS);

    return VLAN_FAILURE;
}

/*************************************************************************/
/* FUNCTION NAME  : VlanCliAddFidListScanAndPrint()                      */
/*                                                                       */
/* DESCRIPTION    : This function will add fdb id of specified vlan      */
/*                  into a bit map.And to display Fdb entries that match */
/*                  required criteria.                                   */
/*                                                                       */
/* INPUT          : CliHandle     - Cli Context Handle                   */
/*                  u4ContextId   - Context Identifier                   */
/*                  u4ArgsMask    - Bit mask indicating the presence     */
/*                                  the arguments (FdbId, MacAddr,       */
/*                                  Port and Status)                     */
/*                  pCliArgs      - Structure containing FdbId, MacAddr, */
/*                                  Port and Status which determine if   */
/*                                  the entry needs to be displayed.     */
/*                                                                       */
/* OUTPUT         : CliHandle     - Contains error messages              */
/*                  pi4Count      - Count of the number of entries       */
/*                                  displayed by this function. This     */
/*                                  must NOT be assigned. It must be     */
/*                                  incremented. If display termination  */
/*                                  occurs (throug page break) then this */
/*                                  counter is set to -1.                */
/*                  i4DispFlag    - flag to choose the required          */
/*                                  display funtion                      */
/*                                                                       */
/*                                                                       */
/* RETURNS        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/

INT4
VlanCliAddFidListScanAndPrint (tCliHandle CliHandle, UINT4 u4ContextId,
                               tVlanId StartVlanId, tVlanId LastVlanId,
                               UINT4 u4ArgsMask, tVlanCliArgs * pCliArgs,
                               INT4 *pi4Count, INT4 i4DispFlag)
{
    UINT4               u4FdbId = VLAN_INIT_VAL;
    tVlanId             VlanIndex = VLAN_INIT_VAL;
    BOOL1               bResult = OSIX_FALSE;

    VLAN_MEMSET (gVlanNullVlanList, 0, sizeof (tVlanList));

    for (VlanIndex = StartVlanId; VlanIndex <= LastVlanId; VlanIndex++)
    {
        u4FdbId = 0;
        /* To get the FdbId corresponding to VlanId and to set it in the bit-map */
        if ((u4FdbId = VlanL2IwfMiGetVlanFdbId (u4ContextId, VlanIndex)) != 0)
        {
            OSIX_BITLIST_IS_BIT_SET (gVlanNullVlanList, u4FdbId, VLAN_LIST_SIZE,
                                     bResult);

            if (bResult == OSIX_FALSE)
            {
                OSIX_BITLIST_SET_BIT (gVlanNullVlanList, u4FdbId,
                                      VLAN_LIST_SIZE);

                pCliArgs->u4VlanIdFdbId1 = u4FdbId;
                switch (i4DispFlag)
                {
                    case VLAN_SCAN_PRINT_FDB_TABLE:
                        VlanCliScanAndPrintFdbTable (CliHandle, u4ContextId,
                                                     VlanCliCompareArgs,
                                                     u4ArgsMask, pCliArgs,
                                                     VLAN_TRUE, pi4Count);
                        break;
                    case VLAN_SCAN_PRINT_STUCAST_TABLE:
                        VlanCliScanAndPrintStUcastTable (CliHandle, u4ContextId,
                                                         VlanCliCompareArgs,
                                                         u4ArgsMask, pCliArgs,
                                                         VLAN_TRUE, pi4Count);
                        break;

                    case VLAN_DOT1D_SCAN_PRINT_STUCAST_TABLE:
                        VlanCliDot1dScanAndPrintStUcastTable (CliHandle,
                                                              u4ContextId,
                                                              VlanCliCompareArgs,
                                                              u4ArgsMask,
                                                              pCliArgs,
                                                              VLAN_TRUE,
                                                              pi4Count);
                        break;
                    case VLAN_DOT1D_SCAN_PRINT_STMCAST_TABLE:
                        VlanCliDot1dScanAndPrintStMcastTable (CliHandle,
                                                              u4ContextId,
                                                              VlanCliCompareArgs,
                                                              u4ArgsMask,
                                                              pCliArgs,
                                                              VLAN_TRUE,
                                                              pi4Count);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCliUnicastMacSecScanAndPrint                   */
/*                                                                           */
/*     DESCRIPTION      : This function will delete fdb id sll               */
/*                                                                           */
/*     INPUT            : pCliFidList - Fid link list                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VlanCliUnicastMacSecScanAndPrint (tCliHandle CliHandle,
                                  UINT4 u4ContextId, UINT4 u4IfIndex)
{
    INT4                i4UnicastMacSecType = 0;
    INT4                i4LearningStatus = 0;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    UNUSED_PARAM (u4ContextId);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    CfaCliConfGetIfName (u4IfIndex, piIfName);
    CliPrintf (CliHandle, "\n\r----------------------------\r\n");
    CliPrintf (CliHandle, "interface %s\r\n", piIfName);
    CliPrintf (CliHandle, "----------------------------\r\n");

    nmhGetDot1qFutureVlanPortUnicastMacLearning ((INT4) u4IfIndex,
                                                 &i4LearningStatus);
    if (i4LearningStatus != VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "MAC learning : disable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "MAC learning : enable\r\n");
    }
    nmhGetDot1qFutureVlanPortUnicastMacSecType ((INT4) u4IfIndex,
                                                &i4UnicastMacSecType);
    if (i4UnicastMacSecType == VLAN_CLI_UNICAST_MAC_SEC_SAV)
    {
        CliPrintf (CliHandle, "port security violation type : protect\r\n");
    }
    else if (i4UnicastMacSecType == VLAN_CLI_UNICAST_MAC_SEC_SHV)
    {
        CliPrintf (CliHandle, "port security violation type : Restrict\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "port security violation type : Shutdown\r\n");
    }
    CliPrintf (CliHandle, "\r\n");

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanCliDelFidList                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will delete fdb id sll               */
/*                                                                           */
/*     INPUT            : pCliFidList - Fid link list                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
VlanCliDelFidList (tTMO_SLL * pCliFidList)
{
    /* delete the linked lists */
    if (TMO_SLL_Count (pCliFidList) > 0)
    {
        TMO_SLL_FreeNodes (pCliFidList, 0);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortFilteringCriteria                       */
/*                                                                           */
/*     DESCRIPTION      : This function will set port filtering criteria     */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u2PortFltCriteria - port filtering criteria        */
/*                                            (default/enhance)              */
/*                        CliHandle - contains error messages                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSetPortFilteringCriteria (tCliHandle CliHandle, UINT4 u4PortId,
                              UINT2 u2PortFltCriteria)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria (&u4ErrCode,
                                                          u4PortId,
                                                          u2PortFltCriteria)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanFilteringUtilityCriteria (u4PortId,
                                                       u2PortFltCriteria)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanAddWildCardEntry                               */
/*                                                                           */
/*     DESCRIPTION      : This function will set wild card entry in wildcard */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : pu1MacAddress - Mac Address of Wildcard entry      */
/*                        pu1Ports - egress port list for this entry         */
/*                        CliHandle - contains error messages                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanAddWildCardEntry (tCliHandle CliHandle, UINT1 *pu1MacAddress,
                      UINT1 *pu1Ports)
{
    UINT1              *pau1WildCardPorts = NULL;
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    pau1WildCardPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1WildCardPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanAddWildCardEntry: "
                  "Error in allocating memory for pau1WildCardPorts\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1WildCardPorts, 0, VLAN_PORT_LIST_SIZE);

    PortList.i4_Length = VLAN_PORT_LIST_SIZE;
    PortList.pu1_OctetList = pau1WildCardPorts;

    MEMCPY (PortList.pu1_OctetList, pu1Ports, PortList.i4_Length);

    if (nmhGetDot1qFutureVlanWildCardRowStatus (pu1MacAddress, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        if (i4RowStatus != VLAN_NOT_IN_SERVICE)
        {
            if (nmhTestv2Dot1qFutureVlanWildCardRowStatus
                (&u4ErrCode, pu1MacAddress, VLAN_NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {
                if (nmhSetDot1qFutureVlanWildCardRowStatus
                    (pu1MacAddress, VLAN_NOT_IN_SERVICE) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    UtilPlstReleaseLocalPortList (pau1WildCardPorts);
                    return CLI_FAILURE;
                }

            }
        }
    }
    else
    {
        if (nmhTestv2Dot1qFutureVlanWildCardRowStatus (&u4ErrCode,
                                                       pu1MacAddress,
                                                       VLAN_CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {
            if (nmhSetDot1qFutureVlanWildCardRowStatus (pu1MacAddress,
                                                        VLAN_CREATE_AND_WAIT) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                UtilPlstReleaseLocalPortList (pau1WildCardPorts);
                return CLI_FAILURE;
            }

        }
    }
    if (nmhTestv2Dot1qFutureVlanWildCardEgressPorts (&u4ErrCode,
                                                     pu1MacAddress,
                                                     &PortList) == SNMP_SUCCESS)
    {
        if (nmhSetDot1qFutureVlanWildCardEgressPorts (pu1MacAddress,
                                                      &PortList) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1WildCardPorts);
            return CLI_FAILURE;
        }
        if (nmhSetDot1qFutureVlanWildCardRowStatus (pu1MacAddress,
                                                    VLAN_ACTIVE) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            UtilPlstReleaseLocalPortList (pau1WildCardPorts);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    UtilPlstReleaseLocalPortList (pau1WildCardPorts);
    CLI_FATAL_ERROR (CliHandle);
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanDelWildCardEntry                               */
/*                                                                           */
/*     DESCRIPTION      : This function will delete wild card entry in       */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : pu1MacAddress - Mac Address of Wildcard entry      */
/*                        CliHandle - contains error messages                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanDelWildCardEntry (tCliHandle CliHandle, UINT1 *pu1MacAddress)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanWildCardRowStatus (&u4ErrCode, pu1MacAddress,
                                                   VLAN_DESTROY)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1qFutureVlanWildCardRowStatus (pu1MacAddress, VLAN_DESTROY)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanMICliShowWildCardTable                         */
/*                                                                           */
/*     DESCRIPTION      : This function will display wildcard entries        */
/*                        for given context and Mac addresss                 */
/*                                                                           */
/*     INPUT            : pu1MacAddr    - Mac Address of Wildcard entry      */
/*                        CliHandle - contains error messages                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMICliShowWildCardTable (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT1 *pu1MacAddr, UINT4 u4Showwildcardflag)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tPortList          *pPorts = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    INT4                i4NextContextId;
    INT4                i4CurrentContextId;
    INT4                i4RowStatus;
    INT4                i4PortType;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1isShowAll = TRUE;
    UINT1               u1isDisplayHeader = TRUE;

    MEMSET (au1String, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);

    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));

    i4CurrentContextId = (INT4) u4ContextId;

    if (nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable
        (i4CurrentContextId, &i4NextContextId, CurrMacAddr, &NextMacAddr)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (pu1MacAddr == NULL)
    {
        return CLI_FAILURE;
    }

    pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    do
    {

        if (i4NextContextId != i4CurrentContextId)
        {
            FsUtilReleaseBitList ((UINT1 *) pPorts);
            return CLI_SUCCESS;
        }
        if (VlanCmpMacAddr (CurrMacAddr, NextMacAddr) != VLAN_EQUAL)
        {
            MEMSET (*pPorts, 0, sizeof (tPortList));
        }

        nmhGetFsMIDot1qFutureVlanWildCardRowStatus (i4NextContextId,
                                                    NextMacAddr, &i4RowStatus);

        /* Memcpy the addr to dispaly all the entries */
        if (u4Showwildcardflag != 0)
        {
            MEMCPY (pu1MacAddr, NextMacAddr, sizeof (tMacAddr));
        }

        if ((pu1MacAddr == NULL) ||
            (VlanCmpMacAddr (pu1MacAddr, NextMacAddr) == VLAN_EQUAL))
        {
            if (i4RowStatus == VLAN_ACTIVE)
            {
                if (u1isDisplayHeader == TRUE)
                {
                    CliPrintf (CliHandle, "\r\n%-20s", "Wild Card Entries:");
                    CliPrintf (CliHandle, "\r\n%-20s", "------------------");
                    CliPrintf (CliHandle, "\r\n%-8s%-15s%-5s%-20s", " ",
                               "Mac Address", " ", " Ports  ");
                    CliPrintf (CliHandle, "\r\n%-5s%-15s%-1s,%-20s\r\n", " ",
                               "----------------", "  ",
                               " -------------------  ");
                    u1isDisplayHeader = FALSE;
                }

                if (VlanGetFirstPortInContext ((UINT4) i4NextContextId,
                                               &u4IfIndex) == VLAN_SUCCESS)
                {
                    do
                    {
                        nmhGetFsMIDot1qFutureVlanIsWildCardEgressPort
                            (i4NextContextId, NextMacAddr, u4IfIndex,
                             &i4PortType);

                        if (i4PortType == VLAN_SNMP_TRUE)
                        {
                            /* Added for pseudo wire visibility */
                            VLAN_SET_MEMBER_PORT_LIST ((*pPorts), u4IfIndex);
                        }

                        u4PrvIfIndex = u4IfIndex;
                    }
                    while (VlanGetNextPortInContext
                           ((UINT4) i4NextContextId, u4PrvIfIndex,
                            &u4IfIndex) == VLAN_SUCCESS);
                }

                PrintMacAddress (NextMacAddr, au1String);
                CliPrintf (CliHandle, "%-5s%-15s", " ", au1String);

                SnmpPorts.pu1_OctetList = *pPorts;
                SnmpPorts.i4_Length = sizeof (tPortList);

                CliOctetToIfName (CliHandle,
                                  NULL,
                                  &SnmpPorts,
                                  BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                                  sizeof (tPortList), 25, &u4PagingStatus, 4);

            }
        }
        i4CurrentContextId = i4NextContextId;

        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));

        if (nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable
            (i4CurrentContextId, &i4NextContextId,
             CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (i4CurrentContextId != i4NextContextId)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll == TRUE);

    FsUtilReleaseBitList ((UINT1 *) pPorts);
    return CLI_SUCCESS;

}

#ifdef PBB_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanMICliShowDefaultVid                            */
/*                                                                           */
/*     DESCRIPTION      : This function will display default vid for a VIP   */
/*                                                                           */
/*     INPUT            : u4ContextId   - Context id                         */
/*                        CliHandle - contains error messages                */
/*                        u4Isid    - service instance                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMICliShowDefaultVid (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Isid)
{
    INT4                i4Status;
    UINT4               u4RetVal;
    UINT4               u4IfIndex = 0;

    if (VlanPbbCliGetVipIndexOfIsid (u4ContextId, u4Isid, &u4IfIndex) !=
        VLAN_SUCCESS)
    {
        return (CLI_SUCCESS);
    }

    i4Status = nmhGetFsDot1qPvid (u4IfIndex, &u4RetVal);
    if ((i4Status == SNMP_SUCCESS))
    {
        CliPrintf (CliHandle, "Default vid : %d\r\n", u4RetVal);
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetVlanMapForIsid                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets vlan to isid mapping for an     */
/*                           I component                                     */
/*     INPUT            :  pau1VlanList - list of vlans                      */
/*                         i4UntaggedPip - pip to be tagged or untagged      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetVlanMapForIsid (tCliHandle CliHandle,
                       UINT1 *pau1VlanList, INT4 i4UntaggedPip, UINT2 u2VipPort)
{
    UINT1              *pu1TmpAll = NULL;

    tSNMP_OCTET_STRING_TYPE VlanListAll;

    pu1TmpAll = UtlShMemAllocVlanList ();
    if (pu1TmpAll == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    CLI_MEMSET (pu1TmpAll, 0, VLAN_LIST_SIZE);
    VlanListAll.pu1_OctetList = pu1TmpAll;
    VlanListAll.i4_Length = VLAN_LIST_SIZE;

    CLI_MEMCPY (VlanListAll.pu1_OctetList, pau1VlanList, VlanListAll.i4_Length);

    if (i4UntaggedPip == CLI_FALSE)
    {
        if (VlanSetPbbPorts (&VlanListAll, u2VipPort,
                             VLAN_TRUE) == VLAN_FAILURE)
        {
            UtlShMemFreeVlanList (pu1TmpAll);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (VlanSetPbbPorts (&VlanListAll, u2VipPort,
                             VLAN_FALSE) == VLAN_FAILURE)
        {
            UtlShMemFreeVlanList (pu1TmpAll);
            return CLI_FAILURE;
        }

    }

    UtlShMemFreeVlanList (pu1TmpAll);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;

}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetPortProtected                               */
/*                                                                           */
/*     DESCRIPTION      : This function will set the protected               */
/*                        feature of a port.                                 */
/*                                                                           */
/*     INPUT            : u2LocalPortId - Index of port                      */
/*                        i4Status - VLAN_SNMP_TRUE/VLAN_SNMP_FALSE          */
/*                        CliHandle - contains error messages                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetPortProtected (tCliHandle CliHandle, UINT4 u4PortId, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if ((nmhTestv2Dot1qFutureVlanPortProtected (&u4ErrorCode,
                                                (INT4) u4PortId,
                                                i4Status)) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if ((nmhSetDot1qFutureVlanPortProtected ((INT4) u4PortId,
                                             i4Status)) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetSwUnicastMacLimit                           */
/*                                                                           */
/*     DESCRIPTION      : This function will set the dynamic unicast mac     */
/*                        entries limit that can be learnt in a switch       */
/*                                                                           */
/*     INPUT            : u4UnicastMacLimit - Limit value of unicast mac for */
/*                                            switch                         */
/*                        CliHandle - contains error messages                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetSwUnicastMacLimit (tCliHandle CliHandle, UINT4 u4UnicastMacLimit)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureUnicastMacLearningLimit (&u4ErrCode,
                                                     u4UnicastMacLimit)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureUnicastMacLearningLimit (u4UnicastMacLimit)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetBaseBridgeMode                            */
/*                                                                           */
/*    Description         : This function is used to set the base-bridge mode*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetBaseBridgeMode (tCliHandle CliHandle, INT4 i4BaseBridgeMode)
{
    UINT4               u4ErrorCode = VLAN_INIT_VAL;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2Dot1qFutureVlanBaseBridgeMode (&u4ErrorCode, i4BaseBridgeMode)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanBaseBridgeMode (i4BaseBridgeMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMapToPort                                    */
/*                                                                           */
/*    Description         : This function will be called when pvid is set for*/
/*                          a interface. If the port is not member of        */
/*                          vlan(pvid),                                      */
/*                          port will set as member port of the vlan. If vlan*/
/*                          is not created, vlan will be created and port will*/
/*                          set as member port                               */
/*                                                                           */
/*     INPUT            : u2LocalPortId - Index of port                      */
/*                        CliHandle - contains error messages                */
/*                        u4VlanId - Vlan in which port will be memeber      */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMapToPort (tCliHandle CliHandle, UINT4 u4VlanId, UINT2 u2LocalPortId)
{

    UINT1              *pau1LocalUntaggedPorts = NULL;
    UINT1              *pau1LocalForbiddenPorts = NULL;
    UINT1              *pau1LocalMemberPorts = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry;
    INT4                i4OldVlanId = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Dot1qVlanStaticRowStatus = 0;
    INT4                i4RetStatus = 0;
    UINT1               u1Result = 0;
    UINT1               u1NewVlan = 0;

    UNUSED_PARAM (CliHandle);
    if (u2LocalPortId >= (VLAN_MAX_PORTS + 1))
    {
        return CLI_FAILURE;
    }

    /* Check whether the Vlan entry is already present */
    if ((nmhGetDot1qVlanStaticRowStatus (u4VlanId,
                                         &i4Dot1qVlanStaticRowStatus))
        == SNMP_FAILURE)
    {
        /* A row in the Vlan table must have been created */
        CliPrintf (CliHandle, "\r%% Access VLAN does not exist. "
                   "Creating vlan \r\n", u4VlanId);

        /* VLAN NOT present - CREATE */

        if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, u4VlanId,
                                               VLAN_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* CREATE VLAN */
        if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        else
        {
            u1NewVlan = VLAN_TRUE;
        }

    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanId);

    if (pStVlanEntry == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*Check current port is member of vlan */
    VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2LocalPortId, u1Result);

    /* When HwUntaggedPortsForVlanSup is not supported, 
     * Not possible to have utagged member ports for more than one vlan 
     */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_UNTAGGED_PORTS_FOR_VLANS))
    {
        /* Due to h/w Limitation,adding of a port as untagged to any one of the vlans, 
         * Remove the untagged ports from Vlans before adding as untagged to any vlan. 
         */
        i4RetStatus =
            VlanRemoveUntaggedPortFromVlans (CliHandle, u4VlanId,
                                             u2LocalPortId, (UINT1) VLAN_TRUE);
    }

    /*Add member ports to the vlan in following conditions
     * 1. If new vlan is created and
     * 2. Vlan is already created but not current port is not
     *    member of vlan*/
    if ((u1NewVlan == VLAN_TRUE) || (u1Result == VLAN_FALSE))
    {
        pau1LocalUntaggedPorts =
            UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
        if (pau1LocalUntaggedPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanMapToPort: "
                      "Error in allocating memory for pau1LocalUntaggedPorts\r\n");
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalUntaggedPorts, 0, VLAN_PORT_LIST_SIZE);

        pau1LocalForbiddenPorts =
            UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
        if (pau1LocalForbiddenPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanMapToPort: "
                      "Error in allocating memory for pau1LocalForbiddenPorts\r\n");
            UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

        pau1LocalMemberPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
        if (pau1LocalMemberPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanMapToPort: "
                      "Error in allocating memory for pau1LocalMemberPorts\r\n");
            UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
            UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);
        VLAN_GET_EGRESS_PORTS (pStVlanEntry, pau1LocalMemberPorts);
        VLAN_GET_UNTAGGED_PORTS (pStVlanEntry, pau1LocalUntaggedPorts);
        VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pau1LocalForbiddenPorts);
        VLAN_SET_MEMBER_PORT (pau1LocalMemberPorts, u2LocalPortId);
        VLAN_SET_MEMBER_PORT (pau1LocalUntaggedPorts, u2LocalPortId);

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);
        if (pVlanPortEntry->u1PortType == VLAN_TRUNK_PORT)
        {
            VLAN_RESET_MEMBER_PORT (pau1LocalUntaggedPorts, u2LocalPortId);
        }

        i4OldVlanId = CLI_GET_VLANID ();
        CLI_SET_VLANID ((tVlanId) u4VlanId);
        i4RetStatus = VlanSetPorts (CliHandle, pau1LocalMemberPorts,
                                    pau1LocalUntaggedPorts,
                                    pau1LocalForbiddenPorts, VLAN_ADD);
        if (i4RetStatus == CLI_FAILURE)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "VlanSetPorts: " "Error in port setting \r\n");
        }
        CLI_SET_VLANID ((tVlanId) i4OldVlanId);
        UtilPlstReleaseLocalPortList (pau1LocalUntaggedPorts);
        UtilPlstReleaseLocalPortList (pau1LocalForbiddenPorts);
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
    }
    /* When HwUntaggedPortsForVlanSup is supported,
     * Having untagged member ports for more than one vlan is possible.
     */
    if (ISS_HW_SUPPORTED ==
        IssGetHwCapabilities (ISS_HW_UNTAGGED_PORTS_FOR_VLANS))
    {
        i4RetStatus =
            (INT4) VlanRemoveUntaggedPortFromVlans (CliHandle, u4VlanId,
                                                    u2LocalPortId,
                                                    (UINT1) VLAN_TRUE);
    }

    i4RetStatus = VlanSetPortPvid (CliHandle, u4VlanId, (UINT4) u2LocalPortId);
    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name      : VlanLockAndSelCliContext                             */
/*                                                                           */
/* Description        : This function is used to take the VLAN mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread. This function also selects*/
/*                      the context stored in gu4VlanCliContext variable.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanLockAndSelCliContext (VOID)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    i4RetVal = VLAN_LOCK ();
    VlanSelectContext (gu4VlanCliContext);
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanSrcDisplayPortPvid                               */
/*                                                                           */
/* Description        : This function displays the user configured           */
/*                      port pvid value for a interface in  show running     */
/*                      configuration.                                       */
/*                                                                           */
/*    INPUT           : CliHandle - Handle to the cli context                */
/*                      u4ContextId - Context Identifier                     */
/*                      u4IfIndex - Global interface index                   */
/*                      i4LocalPort - Local port of particular context       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSrcDisplayPortPvid (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4IfIndex, INT4 i4LocalPort, UINT1 *pu1HeadFlag)
{
    INT4                i4Pvid = SNMP_SUCCESS;
    INT4                i4Status = SNMP_FAILURE;
    UINT4               u4IcchIfIndex = 0;

    i4Status = nmhGetDot1qPvid (i4LocalPort, (UINT4 *) &i4Pvid);

    if (i4Status == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    VlanIcchGetIcclIfIndex (&u4IcchIfIndex);

    /* Since the MI nmhGet routines are called above, current context would
     * have got released. Hence select the context. */

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((u4IcchIfIndex != u4IfIndex) && (i4Pvid != VLAN_DEFAULT_PORT_VID))
    {
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        CliPrintf (CliHandle, "switchport pvid %d\r\n", i4Pvid);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanSrcDisplayPortType                               */
/*                                                                           */
/* Description        : This function displays the user configured           */
/*                      port type value for a interface in show running      */
/*                      configuration.                                       */
/*                                                                           */
/*    INPUT           : CliHandle - Handle to the cli context                */
/*                      i4LocalPort - Local port of particular context       */
/*                      u1PbPortType - Port type                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSrcDisplayPortType (tCliHandle CliHandle, INT4 i4LocalPort,
                        UINT1 u1PbPortType, UINT1 *pu1HeadFlag)
{
    INT4                i4Status = SNMP_FAILURE;
    INT4                i4PortType = 0;
    i4Status = nmhGetDot1qFutureVlanPortType (i4LocalPort, &i4PortType);

    if (i4Status == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    /* Default values for Vlan Port Type for Different types of bridge
     * ports -
     *
     * Bridge Port    Vlan PortType
     * -----------    -------------
     * CEP            Access
     * CNP(PortBased) Access
     * PCEP           Access
     * PCNP           Hybrid
     * PNP            Hybrid
     * CNP(S-Tagged)  Hybrid
     * PPNP           Hybrid
     * CBP            Hybrid
     */

    if (i4PortType == VLAN_ACCESS_PORT)
    {
        if ((u1PbPortType != VLAN_CUSTOMER_EDGE_PORT)
            && (u1PbPortType != VLAN_CNP_PORTBASED_PORT)
            && (u1PbPortType != VLAN_PROP_CUSTOMER_EDGE_PORT))
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, "switchport mode access\r\n");
        }
    }
    else if (i4PortType == VLAN_HYBRID_PORT)
    {
        if ((u1PbPortType != VLAN_PROVIDER_NETWORK_PORT)
            && (u1PbPortType != VLAN_PROP_CUSTOMER_NETWORK_PORT)
            && (u1PbPortType != VLAN_CNP_TAGGED_PORT)
            && (u1PbPortType != VLAN_PROP_PROVIDER_NETWORK_PORT)
            && (u1PbPortType != VLAN_CUSTOMER_BRIDGE_PORT))
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

            CliPrintf (CliHandle, "switchport mode hybrid\r\n");
        }
    }
    else if (i4PortType == VLAN_TRUNK_PORT)
    {
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);

        CliPrintf (CliHandle, "switchport mode trunk\r\n");
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name       : VlanAddPortToDefaultVlan                          */
/*                                                                            */
/*    Description         : This function is used to add port to default vlan */
/*                                                                            */
/*    Input(s)            : CliHandle     - Cli Handle                        */
/*                          u2LocalPortId - Port Id                           */
/*                                                                            */
/*    Output(s)           : None                                              */
/*                                                                            */
/*    Returns             : None                                              */
/*                                                                            */
/******************************************************************************/
VOID
VlanAddPortToDefaultVlan (tCliHandle CliHandle, UINT2 u2LocalPortId)
{

    UINT1              *pau1LocalUntaggedPorts = NULL;
    UINT1              *pau1LocalForbiddenPorts = NULL;
    UINT1              *pau1LocalMemberPorts = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    INT4                i4OldVlanId = 0;

    UINT1               u1Result = 0;

    pStVlanEntry = VlanGetStaticVlanEntry (VLAN_DEF_VLAN_ID);

    if (pStVlanEntry == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    /*Check current port is member of vlan */
    VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2LocalPortId, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        pau1LocalUntaggedPorts = FsUtilAllocBitList (VLAN_IFPORT_LIST_SIZE);

        if (pau1LocalUntaggedPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanAddPortToDefaultVlan: Error in allocating memory "
                      "for pau1LocalUntaggedPorts\r\n");
            return;
        }
        MEMSET (pau1LocalUntaggedPorts, 0, VLAN_IFPORT_LIST_SIZE);

        pau1LocalForbiddenPorts = FsUtilAllocBitList (VLAN_IFPORT_LIST_SIZE);

        if (pau1LocalForbiddenPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanAddPortToDefaultVlan: Error in allocating memory "
                      "for pau1LocalForbiddenPorts \r\n");
            FsUtilReleaseBitList (pau1LocalUntaggedPorts);
            return;
        }
        MEMSET (pau1LocalForbiddenPorts, 0, VLAN_IFPORT_LIST_SIZE);

        pau1LocalMemberPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
        if (pau1LocalMemberPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanMapToPort: "
                      "Error in allocating memory for pau1LocalMemberPorts\r\n");
            FsUtilReleaseBitList (pau1LocalUntaggedPorts);
            FsUtilReleaseBitList (pau1LocalForbiddenPorts);
            return;
        }
        MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);
        VLAN_GET_EGRESS_PORTS (pStVlanEntry, pau1LocalMemberPorts);
        VLAN_GET_UNTAGGED_PORTS (pStVlanEntry, pau1LocalUntaggedPorts);
        VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pau1LocalForbiddenPorts);
        VLAN_SET_MEMBER_PORT (pau1LocalMemberPorts, u2LocalPortId);
        VLAN_SET_MEMBER_PORT (pau1LocalUntaggedPorts, u2LocalPortId);
        i4OldVlanId = CLI_GET_VLANID ();
        CLI_SET_VLANID ((tVlanId) VLAN_DEF_VLAN_ID);
        VlanSetPorts (CliHandle, pau1LocalMemberPorts,
                      pau1LocalUntaggedPorts, pau1LocalForbiddenPorts, 0);
        CLI_SET_VLANID ((tVlanId) i4OldVlanId);
        FsUtilReleaseBitList (pau1LocalUntaggedPorts);
        FsUtilReleaseBitList (pau1LocalForbiddenPorts);
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanRemoveUntaggedPortFromVlans                  */
/*                                                                           */
/*    Description         : This function is used to remove port from all    */
/*                          vlan other than given vlan id - u4VlanId         */
/*                                                                           */
/*    Input(s)            :  CliHandle     - Cli Handle                      */
/*                           u4VlanId      - Vlan Id                         */
/*                           u2LocalPortId - Port number                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SUCCESS/FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanRemoveUntaggedPortFromVlans (tCliHandle CliHandle, UINT4 u4VlanId,
                                 UINT2 u2LocalPortId, UINT1 u1SkipVlan)
{
    UINT1              *pau1LocalUntaggedPorts = NULL;
    UINT1              *pau1LocalForbiddenPorts = NULL;
    UINT1              *pau1LocalMemberPorts = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4OldVlanId = 0;
    INT4                i4RetStatus = 0;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4VlanIndex = 0;
    tSNMP_OCTET_STRING_TYPE OldEgressPorts;

    for (u4VlanIndex = 1; u4VlanIndex <= VLAN_DEV_MAX_VLAN_ID; u4VlanIndex++)
    {
        if ((u4VlanIndex == u4VlanId) && (u1SkipVlan == (UINT1) VLAN_TRUE))
        {
            continue;
        }
        if (AstIsPvrstStartedInContext (VLAN_DEF_CONTEXT_ID))
        {
            if (u4VlanIndex == VLAN_DEF_VLAN_ID)
            {
                continue;
            }
        }
        pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanIndex);
        if (pStVlanEntry == NULL)
        {
            continue;
        }

        VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2LocalPortId, u1Result);
        if (u1Result == VLAN_FALSE)
        {
            continue;
        }
        pau1LocalUntaggedPorts = FsUtilAllocBitList (VLAN_IFPORT_LIST_SIZE);

        if (pau1LocalUntaggedPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanRemoveUntaggedPortFromVlans: Error in allocating memory "
                      "for pau1LocalUntaggedPorts\r\n");
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalUntaggedPorts, 0, VLAN_IFPORT_LIST_SIZE);

        pau1LocalForbiddenPorts = FsUtilAllocBitList (VLAN_IFPORT_LIST_SIZE);

        if (pau1LocalForbiddenPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanRemoveUntaggedPortFromVlans: Error in allocating memory "
                      "for pau1LocalForbiddenPorts \r\n");
            FsUtilReleaseBitList (pau1LocalUntaggedPorts);
            return CLI_FAILURE;
        }
        MEMSET (pau1LocalForbiddenPorts, 0, VLAN_IFPORT_LIST_SIZE);

        pau1LocalMemberPorts = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
        if (pau1LocalMemberPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanRemoveUntaggedPortFromVlans: Error in allocating memory "
                      "for pau1LocalMemberPorts\r\n");
            FsUtilReleaseBitList (pau1LocalUntaggedPorts);
            FsUtilReleaseBitList (pau1LocalForbiddenPorts);
            return CLI_FAILURE;

        }
        MEMSET (pau1LocalMemberPorts, 0, VLAN_PORT_LIST_SIZE);

        VLAN_GET_EGRESS_PORTS (pStVlanEntry, pau1LocalMemberPorts);
        VLAN_GET_UNTAGGED_PORTS (pStVlanEntry, pau1LocalUntaggedPorts);
        VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pau1LocalForbiddenPorts);

        VLAN_RESET_MEMBER_PORT (pau1LocalMemberPorts, u2LocalPortId);
        VLAN_RESET_MEMBER_PORT (pau1LocalUntaggedPorts, u2LocalPortId);

        if (MEMCMP (gNullPortList, pau1LocalMemberPorts, VLAN_PORT_LIST_SIZE) ==
            0)
        {
            OldEgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
            OldEgressPorts.pu1_OctetList = gNullPortList;
            nmhSetDot1qVlanStaticEgressPorts (u4VlanIndex, &OldEgressPorts);
            nmhSetDot1qVlanStaticUntaggedPorts (u4VlanIndex, &OldEgressPorts);
            nmhSetDot1qVlanForbiddenEgressPorts (u4VlanIndex, &OldEgressPorts);
        }
        else
        {
            u4OldVlanId = CLI_GET_VLANID ();
            CLI_SET_VLANID ((tVlanId) u4VlanIndex);
            i4RetStatus = VlanSetPorts (CliHandle, pau1LocalMemberPorts,
                                        pau1LocalUntaggedPorts,
                                        pau1LocalForbiddenPorts, 0);
            CLI_SET_VLANID ((tVlanId) u4OldVlanId);
        }
        FsUtilReleaseBitList (pau1LocalUntaggedPorts);
        FsUtilReleaseBitList (pau1LocalForbiddenPorts);
        UtilPlstReleaseLocalPortList (pau1LocalMemberPorts);
    }
    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetCounterStatus                             */
/*                                                                           */
/*    Description         : This function is used to enable/disable the      */
/*                          statistics collection on the configured VLAN     */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4VlanId        - Vlan Id                        */
/*                          i4CounterStatus - Counter Status Enable/Disable  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetCounterStatus (tCliHandle CliHandle, UINT4 u4VlanId,
                      INT4 i4CounterStatus)
{
    UINT4               u4ErrorCode;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;

    u4ContextId = CLI_GET_CXT_ID ();

    if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        u4ContextId = VLAN_DEF_CONTEXT_ID;
    }
    if (nmhTestv2FsMIDot1qFutureVlanCounterStatus
        (&u4ErrorCode, (INT4) u4ContextId, u4VlanId,
         i4CounterStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIDot1qFutureVlanCounterStatus
        ((INT4) u4ContextId, u4VlanId, i4CounterStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetLoopbackStatus                            */
/*                                                                           */
/*    Description         : This function is used to enable/disable the      */
/*                          statistics collection on the configured VLAN     */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4VlanId        - Vlan Id                        */
/*                          i4LoopbackStatus - Loopback Status Enable/Disable*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetLoopbackStatus (tCliHandle CliHandle, UINT4 u4VlanId,
                       INT4 i4LoopbackStatus)
{
    UINT4               u4ErrorCode;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;

    u4ContextId = CLI_GET_CXT_ID ();

    if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        u4ContextId = VLAN_DEF_CONTEXT_ID;
    }

    if (nmhTestv2FsMIDot1qFutureVlanLoopbackStatus
        (&u4ErrorCode, (INT4) u4ContextId, u4VlanId,
         i4LoopbackStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIDot1qFutureVlanLoopbackStatus
        ((INT4) u4ContextId, u4VlanId, i4LoopbackStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCliFlushFdb                                  */
/*                                                                           */
/*    Description         : This function is used to Flush the Fdb entries   */
/*                          based on the provided inputs.                    */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4IfIndex       - Interface Index                */
/*                          u4VlanId        - Vlan Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliFlushFdb (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VlanId)
{
    UINT4               u4ErrorCode = VLAN_INIT_VAL;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;

    UNUSED_PARAM (CliHandle);
    u4ContextId = CLI_GET_CXT_ID ();

    if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        u4ContextId = VLAN_DEF_CONTEXT_ID;
    }

    /*Global FDB Flush */
    if ((u4IfIndex == VLAN_ZERO) && (u4VlanId == VLAN_ZERO))
    {
        if (nmhTestv2FsMIDot1qFutureVlanGlobalsFdbFlush (&u4ErrorCode,
                                                         (INT4) u4ContextId,
                                                         VLAN_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDot1qFutureVlanGlobalsFdbFlush ((INT4) u4ContextId,
                                                      VLAN_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    /*Port based FDB Flush */
    else if ((u4IfIndex != VLAN_ZERO) && (u4VlanId == VLAN_ZERO))
    {
        if (nmhTestv2FsMIDot1qFutureVlanPortFdbFlush (&u4ErrorCode,
                                                      (INT4) u4IfIndex,
                                                      VLAN_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDot1qFutureVlanPortFdbFlush ((INT4) u4IfIndex, VLAN_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    /*Vlan based FDB Flush */
    else if ((u4IfIndex == VLAN_ZERO) && (u4VlanId != VLAN_ZERO))
    {
        if (nmhTestv2FsMIDot1qFutureStVlanFdbFlush (&u4ErrorCode,
                                                    (INT4) u4ContextId,
                                                    u4VlanId, VLAN_ENABLED)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDot1qFutureStVlanFdbFlush ((INT4) u4ContextId,
                                                 u4VlanId, VLAN_ENABLED)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    /*Port, Vlan based FDB Flush */
    else if ((u4IfIndex != VLAN_ZERO) && (u4VlanId != VLAN_ZERO))
    {
        if (nmhTestv2FsMIDot1qFuturePortVlanFdbFlush (&u4ErrorCode,
                                                      (INT4) u4ContextId,
                                                      u4VlanId,
                                                      (INT4) u4IfIndex,
                                                      VLAN_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDot1qFuturePortVlanFdbFlush ((INT4) u4ContextId, u4VlanId,
                                                   (INT4) u4IfIndex, VLAN_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/********************************************************************************/
/*     FUNCTION NAME    : VlanSetPortTPID                                       */
/*                                                                              */
/*     DESCRIPTION      : This function will Set allowable Ether Type for Port  */
/*                                                                              */
/*     INPUT            : i4PortId  - Port Index                                */
/*                        u4AllowableTpid - Ether Type to be configured         */
/*                                                                              */
/*     OUTPUT           : CliHandle - Contains error messages                   */
/*                                                                              */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                               */
/********************************************************************************/

INT4
VlanSetPortTPID (tCliHandle CliHandle, INT4 i4PortId,
                 UINT4 u4AllowableTpidType, UINT4 u4AllowableTpid)
{
    UINT4               u4ErrCode;
    INT4                i4UserDefinedTPID = 0;
    if (u4AllowableTpidType == CLI_VLAN_PORT_TPID1)
    {
        if (nmhTestv2Dot1qFutureVlanPortAllowableTPID1
            (&u4ErrCode, i4PortId, (INT4) u4AllowableTpid) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetDot1qFutureVlanPortAllowableTPID1
            (i4PortId, (INT4) u4AllowableTpid) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    else if (u4AllowableTpidType == CLI_VLAN_PORT_TPID2)
    {
        if (nmhTestv2Dot1qFutureVlanPortAllowableTPID2
            (&u4ErrCode, i4PortId, (INT4) u4AllowableTpid) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetDot1qFutureVlanPortAllowableTPID2
            (i4PortId, (INT4) u4AllowableTpid) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    else if (u4AllowableTpidType == CLI_VLAN_PORT_TPID3)
    {
        if (u4AllowableTpid != 0)
        {
            if (nmhGetDot1qFutureVlanUserDefinedTPID (&i4UserDefinedTPID) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2Dot1qFutureVlanPortAllowableTPID3
            (&u4ErrCode, i4PortId, i4UserDefinedTPID) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetDot1qFutureVlanPortAllowableTPID3
            (i4PortId, i4UserDefinedTPID) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanSetUserDefinedTPID                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Set the User defined TPID
 *                        configurable for a port in Provider Bridge        */
/*                                                                          */
/*     INPUT            : u4UserDefinedTPID                                 */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanSetUserDefinedTPID (tCliHandle CliHandle, UINT4 u4UserDefinedTPID)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanUserDefinedTPID
        (&u4ErrCode, (INT4) u4UserDefinedTPID) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanUserDefinedTPID ((INT4) u4UserDefinedTPID) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanShowUserDefinedTPID                         */
/*     DESCRIPTION      : This function will Displays User defined TPID     */
/*                                                                          */
/*     INPUT            : CliHandle -CliHandle                              */
/*                        u4ContextId - Switch Context Id                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanShowUserDefinedTPID (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4UserDefinedTPID = 0;

    nmhGetFsMIDot1qFutureVlanUserDefinedTPID ((INT4) u4ContextId,
                                              &i4UserDefinedTPID);
    CliPrintf (CliHandle, "%-33s : 0x%x\r\n", "User Defined TPID ",
               i4UserDefinedTPID);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortEtherType                            */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Ether Type for Port        */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Direction- Ingress/Egress direction             */
/*                        u4EtherType - Ether Type to be configured         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanSetPortEtherType (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Direction,
                      UINT4 u4EtherType)
{
    UINT4               u4ErrCode;
    INT4                i4TmpEtherType = 0;

    i4TmpEtherType = (INT4) u4EtherType;

    if (u4Direction == CLI_INGRESS)
    {
        if (nmhTestv2Dot1qFutureVlanPortIngressEtherType
            (&u4ErrCode, i4PortId, i4TmpEtherType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureVlanPortIngressEtherType (i4PortId, i4TmpEtherType)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2Dot1qFutureVlanPortEgressEtherType
            (&u4ErrCode, i4PortId, i4TmpEtherType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureVlanPortEgressEtherType (i4PortId, i4TmpEtherType)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanSetVlanEgressEthertype                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Ether Type for Port        */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Direction- Ingress/Egress direction             */
/*                        u4EtherType - Ether Type to be configured         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanSetVlanEgressEthertype (tCliHandle CliHandle, tVlanId VlanId,
                            UINT4 u4EtherType)
{
    UINT4               u4ErrCode;

    if (u4EtherType == VLAN_CLI_USER_DEFINED_TPID)
    {
        if (VLAN_CURR_CONTEXT_PTR () == NULL)
        {
            return CLI_FAILURE;
        }

        u4EtherType = VLAN_PORT_USER_DEFINED_TPID;
    }

    if (nmhTestv2Dot1qFutureStVlanEgressEthertype
        (&u4ErrCode, (UINT4) VlanId, (INT4) u4EtherType) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureStVlanEgressEthertype
        ((UINT4) VlanId, (INT4) u4EtherType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanSetPortEgressTPIDType                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Ether Type for Port        */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Direction- Ingress/Egress direction             */
/*                        u4EtherType - Ether Type to be configured         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanSetPortEgressTPIDType (tCliHandle CliHandle, INT4 i4PortId,
                           UINT4 u4EgressTPIDType)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qFutureVlanPortEgressTPIDType
        (&u4ErrCode, i4PortId, (INT4) u4EgressTPIDType) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureVlanPortEgressTPIDType
        (i4PortId, (INT4) u4EgressTPIDType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanGetDefaultPortEtherType                       */
/*                                                                          */
/*     DESCRIPTION      : This function will Get Default Ether Type for Port*/
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Direction- Ingress/Egress direction             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        u4EtherType - Ether Type to be fetched            */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanGetDefaultPortEtherType (UINT2 u2PortId,
                             UINT4 u4Direction, UINT4 *u4EtherType)
{
    switch (u4Direction)
    {
        case CLI_INGRESS:
            if (VLAN_CUSTOMER_BRIDGE_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_PROP_PROVIDER_NETWORK_PORT == VLAN_PB_PORT_TYPE (u2PortId))
            {
                *u4EtherType = VLAN_CUSTOMER_PROTOCOL_ID;
                break;
            }
            if (VLAN_PROVIDER_NETWORK_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_CNP_PORTBASED_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_CNP_TAGGED_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_CUSTOMER_EDGE_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_PROP_CUSTOMER_NETWORK_PORT == VLAN_PB_PORT_TYPE (u2PortId)
                || VLAN_PROP_CUSTOMER_EDGE_PORT == VLAN_PB_PORT_TYPE (u2PortId))
            {
                *u4EtherType = VLAN_PROVIDER_PROTOCOL_ID;
                break;
            }

        case CLI_EGRESS:
            if (VLAN_CUSTOMER_BRIDGE_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_CUSTOMER_EDGE_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_PROP_PROVIDER_NETWORK_PORT == VLAN_PB_PORT_TYPE (u2PortId))
            {
                *u4EtherType = VLAN_CUSTOMER_PROTOCOL_ID;
                break;
            }
            if (VLAN_PROVIDER_NETWORK_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_CNP_PORTBASED_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_CNP_TAGGED_PORT == VLAN_PB_PORT_TYPE (u2PortId) ||
                VLAN_PROP_CUSTOMER_NETWORK_PORT == VLAN_PB_PORT_TYPE (u2PortId)
                || VLAN_PROP_CUSTOMER_EDGE_PORT == VLAN_PB_PORT_TYPE (u2PortId))
            {
                *u4EtherType = VLAN_PROVIDER_PROTOCOL_ID;
                break;
            }

        default:
            break;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliGetShowCmdOutputToFile                        */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    CHR1               *au1vlanShowCmdList[VLAN_DYN_MAX_CMDS] =
        { "show vlan ascending > ",
        "show interfaces description >> ",
        "show vlan learning params >> ",
        "show vlan traffic-classes >> ",
        "show vlan port config >> ",
        "show mac-address-table >> ",
        "show mac-address-table dynamic multicast >> ",
        "show mac-address-table dynamic unicast >> ",
        "show mac-address-table count >> "
    };
    INT4                i4Cmd = 0;
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return VLAN_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < VLAN_DYN_MAX_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile ((UINT1 *) pu1FileName,
                                       (UINT1 *) au1vlanShowCmdList[i4Cmd]) ==
            CLI_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliCalcSwAudCheckSum                             */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[VLAN_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, VLAN_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return VLAN_FAILURE;
    }
    MEMSET (ai1Buf, 0, VLAN_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (VlanCliReadLineFromFile (i4Fd, ai1Buf, VLAN_CLI_MAX_GROUPS_LINE_LEN,
                                    &i2ReadLen) != VLAN_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCalcCheckSum ((UINT1 *) ai1Buf, (UINT4) i2ReadLen, &u4Sum,
                              &u2CkSum, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', VLAN_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }

    if (FileClose (i4Fd) < 0)
    {
        return VLAN_FAILURE;
    }

    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCalcCheckSum ((UINT1 *) ai1Buf, (UINT4) i2ReadLen, &u4Sum,
                          &u2CkSum, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliReadLineFromFile                              */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT1
VlanCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                         INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (VLAN_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (VLAN_CLI_EOF);
}

/*****************************************************************************/
/* Function Name      : VlanCliFlushRemoteFdb                                */
/*                                                                           */
/* Description        : This function is called to delete the remote FDB     */
/*                      entries learnt on MC-LAG nodes.                      */
/*                                                                           */
/* Input(s)           : CliHandle     - Cli Context Handle                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/

INT4
VlanCliFlushRemoteFdb (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = VLAN_INIT_VAL;
    INT4                i4ContextId = VLAN_DEF_CONTEXT_ID;

    i4ContextId = (INT4) CLI_GET_CXT_ID ();

    if (i4ContextId == (INT4) VLAN_CLI_INVALID_CONTEXT)
    {
        i4ContextId = VLAN_DEF_CONTEXT_ID;
    }
    /*Remote FDB Flush */
    if (nmhTestv2FsMIDot1qFutureVlanRemoteFdbFlush (&u4ErrorCode, i4ContextId,
                                                    VLAN_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIDot1qFutureVlanRemoteFdbFlush (i4ContextId, VLAN_TRUE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliSetPortPacketReflectionStatus                 */
/*                                                                           */
/* Description        : This function calls low level routine to set port    */
/*                      reflection status.                                   */
/*                                                                           */
/* Input(s)           : u2PortId - Port for which reflection status          */
/*                                should be set.                             */
/*                      i4ReflectionStatus - Contains status which is to     */
/*                                be set (enable/disable)                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI__FAILURE                             */
/*****************************************************************************/

INT4
VlanCliSetPortPacketReflectionStatus (tCliHandle CliHandle,
                                      UINT4 u4ContextId, UINT2 u2PortId,
                                      INT4 i4ReflectionStatus)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VLAN_SUCCESS != VlanSelectContext (u4ContextId))
    {
        return CLI_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2PortId);

    if (NULL != pVlanPortEntry)
    {
        u4IfIndex = (UINT4) VLAN_GET_PHY_PORT (u2PortId);
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Invalid port index\r\n ");

        VlanReleaseContext ();
        return CLI_FAILURE;
    }

    VlanReleaseContext ();

    i1RetVal = nmhTestv2FsMIDot1qFuturePortPacketReflectionStatus (&u4ErrorCode,
                                                                   (INT4)
                                                                   u4IfIndex,
                                                                   i4ReflectionStatus);
    if (SNMP_FAILURE == i1RetVal)
    {
        return CLI_FAILURE;
    }
    else
    {
        i1RetVal =
            nmhSetFsMIDot1qFuturePortPacketReflectionStatus ((INT4) u4IfIndex,
                                                             i4ReflectionStatus);
        if (SNMP_FAILURE == i1RetVal)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set reflection status for the port %d",
                       u4IfIndex);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanCliPrintIntf                             */
/*                                                                           */
/* Description        : This function calls the CliPrintf to print           */
/*             the interface details                     */
/*                                                   */
/* INPUT              :    CliHandle, i4LocalPort, pu1HeadFlag             */
/*                                          */
/* OUTPUT          :   None                                               */
/*                                                                           */
/* Return Value(s)    :    Void                                         */
/*****************************************************************************/

VOID
VlanCliPrintIntf (tCliHandle CliHandle, UINT4 i4LocalPort, UINT1 *pu1HeadFlag)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (*pu1HeadFlag == VLAN_FALSE)
    {
        VlanCfaCliConfGetIfName (VLAN_GET_IFINDEX (i4LocalPort),
                                 ((INT1 *) au1NameStr));
        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
        *pu1HeadFlag = VLAN_TRUE;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : VlanCliPrintCtxt                                     */
/*                                                                           */
/* Description        : This function calls the CliPrintf to print           */
/*                      the interface details                                */
/*                                                                           */
/* INPUT              : CliHandle, , u4ContextId, pu1HeadFlag                */
/*                                                                           */
/* OUTPUT             : None                                                 */
/*                                                                           */
/* Return Value(s)    : Void                                                 */
/*****************************************************************************/

VOID
VlanCliPrintCtxt (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag)
{
    UINT1               au1ContextName[VLAN_SWITCH_ALIAS_LEN];

    if (*pu1HeadFlag == VLAN_FALSE)
    {
        VLAN_MEMSET (au1ContextName, 0, VLAN_SWITCH_ALIAS_LEN);
        VlanVcmGetAliasName (u4ContextId, au1ContextName);

        if (STRLEN (au1ContextName) != 0)
        {
            CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
            *pu1HeadFlag = VLAN_TRUE;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanCliPrintDefVlan                                  */
/*                                                                           */
/* Description        : This function calls the CliPrintf to print           */
/*                      the interface details                                */
/*                                                                           */
/* INPUT              : CliHandle, , u4ContextId, pu1isActiveVlan,pu1HeadFlag  */
/*                                                                           */
/* OUTPUT             :   None                                               */
/*                                                                           */
/* Return Value(s)    : Void                                                 */
/*****************************************************************************/

VOID
VlanCliPrintDefVlan (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT4 u4VlanIndex, UINT1 *pu1HeadFlag,
                     UINT1 *pu1isActiveVlan)
{
    if (u4VlanIndex == VLAN_DEF_VLAN_ID)
    {
        VlanCliPrintCtxt (CliHandle, u4ContextId, pu1HeadFlag);
        CliPrintf (CliHandle, "vlan %d\r\n", u4VlanIndex);
        *pu1isActiveVlan = VLAN_TRUE;
    }
    return;
}
#endif
