/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: fsmid1wr.c,v 1.5 2016/07/16 11:15:04 siva Exp $
* Description: This header file contains all prototype of the wrapper
*              routines in VLAN Modules.
****************************************************************************/

# include  "lr.h" 
# include  "fssnmp.h"
# include  "vlaninc.h"
# include  "fsmid1db.h"

INT4 GetNextIndexFsMIEvbSystemTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIEvbSystemTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIEvbSystemTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterFSMID1 ()
{
	SNMPRegisterMibWithLock (&fsmid1OID, &fsmid1Entry, VlanLock, VlanUnLock,
                             SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&fsmid1OID, (const UINT1 *) "fsmid1evb");
}



VOID UnRegisterFSMID1 ()
{
	SNMPUnRegisterMib (&fsmid1OID, &fsmid1Entry);
	SNMPDelSysorEntry (&fsmid1OID, (const UINT1 *) "fsmid1evb");
}

INT4 FsMIEvbSystemControlGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSystemControl(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbSystemModuleStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSystemModuleStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbSystemTraceLevelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSystemTraceLevel(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbSystemTrapStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSystemTrapStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbSystemStatsClearGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSystemStatsClear(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbSchannelIdModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSchannelIdMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbSystemRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbSystemTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSystemRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbSystemControlSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSystemControl(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemModuleStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSystemModuleStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemTraceLevelSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSystemTraceLevel(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMIEvbSystemTrapStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSystemTrapStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemStatsClearSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSystemStatsClear(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSchannelIdModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSchannelIdMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSystemRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemControlTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSystemControl(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemModuleStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSystemModuleStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemTraceLevelTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSystemTraceLevel(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMIEvbSystemTrapStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSystemTrapStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemStatsClearTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSystemStatsClear(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSchannelIdModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSchannelIdMode(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSystemRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSystemTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIEvbSystemTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMIEvbCAPConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIEvbCAPConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIEvbCAPConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIEvbCAPSChannelIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbCAPSChannelID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbCAPSChannelIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbCAPSChannelIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbCAPSChNegoStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbCAPSChNegoStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbPhyPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMIEvbPhyPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbSchIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMIEvbSchID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbSChannelFilterStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbCAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSChannelFilterStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 FsMIEvbCAPSChannelIDSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbCAPSChannelID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMIEvbCAPSChannelIfIndexSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbCAPSChannelIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSChannelFilterStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbSChannelFilterStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbCAPSChannelIDTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbCAPSChannelID(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMIEvbCAPSChannelIfIndexTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbCAPSChannelIfIndex(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbSChannelFilterStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbSChannelFilterStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbCAPConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIEvbCAPConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMIEvbUAPConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIEvbUAPConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIEvbUAPConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIEvbUAPSchCdcpModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbUAPSchCdcpMode(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIEvbUAPSchCdcpModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIEvbUAPSchCdcpMode(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbUAPSchCdcpModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIEvbUAPSchCdcpMode(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIEvbUAPConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIEvbUAPConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMIEvbUAPStatsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIEvbUAPStatsTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIEvbUAPStatsTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIEvbTxCdcpCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbTxCdcpCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbRxCdcpCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbRxCdcpCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbSChAllocFailCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSChAllocFailCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbSChActiveFailCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSChActiveFailCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbSVIDPoolExceedsCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbSVIDPoolExceedsCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbCdcpRejectStationReqGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbCdcpRejectStationReq(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIEvbCdcpOtherDropCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIEvbUAPStatsTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIEvbCdcpOtherDropCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}


