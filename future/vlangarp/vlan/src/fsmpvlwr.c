/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmpvlwr.c,v 1.38.22.1 2018/03/15 12:59:56 siva Exp $
 *
 * Description: This file contains wrapper routines used in VLAN module.
 *
 *******************************************************************/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsmpvlwr.h"
# include  "fsmpvldb.h"

VOID
RegisterFSMPVL ()
{
    SNMPRegisterMib (&fsmpvlOID, &fsmpvlEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmpvlOID, (const UINT1 *) "fsmpvlan");
}

VOID
UnRegisterFSMPVL ()
{
    SNMPUnRegisterMib (&fsmpvlOID, &fsmpvlEntry);
    SNMPDelSysorEntry (&fsmpvlOID, (const UINT1 *) "fsmpvlan");
}

INT4
FsMIDot1qFutureVlanGlobalTraceGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetFsMIDot1qFutureVlanGlobalTrace
        (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalTraceSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = (INT4) nmhSetFsMIDot1qFutureVlanGlobalTrace
        (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanGlobalTrace
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalTraceDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanGlobalTrace
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanGlobalsTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanGlobalsTable
            (&i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanGlobalsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureBaseBridgeModeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        (nmhGetFsMIDot1qFutureBaseBridgeMode
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanSubnetBasedOnAllPortsGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetFsMIDot1qFutureVlanSubnetBasedOnAllPorts
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanGlobalMacLearningStatusGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanGlobalMacLearningStatus (pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          &
                                                          (pMultiData->
                                                           i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaGet (tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanGlobalsFdbFlushGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanGlobalsFdbFlush (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanUserDefinedTPIDGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanUserDefinedTPID (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanRemoteFdbFlushGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanRemoteFdbFlush (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanMacBasedOnAllPortsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanMacBasedOnAllPorts (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortProtoBasedOnAllPortsGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts (pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           &
                                                           (pMultiData->
                                                            i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanShutdownStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanShutdownStatus (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpShutdownStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

#ifdef GARP_WANTED

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsMIDot1qFutureGarpShutdownStatus (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanDebug (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanLearningModeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanLearningMode (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanHybridTypeDefaultGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanHybridTypeDefault (pMultiIndex->
                                                    pIndex[0].i4_SLongValue,
                                                    &
                                                    (pMultiData->
                                                     i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanOperStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanOperStatus (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGvrpOperStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

#ifdef GARP_WANTED

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsMIDot1qFutureGvrpOperStatus (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureGmrpOperStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

#ifdef GARP_WANTED

    GARP_LOCK ();
    i4RetVal =
        nmhGetFsMIDot1qFutureGmrpOperStatus (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanContextNameGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanContextName (pMultiIndex->
                                              pIndex[0].i4_SLongValue,
                                              pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        nmhGetFsMIDot1qFutureGarpDebug (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureUnicastMacLearningLimitGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureUnicastMacLearningLimit (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      &
                                                      (pMultiData->
                                                       u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureBaseBridgeModeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (INT4) (nmhSetFsMIDot1qFutureBaseBridgeMode
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanSubnetBasedOnAllPortsSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetFsMIDot1qFutureVlanSubnetBasedOnAllPorts
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalMacLearningStatusSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (nmhSetFsMIDot1qFutureVlanGlobalMacLearningStatus
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaSet (tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalsFdbFlushSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanGlobalsFdbFlush (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanUserDefinedTPIDSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanUserDefinedTPID (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanRemoteFdbFlushSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMIDot1qFutureVlanRemoteFdbFlush
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIDot1qFutureVlanMacBasedOnAllPortsSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanMacBasedOnAllPorts (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortProtoBasedOnAllPortsSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts (pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanShutdownStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanShutdownStatus (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpShutdownStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureGarpShutdownStatus (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanDebug (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanLearningModeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanLearningMode (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanHybridTypeDefaultSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanHybridTypeDefault (pMultiIndex->
                                                    pIndex[0].i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureGarpDebug (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureUnicastMacLearningLimitSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureUnicastMacLearningLimit (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanStatus (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[0].i4_SLongValue,
                                                   pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureBaseBridgeModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureBaseBridgeMode (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanSubnetBasedOnAllPortsTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanSubnetBasedOnAllPorts (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanGlobalMacLearningStatusTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (nmhTestv2FsMIDot1qFutureVlanGlobalMacLearningStatus (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaTest (UINT4 *pu4Error,
                                                       tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalsFdbFlushTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanGlobalsFdbFlush (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanUserDefinedTPIDTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanUserDefinedTPID (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanRemoteFdbFlushTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIDot1qFutureVlanRemoteFdbFlush (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsMIDot1qFutureVlanMacBasedOnAllPortsTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanMacBasedOnAllPorts (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortProtoBasedOnAllPortsTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanPortProtoBasedOnAllPorts (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     i4_SLongValue,
                                                                     pMultiData->
                                                                     i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanShutdownStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanShutdownStatus (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpShutdownStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureGarpShutdownStatus (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanDebug (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanLearningModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanLearningMode (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanHybridTypeDefaultTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanHybridTypeDefault (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpDebugTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureGarpDebug (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureUnicastMacLearningLimitTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureUnicastMacLearningLimit (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiData->
                                                                u4_ULongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanGlobalsTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanGlobalsTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanPortTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsMIDot1qPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanPortTable (&i4FsMIDot1qPort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsMIDot1qPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsMIDot1qPort;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanPortTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortProtectedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIDot1qFutureVlanPortProtected
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetBasedClassificationGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetFsMIDot1qFutureVlanPortSubnetBasedClassification
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortUnicastMacLearningGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetFsMIDot1qFutureVlanPortUnicastMacLearning
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpJoinEmptyTxCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpJoinEmptyRxCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpJoinInTxCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpJoinInTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpJoinInRxCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpJoinInRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpLeaveInTxCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpLeaveInRxCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCountGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCountGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpEmptyTxCountGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpEmptyTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpEmptyRxCountGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpEmptyRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpLeaveAllTxCountGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpLeaveAllRxCountGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGmrpDiscardCountGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGmrpDiscardCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpJoinEmptyTxCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpJoinEmptyRxCountGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpJoinInTxCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpJoinInTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpJoinInRxCountGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpJoinInRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpLeaveInTxCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpLeaveInRxCountGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCountGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCountGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpEmptyTxCountGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpEmptyTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpEmptyRxCountGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpEmptyRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpLeaveAllTxCountGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpLeaveAllRxCountGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortGvrpDiscardCountGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortGvrpDiscardCount
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortFdbFlushGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortFdbFlush (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortIngressEtherTypeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortIngressEtherType (pMultiIndex->
                                                       pIndex[0].i4_SLongValue,
                                                       &
                                                       (pMultiData->
                                                        i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortEgressEtherTypeGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortEgressEtherType (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      &
                                                      (pMultiData->
                                                       i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortEgressTPIDTypeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortEgressTPIDType (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID1Get (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortAllowableTPID1 (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID2Get (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortAllowableTPID2 (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID3Get (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortAllowableTPID3 (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortClearGarpStatsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortClearGarpStats
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortMacBasedClassificationGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortMacBasedClassification (pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             &
                                                             (pMultiData->
                                                              i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortPortProtoBasedClassificationGet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortPortProtoBasedClassification
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanFilteringUtilityCriteriaGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIDot1qFutureVlanFilteringUtilityCriteria
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFuturePortPacketReflectionStatusGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal = (INT4) nmhGetFsMIDot1qFuturePortPacketReflectionStatus
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortProtectedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4) nmhSetFsMIDot1qFutureVlanPortProtected
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetBasedClassificationSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetFsMIDot1qFutureVlanPortSubnetBasedClassification
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortUnicastMacLearningSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhSetFsMIDot1qFutureVlanPortUnicastMacLearning
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortFdbFlushSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortFdbFlush (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortIngressEtherTypeSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortIngressEtherType (pMultiIndex->
                                                       pIndex[0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortEgressEtherTypeSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortEgressEtherType (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortEgressTPIDTypeSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortEgressTPIDType (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID1Set (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortAllowableTPID1 (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID2Set (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortAllowableTPID2 (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID3Set (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortAllowableTPID3 (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortClearGarpStatsSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{

    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        nmhSetFsMIDot1qFutureVlanPortClearGarpStats (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortMacBasedClassificationSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortMacBasedClassification (pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             pMultiData->
                                                             i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortPortProtoBasedClassificationSet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortPortProtoBasedClassification
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanFilteringUtilityCriteriaSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4) nmhSetFsMIDot1qFutureVlanFilteringUtilityCriteria
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFuturePortPacketReflectionStatusSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4) nmhSetFsMIDot1qFuturePortPacketReflectionStatus
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanPortType (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortProtectedTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanPortProtected (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetBasedClassificationTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (nmhTestv2FsMIDot1qFutureVlanPortSubnetBasedClassification
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortUnicastMacLearningTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortUnicastMacLearning (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortFdbFlushTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortFdbFlush (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortIngressEtherTypeTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortIngressEtherType (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortEgressEtherTypeTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortEgressEtherType (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortEgressTPIDTypeTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortEgressTPIDType (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID1Test (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID1 (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID2Test (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID2 (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortAllowableTPID3Test (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID3 (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortClearGarpStatsTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanPortClearGarpStats (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue);
    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacBasedClassificationTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanPortMacBasedClassification (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       i4_SLongValue,
                                                                       pMultiData->
                                                                       i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortPortProtoBasedClassificationTest (UINT4 *pu4Error,
                                                         tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortPortProtoBasedClassification (pu4Error,
                                                                      pMultiIndex->
                                                                      pIndex[0].
                                                                      i4_SLongValue,
                                                                      pMultiData->
                                                                      i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanFilteringUtilityCriteriaTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanFilteringUtilityCriteria
        (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFuturePortPacketReflectionStatusTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (INT4) nmhTestv2FsMIDot1qFuturePortPacketReflectionStatus (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanPortTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanPortMacMapTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanPortMacMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanPortMacMapVidGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortMacMapVid (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                (*(tMacAddr *)
                                                 pMultiIndex->pIndex[1].
                                                 pOctetStrValue->pu1_OctetList),
                                                &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapNameGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortMacMapName (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 (*(tMacAddr *)
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue->
                                                  pu1_OctetList),
                                                 pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapMcastBcastOptionGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortMacMapMcastBcastOption (pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             (*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              pOctetStrValue->
                                                              pu1_OctetList),
                                                             &(pMultiData->
                                                               i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanPortMacMapRowStatus (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->
                                                       pIndex
                                                       [1].pOctetStrValue->
                                                       pu1_OctetList),
                                                      &(pMultiData->
                                                        i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapVidSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortMacMapVid (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                (*(tMacAddr *)
                                                 pMultiIndex->pIndex[1].
                                                 pOctetStrValue->pu1_OctetList),
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapNameSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortMacMapName (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 (*(tMacAddr *)
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue->
                                                  pu1_OctetList),
                                                 pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapMcastBcastOptionSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortMacMapMcastBcastOption (pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             (*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              pOctetStrValue->
                                                              pu1_OctetList),
                                                             pMultiData->
                                                             i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanPortMacMapRowStatus (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->
                                                       pIndex
                                                       [1].pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortMacMapVid (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[0].i4_SLongValue,
                                                   (*(tMacAddr *)
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue->
                                                    pu1_OctetList),
                                                   pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapNameTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortMacMapName (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[0].i4_SLongValue,
                                                    (*(tMacAddr *)
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue->
                                                     pu1_OctetList),
                                                    pMultiData->pOctetStrValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapMcastBcastOptionTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortMacMapMcastBcastOption (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                (*(tMacAddr *)
                                                                 pMultiIndex->
                                                                 pIndex[1].
                                                                 pOctetStrValue->
                                                                 pu1_OctetList),
                                                                pMultiData->
                                                                i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapRowStatusTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanPortMacMapRowStatus (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         (*(tMacAddr *)
                                                          pMultiIndex->pIndex
                                                          [1].pOctetStrValue->
                                                          pu1_OctetList),
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortMacMapTableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanPortMacMapTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanFidMapTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanFidMapTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanFidMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanFidMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIDot1qFutureVlanFidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanFidMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanFid (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanFidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanFid (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanFidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanFid (pu4Error,
                                                pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                pMultiIndex->
                                                pIndex[1].u4_ULongValue,
                                                pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanFidMapTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanFidMapTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanTunnelConfigTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanTunnelConfigTable
            (&i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanTunnelConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanBridgeModeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanBridgeMode (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelBpduPriGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelBpduPri (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanBridgeModeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanBridgeMode (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelBpduPriSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanTunnelBpduPri (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanBridgeModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanBridgeMode (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelBpduPriTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanTunnelBpduPri (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelConfigTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanTunnelConfigTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanTunnelTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsMIDot1qPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanTunnelTable (&i4FsMIDot1qPort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanTunnelTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsMIDot1qPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsMIDot1qPort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanTunnelStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelStatus (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanTunnelStatus (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanTunnelStatus (pu4Error,
                                                         pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanTunnelTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable (tSnmpIndex *
                                                    pFirstMultiIndex,
                                                    tSnmpIndex *
                                                    pNextMultiIndex)
{
    INT4                i4FsMIDot1qPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanTunnelProtocolTable
            (&i4FsMIDot1qPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsMIDot1qPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsMIDot1qPort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanTunnelStpPDUsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelStpPDUs (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelStpPDUsRecvdGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelStpPDUsRecvd (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelStpPDUsSentGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelStpPDUsSent (pMultiIndex->
                                                    pIndex[0].i4_SLongValue,
                                                    &
                                                    (pMultiData->
                                                     u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelGvrpPDUsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUs (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelGvrpPDUsRecvdGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsRecvd (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      &
                                                      (pMultiData->
                                                       u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelGvrpPDUsSentGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsSent (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelIgmpPktsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelIgmpPkts (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelIgmpPktsRecvdGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsRecvd (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      &
                                                      (pMultiData->
                                                       u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelIgmpPktsSentGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsSent (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     &
                                                     (pMultiData->
                                                      u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelStpPDUsSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanTunnelStpPDUs (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelGvrpPDUsSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanTunnelGvrpPDUs (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelIgmpPktsSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanTunnelIgmpPkts (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelStpPDUsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanTunnelStpPDUs (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanTunnelGvrpPDUsTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanTunnelGvrpPDUs (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelIgmpPktsTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanTunnelIgmpPkts (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanTunnelProtocolTableDep (UINT4 *pu4Error,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanTunnelProtocolTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanCounterTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanCounterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanCounterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanCounterRxUcastGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanCounterRxUcast (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiIndex->
                                                 pIndex[1].u4_ULongValue,
                                                 &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterRxMcastBcastGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanCounterRxMcastBcast (pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      pMultiIndex->
                                                      pIndex[1].u4_ULongValue,
                                                      &
                                                      (pMultiData->
                                                       u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterTxUnknUcastGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanCounterTxUnknUcast (pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiIndex->
                                                     pIndex[1].u4_ULongValue,
                                                     &
                                                     (pMultiData->
                                                      u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterTxUcastGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanCounterTxUcast (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiIndex->
                                                 pIndex[1].u4_ULongValue,
                                                 &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanCounterTxBcastGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanCounterTxBcast (pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiIndex->
                                                 pIndex[1].u4_ULongValue,
                                                 &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanCounterStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureVlanCounterStatus (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                pMultiIndex->
                                                pIndex[1].u4_ULongValue,
                                                &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanCounterStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanCounterStatus (pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                pMultiIndex->
                                                pIndex[1].u4_ULongValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanCounterStatusTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhTestv2FsMIDot1qFutureVlanCounterStatus (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[0].i4_SLongValue,
                                                   pMultiIndex->
                                                   pIndex[1].u4_ULongValue,
                                                   pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanCounterTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanCounterTable (pu4Error,
                                                        pSnmpIndexList,
                                                        pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4                GetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable
    (tSnmpIndex * pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanUnicastMacControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanUnicastMacLimitGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanUnicastMacLimit (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiIndex->
                                                  pIndex[1].u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanAdminMacLearningStatusGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus (pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [1].u4_ULongValue,
                                                         &
                                                         (pMultiData->
                                                          i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanOperMacLearningStatusGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanOperMacLearningStatus (pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiIndex->
                                                        pIndex[1].u4_ULongValue,
                                                        &
                                                        (pMultiData->
                                                         i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanUnicastMacLimitSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanUnicastMacLimit (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiIndex->
                                                  pIndex[1].u4_ULongValue,
                                                  pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanAdminMacLearningStatusSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureVlanAdminMacLearningStatus (pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [1].u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanUnicastMacLimitTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanUnicastMacLimit (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiIndex->pIndex
                                                            [1].u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanAdminMacLearningStatusTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanAdminMacLearningStatus (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiIndex->
                                                                   pIndex[1].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanUnicastMacControlTableDep (UINT4 *pu4Error,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanUnicastMacControlTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpGlobalTraceGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef GARP_WANTED

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsMIDot1qFutureGarpGlobalTrace (&(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpGlobalTraceSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef GARP_WANTED

    GARP_LOCK ();

    i4RetVal = (INT4) nmhSetFsMIDot1qFutureGarpGlobalTrace
        (pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpGlobalTraceTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef GARP_WANTED

    GARP_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureGarpGlobalTrace (pu4Error,
                                                 pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
FsMIDot1qFutureGarpGlobalTraceDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureGarpGlobalTrace
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif
    return i4RetVal;

}

INT4
GetNextIndexFsMIDot1qFutureVlanTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanTpFdbTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanTpFdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanOldTpFdbPortGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIDot1qFutureVlanOldTpFdbPort
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureConnectionIdentifierGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pMultiData->pOctetStrValue->i4_Length = 6;

    i4RetVal = nmhGetFsMIDot1qFutureConnectionIdentifier
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4                GetNextIndexFsMIDot1qFutureVlanWildCardTable
    (tSnmpIndex * pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanWildCardTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4                FsMIDot1qFutureVlanWildCardRowStatusGet
    (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList))
        == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal = (nmhGetFsMIDot1qFutureVlanWildCardRowStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 (*(tMacAddr *) pMultiIndex->pIndex[1].
                  pOctetStrValue->pu1_OctetList),
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanWildCardRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4) nmhSetFsMIDot1qFutureVlanWildCardRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanWildCardRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanWildCardRowStatus
                (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
                 (*(tMacAddr *) pMultiIndex->pIndex[1].
                  pOctetStrValue->pu1_OctetList), pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanWildCardTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanWildCardTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4                GetNextIndexFsMIDot1qFutureVlanWildCardPortTable
    (tSnmpIndex * pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanWildCardPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanWildCardPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanIsWildCardEgressPortGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal = (nmhGetFsMIDot1qFutureVlanIsWildCardEgressPort
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 (*(tMacAddr *) pMultiIndex->pIndex[1].
                  pOctetStrValue->pu1_OctetList),
                 pMultiIndex->pIndex[2].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanIsWildCardEgressPortSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4) (nmhSetFsMIDot1qFutureVlanIsWildCardEgressPort
                       (pMultiIndex->pIndex[0].i4_SLongValue,
                        (*(tMacAddr *) pMultiIndex->pIndex[1].
                         pOctetStrValue->pu1_OctetList),
                        pMultiIndex->pIndex[2].i4_SLongValue,
                        pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanIsWildCardEgressPortTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanIsWildCardEgressPort
                (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
                 (*(tMacAddr *) pMultiIndex->pIndex[1].
                  pOctetStrValue->pu1_OctetList),
                 pMultiIndex->pIndex[2].i4_SLongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanWildCardPortTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureVlanWildCardPortTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureStaticUnicastExtnTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureStaticUnicastExtnTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].
             pOctetStrValue->pu1_OctetList,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureStaticUnicastExtnTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureStaticConnectionIdentifierGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureStaticUnicastExtnTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pMultiData->pOctetStrValue->i4_Length = 6;
    i4RetVal =
        nmhGetFsMIDot1qFutureStaticConnectionIdentifier (pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [1].u4_ULongValue,
                                                         (*(tMacAddr *)
                                                          pMultiIndex->pIndex
                                                          [2].pOctetStrValue->
                                                          pu1_OctetList),
                                                         pMultiIndex->pIndex[3].
                                                         i4_SLongValue,
                                                         (tMacAddr *)
                                                         pMultiData->
                                                         pOctetStrValue->
                                                         pu1_OctetList);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStaticConnectionIdentifierSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (INT4)
        nmhSetFsMIDot1qFutureStaticConnectionIdentifier (pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->
                                                         pIndex
                                                         [1].u4_ULongValue,
                                                         (*(tMacAddr *)
                                                          pMultiIndex->pIndex
                                                          [2].pOctetStrValue->
                                                          pu1_OctetList),
                                                         pMultiIndex->pIndex[3].
                                                         i4_SLongValue,
                                                         (*(tMacAddr *)
                                                          pMultiData->
                                                          pOctetStrValue->
                                                          pu1_OctetList));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStaticConnectionIdentifierTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhTestv2FsMIDot1qFutureStaticConnectionIdentifier (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiIndex->
                                                                   pIndex[1].
                                                                   u4_ULongValue,
                                                                   (*
                                                                    (tMacAddr *)
                                                                    pMultiIndex->
                                                                    pIndex[2].
                                                                    pOctetStrValue->
                                                                    pu1_OctetList),
                                                                   pMultiIndex->
                                                                   pIndex[3].
                                                                   i4_SLongValue,
                                                                   (*
                                                                    (tMacAddr *)
                                                                    pMultiData->
                                                                    pOctetStrValue->
                                                                    pu1_OctetList));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureStaticUnicastExtnTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsMIDot1qFutureStaticUnicastExtnTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapVidGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetFsMIDot1qFutureVlanPortSubnetMapVid
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapARPOptionGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetFsMIDot1qFutureVlanPortSubnetMapARPOption
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetFsMIDot1qFutureVlanPortSubnetMapRowStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapVidSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetFsMIDot1qFutureVlanPortSubnetMapVid
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapARPOptionSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetFsMIDot1qFutureVlanPortSubnetMapARPOption
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetFsMIDot1qFutureVlanPortSubnetMapRowStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapVidTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortSubnetMapVid (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapARPOptionTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortSubnetMapARPOption (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapRowStatusTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortSubnetMapRowStatus (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanPortSubnetMapTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhDepv2FsMIDot1qFutureVlanPortSubnetMapTable
                (pu4Error, pSnmpIndexList, pSnmpvarbinds));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureStVlanExtTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureStVlanExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureStVlanExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanCounterRxFramesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal = 0;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanCounterRxFrames
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterRxBytesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = 0;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanCounterRxBytes
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterTxFramesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal = 0;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanCounterTxFrames
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterTxBytesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = 0;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanCounterTxBytes
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanCounterDiscardFramesGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = 0;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanCounterDiscardFrames
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanCounterDiscardBytesGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal = 0;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanCounterDiscardBytes
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanFdbFlushGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureStVlanFdbFlush (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiIndex->
                                             pIndex[1].u4_ULongValue,
                                             &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanEgressEthertypeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureStVlanEgressEthertype (pMultiIndex->
                                                    pIndex[0].i4_SLongValue,
                                                    pMultiIndex->
                                                    pIndex[1].u4_ULongValue,
                                                    &
                                                    (pMultiData->
                                                     i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanFdbFlushSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureStVlanFdbFlush (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiIndex->
                                             pIndex[1].u4_ULongValue,
                                             pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanEgressEthertypeSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFutureStVlanEgressEthertype (pMultiIndex->
                                                    pIndex[0].i4_SLongValue,
                                                    pMultiIndex->
                                                    pIndex[1].u4_ULongValue,
                                                    pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanFdbFlushTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureStVlanFdbFlush (pu4Error,
                                                pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                pMultiIndex->
                                                pIndex[1].u4_ULongValue,
                                                pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanEgressEthertypeTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFutureStVlanEgressEthertype (pu4Error,
                                                       pMultiIndex->
                                                       pIndex[0].i4_SLongValue,
                                                       pMultiIndex->
                                                       pIndex[1].u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanExtTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = 0;

    VLAN_LOCK ();

    i4RetVal = (nmhDepv2FsMIDot1qFutureStVlanExtTable (pu4Error,
                                                       pSnmpIndexList,
                                                       pSnmpvarbinds));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanSwStatsEnabledGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanSwStatsEnabled (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanSwStatsEnabledSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsMIDot1qFutureVlanSwStatsEnabled (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanSwStatsEnabledTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2FsMIDot1qFutureVlanSwStatsEnabled (pu4Error,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanSwStatsEnabledDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhDepv2FsMIDot1qFutureVlanSwStatsEnabled (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4                GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
    (tSnmpIndex * pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtVidGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortSubnetMapExtVid
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtARPOptionGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortSubnetMapExtARPOption
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtRowStatusGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtVidSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetFsMIDot1qFutureVlanPortSubnetMapExtVid
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtARPOptionSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetFsMIDot1qFutureVlanPortSubnetMapExtARPOption
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtRowStatusSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanPortSubnetMapExtVidTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtVid (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 i4_SLongValue,
                                                                 pMultiIndex->
                                                                 pIndex[1].
                                                                 u4_ULongValue,
                                                                 pMultiIndex->
                                                                 pIndex[2].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4                FsMIDot1qFutureVlanPortSubnetMapExtARPOptionTest
    (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtARPOption (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       i4_SLongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [1].
                                                                       u4_ULongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [2].
                                                                       u4_ULongValue,
                                                                       pMultiData->
                                                                       i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4                FsMIDot1qFutureVlanPortSubnetMapExtRowStatusTest
    (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtRowStatus (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       i4_SLongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [1].
                                                                       u4_ULongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [2].
                                                                       u4_ULongValue,
                                                                       pMultiData->
                                                                       i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4                FsMIDot1qFutureVlanPortSubnetMapExtTableDep
    (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhDepv2FsMIDot1qFutureVlanPortSubnetMapExtTable
                (pu4Error, pSnmpIndexList, pSnmpvarbinds));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFuturePortVlanExtTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFuturePortVlanExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFuturePortVlanExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFuturePortVlanFdbFlushGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsMIDot1qFuturePortVlanExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFuturePortVlanFdbFlush (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiIndex->
                                               pIndex[1].u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[2].i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFuturePortVlanFdbFlushSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetFsMIDot1qFuturePortVlanFdbFlush (pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiIndex->
                                               pIndex[1].u4_ULongValue,
                                               pMultiIndex->
                                               pIndex[2].i4_SLongValue,
                                               pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFuturePortVlanFdbFlushTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2FsMIDot1qFuturePortVlanFdbFlush (pu4Error,
                                                  pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  pMultiIndex->
                                                  pIndex[1].u4_ULongValue,
                                                  pMultiIndex->
                                                  pIndex[2].i4_SLongValue,
                                                  pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIDot1qFuturePortVlanExtTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();
    i4RetVal =
        nmhDepv2FsMIDot1qFuturePortVlanExtTable (pu4Error,
                                                 pSnmpIndexList, pSnmpvarbinds);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIDot1qFutureVlanLoopbackTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIDot1qFutureVlanLoopbackTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIDot1qFutureVlanLoopbackTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsMIDot1qFutureVlanLoopbackStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureVlanLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetFsMIDot1qFutureVlanLoopbackStatus (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureVlanLoopbackStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsMIDot1qFutureVlanLoopbackStatus (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanLoopbackStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsMIDot1qFutureVlanLoopbackStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureVlanLoopbackTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    i4RetVal =
        nmhDepv2FsMIDot1qFutureVlanLoopbackTable (pu4Error, pSnmpIndexList,
                                                  pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMIDot1qFutureStVlanTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureStVlanType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsMIDot1qFutureStVlanVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIDot1qFutureStVlanVid (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}
