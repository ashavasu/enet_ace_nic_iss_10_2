/* $Id: fsvlannc.c,v 1.1 2016/06/21 09:54:51 siva Exp $
   ISS Wrapper module
   module ARICENT-VLAN-MIB

*/
#include "lr.h"
#include "vlaninc.h"
#include "fssnmp.h"
#include "fsvlanlw.h"
#include "fsvlannc.h"
/********************************************************************
 * FUNCTION NcDot1qFutureVlanStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanStatusSet (
				INT4 i4Dot1qFutureVlanStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanStatus(
						i4Dot1qFutureVlanStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanStatus(pu4Error,
						i4Dot1qFutureVlanStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanMacBasedOnAllPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanMacBasedOnAllPortsSet (
				INT4 i4Dot1qFutureVlanMacBasedOnAllPorts )
{

		INT1 i1RetVal;
		VLAN_LOCK ();

		i1RetVal = nmhSetDot1qFutureVlanMacBasedOnAllPorts(
						i4Dot1qFutureVlanMacBasedOnAllPorts);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanMacBasedOnAllPortsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanMacBasedOnAllPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanMacBasedOnAllPortsTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanMacBasedOnAllPorts )
{

		INT1 i1RetVal;
		VLAN_LOCK ();

		i1RetVal = nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts(pu4Error,
						i4Dot1qFutureVlanMacBasedOnAllPorts);
		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanMacBasedOnAllPortsTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortProtoBasedOnAllPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortProtoBasedOnAllPortsSet (
				INT4 i4Dot1qFutureVlanPortProtoBasedOnAllPorts )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts(
						i4Dot1qFutureVlanPortProtoBasedOnAllPorts);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortProtoBasedOnAllPortsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortProtoBasedOnAllPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortProtoBasedOnAllPortsTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPortProtoBasedOnAllPorts )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts(pu4Error,
						i4Dot1qFutureVlanPortProtoBasedOnAllPorts);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortProtoBasedOnAllPortsTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanShutdownStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanShutdownStatusSet (
				INT4 i4Dot1qFutureVlanShutdownStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanShutdownStatus(
						i4Dot1qFutureVlanShutdownStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanShutdownStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanShutdownStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanShutdownStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanShutdownStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanShutdownStatus(pu4Error,
						i4Dot1qFutureVlanShutdownStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanShutdownStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureGarpShutdownStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureGarpShutdownStatusSet (
				INT4 i4Dot1qFutureGarpShutdownStatus )
{

		INT1 i1RetVal;

		GARP_LOCK ();
		i1RetVal = nmhSetDot1qFutureGarpShutdownStatus(
						i4Dot1qFutureGarpShutdownStatus);
		GARP_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureGarpShutdownStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureGarpShutdownStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureGarpShutdownStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureGarpShutdownStatus )
{

		INT1 i1RetVal;

		GARP_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureGarpShutdownStatus(pu4Error,
						i4Dot1qFutureGarpShutdownStatus);

		GARP_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureGarpShutdownStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanDebugSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanDebugSet (
				INT4 i4Dot1qFutureVlanDebug )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanDebug(
						i4Dot1qFutureVlanDebug);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanDebugSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanDebugTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanDebugTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanDebug )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanDebug(pu4Error,
						i4Dot1qFutureVlanDebug);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanDebugTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanLearningModeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanLearningModeSet (
				INT4 i4Dot1qFutureVlanLearningMode )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanLearningMode(
						i4Dot1qFutureVlanLearningMode);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanLearningModeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanLearningModeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanLearningModeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanLearningMode )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanLearningMode(pu4Error,
						i4Dot1qFutureVlanLearningMode);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanLearningModeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanHybridTypeDefaultSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanHybridTypeDefaultSet (
				INT4 i4Dot1qFutureVlanHybridTypeDefault )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanHybridTypeDefault(
						i4Dot1qFutureVlanHybridTypeDefault);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanHybridTypeDefaultSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanHybridTypeDefaultTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanHybridTypeDefaultTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanHybridTypeDefault )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanHybridTypeDefault(pu4Error,
						i4Dot1qFutureVlanHybridTypeDefault);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanHybridTypeDefaultTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanOperStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanOperStatusGet (
				INT4 *pi4Dot1qFutureVlanOperStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhGetDot1qFutureVlanOperStatus(
						pi4Dot1qFutureVlanOperStatus );
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanOperStatusGet */

/********************************************************************
 * FUNCTION NcDot1qFutureGvrpOperStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureGvrpOperStatusGet (
				INT4 *pi4Dot1qFutureGvrpOperStatus )
{

		INT1 i1RetVal;
		GARP_LOCK ();
		i1RetVal = nmhGetDot1qFutureGvrpOperStatus(
						pi4Dot1qFutureGvrpOperStatus );

		GARP_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureGvrpOperStatusGet */

/********************************************************************
 * FUNCTION NcDot1qFutureGmrpOperStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureGmrpOperStatusGet (
				INT4 *pi4Dot1qFutureGmrpOperStatus )
{

		INT1 i1RetVal;
		GARP_LOCK ();
		i1RetVal = nmhGetDot1qFutureGmrpOperStatus(
						pi4Dot1qFutureGmrpOperStatus );

		GARP_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureGmrpOperStatusGet */

/********************************************************************
 * FUNCTION NcDot1qFutureGarpDebugSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureGarpDebugSet (
				INT4 i4Dot1qFutureGarpDebug )
{

		INT1 i1RetVal;

		GARP_LOCK ();
		i1RetVal = nmhSetDot1qFutureGarpDebug(
						i4Dot1qFutureGarpDebug);

		GARP_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureGarpDebugSet */

/********************************************************************
 * FUNCTION NcDot1qFutureGarpDebugTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureGarpDebugTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureGarpDebug )
{

		INT1 i1RetVal;

		GARP_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureGarpDebug(pu4Error,
						i4Dot1qFutureGarpDebug);

		GARP_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureGarpDebugTest */

/********************************************************************
 * FUNCTION NcDot1qFutureUnicastMacLearningLimitSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureUnicastMacLearningLimitSet (
				UINT4 u4Dot1qFutureUnicastMacLearningLimit )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureUnicastMacLearningLimit(
						u4Dot1qFutureUnicastMacLearningLimit);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureUnicastMacLearningLimitSet */

/********************************************************************
 * FUNCTION NcDot1qFutureUnicastMacLearningLimitTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureUnicastMacLearningLimitTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureUnicastMacLearningLimit )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureUnicastMacLearningLimit(pu4Error,
						u4Dot1qFutureUnicastMacLearningLimit);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureUnicastMacLearningLimitTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanBaseBridgeModeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanBaseBridgeModeSet (
				INT4 i4Dot1qFutureVlanBaseBridgeMode )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanBaseBridgeMode(
						i4Dot1qFutureVlanBaseBridgeMode);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanBaseBridgeModeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanBaseBridgeModeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanBaseBridgeModeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanBaseBridgeMode )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanBaseBridgeMode(pu4Error,
						i4Dot1qFutureVlanBaseBridgeMode);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanBaseBridgeModeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanSubnetBasedOnAllPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanSubnetBasedOnAllPortsSet (
				INT4 i4Dot1qFutureVlanSubnetBasedOnAllPorts )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanSubnetBasedOnAllPorts(
						i4Dot1qFutureVlanSubnetBasedOnAllPorts);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanSubnetBasedOnAllPortsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanSubnetBasedOnAllPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanSubnetBasedOnAllPortsTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanSubnetBasedOnAllPorts )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts(pu4Error,
						i4Dot1qFutureVlanSubnetBasedOnAllPorts);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanSubnetBasedOnAllPortsTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanGlobalMacLearningStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanGlobalMacLearningStatusSet (
				INT4 i4Dot1qFutureVlanGlobalMacLearningStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanGlobalMacLearningStatus(
						i4Dot1qFutureVlanGlobalMacLearningStatus);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanGlobalMacLearningStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanGlobalMacLearningStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanGlobalMacLearningStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanGlobalMacLearningStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus(pu4Error,
						i4Dot1qFutureVlanGlobalMacLearningStatus);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanGlobalMacLearningStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanApplyEnhancedFilteringCriteriaSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanApplyEnhancedFilteringCriteriaSet (
				INT4 i4Dot1qFutureVlanApplyEnhancedFilteringCriteria )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria(
						i4Dot1qFutureVlanApplyEnhancedFilteringCriteria);
		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanApplyEnhancedFilteringCriteriaSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanApplyEnhancedFilteringCriteriaTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanApplyEnhancedFilteringCriteriaTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanApplyEnhancedFilteringCriteria )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria(pu4Error,
						i4Dot1qFutureVlanApplyEnhancedFilteringCriteria);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanApplyEnhancedFilteringCriteriaTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanSwStatsEnabledSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanSwStatsEnabledSet (
				INT4 i4Dot1qFutureVlanSwStatsEnabled )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanSwStatsEnabled(
						i4Dot1qFutureVlanSwStatsEnabled);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanSwStatsEnabledSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanSwStatsEnabledTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanSwStatsEnabledTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanSwStatsEnabled )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanSwStatsEnabled(pu4Error,
						i4Dot1qFutureVlanSwStatsEnabled);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanSwStatsEnabledTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanGlobalsFdbFlushSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanGlobalsFdbFlushSet (
				INT4 i4Dot1qFutureVlanGlobalsFdbFlush )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanGlobalsFdbFlush(
						i4Dot1qFutureVlanGlobalsFdbFlush);
		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1qFutureVlanGlobalsFdbFlushSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanGlobalsFdbFlushTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanGlobalsFdbFlushTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanGlobalsFdbFlush )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanGlobalsFdbFlush(pu4Error,
						i4Dot1qFutureVlanGlobalsFdbFlush);
		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1qFutureVlanGlobalsFdbFlushTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanUserDefinedTPIDSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanUserDefinedTPIDSet (
				INT4 i4Dot1qFutureVlanUserDefinedTPID )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanUserDefinedTPID(
						i4Dot1qFutureVlanUserDefinedTPID);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanUserDefinedTPIDSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanUserDefinedTPIDTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanUserDefinedTPIDTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanUserDefinedTPID )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanUserDefinedTPID(pu4Error,
						i4Dot1qFutureVlanUserDefinedTPID);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanUserDefinedTPIDTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanBridgeModeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanBridgeModeSet (
				INT4 i4Dot1qFutureVlanBridgeMode )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanBridgeMode(
						i4Dot1qFutureVlanBridgeMode);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanBridgeModeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanBridgeModeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanBridgeModeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanBridgeMode )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanBridgeMode(pu4Error,
						i4Dot1qFutureVlanBridgeMode);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanBridgeModeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelBpduPriSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelBpduPriSet (
				INT4 i4Dot1qFutureVlanTunnelBpduPri )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanTunnelBpduPri(
						i4Dot1qFutureVlanTunnelBpduPri);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanTunnelBpduPriSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelBpduPriTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelBpduPriTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanTunnelBpduPri )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanTunnelBpduPri(pu4Error,
						i4Dot1qFutureVlanTunnelBpduPri);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanTunnelBpduPriTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortTypeSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortType(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortType);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortTypeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortTypeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortType(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortType);
		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortTypeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacBasedClassificationSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacBasedClassificationSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortMacBasedClassification )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortMacBasedClassification(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortMacBasedClassification);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortMacBasedClassificationSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacBasedClassificationTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacBasedClassificationTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortMacBasedClassification )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortMacBasedClassification(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortMacBasedClassification);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortMacBasedClassificationTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortPortProtoBasedClassificationSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortPortProtoBasedClassificationSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortPortProtoBasedClassification )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortPortProtoBasedClassification(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortPortProtoBasedClassification);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortPortProtoBasedClassificationSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortPortProtoBasedClassificationTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortPortProtoBasedClassificationTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortPortProtoBasedClassification )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortPortProtoBasedClassification);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortPortProtoBasedClassificationTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanFilteringUtilityCriteriaSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanFilteringUtilityCriteriaSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanFilteringUtilityCriteria )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanFilteringUtilityCriteria(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanFilteringUtilityCriteria);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanFilteringUtilityCriteriaSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanFilteringUtilityCriteriaTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanFilteringUtilityCriteriaTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanFilteringUtilityCriteria )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanFilteringUtilityCriteria);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanFilteringUtilityCriteriaTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortProtectedSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortProtectedSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortProtected )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortProtected(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortProtected);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortProtectedSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortProtectedTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortProtectedTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortProtected )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortProtected(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortProtected);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortProtectedTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetBasedClassificationSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetBasedClassificationSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortSubnetBasedClassification )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetBasedClassification(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortSubnetBasedClassification);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetBasedClassificationSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetBasedClassificationTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetBasedClassificationTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortSubnetBasedClassification )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortSubnetBasedClassification);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetBasedClassificationTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortUnicastMacLearningSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortUnicastMacLearningSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortUnicastMacLearning )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortUnicastMacLearning(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortUnicastMacLearning);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortUnicastMacLearningSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortUnicastMacLearningTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortUnicastMacLearningTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortUnicastMacLearning )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortUnicastMacLearning(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortUnicastMacLearning);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortUnicastMacLearningTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortIngressEtherTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortIngressEtherTypeSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortIngressEtherType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortIngressEtherType(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortIngressEtherType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortIngressEtherTypeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortIngressEtherTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortIngressEtherTypeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortIngressEtherType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortIngressEtherType(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortIngressEtherType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortIngressEtherTypeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortEgressEtherTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortEgressEtherTypeSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortEgressEtherType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortEgressEtherType(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortEgressEtherType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortEgressEtherTypeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortEgressEtherTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortEgressEtherTypeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortEgressEtherType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortEgressEtherType(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortEgressEtherType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortEgressEtherTypeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortEgressTPIDTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortEgressTPIDTypeSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortEgressTPIDType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortEgressTPIDType(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortEgressTPIDType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortEgressTPIDTypeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortEgressTPIDTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortEgressTPIDTypeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortEgressTPIDType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortEgressTPIDType(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortEgressTPIDType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortEgressTPIDTypeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortAllowableTPID1Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortAllowableTPID1Set (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortAllowableTPID1 )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortAllowableTPID1(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortAllowableTPID1);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortAllowableTPID1Set */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortAllowableTPID1Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortAllowableTPID1Test (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortAllowableTPID1 )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortAllowableTPID1(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortAllowableTPID1);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortAllowableTPID1Test */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortAllowableTPID2Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortAllowableTPID2Set (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortAllowableTPID2 )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortAllowableTPID2(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortAllowableTPID2);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortAllowableTPID2Set */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortAllowableTPID2Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortAllowableTPID2Test (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortAllowableTPID2 )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortAllowableTPID2(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortAllowableTPID2);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortAllowableTPID2Test */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortAllowableTPID3Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortAllowableTPID3Set (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortAllowableTPID3 )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortAllowableTPID3(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortAllowableTPID3);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortAllowableTPID3Set */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortAllowableTPID3Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortAllowableTPID3Test (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortAllowableTPID3 )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortAllowableTPID3(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortAllowableTPID3);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortAllowableTPID3Test */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortUnicastMacSecTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortUnicastMacSecTypeSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortUnicastMacSecType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortUnicastMacSecType(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortUnicastMacSecType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortUnicastMacSecTypeSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortUnicastMacSecTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortUnicastMacSecTypeTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanPortUnicastMacSecType )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortUnicastMacSecType(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortUnicastMacSecType);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortUnicastMacSecTypeTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapVidSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapVidSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				INT4 i4Dot1qFutureVlanPortMacMapVid )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortMacMapVid(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						i4Dot1qFutureVlanPortMacMapVid);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapVidSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapVidTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapVidTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				INT4 i4Dot1qFutureVlanPortMacMapVid )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortMacMapVid(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						i4Dot1qFutureVlanPortMacMapVid);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapVidTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapNameSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapNameSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				UINT1 *pDot1qFutureVlanPortMacMapName )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE Dot1qFutureVlanPortMacMapName;
		MEMSET (&Dot1qFutureVlanPortMacMapName, 0,  sizeof (Dot1qFutureVlanPortMacMapName));

		Dot1qFutureVlanPortMacMapName.i4_Length = (INT4) STRLEN (pDot1qFutureVlanPortMacMapName);
		Dot1qFutureVlanPortMacMapName.pu1_OctetList = pDot1qFutureVlanPortMacMapName;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortMacMapName(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						&Dot1qFutureVlanPortMacMapName);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapNameSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapNameTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapNameTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				UINT1 *pDot1qFutureVlanPortMacMapName )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE Dot1qFutureVlanPortMacMapName;
		MEMSET (&Dot1qFutureVlanPortMacMapName, 0,  sizeof (Dot1qFutureVlanPortMacMapName));

		Dot1qFutureVlanPortMacMapName.i4_Length = (INT4) STRLEN (pDot1qFutureVlanPortMacMapName);
		Dot1qFutureVlanPortMacMapName.pu1_OctetList = pDot1qFutureVlanPortMacMapName;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortMacMapName(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						&Dot1qFutureVlanPortMacMapName);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapNameTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapMcastBcastOptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapMcastBcastOptionSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				INT4 i4Dot1qFutureVlanPortMacMapMcastBcastOption )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortMacMapMcastBcastOption(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						i4Dot1qFutureVlanPortMacMapMcastBcastOption);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapMcastBcastOptionSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapMcastBcastOptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapMcastBcastOptionTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				INT4 i4Dot1qFutureVlanPortMacMapMcastBcastOption )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						i4Dot1qFutureVlanPortMacMapMcastBcastOption);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapMcastBcastOptionTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapRowStatusSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				INT4 i4Dot1qFutureVlanPortMacMapRowStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanPortMacMapRowStatus(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						i4Dot1qFutureVlanPortMacMapRowStatus);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortMacMapRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortMacMapRowStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT1 *pDot1qFutureVlanPortMacMapAddr,
				INT4 i4Dot1qFutureVlanPortMacMapRowStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortMacMapRowStatus(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortMacMapAddr,
						i4Dot1qFutureVlanPortMacMapRowStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanPortMacMapRowStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanFidSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanFidSet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 u4Dot1qFutureVlanFid )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanFid(
						u4Dot1qFutureVlanIndex,
						u4Dot1qFutureVlanFid);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanFidSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanFidTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanFidTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 u4Dot1qFutureVlanFid )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanFid(pu4Error,
						u4Dot1qFutureVlanIndex,
						u4Dot1qFutureVlanFid);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanFidTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterRxUcastGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterRxUcastGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterRxUcast )
{

		INT1 i1RetVal;
		VLAN_LOCK ();

		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterRxUcast(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterRxUcast );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterRxUcastGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterRxMcastBcastGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterRxMcastBcastGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterRxMcastBcast )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterRxMcastBcast(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterRxMcastBcast );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterRxMcastBcastGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterTxUnknUcastGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterTxUnknUcastGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterTxUnknUcast )
{

		INT1 i1RetVal;
		VLAN_LOCK ();

		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterTxUnknUcast(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterTxUnknUcast );
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanCounterTxUnknUcastGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterTxUcastGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterTxUcastGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterTxUcast )
{

		INT1 i1RetVal;
		VLAN_LOCK ();

		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterTxUcast(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterTxUcast );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterTxUcastGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterTxBcastGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterTxBcastGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterTxBcast )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterTxBcast(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterTxBcast );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterTxBcastGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterRxFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterRxFramesGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterRxFrames )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterRxFrames(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterRxFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterRxFramesGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterRxBytesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterRxBytesGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterRxBytes )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterRxBytes(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterRxBytes );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterRxBytesGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterTxFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterTxFramesGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterTxFrames )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterTxFrames(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterTxFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterTxFramesGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterTxBytesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterTxBytesGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterTxBytes )
{

		INT1 i1RetVal;
		 VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				 VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterTxBytes(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterTxBytes );
		 VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterTxBytesGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterDiscardFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterDiscardFramesGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterDiscardFrames )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterDiscardFrames(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterDiscardFrames );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterDiscardFramesGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterDiscardBytesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterDiscardBytesGet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 *pu4Dot1qFutureVlanCounterDiscardBytes )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanCounterDiscardBytes(
						u4Dot1qFutureVlanIndex,
						pu4Dot1qFutureVlanCounterDiscardBytes );

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterDiscardBytesGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterStatusSet (
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanCounterStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK ();

		i1RetVal = nmhSetDot1qFutureVlanCounterStatus(
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanCounterStatus);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanCounterStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanCounterStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanCounterStatusTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanCounterStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanCounterStatus(pu4Error,
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanCounterStatus);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanCounterStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanUnicastMacLimitSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanUnicastMacLimitSet (
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 u4Dot1qFutureVlanUnicastMacLimit )
{

		INT1 i1RetVal;

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1qFutureVlanUnicastMacLimit(
						u4Dot1qFutureVlanIndex,
						u4Dot1qFutureVlanUnicastMacLimit);
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1qFutureVlanUnicastMacLimitSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanUnicastMacLimitTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanUnicastMacLimitTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureVlanIndex,
				UINT4 u4Dot1qFutureVlanUnicastMacLimit )
{

		INT1 i1RetVal;
		 VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1qFutureVlanUnicastMacLimit(pu4Error,
						u4Dot1qFutureVlanIndex,
						u4Dot1qFutureVlanUnicastMacLimit);
		 VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1qFutureVlanUnicastMacLimitTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanAdminMacLearningStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanAdminMacLearningStatusSet (
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanAdminMacLearningStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanAdminMacLearningStatus(
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanAdminMacLearningStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanAdminMacLearningStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanAdminMacLearningStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanAdminMacLearningStatusTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanAdminMacLearningStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanAdminMacLearningStatus(pu4Error,
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanAdminMacLearningStatus);
		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanAdminMacLearningStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanOperMacLearningStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanOperMacLearningStatusGet (
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 *pi4Dot1qFutureVlanOperMacLearningStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		if (nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable(
								u4Dot1qFutureVlanIndex) == SNMP_FAILURE)

		{
				VLAN_UNLOCK();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1qFutureVlanOperMacLearningStatus(
						u4Dot1qFutureVlanIndex,
						pi4Dot1qFutureVlanOperMacLearningStatus );

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanOperMacLearningStatusGet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortFdbFlushSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortFdbFlushSet (
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanPortFdbFlush )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortFdbFlush(
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanPortFdbFlush);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortFdbFlushSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortFdbFlushTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortFdbFlushTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanPortFdbFlush )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortFdbFlush(pu4Error,
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanPortFdbFlush);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortFdbFlushTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanWildCardEgressPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanWildCardEgressPortsSet (
				UINT1 *pDot1qFutureVlanWildCardMacAddress,
				UINT1 *pau1Dot1qFutureVlanWildCardEgressPorts )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE Dot1qFutureVlanWildCardEgressPorts;
		MEMSET (&Dot1qFutureVlanWildCardEgressPorts, 0,  sizeof (Dot1qFutureVlanWildCardEgressPorts));

		Dot1qFutureVlanWildCardEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qFutureVlanWildCardEgressPorts);
		Dot1qFutureVlanWildCardEgressPorts.pu1_OctetList = pau1Dot1qFutureVlanWildCardEgressPorts;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanWildCardEgressPorts(
						pDot1qFutureVlanWildCardMacAddress,
						&Dot1qFutureVlanWildCardEgressPorts);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanWildCardEgressPortsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanWildCardEgressPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanWildCardEgressPortsTest (UINT4 *pu4Error,
				UINT1 *pDot1qFutureVlanWildCardMacAddress,
				UINT1 *pau1Dot1qFutureVlanWildCardEgressPorts )
{

		INT1 i1RetVal;
		tSNMP_OCTET_STRING_TYPE Dot1qFutureVlanWildCardEgressPorts;
		MEMSET (&Dot1qFutureVlanWildCardEgressPorts, 0,  sizeof (Dot1qFutureVlanWildCardEgressPorts));

		Dot1qFutureVlanWildCardEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qFutureVlanWildCardEgressPorts);
		Dot1qFutureVlanWildCardEgressPorts.pu1_OctetList = pau1Dot1qFutureVlanWildCardEgressPorts;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanWildCardEgressPorts(pu4Error,
						pDot1qFutureVlanWildCardMacAddress,
						&Dot1qFutureVlanWildCardEgressPorts);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanWildCardEgressPortsTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanWildCardRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanWildCardRowStatusSet (
				UINT1 *pDot1qFutureVlanWildCardMacAddress,
				INT4 i4Dot1qFutureVlanWildCardRowStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanWildCardRowStatus(
						pDot1qFutureVlanWildCardMacAddress,
						i4Dot1qFutureVlanWildCardRowStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanWildCardRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanWildCardRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanWildCardRowStatusTest (UINT4 *pu4Error,
				UINT1 *pDot1qFutureVlanWildCardMacAddress,
				INT4 i4Dot1qFutureVlanWildCardRowStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanWildCardRowStatus(pu4Error,
						pDot1qFutureVlanWildCardMacAddress,
						i4Dot1qFutureVlanWildCardRowStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanWildCardRowStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapVidSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapVidSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapAddr,
				INT4 i4Dot1qFutureVlanPortSubnetMapVid )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapVid(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapAddr,
						i4Dot1qFutureVlanPortSubnetMapVid);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapVidSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapVidTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapVidTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapAddr,
				INT4 i4Dot1qFutureVlanPortSubnetMapVid )
{

		INT1 i1RetVal;
		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapVid(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapAddr,
						i4Dot1qFutureVlanPortSubnetMapVid);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapVidTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapARPOptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapARPOptionSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapAddr,
				INT4 i4Dot1qFutureVlanPortSubnetMapARPOption )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapARPOption(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapAddr,
						i4Dot1qFutureVlanPortSubnetMapARPOption);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapARPOptionSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapARPOptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapARPOptionTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapAddr,
				INT4 i4Dot1qFutureVlanPortSubnetMapARPOption )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapAddr,
						i4Dot1qFutureVlanPortSubnetMapARPOption);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapARPOptionTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapRowStatusSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapAddr,
				INT4 i4Dot1qFutureVlanPortSubnetMapRowStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapRowStatus(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapAddr,
						i4Dot1qFutureVlanPortSubnetMapRowStatus);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapRowStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapAddr,
				INT4 i4Dot1qFutureVlanPortSubnetMapRowStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapAddr,
						i4Dot1qFutureVlanPortSubnetMapRowStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapRowStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapExtVidSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapExtVidSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
				UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
				INT4 i4Dot1qFutureVlanPortSubnetMapExtVid )
{

		INT1 i1RetVal;
		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapExtVid(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapExtAddr,
						pDot1qFutureVlanPortSubnetMapExtMask,
						i4Dot1qFutureVlanPortSubnetMapExtVid);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapExtVidSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapExtVidTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapExtVidTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
				UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
				INT4 i4Dot1qFutureVlanPortSubnetMapExtVid )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapExtAddr,
						pDot1qFutureVlanPortSubnetMapExtMask,
						i4Dot1qFutureVlanPortSubnetMapExtVid);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapExtVidTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapExtARPOptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapExtARPOptionSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
				UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
				INT4 i4Dot1qFutureVlanPortSubnetMapExtARPOption )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapExtARPOption(
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapExtAddr,
						pDot1qFutureVlanPortSubnetMapExtMask,
						i4Dot1qFutureVlanPortSubnetMapExtARPOption);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapExtARPOptionSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapExtARPOptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapExtARPOptionTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
				UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
				INT4 i4Dot1qFutureVlanPortSubnetMapExtARPOption )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption(pu4Error,
						i4Dot1qFutureVlanPort,
						pDot1qFutureVlanPortSubnetMapExtAddr,
						pDot1qFutureVlanPortSubnetMapExtMask,
						i4Dot1qFutureVlanPortSubnetMapExtARPOption);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapExtARPOptionTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapExtRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapExtRowStatusSet (
				INT4 i4Dot1qFutureVlanPort,
				UINT4 i4Dot1qFutureVlanPortSubnetMapExtAddr,
				UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
				INT4 i4Dot1qFutureVlanPortSubnetMapExtRowStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortSubnetMapExtAddr,
						pDot1qFutureVlanPortSubnetMapExtMask,
						i4Dot1qFutureVlanPortSubnetMapExtRowStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapExtRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanPortSubnetMapExtRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanPortSubnetMapExtRowStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				UINT4 i4Dot1qFutureVlanPortSubnetMapExtAddr,
				UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
				INT4 i4Dot1qFutureVlanPortSubnetMapExtRowStatus )
{

		INT1 i1RetVal;
		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanPortSubnetMapExtAddr,
						pDot1qFutureVlanPortSubnetMapExtMask,
						i4Dot1qFutureVlanPortSubnetMapExtRowStatus);
		VLAN_UNLOCK();

		return i1RetVal;


} /* NcDot1qFutureVlanPortSubnetMapExtRowStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanLoopbackStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanLoopbackStatusSet (
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanLoopbackStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanLoopbackStatus(
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanLoopbackStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanLoopbackStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanLoopbackStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanLoopbackStatusTest (UINT4 *pu4Error,
				UINT4 u4Dot1qFutureVlanIndex,
				INT4 i4Dot1qFutureVlanLoopbackStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanLoopbackStatus(pu4Error,
						u4Dot1qFutureVlanIndex,
						i4Dot1qFutureVlanLoopbackStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanLoopbackStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelStatusSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhSetDot1qFutureVlanTunnelStatus(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanTunnelStatusSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelStatusTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelStatus )
{

		INT1 i1RetVal;

		VLAN_LOCK();
		i1RetVal = nmhTestv2Dot1qFutureVlanTunnelStatus(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelStatus);

		VLAN_UNLOCK();
		return i1RetVal;


} /* NcDot1qFutureVlanTunnelStatusTest */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelStpPDUsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelStpPDUsSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelStpPDUs )
{

		INT1 i1RetVal;

		i1RetVal = nmhSetDot1qFutureVlanTunnelStpPDUs(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelStpPDUs);

		return i1RetVal;


} /* NcDot1qFutureVlanTunnelStpPDUsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelStpPDUsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelStpPDUsTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelStpPDUs )
{

		INT1 i1RetVal;

		i1RetVal = nmhTestv2Dot1qFutureVlanTunnelStpPDUs(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelStpPDUs);

		return i1RetVal;


} /* NcDot1qFutureVlanTunnelStpPDUsTest */



/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelGvrpPDUsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelGvrpPDUsSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelGvrpPDUs )
{

		INT1 i1RetVal;

		i1RetVal = nmhSetDot1qFutureVlanTunnelGvrpPDUs(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelGvrpPDUs);

		return i1RetVal;


} /* NcDot1qFutureVlanTunnelGvrpPDUsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelGvrpPDUsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelGvrpPDUsTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelGvrpPDUs )
{

		INT1 i1RetVal;

		i1RetVal = nmhTestv2Dot1qFutureVlanTunnelGvrpPDUs(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelGvrpPDUs);

		return i1RetVal;


} /* NcDot1qFutureVlanTunnelGvrpPDUsTest */



/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelIgmpPktsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelIgmpPktsSet (
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelIgmpPkts )
{

		INT1 i1RetVal;

		i1RetVal = nmhSetDot1qFutureVlanTunnelIgmpPkts(
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelIgmpPkts);

		return i1RetVal;


} /* NcDot1qFutureVlanTunnelIgmpPktsSet */

/********************************************************************
 * FUNCTION NcDot1qFutureVlanTunnelIgmpPktsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFutureVlanTunnelIgmpPktsTest (UINT4 *pu4Error,
				INT4 i4Dot1qFutureVlanPort,
				INT4 i4Dot1qFutureVlanTunnelIgmpPkts )
{

		INT1 i1RetVal;

		i1RetVal = nmhTestv2Dot1qFutureVlanTunnelIgmpPkts(pu4Error,
						i4Dot1qFutureVlanPort,
						i4Dot1qFutureVlanTunnelIgmpPkts);

		return i1RetVal;


} /* NcDot1qFutureVlanTunnelIgmpPktsTest */


/* END i_ARICENT_VLAN_MIB.c */
