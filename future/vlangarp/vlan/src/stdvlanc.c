/******************************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdvlanc.c,v 1.3 2016/10/21 13:42:07 siva Exp $
 *
 * Description: This file contains wrapper functions netconf implementation in VLAN module.
 *
 *********************************************************************************************/
#include "lr.h"
#include "vlaninc.h"
#include "stdvlanc.h"
#include "fssnmp.h"

INT4
VlanSetProtocolGroup ( UINT1 u1FrameType,
                         UINT4 u4GroupId, UINT1 *pu1ProtocolValue,
                         UINT1 u1Length, UINT1 u1Action);

INT4
NcVlanFormPortList (UINT1 *pu1MemberPorts,
                  UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts, UINT4 u4Dot1qVlanIndex);


INT4
VlanTestStaticDefGroupModification_Info (UINT4 u4VlanId,
        tSNMP_OCTET_STRING_TYPE * pStaticPorts,
        tSNMP_OCTET_STRING_TYPE *pForbiddenPorts, UINT4 u4Type);

INT4
VlanTestModificationPort ( UINT4 u4VlanId,
        tSNMP_OCTET_STRING_TYPE * pEgressPorts,
        tSNMP_OCTET_STRING_TYPE * pUntaggedPorts,
        tSNMP_OCTET_STRING_TYPE * pForbiddenPorts);


/********************************************************************
 * FUNCTION NcDot1qVlanVersionNumberGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanVersionNumberGet (
		INT4 *pi4Dot1qVlanVersionNumber )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhGetDot1qVlanVersionNumber(
			pi4Dot1qVlanVersionNumber );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanVersionNumberGet */

/********************************************************************
 * FUNCTION NcDot1qMaxVlanIdGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qMaxVlanIdGet (
		INT4 *pi4Dot1qMaxVlanId )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhGetDot1qMaxVlanId(
			pi4Dot1qMaxVlanId );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qMaxVlanIdGet */

/********************************************************************
 * FUNCTION NcDot1qMaxSupportedVlansGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qMaxSupportedVlansGet (
		UINT4 *pu4Dot1qMaxSupportedVlans )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhGetDot1qMaxSupportedVlans(
			pu4Dot1qMaxSupportedVlans );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qMaxSupportedVlansGet */

/********************************************************************
 * FUNCTION NcDot1qNumVlansGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qNumVlansGet (
		UINT4 *pu4Dot1qNumVlans )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhGetDot1qNumVlans(
			pu4Dot1qNumVlans );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qNumVlansGet */

/********************************************************************
 * FUNCTION NcDot1qGvrpStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qGvrpStatusSet (
		INT4 i4Dot1qGvrpStatus )
{

	INT1 i1RetVal;

	GARP_LOCK();
	i1RetVal = nmhSetDot1qGvrpStatus(
			i4Dot1qGvrpStatus);

	GARP_UNLOCK();
	return i1RetVal;


} /* NcDot1qGvrpStatusSet */

/********************************************************************
 * FUNCTION NcDot1qGvrpStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qGvrpStatusTest (UINT4 *pu4Error,
		INT4 i4Dot1qGvrpStatus )
{

	INT1 i1RetVal;

	GARP_LOCK();
	i1RetVal = nmhTestv2Dot1qGvrpStatus(pu4Error,
			i4Dot1qGvrpStatus);

	GARP_UNLOCK();
	return i1RetVal;


} /* NcDot1qGvrpStatusTest */

/********************************************************************
 * FUNCTION NcDot1qVlanNumDeletesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanNumDeletesGet (
		UINT4 *pu4Dot1qVlanNumDeletes )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhGetDot1qVlanNumDeletes(
			pu4Dot1qVlanNumDeletes );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanNumDeletesGet */

/********************************************************************
 * FUNCTION NcDot1qNextFreeLocalVlanIndexGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qNextFreeLocalVlanIndexGet (
		INT4 *pi4Dot1qNextFreeLocalVlanIndex )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhGetDot1qNextFreeLocalVlanIndex(
			pi4Dot1qNextFreeLocalVlanIndex );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qNextFreeLocalVlanIndexGet */

/********************************************************************
 * FUNCTION NcDot1qConstraintSetDefaultSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintSetDefaultSet (
		INT4 i4Dot1qConstraintSetDefault )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qConstraintSetDefault(
			i4Dot1qConstraintSetDefault);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintSetDefaultSet */

/********************************************************************
 * FUNCTION NcDot1qConstraintSetDefaultTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintSetDefaultTest (UINT4 *pu4Error,
		INT4 i4Dot1qConstraintSetDefault )
{

	INT1 i1RetVal;
	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qConstraintSetDefault(pu4Error,
			i4Dot1qConstraintSetDefault);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintSetDefaultTest */

/********************************************************************
 * FUNCTION NcDot1qConstraintTypeDefaultSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintTypeDefaultSet (
		INT4 i4Dot1qConstraintTypeDefault )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qConstraintTypeDefault(
			i4Dot1qConstraintTypeDefault);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintTypeDefaultSet */

/********************************************************************
 * FUNCTION NcDot1qConstraintTypeDefaultTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintTypeDefaultTest (UINT4 *pu4Error,
		INT4 i4Dot1qConstraintTypeDefault )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qConstraintTypeDefault(pu4Error,
			i4Dot1qConstraintTypeDefault);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintTypeDefaultTest */

/********************************************************************
 * FUNCTION NcDot1qFdbDynamicCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qFdbDynamicCountGet (
		UINT4 u4Dot1qFdbId,
		UINT4 *pu4Dot1qFdbDynamicCount )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qFdbTable(
				u4Dot1qFdbId) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qFdbDynamicCount(
			u4Dot1qFdbId,
			pu4Dot1qFdbDynamicCount );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qFdbDynamicCountGet */

/********************************************************************
 * FUNCTION NcDot1qTpFdbPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpFdbPortGet (
		UINT4 u4Dot1qFdbId,
		UINT1 *pDot1qTpFdbAddress,
		INT4 *pi4Dot1qTpFdbPort )
{

	INT1 i1RetVal;
	INT4    i4Size = (INT4) STRLEN(pDot1qTpFdbAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qTpFdbAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qTpFdbTable(
				u4Dot1qFdbId,
				au1MacAddr) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpFdbPort(
			u4Dot1qFdbId,
			au1MacAddr,
			pi4Dot1qTpFdbPort );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpFdbPortGet */

/********************************************************************
 * FUNCTION NcDot1qTpFdbStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpFdbStatusGet (
		UINT4 u4Dot1qFdbId,
		UINT1 *pDot1qTpFdbAddress,
		INT4 *pi4Dot1qTpFdbStatus )
{

	INT1 i1RetVal;
	INT4    i4Size = (INT4) STRLEN(pDot1qTpFdbAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qTpFdbAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qTpFdbTable(
				u4Dot1qFdbId,
				au1MacAddr) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpFdbStatus(
			u4Dot1qFdbId,
			au1MacAddr,
			pi4Dot1qTpFdbStatus );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpFdbStatusGet */

/********************************************************************
 * FUNCTION NcDot1qTpGroupEgressPortsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpGroupEgressPortsGet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qTpGroupAddress,
		UINT1 *pau1Dot1qTpGroupEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qTpGroupEgressPorts;
	INT4    i4Size = (INT4) STRLEN(pDot1qTpGroupAddress) + 1;
	MEMSET (pau1Dot1qTpGroupEgressPorts, '\0', VLAN_PORT_LENGTH);
	MEMSET (&Dot1qTpGroupEgressPorts, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1qTpGroupEgressPorts.pu1_OctetList = pau1Dot1qTpGroupEgressPorts;

	UINT1   au1FdbAddress[i4Size];
	UINT1 au1MacAddr[MAC_ADDR_LEN];
	MEMSET (au1FdbAddress, '\0', i4Size);
	MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

	MEMCPY (au1FdbAddress, pDot1qTpGroupAddress, i4Size);

	CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qTpGroupTable(
				u4Dot1qVlanIndex,
				au1MacAddr) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpGroupEgressPorts(
			u4Dot1qVlanIndex,
			au1MacAddr,
			&Dot1qTpGroupEgressPorts );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpGroupEgressPortsGet */

/********************************************************************
 * FUNCTION NcDot1qTpGroupLearntGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpGroupLearntGet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qTpGroupAddress,
		UINT1 *pau1Dot1qTpGroupLearnt )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qTpGroupLearnt;
	MEMSET (&Dot1qTpGroupLearnt, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1qTpGroupLearnt.pu1_OctetList = pau1Dot1qTpGroupLearnt;

	INT4    i4Size = (INT4) STRLEN(pDot1qTpGroupAddress) + 1;

	UINT1   au1FdbAddress[i4Size];
	UINT1 au1MacAddr[MAC_ADDR_LEN];
	MEMSET (au1FdbAddress, '\0', i4Size);
	MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);

	MEMCPY (au1FdbAddress, pDot1qTpGroupAddress, i4Size);

	CliDotStrToMac (au1FdbAddress, au1MacAddr);


	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qTpGroupTable(
				u4Dot1qVlanIndex,
				au1MacAddr) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpGroupLearnt(
			u4Dot1qVlanIndex,
			au1MacAddr,
			&Dot1qTpGroupLearnt );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpGroupLearntGet */

INT4
VlanTestStaticDefGroupModification_Info (UINT4 u4VlanId,
		tSNMP_OCTET_STRING_TYPE * pStaticPorts,
		tSNMP_OCTET_STRING_TYPE *pForbiddenPorts, UINT4 u4Type)
{
	UINT1              *pPortList1 = NULL;
	UINT1              *pPortList2 = NULL;
	tVlanCurrEntry     *pCurrEntry;
	UINT1               u1Result = VLAN_TRUE;
	UINT1               bu1RetVal = VLAN_FALSE;

	if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
	{
		return SNMP_FAILURE;
	}

	pCurrEntry = VlanGetVlanEntry ((tVlanId) u4VlanId);

	if (pCurrEntry == NULL)
	{
		/*
		 * pCurrEntry must be created in Current Vlan entry for
		 * the configuration of Forward All Groups.
		 */
		return SNMP_FAILURE;
	}

	if ((pStaticPorts->i4_Length <= 0) ||
			(pStaticPorts->i4_Length > VLAN_PORT_LIST_SIZE))
	{
		return SNMP_FAILURE;
	}

	VLAN_IS_EXCEED_MAX_PORTS (pStaticPorts->pu1_OctetList,
			pStaticPorts->i4_Length, bu1RetVal);

	if (bu1RetVal == VLAN_TRUE)
	{
		return SNMP_FAILURE;
	}

	if (VlanIsPortListValid (pStaticPorts->pu1_OctetList,
				pStaticPorts->i4_Length) == VLAN_FALSE)
	{
		return SNMP_FAILURE;
	}

	if ((pForbiddenPorts->i4_Length <= 0) ||
			(pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
	{
		return SNMP_FAILURE;
	}

	VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
			pForbiddenPorts->i4_Length, bu1RetVal);

	if (bu1RetVal == VLAN_TRUE)
	{
		return SNMP_FAILURE;
	}

	if (VlanIsPortListValid (pForbiddenPorts->pu1_OctetList,
				pForbiddenPorts->i4_Length) == VLAN_FALSE)
	{
		return SNMP_FAILURE;
	}
	pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

	if (pPortList1 == NULL)
	{
		VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
				"VlanTestStaticDefGroupModificationInfo: Error in allocating memory "
				"for pPortList1\r\n");
		return SNMP_FAILURE;
	}
	pPortList2 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

	if (pPortList2 == NULL)
	{
		UtilPlstReleaseLocalPortList (pPortList1);
		VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
				"VlanTestStaticDefGroupModificationInfo: Error in allocating memory "
				"for pPortList2\r\n");
		return SNMP_FAILURE;
	}

	VLAN_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
	VLAN_MEMSET (pPortList2, 0, sizeof (tLocalPortList));
	if (VLAN_MEMCMP (pStaticPorts->pu1_OctetList, gNullPortList,
				VLAN_PORT_LIST_SIZE) == 0)
	{
		if (u4Type == VLAN_ALL_GROUPS)
		{
			VLAN_MEMCPY (pPortList1,
					pCurrEntry->AllGrps.StaticPorts, VLAN_PORT_LIST_SIZE);
		}
		else
		{
			VLAN_MEMCPY (pPortList1,
					pCurrEntry->UnRegGrps.StaticPorts,
					VLAN_PORT_LIST_SIZE);
		}
	}
	else
	{
		VLAN_MEMCPY (pPortList1,
				pStaticPorts->pu1_OctetList, pStaticPorts->i4_Length);
	}

	if (VLAN_MEMCMP (pForbiddenPorts->pu1_OctetList, gNullPortList,
				VLAN_PORT_LIST_SIZE) == 0)
	{
		if (u4Type == VLAN_ALL_GROUPS)
		{
			VLAN_MEMCPY (pPortList2,
					pCurrEntry->AllGrps.ForbiddenPorts,
					VLAN_PORT_LIST_SIZE);
		}
		else
		{
			VLAN_MEMCPY (pPortList2,
					pCurrEntry->UnRegGrps.ForbiddenPorts,
					VLAN_PORT_LIST_SIZE);
		}
	}
	else
	{
		VLAN_MEMCPY (pPortList2,
				pForbiddenPorts->pu1_OctetList,
				pForbiddenPorts->i4_Length);
	}

	VLAN_ARE_PORTS_EXCLUSIVE (pPortList1, pPortList2, u1Result);

	UtilPlstReleaseLocalPortList (pPortList1);
	UtilPlstReleaseLocalPortList (pPortList2);
	if (u1Result == VLAN_FALSE)
	{
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}


INT1 VlanConfigureForwardPort (UINT1 *pu1EgressPorts,
		UINT1 *pu1ForbiddenPorts, UINT4 u4EgressFlag,
		UINT4 u4Type,UINT4 u4VlanId,INT1 PortType)
{
	UINT1              *pau1StaticPortList = NULL;
	UINT1              *pau1ForbiddenPortList = NULL;
	UINT1              *pau1OldStaticPortList = NULL;
	/*INT4                i4Val = 0;*/
	tSNMP_OCTET_STRING_TYPE StaticPortList;
	tSNMP_OCTET_STRING_TYPE *pStaticPortList = &StaticPortList;
	tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
	tSNMP_OCTET_STRING_TYPE OldStaticPortList;

	/*i4Val = CLI_GET_VLANID ();

	  if (i4Val == CLI_ERROR)
	  {
	  return SNMP_FAILURE;
	  }*/

	pau1ForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1ForbiddenPortList == NULL)
	{
		VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
				"VlanSetFwdEntry: "
				"Error in allocating memory for pau1ForbiddenPortList\r\n");
		return SNMP_FAILURE;
	}
	MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

	pau1StaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1StaticPortList == NULL)
	{
		VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
				"VlanSetFwdEntry: "
				"Error in allocating memory for pau1StaticPortList\r\n");
		UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
		return SNMP_FAILURE;
	}
	MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

	MEMCPY (pau1StaticPortList, pu1EgressPorts, VLAN_PORT_LIST_SIZE);
	StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
	StaticPortList.pu1_OctetList = pau1StaticPortList;

	MEMCPY (pau1ForbiddenPortList, pu1ForbiddenPorts, VLAN_PORT_LIST_SIZE);
	ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
	ForbiddenPortList.pu1_OctetList = pau1ForbiddenPortList;

	/*
	 * we call a separate test function to validate the inputs,
	 * since the normal nmh Validation routines check for mutual
	 * exclusiveness with the existing egress or forbidden ports
	 */

	if (VlanTestStaticDefGroupModification_Info (u4VlanId,
				&StaticPortList,
				&ForbiddenPortList,
				u4Type) == SNMP_FAILURE)
	{
		UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
		UtilPlstReleaseLocalPortList (pau1StaticPortList);
		return SNMP_FAILURE;
	}
	pau1OldStaticPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1OldStaticPortList == NULL)
	{
		VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
				"VlanSetFwdEntry: "
				"Error in allocating memory for pau1OldStaticPortList\r\n");
		UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
		UtilPlstReleaseLocalPortList (pau1StaticPortList);
		return VLAN_FAILURE;
	}

	MEMSET (pau1OldStaticPortList, 0, VLAN_PORT_LIST_SIZE);

	OldStaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
	OldStaticPortList.pu1_OctetList = pau1OldStaticPortList;

	/*
	 * From here we go ahead setting the egress and forbidden
	 * ports since validation has passed
	 */
	if (u4Type == VLAN_ALL_GROUPS)
	{
		if(PortType == VLAN_FORWARDALL_STATIC_PORT)
		{ 
			if (u4EgressFlag == VLAN_TRUE)
			{
				nmhGetDot1qForwardAllStaticPorts (u4VlanId, &OldStaticPortList);

				if (nmhSetDot1qForwardAllStaticPorts (u4VlanId,
							pStaticPortList)
						== SNMP_FAILURE)
				{
					UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
					UtilPlstReleaseLocalPortList (pau1StaticPortList);
					UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
					return SNMP_FAILURE;
				}
			}
		}
		else if(PortType == VLAN_FORWARDALL_FORBIDDEN_PORT)
		{
			if (nmhSetDot1qForwardAllForbiddenPorts (u4VlanId,
						&ForbiddenPortList)
					== SNMP_FAILURE)
			{
				if (u4EgressFlag == VLAN_TRUE)
				{
					nmhSetDot1qForwardAllStaticPorts (u4VlanId, &OldStaticPortList);
				}

				UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
				UtilPlstReleaseLocalPortList (pau1StaticPortList);
				UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
				return SNMP_FAILURE;
			}
		}
	}
	else if (u4Type == VLAN_UNREG_GROUPS)
	{
		if (u4EgressFlag == VLAN_TRUE)
		{
			if(PortType == VLAN_FORWARD_UNREG_STATIC_PORT)
			{
			nmhGetDot1qForwardUnregisteredPorts (u4VlanId, &OldStaticPortList);

			if (nmhSetDot1qForwardUnregisteredStaticPorts (u4VlanId,
						pStaticPortList)
					== SNMP_FAILURE)
			{
				UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
				UtilPlstReleaseLocalPortList (pau1StaticPortList);
				UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
				return SNMP_FAILURE;
			}
		}
		}
		else if(PortType == VLAN_FORWARD_UNREG_FORBIDDEN_PORT)
		{

		if (nmhSetDot1qForwardUnregisteredForbiddenPorts (u4VlanId,
					&ForbiddenPortList)
				== SNMP_FAILURE)
		{
			if (u4EgressFlag == VLAN_TRUE)
			{
				nmhSetDot1qForwardUnregisteredStaticPorts (u4VlanId,
						&OldStaticPortList);
			}

			UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
			UtilPlstReleaseLocalPortList (pau1StaticPortList);
			UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
			return SNMP_FAILURE;
		}
		}
	}

	UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
	UtilPlstReleaseLocalPortList (pau1StaticPortList);
	UtilPlstReleaseLocalPortList (pau1OldStaticPortList);
	return SNMP_SUCCESS;

}

/********************************************************************
 * FUNCTION NcDot1qForwardAllPortsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardAllPortsGet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardAllPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardAllPorts;
	MEMSET (&Dot1qForwardAllPorts, 0,  sizeof (Dot1qForwardAllPorts));

	Dot1qForwardAllPorts.pu1_OctetList = pau1Dot1qForwardAllPorts;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qForwardAllTable(
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qForwardAllPorts(
			u4Dot1qVlanIndex,
			&Dot1qForwardAllPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardAllPortsGet */

/********************************************************************
 * FUNCTION NcDot1qForwardAllStaticPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardAllStaticPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardAllStaticPorts )
{

	INT1 i1RetVal = 0;
	UINT4 u4Type = VLAN_ALL_GROUPS;
	UINT4 u4EgressFlag = VLAN_TRUE;
	INT1 PortType = VLAN_FORWARDALL_STATIC_PORT;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardAllStaticPorts;
	MEMSET (&Dot1qForwardAllStaticPorts, 0,  sizeof (Dot1qForwardAllStaticPorts));

	Dot1qForwardAllStaticPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardAllStaticPorts);
	Dot1qForwardAllStaticPorts.pu1_OctetList = pau1Dot1qForwardAllStaticPorts;
	UINT1 *pau1Dot1qForwardAllForbiddenPorts = NULL;


	pau1Dot1qForwardAllForbiddenPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

	MEMSET (pau1Dot1qForwardAllForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);


	VLAN_LOCK();

	i1RetVal = VlanConfigureForwardPort (pau1Dot1qForwardAllStaticPorts,
			pau1Dot1qForwardAllForbiddenPorts,u4EgressFlag,
			u4Type,u4Dot1qVlanIndex,PortType);

	/*i1RetVal = nmhSetDot1qForwardAllStaticPorts(
	  u4Dot1qVlanIndex,
	  &Dot1qForwardAllStaticPorts);
	  */
	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardAllStaticPortsSet */

/********************************************************************
 * FUNCTION NcDot1qForwardAllStaticPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardAllStaticPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardAllStaticPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardAllStaticPorts;
	MEMSET (&Dot1qForwardAllStaticPorts, 0,  sizeof (Dot1qForwardAllStaticPorts));

	Dot1qForwardAllStaticPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardAllStaticPorts);
	Dot1qForwardAllStaticPorts.pu1_OctetList = pau1Dot1qForwardAllStaticPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qForwardAllStaticPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qForwardAllStaticPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardAllStaticPortsTest */

/********************************************************************
 * FUNCTION NcDot1qForwardAllForbiddenPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardAllForbiddenPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardAllForbiddenPorts )
{

	INT1 i1RetVal = 0;
	UINT4 u4Type = VLAN_ALL_GROUPS;
	UINT4 u4EgressFlag = VLAN_TRUE;
	INT1 PortType = VLAN_FORWARDALL_FORBIDDEN_PORT;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardAllForbiddenPorts;
	MEMSET (&Dot1qForwardAllForbiddenPorts, 0,  sizeof (Dot1qForwardAllForbiddenPorts));

	Dot1qForwardAllForbiddenPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardAllForbiddenPorts);
	Dot1qForwardAllForbiddenPorts.pu1_OctetList = pau1Dot1qForwardAllForbiddenPorts;

	UINT1 *pau1Dot1qForwardAllStaticPorts = NULL;

	pau1Dot1qForwardAllStaticPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

	MEMSET (pau1Dot1qForwardAllStaticPorts, 0, VLAN_PORT_LIST_SIZE);

	VLAN_LOCK();

	i1RetVal = VlanConfigureForwardPort (pau1Dot1qForwardAllStaticPorts,
			pau1Dot1qForwardAllForbiddenPorts,u4EgressFlag,
			u4Type,u4Dot1qVlanIndex,PortType);

	/*i1RetVal = nmhSetDot1qForwardAllForbiddenPorts(
	  u4Dot1qVlanIndex,
	  &Dot1qForwardAllForbiddenPorts);
	  */
	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardAllForbiddenPortsSet */

/********************************************************************
 * FUNCTION NcDot1qForwardAllForbiddenPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardAllForbiddenPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardAllForbiddenPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardAllForbiddenPorts;
	MEMSET (&Dot1qForwardAllForbiddenPorts, 0,  sizeof (Dot1qForwardAllForbiddenPorts));

	Dot1qForwardAllForbiddenPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardAllForbiddenPorts);
	Dot1qForwardAllForbiddenPorts.pu1_OctetList = pau1Dot1qForwardAllForbiddenPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qForwardAllForbiddenPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qForwardAllForbiddenPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardAllForbiddenPortsTest */

/********************************************************************
 * FUNCTION NcDot1qForwardUnregisteredPortsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardUnregisteredPortsGet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardUnregisteredPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardUnregisteredPorts;
	MEMSET (&Dot1qForwardUnregisteredPorts, 0,  sizeof (Dot1qForwardUnregisteredPorts));

	Dot1qForwardUnregisteredPorts.pu1_OctetList = pau1Dot1qForwardUnregisteredPorts;


	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qForwardUnregisteredTable(
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qForwardUnregisteredPorts(
			u4Dot1qVlanIndex,
			&Dot1qForwardUnregisteredPorts );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardUnregisteredPortsGet */

/********************************************************************
 * FUNCTION NcDot1qForwardUnregisteredStaticPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardUnregisteredStaticPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardUnregisteredStaticPorts )
{

	INT1 i1RetVal;
	UINT4 u4Type = VLAN_UNREG_GROUPS;
    UINT4 u4EgressFlag = VLAN_TRUE;
    INT1 PortType = VLAN_FORWARD_UNREG_STATIC_PORT;

	tSNMP_OCTET_STRING_TYPE Dot1qForwardUnregisteredStaticPorts;
	MEMSET (&Dot1qForwardUnregisteredStaticPorts, 0,  sizeof (Dot1qForwardUnregisteredStaticPorts));

	Dot1qForwardUnregisteredStaticPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardUnregisteredStaticPorts);
	Dot1qForwardUnregisteredStaticPorts.pu1_OctetList = pau1Dot1qForwardUnregisteredStaticPorts;
	
	UINT1 *pau1Dot1qForwardUnregisteredForbiddenPorts = NULL;

    pau1Dot1qForwardUnregisteredForbiddenPorts =
        UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

    MEMSET (pau1Dot1qForwardUnregisteredForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

    VLAN_LOCK();
    i1RetVal = VlanConfigureForwardPort (pau1Dot1qForwardUnregisteredStaticPorts,
            pau1Dot1qForwardUnregisteredForbiddenPorts,u4EgressFlag,
            u4Type,u4Dot1qVlanIndex,PortType);


	/*i1RetVal = nmhSetDot1qForwardUnregisteredStaticPorts(
			u4Dot1qVlanIndex,
			&Dot1qForwardUnregisteredStaticPorts);
	*/
	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardUnregisteredStaticPortsSet */

/********************************************************************
 * FUNCTION NcDot1qForwardUnregisteredStaticPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardUnregisteredStaticPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardUnregisteredStaticPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardUnregisteredStaticPorts;
	MEMSET (&Dot1qForwardUnregisteredStaticPorts, 0,  sizeof (Dot1qForwardUnregisteredStaticPorts));

	Dot1qForwardUnregisteredStaticPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardUnregisteredStaticPorts);
	Dot1qForwardUnregisteredStaticPorts.pu1_OctetList = pau1Dot1qForwardUnregisteredStaticPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qForwardUnregisteredStaticPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qForwardUnregisteredStaticPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardUnregisteredStaticPortsTest */

/********************************************************************
 * FUNCTION NcDot1qForwardUnregisteredForbiddenPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardUnregisteredForbiddenPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardUnregisteredForbiddenPorts )
{

	INT1 i1RetVal;
	UINT4 u4Type = VLAN_UNREG_GROUPS;
    UINT4 u4EgressFlag = VLAN_TRUE;
    INT1 PortType = VLAN_FORWARD_UNREG_FORBIDDEN_PORT;

	tSNMP_OCTET_STRING_TYPE Dot1qForwardUnregisteredForbiddenPorts;
	MEMSET (&Dot1qForwardUnregisteredForbiddenPorts, 0,  sizeof (Dot1qForwardUnregisteredForbiddenPorts));

	Dot1qForwardUnregisteredForbiddenPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardUnregisteredForbiddenPorts);
	Dot1qForwardUnregisteredForbiddenPorts.pu1_OctetList = pau1Dot1qForwardUnregisteredForbiddenPorts;
	
	UINT1 *pau1Dot1qForwardUnregisteredStaticPorts = NULL;

   	pau1Dot1qForwardUnregisteredStaticPorts =
        UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

    MEMSET (pau1Dot1qForwardUnregisteredStaticPorts, 0, VLAN_PORT_LIST_SIZE);

    VLAN_LOCK();
    i1RetVal = VlanConfigureForwardPort (pau1Dot1qForwardUnregisteredStaticPorts,
            pau1Dot1qForwardUnregisteredForbiddenPorts,u4EgressFlag,
            u4Type,u4Dot1qVlanIndex,PortType);

	/*i1RetVal = nmhSetDot1qForwardUnregisteredForbiddenPorts(
			u4Dot1qVlanIndex,
			&Dot1qForwardUnregisteredForbiddenPorts);
	*/
	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardUnregisteredForbiddenPortsSet */

/********************************************************************
 * FUNCTION NcDot1qForwardUnregisteredForbiddenPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qForwardUnregisteredForbiddenPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qForwardUnregisteredForbiddenPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qForwardUnregisteredForbiddenPorts;
	MEMSET (&Dot1qForwardUnregisteredForbiddenPorts, 0,  sizeof (Dot1qForwardUnregisteredForbiddenPorts));

	Dot1qForwardUnregisteredForbiddenPorts.i4_Length = (INT4) STRLEN (pau1Dot1qForwardUnregisteredForbiddenPorts);
	Dot1qForwardUnregisteredForbiddenPorts.pu1_OctetList = pau1Dot1qForwardUnregisteredForbiddenPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qForwardUnregisteredForbiddenPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qForwardUnregisteredForbiddenPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qForwardUnregisteredForbiddenPortsTest */

/********************************************************************
 * FUNCTION NcDot1qStaticUnicastAllowedToGoToSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticUnicastAllowedToGoToSet (
		UINT4 u4Dot1qFdbId,
		UINT1 *pDot1qStaticUnicastAddress,
		INT4 i4Dot1qStaticUnicastReceivePort,
		UINT1 *pau1Dot1qStaticUnicastAllowedToGoTo )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qStaticUnicastAllowedToGoTo;
	MEMSET (&Dot1qStaticUnicastAllowedToGoTo, 0,  sizeof (Dot1qStaticUnicastAllowedToGoTo));

	Dot1qStaticUnicastAllowedToGoTo.i4_Length = (INT4) STRLEN (pau1Dot1qStaticUnicastAllowedToGoTo);
	Dot1qStaticUnicastAllowedToGoTo.pu1_OctetList = pau1Dot1qStaticUnicastAllowedToGoTo;
	
	INT4    i4Size = (INT4) STRLEN(pDot1qStaticUnicastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticUnicastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qStaticUnicastAllowedToGoTo(
			u4Dot1qFdbId,
			au1MacAddr,
			i4Dot1qStaticUnicastReceivePort,
			&Dot1qStaticUnicastAllowedToGoTo);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticUnicastAllowedToGoToSet */

/********************************************************************
 * FUNCTION NcDot1qStaticUnicastAllowedToGoToTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticUnicastAllowedToGoToTest (UINT4 *pu4Error,
		UINT4 u4Dot1qFdbId,
		UINT1 *pDot1qStaticUnicastAddress,
		INT4 i4Dot1qStaticUnicastReceivePort,
		UINT1 *pau1Dot1qStaticUnicastAllowedToGoTo )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qStaticUnicastAllowedToGoTo;
	MEMSET (&Dot1qStaticUnicastAllowedToGoTo, 0,  sizeof (Dot1qStaticUnicastAllowedToGoTo));

	Dot1qStaticUnicastAllowedToGoTo.i4_Length = (INT4) STRLEN (pau1Dot1qStaticUnicastAllowedToGoTo);
	Dot1qStaticUnicastAllowedToGoTo.pu1_OctetList = pau1Dot1qStaticUnicastAllowedToGoTo;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qStaticUnicastAllowedToGoTo(pu4Error,
			u4Dot1qFdbId,
			pDot1qStaticUnicastAddress,
			i4Dot1qStaticUnicastReceivePort,
			&Dot1qStaticUnicastAllowedToGoTo);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticUnicastAllowedToGoToTest */

/********************************************************************
 * FUNCTION NcDot1qStaticUnicastStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticUnicastStatusSet (
		UINT4 u4Dot1qFdbId,
		UINT1 *pDot1qStaticUnicastAddress,
		INT4 i4Dot1qStaticUnicastReceivePort,
		INT4 i4Dot1qStaticUnicastStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qStaticUnicastStatus(
			u4Dot1qFdbId,
			pDot1qStaticUnicastAddress,
			i4Dot1qStaticUnicastReceivePort,
			i4Dot1qStaticUnicastStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticUnicastStatusSet */

/********************************************************************
 * FUNCTION NcDot1qStaticUnicastStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticUnicastStatusTest (UINT4 *pu4Error,
		UINT4 u4Dot1qFdbId,
		UINT1 *pDot1qStaticUnicastAddress,
		INT4 i4Dot1qStaticUnicastReceivePort,
		INT4 i4Dot1qStaticUnicastStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qStaticUnicastStatus(pu4Error,
			u4Dot1qFdbId,
			pDot1qStaticUnicastAddress,
			i4Dot1qStaticUnicastReceivePort,
			i4Dot1qStaticUnicastStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticUnicastStatusTest */

/********************************************************************
 * FUNCTION NcDot1qStaticMulticastStaticEgressPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticMulticastStaticEgressPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qStaticMulticastAddress,
		INT4 i4Dot1qStaticMulticastReceivePort,
		UINT1 *pau1Dot1qStaticMulticastStaticEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qStaticMulticastStaticEgressPorts;
	MEMSET (&Dot1qStaticMulticastStaticEgressPorts, 0,  sizeof (Dot1qStaticMulticastStaticEgressPorts));

	Dot1qStaticMulticastStaticEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qStaticMulticastStaticEgressPorts);
	Dot1qStaticMulticastStaticEgressPorts.pu1_OctetList = pau1Dot1qStaticMulticastStaticEgressPorts;

	INT4    i4Size = (INT4) STRLEN(pDot1qStaticMulticastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticMulticastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qStaticMulticastStaticEgressPorts(
			u4Dot1qVlanIndex,
			au1MacAddr,
			i4Dot1qStaticMulticastReceivePort,
			&Dot1qStaticMulticastStaticEgressPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticMulticastStaticEgressPortsSet */

/********************************************************************
 * FUNCTION NcDot1qStaticMulticastStaticEgressPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticMulticastStaticEgressPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qStaticMulticastAddress,
		INT4 i4Dot1qStaticMulticastReceivePort,
		UINT1 *pau1Dot1qStaticMulticastStaticEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qStaticMulticastStaticEgressPorts;
	MEMSET (&Dot1qStaticMulticastStaticEgressPorts, 0,  sizeof (Dot1qStaticMulticastStaticEgressPorts));

	Dot1qStaticMulticastStaticEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qStaticMulticastStaticEgressPorts);
	Dot1qStaticMulticastStaticEgressPorts.pu1_OctetList = pau1Dot1qStaticMulticastStaticEgressPorts;

	INT4    i4Size = (INT4) STRLEN(pDot1qStaticMulticastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticMulticastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qStaticMulticastStaticEgressPorts(pu4Error,
			u4Dot1qVlanIndex,
			au1MacAddr,
			i4Dot1qStaticMulticastReceivePort,
			&Dot1qStaticMulticastStaticEgressPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticMulticastStaticEgressPortsTest */

/********************************************************************
 * FUNCTION NcDot1qStaticMulticastForbiddenEgressPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticMulticastForbiddenEgressPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qStaticMulticastAddress,
		INT4 i4Dot1qStaticMulticastReceivePort,
		UINT1 *pau1Dot1qStaticMulticastForbiddenEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qStaticMulticastForbiddenEgressPorts;
	MEMSET (&Dot1qStaticMulticastForbiddenEgressPorts, 0,  sizeof (Dot1qStaticMulticastForbiddenEgressPorts));

	Dot1qStaticMulticastForbiddenEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qStaticMulticastForbiddenEgressPorts);
	Dot1qStaticMulticastForbiddenEgressPorts.pu1_OctetList = pau1Dot1qStaticMulticastForbiddenEgressPorts;

	INT4    i4Size = (INT4)STRLEN(pDot1qStaticMulticastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticMulticastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);
	
	VLAN_LOCK();
	i1RetVal = nmhSetDot1qStaticMulticastForbiddenEgressPorts(
			u4Dot1qVlanIndex,
			au1MacAddr,
			i4Dot1qStaticMulticastReceivePort,
			&Dot1qStaticMulticastForbiddenEgressPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticMulticastForbiddenEgressPortsSet */

/********************************************************************
 * FUNCTION NcDot1qStaticMulticastForbiddenEgressPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticMulticastForbiddenEgressPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qStaticMulticastAddress,
		INT4 i4Dot1qStaticMulticastReceivePort,
		UINT1 *pau1Dot1qStaticMulticastForbiddenEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qStaticMulticastForbiddenEgressPorts;
	MEMSET (&Dot1qStaticMulticastForbiddenEgressPorts, 0,  sizeof (Dot1qStaticMulticastForbiddenEgressPorts));

	Dot1qStaticMulticastForbiddenEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qStaticMulticastForbiddenEgressPorts);
	Dot1qStaticMulticastForbiddenEgressPorts.pu1_OctetList = pau1Dot1qStaticMulticastForbiddenEgressPorts;

	INT4    i4Size = (INT4) STRLEN(pDot1qStaticMulticastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticMulticastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts(pu4Error,
			u4Dot1qVlanIndex,
			au1MacAddr,
			i4Dot1qStaticMulticastReceivePort,
			&Dot1qStaticMulticastForbiddenEgressPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticMulticastForbiddenEgressPortsTest */

/********************************************************************
 * FUNCTION NcDot1qStaticMulticastStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticMulticastStatusSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qStaticMulticastAddress,
		INT4 i4Dot1qStaticMulticastReceivePort,
		INT4 i4Dot1qStaticMulticastStatus )
{

	INT1 i1RetVal;
	INT4    i4Size = (INT4)STRLEN(pDot1qStaticMulticastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticMulticastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qStaticMulticastStatus(
			u4Dot1qVlanIndex,
			au1MacAddr,
			i4Dot1qStaticMulticastReceivePort,
			i4Dot1qStaticMulticastStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticMulticastStatusSet */

/********************************************************************
 * FUNCTION NcDot1qStaticMulticastStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qStaticMulticastStatusTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qStaticMulticastAddress,
		INT4 i4Dot1qStaticMulticastReceivePort,
		INT4 i4Dot1qStaticMulticastStatus )
{

	INT1 i1RetVal;
	INT4    i4Size = (INT4)STRLEN(pDot1qStaticMulticastAddress) + 1;
    UINT1   au1FdbAddress[i4Size];
    UINT1 au1MacAddr[MAC_ADDR_LEN];
    MEMSET (au1FdbAddress, '\0', i4Size);
    MEMSET (au1MacAddr, '\0', MAC_ADDR_LEN);
    MEMCPY (au1FdbAddress, pDot1qStaticMulticastAddress, i4Size);
    CliDotStrToMac (au1FdbAddress, au1MacAddr);

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qStaticMulticastStatus(pu4Error,
			u4Dot1qVlanIndex,
			au1MacAddr,
			i4Dot1qStaticMulticastReceivePort,
			i4Dot1qStaticMulticastStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qStaticMulticastStatusTest */

/********************************************************************
 * FUNCTION NcDot1qVlanFdbIdGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanFdbIdGet (
		UINT4 u4Dot1qVlanTimeMark,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qVlanFdbId )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qVlanCurrentTable(
				u4Dot1qVlanTimeMark,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qVlanFdbId(
			u4Dot1qVlanTimeMark,
			u4Dot1qVlanIndex,
			pu4Dot1qVlanFdbId );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanFdbIdGet */

/********************************************************************
 * FUNCTION NcDot1qVlanCurrentEgressPortsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanCurrentEgressPortsGet (
		UINT4 u4Dot1qVlanTimeMark,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanCurrentEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanCurrentEgressPorts;
	MEMSET (pau1Dot1qVlanCurrentEgressPorts, '\0', VLAN_PORT_LENGTH);
	MEMSET (&Dot1qVlanCurrentEgressPorts, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1qVlanCurrentEgressPorts.pu1_OctetList = pau1Dot1qVlanCurrentEgressPorts;


	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qVlanCurrentTable(
				u4Dot1qVlanTimeMark,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qVlanCurrentEgressPorts(
			u4Dot1qVlanTimeMark,
			u4Dot1qVlanIndex,
			&Dot1qVlanCurrentEgressPorts );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanCurrentEgressPortsGet */

/********************************************************************
 * FUNCTION NcDot1qVlanCurrentUntaggedPortsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanCurrentUntaggedPortsGet (
		UINT4 u4Dot1qVlanTimeMark,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanCurrentUntaggedPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanCurrentUntaggedPorts;
	MEMSET (pau1Dot1qVlanCurrentUntaggedPorts, '\0', VLAN_PORT_LENGTH);
	MEMSET (&Dot1qVlanCurrentUntaggedPorts, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1qVlanCurrentUntaggedPorts.pu1_OctetList = pau1Dot1qVlanCurrentUntaggedPorts;


	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qVlanCurrentTable(
				u4Dot1qVlanTimeMark,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qVlanCurrentUntaggedPorts(
			u4Dot1qVlanTimeMark,
			u4Dot1qVlanIndex,
			&Dot1qVlanCurrentUntaggedPorts );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanCurrentUntaggedPortsGet */

/********************************************************************
 * FUNCTION NcDot1qVlanStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStatusGet (
		UINT4 u4Dot1qVlanTimeMark,
		UINT4 u4Dot1qVlanIndex,
		INT4 *pi4Dot1qVlanStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qVlanCurrentTable(
				u4Dot1qVlanTimeMark,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qVlanStatus(
			u4Dot1qVlanTimeMark,
			u4Dot1qVlanIndex,
			pi4Dot1qVlanStatus );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStatusGet */

/********************************************************************
 * FUNCTION NcDot1qVlanCreationTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanCreationTimeGet (
		UINT4 u4Dot1qVlanTimeMark,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qVlanCreationTime )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qVlanCurrentTable(
				u4Dot1qVlanTimeMark,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qVlanCreationTime(
			u4Dot1qVlanTimeMark,
			u4Dot1qVlanIndex,
			pu4Dot1qVlanCreationTime );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanCreationTimeGet */

/********************************************************************
 * FUNCTION NcDot1qVlanStaticNameSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticNameSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qVlanStaticName )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanStaticName;
	MEMSET (&Dot1qVlanStaticName, 0,  sizeof (Dot1qVlanStaticName));

	Dot1qVlanStaticName.i4_Length = (INT4) STRLEN (pDot1qVlanStaticName);
	Dot1qVlanStaticName.pu1_OctetList = pDot1qVlanStaticName;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qVlanStaticName(
			u4Dot1qVlanIndex,
			&Dot1qVlanStaticName);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticNameSet */

/********************************************************************
 * FUNCTION NcDot1qVlanStaticNameTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticNameTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pDot1qVlanStaticName )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanStaticName;
	MEMSET (&Dot1qVlanStaticName, 0,  sizeof (Dot1qVlanStaticName));

	Dot1qVlanStaticName.i4_Length = (INT4) STRLEN (pDot1qVlanStaticName);
	Dot1qVlanStaticName.pu1_OctetList = pDot1qVlanStaticName;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qVlanStaticName(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qVlanStaticName);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticNameTest */

	INT4
VlanTestModificationPort ( UINT4 u4VlanId,
		tSNMP_OCTET_STRING_TYPE * pEgressPorts,
		tSNMP_OCTET_STRING_TYPE * pUntaggedPorts,
		tSNMP_OCTET_STRING_TYPE * pForbiddenPorts)
{
	UINT1              *pau1TaggedPortList = NULL;
	UINT1               bu1RetVal = VLAN_FALSE;
	UINT1               u1PortType;
	UINT2               u2PortNumber = 0;
	BOOL1               bResult = OSIX_FALSE;
	UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
	UINT4               u4ErrorCode = 0;
	tStaticVlanEntry   *pStVlanEntry = NULL;
	tVlanPortEntry     *pVlanPortEntry = NULL;

	u4ContextId = CLI_GET_CXT_ID ();

	/* Added for pseudo wire visibility */
	/* Check if the command is a switch mode command */
	if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
	{
		/* Non Switch-mode Command */
		u4ContextId = VLAN_DEF_CONTEXT_ID;
	}

	if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
	{

		return SNMP_FAILURE;
	}

	if (VLAN_IS_VLAN_ID_VALID (u4VlanId) == VLAN_FALSE)
	{

		return SNMP_FAILURE;
	}

	if ((pEgressPorts->i4_Length <= 0) ||
			(pEgressPorts->i4_Length > VLAN_PORT_LIST_SIZE))
	{

		return SNMP_FAILURE;
	}

	if ((pForbiddenPorts->i4_Length <= 0) ||
			(pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
	{

		return SNMP_FAILURE;
	}

	if ((pUntaggedPorts->i4_Length <= 0) ||
			(pUntaggedPorts->i4_Length > VLAN_PORT_LIST_SIZE))
	{

		return SNMP_FAILURE;
	}
	/* Added for pseudo wire visibility */
	if (VlanUtilIsPortListValid (u4ContextId, pEgressPorts->pu1_OctetList,
				pEgressPorts->i4_Length) == VLAN_FALSE)
	{

		return SNMP_FAILURE;
	}

	if (VlanUtilIsPortListValid (u4ContextId, pForbiddenPorts->pu1_OctetList,
				pForbiddenPorts->i4_Length) == VLAN_FALSE)
	{

		return SNMP_FAILURE;
	}

	if (VlanUtilIsPortListValid (u4ContextId, pUntaggedPorts->pu1_OctetList,
				pUntaggedPorts->i4_Length) == VLAN_FALSE)
	{

		return SNMP_FAILURE;
	}

	/*
	 * Check if the number of egress ports is more than the
	 * the Max number of ports in the syatem
	 */

	VLAN_IS_EXCEED_MAX_PORTS (pEgressPorts->pu1_OctetList,
			pEgressPorts->i4_Length, bu1RetVal);
	if (bu1RetVal == VLAN_TRUE)
	{

		return SNMP_FAILURE;
	}

	/*
	 * Check if the number of untagged ports is more than the
	 * the Max number of ports in the syatem
	 */

	VLAN_IS_EXCEED_MAX_PORTS (pUntaggedPorts->pu1_OctetList,
			pUntaggedPorts->i4_Length, bu1RetVal);

	if (bu1RetVal == VLAN_TRUE)
	{

		return SNMP_FAILURE;
	}

	/*
	 * Check if the number of forbidden ports is more than the
	 * the Max number of ports in the syatem
	 */

	VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
			pForbiddenPorts->i4_Length, bu1RetVal);

	if (bu1RetVal == VLAN_TRUE)
	{

		return SNMP_FAILURE;
	}

	/*
	 * Check if any of ports in the forbidden port list are
	 * members of the egress port list
	 */
	VLAN_ARE_PORTS_EXCLUSIVE (pEgressPorts->pu1_OctetList,
			pForbiddenPorts->pu1_OctetList, bu1RetVal);

	if (bu1RetVal == VLAN_FALSE)
	{

		return SNMP_FAILURE;
	}

	/*
	 * The size of the Portlist array passed as arguments to the macro
	 * VLAN_IS_PORT_LIST_A_SUBSET () must always be VLAN_PORT_LIST_SIZE
	 */
	/*
	   VLAN_IS_PORT_LIST_A_SUBSET (pEgressPorts->pu1_OctetList,
	   pUntaggedPorts->pu1_OctetList, bu1RetVal);

	   if (bu1RetVal == VLAN_FALSE)
	   {

	   return SNMP_FAILURE;
	   }
	   */
	/* Valid for Both PB and PBB */
	if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
	{

		if (VlanPbIsPortTypePresentInPortList (pUntaggedPorts->pu1_OctetList,
					pUntaggedPorts->i4_Length,
					VLAN_PROVIDER_NETWORK_PORT)
				== VLAN_TRUE)
		{

			return SNMP_FAILURE;
		}

		if (VlanIsThisInterfaceVlan ((tVlanId) u4VlanId) == VLAN_TRUE)
		{
			/* An IVR exists for this vlan. */
			if (VlanPbIsAnyCustomerPortPresent (pEgressPorts->pu1_OctetList,
						pEgressPorts->i4_Length)
					== VLAN_TRUE)
			{
				/* Some customer port (CEP/CNP/PCEP/PCNP) is present in
				 * the egress port list. So don't allow this configuration.
				 * If allowed, customer will be able to configure the
				 * system and this may lead to serious security issues. */

				return SNMP_FAILURE;
			}
		}

		pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4VlanId);

		if (pStVlanEntry != NULL)
		{
			if ((pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
					|| (pStVlanEntry->u1RowStatus == VLAN_NOT_IN_SERVICE))
			{
				if (VlanPbCheckVlanServiceType ((tVlanId) u4VlanId,
							pEgressPorts->pu1_OctetList)
						== VLAN_FAILURE)
				{

					return SNMP_FAILURE;
				}
			}
		}

	}

	/* PBB Changes */
	if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
	{
		if (VlanIsPbbPortTypeValid (pEgressPorts->pu1_OctetList,
					pEgressPorts->i4_Length) == VLAN_FALSE)
		{
			/* Validate that in I-Comp its CNP and
			 * if B-Comp then CBP, PNP.
			 */

			return SNMP_FAILURE;
		}

		if (VLAN_BRIDGE_MODE () == VLAN_PBB_BCOMPONENT_BRIDGE_MODE)
		{

			if (VlanIsPbbPortTypeInPortList (pEgressPorts->pu1_OctetList,
						pUntaggedPorts->pu1_OctetList,
						pEgressPorts->i4_Length,
						VLAN_CUSTOMER_BACKBONE_PORT)
					== VLAN_FALSE)
			{

				return SNMP_FAILURE;
			}
		}
	}
	u1PortType = VLAN_TRUNK_PORT;
	if( !(AstIsPvrstStartedInContext(VLAN_DEF_CONTEXT_ID)) || (u4VlanId != 1))
	{
		if (VlanIsPortListPortTypeValid (pUntaggedPorts->pu1_OctetList,
					pUntaggedPorts->i4_Length,
					u1PortType) == VLAN_FALSE)
		{

			return SNMP_FAILURE;
		}
	}
	u1PortType = VLAN_ACCESS_PORT;
	pau1TaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1TaggedPortList == NULL)
	{

		return SNMP_FAILURE;
	}
	MEMSET (pau1TaggedPortList, 0, VLAN_PORT_LIST_SIZE);

	MEMSET (pau1TaggedPortList, 0, VLAN_PORT_LIST_SIZE);
	MEMCPY (pau1TaggedPortList, pEgressPorts->pu1_OctetList,
			pEgressPorts->i4_Length);

	VLAN_RESET_PORT_LIST (pau1TaggedPortList, pUntaggedPorts->pu1_OctetList);

	if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_UNTAGGED_PORTS_FOR_VLANS))
	{
		if (VlanIsPortListPortTypeValid (pau1TaggedPortList,
					VLAN_PORT_LIST_SIZE,
					u1PortType) == VLAN_FALSE)
		{

			UtilPlstReleaseLocalPortList (pau1TaggedPortList);
			return SNMP_FAILURE;
		}
	}
	UtilPlstReleaseLocalPortList (pau1TaggedPortList);

	if (VlanVcmSispIsPortVlanMappingValid (u4ContextId, (tVlanId) u4VlanId,
				pEgressPorts->pu1_OctetList)
			== VCM_FALSE)
	{

		return SNMP_FAILURE;
	}

	for (u2PortNumber = 1; u2PortNumber < VLAN_MAX_PORTS; u2PortNumber++)
	{
		OSIX_BITLIST_IS_BIT_SET (pEgressPorts->pu1_OctetList, u2PortNumber,
				VLAN_PORT_LIST_SIZE, bResult);

		if (bResult == OSIX_TRUE)
		{
			/* Added for pseudo wire visibility */
			pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2PortNumber);
			if (pVlanPortEntry == NULL)
			{
				continue;
			}
			if (VlanMstSispValidateInstRestriction (u4ContextId,
						pVlanPortEntry->u4IfIndex,
						(tVlanId) u4VlanId)
					!= MST_SUCCESS)
			{


				return SNMP_FAILURE;
			}
		}
	}                         
	/* For all ports present in port list */
	if (VlanTestConfigMembersOnPvlan (u4VlanId, &u4ErrorCode,
				pEgressPorts) != VLAN_SUCCESS)
	{
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

INT1 VlanConfigureStaticPorts (UINT4 u4Dot1qVlanIndex, UINT1 *pu1MemberPorts,
		UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts,INT1 PortType )
{
	UINT1              *pau1OldEgressPortList = NULL;
	UINT1              *pau1OldUntaggedPortList = NULL;
	UINT1              *pau1OldForbiddenPortList = NULL;
	UINT4               u4VlanId;
	tSNMP_OCTET_STRING_TYPE OldEgressPorts;
	tSNMP_OCTET_STRING_TYPE OldForbiddenPorts;
	tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
	tSNMP_OCTET_STRING_TYPE EgressPorts;
	tSNMP_OCTET_STRING_TYPE ForbiddenPorts;
	tSNMP_OCTET_STRING_TYPE UntaggedPorts;

	u4VlanId = (UINT4) u4Dot1qVlanIndex;


	pau1OldEgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1OldEgressPortList == NULL)
	{

		return SNMP_FAILURE;
	}
	MEMSET (pau1OldEgressPortList, 0, VLAN_PORT_LIST_SIZE);

	pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1OldUntaggedPortList == NULL)
	{

		UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
		return SNMP_FAILURE;
	}
	MEMSET (pau1OldUntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

	pau1OldForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	if (pau1OldForbiddenPortList == NULL)
	{

		UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
		UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
		return SNMP_FAILURE;
	}
	MEMSET (pau1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

	EgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
	EgressPorts.pu1_OctetList = pu1MemberPorts;

	ForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
	ForbiddenPorts.pu1_OctetList = pu1ForbiddenPorts;

	UntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
	UntaggedPorts.pu1_OctetList = pu1UntaggedPorts;

	/* OldxxxPorts are used to restore the objects in case of any failure. */
	OldEgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
	OldEgressPorts.pu1_OctetList = pau1OldEgressPortList;

	OldForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
	OldForbiddenPorts.pu1_OctetList = pau1OldForbiddenPortList;

	OldUntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
	OldUntaggedPorts.pu1_OctetList = pau1OldUntaggedPortList;

	/*
	 * Obtain the previous value, in order to restore the object
	 * in case of any failure.
	 */
	nmhGetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
	nmhGetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);
	nmhGetDot1qVlanForbiddenEgressPorts (u4VlanId, &OldForbiddenPorts);

	/*if (u4Flag == VLAN_UPDATE)
	  {*/
	/* Add new member ports to existing member portlist
	 * EgressPorts = OldEgressPorts + EgressPorts
	 * UntaggedPorts = OldUntaggedports + UntaggedPorts
	 * ForbiddenPorts = OldForbiddenPorts + ForbiddenPorts
	 * */
	/*   VLAN_ADD_PORT_LIST (EgressPorts.pu1_OctetList,
		 OldEgressPorts.pu1_OctetList);
		 VLAN_ADD_PORT_LIST (UntaggedPorts.pu1_OctetList,
		 OldUntaggedPorts.pu1_OctetList);
		 VLAN_ADD_PORT_LIST (ForbiddenPorts.pu1_OctetList,
		 OldForbiddenPorts.pu1_OctetList);
		 }*/
	/*
	 * If Untagged ports and Forbidden ports are not provided by the user,
	 * then they will take the default value of empty portlist. So need
	 * not check if Untagged ports and Forbiddne ports are provided.
	 *
	 * VlanTestModificationInfo () checks if the Member ports, Untagged ports
	 * and Forbidden ports satisfy all the dependencies. Once this is done
	 * we need not call the specific Test routines.
	 */
	if ((VlanTestModificationPort ( u4VlanId, &EgressPorts,
					&UntaggedPorts, &ForbiddenPorts))
			== SNMP_FAILURE)
	{
		UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
		UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
		UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
		return SNMP_FAILURE;
	}
	if (PortType == VLAN_STATIC_UNTAGGED_PORT)
	{
		if (nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &UntaggedPorts)
				== SNMP_FAILURE)
		{
			nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);

			UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
			UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
			UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
			return SNMP_FAILURE;
		}
	}
	else if (PortType == VLAN_STATIC_EGRESS_PORT)
	{
		if (nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &EgressPorts)
				== SNMP_FAILURE)
		{
			UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
			UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
			UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
			return SNMP_FAILURE;
		}

	}
	else if (PortType == VLAN_STATIC_FORBIDDEN_PORT)
	{
		if (nmhSetDot1qVlanForbiddenEgressPorts (u4VlanId, &ForbiddenPorts)
				== SNMP_FAILURE)
		{
			nmhSetDot1qVlanStaticEgressPorts (u4VlanId, &OldEgressPorts);
			nmhSetDot1qVlanStaticUntaggedPorts (u4VlanId, &OldUntaggedPorts);
			UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
			UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
			UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
			return SNMP_FAILURE;
		}
	}
	return SNMP_SUCCESS;

}
INT4
NcVlanFormPortList (UINT1 *pu1MemberPorts,
                  UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts, UINT4 u4Dot1qVlanIndex)
{
    UINT1              *pDelEgressPortList = NULL;
    UINT1              *pDelForbiddenPortList = NULL;
    UINT1              *pDelUntaggedPortList = NULL;
    UINT1              *pau1OldEgressPortList = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    UINT1              *pau1OldForbiddenPortList = NULL;
    UINT1              *pau1EgressPortList = NULL;
    UINT1              *pau1UntaggedPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    
    UINT1               u1Result = VLAN_FALSE;
  
    tSNMP_OCTET_STRING_TYPE OldEgressPorts;
    tSNMP_OCTET_STRING_TYPE OldForbiddenPorts;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
  

    pau1EgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1EgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: "
                  "Error in allocating memory for pau1EgressPortList\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pau1EgressPortList, 0, VLAN_PORT_LIST_SIZE);

    
    OldEgressPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldEgressPorts.pu1_OctetList = pau1EgressPortList;

    pau1ForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1ForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: "
                  "Error in allocating memory for pau1ForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    OldForbiddenPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldForbiddenPorts.pu1_OctetList = pau1ForbiddenPortList;

    pau1UntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1UntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: "
                  "Error in allocating memory for pau1UntaggedPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pau1UntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    OldUntaggedPorts.i4_Length = VLAN_PORT_LIST_SIZE;
    OldUntaggedPorts.pu1_OctetList = pau1UntaggedPortList;

    if ((nmhGetDot1qVlanStaticEgressPorts (u4Dot1qVlanIndex, &OldEgressPorts)) ==
        SNMP_FAILURE)
    {
        
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    if ((nmhGetDot1qVlanStaticUntaggedPorts (u4Dot1qVlanIndex, &OldUntaggedPorts)) ==
        SNMP_FAILURE)
    {
        
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    if ((nmhGetDot1qVlanForbiddenEgressPorts (u4Dot1qVlanIndex, &OldForbiddenPorts)) ==
        SNMP_FAILURE)
    {
        
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    pDelEgressPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDelEgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pDelEgressPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDelEgressPortList, 0, sizeof (tLocalPortList));

    pau1OldEgressPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldEgressPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pau1OldEgressPortList\r\n");
        UtilPlstReleaseLocalPortList (pDelEgressPortList);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pau1OldEgressPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1OldEgressPortList, OldEgressPorts.pu1_OctetList,
            VLAN_PORT_LIST_SIZE);

    MEMCPY (pDelEgressPortList, pu1MemberPorts, VLAN_PORT_LIST_SIZE);
    if (VlanIsPortListPortTypeValid (pDelEgressPortList, VLAN_PORT_LIST_SIZE,
                                     VLAN_TRUNK_PORT) == VLAN_FALSE)
    {
        UtilPlstReleaseLocalPortList (pDelEgressPortList);
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        CLI_SET_ERR (CLI_VLAN_TRUNK_UNTAGGED_PORT_ERR);
        return SNMP_FAILURE;
    }
    VLAN_RESET_PORT_LIST (pau1OldEgressPortList, pDelEgressPortList);
    VLAN_ARE_PORTS_INCLUSIVE (pau1OldEgressPortList, pDelEgressPortList,
                              pau1EgressPortList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Some Egress ports are not a member of given vlan \r\n");
        UtilPlstReleaseLocalPortList (pau1EgressPortList);
        UtilPlstReleaseLocalPortList (pDelEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMCPY (pu1MemberPorts, pau1OldEgressPortList, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pau1EgressPortList);
    UtilPlstReleaseLocalPortList (pDelEgressPortList);
    UtilPlstReleaseLocalPortList (pau1OldEgressPortList);

    pDelUntaggedPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDelUntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for DelUntaggedPortList \r\n");
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDelUntaggedPortList, 0, sizeof (tLocalPortList));

    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldUntaggedPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pau1OldUntaggedPortList\r\n");
        UtilPlstReleaseLocalPortList (pDelUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1OldUntaggedPortList, OldUntaggedPorts.pu1_OctetList,
            VLAN_PORT_LIST_SIZE);

    MEMCPY (pDelUntaggedPortList, pu1UntaggedPorts, VLAN_PORT_LIST_SIZE);
    VLAN_RESET_PORT_LIST (pau1OldUntaggedPortList, pDelUntaggedPortList);
    VLAN_ARE_PORTS_INCLUSIVE (pau1OldUntaggedPortList, pDelUntaggedPortList,
                              pau1UntaggedPortList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Some Untagged ports are not a member of given vlan \r\n");
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
        UtilPlstReleaseLocalPortList (pDelUntaggedPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return SNMP_FAILURE;
    }
    MEMCPY (pu1UntaggedPorts, pau1OldUntaggedPortList, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
    UtilPlstReleaseLocalPortList (pDelUntaggedPortList);
    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);

    pDelForbiddenPortList =
        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pDelForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pDelForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pDelForbiddenPortList, 0, sizeof (tLocalPortList));

    pau1OldForbiddenPortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
    if (pau1OldForbiddenPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Error in allocating memory "
                  "for pau1OldForbiddenPortList\r\n");
        UtilPlstReleaseLocalPortList (pDelForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }
    MEMSET (pau1OldForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

    MEMCPY (pau1OldForbiddenPortList, OldForbiddenPorts.pu1_OctetList,
            VLAN_PORT_LIST_SIZE);

    MEMCPY (pDelForbiddenPortList, pu1ForbiddenPorts, VLAN_PORT_LIST_SIZE);
    VLAN_RESET_PORT_LIST (pau1OldForbiddenPortList, pDelForbiddenPortList);
    VLAN_ARE_PORTS_INCLUSIVE (pau1OldForbiddenPortList, pDelForbiddenPortList,
                              pau1ForbiddenPortList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFormPortList: Some Forbidden ports are not a member of given vlan \r\n");
        UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);
        UtilPlstReleaseLocalPortList (pDelForbiddenPortList);
        UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
        return SNMP_FAILURE;
    }

    MEMCPY (pu1ForbiddenPorts, pau1OldForbiddenPortList, VLAN_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
    UtilPlstReleaseLocalPortList (pDelForbiddenPortList);
    UtilPlstReleaseLocalPortList (pau1OldForbiddenPortList);

    return SNMP_SUCCESS;
}


/********************************************************************
 * FUNCTION NcDot1qVlanStaticEgressPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticEgressPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanStaticEgressPorts ,UINT4 u4Flag)
{

	INT1 i1RetVal;
	INT1 PortType = VLAN_STATIC_EGRESS_PORT;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanStaticEgressPorts;
	MEMSET (&Dot1qVlanStaticEgressPorts, 0,  sizeof (Dot1qVlanStaticEgressPorts));

	Dot1qVlanStaticEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qVlanStaticEgressPorts);
	Dot1qVlanStaticEgressPorts.pu1_OctetList = pau1Dot1qVlanStaticEgressPorts;

	UINT1 *pau1LocalUntaggedPorts = NULL;
	UINT1 *pau1LocalForbiddenPorts = NULL;

	pau1LocalUntaggedPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

	pau1LocalForbiddenPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	MEMSET (pau1LocalUntaggedPorts, 0, VLAN_PORT_LIST_SIZE);
	MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);

	VLAN_LOCK();
    if (u4Flag == 2)  /*VLAN Delete*/
    {

        NcVlanFormPortList (pau1Dot1qVlanStaticEgressPorts,
                pau1LocalUntaggedPorts, pau1LocalForbiddenPorts,  u4Dot1qVlanIndex);

    }

	i1RetVal = VlanConfigureStaticPorts (u4Dot1qVlanIndex, pau1Dot1qVlanStaticEgressPorts,pau1LocalUntaggedPorts,pau1LocalForbiddenPorts,PortType);
	/*
	   i1RetVal = nmhSetDot1qVlanStaticEgressPorts(
	   u4Dot1qVlanIndex,
	   &Dot1qVlanStaticEgressPorts);
	   */

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticEgressPortsSet */


/********************************************************************
 * FUNCTION NcDot1qVlanStaticEgressPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticEgressPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanStaticEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanStaticEgressPorts;
	MEMSET (&Dot1qVlanStaticEgressPorts, 0,  sizeof (Dot1qVlanStaticEgressPorts));

	Dot1qVlanStaticEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qVlanStaticEgressPorts);
	Dot1qVlanStaticEgressPorts.pu1_OctetList = pau1Dot1qVlanStaticEgressPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qVlanStaticEgressPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qVlanStaticEgressPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticEgressPortsTest */

/********************************************************************
 * FUNCTION NcDot1qVlanForbiddenEgressPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanForbiddenEgressPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanForbiddenEgressPorts )
{

	INT1 i1RetVal;
	INT1 PortType = VLAN_STATIC_FORBIDDEN_PORT;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanForbiddenEgressPorts;

	UINT1 *pau1LocalUntaggedPorts = NULL;
	UINT1 *pau1Dot1qVlanStaticEgressPorts = NULL;

	pau1LocalUntaggedPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

	pau1Dot1qVlanStaticEgressPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	MEMSET (pau1LocalUntaggedPorts, 0, VLAN_PORT_LIST_SIZE);
	MEMSET (pau1Dot1qVlanStaticEgressPorts, 0, VLAN_PORT_LIST_SIZE);


	MEMSET (&Dot1qVlanForbiddenEgressPorts, 0,  sizeof (Dot1qVlanForbiddenEgressPorts));

	Dot1qVlanForbiddenEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qVlanForbiddenEgressPorts);
	Dot1qVlanForbiddenEgressPorts.pu1_OctetList = pau1Dot1qVlanForbiddenEgressPorts;

	VLAN_LOCK();

	i1RetVal = VlanConfigureStaticPorts (u4Dot1qVlanIndex, pau1Dot1qVlanStaticEgressPorts,pau1LocalUntaggedPorts,pau1Dot1qVlanForbiddenEgressPorts,PortType);

	/*
	   i1RetVal = nmhSetDot1qVlanForbiddenEgressPorts(
	   u4Dot1qVlanIndex,
	   &Dot1qVlanForbiddenEgressPorts);

*/

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanForbiddenEgressPortsSet */

/********************************************************************
 * FUNCTION NcDot1qVlanForbiddenEgressPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanForbiddenEgressPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanForbiddenEgressPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanForbiddenEgressPorts;
	MEMSET (&Dot1qVlanForbiddenEgressPorts, 0,  sizeof (Dot1qVlanForbiddenEgressPorts));

	Dot1qVlanForbiddenEgressPorts.i4_Length = (INT4) STRLEN (pau1Dot1qVlanForbiddenEgressPorts);
	Dot1qVlanForbiddenEgressPorts.pu1_OctetList = pau1Dot1qVlanForbiddenEgressPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qVlanForbiddenEgressPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qVlanForbiddenEgressPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanForbiddenEgressPortsTest */

/********************************************************************
 * FUNCTION NcDot1qVlanStaticUntaggedPortsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticUntaggedPortsSet (
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanStaticUntaggedPorts )
{

	INT1 i1RetVal;
	INT1 PortType = VLAN_STATIC_UNTAGGED_PORT;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanStaticUntaggedPorts;
	MEMSET (&Dot1qVlanStaticUntaggedPorts, 0,  sizeof (Dot1qVlanStaticUntaggedPorts));

	Dot1qVlanStaticUntaggedPorts.i4_Length = (INT4) STRLEN (pau1Dot1qVlanStaticUntaggedPorts);
	Dot1qVlanStaticUntaggedPorts.pu1_OctetList = pau1Dot1qVlanStaticUntaggedPorts;

	UINT1 *pau1Dot1qVlanStaticEgressPorts = NULL;
	UINT1 *pau1LocalForbiddenPorts = NULL;

	pau1Dot1qVlanStaticEgressPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);

	pau1LocalForbiddenPorts =
		UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
	MEMSET (pau1Dot1qVlanStaticEgressPorts, 0, VLAN_PORT_LIST_SIZE);
	MEMSET (pau1LocalForbiddenPorts, 0, VLAN_PORT_LIST_SIZE);



	VLAN_LOCK();

	i1RetVal = VlanConfigureStaticPorts (u4Dot1qVlanIndex, pau1Dot1qVlanStaticEgressPorts,pau1Dot1qVlanStaticUntaggedPorts,pau1LocalForbiddenPorts,PortType);

	/*    i1RetVal = nmhSetDot1qVlanStaticUntaggedPorts(
		  u4Dot1qVlanIndex,
		  &Dot1qVlanStaticUntaggedPorts);*/

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticUntaggedPortsSet */

/********************************************************************
 * FUNCTION NcDot1qVlanStaticUntaggedPortsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticUntaggedPortsTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		UINT1 *pau1Dot1qVlanStaticUntaggedPorts )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1qVlanStaticUntaggedPorts;
	MEMSET (&Dot1qVlanStaticUntaggedPorts, 0,  sizeof (Dot1qVlanStaticUntaggedPorts));

	Dot1qVlanStaticUntaggedPorts.i4_Length = (INT4) STRLEN (pau1Dot1qVlanStaticUntaggedPorts);
	Dot1qVlanStaticUntaggedPorts.pu1_OctetList = pau1Dot1qVlanStaticUntaggedPorts;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qVlanStaticUntaggedPorts(pu4Error,
			u4Dot1qVlanIndex,
			&Dot1qVlanStaticUntaggedPorts);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticUntaggedPortsTest */

/********************************************************************
 * FUNCTION NcDot1qVlanStaticRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticRowStatusSet (
		UINT4 u4Dot1qVlanIndex,
		INT4 i4Dot1qVlanStaticRowStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qVlanStaticRowStatus(
			u4Dot1qVlanIndex,
			i4Dot1qVlanStaticRowStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1qVlanStaticRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qVlanStaticRowStatusTest (UINT4 *pu4Error,
		UINT4 u4Dot1qVlanIndex,
		INT4 i4Dot1qVlanStaticRowStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qVlanStaticRowStatus(pu4Error,
			u4Dot1qVlanIndex,
			i4Dot1qVlanStaticRowStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qVlanStaticRowStatusTest */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortInFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortInFramesGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qTpVlanPortInFrames )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortInFrames(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			pu4Dot1qTpVlanPortInFrames );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpVlanPortInFramesGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortOutFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortOutFramesGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qTpVlanPortOutFrames )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortOutFrames(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			pu4Dot1qTpVlanPortOutFrames );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpVlanPortOutFramesGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortInDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortInDiscardsGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qTpVlanPortInDiscards )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortInDiscards(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			pu4Dot1qTpVlanPortInDiscards );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpVlanPortInDiscardsGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortInOverflowFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortInOverflowFramesGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qTpVlanPortInOverflowFrames )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortInOverflowFrames(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			pu4Dot1qTpVlanPortInOverflowFrames );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpVlanPortInOverflowFramesGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortOutOverflowFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortOutOverflowFramesGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qTpVlanPortOutOverflowFrames )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortOutOverflowFrames(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			pu4Dot1qTpVlanPortOutOverflowFrames );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpVlanPortOutOverflowFramesGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortInOverflowDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortInOverflowDiscardsGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		UINT4 *pu4Dot1qTpVlanPortInOverflowDiscards )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortInOverflowDiscards(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			pu4Dot1qTpVlanPortInOverflowDiscards );

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qTpVlanPortInOverflowDiscardsGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortHCInFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortHCInFramesGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		unsigned long long *pu8Dot1qTpVlanPortHCInFrames )
{

	INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE Dot1qTpVlanPortHCInFrames;

	MEMSET (&Dot1qTpVlanPortHCInFrames, 0, sizeof(tSNMP_COUNTER64_TYPE));

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortHCInFrames(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			&Dot1qTpVlanPortHCInFrames );

	VLAN_UNLOCK();

	*pu8Dot1qTpVlanPortHCInFrames = ((*pu8Dot1qTpVlanPortHCInFrames | Dot1qTpVlanPortHCInFrames.msn) << 32) | Dot1qTpVlanPortHCInFrames.lsn;

	return i1RetVal;


} /* NcDot1qTpVlanPortHCInFramesGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortHCOutFramesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortHCOutFramesGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		unsigned long long *pu8Dot1qTpVlanPortHCOutFrames )
{

	INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE Dot1qTpVlanPortHCOutFrames;

	MEMSET (&Dot1qTpVlanPortHCOutFrames, 0, sizeof(tSNMP_COUNTER64_TYPE));

	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortHCOutFrames(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			&Dot1qTpVlanPortHCOutFrames );
	VLAN_UNLOCK();

	*pu8Dot1qTpVlanPortHCOutFrames = ((*pu8Dot1qTpVlanPortHCOutFrames | Dot1qTpVlanPortHCOutFrames.msn) << 32) | Dot1qTpVlanPortHCOutFrames.lsn;


	return i1RetVal;


} /* NcDot1qTpVlanPortHCOutFramesGet */

/********************************************************************
 * FUNCTION NcDot1qTpVlanPortHCInDiscardsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qTpVlanPortHCInDiscardsGet (
		INT4 i4Dot1dBasePort,
		UINT4 u4Dot1qVlanIndex,
		unsigned long long *pu8Dot1qTpVlanPortHCInDiscards )
{

	INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE Dot1qTpVlanPortHCInDiscards;

	MEMSET (&Dot1qTpVlanPortHCInDiscards, 0, sizeof(tSNMP_COUNTER64_TYPE));


	VLAN_LOCK();
	if (nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable(
				i4Dot1dBasePort,
				u4Dot1qVlanIndex) == SNMP_FAILURE)

	{
		VLAN_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetDot1qTpVlanPortHCInDiscards(
			i4Dot1dBasePort,
			u4Dot1qVlanIndex,
			&Dot1qTpVlanPortHCInDiscards );

	VLAN_UNLOCK();

	*pu8Dot1qTpVlanPortHCInDiscards = ((*pu8Dot1qTpVlanPortHCInDiscards | Dot1qTpVlanPortHCInDiscards.msn) << 32) | Dot1qTpVlanPortHCInDiscards.lsn;

	return i1RetVal;


} /* NcDot1qTpVlanPortHCInDiscardsGet */

/********************************************************************
 * FUNCTION NcDot1qConstraintTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintTypeSet (
		UINT4 u4Dot1qConstraintVlan,
		INT4 i4Dot1qConstraintSet,
		INT4 i4Dot1qConstraintType )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qConstraintType(
			u4Dot1qConstraintVlan,
			i4Dot1qConstraintSet,
			i4Dot1qConstraintType);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintTypeSet */

/********************************************************************
 * FUNCTION NcDot1qConstraintTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintTypeTest (UINT4 *pu4Error,
		UINT4 u4Dot1qConstraintVlan,
		INT4 i4Dot1qConstraintSet,
		INT4 i4Dot1qConstraintType )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qConstraintType(pu4Error,
			u4Dot1qConstraintVlan,
			i4Dot1qConstraintSet,
			i4Dot1qConstraintType);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintTypeTest */

/********************************************************************
 * FUNCTION NcDot1qConstraintStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintStatusSet (
		UINT4 u4Dot1qConstraintVlan,
		INT4 i4Dot1qConstraintSet,
		INT4 i4Dot1qConstraintStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1qConstraintStatus(
			u4Dot1qConstraintVlan,
			i4Dot1qConstraintSet,
			i4Dot1qConstraintStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintStatusSet */

/********************************************************************
 * FUNCTION NcDot1qConstraintStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1qConstraintStatusTest (UINT4 *pu4Error,
		UINT4 u4Dot1qConstraintVlan,
		INT4 i4Dot1qConstraintSet,
		INT4 i4Dot1qConstraintStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1qConstraintStatus(pu4Error,
			u4Dot1qConstraintVlan,
			i4Dot1qConstraintSet,
			i4Dot1qConstraintStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1qConstraintStatusTest */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanSetProtocolGroup                               */
/*                                                                           */
/*     DESCRIPTION      : This function will adds/removes entries from       */
/*                        protocol group table                               */
/*                                                                           */
/*     INPUT            : u1FrameType  - Frame encapsulation type            */
/*                        u4GroupId    - Protocol group identifier           */
/*                        pu1ProtocolValue - pointer to other protocol value */
/*                        u1Length     - Length of the other protocol value  */
/*                        u1Action     - Action to be performed (Add/Remove) */
/*                                                                           */
/*     OUTPUT           : no output                                          */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSetProtocolGroup ( UINT1 u1FrameType,
                         UINT4 u4GroupId, UINT1 *pu1ProtocolValue,
                         UINT1 u1Length, UINT1 u1Action)
{
    INT4                i4GroupIdentifier;
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE ProtocolGroup;
    UINT1               au1ProtoValue[VLAN_MAX_PROTO_SIZE];

    MEMSET (au1ProtoValue, 0, sizeof (au1ProtoValue));
    ProtocolGroup.i4_Length = u1Length;

    ProtocolGroup.pu1_OctetList = au1ProtoValue;

    MEMCPY (ProtocolGroup.pu1_OctetList,
            pu1ProtocolValue, ProtocolGroup.i4_Length);

    switch (u1Action)
    {
        case VLAN_SET_CMD:
            if (nmhGetDot1vProtocolGroupId (u1FrameType,
                                            &ProtocolGroup,
                                            &i4GroupIdentifier) == SNMP_FAILURE)
            {

                if (nmhTestv2Dot1vProtocolGroupRowStatus (&u4ErrCode,
                                                          u1FrameType,
                                                          &ProtocolGroup,
                                                          VLAN_CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    return SNMP_FAILURE;
                }


                if (nmhTestv2Dot1vProtocolGroupId (&u4ErrCode, u1FrameType,
                                                   &ProtocolGroup,
                                                   u4GroupId) == SNMP_FAILURE)
                {
					nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_DESTROY);
                    return SNMP_FAILURE;
                }

                if (nmhSetDot1vProtocolGroupId (u1FrameType, &ProtocolGroup,
                                                u4GroupId) == SNMP_FAILURE)
                {
                    nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_DESTROY);
                    return SNMP_FAILURE;
                }

                if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_ACTIVE) ==
                    SNMP_FAILURE)
                {
                    if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                           &ProtocolGroup,
                                                           VLAN_DESTROY) ==
                        SNMP_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
						return SNMP_FAILURE;
                }
            }
            else
            {
                if ((UINT4) i4GroupIdentifier != u4GroupId)
                {
                    if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                           &ProtocolGroup,
                                                           VLAN_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        
                        return SNMP_FAILURE;
                    }

                    if (nmhTestv2Dot1vProtocolGroupId (&u4ErrCode, u1FrameType,
                                                       &ProtocolGroup,
                                                       u4GroupId)
                        == SNMP_FAILURE)
                    {
						return SNMP_FAILURE;
                    }

                    if (nmhSetDot1vProtocolGroupId (u1FrameType, &ProtocolGroup,
                                                    u4GroupId) == SNMP_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }

                    nmhSetDot1vProtocolGroupRowStatus (u1FrameType,
                                                       &ProtocolGroup,
                                                       VLAN_ACTIVE);
                }
            }

            break;

#if 0
        case VLAN_NO_CMD:

            if (nmhTestv2Dot1vProtocolGroupRowStatus (&u4ErrCode, u1FrameType,
                                                      &ProtocolGroup,
                                                      VLAN_DESTROY)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetDot1vProtocolGroupRowStatus (u1FrameType, &ProtocolGroup,
                                                   VLAN_DESTROY)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;
#endif

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/********************************************************************
 * FUNCTION NcDot1vProtocolGroupIdSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolGroupIdSet (
		INT4 i4Dot1vProtocolTemplateFrameType,
		UINT1 *au1Dot1vProtocolTemplateProtocolValue,
		INT4 i4Dot1vProtocolGroupId )
{

	INT1 i1RetVal;
	UINT4 u4GroupId = (UINT4)i4Dot1vProtocolGroupId;
	UINT1 u1FrameType = (UINT1)i4Dot1vProtocolTemplateFrameType;
	UINT1 u1Action = 1;
	UINT1 u1Length =2;
	if (u1FrameType == VLAN_PORT_PROTO_SNAPOTHER)
	u1Length = 5;
	tSNMP_OCTET_STRING_TYPE Dot1vProtocolTemplateProtocolValue;
	MEMSET (&Dot1vProtocolTemplateProtocolValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1vProtocolTemplateProtocolValue.pu1_OctetList = au1Dot1vProtocolTemplateProtocolValue;


	VLAN_LOCK();
	i1RetVal = VlanSetProtocolGroup ( u1FrameType,
                         u4GroupId,au1Dot1vProtocolTemplateProtocolValue,
                         u1Length, u1Action);

/*	i1RetVal = nmhSetDot1vProtocolGroupId(
			i4Dot1vProtocolTemplateFrameType,
			&Dot1vProtocolTemplateProtocolValue,
			i4Dot1vProtocolGroupId);
*/
	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolGroupIdSet */

/********************************************************************
 * FUNCTION NcDot1vProtocolGroupIdTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolGroupIdTest (UINT4 *pu4Error,
		INT4 i4Dot1vProtocolTemplateFrameType,
		UINT1 au1Dot1vProtocolTemplateProtocolValue,
		INT4 i4Dot1vProtocolGroupId )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1vProtocolTemplateProtocolValue;
	MEMSET (&Dot1vProtocolTemplateProtocolValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1vProtocolTemplateProtocolValue.pu1_OctetList = &au1Dot1vProtocolTemplateProtocolValue;


	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1vProtocolGroupId(pu4Error,
			i4Dot1vProtocolTemplateFrameType,
			&Dot1vProtocolTemplateProtocolValue,
			i4Dot1vProtocolGroupId);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolGroupIdTest */

/********************************************************************
 * FUNCTION NcDot1vProtocolGroupRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolGroupRowStatusSet (
		INT4 i4Dot1vProtocolTemplateFrameType,
		UINT1 *au1Dot1vProtocolTemplateProtocolValue,
		INT4 i4Dot1vProtocolGroupRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1vProtocolTemplateProtocolValue;
	MEMSET (&Dot1vProtocolTemplateProtocolValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1vProtocolTemplateProtocolValue.pu1_OctetList = au1Dot1vProtocolTemplateProtocolValue;


	VLAN_LOCK();
	i1RetVal = nmhSetDot1vProtocolGroupRowStatus(
			i4Dot1vProtocolTemplateFrameType,
			&Dot1vProtocolTemplateProtocolValue,
			i4Dot1vProtocolGroupRowStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolGroupRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1vProtocolGroupRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolGroupRowStatusTest (UINT4 *pu4Error,
		INT4 i4Dot1vProtocolTemplateFrameType,
		UINT1 *au1Dot1vProtocolTemplateProtocolValue,
		INT4 i4Dot1vProtocolGroupRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE Dot1vProtocolTemplateProtocolValue;
	MEMSET (&Dot1vProtocolTemplateProtocolValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot1vProtocolTemplateProtocolValue.pu1_OctetList = au1Dot1vProtocolTemplateProtocolValue;


	VLAN_LOCK();

	i1RetVal = nmhTestv2Dot1vProtocolGroupRowStatus(pu4Error,
			i4Dot1vProtocolTemplateFrameType,
			&Dot1vProtocolTemplateProtocolValue,
			i4Dot1vProtocolGroupRowStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolGroupRowStatusTest */

/********************************************************************
 * FUNCTION NcDot1vProtocolPortGroupVidSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolPortGroupVidSet (
		INT4 i4Dot1dBasePort,
		INT4 i4Dot1vProtocolPortGroupId,
		INT4 i4Dot1vProtocolPortGroupVid )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1vProtocolPortGroupVid(
			i4Dot1dBasePort,
			i4Dot1vProtocolPortGroupId,
			i4Dot1vProtocolPortGroupVid);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolPortGroupVidSet */

/********************************************************************
 * FUNCTION NcDot1vProtocolPortGroupVidTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolPortGroupVidTest (UINT4 *pu4Error,
		INT4 i4Dot1dBasePort,
		INT4 i4Dot1vProtocolPortGroupId,
		INT4 i4Dot1vProtocolPortGroupVid )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1vProtocolPortGroupVid(pu4Error,
			i4Dot1dBasePort,
			i4Dot1vProtocolPortGroupId,
			i4Dot1vProtocolPortGroupVid);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolPortGroupVidTest */

/********************************************************************
 * FUNCTION NcDot1vProtocolPortRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolPortRowStatusSet (
		INT4 i4Dot1dBasePort,
		INT4 i4Dot1vProtocolPortGroupId,
		INT4 i4Dot1vProtocolPortRowStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhSetDot1vProtocolPortRowStatus(
			i4Dot1dBasePort,
			i4Dot1vProtocolPortGroupId,
			i4Dot1vProtocolPortRowStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolPortRowStatusSet */

/********************************************************************
 * FUNCTION NcDot1vProtocolPortRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcDot1vProtocolPortRowStatusTest (UINT4 *pu4Error,
		INT4 i4Dot1dBasePort,
		INT4 i4Dot1vProtocolPortGroupId,
		INT4 i4Dot1vProtocolPortRowStatus )
{

	INT1 i1RetVal;

	VLAN_LOCK();
	i1RetVal = nmhTestv2Dot1vProtocolPortRowStatus(pu4Error,
			i4Dot1dBasePort,
			i4Dot1vProtocolPortGroupId,
			i4Dot1vProtocolPortRowStatus);

	VLAN_UNLOCK();
	return i1RetVal;


} /* NcDot1vProtocolPortRowStatusTest */

/* END i_Q_BRIDGE_MIB.c */
