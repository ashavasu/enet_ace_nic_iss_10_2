/******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevcli.c,v 1.9 2016/07/22 06:46:31 siva Exp $
 *
 * Description     : Action routines for EVB CLI commands       
******************************************************************/
 
#ifndef __VLNEVCLI_C__
#define __VLNEVCLI_C__
#include "vlaninc.h"
#include "fsmid1cli.h"
#include "std1evcli.h"
#include "std1llcli.h"


/****************************************************************************/
/*     FUNCTION NAME    : cli_process_vlan_evb_show_cmd                     */
/*                                                                          */
/*     DESCRIPTION      : This function takes in variable no. of arguments  */
/*                        and configures the parameters based on the given  */
/*                        values                                            */
/*                                                                          */
/*     INPUT            : CliHandle -  CLIHandler                           */
/*                        u4Command -  Command Identifier                   */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/


INT4
cli_process_vlan_evb_cmd(tCliHandle CliHandle, UINT4 u4Command, ...)
{

    va_list             ap;
    UINT4              *args[VLAN_MAX_ARGS];
    UINT1              *pu1Inst       = NULL;
    UINT4               u4ContextId   = CLI_GET_CXT_ID ();
    UINT4               u4ErrCode     = 0;
    UINT4               u4Trace       = 0;
    INT4                i4IfIndex     = 0;
    INT4                i4RetStatus   = 0;
    UINT2               u2LocalPortId = 0;
    INT1                argno         = 0;

    if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        /* Setting default context . in switch mode */
        u4ContextId = 0;
        CLI_SET_CXT_ID(0);
    }

    CLI_SET_ERR (0);
    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);

    VLAN_LOCK ();
    
    if (VLAN_FAILURE == VlnEvbCliSelectContextOnMode
            (CliHandle, u4Command, &u4ContextId, &u2LocalPortId))
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    } 
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    if (pu1Inst != VLAN_EVB_ZERO)
    {
        i4IfIndex = (INT4)CLI_PTR_TO_U4 (pu1Inst);
        if (VLAN_SUCCESS != VcmGetContextInfoFromIfIndex
                ((UINT4) i4IfIndex, &u4ContextId, &u2LocalPortId))
        {
            CliPrintf (CliHandle,
                    "\r%% Port not mapped to any of the context \r\n");
            i4RetStatus = CLI_FAILURE;
        }
    }
    /* Walk through the rest of the arguements and store in args array.
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    if (u4Command != CLI_VLAN_EVB_NO_SHUT)
    {
        if ((gEvbGlobalInfo.apEvbContextInfo[u4ContextId] == NULL) ||
            (VLAN_EVB_SYSTEM_STATUS(u4ContextId) == VLAN_EVB_SYSTEM_SHUTDOWN))
        {
            CliPrintf (CliHandle, "\r%% EVB Module is Shutdown\r\n");
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }
    switch (u4Command)
    {
        case CLI_VLAN_EVB_SHUT:
            i4RetStatus = VlanEvbCliSetSysStatus(CliHandle, 
                                                 VLAN_EVB_SYSTEM_SHUTDOWN);
            break;

        case CLI_VLAN_EVB_NO_SHUT:
            i4RetStatus = VlanEvbCliSetSysStatus(CliHandle, 
                                    VLAN_EVB_SYSTEM_START);
            break;

        case CLI_VLAN_EVB_MOD_STATUS:
            i4RetStatus = VlanEvbCliModStatus(CliHandle,
                               CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_EVB_DEBUG:
            i4RetStatus = VlanEvbCliSetDebug (CliHandle, 
                                             CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VLAN_EVB_NO_DEBUG:
            /* Fetching the existing trace and reset for NO command */
            nmhGetFsMIEvbSystemTraceLevel ((INT4) u4ContextId, &u4Trace);
            u4Trace &= (~CLI_PTR_TO_U4 (args[0]));

            i4RetStatus = VlanEvbCliSetDebug (CliHandle, u4Trace); 
            break;

        case CLI_VLAN_EVB_CLR_STATS:
            i4RetStatus = VlanEvbCliClearEvbStats (CliHandle);            
            break;

        case CLI_VLAN_EVB_TRAP_STATUS:
            i4RetStatus = VlanEvbCliSetTrapStatus(CliHandle, 
                                   CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_EVB_CDCP_ENABLE: 
            i4RetStatus = VlanEvbCdcpEnable(CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_VLAN_EVB_CDCP_DISABLE: 
            i4RetStatus = VlanEvbCdcpDisable(CliHandle);
            break;

        case CLI_VLAN_CDCP_MAX_SCHANNEL:
            i4RetStatus = VlanEvbCliMaxSchannel(CliHandle,((INT4)(*args[0])),
                                                           (UINT4)u2LocalPortId);
            break;

        case CLI_VLAN_EVB_SERV_VLAN_POOL:
            /* Both Lower limit and higher limit need to be configured */

            if((NULL == args[0]) && (NULL != args[1])) 
            {
                i4RetStatus = VlanEvbServVlanPoolConfig(CliHandle, 
                                 (UINT4)u2LocalPortId, 0, *(args[1]));
            }
            else if ((NULL == args[1]) && (NULL != args[0])) 
            {
                i4RetStatus = VlanEvbServVlanPoolConfig(CliHandle, 
                                 (UINT4)u2LocalPortId, *(args[0]), 0);
            }
            else
            {
                i4RetStatus = VlanEvbServVlanPoolConfig(CliHandle,
                                 (UINT4)u2LocalPortId, *(args[0]), *(args[1]));

            }

            break;

        case CLI_VLAN_EVB_NO_SERV_VLAN_POOL:
            i4RetStatus = VlanEvbServVlanPoolConfig(CliHandle, (UINT4)u2LocalPortId, 
                                 VLAN_EVB_DEF_CDCP_SVID_POOL_LOW,
                                 VLAN_EVB_DEF_CDCP_SVID_POOL_HIGH);
              
            break;

        case CLI_VLAN_EVB_STORAGE_TYPE:
            i4RetStatus = VlanEvbStorageType(CliHandle, (UINT4)u2LocalPortId,
                               CLI_PTR_TO_I4(args[0]));
            break;

        case CLI_VLAN_EVB_NO_STORAGE_TYPE:
            i4RetStatus = VlanEvbStorageType(CliHandle, (UINT4)u2LocalPortId,
                            VLAN_EVB_STORAGE_TYPE_NON_VOLATILE);

            break;

        case CLI_VLAN_EVB_SCH_MODE:
            i4RetStatus = VlanEvbConfigSchMode(CliHandle, 
                               CLI_PTR_TO_I4(args[0]));
            break;
    
        case CLI_VLAN_EVB_NO_SCH_MODE:
            i4RetStatus = VlanEvbConfigSchMode(CliHandle,
                            VLAN_EVB_DEF_SCH_MODE);
            break;
    
        case CLI_VLAN_EVB_SCH_SERV_VLAN:
            if (args[0] != NULL)
            {
                i4RetStatus = VlanEvbAddScidEntry(CliHandle, (UINT4)u2LocalPortId,
                         (*args[0]),
                         (*args[1]));
            }
            else
            {
                /* Since the SCHID is not given as user input, sending it
                 * * as zero*/
                i4RetStatus = VlanEvbAddScidEntry(CliHandle, (UINT4)u2LocalPortId, 0,
                                                   (*args[1]));
            }
            break;

        case CLI_VLAN_EVB_NO_SCH_SERV_VLAN:
            if (args[0] != NULL)
            {
                i4RetStatus = VlanEvbDelScidEntry(CliHandle, 
                                                 (UINT4)u2LocalPortId, 
                                                 *(args[0]), 
                                                 *(args[1]));
            }
            else
            {
                /* Since the SCHID is not given as user input, sending it
                 * * as zero*/
                i4RetStatus = VlanEvbDelScidEntry (CliHandle,
                                                  (UINT4)u2LocalPortId, 0,
                                                   (*args[1]));
            }
            break;

        case CLI_VLAN_EVB_CDCP_TLV_SELECT:
            i4RetStatus = VlanEvbTlvSelect(CliHandle);
            break;
        case CLI_VLAN_EVB_CDCP_NO_TLV_SELECT:
            i4RetStatus = VlanEvbNoTlvSlct(CliHandle);
            break;
        case CLI_VLAN_EVB_SCH_L2_FILTER_STATUS:
            i4RetStatus = VlanEvbConfigSChL2FilterStatus (CliHandle ,
                                                CLI_PTR_TO_I4 (args[0]));
            break;

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;

    }
    VlanReleaseContext ();
    
    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS) &&
            (i4RetStatus == CLI_FAILURE))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_VLAN_EVB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", EvbCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4)i4RetStatus);
    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : cli_process_vlan_evb_show_cmd                     */
/*                                                                          */
/*     DESCRIPTION      : This function takes in variable no. of arguments  */
/*                        and displays the EVB related information.         */
/*                                                                          */
/*     INPUT            : CliHandle -  CLIHandler                           */
/*                        u4Command -  Command Identifier                   */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
cli_process_vlan_evb_show_cmd(tCliHandle CliHandle, UINT4 u4Command, ...)
{

    UINT4               u4ContextId = VLAN_EVB_ZERO;
    UINT2               u2LocalPortId = VLAN_EVB_ZERO;
    va_list             ap;
    UINT4              *args[VLAN_MAX_ARGS];
    INT4                i4Inst = VLAN_EVB_ZERO;
    UINT4               u4IfIndex = VLAN_EVB_ZERO;
    INT4                i4RetStatus = VLAN_EVB_ZERO;
    INT1                argno = VLAN_EVB_ZERO;


    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);

    VLAN_LOCK ();

    if (VLAN_FAILURE == VlnEvbCliSelectContextOnMode
            (CliHandle, u4Command, &u4ContextId, &u2LocalPortId))
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
        if (VLAN_SUCCESS != VcmGetContextInfoFromIfIndex
                (u4IfIndex, &u4ContextId, &u2LocalPortId))
        {
            CliPrintf (CliHandle,
                    "\r%% Port not mapped to any of the context \r\n");
            i4RetStatus = CLI_FAILURE;
        }
    }

    /* Walk through the rest of the arguements and store in args array.
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);
    switch (u4Command)
    {
        case CLI_VLAN_EVB_SHOW_DETAIL:
            /* args[0] - Context Id */
            VlanEvbShowEvbDetail(CliHandle, u4ContextId);
            break;
        case CLI_VLAN_EVB_UAP_SHOW_DESC:
            /* args[0] - Context Id */
            VlanEvbShow(CliHandle,u4IfIndex,CLI_VLAN_EVB_UAP_SHOW_DESC);
            break;
        case CLI_VLAN_EVB_UAP_SHOW_STATS :
            VlanEvbShow(CliHandle,u4IfIndex,CLI_VLAN_EVB_UAP_SHOW_STATS);
            break;
        case CLI_VLAN_EVB_SHOW_LOCAL_CDCP_TLV:
            VlanEvbShow(CliHandle,u4IfIndex,CLI_VLAN_EVB_SHOW_LOCAL_CDCP_TLV);
            break;
        case CLI_VLAN_EVB_SHOW_REMOTE_CDCP_TLV:
            VlanEvbShow(CliHandle,u4IfIndex,CLI_VLAN_EVB_SHOW_REMOTE_CDCP_TLV);
            break;
        case CLI_VLAN_EVB_SHOW_SCH_INT:
            VlanEvbShow(CliHandle,u4IfIndex,CLI_VLAN_EVB_SHOW_SCH_INT);
            break;
        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;

    }
    VlanReleaseContext ();
    CLI_SET_CMD_STATUS ((UINT4)i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);
    UNUSED_PARAM (args);

    return CLI_SUCCESS;

}


/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCliSelectContextOnMode                       */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd       - CLI Command.                       */
/*                          CliHandle   - CLI Handler                        */
/*                          pu4Context  - Context Id                         */
/*                          pu2LocalPort- Local port ID                      */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu2LocalPort - Local Port Number.                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlnEvbCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                            UINT4 *pu4Context, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId = VLAN_EVB_ZERO;
    INT4                i4PortId = VLAN_EVB_ZERO;
    UINT1               u1IntfCmdFlag = VLAN_FALSE;  /* This flag is used in MI
                                                       case, to know whether
                                                       the command is a
                                                       interface mode command
                                                       or vlan mode command. */

    /* For debug commands the context-name will be present in args[0], so
     * the select context will be done seperately within the
     * switch statement*/
    if((u4Cmd == CLI_VLAN_EVB_DEBUG) || (u4Cmd == CLI_VLAN_EVB_NO_DEBUG))
    {
        return CLI_SUCCESS;
    }

    *pu4Context = VLAN_EVB_ZERO;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = (UINT4) CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != VLAN_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI.
         * If the command is a vlan mode command then the context-id won't be
         * VLAN_CLI_INVALID_CONTEXT, So this is an interface mode command.
         * Now by refering this flag we have to get the context-id and
         * local port number from the IfIndex (CLI_GET_IFINDEX). */
        u1IntfCmdFlag = VLAN_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    if (i4PortId == CLI_ERROR)
    {
        return CLI_FAILURE;
    }
    /* In SI both the i4PortId and Localport are same. So no need to call
     * VlanGetContextInfoFromIfIndex. */
    *pu2LocalPort = (UINT2) i4PortId;

    if (VlanVcmGetSystemMode (VLANMOD_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == VLAN_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (VlanVcmGetContextInfoFromIfIndex
                    ((UINT4) i4PortId, pu4Context, pu2LocalPort)
                    != VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return CLI_FAILURE;
                }
            }
        }
    }

    /* Since we are calling SI nmh routine for Configuration commands
     * we have to do SelectContext for Config commands*/
    /* NOTE: check whether rqrd */
/*    if (VlanEvbSelectContext (*pu4Context) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return CLI_FAILURE;
    }
*/
    if ((VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE) &&
        (u4Cmd != CLI_VLAN_NO_SHUT))
    {
        CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
        VlanReleaseContext ();
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbCliSetSysStatus                               */
/*                                                                           */
/* Description        : CLI utility to enable or shutdown EVB system         */
/*                                                                           */
/*   INPUT            : i4Status - Vlan EVB System status                    */
/*                      VLAN_EVB_SYSTEM_START/VLAN_EVB_SYSTEM_SHUTDOWN       */
/*                                                                           */
/*   OUTPUT           : CliHandle - Contains error messages                  */
/*                                                                           */
/*   RETURNS          : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/****************************************************************************/

INT4
VlanEvbCliSetSysStatus(tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = VLAN_EVB_ZERO;
    INT4                i4ContextId = VLAN_EVB_ZERO;

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());
   
    /* Validating EVB System status */

    if (SNMP_FAILURE == nmhTestv2FsMIEvbSystemControl
                        (&u4ErrorCode, i4ContextId, i4Status))
    {
        CliPrintf (CliHandle, 
            "\r%%EVB system control status validation failed.\r\n");
        return CLI_FAILURE;
    }

    /* Setting EVB Start or Shutdown status.
       if i4Status == VLAN_EVB_SYSTEM_START then EVB module is enabled.
       if i4Status == VLAN_EVB_SYSTEM_SHUTDOWN then EVB module is shutdown.
    */

    if (SNMP_FAILURE ==  nmhSetFsMIEvbSystemControl
                (i4ContextId, i4Status))
    {
        CliPrintf (CliHandle, 
            "\r%%Failed to set EVB system control status.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}
/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : VlanEvbCliModStatus                               */
/*                                                                          */
/*     DESCRIPTION      : This function enables/disable EVB module status   */
/*                                                                          */
/*     INPUT            : i4Status   - VLAN_EVB_MODULE_ENABLE/              */
/*                                     VLAN_EVB _MODULE_DISABLE             */
/*                        CliHandle  - CLI Handler                          */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/****************************************************************************/
INT4
VlanEvbCliModStatus(tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;
    INT4                i4ContextId = VLAN_EVB_ZERO;

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());

    /* Validating the EVB Module Status */
    if(SNMP_FAILURE == nmhTestv2FsMIEvbSystemModuleStatus
            (&u4ErrorCode, i4ContextId, i4Status))
    {
        CliPrintf (CliHandle, "\r%%EVB module status validation failed.\r\n");
        return (CLI_FAILURE);
    }

    /* Setting the EVB Module Status.
       if i4Status == VLAN_EVB_MODULE_ENABLE, then EVB Module is enabled. 
       if i4Status == VLAN_EVB_MODULE_DISABLE, then EVB Module is disabled. */

    if(SNMP_FAILURE == 
        nmhSetFsMIEvbSystemModuleStatus(i4ContextId, i4Status))
    {
        CliPrintf (CliHandle, "\r%%Failed to set the Module Status.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbCliSetDebug
 *
 *     DESCRIPTION      : This function configures EVB debug level
 *                        and enables debug messages
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4TrcLevel - EVB trace level
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

INT4
VlanEvbCliSetDebug(tCliHandle CliHandle, UINT4 u4TrcLevel)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;
    INT4                i4ContextId = VLAN_EVB_ZERO;
    INT4                i4RetVal    = 0;

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());

    if(u4TrcLevel ==  VLAN_EVB_MAX_TRACE_LEVEL)
    {
         /* Warning message to enable the all EVB trace messages */
        i4RetVal = CliDisplayMessageAndUserPromptResponse
                    ("This will enable all EVB traces ", 1, VlanLock, VlanUnLock);
        if (i4RetVal != CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }

    if(SNMP_FAILURE == nmhTestv2FsMIEvbSystemTraceLevel
              (&u4ErrorCode, i4ContextId, u4TrcLevel))
    {
        CliPrintf (CliHandle, "\r%%EVB Debug validation failed" 
                " with the given trace level.\r\n");
        return CLI_FAILURE;

    }

    if (SNMP_FAILURE == nmhSetFsMIEvbSystemTraceLevel
              (i4ContextId, u4TrcLevel))
    {
        CliPrintf (CliHandle, "\r%%Failed to enable EVB debug traces.\r\n");
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanEvbCliClearEvbStats                            */
/*                                                                           */
/*     DESCRIPTION      : This function clears System EVB statistics         */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanEvbCliClearEvbStats (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode     = VLAN_EVB_ZERO;
    INT4                i4ContextId     = VLAN_EVB_ZERO;
    INT4                i4EvbStatsClear = VLAN_TRUE;
    /* Getting current context ID */
    i4ContextId = ((INT4) CLI_GET_CXT_ID ());
    
    if(SNMP_FAILURE == nmhTestv2FsMIEvbSystemStatsClear(&u4ErrorCode,
                                                        i4ContextId,
                                                        i4EvbStatsClear))
    {
        CliPrintf (CliHandle, "\r%%EVB Clear Stats validation failed.\r\n");
        return CLI_FAILURE;
    }
    
    if (SNMP_FAILURE == nmhSetFsMIEvbSystemStatsClear 
              (i4ContextId, i4EvbStatsClear))
    {
        CliPrintf (CliHandle, "\r%%Failed to clear EVB stats.\r\n");
        return CLI_FAILURE;
    }
    
    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbCliSetTrapStatus 
 *
 *     DESCRIPTION      : Enables EVB traps at system level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4TrapFlg - EVB trap flag
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbCliSetTrapStatus(tCliHandle CliHandle, INT4 i4TrapFlg)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;
    INT4                i4ContextId = VLAN_EVB_ZERO;

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());

    if(SNMP_FAILURE == nmhTestv2FsMIEvbSystemTrapStatus 
              (&u4ErrorCode, i4ContextId, i4TrapFlg))
    {
        CliPrintf (CliHandle, "\r%%EVB TRAP control validation failed.\r\n");
        return CLI_FAILURE;

    }

    if (SNMP_FAILURE == nmhSetFsMIEvbSystemTrapStatus 
              (i4ContextId, i4TrapFlg))
    {
        CliPrintf (CliHandle, "\r%%Failed to set the EVB Traps.\r\n ");
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbCdcpEnable 
 *
 *     DESCRIPTION      : This function enables EVB CDCP 
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4CdcpMode - CDCP mode 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbCdcpEnable(tCliHandle CliHandle, INT4 i4CdcpMode)
{
    UINT4        u4ErrorCode          = VLAN_EVB_ZERO;
    INT4         i4UapCdcpAdminEnable = VLAN_EVB_UAP_CDCP_ENABLE;
    UINT4        u4IfIndex            = VLAN_EVB_ZERO;

    u4IfIndex   =((UINT4) CLI_GET_IFINDEX()); 
    
    if ((INT4)u4IfIndex == CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    if(SNMP_FAILURE == nmhTestv2FsMIEvbUAPSchCdcpMode(&u4ErrorCode ,
                                                      u4IfIndex , 
                                                      i4CdcpMode))
    {
        return CLI_FAILURE;
    }


    if(SNMP_FAILURE == nmhSetFsMIEvbUAPSchCdcpMode(u4IfIndex, i4CdcpMode))
    {
        CliPrintf (CliHandle, "\r%%Failed to set the EVB"
                "S-channel CDCP mode.\r\n");
        return CLI_FAILURE;
    }
    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable
                        (&u4ErrorCode, u4IfIndex, i4UapCdcpAdminEnable))
    {
        return CLI_FAILURE;
    }

  
    if(SNMP_FAILURE == nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable(u4IfIndex,
                                                        i4UapCdcpAdminEnable))
    {
        CliPrintf (CliHandle, "\r%%Failed to set the EVB"
                "S-channel CDCP amdin enable.\r\n");
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbCdcpDisable
 *
 *     DESCRIPTION      : This function disables the CDCP admin status
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbCdcpDisable(tCliHandle CliHandle)
{
    UINT4               u4ErrorCode          = VLAN_EVB_ZERO;
    INT4                i4UapCdcpAdminEnable = VLAN_EVB_UAP_CDCP_DISABLE;
    UINT4               u4IfIndex            = VLAN_EVB_ZERO;

    /* Getting Interface Index */

    u4IfIndex   = ((UINT4)CLI_GET_IFINDEX());

    if ((INT4)u4IfIndex == CLI_ERROR)
    {
        return CLI_FAILURE;
    }
    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable
                        (&u4ErrorCode, u4IfIndex, i4UapCdcpAdminEnable))
    {
        CliPrintf (CliHandle,
            "\r%%Validation failed for Cdcp Admin Enable.\r\n");
        return CLI_FAILURE;
    }


    if(SNMP_FAILURE == nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable(u4IfIndex,
                                                        i4UapCdcpAdminEnable))
    {
        CliPrintf (CliHandle,
            "\r%%Failed to set Cdcp Admin Enable.\r\n");
        return CLI_FAILURE;
    }
  
    /*How to delete only hardware entries */
    return (CLI_SUCCESS);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbCliMaxSchannel 
 *
 *     DESCRIPTION      : This function is used configure the Maximum
 *                        s-channel capacity for a port 
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4CdcpChannelCap - Channel capacity 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbCliMaxSchannel(tCliHandle CliHandle, INT4 i4CdcpChannelCap,UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;

    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap
              (&u4ErrorCode, u4IfIndex, i4CdcpChannelCap))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap
              (u4IfIndex, i4CdcpChannelCap))
    {
        CliPrintf (CliHandle, 
            "\r%%Failed to set maximum S-channel entries.\r\n");
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbServVlanPoolConfig 
 *
 *     DESCRIPTION      : This function is used to configure the 
 *                         service-vlan pool vlaues.  
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Interface Index.
 *                        i4PoolType - ContextId (Low/High)
 *                        u4SvidPoolVal - Pool id 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbServVlanPoolConfig(tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT4 u4PoolLow, UINT4 u4PoolHigh)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;

    if(u4PoolLow !=0)
    {
        if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow 
                (&u4ErrorCode, u4IfIndex, u4PoolLow))
        {
            CliPrintf (CliHandle, "\r%%Svid Low Pool value should not exceed"
                    "high pool value.\r\n");
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow 
                (u4IfIndex, u4PoolLow))
        {
            CliPrintf (CliHandle, "\r%%Failed to set low Svid Pool value\r\n");
            return CLI_FAILURE;
        }
    }

    if(u4PoolHigh !=0)
    {
        if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
                (&u4ErrorCode, u4IfIndex, u4PoolHigh))
        {
            CliPrintf (CliHandle, "\r%%Svid High Pool value should not be"
            " lesser than low pool value.\r\n");
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh 
                (u4IfIndex, u4PoolHigh))
        {
            CliPrintf (CliHandle, 
                    "\r%%Failed to set maximum S-channel entries.\r\n");
            return CLI_FAILURE;
        }
    }

    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbStorageType
 *
 *     DESCRIPTION      : This function is used to configure the storage type
 *                        for EVB module
 *                        The following storage types are supported:
 *                        1. Volatile
 *                        2. Non-volatile
 *                        Default storage type is Non-volatile
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Interface Index.
 *                        i4StorageType - Type of the Storage
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

INT4
VlanEvbStorageType(tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4StorageType)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;

    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType 
              (&u4ErrorCode, u4IfIndex, i4StorageType))
    {
        CliPrintf (CliHandle, "\r%%Validation failed for the given "
            "storage type.\r\n");
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetIeee8021BridgeEvbUAPConfigStorageType 
              (u4IfIndex, i4StorageType))
    {
        CliPrintf (CliHandle, "\r%%Failed to set the storage type.\r\n");
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);

}


/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbConfigSchMode 
 *
 *     DESCRIPTION      : This function configures the s-channel mode 
 *                        The following mode types can be set:
 *                          1. auto        2. Manual 
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4SchMode - S-channel mode
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbConfigSchMode(tCliHandle CliHandle, INT4 i4SchMode)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;
    INT4                i4ContextId = VLAN_EVB_ZERO;

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());

    if(SNMP_FAILURE == nmhTestv2FsMIEvbSchannelIdMode 
              (&u4ErrorCode, i4ContextId,i4SchMode))
    { 
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFsMIEvbSchannelIdMode 
              (i4ContextId, i4SchMode))
    {
        CliPrintf (CliHandle, "\r%%Failed to set S-channel mode.\r\n");
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbAddScidEntry 
 *
 *     DESCRIPTION      : This function adds a new static SCID,SVID pair in 
 *                         CDCP tlv 
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - interface index
 *                        i4SChannelId - S-channel ID 
 *                        i4SVId - S-VId
 *                        
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbAddScidEntry (tCliHandle CliHandle,UINT4 u4IfIndex, 
                        UINT4 u4SChannelId, UINT4 u4SVId)
{
    UINT4               u4ErrCode = VLAN_EVB_ZERO;
    INT4                i4ContextId = VLAN_EVB_ZERO;
    INT4                i4SChIdMode = VLAN_EVB_DEF_SCH_MODE;
    tEvbUapIfEntry       *pEvbUapIfEntry = NULL;        

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());
    /* Getting the UAP entry for the given Interface Index */
    pEvbUapIfEntry = VlanEvbUapIfGetEntry(u4IfIndex);
    if (NULL == pEvbUapIfEntry)
    {
        CliPrintf (CliHandle, "\r%%UAP entry is not present"
           "for the given interface.\r\n");
        return CLI_FAILURE;
    }
  
    /* Verifying the S-channel mode - Auto/Manual */ 
     
    if(SNMP_FAILURE == nmhGetFsMIEvbSchannelIdMode(i4ContextId,
                                                  &i4SChIdMode))
    {
        CliPrintf (CliHandle, "\r%%Failed to get the S-Channel ID Mode.\r\n");
        return (CLI_FAILURE);
    } 
   
    /* If the mode is Auto VlanEvbSChGetNextSChannelId is used to get next
        available entry */
               
    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbCAPRowStatus(&u4ErrCode,
                                                      u4IfIndex, 
                                                      u4SVId, 
                                                        CREATE_AND_WAIT))
    {
        return (CLI_FAILURE);
    }
    
               
    if (SNMP_FAILURE == nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex, 
                                                   u4SVId,
                                                      CREATE_AND_WAIT)) 
    {
        return (CLI_FAILURE);
    }
    if(SNMP_FAILURE == nmhTestv2FsMIEvbCAPSChannelID(&u4ErrCode, 
                                                     u4IfIndex,
                                                     u4SVId,
                                                     u4SChannelId))
    {
        nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex,
                                            u4SVId,
                                            DESTROY);
        return (CLI_FAILURE);
    }
    if(SNMP_FAILURE == nmhSetFsMIEvbCAPSChannelID(u4IfIndex,
                                                  u4SVId,
                                                  u4SChannelId)) 
    {
        nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex,
                                            u4SVId,
                                            DESTROY);
        CliPrintf (CliHandle, "\r%%Failed to set EVB CAP S-channel ID\r\n");
        return (CLI_FAILURE);
    }
   /* Call testRowStatus */ 
    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbCAPRowStatus(&u4ErrCode, 
                                                      u4IfIndex, 
                                                      u4SVId, 
                                                                 ACTIVE))
    {
        nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex,
                                               u4SVId,
                                              DESTROY);
        CliPrintf (CliHandle, "\r%%Failed to Validate CAP Row Status\r\n");
        return (CLI_FAILURE);
    }
    if(SNMP_FAILURE == nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex,
                                                           u4SVId,
                                                           ACTIVE))
    {
        nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex,
                                              u4SVId,
                                              DESTROY);
        CliPrintf (CliHandle, "\r%%Failed to set CAP Row Status\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbDelScidEntry
 *
 *     DESCRIPTION      : This function adds a new static SCID,SVID pair in
 *                         CDCP tlv
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - ContextId
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbDelScidEntry (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 u4SChannelId, UINT4 u4SChId)
{
    UINT4               u4ErrCode = VLAN_EVB_ZERO;
    UINT4               u4TestSChannelId = VLAN_EVB_ZERO; 
    INT4                i4SChMode = 0;
    INT4                i4ContextId = 0;
    /* Getting the S-Channel ID mode 
       If Mode == AUTO, S-channel is ZERO. S-Channel ID is dynamic value 
       If Mode == MANUAL, both S-channel and SVID should NOT be zero.*/

    i4ContextId = ((INT4)CLI_GET_CXT_ID ());
       
    nmhGetFsMIEvbSchannelIdMode(i4ContextId, &i4SChMode);

    if(VLAN_EVB_SCH_MODE_MANUAL == i4SChMode)
    {
        if(SNMP_FAILURE == nmhGetFsMIEvbCAPSChannelID(u4IfIndex, 
                               u4SChId, &u4TestSChannelId))
        {
            return (CLI_FAILURE);
        }


        if(u4TestSChannelId != u4SChannelId)
        {
            CliPrintf (CliHandle, "\r%%Given S-channel and "
                    "SVid pair doesn't exist.\r\n");
            return (CLI_FAILURE);
        }
    }

    if((i4SChMode == VLAN_EVB_SCH_MODE_AUTO) && (u4SChannelId != VLAN_EVB_ZERO))
    {
        CliPrintf (CliHandle, "\r%%S-channel ID should not be provided "
                 "when mode is AUTO\r\n");
        return (CLI_FAILURE);
    }
    

    if(SNMP_FAILURE == nmhTestv2Ieee8021BridgeEvbCAPRowStatus(&u4ErrCode,
                                                      u4IfIndex,
                                                      u4SChId,
                                                                DESTROY))
    {
        return (CLI_FAILURE);
    }

    if(SNMP_FAILURE == nmhSetIeee8021BridgeEvbCAPRowStatus(u4IfIndex,
                                                   u4SChId, 
                                                            DESTROY))
    {
        CliPrintf (CliHandle, "\r%%Failed to Set Row Status\r\n");
        return (CLI_FAILURE);
    }
    
    return (CLI_SUCCESS);
}
/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbTlvSelect 
 *
 *     DESCRIPTION      : This function enables TLV transmission
 *                            in the S-Channel interface 
 *                         CDCP tlv
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbTlvSelect(tCliHandle CliHandle)
{
    UINT4               u4ErrorCode         = VLAN_EVB_ZERO;
    UINT4               u4DestAddrIndex     = VLAN_EVB_ZERO;
    INT4               i4IfIndex           = VLAN_EVB_ZERO;

    i4IfIndex   = CLI_GET_IFINDEX();
    if (i4IfIndex == CLI_ERROR)
    {
        return CLI_FAILURE;
    }
    if(SNMP_FAILURE == nmhTestv2LldpXdot1EvbConfigCdcpTxEnable(&u4ErrorCode, 
                        i4IfIndex,u4DestAddrIndex,VLAN_EVB_CDCP_LLDP_ENABLE))
    {
        CliPrintf (CliHandle, "\r%%Failed to validate LLDP CDCP"
            "Tx Enable.\r\n");
        return (CLI_FAILURE);
    }

    if(SNMP_FAILURE == nmhSetLldpXdot1EvbConfigCdcpTxEnable(i4IfIndex ,
                          u4DestAddrIndex,VLAN_EVB_CDCP_LLDP_ENABLE))
    {
        CliPrintf (CliHandle, "\r%%Failed to set CDCP Tx Enable.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbNoTlvSlct
 *
 *     DESCRIPTION      : This function enables TLV transmission
 *                            in the S-Channel interface
 *                         CDCP tlv
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbNoTlvSlct(tCliHandle CliHandle)
{
    UINT4               u4ErrorCode         = VLAN_EVB_ZERO;
    UINT4               u4DestAddrIndex     = VLAN_EVB_ZERO;
    INT4                i4IfIndex           = VLAN_EVB_ZERO;
    i4IfIndex   = CLI_GET_IFINDEX();

    if (i4IfIndex == CLI_ERROR)
    {
        return CLI_FAILURE;
    }
    if(SNMP_FAILURE == nmhTestv2LldpXdot1EvbConfigCdcpTxEnable(&u4ErrorCode, 
                       i4IfIndex, u4DestAddrIndex, VLAN_EVB_CDCP_LLDP_DISABLE))
    {
        CliPrintf (CliHandle, "\r%%Failed to validate LLDP CDCP"
            "Tx Enable.\r\n");
        return (CLI_FAILURE);
    }

    if(SNMP_FAILURE == nmhSetLldpXdot1EvbConfigCdcpTxEnable(i4IfIndex,
                          u4DestAddrIndex,VLAN_EVB_CDCP_LLDP_DISABLE))
    {
        CliPrintf (CliHandle, "\r%%Failed to set CDCP Tx Enable.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}


/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbConfigSChL2FilterStatus
 *
 *     DESCRIPTION      : This function enables installation of L2 Filters
 *                            in the S-Channel interface
 *                         
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4Status   - Enable/Disable
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

INT4
VlanEvbConfigSChL2FilterStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = VLAN_EVB_ZERO;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4IfIndex    = 0;
    UINT2               u2SVID = 0;

    u4IfIndex = ((UINT4)CLI_GET_IFINDEX());

    if ((INT4)u4IfIndex == CLI_ERROR)
    {
        return CLI_FAILURE;
    }
    if (VlanGetSChInfoFromSChIfIndex ( u4IfIndex, 
                    &u4UapIfIndex, &u2SVID) == VLAN_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid L2 Filter Option "
                    "selected for S-Channel the Status .\r\n");
        return (CLI_FAILURE);

    }
    if ((nmhTestv2FsMIEvbSChannelFilterStatus(&u4ErrorCode , 
          u4UapIfIndex , (UINT4)u2SVID , i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid / Redundant S-Channel Filter Option "
                    "selected for S-Channel the Status .\r\n");
        return (CLI_FAILURE);

    }
    if ((nmhSetFsMIEvbSChannelFilterStatus( u4UapIfIndex , 
                    (UINT4)u2SVID , i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Failed to set the S-Channel "
                    "L2 Filter Status.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}


/* Show commands */
/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShowEvbDetail 
 *
 *     DESCRIPTION      : This function is used to display the EVB details
 *                        The following information is displayed:
 *                        1. EVB System Control
 *                        2. EVB System Module Status
 *                        3. EVB Trap Status
 *                        4. EVB SysType
 *                        5. Number of EVB External Ports
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - ContextId
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbShowEvbDetail(tCliHandle CliHandle,UINT4 u4ContextId) 
{
    INT4     i4EvbSystemControl  = VLAN_EVB_ZERO;
    INT4     i4EvbSysModStatus   = VLAN_EVB_ZERO;
    INT4     i4EvbTrapStatus     = VLAN_EVB_ZERO;
    INT4     i4EvbBrgSysType     = VLAN_EVB_ZERO;
    UINT4     u4EvbSysNumExtPorts = VLAN_EVB_ZERO; 
    /* Getting the EVB System Control Status */
    if (nmhValidateIndexInstanceFsMIEvbSystemTable((INT4)u4ContextId) 
            == SNMP_FAILURE )
    {
        return CLI_SUCCESS;
    }
    nmhGetFsMIEvbSystemControl((INT4)u4ContextId, 
            &i4EvbSystemControl);

    /* Getting the EVB Module Status */
    nmhGetFsMIEvbSystemModuleStatus((INT4)u4ContextId, 
            &i4EvbSysModStatus); 

    /* Getting the EVB Trap Status */
    nmhGetFsMIEvbSystemTrapStatus((INT4)u4ContextId, 
            &i4EvbTrapStatus); 

    /* Getting the EVB Bridge port type */
    nmhGetIeee8021BridgeEvbSysType(&i4EvbBrgSysType);

    /* Getting the number of EVB External ports */
    nmhGetIeee8021BridgeEvbSysNumExternalPorts
        (&u4EvbSysNumExtPorts);

    /* Displaying the information */
    CliPrintf (CliHandle,"\r\nEVB Global information\r\n");    
    CliPrintf (CliHandle, "----------------------- \r\n");

    if(0 == u4ContextId)
    {
        CliPrintf (CliHandle,"Context Name: Default\r\n");
        CliPrintf (CliHandle,"--------------------- \r\n");
    }
    if(VLAN_EVB_SYSTEM_START == i4EvbSystemControl)
    {
        CliPrintf (CliHandle,"EVB System Status            : Start\r\n");
    }
    else
    {
        CliPrintf (CliHandle,"EVB System Status            : Shutdown\r\n");
    }
    if(VLAN_EVB_MODULE_ENABLE == i4EvbSysModStatus)
    {
        CliPrintf (CliHandle,"EVB Module Status            : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,"EVB Module Status            : Disabled\r\n");
    }
    if(VLAN_EVB_TRAP_ENABLE == i4EvbTrapStatus)
    {
        CliPrintf (CliHandle,"EVB Trap Status              : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,"EVB Trap Status              : Disabled\r\n");
    }
    if(VLAN_EVB_ROLE_BRIDGE == i4EvbBrgSysType)
    {
        CliPrintf (CliHandle,"EVB Bridge Type              : Bridge\r\n");
    }
    else
    {
        CliPrintf (CliHandle,"EVB Bridge Type              : Station\r\n");
    }
    CliPrintf (CliHandle,"Number of EVB External ports : %d\r\n",
            u4EvbSysNumExtPorts);
    CliPrintf ( CliHandle , "\r\n");
    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShow 
 *
 *     DESCRIPTION      : This function is used to call the appropriate 
 *                        function which displays the details of the 
 *                        Command
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4IfIndex  - Interface Index
 *                        u4Command  - Show Command
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbShow(tCliHandle CliHandle, UINT4 u4IfIndex,UINT4 u4Command) 
{
    INT1                i1RetStatus             = SNMP_FAILURE;
    UINT4               u4NextIfIndex = VLAN_EVB_ZERO;
    /* Display All */
    if (u4IfIndex == 0)
    { 
        i1RetStatus = nmhGetFirstIndexFsMIEvbUAPConfigTable(&u4IfIndex);
        if (i1RetStatus == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4NextIfIndex = u4IfIndex;
        do 
        {
            u4IfIndex = u4NextIfIndex;
            if ( u4Command == CLI_VLAN_EVB_UAP_SHOW_DESC)
            {
                VlanEvbShowEvbDesc (CliHandle , u4IfIndex);
            }
            if ( u4Command == CLI_VLAN_EVB_UAP_SHOW_STATS)
            {
                VlanEvbShowEvbStats  (CliHandle , u4IfIndex);
            }
            if ( u4Command == CLI_VLAN_EVB_SHOW_LOCAL_CDCP_TLV)
            {
                VlanEvbShowCdcpLocTlvInfo (CliHandle, u4IfIndex);
            }
            if ( u4Command == CLI_VLAN_EVB_SHOW_REMOTE_CDCP_TLV)
            {
                VlanEvbShowCdcpRemTlvInfo (CliHandle, u4IfIndex);
            }
            if ( u4Command == CLI_VLAN_EVB_SHOW_SCH_INT)
            {
                VlanEvbShowSChannel (CliHandle,u4IfIndex);
            }
        }while( nmhGetNextIndexFsMIEvbUAPConfigTable 
                (u4IfIndex,&u4NextIfIndex) != SNMP_FAILURE);
    }
	else
	{
		if ( u4Command == CLI_VLAN_EVB_UAP_SHOW_DESC)
		{
			VlanEvbShowEvbDesc (CliHandle , u4IfIndex);
		}
		if ( u4Command == CLI_VLAN_EVB_UAP_SHOW_STATS)
		{
			VlanEvbShowEvbStats  (CliHandle , u4IfIndex);
		}
		if ( u4Command == CLI_VLAN_EVB_SHOW_LOCAL_CDCP_TLV)
		{
			VlanEvbShowCdcpLocTlvInfo (CliHandle, u4IfIndex);
		}
		if ( u4Command == CLI_VLAN_EVB_SHOW_REMOTE_CDCP_TLV)
		{
			VlanEvbShowCdcpRemTlvInfo (CliHandle, u4IfIndex);
		}
		if ( u4Command == CLI_VLAN_EVB_SHOW_SCH_INT)
		{
			VlanEvbShowSChannel (CliHandle,u4IfIndex);
		}
	}
    return CLI_SUCCESS;
}
/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShowEvbDesc 
 *
 *     DESCRIPTION      : This function is used to display the EVB details
 *                        The following information is displayed:
 *                        1.EVB UAP Component Id
 *                        2.EVB UAP Port
 *                        3.EVB UAP Config IfIndex
 *                        4.EVB UAP SCh CDCP AdminEnable
 *                        5.EVB UAP SchAdmin CDCP Role
 *                        6.EVB UAP SchAdmin CDCP ChanCap
 *                        7.EVB UAP Sch Oper CDCP ChanCap
 *                        8.EVB UAP Sch CDCP SVID Pool Low
 *                        9.EVB UAP Sch CDCP SVID Pool High
 *                       10.EVB UAP Sch Oper State
 *                       11.EVB Sch Cdcp RemoteRole
 *                       12.EVB UAP Sch Cdcp Mode
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - InterfaceIndex 
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

INT4 
VlanEvbShowEvbDesc(tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4EvbUAPComponentId     = VLAN_EVB_ZERO;  
    UINT4               u4BridgeEvbUAPPort      = VLAN_EVB_ZERO;
    INT4                i4UapConfigIfIndex      = VLAN_EVB_ZERO; 
    INT4                i4CdcpAdminEnable       = VLAN_EVB_ZERO; 
    INT4                i4UAPSchAdminCDCPRole   = VLAN_EVB_ZERO; 
    INT4                i4UAPSchAdminCDCPChanCap= VLAN_EVB_ZERO; 
    INT4                i4UAPSchOperCDCPChanCap = VLAN_EVB_ZERO; 
    UINT4               u4UapSchSVIDPoolLow     = VLAN_EVB_ZERO;    
    UINT4               u4UapSchSVIDPoolHigh    = VLAN_EVB_ZERO; 
    INT4                i4UAPSchOperState       = VLAN_EVB_ZERO; 
    INT4                i4SchCdcpRemoteEnabled  = VLAN_EVB_ZERO; 
    INT4                i4SchCdcpRemoteRole     = VLAN_EVB_ZERO; 
    INT4                i4UAPSchCdcpMode        = VLAN_EVB_ZERO;
    INT1                au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1OperStatus= CFA_IF_DOWN;
    if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(u4IfIndex)
            == SNMP_FAILURE) 
    {
        return CLI_SUCCESS;
    } 
    nmhGetIeee8021BridgeEvbUAPComponentId(u4IfIndex, 
            &u4EvbUAPComponentId);
    nmhGetIeee8021BridgeEvbUAPPort(u4IfIndex, 
            &u4BridgeEvbUAPPort);
    nmhGetIeee8021BridgeEvbUapConfigIfIndex(u4IfIndex, 
            &i4UapConfigIfIndex);
    nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable(
            u4IfIndex, &i4CdcpAdminEnable);
    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPRole(u4IfIndex,
            &i4UAPSchAdminCDCPRole);
    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap
        (u4IfIndex, &i4UAPSchAdminCDCPChanCap);
    nmhGetIeee8021BridgeEvbUAPSchOperCDCPChanCap
        (u4IfIndex, &i4UAPSchOperCDCPChanCap);
    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow
        (u4IfIndex, &u4UapSchSVIDPoolLow);
    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh
        (u4IfIndex, &u4UapSchSVIDPoolHigh);
    nmhGetIeee8021BridgeEvbUAPSchOperState(u4IfIndex, 
            &i4UAPSchOperState);
    nmhGetIeee8021BridgeEvbSchCdcpRemoteEnabled
        (u4IfIndex, &i4SchCdcpRemoteEnabled);
    nmhGetIeee8021BridgeEvbSchCdcpRemoteRole
        (u4IfIndex, &i4SchCdcpRemoteRole);
    nmhGetFsMIEvbUAPSchCdcpMode(u4IfIndex, 
            &i4UAPSchCdcpMode);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    VlanCfaCliGetIfName(u4IfIndex,au1IfName);
    CfaGetIfOperStatus(u4IfIndex,&u1OperStatus);

    if(u1OperStatus == CFA_IF_UP)
    {
        CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "UP");
    }
    else if(u1OperStatus == CFA_IF_NP)
    {
        CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "NOT-PRESENT");
    }
    else
    {
        CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "DOWN");
    }

    CliPrintf (CliHandle, 
            "\r\nEVB UAP Component Id            : %d",u4EvbUAPComponentId);
    CliPrintf (CliHandle, 
            "\r\nEVB UAP Port                    : %d",u4BridgeEvbUAPPort);
    CliPrintf (CliHandle, 
            "\r\nEVB UAP Config IfIndex          : %d",i4UapConfigIfIndex);
    if (i4CdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP SCh CDCP AdminEnable    : " "Enable");
    }
    else if (i4CdcpAdminEnable == VLAN_EVB_UAP_CDCP_DISABLE)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP SCh CDCP AdminEnable    : " "Disable");
    }
    if (i4UAPSchAdminCDCPRole == VLAN_EVB_ROLE_BRIDGE)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP SchAdmin CDCP Role      : ""Bridge");
    }
    else if (i4UAPSchAdminCDCPRole == VLAN_EVB_ROLE_STATION)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP SchAdmin CDCP Role      : ""Station");
    }
    CliPrintf (CliHandle, 
            "\r\nEVB UAP SchAdmin CDCP ChanCap   : %d",i4UAPSchAdminCDCPChanCap);
    CliPrintf (CliHandle, 
            "\r\nEVB UAP Sch Oper CDCP ChanCap   : %d",i4UAPSchOperCDCPChanCap);
    CliPrintf (CliHandle, 
            "\r\nEVB UAP Sch CDCP SVID Pool Low  : %d",u4UapSchSVIDPoolLow);
    CliPrintf (CliHandle, 
            "\r\nEVB UAP Sch CDCP SVID Pool High : %d",u4UapSchSVIDPoolHigh);
    if (i4UAPSchOperState == VLAN_EVB_UAP_CDCP_RUNNING)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP Sch Oper State          : ""Running");
    }
    else if (i4UAPSchOperState == VLAN_EVB_UAP_CDCP_NOT_RUNNING)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP Sch Oper State          : ""Not-Running");
    }
    if (i4SchCdcpRemoteEnabled == VLAN_EVB_UAP_CDCP_ENABLE)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB Sch Cdcp RemoteEnabled      : ""Enable");
    }
    else if (i4SchCdcpRemoteEnabled == VLAN_EVB_UAP_CDCP_DISABLE)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB Sch Cdcp RemoteEnabled      : ""Disable");
    }
    if (i4SchCdcpRemoteRole == VLAN_EVB_ROLE_BRIDGE)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB Sch Cdcp RemoteRole         : ""Bridge");
    }
    else if (i4SchCdcpRemoteRole == VLAN_EVB_ROLE_STATION)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB Sch Cdcp RemoteRole         : ""Station");
    }
    if (i4UAPSchCdcpMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP Sch Cdcp Mode           : ""Dynamic");
    }
    else if (i4UAPSchCdcpMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
    {
        CliPrintf (CliHandle, 
                "\r\nEVB UAP Sch Cdcp Mode           : ""Hybrid");
    }

    CliPrintf ( CliHandle , "\r\n");

    return CLI_SUCCESS;
}
/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShowCdcpLocTlvInfo 
 *
 *     DESCRIPTION      : This function is used to display the EVB details
 *                        The following information is displayed:
 *                        1. EVB CDCP TX status
 *                        2. EVB CDCP Pairs
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4IfIndex  - Interface Index
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbShowCdcpLocTlvInfo(tCliHandle CliHandle, UINT4 u4IfIndex)
{
  tSNMP_OCTET_STRING_TYPE  RetValLldpV2Xdot1LocCdcpTlvString;
  UINT1                    au1Buf[VLAN_EVB_CDCP_TLV_LEN];
  INT4                     i4EvbCdcpTxEnable = 0;
  UINT4                    u4DestAddressIndex = 0; 
  UINT2                    u2Loop            = 0;
  UINT2                    u2OffSet          = 0;
  UINT4                    u4Pair            = 0;
  UINT2                    u2SCID            = 0;
  UINT2                    u2SVID            = 0;
  UINT2                    u2PairCount       = 0;
  UINT1                    *pu1Buf           = NULL;
  UINT1                     u1OperStatus     = 0;
  INT1                     au1IfName[CFA_MAX_PORT_NAME_LENGTH];
  
  MEMSET(&RetValLldpV2Xdot1LocCdcpTlvString ,0,sizeof(tSNMP_OCTET_STRING_TYPE));
  MEMSET (au1Buf , 0 ,sizeof(au1Buf));
  
  MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
  VlanCfaCliGetIfName(u4IfIndex,au1IfName);
  CfaGetIfOperStatus(u4IfIndex,&u1OperStatus);

  if(u1OperStatus == CFA_IF_UP)
  {
      CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "UP");
  }
  else if(u1OperStatus == CFA_IF_NP)
  {
      CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "NOT-PRESENT");
  }
  else
  {
      CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "DOWN");
  }


  RetValLldpV2Xdot1LocCdcpTlvString.pu1_OctetList = au1Buf;
   
  nmhGetLldpXdot1EvbConfigCdcpTxEnable((INT4)u4IfIndex,
                u4DestAddressIndex,&i4EvbCdcpTxEnable);
  if (i4EvbCdcpTxEnable == VLAN_EVB_CDCP_LLDP_DISABLE)            
  {
      CliPrintf(CliHandle,"\r\nEVB Config CDCP Tx State : ""Disable");
      CliPrintf(CliHandle,"\r\n");
      return CLI_SUCCESS;
  }
      CliPrintf(CliHandle,"\r\nEVB Config CDCP Tx State : ""Enable");
  
  nmhGetLldpV2Xdot1LocCdcpTlvString((INT4)u4IfIndex,
             &RetValLldpV2Xdot1LocCdcpTlvString); 
  if (RetValLldpV2Xdot1LocCdcpTlvString.i4_Length == 0)
  {
      CliPrintf(CliHandle,"\r\n");
      return CLI_SUCCESS;
  }
  pu1Buf = (RetValLldpV2Xdot1LocCdcpTlvString.pu1_OctetList);

  if (RetValLldpV2Xdot1LocCdcpTlvString.i4_Length > 0)
  {
      CliPrintf(CliHandle,"\r\nCDCP Pairs[SCID,SVID]    : ");
      for ( ; u2Loop < ((UINT2) RetValLldpV2Xdot1LocCdcpTlvString.i4_Length);
            u2Loop =((UINT2) (u2Loop + VLAN_EVB_CDCP_THREE)),
            u2OffSet =((UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE)))
      {
           VLAN_EVB_GET_3BYTE (pu1Buf, (UINT1 *)(VOID *)&u4Pair);
           VLAN_EVB_PARSE_PAIR (u4Pair, u2SCID, u2SVID);
           u2PairCount = ((UINT2) (u2PairCount + 1));
           if (u2PairCount > 5)
           {
               CliPrintf(CliHandle,"\r\n%27s","");
               u2PairCount = 0;
           }
           CliPrintf(CliHandle,"[%d,%d]",u2SCID,u2SVID);
           if (u2Loop != RetValLldpV2Xdot1LocCdcpTlvString.i4_Length - 3)
           {
             CliPrintf(CliHandle,",");
           }
      }                 
      CliPrintf(CliHandle,"\r\n");
   } 
    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShowCdcpRemTlvInfo
 *
 *     DESCRIPTION      : This function is used to display the EVB details
 *                        The following information is displayed:
 *                        1. EVB CDCP TX status
 *                        2. EVB CDCP Pairs
 *                        
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4IfIndex  - Interface Index
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbShowCdcpRemTlvInfo(tCliHandle CliHandle, UINT4 u4IfIndex)
{
  tSNMP_OCTET_STRING_TYPE  RetValLldpV2Xdot1RemCdcpTlvString;
  UINT4                    u4LldpV2RemLocalDestMACAddress = 0;
  UINT1                    au1Buf[VLAN_EVB_CDCP_TLV_LEN];
  INT4                     i4EvbCdcpTxEnable = 0;
  UINT2                    u2Loop            = 0;
  UINT2                    u2OffSet          = 0;
  UINT4                    u4Pair            = 0;
  UINT2                    u2SCID            = 0;
  UINT2                    u2SVID            = 0;
  UINT2                    u2PairCount       = 0;
  UINT1                    *pu1Buf           = NULL;
  UINT1                     u1OperStatus     = 0;
  INT1                      au1IfName[CFA_MAX_PORT_NAME_LENGTH];
  UINT4                     u4LldpV2RemTimeMark = 0;
  INT4                      i4LldpV2RemIndex = 0;

  UNUSED_PARAM(u4LldpV2RemTimeMark);
  UNUSED_PARAM(i4LldpV2RemIndex);
  UNUSED_PARAM(u4LldpV2RemLocalDestMACAddress);

  MEMSET(&RetValLldpV2Xdot1RemCdcpTlvString ,0,sizeof(tSNMP_OCTET_STRING_TYPE));
  MEMSET (au1Buf , 0 ,sizeof(au1Buf));

  MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
  VlanCfaCliGetIfName(u4IfIndex,au1IfName);
  CfaGetIfOperStatus(u4IfIndex,&u1OperStatus);

  if(u1OperStatus == CFA_IF_UP)
  {
      CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "UP");
  }
  else if(u1OperStatus == CFA_IF_NP)
  {
      CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "NOT-PRESENT");
  }
  else
  {
      CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "DOWN");
  }


  RetValLldpV2Xdot1RemCdcpTlvString.pu1_OctetList = au1Buf;

  nmhGetLldpXdot1EvbConfigCdcpTxEnable((INT4)u4IfIndex,
            u4LldpV2RemLocalDestMACAddress,&i4EvbCdcpTxEnable);
  if (i4EvbCdcpTxEnable == VLAN_EVB_CDCP_LLDP_DISABLE)
  {
      CliPrintf(CliHandle,"\r\nEVB Config CDCP Tx State : ""Disable");
      CliPrintf(CliHandle,"\r\n");
      return CLI_SUCCESS;
  }
      CliPrintf(CliHandle,"\r\nEVB Config CDCP Tx State : ""Enable");

   nmhGetLldpV2Xdot1RemCdcpTlvString(u4LldpV2RemTimeMark,(INT4)u4IfIndex,
            u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex, &RetValLldpV2Xdot1RemCdcpTlvString);
  if (RetValLldpV2Xdot1RemCdcpTlvString.i4_Length == 0)
  {
      CliPrintf(CliHandle,"\r\n");
      return CLI_SUCCESS;
  }
  pu1Buf = (RetValLldpV2Xdot1RemCdcpTlvString.pu1_OctetList);

  if (RetValLldpV2Xdot1RemCdcpTlvString.i4_Length > 0)
  {
      CliPrintf(CliHandle,"\r\nCDCP Pairs[SCID,SVID]    : ");
      for ( ; u2Loop < ((UINT2) RetValLldpV2Xdot1RemCdcpTlvString.i4_Length);
            u2Loop = ((UINT2)(u2Loop + VLAN_EVB_CDCP_THREE)),
            u2OffSet = ((UINT2)(u2OffSet + VLAN_EVB_CDCP_THREE)))
      {
           VLAN_EVB_GET_3BYTE (pu1Buf, (UINT1 *)(VOID *)&u4Pair);
           VLAN_EVB_PARSE_PAIR (u4Pair, u2SCID, u2SVID);
           u2PairCount =((UINT2)(u2PairCount + 1));
           if (u2PairCount > 5)
           {
               CliPrintf(CliHandle,"\r\n%27s","");
               u2PairCount = 0;
           }
           CliPrintf(CliHandle,"[%d,%d]",u2SCID,u2SVID);
           if (u2Loop != RetValLldpV2Xdot1RemCdcpTlvString.i4_Length - 3)
           {
             CliPrintf(CliHandle,",");
           }
      }
      CliPrintf(CliHandle,"\r\n");
   }
    return (CLI_SUCCESS);
 }

/********************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShowSChannel 
 *
 *     DESCRIPTION      : This function is used to display the EVB S-Channel Info
 *                        The following information is displayed:
 *                        1. EVB Interface Index
 *                        2. EVB S-Channel Id
 *                        3. EVB S-Channel Access Ports(CAP)
 *                        4. EVB CAP Associate (Alias Port) 
 *                        5. S-channel Status (CFA)
 *                        6. EVB CAP S-Channel Negotiation Status
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex  - Interface Index
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *********************************************************************************/
INT4 
VlanEvbShowSChannel(tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4NextIfIndex = VLAN_EVB_ZERO;
    INT4                i4EvbCAPSChannelIfIndex = VLAN_EVB_ZERO;
    INT4                i4EvbCAPSChNegoStatus = VLAN_EVB_ZERO;
    UINT4               u4EvbSchId = VLAN_EVB_ZERO; 
    UINT4               u4EvbNextSchId = VLAN_EVB_ZERO;
    UINT4               u4EvbCAPSChannelId = VLAN_EVB_ZERO;
    UINT4               u4EvbCAPAssociateSBPOrURPPort = VLAN_EVB_ZERO;
    UINT4               u4UapIfIndex=0;
    UINT2               u2Svid=0;
    INT1                au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1EvbSchannelOperStatus = CFA_IF_DOWN;
    UINT1               u1OperStatus = 0;
    UINT1               u1UapOperStatus = 0;
    INT4                i4SchCdcpMode = VLAN_EVB_UAP_SCH_MODE_DYNAMIC;
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    VlanCfaCliGetIfName(u4IfIndex,au1IfName);
    CfaGetIfOperStatus(u4IfIndex,&u1OperStatus);
 
    if(u1OperStatus == CFA_IF_UP)
    {
        CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "UP");
    }
    else if(u1OperStatus == CFA_IF_NP)
    {
        CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "NOT-PRESENT");
    }
    else
    {
        CliPrintf ( CliHandle,"\r\n%s is %s", au1IfName, "DOWN");
    }



    while(SNMP_FAILURE != nmhGetNextIndexIeee8021BridgeEvbCAPConfigTable (
                u4IfIndex,&u4NextIfIndex, u4EvbSchId, &u4EvbNextSchId))
    {
        if (u4IfIndex != u4NextIfIndex)
            break;

        u4EvbSchId = u4EvbNextSchId;
        if ( nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable
                (u4IfIndex,u4EvbSchId)!= SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        nmhGetIeee8021BridgeEvbCAPSChannelID (u4IfIndex,
                u4EvbSchId, &u4EvbCAPSChannelId);

        nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPPort (
                u4IfIndex,u4EvbSchId,&u4EvbCAPAssociateSBPOrURPPort);

        nmhGetFsMIEvbCAPSChannelIfIndex (u4IfIndex ,
                u4EvbSchId ,&i4EvbCAPSChannelIfIndex);
        u1EvbSchannelOperStatus = CFA_IF_DOWN;

        VlanCfaGetIfOperStatus ((UINT4)i4EvbCAPSChannelIfIndex,
                &u1EvbSchannelOperStatus);
        if (u1EvbSchannelOperStatus != CFA_IF_UP)
        {
            /* OPER Status of the S-Channel is down
             * so get the oper status of the underlying
             * UAP of this particular s-channel
             * and display the Oper status based on the 
             * UAP Oper status */
            VlanGetSChInfoFromSChIfIndex ((UINT4)i4EvbCAPSChannelIfIndex,
                    &u4UapIfIndex,&u2Svid);
            VlanCfaGetIfOperStatus (u4UapIfIndex, &u1UapOperStatus);
            if (u1UapOperStatus==CFA_IF_NP)
            {
                u1EvbSchannelOperStatus = CFA_IF_NP;
            }
            else
            {
                u1EvbSchannelOperStatus = CFA_IF_DOWN;
            }

        }
        nmhGetFsMIEvbCAPSChNegoStatus (u4IfIndex , u4EvbSchId,
                &i4EvbCAPSChNegoStatus);

        nmhGetFsMIEvbUAPSchCdcpMode(u4IfIndex,
                &i4SchCdcpMode);
    

        CliPrintf ( CliHandle,"\r\nEVB S-VID%28s: %d","",u4EvbSchId);
        CliPrintf ( CliHandle,"\r\nEVB CAP S-Channel ID%16s : %d","",
                u4EvbCAPSChannelId);
        CliPrintf ( CliHandle,"\r\nEVB CAP Associate SBP Port%10s : %d","",
                u4EvbCAPAssociateSBPOrURPPort);
        if(u1EvbSchannelOperStatus == CFA_IF_UP)
        {
            CliPrintf ( CliHandle,"\r\nEVB S-channel Oper Status%11s : %s",
                    "", "UP");
        }
        else if(u1EvbSchannelOperStatus == CFA_IF_NP)
        {
            CliPrintf ( CliHandle,"\r\nEVB S-channel Oper Status%11s : %s",
                    "", "NOT-PRESENT");
        }
        else
        {
            CliPrintf ( CliHandle,"\r\nEVB S-channel Oper Status%11s : %s",
                    "","DOWN");
        }

        if (i4SchCdcpMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
        {
			if (i4EvbCAPSChNegoStatus == VLAN_EVB_SCH_FREE)
			{
				CliPrintf ( CliHandle,"\r\nEVB CAP S-Channel Negotiation Status : "
						"Free");
			}
			else if (i4EvbCAPSChNegoStatus == VLAN_EVB_SCH_NEGOTIATING)
			{  
				CliPrintf ( CliHandle,"\r\nEVB CAP S-Channel Negotiation Status : "
						"Negotiating");
			}
			else if ( i4EvbCAPSChNegoStatus == VLAN_EVB_SCH_CONFIRMED)
			{
				CliPrintf ( CliHandle,"\r\nEVB CAP S-Channel Negotiation Status : "
						"Confirmed");
			}
        }
        CliPrintf ( CliHandle , "\r\n");
    }
    return CLI_SUCCESS;
}
/****************************************************************************
 *
 *     FUNCTION NAME    : VlanEvbShowEvbStats 
 *
 *     DESCRIPTION      : This function is used to display the EVB details
 *                        The following information is displayed:
 *                        1.TxCdcpCount
 *                        2.RxCdcpCount
 *                        3.FailCount
 *                        4.ActiveFailCount
 *                        5.ExceedsCount
 *                        6.OtherDropCount
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex  - Interface Index
 *                        
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
VlanEvbShowEvbStats(tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT1                au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT4               u4EvbTxCdcpCount = VLAN_EVB_ZERO;
    UINT4               u4EvbRxCdcpCount = VLAN_EVB_ZERO;
    UINT4               u4EvbFailCount = VLAN_EVB_ZERO;
    UINT4               u4EvbActiveFailCount = VLAN_EVB_ZERO;
    UINT4               u4EvbExceedsCount = VLAN_EVB_ZERO;
    UINT4               u4EvbOtherDropCount = VLAN_EVB_ZERO;
    UINT4               u4EvbCdcpRejStatReqCount = VLAN_EVB_ZERO;

	if (nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable(u4IfIndex)
			== SNMP_FAILURE) 
	{
		return CLI_SUCCESS;
	} 
	nmhGetFsMIEvbTxCdcpCount
			(u4IfIndex, &u4EvbTxCdcpCount);
	nmhGetFsMIEvbRxCdcpCount
			(u4IfIndex, &u4EvbRxCdcpCount);
	nmhGetFsMIEvbSChAllocFailCount
			(u4IfIndex, &u4EvbFailCount);
	nmhGetFsMIEvbSChActiveFailCount
			(u4IfIndex, &u4EvbActiveFailCount);
	nmhGetFsMIEvbSVIDPoolExceedsCount
			(u4IfIndex, &u4EvbExceedsCount);
	nmhGetFsMIEvbCdcpOtherDropCount
			(u4IfIndex, &u4EvbOtherDropCount);
    nmhGetFsMIEvbCdcpRejectStationReq
            (u4IfIndex, &u4EvbCdcpRejStatReqCount);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    VlanCfaCliGetIfName(u4IfIndex,au1IfName);
    CfaGetIfOperStatus(u4IfIndex,&u1OperStatus);
    CliPrintf (CliHandle,"\r\n %s is %s",au1IfName,((u1OperStatus==CFA_IF_DOWN) ?
                                                "DOWN" : "UP"));
	CliPrintf (CliHandle,
			"\r\n TxCdcpCount          :%d", u4EvbTxCdcpCount);
	CliPrintf (CliHandle,
			"\r\n RxCdcpCount          :%d", u4EvbRxCdcpCount);
	CliPrintf (CliHandle,
			"\r\n SChAllocFailCount    :%d", u4EvbFailCount);
	CliPrintf (CliHandle,
			"\r\n ActiveFailCount      :%d", u4EvbActiveFailCount);
	CliPrintf (CliHandle,
			"\r\n ExceedsCount         :%d", u4EvbExceedsCount);
    CliPrintf (CliHandle,
            "\r\n CdcpRejectionCount   :%d", u4EvbCdcpRejStatReqCount);
	CliPrintf (CliHandle,
			"\r\n OtherDropCount       :%d", u4EvbOtherDropCount);
	CliPrintf ( CliHandle , "\r\n");

    return (CLI_SUCCESS);
}

/****************************************************************************
 Function    : VlanEvbShowRunningConfig
 Description : This function displays the current configuration
                of EVB module
 Input       : Variable Arguments
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
VlanEvbShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    UINT4    u4IfIndex          = VLAN_EVB_ZERO; 
    INT4     i4ContextId        = VLAN_EVB_ZERO;
    INT4     i4EvbSystemControl = VLAN_EVB_ZERO;

    UNUSED_PARAM(u4Module);

    i4ContextId = ((INT4) CLI_GET_CXT_ID ());
    if ((UINT4)i4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        /* Setting default context . in switch mode */
        i4ContextId = 0;
        CLI_SET_CXT_ID(0);
    }
    nmhGetFsMIEvbSystemControl(i4ContextId, &i4EvbSystemControl);
    /* If module is not started return without process */
    if (i4EvbSystemControl != VLAN_EVB_SYSTEM_START)
    {
        return  CLI_SUCCESS; 
    }

    if (VlanEvbShowRunningConfigScalars (CliHandle) == CLI_SUCCESS)
    {
        VlanEvbShowRunningConfigInterface (CliHandle,u4IfIndex);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanEvbShowRunningConfigScalars                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in LA        */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanEvbShowRunningConfigScalars(tCliHandle CliHandle)
{
    INT4     i4EvbSystemControl  = VLAN_EVB_ZERO;
    INT4     i4EvbSysModStatus   = VLAN_EVB_ZERO;
    INT4     i4EvbTrapStatus     = VLAN_EVB_ZERO;
    INT4     i4EvbSChIdMode      = VLAN_EVB_ZERO;
    INT4     i4ContextId         = VLAN_EVB_ZERO;

    /* TODO: SRC to be updated for non default context.*/
    i4ContextId =((INT4) CLI_GET_CXT_ID ());
    if ((UINT4)i4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        /* Setting default context . in switch mode */
        i4ContextId = 0;
        CLI_SET_CXT_ID(0);
    }
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "!\r\n");
    /* Getting the EVB System Control Status */
    nmhGetFsMIEvbSystemControl(i4ContextId, &i4EvbSystemControl);
    if (i4EvbSystemControl == VLAN_EVB_SYSTEM_START)
    {
	    if(VLAN_EVB_DEF_SYSTEM_STATUS != i4EvbSystemControl)
	    {
		    CliPrintf (CliHandle, "no shutdown evb\r\n");
	    }
	    /* Getting the EVB Module Status */
	    nmhGetFsMIEvbSystemModuleStatus(i4ContextId, 
			    &i4EvbSysModStatus);
	    if(VLAN_EVB_DEF_MODULE_STATUS != i4EvbSysModStatus)
	    {
		    CliPrintf (CliHandle, "set evb enable\r\n");
	    }
	    /* Getting the EVB Trap Status */
	    nmhGetFsMIEvbSystemTrapStatus(i4ContextId, 
			    &i4EvbTrapStatus);
	    if(VLAN_EVB_DEF_TRAP_STATUS != i4EvbTrapStatus)
	    {
		    CliPrintf (CliHandle, "set evb trap enable\r\n");
	    }

	    nmhGetFsMIEvbSchannelIdMode (i4ContextId, 
			    &i4EvbSChIdMode);
	    if(VLAN_EVB_DEF_SCH_MODE != i4EvbSChIdMode)
	    {
		    CliPrintf (CliHandle, "evb s-channel-id mode manual\r\n");
	    }
	    CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanEvbShowRunningConfigTables                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the table  objects in LA   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanEvbShowRunningConfigTables (tCliHandle CliHandle ,UINT4 u4IfIndex)
{
    UINT4               u4NxtIfIndex          = VLAN_EVB_ZERO;
    UINT4               u4BridgeEvbSchID      = VLAN_EVB_ZERO;
    UINT4               u4NextBridgeEvbSchID  = VLAN_EVB_ZERO;
    INT4                i4SchCDCPChanCap      = VLAN_EVB_ZERO; 
    UINT4               u4CDCPSVIDPoolLow     = VLAN_EVB_ZERO; 
    UINT4               u4CDCPSVIDPoolHigh    = VLAN_EVB_ZERO; 
    UINT4               u4ContextId           = VLAN_EVB_ZERO;
    INT4                i4EvbStorageType      = VLAN_EVB_ZERO; 
    INT4                i4SchCdcpMode         = VLAN_EVB_ZERO;  
    INT4                i4CdcpAdminEnable     = VLAN_EVB_ZERO;  
    INT4                i4UAPSchOperState     = VLAN_EVB_ZERO;
    INT4                i4EvbSChIdMode        = VLAN_EVB_ZERO;
    INT4                i4EvbSystemControl    = VLAN_EVB_ZERO;
    INT4                i4BridgePortType      = VLAN_EVB_ZERO;
    /*  S-Channel Interface table related variables */
    UINT4               u4SChannelId          = SNMP_FAILURE;
    INT1                i1NameStr[CFA_MAX_PORT_NAME_LENGTH];     
    u4ContextId = CLI_GET_CXT_ID ();
    if (u4ContextId == VLAN_CLI_INVALID_CONTEXT)
    {
        /* Setting default context . in switch mode */
        u4ContextId = 0;
        CLI_SET_CXT_ID(0);
    }
    nmhGetFsMIEvbSystemControl((INT4)u4ContextId, &i4EvbSystemControl);
    if ((VlanCfaGetInterfaceBrgPortType(u4IfIndex, &i4BridgePortType) 
            == VLAN_SUCCESS) &&
        (i4BridgePortType == VLAN_UPLINK_ACCESS_PORT ))
    {
      if (i4EvbSystemControl == VLAN_EVB_SYSTEM_START) 
      {
	    VlanCfaCliConfGetIfName (u4IfIndex, i1NameStr);
	    CliPrintf (CliHandle, "interface %s\r\n", i1NameStr);
	    /* Getting and displaying the EVB S-Channel mode */
	    CliPrintf (CliHandle, "bridge port-type uplinkAccessPort\r\n");
	    nmhGetFsMIEvbUAPSchCdcpMode(u4IfIndex, 
			    &i4SchCdcpMode);
        nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable(
            u4IfIndex, &i4CdcpAdminEnable);

	    if((VLAN_EVB_UAP_SCH_MODE_HYBRID == i4SchCdcpMode) && 
            (VLAN_EVB_UAP_CDCP_ENABLE == i4CdcpAdminEnable))       
	    {
		    CliPrintf (CliHandle, "evb cdcp enable mode hybrid\r\n");
	    }
        /* Default mode is VLAN_EVB_UAP_SCH_MODE_DYNAMIC */
        else if(VLAN_EVB_UAP_CDCP_ENABLE == i4CdcpAdminEnable)
        {
		    CliPrintf (CliHandle, "evb cdcp enable\r\n");

        }
	    nmhGetIeee8021BridgeEvbUAPSchOperState(u4IfIndex, 
			    &i4UAPSchOperState);
	    if (i4UAPSchOperState == VLAN_EVB_UAP_CDCP_RUNNING)
	    {
		    CliPrintf (CliHandle,"evb tlv-select cdcp\r\n");
	    }

	    /* Getting and displaying the EVB CDCP Channel Capacity  */
	    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap
		    (u4IfIndex, &i4SchCDCPChanCap);
	    if(VLAN_EVB_DEF_CDCP_ADMIN_CHAN_CAP != i4SchCDCPChanCap)       
	    {
		    CliPrintf (CliHandle, "evb cdcp max-schannel %d\r\n",
				    i4SchCDCPChanCap);
	    }

	    /* Getting and displaying the EVB CDCP SVID Low Pool value */

	    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow(u4IfIndex,
			    &u4CDCPSVIDPoolLow);
	    if(VLAN_EVB_DEF_CDCP_SVID_POOL_LOW != u4CDCPSVIDPoolLow)       
	    {
		    CliPrintf (CliHandle, "evb cdcp service-vlan pool low %d\r\n",
				    u4CDCPSVIDPoolLow);
	    }

	    /* Getting and displaying the EVB CDCP SVID high Pool value */

	    nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh(u4IfIndex,
			    &u4CDCPSVIDPoolHigh);
	    if(VLAN_EVB_DEF_CDCP_SVID_POOL_HIGH != u4CDCPSVIDPoolHigh)       
	    {
		    CliPrintf (CliHandle, "evb cdcp service-vlan pool high %d\r\n",
				    u4CDCPSVIDPoolHigh);
	    }

	    /* Getting and displaying the EVB Storage Type */

	    nmhGetIeee8021BridgeEvbUAPConfigStorageType(u4IfIndex,
			    &i4EvbStorageType);
	    if(VLAN_EVB_STORAGE_TYPE_NON_VOLATILE != i4EvbStorageType)       
	    {
		    CliPrintf (CliHandle, "evb storage-type volatile\r\n");
	    }

	    /* Getting the first index of CAP config table */
	    while(SNMP_FAILURE != nmhGetNextIndexFsMIEvbCAPConfigTable(u4IfIndex,
				    &u4NxtIfIndex,u4BridgeEvbSchID,&u4NextBridgeEvbSchID))
	    {
		    if (u4IfIndex != u4NxtIfIndex)
			    break;
		    u4BridgeEvbSchID = u4NextBridgeEvbSchID;

		    /* Getting S-channel ID and displaying the same */
		    nmhGetFsMIEvbCAPSChannelID(u4IfIndex,
				    u4BridgeEvbSchID,
				    &u4SChannelId);
		    /* Getting S-Vid */
		    nmhGetFsMIEvbSchannelIdMode ((INT4)u4ContextId, 
				    &i4EvbSChIdMode);

		    if((VLAN_EVB_DEF_SCH_MODE != i4EvbSChIdMode)&&
				    ((VLAN_EVB_DEF_SCID != u4SChannelId) || 
				     (VLAN_EVB_DEF_SVID != u4BridgeEvbSchID))) 
		    {
			    CliPrintf (CliHandle, "evb s-channel %d service-vlan %d\r\n",
				            u4SChannelId,u4BridgeEvbSchID);
		    }
		    else if((VLAN_EVB_DEF_SCID != u4SChannelId) || 
				    (VLAN_EVB_DEF_SVID != u4BridgeEvbSchID)) 
		    {
			    CliPrintf (CliHandle, "evb s-channel  service-vlan %d\r\n",
					        u4BridgeEvbSchID);
		    }

		    u4IfIndex = u4NxtIfIndex;
		    /* Getting the next index values for CAP config table */
	    } /* End of while */
    }
    }
            CliPrintf (CliHandle, "!\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanEvbShowRunningConfigInterface                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the table  objects in LA   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
VlanEvbShowRunningConfigInterface(tCliHandle CliHandle, UINT4 u4IfIndex) 
{
    UINT4                u4NextIfIndex  = VLAN_EVB_ZERO;
    /* Display All */
    if (u4IfIndex == 0)
    { 
        if (SNMP_FAILURE == nmhGetFirstIndexFsMIEvbUAPConfigTable(&u4IfIndex))
        {
            return CLI_SUCCESS;
        }
        u4NextIfIndex = u4IfIndex;
        do 
        {
            u4IfIndex = u4NextIfIndex;         
            VlanEvbShowRunningConfigTables(CliHandle , u4IfIndex);

        }while( nmhGetNextIndexFsMIEvbUAPConfigTable 
                (u4IfIndex,&u4NextIfIndex) != SNMP_FAILURE);
    }
    else
    {
        VlanEvbShowRunningConfigTables( CliHandle,u4IfIndex);
    }
    return CLI_SUCCESS;
}
#endif
