/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvlanwr.c,v 1.43.22.1 2018/03/15 12:59:57 siva Exp $
 *
 * Description: This file contains wrapper routines used in VLAN module.
 *
 *******************************************************************/
#include "lr.h"
#include "vlaninc.h"
#include "fssnmp.h"
#include "fsvlanwr.h"
#include "fsvlandb.h"

VOID
RegisterFSVLAN ()
{
    SNMPRegisterMib (&fsvlanOID, &fsvlanEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsvlanOID, (const UINT1 *) "fsvlan");
}

VOID
UnRegisterFSVLAN ()
{
    SNMPUnRegisterMib (&fsvlanOID, &fsvlanEntry);
    SNMPDelSysorEntry (&fsvlanOID, (const UINT1 *) "fsvlan");
}

INT4
Dot1qFutureVlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1qFutureVlanStatus (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanMacBasedOnAllPortsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1qFutureVlanMacBasedOnAllPorts
        (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtoBasedOnAllPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1qFutureVlanPortProtoBasedOnAllPorts
        (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBaseBridgeModeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();
    i4RetVal = (nmhGetDot1qFutureVlanBaseBridgeMode
                (&(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanSubnetBasedOnAllPortsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();
    i4RetVal = (nmhGetDot1qFutureVlanSubnetBasedOnAllPorts
                (&(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBaseBridgeModeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanBaseBridgeMode (pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanShutdownStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureVlanShutdownStatus (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGarpShutdownStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    GARP_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureGarpShutdownStatus (&(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1qFutureVlanDebug (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanLearningModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1qFutureVlanLearningMode (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanHybridTypeDefaultGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureVlanHybridTypeDefault (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalMacLearningStatusGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureVlanGlobalMacLearningStatus (&
                                                      (pMultiData->
                                                       i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanApplyEnhancedFilteringCriteriaGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureVlanApplyEnhancedFilteringCriteria (&
                                                             (pMultiData->
                                                              i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanSwStatsEnabledGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureVlanSwStatsEnabled (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanStatus (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanMacBasedOnAllPortsSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanMacBasedOnAllPorts
        (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtoBasedOnAllPortsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts
        (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanShutdownStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanShutdownStatus (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGarpShutdownStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetDot1qFutureGarpShutdownStatus (pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanDebug (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanLearningModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanLearningMode (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanHybridTypeDefaultSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanHybridTypeDefault (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalMacLearningStatusSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanGlobalMacLearningStatus
        (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanApplyEnhancedFilteringCriteriaSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria
        (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanSwStatsEnabledSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanSwStatsEnabled (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanStatus
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBaseBridgeModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanBaseBridgeMode
                (pu4Error, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanMacBasedOnAllPortsTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtoBasedOnAllPortsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanShutdownStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanShutdownStatus
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGarpShutdownStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    UNUSED_PARAM (pMultiIndex);

    i4RetVal = nmhTestv2Dot1qFutureGarpShutdownStatus
        (pu4Error, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanDebug
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanLearningModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanLearningMode
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanHybridTypeDefaultTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanHybridTypeDefault (pu4Error,
                                                   pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalMacLearningStatusTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus (pu4Error,
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanApplyEnhancedFilteringCriteriaTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanSwStatsEnabledTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanSwStatsEnabled (pu4Error,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBaseBridgeModeDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhDepv2Dot1qFutureVlanBaseBridgeMode (pu4Error,
                                                       pSnmpIndexList,
                                                       pSnmpvarbinds));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanStatus (pu4Error, pSnmpIndexList,
                                              pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanMacBasedOnAllPortsDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanMacBasedOnAllPorts
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtoBasedOnAllPortsDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanPortProtoBasedOnAllPorts
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanShutdownStatusDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanShutdownStatus
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGarpShutdownStatusDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureGarpShutdownStatus
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanDebug (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanLearningModeDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanLearningMode (pu4Error, pSnmpIndexList,
                                                    pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanHybridTypeDefaultDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanHybridTypeDefault (pu4Error,
                                                         pSnmpIndexList,
                                                         pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalMacLearningStatusDep (UINT4 *pu4Error,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanGlobalMacLearningStatus (pu4Error,
                                                               pSnmpIndexList,
                                                               pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanApplyEnhancedFilteringCriteriaDep (UINT4 *pu4Error,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND *
                                                  pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanApplyEnhancedFilteringCriteria
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanSwStatsEnabledDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanSwStatsEnabled
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanPortTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanPortTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanPortType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacBasedClassificationGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanPortMacBasedClassification
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortPortProtoBasedClassificationGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanPortPortProtoBasedClassification
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanFilteringUtilityCriteriaGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanFilteringUtilityCriteria
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtectedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanPortProtected
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetBasedClassificationGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1qFutureVlanPortSubnetBasedClassification
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortUnicastMacLearningGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortUnicastMacLearning
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortIngressEtherTypeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortIngressEtherType
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortEgressEtherTypeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortEgressEtherType
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortEgressTPIDTypeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortEgressTPIDType
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortAllowableTPID1Get (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortAllowableTPID1
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortAllowableTPID2Get (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortAllowableTPID2
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortAllowableTPID3Get (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortAllowableTPID3
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortUnicastMacSecTypeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortUnicastMacSecType
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFuturePortPacketReflectionStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{

    INT4                i4RetVal;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1qFuturePortPacketReflectionStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanPortTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanPortType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacBasedClassificationSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanPortMacBasedClassification
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortPortProtoBasedClassificationSet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanPortPortProtoBasedClassification
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanFilteringUtilityCriteriaSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanFilteringUtilityCriteria
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtectedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanPortProtected
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanPortSubnetBasedClassificationSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetDot1qFutureVlanPortSubnetBasedClassification
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortUnicastMacLearningSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortUnicastMacLearning
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortIngressEtherTypeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortIngressEtherType
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortEgressEtherTypeSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortEgressEtherType
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortEgressTPIDTypeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortEgressTPIDType
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortAllowableTPID1Set (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortAllowableTPID1
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortAllowableTPID2Set (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortAllowableTPID2
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortAllowableTPID3Set (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortAllowableTPID3
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortUnicastMacSecTypeSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortUnicastMacSecType
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFuturePortPacketReflectionStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhSetDot1qFuturePortPacketReflectionStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanPortTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanPortType (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacBasedClassificationTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanPortMacBasedClassification (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanPortPortProtoBasedClassificationTest (UINT4 *pu4Error,
                                                     tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4                Dot1qFutureVlanFilteringUtilityCriteriaTest
    (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria
        (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortProtectedTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanPortProtected (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanPortSubnetBasedClassificationTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       i4_SLongValue,
                                                                       pMultiData->
                                                                       i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortUnicastMacLearningTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortUnicastMacLearning (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiData->
                                                                i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortIngressEtherTypeTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortIngressEtherType (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortEgressEtherTypeTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortEgressEtherType (pu4Error,
                                                             pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             pMultiData->
                                                             i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortEgressTPIDTypeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortEgressTPIDType (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortAllowableTPID1Test (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortAllowableTPID1 (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortAllowableTPID2Test (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortAllowableTPID2 (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortAllowableTPID3Test (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortAllowableTPID3 (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortUnicastMacSecTypeTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortUnicastMacSecType (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFuturePortPacketReflectionStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFuturePortPacketReflectionStatus
                (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanPortTable (pu4Error, pSnmpIndexList,
                                                 pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanPortMacMapTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanPortMacMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;

        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanPortMacMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanPortMacMapVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanPortMacMapVid (pMultiIndex->
                                            pIndex[0].i4_SLongValue,
                                            (*(tMacAddr *)
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue->pu1_OctetList),
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapNameGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanPortMacMapName (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             (*(tMacAddr *)
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue->pu1_OctetList),
                                             pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapMcastBcastOptionGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanPortMacMapMcastBcastOption (pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         (*(tMacAddr *)
                                                          pMultiIndex->pIndex
                                                          [1].pOctetStrValue->
                                                          pu1_OctetList),
                                                         &(pMultiData->
                                                           i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanPortMacMapRowStatus (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  (*(tMacAddr *)
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue->
                                                   pu1_OctetList),
                                                  &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanPortMacMapVid (pMultiIndex->
                                            pIndex[0].i4_SLongValue,
                                            (*(tMacAddr *)
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue->pu1_OctetList),
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanPortMacMapNameSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanPortMacMapName (pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             (*(tMacAddr *)
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue->pu1_OctetList),
                                             pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapMcastBcastOptionSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetDot1qFutureVlanPortMacMapMcastBcastOption (pMultiIndex->
                                                         pIndex
                                                         [0].i4_SLongValue,
                                                         (*(tMacAddr *)
                                                          pMultiIndex->pIndex
                                                          [1].pOctetStrValue->
                                                          pu1_OctetList),
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanPortMacMapRowStatus (pMultiIndex->
                                                  pIndex[0].i4_SLongValue,
                                                  (*(tMacAddr *)
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapVidTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapVid (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               (*(tMacAddr *)
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue->pu1_OctetList),
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapNameTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapName (pu4Error,
                                                pMultiIndex->
                                                pIndex[0].i4_SLongValue,
                                                (*(tMacAddr *)
                                                 pMultiIndex->pIndex[1].
                                                 pOctetStrValue->pu1_OctetList),
                                                pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapMcastBcastOptionTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            (*(tMacAddr *)
                                                             pMultiIndex->pIndex
                                                             [0].
                                                             pOctetStrValue->
                                                             pu1_OctetList),
                                                            pMultiData->
                                                            i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureVlanPortMacMapRowStatus (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     (*(tMacAddr *)
                                                      pMultiIndex->
                                                      pIndex
                                                      [1].pOctetStrValue->
                                                      pu1_OctetList),
                                                     pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortMacMapTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanPortMacMapTable (pu4Error,
                                                       pSnmpIndexList,
                                                       pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanFidMapTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanFidMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();

            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanFidMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();

            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanFidMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanFidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanFidMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();

        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanFid (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanFidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanFid (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanFidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanFid (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].u4_ULongValue,
                                            pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanFidMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanFidMapTable (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhGetDot1qFutureVlanOperStatus (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGvrpOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    GARP_LOCK ();

    i4RetVal = nmhGetDot1qFutureGvrpOperStatus (&(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGmrpOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    GARP_LOCK ();

    i4RetVal = nmhGetDot1qFutureGmrpOperStatus (&(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBridgeModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qFutureVlanBridgeMode (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelBpduPriGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhGetDot1qFutureVlanTunnelBpduPri (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBridgeModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanBridgeMode (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelBpduPriSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetDot1qFutureVlanTunnelBpduPri (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBridgeModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2Dot1qFutureVlanBridgeMode
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelBpduPriTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2Dot1qFutureVlanTunnelBpduPri
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanBridgeModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanBridgeMode (pu4Error, pSnmpIndexList,
                                                  pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelBpduPriDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanTunnelBpduPri (pu4Error, pSnmpIndexList,
                                                     pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanTunnelTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanTunnelTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanTunnelTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanTunnelStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanTunnelStatus
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanTunnelStatus
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanTunnelStatus (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanTunnelTable (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanTunnelProtocolTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanTunnelProtocolTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanTunnelProtocolTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanTunnelStpPDUsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanTunnelStpPDUs
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelStpPDUsRecvdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanTunnelStpPDUsRecvd
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelStpPDUsSentGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanTunnelStpPDUsSent
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelGvrpPDUsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanTunnelGvrpPDUs
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelGvrpPDUsRecvdGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanTunnelGvrpPDUsRecvd
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelGvrpPDUsSentGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanTunnelGvrpPDUsSent
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelIgmpPktsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanTunnelIgmpPkts
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelIgmpPktsRecvdGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanTunnelIgmpPktsRecvd
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelIgmpPktsSentGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanTunnelIgmpPktsSent
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelStpPDUsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanTunnelStpPDUs
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelGvrpPDUsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (nmhSetDot1qFutureVlanTunnelGvrpPDUs
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelIgmpPktsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        (nmhSetDot1qFutureVlanTunnelIgmpPkts
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelStpPDUsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanTunnelStpPDUs (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelGvrpPDUsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanTunnelGvrpPDUs (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanTunnelIgmpPktsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanTunnelIgmpPkts (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanTunnelProtocolTableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanTunnelProtocolTable (pu4Error,
                                                           pSnmpIndexList,
                                                           pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanCounterTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanCounterTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanCounterTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanCounterRxUcastGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanCounterRxUcast (pMultiIndex->
                                             pIndex[0].u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterRxMcastBcastGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterRxMcastBcast (pMultiIndex->
                                                  pIndex[0].u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterTxUnknUcastGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterTxUnknUcast (pMultiIndex->
                                                 pIndex[0].u4_ULongValue,
                                                 &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterTxUcastGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterTxUcast (pMultiIndex->
                                             pIndex[0].u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterTxBcastGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterTxBcast (pMultiIndex->
                                             pIndex[0].u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterRxFramesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterRxFrames (pMultiIndex->
                                              pIndex[0].u4_ULongValue,
                                              &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterRxBytesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterRxBytes (pMultiIndex->
                                             pIndex[0].u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterTxFramesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterTxFrames (pMultiIndex->
                                              pIndex[0].u4_ULongValue,
                                              &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterTxBytesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterTxBytes (pMultiIndex->
                                             pIndex[0].u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterDiscardFramesGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterDiscardFrames (pMultiIndex->
                                                   pIndex[0].u4_ULongValue,
                                                   &
                                                   (pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterDiscardBytesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterDiscardBytes (pMultiIndex->
                                                  pIndex[0].u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanCounterStatus (pMultiIndex->
                                            pIndex[0].u4_ULongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanCounterStatus (pMultiIndex->
                                            pIndex[0].u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterStatusTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanCounterStatus (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[0].u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanCounterTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanCounterTable (pu4Error, pSnmpIndexList,
                                                    pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4 
     
     
     
     
     
     
     
    GetNextIndexDot1qFutureVlanUnicastMacControlTable
    (tSnmpIndex * pFirstMultiIndex, tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanUnicastMacControlTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanUnicastMacControlTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanUnicastMacLimitGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanUnicastMacLimit (pMultiIndex->
                                              pIndex[0].u4_ULongValue,
                                              &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanAdminMacLearningStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanAdminMacLearningStatus (pMultiIndex->
                                                     pIndex[0].u4_ULongValue,
                                                     &
                                                     (pMultiData->
                                                      i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanOperMacLearningStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanOperMacLearningStatus (pMultiIndex->
                                                    pIndex[0].u4_ULongValue,
                                                    &
                                                    (pMultiData->
                                                     i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortFdbFlushGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureVlanPortFdbFlush (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanUnicastMacLimitSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanUnicastMacLimit (pMultiIndex->
                                              pIndex[0].u4_ULongValue,
                                              pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanAdminMacLearningStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanAdminMacLearningStatus (pMultiIndex->
                                                     pIndex[0].u4_ULongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortFdbFlushSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        nmhSetDot1qFutureVlanPortFdbFlush (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanUnicastMacLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanUnicastMacLimit (pu4Error,
                                                        pMultiIndex->
                                                        pIndex[0].u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanAdminMacLearningStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanAdminMacLearningStatus (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureVlanPortFdbFlushTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanPortFdbFlush (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].u4_ULongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanUnicastMacControlTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanUnicastMacControlTable (pu4Error,
                                                              pSnmpIndexList,
                                                              pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureGarpDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (pMultiIndex);

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = nmhGetDot1qFutureGarpDebug (&(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
Dot1qFutureGarpDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = nmhSetDot1qFutureGarpDebug (pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pMultiData);
#endif
    return i4RetVal;
}

INT4
Dot1qFutureGarpDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal =
        nmhTestv2Dot1qFutureGarpDebug (pu4Error, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
#endif

    return i4RetVal;
}

INT4
Dot1qFutureGarpDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

#ifdef GARP_WANTED
    GARP_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureGarpDebug (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    GARP_UNLOCK ();
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
#endif

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanTpFdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanTpFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanTpFdbPwGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanTpFdbPw (pMultiIndex->pIndex[0].u4_ULongValue,
                                      (*(tMacAddr *) pMultiIndex->
                                       pIndex[1].pOctetStrValue->pu1_OctetList),
                                      &(pMultiData->u4_ULongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qTpOldFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpOldFdbPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                        (*(tMacAddr *) pMultiIndex->
                                         pIndex[1].pOctetStrValue->
                                         pu1_OctetList),
                                        &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureConnectionIdentifierGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pMultiData->pOctetStrValue->i4_Length = 6;

    i4RetVal = nmhGetDot1qFutureConnectionIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanWildCardTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanWildCardTable
            ((tMacAddr *) pNextMultiIndex->pIndex[0].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanWildCardTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanWildCardEgressPortsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanWildCardTable
        ((*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList))
        == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot1qFutureVlanWildCardEgressPorts
        ((*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanWildCardRowStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanWildCardTable
        ((*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList))
        == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureVlanWildCardRowStatus
        ((*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanWildCardEgressPortsSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanWildCardEgressPorts
        ((*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanWildCardRowStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureVlanWildCardRowStatus
        ((*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanWildCardEgressPortsTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanWildCardEgressPorts
        (pu4Error,
         (*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanWildCardRowStatusTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanWildCardRowStatus
        (pu4Error,
         (*(tMacAddr *) pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanWildCardTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureVlanWildCardTable (pu4Error, pSnmpIndexList,
                                                     pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureStaticUnicastExtnTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureStaticUnicastExtnTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureStaticUnicastExtnTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureStaticConnectionIdentifierGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureStaticUnicastExtnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pMultiData->pOctetStrValue->i4_Length = 6;

    i4RetVal =
        nmhGetDot1qFutureStaticConnectionIdentifier (pMultiIndex->
                                                     pIndex[0].u4_ULongValue,
                                                     (*(tMacAddr *)
                                                      pMultiIndex->
                                                      pIndex
                                                      [1].pOctetStrValue->
                                                      pu1_OctetList),
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     (tMacAddr *) pMultiData->
                                                     pOctetStrValue->
                                                     pu1_OctetList);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureStaticConnectionIdentifierSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qFutureStaticConnectionIdentifier
        (pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureStaticConnectionIdentifierTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal = nmhTestv2Dot1qFutureStaticConnectionIdentifier
        (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[2].i4_SLongValue,
         (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureStaticUnicastExtnTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureStaticUnicastExtnTable (pu4Error,
                                                          pSnmpIndexList,
                                                          pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureUnicastMacLearningLimitGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhGetDot1qFutureUnicastMacLearningLimit (&(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureUnicastMacLearningLimitSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureUnicastMacLearningLimit (pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanSubnetBasedOnAllPortsSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();
    i4RetVal = (nmhSetDot1qFutureVlanSubnetBasedOnAllPorts
                (pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureUnicastMacLearningLimitTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureUnicastMacLearningLimit (pu4Error,
                                                            pMultiData->
                                                            u4_ULongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanSubnetBasedOnAllPortsTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts
                (pu4Error, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFutureUnicastMacLearningLimitDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qFutureUnicastMacLearningLimit (pu4Error,
                                                           pSnmpIndexList,
                                                           pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanSubnetBasedOnAllPortsDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhDepv2Dot1qFutureVlanSubnetBasedOnAllPorts
                (pu4Error, pSnmpIndexList, pSnmpvarbinds));
    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanPortSubnetMapTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanPortSubnetMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanPortSubnetMapVidGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1qFutureVlanPortSubnetMapVid
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapARPOptionGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1qFutureVlanPortSubnetMapARPOption
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetDot1qFutureVlanPortSubnetMapRowStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapVidSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetDot1qFutureVlanPortSubnetMapVid
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapARPOptionSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetDot1qFutureVlanPortSubnetMapARPOption
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhSetDot1qFutureVlanPortSubnetMapRowStatus
                (pMultiIndex->pIndex[0].i4_SLongValue,
                 pMultiIndex->pIndex[1].u4_ULongValue,
                 pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetMapVid (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiIndex->pIndex
                                                          [1].u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapARPOptionTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapRowStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = (nmhDepv2Dot1qFutureVlanPortSubnetMapTable
                (pu4Error, pSnmpIndexList, pSnmpvarbinds));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureStVlanExtTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureStVlanExtTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureStVlanExtTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureStVlanEgressEthertypeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureStVlanEgressEthertype
         (pMultiIndex->pIndex[0].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureStVlanEgressEthertypeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureStVlanEgressEthertype
         (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureStVlanEgressEthertypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureStVlanEgressEthertype (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureStVlanExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhDepv2Dot1qFutureStVlanExtTable (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanPortSubnetMapExtTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanPortSubnetMapExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanPortSubnetMapExtVidGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        (nmhGetDot1qFutureVlanPortSubnetMapExtVid
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtARPOptionGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortSubnetMapExtARPOption
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtRowStatusGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        (nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, &(pMultiData->i4_SLongValue)));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtVidSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortSubnetMapExtVid
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtARPOptionSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortSubnetMapExtARPOption
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtRowStatusSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal =
        (nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus
         (pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiIndex->pIndex[1].u4_ULongValue,
          pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtVidTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid (pu4Error,
                                                             pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             pMultiIndex->pIndex
                                                             [1].u4_ULongValue,
                                                             pMultiIndex->pIndex
                                                             [2].u4_ULongValue,
                                                             pMultiData->
                                                             i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtARPOptionTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiIndex->
                                                                   pIndex[1].
                                                                   u4_ULongValue,
                                                                   pMultiIndex->
                                                                   pIndex[2].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtRowStatusTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiIndex->
                                                                   pIndex[1].
                                                                   u4_ULongValue,
                                                                   pMultiIndex->
                                                                   pIndex[2].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanPortSubnetMapExtTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = (nmhDepv2Dot1qFutureVlanPortSubnetMapExtTable
                (pu4Error, pSnmpIndexList, pSnmpvarbinds));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalsFdbFlushGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhGetDot1qFutureVlanGlobalsFdbFlush
        (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanUserDefinedTPIDGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhGetDot1qFutureVlanUserDefinedTPID
        (&(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalsFdbFlushSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhSetDot1qFutureVlanGlobalsFdbFlush (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanUserDefinedTPIDSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhSetDot1qFutureVlanUserDefinedTPID (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalsFdbFlushTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhTestv2Dot1qFutureVlanGlobalsFdbFlush
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanUserDefinedTPIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhTestv2Dot1qFutureVlanUserDefinedTPID
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanGlobalsFdbFlushDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = nmhDepv2Dot1qFutureVlanGlobalsFdbFlush (pu4Error,
                                                       pSnmpIndexList,
                                                       pSnmpvarbinds);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanUserDefinedTPIDDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    i4RetVal = nmhDepv2Dot1qFutureVlanUserDefinedTPID (pu4Error,
                                                       pSnmpIndexList,
                                                       pSnmpvarbinds);
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1qFutureVlanLoopbackTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFutureVlanLoopbackTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFutureVlanLoopbackTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1qFutureVlanLoopbackStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFutureVlanLoopbackTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4RetVal =
        nmhGetDot1qFutureVlanLoopbackStatus (pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanLoopbackStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qFutureVlanLoopbackStatus (pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanLoopbackStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qFutureVlanLoopbackStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanLoopbackTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;

    VLAN_LOCK ();

    i4RetVal =
        nmhDepv2Dot1qFutureVlanLoopbackTable (pu4Error, pSnmpIndexList,
                                              pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1qFutureVlanRemoteFdbFlushGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal =
        (nmhGetDot1qFutureVlanRemoteFdbFlush (&(pMultiData->i4_SLongValue)));
    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qFutureVlanRemoteFdbFlushSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal = nmhSetDot1qFutureVlanRemoteFdbFlush (pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanRemoteFdbFlushTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    VLAN_LOCK ();
    i4RetVal =
        nmhTestv2Dot1qFutureVlanRemoteFdbFlush (pu4Error,
                                                pMultiData->i4_SLongValue);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureVlanRemoteFdbFlushDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal = SNMP_FAILURE;
    VLAN_LOCK ();
    i4RetVal =
        nmhDepv2Dot1qFutureVlanRemoteFdbFlush (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds);
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureStVlanTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qFutureStVlanType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue));
    VLAN_UNLOCK ();
    return i4RetVal;

}

INT4
Dot1qFutureStVlanVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceDot1qFutureStVlanExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFutureStVlanVid (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;

}
