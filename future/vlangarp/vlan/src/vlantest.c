/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlantest.c,v 1.179.22.1 2018/03/15 13:00:05 siva Exp $
 *
 * Description           : This file contains test routines for VLAN objects
 *
 *******************************************************************/
/*****************************************************************************/
/*  FILE NAME             : vlantest.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"
#include "vlancli.h"
#include "cli.h"

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Dot1qForwardAllStaticPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qForwardAllStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qForwardAllStaticPorts (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1qVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValDot1qForwardAllStaticPorts)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestForwardAllStaticPorts (pu4ErrorCode,
                                       u4Dot1qVlanIndex,
                                       pTestValDot1qForwardAllStaticPorts)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qForwardAllForbiddenPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qForwardAllForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qForwardAllForbiddenPorts (UINT4 *pu4ErrorCode,
                                        UINT4 u4Dot1qVlanIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pForbiddenPorts)
{
#ifdef GARP_EXTENDED_FILTER

    UINT1              *pPortList = NULL;
    UINT1               bu1RetVal = VLAN_FALSE;
    UINT1               u1Result = VLAN_TRUE;
    tVlanId             VlanId = (tVlanId) u4Dot1qVlanIndex;
    tVlanCurrEntry     *pCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_NOT_ACTIVE_ERR);
        return SNMP_FAILURE;
    }

    if ((pForbiddenPorts->i4_Length <= 0) ||
        (pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
                              pForbiddenPorts->i4_Length, bu1RetVal);
    if (bu1RetVal == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_MAX_PORTLIST_ERR);
        return SNMP_FAILURE;
    }

    if (VlanIsPortListValid (pForbiddenPorts->pu1_OctetList,
                             pForbiddenPorts->i4_Length) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORTLIST_ERR);
        return SNMP_FAILURE;
    }
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhTestv2Dot1qForwardAllForbiddenPorts: Error in allocating memory "
                  "for pPortList\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (pPortList, 0, sizeof (tLocalPortList));
    MEMCPY (pPortList,
            pForbiddenPorts->pu1_OctetList, pForbiddenPorts->i4_Length);
    VLAN_ARE_PORTS_EXCLUSIVE (pCurrEntry->AllGrps.StaticPorts,
                              pPortList, u1Result);
    if (u1Result == VLAN_FALSE)
    {
        CLI_SET_ERR (CLI_VLAN_FWD_PORTS_EXCL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        UtilPlstReleaseLocalPortList (pPortList);
        return SNMP_FAILURE;
    }

    UtilPlstReleaseLocalPortList (pPortList);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4Dot1qVlanIndex);
    UNUSED_PARAM (pForbiddenPorts);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
#endif /* FEXTENDED_FILTERING_CHANGE */
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qForwardAllTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qForwardAllTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qForwardUnregisteredTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qForwardUnregisteredStaticPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qForwardUnregisteredStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qForwardUnregisteredStaticPorts (UINT4 *pu4ErrorCode,
                                              UINT4 u4Dot1qVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pTestValUnregPorts)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestForwardUnregStaticPorts (pu4ErrorCode,
                                         u4Dot1qVlanIndex,
                                         pTestValUnregPorts) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qForwardUnregisteredForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qForwardUnregisteredForbiddenPorts (UINT4 *pu4ErrorCode,
                                                 UINT4 u4Dot1qVlanIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pUnregPorts)
{

#ifdef GARP_EXTENDED_FILTER

    UINT1               bu1RetVal = VLAN_FALSE;
    UINT1               u1Result = VLAN_TRUE;
    tLocalPortList      ForbiddenPorts;
    tVlanId             VlanId = (tVlanId) u4Dot1qVlanIndex;
    tVlanCurrEntry     *pCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_NOT_ACTIVE_ERR);
        return SNMP_FAILURE;
    }

    if ((pUnregPorts->i4_Length <= 0) ||
        (pUnregPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pUnregPorts->pu1_OctetList,
                              pUnregPorts->i4_Length, bu1RetVal);
    if (bu1RetVal == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_MAX_PORTLIST_ERR);
        return SNMP_FAILURE;
    }

    if (VlanIsPortListValid (pUnregPorts->pu1_OctetList,
                             pUnregPorts->i4_Length) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORTLIST_ERR);
        return SNMP_FAILURE;
    }

    MEMSET (ForbiddenPorts, 0, sizeof (tLocalPortList));
    MEMCPY (ForbiddenPorts, pUnregPorts->pu1_OctetList, pUnregPorts->i4_Length);
    VLAN_ARE_PORTS_EXCLUSIVE (pCurrEntry->UnRegGrps.StaticPorts,
                              ForbiddenPorts, u1Result);
    if (u1Result == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_VLAN_PORT_OVERLAP_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4Dot1qVlanIndex);
    UNUSED_PARAM (pUnregPorts);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qForwardUnregisteredTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qForwardUnregisteredTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qStaticUnicastTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qStaticUnicastAllowedToGoTo
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort

                The Object 
                testValDot1qStaticUnicastAllowedToGoTo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qStaticUnicastAllowedToGoTo (UINT4 *pu4ErrorCode,
                                          UINT4 u4Dot1qFdbId,
                                          tMacAddr Dot1qStaticUnicastAddress,
                                          INT4 i4Dot1qStaticUnicastReceivePort,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pAllowedToGo)
{
    UNUSED_PARAM (u4Dot1qFdbId);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestStaticUcastAllowedToGoTo (pu4ErrorCode, u4Dot1qFdbId,
                                          Dot1qStaticUnicastAddress,
                                          i4Dot1qStaticUnicastReceivePort,
                                          pAllowedToGo) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qStaticUnicastStatus
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort

                The Object 
                testValDot1qStaticUnicastStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qStaticUnicastStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1qFdbId,
                                   tMacAddr Dot1qStaticUnicastAddress,
                                   INT4 i4Dot1qStaticUnicastReceivePort,
                                   INT4 i4TestValDot1qStaticUnicastStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestStaticUnicastStatus (pu4ErrorCode, u4Dot1qFdbId,
                                     Dot1qStaticUnicastAddress,
                                     i4Dot1qStaticUnicastReceivePort,
                                     i4TestValDot1qStaticUnicastStatus)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qStaticUnicastTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qStaticUnicastTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qStaticMulticastTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort

                The Object 
                testValDot1qStaticMulticastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qStaticMulticastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                UINT4 u4Dot1qVlanIndex,
                                                tMacAddr
                                                Dot1qStaticMulticastAddress,
                                                INT4
                                                i4Dot1qStaticMulticastReceivePort,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pEgressPorts)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestStaticMcastEgressPorts (pu4ErrorCode, u4Dot1qVlanIndex,
                                        Dot1qStaticMulticastAddress,
                                        i4Dot1qStaticMulticastReceivePort,
                                        pEgressPorts) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort

                The Object 
                testValDot1qStaticMulticastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                   UINT4 u4Dot1qVlanIndex,
                                                   tMacAddr
                                                   Dot1qStaticMulticastAddress,
                                                   INT4
                                                   i4Dot1qStaticMulticastReceivePort,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pForbiddenPorts)
{
    tVlanId             VlanId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               au1ForbiddenPortList[VLAN_PORT_LIST_SIZE];
    UINT4               u4IfIndex = 0;

    MEMSET (au1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMCPY (au1ForbiddenPortList, pForbiddenPorts->pu1_OctetList,
            pForbiddenPorts->i4_Length);

    if (i4Dot1qStaticMulticastReceivePort > VLAN_MAX_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanIsValidMcastAddr (Dot1qStaticMulticastAddress) == VLAN_FAILURE)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_MULTICAST_MAC);
        return SNMP_FAILURE;
    }

    if ((pForbiddenPorts->i4_Length <= 0) ||
        (pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {

        CLI_SET_ERR (CLI_VLAN_INVALID_PORTLIST);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    {
        UINT1               bu1RetVal = VLAN_FALSE;
        VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
                                  pForbiddenPorts->i4_Length, bu1RetVal);
        if (bu1RetVal == VLAN_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_MAX_PORTLIST_ERR);
            return SNMP_FAILURE;

        }
    }
    VlanId = (tVlanId) u4Dot1qVlanIndex;
    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_STATIC_ENTRY_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    VLAN_IS_PORTLIST_SUBSET_TO_EGRESS (pStVlanEntry, au1ForbiddenPortList,
                                       u1Result);

    if (u1Result == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_MCAST_FBIDEN_PORTLIST_SUBSET_ERR);
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_ENTRY_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    pStMcastEntry
        = VlanGetStMcastEntryWithExactRcvPort (Dot1qStaticMulticastAddress,
                                               (INT2)
                                               i4Dot1qStaticMulticastReceivePort,
                                               pVlanEntry);

    if (pStMcastEntry != NULL)
    {
        VLAN_ARE_PORTS_EXCLUSIVE (pStMcastEntry->EgressPorts,
                                  au1ForbiddenPortList, u1Result);

        if (u1Result == VLAN_FALSE)
        {

            CLI_SET_ERR (CLI_VLAN_PORT_OVERLAP_ERR);
            return SNMP_FAILURE;
        }
    }

    if (i4Dot1qStaticMulticastReceivePort != 0)
    {
        if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                        i4Dot1qStaticMulticastReceivePort,
                                        &u4IfIndex) != VCM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
            return SNMP_FAILURE;
        }

        if ((VLAN_GET_PORT_ENTRY (i4Dot1qStaticMulticastReceivePort)) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
            return SNMP_FAILURE;
        }
    }

    if (VlanIsPortListValid (pForbiddenPorts->pu1_OctetList,
                             pForbiddenPorts->i4_Length) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORTLIST_ERR);
        return SNMP_FAILURE;
    }

    if (pStMcastEntry == NULL)
    {
        if (VlanPbIsMulticastMacLimitExceeded () == VLAN_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_MAX_MCAST_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qStaticMulticastStatus
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort

                The Object 
                testValDot1qStaticMulticastStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qStaticMulticastStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1qVlanIndex,
                                     tMacAddr Dot1qStaticMulticastAddress,
                                     INT4 i4Dot1qStaticMulticastReceivePort,
                                     INT4 i4TestValDot1qStaticMulticastStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestStaticMulticastStatus (pu4ErrorCode, u4Dot1qVlanIndex,
                                       Dot1qStaticMulticastAddress,
                                       i4Dot1qStaticMulticastReceivePort,
                                       i4TestValDot1qStaticMulticastStatus)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qStaticMulticastTable
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qStaticMulticastTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qVlanStaticTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qVlanStaticName
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qVlanStaticName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qVlanStaticName (UINT4 *pu4ErrorCode,
                              UINT4 u4Dot1qVlanIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValDot1qVlanStaticName)
{
    UNUSED_PARAM (u4Dot1qVlanIndex);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (pTestValDot1qVlanStaticName->i4_Length <= VLAN_STATIC_MAX_NAME_LEN)
    {

        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qVlanStaticEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qVlanStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qVlanStaticEgressPorts (UINT4 *pu4ErrorCode,
                                     UINT4 u4Dot1qVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE * pEgressPorts)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    VLAN_PBB_PERF_MARK_START_TIME ();
    VLAN_PBB_PERF_RESET (gu4DelVlanIsidTimeTaken);
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }
    if (VlanTestDot1qVlanStaticEgressPorts (pu4ErrorCode, u4Dot1qVlanIndex,
                                            pEgressPorts) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
    VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qVlanForbiddenEgressPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qVlanForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qVlanForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                        UINT4 u4Dot1qVlanIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pForbiddenPorts)
{
    tStaticVlanEntry   *pStVlanEntry;
    UINT1               u1Result = VLAN_FALSE;

    UNUSED_PARAM (u4Dot1qVlanIndex);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((pForbiddenPorts->i4_Length <= 0) ||
        (pForbiddenPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    {
        UINT1               bu1RetVal = VLAN_FALSE;
        VLAN_IS_EXCEED_MAX_PORTS (pForbiddenPorts->pu1_OctetList,
                                  pForbiddenPorts->i4_Length, bu1RetVal);
        if (bu1RetVal == VLAN_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_MAX_PORTLIST_ERR);
            return SNMP_FAILURE;

        }
    }
    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qVlanIndex);

    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    VLAN_ARE_PORTS_EXCL_WITH_EGRESS (pStVlanEntry,
                                     pForbiddenPorts->pu1_OctetList, u1Result);

    if (u1Result == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_OVERLAP_ERR);
        return SNMP_FAILURE;
    }

    if (VlanIsPortListValid (pForbiddenPorts->pu1_OctetList,
                             pForbiddenPorts->i4_Length) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_VLAN_PORTLIST_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qVlanStaticUntaggedPorts
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qVlanStaticUntaggedPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qVlanStaticUntaggedPorts (UINT4 *pu4ErrorCode,
                                       UINT4 u4Dot1qVlanIndex,
                                       tSNMP_OCTET_STRING_TYPE * pUntagPorts)
{
    VLAN_PBB_PERF_MARK_START_TIME ();
    VLAN_PBB_PERF_RESET (gu4DelVlanIsidTimeTaken);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestDot1qVlanStaticUntaggedPorts (pu4ErrorCode, u4Dot1qVlanIndex,
                                              pUntagPorts) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;

    }

    VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
    VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qVlanStaticRowStatus
 Input       :  The Indices
                Dot1qVlanIndex

                The Object 
                testValDot1qVlanStaticRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qVlanStaticRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4Dot1qVlanIndex,
                                   INT4 i4TestValDot1qVlanStaticRowStatus)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT4               u4NumVlans;
#ifdef MEF_WANTED
    tEvcInfo            IssEvcInfo;
#endif

    if (CfaIsPortVlanExists ((UINT2) u4Dot1qVlanIndex) == OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_VID_USED_ERR);
        return SNMP_FAILURE;
    }

    nmhGetDot1qNumVlans (&u4NumVlans);
    if ((i4TestValDot1qVlanStaticRowStatus == VLAN_CREATE_AND_WAIT) ||
        (i4TestValDot1qVlanStaticRowStatus == VLAN_CREATE_AND_GO))
    {

        pVlanCurrEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qVlanIndex);

        if ((u4NumVlans == VLAN_MAX_CURR_ENTRIES) && (pVlanCurrEntry == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_MAX_LIMIT_REACHED);
            return SNMP_FAILURE;
        }

    }

    if ((i4TestValDot1qVlanStaticRowStatus == VLAN_CREATE_AND_WAIT) ||
        (i4TestValDot1qVlanStaticRowStatus == VLAN_CREATE_AND_GO))
    {
        if (VlanAstIsPvrstStartedInContext (VLAN_CURR_CONTEXT_ID ())
            == AST_TRUE)
        {
            if (gpVlanContextInfo->VlanInfo.u4NumConfiguredVlans >=
                AST_MAX_PVRST_INSTANCES)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;;
                CLI_SET_ERR (CLI_PVRST_MAX_VLAN);
                return SNMP_FAILURE;
            }

        }
    }

    if ((u4Dot1qVlanIndex == VLAN_DEF_VLAN_ID) &&
        (i4TestValDot1qVlanStaticRowStatus == VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_DEF_VLAN_ERR);
        return SNMP_FAILURE;

    }
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qVlanStaticRowStatus < VLAN_ACTIVE) ||
        (i4TestValDot1qVlanStaticRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qVlanIndex) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_VLANID_ERR);
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((UINT2) u4Dot1qVlanIndex);

    if (i4TestValDot1qVlanStaticRowStatus == VLAN_DESTROY &&
        pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    /* Verify whether EVC is associated with Vlan */
#ifdef MEF_WANTED
    VLAN_MEMSET (&IssEvcInfo, 0, sizeof (tEvcInfo));
    MefApiGetEvcInfo (u4Dot1qVlanIndex, &IssEvcInfo);

    if ((i4TestValDot1qVlanStaticRowStatus == VLAN_DESTROY) &&
        (IssEvcInfo.SVlanId == ((UINT2) u4Dot1qVlanIndex)))
    {
        CLI_SET_ERR (CLI_VLAN_EVC_ASSOCIATION_EXIST_ERR);
        return SNMP_FAILURE;
    }
#endif

    VLAN_MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.InVlanId = (tVlanId) u4Dot1qVlanIndex;
    L2PvlanMappingInfo.u1RequestType = L2IWF_NUM_MAPPED_VLANS;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (i4TestValDot1qVlanStaticRowStatus == VLAN_DESTROY)
    {
        if (VlanL2VpnIsMplsIfPresent ((tVlanId) u4Dot1qVlanIndex) != VLAN_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_L2VPN_VLAN_DEL_ERR);
            return SNMP_FAILURE;
        }

        /* On deleting the primary vlan, if the associated secondary vlan is
         * present then should not allow to delete the vlan. */
        if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
        {
            if (L2PvlanMappingInfo.u2NumMappedVlans != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_PVLAN_SEC_VLAN_ASSOCIATION_EXIST_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    /* Check if the VLAN is ICCL VLAN */
    if (VlanIcchIsIcclVlan ((UINT2) u4Dot1qVlanIndex) == OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_BLOCK_ICCL_VLAN);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qVlanStaticTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qVlanStaticTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qPortVlanTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qPvid
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1qPvid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPvid (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                    UINT4 u4TestValDot1qPvid)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT4               u4IfIndex = 0;

    if (i4Dot1dBasePort > VLAN_MAX_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4TestValDot1qPvid) == VLAN_FALSE)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_VLANID_ERR);
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1dBasePort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1dBasePort)) == VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry ((UINT2) u4TestValDot1qPvid);

    /* If the current entry is null, then don't allow that vlan to
     * be set as the PVID for a port. */
    if (pVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qPortAcceptableFrameTypes
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1qPortAcceptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPortAcceptableFrameTypes (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1dBasePort,
                                        INT4
                                        i4TestValDot1qPortAcceptableFrameTypes)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT2               u2Port;
    UINT4               u4IfIndex = 0;
    UINT4               u4IcclIfIndex = 0;

    u2Port = (UINT2) i4Dot1dBasePort;
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    /* Acceptable frame type should be ALL always on ICCL */
    VlanIcchGetIcclIfIndex (&u4IcclIfIndex);
    if (u4IfIndex == u4IcclIfIndex)
    {
        if (i4TestValDot1qPortAcceptableFrameTypes != VLAN_ADMIT_ALL_FRAMES)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_BLOCK_FRAME_TYPE_ON_ICCL);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValDot1qPortAcceptableFrameTypes != VLAN_ADMIT_ALL_FRAMES) &&
        (i4TestValDot1qPortAcceptableFrameTypes !=
         VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES) &&
        (i4TestValDot1qPortAcceptableFrameTypes !=
         VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u1PortType == (UINT1) VLAN_ACCESS_PORT)
    {
        if (i4TestValDot1qPortAcceptableFrameTypes !=
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            CLI_SET_ERR (CLI_VLAN_PORT_ACCEPT_FRAME_TYPE_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValDot1qPortAcceptableFrameTypes == VLAN_ADMIT_ALL_FRAMES) ||
        (i4TestValDot1qPortAcceptableFrameTypes ==
         VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
    {
        if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1dBasePort)) ==
            VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
            return SNMP_FAILURE;
        }
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if (pVlanPbPortEntry != NULL)
        {
            if ((VLAN_PB_NETWORK_PORT (u2Port) == VLAN_TRUE) &&
                (i4TestValDot1qPortAcceptableFrameTypes ==
                 VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_VLAN_PB_NETWORK_PORT_FRAME_TYPE_ERR);
                return SNMP_FAILURE;
            }

            /* On CNP - Port based acceptable frame types is always 
             * Admit only Untagged and priority tagged frames. */
            if (((pVlanPbPortEntry->u1PbPortType) ==
                 VLAN_CNP_PORTBASED_PORT) &&
                (i4TestValDot1qPortAcceptableFrameTypes !=
                 VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_VLAN_PB_FRAME_TYPE_ERR);
                return SNMP_FAILURE;
            }

            /* On PCEP acceptable frame types is always 
             * Admit only Untagged and priority tagged frames. */
            if (((pVlanPbPortEntry->u1PbPortType) ==
                 VLAN_PROP_CUSTOMER_EDGE_PORT)
                && (i4TestValDot1qPortAcceptableFrameTypes !=
                    VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_VLAN_PB_FRAME_TYPE_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
    {
        /* On CBP or PIP - frame types is always Tagged
           tagged frames. */
        if (pVlanPbPortEntry != NULL)
        {
            if (((pVlanPbPortEntry->u1PbPortType) ==
                 VLAN_CUSTOMER_BACKBONE_PORT) &&
                (i4TestValDot1qPortAcceptableFrameTypes !=
                 VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_VLAN_PBB_FRAME_TYPE_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    else                        /* Tunnel Status is not considered in PBB */
    {
        /* Tunneld ports should not be set to Accept Only Tagged frames */
        if (((INT4) VLAN_TUNNEL_STATUS (pVlanPortEntry) == VLAN_ENABLED)
            && (i4TestValDot1qPortAcceptableFrameTypes ==
                VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_VLAN_TUNNELD_PORT_STATUS_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qPortIngressFiltering
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1qPortIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPortIngressFiltering (UINT4 *pu4ErrorCode,
                                    INT4 i4Dot1dBasePort,
                                    INT4 i4TestValDot1qPortIngressFiltering)
{
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1PvlanPortType;
    UINT4               u4IfIndex = 0;

    if (i4Dot1dBasePort > VLAN_MAX_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY ((UINT2) i4Dot1dBasePort);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1dBasePort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qPortIngressFiltering != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qPortIngressFiltering != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_INCONSISTENT_VALUE_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qPortIngressFiltering == VLAN_SNMP_FALSE)
    {
        if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1dBasePort))
            == VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
            return SNMP_FAILURE;
        }

        /* If the port type is of pvlan port type then should not allow to
         * disable the ingress filtering */
        VlanL2IwfGetVlanPortType (VLAN_GET_IFINDEX (i4Dot1dBasePort),
                                  &u1PvlanPortType);

        if ((u1PvlanPortType == VLAN_PROMISCOUS_PORT) ||
            (u1PvlanPortType == VLAN_HOST_PORT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_PVLAN_ING_FILTER_ERR);
            return SNMP_FAILURE;
        }
    }

    /* Error message is displayed only when disabling Ingress-Filtering */
    if (i4TestValDot1qPortIngressFiltering == VLAN_SNMP_FALSE)
    {
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {
            if (pVlanPbPortEntry != NULL)
            {
                if ((pVlanPbPortEntry->u1PbPortType) == VLAN_CNP_TAGGED_PORT)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_VLAN_PB_ING_FILTER_ERR);
                    return SNMP_FAILURE;
                }
            }
        }
    }

    if (i4TestValDot1qPortIngressFiltering == VLAN_SNMP_TRUE)
    {
        if (VlanLaIsMclagInterface (u4IfIndex) == VLAN_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BLOCK_INGRESS_FILTER_ON_MCLAG_IF);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qPortVlanTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qPortVlanTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qLearningConstraintsTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qConstraintType
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet

                The Object 
                testValDot1qConstraintType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qConstraintType (UINT4 *pu4ErrorCode,
                              UINT4 u4Dot1qConstraintVlan,
                              INT4 i4Dot1qConstraintSet,
                              INT4 i4TestValDot1qConstraintType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1qConstraintVlan);
    UNUSED_PARAM (i4Dot1qConstraintSet);
    UNUSED_PARAM (i4TestValDot1qConstraintType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qConstraintStatus
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet

                The Object 
                testValDot1qConstraintStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qConstraintStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4Dot1qConstraintVlan,
                                INT4 i4Dot1qConstraintSet,
                                INT4 i4TestValDot1qConstraintStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Dot1qConstraintVlan);
    UNUSED_PARAM (i4Dot1qConstraintSet);
    UNUSED_PARAM (i4TestValDot1qConstraintStatus);

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qLearningConstraintsTable
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qLearningConstraintsTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qConstraintSetDefault
 Input       :  The Indices

                The Object 
                testValDot1qConstraintSetDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qConstraintSetDefault (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValDot1qConstraintSetDefault)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDot1qConstraintSetDefault);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qConstraintTypeDefault
 Input       :  The Indices

                The Object 
                testValDot1qConstraintTypeDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qConstraintTypeDefault (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValDot1qConstraintTypeDefault)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDot1qConstraintTypeDefault);

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qConstraintSetDefault
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qConstraintSetDefault (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qConstraintTypeDefault
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qConstraintTypeDefault (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dTrafficClassesEnabled
 Input       :  The Indices

                The Object 
                testValDot1dTrafficClassesEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dTrafficClassesEnabled (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValDot1dTrafficClassesEnabled)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dTrafficClassesEnabled == VLAN_SNMP_TRUE)
        || (i4TestValDot1dTrafficClassesEnabled == VLAN_SNMP_FALSE))
    {

        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dTrafficClassesEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dTrafficClassesEnabled (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortDefaultUserPriority
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1dPortDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortDefaultUserPriority (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1dBasePort,
                                       INT4
                                       i4TestValDot1dPortDefaultUserPriority)
{
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dPortDefaultUserPriority < 0) ||
        (i4TestValDot1dPortDefaultUserPriority > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1dBasePort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1dBasePort)) == VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortNumTrafficClasses
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1dPortNumTrafficClasses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortNumTrafficClasses (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                     INT4 i4TestValDot1dPortNumTrafficClasses)
{
    UINT4               u4IfIndex = 0;
    INT4                i4BrgPortType = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((VlanCfaGetInterfaceBrgPortType ((UINT4) i4Dot1dBasePort,
                                         &i4BrgPortType) == VLAN_SUCCESS) &&
        (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT))

    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dPortNumTrafficClasses < 1) ||
        (i4TestValDot1dPortNumTrafficClasses > VLAN_MAX_TRAFF_CLASS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1dBasePort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dPortPriorityTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dPortPriorityTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dRegenUserPriority
 Input       :  The Indices
                Dot1dBasePort
                Dot1dUserPriority

                The Object 
                testValDot1dRegenUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dRegenUserPriority (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                 INT4 i4Dot1dUserPriority,
                                 INT4 i4TestValDot1dRegenUserPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4Dot1dUserPriority < 0)
        || (i4Dot1dUserPriority > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dRegenUserPriority < 0) ||
        (i4TestValDot1dRegenUserPriority > VLAN_HIGHEST_PRIORITY))
    {
        /* To return WrongValue Error when the Value does not fall 
         * in the expected range.
         */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    if ((pPortEntry->u1IfType == VLAN_ETHERNET_INTERFACE_TYPE) ||
        (pPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
    {
        /* Priority Regeneration is not allowed for logical ports */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dUserPriorityRegenTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dUserPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dUserPriorityRegenTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dTrafficClass
 Input       :  The Indices
                Dot1dBasePort
                Dot1dTrafficClassPriority

                The Object 
                testValDot1dTrafficClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dTrafficClass (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                            INT4 i4Dot1dTrafficClassPriority,
                            INT4 i4TestValDot1dTrafficClass)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;
    UINT4               u4IfIndex = 0;
    INT4                i4BrgPortType = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((VlanCfaGetInterfaceBrgPortType ((UINT4) i4Dot1dBasePort,
                                         &i4BrgPortType) == VLAN_SUCCESS) &&
        (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT))

    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4Dot1dTrafficClassPriority < 0) ||
        (i4Dot1dTrafficClassPriority > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dTrafficClass < 0) ||
        (i4TestValDot1dTrafficClass >= VLAN_MAX_TRAFF_CLASS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1dBasePort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dTrafficClassTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dTrafficClassPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dTrafficClassTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1qFutureVlanPortType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Dot1qFutureVlanPortType (UINT4 *pu4ErrorCode,
                                  INT4 i4Dot1qFutureVlanPort,
                                  INT4 i4TestValDot1qFutureVlanPortType)
{
    UINT2               u2Port;
    tVlanId             ScanVlanId;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanCurrEntry     *pVlanEntry;
    UINT1               u1Result = VLAN_FALSE;
    INT4                i4RetVal;
    INT4                i4GvrpStatus = OSIX_DISABLED;
    UINT1               u1MvrpStatus = OSIX_DISABLED;
    UINT4               u4IfIndex = 0;
    UINT4               u4IcclIfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

/* LAG port cannot be configured as HOST_PORT or PROMISCOUS_PORT*/
    if ((i4TestValDot1qFutureVlanPortType == VLAN_PROMISCOUS_PORT) ||
        (i4TestValDot1qFutureVlanPortType == VLAN_HOST_PORT))
    {
        if (pVlanPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_PVLAN_PORT_ERR);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValDot1qFutureVlanPortType != VLAN_ACCESS_PORT) &&
        (i4TestValDot1qFutureVlanPortType != VLAN_TRUNK_PORT) &&
        (i4TestValDot1qFutureVlanPortType != VLAN_HYBRID_PORT) &&
        (i4TestValDot1qFutureVlanPortType != VLAN_PROMISCOUS_PORT) &&
        (i4TestValDot1qFutureVlanPortType != VLAN_HOST_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to change the port type from access port if tunnelling is
     * enabled on that port */
    if ((VLAN_IS_TUNNEL_ENABLED_ON_PORT (pVlanPortEntry) == VLAN_TRUE) &&
        (VLAN_PB_CUSTOMER_PORT (u2Port) == VLAN_TRUE))
    {
        /* In case of PCEP/CEP/CNP/PCNP, tunnel status will always be enabled.
         * So those ports will come under this if check.
         * Those ports will only be access ports*/
        if (i4TestValDot1qFutureVlanPortType != VLAN_ACCESS_PORT)
        {
            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {

                if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR
                        (CLI_VLAN_CONFIG_NOT_ALLOWED_1AH_CNP_PORT_BASED);
                    return SNMP_FAILURE;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_VLAN_CONFIG_NOT_ALLOWED_1AD);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_MODE_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
    {
        if (pVlanPortEntry->pVlanPbPortEntry)
        {
            if (((pVlanPortEntry->pVlanPbPortEntry->u1PbPortType ==
                  VLAN_VIRTUAL_INSTANCE_PORT) ||
                 (pVlanPortEntry->pVlanPbPortEntry->u1PbPortType ==
                  VLAN_CUSTOMER_BACKBONE_PORT)) &&
                (i4TestValDot1qFutureVlanPortType != VLAN_HYBRID_PORT))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_CONFIG_NOT_ALLOWED_1AH_CBP);
                return SNMP_FAILURE;
            }
        }
    }

    if (i4TestValDot1qFutureVlanPortType == VLAN_ACCESS_PORT)
    {
        GARP_LOCK ();
        i4RetVal = nmhGetFsDot1qPortGvrpStatus
            (VLAN_GET_PHY_PORT (i4Dot1qFutureVlanPort), &i4GvrpStatus);

        GARP_UNLOCK ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            if (i4GvrpStatus == OSIX_ENABLED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_GVRP_PORT_MODE_ERR);
                return SNMP_FAILURE;
            }
        }
        if (pVlanPortEntry->u1AccpFrmTypes !=
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_PORT_ACCEPT_FRAME_TYPE_ERR);
            return SNMP_FAILURE;
        }

        i4RetVal = VlanL2IwfGetVlanPortMvrpStatus
            (VLAN_GET_PHY_PORT (i4Dot1qFutureVlanPort), &u1MvrpStatus);

        if (i4RetVal == L2IWF_SUCCESS)
        {
            if (u1MvrpStatus == OSIX_ENABLED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_MVRP_PORT_MODE_ERR);
                return SNMP_FAILURE;
            }
        }

        VLAN_SCAN_VLAN_TABLE (ScanVlanId)
        {
            pVlanEntry = VLAN_GET_CURR_ENTRY (ScanVlanId);

            if (pVlanEntry->pStVlanEntry != NULL)
            {
                VLAN_IS_UNTAGGED_PORT (pVlanEntry->pStVlanEntry,
                                       i4Dot1qFutureVlanPort, u1Result);

                if (u1Result == VLAN_FALSE)
                {
                    VLAN_IS_EGRESS_PORT (pVlanEntry->pStVlanEntry,
                                         i4Dot1qFutureVlanPort, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_VLAN_TAGGED_MEMBER_PORT_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
        }

    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if (VLAN_PB_NETWORK_PORT (i4Dot1qFutureVlanPort) == VLAN_TRUE)
        {
            if ((i4TestValDot1qFutureVlanPortType != VLAN_TRUNK_PORT) &&
                (i4TestValDot1qFutureVlanPortType != VLAN_HYBRID_PORT))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_TUNNEL_NETWORK_PORT_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    if (i4TestValDot1qFutureVlanPortType != VLAN_TRUNK_PORT)
    {
        VlanIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex == u4IfIndex)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_ICCL_TRUNK_PORT_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortMacMapVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                testValDot1qFutureVlanPortMacMapVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortMacMapVid (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1qFutureVlanPort,
                                       tMacAddr Dot1qFutureVlanPortMacMapAddr,
                                       INT4
                                       i4TestValDot1qFutureVlanPortMacMapVid)
{
    UNUSED_PARAM (Dot1qFutureVlanPortMacMapAddr);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }
    if ((i4TestValDot1qFutureVlanPortMacMapVid <= 0) ||
        (i4TestValDot1qFutureVlanPortMacMapVid > VLAN_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetVlanEntry ((tVlanId) i4TestValDot1qFutureVlanPortMacMapVid)
        == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanBaseBridgeMode
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanBaseBridgeMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanBaseBridgeMode (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValDot1qFutureVlanBaseBridgeMode)
{
    if ((i4TestValDot1qFutureVlanBaseBridgeMode != DOT_1D_BRIDGE_MODE) &&
        (i4TestValDot1qFutureVlanBaseBridgeMode != DOT_1Q_VLAN_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_MODE_UNKNOWN);
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qFutureVlanBaseBridgeMode == DOT_1D_BRIDGE_MODE)
    {
        if (VlanMrpIsMrpStarted (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_MRP_ENABLED);
            return SNMP_FAILURE;
        }
#ifdef GARP_WANTED
        if (VlanGarpIsGarpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) ==
            GARP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_GARP_ENABLED);
            return SNMP_FAILURE;
        }
#endif
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
        if (SnoopIsSnoopStartedInContext (VLAN_CURR_CONTEXT_ID ()) !=
            SNOOP_SHUTDOWN)
        {

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_SNOOP_ENABLED);
            return SNMP_FAILURE;
        }
#endif
#ifdef PVRST_WANTED
        if ((VlanAstIsPvrstStartedInContext (VLAN_CURR_CONTEXT_ID ())) == TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_PVRST_ENABLED);
            return SNMP_FAILURE;
        }
#endif
#ifdef MSTP_WANTED
        if ((VlanAstIsMstStartedInContext (VLAN_CURR_CONTEXT_ID ())) == TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_MST_ENABLED);
            return SNMP_FAILURE;
        }
#endif
#ifdef PNAC_WANTED
        if (PnacGetStartedStatus () == PNAC_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_PNAC_ENABLED);
            return SNMP_FAILURE;
        }
#endif
#ifdef LA_WANTED
        if ((LaIsLaStarted ()) == LA_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_LA_ENABLED);
            return SNMP_FAILURE;
        }
#endif
#ifdef LLDP_WANTED
        if ((LldpIsLldpStarted ()) == LLDP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_LLDP_ENABLED);
            return SNMP_FAILURE;
        }
#endif
        /* Check whether all non physical interfaces are deleted. */
        if (CfaCheckNonPhysicalInterfaces () == CFA_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_INTF_PRESENT);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts
 Input       :  The Indices

                The Object
                testValDot1qFutureVlanSubnetBasedOnAllPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4TestValDot1qFutureVlanSubnetBasedOnAllPorts)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanSubnetBasedOnAllPorts != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanSubnetBasedOnAllPorts != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to enable Subnet based vlans in provider bridge mode */
    if (i4TestValDot1qFutureVlanSubnetBasedOnAllPorts == VLAN_SNMP_TRUE)
    {
        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) ||
            (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE))
        {
            CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_CFY_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortMacMapName
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                testValDot1qFutureVlanPortMacMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortMacMapName (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1qFutureVlanPort,
                                        tMacAddr Dot1qFutureVlanPortMacMapAddr,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValDot1qFutureVlanPortMacMapName)
{
    UNUSED_PARAM (Dot1qFutureVlanPortMacMapAddr);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((pTestValDot1qFutureVlanPortMacMapName->i4_Length <= 0) ||
        (pTestValDot1qFutureVlanPortMacMapName->i4_Length >
         VLAN_STATIC_MAX_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;

    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars
        (pTestValDot1qFutureVlanPortMacMapName->pu1_OctetList,
         pTestValDot1qFutureVlanPortMacMapName->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object
                testValDot1qFutureVlanPortMacMapMcastBcastOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption (UINT4 *pu4ErrorCode,
                                                    INT4 i4Dot1qFutureVlanPort,
                                                    tMacAddr
                                                    Dot1qFutureVlanPortMacMapAddr,
                                                    INT4
                                                    i4TestValDot1qFutureVlanPortMacMapMcastBcastOption)
{
    UNUSED_PARAM (Dot1qFutureVlanPortMacMapAddr);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortMacMapMcastBcastOption >= VLAN_MCAST_USE)
        && (i4TestValDot1qFutureVlanPortMacMapMcastBcastOption <=
            VLAN_MCAST_DROP))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortMacMapRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr

                The Object 
                testValDot1qFutureVlanPortMacMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortMacMapRowStatus (UINT4 *pu4ErrorCode,
                                             INT4 i4Dot1qFutureVlanPort,
                                             tMacAddr
                                             Dot1qFutureVlanPortMacMapAddr,
                                             INT4
                                             i4TestValDot1qFutureVlanPortMacMapRowStatus)
{
    tVlanMacMapEntry   *pMacMapEntry = NULL;
    UINT4               u4IfIndex = 0;

    /* Cannot create  source address with multicast address */
    if (VLAN_IS_MCASTADDR (Dot1qFutureVlanPortMacMapAddr) ||
        VLAN_IS_BCASTADDR (Dot1qFutureVlanPortMacMapAddr))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortMacMapRowStatus < VLAN_ACTIVE) ||
        (i4TestValDot1qFutureVlanPortMacMapRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort))
        == VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
        return SNMP_FAILURE;
    }

    pMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                       i4Dot1qFutureVlanPort);

    switch (i4TestValDot1qFutureVlanPortMacMapRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            if (pMacMapEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (gu2VlanMacMapCount >= VLAN_MAX_MAC_MAP_ENTRIES)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VLAN_NOT_IN_SERVICE:

            if (pMacMapEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (pMacMapEntry->u1RowStatus == VLAN_NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VLAN_DESTROY:
            /* Fall through */

        case VLAN_ACTIVE:

            if (pMacMapEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if ((i4TestValDot1qFutureVlanPortMacMapRowStatus == VLAN_ACTIVE) &&
                (pMacMapEntry->u1RowStatus == VLAN_NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_UNKNOWN_ERR);
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanPortMacMapTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanBaseBridgeMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanBaseBridgeMode (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanSubnetBasedOnAllPorts                        Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanSubnetBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanStatus
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1qFutureVlanStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Dot1qFutureVlanStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValDot1qFutureVlanStatus)
{
    UINT1               u1VlanLearningType;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        /* 
         * VLAN module is shutdown.
         * VLAN module can be enabled after the shut down status is changed to
         * false 
         */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u1VlanLearningType = VlanGetVlanLearningMode ();

    switch (i4TestValDot1qFutureVlanStatus)
    {

        case VLAN_ENABLED:
            if (u1VlanLearningType == VLAN_INVALID_LEARNING)
            {

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VLAN_DISABLED:

            if ((VlanGvrpIsGvrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) ==
                 GVRP_TRUE)
                || (VlanGmrpIsGmrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) ==
                    GMRP_TRUE)
                || (SNOOP_ENABLED ==
                    VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                    || SNOOP_ENABLED ==
                    VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())))
            {

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanShutdownStatus
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanShutdownStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1qFutureVlanShutdownStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Dot1qFutureVlanShutdownStatus (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValDot1qFutureVlanShutdownStatus)
{
    UNUSED_PARAM (i4TestValDot1qFutureVlanShutdownStatus);

    /* shutting down vlan is not supported */
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanDebug
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanDebug (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDot1qFutureVlanDebug)
{
#ifdef TRACE_WANTED

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qFutureVlanDebug < VLAN_MIN_TRACE_VAL ||
        i4TestValDot1qFutureVlanDebug > VLAN_MAX_TRACE_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValDot1qFutureVlanDebug);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanLearningMode
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanLearningMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanLearningMode (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValDot1qFutureVlanLearningMode)
{
    UINT2               u2MstInst;
    UINT2               u2CommonInst;
    UINT2               u2VlanId;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qFutureVlanLearningMode == VLAN_SHARED_LEARNING)
    {

        for (u2VlanId = 1; u2VlanId <= VLAN_MAX_VLAN_ID; u2VlanId++)
        {
            u2MstInst = VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                                       u2VlanId);
            if (u2MstInst != MST_CIST_CONTEXT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_INCONSISTENT_INST_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    if (i4TestValDot1qFutureVlanLearningMode == VLAN_HYBRID_LEARNING)
    {
        if (gpVlanContextInfo->VlanInfo.u1DefConstType == VLAN_SHARED_LEARNING)
        {
            u2CommonInst =
                VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                               VLAN_DEF_VLAN_ID);

            for (u2VlanId = 1; u2VlanId <= VLAN_MAX_VLAN_ID; u2VlanId++)
            {
                u2MstInst =
                    VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                                   u2VlanId);
                if (u2MstInst != u2CommonInst)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_VLAN_INCONSISTENT_INSTMAP_ERR);
                    return SNMP_FAILURE;
                }
            }
        }
    }

    if ((i4TestValDot1qFutureVlanLearningMode == VLAN_INDEP_LEARNING) ||
        (i4TestValDot1qFutureVlanLearningMode == VLAN_SHARED_LEARNING) ||
        (i4TestValDot1qFutureVlanLearningMode == VLAN_HYBRID_LEARNING))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanHybridTypeDefault
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanHybridTypeDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanHybridTypeDefault (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValDot1qFutureVlanHybridTypeDefault)
{
    UINT1               u1VlanLearningType;
    UINT2               u2VlanId;
    UINT2               u2MstInst = 0;
    UINT2               u2CommonInst = 0;

    u1VlanLearningType = VlanGetVlanLearningMode ();

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (u1VlanLearningType == VLAN_HYBRID_LEARNING)

    {
        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }

        if ((i4TestValDot1qFutureVlanHybridTypeDefault != VLAN_SHARED_LEARNING)
            &&
            (i4TestValDot1qFutureVlanHybridTypeDefault != VLAN_INDEP_LEARNING))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if (i4TestValDot1qFutureVlanHybridTypeDefault == VLAN_SHARED_LEARNING)
        {
            u2CommonInst =
                VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                               VLAN_DEF_VLAN_ID);

            for (u2VlanId = 1; u2VlanId <= VLAN_MAX_VLAN_ID; u2VlanId++)
            {

                u2MstInst =
                    VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                                   u2VlanId);
                if (u2MstInst != u2CommonInst)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_VLAN_INCONSISTENT_INSTMAP_ERR);
                    return SNMP_FAILURE;

                }
            }
        }

        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    CLI_SET_ERR (CLI_VLAN_INVALID_VLANMODE_ERR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus
 Input       :  The Indices

                The Object
                testValDot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4TestValDot1qFutureVlanGlobalMacLearningStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTstGlobalMacLearnStatus
        (pu4ErrorCode,
         i4TestValDot1qFutureVlanGlobalMacLearningStatus) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4TestValDot1qFutureVlanApplyEnhancedFilteringCriteria)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanApplyEnhancedFilteringCriteria !=
         VLAN_SNMP_TRUE)
        && (i4TestValDot1qFutureVlanApplyEnhancedFilteringCriteria !=
            VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanSwStatsEnabled
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanSwStatsEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanSwStatsEnabled (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValDot1qFutureVlanSwStatsEnabled)
{
    if ((i4TestValDot1qFutureVlanSwStatsEnabled != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanSwStatsEnabled != VLAN_SNMP_FALSE))
    {
        /* No need to cli set err, as this case will not be hit in 
         * the cli calling sequence */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (i4TestValDot1qFutureVlanSwStatsEnabled == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_VLAN_SW_STATS_CONF_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanMacBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanPortProtoBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanShutdownStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanShutdownStatus (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanDebug (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanLearningMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanLearningMode (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanHybridTypeDefault
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanHybridTypeDefault (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanGlobalMacLearningStatus (UINT4 *pu4ErrorCode,
                                                tSnmpIndexList * pSnmpIndexList,
                                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanApplyEnhancedFilteringCriteria (UINT4 *pu4ErrorCode,
                                                       tSnmpIndexList *
                                                       pSnmpIndexList,
                                                       tSNMP_VAR_BIND *
                                                       pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanSwStatsEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanSwStatsEnabled (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValDot1qFutureVlanMacBasedOnAllPorts)
{

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanMacBasedOnAllPorts != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanMacBasedOnAllPorts != VLAN_SNMP_FALSE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to enable mac based vlans in provider bridge mode */
    if (i4TestValDot1qFutureVlanMacBasedOnAllPorts == VLAN_SNMP_TRUE)
    {
        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) ||
            (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE))
        {
            CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_CFY_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4TestValDot1qFutureVlanPortProtoBasedOnAllPorts)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)

    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortProtoBasedOnAllPorts
         != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanPortProtoBasedOnAllPorts != VLAN_SNMP_FALSE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to enable protocol vlans in provider bridge mode */
    if (i4TestValDot1qFutureVlanPortProtoBasedOnAllPorts == VLAN_SNMP_TRUE)
    {
        if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_CFY_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortMacBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanPortMacBasedClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortMacBasedClassification (UINT4 *pu4ErrorCode,
                                                    INT4 i4Dot1qFutureVlanPort,
                                                    INT4
                                                    i4TestValDot1qFutureVlanPortMacBasedClassification)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortMacBasedClassification
         != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanPortMacBasedClassification != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to enable mac based vlans in provider bridge mode */
    if (i4TestValDot1qFutureVlanPortMacBasedClassification == VLAN_SNMP_TRUE)
    {
        if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_CFY_ERR);
            return SNMP_FAILURE;
        }
        if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanPortPortProtoBasedClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification (UINT4 *pu4ErrorCode,
                                                          INT4
                                                          i4Dot1qFutureVlanPort,
                                                          INT4
                                                          i4TestValDot1qFutureVlanPortPortProtoBasedClassification)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortPortProtoBasedClassification
         != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanPortPortProtoBasedClassification
         != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to enable protocol based vlans in provider bridge mode */
    if (i4TestValDot1qFutureVlanPortPortProtoBasedClassification ==
        VLAN_SNMP_TRUE)
    {
        if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_CFY_ERR);
            return SNMP_FAILURE;
        }
        if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1vProtocolGroupId
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue

                The Object 
                testValDot1vProtocolGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1vProtocolGroupId (UINT4 *pu4ErrorCode,
                               INT4 i4Dot1vProtocolTemplateFrameType,
                               tSNMP_OCTET_STRING_TYPE *
                               pDot1vProtocolTemplateProtocolValue,
                               INT4 i4TestValDot1vProtocolGroupId)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];
    UINT1               u1FrameType;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u1FrameType = (UINT1) i4Dot1vProtocolTemplateFrameType;

    if ((u1FrameType != VLAN_PORT_PROTO_ETHERTYPE) &&
        (u1FrameType != VLAN_PORT_PROTO_RFC1042) &&
        (u1FrameType != VLAN_PORT_PROTO_SNAP8021H) &&
        (u1FrameType != VLAN_PORT_PROTO_SNAPOTHER) &&
        (u1FrameType != VLAN_PORT_PROTO_LLCOTHER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_2BYTE_PROTO_TEMP_SIZE) &&
        (pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_5BYTE_PROTO_TEMP_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_PORT_PROTO_SNAPOTHER == (UINT1) i4Dot1vProtocolTemplateFrameType)
    {
        /*  Check whether the PID value for SNAP_OTHER type 
         *  is 5 byte
         */
        if (VLAN_5BYTE_PROTO_TEMP_SIZE
            != (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /*  Check whether the PID value for other type 
         *  is 2 byte
         */
        if (VLAN_2BYTE_PROTO_TEMP_SIZE
            != (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValDot1vProtocolGroupId < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Checking for dependencies here */
    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);

    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;
    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry == NULL)
    {
        /*
         * No Group entry is present in Protocol Group Database 
         */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_MAP_PROTO_GRP_ERR);
        return SNMP_FAILURE;
    }

    /* 
     * Set the Group ID iff the row status is not active since this
     * table has lots of dependencies with Protocol VID set table
     * of the port.
     */
    if (pVlanProtGrpEntry->u1RowStatus == VLAN_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1vProtocolGroupRowStatus
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue

                The Object 
                testValDot1vProtocolGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1vProtocolGroupRowStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1vProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pDot1vProtocolTemplateProtocolValue,
                                      INT4 i4TestValDot1vProtocolGroupRowStatus)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    UINT4               u4BridgeMode = 0;

    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];
    UINT1               u1FrameType;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanL2IwfGetBridgeMode (VLAN_CURR_CONTEXT_ID (),
                                &u4BridgeMode) == VLAN_SUCCESS)
    {

        if (u4BridgeMode != VLAN_CUSTOMER_BRIDGE_MODE)
        {

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_PROTO_ERR);
            return SNMP_FAILURE;
        }
    }
    u1FrameType = (UINT1) i4Dot1vProtocolTemplateFrameType;
    if ((u1FrameType != VLAN_PORT_PROTO_ETHERTYPE) &&
        (u1FrameType != VLAN_PORT_PROTO_RFC1042) &&
        (u1FrameType != VLAN_PORT_PROTO_SNAP8021H) &&
        (u1FrameType != VLAN_PORT_PROTO_SNAPOTHER) &&
        (u1FrameType != VLAN_PORT_PROTO_LLCOTHER))
    {
        CLI_SET_ERR (CLI_VLAN_MAP_PROTO_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_2BYTE_PROTO_TEMP_SIZE) &&
        (pDot1vProtocolTemplateProtocolValue->i4_Length
         != VLAN_5BYTE_PROTO_TEMP_SIZE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_PORT_PROTO_SNAPOTHER == (UINT1) i4Dot1vProtocolTemplateFrameType)
    {

        /*  Check whether the PID value for SNAP_OTHER type 
         *  is 5 byte
         */
        if (VLAN_5BYTE_PROTO_TEMP_SIZE
            != (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length)
        {

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /*  Check whether the PID value for other type 
         *  is 2 byte
         */
        if (VLAN_2BYTE_PROTO_TEMP_SIZE
            != (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length)
        {

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValDot1vProtocolGroupRowStatus < VLAN_ACTIVE) ||
        (i4TestValDot1vProtocolGroupRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Dot1vProtocolTemplateFrameType == VLAN_PORT_PROTO_LLCOTHER)
        && (pDot1vProtocolTemplateProtocolValue->pu1_OctetList[0] == 0x81)
        && (pDot1vProtocolTemplateProtocolValue->pu1_OctetList[1] == 0x37))
    {
        CLI_SET_ERR (CLI_VLAN_LLC_HEADER_NOT_SUPPORTED);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Checking for dependencies */
    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);

    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;

    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry != NULL)
    {
        /* Entry Already present */

        if (pVlanProtGrpEntry->u1RowStatus ==
            (UINT1) i4TestValDot1vProtocolGroupRowStatus)
        {
            return SNMP_SUCCESS;
        }

        /* Return failure if entry is create and 
         * wait */

        if (i4TestValDot1vProtocolGroupRowStatus == VLAN_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_UNKNOWN_ERR);
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Port Protocol entry is not present */

        if (i4TestValDot1vProtocolGroupRowStatus != VLAN_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_VLAN_PORT_PROTOCOL_NOT_PRESENT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValDot1vProtocolGroupRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
            /* Fall through */
        case VLAN_NOT_IN_SERVICE:
            /* Fall through */
        case VLAN_DESTROY:
            break;

        case VLAN_ACTIVE:
            if (pVlanProtGrpEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_VLAN_UNKNOWN_ERR);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1vProtocolGroupTable
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1vProtocolGroupTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1vProtocolPortGroupVid
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId

                The Object 
                testValDot1vProtocolPortGroupVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1vProtocolPortGroupVid (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                    INT4 i4Dot1vProtocolPortGroupId,
                                    INT4 i4TestValDot1vProtocolPortGroupVid)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port = 0;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;
    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4Dot1vProtocolPortGroupId < 0)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (i4TestValDot1vProtocolPortGroupVid)
        == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_VLANID_ERR);
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    /* Checking for dependencies */
    u2Port = (UINT2) i4Dot1dBasePort;

    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2Port,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry == NULL)
    {
        /* Entry not yet created */
        CLI_SET_ERR (CLI_VLAN_MAP_PROTO_PORT_GRP_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (u2Port)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1vProtocolPortRowStatus
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId

                The Object 
                testValDot1vProtocolPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1vProtocolPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                     INT4 i4Dot1vProtocolPortGroupId,
                                     INT4 i4TestValDot1vProtocolPortRowStatus)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4BrgPortType = 0;
    UINT2               u2Port = 0;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        /* VLAN is shutdown */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }
    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (), i4Dot1dBasePort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((VlanCfaGetInterfaceBrgPortType (u4IfIndex,
                                         &i4BrgPortType) == VLAN_SUCCESS) &&
        ((i4BrgPortType == CFA_UPLINK_ACCESS_PORT) ||
         (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)))
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    if (i4Dot1vProtocolPortGroupId < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_SISP_LOGICAL_PORT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1vProtocolPortRowStatus < VLAN_ACTIVE) ||
        (i4TestValDot1vProtocolPortRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check for other dependencies */

    /* To check whether the active protocol group entry exists for the
     * group ID
     */
    pVlanProtGrpEntry =
        VlanGetActiveProtoGrpEntry ((UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanProtGrpEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_MAP_PROTO_GRP_ERR);
        return SNMP_FAILURE;
    }

    /* Check for Row status transitions */
    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2Port,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry != NULL)
    {
        /* Entry Already present */

        if (pVlanPortVidSetEntry->u1RowStatus ==
            (UINT1) i4TestValDot1vProtocolPortRowStatus)
        {
            return SNMP_SUCCESS;
        }

        /* Return failure if entry is create and 
         * wait */

        if (i4TestValDot1vProtocolPortRowStatus == VLAN_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_UNKNOWN_ERR);
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Port Protocol entry is not present */
        if (i4TestValDot1vProtocolPortRowStatus != VLAN_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValDot1vProtocolPortRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
            /* Fall through */
        case VLAN_NOT_IN_SERVICE:
            /* Fall through */
        case VLAN_DESTROY:
            break;

        case VLAN_ACTIVE:
            if (pVlanPortVidSetEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_UNKNOWN_ERR);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1vProtocolPortTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1vProtocolPortTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanBridgeMode
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanBridgeMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanBridgeMode (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValDot1qFutureVlanBridgeMode)
{
    UNUSED_PARAM (i4TestValDot1qFutureVlanBridgeMode);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanTunnelBpduPri
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanTunnelBpduPri
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanTunnelBpduPri (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValDot1qFutureVlanTunnelBpduPri)
{
    UNUSED_PARAM (i4TestValDot1qFutureVlanTunnelBpduPri);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanBridgeMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanBridgeMode (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanTunnelBpduPri
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanTunnelBpduPri (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanTunnelStatus
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanTunnelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanTunnelStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1qFutureVlanPort,
                                      INT4 i4TestValDot1qFutureVlanTunnelStatus)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (i4TestValDot1qFutureVlanTunnelStatus);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanTunnelTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanTunnelTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanTunnelStpPDUs
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanTunnelStpPDUs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanTunnelStpPDUs (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1qFutureVlanPort,
                                       INT4
                                       i4TestValDot1qFutureVlanTunnelStpPDUs)
{
    UNUSED_PARAM (i4TestValDot1qFutureVlanTunnelStpPDUs);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanTunnelGvrpPDUs
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanTunnelGvrpPDUs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanTunnelGvrpPDUs (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1qFutureVlanPort,
                                        INT4
                                        i4TestValDot1qFutureVlanTunnelGvrpPDUs)
{
    UNUSED_PARAM (i4TestValDot1qFutureVlanTunnelGvrpPDUs);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanTunnelIgmpPkts
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanTunnelIgmpPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanTunnelIgmpPkts (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1qFutureVlanPort,
                                        INT4
                                        i4TestValDot1qFutureVlanTunnelIgmpPkts)
{

    UNUSED_PARAM (i4TestValDot1qFutureVlanTunnelIgmpPkts);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanCounterStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                testValDot1qFutureVlanCounterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanCounterStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4Dot1qFutureVlanIndex,
                                       INT4
                                       i4TestValDot1qFutureVlanCounterStatus)
{
    if (VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanCounterStatus != VLAN_ENABLED) &&
        (i4TestValDot1qFutureVlanCounterStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanCounterTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanCounterTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanFid
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object 
                testValDot1qFutureVlanFid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanFid (UINT4 *pu4ErrorCode,
                             UINT4 u4Dot1qFutureVlanIndex,
                             UINT4 u4TestValDot1qFutureVlanFid)
{
    UINT4               u4VlanIndex;
    UINT2               u2MstInst;
    UINT2               u2TempInst;
    UINT4               u4FdbId;
    UINT4               u4VlanFdbId;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_VLANID_ERR);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_FDB_ID_VALID (u4TestValDot1qFutureVlanFid) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_FDBID_ERR);
        return SNMP_FAILURE;
    }

    if (VlanGetVlanLearningMode () != VLAN_HYBRID_LEARNING)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4VlanFdbId = VlanL2IwfMiGetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                                           (tVlanId) u4Dot1qFutureVlanIndex);

    if (u4VlanFdbId == u4TestValDot1qFutureVlanFid)
    {
        return SNMP_SUCCESS;
    }

    u2MstInst = VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                               (tVlanId)
                                               u4Dot1qFutureVlanIndex);

    for (u4VlanIndex = 1; u4VlanIndex <= VLAN_MAX_VLAN_ID; u4VlanIndex++)
    {
        if (u4VlanIndex != u4Dot1qFutureVlanIndex)
        {
            u4FdbId = VlanL2IwfMiGetVlanFdbId (VLAN_CURR_CONTEXT_ID (),
                                               (tVlanId) u4VlanIndex);

            u2TempInst = VlanL2IwfMiGetVlanInstMapping (VLAN_CURR_CONTEXT_ID (),
                                                        (tVlanId) u4VlanIndex);

            if ((u4FdbId == u4TestValDot1qFutureVlanFid) &&
                (u2TempInst != u2MstInst))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_INCONSISTENT_MSTFID_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanFidMapTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanFidMapTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanUnicastMacLimit
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                testValDot1qFutureVlanUnicastMacLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanUnicastMacLimit (UINT4 *pu4ErrorCode,
                                         UINT4 u4Dot1qFutureVlanIndex,
                                         UINT4
                                         u4TestValDot1qFutureVlanUnicastMacLimit)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanGetVlanLearningMode () != VLAN_INDEP_LEARNING)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_MAC_LEARNING_MODE_ERR);
        return SNMP_FAILURE;
    }

    if (VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    if (u4TestValDot1qFutureVlanUnicastMacLimit > VLAN_DYNAMIC_UNICAST_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_MAC_LIMIT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanAdminMacLearningStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                testValDot1qFutureVlanAdminMacLearningStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanAdminMacLearningStatus (UINT4 *pu4ErrorCode,
                                                UINT4 u4Dot1qFutureVlanIndex,
                                                INT4
                                                i4TestValDot1qFutureVlanAdminMacLearningStatus)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTstVlanAdminMacLearnStatus
        (pu4ErrorCode,
         u4Dot1qFutureVlanIndex,
         i4TestValDot1qFutureVlanAdminMacLearningStatus) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortFdbFlush
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object 
                testValDot1qFutureVlanPortFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortFdbFlush (UINT4 *pu4ErrorCode,
                                      UINT4 u4Dot1qFutureVlanIndex,
                                      INT4 i4TestValDot1qFutureVlanPortFdbFlush)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;

    if ((i4TestValDot1qFutureVlanPortFdbFlush != VLAN_ENABLED) &&
        (i4TestValDot1qFutureVlanPortFdbFlush != VLAN_DISABLED))
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex);
    if (pStVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanUnicastMacControlTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object 
                testValDot1qFutureVlanFilteringUtilityCriteria
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     INT4 i4TestFilteringUtilityCriteria)
{
    UINT4               u4IfIndex = 0;
    if (i4Dot1qFutureVlanPort > VLAN_MAX_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestFilteringUtilityCriteria != VLAN_DEFAULT_FILTERING_CRITERIA) &&
        (i4TestFilteringUtilityCriteria != VLAN_ENHANCE_FILTERING_CRITERIA))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_WRONG_FILTERING_UTILITY_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanWildCardEgressPorts
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress

                The Object 
                testValDot1qFutureVlanWildCardEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanWildCardEgressPorts (UINT4 *pu4ErrorCode,
                                             tMacAddr WildCardMacAddress,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestWildCardEgressPorts)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    UINT1               bu1RetVal = VLAN_FALSE;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((pTestWildCardEgressPorts->i4_Length <= 0) ||
        (pTestWildCardEgressPorts->i4_Length > VLAN_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    VLAN_IS_EXCEED_MAX_PORTS (pTestWildCardEgressPorts->pu1_OctetList,
                              pTestWildCardEgressPorts->i4_Length, bu1RetVal);

    if (bu1RetVal == VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_MAX_PORTLIST_ERR);
        return SNMP_FAILURE;

    }

    if (VlanIsPortListValid (pTestWildCardEgressPorts->pu1_OctetList,
                             pTestWildCardEgressPorts->i4_Length) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORTLIST_ERR);
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_WILDCARD_ENTRY_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanWildCardRowStatus
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress

                The Object 
                testValDot1qFutureVlanWildCardRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanWildCardRowStatus (UINT4 *pu4ErrorCode,
                                           tMacAddr WildCardMacAddress,
                                           INT4 i4TestWildCardRowStatus)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanIsValidUcastAddr (WildCardMacAddress) == VLAN_FAILURE)
    {
        if (VlanIsValidMcastAddr (WildCardMacAddress) == VLAN_FAILURE)
        {
            if (VLAN_IS_BCASTADDR (WildCardMacAddress) == VLAN_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_VLAN_MAC_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    if ((i4TestWildCardRowStatus < VLAN_ACTIVE) ||
        (i4TestWildCardRowStatus > VLAN_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (i4TestWildCardRowStatus == VLAN_DESTROY && pWildCardEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_WILDCARD_ENTRY_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanWildCardTable
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanWildCardTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qFutureVlanTpFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanTpFdbTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1qFutureVlanTpFdbTable (UINT4 u4Dot1qFdbId,
                                                   tMacAddr Dot1qTpFdbAddress)
{
    return (nmhValidateIndexInstanceDot1qTpFdbTable (u4Dot1qFdbId,
                                                     Dot1qTpFdbAddress));
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortProtected
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortProtected
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortProtected (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1qFutureVlanPort,
                                       INT4
                                       i4TestValDot1qFutureVlanPortProtected)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortProtected != VLAN_SNMP_TRUE) &&
        (i4TestValDot1qFutureVlanPortProtected != VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortSubnetBasedClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification (UINT4 *pu4ErrorCode,
                                                       INT4
                                                       i4Dot1qFutureVlanPort,
                                                       INT4
                                                       i4TestValDot1qFutureVlanPortSubnetBasedClassification)
{
    UINT2               u2Port = VLAN_INIT_VAL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1qFutureVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    i4Dot1qFutureVlanPort,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortSubnetBasedClassification !=
         VLAN_SNMP_TRUE)
        && (i4TestValDot1qFutureVlanPortSubnetBasedClassification !=
            VLAN_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Dont allow to enable Subnet based vlans in provider bridge mode */
    if (i4TestValDot1qFutureVlanPortSubnetBasedClassification == VLAN_SNMP_TRUE)
    {
        if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_TUNNEL_PORT_CFY_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortUnicastMacLearning
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortUnicastMacLearning
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)                                                                        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortUnicastMacLearning (UINT4 *pu4ErrorCode,
                                                INT4 i4Dot1qFutureVlanPort,
                                                INT4
                                                i4TestValDot1qFutureVlanPortUnicastMacLearning)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IcclIfIndex = 0;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortUnicastMacLearning != VLAN_ENABLED) &&
        (i4TestValDot1qFutureVlanPortUnicastMacLearning != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qFutureVlanPortUnicastMacLearning == VLAN_ENABLED)
    {
        u4IfIndex = VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort);

        VlanIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex == u4IfIndex)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_VLAN_ICCL_MAC_LEARNING_DISABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortIngressEtherType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortIngressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                                        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortIngressEtherType (UINT4 *pu4ErrorCode,
                                              INT4 i4Dot1qFutureVlanPort,
                                              INT4
                                              i4TestValDot1qFutureVlanPortIngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
#ifdef PB_WANTED
    /* Ethertype configurations are not allowed on SISP ports */
    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort)) ==
        VCM_TRUE)
    {
        CLI_SET_ERR (CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if (VLAN_IS_ETHER_TYPE_VALID (i4TestValDot1qFutureVlanPortIngressEtherType)
        != VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortEgressEtherType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortEgressEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortEgressEtherType (UINT4 *pu4ErrorCode,
                                             INT4 i4Dot1qFutureVlanPort,
                                             INT4
                                             i4TestValDot1qFutureVlanPortEgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
#ifdef PB_WANTED
    /* Ethertype configurations are not allowed on SISP ports */
    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (i4Dot1qFutureVlanPort)) ==
        VCM_TRUE)
    {
        CLI_SET_ERR (CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if (VLAN_IS_ETHER_TYPE_VALID (i4TestValDot1qFutureVlanPortEgressEtherType)
        != VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortEgressTPIDType
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortEgressTPIDType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortEgressTPIDType (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1qFutureVlanPort,
                                            INT4
                                            i4TestValDot1qFutureVlanPortEgressTPIDType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4BrgPortType = 0;
    UINT4               u4IfIndex = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    (UINT2) i4Dot1qFutureVlanPort,
                                    &u4IfIndex) == VCM_SUCCESS)
    {
        if ((VlanCfaGetInterfaceBrgPortType (u4IfIndex,
                                             &i4BrgPortType) == VLAN_SUCCESS) &&
            ((i4BrgPortType == CFA_UPLINK_ACCESS_PORT) ||
             (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)))
        {
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_EGRESS_TPID_TYPE_VALID
        (i4TestValDot1qFutureVlanPortEgressTPIDType) != VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
*  Function    :  nmhTestv2Dot1qFutureVlanPortUnicastMacSecType
*  Input       :  The Indices
*                   Dot1qFutureVlanPort
*                    The Object
*                  testValDot1qFutureVlanPortUnicastMacSecType
*  Output      :  The Test Low Lev Routine Take the Indices &
*                 Test whether that Value is Valid Input for Set.
*                 Stores the value of error code in the Return val
*                 Error Codes :  The following error codes are to be returned
*                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
*                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
*                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
*                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
*                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortUnicastMacSecType (UINT4 *pu4ErrorCode,
                                               INT4 i4Dot1qFutureVlanPort,
                                               INT4
                                               i4TestValDot1qFutureVlanPortUnicastMacSecType)
{

    tVlanPortEntry     *pVlanPortEntry = NULL;
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortUnicastMacSecType !=
         VLAN_CLI_UNICAST_MAC_SEC_SAV)
        && (i4TestValDot1qFutureVlanPortUnicastMacSecType !=
            VLAN_CLI_UNICAST_MAC_SEC_SHV)
        && (i4TestValDot1qFutureVlanPortUnicastMacSecType !=
            VLAN_CLI_UNICAST_MAC_SEC_OFF))
    {
        CLI_SET_ERR (CLI_INVALID_UNICAST_MAC_SEC_TYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortAllowableTPID1
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortAllowableTPID1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortAllowableTPID1 (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1qFutureVlanPort,
                                            INT4
                                            i4TestValDot1qFutureVlanPortAllowableTPID1)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4BrgPortType = 0;
    INT4                i4CurBrgPortType = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4BridgeMode = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    (UINT2) i4Dot1qFutureVlanPort,
                                    &u4IfIndex) == VCM_SUCCESS)
    {
        if ((VlanCfaGetInterfaceBrgPortType (u4IfIndex,
                                             &i4CurBrgPortType) == VLAN_SUCCESS)
            && ((i4CurBrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
                || ((i4CurBrgPortType == CFA_UPLINK_ACCESS_PORT)
                    && (i4TestValDot1qFutureVlanPortAllowableTPID1 == 0))))

        {
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortAllowableTPID1 != VLAN_PROTOCOL_ID) &&
        (i4TestValDot1qFutureVlanPortAllowableTPID1 !=
         VLAN_PROVIDER_PROTOCOL_ID)
        && (i4TestValDot1qFutureVlanPortAllowableTPID1 != 0))
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /*If the Primary Ingress EhterType is same as TPID1 to be set, return failure  */
    if (i4TestValDot1qFutureVlanPortAllowableTPID1 ==
        (INT4) pVlanPortEntry->u2IngressEtherType)
    {
        CLI_SET_ERR (CLI_VLAN_CONFLICT_PRI_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
/* Check if HW supports TPID configuration on Customer Ports */
    if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_CP_TPID_ALLOW))
    {
        if (VlanCfaGetInterfaceBrgPortType (pVlanPortEntry->u4IfIndex,
                                            &i4BrgPortType) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        if (VlanL2IwfGetBridgeMode (VLAN_CURR_CONTEXT_ID (),
                                    &u4BridgeMode) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        if (((u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
             && (i4BrgPortType == VLAN_CUSTOMER_BRIDGE_PORT))
            || ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
                && ((i4BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
                    || (i4BrgPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))))
        {
            CLI_SET_ERR (CLI_VLAN_CONFIG_NOT_ALLOWED_1AD);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortAllowableTPID2
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortAllowableTPID2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortAllowableTPID2 (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1qFutureVlanPort,
                                            INT4
                                            i4TestValDot1qFutureVlanPortAllowableTPID2)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4BrgPortType = 0;
    INT4                i4CurBrgPortType = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4BridgeMode = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    (UINT2) i4Dot1qFutureVlanPort,
                                    &u4IfIndex) == VCM_SUCCESS)
    {
        if ((VlanCfaGetInterfaceBrgPortType (u4IfIndex,
                                             &i4CurBrgPortType) == VLAN_SUCCESS)
            && (i4CurBrgPortType == CFA_STATION_FACING_BRIDGE_PORT))

        {
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* ZERO is allowed in test routine for delete operation */
    if ((i4TestValDot1qFutureVlanPortAllowableTPID2 != VLAN_QINQ_PROTOCOL_ID) &&
        (i4TestValDot1qFutureVlanPortAllowableTPID2 != 0))
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
/* Check if HW supports TPID configuration on Customer Ports */
    if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_CP_TPID_ALLOW))
    {
        if (VlanCfaGetInterfaceBrgPortType (pVlanPortEntry->u4IfIndex,
                                            &i4BrgPortType) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (VlanL2IwfGetBridgeMode (VLAN_CURR_CONTEXT_ID (),
                                    &u4BridgeMode) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (((u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
             && (i4BrgPortType == VLAN_CUSTOMER_BRIDGE_PORT))
            || ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
                && ((i4BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
                    || (i4BrgPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))))
        {
            CLI_SET_ERR (CLI_VLAN_CONFIG_NOT_ALLOWED_1AD);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortAllowableTPID3
 Input       :  The Indices
                Dot1qFutureVlanPort

                The Object
                testValDot1qFutureVlanPortAllowableTPID3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanPortAllowableTPID3 (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1qFutureVlanPort,
                                            INT4
                                            i4TestValDot1qFutureVlanPortAllowableTPID3)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    INT4                i4BrgPortType = 0;
    INT4                i4CurBrgPortType = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4BridgeMode = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                    (UINT2) i4Dot1qFutureVlanPort,
                                    &u4IfIndex) == VCM_SUCCESS)
    {
        if ((VlanCfaGetInterfaceBrgPortType (u4IfIndex,
                                             &i4CurBrgPortType) == VLAN_SUCCESS)
            && (i4CurBrgPortType == CFA_STATION_FACING_BRIDGE_PORT))

        {
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_CURR_CONTEXT_PTR () == NULL)
    {
        return CLI_FAILURE;
    }

    /* ZERO is allowed in test routine for delete operation */
    if ((i4TestValDot1qFutureVlanPortAllowableTPID3 != 0) &&
        (i4TestValDot1qFutureVlanPortAllowableTPID3 !=
         (INT4) VLAN_PORT_USER_DEFINED_TPID))
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_USERDEFINED_TPID_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
/* Check if HW supports TPID configuration on Customer Ports */

    if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_CP_TPID_ALLOW))
    {
        if (VlanCfaGetInterfaceBrgPortType (pVlanPortEntry->u4IfIndex,
                                            &i4BrgPortType) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (VlanL2IwfGetBridgeMode (VLAN_CURR_CONTEXT_ID (),
                                    &u4BridgeMode) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (((u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
             && (i4BrgPortType == VLAN_CUSTOMER_BRIDGE_PORT))
            || ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
                && ((i4BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
                    || (i4BrgPortType == VLAN_PROP_CUSTOMER_EDGE_PORT))))
        {
            CLI_SET_ERR (CLI_VLAN_CONFIG_NOT_ALLOWED_1AD);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanPortTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanPortTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureUnicastMacLearningLimit
 Input       :  The Indices

                The Object 
                testValDot1qFutureUnicastMacLearningLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureUnicastMacLearningLimit (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4TestValDot1qFutureUnicastMacLearningLimit)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanId             ScanVlanId;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (u4TestValDot1qFutureUnicastMacLearningLimit >
        VLAN_DEF_UNICAST_LEARNING_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_SWITCH_MAC_LIMIT_ERR);
        return SNMP_FAILURE;
    }

    VLAN_SCAN_VLAN_TABLE (ScanVlanId)
    {
        pVlanEntry = VLAN_GET_CURR_ENTRY (ScanVlanId);
        if (pVlanEntry->pVlanMacControl != NULL)
        {
            if (u4TestValDot1qFutureUnicastMacLearningLimit <
                VLAN_CONTROL_MAC_LEARNING_LIMIT (ScanVlanId))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VLAN_MAC_LIMIT_MORE_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureUnicastMacLearningLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureUnicastMacLearningLimit (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureStaticConnectionIdentifier
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort

                The Object
                testValDot1qFutureStaticConnectionIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureStaticConnectionIdentifier
    (UINT4 *pu4ErrorCode,
     UINT4 u4Dot1qFdbId,
     tMacAddr Dot1qStaticUnicastAddress,
     INT4 i4Dot1qStaticUnicastReceivePort,
     tMacAddr TestValDot1qFutureStaticConnectionIdentifier)
{
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if (VlanTestConnectionIdentifier
        (pu4ErrorCode, u4Dot1qFdbId,
         Dot1qStaticUnicastAddress,
         i4Dot1qStaticUnicastReceivePort,
         TestValDot1qFutureStaticConnectionIdentifier) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureStaticUnicastExtnTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetMapVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                testValDot1qFutureVlanPortSubnetMapVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureVlanPortSubnetMapVid
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 i4TestValDot1qFutureVlanPortSubnetMapVid)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;
    UNUSED_PARAM (u4Dot1qFutureVlanPortSubnetMapAddr);

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid
            (pu4ErrorCode, i4Dot1qFutureVlanPort,
             u4Dot1qFutureVlanPortSubnetMapAddr, u4DefMask,
             i4TestValDot1qFutureVlanPortSubnetMapVid));
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                testValDot1qFutureVlanPortSubnetMapARPOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 i4TestValDot1qFutureVlanPortSubnetMapARPOption)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption
            (pu4ErrorCode, i4Dot1qFutureVlanPort,
             u4Dot1qFutureVlanPortSubnetMapAddr, u4DefMask,
             i4TestValDot1qFutureVlanPortSubnetMapARPOption));
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr

                The Object
                testValDot1qFutureVlanPortSubnetMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     INT4 i4TestValDot1qFutureVlanPortSubnetMapRowStatus)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    return (nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus
            (pu4ErrorCode, i4Dot1qFutureVlanPort,
             u4Dot1qFutureVlanPortSubnetMapAddr, u4DefMask,
             i4TestValDot1qFutureVlanPortSubnetMapRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanPortSubnetMapTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                testValDot1qFutureVlanPortSubnetMapExtVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4TestValDot1qFutureVlanPortSubnetMapExtVid)
{
    UNUSED_PARAM (u4Dot1qFutureVlanPortSubnetMapExtAddr);
    UNUSED_PARAM (u4Dot1qFutureVlanPortSubnetMapExtMask);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }
    if ((i4TestValDot1qFutureVlanPortSubnetMapExtVid <= 0) ||
        (i4TestValDot1qFutureVlanPortSubnetMapExtVid > VLAN_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetVlanEntry ((tVlanId) i4TestValDot1qFutureVlanPortSubnetMapExtVid)
        == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                testValDot1qFutureVlanPortSubnetMapExtARPOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4TestValDot1qFutureVlanPortSubnetMapExtARPOption)
{
    UNUSED_PARAM (u4Dot1qFutureVlanPortSubnetMapExtAddr);
    UNUSED_PARAM (u4Dot1qFutureVlanPortSubnetMapExtMask);
    UNUSED_PARAM (i4Dot1qFutureVlanPort);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortSubnetMapExtARPOption >= VLAN_ARP_ALLOW)
        && (i4TestValDot1qFutureVlanPortSubnetMapExtARPOption <=
            VLAN_ARP_SUPPRESS))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask

                The Object
                testValDot1qFutureVlanPortSubnetMapExtRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4Dot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4TestValDot1qFutureVlanPortSubnetMapExtRowStatus)
{
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;
    UINT4               u4IfIndex = 0;

    if (i4Dot1qFutureVlanPort > VLAN_MAX_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Cannot create  source address with multicast address */
    if (u4Dot1qFutureVlanPortSubnetMapExtAddr <= VLAN_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!gu4SubnetGlobalOption)
    {
        if (VcmGetIfIndexFromLocalPort (VLAN_CURR_CONTEXT_ID (),
                                        i4Dot1qFutureVlanPort,
                                        &u4IfIndex) != VCM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == VLAN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR);
            return SNMP_FAILURE;
        }

        if ((VLAN_GET_PORT_ENTRY (i4Dot1qFutureVlanPort)) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
            return SNMP_FAILURE;
        }

        if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanPortSubnetMapExtRowStatus < VLAN_ACTIVE) ||
        (i4TestValDot1qFutureVlanPortSubnetMapExtRowStatus > VLAN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               i4Dot1qFutureVlanPort,
                               u4Dot1qFutureVlanPortSubnetMapExtMask);

    switch (i4TestValDot1qFutureVlanPortSubnetMapExtRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            if (pSubnetMapEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (gu2VlanSubnetMapCount >= VLAN_MAX_SUBNET_MAP_ENTRIES)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VLAN_NOT_IN_SERVICE:

            if (pSubnetMapEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (pSubnetMapEntry->u1RowStatus == VLAN_NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VLAN_DESTROY:
            /* Fall through */

        case VLAN_ACTIVE:

            if (pSubnetMapEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if ((i4TestValDot1qFutureVlanPortSubnetMapExtRowStatus
                 == VLAN_ACTIVE)
                && (pSubnetMapEntry->u1RowStatus == VLAN_NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_VLAN_UNKNOWN_ERR);
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanPortSubnetMapExtTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureStVlanEgressEthertype
 Input       :  The Indices
                Dot1qVlanIndex

                The Object
                testValDot1qFutureStVlanEgressEthertype
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureStVlanEgressEthertype (UINT4 *pu4ErrorCode,
                                           UINT4 u4Dot1qVlanIndex,
                                           INT4
                                           i4TestValDot1qFutureStVlanEgressEthertype)
{
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Validate the vlan index. */
    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qVlanIndex) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_VLAN_INVALID_VLANID_ERR);
        return SNMP_FAILURE;
    }

    if (VLAN_IS_ETHER_TYPE_VALID (i4TestValDot1qFutureStVlanEgressEthertype) !=
        VLAN_TRUE)
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pVlanEntry = VlanGetVlanEntry (u4Dot1qVlanIndex);

    if (pVlanEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VLAN_ENTRY_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureStVlanExtTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureStVlanExtTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanGlobalsFdbFlush
 Input       :  The Indices

                The Object 
                testValDot1qFutureVlanGlobalsFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanGlobalsFdbFlush (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValDot1qFutureVlanGlobalsFdbFlush)
{
    if ((i4TestValDot1qFutureVlanGlobalsFdbFlush != VLAN_TRUE) &&
        (i4TestValDot1qFutureVlanGlobalsFdbFlush != VLAN_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanUserDefinedTPID
 Input       :  The Indices

                The Object
                testValDot1qFutureVlanUserDefinedTPID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)                                                                         SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanUserDefinedTPID (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValDot1qFutureVlanUserDefinedTPID)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* ZERO is considered valid for Deleting the configured the value */

    if ((i4TestValDot1qFutureVlanUserDefinedTPID == VLAN_PROTOCOL_ID) ||
        (i4TestValDot1qFutureVlanUserDefinedTPID == VLAN_PROVIDER_PROTOCOL_ID)
        || (i4TestValDot1qFutureVlanUserDefinedTPID == VLAN_QINQ_PROTOCOL_ID)
        || (i4TestValDot1qFutureVlanUserDefinedTPID < 0)
        || (i4TestValDot1qFutureVlanUserDefinedTPID > 65535))
    {
        CLI_SET_ERR (CLI_VLAN_INVALID_ETHERTYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanGlobalsFdbFlush
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanGlobalsFdbFlush (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanUserDefinedTPID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanUserDefinedTPID (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanLoopbackStatus
 Input       :  The Indices
                Dot1qFutureVlanIndex

                The Object
                testValDot1qFutureVlanLoopbackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanLoopbackStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4Dot1qFutureVlanIndex,
                                        INT4
                                        i4TestValDot1qFutureVlanLoopbackStatus)
{
    if (VlanGetVlanEntry ((tVlanId) u4Dot1qFutureVlanIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VLAN_NOT_PRESENT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureVlanLoopbackStatus != VLAN_ENABLED) &&
        (i4TestValDot1qFutureVlanLoopbackStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanLoopbackTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanLoopbackTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFuturePortPacketReflectionStatus
 Input       :  The Indices
                Dot1qFutureVlanPort
                The Object
                testValDot1qFuturePortPacketReflectionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returnedi
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2Dot1qFuturePortPacketReflectionStatus (UINT4 *pu4ErrorCode,
                                                INT4 i4Dot1qFutureVlanPort,
                                                INT4
                                                i4TestValDot1qFuturePortPacketReflectionStatus)
{

    /*Check whether the port is valid */
    if (VLAN_IS_PORT_VALID ((UINT2) i4Dot1qFutureVlanPort) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /*Status can be eitehr enabled or disabled */
    if ((i4TestValDot1qFuturePortPacketReflectionStatus != VLAN_ENABLED) &&
        (i4TestValDot1qFuturePortPacketReflectionStatus != VLAN_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureVlanRemoteFdbFlush
 Input       :  The Indices

                The Object
                testValDot1qFutureVlanRemoteFdbFlush
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureVlanRemoteFdbFlush (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValDot1qFutureVlanRemoteFdbFlush)
{
    if ((i4TestValDot1qFutureVlanRemoteFdbFlush != VLAN_TRUE) &&
        (i4TestValDot1qFutureVlanRemoteFdbFlush != VLAN_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureVlanRemoteFdbFlush
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureVlanRemoteFdbFlush (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
