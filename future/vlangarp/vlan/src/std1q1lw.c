/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1q1lw.c,v 1.10 2014/12/23 11:05:41 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"
# include  "fsmsbrlw.h"

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTable (UINT4
                                              u4Ieee8021QBridgeComponentId)
{

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Release context info */
    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTable (UINT4 *pu4Ieee8021QBridgeComponentId)
{

    if (VlanGetFirstActiveContext (pu4Ieee8021QBridgeComponentId) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeComponentId));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
                nextIeee8021QBridgeComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTable (UINT4 u4Ieee8021QBridgeComponentId,
                                     UINT4 *pu4NextIeee8021QBridgeComponentId)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeComponentId);
    if (VlanGetNextActiveContext (u4Ieee8021QBridgeComponentId,
                                  pu4NextIeee8021QBridgeComponentId)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeComponentId));
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanVersionNumber
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeVlanVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanVersionNumber (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        *pi4RetValIeee8021QBridgeVlanVersionNumber)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeComponentId);

    i1RetVal =
        nmhGetFsDot1qVlanVersionNumber ((INT4) u4Ieee8021QBridgeComponentId,
                                        pi4RetValIeee8021QBridgeVlanVersionNumber);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMaxVlanId
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMaxVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMaxVlanId (UINT4 u4Ieee8021QBridgeComponentId,
                                INT4 *pi4RetValIeee8021QBridgeMaxVlanId)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeComponentId);

    i1RetVal = nmhGetFsDot1qMaxVlanId ((INT4) u4Ieee8021QBridgeComponentId,
                                       pi4RetValIeee8021QBridgeMaxVlanId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMaxSupportedVlans
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMaxSupportedVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMaxSupportedVlans (UINT4 u4Ieee8021QBridgeComponentId,
                                        UINT4
                                        *pu4RetValIeee8021QBridgeMaxSupportedVlans)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeComponentId);

    i1RetVal =
        nmhGetFsDot1qMaxSupportedVlans ((INT4) u4Ieee8021QBridgeComponentId,
                                        pu4RetValIeee8021QBridgeMaxSupportedVlans);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeNumVlans
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeNumVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeNumVlans (UINT4 u4Ieee8021QBridgeComponentId,
                               UINT4 *pu4RetValIeee8021QBridgeNumVlans)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeComponentId);

    i1RetVal = nmhGetFsDot1qNumVlans ((INT4) u4Ieee8021QBridgeComponentId,
                                      pu4RetValIeee8021QBridgeNumVlans);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        *pi4RetValIeee8021QBridgeMvrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MvrpEnabledStatus = 0;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.u4ContextId =
        u4Ieee8021QBridgeComponentId;
    MrpConfigInfo.u4InfoType = MVRP_ENABLED_STATUS_GET;
    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pi4MvrpEnabledStatus =
        &i4MvrpEnabledStatus;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        *pi4RetValIeee8021QBridgeMvrpEnabledStatus =
            *(MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pi4MvrpEnabledStatus);
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                setValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        i4SetValIeee8021QBridgeMvrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MvrpEnabledStatus = 0;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.u4ContextId =
        u4Ieee8021QBridgeComponentId;
    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.i4MvrpEnabledStatus =
        i4SetValIeee8021QBridgeMvrpEnabledStatus;
    MrpConfigInfo.u4InfoType = MVRP_ENABLED_STATUS_SET;
    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pi4MvrpEnabledStatus =
        &i4MvrpEnabledStatus;
    *(MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pi4MvrpEnabledStatus) =
        i4SetValIeee8021QBridgeMvrpEnabledStatus;
    VLAN_UNLOCK ();
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
	    VLAN_LOCK ();
	    return SNMP_SUCCESS;
    }
    VLAN_LOCK ();
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                testValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Ieee8021QBridgeComponentId,
                                           INT4
                                           i4TestValIeee8021QBridgeMvrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MvrpEnabledStatus = 0;
    UINT4               u4ErrorCode = SNMP_FAILURE;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.u4ContextId =
        u4Ieee8021QBridgeComponentId;
    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.i4MvrpEnabledStatus =
        i4TestValIeee8021QBridgeMvrpEnabledStatus;
    MrpConfigInfo.u4InfoType = MVRP_ENABLED_STATUS_TEST;
    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pi4MvrpEnabledStatus =
        &i4MvrpEnabledStatus;
    *(MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pi4MvrpEnabledStatus) =
        i4TestValIeee8021QBridgeMvrpEnabledStatus;
    MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pu4ErrorCode = &u4ErrorCode;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = *(MrpConfigInfo.unMrpInfo.MvrpStatusInfo.pu4ErrorCode);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeCVlanPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable (UINT4
                                                       u4Ieee8021QBridgeCVlanPortComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeCVlanPortNumber)
{
    return (nmhValidateIndexInstanceIeee8021BridgeDot1dPortTable
            (u4Ieee8021QBridgeCVlanPortComponentId,
             u4Ieee8021QBridgeCVlanPortNumber));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeCVlanPortTable (UINT4
                                               *pu4Ieee8021QBridgeCVlanPortComponentId,
                                               UINT4
                                               *pu4Ieee8021QBridgeCVlanPortNumber)
{
    return (nmhGetFirstIndexIeee8021BridgeDot1dPortTable
            (pu4Ieee8021QBridgeCVlanPortComponentId,
             pu4Ieee8021QBridgeCVlanPortNumber));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                nextIeee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
                nextIeee8021QBridgeCVlanPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeCVlanPortTable (UINT4
                                              u4Ieee8021QBridgeCVlanPortComponentId,
                                              UINT4
                                              *pu4NextIeee8021QBridgeCVlanPortComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeCVlanPortNumber,
                                              UINT4
                                              *pu4NextIeee8021QBridgeCVlanPortNumber)
{
    return (nmhGetNextIndexIeee8021BridgeDot1dPortTable
            (u4Ieee8021QBridgeCVlanPortComponentId,
             pu4NextIeee8021QBridgeCVlanPortComponentId,
             u4Ieee8021QBridgeCVlanPortNumber,
             pu4NextIeee8021QBridgeCVlanPortNumber));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                retValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeCVlanPortRowStatus (UINT4
                                         u4Ieee8021QBridgeCVlanPortComponentId,
                                         UINT4 u4Ieee8021QBridgeCVlanPortNumber,
                                         INT4
                                         *pi4RetValIeee8021QBridgeCVlanPortRowStatus)
{
    return (nmhGetIeee8021BridgeDot1dPortRowStatus
            (u4Ieee8021QBridgeCVlanPortComponentId,
             u4Ieee8021QBridgeCVlanPortNumber,
             pi4RetValIeee8021QBridgeCVlanPortRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                setValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeCVlanPortRowStatus (UINT4
                                         u4Ieee8021QBridgeCVlanPortComponentId,
                                         UINT4 u4Ieee8021QBridgeCVlanPortNumber,
                                         INT4
                                         i4SetValIeee8021QBridgeCVlanPortRowStatus)
{
    return (nmhSetIeee8021BridgeDot1dPortRowStatus
            (u4Ieee8021QBridgeCVlanPortComponentId,
             u4Ieee8021QBridgeCVlanPortNumber,
             i4SetValIeee8021QBridgeCVlanPortRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                testValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeCVlanPortRowStatus (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021QBridgeCVlanPortComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeCVlanPortNumber,
                                            INT4
                                            i4TestValIeee8021QBridgeCVlanPortRowStatus)
{
    return (nmhTestv2Ieee8021BridgeDot1dPortRowStatus
            (pu4ErrorCode,
             u4Ieee8021QBridgeCVlanPortComponentId,
             u4Ieee8021QBridgeCVlanPortNumber,
             i4TestValIeee8021QBridgeCVlanPortRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeCVlanPortTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeFdbTable (UINT4
                                                 u4Ieee8021QBridgeFdbComponentId,
                                                 UINT4 u4Ieee8021QBridgeFdbId)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);

    return (nmhValidateIndexInstanceFsDot1qFdbTable
            (u4Ieee8021QBridgeFdbComponentId, u4Ieee8021QBridgeFdbId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeFdbTable (UINT4
                                         *pu4Ieee8021QBridgeFdbComponentId,
                                         UINT4 *pu4Ieee8021QBridgeFdbId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qFdbTable ((INT4 *)
                                                pu4Ieee8021QBridgeFdbComponentId,
                                                pu4Ieee8021QBridgeFdbId);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeFdbComponentId));

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                nextIeee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                nextIeee8021QBridgeFdbId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeFdbTable (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                        UINT4
                                        *pu4NextIeee8021QBridgeFdbComponentId,
                                        UINT4 u4Ieee8021QBridgeFdbId,
                                        UINT4 *pu4NextIeee8021QBridgeFdbId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    i1RetVal = nmhGetNextIndexFsDot1qFdbTable ((INT4)
                                               u4Ieee8021QBridgeFdbComponentId,
                                               (INT4 *)
                                               pu4NextIeee8021QBridgeFdbComponentId,
                                               u4Ieee8021QBridgeFdbId,
                                               pu4NextIeee8021QBridgeFdbId);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeFdbComponentId));

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbDynamicCount
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbDynamicCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbDynamicCount (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                      UINT4 u4Ieee8021QBridgeFdbId,
                                      UINT4
                                      *pu4RetValIeee8021QBridgeFdbDynamicCount)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    return (nmhGetFsDot1qFdbDynamicCount (u4Ieee8021QBridgeFdbComponentId,
                                          u4Ieee8021QBridgeFdbId,
                                          pu4RetValIeee8021QBridgeFdbDynamicCount));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbLearnedEntryDiscards
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbLearnedEntryDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbLearnedEntryDiscards (UINT4
                                              u4Ieee8021QBridgeFdbComponentId,
                                              UINT4 u4Ieee8021QBridgeFdbId,
                                              tSNMP_COUNTER64_TYPE *
                                              pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbAgingTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                   UINT4 u4Ieee8021QBridgeFdbId,
                                   INT4 *pi4RetValIeee8021QBridgeFdbAgingTime)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);

    return (nmhGetFsDot1dTpAgingTime ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                      pi4RetValIeee8021QBridgeFdbAgingTime));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                setValIeee8021QBridgeFdbAgingTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                   UINT4 u4Ieee8021QBridgeFdbId,
                                   INT4 i4SetValIeee8021QBridgeFdbAgingTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);

    i1RetVal =
        nmhSetFsDot1dTpAgingTime ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                  i4SetValIeee8021QBridgeFdbAgingTime);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                testValIeee8021QBridgeFdbAgingTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeFdbAgingTime (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021QBridgeFdbComponentId,
                                      UINT4 u4Ieee8021QBridgeFdbId,
                                      INT4 i4TestValIeee8021QBridgeFdbAgingTime)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    return (nmhTestv2FsDot1dTpAgingTime (pu4ErrorCode,
                                         u4Ieee8021QBridgeFdbComponentId,
                                         i4TestValIeee8021QBridgeFdbAgingTime));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeFdbTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTpFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable (UINT4
                                                   u4Ieee8021QBridgeFdbComponentId,
                                                   UINT4 u4Ieee8021QBridgeFdbId,
                                                   tMacAddr
                                                   Ieee8021QBridgeTpFdbAddress)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    return (nmhValidateIndexInstanceFsDot1qTpFdbTable ((INT4)
                                                       u4Ieee8021QBridgeFdbComponentId,
                                                       u4Ieee8021QBridgeFdbId,
                                                       Ieee8021QBridgeTpFdbAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTpFdbTable (UINT4
                                           *pu4Ieee8021QBridgeFdbComponentId,
                                           UINT4 *pu4Ieee8021QBridgeFdbId,
                                           tMacAddr *
                                           pIeee8021QBridgeTpFdbAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qTpFdbTable ((INT4 *)
                                                  pu4Ieee8021QBridgeFdbComponentId,
                                                  pu4Ieee8021QBridgeFdbId,
                                                  pIeee8021QBridgeTpFdbAddress);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeFdbComponentId));

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                nextIeee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                nextIeee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
                nextIeee8021QBridgeTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTpFdbTable (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                          UINT4
                                          *pu4NextIeee8021QBridgeFdbComponentId,
                                          UINT4 u4Ieee8021QBridgeFdbId,
                                          UINT4 *pu4NextIeee8021QBridgeFdbId,
                                          tMacAddr Ieee8021QBridgeTpFdbAddress,
                                          tMacAddr *
                                          pNextIeee8021QBridgeTpFdbAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    i1RetVal = nmhGetNextIndexFsDot1qTpFdbTable ((INT4)
                                                 u4Ieee8021QBridgeFdbComponentId,
                                                 (INT4 *)
                                                 pu4NextIeee8021QBridgeFdbComponentId,
                                                 u4Ieee8021QBridgeFdbId,
                                                 pu4NextIeee8021QBridgeFdbId,
                                                 Ieee8021QBridgeTpFdbAddress,
                                                 pNextIeee8021QBridgeTpFdbAddress);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeFdbComponentId));
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpFdbPort
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress

                The Object 
                retValIeee8021QBridgeTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpFdbPort (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                UINT4 u4Ieee8021QBridgeFdbId,
                                tMacAddr Ieee8021QBridgeTpFdbAddress,
                                UINT4 *pu4RetValIeee8021QBridgeTpFdbPort)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    return (nmhGetFsDot1qTpFdbPort ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                    u4Ieee8021QBridgeFdbId,
                                    Ieee8021QBridgeTpFdbAddress,
                                    (INT4 *)
                                    pu4RetValIeee8021QBridgeTpFdbPort));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpFdbStatus
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress

                The Object 
                retValIeee8021QBridgeTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpFdbStatus (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                  UINT4 u4Ieee8021QBridgeFdbId,
                                  tMacAddr Ieee8021QBridgeTpFdbAddress,
                                  INT4 *pi4RetValIeee8021QBridgeTpFdbStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeFdbComponentId);
    return (nmhGetFsDot1qTpFdbStatus ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                      u4Ieee8021QBridgeFdbId,
                                      Ieee8021QBridgeTpFdbAddress,
                                      pi4RetValIeee8021QBridgeTpFdbStatus));
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTpGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeTpGroupAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhValidateIndexInstanceDot1qTpGroupTable (u4Ieee8021QBridgeVlanIndex,
                                                   Ieee8021QBridgeTpGroupAddress);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTpGroupTable (UINT4
                                             *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                             UINT4 *pu4Ieee8021QBridgeVlanIndex,
                                             tMacAddr *
                                             pIeee8021QBridgeTpGroupAddress)
{
    UINT4               u4ComponentId = 0;

    if (VlanGetFirstActiveContext (&u4ComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021QBridgeVlanCurrentComponentId = u4ComponentId;
        if (nmhGetFirstIndexDot1qTpGroupTable (pu4Ieee8021QBridgeVlanIndex,
                                               pIeee8021QBridgeTpGroupAddress)
            == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID
                ((*pu4Ieee8021QBridgeVlanCurrentComponentId));
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021QBridgeVlanCurrentComponentId, &u4ComponentId)
           == VLAN_SUCCESS);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
                nextIeee8021QBridgeTpGroupAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTpGroupTable (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                            UINT4 u4Ieee8021QBridgeVlanIndex,
                                            UINT4
                                            *pu4NextIeee8021QBridgeVlanIndex,
                                            tMacAddr
                                            Ieee8021QBridgeTpGroupAddress,
                                            tMacAddr *
                                            pNextIeee8021QBridgeTpGroupAddress)
{
    UINT4               u4ComponentId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        == VLAN_SUCCESS)
    {
        if (nmhGetNextIndexDot1qTpGroupTable (u4Ieee8021QBridgeVlanIndex,
                                              pu4NextIeee8021QBridgeVlanIndex,
                                              Ieee8021QBridgeTpGroupAddress,
                                              pNextIeee8021QBridgeTpGroupAddress)
            == SNMP_SUCCESS)

        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID
                (u4Ieee8021QBridgeVlanCurrentComponentId);
            *pu4NextIeee8021QBridgeVlanCurrentComponentId =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext (u4Ieee8021QBridgeVlanCurrentComponentId,
                                      &u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        u4Ieee8021QBridgeVlanCurrentComponentId = u4ComponentId;
        *pu4NextIeee8021QBridgeVlanCurrentComponentId = u4ComponentId;

    }
    while (nmhGetFirstIndexDot1qTpGroupTable (pu4NextIeee8021QBridgeVlanIndex,
                                              pNextIeee8021QBridgeTpGroupAddress)
           != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpGroupEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress

                The Object 
                retValIeee8021QBridgeTpGroupEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpGroupEgressPorts (UINT4
                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                         UINT4 u4Ieee8021QBridgeVlanIndex,
                                         tMacAddr Ieee8021QBridgeTpGroupAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIeee8021QBridgeTpGroupEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1qTpGroupEgressPorts (u4Ieee8021QBridgeVlanIndex,
                                              Ieee8021QBridgeTpGroupAddress,
                                              pRetValIeee8021QBridgeTpGroupEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpGroupLearnt
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress

                The Object 
                retValIeee8021QBridgeTpGroupLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpGroupLearnt (UINT4
                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                    UINT4 u4Ieee8021QBridgeVlanIndex,
                                    tMacAddr Ieee8021QBridgeTpGroupAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValIeee8021QBridgeTpGroupLearnt)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1qTpGroupLearnt (u4Ieee8021QBridgeVlanIndex,
                                         Ieee8021QBridgeTpGroupAddress,
                                         pRetValIeee8021QBridgeTpGroupLearnt);
    VlanReleaseContext ();
    return i1RetVal;

}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeForwardAllTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardAllVlanIndex)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhValidateIndexInstanceDot1qForwardAllTable
        (u4Ieee8021QBridgeForwardAllVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardAllTable (UINT4
                                                *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                *pu4Ieee8021QBridgeForwardAllVlanIndex)
{

    UINT4               u4ComponentId = 0;

    if (VlanGetFirstActiveContext (&u4ComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021QBridgeVlanCurrentComponentId = u4ComponentId;
        if (nmhGetFirstIndexDot1qForwardAllTable
            (pu4Ieee8021QBridgeForwardAllVlanIndex) == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID
                ((*pu4Ieee8021QBridgeVlanCurrentComponentId));
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021QBridgeVlanCurrentComponentId, &u4ComponentId)
           == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
                nextIeee8021QBridgeForwardAllVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeForwardAllTable (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               UINT4
                                               *pu4NextIeee8021QBridgeForwardAllVlanIndex)
{

    UINT4               u4ComponentId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        == VLAN_SUCCESS)
    {
        if ((nmhGetNextIndexDot1qForwardAllTable
             (u4Ieee8021QBridgeForwardAllVlanIndex,
              pu4NextIeee8021QBridgeForwardAllVlanIndex)) == SNMP_SUCCESS)

        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID
                (u4Ieee8021QBridgeVlanCurrentComponentId);
            *pu4NextIeee8021QBridgeVlanCurrentComponentId =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext (u4Ieee8021QBridgeVlanCurrentComponentId,
                                      &u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        u4Ieee8021QBridgeVlanCurrentComponentId = u4ComponentId;
        *pu4NextIeee8021QBridgeVlanCurrentComponentId = u4ComponentId;

    }
    while (nmhGetFirstIndexDot1qForwardAllTable
           (pu4NextIeee8021QBridgeForwardAllVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllPorts (UINT4
                                      u4Ieee8021QBridgeVlanCurrentComponentId,
                                      UINT4
                                      u4Ieee8021QBridgeForwardAllVlanIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValIeee8021QBridgeForwardAllPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1qForwardAllPorts (u4Ieee8021QBridgeForwardAllVlanIndex,
                                           pRetValIeee8021QBridgeForwardAllPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllStaticPorts (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeForwardAllVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021QBridgeForwardAllStaticPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qForwardAllStaticPorts (u4Ieee8021QBridgeForwardAllVlanIndex,
                                          pRetValIeee8021QBridgeForwardAllStaticPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeForwardAllForbiddenPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qForwardAllForbiddenPorts
        (u4Ieee8021QBridgeForwardAllVlanIndex,
         pRetValIeee8021QBridgeForwardAllForbiddenPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                setValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardAllStaticPorts (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeForwardAllVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021QBridgeForwardAllStaticPorts)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qForwardAllStaticPorts (u4Ieee8021QBridgeForwardAllVlanIndex,
                                          pSetValIeee8021QBridgeForwardAllStaticPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                setValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValIeee8021QBridgeForwardAllForbiddenPorts)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qForwardAllForbiddenPorts
        (u4Ieee8021QBridgeForwardAllVlanIndex,
         pSetValIeee8021QBridgeForwardAllForbiddenPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                testValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardAllStaticPorts (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021QBridgeForwardAllStaticPorts)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qForwardAllStaticPorts (pu4ErrorCode,
                                                    u4Ieee8021QBridgeForwardAllVlanIndex,
                                                    pTestValIeee8021QBridgeForwardAllStaticPorts);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                testValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanCurrentComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeForwardAllVlanIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValIeee8021QBridgeForwardAllForbiddenPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qForwardAllForbiddenPorts (pu4ErrorCode,
                                                       u4Ieee8021QBridgeForwardAllVlanIndex,
                                                       pTestValIeee8021QBridgeForwardAllForbiddenPorts);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeForwardAllTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeForwardUnregisteredTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                                 UINT4
                                                                 u4Ieee8021QBridgeForwardUnregisteredVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                         *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         *pu4Ieee8021QBridgeForwardUnregisteredVlanIndex)
{

    UINT4               u4ComponentId = 0;

    if (VlanGetFirstActiveContext (&u4ComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4Ieee8021QBridgeVlanCurrentComponentId = u4ComponentId;
        if (nmhGetFirstIndexDot1qForwardUnregisteredTable
            (pu4Ieee8021QBridgeForwardUnregisteredVlanIndex) == SNMP_SUCCESS)
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeVlanCurrentComponentId));
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021QBridgeVlanCurrentComponentId, &u4ComponentId)
           == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
                nextIeee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeForwardUnregisteredVlanIndex)
{

    UINT4               u4ComponentId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        == VLAN_SUCCESS)
    {
        if ((nmhGetNextIndexDot1qForwardUnregisteredTable
             (u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
              pu4NextIeee8021QBridgeForwardUnregisteredVlanIndex)) ==
            SNMP_SUCCESS)

        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID
                (u4Ieee8021QBridgeVlanCurrentComponentId);
            *pu4NextIeee8021QBridgeVlanCurrentComponentId =
                u4Ieee8021QBridgeVlanCurrentComponentId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext (u4Ieee8021QBridgeVlanCurrentComponentId,
                                      &u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ComponentId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        u4Ieee8021QBridgeVlanCurrentComponentId = u4ComponentId;
        *pu4NextIeee8021QBridgeVlanCurrentComponentId = u4ComponentId;

    }
    while (nmhGetFirstIndexDot1qForwardUnregisteredTable
           (pu4NextIeee8021QBridgeForwardUnregisteredVlanIndex) !=
           SNMP_SUCCESS);
    VlanReleaseContext ();
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeForwardUnregisteredPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qForwardUnregisteredPorts
        (u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pRetValIeee8021QBridgeForwardUnregisteredPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pRetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qForwardUnregisteredStaticPorts
        (u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pRetValIeee8021QBridgeForwardUnregisteredStaticPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pRetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qForwardUnregisteredForbiddenPorts
        (u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pRetValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                setValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pSetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qForwardUnregisteredStaticPorts
        (u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pSetValIeee8021QBridgeForwardUnregisteredStaticPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                setValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qForwardUnregisteredForbiddenPorts
        (u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
         pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                testValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pTestValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1qForwardUnregisteredStaticPorts (pu4ErrorCode,
                                                      u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                      pTestValIeee8021QBridgeForwardUnregisteredStaticPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                testValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021QBridgeVlanCurrentComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pTestValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1qForwardUnregisteredForbiddenPorts (pu4ErrorCode,
                                                         u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                         pTestValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeForwardUnregisteredTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeStaticUnicastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable (UINT4
                                                           u4Ieee8021QBridgeStaticUnicastComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                           tMacAddr
                                                           Ieee8021QBridgeStaticUnicastAddress,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastReceivePort)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhValidateIndexInstanceFsDot1qStaticUnicastTable
            (u4Ieee8021QBridgeStaticUnicastComponentId,
             u4Ieee8021QBridgeStaticUnicastVlanIndex,
             Ieee8021QBridgeStaticUnicastAddress,
             u4Ieee8021QBridgeStaticUnicastReceivePort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable (UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastComponentId,
                                                   UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                   tMacAddr *
                                                   pIeee8021QBridgeStaticUnicastAddress,
                                                   UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastReceivePort)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qStaticUnicastTable ((INT4 *)
                                                          pu4Ieee8021QBridgeStaticUnicastComponentId,
                                                          pu4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                          pIeee8021QBridgeStaticUnicastAddress,
                                                          (INT4 *)
                                                          pu4Ieee8021QBridgeStaticUnicastReceivePort);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeStaticUnicastComponentId));
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                nextIeee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                nextIeee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                nextIeee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
                nextIeee8021QBridgeStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeStaticUnicastTable (UINT4
                                                  u4Ieee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticUnicastAddress,
                                                  tMacAddr *
                                                  pNextIeee8021QBridgeStaticUnicastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastReceivePort)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qStaticUnicastTable
        (u4Ieee8021QBridgeStaticUnicastComponentId,
         (INT4 *) pu4NextIeee8021QBridgeStaticUnicastComponentId,
         u4Ieee8021QBridgeStaticUnicastVlanIndex,
         pu4NextIeee8021QBridgeStaticUnicastVlanIndex,
         Ieee8021QBridgeStaticUnicastAddress,
         pNextIeee8021QBridgeStaticUnicastAddress,
         (INT4) u4Ieee8021QBridgeStaticUnicastReceivePort,
         (INT4 *) pu4NextIeee8021QBridgeStaticUnicastReceivePort);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeStaticUnicastComponentId));

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                     u4Ieee8021QBridgeStaticUnicastComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeStaticUnicastAddress,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pRetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeStaticUnicastComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qStaticUnicastAllowedToGoTo
        (u4Ieee8021QBridgeStaticUnicastVlanIndex,
         Ieee8021QBridgeStaticUnicastAddress,
         u4Ieee8021QBridgeStaticUnicastReceivePort,
         pRetValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pRetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeStaticUnicastComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qVlanForbiddenEgressPorts
        (u4Ieee8021QBridgeStaticUnicastVlanIndex,
         pRetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastStorageType (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgeStaticUnicastStorageType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhGetFsDot1qStaticUnicastStatus
            (u4Ieee8021QBridgeStaticUnicastComponentId,
             u4Ieee8021QBridgeStaticUnicastVlanIndex,
             Ieee8021QBridgeStaticUnicastAddress,
             u4Ieee8021QBridgeStaticUnicastReceivePort,
             pi4RetValIeee8021QBridgeStaticUnicastStorageType));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                             u4Ieee8021QBridgeStaticUnicastComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                             tMacAddr
                                             Ieee8021QBridgeStaticUnicastAddress,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastReceivePort,
                                             INT4
                                             *pi4RetValIeee8021QBridgeStaticUnicastRowStatus)
{

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhGetFsDot1qStaticUnicastRowStatus
            (u4Ieee8021QBridgeStaticUnicastComponentId,
             u4Ieee8021QBridgeStaticUnicastVlanIndex,
             Ieee8021QBridgeStaticUnicastAddress,
             u4Ieee8021QBridgeStaticUnicastReceivePort,
             pi4RetValIeee8021QBridgeStaticUnicastRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                     u4Ieee8021QBridgeStaticUnicastComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeStaticUnicastAddress,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeStaticUnicastComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qStaticUnicastAllowedToGoTo
        (u4Ieee8021QBridgeStaticUnicastVlanIndex,
         Ieee8021QBridgeStaticUnicastAddress,
         u4Ieee8021QBridgeStaticUnicastReceivePort,
         pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeStaticUnicastComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qVlanForbiddenEgressPorts
        (u4Ieee8021QBridgeStaticUnicastVlanIndex,
         pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastStorageType (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticUnicastStorageType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhSetFsDot1qStaticUnicastStatus
            (u4Ieee8021QBridgeStaticUnicastComponentId,
             u4Ieee8021QBridgeStaticUnicastVlanIndex,
             Ieee8021QBridgeStaticUnicastAddress,
             u4Ieee8021QBridgeStaticUnicastReceivePort,
             i4SetValIeee8021QBridgeStaticUnicastStorageType));
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                             u4Ieee8021QBridgeStaticUnicastComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                             tMacAddr
                                             Ieee8021QBridgeStaticUnicastAddress,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastReceivePort,
                                             INT4
                                             i4SetValIeee8021QBridgeStaticUnicastRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhSetFsDot1qStaticUnicastRowStatus
            (u4Ieee8021QBridgeStaticUnicastComponentId,
             u4Ieee8021QBridgeStaticUnicastVlanIndex,
             Ieee8021QBridgeStaticUnicastAddress,
             u4Ieee8021QBridgeStaticUnicastReceivePort,
             i4SetValIeee8021QBridgeStaticUnicastRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pTestValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeStaticUnicastComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qStaticUnicastAllowedToGoTo (pu4ErrorCode,
                                                         u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                         Ieee8021QBridgeStaticUnicastAddress,
                                                         u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                         pTestValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                           tMacAddr
                                                           Ieee8021QBridgeStaticUnicastAddress,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pTestValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeStaticUnicastComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2Dot1qVlanForbiddenEgressPorts
        (pu4ErrorCode, u4Ieee8021QBridgeStaticUnicastVlanIndex,
         pTestValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStorageType (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticUnicastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgeStaticUnicastStorageType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhTestv2FsDot1qStaticUnicastStatus (pu4ErrorCode,
                                                 u4Ieee8021QBridgeStaticUnicastComponentId,
                                                 u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                 Ieee8021QBridgeStaticUnicastAddress,
                                                 u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                 i4TestValIeee8021QBridgeStaticUnicastStorageType));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                tMacAddr
                                                Ieee8021QBridgeStaticUnicastAddress,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                INT4
                                                i4TestValIeee8021QBridgeStaticUnicastRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeStaticUnicastComponentId);

    return (nmhTestv2FsDot1qStaticUnicastRowStatus (pu4ErrorCode,
                                                    u4Ieee8021QBridgeStaticUnicastComponentId,
                                                    u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                    Ieee8021QBridgeStaticUnicastAddress,
                                                    u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                    i4TestValIeee8021QBridgeStaticUnicastRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeStaticUnicastTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeStaticMulticastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable (UINT4
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanIndex,
                                                             tMacAddr
                                                             Ieee8021QBridgeStaticMulticastAddress,
                                                             UINT4
                                                             u4Ieee8021QBridgeStaticMulticastReceivePort)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhValidateIndexInstanceFsDot1qStaticMulticastTable
            (u4Ieee8021QBridgeVlanCurrentComponentId,
             u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
             u4Ieee8021QBridgeStaticMulticastReceivePort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable (UINT4
                                                     *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     *pu4Ieee8021QBridgeVlanIndex,
                                                     tMacAddr *
                                                     pIeee8021QBridgeStaticMulticastAddress,
                                                     UINT4
                                                     *pu4Ieee8021QBridgeStaticMulticastReceivePort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qStaticMulticastTable ((INT4 *)
                                                            pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                            pu4Ieee8021QBridgeVlanIndex,
                                                            pIeee8021QBridgeStaticMulticastAddress,
                                                            (INT4 *)
                                                            pu4Ieee8021QBridgeStaticMulticastReceivePort);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeVlanCurrentComponentId));
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                nextIeee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
                nextIeee8021QBridgeStaticMulticastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeStaticMulticastTable (UINT4
                                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanIndex,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeVlanIndex,
                                                    tMacAddr
                                                    Ieee8021QBridgeStaticMulticastAddress,
                                                    tMacAddr *
                                                    pNextIeee8021QBridgeStaticMulticastAddress,
                                                    UINT4
                                                    u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeStaticMulticastReceivePort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qStaticMulticastTable ((INT4)
                                                           u4Ieee8021QBridgeVlanCurrentComponentId,
                                                           (INT4 *)
                                                           pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                           u4Ieee8021QBridgeVlanIndex,
                                                           pu4NextIeee8021QBridgeVlanIndex,
                                                           Ieee8021QBridgeStaticMulticastAddress,
                                                           pNextIeee8021QBridgeStaticMulticastAddress,
                                                           (INT4)
                                                           u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                           (INT4 *)
                                                           pu4NextIeee8021QBridgeStaticMulticastReceivePort);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeVlanCurrentComponentId));
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       tMacAddr
                                                       Ieee8021QBridgeStaticMulticastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pRetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qStaticMulticastStaticEgressPorts (u4Ieee8021QBridgeVlanIndex,
                                                     Ieee8021QBridgeStaticMulticastAddress,
                                                     u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                     pRetValIeee8021QBridgeStaticMulticastStaticEgressPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pRetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qStaticMulticastForbiddenEgressPorts
        (u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
         u4Ieee8021QBridgeStaticMulticastReceivePort,
         pRetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 *pi4RetValIeee8021QBridgeStaticMulticastStorageType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhGetFsDot1qStaticMulticastStatus
            (u4Ieee8021QBridgeVlanCurrentComponentId,
             u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
             u4Ieee8021QBridgeStaticMulticastReceivePort,
             pi4RetValIeee8021QBridgeStaticMulticastStorageType));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticMulticastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgeStaticMulticastRowStatus)
{

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhGetFsDot1qStaticMulticastRowStatus
            (u4Ieee8021QBridgeVlanCurrentComponentId,
             u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
             u4Ieee8021QBridgeStaticMulticastReceivePort,
             pi4RetValIeee8021QBridgeStaticMulticastRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       tMacAddr
                                                       Ieee8021QBridgeStaticMulticastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qStaticMulticastStaticEgressPorts (u4Ieee8021QBridgeVlanIndex,
                                                     Ieee8021QBridgeStaticMulticastAddress,
                                                     u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                     pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qStaticMulticastForbiddenEgressPorts
        (u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
         u4Ieee8021QBridgeStaticMulticastReceivePort,
         pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 i4SetValIeee8021QBridgeStaticMulticastStorageType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhSetFsDot1qStaticMulticastStatus
            (u4Ieee8021QBridgeVlanCurrentComponentId,
             u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
             u4Ieee8021QBridgeStaticMulticastReceivePort,
             i4SetValIeee8021QBridgeStaticMulticastStorageType));
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticMulticastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticMulticastRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhSetFsDot1qStaticMulticastRowStatus
            (u4Ieee8021QBridgeVlanCurrentComponentId,
             u4Ieee8021QBridgeVlanIndex, Ieee8021QBridgeStaticMulticastAddress,
             u4Ieee8021QBridgeStaticMulticastReceivePort,
             i4SetValIeee8021QBridgeStaticMulticastRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pTestValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qStaticMulticastStaticEgressPorts (pu4ErrorCode,
                                                               u4Ieee8021QBridgeVlanIndex,
                                                               Ieee8021QBridgeStaticMulticastAddress,
                                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                               pTestValIeee8021QBridgeStaticMulticastStaticEgressPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                             *pu4ErrorCode,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanIndex,
                                                             tMacAddr
                                                             Ieee8021QBridgeStaticMulticastAddress,
                                                             UINT4
                                                             u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pTestValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts (pu4ErrorCode,
                                                                  u4Ieee8021QBridgeVlanIndex,
                                                                  Ieee8021QBridgeStaticMulticastAddress,
                                                                  u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                                  pTestValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStorageType (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanIndex,
                                                    tMacAddr
                                                    Ieee8021QBridgeStaticMulticastAddress,
                                                    UINT4
                                                    u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                    INT4
                                                    i4TestValIeee8021QBridgeStaticMulticastStorageType)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhTestv2FsDot1qStaticMulticastStatus (pu4ErrorCode,
                                                   u4Ieee8021QBridgeVlanCurrentComponentId,
                                                   u4Ieee8021QBridgeVlanIndex,
                                                   Ieee8021QBridgeStaticMulticastAddress,
                                                   u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                   i4TestValIeee8021QBridgeStaticMulticastStorageType));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanCurrentComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticMulticastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgeStaticMulticastRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhTestv2FsDot1qStaticMulticastRowStatus (pu4ErrorCode,
                                                      u4Ieee8021QBridgeVlanCurrentComponentId,
                                                      u4Ieee8021QBridgeVlanIndex,
                                                      Ieee8021QBridgeStaticMulticastAddress,
                                                      u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                      i4TestValIeee8021QBridgeStaticMulticastRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeStaticMulticastTable (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanNumDeletes
 Input       :  The Indices

                The Object 
                retValIeee8021QBridgeVlanNumDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanNumDeletes (tSNMP_COUNTER64_TYPE *
                                     pu8RetValIeee8021QBridgeVlanNumDeletes)
{
    UINT4              *pu4lsn = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (0) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pu8RetValIeee8021QBridgeVlanNumDeletes->lsn = 0;
    pu8RetValIeee8021QBridgeVlanNumDeletes->msn = 0;

    pu4lsn = &(pu8RetValIeee8021QBridgeVlanNumDeletes->lsn);
    i1RetVal = nmhGetDot1qVlanNumDeletes (pu4lsn);
    VlanReleaseContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeVlanCurrentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable (UINT4
                                                         u4Ieee8021QBridgeVlanTimeMark,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanIndex)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhValidateIndexInstanceFsDot1qVlanCurrentTable ((INT4)
                                                             (INT4)
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             u4Ieee8021QBridgeVlanTimeMark,
                                                             u4Ieee8021QBridgeVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable (UINT4
                                                 *pu4Ieee8021QBridgeVlanTimeMark,
                                                 UINT4
                                                 *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 *pu4Ieee8021QBridgeVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qVlanCurrentTable ((INT4 *)
                                                        pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                        pu4Ieee8021QBridgeVlanTimeMark,
                                                        pu4Ieee8021QBridgeVlanIndex);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeVlanCurrentComponentId));
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                nextIeee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeVlanCurrentTable (UINT4
                                                u4Ieee8021QBridgeVlanTimeMark,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanTimeMark,
                                                UINT4
                                                u4Ieee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeVlanIndex,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qVlanCurrentTable ((INT4)
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       (INT4 *)
                                                       pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                       u4Ieee8021QBridgeVlanTimeMark,
                                                       pu4NextIeee8021QBridgeVlanTimeMark,
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       pu4NextIeee8021QBridgeVlanIndex);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeVlanCurrentComponentId));
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanFdbId
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanFdbId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanFdbId (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                UINT4 u4Ieee8021QBridgeVlanCurrentComponentId,
                                UINT4 u4Ieee8021QBridgeVlanIndex,
                                UINT4 *pu4RetValIeee8021QBridgeVlanFdbId)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);
    return (nmhGetFsDot1qVlanFdbId ((INT4)
                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                    u4Ieee8021QBridgeVlanTimeMark,
                                    u4Ieee8021QBridgeVlanIndex,
                                    pu4RetValIeee8021QBridgeVlanFdbId));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCurrentEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCurrentEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCurrentEgressPorts (UINT4
                                             u4Ieee8021QBridgeVlanTimeMark,
                                             UINT4
                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                             UINT4 u4Ieee8021QBridgeVlanIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValIeee8021QBridgeVlanCurrentEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanCurrentEgressPorts (u4Ieee8021QBridgeVlanTimeMark,
                                                  u4Ieee8021QBridgeVlanIndex,
                                                  pRetValIeee8021QBridgeVlanCurrentEgressPorts);

    /* release context info */
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCurrentUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts (UINT4
                                               u4Ieee8021QBridgeVlanTimeMark,
                                               UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeVlanCurrentUntaggedPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qVlanCurrentUntaggedPorts (u4Ieee8021QBridgeVlanTimeMark,
                                             u4Ieee8021QBridgeVlanIndex,
                                             pRetValIeee8021QBridgeVlanCurrentUntaggedPorts);

    /* release context info */
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStatus (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                 UINT4 u4Ieee8021QBridgeVlanCurrentComponentId,
                                 UINT4 u4Ieee8021QBridgeVlanIndex,
                                 INT4 *pi4RetValIeee8021QBridgeVlanStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhGetFsDot1qVlanStatus (u4Ieee8021QBridgeVlanCurrentComponentId,
                                     u4Ieee8021QBridgeVlanTimeMark,
                                     u4Ieee8021QBridgeVlanIndex,
                                     pi4RetValIeee8021QBridgeVlanStatus));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCreationTime
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCreationTime (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                       UINT4
                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                       UINT4 u4Ieee8021QBridgeVlanIndex,
                                       UINT4
                                       *pu4RetValIeee8021QBridgeVlanCreationTime)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanCurrentComponentId);

    return (nmhGetFsDot1qVlanCreationTime
            (u4Ieee8021QBridgeVlanCurrentComponentId,
             u4Ieee8021QBridgeVlanTimeMark, u4Ieee8021QBridgeVlanIndex,
             pu4RetValIeee8021QBridgeVlanCreationTime));
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeVlanStaticTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable (UINT4
                                                        u4Ieee8021QBridgeVlanStaticComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeVlanStaticVlanIndex)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    return (nmhValidateIndexInstanceFsDot1qVlanStaticTable
            (u4Ieee8021QBridgeVlanStaticComponentId,
             u4Ieee8021QBridgeVlanStaticVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanStaticTable (UINT4
                                                *pu4Ieee8021QBridgeVlanStaticComponentId,
                                                UINT4
                                                *pu4Ieee8021QBridgeVlanStaticVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qVlanStaticTable ((INT4 *)
                                                       pu4Ieee8021QBridgeVlanStaticComponentId,
                                                       pu4Ieee8021QBridgeVlanStaticVlanIndex);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeVlanStaticComponentId));

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                nextIeee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
                nextIeee8021QBridgeVlanStaticVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeVlanStaticTable (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanStaticVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qVlanStaticTable ((INT4)
                                                      u4Ieee8021QBridgeVlanStaticComponentId,
                                                      (INT4 *)
                                                      pu4NextIeee8021QBridgeVlanStaticComponentId,
                                                      u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                      pu4NextIeee8021QBridgeVlanStaticVlanIndex);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeVlanStaticComponentId));
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticName (UINT4
                                     u4Ieee8021QBridgeVlanStaticComponentId,
                                     UINT4 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValIeee8021QBridgeVlanStaticName)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);
    return (nmhGetFsDot1qVlanStaticName (u4Ieee8021QBridgeVlanStaticComponentId,
                                         u4Ieee8021QBridgeVlanStaticVlanIndex,
                                         pRetValIeee8021QBridgeVlanStaticName));
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021QBridgeVlanStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qVlanStaticEgressPorts (u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          pRetValIeee8021QBridgeVlanStaticEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qVlanForbiddenEgressPorts
        (u4Ieee8021QBridgeVlanStaticVlanIndex,
         pRetValIeee8021QBridgeVlanForbiddenEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1qVlanStaticUntaggedPorts
        (u4Ieee8021QBridgeVlanStaticVlanIndex,
         pRetValIeee8021QBridgeVlanStaticUntaggedPorts);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticRowStatus (UINT4
                                          u4Ieee8021QBridgeVlanStaticComponentId,
                                          UINT4
                                          u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          INT4
                                          *pi4RetValIeee8021QBridgeVlanStaticRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    return (nmhGetFsDot1qVlanStaticRowStatus
            (u4Ieee8021QBridgeVlanStaticComponentId,
             u4Ieee8021QBridgeVlanStaticVlanIndex,
             pi4RetValIeee8021QBridgeVlanStaticRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticName (UINT4
                                     u4Ieee8021QBridgeVlanStaticComponentId,
                                     UINT4 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValIeee8021QBridgeVlanStaticName)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    return (nmhSetFsDot1qVlanStaticName (u4Ieee8021QBridgeVlanStaticComponentId,
                                         u4Ieee8021QBridgeVlanStaticVlanIndex,
                                         pSetValIeee8021QBridgeVlanStaticName));
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021QBridgeVlanStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qVlanStaticEgressPorts (u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          pSetValIeee8021QBridgeVlanStaticEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qVlanForbiddenEgressPorts
        (u4Ieee8021QBridgeVlanStaticVlanIndex,
         pSetValIeee8021QBridgeVlanForbiddenEgressPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1qVlanStaticUntaggedPorts
        (u4Ieee8021QBridgeVlanStaticVlanIndex,
         pSetValIeee8021QBridgeVlanStaticUntaggedPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticRowStatus (UINT4
                                          u4Ieee8021QBridgeVlanStaticComponentId,
                                          UINT4
                                          u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          INT4
                                          i4SetValIeee8021QBridgeVlanStaticRowStatus)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    return (nmhSetFsDot1qVlanStaticRowStatus
            (u4Ieee8021QBridgeVlanStaticComponentId,
             u4Ieee8021QBridgeVlanStaticVlanIndex,
             i4SetValIeee8021QBridgeVlanStaticRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticName (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021QBridgeVlanStaticComponentId,
                                        UINT4
                                        u4Ieee8021QBridgeVlanStaticVlanIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValIeee8021QBridgeVlanStaticName)
{
    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    return (nmhTestv2FsDot1qVlanStaticName (pu4ErrorCode,
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            pTestValIeee8021QBridgeVlanStaticName));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021QBridgeVlanStaticEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qVlanStaticEgressPorts (pu4ErrorCode,
                                                    u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                    pTestValIeee8021QBridgeVlanStaticEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanStaticComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qVlanForbiddenEgressPorts (pu4ErrorCode,
                                                       u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                       pTestValIeee8021QBridgeVlanForbiddenEgressPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pTestValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    if (VlanSelectContext (u4Ieee8021QBridgeVlanStaticComponentId)
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Dot1qVlanStaticUntaggedPorts (pu4ErrorCode,
                                                      u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                      pTestValIeee8021QBridgeVlanStaticUntaggedPorts);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticRowStatus (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021QBridgeVlanStaticComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeVlanStaticVlanIndex,
                                             INT4
                                             i4TestValIeee8021QBridgeVlanStaticRowStatus)
{

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeVlanStaticComponentId);

    return (nmhTestv2FsDot1qVlanStaticRowStatus (pu4ErrorCode,
                                                 u4Ieee8021QBridgeVlanStaticComponentId,
                                                 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                 i4TestValIeee8021QBridgeVlanStaticRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeVlanStaticTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeNextFreeLocalVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                               u4Ieee8021QBridgeNextFreeLocalVlanComponentId)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);

    i1RetVal =
        nmhValidateIndexInstanceFsDot1qNextFreeLocalVlanIndexTable
        (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                       *pu4Ieee8021QBridgeNextFreeLocalVlanComponentId)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qNextFreeLocalVlanIndexTable ((INT4 *)
                                                                   pu4Ieee8021QBridgeNextFreeLocalVlanComponentId);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeNextFreeLocalVlanComponentId));
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
                nextIeee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                      u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                                      UINT4
                                                      *pu4NextIeee8021QBridgeNextFreeLocalVlanComponentId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qNextFreeLocalVlanIndexTable ((INT4)
                                                                  u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                                                  (INT4 *)
                                                                  pu4NextIeee8021QBridgeNextFreeLocalVlanComponentId);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeNextFreeLocalVlanComponentId));
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeNextFreeLocalVlanIndex
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId

                The Object 
                retValIeee8021QBridgeNextFreeLocalVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeNextFreeLocalVlanIndex (UINT4
                                             u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                             UINT4
                                             *pu4RetValIeee8021QBridgeNextFreeLocalVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);

    i1RetVal = nmhGetFsDot1qNextFreeLocalVlanIndex ((INT4)
                                                    u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                                    (INT4 *)
                                                    pu4RetValIeee8021QBridgeNextFreeLocalVlanIndex);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgePortVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanTable (UINT4
                                                      u4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      u4Ieee8021BridgeBasePort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if (VlanSelectContext (u4Ieee8021BridgeBasePortComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhValidateIndexInstanceFsDot1qPortVlanTable ((INT4)
                                                             u4Ieee8021BridgeBasePort);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanTable (UINT4
                                              *pu4Ieee8021BridgeBasePortComponentId,
                                              UINT4 *pu4Ieee8021BridgeBasePort)
{
    UINT2               u2LocalPortId = 0;

    if ((nmhGetFirstIndexFsDot1qPortVlanTable ((INT4 *)
                                               pu4Ieee8021BridgeBasePort)) ==
        SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (*pu4Ieee8021BridgeBasePort,
                                           pu4Ieee8021BridgeBasePortComponentId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        else
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgeBasePortComponentId));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgePortVlanTable (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePort)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexFsDot1qPortVlanTable
         ((INT4) u4Ieee8021BridgeBasePort,
          (INT4 *) pu4NextIeee8021BridgeBasePort)) == SNMP_FAILURE)
    {

        return SNMP_FAILURE;
    }
    if (VlanGetContextInfoFromIfIndex (*pu4NextIeee8021BridgeBasePort,
                                       pu4NextIeee8021BridgeBasePortComponentId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePvid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                           UINT4 u4Ieee8021BridgeBasePort,
                           UINT4 *pu4RetValIeee8021QBridgePvid)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1qPvid ((INT4) u4Ieee8021BridgeBasePort,
                                  pu4RetValIeee8021QBridgePvid);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgePortAcceptableFrameTypes)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    i1RetVal = nmhGetFsDot1qPortAcceptableFrameTypes
        ((INT4) u4Ieee8021BridgeBasePort,
         pi4RetValIeee8021QBridgePortAcceptableFrameTypes);

    if (*pi4RetValIeee8021QBridgePortAcceptableFrameTypes ==
        VLAN_ADMIT_ALL_FRAMES)
    {
        *pi4RetValIeee8021QBridgePortAcceptableFrameTypes =
            STD_ADMIT_ALL_FRAMES;
    }

    else if (*pi4RetValIeee8021QBridgePortAcceptableFrameTypes ==
             VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
    {
        *pi4RetValIeee8021QBridgePortAcceptableFrameTypes =
            STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
    }

    else if (*pi4RetValIeee8021QBridgePortAcceptableFrameTypes ==
             VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
    {
        *pi4RetValIeee8021QBridgePortAcceptableFrameTypes =
            STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortIngressFiltering (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021QBridgePortIngressFiltering)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1qPortIngressFiltering
        ((INT4) u4Ieee8021BridgeBasePort,
         pi4RetValIeee8021QBridgePortIngressFiltering);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            *pi4RetValIeee8021QBridgePortMvrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    INT4                i4MvrpEnabledStatus = 0;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4ContextId =
        u4Ieee8021BridgeBasePortComponentId;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.pi4PortMvrpEnabledStatus =
        &i4MvrpEnabledStatus;
    MrpConfigInfo.u4InfoType = MVRP_PORT_ENABLED_STATUS_GET;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        *pi4RetValIeee8021QBridgePortMvrpEnabledStatus =
            *(MrpConfigInfo.unMrpInfo.MvrpPortInfo.pi4PortMvrpEnabledStatus);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpFailedRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpFailedRegistrations (UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  tSNMP_COUNTER64_TYPE *
                                                  pu8RetValIeee8021QBridgePortMvrpFailedRegistrations)
{
    tMrpConfigInfo      MrpConfigInfo;
    tSNMP_COUNTER64_TYPE tempType;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));
    MEMSET (&tempType, 0, sizeof (tSNMP_COUNTER64_TYPE));

    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4ContextId =
        u4Ieee8021BridgeBasePortComponentId;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MrpConfigInfo.u4InfoType = MVRP_PORT_FAILED_REGIST_GET;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.pu8PortMvrpFailedRegist = &tempType;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        *pu8RetValIeee8021QBridgePortMvrpFailedRegistrations =
            *(MrpConfigInfo.unMrpInfo.MvrpPortInfo.pu8PortMvrpFailedRegist);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpLastPduOrigin
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpLastPduOrigin (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            tMacAddr *
                                            pRetValIeee8021QBridgePortMvrpLastPduOrigin)
{

    tMrpConfigInfo      MrpConfigInfo;
    tMacAddr            tempMacAddr;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));
    MEMSET (&tempMacAddr, 0, sizeof (tMacAddr));

    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4ContextId =
        u4Ieee8021BridgeBasePortComponentId;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MrpConfigInfo.u4InfoType = MVRP_PORT_PDU_ORIGIN_GET;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.pPortMvrpLastPduOrigin = &tempMacAddr;
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        MEMCPY (pRetValIeee8021QBridgePortMvrpLastPduOrigin,
                MrpConfigInfo.unMrpInfo.MvrpPortInfo.pPortMvrpLastPduOrigin,
                MAC_ADDR_LEN);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     *pi4RetValIeee8021QBridgePortRestrictedVlanRegistration)
{

    INT1                i1RetVal = SNMP_FAILURE;
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1qPortRestrictedVlanRegistration
        ((INT4) u4Ieee8021BridgeBasePort,
         pi4RetValIeee8021QBridgePortRestrictedVlanRegistration);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePvid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                           UINT4 u4Ieee8021BridgeBasePort,
                           UINT4 u4SetValIeee8021QBridgePvid)
{

    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhSetFsDot1qPvid ((INT4) u4Ieee8021BridgeBasePort,
                                  u4SetValIeee8021QBridgePvid);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4SetValIeee8021QBridgePortAcceptableFrameTypes)
{
    INT1                i1RetVal = SNMP_FAILURE;
/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    if (i4SetValIeee8021QBridgePortAcceptableFrameTypes == STD_ADMIT_ALL_FRAMES)
    {
        i4SetValIeee8021QBridgePortAcceptableFrameTypes = VLAN_ADMIT_ALL_FRAMES;
    }

    else if (i4SetValIeee8021QBridgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
    {
        i4SetValIeee8021QBridgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
    }

    else if (i4SetValIeee8021QBridgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
    {
        i4SetValIeee8021QBridgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    }

    i1RetVal = nmhSetFsDot1qPortAcceptableFrameTypes
        ((INT4) u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortAcceptableFrameTypes);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortIngressFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortIngressFiltering (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021QBridgePortIngressFiltering)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhSetFsDot1qPortIngressFiltering
        ((INT4) u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortIngressFiltering);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4SetValIeee8021QBridgePortMvrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4ContextId =
        u4Ieee8021BridgeBasePortComponentId;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.i4PortMvrpEnabledStatus =
        i4SetValIeee8021QBridgePortMvrpEnabledStatus;
    MrpConfigInfo.u4InfoType = MVRP_PORT_ENABLED_STATUS_SET;
    VLAN_UNLOCK();
    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
	    VLAN_LOCK();
	    return SNMP_SUCCESS;
    }
    VLAN_LOCK();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     i4SetValIeee8021QBridgePortRestrictedVlanRegistration)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhSetFsDot1qPortRestrictedVlanRegistration
        ((INT4) u4Ieee8021BridgeBasePort,
         i4SetValIeee8021QBridgePortRestrictedVlanRegistration);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePvid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePvid (UINT4 *pu4ErrorCode,
                              UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              UINT4 u4TestValIeee8021QBridgePvid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsDot1qPvid (pu4ErrorCode,
                                     (INT4) u4Ieee8021BridgeBasePort,
                                     u4TestValIeee8021QBridgePvid);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgePortAcceptableFrameTypes)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021QBridgePortAcceptableFrameTypes ==
        STD_ADMIT_ALL_FRAMES)
    {
        i4TestValIeee8021QBridgePortAcceptableFrameTypes =
            VLAN_ADMIT_ALL_FRAMES;
    }

    else if (i4TestValIeee8021QBridgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
    {
        i4TestValIeee8021QBridgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;
    }

    else if (i4TestValIeee8021QBridgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
    {
        i4TestValIeee8021QBridgePortAcceptableFrameTypes =
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES;
    }

    else if (i4TestValIeee8021QBridgePortAcceptableFrameTypes ==
             STD_ADMIT_ONLY_UNTAGGED_FRAMES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsDot1qPortAcceptableFrameTypes (pu4ErrorCode,
                                                         (INT4)
                                                         u4Ieee8021BridgeBasePort,
                                                         i4TestValIeee8021QBridgePortAcceptableFrameTypes);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortIngressFiltering (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021QBridgePortIngressFiltering)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsDot1qPortIngressFiltering (pu4ErrorCode,
                                                     (INT4)
                                                     u4Ieee8021BridgeBasePort,
                                                     i4TestValIeee8021QBridgePortIngressFiltering);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4TestValIeee8021QBridgePortMvrpEnabledStatus)
{
    tMrpConfigInfo      MrpConfigInfo;
    UINT4               u4ErrorCode = SNMP_FAILURE;
    UINT1               i1RetVal = SNMP_FAILURE;

    MEMSET (&MrpConfigInfo, 0, sizeof (MrpConfigInfo));

    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4ContextId =
        u4Ieee8021BridgeBasePortComponentId;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.i4PortMvrpEnabledStatus =
        i4TestValIeee8021QBridgePortMvrpEnabledStatus;
    MrpConfigInfo.u4InfoType = MVRP_PORT_ENABLED_STATUS_TEST;
    MrpConfigInfo.unMrpInfo.MvrpPortInfo.pu4ErrorCode = &u4ErrorCode;
    i1RetVal = MrpApiMrpConfigInfo (&MrpConfigInfo);
    if (i1RetVal == OSIX_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

/*    if ((MrpApiMrpConfigInfo (&MrpConfigInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
*/
    *pu4ErrorCode = *(MrpConfigInfo.unMrpInfo.MvrpPortInfo.pu4ErrorCode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4TestValIeee8021QBridgePortRestrictedVlanRegistration)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsDot1qPortRestrictedVlanRegistration (pu4ErrorCode,
                                                               (INT4)
                                                               u4Ieee8021BridgeBasePort,
                                                               i4TestValIeee8021QBridgePortRestrictedVlanRegistration);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgePortVlanTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgePortVlanStatisticsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                                u4Ieee8021BridgeBasePortComponentId,
                                                                UINT4
                                                                u4Ieee8021BridgeBasePort,
                                                                UINT4
                                                                u4Ieee8021QBridgeVlanIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (u4Ieee8021BridgeBasePort, u4Ieee8021QBridgeVlanIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                        *pu4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        *pu4Ieee8021BridgeBasePort,
                                                        UINT4
                                                        *pu4Ieee8021QBridgeVlanIndex)
{
    UINT2               u2LocalPortId = 0;

    if ((nmhGetFirstIndexFsDot1qPortVlanStatisticsTable ((INT4 *)
                                                         pu4Ieee8021BridgeBasePort,
                                                         pu4Ieee8021QBridgeVlanIndex))
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (*pu4Ieee8021BridgeBasePort,
                                           pu4Ieee8021BridgeBasePortComponentId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        else
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgeBasePortComponentId));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       *pu4NextIeee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       UINT4
                                                       *pu4NextIeee8021BridgeBasePort,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       UINT4
                                                       *pu4NextIeee8021QBridgeVlanIndex)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexFsDot1qPortVlanStatisticsTable
         ((INT4) u4Ieee8021BridgeBasePort,
          (INT4 *) pu4NextIeee8021BridgeBasePort,
          u4Ieee8021QBridgeVlanIndex,
          pu4NextIeee8021QBridgeVlanIndex)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanGetContextInfoFromIfIndex (*pu4NextIeee8021BridgeBasePort,
                                       pu4NextIeee8021BridgeBasePortComponentId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortInFrames
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortInFrames (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         UINT4 u4Ieee8021QBridgeVlanIndex,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValIeee8021QBridgeTpVlanPortInFrames)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4lsn = 0;
    UINT4               u4msn = 0;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1qTpVlanPortInFrames ((INT4) u4Ieee8021BridgeBasePort,
                                                u4Ieee8021QBridgeVlanIndex,
                                                &u4lsn);
    pu8RetValIeee8021QBridgeTpVlanPortInFrames->lsn = u4lsn;
    pu8RetValIeee8021QBridgeTpVlanPortInFrames->msn = u4msn;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortOutFrames
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortOutFrames (UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          UINT4 u4Ieee8021QBridgeVlanIndex,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValIeee8021QBridgeTpVlanPortOutFrames)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4lsn = 0;
    UINT4               u4msn = 0;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1qTpVlanPortOutFrames ((INT4)
                                                 u4Ieee8021BridgeBasePort,
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 &u4lsn);
    pu8RetValIeee8021QBridgeTpVlanPortOutFrames->lsn = u4lsn;
    pu8RetValIeee8021QBridgeTpVlanPortOutFrames->msn = u4msn;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortInDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortInDiscards (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           UINT4 u4Ieee8021QBridgeVlanIndex,
                                           tSNMP_COUNTER64_TYPE *
                                           pu8RetValIeee8021QBridgeTpVlanPortInDiscards)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4lsn = 0;
    UINT4               u4msn = 0;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1qTpVlanPortInDiscards ((INT4)
                                                  u4Ieee8021BridgeBasePort,
                                                  u4Ieee8021QBridgeVlanIndex,
                                                  &u4lsn);
    pu8RetValIeee8021QBridgeTpVlanPortInDiscards->lsn = u4lsn;
    pu8RetValIeee8021QBridgeTpVlanPortInDiscards->msn = u4msn;
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeLearningConstraintsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable (UINT4
                                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                                 UINT4
                                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                                 INT4
                                                                 i4Ieee8021QBridgeLearningConstraintsSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal =
        nmhValidateIndexInstanceFsDot1qLearningConstraintsTable
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable (UINT4
                                                         *pu4Ieee8021QBridgeLearningConstraintsComponentId,
                                                         UINT4
                                                         *pu4Ieee8021QBridgeLearningConstraintsVlan,
                                                         INT4
                                                         *pi4Ieee8021QBridgeLearningConstraintsSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qLearningConstraintsTable ((INT4 *)
                                                                pu4Ieee8021QBridgeLearningConstraintsComponentId,
                                                                pu4Ieee8021QBridgeLearningConstraintsVlan,
                                                                pi4Ieee8021QBridgeLearningConstraintsSet);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeLearningConstraintsComponentId));
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                nextIeee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                nextIeee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
                nextIeee8021QBridgeLearningConstraintsSet
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable (UINT4
                                                        u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeLearningConstraintsComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeLearningConstraintsVlan,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeLearningConstraintsVlan,
                                                        INT4
                                                        i4Ieee8021QBridgeLearningConstraintsSet,
                                                        INT4
                                                        *pi4NextIeee8021QBridgeLearningConstraintsSet)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qLearningConstraintsTable ((INT4)
                                                               u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                               (INT4 *)
                                                               pu4NextIeee8021QBridgeLearningConstraintsComponentId,
                                                               u4Ieee8021QBridgeLearningConstraintsVlan,
                                                               pu4NextIeee8021QBridgeLearningConstraintsVlan,
                                                               i4Ieee8021QBridgeLearningConstraintsSet,
                                                               pi4NextIeee8021QBridgeLearningConstraintsSet);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeLearningConstraintsComponentId));

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                retValIeee8021QBridgeLearningConstraintsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintsType (UINT4
                                              u4Ieee8021QBridgeLearningConstraintsComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeLearningConstraintsVlan,
                                              INT4
                                              i4Ieee8021QBridgeLearningConstraintsSet,
                                              INT4
                                              *pi4RetValIeee8021QBridgeLearningConstraintsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal =
        nmhGetFsDot1qConstraintType
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet,
         pi4RetValIeee8021QBridgeLearningConstraintsType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                retValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                *pi4RetValIeee8021QBridgeLearningConstraintsStatus)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal =
        nmhGetFsDot1qConstraintStatus
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet,
         pi4RetValIeee8021QBridgeLearningConstraintsStatus);
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                setValIeee8021QBridgeLearningConstraintsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintsType (UINT4
                                              u4Ieee8021QBridgeLearningConstraintsComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeLearningConstraintsVlan,
                                              INT4
                                              i4Ieee8021QBridgeLearningConstraintsSet,
                                              INT4
                                              i4SetValIeee8021QBridgeLearningConstraintsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal =
        nmhSetFsDot1qConstraintType
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet,
         i4SetValIeee8021QBridgeLearningConstraintsType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                setValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                i4SetValIeee8021QBridgeLearningConstraintsStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal =
        nmhSetFsDot1qConstraintStatus
        (u4Ieee8021QBridgeLearningConstraintsComponentId,
         u4Ieee8021QBridgeLearningConstraintsVlan,
         i4Ieee8021QBridgeLearningConstraintsSet,
         i4SetValIeee8021QBridgeLearningConstraintsStatus);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                testValIeee8021QBridgeLearningConstraintsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsType (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                 INT4
                                                 i4Ieee8021QBridgeLearningConstraintsSet,
                                                 INT4
                                                 i4TestValIeee8021QBridgeLearningConstraintsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal = nmhTestv2FsDot1qConstraintType (pu4ErrorCode,
                                               u4Ieee8021QBridgeLearningConstraintsComponentId,
                                               u4Ieee8021QBridgeLearningConstraintsVlan,
                                               i4Ieee8021QBridgeLearningConstraintsSet,
                                               i4TestValIeee8021QBridgeLearningConstraintsType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                testValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsStatus (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                   UINT4
                                                   u4Ieee8021QBridgeLearningConstraintsVlan,
                                                   INT4
                                                   i4Ieee8021QBridgeLearningConstraintsSet,
                                                   INT4
                                                   i4TestValIeee8021QBridgeLearningConstraintsStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintsComponentId);

    i1RetVal = nmhTestv2FsDot1qConstraintStatus (pu4ErrorCode,
                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                 i4Ieee8021QBridgeLearningConstraintsSet,
                                                 i4TestValIeee8021QBridgeLearningConstraintsStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeLearningConstraintsTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeLearningConstraintDefaultsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                                        u4Ieee8021QBridgeLearningConstraintDefaultsComponentId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal =
        nmhValidateIndexInstanceFsDot1qConstraintDefaultTable
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                                *pu4Ieee8021QBridgeLearningConstraintDefaultsComponentId)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1qConstraintDefaultTable ((INT4 *)
                                                              pu4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeLearningConstraintDefaultsComponentId));
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
                nextIeee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                               u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                               UINT4
                                                               *pu4NextIeee8021QBridgeLearningConstraintDefaultsComponentId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal = nmhGetNextIndexFsDot1qConstraintDefaultTable ((INT4)
                                                             u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                             (INT4 *)
                                                             pu4NextIeee8021QBridgeLearningConstraintDefaultsComponentId);
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeLearningConstraintDefaultsComponentId));

    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                retValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                    u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                    INT4
                                                    *pi4RetValIeee8021QBridgeLearningConstraintDefaultsSet)
{

    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal = nmhGetFsDot1qConstraintSetDefault ((INT4)
                                                  u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                  pi4RetValIeee8021QBridgeLearningConstraintDefaultsSet);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                retValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     INT4
                                                     *pi4RetValIeee8021QBridgeLearningConstraintDefaultsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal =
        nmhGetFsDot1qConstraintTypeDefault
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
         pi4RetValIeee8021QBridgeLearningConstraintDefaultsType);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                setValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                    u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                    INT4
                                                    i4SetValIeee8021QBridgeLearningConstraintDefaultsSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal =
        nmhSetFsDot1qConstraintSetDefault
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
         i4SetValIeee8021QBridgeLearningConstraintDefaultsSet);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                setValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     INT4
                                                     i4SetValIeee8021QBridgeLearningConstraintDefaultsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal =
        nmhSetFsDot1qConstraintTypeDefault
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
         i4SetValIeee8021QBridgeLearningConstraintDefaultsType);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                testValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet (UINT4 *pu4ErrorCode,
                                                       UINT4
                                                       u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                       INT4
                                                       i4TestValIeee8021QBridgeLearningConstraintDefaultsSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal = nmhTestv2FsDot1qConstraintSetDefault (pu4ErrorCode,
                                                     (INT4)
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     i4TestValIeee8021QBridgeLearningConstraintDefaultsSet);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                testValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                        INT4
                                                        i4TestValIeee8021QBridgeLearningConstraintDefaultsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);

    i1RetVal = nmhTestv2FsDot1qConstraintTypeDefault (pu4ErrorCode,
                                                      (INT4)
                                                      u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                      i4TestValIeee8021QBridgeLearningConstraintDefaultsType);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable (UINT4 *pu4ErrorCode,
                                                        tSnmpIndexList *
                                                        pSnmpIndexList,
                                                        tSNMP_VAR_BIND *
                                                        pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeProtocolGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable (UINT4
                                                           u4Ieee8021QBridgeProtocolGroupComponentId,
                                                           INT4
                                                           i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pIeee8021QBridgeProtocolTemplateProtocolValue)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhValidateIndexInstanceFsDot1vProtocolGroupTable ((INT4)
                                                                  u4Ieee8021QBridgeProtocolGroupComponentId,
                                                                  i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                                  pIeee8021QBridgeProtocolTemplateProtocolValue);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable (UINT4
                                                   *pu4Ieee8021QBridgeProtocolGroupComponentId,
                                                   INT4
                                                   *pi4Ieee8021QBridgeProtocolTemplateFrameType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pIeee8021QBridgeProtocolTemplateProtocolValue)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsDot1vProtocolGroupTable ((INT4 *)
                                                          pu4Ieee8021QBridgeProtocolGroupComponentId,
                                                          pi4Ieee8021QBridgeProtocolTemplateFrameType,
                                                          pIeee8021QBridgeProtocolTemplateProtocolValue);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021QBridgeProtocolGroupComponentId));

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                nextIeee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                nextIeee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
                nextIeee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeProtocolGroupTable (UINT4
                                                  u4Ieee8021QBridgeProtocolGroupComponentId,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeProtocolGroupComponentId,
                                                  INT4
                                                  i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                  INT4
                                                  *pi4NextIeee8021QBridgeProtocolTemplateFrameType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextIeee8021QBridgeProtocolTemplateProtocolValue)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhGetNextIndexFsDot1vProtocolGroupTable ((INT4)
                                                         u4Ieee8021QBridgeProtocolGroupComponentId,
                                                         (INT4 *)
                                                         pu4NextIeee8021QBridgeProtocolGroupComponentId,
                                                         i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                         pi4NextIeee8021QBridgeProtocolTemplateFrameType,
                                                         pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                         pNextIeee8021QBridgeProtocolTemplateProtocolValue);

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021QBridgeProtocolGroupComponentId));

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                retValIeee8021QBridgeProtocolGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolGroupId (UINT4
                                      u4Ieee8021QBridgeProtocolGroupComponentId,
                                      INT4
                                      i4Ieee8021QBridgeProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIeee8021QBridgeProtocolTemplateProtocolValue,
                                      INT4
                                      *pi4RetValIeee8021QBridgeProtocolGroupId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhGetFsDot1vProtocolGroupId ((INT4)
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             pi4RetValIeee8021QBridgeProtocolGroupId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                retValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             INT4
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             INT4
                                             *pi4RetValIeee8021QBridgeProtocolGroupRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhGetFsDot1vProtocolGroupRowStatus ((INT4)
                                                    u4Ieee8021QBridgeProtocolGroupComponentId,
                                                    i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                    pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                    pi4RetValIeee8021QBridgeProtocolGroupRowStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                setValIeee8021QBridgeProtocolGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolGroupId (UINT4
                                      u4Ieee8021QBridgeProtocolGroupComponentId,
                                      INT4
                                      i4Ieee8021QBridgeProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIeee8021QBridgeProtocolTemplateProtocolValue,
                                      INT4
                                      i4SetValIeee8021QBridgeProtocolGroupId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhSetFsDot1vProtocolGroupId ((INT4)
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             i4SetValIeee8021QBridgeProtocolGroupId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                setValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             INT4
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             INT4
                                             i4SetValIeee8021QBridgeProtocolGroupRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhSetFsDot1vProtocolGroupRowStatus ((INT4)
                                                    u4Ieee8021QBridgeProtocolGroupComponentId,
                                                    i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                    pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                    i4SetValIeee8021QBridgeProtocolGroupRowStatus);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                testValIeee8021QBridgeProtocolGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolGroupId (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021QBridgeProtocolGroupComponentId,
                                         INT4
                                         i4Ieee8021QBridgeProtocolTemplateFrameType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pIeee8021QBridgeProtocolTemplateProtocolValue,
                                         INT4
                                         i4TestValIeee8021QBridgeProtocolGroupId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhTestv2FsDot1vProtocolGroupId (pu4ErrorCode,
                                                (INT4)
                                                u4Ieee8021QBridgeProtocolGroupComponentId,
                                                i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                i4TestValIeee8021QBridgeProtocolGroupId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                testValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021QBridgeProtocolGroupComponentId,
                                                INT4
                                                i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                INT4
                                                i4TestValIeee8021QBridgeProtocolGroupRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021QBridgeProtocolGroupComponentId);

    i1RetVal = nmhTestv2FsDot1vProtocolGroupRowStatus (pu4ErrorCode,
                                                       (INT4)
                                                       u4Ieee8021QBridgeProtocolGroupComponentId,
                                                       i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                       pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                       i4TestValIeee8021QBridgeProtocolGroupRowStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeProtocolGroupTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeProtocolPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable (UINT4
                                                          u4Ieee8021BridgeBasePortComponentId,
                                                          UINT4
                                                          u4Ieee8021BridgeBasePort,
                                                          INT4
                                                          i4Ieee8021QBridgeProtocolPortGroupId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhValidateIndexInstanceFsDot1vProtocolPortTable ((INT4)
                                                                 u4Ieee8021BridgeBasePort,
                                                                 i4Ieee8021QBridgeProtocolPortGroupId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolPortTable (UINT4
                                                  *pu4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  *pu4Ieee8021BridgeBasePort,
                                                  INT4
                                                  *pi4Ieee8021QBridgeProtocolPortGroupId)
{
    UINT2               u2LocalPortId = 0;

    if ((nmhGetFirstIndexFsDot1vProtocolPortTable ((INT4 *)
                                                   pu4Ieee8021BridgeBasePort,
                                                   pi4Ieee8021QBridgeProtocolPortGroupId))
        == SNMP_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (*pu4Ieee8021BridgeBasePort,
                                           pu4Ieee8021BridgeBasePortComponentId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        else
        {
            VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021BridgeBasePortComponentId));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
                nextIeee8021QBridgeProtocolPortGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeProtocolPortTable (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4NextIeee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 UINT4
                                                 *pu4NextIeee8021BridgeBasePort,
                                                 INT4
                                                 i4Ieee8021QBridgeProtocolPortGroupId,
                                                 INT4
                                                 *pi4NextIeee8021QBridgeProtocolPortGroupId)
{
    UINT2               u2LocalPortId = 0;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexFsDot1vProtocolPortTable
         ((INT4) u4Ieee8021BridgeBasePort,
          (INT4 *) pu4NextIeee8021BridgeBasePort,
          i4Ieee8021QBridgeProtocolPortGroupId,
          pi4NextIeee8021QBridgeProtocolPortGroupId)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanGetContextInfoFromIfIndex (*pu4NextIeee8021BridgeBasePort,
                                       pu4NextIeee8021BridgeBasePortComponentId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    VLAN_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021BridgeBasePortComponentId));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                retValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolPortGroupVid (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021QBridgeProtocolPortGroupId,
                                           INT4
                                           *pi4RetValIeee8021QBridgeProtocolPortGroupVid)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1vProtocolPortGroupVid ((INT4)
                                                  u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021QBridgeProtocolPortGroupId,
                                                  pi4RetValIeee8021QBridgeProtocolPortGroupVid);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                retValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolPortRowStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021QBridgeProtocolPortGroupId,
                                            INT4
                                            *pi4RetValIeee8021QBridgeProtocolPortRowStatus)
{

    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhGetFsDot1vProtocolPortRowStatus ((INT4)
                                                   u4Ieee8021BridgeBasePort,
                                                   i4Ieee8021QBridgeProtocolPortGroupId,
                                                   pi4RetValIeee8021QBridgeProtocolPortRowStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                setValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolPortGroupVid (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021QBridgeProtocolPortGroupId,
                                           INT4
                                           i4SetValIeee8021QBridgeProtocolPortGroupVid)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhSetFsDot1vProtocolPortGroupVid ((INT4)
                                                  u4Ieee8021BridgeBasePort,
                                                  i4Ieee8021QBridgeProtocolPortGroupId,
                                                  i4SetValIeee8021QBridgeProtocolPortGroupVid);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                setValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolPortRowStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021QBridgeProtocolPortGroupId,
                                            INT4
                                            i4SetValIeee8021QBridgeProtocolPortRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

/* The BridgeBasePortComponentId is not needed in this table because BridgeBasePort itself provides required attributes.The validation of BridgeBasePortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);

    i1RetVal = nmhSetFsDot1vProtocolPortRowStatus ((INT4)
                                                   u4Ieee8021BridgeBasePort,
                                                   i4Ieee8021QBridgeProtocolPortGroupId,
                                                   i4SetValIeee8021QBridgeProtocolPortRowStatus);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                testValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolPortGroupVid (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4Ieee8021QBridgeProtocolPortGroupId,
                                              INT4
                                              i4TestValIeee8021QBridgeProtocolPortGroupVid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsDot1vProtocolPortGroupVid (pu4ErrorCode,
                                                     (INT4)
                                                     u4Ieee8021BridgeBasePort,
                                                     i4Ieee8021QBridgeProtocolPortGroupId,
                                                     i4TestValIeee8021QBridgeProtocolPortGroupVid);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                testValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolPortRowStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4Ieee8021QBridgeProtocolPortGroupId,
                                               INT4
                                               i4TestValIeee8021QBridgeProtocolPortRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021BridgeBasePortComponentId);

    if ((VlanValidateComponentId (u4Ieee8021BridgeBasePortComponentId,
                                  u4Ieee8021BridgeBasePort)) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2FsDot1vProtocolPortRowStatus (pu4ErrorCode,
                                                      (INT4)
                                                      u4Ieee8021BridgeBasePort,
                                                      i4Ieee8021QBridgeProtocolPortGroupId,
                                                      i4TestValIeee8021QBridgeProtocolPortRowStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeProtocolPortTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
