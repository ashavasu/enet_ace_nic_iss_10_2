/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: std1llwr.c,v 1.3 2016/07/16 11:15:04 siva Exp $
* Description: This header file contains all prototype of the wrapper
*              routines in VLAN Modules.
****************************************************************************/

# include  "lr.h" 
# include  "fssnmp.h" 
# include  "vlaninc.h" 
# include  "std1lldb.h"

INT4 GetNextIndexLldpXdot1EvbConfigEvbTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXdot1EvbConfigEvbTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXdot1EvbConfigEvbTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterSTD1LL ()
{
	SNMPRegisterMibWithLock (&std1llOID, &std1llEntry, VlanLock, VlanUnLock,
                             SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&std1llOID, (const UINT1 *) "std1lldevb");
}



VOID UnRegisterSTD1LL ()
{
	SNMPUnRegisterMib (&std1llOID, &std1llEntry);
	SNMPDelSysorEntry (&std1llOID, (const UINT1 *) "std1lldevb");
}

INT4 LldpXdot1EvbConfigEvbTxEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXdot1EvbConfigEvbTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXdot1EvbConfigEvbTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXdot1EvbConfigEvbTxEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLldpXdot1EvbConfigEvbTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 LldpXdot1EvbConfigEvbTxEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LldpXdot1EvbConfigEvbTxEnable(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 LldpXdot1EvbConfigEvbTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LldpXdot1EvbConfigEvbTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexLldpXdot1EvbConfigCdcpTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXdot1EvbConfigCdcpTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXdot1EvbConfigCdcpTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXdot1EvbConfigCdcpTxEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXdot1EvbConfigCdcpTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXdot1EvbConfigCdcpTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXdot1EvbConfigCdcpTxEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLldpXdot1EvbConfigCdcpTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 LldpXdot1EvbConfigCdcpTxEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LldpXdot1EvbConfigCdcpTxEnable(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 LldpXdot1EvbConfigCdcpTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LldpXdot1EvbConfigCdcpTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexLldpV2Xdot1LocEvbTlvTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpV2Xdot1LocEvbTlvTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpV2Xdot1LocEvbTlvTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpV2Xdot1LocEvbTlvStringGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpV2Xdot1LocEvbTlvTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpV2Xdot1LocEvbTlvString(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexLldpV2Xdot1LocCdcpTlvTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpV2Xdot1LocCdcpTlvTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpV2Xdot1LocCdcpTlvTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpV2Xdot1LocCdcpTlvStringGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpV2Xdot1LocCdcpTlvTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpV2Xdot1LocCdcpTlvString(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexLldpV2Xdot1RemEvbTlvTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpV2Xdot1RemEvbTlvTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpV2Xdot1RemEvbTlvTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpV2Xdot1RemEvbTlvStringGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpV2Xdot1RemEvbTlvTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpV2Xdot1RemEvbTlvString(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexLldpV2Xdot1RemCdcpTlvTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpV2Xdot1RemCdcpTlvTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpV2Xdot1RemCdcpTlvTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpV2Xdot1RemCdcpTlvStringGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpV2Xdot1RemCdcpTlvTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpV2Xdot1RemCdcpTlvString(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}


