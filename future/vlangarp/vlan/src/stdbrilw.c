/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbrilw.c,v 1.22 2013/11/28 10:21:57 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "lr.h"
#include "fssnmp.h"
#include "vlaninc.h"
#include "fsmsbrcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeAddress
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeAddress (tMacAddr * pRetValDot1dBaseBridgeAddress)
{
    VLAN_MEMSET (pRetValDot1dBaseBridgeAddress, 0, sizeof (tMacAddr));
    VlanCfaGetSysMacAddress (*pRetValDot1dBaseBridgeAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseNumPorts
 Input       :  The Indices

                The Object 
                retValDot1dBaseNumPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseNumPorts (INT4 *pi4RetValDot1dBaseNumPorts)
{
    *pi4RetValDot1dBaseNumPorts = VLAN_MAX_PORTS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseType
 Input       :  The Indices

                The Object 
                retValDot1dBaseType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseType (INT4 *pi4RetValDot1dBaseType)
{
    *pi4RetValDot1dBaseType = TRANSPARENT_BRIDGE;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dBasePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dBasePortTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dBasePortTable (INT4 i4Dot1dBasePort)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dBasePortTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1dBasePortTable (INT4 *pi4Dot1dBasePort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1dBasePortTable (0, pi4Dot1dBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dBasePortTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dBasePortTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pPortEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4Dot1dBasePort + 1);
         u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry != NULL)
        {
            *pi4NextDot1dBasePort = (INT4) u2Port;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dBasePortIfIndex
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortIfIndex (INT4 i4Dot1dBasePort,
                            INT4 *pi4RetValDot1dBasePortIfIndex)
{
    *pi4RetValDot1dBasePortIfIndex = i4Dot1dBasePort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortCircuit
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortCircuit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortCircuit (INT4 i4Dot1dBasePort,
                            tSNMP_OID_TYPE * pRetValDot1dBasePortCircuit)
{
    /*
     * Currently only Ethernet interfaces are supported and hence returning
     * the OID {0, 0}.
     */

    UNUSED_PARAM (i4Dot1dBasePort);

    pRetValDot1dBasePortCircuit->u4_Length = 2;
    MEMSET (pRetValDot1dBasePortCircuit->pu4_OidList, 0, 2 * sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortDelayExceededDiscards
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortDelayExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortDelayExceededDiscards (INT4 i4Dot1dBasePort,
                                          UINT4
                                          *pu4RetValDot1dBasePortDelayExceededDiscards)
{
#ifdef NPAPI_WANTED
/* Counter for frames discarded due to delay exceeded */
    if (CfaFsHwGetStat
        (i4Dot1dBasePort, NP_STAT_DOT1D_BASE_PORT_DLY_EXCEEDED_DISCARDS,
         pu4RetValDot1dBasePortDelayExceededDiscards) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else /* NPAPI_WANTED */
    UNUSED_PARAM (i4Dot1dBasePort);

    *pu4RetValDot1dBasePortDelayExceededDiscards = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortMtuExceededDiscards
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortMtuExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortMtuExceededDiscards (INT4 i4Dot1dBasePort,
                                        UINT4
                                        *pu4RetValDot1dBasePortMtuExceededDiscards)
{
#ifdef NPAPI_WANTED
/* Counter for frames discarded due to MTU exceeded */
    if (CfaFsHwGetStat
        (i4Dot1dBasePort, NP_STAT_DOT1D_BASE_PORT_MTU_EXCEEDED_DISCARDS,
         pu4RetValDot1dBasePortMtuExceededDiscards) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

#else
    UNUSED_PARAM (i4Dot1dBasePort);

    *pu4RetValDot1dBasePortMtuExceededDiscards = 0;
    return SNMP_SUCCESS;
#endif
}

/* Low Level GET Routine for All Objects  */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dTpLearnedEntryDiscards
 Input       :  The Indices

                The Object 
                retValDot1dTpLearnedEntryDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpLearnedEntryDiscards (UINT4 *pu4RetValDot1dTpLearnedEntryDiscards)
{
    *pu4RetValDot1dTpLearnedEntryDiscards = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpAgingTime
 Input       :  The Indices

                The Object 
                retValDot1dTpAgingTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1dTpAgingTime (INT4 *pi4RetValDot1dTpAgingTime)
{
    *pi4RetValDot1dTpAgingTime = VLAN_FDB_TIME_TO_MGMT
        (gpVlanContextInfo->u4AgeOutTime);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1dTpAgingTime
 Input       :  The Indices

                The Object 
                setValDot1dTpAgingTime
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot1dTpAgingTime (INT4 i4SetValDot1dTpAgingTime)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT4                i4AgeOutTimer = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VlanHwSetAgingTime (VLAN_CURR_CONTEXT_ID (),
                            VLAN_MGMT_TO_FDB_TIME (i4SetValDot1dTpAgingTime))
        != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Restart the MAC age-out split timer with new value
     * so that new MAC aging timer value is effective immediately
     * amd already learnt FDB entries will be flushed
     * based on new MAC age-out value */
    VlanStopTimer (VLAN_AGEOUT_TIMER);
    i4AgeOutTimer = VLAN_SPLIT_AGEOUT_TIME (i4SetValDot1dTpAgingTime);
    VlanStartTimer (VLAN_AGEOUT_TIMER, i4AgeOutTimer);

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dTpAgingTime, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    VLAN_CURR_CONTEXT_PTR ()->u4AgeOutTime =
        VLAN_MGMT_TO_FDB_TIME (i4SetValDot1dTpAgingTime);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValDot1dTpAgingTime));
    VlanSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot1dTpAgingTime
Input       :  The Indices

The Object 
testValDot1dTpAgingTime
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot1dTpAgingTime (UINT4 *pu4ErrorCode, INT4 i4TestValDot1dTpAgingTime)
{

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dTpAgingTime >= (INT4) VLAN_MIN_AGEOUT_TIME) &&
        (i4TestValDot1dTpAgingTime <= (INT4) VLAN_MAX_AGEOUT_TIME))
    {
        return (INT1) SNMP_SUCCESS;
    }

    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
    return (INT1) SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dTpAgingTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dTpAgingTime (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dTpFdbTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot1dTpFdbTable
Input       :  The Indices
Dot1dTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dTpFdbTable (tMacAddr Dot1dTpFdbAddress)
{
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    tVlanFdbInfo       *pVlanFdbInfo;
#else
    tHwUnicastMacEntry  HwUcastMacEntry;
#endif
#else
    tVlanFdbEntry      *pFdbEntry = NULL;
    UINT4               u4FidIndex;
#endif
    UINT4               u4FdbId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NextVlanId = 0;

    if (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId) == VLAN_FAILURE)
    {
        /* No Active Vlans Found */
        return SNMP_FAILURE;
    }

    do
    {
        u2VlanId = u2NextVlanId;

        /* Getting Fdb Id for the Active Vlan Id. This Function 
         * should not fail. So, Failure check is not done */
        VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId);
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
        pVlanFdbInfo = VlanGetFdbEntry (u4FdbId, Dot1dTpFdbAddress);

        if (pVlanFdbInfo != NULL)
        {
            return SNMP_SUCCESS;
        }
#else
        if (VlanHwGetFdbEntry
            (VLAN_CURR_CONTEXT_ID (), u4FdbId, Dot1dTpFdbAddress,
             &HwUcastMacEntry) == VLAN_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
#endif
#else
        u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

        if (u4FidIndex != VLAN_INVALID_FID_INDEX)
        {

            pFdbEntry = VlanGetUcastEntry (u4FidIndex, Dot1dTpFdbAddress);

            if (pFdbEntry != NULL)
            {
                return SNMP_SUCCESS;
            }
        }
#endif /* NPAPI_WANTED */
    }
    while (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId)
           == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpFdbTable
 Input       :  The Indices
                Dot1dTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1dTpFdbTable (tMacAddr * pDot1dTpFdbAddress)
{
    UINT4               u4FdbId = 0;
    UINT4               u4NextFdbId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NextVlanId = 0;
    tMacAddr            NextDot1dTpFdbAddress;
    tMacAddr            ZeroDot1dTpFdbAddress;

    BOOL1               bFlag = FALSE;

    MEMSET ((UINT1 *) &NextDot1dTpFdbAddress, 0, sizeof (tMacAddr));
    MEMSET ((UINT1 *) &ZeroDot1dTpFdbAddress, 0, sizeof (tMacAddr));

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId) == VLAN_FAILURE)
    {
        /* No Active Vlans Found */
        return SNMP_FAILURE;
    }

    do
    {
        u2VlanId = u2NextVlanId;

        /* Getting Fdb Id for the Active Vlan Id. This Function 
         * should not fail. So, Failure check is not done */
        VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId);

        /* Passing this Active Vlan Id and zero FdbAddress to the 
         * Get Next Routine below should fetch the First FdbAddress
         * in this active Vlan. If this routine returns failure,
         * there are no FdbAddresses learnt over any vlan available in
         * the system and hence break. */
        if (VlanGetNextIndexDot1qTpFdbTable (u4FdbId, &u4NextFdbId,
                                             ZeroDot1dTpFdbAddress,
                                             &NextDot1dTpFdbAddress)
            == VLAN_FAILURE)
        {
            break;
        }

        /* Fdb Got Changed, so get Vlan Id of the FDB Id used for
         * further iterations. */
        if (u4FdbId != u4NextFdbId)
        {
            VlanGetVlanIdFromFdbId (u4NextFdbId, &u2VlanId);
        }

        /* For the first time, copying the FdbAddress to a temporary 
         * value for useful for future comparisons. */
        if (bFlag == FALSE)
        {
            VLAN_CPY_MAC_ADDR (pDot1dTpFdbAddress, NextDot1dTpFdbAddress);

            bFlag = TRUE;
            continue;
        }

        /* If the FdbAddress in any other active vlan is lesser than the
         * one we have got store it in the output parameter. */
        if (VLAN_MEMCMP (NextDot1dTpFdbAddress,
                         pDot1dTpFdbAddress, ETHERNET_ADDR_SIZE) < 0)
        {
            VLAN_CPY_MAC_ADDR (pDot1dTpFdbAddress, NextDot1dTpFdbAddress);
        }
    }
    while (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId)
           == VLAN_SUCCESS);

    if (bFlag == FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpFdbTable
 Input       :  The Indices
                Dot1dTpFdbAddress
                nextDot1dTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dTpFdbTable (tMacAddr Dot1dTpFdbAddress,
                                tMacAddr * pNextDot1dTpFdbAddress)
{
    UINT4               u4FdbId = 0;
    UINT4               u4NextFdbId = 0;
    UINT4               u4TempFdbId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NextVlanId = 0;
    tMacAddr            NextDot1dTpFdbAddress;
    tMacAddr            ZeroDot1dTpFdbAddress;
    tMacAddr            TempDot1dTpFdbAddress;

    BOOL1               bOuterLoop = FALSE;
    BOOL1               bInnerLoop = FALSE;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (NextDot1dTpFdbAddress, 0, ETHERNET_ADDR_SIZE);
    MEMSET (TempDot1dTpFdbAddress, 0, ETHERNET_ADDR_SIZE);
    MEMSET (ZeroDot1dTpFdbAddress, 0, ETHERNET_ADDR_SIZE);

    MEMSET (pNextDot1dTpFdbAddress, 0, ETHERNET_ADDR_SIZE);

    if (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId) == VLAN_FAILURE)
    {
        /* No Active Vlans Found */
        return SNMP_FAILURE;
    }

    do
    {
        bInnerLoop = FALSE;

        u2VlanId = u2NextVlanId;

        /* Getting Fdb Id for the Active Vlan Id. This Function 
         * should not fail. So, Failure check is not done */
        VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId);

        /* Store the Fdb Id fetched. */
        u4TempFdbId = u4FdbId;

        VLAN_MEMCPY (TempDot1dTpFdbAddress, ZeroDot1dTpFdbAddress,
                     ETHERNET_ADDR_SIZE);

        /* 1. Initially pass this Fdb Id and zero FdbAddress to the Get Next
         *    Routine. It should fetch the First FdbAddress in this active Vlan. 
         * 2. Store this Fdb Id and FdbAddress.
         * 3. Compare the fetch FdbAddress with the one received as input.
         * 4. If this FdbAddress > Input FdbAddress, break this loop as we have
         *    found a FdbAddress which is greater than the Input FdbAddress.
         * 5. If this routine returns failure, there are no FdbAddresses learnt
         *    over any vlan available in the system. */
        while (VlanGetNextIndexDot1qTpFdbTable (u4FdbId, &u4NextFdbId,
                                                TempDot1dTpFdbAddress,
                                                &NextDot1dTpFdbAddress)
               == VLAN_SUCCESS)
        {
            u4FdbId = u4NextFdbId;
            VLAN_CPY_MAC_ADDR (TempDot1dTpFdbAddress, NextDot1dTpFdbAddress);

            if (VLAN_MEMCMP (NextDot1dTpFdbAddress,
                             Dot1dTpFdbAddress, ETHERNET_ADDR_SIZE) > 0)
            {
                bInnerLoop = TRUE;
                break;
            }
        }

        /* If bInnerLoop is FALSE, we have scanned all the FdbId and FdbAddresses
         * available in the system and hence break this Outer Loop. */
        if (bInnerLoop == FALSE)
        {
            break;
        }

        /* If the FdbId passed to the inner loop is different from the one we 
         * have received from the inner loop, then get the vlan id corresponding
         * to the received FdbId useful for further iterations of outer loop. */
        if (u4TempFdbId != u4NextFdbId)
        {
            VlanGetVlanIdFromFdbId (u4NextFdbId, &u2VlanId);
        }

        /* For the first time, store the received FdbAddress from the inner loop 
         * as the next maximum FdbAddress to the Input FdbAddress. */
        if (bOuterLoop == FALSE)
        {
            VLAN_CPY_MAC_ADDR (pNextDot1dTpFdbAddress, NextDot1dTpFdbAddress);
            bOuterLoop = TRUE;
            continue;
        }

        /* Compare received FdbAddress from the Inner Loop with the already
         * choosen next maximum FdbAddress. If it is greater, choose it
         * as next maximum FdbAddress to the Input FdbAddress. */
        if (VLAN_MEMCMP (NextDot1dTpFdbAddress,
                         pNextDot1dTpFdbAddress, ETHERNET_ADDR_SIZE) < 0)
        {
            VLAN_CPY_MAC_ADDR (pNextDot1dTpFdbAddress, NextDot1dTpFdbAddress);
        }
    }
    while (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId)
           == VLAN_SUCCESS);

    /* If bOuterLoop is FALSE, we have scanned all the FdbId, FdbAddress 
     * combinations in the system but have not found one better than the passed
     * input. So, return SNMP_FAILURE. */
    if (bOuterLoop == FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dTpFdbPort
 Input       :  The Indices
                Dot1dTpFdbAddress

                The Object 
                retValDot1dTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpFdbPort (tMacAddr Dot1dTpFdbAddress, INT4 *pi4RetValDot1dTpFdbPort)
{
    UINT4               u4FdbId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NextVlanId = 0;

    BOOL1               bFlag = FALSE;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId) == VLAN_FAILURE)
    {
        /* No Active Vlans Found */
        return SNMP_FAILURE;
    }

    do
    {
        u2VlanId = u2NextVlanId;

        /* Getting Fdb Id for the Active Vlan Id. This Function 
         * should not fail. So, Failure check is not done */
        VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId);

        if (VlanGetDot1qTpFdbPort (u4FdbId, Dot1dTpFdbAddress,
                                   pi4RetValDot1dTpFdbPort) == VLAN_SUCCESS)
        {
            /* Entry Found */
            bFlag = TRUE;
            break;
        }
    }
    while (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId)
           == VLAN_SUCCESS);

    if (bFlag == FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpFdbStatus
 Input       :  The Indices
                Dot1dTpFdbAddress

                The Object 
                retValDot1dTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpFdbStatus (tMacAddr Dot1dTpFdbAddress,
                        INT4 *pi4RetValDot1dTpFdbStatus)
{
    UINT4               u4FdbId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NextVlanId = 0;

    BOOL1               bFlag = FALSE;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId) == VLAN_FAILURE)
    {
        /* No Active Vlans Found */
        return SNMP_FAILURE;
    }

    do
    {
        u2VlanId = u2NextVlanId;

        /* Getting Fdb Id for the Active Vlan Id. This Function 
         * should not fail. So, Failure check is not done */
        VlanGetFdbIdFromVlanId (u2VlanId, &u4FdbId);

        if (VlanGetDot1qTpFdbStatus (u4FdbId, Dot1dTpFdbAddress,
                                     pi4RetValDot1dTpFdbStatus) == VLAN_SUCCESS)
        {
            /* Entry Found */
            bFlag = TRUE;
            break;
        }
    }
    while (VlanL2IwfGetNextActiveVlan (u2VlanId, &u2NextVlanId)
           == VLAN_SUCCESS);

    if (bFlag == FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dTpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dTpPortTable (INT4 i4Dot1dTpPort)
{
    return (nmhValidateIndexInstanceDot1dBasePortTable (i4Dot1dTpPort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1dTpPortTable (INT4 *pi4Dot1dTpPort)
{
    return (nmhGetFirstIndexDot1dBasePortTable (pi4Dot1dTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpPortTable
 Input       :  The Indices
                Dot1dTpPort
                nextDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dTpPortTable (INT4 i4Dot1dTpPort, INT4 *pi4NextDot1dTpPort)
{
    return (nmhGetNextIndexDot1dBasePortTable (i4Dot1dTpPort,
                                               pi4NextDot1dTpPort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dTpPortMaxInfo
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortMaxInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortMaxInfo (INT4 i4Dot1dTpPort, INT4 *pi4RetValDot1dTpPortMaxInfo)
{
    tCfaIfInfo          IfInfo;

    VlanCfaGetIfInfo (i4Dot1dTpPort, &IfInfo);
    *pi4RetValDot1dTpPortMaxInfo = IfInfo.u4IfMtu;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortInFrames (INT4 i4Dot1dTpPort,
                           UINT4 *pu4RetValDot1dTpPortInFrames)
{
#ifdef NPAPI_WANTED
/* Get the number of data frames received */
    if (CfaFsHwGetStat (i4Dot1dTpPort, NP_STAT_DOT1D_TP_PORT_IN_FRAMES,
                        pu4RetValDot1dTpPortInFrames) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else
    UNUSED_PARAM (i4Dot1dTpPort);

    *pu4RetValDot1dTpPortInFrames = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortOutFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortOutFrames (INT4 i4Dot1dTpPort,
                            UINT4 *pu4RetValDot1dTpPortOutFrames)
{
#ifdef NPAPI_WANTED
/* Get the number of data frames  transmitted */
    if (CfaFsHwGetStat (i4Dot1dTpPort, NP_STAT_DOT1D_TP_PORT_OUT_FRAMES,
                        pu4RetValDot1dTpPortOutFrames) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else
    UNUSED_PARAM (i4Dot1dTpPort);

    *pu4RetValDot1dTpPortOutFrames = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortInDiscards (INT4 i4Dot1dTpPort,
                             UINT4 *pu4RetValDot1dTpPortInDiscards)
{
    INT4                i4RetVal;

    i4RetVal = VlanGetDot1dStats (i4Dot1dTpPort,
                                  VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,
                                  pu4RetValDot1dTpPortInDiscards);

    if (i4RetVal == VLAN_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : Dot1dStaticTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dStaticTable (tMacAddr Dot1dStaticAddress,
                                          INT4 i4Dot1dStaticReceivePort)
{
    UINT4               u4FidIndex = VLAN_INIT_VAL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry (VLAN_DEF_VLAN_ID);
    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (VLAN_DEF_VLAN_ID);
    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        return SNMP_FAILURE;
    }

    /* Check for Static Unicast entry */
    pStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex,
                                                             Dot1dStaticAddress,
                                                             (UINT2)
                                                             i4Dot1dStaticReceivePort);

    if (pStUcastEntry == NULL)
    {
        /* Check for Static Multicast entry */
        pStMcastEntry =
            VlanGetStMcastEntryWithExactRcvPort (Dot1dStaticAddress,
                                                 (UINT2)
                                                 i4Dot1dStaticReceivePort,
                                                 pVlanEntry);

        if (pStMcastEntry == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1dStaticTable (tMacAddr * pDot1dStaticAddress,
                                  INT4 *pi4Dot1dStaticReceivePort)
{
    UINT4               u4FdbId = VLAN_INIT_VAL;
    if (VLAN_CURR_CONTEXT_PTR () != NULL)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            u4FdbId = VLAN_DEF_VLAN_ID;

            if (VlanGetFirstIndexDot1qStaticUnicastTable (&u4FdbId,
                                                          pDot1dStaticAddress,
                                                          pi4Dot1dStaticReceivePort)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }

            if (VlanGetFirstIndexDot1qStaticMulticastTable (&u4FdbId,
                                                            pDot1dStaticAddress,
                                                            pi4Dot1dStaticReceivePort)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                nextDot1dStaticAddress
                Dot1dStaticReceivePort
                nextDot1dStaticReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dStaticTable (tMacAddr Dot1dStaticAddress,
                                 tMacAddr * pNextDot1dStaticAddress,
                                 INT4 i4Dot1dStaticReceivePort,
                                 INT4 *pi4NextDot1dStaticReceivePort)
{

    UINT4               u4Dot1qFdbId = VLAN_INIT_VAL;
    UINT4               u4NextDot1dFdbId = VLAN_INIT_VAL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        u4Dot1qFdbId = VLAN_DEF_VLAN_ID;
        if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) != VLAN_TRUE)
        {
            if (VlanGetNextIndexDot1qStaticUnicastTable
                (u4Dot1qFdbId,
                 &u4NextDot1dFdbId,
                 Dot1dStaticAddress,
                 pNextDot1dStaticAddress,
                 i4Dot1dStaticReceivePort,
                 pi4NextDot1dStaticReceivePort) == VLAN_FAILURE)
            {
                if (VlanGetNextIndexDot1qStaticMulticastTable (u4Dot1qFdbId,
                                                               &u4NextDot1dFdbId,
                                                               Dot1dStaticAddress,
                                                               pNextDot1dStaticAddress,
                                                               i4Dot1dStaticReceivePort,
                                                               pi4NextDot1dStaticReceivePort)
                    == VLAN_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }
        else
        {
            if (VlanGetNextIndexDot1qStaticMulticastTable (u4Dot1qFdbId,
                                                           &u4NextDot1dFdbId,
                                                           Dot1dStaticAddress,
                                                           pNextDot1dStaticAddress,
                                                           i4Dot1dStaticReceivePort,
                                                           pi4NextDot1dStaticReceivePort)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dStaticAllowedToGoTo
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                retValDot1dStaticAllowedToGoTo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStaticAllowedToGoTo (tMacAddr Dot1dStaticAddress,
                                INT4 i4Dot1dStaticReceivePort,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1dStaticAllowedToGoTo)
{
    UINT4               u4Dot1qFdbId = VLAN_INIT_VAL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        u4Dot1qFdbId = VLAN_DEF_VLAN_ID;

        if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) == VLAN_TRUE)
        {
            if (VlanGetStaticMcastEgressPorts (u4Dot1qFdbId,
                                               Dot1dStaticAddress,
                                               i4Dot1dStaticReceivePort,
                                               pRetValDot1dStaticAllowedToGoTo)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else
        {
            if (VlanGetStaticUcastAllowedToGoTo (u4Dot1qFdbId,
                                                 Dot1dStaticAddress,
                                                 i4Dot1dStaticReceivePort,
                                                 pRetValDot1dStaticAllowedToGoTo)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1dStaticStatus
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                retValDot1dStaticStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStaticStatus (tMacAddr Dot1dStaticAddress,
                         INT4 i4Dot1dStaticReceivePort,
                         INT4 *pi4RetValDot1dStaticStatus)
{
    UINT4               u4Dot1qFdbId = VLAN_INIT_VAL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        u4Dot1qFdbId = VLAN_DEF_VLAN_ID;

        if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) == VLAN_TRUE)
        {
            if (VlanGetStaticMulticastStatus (u4Dot1qFdbId,
                                              Dot1dStaticAddress,
                                              i4Dot1dStaticReceivePort,
                                              pi4RetValDot1dStaticStatus)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else
        {
            if (VlanGetStaticUnicastStatus (u4Dot1qFdbId,
                                            Dot1dStaticAddress,
                                            i4Dot1dStaticReceivePort,
                                            pi4RetValDot1dStaticStatus)
                == VLAN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1dStaticAllowedToGoTo
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                setValDot1dStaticAllowedToGoTo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStaticAllowedToGoTo (tMacAddr Dot1dStaticAddress,
                                INT4 i4Dot1dStaticReceivePort,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValDot1dStaticAllowedToGoTo)
{
    UINT4               u4Dot1qFdbId = VLAN_DEF_VLAN_ID;

    if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) == VLAN_TRUE)
    {
        if (VlanSetStaticMcastEgressPorts (u4Dot1qFdbId,
                                           Dot1dStaticAddress,
                                           (UINT4) i4Dot1dStaticReceivePort,
                                           pSetValDot1dStaticAllowedToGoTo)
            == VLAN_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (VlanSetStaticUcastAllowedToGoTo (u4Dot1qFdbId,
                                             Dot1dStaticAddress,
                                             (UINT4) i4Dot1dStaticReceivePort,
                                             pSetValDot1dStaticAllowedToGoTo)
            == VLAN_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dStaticStatus
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                setValDot1dStaticStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStaticStatus (tMacAddr Dot1dStaticAddress,
                         INT4 i4Dot1dStaticReceivePort,
                         INT4 i4SetValDot1dStaticStatus)
{
    UINT4               u4Dot1qFdbId = VLAN_DEF_VLAN_ID;

    if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) != VLAN_TRUE)
    {
        if (VlanSetStaticUnicastStatus (u4Dot1qFdbId,
                                        Dot1dStaticAddress,
                                        i4Dot1dStaticReceivePort,
                                        i4SetValDot1dStaticStatus)
            == VLAN_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (VlanSetStaticMulticastStatus (u4Dot1qFdbId,
                                          Dot1dStaticAddress,
                                          i4Dot1dStaticReceivePort,
                                          i4SetValDot1dStaticStatus)
            == VLAN_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dStaticAllowedToGoTo
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                testValDot1dStaticAllowedToGoTo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStaticAllowedToGoTo (UINT4 *pu4ErrorCode,
                                   tMacAddr Dot1dStaticAddress,
                                   INT4 i4Dot1dStaticReceivePort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValDot1dStaticAllowedToGoTo)
{
    UINT4               u4FdbId = VLAN_INIT_VAL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_DISABLED);
        return SNMP_FAILURE;
    }

    u4FdbId = VLAN_DEF_VLAN_ID;

    if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) == VLAN_TRUE)
    {
        if (VlanTestStaticMcastEgressPorts (pu4ErrorCode, u4FdbId,
                                            Dot1dStaticAddress,
                                            i4Dot1dStaticReceivePort,
                                            pTestValDot1dStaticAllowedToGoTo)
            == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (VlanTestStaticUcastAllowedToGoTo (pu4ErrorCode, u4FdbId,
                                              Dot1dStaticAddress,
                                              i4Dot1dStaticReceivePort,
                                              pTestValDot1dStaticAllowedToGoTo)
            == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStaticStatus
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                testValDot1dStaticStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStaticStatus (UINT4 *pu4ErrorCode, tMacAddr Dot1dStaticAddress,
                            INT4 i4Dot1dStaticReceivePort,
                            INT4 i4TestValDot1dStaticStatus)
{
    UINT4               u4Dot1qFdbId = VLAN_INIT_VAL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VLAN_BASE_BRIDGE_DISABLED);
        return SNMP_FAILURE;
    }

    u4Dot1qFdbId = VLAN_DEF_VLAN_ID;

    if (VLAN_IS_MCASTADDR (Dot1dStaticAddress) != VLAN_TRUE)
    {
        if (VlanTestStaticUnicastStatus (pu4ErrorCode,
                                         u4Dot1qFdbId,
                                         Dot1dStaticAddress,
                                         i4Dot1dStaticReceivePort,
                                         i4TestValDot1dStaticStatus)
            == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (VlanTestStaticMulticastStatus (pu4ErrorCode,
                                           u4Dot1qFdbId,
                                           Dot1dStaticAddress,
                                           i4Dot1dStaticReceivePort,
                                           i4TestValDot1dStaticStatus)
            == VLAN_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStaticTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
