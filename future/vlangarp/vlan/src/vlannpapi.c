/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: vlannpapi.c,v 1.21 2016/03/05 12:02:07 siva Exp $
 *
 * Description:This file contains the wrapper for
 *             for Hardware API's w.r.t VLAN
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __VLANNPAPI_C__
#define __VLANNPAPI_C__

#include  "vlaninc.h"
#ifdef MEF_WANTED
#include  "evcnp.h"
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwInit (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwInit;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDeInit (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDeInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeInit;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetAllGroupsPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetAllGroupsPorts
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetAllGroupsPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tHwPortArray * pHwAllGroupPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetAllGroupsPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetAllGroupsPorts;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwAllGroupPorts = pHwAllGroupPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwEvbConfigSChIface                   
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwEvbConfigSChIface
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwEvbConfigSChIface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwEvbConfigSChIface *pEntry = NULL;
    UINT1                u1RetVal = FNP_FAILURE;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwEvbConfigSChIface;

    MEMCPY(&pEntry->VlanEvbHwConfigInfo, pVlanEvbHwConfigInfo,
                sizeof(tVlanEvbHwConfigInfo));

    u1RetVal = NpUtilHwProgram (&FsHwNp); 

    if (u1RetVal == FNP_SUCCESS)
    {
        pVlanEvbHwConfigInfo->u4VpHwIfIndex = 
                    pEntry->VlanEvbHwConfigInfo.u4VpHwIfIndex;
        pVlanEvbHwConfigInfo->i4RxFilterEntry =
                    pEntry->VlanEvbHwConfigInfo.i4RxFilterEntry;
    }
    return u1RetVal;
}

/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanHwSetBridgePortType
 *
 *    Description         : This function is called when Bridge Port type
 *                          is changed to UAP.
 *
 *    Input(s)            : pVlanHwPortInfo       - Vlan EVB hardware
 *                                                 configuration information
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS/FAILURE 
 *
 *****************************************************************************/


UINT1
VlanFsMiVlanHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo)
{
    tFsHwNp FsHwNp;
    tVlanNpModInfo *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetBridgePortType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,     /* Generic Np Structure */
                         NP_VLAN_MOD,   /* Module Id */
                         FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE,
                         0,        /* IfIndex Value If Applicable */
                         0,        /* No of Port Params */
                         0);        /* No of portList Params */
   pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
   pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBridgePortType;
    
   MEMCPY(&pEntry->VlanHwPortInfo, pVlanHwPortInfo,
           sizeof(tVlanHwPortInfo));
   return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetAllGroupsPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetAllGroupsPorts
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetAllGroupsPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tHwPortArray * pHwResetAllGroupPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetAllGroupsPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetAllGroupsPorts;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwResetAllGroupPorts = pHwResetAllGroupPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetUnRegGroupsPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetUnRegGroupsPorts
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetUnRegGroupsPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                     tHwPortArray * pHwResetUnRegGroupPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetUnRegGroupsPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetUnRegGroupsPorts;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwResetUnRegGroupPorts = pHwResetUnRegGroupPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetUnRegGroupsPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetUnRegGroupsPorts
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetUnRegGroupsPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tHwPortArray * pHwUnRegPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetUnRegGroupsPorts;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwUnRegPorts = pHwUnRegPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddStaticUcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddStaticUcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddStaticUcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4Port,
                                   tHwPortArray * pHwAllowedToGoPorts,
                                   UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddStaticUcastEntry *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;
    pEntry->pHwAllowedToGoPorts = pHwAllowedToGoPorts;
    pEntry->u1Status = u1Status;

#ifdef L2RED_WANTED
    VlanRedHwUpdateStUcastSync (u4ContextId, u4Fid, MacAddr,
                                u4Port, VLAN_RED_SYNC_HW_NOT_UPDATED, VLAN_ADD);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateStUcastSync (u4ContextId, u4Fid, MacAddr,
                                u4Port, VLAN_RED_SYNC_HW_UPDATED, VLAN_ADD);
#endif /* L2RED_WANTED */
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelStaticUcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelStaticUcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelStaticUcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4Port)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelStaticUcastEntry *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelStaticUcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;

#ifdef L2RED_WANTED
    VlanRedHwUpdateStUcastSync (u4ContextId, u4Fid, MacAddr,
                                u4Port, VLAN_RED_SYNC_HW_NOT_UPDATED,
                                VLAN_DELETE);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

#ifdef L2RED_WANTED
    VlanRedHwUpdateStUcastSync (u4ContextId, u4Fid, MacAddr,
                                u4Port, VLAN_RED_SYNC_HW_UPDATED, VLAN_DELETE);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetFdbEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetFdbEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetFdbEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
                           tHwUnicastMacEntry * pHwUnicastMacEntry)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetFdbEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_FDB_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->MacAddr = MacAddr;
    pEntry->pEntry = pHwUnicastMacEntry;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifndef SW_LEARNING

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetFdbCount                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetFdbCount
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetFdbCount
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetFdbCount (UINT4 u4ContextId, UINT4 u4FdbId, UINT4 *pu4Count)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetFdbCount *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_FDB_COUNT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbCount;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->pu4Count = pu4Count;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetFirstTpFdbEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetFirstTpFdbEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetFirstTpFdbEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetFirstTpFdbEntry (UINT4 u4ContextId, UINT4 *pu4FdbId,
                                  tMacAddr MacAddr)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetFirstTpFdbEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_FIRST_TP_FDB_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFirstTpFdbEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pu4FdbId = pu4FdbId;
    pEntry->MacAddr = MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetNextTpFdbEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetNextTpFdbEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetNextTpFdbEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetNextTpFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId,
                                 tMacAddr MacAddr, UINT4 *pu4NextContextId,
                                 UINT4 *pu4NextFdbId, tMacAddr NextMacAddr)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetNextTpFdbEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_NEXT_TP_FDB_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextTpFdbEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->MacAddr = MacAddr;
    pEntry->pu4NextContextId = pu4NextContextId;
    pEntry->pu4NextFdbId = pu4NextFdbId;
    pEntry->NextMacAddr = NextMacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* SW_LEARNING */

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, tHwPortArray * pHwMcastPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddMcastEntry *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->pHwMcastPorts = pHwMcastPorts;
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    /* Set the Multicast Index to be programmed in Hardware */
    if (VlanHwSetMcastIndex (MacAddr, VlanId) == VLAN_FAILURE)
    {
        return (FNP_FAILURE);

    }
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */


    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddStMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddStMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddStMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, INT4 i4RcvPort,
                               tHwPortArray * pHwMcastPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddStMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->i4RcvPort = i4RcvPort;
    pEntry->pHwMcastPorts = pHwMcastPorts;
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              i4RcvPort, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    /* Set the Multicast Index to be programmed in Hardware */
    if (VlanHwSetMcastIndex (MacAddr, VlanId) == FNP_FAILURE)
    {
        return (FNP_FAILURE);

    }

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              i4RcvPort, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetMcastPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetMcastPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetMcastPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetMcastPort (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                            UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetMcastPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_MCAST_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4IfIndex = u4IfIndex;

#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetMcastPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetMcastPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetMcastPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                              tMacAddr MacAddr, UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetMcastPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_MCAST_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetMcastPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4IfIndex = u4IfIndex;

#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelMcastEntry *pEntry = NULL;
   
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              0, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */


    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelStMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelStMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelStMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, INT4 i4RcvPort)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelStMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelStMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->i4RcvPort = i4RcvPort;

#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              i4RcvPort, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateMcastSync (u4ContextId, VlanId, MacAddr,
                              i4RcvPort, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                            tHwPortArray * pHwEgressPorts,
                            tHwPortArray * pHwUnTagPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwEgressPorts = pHwEgressPorts;
    pEntry->pHwUnTagPorts = pHwUnTagPorts;

#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelVlanEntry (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetVlanMemberPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetVlanMemberPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetVlanMemberPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                 UINT4 u4IfIndex, UINT1 u1IsTagged)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetVlanMemberPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanMemberPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1IsTagged = u1IsTagged;

#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetVlanMemberPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetVlanMemberPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetVlanMemberPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                   UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetVlanMemberPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanMemberPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u4IfIndex = u4IfIndex;

#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedSyncHwUpdate (u4ContextId, VlanId, VLAN_RED_SYNC_HW_UPDATED);
#endif /* L2RED_WANTED */

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortPvid                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortPvid
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortPvid
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortPvid *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_PVID,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPvid;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetDefaultVlanId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetDefaultVlanId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetDefaultVlanId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetDefaultVlanId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefaultVlanId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef L2RED_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSyncDefaultVlanId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSyncDefaultVlanId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSyncDefaultVlanId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSyncDefaultVlanId (UINT4 u4ContextId, tVlanId SwVlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSyncDefaultVlanId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SYNC_DEFAULT_VLAN_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSyncDefaultVlanId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->SwVlanId = SwVlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanRedHwUpdateDBForDefaultVlanId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanRedHwUpdateDBForDefaultVlanId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanRedHwUpdateDBForDefaultVlanId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanRedHwUpdateDBForDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_RED_HW_UPDATE_D_B_FOR_DEFAULT_VLAN_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanRedHwUpdateDBForDefaultVlanId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwScanProtocolVlanTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwScanProtocolVlanTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwScanProtocolVlanTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwScanProtocolVlanTbl (UINT4 u4ContextId, UINT4 u4Port,
                                   FsMiVlanHwProtoCb ProtoVlanCallBack)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwScanProtocolVlanTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SCAN_PROTOCOL_VLAN_TBL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwScanProtocolVlanTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->ProtoVlanCallBack = ProtoVlanCallBack;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetVlanProtocolMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetVlanProtocolMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetVlanProtocolMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT4 u4GroupId,
                                  tVlanProtoTemplate * pProtoTemplate,
                                  tVlanId * pVlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetVlanProtocolMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_VLAN_PROTOCOL_MAP,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanProtocolMap;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4GroupId = u4GroupId;
    pEntry->pProtoTemplate = pProtoTemplate;
    pEntry->pVlanId = pVlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwScanMulticastTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwScanMulticastTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwScanMulticastTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwScanMulticastTbl (UINT4 u4ContextId,
                                FsMiVlanHwMcastCb McastCallBack)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwScanMulticastTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SCAN_MULTICAST_TBL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwScanMulticastTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->McastCallBack = McastCallBack;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetStMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetStMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetStMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, UINT4 u4RcvPort,
                               tHwPortArray * pHwMcastPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetStMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->pHwMcastPorts = pHwMcastPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwScanUnicastTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwScanUnicastTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwScanUnicastTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwScanUnicastTbl (UINT4 u4ContextId,
                              FsMiVlanHwUcastCb UcastCallBack)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwScanUnicastTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SCAN_UNICAST_TBL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwScanUnicastTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->UcastCallBack = UcastCallBack;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetStaticUcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetStaticUcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetStaticUcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4Port,
                                   tHwPortArray * pAllowedToGoPorts,
                                   UINT1 *pu1Status)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetStaticUcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;
    pEntry->pAllowedToGoPorts = pAllowedToGoPorts;
    pEntry->pu1Status = pu1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetSyncedTnlProtocolMacAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetSyncedTnlProtocolMacAddr
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetSyncedTnlProtocolMacAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetSyncedTnlProtocolMacAddr (UINT4 u4ContextId, UINT2 u2Protocol,
                                           tMacAddr MacAddr)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_SYNCED_TNL_PROTOCOL_MAC_ADDR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetSyncedTnlProtocolMacAddr;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2Protocol = u2Protocol;
    pEntry->MacAddr = MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* L2RED_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortAccFrameType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortAccFrameType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortAccFrameType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   INT1 u1AccFrameType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortAccFrameType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortAccFrameType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1AccFrameType = u1AccFrameType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortIngFiltering                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortIngFiltering
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortIngFiltering
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT1 u1IngFilterEnable)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortIngFiltering *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_ING_FILTERING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngFiltering;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1IngFilterEnable = u1IngFilterEnable;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwVlanEnable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwVlanEnable
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwVlanEnable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwVlanEnable (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwVlanEnable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_VLAN_ENABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwVlanEnable;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwVlanDisable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwVlanDisable
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwVlanDisable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwVlanDisable (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwVlanDisable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_VLAN_DISABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwVlanDisable;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetVlanLearningType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetVlanLearningType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetVlanLearningType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearningType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetVlanLearningType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanLearningType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1LearningType = u1LearningType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetMacBasedStatusOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetMacBasedStatusOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetMacBasedStatusOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetMacBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       UINT1 u1MacBasedVlanEnable)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetMacBasedStatusOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMacBasedStatusOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1MacBasedVlanEnable = u1MacBasedVlanEnable;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetSubnetBasedStatusOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetSubnetBasedStatusOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetSubnetBasedStatusOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1SubnetBasedVlanEnable)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetSubnetBasedStatusOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1SubnetBasedVlanEnable = u1SubnetBasedVlanEnable;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwEnableProtoVlanOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwEnableProtoVlanOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwEnableProtoVlanOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1VlanProtoEnable)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwEnableProtoVlanOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwEnableProtoVlanOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1VlanProtoEnable = u1VlanProtoEnable;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetDefUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetDefUserPriority
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetDefUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  INT4 i4DefPriority)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetDefUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefUserPriority;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4DefPriority = i4DefPriority;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortNumTrafClasses                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortNumTrafClasses
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortNumTrafClasses
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     INT4 i4NumTraffClass)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortNumTrafClasses *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortNumTrafClasses;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4NumTraffClass = i4NumTraffClass;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetRegenUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetRegenUserPriority
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetRegenUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetRegenUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    INT4 i4UserPriority, INT4 i4RegenPriority)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetRegenUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetRegenUserPriority;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4UserPriority = i4UserPriority;
    pEntry->i4RegenPriority = i4RegenPriority;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetTraffClassMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetTraffClassMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetTraffClassMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetTraffClassMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                                INT4 i4UserPriority, INT4 i4TraffClass)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetTraffClassMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTraffClassMap;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4UserPriority = i4UserPriority;
    pEntry->i4TraffClass = i4TraffClass;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGmrpEnable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGmrpEnable
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGmrpEnable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGmrpEnable (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGmrpEnable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GMRP_ENABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGmrpEnable;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGmrpDisable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGmrpDisable
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGmrpDisable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGmrpDisable (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGmrpDisable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GMRP_DISABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGmrpDisable;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGvrpEnable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGvrpEnable
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGvrpEnable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGvrpEnable (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGvrpEnable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GVRP_ENABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGvrpEnable;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGvrpDisable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGvrpDisable
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGvrpDisable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGvrpDisable (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGvrpDisable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GVRP_DISABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGvrpDisable;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiNpDeleteAllFdbEntries                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpDeleteAllFdbEntries
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpDeleteAllFdbEntries
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiNpDeleteAllFdbEntries (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiNpDeleteAllFdbEntries *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_NP_DELETE_ALL_FDB_ENTRIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiNpDeleteAllFdbEntries;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddVlanProtocolMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddVlanProtocolMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddVlanProtocolMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT4 u4GroupId,
                                  tVlanProtoTemplate * pProtoTemplate,
                                  tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddVlanProtocolMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanProtocolMap;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4GroupId = u4GroupId;
    pEntry->pProtoTemplate = pProtoTemplate;
    pEntry->VlanId = VlanId;

#ifdef L2RED_WANTED
    VlanRedHwUpdateProtoVlanSync (pProtoTemplate, u4ContextId, u4GroupId,
                                  u4IfIndex, VLAN_RED_SYNC_HW_NOT_UPDATED,
                                  VLAN_ADD);
#endif
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateProtoVlanSync (pProtoTemplate, u4ContextId, u4GroupId,
                                  u4IfIndex, VLAN_RED_SYNC_HW_UPDATED,
                                  VLAN_ADD);
#endif
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelVlanProtocolMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelVlanProtocolMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelVlanProtocolMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT4 u4GroupId,
                                  tVlanProtoTemplate * pProtoTemplate)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelVlanProtocolMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanProtocolMap;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4GroupId = u4GroupId;
    pEntry->pProtoTemplate = pProtoTemplate;

#ifdef L2RED_WANTED
    VlanRedHwUpdateProtoVlanSync (pProtoTemplate, u4ContextId, u4GroupId,
                                  u4IfIndex, VLAN_RED_SYNC_HW_NOT_UPDATED,
                                  VLAN_DELETE);
#endif
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateProtoVlanSync (pProtoTemplate, u4ContextId, u4GroupId,
                                  u4IfIndex, VLAN_RED_SYNC_HW_UPDATED,
                                  VLAN_DELETE);
#endif
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetPortStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetPortStats
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetPortStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetPortStats (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                            UINT1 u1StatsType, UINT4 *pu4PortStatsValue)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetPortStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_PORT_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->VlanId = VlanId;
    pEntry->u1StatsType = u1StatsType;
    pEntry->pu4PortStatsValue = pu4PortStatsValue;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetPortStats64                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetPortStats64
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetPortStats64
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetPortStats64 (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                              UINT1 u1StatsType, tSNMP_COUNTER64_TYPE * pValue)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetPortStats64 *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_PORT_STATS64,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats64;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->VlanId = VlanId;
    pEntry->u1StatsType = u1StatsType;
    pEntry->pValue = pValue;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetVlanStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetVlanStats
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetVlanStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetVlanStats (UINT4 u4ContextId, tVlanId VlanId,
                            UINT1 u1StatsType, UINT4 *pu4VlanStatsValue)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetVlanStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_VLAN_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanStats;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u1StatsType = u1StatsType;
    pEntry->pu4VlanStatsValue = pu4VlanStatsValue;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetVlanStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetVlanStats
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetVlanStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetVlanStats (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetVlanStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_VLAN_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanStats;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortTunnelMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortTunnelMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortTunnelMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortTunnelMode (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT4 u4Mode)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortTunnelMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortTunnelMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4Mode = u4Mode;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetTunnelFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetTunnelFilter
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetTunnelFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetTunnelFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_TUNNEL_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTunnelFilter;

    pEntry->u4ContextId = u4ContextId;
    pEntry->i4BridgeMode = i4BridgeMode;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwCheckTagAtEgress                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwCheckTagAtEgress
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwCheckTagAtEgress
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwCheckTagAtEgress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwCheckTagAtEgress *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_CHECK_TAG_AT_EGRESS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCheckTagAtEgress;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pu1TagSet = pu1TagSet;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwCheckTagAtIngress                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwCheckTagAtIngress
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwCheckTagAtIngress
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwCheckTagAtIngress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwCheckTagAtIngress *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_CHECK_TAG_AT_INGRESS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCheckTagAtIngress;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pu1TagSet = pu1TagSet;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwCreateFdbId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwCreateFdbId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwCreateFdbId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwCreateFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwCreateFdbId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_CREATE_FDB_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCreateFdbId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDeleteFdbId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDeleteFdbId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDeleteFdbId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDeleteFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDeleteFdbId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DELETE_FDB_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteFdbId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAssociateVlanFdb                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAssociateVlanFdb
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAssociateVlanFdb
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAssociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAssociateVlanFdb *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAssociateVlanFdb;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDisassociateVlanFdb                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDisassociateVlanFdb
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDisassociateVlanFdb
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDisassociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid,
                                   tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDisassociateVlanFdb *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDisassociateVlanFdb;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwFlushPortFdbId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwFlushPortFdbId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwFlushPortFdbId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwFlushPortFdbId (UINT4 u4ContextId, UINT4 u4Port, UINT4 u4Fid,
                              INT4 i4OptimizeFlag)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwFlushPortFdbId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->u4Fid = u4Fid;
    pEntry->i4OptimizeFlag = i4OptimizeFlag;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwFlushPortFdbList                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwFlushPortFdbList
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwFlushPortFdbList
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwFlushPortFdbList (UINT4 u4ContextId,
                                tVlanFlushInfo * pVlanFlushInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwFlushPortFdbList *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbList;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pVlanFlushInfo = pVlanFlushInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwFlushPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwFlushPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwFlushPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwFlushPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                         INT4 i4OptimizeFlag)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwFlushPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_FLUSH_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4OptimizeFlag = i4OptimizeFlag;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwFlushFdbId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwFlushFdbId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwFlushFdbId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwFlushFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwFlushFdbId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_FLUSH_FDB_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushFdbId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetShortAgeout                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetShortAgeout
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetShortAgeout
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetShortAgeout (UINT4 u4ContextId, UINT4 u4Port, INT4 i4AgingTime)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetShortAgeout *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_SHORT_AGEOUT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetShortAgeout;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->i4AgingTime = i4AgingTime;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetShortAgeout                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetShortAgeout
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetShortAgeout
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetShortAgeout (UINT4 u4ContextId, UINT4 u4Port,
                                INT4 i4LongAgeout)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetShortAgeout *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_SHORT_AGEOUT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetShortAgeout;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->i4LongAgeout = i4LongAgeout;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetVlanInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetVlanInfo
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetVlanInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                           tHwVlanPortArray * pHwEntry)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetVlanInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_VLAN_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanInfo;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwEntry = pHwEntry;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, tHwPortArray * pHwMcastPorts)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->pHwMcastPorts = pHwMcastPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwTraffClassMapInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwTraffClassMapInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwTraffClassMapInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority,
                                 INT4 i4CosqValue)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwTraffClassMapInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwTraffClassMapInit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1Priority = u1Priority;
    pEntry->i4CosqValue = i4CosqValue;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifndef BRIDGE_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiBrgSetAgingTime                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiBrgSetAgingTime
 *                                                                          
 *    Input(s)            : Arguments of FsMiBrgSetAgingTime
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiBrgSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiBrgSetAgingTime *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_BRG_SET_AGING_TIME,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiBrgSetAgingTime;

    pEntry->u4ContextId = u4ContextId;
    pEntry->i4AgingTime = i4AgingTime;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* BRIDGE_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetBrgMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetBrgMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetBrgMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetBrgMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_BRG_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBrgMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4BridgeMode = u4BridgeMode;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetBaseBridgeMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetBaseBridgeMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetBaseBridgeMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetBaseBridgeMode (UINT4 u4IfIndex, UINT4 u4Mode)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetBaseBridgeMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBaseBridgeMode;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4Mode = u4Mode;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddPortMacVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddPortMacVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddPortMacVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddPortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tMacAddr MacAddr, tVlanId VlanId,
                                   BOOL1 bSuppressOption)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddPortMacVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortMacVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->MacAddr = MacAddr;
    pEntry->VlanId = VlanId;
    pEntry->bSuppressOption = bSuppressOption;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDeletePortMacVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDeletePortMacVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDeletePortMacVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDeletePortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      tMacAddr MacAddr)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortMacVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->MacAddr = MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddPortSubnetVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddPortSubnetVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddPortSubnetVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT4 SubnetAddr, tVlanId VlanId,
                                      BOOL1 bARPOption)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortSubnetVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SubnetAddr = SubnetAddr;
    pEntry->VlanId = VlanId;
    pEntry->bARPOption = bARPOption;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDeletePortSubnetVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDeletePortSubnetVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDeletePortSubnetVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDeletePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT4 SubnetAddr)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortSubnetVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SubnetAddr = SubnetAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwUpdatePortSubnetVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwUpdatePortSubnetVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwUpdatePortSubnetVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwUpdatePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT4 u4SubnetAddr, UINT4 u4SubnetMask,
                                         tVlanId VlanId, BOOL1 bARPOption,
                                         UINT1 u1Action)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwUpdatePortSubnetVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4SubnetAddr = u4SubnetAddr;
    pEntry->u4SubnetMask = u4SubnetMask;
    pEntry->VlanId = VlanId;
    pEntry->bARPOption = bARPOption;
    pEntry->u1Action = u1Action;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanNpHwRunMacAgeing                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanNpHwRunMacAgeing
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanNpHwRunMacAgeing
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanNpHwRunMacAgeing (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanNpHwRunMacAgeing *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_NP_HW_RUN_MAC_AGEING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanNpHwRunMacAgeing;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwMacLearningLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwMacLearningLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwMacLearningLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                                UINT2 u2FdbId, UINT4 u4MacLimit)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwMacLearningLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_MAC_LEARNING_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u2FdbId = u2FdbId;
    pEntry->u4MacLimit = u4MacLimit;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSwitchMacLearningLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSwitchMacLearningLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSwitchMacLearningLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSwitchMacLearningLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSwitchMacLearningLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4MacLimit = u4MacLimit;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwMacLearningStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwMacLearningStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwMacLearningStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId,
                                 UINT2 u2FdbId, tHwPortArray * pHwEgressPorts,
                                 UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwMacLearningStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_MAC_LEARNING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u2FdbId = u2FdbId;
    pEntry->pHwEgressPorts = pHwEgressPorts;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetFidPortLearningStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetFidPortLearningStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetFidPortLearningStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                        tHwPortArray HwPortList, UINT4 u4Port,
                                        UINT1 u1Action)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetFidPortLearningStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetFidPortLearningStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    MEMCPY (&(pEntry->HwPortList), &HwPortList, sizeof (tHwPortArray));
    pEntry->u4Port = u4Port;
    pEntry->u1Action = u1Action;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetProtocolTunnelStatusOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetProtocolTunnelStatusOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetProtocolTunnelStatusOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             tVlanHwTunnelFilters ProtocolId,
                                             UINT4 u4TunnelStatus)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetProtocolTunnelStatusOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    MEMCPY (&(pEntry->ProtocolId), &ProtocolId, sizeof (tVlanHwTunnelFilters));
    pEntry->u4TunnelStatus = u4TunnelStatus;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortProtectedStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortProtectedStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortProtectedStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      INT4 i4ProtectedStatus)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortProtectedStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProtectedStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4ProtectedStatus = i4ProtectedStatus;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddStaticUcastEntryEx                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddStaticUcastEntryEx
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddStaticUcastEntryEx
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                     tMacAddr MacAddr, UINT4 u4Port,
                                     tHwPortArray * pHwAllowedToGoPorts,
                                     UINT1 u1Status, tMacAddr ConnectionId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddStaticUcastEntryEx *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntryEx;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;
    pEntry->pHwAllowedToGoPorts = pHwAllowedToGoPorts;
    pEntry->u1Status = u1Status;
    pEntry->ConnectionId = ConnectionId;
#ifdef L2RED_WANTED
    VlanRedHwUpdateStUcastSync (u4ContextId, u4Fid, MacAddr,
                                u4Port, VLAN_RED_SYNC_HW_NOT_UPDATED, VLAN_ADD);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#ifdef L2RED_WANTED
    VlanRedHwUpdateStUcastSync (u4ContextId, u4Fid, MacAddr,
                                u4Port, VLAN_RED_SYNC_HW_UPDATED, VLAN_ADD);
#endif /* L2RED_WANTED */
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetStaticUcastEntryEx                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetStaticUcastEntryEx
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetStaticUcastEntryEx
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                     tMacAddr MacAddr, UINT4 u4Port,
                                     tHwPortArray * pAllowedToGoPorts,
                                     UINT1 *pu1Status, tMacAddr ConnectionId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetStaticUcastEntryEx *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntryEx;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;
    pEntry->pAllowedToGoPorts = pAllowedToGoPorts;
    pEntry->pu1Status = pu1Status;
    pEntry->ConnectionId = ConnectionId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsVlanHwForwardPktOnPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVlanHwForwardPktOnPorts
 *                                                                          
 *    Input(s)            : Arguments of FsVlanHwForwardPktOnPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsVlanHwForwardPktOnPorts (UINT1 *pu1Packet, UINT2 u2PacketLen,
                               tHwVlanFwdInfo * pVlanFwdInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsVlanHwForwardPktOnPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_VLAN_HW_FORWARD_PKT_ON_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsVlanHwForwardPktOnPorts;

    pEntry->pu1Packet = pu1Packet;
    pEntry->u2PacketLen = u2PacketLen;
    pEntry->pVlanFwdInfo = pVlanFwdInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwPortMacLearningStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwPortMacLearningStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwPortMacLearningStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwPortMacLearningStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsVlanHwGetMacLearningMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVlanHwGetMacLearningMode
 *                                                                          
 *    Input(s)            : Arguments of FsVlanHwGetMacLearningMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsVlanHwGetMacLearningMode (UINT4 *pu4LearningMode)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsVlanHwGetMacLearningMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_VLAN_HW_GET_MAC_LEARNING_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsVlanHwGetMacLearningMode;

    pEntry->pu4LearningMode = pu4LearningMode;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetMcastIndex                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetMcastIndex
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetMcastIndex
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetMcastIndex (tHwMcastIndexInfo HwMcastIndexInfo,
                             UINT4 *pu4McastIndex)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetMcastIndex *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_MCAST_INDEX,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastIndex;
    MEMCPY (&(pEntry->HwMcastIndexInfo), &HwMcastIndexInfo,
            sizeof (tHwMcastIndexInfo));
    pEntry->pu4McastIndex = pu4McastIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortIngressEtherType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortIngressEtherType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortIngressEtherType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       UINT2 u2EtherType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortIngressEtherType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngressEtherType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2EtherType = u2EtherType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortEgressEtherType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortEgressEtherType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortEgressEtherType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT2 u2EtherType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortEgressEtherType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEgressEtherType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2EtherType = u2EtherType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanHwSetPortProperty
 *
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortPcpSelection
 *
 *    Input(s)            : Arguments of FsMiVlanHwSetPbPortProperty
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tHwVlanPortProperty VlanPortProperty)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortProperty *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_PROPERTY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProperty;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanPortProperty = VlanPortProperty;

    return (NpUtilHwProgram (&FsHwNp));
}


/***************************************************************************
*  Function Name       : VlanFsMiVlanHwPortUnicastMacSecType
*  Description         : This function using its arguments populates the
*                      generic NP structure and invokes NpUtilHwProgram
*                       The Generic NP wrapper after validating the H/W
*                      parameters invokes FsMiVlanHwPortMacLearningLimit
*  Input(s)            : Arguments of FsMiVlanHwPortMacLearningLimit
*  Output(s)           : None
*  Global Var Referred : None
*  Global Var Modified : None.
*  Use of Recursion    : None.
*  Exceptions or Operating
*  System Error Handling    : None.
*  Returns            : FNP_SUCCESS OR FNP_FAILURE
*******************************************************************************/                                             
UINT1
VlanFsMiVlanHwPortUnicastMacSecType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    INT4 i4MacSecType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwPortUnicastMacSecType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_PORT_UNICAST_MAC_SEC_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortUnicastMacSecType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4MacSecType = i4MacSecType;

    return (NpUtilHwProgram (&FsHwNp));
}



#ifdef MBSM_WANTED
/***************************************************************************
*
*    Function Name       : VlanFsMiVlanMbsmHwSetPortProperty
*
*    Description         : This function using its arguments populates the
*                          generic NP structure and invokes NpUtilHwProgram
*                          The Generic NP wrapper after validating the H/W
*                          parameters invokes FsMiVlanMbsmHwSetPortProperty
*
*    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortProperty
*
*    Output(s)           : None
*
*    Global Var Referred : None
*
*    Global Var Modified : None.
*
*    Use of Recursion    : None.
*
*    Exceptions or Operating
*    System Error Handling    : None.
*
*    Returns            : FNP_SUCCESS OR FNP_FAILURE
*
*****************************************************************************/
UINT1
VlanFsMiVlanMbsmHwSetPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tHwVlanPortProperty VlanPortProperty,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortProperty *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortProperty;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanPortProperty = VlanPortProperty;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef PB_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetProviderBridgePortType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetProviderBridgePortType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetProviderBridgePortType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             UINT4 u4PortType,
                                             tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetProviderBridgePortType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetProviderBridgePortType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4PortType = u4PortType;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortIngressEtherType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortIngressEtherType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortIngressEtherType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                           UINT2 u2EtherType,
                                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortIngressEtherType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortIngressEtherType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2EtherType = u2EtherType;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortEgressEtherType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortEgressEtherType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortEgressEtherType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT2 u2EtherType,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortEgressEtherType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortEgressEtherType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2EtherType = u2EtherType;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortSVlanTranslationStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortSVlanTranslationStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortSVlanTranslationStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortSVlanTranslationStatus (UINT4 u4ContextId,
                                                 UINT4 u4IfIndex,
                                                 UINT1 u1Status,
                                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortSVlanTranslationStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortSVlanTranslationStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddSVlanTranslationEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddSVlanTranslationEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddSVlanTranslationEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                            UINT2 u2LocalSVlan,
                                            UINT2 u2RelaySVlan,
                                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddSVlanTranslationEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddSVlanTranslationEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalSVlan = u2LocalSVlan;
    pEntry->u2RelaySVlan = u2RelaySVlan;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortEtherTypeSwapStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortEtherTypeSwapStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortEtherTypeSwapStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId,
                                              UINT4 u4IfIndex, UINT1 u1Status,
                                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortEtherTypeSwapStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortEtherTypeSwapStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddEtherTypeSwapEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddEtherTypeSwapEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddEtherTypeSwapEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT2 u2LocalEtherType,
                                         UINT2 u2RelayEtherType,
                                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddEtherTypeSwapEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddEtherTypeSwapEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalEtherType = u2LocalEtherType;
    pEntry->u2RelayEtherType = u2RelayEtherType;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddSVlanMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddSVlanMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddSVlanMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddSVlanMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddSVlanMap;

    pEntry->u4ContextId = u4ContextId;
    MEMCPY (&(pEntry->VlanSVlanMap), &VlanSVlanMap, sizeof (tVlanSVlanMap));
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortSVlanClassifyMethod                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortSVlanClassifyMethod
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortSVlanClassifyMethod
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortSVlanClassifyMethod (UINT4 u4ContextId,
                                              UINT4 u4IfIndex,
                                              UINT1 u1TableType,
                                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortSVlanClassifyMethod *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortSVlanClassifyMethod;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1TableType = u1TableType;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwPortMacLearningLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwPortMacLearningLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwPortMacLearningLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT4 u4MacLimit,
                                        tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwPortMacLearningLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortMacLearningLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4MacLimit = u4MacLimit;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortCustomerVlan                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortCustomerVlan
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortCustomerVlan
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       tVlanId CVlanId,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortCustomerVlan *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortCustomerVlan;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->CVlanId = CVlanId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwCreateProviderEdgePort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwCreateProviderEdgePort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwCreateProviderEdgePort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          tVlanId SVlanId,
                                          tHwVlanPbPepInfo PepConfig,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwCreateProviderEdgePort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwCreateProviderEdgePort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    MEMCPY (&(pEntry->PepConfig), &PepConfig, sizeof (tHwVlanPbPepInfo));
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPcpEncodTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPcpEncodTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPcpEncodTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPcpEncodTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPcpEncodTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    MEMCPY (&(pEntry->NpPbVlanPcpInfo), &NpPbVlanPcpInfo,
            sizeof (tHwVlanPbPcpInfo));
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPcpDecodTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPcpDecodTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPcpDecodTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPcpDecodTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPcpDecodTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    MEMCPY (&(pEntry->NpPbVlanPcpInfo), &NpPbVlanPcpInfo,
            sizeof (tHwVlanPbPcpInfo));
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortUseDei                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortUseDei
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortUseDei
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1UseDei, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortUseDei *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortUseDei;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1UseDei = u1UseDei;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortReqDropEncoding                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortReqDropEncoding
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortReqDropEncoding
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1ReqDrpEncoding,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortReqDropEncoding *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortReqDropEncoding;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1ReqDrpEncoding = u1ReqDrpEncoding;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortPcpSelection                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortPcpSelection
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortPcpSelection
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       UINT2 u2PcpSelection,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortPcpSelection *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortPcpSelection;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2PcpSelection = u2PcpSelection;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetServicePriRegenEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetServicePriRegenEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetServicePriRegenEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                           tVlanId SVlanId, INT4 i4RecvPriority,
                                           INT4 i4RegenPriority,
                                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetServicePriRegenEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetServicePriRegenEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->i4RecvPriority = i4RecvPriority;
    pEntry->i4RegenPriority = i4RegenPriority;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwMulticastMacTableLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwMulticastMacTableLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwMulticastMacTableLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwMulticastMacTableLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwMulticastMacTableLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4MacLimit = u4MacLimit;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* PB_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwInit (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwInit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetBrgMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetBrgMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetBrgMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode,
                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetBrgMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_BRG_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBrgMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;
    pEntry->u4BridgeMode = u4BridgeMode;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetDefaultVlanId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetDefaultVlanId
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetDefaultVlanId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetDefaultVlanId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetDefaultVlanId;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddStaticUcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddStaticUcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddStaticUcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4FdbId,
                                       tMacAddr MacAddr, UINT4 u4RcvPort,
                                       tHwPortArray * pHwAllowedToGoPorts,
                                       UINT1 u1Status,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->pHwAllowedToGoPorts = pHwAllowedToGoPorts;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetUcastPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetUcastPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetUcastPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetUcastPort (UINT4 u4ContextId, UINT4 u4FdbId,
                                tMacAddr MacAddr, UINT4 u4RcvPort,
                                UINT4 u4AllowedToGoPort, UINT1 u1Status,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetUcastPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         2,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetUcastPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->u4AllowedToGoPort = u4AllowedToGoPort;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwResetUcastPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwResetUcastPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwResetUcastPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwResetUcastPort (UINT4 u4ContextId, UINT4 u4FdbId,
                                  tMacAddr MacAddr, UINT4 u4RcvPort,
                                  UINT4 u4AllowedToGoPort,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwResetUcastPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         2,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetUcastPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->u4AllowedToGoPort = u4AllowedToGoPort;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwDelStaticUcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwDelStaticUcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwDelStaticUcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwDelStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                       tMacAddr MacAddr, UINT4 u4RcvPort,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwDelStaticUcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwDelStaticUcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                                 tMacAddr MacAddr, tHwPortArray * pHwMcastPorts,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->pHwMcastPorts = pHwMcastPorts;
    pEntry->pSlotInfo = pSlotInfo;
    /* Set the Multicast Index to be programmed in Hardware */
    if (VlanHwSetMcastIndex (MacAddr, VlanId) == VLAN_FAILURE)
    {
        return (FNP_FAILURE);

    }

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetMcastPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetMcastPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetMcastPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                                tMacAddr MacAddr, UINT4 u4Port,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetMcastPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetMcastPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwResetMcastPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwResetMcastPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwResetMcastPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwResetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                                  tMacAddr MacAddr, UINT4 u4Port,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwResetMcastPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetMcastPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4Port = u4Port;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                                tHwPortArray * pHwEgressPorts,
                                tHwPortArray * pHwUnTagPorts,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwEgressPorts = pHwEgressPorts;
    pEntry->pHwUnTagPorts = pHwUnTagPorts;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetVlanMemberPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetVlanMemberPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetVlanMemberPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                     UINT4 u4Port, UINT1 u1IsTagged,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetVlanMemberPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetVlanMemberPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u4Port = u4Port;
    pEntry->u1IsTagged = u1IsTagged;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwResetVlanMemberPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwResetVlanMemberPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwResetVlanMemberPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                       UINT4 u4Port, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwResetVlanMemberPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetVlanMemberPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u4Port = u4Port;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortPvid                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortPvid
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortPvid
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortPvid (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortPvid *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_PVID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortPvid;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->VlanId = VlanId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortAccFrameType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortAccFrameType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortAccFrameType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortAccFrameType (UINT4 u4ContextId, UINT4 u4Port,
                                       UINT1 u1AccFrameType,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortAccFrameType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortAccFrameType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->u1AccFrameType = u1AccFrameType;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortIngFiltering                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortIngFiltering
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortIngFiltering
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortIngFiltering (UINT4 u4ContextId, UINT4 u4Port,
                                       UINT1 u1IngFilterEnable,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortIngFiltering *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortIngFiltering;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->u1IngFilterEnable = u1IngFilterEnable;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetDefUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetDefUserPriority
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetDefUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetDefUserPriority (UINT4 u4ContextId, UINT4 u4Port,
                                      INT4 i4DefPriority,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetDefUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetDefUserPriority;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->i4DefPriority = i4DefPriority;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwTraffClassMapInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwTraffClassMapInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwTraffClassMapInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority,
                                     INT4 i4CosqValue,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwTraffClassMapInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwTraffClassMapInit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1Priority = u1Priority;
    pEntry->i4CosqValue = i4CosqValue;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetTraffClassMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetTraffClassMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetTraffClassMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetTraffClassMap (UINT4 u4ContextId, UINT4 u4Port,
                                    INT4 i4UserPriority, INT4 i4TraffClass,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetTraffClassMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetTraffClassMap;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->i4UserPriority = i4UserPriority;
    pEntry->i4TraffClass = i4TraffClass;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddStMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddStMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddStMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                                   tMacAddr McastAddr, INT4 i4RcvPort,
                                   tHwPortArray * pHwMcastPorts,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddStMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->McastAddr = McastAddr;
    pEntry->i4RcvPort = i4RcvPort;
    pEntry->pHwMcastPorts = pHwMcastPorts;
    pEntry->pSlotInfo = pSlotInfo;

    /* Set the Multicast Index to be programmed in Hardware */
    if (VlanHwSetMcastIndex (McastAddr, VlanId) == VLAN_FAILURE)
    {
        return (FNP_FAILURE);

    }
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwDelStMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwDelStMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwDelStMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwDelStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                                   tMacAddr MacAddr, UINT4 u4RcvPort,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwDelStMcastEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwDelStMcastEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortNumTrafClasses                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortNumTrafClasses
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortNumTrafClasses
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT4 u4Port,
                                         INT4 i4NumTraffClass,
                                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortNumTrafClasses *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortNumTrafClasses;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->i4NumTraffClass = i4NumTraffClass;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetRegenUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetRegenUserPriority
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetRegenUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetRegenUserPriority (UINT4 u4ContextId, UINT4 u4Port,
                                        INT4 i4UserPriority,
                                        INT4 i4RegenPriority,
                                        tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetRegenUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetRegenUserPriority;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->i4UserPriority = i4UserPriority;
    pEntry->i4RegenPriority = i4RegenPriority;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetSubnetBasedStatusOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetSubnetBasedStatusOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetSubnetBasedStatusOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4Port,
                                              UINT1 u1VlanSubnetEnable,
                                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetSubnetBasedStatusOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetSubnetBasedStatusOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->u1VlanSubnetEnable = u1VlanSubnetEnable;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwEnableProtoVlanOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwEnableProtoVlanOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwEnableProtoVlanOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT4 u4Port,
                                         UINT1 u1VlanProtoEnable,
                                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwEnableProtoVlanOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEnableProtoVlanOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->u1VlanProtoEnable = u1VlanProtoEnable;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetAllGroupsPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetAllGroupsPorts
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetAllGroupsPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                     tHwPortArray * pHwAllGroupPorts,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetAllGroupsPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetAllGroupsPorts;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwAllGroupPorts = pHwAllGroupPorts;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanMbsmHwEvbConfigSChIface
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwEvbConfigSChIface
 *
 *    Input(s)            : Arguments of FsMiVlanMbsmHwEvbConfigSChIface
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEvbConfigSChIface;

    MEMCPY (&pEntry->VlanEvbHwConfigInfo, pVlanEvbHwConfigInfo,
            sizeof (tVlanEvbHwConfigInfo));
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanMbsmHwSetBridgePortType
 *
 *    Description         : This function is called when Bridge Port type
 *                          is changed to UAP.
 *
 *    Input(s)            : pVlanHwPortInfo - Vlan EVB hardware config Info
 *                          pSlotInfo - Slot Info
 *                          
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : VLAN_SUCCESS OR VLAN_FAILURE
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo ,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp FsHwNp;
    tVlanNpModInfo *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetBridgePortType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,     /* Generic Np Structure */
                         NP_VLAN_MOD,   /* Module Id */
                         FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE,
                         0,        /* IfIndex Value If Applicable */
                         0,        /* No of Port Params */
                         0);        /* No of portList Params */
   pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
   pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBridgePortType;

   MEMCPY (&pEntry->VlanHwPortInfo, pVlanHwPortInfo,
           sizeof (tVlanHwPortInfo));
   pEntry->pSlotInfo = pSlotInfo;

   return (NpUtilHwProgram (&FsHwNp));

}


/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetUnRegGroupsPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetUnRegGroupsPorts
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetUnRegGroupsPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                       tHwPortArray * pHwUnRegPorts,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetUnRegGroupsPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetUnRegGroupsPorts;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->pHwUnRegPorts = pHwUnRegPorts;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetTunnelFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetTunnelFilter
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetTunnelFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode,
                                   tMacAddr MacAddr, UINT2 u2ProtocolId,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetTunnelFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetTunnelFilter;

    pEntry->u4ContextId = u4ContextId;
    pEntry->i4BridgeMode = i4BridgeMode;
    pEntry->MacAddr = MacAddr;
    pEntry->u2ProtocolId = u2ProtocolId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddPortSubnetVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddPortSubnetVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddPortSubnetVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4Port,
                                          UINT4 u4SubnetAddr, tVlanId VlanId,
                                          UINT1 u1ArpOption,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddPortSubnetVlanEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddPortSubnetVlanEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Port = u4Port;
    pEntry->u4SubnetAddr = u4SubnetAddr;
    pEntry->VlanId = VlanId;
    pEntry->u1ArpOption = u1ArpOption;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifndef BRIDGE_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiBrgMbsmSetAgingTime                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiBrgMbsmSetAgingTime
 *                                                                          
 *    Input(s)            : Arguments of FsMiBrgMbsmSetAgingTime
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiBrgMbsmSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime,
                             tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiBrgMbsmSetAgingTime *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_BRG_MBSM_SET_AGING_TIME,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiBrgMbsmSetAgingTime;

    pEntry->u4ContextId = u4ContextId;
    pEntry->i4AgingTime = i4AgingTime;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* BRIDGE_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetFidPortLearningStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetFidPortLearningStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetFidPortLearningStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                            UINT4 u4Port, UINT1 u1Action,
                                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetFidPortLearningStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetFidPortLearningStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4Fid = u4Fid;
    pEntry->u4Port = u4Port;
    pEntry->u1Action = u1Action;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId,
                                                 UINT4 u4IfIndex,
                                                 tVlanHwTunnelFilters
                                                 ProtocolId,
                                                 UINT4 u4TunnelStatus,
                                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    MEMCPY (&(pEntry->ProtocolId), &ProtocolId, sizeof (tVlanHwTunnelFilters));;
    pEntry->u4TunnelStatus = u4TunnelStatus;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSetPortProtectedStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSetPortProtectedStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSetPortProtectedStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          INT4 u4ProtectedStatus,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSetPortProtectedStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortProtectedStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4ProtectedStatus = u4ProtectedStatus;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwSwitchMacLearningLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwSwitchMacLearningLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwSwitchMacLearningLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwSwitchMacLearningLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSwitchMacLearningLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4MacLimit = u4MacLimit;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwPortMacLearningStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwPortMacLearningStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwPortMacLearningStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT1 u1Status,
                                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwPortMacLearningStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortMacLearningStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwMacLearningLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwMacLearningLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwMacLearningLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                                    UINT2 u2FdbId, UINT4 u4MacLimit,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwMacLearningLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwMacLearningLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u2FdbId = u2FdbId;
    pEntry->u4MacLimit = u4MacLimit;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmHwAddStaticUcastEntryEx                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwAddStaticUcastEntryEx
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmHwAddStaticUcastEntryEx
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwAddStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4FdbId,
                                         tMacAddr MacAddr, UINT4 u4RcvPort,
                                         tHwPortArray * pHwAllowedToGoPorts,
                                         UINT1 u1Status, tMacAddr ConnectionId,
                                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntryEx *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntryEx;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4FdbId = u4FdbId;
    pEntry->MacAddr = MacAddr;
    pEntry->u4RcvPort = u4RcvPort;
    pEntry->pHwAllowedToGoPorts = pHwAllowedToGoPorts;
    pEntry->u1Status = u1Status;
    pEntry->ConnectionId = ConnectionId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanMbsmSyncFDBInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmSyncFDBInfo
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanMbsmSyncFDBInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmSyncFDBInfo (tFDBInfoArray * pFDBInfoArray,
                             tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmSyncFDBInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmSyncFDBInfo;

    pEntry->pFDBInfoArray = pFDBInfoArray;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}


/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanMbsmHwPortPktReflectStatus
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanMbsmHwPortPktReflectStatus
 *
 *    Input(s)            : Arguments of FsMiVlanMbsmHwPortPktReflectStatus
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanMbsmHwPortPktReflectStatus (tFsNpVlanPortReflectEntry *
                                        pPortReflectEntry)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanMbsmHwPortPktReflectStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_MBSM_HW_PORT_PKT_REFLECT_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortPktReflectStatus;

    pEntry->pPortReflectEntry = pPortReflectEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}


#endif /* MBSM_WANTED */
#ifdef PB_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetProviderBridgePortType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetProviderBridgePortType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetProviderBridgePortType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT4 u4PortType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetProviderBridgePortType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetProviderBridgePortType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4PortType = u4PortType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortSVlanTranslationStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortSVlanTranslationStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortSVlanTranslationStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanTranslationStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddSVlanTranslationEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddSVlanTranslationEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddSVlanTranslationEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddSVlanTranslationEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanTranslationEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalSVlan = u2LocalSVlan;
    pEntry->u2RelaySVlan = u2RelaySVlan;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelSVlanTranslationEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelSVlanTranslationEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelSVlanTranslationEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelSVlanTranslationEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelSVlanTranslationEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalSVlan = u2LocalSVlan;
    pEntry->u2RelaySVlan = u2RelaySVlan;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortEtherTypeSwapStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortEtherTypeSwapStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortEtherTypeSwapStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEtherTypeSwapStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddEtherTypeSwapEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddEtherTypeSwapEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddEtherTypeSwapEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT2 u2LocalEtherType,
                                     UINT2 u2RelayEtherType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddEtherTypeSwapEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddEtherTypeSwapEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalEtherType = u2LocalEtherType;
    pEntry->u2RelayEtherType = u2RelayEtherType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelEtherTypeSwapEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelEtherTypeSwapEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelEtherTypeSwapEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT2 u2LocalEtherType,
                                     UINT2 u2RelayEtherType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelEtherTypeSwapEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelEtherTypeSwapEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalEtherType = u2LocalEtherType;
    pEntry->u2RelayEtherType = u2RelayEtherType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwAddSVlanMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwAddSVlanMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwAddSVlanMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    tFsHwNp            FsHwNp;
#ifdef MEF_WANTED
    tEvcInfo           IssEvcInfo;
#endif
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwAddSVlanMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_ADD_S_VLAN_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanMap;

    pEntry->u4ContextId = u4ContextId;
    MEMCPY (&(pEntry->VlanSVlanMap), &VlanSVlanMap, sizeof (tVlanSVlanMap));    

#ifdef MEF_WANTED
     /*Filling Evc Info to PB-VLAN module */
     MEMSET (&IssEvcInfo, 0, sizeof (tEvcInfo));
     MefApiGetEvcInfo (pEntry->VlanSVlanMap.SVlanId, &IssEvcInfo);
        
     pEntry->VlanSVlanMap.HwEvcInfo.u4EvcId = IssEvcInfo.u4EvcId;
     pEntry->VlanSVlanMap.HwEvcInfo.SVlanId = IssEvcInfo.SVlanId;
     pEntry->VlanSVlanMap.HwEvcInfo.bCeVlanIdPreservation = IssEvcInfo.bCeVlanIdPreservation;
     pEntry->VlanSVlanMap.HwEvcInfo.bCeVlanCoSPreservation = IssEvcInfo.bCeVlanCoSPreservation;
#endif
    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDeleteSVlanMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDeleteSVlanMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDeleteSVlanMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDeleteSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    tFsHwNp             FsHwNp;
#ifdef MEF_WANTED
    tEvcInfo           IssEvcInfo; 
#endif
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDeleteSVlanMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DELETE_S_VLAN_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteSVlanMap;

    pEntry->u4ContextId = u4ContextId;
    MEMCPY (&(pEntry->VlanSVlanMap), &VlanSVlanMap, sizeof (tVlanSVlanMap));

#ifdef MEF_WANTED
     /* Filling Evc Info to PB-VLAN module */
    MEMSET (&IssEvcInfo, 0, sizeof (tEvcInfo));
    MefApiGetEvcInfo (pEntry->VlanSVlanMap.SVlanId, &IssEvcInfo);
    pEntry->VlanSVlanMap.HwEvcInfo.u4EvcId = IssEvcInfo.u4EvcId;
    pEntry->VlanSVlanMap.HwEvcInfo.SVlanId = IssEvcInfo.SVlanId;
    pEntry->VlanSVlanMap.HwEvcInfo.bCeVlanIdPreservation = IssEvcInfo.bCeVlanIdPreservation;
    pEntry->VlanSVlanMap.HwEvcInfo.bCeVlanCoSPreservation = IssEvcInfo.bCeVlanCoSPreservation;
#endif
                             
    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortSVlanClassifyMethod                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortSVlanClassifyMethod
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortSVlanClassifyMethod
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1TableType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanClassifyMethod;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1TableType = u1TableType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwPortMacLearningLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwPortMacLearningLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwPortMacLearningLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT4 u4MacLimit)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwPortMacLearningLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4MacLimit = u4MacLimit;

    return (NpUtilHwProgram (&FsHwNp));
}
/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwMulticastMacTableLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwMulticastMacTableLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwMulticastMacTableLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwMulticastMacTableLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMulticastMacTableLimit;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4MacLimit = u4MacLimit;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortCustomerVlan                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortCustomerVlan
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortCustomerVlan
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId CVlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortCustomerVlan *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortCustomerVlan;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->CVlanId = CVlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetPortCustomerVlan                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwResetPortCustomerVlan
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwResetPortCustomerVlan
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwResetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwResetPortCustomerVlan *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetPortCustomerVlan;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwCreateProviderEdgePort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwCreateProviderEdgePort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwCreateProviderEdgePort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      tVlanId SVlanId,
                                      tHwVlanPbPepInfo PepConfig)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwCreateProviderEdgePort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCreateProviderEdgePort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    MEMCPY (&(pEntry->PepConfig), &PepConfig, sizeof (tHwVlanPbPepInfo));
    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwDelProviderEdgePort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwDelProviderEdgePort
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwDelProviderEdgePort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwDelProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId SVlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwDelProviderEdgePort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelProviderEdgePort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPepPvid                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPepPvid
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPepPvid
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPepPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                          tVlanId Pvid)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPepPvid *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PEP_PVID,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepPvid;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->Pvid = Pvid;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPepAccFrameType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPepAccFrameType
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPepAccFrameType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPepAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId SVlanId, UINT1 u1AccepFrameType)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPepAccFrameType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepAccFrameType;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->u1AccepFrameType = u1AccepFrameType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPepDefUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPepDefUserPriority
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPepDefUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPepDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     tVlanId SVlanId, INT4 i4DefUsrPri)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPepDefUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepDefUserPriority;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->i4DefUsrPri = i4DefUsrPri;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPepIngFiltering                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPepIngFiltering
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPepIngFiltering
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPepIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId SVlanId, UINT1 u1IngFilterEnable)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPepIngFiltering *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PEP_ING_FILTERING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepIngFiltering;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->u1IngFilterEnable = u1IngFilterEnable;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetCvidUntagPep                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetCvidUntagPep
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetCvidUntagPep
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetCvidUntagPep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetCvidUntagPep *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagPep;

    pEntry->u4ContextId = u4ContextId;
    MEMCPY (&(pEntry->VlanSVlanMap), &VlanSVlanMap, sizeof (tVlanSVlanMap));

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetCvidUntagCep                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetCvidUntagCep
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetCvidUntagCep
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetCvidUntagCep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetCvidUntagCep *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagCep;

    pEntry->u4ContextId = u4ContextId;
    MEMCPY (&(pEntry->VlanSVlanMap), &VlanSVlanMap, sizeof (tVlanSVlanMap));

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPcpEncodTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPcpEncodTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPcpEncodTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPcpEncodTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpEncodTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    MEMCPY (&(pEntry->NpPbVlanPcpInfo), &NpPbVlanPcpInfo,
            sizeof (tHwVlanPbPcpInfo));

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPcpDecodTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPcpDecodTbl
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPcpDecodTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPcpDecodTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PCP_DECOD_TBL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpDecodTbl;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    MEMCPY (&(pEntry->NpPbVlanPcpInfo), &NpPbVlanPcpInfo,
            sizeof (tHwVlanPbPcpInfo));

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortUseDei                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortUseDei
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortUseDei
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortUseDei *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_USE_DEI,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortUseDei;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1UseDei = u1UseDei;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortReqDropEncoding                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortReqDropEncoding
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortReqDropEncoding
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1ReqDrpEncoding)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortReqDropEncoding *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortReqDropEncoding;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1ReqDrpEncoding = u1ReqDrpEncoding;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetPortPcpSelection                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetPortPcpSelection
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetPortPcpSelection
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT2 u2PcpSelection)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetPortPcpSelection *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPcpSelection;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2PcpSelection = u2PcpSelection;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetServicePriRegenEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetServicePriRegenEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetServicePriRegenEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       tVlanId SVlanId, INT4 i4RecvPriority,
                                       INT4 i4RegenPriority)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetServicePriRegenEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetServicePriRegenEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->i4RecvPriority = i4RecvPriority;
    pEntry->i4RegenPriority = i4RegenPriority;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetTunnelMacAddress                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetTunnelMacAddress
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetTunnelMacAddress
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetTunnelMacAddress (UINT4 u4ContextId, tMacAddr MacAddr,
                                   UINT2 u2Protocol)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetTunnelMacAddress *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTunnelMacAddress;

    pEntry->u4ContextId = u4ContextId;
    pEntry->MacAddr = MacAddr;
    pEntry->u2Protocol = u2Protocol;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetNextSVlanMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetNextSVlanMap
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetNextSVlanMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetNextSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                               tVlanSVlanMap * pRetVlanSVlanMap)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetNextSVlanMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanMap;

    pEntry->u4ContextId = u4ContextId;
    MEMCPY (&(pEntry->VlanSVlanMap), &VlanSVlanMap, sizeof (tVlanSVlanMap));
    pEntry->pRetVlanSVlanMap = pRetVlanSVlanMap;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetNextSVlanTranslationEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwGetNextSVlanTranslationEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwGetNextSVlanTranslationEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwGetNextSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                            tVlanId u2LocalSVlan,
                                            tVidTransEntryInfo *
                                            pVidTransEntryInfo)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanTranslationEntry;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2LocalSVlan = u2LocalSVlan;
    pEntry->pVidTransEntryInfo = pVidTransEntryInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanHwSetEvcAttribute
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes VlanFsMiVlanHwSetEvcAttribute
 *
 *    Input(s)            : Arguments of FsMiVlanHwSetEvcAttribute
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
*****************************************************************************/
INT1
VlanFsMiVlanHwSetEvcAttribute (INT4 i4FsEvcContextId, UINT4 u4Action, tEvcInfo * pIssEvcInfo)
{

    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetEvcAttribute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_EVC_ATTRIBUTE,    /* Function/OpCode */
                         0,  
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetEvcAttribute;
    pEntry->i4FsEvcContextId = i4FsEvcContextId;
    pEntry->u4Action = u4Action;
    pEntry->pIssEvcInfo = pIssEvcInfo;
    
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);  
    }
    return (FNP_SUCCESS);
    
}

                                                                                                                                         
/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetCVlanStat                                          
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes VlanFsMiVlanHwSetCVlanStat
 *                                                                          
 *    Input(s)            : Arguments of VlanFsMiVlanHwSetCVlanStat
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
INT4
VlanFsMiVlanHwSetCVlanStat (UINT4 u4ContextId, tHwVlanCVlanStat  VlanStat)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetCVlanStat *pEntry = NULL;
    UNUSED_PARAM(u4ContextId);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_CVLAN_STAT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCVlanStat;
    MEMCPY (&(pEntry->VlanStat), &VlanStat, sizeof (tHwVlanCVlanStat));
    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwGetCVlanStats                                          
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes VlanFsMiVlanHwGetCVlanStats
 *                                                                          
 *    Input(s)            : Arguments of VlanFsMiVlanHwSetCVlanStat
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
VlanFsMiVlanHwGetCVlanStats (UINT4 u4ContextId, UINT4 u2Port, UINT2 u2CVlanId,
                             UINT1 u1StatsType, UINT4 *pu4VlanStatsValue)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwGetCVlanStat *pEntry = NULL;
    UNUSED_PARAM(u4ContextId);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_GET_CVLAN_STAT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetCVlanStat;

    pEntry->u2CVlanId = u2CVlanId;
    pEntry->u2Port = u2Port;
    pEntry->u1StatsType = u1StatsType;
    pEntry->pu4VlanStatsValue = pu4VlanStatsValue;
    return (NpUtilHwProgram (&FsHwNp));



}

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwResetCVlanStats                                          
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes VlanFsMiVlanHwGetCVlanStats
 *                                                                          
 *    Input(s)            : Arguments of VlanFsMiVlanHwSetCVlanStat
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
VlanFsMiVlanHwClearCVlanStats (UINT4 u4ContextId, UINT2 u2Port, UINT2 u2CVlanId)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwClearCVlanStat *pEntry = NULL;
    UNUSED_PARAM(u4ContextId);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_CLEAR_CVLAN_STAT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwClearCVlanStat;
    pEntry->u2CVlanId = u2CVlanId;
    pEntry->u2Port = u2Port;
    return (NpUtilHwProgram (&FsHwNp));
}


#endif /* PB_WANTED */
/***************************************************************************
 *                                                                          
 *    Function Name       : VlanFsMiVlanHwSetVlanLoopbackStatus                                          
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwSetVlanLoopbackStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiVlanHwSetVlanLoopbackStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwSetVlanLoopbackStatus (UINT4 u4ContextId, tVlanId VlanId,
                                     INT4 i4LoopbackStatus)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwSetVlanLoopbackStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_SET_LOOPBACK_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanLoopbackStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->i4LoopbackStatus = i4LoopbackStatus;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : VlanFsMiVlanHwPortPktReflectStatus
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiVlanHwPortPktReflectStatus
 *
 *    Input(s)            : pPortReflectEntry -pointer to the port Reflection 
 *                          entry
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
VlanFsMiVlanHwPortPktReflectStatus (tFsNpVlanPortReflectEntry *
                                    pPortReflectEntry)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwPortPktReflectStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS,    /* Function/OpCode */
                         pPortReflectEntry->u4IfIndex,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortPktReflectStatus;

    pEntry->pPortReflectEntry = pPortReflectEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}


#endif /* __VLANNPAPI_C__ */
