/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlancfy.c,v 1.19 2014/02/27 12:34:25 siva Exp $
 *
 * Description:  This file contains VLAN routines which are used for 
 *               Port and Protocol classification.
 *
 *******************************************************************/

#include "vlaninc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanIdForPortAndProtocol                  */
/*                                                                           */
/*    Description         : This function gets the Vlan Id for port protocol */
/*                         support                                           */
/*                                                                           */
/*    Input(s)            : u2Port - Port Number                             */
/*                          pFrame - pointer to the buffer                   */
/*    Output(s)           : VlanId - VlanId is valid when VLAN_SUCCESS       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS or VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetVlanIdForPortAndProtocol (UINT2 u2Port, UINT1 *pFrame, tVlanId * pVlanId)
{
    tVlanProtoTemplate  ProtoTemplate;
    UINT4               u4GroupId = 0;
    INT4                i4RetStatus = VLAN_SUCCESS;

    VLAN_MEMSET (&ProtoTemplate, 0, sizeof (tVlanProtoTemplate));

    /* Extract the protocol template from the frame */
    i4RetStatus =
        VlanGetProtocolTemplateInFrame (pFrame, u2Port, &ProtoTemplate);
    if (i4RetStatus == VLAN_FAILURE)
    {

        return VLAN_FAILURE;
    }

    /* Get the group Id from the Protocol Group Table for the prototemplate */
    i4RetStatus = VlanGetGroupIdForProtoTemplate (ProtoTemplate, &u4GroupId);
    if (i4RetStatus == VLAN_FAILURE)
    {

        return VLAN_FAILURE;
    }

    /* Get the Vlan ID from the port Protocol Table for the group ID */
    i4RetStatus = VlanGetVlanIdInPortProtoTbl (u2Port, u4GroupId, pVlanId);
    if (i4RetStatus == VLAN_FAILURE)
    {

        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetProtocolTemplateInFrame                   */
/*                                                                           */
/*    Description         : This function extract the protocol template value*/
/*                         in the frame                                      */
/*                                                                           */
/*    Input(s)            : pu1Frame - pointer to the buffer                 */
/*                                                                           */
/*    Output(s)           : pProtoTemplate - pointer to protocol template.   */
/*                        Value is valid only when VLAN_SUCCESS              */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS or VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetProtocolTemplateInFrame (UINT1 *pu1Frame, UINT2 u2Port,
                                tVlanProtoTemplate * pProtoTemplate)
{
    UINT2               u2LenOrType = 0;
    UINT2               u2TwoByteValue = 0;
    UINT1              *pu1Offset = NULL;
    UINT1              *pu1TmpOffset = NULL;    /* Temporary poniter */
    UINT1               au1Rfc1042Value[VLAN_FRAME_RFC_1042_LENGTH] =
        { 0xAA, 0xAA, 0x03, 0x00, 0x00, 0x00 };
    UINT1               au1Snap8021HValue[VLAN_FRAME_SNAP_802_1H_LENGTH] =
        { 0xAA, 0xAA, 0x03, 0x00, 0x00, 0xF8 };
    UINT1               au1SnapOtherValue[VLAN_FRAME_SNAP_OTHER_LENGTH] =
        { 0xAA, 0xAA, 0x03 };

    /* To get the ethernet offset */
    pu1Offset = pu1Frame + VLAN_TAG_OFFSET;
    VLAN_MEMCPY ((UINT1 *) &u2LenOrType, pu1Offset, sizeof (UINT2));
    u2LenOrType = OSIX_NTOHS (u2LenOrType);

    /* 
     * To check if the priority tagged frame is received
     */

    if (VlanClassifyFrame
        ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pu1Frame, u2LenOrType,
         u2Port) == VLAN_TAGGED)
    {
        /* Skipping the tag */
        pu1Offset = (pu1Frame + VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN);
        VLAN_MEMCPY ((UINT1 *) &u2LenOrType, pu1Offset, sizeof (UINT2));
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    if (VLAN_ENET_IS_TYPE (u2LenOrType))
    {
        /* Frame type is ethernet, now extract the value */
        pProtoTemplate->u1TemplateProtoFrameType = VLAN_PORT_PROTO_ETHERTYPE;
        pProtoTemplate->u1Length = VLAN_2BYTE_PROTO_TEMP_SIZE;
        VLAN_MEMCPY (pProtoTemplate->au1ProtoValue, pu1Offset,
                     VLAN_2BYTE_PROTO_TEMP_SIZE);
        return VLAN_SUCCESS;
    }
    else
    {
        /* 
         * Skipping the length.
         * pu1Offset - Always points to the length field in the frame.
         * pu1TmpOffset - Used to go around the frame temporaily.
         */
        pu1TmpOffset = pu1Offset + VLAN_2BYTE_OFFSET;

        VLAN_MEMCPY ((UINT1 *) &u2TwoByteValue, pu1TmpOffset, sizeof (UINT2));
        u2TwoByteValue = OSIX_NTOHS (u2TwoByteValue);

        /* Check whether the frame type is LLC_OTHER */
        if ((u2TwoByteValue == VLAN_FRAME_IPX_RAW_802_3) ||
            (u2TwoByteValue == VLAN_FRAME_IP_802_3))
        {

            /* Frame type is LLC_OTHER, extract the 2 byte value */
            pProtoTemplate->u1TemplateProtoFrameType = VLAN_PORT_PROTO_LLCOTHER;
            pProtoTemplate->u1Length = VLAN_2BYTE_PROTO_TEMP_SIZE;

            VLAN_MEMCPY (pProtoTemplate->au1ProtoValue,
                         pu1TmpOffset, VLAN_2BYTE_PROTO_TEMP_SIZE);
            return VLAN_SUCCESS;
        }
        /* Frame type is RFC_1042, extract the 2 byte value */
        else if (!VLAN_MEMCMP (pu1TmpOffset, au1Rfc1042Value,
                               VLAN_FRAME_RFC_1042_LENGTH))
        {

            pProtoTemplate->u1TemplateProtoFrameType = VLAN_PORT_PROTO_RFC1042;
            pProtoTemplate->u1Length = VLAN_2BYTE_PROTO_TEMP_SIZE;

            pu1TmpOffset = pu1Offset + VLAN_FRAME_RFC_1042_OFFSET;

            VLAN_MEMCPY (pProtoTemplate->au1ProtoValue,
                         pu1TmpOffset, VLAN_2BYTE_PROTO_TEMP_SIZE);
            return VLAN_SUCCESS;
        }
        else if (!VLAN_MEMCMP (pu1TmpOffset, au1Snap8021HValue,
                               VLAN_FRAME_SNAP_802_1H_LENGTH))
        {
            /* Frame type is SNAP_8021H, extract the 2 byte value */
            pProtoTemplate->u1TemplateProtoFrameType =
                VLAN_PORT_PROTO_SNAP8021H;
            pProtoTemplate->u1Length = VLAN_2BYTE_PROTO_TEMP_SIZE;

            pu1TmpOffset = pu1Offset + VLAN_FRAME_SNAP_802_1H_OFFSET;

            VLAN_MEMCPY (pProtoTemplate->au1ProtoValue,
                         pu1TmpOffset, VLAN_2BYTE_PROTO_TEMP_SIZE);
            return VLAN_SUCCESS;
        }
        else if (!VLAN_MEMCMP (pu1TmpOffset, au1SnapOtherValue,
                               VLAN_FRAME_SNAP_OTHER_LENGTH))
        {
            /* Frame type is SNAP_Other, extract the 5 byte value */
            pProtoTemplate->u1TemplateProtoFrameType =
                VLAN_PORT_PROTO_SNAPOTHER;
            pProtoTemplate->u1Length = VLAN_5BYTE_PROTO_TEMP_SIZE;

            pu1TmpOffset = pu1Offset + VLAN_FRAME_SNAP_OTHER_OFFSET;

            VLAN_MEMCPY (pProtoTemplate->au1ProtoValue,
                         pu1TmpOffset, VLAN_5BYTE_PROTO_TEMP_SIZE);
            return VLAN_SUCCESS;
        }
        else
        {
            /* In future, the classification type can be added 
             */
            return VLAN_FAILURE;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetGroupIdForProtoTemplate                   */
/*                                                                           */
/*    Description         : This function gets the group Id for              */
/*                         for protocol template in the Protocol Group Table */
/*                                                                           */
/*    Input(s)            : ProtoTemplate - protocol template                */
/*                                                                           */
/*    Output(s)           : pu4GroupId - pointer to group Id                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS or VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetGroupIdForProtoTemplate (tVlanProtoTemplate ProtoTemplate,
                                UINT4 *pu4GroupId)
{
    tVlanProtGrpEntry  *pVlanGetProtGrpEntry = NULL;

    pVlanGetProtGrpEntry =
        VlanGetProtGrpEntry (ProtoTemplate.u1TemplateProtoFrameType,
                             ProtoTemplate.u1Length,
                             ProtoTemplate.au1ProtoValue);

    if (pVlanGetProtGrpEntry == NULL)
    {

        return VLAN_FAILURE;
    }

    /* Entry is valid only when the entry status is active 
     */
    if (pVlanGetProtGrpEntry->u1RowStatus != VLAN_ACTIVE)
    {

        return VLAN_FAILURE;
    }

    *pu4GroupId = pVlanGetProtGrpEntry->u4ProtGrpId;
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanIdInPortProtoTbl                      */
/*                                                                           */
/*    Description         : This function gets the Vlan Id from              */
/*                         port and protocol table                          */
/*                                                                           */
/*    Input(s)            :  u2Port - Port number                            */
/*                           u4GroupId - pointer to group Id                 */
/*    Output(s)           :  pVlanId- Vlan Id                                */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS or VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetVlanIdInPortProtoTbl (UINT2 u2Port, UINT4 u4GroupId, tVlanId * pVlanId)
{
    tVlanPortVidSet    *pVlanPortVidSet = NULL;

    pVlanPortVidSet = VlanGetPortProtoVidSetEntry (u2Port, u4GroupId);
    if (pVlanPortVidSet == NULL)
    {

        return VLAN_FAILURE;
    }
    /* Entry is valid only when the entry status is active 
     */
    if (pVlanPortVidSet->u1RowStatus != VLAN_ACTIVE)
    {

        return VLAN_FAILURE;
    }
    *pVlanId = pVlanPortVidSet->VlanId;
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetProtGrpEntry                              */
/*                                                                           */
/*    Description         : This function gets the entry from                */
/*                          Vlan protocol group  Table for the given         */
/*                          Template value.                                  */
/*                                                                           */
/*    Input(s)            : u1Length - length of the protocol                */
/*                          pVlanProtGrpEntry- Pointer to  proto group entry */
/*                          *pu1Value - pointer to protocol valu     e       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the Protocol Group Entry / NULL        */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
tVlanProtGrpEntry  *
VlanGetProtGrpEntry (UINT1 u1FrameType, UINT1 u1Length, UINT1 *pu1Value)
{
    UINT4               u4HashIndex = 0;
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;

    VlanGetHashIndexProtocolValue (u1Length, pu1Value, &u4HashIndex);
    /* Get the node into corresponding hash bucket */
    VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                           u4HashIndex, pVlanProtGrpEntry, tVlanProtGrpEntry *)
    {
        if (pVlanProtGrpEntry->VlanProtoTemplate.u1TemplateProtoFrameType
            == u1FrameType)
        {
            if (VlanCompProtoValue (u1Length, pu1Value, pVlanProtGrpEntry)
                == VLAN_EQUAL)
            {

                return pVlanProtGrpEntry;
            }
        }

    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetActiveProtoGrpEntry                       */
/*                                                                           */
/*    Description         : This function gets the active entry from         */
/*                          Vlan protocol group Table for the given          */
/*                          group Id                                         */
/*                                                                           */
/*    Input(s)            : u4GroupId - group ID                             */
/*                                                                           */
/*    Output(s)           : pVlanProtGrpEntry- Pointer to  proto group entry */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the Protocol Group Entry / NULL        */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
tVlanProtGrpEntry  *
VlanGetActiveProtoGrpEntry (UINT4 u4GroupId)
{
    UINT4               u4HashIndex = 0;
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;

    if (VLAN_PROTOCOL_GROUP_TBL != NULL)
    {
        for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
        {
            /* Get the node into corresponding hash bucket */
            VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                                   u4HashIndex, pVlanProtGrpEntry,
                                   tVlanProtGrpEntry *)
            {
                if (pVlanProtGrpEntry->u4ProtGrpId == u4GroupId)
                {
                    /* There could be multiple group entries with the
                     * same group ID. */
                    if (pVlanProtGrpEntry->u1RowStatus == VLAN_ACTIVE)
                    {
                        /* Return for the active protocol group
                         * entry
                         */
                        return pVlanProtGrpEntry;
                    }
                }
            }
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetHashIndexProtocolValue                    */
/*                                                                           */
/*    Description         : This function used to get  the index value       */
/*                         in protocol group table                           */
/*    Input(s)            : u1Length - length of the protocol                */
/*                          pu1Value - pointer to the protocol value         */
/*                                                                           */
/*    Output(s)           : pu4HashIndex - pointer to hash index value       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetHashIndexProtocolValue (UINT1 u1Length, UINT1 *pu1Value,
                               UINT4 *pu4HashIndex)
{
    /* To hash based on the length of the protocol */
    if (u1Length == VLAN_5BYTE_PROTO_TEMP_SIZE)
    {

        *pu4HashIndex = (UINT4) pu1Value[VLAN_5BYTE_PROTO_TEMP_SIZE - 1];
    }
    else
    {

        *pu4HashIndex = (UINT4) pu1Value[VLAN_2BYTE_PROTO_TEMP_SIZE - 1];
    }

    *pu4HashIndex = (UINT4) (*pu4HashIndex % VLAN_MAX_BUCKETS);
    return;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanStoreProtoValue                              */
/*                                                                           */
/*    Description         : This function used to store the protocol value   */
/*                         in protocol group table                           */
/*    Input(s)            : u1Length - length of the protocol  */
/*                          pVlanProtGrpEntry- Pointer to  proto group entry */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanStoreProtoValue (UINT1 u1Length, UINT1 *pu1Value,
                     tVlanProtGrpEntry * pVlanProtGrpEntry)
{

    pVlanProtGrpEntry->VlanProtoTemplate.u1Length = u1Length;
    VLAN_MEMCPY (pVlanProtGrpEntry->VlanProtoTemplate.au1ProtoValue,
                 pu1Value, u1Length);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCompProtoValue                               */
/*                                                                           */
/*    Description         : This function used to compare the protocol value */
/*                         in protocol group table                           */
/*    Input(s)            : u1Length - length of the protocol                */
/*                          pVlanProtGrpEntry- Pointer to  proto group entry */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_EQUAL or VLAN_LESSER or VLAN_GREATER         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
VlanCompProtoValue (UINT1 u1Length, UINT1 *pu1Value,
                    tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    INT1                i1CompStatus = VLAN_EQUAL;
    INT4                i4RetVal = VLAN_EQUAL;

    i4RetVal =
        VLAN_MEMCMP (pVlanProtGrpEntry->VlanProtoTemplate.au1ProtoValue,
                     pu1Value, u1Length);

    if (i4RetVal < 0)
    {
        i1CompStatus = VLAN_LESSER;
    }
    else if (i4RetVal > 0)
    {
        i1CompStatus = VLAN_GREATER;
    }
    else
    {
        i1CompStatus = VLAN_EQUAL;
    }

    return i1CompStatus;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCompareFrameTypeAndProtoValue                */
/*                                                                           */
/*    Description         : This function used to compare the frame type     */
/*                          and protocol value                               */
/*    Input(s)            : u1FrameType - Frame Type                         */
/*                          u1ProtoLength - Protocol length                  */
/*                          pu1ProtoValue - pointer to protocol value        */
/*                          pVlanProtGrpEntry- Pointer to  proto group entry */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_LESSER or VLAN_GREATER or VLAN_EQUAL         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT1
VlanCompareFrameTypeAndProtoValue (INT4 i4FrameType,
                                   UINT1 u1ProtoLength, UINT1 *pu1ProtoValue,
                                   tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    INT1                i1CompValue = VLAN_EQUAL;

    if (i4FrameType <
        pVlanProtGrpEntry->VlanProtoTemplate.u1TemplateProtoFrameType)
    {

        i1CompValue = VLAN_LESSER;
    }
    else if (i4FrameType >
             pVlanProtGrpEntry->VlanProtoTemplate.u1TemplateProtoFrameType)
    {

        i1CompValue = VLAN_GREATER;
    }
    else
    {

        i1CompValue =
            VlanCompProtoValue (u1ProtoLength, pu1ProtoValue,
                                pVlanProtGrpEntry);
    }

    return i1CompValue;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAddProtGrpEntry                              */
/*                                                                           */
/*    Description         : This function adds an entry to protocol grouptable*/
/*                                                                            */
/*    Input(s)            : pVlanProtGrpEntry- Pointer to the new proto group */
/*                          entry                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred :VLAN_PROTOCOL_GROUP_TBL                     */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanAddProtGrpEntry (tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    UINT4               u4HashIndex;

    VlanGetHashIndexProtocolValue (pVlanProtGrpEntry->VlanProtoTemplate.
                                   u1Length,
                                   pVlanProtGrpEntry->VlanProtoTemplate.
                                   au1ProtoValue, &u4HashIndex);

    /* Add the node to the corresponding hash bucket */
    TMO_HASH_Add_Node (VLAN_PROTOCOL_GROUP_TBL,
                       (tTMO_HASH_NODE *) & (pVlanProtGrpEntry->NextHashNode),
                       u4HashIndex, NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteProtGrpEntry                           */
/*                                                                           */
/*    Description         : This function deletes the given protocol group   */
/*                         entry from the VLAN Protocol Group table.         */
/*                                                                           */
/*    Input(s)            : pVlanProtGrpEntry - Pointer to protocol group to */
/*                         entry to be deleted.                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_PROTOCOL_GROUP_TBL                    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteProtGrpEntry (tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    UINT4               u4HashIndex;
    /* Delete the node from the corresponding hash bucket */
    VlanGetHashIndexProtocolValue (pVlanProtGrpEntry->VlanProtoTemplate.
                                   u1Length,
                                   pVlanProtGrpEntry->VlanProtoTemplate.
                                   au1ProtoValue, &u4HashIndex);

    TMO_HASH_Delete_Node (VLAN_PROTOCOL_GROUP_TBL,
                          (tTMO_HASH_NODE *) & (pVlanProtGrpEntry->
                                                NextHashNode), u4HashIndex);

    VLAN_RELEASE_BUF (VLAN_PROTO_GROUP_ENTRY, (UINT1 *) pVlanProtGrpEntry);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPortProtoVidSetEntry                      */
/*                                                                           */
/*    Description         : This function gets the entry from                */
/*                          port protocol vid set                            */
/*                                                                           */
/*    Input(s)            : u2Port - port number                             */
/*                          u4ProtoGrpId - protocol group Id                 */
/*                                                                           */
/*    Output(s)           : pVlanProtGrpEntry- Pointer to  proto group entry */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :pointer to port VID set                            */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
tVlanPortVidSet    *
VlanGetPortProtoVidSetEntry (UINT2 u2Port, UINT4 u4ProtoGrpId)
{
    tVlanPortVidSet    *pVlanPortVidSet = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {

        return NULL;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {

        return NULL;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
    {
        return NULL;
    }

    VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSet,
                   tVlanPortVidSet *)
    {

        if (pVlanPortVidSet->u4ProtGrpId == u4ProtoGrpId)
        {
            return pVlanPortVidSet;
        }

    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAddPortProtoVidSetEntry                      */
/*                                                                           */
/*    Description         : This function adds an entry to port and protocol */
/*                          table                                            */
/*                                                                           */
/*    Input(s)            : u2Port - port number                             */
/*                          pVlanPortVidSetEntry- Pointer to the new         */
/*                          port and protocol entry into port                */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanAddPortProtoVidSetEntry (tVlanPortEntry * pVlanPortEntry,
                             tVlanPortVidSet * pVlanPortVidSet)
{
    VLAN_SLL_ADD (&pVlanPortEntry->VlanVidSet,
                  (tTMO_SLL_NODE *) & (pVlanPortVidSet->NextNode));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanReleaseProtocolGrpTbl                        */
/*                                                                           */
/*    Description         : This function releases the Protocol              */
/*                         group Table enrties                               */
/*                                                                           */
/*    Input(s)            :   None                                           */
/*                                                                           */
/*    Output(s)           :  None                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanReleaseProtocolGrpTbl (VOID)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    tVlanProtGrpEntry  *pVlanNextProtGrpEntry = NULL;
    UINT4               u4HashIndex = 0;
    if (VLAN_PROTOCOL_GROUP_TBL != NULL)
    {
        for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
        {
            VLAN_HASH_DYN_Scan_Bucket (VLAN_PROTOCOL_GROUP_TBL, u4HashIndex,
                                       pVlanProtGrpEntry, pVlanNextProtGrpEntry,
                                       tVlanProtGrpEntry *)
            {
                /* Delete the node from the corresponding hash bucket */
                TMO_HASH_Delete_Node (VLAN_PROTOCOL_GROUP_TBL,
                                      (tTMO_HASH_NODE *) & (pVlanProtGrpEntry->
                                                            NextHashNode),
                                      (UINT4) u4HashIndex);
                VLAN_RELEASE_BUF (VLAN_PROTO_GROUP_ENTRY,
                                  (UINT1 *) pVlanProtGrpEntry);
                pVlanProtGrpEntry = NULL;
            }
        }
        VLAN_HASH_DELETE_TABLE (VLAN_PROTOCOL_GROUP_TBL, NULL);
        VLAN_PROTOCOL_GROUP_TBL = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckProtGrpEntryMappedToPort                */
/*                                                                           */
/*    Description         : This function checks whether the protocol group  */
/*                         entry mapped to any of the port                   */
/*                                                                           */
/*    Input(s)            : pVlanProtGrpEntry - Pointer to protocol group    */
/*                         entry                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_PROTOCOL_GROUP_TBL                    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE  or VLAN_FALSE                          */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
VlanCheckProtGrpEntryMappedToPort (tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT2               u2Port = 0;

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        /* 
         * Check whether the port contains this Vid Set entry for
         * this group.
         */

        pVlanPortVidSetEntry =
            VlanGetPortProtoVidSetEntry (u2Port,
                                         pVlanProtGrpEntry->u4ProtGrpId);
        if (pVlanPortVidSetEntry != NULL)
        {
            return VLAN_TRUE;
        }
    }

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckAndDelProtGrpInfo                       */
/*                                                                           */
/*    Description         : This function checks whether the given group     */
/*                         entry can be deleted or not.                      */
/*                                                                           */
/*    Input(s)            : pVlanProtGrpEntry - Pointer to protocol group to */
/*                         entry to be deleted.                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_PROTOCOL_GROUP_TBL                    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS - if entry can be deleted            */
/*                         VLAN_FAILURE - if entry should not be deleted     */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckAndDelProtGrpInfo (tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    UINT4               u4HashIndex = 0;
    UINT4               u4Count = 0;
    UINT2               u2Port = 0;
    tVlanProtGrpEntry  *pVlanTmpProtGrpEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    INT4                i4RetVal;

    if (VLAN_PROTOCOL_GROUP_TBL != NULL)
    {
        for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
        {
            VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                                   u4HashIndex, pVlanTmpProtGrpEntry,
                                   tVlanProtGrpEntry *)
            {

                if (pVlanTmpProtGrpEntry->u1RowStatus != VLAN_ACTIVE)
                {
                    continue;
                }

                /* Count the no of active group entries */
                if (pVlanTmpProtGrpEntry->u4ProtGrpId ==
                    pVlanProtGrpEntry->u4ProtGrpId)
                {
                    u4Count++;
                }
            }
        }
    }

    if (u4Count > 1)
    {

        /*
         * More ACTIVE group entries exist with the same group ID.
         * So Check all the ports for which this group id has been mapped
         * into VID. So give delete indication to hardware for the frame type
         * and protocol value associated with the group entry to be deleted.
         */
        VLAN_SCAN_PORT_TABLE (u2Port)
        {
            /* 
             * Check whether the port contains this Vid Set entry for
             * this group.
             */
            pVlanPortVidSetEntry =
                VlanGetPortProtoVidSetEntry (u2Port,
                                             pVlanProtGrpEntry->u4ProtGrpId);

            if (pVlanPortVidSetEntry == NULL)
            {
                continue;
            }

            /* Give delete indication to Hardware iff row status is active */
            if (pVlanPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
            {
                /* Need to program the hardware here */
                i4RetVal =
                    VlanHwDelVlanProtocolMap (VLAN_CURR_CONTEXT_ID (),
                                              u2Port,
                                              pVlanProtGrpEntry->
                                              u4ProtGrpId,
                                              &pVlanProtGrpEntry->
                                              VlanProtoTemplate);

                if (i4RetVal == VLAN_FAILURE)
                {
                    CLI_SET_ERR (CLI_VLAN_HW_DEL_ERR);
                    return VLAN_FAILURE;
                }
            }
        }
    }
    else
    {
        /* 
         * Only one ACTIVE entry present with the same group ID.
         * Check whether this group ID is mapped to any port VID set.
         * If mapped, return failure.
         */
        VLAN_SCAN_PORT_TABLE (u2Port)
        {
            /* 
             * Check whether the port contains this Vid Set entry for
             * this group.
             */
            pVlanPortVidSetEntry =
                VlanGetPortProtoVidSetEntry (u2Port,
                                             pVlanProtGrpEntry->u4ProtGrpId);

            if (pVlanPortVidSetEntry == NULL)
            {
                continue;
            }

            if (pVlanPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
            {
                CLI_SET_ERR (CLI_VLAN_PROTO_GRP_INFO_DEL_ERR);
                return VLAN_FAILURE;
            }
        }

    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAddNewGroupInfoInHw                          */
/*                                                                           */
/*    Description         : This function will be invoked when a group entry */
/*                         is made active. This function scans the VID sets  */
/*                         of all ports for the new group entry configured   */
/*                         and updates the hardware.                         */
/*                                                                           */
/*    Input(s)            : pVlanProtGrpEntry - Pointer to protocol group    */
/*                         entry                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_PROTOCOL_GROUP_TBL                    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanAddNewGroupInfoInHw (tVlanProtGrpEntry * pVlanProtGrpEntry)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT2               u2Port = 0;
    INT4                i4RetVal;

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        /* 
         * Check whether the port contains this Vid Set entry for
         * this group.
         */

        pVlanPortVidSetEntry =
            VlanGetPortProtoVidSetEntry (u2Port,
                                         pVlanProtGrpEntry->u4ProtGrpId);
        if (pVlanPortVidSetEntry == NULL)
        {
            continue;
        }
        if (pVlanPortVidSetEntry->u1RowStatus != VLAN_ACTIVE)
        {
            continue;
        }

        i4RetVal =
            VlanHwAddVlanProtocolMap (VLAN_CURR_CONTEXT_ID (),
                                      u2Port,
                                      pVlanProtGrpEntry->u4ProtGrpId,
                                      &pVlanProtGrpEntry->
                                      VlanProtoTemplate,
                                      pVlanPortVidSetEntry->VlanId);

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Protocol Vlan Map Addition in hardware failed \n");
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdatePortMacVlanInfoInHw                    */
/*                                                                           */
/*    Description         : This function updates the Port MAC VLAN          */
/*                          in hardware.                                     */
/*                                                                           */
/*    Input(s)            : u4contextId- Current context Id                  */
/*                          u4Port - Port on which the MAC VLAN has to be    */
/*                                   programmed.                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on successful NP programming,        */
/*                         VLAN_FAILURE otherwise.                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdatePortMacVlanInfoInHw (UINT4 u4ContextId, UINT2 u2Port)
{
    UINT2               u2HashIndex = 0;
    tVlanMacMapEntry   *pMacMapEntry = NULL;

    /* Enable MAC based status for this port in hardware */
    if (VlanHwSetMacBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                       u2Port,
                                       VLAN_PORT_MAC_BASED (u2Port)) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "Setting MAC based status on a port failed "
                       "for Port %d \n", u2Port);
        return VLAN_FAILURE;
    }

    VLAN_HASH_MAC_MAP_ADDR ((UINT4) u2Port, u2HashIndex);

    pMacMapEntry = VLAN_GET_MAC_MAP_ENTRY (u2HashIndex);

    /* Scan all the MAC Map entries present for this port and program the
     * same in hardware. 
     */
    while (pMacMapEntry != NULL)
    {
        if (VLAN_FAILURE == (VlanHwAddPortMacVlanEntry (u4ContextId,
                                                        VLAN_GET_IFINDEX
                                                        (u2Port),
                                                        pMacMapEntry->MacAddr,
                                                        pMacMapEntry->VlanId,
                                                        (BOOL1) pMacMapEntry->
                                                        u1McastOption)))
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "Adding Port MAC VLAN entry failed "
                           "for Port %d and VLAN %d\n", u2Port,
                           pMacMapEntry->VlanId);
            return VLAN_FAILURE;
        }

        pMacMapEntry = pMacMapEntry->pNextHashNode;
    }                            /* End of while */

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateGroupVlanMapInfoInHw                   */
/*                                                                           */
/*    Description         : This function will be invoked whenever the       */
/*                         Group ID - VLAN Id mapping is Added/Deleted.      */
/*                         This function scans the whold protocol group      */
/*                         database for the group entries with the same      */
/*                         group ID and calls appropriate hardware API's     */
/*                         to program the hardware.                          */
/*                                                                           */
/*    Input(s)            : u4Port - Port no in which the mapping is deleted */
/*                          u4GroupId - Group ID                             */
/*                          VlanId - Vlan ID                                 */
/*                          u1Flag - VLAN_ADD / VLAN_DELETE                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : VLAN_PROTOCOL_GROUP_TBL                    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanUpdateGroupVlanMapInfoInHw (UINT4 u4Port, UINT4 u4GroupId, tVlanId VlanId,
                                UINT1 u1Flag)
{
    UINT4               u4HashIndex;
    INT4                i4RetVal;
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;

    /* 
     * Multiple Group entries with the same Group ID can exist in
     * the Protocol Group Database. So give delete indication for all
     * group entries.
     */
    for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
    {
        VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                               u4HashIndex, pVlanProtGrpEntry,
                               tVlanProtGrpEntry *)
        {
            if (pVlanProtGrpEntry->u4ProtGrpId != u4GroupId)
            {
                continue;
            }

            if (pVlanProtGrpEntry->u1RowStatus != VLAN_ACTIVE)
            {
                continue;
            }

            if (u1Flag == VLAN_ADD)
            {
                i4RetVal =
                    VlanHwAddVlanProtocolMap (VLAN_CURR_CONTEXT_ID (),
                                              (UINT2) u4Port, u4GroupId,
                                              &pVlanProtGrpEntry->
                                              VlanProtoTemplate, VlanId);
            }
            else
            {
                i4RetVal =
                    VlanHwDelVlanProtocolMap (VLAN_CURR_CONTEXT_ID (),
                                              (UINT2) u4Port, u4GroupId,
                                              &pVlanProtGrpEntry->
                                              VlanProtoTemplate);
            }

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Protocol Vlan Map updation in hardware failed \n");
            }
        }
    }
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteGroupVlanMapInfoInHw                   */
/*                                                                           */
/*    Description         : This function will be invoked whenever the       */
/*                         Group ID - VLAN Id mapping has to be deleted      */
/*                         for an interface removed from a portchannel       */
/*                         and the underlying hw does not have trunk support */
/*                         This function scans the whold protocol group      */
/*                         database for the group entries with the same      */
/*                         group ID and calls appropriate hardware API's     */
/*                         to program the hardware.                          */
/*                                                                           */
/*    Input(s)            : u4DstPort - Port in which the mapping is deleted */
/*                          u2SrcPort - Portchannel index for which the      */
/*                                      are added                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*****************************************************************************/
VOID
VlanDeleteGroupVlanMapInfoInHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    tVlanPortEntry     *pSrcPortEntry = NULL;
    tVlanPortVidSet    *pPortVidSetEntry = NULL;
    UINT4               u4HashIndex;
    UINT4               u4GroupId = 0;
    INT4                i4RetVal;

    pSrcPortEntry = VLAN_GET_PORT_ENTRY ((UINT2) u4SrcPort);

    if (pSrcPortEntry == NULL)
    {
        return;
    }

    VLAN_SLL_SCAN (&pSrcPortEntry->VlanVidSet, pPortVidSetEntry,
                   tVlanPortVidSet *)
    {
        u4GroupId = pPortVidSetEntry->u4ProtGrpId;
        /* Multiple Group entries with the same Group ID can exist in
         * the Protocol Group Database. So give delete indication for all
         * group entries. */
        for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
        {
            VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                                   u4HashIndex, pVlanProtGrpEntry,
                                   tVlanProtGrpEntry *)
            {
                if (pVlanProtGrpEntry->u4ProtGrpId != u4GroupId)
                {
                    continue;
                }

                if (pVlanProtGrpEntry->u1RowStatus != VLAN_ACTIVE)
                {
                    continue;
                }

                i4RetVal = VlanFsMiVlanHwDelVlanProtocolMap
                    (VLAN_CURR_CONTEXT_ID (), u4DstPort, u4GroupId,
                     &pVlanProtGrpEntry->VlanProtoTemplate);

                if (i4RetVal == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Protocol Vlan Map deletion in hardware failed \n");
                }
            }
        }
    }
    return;
}
#endif
