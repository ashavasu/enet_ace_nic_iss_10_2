/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      
 *
 * $Id: vlnmiset.c,v 1.91.22.1 2018/03/15 13:00:06 siva Exp $
 *
 *  Description           : This file contains routines to set VLAN objects  
 *
 *****************************************************************************/

/*****************************************************************************/
/*  FILE NAME             : vlanset.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      :                                                  */
/*  Modified by           : MI TEAM                                          */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"
#include "vcm.h"
#include "fsmpvlcli.h"

PRIVATE INT4
 
 
 
 VlanMiUpdateStaticUcastEntry (UINT4 u4FdbId, tMacAddr MacAddr,
                               UINT2 u2Port, INT4 i4Status);

PRIVATE INT4
 
 
 
 VlanStaticUcastCreateTempPortList (UINT4 u4FdbId, UINT2 u2Port,
                                    tMacAddr MacAddr, tLocalPortList Ports,
                                    UINT1 u1Status, tMacAddr ConnectionId);

PRIVATE VOID
 
 
 
 VlanMIDeleteFdbStUcastEntry (tVlanStUcastEntry * pVlanStUcastEntry,
                              UINT4 u4FidIndex);

extern UINT4        FsDot1qStaticUnicastRowStatus[14];
extern UINT4        FsDot1qStaticUnicastStatus[14];
extern UINT4        FsDot1qStaticAllowedIsMember[14];
extern UINT4        FsDot1qStaticMulticastRowStatus[14];
extern UINT4        FsDot1qStaticMcastPort[14];
extern UINT4        FsDot1qVlanStaticRowStatus[14];
extern UINT4        FsDot1qVlanStaticPort[14];

/****************************************************************************
 Function    :  nmhSetFsDot1qForwardAllRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                setValFsDot1qForwardAllRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qForwardAllRowStatus (INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qVlanIndex,
                                  INT4 i4SetValFsDot1qForwardAllRowStatus)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE *pStaticPortList = &StaticPortList;
    tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pCurrEntry->AllGrps.u1RowStatus == i4SetValFsDot1qForwardAllRowStatus)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    if (i4SetValFsDot1qForwardAllRowStatus == VLAN_NOT_IN_SERVICE)
    {
        pTmpPortList =
            (tVlanTempPortList *) (VOID *)
            VLAN_GET_BUF (VLAN_TEMP_PORTLIST_ENTRY, 0);

        if (pTmpPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "No Free Temp PortList Entry \n");
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        MEMSET (pTmpPortList, 0, sizeof (tVlanTempPortList));

        VLAN_SLL_INIT_NODE (&(pTmpPortList->NextNode));

        pTmpPortList->u4ContextId = i4FsDot1qVlanContextId;
        pTmpPortList->u1Type = VLAN_FORWARD_ALL_TABLE;
        pTmpPortList->PortListTbl.FwdTbl.VlanId = VlanId;

        MEMCPY (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                pCurrEntry->AllGrps.StaticPorts, VLAN_PORT_LIST_SIZE);
        MEMCPY (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                pCurrEntry->AllGrps.ForbiddenPorts, VLAN_PORT_LIST_SIZE);

        VLAN_SLL_ADD (&gVlanTempPortList,
                      (tTMO_SLL_NODE *) & (pTmpPortList->NextNode));

        pCurrEntry->AllGrps.u1RowStatus
            = (UINT1) i4SetValFsDot1qForwardAllRowStatus;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    else if (i4SetValFsDot1qForwardAllRowStatus == VLAN_ACTIVE)
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_FORWARD_ALL_TABLE) &&
                (pTmpPortList->PortListTbl.FwdTbl.VlanId == VlanId))
            {
                break;
            }
        }

        if (pTmpPortList != NULL)
        {
            pau1ForbiddenPortList =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1ForbiddenPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "nmhSetFsDot1qForwardAllRowStatus: "
                          "Error in allocating memory for pau1ForbiddenPortList\r\n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

            pau1StaticPortList =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1StaticPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "nmhSetFsDot1qForwardAllRowStatus: "
                          "Error in allocating memory for pau1StaticPortList\r\n");
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);

            MEMCPY (pau1StaticPortList,
                    pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                    VLAN_PORT_LIST_SIZE);
            StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
            StaticPortList.pu1_OctetList = pau1StaticPortList;

            MEMCPY (pau1ForbiddenPortList,
                    pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                    VLAN_PORT_LIST_SIZE);
            ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
            ForbiddenPortList.pu1_OctetList = pau1ForbiddenPortList;

            if (nmhSetDot1qForwardAllStaticPorts (u4FsDot1qVlanIndex,
                                                  pStaticPortList) !=
                SNMP_SUCCESS)
            {
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
                return SNMP_FAILURE;
            }
            if (nmhSetDot1qForwardAllForbiddenPorts (u4FsDot1qVlanIndex,
                                                     &ForbiddenPortList) !=
                SNMP_SUCCESS)
            {
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
                return SNMP_FAILURE;
            }

            VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));

            VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY, (UINT1 *) pTmpPortList);
            pTmpPortList = NULL;
            pCurrEntry->AllGrps.u1RowStatus =
                (UINT1) i4SetValFsDot1qForwardAllRowStatus;
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return SNMP_SUCCESS;
        }
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4SetValFsDot1qForwardAllRowStatus);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qForwardAllPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                setValFsDot1qForwardAllPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qForwardAllPort (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                             INT4 i4SetValFsDot1qForwardAllPort)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pCurrEntry->AllGrps.u1RowStatus == NOT_IN_SERVICE)
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_FORWARD_ALL_TABLE) &&
                (pTmpPortList->PortListTbl.FwdTbl.VlanId == VlanId))
            {
                switch (i4SetValFsDot1qForwardAllPort)
                {
                    case VLAN_ADD_MEMBER_PORT:
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_ADD_FORBIDDEN_PORT:
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_DEL_MEMBER_PORT:
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_DEL_FORBIDDEN_PORT:
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                             u2LocalPortId);
                        break;

                    default:

                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                }

                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (i4SetValFsDot1qForwardAllPort);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qForwardUnregRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                setValFsDot1qForwardUnregRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qForwardUnregRowStatus (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qVlanIndex,
                                    INT4 i4SetValFsDot1qForwardUnregRowStatus)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE *pStaticPortList = &StaticPortList;
    tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pCurrEntry->UnRegGrps.u1RowStatus ==
        i4SetValFsDot1qForwardUnregRowStatus)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    if ((i4SetValFsDot1qForwardUnregRowStatus == VLAN_NOT_IN_SERVICE) ||
        (i4SetValFsDot1qForwardUnregRowStatus == VLAN_CREATE_AND_WAIT))
    {
        pTmpPortList =
            (tVlanTempPortList *) (VOID *)
            VLAN_GET_BUF (VLAN_TEMP_PORTLIST_ENTRY, 0);

        if (pTmpPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "No Free Temp PortList Entry \n");
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        MEMSET (pTmpPortList, 0, sizeof (tVlanTempPortList));

        VLAN_SLL_INIT_NODE (&(pTmpPortList->NextNode));

        pTmpPortList->u4ContextId = i4FsDot1qVlanContextId;
        pTmpPortList->u1Type = VLAN_FORWARD_UNREG_TABLE;
        pTmpPortList->PortListTbl.FwdTbl.VlanId = VlanId;

        if (i4SetValFsDot1qForwardUnregRowStatus == VLAN_CREATE_AND_WAIT)
        {
            MEMCPY (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                    gNullPortList, VLAN_PORT_LIST_SIZE);
            MEMCPY (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                    gNullPortList, VLAN_PORT_LIST_SIZE);
        }
        else
        {
            MEMCPY (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                    pCurrEntry->UnRegGrps.StaticPorts, VLAN_PORT_LIST_SIZE);
            MEMCPY (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                    pCurrEntry->UnRegGrps.ForbiddenPorts, VLAN_PORT_LIST_SIZE);
        }

        VLAN_SLL_ADD (&gVlanTempPortList,
                      (tTMO_SLL_NODE *) & (pTmpPortList->NextNode));

        pCurrEntry->UnRegGrps.u1RowStatus = VLAN_NOT_IN_SERVICE;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    else if (i4SetValFsDot1qForwardUnregRowStatus == VLAN_ACTIVE)
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_FORWARD_UNREG_TABLE) &&
                (pTmpPortList->PortListTbl.FwdTbl.VlanId == VlanId))
            {
                break;
            }
        }

        if (pTmpPortList != NULL)
        {
            pau1ForbiddenPortList =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1ForbiddenPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "nmhSetFsDot1qForwardUnregRowStatus: "
                          "Error in allocating memory for pau1ForbiddenPortList\r\n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

            pau1StaticPortList =
                UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
            if (pau1StaticPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "nmhSetFsDot1qForwardUnregRowStatus: "
                          "Error in allocating memory for pau1StaticPortList\r\n");
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);
            MEMCPY (pau1StaticPortList,
                    pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                    VLAN_PORT_LIST_SIZE);
            StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
            StaticPortList.pu1_OctetList = pau1StaticPortList;

            MEMCPY (pau1ForbiddenPortList,
                    pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                    VLAN_PORT_LIST_SIZE);
            ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
            ForbiddenPortList.pu1_OctetList = pau1ForbiddenPortList;

            if (nmhSetDot1qForwardUnregisteredStaticPorts
                (u4FsDot1qVlanIndex, pStaticPortList) != SNMP_SUCCESS)
            {
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
                return SNMP_FAILURE;
            }
            if (nmhSetDot1qForwardUnregisteredForbiddenPorts
                (u4FsDot1qVlanIndex, &ForbiddenPortList) != SNMP_SUCCESS)
            {
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
                return SNMP_FAILURE;
            }

            VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));

            VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY, (UINT1 *) pTmpPortList);
            pTmpPortList = NULL;
            pCurrEntry->UnRegGrps.u1RowStatus = VLAN_ACTIVE;
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
            UtilPlstReleaseLocalPortList (pau1StaticPortList);
            return SNMP_SUCCESS;
        }
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4SetValFsDot1qForwardUnregRowStatus);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qForwardUnregPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                setValFsDot1qForwardUnregPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qForwardUnregPort (INT4 i4FsDot1qVlanContextId,
                               UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                               INT4 i4SetValFsDot1qForwardUnregPort)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                          &u4ContextId,
                                          &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pCurrEntry->UnRegGrps.u1RowStatus == NOT_IN_SERVICE)
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_FORWARD_UNREG_TABLE) &&
                (pTmpPortList->PortListTbl.FwdTbl.VlanId == VlanId))
            {
                switch (i4SetValFsDot1qForwardUnregPort)
                {
                    case VLAN_ADD_MEMBER_PORT:
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_ADD_FORBIDDEN_PORT:
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_DEL_MEMBER_PORT:
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.StaticPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_DEL_FORBIDDEN_PORT:
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.FwdTbl.ForbiddenPorts,
                             u2LocalPortId);
                        break;

                    default:

                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                }

                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (i4SetValFsDot1qForwardUnregPort);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qStaticUnicastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                

                The Object 
                setValFsDot1qStaticUnicastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qStaticUnicastRowStatus (INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qFdbId,
                                     tMacAddr FsDot1qStaticUnicastAddress,
                                     INT4 i4FsDot1qStaticUnicastReceivePort,
                                     INT4 i4SetValFsDot1qStaticUnicastRowStatus)
{
    UINT1              *pAddPortList = NULL;
    UINT1              *pPorts = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    tVlanCurrEntry     *pVlanRec = NULL;
    tVlanStUcastEntry  *pNewStUcastEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanFdbEntry      *pVlanFdbEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4FidIndex;
    UINT4               u4ContextId;
    INT4                i4Result;
    tVlanId             VlanId = 0;
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    UINT4               au4IfPortArray[VLAN_PORT_LIST_SIZE];
    UINT2               u2NumPorts = 0;
#endif
#endif
    tMacAddr            ConnectionId;
    UINT2               u2LocalPortId = 0;
    UINT1               u1VlanLearningType;
#ifndef NPAPI_WANTED
    UINT1               u1PrvRowStatus = VLAN_ACTIVE;
#endif
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
    INT1                i1Action = 0;
#endif

    VLAN_PERF_MARK_START_TIME ();

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u1VlanLearningType = VlanGetVlanLearningMode ();

    if ((u1VlanLearningType == VLAN_SHARED_LEARNING) &&
        (u4FsDot1qFdbId != VLAN_SHARED_DEF_FDBID))
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

    pVlanStUcastEntry =
        VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2LocalPortId);

    switch (i4SetValFsDot1qStaticUnicastRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:

            if (pVlanStUcastEntry != NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            /*A new entry is being created. Check if the maximum limit is reached */
            else if (VlanIsStaticUcastMacLimitExceeded () == VLAN_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            pNewStUcastEntry = (tVlanStUcastEntry *) (VOID *)
                VLAN_GET_BUF (VLAN_ST_UCAST_ENTRY, sizeof (tVlanStUcastEntry));

            if (pNewStUcastEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Static Unicast Entry \n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            MEMSET (pNewStUcastEntry, 0, sizeof (tVlanStUcastEntry));
            MEMSET (ConnectionId, 0, sizeof (tMacAddr));

            VLAN_CPY_MAC_ADDR (pNewStUcastEntry->MacAddr,
                               FsDot1qStaticUnicastAddress);
            VLAN_CPY_MAC_ADDR (pNewStUcastEntry->ConnectionId, ConnectionId);
            pNewStUcastEntry->u2RcvPort = u2LocalPortId;
            MEMCPY (pNewStUcastEntry->AllowedToGo, gNullPortList,
                    sizeof (tLocalPortList));
            pNewStUcastEntry->u1RowStatus = VLAN_NOT_READY;
            pNewStUcastEntry->u1Status = VLAN_PERMANENT;
            pNewStUcastEntry->VlanId = (tVlanId) u4FsDot1qFdbId;

            /* Adding the node to the static entries list */
            pNewStUcastEntry->pNextNode = pFidEntry->pStUcastTbl;
            pFidEntry->pStUcastTbl = pNewStUcastEntry;

            /* Adding the node to RBTree */

            if (RBTreeAdd (VLAN_CURR_CONTEXT_PTR ()->VlanStaticUnicastInfo,
                           (tRBElem *) pNewStUcastEntry) == RB_FAILURE)
            {
                VLAN_RELEASE_BUF (VLAN_ST_UCAST_ENTRY,
                                  (UINT1 *) pVlanStUcastEntry);
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "Unable to add Static Unicast info to global RBTree \n");
                VlanReleaseContext ();
                return SNMP_FAILURE;

            }

            /* Create Tempportlist to modify the portlist */
            if (VlanStaticUcastCreateTempPortList
                (u4FsDot1qFdbId, u2LocalPortId, FsDot1qStaticUnicastAddress,
                 gNullPortList, pNewStUcastEntry->u1Status,
                 pNewStUcastEntry->ConnectionId) != VLAN_SUCCESS)
            {
                VLAN_RELEASE_BUF (VLAN_ST_UCAST_ENTRY,
                                  (UINT1 *) pVlanStUcastEntry);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            VlanUpdateCurrentStaticUcastMacCount (VLAN_ADD);
            break;

        case VLAN_NOT_IN_SERVICE:

            if (pVlanStUcastEntry == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (pVlanStUcastEntry->u1RowStatus == VLAN_NOT_READY)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (pVlanStUcastEntry->u1RowStatus ==
                i4SetValFsDot1qStaticUnicastRowStatus)
            {
                VlanReleaseContext ();
                VLAN_PERF_MARK_END_TIME ();
                return SNMP_SUCCESS;
            }

            /* Create Tempportlist to modify the portlist */
            if (VlanStaticUcastCreateTempPortList
                (u4FsDot1qFdbId, u2LocalPortId, FsDot1qStaticUnicastAddress,
                 pVlanStUcastEntry->AllowedToGo,
                 pVlanStUcastEntry->u1Status,
                 pVlanStUcastEntry->ConnectionId) != VLAN_SUCCESS)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (pVlanStUcastEntry->u1Status == VLAN_DELETE_ON_TIMEOUT)
            {
                /* If the status is DELETE_ON_TIMEOUT, to avoid aging timeout
                 * we have to make the entry as permenant */
                pVlanStUcastEntry->u1Status = VLAN_PERMANENT;
            }
            pVlanStUcastEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            break;

        case ACTIVE:

            if (pVlanStUcastEntry == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (pVlanStUcastEntry->u1RowStatus ==
                i4SetValFsDot1qStaticUnicastRowStatus)
            {
                VlanReleaseContext ();
                VLAN_PERF_MARK_END_TIME ();
                return SNMP_SUCCESS;
            }

            VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList,
                           tVlanTempPortList *)
            {
                if ((pTmpPortList->u4ContextId ==
                     (UINT4) i4FsDot1qVlanContextId) &&
                    (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE) &&
                    (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId ==
                     u4FsDot1qFdbId) &&
                    (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort ==
                     u2LocalPortId) &&
                    (VLAN_ARE_MAC_ADDR_EQUAL
                     (pTmpPortList->PortListTbl.StUcastTbl.MacAddr,
                      FsDot1qStaticUnicastAddress)))
                {
                    break;
                }
            }

            if (pTmpPortList == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
            if (pPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "nmhSetFsDot1qStaticUnicastRowStatus: Error in allocating memory "
                          "for pPorts\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pPorts, 0, sizeof (tLocalPortList));
            MEMCPY (pPorts, pTmpPortList->PortListTbl.StUcastTbl.AllowedToGo,
                    sizeof (tLocalPortList));

            /* Create the entry in the H/W Static Unicast Table */
            i4Result =
                VlanHwAddStaticUcastEntry
                (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress, u2LocalPortId,
                 pPorts, pTmpPortList->PortListTbl.StUcastTbl.u1Status,
                 pTmpPortList->PortListTbl.StUcastTbl.ConnectionId);

            if (i4Result == VLAN_FAILURE)
            {
                /* Delete the TempPortList entry */
                VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));
                VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY,
                                  (UINT1 *) pTmpPortList);

                if (pVlanStUcastEntry->u1RowStatus == VLAN_NOT_READY)
                {
                    /* Since this is a new entry delete the Entry itself */
                    VlanDeleteStUcastEntry (pFidEntry, pVlanStUcastEntry);
                }
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pPorts);
                return SNMP_FAILURE;
            }
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            i1Action = VLAN_ADD;
#endif
            VLAN_GET_TIMESTAMP (&pVlanStUcastEntry->u4TimeStamp);
            MEMCPY (pVlanStUcastEntry->AllowedToGo,
                    pPorts, sizeof (tLocalPortList));

            pVlanStUcastEntry->u1Status =
                pTmpPortList->PortListTbl.StUcastTbl.u1Status;

            MEMCPY (pVlanStUcastEntry->ConnectionId,
                    pTmpPortList->PortListTbl.StUcastTbl.ConnectionId,
                    sizeof (tMacAddr));

            /* Add the node to the corresponding FDB table if it doesnot 
             * have an entry in it. */
#ifndef NPAPI_WANTED
            /* storing the previous status for futher reference */
            u1PrvRowStatus = pVlanStUcastEntry->u1RowStatus;

            pVlanFdbEntry = VlanGetUcastEntry (u4FidIndex,
                                               FsDot1qStaticUnicastAddress);

            if (pVlanFdbEntry != NULL)
            {
                if (u1PrvRowStatus == VLAN_NOT_READY)
                {
                    pVlanFdbEntry->u1StRefCount++;
                    pVlanFdbEntry->u1Status = VLAN_FDB_MGMT;
                }
            }
            else
            {
                /* add the FDB entry here */
                pVlanFdbEntry = (tVlanFdbEntry *) (VOID *) VLAN_GET_BUF
                    (VLAN_FDB_ENTRY, sizeof (tVlanFdbEntry));

                if (pVlanFdbEntry == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME, "No Free FDB Entry \n");
                    VLAN_RELEASE_BUF (VLAN_ST_UCAST_ENTRY,
                                      (UINT1 *) pVlanStUcastEntry);
                    VlanUpdateCurrentStaticUcastMacCount (VLAN_DELETE);
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pPorts);
                    return SNMP_FAILURE;
                }

                MEMSET (pVlanFdbEntry, 0, sizeof (tVlanFdbEntry));

                VLAN_GET_TIMESTAMP (&pVlanFdbEntry->u4TimeStamp);
                VLAN_CPY_MAC_ADDR (pVlanFdbEntry->MacAddr,
                                   FsDot1qStaticUnicastAddress);

                pVlanFdbEntry->u2Port = 0;
                pVlanFdbEntry->VlanId = 0;
                pVlanFdbEntry->u1Status = VLAN_FDB_MGMT;
                pVlanFdbEntry->u1StRefCount = 1;

                VlanAddFdbEntry (u4FidIndex, pVlanFdbEntry);
            }
#endif
            pVlanStUcastEntry->pFdbEntry = pVlanFdbEntry;

#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
            VlanGetVlanIdFromFdbId (u4FsDot1qFdbId, &VlanId);
            VlanConvertLocalPortListToArray (pPorts,
                                             au4IfPortArray, &u2NumPorts);
            VlanFdbTableAddInCtxt ((UINT2) au4IfPortArray[VLAN_INIT_VAL],
                                   FsDot1qStaticUnicastAddress,
                                   VlanId, VLAN_FDB_MGMT,
                                   pVlanStUcastEntry->ConnectionId,
                                   VLAN_LOCAL_FDB, FNP_ZERO, FNP_ZERO);
#endif
#endif

            /* delete the TempPortList entry */
            VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));
            VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY, (UINT1 *) pTmpPortList);

            pVlanStUcastEntry->u1RowStatus = VLAN_ACTIVE;

            /* Propagate Ucast information to other modules only when
             * receive port is zero. */
            if ((VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE)
                && (VLAN_DEF_RECVPORT == pVlanStUcastEntry->u2RcvPort))
            {
                pAddPortList =
                    UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

                if (pAddPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "nmhSetFsDot1qStaticUnicastRowStatus: Error in allocating memory "
                              "for pAddPortList\r\n");
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pPorts);
                    return SNMP_FAILURE;
                }
                MEMSET (pAddPortList, 0, sizeof (tLocalPortList));

                MEMCPY (pAddPortList, pVlanStUcastEntry->AllowedToGo,
                        sizeof (tLocalPortList));

                VlanPropagateMacInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                              FsDot1qStaticUnicastAddress,
                                              (tVlanId) u4FsDot1qFdbId,
                                              pAddPortList, gNullPortList);
                UtilPlstReleaseLocalPortList (pAddPortList);
            }
            UtilPlstReleaseLocalPortList (pPorts);

            break;
        case VLAN_DESTROY:

            if (pVlanStUcastEntry == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            VlanGetVlanIdFromFdbId (u4FsDot1qFdbId, &VlanId);
            pVlanRec = VlanGetVlanEntry (VlanId);
            if (pVlanRec == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                          "Vlan Is Not Active\n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            if (VlanHandleStUcastDeletion (pVlanRec, pVlanStUcastEntry) ==
                VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                          "Deletion of static unicast "
                          "entry for this vlan Failed\n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            VlanDeleteStUcastEntry (pFidEntry, pVlanStUcastEntry);
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
            VlanHandleDeleteFdbEntry (VlanId, FsDot1qStaticUnicastAddress,
                                      VLAN_LOCAL_FDB);
#endif /* SW_LEARNING */
#endif /* NPAPI_WANTED */
            break;
        default:
            VlanReleaseContext ();
            return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticUnicastRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 4,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i", i4FsDot1qVlanContextId,
                      u4FsDot1qFdbId, FsDot1qStaticUnicastAddress,
                      i4FsDot1qStaticUnicastReceivePort,
                      i4SetValFsDot1qStaticUnicastRowStatus));

#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
    VlanRedSyncStUcastSwUpdate (i4FsDot1qVlanContextId,
                                u4FsDot1qFdbId,
                                FsDot1qStaticUnicastAddress,
                                VLAN_GET_IFINDEX
                                (i4FsDot1qStaticUnicastReceivePort), i1Action);
#endif /*L2RED_WANTED */

    VlanReleaseContext ();

    VLAN_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qStaticUnicastStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                

                The Object 
                setValFsDot1qStaticUnicastStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qStaticUnicastStatus (INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qFdbId,
                                  tMacAddr FsDot1qStaticUnicastAddress,
                                  INT4 i4FsDot1qStaticUnicastReceivePort,
                                  INT4 i4SetValFsDot1qStaticUnicastStatus)
{
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyRowStatusInfo;
    UINT4               u4FidIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal;

    VLAN_PERF_MARK_START_TIME ();

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanStUcastEntry =
        VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2LocalPortId);

    /* If StUcast entry is not there we can simply call the SI routine */
    if ((pVlanStUcastEntry == NULL) ||
        ((pVlanStUcastEntry->u1RowStatus == VLAN_ACTIVE) &&
         (i4SetValFsDot1qStaticUnicastStatus == VLAN_MGMT_INVALID)))
    {
        i1RetVal =
            nmhSetDot1qStaticUnicastStatus
            (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress,
             (INT4) u2LocalPortId, i4SetValFsDot1qStaticUnicastStatus);
        VlanReleaseContext ();
        return i1RetVal;
    }

    /* If StUcast entry is there we have to update the static unicast 
     * entry depending upon the status */
    if (VlanMiUpdateStaticUcastEntry
        (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress, u2LocalPortId,
         i4SetValFsDot1qStaticUnicastStatus) == VLAN_FAILURE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    /* Send UnicastRowStatus modification trigger to MSR when
     * i4SetValDot1qStaticUnicastStatus value is deleteOnTimeout or
     * deleteOnReset.*/

    if ((i4SetValFsDot1qStaticUnicastStatus == VLAN_DELETE_ON_TIMEOUT) ||
        (i4SetValFsDot1qStaticUnicastStatus == VLAN_DELETE_ON_RESET) ||
        (i4SetValFsDot1qStaticUnicastStatus == VLAN_MGMT_INVALID))
    {
        VLAN_MEMSET (&SnmpNotifyRowStatusInfo, 0, sizeof (tSnmpNotifyInfo));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyRowStatusInfo,
                              FsDot1qStaticUnicastRowStatus,
                              0, TRUE, VlanLock, VlanUnLock, 4, SNMP_SUCCESS);
        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyRowStatusInfo, "%i %u %m %i %i",
                              VLAN_CURR_CONTEXT_ID (), u4FsDot1qFdbId,
                              FsDot1qStaticUnicastAddress,
                              i4FsDot1qStaticUnicastReceivePort, VLAN_DESTROY));
    }
    else
    {
        UNUSED_PARAM (SnmpNotifyRowStatusInfo);
    }

    VlanReleaseContext ();
    VLAN_PERF_MARK_END_TIME ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qStaticAllowedIsMember
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
                

                The Object 
                setValFsDot1qStaticAllowedIsMember
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qStaticAllowedIsMember (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qFdbId,
                                    tMacAddr FsDot1qStaticUnicastAddress,
                                    INT4 i4FsDot1qStaticUnicastReceivePort,
                                    INT4 i4FsDot1qTpPort,
                                    INT4 i4SetValFsDot1qStaticAllowedIsMember)
{
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    UINT4               u4FidIndex;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT2               u2LocalPortId;
    UINT2               u2RcvLocalPortId;
    UINT1               u1VlanLearningType;

    VLAN_PERF_MARK_START_TIME ();

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = (UINT4) i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u1VlanLearningType = VlanGetVlanLearningMode ();

    if ((u1VlanLearningType == VLAN_SHARED_LEARNING) &&
        (u4FsDot1qFdbId == VLAN_SHARED_DEF_FDBID))
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2RcvLocalPortId);

    if (pVlanStUcastEntry != NULL)
    {
        if ((pVlanStUcastEntry->u1RowStatus == VLAN_NOT_IN_SERVICE) ||
            (pVlanStUcastEntry->u1RowStatus == VLAN_NOT_READY))
        {
            VLAN_SLL_SCAN (&gVlanTempPortList,
                           pTmpPortList, tVlanTempPortList *)
            {
                if ((pTmpPortList->u4ContextId ==
                     (UINT4) i4FsDot1qVlanContextId)
                    && (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE)
                    && (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId ==
                        u4FsDot1qFdbId)
                    && (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort ==
                        u2RcvLocalPortId)
                    &&
                    (VLAN_ARE_MAC_ADDR_EQUAL
                     (pTmpPortList->PortListTbl.StUcastTbl.MacAddr,
                      FsDot1qStaticUnicastAddress)))
                {
                    if (i4SetValFsDot1qStaticAllowedIsMember == VLAN_TRUE)
                    {
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.StUcastTbl.AllowedToGo,
                             u2LocalPortId);
                    }
                    else
                    {
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.StUcastTbl.AllowedToGo,
                             u2LocalPortId);
                    }
                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                          FsDot1qStaticAllowedIsMember,
                                          u4SeqNum, FALSE, VlanLock, VlanUnLock,
                                          5, SNMP_SUCCESS);
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i %i",
                                      i4FsDot1qVlanContextId, u4FsDot1qFdbId,
                                      FsDot1qStaticUnicastAddress,
                                      i4FsDot1qStaticUnicastReceivePort,
                                      i4FsDot1qTpPort,
                                      i4SetValFsDot1qStaticAllowedIsMember));
                    VlanReleaseContext ();
                    VLAN_PERF_MARK_END_TIME ();
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qStaticMulticastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                

                The Object 
                setValFsDot1qStaticMulticastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qStaticMulticastRowStatus (INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       tMacAddr FsDot1qStaticMulticastAddress,
                                       INT4
                                       i4FsDot1qStaticMulticastReceivePort,
                                       INT4
                                       i4SetValFsDot1qStaticMulticastRowStatus)
{
    UINT1              *pau1StaticPortList = NULL;
    UINT1              *pau1ForbiddenPortList = NULL;
    UINT4               u4ContextId;
    INT4                i4Status = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSNMP_OCTET_STRING_TYPE StaticPortList;
    tSNMP_OCTET_STRING_TYPE ForbiddenPortList;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    switch (i4SetValFsDot1qStaticMulticastRowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
        case VLAN_NOT_IN_SERVICE:

            if (i4SetValFsDot1qStaticMulticastRowStatus == VLAN_CREATE_AND_WAIT)
            {
                nmhGetDot1qStaticMulticastStatus
                    (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
                     (INT4) u2LocalPortId, &i4Status);

                if (i4Status != 0)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME, "Static entry already exists\n");
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                /* Since the entry itself not present we have to create the
                 * entry, and let the entry run with all ports, meanwhile
                 * we can update the new portlist */
                if (VlanStaticMulticastStatus (u4FsDot1qVlanIndex,
                                               FsDot1qStaticMulticastAddress,
                                               (INT4) u2LocalPortId,
                                               VLAN_PERMANENT) == VLAN_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

            pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort
                (FsDot1qStaticMulticastAddress, u2LocalPortId, pCurrEntry);

            if (pStMcastEntry == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (pStMcastEntry->u1RowStatus ==
                i4SetValFsDot1qStaticMulticastRowStatus)
            {
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }

            pTmpPortList =
                (tVlanTempPortList *) (VOID *)
                VLAN_GET_BUF (VLAN_TEMP_PORTLIST_ENTRY, 0);

            if (pTmpPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Temp PortList Entry \n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            MEMSET (pTmpPortList, 0, sizeof (tVlanTempPortList));

            VLAN_SLL_INIT_NODE ((tTMO_SLL_NODE *) (VOID *) pTmpPortList);

            pTmpPortList->u4ContextId = i4FsDot1qVlanContextId;
            pTmpPortList->u1Type = VLAN_ST_MCAST_TABLE;
            pTmpPortList->PortListTbl.StMcastTbl.VlanId = VlanId;
            pTmpPortList->PortListTbl.StMcastTbl.u2RcvPort = u2LocalPortId;
            VLAN_CPY_MAC_ADDR (pTmpPortList->PortListTbl.StMcastTbl.MacAddr,
                               FsDot1qStaticMulticastAddress);

            if (i4SetValFsDot1qStaticMulticastRowStatus == VLAN_CREATE_AND_WAIT)
            {
                MEMCPY (pTmpPortList->PortListTbl.StMcastTbl.StaticPorts,
                        gNullPortList, VLAN_PORT_LIST_SIZE);
            }
            else
            {
                MEMCPY (pTmpPortList->PortListTbl.StMcastTbl.StaticPorts,
                        pStMcastEntry->EgressPorts, VLAN_PORT_LIST_SIZE);
            }

            MEMCPY (pTmpPortList->PortListTbl.StMcastTbl.ForbiddenPorts,
                    pStMcastEntry->ForbiddenPorts, VLAN_PORT_LIST_SIZE);

            VLAN_SLL_ADD (&gVlanTempPortList,
                          (tTMO_SLL_NODE *) & (pTmpPortList->NextNode));

            pStMcastEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            break;
        case VLAN_ACTIVE:

            pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort
                (FsDot1qStaticMulticastAddress, u2LocalPortId, pCurrEntry);

            if (pStMcastEntry == NULL)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (pStMcastEntry->u1RowStatus ==
                i4SetValFsDot1qStaticMulticastRowStatus)
            {
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }

            VLAN_SLL_SCAN (&gVlanTempPortList,
                           pTmpPortList, tVlanTempPortList *)
            {
                if ((pTmpPortList->u4ContextId ==
                     (UINT4) i4FsDot1qVlanContextId) &&
                    (pTmpPortList->u1Type == VLAN_ST_MCAST_TABLE) &&
                    (pTmpPortList->PortListTbl.StMcastTbl.VlanId == VlanId) &&
                    (pTmpPortList->PortListTbl.StMcastTbl.u2RcvPort ==
                     u2LocalPortId) &&
                    (VLAN_ARE_MAC_ADDR_EQUAL
                     (pTmpPortList->PortListTbl.StMcastTbl.MacAddr,
                      FsDot1qStaticMulticastAddress)))
                {
                    break;
                }
            }

            if (pTmpPortList != NULL)
            {
                pau1ForbiddenPortList =
                    UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
                if (pau1ForbiddenPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "nmhSetFsDot1qStaticMulticastRowStatus: "
                              "Error in allocating memory for pau1ForbiddenPortList\r\n");
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                MEMSET (pau1ForbiddenPortList, 0, VLAN_PORT_LIST_SIZE);

                pau1StaticPortList =
                    UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
                if (pau1StaticPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "nmhSetFsDot1qStaticMulticastRowStatus: "
                              "Error in allocating memory for pau1StaticPortList\r\n");
                    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                MEMSET (pau1StaticPortList, 0, VLAN_PORT_LIST_SIZE);
                MEMCPY (pau1StaticPortList,
                        pTmpPortList->PortListTbl.StMcastTbl.StaticPorts,
                        VLAN_PORT_LIST_SIZE);
                StaticPortList.i4_Length = VLAN_PORT_LIST_SIZE;
                StaticPortList.pu1_OctetList = pau1StaticPortList;

                MEMCPY (pau1ForbiddenPortList,
                        pTmpPortList->PortListTbl.StMcastTbl.ForbiddenPorts,
                        VLAN_PORT_LIST_SIZE);
                ForbiddenPortList.i4_Length = VLAN_PORT_LIST_SIZE;
                ForbiddenPortList.pu1_OctetList = pau1ForbiddenPortList;

                if (VlanStaticMulticastStaticEgressPorts
                    (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
                     (INT4) u2LocalPortId, &StaticPortList) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                    UtilPlstReleaseLocalPortList (pau1StaticPortList);
                    return SNMP_FAILURE;
                }
                if (VlanStaticMulticastForbiddenEgressPorts
                    (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
                     (INT4) u2LocalPortId, &ForbiddenPortList) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                    UtilPlstReleaseLocalPortList (pau1StaticPortList);
                    return SNMP_FAILURE;
                }

                VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));

                VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY,
                                  (UINT1 *) pTmpPortList);
                pTmpPortList = NULL;
                pStMcastEntry->u1RowStatus = VLAN_ACTIVE;
                UtilPlstReleaseLocalPortList (pau1ForbiddenPortList);
                UtilPlstReleaseLocalPortList (pau1StaticPortList);
            }
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            VlanRedSyncMcastSwUpdate (i4FsDot1qVlanContextId,
                                      u4FsDot1qVlanIndex,
                                      FsDot1qStaticMulticastAddress,
                                      i4FsDot1qStaticMulticastReceivePort,
                                      VLAN_ADD);
#endif /*L2RED_WANTED */

            break;
        default:
            VlanReleaseContext ();
            return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticMulticastRowStatus,
                          u4SeqNum, TRUE, VlanLock, VlanUnLock, 4,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i", i4FsDot1qVlanContextId,
                      u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
                      i4FsDot1qStaticMulticastReceivePort,
                      i4SetValFsDot1qStaticMulticastRowStatus));
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qStaticMulticastStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                

                The Object 
                setValFsDot1qStaticMulticastStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qStaticMulticastStatus (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qVlanIndex,
                                    tMacAddr FsDot1qStaticMulticastAddress,
                                    INT4 i4FsDot1qStaticMulticastReceivePort,
                                    INT4 i4SetValFsDot1qStaticMulticastStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qStaticMulticastStatus (u4FsDot1qVlanIndex,
                                                 FsDot1qStaticMulticastAddress,
                                                 (INT4) u2LocalPortId,
                                                 i4SetValFsDot1qStaticMulticastStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qStaticMcastPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
                

                The Object 
                setValFsDot1qStaticMcastPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qStaticMcastPort (INT4 i4FsDot1qVlanContextId,
                              UINT4 u4FsDot1qVlanIndex,
                              tMacAddr FsDot1qStaticMulticastAddress,
                              INT4 i4FsDot1qStaticMulticastReceivePort,
                              INT4 i4FsDot1qTpPort,
                              INT4 i4SetValFsDot1qStaticMcastPort)
{
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanId             VlanId = (tVlanId) u4FsDot1qVlanIndex;
    UINT2               u2LocalPortId;
    UINT2               u2RcvLocalPortId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = (UINT4) i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pVlanEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pStMcastEntry =
        VlanGetStMcastEntryWithExactRcvPort (FsDot1qStaticMulticastAddress,
                                             u2RcvLocalPortId, pVlanEntry);

    if (pStMcastEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pStMcastEntry->u1RowStatus == VLAN_NOT_IN_SERVICE)
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_ST_MCAST_TABLE) &&
                (pTmpPortList->PortListTbl.StMcastTbl.VlanId == VlanId) &&
                (pTmpPortList->PortListTbl.StMcastTbl.u2RcvPort ==
                 u2RcvLocalPortId) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.StMcastTbl.MacAddr,
                  FsDot1qStaticMulticastAddress)))
            {
                switch (i4SetValFsDot1qStaticMcastPort)
                {
                    case VLAN_ADD_MEMBER_PORT:
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.StMcastTbl.StaticPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_ADD_FORBIDDEN_PORT:
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.
                             StMcastTbl.ForbiddenPorts, u2LocalPortId);
                        break;

                    case VLAN_DEL_MEMBER_PORT:
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.StMcastTbl.StaticPorts,
                             u2LocalPortId);
                        break;

                    case VLAN_DEL_FORBIDDEN_PORT:
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.
                             StMcastTbl.ForbiddenPorts, u2LocalPortId);
                        break;

                    default:
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                }
                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticMcastPort,
                                      u4SeqNum, FALSE, VlanLock, VlanUnLock, 5,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i %i",
                                  i4FsDot1qVlanContextId, u4FsDot1qVlanIndex,
                                  FsDot1qStaticMulticastAddress,
                                  i4FsDot1qStaticMulticastReceivePort,
                                  i4FsDot1qTpPort,
                                  i4SetValFsDot1qStaticMcastPort));
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
                VlanRedSyncMcastSwUpdate (i4FsDot1qVlanContextId,
                                          u4FsDot1qVlanIndex,
                                          FsDot1qStaticMulticastAddress,
                                          VLAN_GET_PHY_PORT
                                          (i4FsDot1qStaticMulticastReceivePort),
                                          VLAN_ADD);
#endif /*L2RED_WANTED */

                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qVlanStaticName
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                setValFsDot1qVlanStaticName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qVlanStaticName (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qVlanIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsDot1qVlanStaticName)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qVlanStaticName
        (u4FsDot1qVlanIndex, pSetValFsDot1qVlanStaticName);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qVlanStaticRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                setValFsDot1qVlanStaticRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qVlanStaticRowStatus (INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qVlanIndex,
                                  INT4 i4SetValFsDot1qVlanStaticRowStatus)
{
    tPortList          *pEgressPorts = NULL;
    tPortList          *pForbiddenPorts = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tVlanId             VlanId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;
    INT4                i4RetVal;
    tMacAddr            McastAddr;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
    INT1                i1Action = 0;
#endif

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    VlanId = (tVlanId) u4FsDot1qVlanIndex;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

    if (pStVlanEntry != NULL)
    {
        if (pStVlanEntry->u1RowStatus ==
            (UINT1) i4SetValFsDot1qVlanStaticRowStatus)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        if ((i4SetValFsDot1qVlanStaticRowStatus == VLAN_CREATE_AND_WAIT) ||
            (i4SetValFsDot1qVlanStaticRowStatus == VLAN_CREATE_AND_GO))

        {
#ifdef L2RED_WANTED
            /* In case of standby node, we can ignore the default
             * vlan syncup failure, because its already created in
             * standby
             */
            if ((VlanRmGetNodeState () == RM_STANDBY))
            {
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
#endif
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    else
    {

        /* this static vlan entry is not there in the database */

        if ((i4SetValFsDot1qVlanStaticRowStatus != VLAN_CREATE_AND_WAIT) &&
            (i4SetValFsDot1qVlanStaticRowStatus != VLAN_CREATE_AND_GO))
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetFsDot1qVlanStaticRowStatus: "
                  "Error in allocating memory for pEgressPorts\r\n");
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    MEMSET (pEgressPorts, 0, sizeof (tPortList));

    pForbiddenPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pForbiddenPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhSetFsDot1qVlanStaticRowStatus: "
                  "Error in allocating memory for pForbiddenPorts\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    MEMSET (pForbiddenPorts, 0, sizeof (tPortList));

    switch (i4SetValFsDot1qVlanStaticRowStatus)
    {

        case VLAN_CREATE_AND_WAIT:
            VLAN_PBB_PERF_MARK_START_TIME ();
            VLAN_PBB_PERF_RESET (gu4CreateBVLANTimeTaken);
            VLAN_PBB_PERF_RESET (gu4CreateBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_RESET (gu4VlanIsidTimeTaken);

            pStVlanEntry = (tStaticVlanEntry *) (VOID *)
                VlanCustGetBuf (VlanId, VLAN_ST_VLAN_ENTRY, sizeof
                                (tStaticVlanEntry));

            if (pStVlanEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Static VLAN Entry \n");

                VlanReleaseContext ();
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            MEMSET (pStVlanEntry, 0, sizeof (tStaticVlanEntry));

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            pStVlanEntry->VlanId = VlanId;
            /* Initialize u1NameModifiedFlag with default value(VLAN_FALSE) */
            pStVlanEntry->u1NameModifiedFlag = VLAN_FALSE;
            pStVlanEntry->u1ServiceType = VLAN_E_LAN;
            pStVlanEntry->pNextNode = gpVlanContextInfo->pStaticVlanTable;
            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                pStVlanEntry->u2EgressEtherType = VLAN_PROVIDER_PROTOCOL_ID;
            }
            else
            {
                pStVlanEntry->u2EgressEtherType = VLAN_PROTOCOL_ID;
            }
            gpVlanContextInfo->pStaticVlanTable = pStVlanEntry;

            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
            break;

        case VLAN_NOT_IN_SERVICE:

            if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {

                i4RetVal = VlanMIDelStVlanInfo (pStVlanEntry);

                if (i4RetVal == VLAN_FAILURE)
                {
                    VlanReleaseContext ();
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                pStVlanEntry->pCurrEntry = NULL;

                /* Indicate GVRP about the VLAN Deletion */

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                VlanAttrRPSetVlanForbiddenPorts (VLAN_CURR_CONTEXT_ID (),
                                                 pStVlanEntry->VlanId,
                                                 gNullPortList,
                                                 pu1LocalPortList);
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId,
                                               gNullPortList, pu1LocalPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);

                if ((SNOOP_ENABLED ==
                     VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
                    || (SNOOP_ENABLED ==
                        VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID
                                                       ())))
                {
                    MEMSET (McastAddr, 0, sizeof (tMacAddr));

                    /* For MI the port list to the externa modules should be 
                       physical port list so convert and pass it */
                    VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pStVlanEntry,
                                                          *pForbiddenPorts);
                    VLAN_CONVERT_EGRESS_TO_IFPORTLIST (pStVlanEntry,
                                                       *pEgressPorts);

                    VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId, McastAddr,
                                               *pForbiddenPorts,
                                               *pEgressPorts,
                                               VLAN_UPDT_VLAN_PORTS);
                }
            }

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;
            break;

        case VLAN_ACTIVE:

            VLAN_PBB_PERF_MARK_START_TIME ();
            if (pStVlanEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
            {
                VlanReleaseContext ();
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }
            i4RetVal = VlanAddCurrEntry (pStVlanEntry);

            if (i4RetVal == VLAN_FAILURE)
            {
                VlanReleaseContext ();
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            VLAN_MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

            L2PvlanMappingInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
            L2PvlanMappingInfo.InVlanId = VlanId;
            L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;
            VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

            /* Handle the pvlan association also. the port isloation 
             *              * table updation is handled inside this function */
            VlanUpdatePortIsolation (&L2PvlanMappingInfo);

            pStVlanEntry->u1RowStatus = VLAN_ACTIVE;

            /* 
             * Propagate the static vlan info eventhough the curr vlan entry
             * already exists.         
             */
            if (L2PvlanMappingInfo.u1VlanType == L2IWF_NORMAL_VLAN)
            {

                if (VLAN_MODULE_STATUS () == VLAN_ENABLED)
                {
                    pu1LocalPortList = UtilPlstAllocLocalPortList
                        (sizeof (tLocalPortList));
                    if (pu1LocalPortList == NULL)
                    {
                        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                  "Memory allocation failed for tLocalPortList\n");
                        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                        FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                    VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                    VlanAttrRPSetVlanForbiddenPorts
                        (VLAN_CURR_CONTEXT_ID (),
                         pStVlanEntry->VlanId, pu1LocalPortList, gNullPortList);

                    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                    VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                    VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                                   pStVlanEntry->VlanId,
                                                   pu1LocalPortList,
                                                   gNullPortList);
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                }
            }

            if (SNOOP_ENABLED ==
                VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                || SNOOP_ENABLED ==
                VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
            {
                MEMSET (McastAddr, 0, sizeof (tMacAddr));

                /* For MI the port list to the externa modules should be 
                   physical port list so convert and pass it */
                VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pStVlanEntry,
                                                      *pForbiddenPorts);
                VLAN_CONVERT_EGRESS_TO_IFPORTLIST (pStVlanEntry, *pEgressPorts);

                VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                           pStVlanEntry->VlanId, McastAddr,
                                           *pEgressPorts,
                                           *pForbiddenPorts,
                                           VLAN_UPDT_VLAN_PORTS);
            }
            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4CreateBVLANTunnelTimeTaken);
            VLAN_PBB_PERF_MARK_END_TIME (gu4VlanIsidTimeTaken);
            VLAN_PBB_PERF_PRINT_TIME (gu4CreateBVLANTimeTaken);
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            i1Action = VLAN_ADD;
#endif
            break;

        case VLAN_DESTROY:

            /* Not to allow default VLAN entry deletion */
            if (pStVlanEntry->VlanId == VLAN_DEFAULT_PORT_VID)
            {
                VlanReleaseContext ();
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }
            VLAN_PBB_PERF_MARK_START_TIME ();

            if (pStVlanEntry->u1RowStatus == VLAN_ACTIVE)
            {
                /* if this vlan is associated with pvlan, then
                 * delete that also. Default PVID configuration will taken care
                 * inside the VlanDelStVlanInfo function. So no need configure
                 * explicitly inside the below function. */
                VlanL2IwfDeletePvlanMapping (VLAN_CURR_CONTEXT_ID (),
                                             pStVlanEntry->VlanId);

                i4RetVal = VlanDelStVlanInfo (pStVlanEntry);

                if (i4RetVal == VLAN_FAILURE)
                {
                    VlanReleaseContext ();
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    return SNMP_FAILURE;
                }

                /* Indicate GVRP about the VLAN Deletion */

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                VlanAttrRPSetVlanForbiddenPorts (VLAN_CURR_CONTEXT_ID (),
                                                 pStVlanEntry->VlanId,
                                                 gNullPortList,
                                                 pu1LocalPortList);

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId,
                                               gNullPortList, pu1LocalPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }

            VlanDeleteStVlanEntry (pStVlanEntry);
            VLAN_PBB_PERF_MARK_END_TIME (gu4DelVlanIsidTimeTaken);
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            i1Action = VLAN_DELETE;
#endif
            break;
        case VLAN_CREATE_AND_GO:
            pStVlanEntry = (tStaticVlanEntry *) (VOID *)
                VlanCustGetBuf (VlanId, VLAN_ST_VLAN_ENTRY, sizeof
                                (tStaticVlanEntry));

            if (pStVlanEntry == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Static VLAN Entry \n");

                VlanReleaseContext ();
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            MEMSET (pStVlanEntry, 0, sizeof (tStaticVlanEntry));

            if (VlanId == VLAN_DEF_VLAN_ID)
            {
                VLAN_SET_ALL_EGRESS_PORTS (pStVlanEntry);
                VLAN_SET_ALL_UNTAGGED_PORTS (pStVlanEntry);
            }

            pStVlanEntry->u1RowStatus = VLAN_NOT_IN_SERVICE;

            pStVlanEntry->VlanId = VlanId;
            pStVlanEntry->pNextNode = gpVlanContextInfo->pStaticVlanTable;
            gpVlanContextInfo->pStaticVlanTable = pStVlanEntry;

            i4RetVal = VlanAddCurrEntry (pStVlanEntry);
            if (i4RetVal == VLAN_FAILURE)
            {
                VlanReleaseContext ();
                FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                return SNMP_FAILURE;
            }

            pStVlanEntry->u1RowStatus = VLAN_ACTIVE;

            /*
             * Propagate the static vlan info eventhough the curr vlan entry
             * already exists.
             */
            if (VLAN_MODULE_STATUS () == VLAN_ENABLED)
            {

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
                VlanAttrRPSetVlanForbiddenPorts (VLAN_CURR_CONTEXT_ID (),
                                                 pStVlanEntry->VlanId,
                                                 pu1LocalPortList,
                                                 gNullPortList);
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

                VlanPropagateVlanInfoToAttrRP (VLAN_CURR_CONTEXT_ID (),
                                               pStVlanEntry->VlanId,
                                               pu1LocalPortList, gNullPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }

            if (SNOOP_ENABLED ==
                VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
                || SNOOP_ENABLED ==
                VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
            {
                MEMSET (McastAddr, 0, sizeof (tMacAddr));

                /* For MI the port list to the externa modules should be 
                   physical port list so convert and pass it */
                VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST (pStVlanEntry,
                                                      *pForbiddenPorts);
                VLAN_CONVERT_EGRESS_TO_IFPORTLIST (pStVlanEntry, *pEgressPorts);

                VlanSnoopMiUpdatePortList (VLAN_CURR_CONTEXT_ID (),
                                           pStVlanEntry->VlanId, McastAddr,
                                           *pEgressPorts,
                                           *pForbiddenPorts,
                                           VLAN_UPDT_VLAN_PORTS);
            }
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
            i1Action = VLAN_ADD;
#endif
            break;

        default:
            VlanReleaseContext ();
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
            return SNMP_FAILURE;
    }

    if (i4SetValFsDot1qVlanStaticRowStatus != VLAN_DESTROY)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qVlanStaticRowStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 2,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", i4FsDot1qVlanContextId,
                          u4FsDot1qVlanIndex,
                          i4SetValFsDot1qVlanStaticRowStatus));
    }
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
    VlanRedSyncSwUpdate (VLAN_CURR_CONTEXT_ID (), u4FsDot1qVlanIndex, i1Action);
#endif /* L2RED_WANTED */

    VlanReleaseContext ();
    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    FsUtilReleaseBitList ((UINT1 *) pForbiddenPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qVlanStaticPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                setValFsDot1qVlanStaticPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qVlanStaticPort (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                             INT4 i4SetValFsDot1qVlanStaticPort)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tVlanPortEntry     *pPortEntry = NULL;
    tLocalPortList      AddedTaggedPorts;
    tLocalPortList      DeletedTaggedPorts;
#ifdef PVRST_WANTED
    UINT2               u2LocalPort;
    UINT4               u4ContextId;
#endif
    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (AddedTaggedPorts, 0, sizeof (tLocalPortList));
    MEMSET (DeletedTaggedPorts, 0, sizeof (tLocalPortList));

    L2PvlanMappingInfo.u4ContextId = i4FsDot1qVlanContextId;
    L2PvlanMappingInfo.InVlanId = (tVlanId) u4FsDot1qVlanIndex;
    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;

    VlanL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (VlanUtilMISetVlanStEgressPort (i4FsDot1qVlanContextId,
                                       u4FsDot1qVlanIndex,
                                       i4FsDot1qTpPort,
                                       i4SetValFsDot1qVlanStaticPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    VlanSelectContext (i4FsDot1qVlanContextId);

    if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
    {
        if ((i4SetValFsDot1qVlanStaticPort == VLAN_ADD_TAGGED_PORT) ||
            (i4SetValFsDot1qVlanStaticPort == VLAN_ADD_UNTAGGED_PORT))
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (i4FsDot1qTpPort);

            VlanSetPvlanProperties (pPortEntry, &L2PvlanMappingInfo,
                                    (UINT2) i4FsDot1qTpPort, VLAN_ADD);
        }
        else if ((i4SetValFsDot1qVlanStaticPort == VLAN_DEL_TAGGED_PORT) ||
                 (i4SetValFsDot1qVlanStaticPort == VLAN_DEL_UNTAGGED_PORT))
        {
            VlanSetPvlanProperties (NULL, &L2PvlanMappingInfo,
                                    (UINT2) i4FsDot1qTpPort, VLAN_DELETE);
        }
    }
#ifdef PVRST_WANTED
    /**  When a port is added as Tagged Port of a vlan send indication to pvrst to add the port to 
      the corresponding spanning-tree instance. On changing the port property send indication to delete it from 
      spanning-tree instance          **/
    if (VlanVcmGetContextInfoFromIfIndex
        (i4FsDot1qTpPort, &u4ContextId, &u2LocalPort) == VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    if (i4SetValFsDot1qVlanStaticPort == VLAN_ADD_TAGGED_PORT)
    {
        VLAN_SET_MEMBER_PORT (AddedTaggedPorts, u2LocalPort);
        PvrstSetTaggedPort (AddedTaggedPorts, DeletedTaggedPorts,
                            (UINT2) u4FsDot1qVlanIndex);
    }
    else if ((i4SetValFsDot1qVlanStaticPort == VLAN_DEL_TAGGED_PORT) ||
             (i4SetValFsDot1qVlanStaticPort == VLAN_ADD_UNTAGGED_PORT) ||
             (i4SetValFsDot1qVlanStaticPort == VLAN_ADD_ST_FORBIDDEN_PORT))
    {
        VLAN_SET_MEMBER_PORT (DeletedTaggedPorts, u2LocalPort);
        PvrstSetTaggedPort (AddedTaggedPorts, DeletedTaggedPorts,
                            (UINT2) u4FsDot1qVlanIndex);
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPvid
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPvid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPvid (INT4 i4FsDot1dBasePort, UINT4 u4SetValFsDot1qPvid)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qPvid ((INT4) u2LocalPortId, u4SetValFsDot1qPvid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPortAcceptableFrameTypes
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPortAcceptableFrameTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPortAcceptableFrameTypes (INT4 i4FsDot1dBasePort,
                                       INT4
                                       i4SetValFsDot1qPortAcceptableFrameTypes)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qPortAcceptableFrameTypes
        ((INT4) u2LocalPortId, i4SetValFsDot1qPortAcceptableFrameTypes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPortIngressFiltering
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPortIngressFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPortIngressFiltering (INT4 i4FsDot1dBasePort,
                                   INT4 i4SetValFsDot1qPortIngressFiltering)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qPortIngressFiltering
        ((INT4) u2LocalPortId, i4SetValFsDot1qPortIngressFiltering);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qConstraintType
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet

                The Object 
                setValFsDot1qConstraintType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qConstraintType (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qConstraintVlan,
                             INT4 i4FsDot1qConstraintSet,
                             INT4 i4SetValFsDot1qConstraintType)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qConstraintType (u4FsDot1qConstraintVlan,
                                          i4FsDot1qConstraintSet,
                                          i4SetValFsDot1qConstraintType);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qConstraintStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet

                The Object 
                setValFsDot1qConstraintStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qConstraintStatus (INT4 i4FsDot1qVlanContextId,
                               UINT4 u4FsDot1qConstraintVlan,
                               INT4 i4FsDot1qConstraintSet,
                               INT4 i4SetValFsDot1qConstraintStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qConstraintStatus (u4FsDot1qConstraintVlan,
                                            i4FsDot1qConstraintSet,
                                            i4SetValFsDot1qConstraintStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qConstraintSetDefault
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                setValFsDot1qConstraintSetDefault
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qConstraintSetDefault (INT4 i4FsDot1qVlanContextId,
                                   INT4 i4SetValFsDot1qConstraintSetDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qConstraintSetDefault
        (i4SetValFsDot1qConstraintSetDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qConstraintTypeDefault
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                setValFsDot1qConstraintTypeDefault
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qConstraintTypeDefault (INT4 i4FsDot1qVlanContextId,
                                    INT4 i4SetValFsDot1qConstraintTypeDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qConstraintTypeDefault
        (i4SetValFsDot1qConstraintTypeDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1vProtocolGroupId
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue

                The Object 
                setValFsDot1vProtocolGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1vProtocolGroupId (INT4 i4FsDot1qVlanContextId,
                              INT4 i4FsDot1vProtocolTemplateFrameType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsDot1vProtocolTemplateProtocolValue,
                              INT4 i4SetValFsDot1vProtocolGroupId)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1vProtocolGroupId
        (i4FsDot1vProtocolTemplateFrameType,
         pFsDot1vProtocolTemplateProtocolValue, i4SetValFsDot1vProtocolGroupId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1vProtocolGroupRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue

                The Object 
                setValFsDot1vProtocolGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1vProtocolGroupRowStatus (INT4 i4FsDot1qVlanContextId,
                                     INT4 i4FsDot1vProtocolTemplateFrameType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsDot1vProtocolTemplateProtocolValue,
                                     INT4 i4SetValFsDot1vProtocolGroupRowStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1vProtocolGroupRowStatus
        (i4FsDot1vProtocolTemplateFrameType,
         pFsDot1vProtocolTemplateProtocolValue,
         i4SetValFsDot1vProtocolGroupRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1vProtocolPortGroupVid
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId

                The Object 
                setValFsDot1vProtocolPortGroupVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1vProtocolPortGroupVid (INT4 i4FsDot1dBasePort,
                                   INT4 i4FsDot1vProtocolPortGroupId,
                                   INT4 i4SetValFsDot1vProtocolPortGroupVid)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1vProtocolPortGroupVid ((INT4) u2LocalPortId,
                                                i4FsDot1vProtocolPortGroupId,
                                                i4SetValFsDot1vProtocolPortGroupVid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1vProtocolPortRowStatus
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId

                The Object 
                setValFsDot1vProtocolPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1vProtocolPortRowStatus (INT4 i4FsDot1dBasePort,
                                    INT4 i4FsDot1vProtocolPortGroupId,
                                    INT4 i4SetValFsDot1vProtocolPortRowStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1vProtocolPortRowStatus ((INT4) u2LocalPortId,
                                                 i4FsDot1vProtocolPortGroupId,
                                                 i4SetValFsDot1vProtocolPortRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/************************** Proprietary MIB Set Routines ********************/

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanGlobalTrace
 Input       :  The Indices

                The Object 
                setValFsMIDot1qFutureVlanGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanGlobalTrace (INT4
                                      i4SetValFsMIDot1qFutureVlanGlobalTrace)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);

    gu4VlanGlobalTrace = i4SetValFsMIDot1qFutureVlanGlobalTrace;

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureVlanGlobalTrace,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 0,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsMIDot1qFutureVlanGlobalTrace));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIDot1qFutureVlanGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                 INT4 i4SetValFsMIDot1qFutureVlanStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanStatus (i4SetValFsMIDot1qFutureVlanStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanMacBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanMacBasedOnAllPorts (INT4
                                             i4FsMIDot1qFutureVlanContextId,
                                             INT4
                                             i4SetValFsMIDot1qFutureVlanMacBasedOnAllPorts)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanMacBasedOnAllPorts
        (i4SetValFsMIDot1qFutureVlanMacBasedOnAllPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts (INT4
                                                   i4FsMIDot1qFutureVlanContextId,
                                                   INT4
                                                   i4SetValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts
        (i4SetValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanShutdownStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanShutdownStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         i4SetValFsMIDot1qFutureVlanShutdownStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanShutdownStatus
        (i4SetValFsMIDot1qFutureVlanShutdownStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanDebug
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanDebug (INT4 i4FsMIDot1qFutureVlanContextId,
                                INT4 i4SetValFsMIDot1qFutureVlanDebug)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanDebug (i4SetValFsMIDot1qFutureVlanDebug);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanLearningMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanLearningMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanLearningMode (INT4 i4FsMIDot1qFutureVlanContextId,
                                       INT4
                                       i4SetValFsMIDot1qFutureVlanLearningMode)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanLearningMode
        (i4SetValFsMIDot1qFutureVlanLearningMode);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanHybridTypeDefault
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanHybridTypeDefault
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanHybridTypeDefault (INT4
                                            i4FsMIDot1qFutureVlanContextId,
                                            INT4
                                            i4SetValFsMIDot1qFutureVlanHybridTypeDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanHybridTypeDefault
        (i4SetValFsMIDot1qFutureVlanHybridTypeDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortType (INT4 i4FsMIDot1qFutureVlanPort,
                                   INT4 i4SetValFsMIDot1qFutureVlanPortType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortType ((INT4) u2LocalPortId,
                                              i4SetValFsMIDot1qFutureVlanPortType);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureBaseBridgeMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureBaseBridgeMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureBaseBridgeMode (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4 i4SetValFsMIDot1qFutureBaseBridgeMode)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanBaseBridgeMode
        (i4SetValFsMIDot1qFutureBaseBridgeMode);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanSubnetBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                setValFsMIDot1qFutureVlanSubnetBasedOnAllPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanSubnetBasedOnAllPorts (INT4
                                                i4FsMIDot1qFutureVlanContextId,
                                                INT4
                                                i4SetValFsMIDot1qFutureVlanSubnetBasedOnAllPorts)
{
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanSubnetBasedOnAllPorts
        (i4SetValFsMIDot1qFutureVlanSubnetBasedOnAllPorts);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanGlobalMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                setValFsMIDot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanGlobalMacLearningStatus (INT4
                                                  i4FsMIDot1qFutureVlanContextId,
                                                  INT4
                                                  i4SetValFsMIDot1qFutureVlanGlobalMacLearningStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanGlobalMacLearningStatus
        (i4SetValFsMIDot1qFutureVlanGlobalMacLearningStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria (INT4
                                                         i4FsMIDot1qFutureVlanContextId,
                                                         INT4
                                                         i4SetValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria
        (i4SetValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortMacBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanPortMacBasedClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortMacBasedClassification (INT4
                                                     i4FsMIDot1qFutureVlanPort,
                                                     INT4
                                                     i4SetValFsMIDot1qFutureVlanPortMacBasedClassification)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortMacBasedClassification
        ((INT4) u2LocalPortId,
         i4SetValFsMIDot1qFutureVlanPortMacBasedClassification);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortPortProtoBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanPortPortProtoBasedClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortPortProtoBasedClassification (INT4
                                                           i4FsMIDot1qFutureVlanPort,
                                                           INT4
                                                           i4SetValFsMIDot1qFutureVlanPortPortProtoBasedClassification)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortPortProtoBasedClassification
        ((INT4) u2LocalPortId,
         i4SetValFsMIDot1qFutureVlanPortPortProtoBasedClassification);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanFilteringUtilityCriteria
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanFilteringUtilityCriteria
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMIDot1qFutureVlanFilteringUtilityCriteria
    (INT4 i4FsMIDot1qFutureVlanPort,
     INT4 i4SetValFsMIDot1qFutureVlanFilteringUtilityCriteria)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanFilteringUtilityCriteria
        (u2LocalPortId, i4SetValFsMIDot1qFutureVlanFilteringUtilityCriteria);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortMacMapVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                setValFsMIDot1qFutureVlanPortMacMapVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortMacMapVid (INT4 i4FsMIDot1qFutureVlanPort,
                                        tMacAddr
                                        FsMIDot1qFutureVlanPortMacMapAddr,
                                        INT4
                                        i4SetValFsMIDot1qFutureVlanPortMacMapVid)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortMacMapVid ((INT4) u2LocalPortId,
                                                   FsMIDot1qFutureVlanPortMacMapAddr,
                                                   i4SetValFsMIDot1qFutureVlanPortMacMapVid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortMacMapName
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                setValFsMIDot1qFutureVlanPortMacMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortMacMapName (INT4 i4FsMIDot1qFutureVlanPort,
                                         tMacAddr
                                         FsMIDot1qFutureVlanPortMacMapAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValFsMIDot1qFutureVlanPortMacMapName)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortMacMapName
        ((INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         pSetValFsMIDot1qFutureVlanPortMacMapName);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortMacMapMcastBcastOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                setValFsMIDot1qFutureVlanPortMacMapMcastBcastOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortMacMapMcastBcastOption (INT4
                                                     i4FsMIDot1qFutureVlanPort,
                                                     tMacAddr
                                                     FsMIDot1qFutureVlanPortMacMapAddr,
                                                     INT4
                                                     i4SetValFsMIDot1qFutureVlanPortMacMapMcastBcastOption)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortMacMapMcastBcastOption ((INT4) u2LocalPortId,
                                                         FsMIDot1qFutureVlanPortMacMapAddr,
                                                         i4SetValFsMIDot1qFutureVlanPortMacMapMcastBcastOption);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortMacMapRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                setValFsMIDot1qFutureVlanPortMacMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortMacMapRowStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                              tMacAddr
                                              FsMIDot1qFutureVlanPortMacMapAddr,
                                              INT4
                                              i4SetValFsMIDot1qFutureVlanPortMacMapRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortMacMapRowStatus
        ((INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         i4SetValFsMIDot1qFutureVlanPortMacMapRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanFid
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object 
                setValFsMIDot1qFutureVlanFid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanFid (INT4 i4FsMIDot1qFutureVlanContextId,
                              UINT4 u4FsMIDot1qFutureVlanIndex,
                              UINT4 u4SetValFsMIDot1qFutureVlanFid)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanFid (u4FsMIDot1qFutureVlanIndex,
                                         u4SetValFsMIDot1qFutureVlanFid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanBridgeMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanBridgeMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanBridgeMode (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4 i4SetValFsMIDot1qFutureVlanBridgeMode)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (i4SetValFsMIDot1qFutureVlanBridgeMode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanTunnelBpduPri
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureVlanTunnelBpduPri
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanTunnelBpduPri (INT4 i4FsMIDot1qFutureVlanContextId,
                                        INT4
                                        i4SetValFsMIDot1qFutureVlanTunnelBpduPri)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (i4SetValFsMIDot1qFutureVlanTunnelBpduPri);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanTunnelStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanTunnelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanTunnelStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                       INT4
                                       i4SetValFsMIDot1qFutureVlanTunnelStatus)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValFsMIDot1qFutureVlanTunnelStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanTunnelStpPDUs
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanTunnelStpPDUs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanTunnelStpPDUs (INT4 i4FsMIDot1qFutureVlanPort,
                                        INT4
                                        i4SetValFsMIDot1qFutureVlanTunnelStpPDUs)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValFsMIDot1qFutureVlanTunnelStpPDUs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanTunnelGvrpPDUs
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanTunnelGvrpPDUs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanTunnelGvrpPDUs (INT4 i4FsMIDot1qFutureVlanPort,
                                         INT4 i4SetValTunnelGvrpPDUs)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValTunnelGvrpPDUs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanTunnelIgmpPkts
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                setValFsMIDot1qFutureVlanTunnelIgmpPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanTunnelIgmpPkts (INT4 i4FsMIDot1qFutureVlanPort,
                                         INT4 i4SetValTunnelIgmpPkts)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (i4SetValTunnelIgmpPkts);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanCounterStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                setValFsMIDot1qFutureVlanCounterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanCounterStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                        UINT4 u4FsMIDot1qFutureVlanIndex,
                                        INT4
                                        i4SetValFsMIDot1qFutureVlanCounterStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanCounterStatus
        (u4FsMIDot1qFutureVlanIndex, i4SetValFsMIDot1qFutureVlanCounterStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanLoopbackStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                setValFsMIDot1qFutureVlanLoopbackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanLoopbackStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         UINT4 u4FsMIDot1qFutureVlanIndex,
                                         INT4
                                         i4SetValFsMIDot1qFutureVlanLoopbackStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanLoopbackStatus
        (u4FsMIDot1qFutureVlanIndex, i4SetValFsMIDot1qFutureVlanLoopbackStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/************************** Proprietary MIB Set Routines ********************/

/****************************************************************************
 Function    :  nmhSetFsDot1dTrafficClassesEnabled
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                setValFsDot1dTrafficClassesEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dTrafficClassesEnabled (INT4 i4FsDot1dBridgeContextId,
                                    INT4 i4SetValFsDot1dTrafficClassesEnabled)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1dBridgeContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dTrafficClassesEnabled
        (i4SetValFsDot1dTrafficClassesEnabled);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortDefaultUserPriority
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortDefaultUserPriority (INT4 i4FsDot1dBasePort,
                                      INT4
                                      i4SetValFsDot1dPortDefaultUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortDefaultUserPriority ((INT4) u2LocalPortId,
                                                   i4SetValFsDot1dPortDefaultUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortNumTrafficClasses
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortNumTrafficClasses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortNumTrafficClasses (INT4 i4FsDot1dBasePort,
                                    INT4 i4SetValFsDot1dPortNumTrafficClasses)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortNumTrafficClasses ((INT4) u2LocalPortId,
                                                 i4SetValFsDot1dPortNumTrafficClasses);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dRegenUserPriority
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dUserPriority

                The Object
                setValFsDot1dRegenUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dRegenUserPriority (INT4 i4FsDot1dBasePort,
                                INT4 i4FsDot1dUserPriority,
                                INT4 i4SetValFsDot1dRegenUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dRegenUserPriority ((INT4) u2LocalPortId,
                                             i4FsDot1dUserPriority,
                                             i4SetValFsDot1dRegenUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dTrafficClass
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dTrafficClassPriority

                The Object
                setValFsDot1dTrafficClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dTrafficClass (INT4 i4FsDot1dBasePort,
                           INT4 i4FsDot1dTrafficClassPriority,
                           INT4 i4SetValFsDot1dTrafficClass)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dTrafficClass ((INT4) u2LocalPortId,
                                        i4FsDot1dTrafficClassPriority,
                                        i4SetValFsDot1dTrafficClass);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanUnicastMacLimit
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                setValFsMIDot1qFutureVlanUnicastMacLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMIDot1qFutureVlanUnicastMacLimit
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 u4SetValFsMIDot1qFutureVlanUnicastMacLimit)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanUnicastMacLimit
        (u4FsMIDot1qFutureVlanIndex,
         u4SetValFsMIDot1qFutureVlanUnicastMacLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanAdminMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                setValFsMIDot1qFutureVlanAdminMacLearningStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanAdminMacLearningStatus (INT4
                                                 i4FsMIDot1qFutureVlanContextId,
                                                 UINT4
                                                 u4FsMIDot1qFutureVlanIndex,
                                                 INT4
                                                 i4SetValFsMIDot1qFutureVlanAdminMacLearningStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanAdminMacLearningStatus
        (u4FsMIDot1qFutureVlanIndex,
         i4SetValFsMIDot1qFutureVlanAdminMacLearningStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

PRIVATE INT4
VlanStaticUcastCreateTempPortList (UINT4 u4FdbId, UINT2 u2Port,
                                   tMacAddr MacAddr, tLocalPortList Ports,
                                   UINT1 u1Status, tMacAddr ConnectionId)
{
    tVlanTempPortList  *pTmpPortList = NULL;

    pTmpPortList =
        (tVlanTempPortList *) (VOID *) VLAN_GET_BUF (VLAN_TEMP_PORTLIST_ENTRY,
                                                     0);

    if (pTmpPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "No Free Temp PortList Entry \n");
        return VLAN_FAILURE;
    }

    MEMSET (pTmpPortList, 0, sizeof (tVlanTempPortList));

    VLAN_SLL_INIT_NODE (&(pTmpPortList->NextNode));

    pTmpPortList->u4ContextId = VLAN_CURR_CONTEXT_ID ();
    pTmpPortList->u1Type = VLAN_ST_UCAST_TABLE;
    pTmpPortList->PortListTbl.StUcastTbl.u4FdbId = u4FdbId;
    pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort = u2Port;
    VLAN_CPY_MAC_ADDR (pTmpPortList->PortListTbl.StUcastTbl.MacAddr, MacAddr);
    MEMCPY (pTmpPortList->PortListTbl.StUcastTbl.AllowedToGo,
            Ports, VLAN_PORT_LIST_SIZE);
    pTmpPortList->PortListTbl.StUcastTbl.u1Status = u1Status;

    MEMCPY (pTmpPortList->PortListTbl.StUcastTbl.ConnectionId, ConnectionId,
            sizeof (tMacAddr));

    VLAN_SLL_ADD (&gVlanTempPortList,
                  (tTMO_SLL_NODE *) & (pTmpPortList->NextNode));

    return VLAN_SUCCESS;
}

PRIVATE INT4
VlanMiUpdateStaticUcastEntry (UINT4 u4FdbId, tMacAddr MacAddr,
                              UINT2 u2Port, INT4 i4Status)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4FidIndex;
    UINT4               u4CurrContextId;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        return VLAN_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

    pVlanStUcastEntry =
        VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex, MacAddr, u2Port);
    if (NULL == pVlanStUcastEntry)
    {
        VlanSelectContext (u4CurrContextId);
        return VLAN_FAILURE;
    }

    /* If RowStatus is ACTIVE  and u1Status is not INVALID we can 
     * directly update the status in the software */
    if ((pVlanStUcastEntry->u1RowStatus == VLAN_ACTIVE) &&
        (pVlanStUcastEntry->u1Status != VLAN_MGMT_INVALID))
    {
        pVlanStUcastEntry->u1Status = (UINT1) i4Status;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticUnicastStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 4,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i", u4CurrContextId,
                          u4FdbId, MacAddr, (INT4) u2Port, i4Status));
        VlanSelectContext (u4CurrContextId);
        return VLAN_SUCCESS;
    }

    VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
    {
        if ((pTmpPortList->u4ContextId == VLAN_CURR_CONTEXT_ID ()) &&
            (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE) &&
            (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId == u4FdbId) &&
            (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort == u2Port) &&
            (VLAN_ARE_MAC_ADDR_EQUAL
             (pTmpPortList->PortListTbl.StUcastTbl.MacAddr, MacAddr)))
        {
            break;
        }
    }

    if (pTmpPortList == NULL)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticUnicastStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 4,
                              SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i", u4CurrContextId,
                          u4FdbId, MacAddr, (INT4) u2Port, i4Status));
        VlanSelectContext (u4CurrContextId);
        return VLAN_FAILURE;
    }

    if (i4Status != VLAN_MGMT_INVALID)
    {
        pTmpPortList->PortListTbl.StUcastTbl.u1Status = (UINT1) i4Status;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticUnicastStatus,
                              u4SeqNum, TRUE, VlanLock, VlanUnLock, 4,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i", u4CurrContextId,
                          u4FdbId, MacAddr, (INT4) u2Port, i4Status));
        VlanSelectContext (u4CurrContextId);
        return VLAN_SUCCESS;
    }

    /* i4Status is INVALID, so delete the Static unicast entry, 
     * FidEntry, TempPortList and FDB entry */

    /* Deleting the FDB Entry */
    VlanMIDeleteFdbStUcastEntry (pVlanStUcastEntry, u4FidIndex);

    /* Deleting the Fid, Static Unicast entry */
    VlanDeleteStUcastEntry (pFidEntry, pVlanStUcastEntry);

    /* Deleting the TempPortList entry */
    VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));
    VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY, (UINT1 *) pTmpPortList);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qStaticUnicastStatus, u4SeqNum,
                          TRUE, VlanLock, VlanUnLock, 4, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %i",
                      u4CurrContextId, u4FdbId,
                      MacAddr, (INT4) u2Port, i4Status));
    VlanSelectContext (u4CurrContextId);
    return VLAN_SUCCESS;
}

PRIVATE VOID
VlanMIDeleteFdbStUcastEntry (tVlanStUcastEntry * pVlanStUcastEntry,
                             UINT4 u4FidIndex)
{
    tVlanFdbEntry      *pVlanFdbEntry = NULL;

    if (pVlanStUcastEntry->u1RowStatus != VLAN_NOT_IN_SERVICE)
    {
        return;
    }

    /* Rowstatus is NOT_IN_SERVICE, so the FDB entry should be there.
     * so we have to remove that FDB entry. */
    pVlanFdbEntry = pVlanStUcastEntry->pFdbEntry;

    if (pVlanFdbEntry != NULL)
    {
        pVlanFdbEntry->u1StRefCount--;
        if (pVlanFdbEntry->u1StRefCount == 0)
        {
            if (pVlanFdbEntry->u2Port == VLAN_INVALID_PORT)
            {
                /* Delete the FDB entry from the FDB table */
                VlanDeleteFdbEntry (u4FidIndex, pVlanFdbEntry);
            }
            else
            {
                pVlanFdbEntry->u1Status = VLAN_FDB_LEARNT;
            }
        }
    }
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanWildCardRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress

                The Object 
                setValFsMIDot1qFutureVlanWildCardRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMIDot1qFutureVlanWildCardRowStatus
    (INT4 i4FsDot1qVlanContextId, tMacAddr WildCardMacAddress, INT4 i4RowStatus)
{
    UINT1              *pau1PortList = NULL;
    tVlanWildCardEntry *pWildCardEntry;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSNMP_OCTET_STRING_TYPE PortList;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry != NULL)
    {

        if (pWildCardEntry->u1RowStatus == (UINT1) i4RowStatus)
        {

            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

        if (i4RowStatus == VLAN_CREATE_AND_WAIT)
        {

            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* entry is not present in the database */
        if (i4RowStatus != VLAN_CREATE_AND_WAIT)
        {

            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }

    switch (i4RowStatus)
    {
        case VLAN_CREATE_AND_WAIT:
        case VLAN_NOT_IN_SERVICE:

            pTmpPortList =
                (tVlanTempPortList *) (VOID *)
                VLAN_GET_BUF (VLAN_TEMP_PORTLIST_ENTRY, 0);

            if (pTmpPortList == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free Temp PortList Entry \n");
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (nmhSetDot1qFutureVlanWildCardRowStatus (WildCardMacAddress,
                                                        i4RowStatus)
                != SNMP_SUCCESS)
            {
                VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY,
                                  (UINT1 *) pTmpPortList);
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            MEMSET (pTmpPortList, 0, sizeof (tVlanTempPortList));

            VLAN_SLL_INIT_NODE (&(pTmpPortList->NextNode));

            pTmpPortList->u4ContextId = i4FsDot1qVlanContextId;
            pTmpPortList->u1Type = VLAN_WILDCARD_TABLE;
            VLAN_CPY_MAC_ADDR (pTmpPortList->PortListTbl.WildCardTbl.MacAddr,
                               WildCardMacAddress);

            if (i4RowStatus == VLAN_CREATE_AND_WAIT)
            {
                MEMCPY (pTmpPortList->PortListTbl.WildCardTbl.EgressPorts,
                        gNullPortList, VLAN_PORT_LIST_SIZE);
            }
            else
            {
                MEMCPY (pTmpPortList->PortListTbl.WildCardTbl.EgressPorts,
                        pWildCardEntry->EgressPorts, VLAN_PORT_LIST_SIZE);
            }

            VLAN_SLL_ADD (&gVlanTempPortList,
                          (tTMO_SLL_NODE *) & (pTmpPortList->NextNode));

            break;

        case VLAN_ACTIVE:

            VLAN_SLL_SCAN (&gVlanTempPortList,
                           pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId ==
                 (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_WILDCARD_TABLE) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.WildCardTbl.MacAddr,
                  WildCardMacAddress)))
            {
                break;
            }
        }

            if (pTmpPortList != NULL)
            {
                pau1PortList = UtilPlstAllocLocalPortList (VLAN_PORT_LIST_SIZE);
                if (pau1PortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "nmhSetFsMIDot1qFutureVlanWildCardRowStatus: "
                              "Error in allocating memory for pau1PortList\r\n");
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                MEMSET (pau1PortList, 0, VLAN_PORT_LIST_SIZE);

                MEMCPY (pau1PortList,
                        pTmpPortList->PortListTbl.WildCardTbl.EgressPorts,
                        VLAN_PORT_LIST_SIZE);
                PortList.i4_Length = VLAN_PORT_LIST_SIZE;
                PortList.pu1_OctetList = pau1PortList;

                if (nmhSetDot1qFutureVlanWildCardEgressPorts
                    (WildCardMacAddress, &PortList) != SNMP_SUCCESS)
                {

                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pau1PortList);
                    return SNMP_FAILURE;
                }
                UtilPlstReleaseLocalPortList (pau1PortList);
                if (nmhSetDot1qFutureVlanWildCardRowStatus
                    (WildCardMacAddress, i4RowStatus) != SNMP_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_FAILURE;

                }

                VLAN_SLL_DEL (&gVlanTempPortList, &(pTmpPortList->NextNode));

                VLAN_RELEASE_BUF (VLAN_TEMP_PORTLIST_ENTRY,
                                  (UINT1 *) pTmpPortList);
                pTmpPortList = NULL;
            }
            break;

        case VLAN_DESTROY:
            if (nmhSetDot1qFutureVlanWildCardRowStatus (WildCardMacAddress,
                                                        i4RowStatus)
                != SNMP_SUCCESS)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;

            }
            break;

        default:
            VlanReleaseContext ();
            return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanIsWildCardEgressPort
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort

                The Object 
                setValFsMIDot1qFutureVlanIsWildCardEgressPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMIDot1qFutureVlanIsWildCardEgressPort
    (INT4 i4FsDot1qVlanContextId, tMacAddr WildCardMacAddress,
     INT4 i4FsDot1qTpPort, INT4 i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4ContextId;
    UINT4               u4SeqNum = 0;
    UINT2               u2LocalPortId;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry != NULL)
    {
        if (pWildCardEntry->u1RowStatus == VLAN_NOT_IN_SERVICE)
        {
            VLAN_SLL_SCAN (&gVlanTempPortList,
                           pTmpPortList, tVlanTempPortList *)
            {
                if ((pTmpPortList->u4ContextId ==
                     (UINT4) i4FsDot1qVlanContextId)
                    && (pTmpPortList->u1Type == VLAN_WILDCARD_TABLE)
                    &&
                    (VLAN_ARE_MAC_ADDR_EQUAL
                     (pTmpPortList->PortListTbl.WildCardTbl.MacAddr,
                      WildCardMacAddress)))
                {
                    if (i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort
                        == VLAN_TRUE)
                    {
                        VLAN_SET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.WildCardTbl.EgressPorts,
                             u2LocalPortId);
                    }
                    else
                    {
                        VLAN_RESET_MEMBER_PORT
                            (pTmpPortList->PortListTbl.WildCardTbl.EgressPorts,
                             u2LocalPortId);
                    }
                    VlanReleaseContext ();

                    /* Sending Trigger to MSR */

                    RM_GET_SEQ_NUM (&u4SeqNum);
                    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                          FsMIDot1qFutureVlanIsWildCardEgressPort,
                                          u4SeqNum, FALSE, VlanLock, VlanUnLock,
                                          3, SNMP_SUCCESS);

                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m %i %i",
                                      i4FsDot1qVlanContextId,
                                      WildCardMacAddress, i4FsDot1qTpPort,
                                      i4SetValFsMIDot1qFutureVlanIsWildCardEgressPort));

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    VlanReleaseContext ();

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortProtected
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortProtected
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortProtected (INT4 i4FsMIDot1qFutureVlanPort,
                                        INT4
                                        i4SetValFsMIDot1qFutureVlanPortProtected)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortProtected ((INT4) u2LocalPortId,
                                                   i4SetValFsMIDot1qFutureVlanPortProtected);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortSubnetBasedClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortSubnetBasedClassification (INT4
                                                        i4FsMIDot1qFutureVlanPort,
                                                        INT4
                                                        i4SetValFsMIDot1qFutureVlanPortSubnetBasedClassification)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortSubnetBasedClassification
        ((INT4) u2LocalPortId,
         i4SetValFsMIDot1qFutureVlanPortSubnetBasedClassification);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortUnicastMacLearning
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortUnicastMacLearning
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortUnicastMacLearning (INT4 i4FsMIDot1qFutureVlanPort,
                                                 INT4
                                                 i4SetValFsMIDot1qFutureVlanPortUnicastMacLearning)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortUnicastMacLearning ((INT4) u2LocalPort,
                                                     i4SetValFsMIDot1qFutureVlanPortUnicastMacLearning);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureUnicastMacLearningLimit
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureUnicastMacLearningLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureUnicastMacLearningLimit (INT4
                                              i4FsMIDot1qFutureVlanContextId,
                                              UINT4
                                              u4SetValFsMIDot1qFutureUnicastMacLearningLimit)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureUnicastMacLearningLimit
        (u4SetValFsMIDot1qFutureUnicastMacLearningLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureStaticConnectionIdentifier
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort

                The Object
                setValFsMIDot1qFutureStaticConnectionIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureStaticConnectionIdentifier (INT4 i4FsDot1qVlanContextId,
                                                 UINT4 u4FsDot1qFdbId,
                                                 tMacAddr
                                                 FsDot1qStaticUnicastAddress,
                                                 INT4
                                                 i4FsDot1qStaticUnicastReceivePort,
                                                 tMacAddr
                                                 SetValFsMIDot1qFutureStaticConnectionIdentifier)
{
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    tVlanTempPortList  *pTmpPortList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4FidIndex;
    UINT4               u4ContextId;
    UINT4               u4SeqNum = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    pVlanStUcastEntry =
        VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2LocalPortId);

    /* If StUcast entry is not there we can simply call the SI routine */
    if (pVlanStUcastEntry == NULL)
    {
        i1RetVal = nmhSetDot1qFutureStaticConnectionIdentifier
            (u4FsDot1qFdbId,
             FsDot1qStaticUnicastAddress,
             i4FsDot1qStaticUnicastReceivePort,
             SetValFsMIDot1qFutureStaticConnectionIdentifier);
    }
    else if ((pVlanStUcastEntry->u1RowStatus == VLAN_NOT_IN_SERVICE) ||
             (pVlanStUcastEntry->u1RowStatus == VLAN_NOT_READY))
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId ==
                 (UINT4) i4FsDot1qVlanContextId)
                && (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE)
                && (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId ==
                    u4FsDot1qFdbId)
                && (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort ==
                    i4FsDot1qStaticUnicastReceivePort)
                &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.StUcastTbl.MacAddr,
                  FsDot1qStaticUnicastAddress)))
            {
                MEMCPY (pTmpPortList->PortListTbl.StUcastTbl.ConnectionId,
                        SetValFsMIDot1qFutureStaticConnectionIdentifier,
                        sizeof (tMacAddr));
                i1RetVal = SNMP_SUCCESS;
            }
        }
    }

    VlanReleaseContext ();

    /* Sending Trigger to MSR */

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIDot1qFutureStaticConnectionIdentifier, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 4, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %m %i %m", i4FsDot1qVlanContextId,
                      u4FsDot1qFdbId, FsDot1qStaticUnicastAddress,
                      i4FsDot1qStaticUnicastReceivePort,
                      SetValFsMIDot1qFutureStaticConnectionIdentifier));
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetMapVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                setValFsMIDot1qFutureVlanPortSubnetMapVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapVid (INT4 i4FsMIDot1qFutureVlanPort,
                                           UINT4
                                           u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                           INT4
                                           i4SetValFsMIDot1qFutureVlanPortSubnetMapVid)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

#ifndef BCMX_WANTED
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    u4ContextId = VLAN_DEF_CONTEXT_ID;
#endif

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapVid ((INT4) u2LocalPortId,
                                                      u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                      i4SetValFsMIDot1qFutureVlanPortSubnetMapVid);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetMapARPOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                setValFsMIDot1qFutureVlanPortSubnetMapARPOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapARPOption (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                 INT4
                                                 i4SetValFsMIDot1qFutureVlanPortSubnetMapARPOption)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

#ifndef BCMX_WANTED
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    u4ContextId = VLAN_DEF_CONTEXT_ID;
#endif

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortSubnetMapARPOption ((INT4) u2LocalPortId,
                                                     u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                     i4SetValFsMIDot1qFutureVlanPortSubnetMapARPOption);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetMapRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                setValFsMIDot1qFutureVlanPortSubnetMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapRowStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                 INT4
                                                 i4SetValFsMIDot1qFutureVlanPortSubnetMapRowStatus)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

#ifndef BCMX_WANTED
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    u4ContextId = VLAN_DEF_CONTEXT_ID;
#endif
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapRowStatus
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         i4SetValFsMIDot1qFutureVlanPortSubnetMapRowStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanSwStatsEnabled
 Input       :  The Indices

                The Object 
                setValFsMIDot1qFutureVlanSwStatsEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanSwStatsEnabled (INT4
                                         i4SetValFsMIDot1qFutureVlanSwStatsEnabled)
{
    return (nmhSetDot1qFutureVlanSwStatsEnabled
            (i4SetValFsMIDot1qFutureVlanSwStatsEnabled));

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetMapExtVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                setValFsMIDot1qFutureVlanPortSubnetMapExtVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsMIDot1qFutureVlanPortSubnetMapExtVid
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4SetValFsMIDot1qFutureVlanPortSubnetMapExtVid)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

#ifndef BCMX_WANTED
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    u4ContextId = VLAN_DEF_CONTEXT_ID;
#endif

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortSubnetMapExtVid
        ((INT4) u2LocalPortId, u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         i4SetValFsMIDot1qFutureVlanPortSubnetMapExtVid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetMapExtARPOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                setValFsMIDot1qFutureVlanPortSubnetMapExtARPOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsMIDot1qFutureVlanPortSubnetMapExtARPOption
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4SetValFsMIDot1qFutureVlanPortSubnetMapExtARPOption)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

#ifndef BCMX_WANTED
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    u4ContextId = VLAN_DEF_CONTEXT_ID;
#endif

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortSubnetMapExtARPOption
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         i4SetValFsMIDot1qFutureVlanPortSubnetMapExtARPOption);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                setValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 i4SetValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

#ifndef BCMX_WANTED
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    u4ContextId = VLAN_DEF_CONTEXT_ID;
#endif
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         i4SetValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanGlobalsFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                setValFsMIDot1qFutureVlanGlobalsFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanGlobalsFdbFlush (INT4 i4FsMIDot1qFutureVlanContextId,
                                          INT4
                                          i4SetValFsMIDot1qFutureVlanGlobalsFdbFlush)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanGlobalsFdbFlush
        (i4SetValFsMIDot1qFutureVlanGlobalsFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanUserDefinedTPID
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                setValFsMIDot1qFutureVlanUserDefinedTPID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanUserDefinedTPID (INT4 i4FsMIDot1qFutureVlanContextId,
                                          INT4
                                          i4SetValFsMIDot1qFutureVlanUserDefinedTPID)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanUserDefinedTPID
        (i4SetValFsMIDot1qFutureVlanUserDefinedTPID);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanRemoteFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                setValFsMIDot1qFutureVlanRemoteFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanRemoteFdbFlush (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         i4SetValFsMIDot1qFutureVlanRemoteFdbFlush)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureVlanRemoteFdbFlush
        (i4SetValFsMIDot1qFutureVlanRemoteFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortFdbFlush (INT4 i4FsMIDot1qFutureVlanPort,
                                       INT4
                                       i4SetValFsMIDot1qFutureVlanPortFdbFlush)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);
    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIDot1qFutureVlanPortFdbFlush == VLAN_TRUE)
    {
        pVlanPortEntry->bPortFdbFlush =
            (BOOLEAN) i4SetValFsMIDot1qFutureVlanPortFdbFlush;
        if (VlanHwFlushPort (u4ContextId, u2LocalPortId, VLAN_NO_OPTIMIZE)
            == VLAN_FAILURE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    pVlanPortEntry->bPortFdbFlush = VLAN_FALSE;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortIngressEtherType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortIngressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &                                                                                    Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortIngressEtherType (INT4 i4FsMIDot1qFutureVlanPort,
                                               INT4
                                               i4SetValFsMIDot1qFutureVlanPortIngressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortIngressEtherType ((INT4) u2LocalPort,
                                                   i4SetValFsMIDot1qFutureVlanPortIngressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortEgressEtherType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortEgressEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortEgressEtherType (INT4 i4FsMIDot1qFutureVlanPort,
                                              INT4
                                              i4SetValFsMIDot1qFutureVlanPortEgressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortEgressEtherType ((INT4) u2LocalPort,
                                                  i4SetValFsMIDot1qFutureVlanPortEgressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortEgressTPIDType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortEgressTPIDType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortEgressTPIDType (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             i4SetValFsMIDot1qFutureVlanPortEgressTPIDType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortEgressTPIDType ((INT4) u2LocalPort,
                                                 i4SetValFsMIDot1qFutureVlanPortEgressTPIDType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortAllowableTPID1
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortAllowableTPID1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortAllowableTPID1 (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             i4SetValFsMIDot1qFutureVlanPortAllowableTPID1)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortAllowableTPID1 ((INT4) u2LocalPort,
                                                 i4SetValFsMIDot1qFutureVlanPortAllowableTPID1);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortAllowableTPID2
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortAllowableTPID2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortAllowableTPID2 (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             i4SetValFsMIDot1qFutureVlanPortAllowableTPID2)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortAllowableTPID2 ((INT4) u2LocalPort,
                                                 i4SetValFsMIDot1qFutureVlanPortAllowableTPID2);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortAllowableTPID3
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortAllowableTPID3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortAllowableTPID3 (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             i4SetValFsMIDot1qFutureVlanPortAllowableTPID3)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFutureVlanPortAllowableTPID3 ((INT4) u2LocalPort,
                                                 i4SetValFsMIDot1qFutureVlanPortAllowableTPID3);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureStVlanFdbFlush
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object
                setValFsMIDot1qFutureStVlanFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureStVlanFdbFlush (INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qVlanIndex,
                                     INT4 i4SetValFsMIDot1qFutureStVlanFdbFlush)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1qFutureVlanPortFdbFlush
        (u4FsDot1qVlanIndex, i4SetValFsMIDot1qFutureStVlanFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureStVlanEgressEthertype
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object
                setValFsMIDot1qFutureStVlanEgressEthertype
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureStVlanEgressEthertype (INT4 i4FsDot1qVlanContextId,
                                            UINT4 u4FsDot1qVlanIndex,
                                            INT4
                                            i4SetValFsMIDot1qFutureStVlanEgressEthertype)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1qFutureStVlanEgressEthertype
        (u4FsDot1qVlanIndex, i4SetValFsMIDot1qFutureStVlanEgressEthertype);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFuturePortVlanFdbFlush
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object
                setValFsMIDot1qFuturePortVlanFdbFlush
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFuturePortVlanFdbFlush (INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       INT4 i4FsDot1qTpPort,
                                       INT4
                                       i4SetValFsMIDot1qFuturePortVlanFdbFlush)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4ContextId = VLAN_ZERO;
    UINT2               u2LocalPortId = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (u2LocalPortId >= VLAN_MAX_PORTS + 1)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4FsDot1qVlanIndex);
    if (pStVlanEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIDot1qFuturePortVlanFdbFlush == VLAN_TRUE)
    {
        VLAN_SET_FDB_FLUSH (pStVlanEntry, u2LocalPortId);
        if (VlanHwFlushPortFdbId ((UINT4) i4FsDot1qVlanContextId, u2LocalPortId,
                                  u4FsDot1qVlanIndex, VLAN_NO_OPTIMIZE)
            == VLAN_FAILURE)
        {
            VLAN_RESET_FDB_FLUSH (pStVlanEntry, u2LocalPortId);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    VLAN_RESET_FDB_FLUSH (pStVlanEntry, u2LocalPortId);
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/**************************************************************************** 
 Function    :  nmhSetFsMIDot1qFuturePortPacketReflectionStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                The Object
                setValFsMIDot1qFuturePortPacketReflectionStatus
Output       :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
Returns      :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFsMIDot1qFuturePortPacketReflectionStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                                 INT4
                                                 i4SetValFsMIDot1qFuturePortPacketReflectionStatus)
{

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    /*Get Context and Local port ID using interface index */
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1qFuturePortPacketReflectionStatus ((INT4) u2LocalPortId,
                                                     i4SetValFsMIDot1qFuturePortPacketReflectionStatus);

    if (i1RetVal == SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;

}
