/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1lllw.c,v 1.6 2016/07/16 11:15:04 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "vlaninc.h"

/* LOW LEVEL Routines for Table : LldpXdot1EvbConfigEvbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1EvbConfigEvbTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceLldpXdot1EvbConfigEvbTable
    (INT4 i4LldpV2PortConfigIfIndex, UINT4 u4LldpV2PortConfigDestAddressIndex)
{
    UNUSED_PARAM (i4LldpV2PortConfigIfIndex);
    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1EvbConfigEvbTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexLldpXdot1EvbConfigEvbTable
    (INT4 *pi4LldpV2PortConfigIfIndex,
     UINT4 *pu4LldpV2PortConfigDestAddressIndex)
{
    UNUSED_PARAM (pi4LldpV2PortConfigIfIndex);
    UNUSED_PARAM (pu4LldpV2PortConfigDestAddressIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1EvbConfigEvbTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexLldpXdot1EvbConfigEvbTable
    (INT4 i4LldpV2PortConfigIfIndex,
     INT4 *pi4NextLldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
     UINT4 *pu4NextLldpV2PortConfigDestAddressIndex)
{
    UNUSED_PARAM (i4LldpV2PortConfigIfIndex);
    UNUSED_PARAM (pi4NextLldpV2PortConfigIfIndex);
    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);
    UNUSED_PARAM (pu4NextLldpV2PortConfigDestAddressIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1EvbConfigEvbTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpXdot1EvbConfigEvbTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetLldpXdot1EvbConfigEvbTxEnable
    (INT4 i4LldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
    INT4 *pi4RetValLldpXdot1EvbConfigEvbTxEnable)
{
    UNUSED_PARAM (i4LldpV2PortConfigIfIndex);
    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);
	UNUSED_PARAM(pi4RetValLldpXdot1EvbConfigEvbTxEnable);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1EvbConfigEvbTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpXdot1EvbConfigEvbTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetLldpXdot1EvbConfigEvbTxEnable
    (INT4 i4LldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
    INT4 i4SetValLldpXdot1EvbConfigEvbTxEnable)
{

    UNUSED_PARAM (i4LldpV2PortConfigIfIndex);
    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);
	UNUSED_PARAM(i4SetValLldpXdot1EvbConfigEvbTxEnable);
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1EvbConfigEvbTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpXdot1EvbConfigEvbTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2LldpXdot1EvbConfigEvbTxEnable
    (UINT4 *pu4ErrorCode,
     INT4 i4LldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
    INT4 i4TestValLldpXdot1EvbConfigEvbTxEnable)
{
	UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM (i4LldpV2PortConfigIfIndex);
    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);
	UNUSED_PARAM(i4TestValLldpXdot1EvbConfigEvbTxEnable);
    return SNMP_FAILURE;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1EvbConfigEvbTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1EvbConfigEvbTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1EvbConfigCdcpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1EvbConfigCdcpTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceLldpXdot1EvbConfigCdcpTable
    (INT4 i4LldpV2PortConfigIfIndex, UINT4 u4LldpV2PortConfigDestAddressIndex)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);

    if (VlanVcmGetContextInfoFromIfIndex
        ((UINT4) i4LldpV2PortConfigIfIndex, &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry ((UINT4) i4LldpV2PortConfigIfIndex);

    if(NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1EvbConfigCdcpTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexLldpXdot1EvbConfigCdcpTable
    (INT4 *pi4LldpV2PortConfigIfIndex,
     UINT4 *pu4LldpV2PortConfigDestAddressIndex)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UNUSED_PARAM (pu4LldpV2PortConfigDestAddressIndex);

    pEvbUapIfEntry = VlanEvbUapIfGetFirstEntry();

    if(NULL == pEvbUapIfEntry)
    {
        return SNMP_FAILURE;
    }
    *pi4LldpV2PortConfigIfIndex = (INT4) pEvbUapIfEntry->u4UapIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1EvbConfigCdcpTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexLldpXdot1EvbConfigCdcpTable
    (INT4 i4LldpV2PortConfigIfIndex,
     INT4 *pi4NextLldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
     UINT4 *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);
    UNUSED_PARAM (pu4NextLldpV2PortConfigDestAddressIndex);

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4) i4LldpV2PortConfigIfIndex,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry =
        VlanEvbUapIfGetNextEntry ((UINT4) i4LldpV2PortConfigIfIndex);

    if (NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4NextLldpV2PortConfigIfIndex = (INT4) pEvbUapIfEntry->u4UapIfIndex;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1EvbConfigCdcpTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpXdot1EvbConfigCdcpTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetLldpXdot1EvbConfigCdcpTxEnable
    (INT4 i4LldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
    INT4 *pi4RetValLldpXdot1EvbConfigCdcpTxEnable)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4) i4LldpV2PortConfigIfIndex,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry ((UINT4) i4LldpV2PortConfigIfIndex);

    if(NULL == pEvbUapIfEntry)
    {
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1EvbConfigCdcpTxEnable = 
        pEvbUapIfEntry->i4EvbSysEvbLldpTxEnable;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1EvbConfigCdcpTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpXdot1EvbConfigCdcpTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetLldpXdot1EvbConfigCdcpTxEnable
    (INT4 i4LldpV2PortConfigIfIndex,
     UINT4 u4LldpV2PortConfigDestAddressIndex,
    INT4 i4SetValLldpXdot1EvbConfigCdcpTxEnable)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4) i4LldpV2PortConfigIfIndex,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pEvbUapIfEntry = VlanEvbUapIfGetEntry ((UINT4)i4LldpV2PortConfigIfIndex);

    if(NULL == pEvbUapIfEntry)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                "nmhSetLldpXdot1EvbConfigCdcpTxEnable"
                "UAP Entry %d not present \r\n", 
                       i4LldpV2PortConfigIfIndex);
        CLI_SET_ERR(CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if (VlanEvbCdcpUtlTlvStatusChange (pEvbUapIfEntry,
                                       i4SetValLldpXdot1EvbConfigCdcpTxEnable)
        == VLAN_FAILURE)
    {
       VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "nmhSetLldpXdot1EvbConfigCdcpTxEnable"
                      "Failed to Change Tlv Status for the UAP Entry \r\n", 
                       pEvbUapIfEntry->u4UapIfIndex);
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1EvbConfigCdcpTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpXdot1EvbConfigCdcpTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1EvbConfigCdcpTxEnable (UINT4 *pu4ErrorCode,
                                         INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         INT4
                                         i4TestValLldpXdot1EvbConfigCdcpTxEnable)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4       u4ContextId     = 0;
    UINT2       u2LocalPortId   = 0;

    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4) i4LldpV2PortConfigIfIndex,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u2LocalPortId);

    /* Verifying the EVB System status */
    if (VLAN_EVB_SYSTEM_START != VLAN_EVB_SYSTEM_STATUS (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if(NULL == pEvbUapIfEntry)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2LldpXdot1EvbConfigCdcpTxEnable"
                       "UAP Entry %d not present: \n",
                       i4LldpV2PortConfigIfIndex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((VLAN_EVB_CDCP_LLDP_ENABLE != i4TestValLldpXdot1EvbConfigCdcpTxEnable)
        && (VLAN_EVB_CDCP_LLDP_DISABLE !=
            i4TestValLldpXdot1EvbConfigCdcpTxEnable))
    {
         VLAN_TRC_ARG2 (VLAN_EVB_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                   "nmhTestv2LldpXdot1EvbConfigCdcpTxEnable"
                  "Wrong Test Value %d for UAP %d\n",
                  i4TestValLldpXdot1EvbConfigCdcpTxEnable, 
                       i4LldpV2PortConfigIfIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1EvbConfigCdcpTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1EvbConfigCdcpTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocEvbTlvTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocEvbTlvTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceLldpV2Xdot1LocEvbTlvTable
    (INT4 i4LldpV2LocPortIfIndex)
{
/* Not supported */
	UNUSED_PARAM(i4LldpV2LocPortIfIndex);
return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocEvbTlvTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocEvbTlvTable (INT4 *pi4LldpV2LocPortIfIndex)
{
/* Not supported */
	UNUSED_PARAM(pi4LldpV2LocPortIfIndex);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocEvbTlvTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocEvbTlvTable (INT4 i4LldpV2LocPortIfIndex,
    INT4 *pi4NextLldpV2LocPortIfIndex )
{
    /*Not supported*/
	UNUSED_PARAM(i4LldpV2LocPortIfIndex);
	UNUSED_PARAM(pi4NextLldpV2LocPortIfIndex);
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocEvbTlvString
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocEvbTlvString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocEvbTlvString (INT4 i4LldpV2LocPortIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValLldpV2Xdot1LocEvbTlvString)
{
    /*Not supported*/
	UNUSED_PARAM(i4LldpV2LocPortIfIndex);
	UNUSED_PARAM(pRetValLldpV2Xdot1LocEvbTlvString);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : LldpV2Xdot1LocCdcpTlvTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocCdcpTlvTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceLldpV2Xdot1LocCdcpTlvTable
    (INT4 i4LldpV2LocPortIfIndex)
{
    tEvbUapIfEntry * pEvbUapIfEntry= NULL;
    UINT4            u4ContextId     = 0;
    UINT2            u2LocalPortId   = 0;

    if (VlanVcmGetContextInfoFromIfIndex ((UINT4)i4LldpV2LocPortIfIndex,
                &u4ContextId,
                &u2LocalPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pEvbUapIfEntry = VlanEvbUapIfGetEntry ((UINT4)i4LldpV2LocPortIfIndex);

    if(NULL == pEvbUapIfEntry)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocCdcpTlvTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocCdcpTlvTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1LocCdcpTlvTable (0, 
                pi4LldpV2LocPortIfIndex));
}
/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocCdcpTlvTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocCdcpTlvTable (INT4 i4LldpV2LocPortIfIndex,
    INT4 *pi4NextLldpV2LocPortIfIndex )
{
    tEvbUapIfEntry *pEvbUapIfEntry = NULL;

    while (i4LldpV2LocPortIfIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        /* Make sure no VLAN traces are present inside */
        pEvbUapIfEntry =
            VlanEvbUapIfGetNextEntry ((UINT4) i4LldpV2LocPortIfIndex);

        if (NULL == pEvbUapIfEntry)
        {
            return SNMP_FAILURE;
        }
        if (pEvbUapIfEntry->i4EvbSysEvbLldpTxEnable == VLAN_EVB_UAP_CDCP_ENABLE)
        {
            *pi4NextLldpV2LocPortIfIndex = (INT4) pEvbUapIfEntry->u4UapIfIndex;
            return SNMP_SUCCESS;
        }
        i4LldpV2LocPortIfIndex = (INT4) pEvbUapIfEntry->u4UapIfIndex;
    }
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocCdcpTlvString
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocCdcpTlvString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocCdcpTlvString (INT4 i4LldpV2LocPortIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValLldpV2Xdot1LocCdcpTlvString)
{
	tEvbUapIfEntry *pUapIfEntry = NULL;

    pUapIfEntry = VlanEvbUapIfGetEntry ((UINT4)i4LldpV2LocPortIfIndex);
	if (pUapIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

	MEMSET (pRetValLldpV2Xdot1LocCdcpTlvString->pu1_OctetList ,0,
			VLAN_EVB_CDCP_TLV_LEN);
     
	pRetValLldpV2Xdot1LocCdcpTlvString->i4_Length = 
        (pUapIfEntry->u2UapCdcpTlvLength) - (VLAN_EVB_CDCP_TLV_HDR_LEN +
                                            VLAN_EVB_CDCP_MANDATORY);

	if (pRetValLldpV2Xdot1LocCdcpTlvString->i4_Length > 0)
	{
		MEMCPY(pRetValLldpV2Xdot1LocCdcpTlvString->pu1_OctetList ,
                &(pUapIfEntry->au1UapCdcpTlv
                  [VLAN_EVB_CDCP_TLV_HDR_LEN + VLAN_EVB_CDCP_MANDATORY]),
  		     pRetValLldpV2Xdot1LocCdcpTlvString->i4_Length);
	}
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : LldpV2Xdot1RemEvbTlvTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemEvbTlvTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceLldpV2Xdot1RemEvbTlvTable
    (UINT4 u4LldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex)
{
    /*Not supported*/
	UNUSED_PARAM(u4LldpV2RemTimeMark);
	UNUSED_PARAM(i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemEvbTlvTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexLldpV2Xdot1RemEvbTlvTable
    (UINT4 *pu4LldpV2RemTimeMark,
    INT4 *pi4LldpV2RemLocalIfIndex , 
     UINT4 *pu4LldpV2RemLocalDestMACAddress, INT4 *pi4LldpV2RemIndex)
{
    /*Not supported*/
	UNUSED_PARAM(pu4LldpV2RemTimeMark);
	UNUSED_PARAM(pi4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (pu4LldpV2RemLocalDestMACAddress);
	UNUSED_PARAM(pi4LldpV2RemIndex);
	return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemEvbTlvTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexLldpV2Xdot1RemEvbTlvTable
    (UINT4 u4LldpV2RemTimeMark, UINT4 *pu4NextLldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex, INT4 *pi4NextLldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress,
     UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
    INT4 i4LldpV2RemIndex ,INT4 *pi4NextLldpV2RemIndex )
{
    /*Not supported*/
	UNUSED_PARAM(u4LldpV2RemTimeMark);
	UNUSED_PARAM(pu4NextLldpV2RemTimeMark);
	UNUSED_PARAM(i4LldpV2RemLocalIfIndex);
	UNUSED_PARAM(pi4NextLldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (pu4NextLldpV2RemLocalDestMACAddress);
	UNUSED_PARAM(i4LldpV2RemIndex);
	UNUSED_PARAM(pi4NextLldpV2RemIndex);
	return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemEvbTlvString
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot1RemEvbTlvString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetLldpV2Xdot1RemEvbTlvString
    (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex,
    tSNMP_OCTET_STRING_TYPE * pRetValLldpV2Xdot1RemEvbTlvString)
{
    /*Not supported*/
	UNUSED_PARAM(u4LldpV2RemTimeMark);
	UNUSED_PARAM(i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
	UNUSED_PARAM(i4LldpV2RemIndex);
	UNUSED_PARAM(pRetValLldpV2Xdot1RemEvbTlvString);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : LldpV2Xdot1RemCdcpTlvTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemCdcpTlvTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceLldpV2Xdot1RemCdcpTlvTable
    (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex)
{
    /*Not supported*/
	UNUSED_PARAM(u4LldpV2RemTimeMark);
	UNUSED_PARAM(i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
	UNUSED_PARAM(i4LldpV2RemIndex);
	return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemCdcpTlvTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemCdcpTlvTable (UINT4 *pu4LldpV2RemTimeMark,
    INT4 *pi4LldpV2RemLocalIfIndex , 
                                            UINT4
                                            *pu4LldpV2RemLocalDestMACAddress,
    INT4 *pi4LldpV2RemIndex)
{
    /*Not supported*/
	UNUSED_PARAM(pu4LldpV2RemTimeMark);
	UNUSED_PARAM(pi4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (pu4LldpV2RemLocalDestMACAddress);
	UNUSED_PARAM(pi4LldpV2RemIndex);
	return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemCdcpTlvTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexLldpV2Xdot1RemCdcpTlvTable
    (UINT4 u4LldpV2RemTimeMark, UINT4 *pu4NextLldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex, INT4 *pi4NextLldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress,
     UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
    INT4 i4LldpV2RemIndex ,INT4 *pi4NextLldpV2RemIndex )
{
    /*Not supported*/
	UNUSED_PARAM(u4LldpV2RemTimeMark);
	UNUSED_PARAM(pu4NextLldpV2RemTimeMark);
	UNUSED_PARAM(i4LldpV2RemLocalIfIndex);
	UNUSED_PARAM(pi4NextLldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (pu4NextLldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
	UNUSED_PARAM(pi4NextLldpV2RemIndex);
	return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemCdcpTlvString
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot1RemCdcpTlvString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemCdcpTlvString (UINT4 u4LldpV2RemTimeMark,
    INT4 i4LldpV2RemLocalIfIndex , 
                                   UINT4 u4LldpV2RemLocalDestMACAddress,
    INT4 i4LldpV2RemIndex , 
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValLldpV2Xdot1RemCdcpTlvString)
{
    UNUSED_PARAM(u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM(i4LldpV2RemIndex);

    tEvbUapIfEntry *pUapIfEntry = NULL;

    pUapIfEntry = VlanEvbUapIfGetEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pUapIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pRetValLldpV2Xdot1RemCdcpTlvString->pu1_OctetList ,0,
            VLAN_EVB_CDCP_TLV_LEN);

    pRetValLldpV2Xdot1RemCdcpTlvString->i4_Length =
        (pUapIfEntry->u2UapCdcpTlvLength) - (VLAN_EVB_CDCP_TLV_HDR_LEN +
                                            VLAN_EVB_CDCP_MANDATORY);

    if (pRetValLldpV2Xdot1RemCdcpTlvString->i4_Length > 0)
    {
        MEMCPY(pRetValLldpV2Xdot1RemCdcpTlvString->pu1_OctetList ,
                &(pUapIfEntry->au1UapCdcpTlv
                  [VLAN_EVB_CDCP_TLV_HDR_LEN + VLAN_EVB_CDCP_MANDATORY]),
             pRetValLldpV2Xdot1RemCdcpTlvString->i4_Length);
}
    return SNMP_SUCCESS;

}

