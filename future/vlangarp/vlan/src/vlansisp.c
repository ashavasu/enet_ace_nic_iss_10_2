
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vlansisp.c,v 1.12 2014/02/03 12:35:03 siva Exp $                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*                                                                           */
/*  FILE NAME             : vlansisp.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Feb 2009                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains VLAN and SISP related routines*/
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 16/02/2009                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"

/***************************************************************************/
/* Function Name    : VlanSispCopySispPortPropertiesToHw()                 */
/*                                                                         */
/* Description      : This function programs the hardware with all the     */
/*                       properties of the logical interface. The port     */
/*                    properties  of the "u2DstPort" is programmed along   */
/*                    port channel index "u2SrcPort".                      */
/*                                                                         */
/* Input(s)         : u2SrcLocalPort - The Port whose properties have to be*/
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u4DstPort - The Port which is programmed along with  */
/*                                properties of u2SrcLocalPort             */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanSispCopySispPortPropertiesToHw (UINT4 u4DstPort, UINT2 u2SrcLocalPort)
{

#ifdef GARP_EXTENDED_FILTER
    UINT1              *pPorts = NULL;
#endif
    UINT1              *pUnTagPorts = NULL;
    UINT1               u1Result = VLAN_INIT_VAL;
    UINT1               u1IsTagged = VLAN_INIT_VAL;
    tVlanId             VlanId = VLAN_INIT_VAL;
    tVlanPortEntry     *pSrcPortEntry = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2DstLocalPort = 0;

    /*Check VLAN is not shut down in secondary context */
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispCopySispPortPropertiesToHw ():"
                  "Vlan is Shut down.\n");
        return VLAN_SUCCESS;
    }
    /*Check VLAN is enabled in secondary context */
    if (VLAN_MODULE_STATUS () != VLAN_ENABLED)
    {

        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispCopySispPortPropertiesToHw ():"
                  "Vlan is disabled.\n");
        return VLAN_SUCCESS;
    }

    pSrcPortEntry = VLAN_GET_PORT_ENTRY (u2SrcLocalPort);

    if (pSrcPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispCopySispPortPropertiesToHw():"
                  "Invalid Source Port.\n");
        return VLAN_SUCCESS;
    }

    /* Program the portchannel to the secondary context in the hardware.
     * This is done so that hardware does not return failure when the secondary
     * context related properties are programmed into the hardware.
     * */
#ifndef LA_HW_TRUNK_SUPPORTED
#ifdef NPAPI_WANTED
    if (VcmMapPortToContextInHw (VLAN_CURR_CONTEXT_ID (),
                                 u4DstPort) == VCM_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "Mapping aggregate %d to "
                       "context failed\n", u4DstPort);
        return VLAN_FAILURE;
    }
#endif
#endif
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanSispCopySispPortPropertiesToHw (): Invalid Vlan entry\n");
            return (VLAN_FAILURE);
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2SrcLocalPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if (pCurrEntry->pStVlanEntry == NULL)
            {
                u1IsTagged = VLAN_TRUE;
            }
            else
            {
                pUnTagPorts =
                    UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

                if (pUnTagPorts == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "VlanFwdOnPorts: Error in allocating memory "
                              "for pUnTagPorts\r\n");
                    return VLAN_FAILURE;
                }

                VLAN_MEMSET (pUnTagPorts, VLAN_INIT_VAL,
                             sizeof (tLocalPortList));

                VLAN_IS_UNTAGGED_PORT (pCurrEntry->pStVlanEntry,
                                       u2SrcLocalPort, u1Result);
                UtilPlstReleaseLocalPortList (pUnTagPorts);

                if (u1Result == VLAN_TRUE)
                {
                    u1IsTagged = VLAN_FALSE;
                }
                else
                {
                    u1IsTagged = VLAN_TRUE;
                }
            }

#ifdef NPAPI_WANTED

            if (VlanFsMiVlanHwSetVlanMemberPort (VLAN_CURR_CONTEXT_ID (),
                                                 pCurrEntry->VlanId,
                                                 u4DstPort,
                                                 u1IsTagged) != FNP_SUCCESS)
            {
                VLAN_TRC_ARG3
                    (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                     "VlanSispCopySispPortPropertiesToHw (): Unable to"
                     "add the Port to the Vlan %d. Src Port"
                     " %d - Destination Port %d\n", pCurrEntry->VlanId,
                     u2SrcLocalPort, u4DstPort);
                return VLAN_FAILURE;
            }
#endif
        }
    }

    if (pCurrEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispCopySispPortPropertiesToHw (): Invalid Vlan entry\n");
        return (VLAN_FAILURE);
    }
#ifdef GARP_EXTENDED_FILTER
    pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispCopySispPortPropertiesToHw: Error in allocating memory "
                  "for pPorts\r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pPorts, VLAN_INIT_VAL, sizeof (tLocalPortList));
    VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.Ports, u2SrcLocalPort, u1Result);

    if (u1Result == VLAN_TRUE)
    {
        VLAN_MEMSET (pPorts, VLAN_INIT_VAL, sizeof (tLocalPortList));

        VLAN_ADD_PORT_LIST (pPorts, pCurrEntry->AllGrps.Ports);

        if (VlanHwSetDefGroupInfoForSisp (VLAN_CURR_CONTEXT_ID (),
                                          pCurrEntry->VlanId, VLAN_ALL_GROUPS,
                                          pPorts, u4DstPort) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                           VLAN_NAME,
                           "VlanCopyPortPropertiesToHw (): "
                           "Unable to program the All Groups "
                           "membership for the Vlan %d. Src Port %d"
                           ", Destination Port %d\n", pCurrEntry->VlanId,
                           u2SrcLocalPort, u4DstPort);
            UtilPlstReleaseLocalPortList (pPorts);
            return VLAN_FAILURE;
        }
    }

    VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.Ports, u2SrcLocalPort, u1Result);

    if (u1Result == VLAN_TRUE)
    {
        VLAN_MEMSET (pPorts, VLAN_INIT_VAL, sizeof (tLocalPortList));

        VLAN_ADD_PORT_LIST (pPorts, pCurrEntry->UnRegGrps.Ports);

        if (VlanHwSetDefGroupInfoForSisp (VLAN_CURR_CONTEXT_ID (),
                                          pCurrEntry->VlanId, VLAN_UNREG_GROUPS,
                                          pPorts, u4DstPort) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                           VLAN_NAME,
                           "VlanCopyPortPropertiesToHw (): "
                           "Unable to program the UnReg Groups "
                           "membership for the Vlan %d. Src Port %d, "
                           "Destination Port %d.\n", pCurrEntry->VlanId,
                           u2SrcLocalPort, u4DstPort);
            UtilPlstReleaseLocalPortList (pPorts);
            return VLAN_FAILURE;
        }
    }
    UtilPlstReleaseLocalPortList (pPorts);
#endif

    if (VlanSispCopyLogicalIntfPropToHw (u4DstPort, u2SrcLocalPort)
        != VLAN_SUCCESS)
    {

        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC |
                       CONTROL_PLANE_TRC, VLAN_NAME,
                       "VlanHandleCopyPhyPortPropertiesToHw failed."
                       "Source Port %d, Destination Port %d.\n",
                       u2SrcLocalPort, u4DstPort);
        return VLAN_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE)
    {
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {

            if (VcmGetContextInfoFromIfIndex (u4DstPort, &u4ContextId,
                                              &u2DstLocalPort) == VCM_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC |
                               CONTROL_PLANE_TRC, VLAN_NAME,
                               "VcmGetContextInfoFromIfIndex failed. "
                               "Destination Port %d.\n", u4DstPort);
                return VLAN_FAILURE;
            }

            if (VlanPbHandleCopyPbPortPropertiesToHw
                (u2DstLocalPort, u2SrcLocalPort,
                 VLAN_S_INTERFACE_TYPE) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC |
                               CONTROL_PLANE_TRC, VLAN_NAME,
                               "VlanHandlePbPortPropertiesToHw failed."
                               "Source Port %d, Destination Port %d.\n",
                               u2SrcLocalPort, u4DstPort);
                return VLAN_FAILURE;
            }
        }
    }

#ifndef NPAPI_WANTED
    UNUSED_PARAM (u1IsTagged);
#endif

    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanSispCopyLogicalIntfPropToHw                      */
/*                                                                         */
/* Description      : This function programs copies the u4SrcPort          */
/*                    properties in the hardware with u4DstPort as         */
/*                    index                                                */
/*                                                                         */
/*                    This function must called only for Port Channel      */
/*                                                                         */
/* Input(s)         : u4DstPort - Port which is added in a port channel    */
/*                    u2SrcLocalPort - to indentify the properties of      */
/*                                    the source port(logical interface)   */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanSispCopyLogicalIntfPropToHw (UINT4 u4DstPort, UINT2 u2SrcLocalPort)
{
    UINT2               u2PriIndex = VLAN_INIT_VAL;
    UINT4               u4Mode = VLAN_INVALID_BRIDGE_MODE;
    tVlanPortEntry     *pSrcPortEntry = NULL;

#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        pSrcPortEntry = VLAN_GET_PORT_ENTRY (u2SrcLocalPort);

        if (L2IwfMiIsVlanActive (VLAN_CURR_CONTEXT_ID (),
                                 pSrcPortEntry->Pvid) == OSIX_TRUE)
        {
            /* we program the pvid in hardware only if the vlan is active 
               we pass the secondary context Id and IfIndex of the port 
               to be added in port channel and Pvid of source port (logical 
               interface) */

            if (VlanFsMiVlanHwSetPortPvid (VLAN_CURR_CONTEXT_ID (), u4DstPort,
                                           pSrcPortEntry->Pvid) == FNP_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "VlanSispCopySispPortPropertiesToHw ():"
                               "Unable to program Pvid. Src Port %d, Destination Port %d\n",
                               u2SrcLocalPort, u4DstPort);
                return VLAN_FAILURE;
            }

        }

        /* The Acceptable frame type is programmed with the following args
         * Secondary Context Identifier and port to be added to the port 
         * channel
         * */
        if (VlanFsMiVlanHwSetPortAccFrameType
            (VLAN_CURR_CONTEXT_ID (), u4DstPort,
             pSrcPortEntry->u1AccpFrmTypes) == FNP_FAILURE)
        {

            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME,
                           "VlanSispCopySispPortPropertiesToHw (): Unable to program "
                           "Acceptable Frame Types. Src Port %d, Destination Port %d\n",
                           u2SrcLocalPort, u4DstPort);

            return VLAN_FAILURE;
        }

        /* The Ingress Filtering is programmed with the following args
         * Secondary Context Identifier and port to be added to the port 
         * channel
         * */
        if (pSrcPortEntry->u1IngFiltering == VLAN_ENABLED)
        {

            if (VlanFsMiVlanHwSetPortIngFiltering (VLAN_CURR_CONTEXT_ID (),
                                                   u4DstPort,
                                                   FNP_TRUE) == FNP_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "VlanSispCopySispPortPropertiesToHw (): Unable to program "
                               "Ingress Filtering. Src Port %d, Destination Port %d.\n",
                               u2SrcLocalPort, u4DstPort);

                return VLAN_FAILURE;
            }
        }
        else
        {

            if (VlanFsMiVlanHwSetPortIngFiltering (VLAN_CURR_CONTEXT_ID (),
                                                   u4DstPort,
                                                   FNP_FALSE) == FNP_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "VlanSispCopySispPortPropertiesToHw (): Unable to program "
                               "Ingress Filtering. Src Port %d, Destination Port %d.\n",
                               u2SrcLocalPort, u4DstPort);

                return VLAN_FAILURE;
            }

        }

        /*
         * Default user priority can be set even if the priority module is
         * disabled, since all priorities will be mapping to the same traffic
         * class queue. Hence setting default user priority in the hardware
         * will not cause any problems. Also the VLAN module include priority 
         * in the tag header. So the priority changes should be given to h/w.
         */
        if (VlanFsMiVlanHwSetDefUserPriority (VLAN_CURR_CONTEXT_ID (),
                                              u4DstPort,
                                              pSrcPortEntry->
                                              u1DefUserPriority) != FNP_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME,
                           "VlanSispCopySispPortPropertiesToHw(): Unable to program "
                           "Default User Priority. Src Port %d, Destination Port %d\n",
                           u2SrcLocalPort, u4DstPort);
            return VLAN_FAILURE;
        }

        if (VlanFsMiVlanHwSetPortNumTrafClasses (VLAN_CURR_CONTEXT_ID (),
                                                 u4DstPort,
                                                 pSrcPortEntry->
                                                 u1NumTrfClass) != FNP_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME,
                           "VlanSispCopySispPortPropertiesToHw (): Unable to program "
                           "Number of Traffic Classes. Src Port %d, Destination Port %d\n",
                           u2SrcLocalPort, u4DstPort);
            return VLAN_FAILURE;
        }

        for (u2PriIndex = VLAN_INIT_VAL; u2PriIndex < VLAN_MAX_PRIORITY;
             u2PriIndex++)
        {
            if ((pSrcPortEntry->u1IfType != VLAN_ETHERNET_INTERFACE_TYPE) &&
                (pSrcPortEntry->u1IfType != VLAN_LAGG_INTERFACE_TYPE))
            {
                /* Priority regen table valid only for non-ethernet interfaces.
                 * Since LAGG interfaces are also formed only over ethernet
                 * interfaces, priority regen table not valid for LAGG 
                 * interfaces also. */
                if (VlanFsMiVlanHwSetRegenUserPriority (VLAN_CURR_CONTEXT_ID (),
                                                        u4DstPort,
                                                        (INT4) u2PriIndex,
                                                        (INT4) pSrcPortEntry->
                                                        au1PriorityRegen
                                                        [u2PriIndex]) !=
                    FNP_SUCCESS)
                {

                    VLAN_TRC_ARG2 (VLAN_MOD_TRC,
                                   ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   VLAN_NAME,
                                   "VlanSispCopySispPortPropertiesToHw (): "
                                   "Setting Regen user priority table failed."
                                   "Src Port %d, Destination Port %d.\n",
                                   u2SrcLocalPort, u4DstPort);

                    return VLAN_FAILURE;
                }
            }

            if (VlanFsMiVlanHwSetTraffClassMap
                (VLAN_CURR_CONTEXT_ID (), u4DstPort, (INT4) u2PriIndex,
                 pSrcPortEntry->au1TrfClassMap[u2PriIndex]) != FNP_SUCCESS)

            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "VlanSispCopySispPortPropertiesToHw (): Setting Traffic "
                               "Class mapping table failed. Src Port %d, Destination Port "
                               "%d.\n", u2SrcLocalPort, u4DstPort);

                return VLAN_FAILURE;
            }
        }

        if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
        {
            u4Mode = VLAN_NO_TUNNEL_PORT;
        }
        else
        {
            if (VLAN_TUNNEL_STATUS (pSrcPortEntry) == VLAN_ENABLED)
            {
                u4Mode = VLAN_TUNNEL_EXTERNAL;
            }
            else
            {
                /* Bridge is running in Provider network.
                 * By default tunnelling is disabled
                 * on all ports. */
                u4Mode = VLAN_TUNNEL_INTERNAL;    /* Indicates this port is
                                                 * connected to another provider
                                                 * bridge */
            }
        }
        if (VlanFsMiVlanHwSetPortTunnelMode (VLAN_CURR_CONTEXT_ID (),
                                             u4DstPort, u4Mode) == FNP_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           VLAN_NAME, "Setting tunnel mode fails."
                           "Port %d creation failed.\n", u4DstPort);
            return VLAN_FAILURE;
        }

    }

#else
    UNUSED_PARAM (u2PriIndex);
    UNUSED_PARAM (u4Mode);
    UNUSED_PARAM (u4DstPort);
    UNUSED_PARAM (u2SrcLocalPort);
    UNUSED_PARAM (pSrcPortEntry);
#endif
    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanSispHandleDelAndCopySispPortProp                 */
/*                                                                         */
/* Description      : This function finds out all the logical interface    */
/*                    associated with the given port channel from a logical*/
/*                    interface bit map. Logical interface bit map is      */
/*                    received from SISP moudule.                          */
/*                                                                         */
/* Input(s)         : u4DstPort - Ifindex of port in a port channel,       */
/*                                to be added or deleted in HW             */
/*                    u4SrcPort -  IfIndex of port channel to indentify    */
/*                                 logical interface associated with it    */
/*                    u2MsgType -  To indentify copy or remove logical     */
/*                                 interface properties from hardware      */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanSispHandleDelAndCopySispPortProp (UINT4 u4DstPort,
                                      UINT4 u4SrcPort, UINT2 u2MsgType)
{
    UINT2               au2SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortCnt = VLAN_INIT_VAL;
    UINT4               u4TempPortCnt = VLAN_INIT_VAL;
    UINT4               u4Ctx = VLAN_INVALID_CONTEXT;
    UINT2               u2SrcLocalPort = VLAN_INVALID_PORT;
    UINT1               u1InterfaceType = VLAN_INVALID_INTERFACE_TYPE;

    VLAN_MEMSET (au2SispPorts, VLAN_INIT_VAL, sizeof (au2SispPorts));

    /*Get all the logical interface associated with u4SrcPort(Portchannel) */
    if (VlanVcmSispGetSispPortsInfoOfPhysicalPort (u4SrcPort, VCM_TRUE,
                                                   (VOID *) au2SispPorts,
                                                   &u4PortCnt) == VCM_FAILURE)
    {

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanSispHandleDelAndCopySispPortProp (): failed in"
                       "getting logical port list for interface %d \n",
                       u4DstPort);

        return VLAN_FAILURE;
    }

    /*Scan the logical interface array list */
    for (u4Ctx = 0; (u4TempPortCnt < u4PortCnt); u4Ctx++)
    {
        u2SrcLocalPort = au2SispPorts[u4Ctx];

        if (u2SrcLocalPort == VLAN_INIT_VAL)
        {
            continue;
        }

        u4TempPortCnt++;

        if (VlanSelectContext (u4Ctx) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "VlanSispHandleDelAndCopySispPortProp(): failed in"
                           "programming logical interface properties in Hw "
                           "for interface %d \n", u4DstPort);
            return VLAN_FAILURE;
        }

        switch (u2MsgType)
        {
            case VLAN_COPY_PORT_INFO_MSG:

                VlanL2IwfGetInterfaceType (u4Ctx, &u1InterfaceType);

                if (VlanHandleCopyPortPropertiesToHw (u2SrcLocalPort,
                                                      u2SrcLocalPort,
                                                      u1InterfaceType)
                    != VLAN_SUCCESS)

                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanSispHandleDelAndCopySispPortProp():"
                                   "failed in programming logical interface "
                                   "properties in Hw for interface %d\n",
                                   u4DstPort);

                    return VLAN_FAILURE;
                }

                break;

            case VLAN_PORT_DEL_COPY_PROPERTIES_MSG:
                if (VlanSispCopySispPortPropertiesToHw (u4DstPort,
                                                        u2SrcLocalPort)
                    != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanSispHandleDelAndCopySispPortProp ():"
                                   "failed in programming logical interface "
                                   "properties in Hw for interface %d\n",
                                   u4DstPort);

                    return VLAN_FAILURE;
                }
                break;

            case VLAN_REMOVE_PORT_INFO_MSG:
#ifdef NPAPI_WANTED
                if (VlanHandleRemovePortPropertiesFromHw (u4DstPort,
                                                          u2SrcLocalPort,
                                                          VLAN_TRUE) !=
                    VLAN_SUCCESS)

                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanSispHandleDelAndCopySispPortProp():"
                                   "failed in deleting logical interface from "
                                   "HW for interface %d \n", u4DstPort);

                    return VLAN_FAILURE;
                }

#ifndef LA_HW_TRUNK_SUPPORTED
                if (VcmUnMapPortFromContextInHw
                    (VLAN_CURR_CONTEXT_ID (), u4DstPort) == VCM_FAILURE)
                {

                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "Mapping aggregate %d to "
                                   "context failed\n", u4DstPort);

                    return VLAN_FAILURE;
                }
#endif
#endif
                break;

            case VLAN_COPY_PORT_UCAST_INFO_MSG:
                if (VlanHandleCopyPortUcastPropertiesToHw (u2SrcLocalPort)
                    != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanSispHandleDelAndCopySispPortProp():"
                                   "failed in programming unicast properties of"
                                   "logical interface in HW for "
                                   "interface %d \n", u4DstPort);

                    return VLAN_FAILURE;
                }
                break;

            case VLAN_COPY_PORT_MCAST_INFO_MSG:
                if (VlanHandleCopyPortMcastPropertiesToHw (u2SrcLocalPort)
                    != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanSispHandleDelAndCopySispPortProp():"
                                   "failed in programming multicast properties "
                                   "of logical interface in HW for interface "
                                   "%d \n", u4DstPort);

                    return VLAN_FAILURE;
                }
                break;

            default:
                return VLAN_FAILURE;

        }
    }
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanSispUpdatePortVlanTable                          */
/*                                                                           */
/* Description        : This function will update the Port-VLAN-Context      */
/*                      Mapping information in SISP Module, when there is any*/
/*                      VLAN membership updation. PortList (Added or Delted) */
/*                      will be removed for the particular port, if that     */
/*                      port is already configured for Relay VLAN in VLAN    */
/*                      translatio table.                                    */
/*                                                                           */
/* Input(s)           :  u4ContextId    -  ContextId                         */
/*                       tVlanId        -  VlanId,                           */
/*                       tLocalPortList -  AddedPorts,                       */
/*                       tLocalPortList -  DeletedPorts                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanSispUpdatePortVlanTable (UINT4 u4ContextId, tVlanId VlanId,
                             tLocalPortList AddedPorts,
                             tLocalPortList DeletedPorts)
{
    UINT1              *pAddPortList = NULL;
    UINT1              *pNullPortList = NULL;
    UINT1              *pDelPortList = NULL;
    tVidTransEntryInfo  VidTransInfo;
    UINT2               u2LocalPort = 1;
    UINT2               u2PortListSize = sizeof (tLocalPortList);
    UINT1               u1Result = OSIX_FALSE;
    INT4                i4RetVal;

    pAddPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispUpdatePortVlanTable: Error in allocating memory "
                  "for pAddPortList\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pAddPortList, VLAN_INIT_VAL, u2PortListSize);

    pNullPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pNullPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispUpdatePortVlanTable: Error in allocating memory "
                  "for pNullPortList\r\n");
        UtilPlstReleaseLocalPortList (pAddPortList);
        return VLAN_FAILURE;
    }
    MEMSET (pNullPortList, VLAN_INIT_VAL, u2PortListSize);

    MEMCPY (pAddPortList, AddedPorts, u2PortListSize);
    MEMCPY (pNullPortList, DeletedPorts, u2PortListSize);

    if ((VLAN_MEMCMP (pAddPortList, pNullPortList, u2PortListSize))
        != VLAN_INIT_VAL)
    {
        for (; u2LocalPort <= VLAN_MAX_PORTS; u2LocalPort++)
        {
            OSIX_BITLIST_IS_BIT_SET (pAddPortList, u2LocalPort, u2PortListSize,
                                     u1Result);

            if (u1Result == OSIX_FALSE)
            {
                continue;
            }

            if (VlanL2IwfPbGetVidTransEntry (u4ContextId, u2LocalPort,
                                             VlanId, OSIX_TRUE,
                                             &VidTransInfo) == L2IWF_SUCCESS)
            {
                /* Vlan translation entry is configured for the port & Local 
                   vlan, so don't add to PVC table. PVC table entry would have
                   been already added for the same port, vlan & context in
                   VLAN translation entry configuration itself */

                OSIX_BITLIST_RESET_BIT (pAddPortList, u2LocalPort,
                                        u2PortListSize);
            }
        }
    }
    pDelPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pDelPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanSispUpdatePortVlanTable: Error in allocating memory "
                  "for pDelPortList\r\n");
        UtilPlstReleaseLocalPortList (pAddPortList);
        UtilPlstReleaseLocalPortList (pNullPortList);
        return VLAN_FAILURE;
    }

    if ((VLAN_MEMCMP (pDelPortList, pNullPortList, u2PortListSize))
        != VLAN_INIT_VAL)
    {
        for (; u2LocalPort <= VLAN_MAX_PORTS; u2LocalPort++)
        {
            OSIX_BITLIST_IS_BIT_SET (pDelPortList, u2LocalPort, u2PortListSize,
                                     u1Result);

            if (u1Result == OSIX_FALSE)
            {
                continue;
            }

            if (VlanL2IwfPbGetVidTransEntry (u4ContextId, u2LocalPort,
                                             VlanId, OSIX_TRUE,
                                             &VidTransInfo) == L2IWF_SUCCESS)
            {
                /* Vlan translation entry is configured for the port & Local 
                   vlan, so don't delete from PVC table. PVC table entry will
                   be deleted for the same port, vlan & context in
                   VLAN translation entry deletion */

                OSIX_BITLIST_RESET_BIT (pDelPortList, u2LocalPort,
                                        u2PortListSize);
            }
        }
    }

    i4RetVal = VlanVcmSispUpdatePortVlanMapping (u4ContextId, VlanId,
                                                 pAddPortList, pDelPortList);

    UtilPlstReleaseLocalPortList (pAddPortList);
    UtilPlstReleaseLocalPortList (pNullPortList);
    UtilPlstReleaseLocalPortList (pDelPortList);
    return ((i4RetVal == VCM_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE);
}

/*****************************************************************************/
/* Function Name      : VlanSispUpdatePortVlanTableOnPort                    */
/*                                                                           */
/* Description        : This function will update the Port-VLAN-Context      */
/*                      mapping information in SISP Module, when there is    */
/*                      any VLAN membership updation. If the port is already */
/*                      configured as Relay Vlan in the VLAN translation,    */
/*                      then PVC table updation will not be done.            */
/*                                                                           */
/* Input(s)           :  u4ContextId    -  ContextId                         */
/*                       tVlanId        -  VlanId,                           */
/*                       u2Port         -  Local port number                 */
/*                       u1Status       -  Add / Delete flag                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanSispUpdatePortVlanTableOnPort (UINT4 u4ContextId, tVlanId VlanId,
                                   UINT2 u2Port, UINT1 u1Status)
{
    tVidTransEntryInfo  VidTransInfo;
    INT4                i4RetVal = VLAN_SUCCESS;

    if (VlanL2IwfPbGetVidTransEntry (u4ContextId, u2Port, VlanId, OSIX_TRUE,
                                     &VidTransInfo) != L2IWF_SUCCESS)
    {
        /* Vlan translation entry is configured for the port & Local 
           vlan, so don't add to PVC table. PVC table entry would have
           been already added for the same port, vlan & context in
           VLAN translation entry configuration itself */
        if (u2Port >= VLAN_MAX_PORTS + 1)
        {
            i4RetVal = VLAN_FAILURE;
            return i4RetVal;
        }

        if ((VLAN_GET_PORT_ENTRY (u2Port) == NULL))
        {
            i4RetVal = VLAN_FAILURE;
            return i4RetVal;

        }

        i4RetVal = VcmSispUpdatePortVlanMappingOnPort (u4ContextId, VlanId,
                                                       VLAN_GET_IFINDEX
                                                       (u2Port), u1Status);

        i4RetVal = ((i4RetVal == VCM_SUCCESS) ? VLAN_SUCCESS : VLAN_FAILURE);
    }

    return i4RetVal;
}
