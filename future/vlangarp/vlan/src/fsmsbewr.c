/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbewr.c,v 1.6 2011/10/25 10:39:08 siva Exp $
*
* Description: Protocol wrapper file 
*********************************************************************/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsmsbewr.h"
# include  "fsmsbedb.h"

INT4
GetNextIndexFsDot1dExtBaseTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dExtBaseTable (&i4FsDot1dVlanContextId) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dExtBaseTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dVlanContextId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dDeviceCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dExtBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dDeviceCapabilities (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTrafficClassesEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dExtBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTrafficClassesEnabled (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dGmrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dExtBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal = nmhGetFsDot1dGmrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTrafficClassesEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1dTrafficClassesEnabled (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dGmrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetFsDot1dGmrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTrafficClassesEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dTrafficClassesEnabled (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dExtBaseTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dExtBaseTable (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dGmrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1dGmrpStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dPortCapabilitiesTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dPortCapabilitiesTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dPortCapabilitiesTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dPortCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortCapabilitiesTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortCapabilities (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dPortPriorityTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dPortPriorityTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dPortPriorityTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dPortDefaultUserPriorityGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortDefaultUserPriority (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortNumTrafficClassesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortNumTrafficClasses (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortDefaultUserPrioritySet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortDefaultUserPriority (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortNumTrafficClassesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortNumTrafficClasses (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortDefaultUserPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortDefaultUserPriority (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortNumTrafficClassesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortNumTrafficClasses (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortPriorityTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dPortPriorityTable (pu4Error, pSnmpIndexList,
                                                 pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dUserPriorityRegenTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;
    INT4                i4FsDot1dUserPriority;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dUserPriorityRegenTable (&i4FsDot1dBasePort,
                                                           &i4FsDot1dUserPriority)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dUserPriorityRegenTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4FsDot1dUserPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4FsDot1dUserPriority;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dRegenUserPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dUserPriorityRegenTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dRegenUserPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dRegenUserPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1dRegenUserPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dRegenUserPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dRegenUserPriority (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dUserPriorityRegenTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dUserPriorityRegenTable (pu4Error,
                                                      pSnmpIndexList,
                                                      pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dTrafficClassTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;
    INT4                i4FsDot1dTrafficClassPriority;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dTrafficClassTable (&i4FsDot1dBasePort,
                                                      &i4FsDot1dTrafficClassPriority)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dTrafficClassTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4FsDot1dTrafficClassPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4FsDot1dTrafficClassPriority;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dTrafficClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTrafficClassTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1dTrafficClass (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTrafficClassSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetFsDot1dTrafficClass (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTrafficClassTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1dTrafficClass (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTrafficClassTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1dTrafficClassTable (pu4Error, pSnmpIndexList,
                                                 pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dPortOutboundAccessPriorityTable (tSnmpIndex *
                                                    pFirstMultiIndex,
                                                    tSnmpIndex *
                                                    pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;
    INT4                i4FsDot1dRegenUserPriority;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dPortOutboundAccessPriorityTable
            (&i4FsDot1dBasePort, &i4FsDot1dRegenUserPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dPortOutboundAccessPriorityTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4FsDot1dRegenUserPriority) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4FsDot1dRegenUserPriority;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dPortOutboundAccessPriorityGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortOutboundAccessPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortOutboundAccessPriority (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dPortGarpTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    GARP_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dPortGarpTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dPortGarpTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    GARP_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dPortGarpJoinTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGarpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortGarpJoinTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpLeaveTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGarpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortGarpLeaveTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpLeaveAllTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGarpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortGarpLeaveAllTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpJoinTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortGarpJoinTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpLeaveTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortGarpLeaveTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpLeaveAllTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortGarpLeaveAllTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpJoinTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortGarpJoinTime (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpLeaveTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortGarpLeaveTime (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpLeaveAllTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortGarpLeaveAllTime (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGarpTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhDepv2FsDot1dPortGarpTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dPortGmrpTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    GARP_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dPortGmrpTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dPortGmrpTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            GARP_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    GARP_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dPortGmrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortGmrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGmrpFailedRegistrationsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortGmrpFailedRegistrations (pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  &(pMultiData->u4_ULongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGmrpLastPduOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    i4RetVal =
        nmhGetFsDot1dPortGmrpLastPduOrigin (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (tMacAddr *) pMultiData->
                                            pOctetStrValue->pu1_OctetList);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortRestrictedGroupRegistrationGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dPortGmrpTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        GARP_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dPortRestrictedGroupRegistration (pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      &(pMultiData->
                                                        i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGmrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortGmrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortRestrictedGroupRegistrationSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1dPortRestrictedGroupRegistration (pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGmrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortGmrpStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortRestrictedGroupRegistrationTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1dPortRestrictedGroupRegistration (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiData->
                                                                i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dPortGmrpTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhDepv2FsDot1dPortGmrpTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dTpHCPortTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dTpHCPortTable (&i4FsDot1dTpPort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dTpHCPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dTpPort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dTpHCPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpHCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpHCPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u8_Counter64Value));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpHCPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpHCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpHCPortOutFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u8_Counter64Value));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpHCPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpHCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpHCPortInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u8_Counter64Value));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1dTpPortOverflowTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dTpPortOverflowTable (&i4FsDot1dTpPort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dTpPortOverflowTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dTpPort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsDot1dTpPortInOverflowFramesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortOverflowTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpPortInOverflowFrames (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpPortOutOverflowFramesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortOverflowTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpPortOutOverflowFrames (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1dTpPortInOverflowDiscardsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1dTpPortOverflowTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1dTpPortInOverflowDiscards (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();
    return i4RetVal;
}

VOID
RegisterFSMSBE ()
{
    SNMPRegisterMib (&FsDot1dExtBaseTableOID, &FsDot1dExtBaseTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dPortCapabilitiesTableOID,
                     &FsDot1dPortCapabilitiesTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dPortPriorityTableOID,
                     &FsDot1dPortPriorityTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dUserPriorityRegenTableOID,
                     &FsDot1dUserPriorityRegenTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dTrafficClassTableOID,
                     &FsDot1dTrafficClassTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dPortOutboundAccessPriorityTableOID,
                     &FsDot1dPortOutboundAccessPriorityTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dPortGarpTableOID, &FsDot1dPortGarpTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dPortGmrpTableOID, &FsDot1dPortGmrpTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dTpHCPortTableOID, &FsDot1dTpHCPortTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsDot1dTpPortOverflowTableOID,
                     &FsDot1dTpPortOverflowTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmsbeOID, (const UINT1 *) "fsmsbext");
}

VOID
UnRegisterFSMSBE ()
{
    SNMPUnRegisterMib (&FsDot1dExtBaseTableOID, &FsDot1dExtBaseTableEntry);
    SNMPUnRegisterMib (&FsDot1dPortCapabilitiesTableOID,
                       &FsDot1dPortCapabilitiesTableEntry);
    SNMPUnRegisterMib (&FsDot1dPortPriorityTableOID,
                       &FsDot1dPortPriorityTableEntry);
    SNMPUnRegisterMib (&FsDot1dUserPriorityRegenTableOID,
                       &FsDot1dUserPriorityRegenTableEntry);
    SNMPUnRegisterMib (&FsDot1dTrafficClassTableOID,
                       &FsDot1dTrafficClassTableEntry);
    SNMPUnRegisterMib (&FsDot1dPortOutboundAccessPriorityTableOID,
                       &FsDot1dPortOutboundAccessPriorityTableEntry);
    SNMPUnRegisterMib (&FsDot1dPortGarpTableOID, &FsDot1dPortGarpTableEntry);
    SNMPUnRegisterMib (&FsDot1dPortGmrpTableOID, &FsDot1dPortGmrpTableEntry);
    SNMPUnRegisterMib (&FsDot1dTpHCPortTableOID, &FsDot1dTpHCPortTableEntry);
    SNMPUnRegisterMib (&FsDot1dTpPortOverflowTableOID,
                       &FsDot1dTpPortOverflowTableEntry);
    SNMPDelSysorEntry (&fsmsbeOID, (const UINT1 *) "fsmsbext");
}
