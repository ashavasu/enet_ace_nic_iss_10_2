/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlangfgn.c,v 1.77 2015/11/20 10:38:11 siva Exp $
 *
 * Description: This file contains the GetFirst and GetNext
 *              routines of the VLAN table.
 *
 *******************************************************************/

#include "vlaninc.h"

/*********************** Q-MIB Get First Get Next Routines *****************/

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFdbTable
 Input       :  The Indices
                Dot1qFdbId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qFdbTable (UINT4 *pu4Dot1qFdbId)
{
    tVlanFidEntry      *pFidEntry = NULL;
    UINT1               u1Flag = VLAN_FALSE;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {
            if (u1Flag == VLAN_FALSE)
            {
                *pu4Dot1qFdbId = pFidEntry->u4Fid;
                u1Flag = VLAN_TRUE;
            }
            else
            {
                if (*pu4Dot1qFdbId > pFidEntry->u4Fid)
                {
                    *pu4Dot1qFdbId = pFidEntry->u4Fid;
                }
            }
        }
    }

    if (u1Flag == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFdbTable
 Input       :  The Indices
                Dot1qFdbId
                nextDot1qFdbId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qFdbTable (UINT4 u4Dot1qFdbId, UINT4 *pu4NextDot1qFdbId)
{
    tVlanFidEntry      *pFidEntry = NULL;
    UINT1               u1Flag = VLAN_FALSE;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }
    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {

            if (pFidEntry->u4Fid > u4Dot1qFdbId)
            {

                if (u1Flag == VLAN_FALSE)
                {

                    *pu4NextDot1qFdbId = pFidEntry->u4Fid;
                    u1Flag = VLAN_TRUE;
                }
                else
                {

                    if (*pu4NextDot1qFdbId > pFidEntry->u4Fid)
                    {

                        *pu4NextDot1qFdbId = pFidEntry->u4Fid;
                    }
                }
            }
        }
    }

    if (u1Flag == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qTpFdbTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qTpFdbTable (UINT4 *pu4Dot1qFdbId,
                                 tMacAddr * pDot1qTpFdbAddress)
{
    tMacAddr            MacAddr;
#ifdef NPAPI_WANTED
    INT4                i4Result;
#endif

    /* Initializing the fdbId and fdbAddress with invalid value 0 to get the 
     * next valid entry present in the db/hw */
    *pu4Dot1qFdbId = 0;
    MEMSET (pDot1qTpFdbAddress, 0, sizeof (tMacAddr));

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    UNUSED_PARAM (MacAddr);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
#ifdef SW_LEARNING
    i4Result = VlanGetFirstTpFdbEntry (pu4Dot1qFdbId, *pDot1qTpFdbAddress);
    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else
    i4Result = VlanHwGetFirstTpFdbEntry (VLAN_CURR_CONTEXT_ID (), pu4Dot1qFdbId,
                                         *pDot1qTpFdbAddress);

    if (i4Result == VLAN_SUCCESS)
    {

        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
#else
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexDot1qTpFdbTable (0, pu4Dot1qFdbId,
                                            MacAddr, pDot1qTpFdbAddress));
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qTpFdbTable
 Input       :  The Indices
                Dot1qFdbId
                nextDot1qFdbId
                Dot1qTpFdbAddress
                nextDot1qTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qTpFdbTable (UINT4 u4Dot1qFdbId,
                                UINT4 *pu4NextDot1qFdbId,
                                tMacAddr Dot1qTpFdbAddress,
                                tMacAddr * pNextDot1qTpFdbAddress)
{

#ifdef NPAPI_WANTED
    INT4                i4Result;
    UINT4               u4NextContextId;

    *pu4NextDot1qFdbId = 0;
    MEMSET (pNextDot1qTpFdbAddress, 0, sizeof (tMacAddr));

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
#ifdef SW_LEARNING
    UNUSED_PARAM (u4NextContextId);
    i4Result
        = VlanGetNextTpFdbEntry (u4Dot1qFdbId, Dot1qTpFdbAddress,
                                 pu4NextDot1qFdbId, *pNextDot1qTpFdbAddress);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else
    i4Result = VlanHwGetNextTpFdbEntry (VLAN_CURR_CONTEXT_ID (), u4Dot1qFdbId,
                                        Dot1qTpFdbAddress, &u4NextContextId,
                                        pu4NextDot1qFdbId,
                                        *pNextDot1qTpFdbAddress);

    if (i4Result == VLAN_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif /* SW_LEARNING */
    return SNMP_FAILURE;

#else /* NPAPI_WANTED */

    tVlanFidEntry      *pFidEntry = NULL;
    tVlanFdbEntry      *pFdbEntry;
    UINT1               u1EntryFound = VLAN_FALSE;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {
            if (u4Dot1qFdbId == pFidEntry->u4Fid)
            {
                pFdbEntry = VlanGetNextFdbEntry (pFidEntry->UcastHashTbl,
                                                 Dot1qTpFdbAddress);

                if (pFdbEntry != NULL)
                {
                    *pu4NextDot1qFdbId = pFidEntry->u4Fid;
                    VLAN_CPY_MAC_ADDR ((UINT1 *) *pNextDot1qTpFdbAddress,
                                       pFdbEntry->MacAddr);
                    return SNMP_SUCCESS;
                }
            }
            else if (u4Dot1qFdbId < pFidEntry->u4Fid)
            {
                if (u1EntryFound == VLAN_FALSE)
                {
                    pFdbEntry = VlanGetFirstFdbEntry (pFidEntry->UcastHashTbl);

                    if (pFdbEntry != NULL)
                    {
                        *pu4NextDot1qFdbId = pFidEntry->u4Fid;
                        VLAN_CPY_MAC_ADDR ((UINT1 *) *pNextDot1qTpFdbAddress,
                                           pFdbEntry->MacAddr);

                        u1EntryFound = VLAN_TRUE;
                    }
                }
                else
                {
                    if (*pu4NextDot1qFdbId > pFidEntry->u4Fid)
                    {
                        pFdbEntry =
                            VlanGetFirstFdbEntry (pFidEntry->UcastHashTbl);

                        if (pFdbEntry != NULL)
                        {
                            *pu4NextDot1qFdbId = pFidEntry->u4Fid;
                            VLAN_CPY_MAC_ADDR ((UINT1 *)
                                               *pNextDot1qTpFdbAddress,
                                               pFdbEntry->MacAddr);
                        }
                    }
                }
            }
        }
    }

    if (u1EntryFound == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qTpGroupTable
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qTpGroupAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qTpGroupTable (UINT4 *pu4Dot1qVlanIndex,
                                   tMacAddr * pDot1qTpGroupAddress)
{
    tMacAddr            MacAddr;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexDot1qTpGroupTable (0, pu4Dot1qVlanIndex,
                                              MacAddr, pDot1qTpGroupAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qTpGroupTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
                Dot1qTpGroupAddress
                nextDot1qTpGroupAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qTpGroupTable (UINT4 u4Dot1qVlanIndex,
                                  UINT4 *pu4NextDot1qVlanIndex,
                                  tMacAddr Dot1qTpGroupAddress,
                                  tMacAddr * pNextDot1qTpGroupAddress)
{
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanGroupEntry    *pGroupEntry;
    tVlanId             VlanId;
    tVlanId             VlanStartId;
    UINT1               u1EntryFound = VLAN_FALSE;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (u4Dot1qVlanIndex > VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }

    VlanStartId = (tVlanId) u4Dot1qVlanIndex;

    if (VlanStartId == 0 || (VlanGetVlanEntry (VlanStartId) == NULL))
    {
        VlanStartId = VLAN_START_VLAN_INDEX ();
    }

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            continue;
        }
        if (u4Dot1qVlanIndex == pCurrEntry->VlanId)
        {
            pGroupEntry = VlanGetNextGroupEntry (pCurrEntry->pGroupTable,
                                                 Dot1qTpGroupAddress);

            if (pGroupEntry != NULL)
            {

                *pu4NextDot1qVlanIndex = pCurrEntry->VlanId;
                VLAN_CPY_MAC_ADDR ((UINT1 *) *pNextDot1qTpGroupAddress,
                                   pGroupEntry->MacAddr);

                return SNMP_SUCCESS;
            }
        }
        else
        {
            if (u4Dot1qVlanIndex < pCurrEntry->VlanId)
            {
                if (u1EntryFound == VLAN_FALSE)
                {
                    pGroupEntry =
                        VlanGetFirstGroupEntry (pCurrEntry->pGroupTable);

                    if (pGroupEntry != NULL)
                    {
                        *pu4NextDot1qVlanIndex = pCurrEntry->VlanId;
                        VLAN_CPY_MAC_ADDR
                            ((UINT1 *) *pNextDot1qTpGroupAddress,
                             pGroupEntry->MacAddr);

                        u1EntryFound = VLAN_TRUE;
                    }
                }
                else
                {
                    if (*pu4NextDot1qVlanIndex > pCurrEntry->VlanId)
                    {

                        pGroupEntry
                            = VlanGetFirstGroupEntry (pCurrEntry->pGroupTable);

                        if (pGroupEntry != NULL)
                        {

                            *pu4NextDot1qVlanIndex = pCurrEntry->VlanId;
                            VLAN_CPY_MAC_ADDR ((UINT1 *)
                                               *pNextDot1qTpGroupAddress,
                                               pGroupEntry->MacAddr);
                        }
                    }
                }
            }
        }
    }

    if (u1EntryFound == VLAN_FALSE)
    {

        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qForwardAllTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qForwardAllTable (UINT4 *pu4Dot1qVlanIndex)
{
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Result = nmhGetNextIndexDot1qForwardAllTable (0, pu4Dot1qVlanIndex);

    return i1Result;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qForwardAllTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qForwardAllTable (UINT4 u4Dot1qVlanIndex,
                                     UINT4 *pu4NextDot1qVlanIndex)
{
#ifdef GARP_EXTENDED_FILTER
    INT1                i1Status;
#endif

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

#ifdef GARP_EXTENDED_FILTER
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Status = VlanGetNextVlanId (u4Dot1qVlanIndex, pu4NextDot1qVlanIndex);

    return i1Status;

#else
    UNUSED_PARAM (u4Dot1qVlanIndex);
    UNUSED_PARAM (pu4NextDot1qVlanIndex);

    return SNMP_FAILURE;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qForwardUnregisteredTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qForwardUnregisteredTable (UINT4 *pu4Dot1qVlanIndex)
{
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Result =
        nmhGetNextIndexDot1qForwardUnregisteredTable (0, pu4Dot1qVlanIndex);

    return i1Result;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qForwardUnregisteredTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qForwardUnregisteredTable (UINT4 u4Dot1qVlanIndex,
                                              UINT4 *pu4NextDot1qVlanIndex)
{
#ifdef GARP_EXTENDED_FILTER
    INT1                i1Result;
#endif

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

#ifdef GARP_EXTENDED_FILTER

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Result = VlanGetNextVlanId (u4Dot1qVlanIndex, pu4NextDot1qVlanIndex);

    return i1Result;
#else
    UNUSED_PARAM (u4Dot1qVlanIndex);
    UNUSED_PARAM (pu4NextDot1qVlanIndex);

    return SNMP_FAILURE;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qStaticUnicastTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qStaticUnicastTable (UINT4 *pu4Dot1qFdbId,
                                         tMacAddr *
                                         pDot1qStaticUnicastAddress,
                                         INT4 *pi4Dot1qStaticUnicastReceivePort)
{
    tMacAddr            MacAddr;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexDot1qStaticUnicastTable (0,
                                                    pu4Dot1qFdbId,
                                                    MacAddr,
                                                    pDot1qStaticUnicastAddress,
                                                    0,
                                                    pi4Dot1qStaticUnicastReceivePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qStaticUnicastTable
 Input       :  The Indices
                Dot1qFdbId
                nextDot1qFdbId
                Dot1qStaticUnicastAddress
                nextDot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
                nextDot1qStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qStaticUnicastTable (UINT4 u4Dot1qFdbId,
                                        UINT4 *pu4NextDot1qFdbId,
                                        tMacAddr Dot1qStaticUnicastAddress,
                                        tMacAddr *
                                        pNextDot1qStaticUnicastAddress,
                                        INT4 i4Dot1qStaticUnicastReceivePort,
                                        INT4
                                        *pi4NextDot1qStaticUnicastReceivePort)
{
    tVlanStUcastEntry  *pStUcastEntry;
    tVlanStUcastEntry   StUcastEntry;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    MEMCPY (StUcastEntry.MacAddr,Dot1qStaticUnicastAddress, sizeof (tMacAddr));
    StUcastEntry.u2RcvPort = (UINT2) i4Dot1qStaticUnicastReceivePort;
    StUcastEntry.VlanId = (tVlanId) u4Dot1qFdbId;

    pStUcastEntry =  (tVlanStUcastEntry *) RBTreeGetNext
                     (gpVlanContextInfo->VlanStaticUnicastInfo,
                        (tRBElem *) & StUcastEntry, NULL);

    if (pStUcastEntry != NULL)
    {

       *pu4NextDot1qFdbId = pStUcastEntry->VlanId;

       VLAN_CPY_MAC_ADDR ((UINT1 *)
               *pNextDot1qStaticUnicastAddress,
               pStUcastEntry->MacAddr);

       *pi4NextDot1qStaticUnicastReceivePort =
           (INT4) pStUcastEntry->u2RcvPort;
       return SNMP_SUCCESS;
    }

        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qStaticMulticastTable
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qStaticMulticastTable (UINT4 *pu4Dot1qVlanIndex,
                                           tMacAddr *
                                           pDot1qStaticMulticastAddress,
                                           INT4
                                           *pi4Dot1qStaticMulticastReceivePort)
{
    tMacAddr            MacAddr;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexDot1qStaticMulticastTable (0,
                                                      pu4Dot1qVlanIndex,
                                                      MacAddr,
                                                      pDot1qStaticMulticastAddress,
                                                      0,
                                                      pi4Dot1qStaticMulticastReceivePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qStaticMulticastTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
                Dot1qStaticMulticastAddress
                nextDot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort
                nextDot1qStaticMulticastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qStaticMulticastTable (UINT4 u4Dot1qVlanIndex,
                                          UINT4 *pu4NextDot1qVlanIndex,
                                          tMacAddr
                                          Dot1qStaticMulticastAddress,
                                          tMacAddr *
                                          pNextDot1qStaticMulticastAddress,
                                          INT4
                                          i4Dot1qStaticMulticastReceivePort,
                                          INT4
                                          *pi4NextDot1qStaticMulticastReceivePort)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanId             VlanId;
    tVlanId             VlanStartId;
    UINT1               u1EntryFound = VLAN_FALSE;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qVlanIndex > VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }

    VlanStartId = (tVlanId) u4Dot1qVlanIndex;

    if ((VlanStartId == 0) || (VlanGetVlanEntry (VlanStartId) == NULL))
    {
        VlanStartId = VLAN_START_VLAN_INDEX ();
    }

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }
        if (u4Dot1qVlanIndex == pCurrEntry->VlanId)
        {

            pStMcastEntry =
                VlanGetNextStMcastEntry (pCurrEntry->pStMcastTable,
                                         Dot1qStaticMulticastAddress,
                                         (UINT2)
                                         (i4Dot1qStaticMulticastReceivePort));

            if (pStMcastEntry != NULL)
            {

                *pu4NextDot1qVlanIndex = pCurrEntry->VlanId;
                VLAN_CPY_MAC_ADDR ((UINT1 *)
                                   *pNextDot1qStaticMulticastAddress,
                                   pStMcastEntry->MacAddr);

                *pi4NextDot1qStaticMulticastReceivePort =
                    (INT4) pStMcastEntry->u2RcvPort;

                return SNMP_SUCCESS;
            }
        }
        else
        {

            if (u4Dot1qVlanIndex < pCurrEntry->VlanId)
            {

                if (u1EntryFound == VLAN_FALSE)
                {

                    pStMcastEntry
                        = VlanGetFirstStMcastEntry (pCurrEntry->pStMcastTable);

                    if (pStMcastEntry != NULL)
                    {

                        *pu4NextDot1qVlanIndex = pCurrEntry->VlanId;
                        VLAN_CPY_MAC_ADDR ((UINT1 *)
                                           *pNextDot1qStaticMulticastAddress,
                                           pStMcastEntry->MacAddr);

                        *pi4NextDot1qStaticMulticastReceivePort =
                            (INT4) pStMcastEntry->u2RcvPort;

                        u1EntryFound = VLAN_TRUE;
                    }
                }
                else
                {

                    if (*pu4NextDot1qVlanIndex > pCurrEntry->VlanId)
                    {

                        pStMcastEntry
                            =
                            VlanGetFirstStMcastEntry
                            (pCurrEntry->pStMcastTable);

                        if (pStMcastEntry != NULL)
                        {

                            *pu4NextDot1qVlanIndex = pCurrEntry->VlanId;
                            VLAN_CPY_MAC_ADDR
                                ((UINT1 *)
                                 *pNextDot1qStaticMulticastAddress,
                                 pStMcastEntry->MacAddr);

                            *pi4NextDot1qStaticMulticastReceivePort =
                                (INT4) pStMcastEntry->u2RcvPort;
                        }
                    }
                }
            }
        }
    }

    if (u1EntryFound == VLAN_FALSE)
    {

        return SNMP_FAILURE;
    }
    else
    {

        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qVlanCurrentTable
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qVlanCurrentTable (UINT4 *pu4Dot1qVlanTimeMark,
                                       UINT4 *pu4Dot1qVlanIndex)
{

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qVlanCurrentTable (0, pu4Dot1qVlanTimeMark,
                                                  0, pu4Dot1qVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qVlanCurrentTable
 Input       :  The Indices
                Dot1qVlanTimeMark
                nextDot1qVlanTimeMark
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qVlanCurrentTable (UINT4 u4Dot1qVlanTimeMark,
                                      UINT4 *pu4NextDot1qVlanTimeMark,
                                      UINT4 u4Dot1qVlanIndex,
                                      UINT4 *pu4NextDot1qVlanIndex)
{
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanId             VlanId = 0;
    tVlanId             VlanStartId = 0;
    UINT1               u1EntryFound = VLAN_FALSE;
    UINT4               u4NextTimeMark = 0;
    UINT4               u4NextVlanIndex = 0;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qVlanIndex > VLAN_MAX_VLAN_ID)
    {
        if (u4Dot1qVlanIndex == 0xFFFFFFFF)
        {
            u4Dot1qVlanIndex = 0;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }
        if (u1EntryFound == VLAN_FALSE)
        {
            /* Get any entry greater than received entry */
            if ((u4Dot1qVlanTimeMark < pCurrEntry->u4TimeStamp) ||
                ((u4Dot1qVlanTimeMark == pCurrEntry->u4TimeStamp) &&
                 (u4Dot1qVlanIndex < pCurrEntry->VlanId)))
            {
                u4NextVlanIndex = pCurrEntry->VlanId;
                u4NextTimeMark = pCurrEntry->u4TimeStamp;
                u1EntryFound = VLAN_TRUE;
            }
        }
        else
        {
            /* IF Received TimeStamp == Current TimeStamp,
             * Get next vlan id 
             * ELSE get next vlanentry with new time stamp  
             */

            if (((u4Dot1qVlanTimeMark == pCurrEntry->u4TimeStamp) &&
                 (u4Dot1qVlanIndex < pCurrEntry->VlanId)) ||
                (u4Dot1qVlanTimeMark < pCurrEntry->u4TimeStamp))
            {
                if (((pCurrEntry->u4TimeStamp == u4NextTimeMark) &&
                     (pCurrEntry->VlanId < u4NextVlanIndex)) ||
                    (pCurrEntry->u4TimeStamp < u4NextTimeMark))
                {
                    u4NextTimeMark = pCurrEntry->u4TimeStamp;
                    u4NextVlanIndex = pCurrEntry->VlanId;
                }
            }
        }
    }

    /* No greater entry found, return failure */
    if (u1EntryFound == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    *pu4NextDot1qVlanIndex = u4NextVlanIndex;
    *pu4NextDot1qVlanTimeMark = u4NextTimeMark;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qVlanStaticTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qVlanStaticTable (UINT4 *pu4Dot1qVlanIndex)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qVlanStaticTable (0, pu4Dot1qVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qVlanStaticTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qVlanStaticTable (UINT4 u4Dot1qVlanIndex,
                                     UINT4 *pu4NextDot1qVlanIndex)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1               u1EntryFound = VLAN_FALSE;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;

    while (pStVlanEntry != NULL)
    {

        if (u4Dot1qVlanIndex < pStVlanEntry->VlanId)
        {

            if (u1EntryFound == VLAN_FALSE)
            {

                *pu4NextDot1qVlanIndex = pStVlanEntry->VlanId;
                u1EntryFound = VLAN_TRUE;
            }
            else
            {

                if (*pu4NextDot1qVlanIndex > pStVlanEntry->VlanId)
                {

                    *pu4NextDot1qVlanIndex = pStVlanEntry->VlanId;
                }
            }
        }

        pStVlanEntry = pStVlanEntry->pNextNode;
    }

    if (u1EntryFound == VLAN_FALSE)
    {

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qPortVlanTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qPortVlanTable (INT4 *pi4Dot1dBasePort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qPortVlanTable (0, pi4Dot1dBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qPortVlanTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qPortVlanTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pPortEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4Dot1dBasePort + 1);
         u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry != NULL)
        {
            *pi4NextDot1dBasePort = (INT4) u2Port;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qPortVlanStatisticsTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qPortVlanStatisticsTable (INT4 *pi4Dot1dBasePort,
                                              UINT4 *pu4Dot1qVlanIndex)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qPortVlanStatisticsTable (0, pi4Dot1dBasePort,
                                                         0, pu4Dot1qVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qPortVlanStatisticsTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qPortVlanStatisticsTable (INT4 i4Dot1dBasePort,
                                             INT4 *pi4NextDot1dBasePort,
                                             UINT4 u4Dot1qVlanIndex,
                                             UINT4 *pu4NextDot1qVlanIndex)
{
    tVlanCurrEntry     *pCurrEntry;
    UINT2               u2Port;
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0 || i4Dot1dBasePort > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort == 0)
    {
        i4Dot1dBasePort = 1;
    }

    if (u4Dot1qVlanIndex > VLAN_MAX_VLAN_ID)
    {
        u4Dot1qVlanIndex = 0;
    }

    for (u2Port = (UINT2) i4Dot1dBasePort; u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry != NULL)
        {
            pCurrEntry = VlanGetNextCurrVlanEntry (u4Dot1qVlanIndex, u2Port);

            if (pCurrEntry != NULL)
            {
                *pi4NextDot1dBasePort = (INT4) u2Port;
                *pu4NextDot1qVlanIndex = (UINT4) pCurrEntry->VlanId;

                return SNMP_SUCCESS;
            }
        }

        u4Dot1qVlanIndex = 0;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qPortVlanHCStatisticsTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qPortVlanHCStatisticsTable (INT4 *pi4Dot1dBasePort,
                                                UINT4 *pu4Dot1qVlanIndex)
{

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qPortVlanHCStatisticsTable (0, pi4Dot1dBasePort,
                                                           0,
                                                           pu4Dot1qVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qPortVlanHCStatisticsTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qPortVlanHCStatisticsTable (INT4 i4Dot1dBasePort,
                                               INT4 *pi4NextDot1dBasePort,
                                               UINT4 u4Dot1qVlanIndex,
                                               UINT4 *pu4NextDot1qVlanIndex)
{
    tVlanCurrEntry     *pCurrEntry;
    UINT2               u2Port;
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0 || i4Dot1dBasePort > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort == 0)
    {
        i4Dot1dBasePort = 1;
    }

    if (u4Dot1qVlanIndex > VLAN_MAX_VLAN_ID)
    {
        u4Dot1qVlanIndex = 0;
    }

    for (u2Port = (UINT2) i4Dot1dBasePort; u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if ((pPortEntry != NULL) && (pPortEntry->u1IsHCPort == VLAN_TRUE))
        {
            pCurrEntry = VlanGetNextCurrVlanEntry (u4Dot1qVlanIndex, u2Port);

            if (pCurrEntry != NULL)
            {
                *pi4NextDot1dBasePort = (INT4) u2Port;
                *pu4NextDot1qVlanIndex = (UINT4) pCurrEntry->VlanId;

                return SNMP_SUCCESS;
            }
        }

        u4Dot1qVlanIndex = 0;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qLearningConstraintsTable
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qLearningConstraintsTable (UINT4 *pu4Dot1qConstraintVlan,
                                               INT4 *pi4Dot1qConstraintSet)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qLearningConstraintsTable (0,
                                                          pu4Dot1qConstraintVlan,
                                                          -1,
                                                          pi4Dot1qConstraintSet));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qLearningConstraintsTable
 Input       :  The Indices
                Dot1qConstraintVlan
                nextDot1qConstraintVlan
                Dot1qConstraintSet
                nextDot1qConstraintSet
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qLearningConstraintsTable (UINT4 u4Dot1qConstraintVlan,
                                              UINT4 *pu4NextDot1qConstraintVlan,
                                              INT4 i4Dot1qConstraintSet,
                                              INT4 *pi4NextDot1qConstraintSet)
{
    UINT1               u1VlanSort;
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    /* Vlan is the first index in the dot1qLearningConstraints table.
     * The other index is the constraint set.We sort by the first index
     * (VLAN) when querying the table through SNMP */

    u1VlanSort = VLAN_TRUE;

    if (u4Dot1qConstraintVlan > VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }
    return (VlanGetNextLearningConstraintsTable (u4Dot1qConstraintVlan,
                                                 pu4NextDot1qConstraintVlan,
                                                 i4Dot1qConstraintSet,
                                                 pi4NextDot1qConstraintSet,
                                                 u1VlanSort));
}

/*********************** P-MIB Get First Get Next Routines *****************/

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortCapabilitiesTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortCapabilitiesTable (INT4 *pi4Dot1dBasePort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1dPortCapabilitiesTable (0, pi4Dot1dBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortCapabilitiesTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortCapabilitiesTable (INT4 i4Dot1dBasePort,
                                           INT4 *pi4NextDot1dBasePort)
{
    tVlanPortEntry     *pPortEntry;
    UINT4               u4Index;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = i4Dot1dBasePort + 1; u4Index <= VLAN_MAX_PORTS; u4Index++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u4Index);

        if (pPortEntry != NULL)
        {
            *pi4NextDot1dBasePort = u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortPriorityTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortPriorityTable (INT4 *pi4Dot1dBasePort)
{
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Result = nmhGetFirstIndexDot1dPortCapabilitiesTable (pi4Dot1dBasePort);

    return i1Result;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortPriorityTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortPriorityTable (INT4 i4Dot1dBasePort,
                                       INT4 *pi4NextDot1dBasePort)
{
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    i1Result = nmhGetNextIndexDot1dPortCapabilitiesTable (i4Dot1dBasePort,
                                                          pi4NextDot1dBasePort);

    return i1Result;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dUserPriorityRegenTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dUserPriorityRegenTable (INT4 *pi4Dot1dBasePort,
                                             INT4 *pi4Dot1dUserPriority)
{
#ifdef NPAPI_WANTED
    /* BCM5690 doesn't support Regen Priority.
     * If it is supported by the H/W, the line below should be removed
     */

    UNUSED_PARAM (pi4Dot1dBasePort);
    UNUSED_PARAM (pi4Dot1dUserPriority);

    return SNMP_FAILURE;
#else
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Result = nmhGetFirstIndexDot1dTrafficClassTable (pi4Dot1dBasePort,
                                                       pi4Dot1dUserPriority);

    return i1Result;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dUserPriorityRegenTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
                Dot1dUserPriority
                nextDot1dUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dUserPriorityRegenTable (INT4 i4Dot1dBasePort,
                                            INT4 *pi4NextDot1dBasePort,
                                            INT4 i4Dot1dUserPriority,
                                            INT4 *pi4NextDot1dUserPriority)
{
#ifdef NPAPI_WANTED
    /* BCM5690 doesn't support Regen Priority.
     * If it is supported by the H/W, the line below should be removed
     */

    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4NextDot1dBasePort);
    UNUSED_PARAM (i4Dot1dUserPriority);
    UNUSED_PARAM (pi4NextDot1dUserPriority);

    return SNMP_FAILURE;
#else
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    i1Result = nmhGetNextIndexDot1dTrafficClassTable (i4Dot1dBasePort,
                                                      pi4NextDot1dBasePort,
                                                      i4Dot1dUserPriority,
                                                      pi4NextDot1dUserPriority);

    return i1Result;
#endif /* !NPAPI_WANTED */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTrafficClassTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dTrafficClassPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dTrafficClassTable (INT4 *pi4Dot1dBasePort,
                                        INT4 *pi4Dot1dTrafficClassPriority)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1dTrafficClassTable (0,
                                                   pi4Dot1dBasePort,
                                                   0,
                                                   pi4Dot1dTrafficClassPriority));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTrafficClassTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
                Dot1dTrafficClassPriority
                nextDot1dTrafficClassPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dTrafficClassTable (INT4 i4Dot1dBasePort,
                                       INT4 *pi4NextDot1dBasePort,
                                       INT4 i4Dot1dTrafficClassPriority,
                                       INT4 *pi4NextDot1dTrafficClassPriority)
{
    tVlanPortEntry     *pPortEntry;
    UINT4               u4Index;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry != NULL)
    {
        if (i4Dot1dTrafficClassPriority < VLAN_MAX_PRIORITY - 1)
        {
            *pi4NextDot1dBasePort = i4Dot1dBasePort;

            if (i4Dot1dTrafficClassPriority < 0)
            {
                *pi4NextDot1dTrafficClassPriority = 0;
            }
            else
            {
                *pi4NextDot1dTrafficClassPriority =
                    i4Dot1dTrafficClassPriority + 1;
            }

            return SNMP_SUCCESS;;
        }
    }

    for (u4Index = i4Dot1dBasePort + 1; u4Index <= VLAN_MAX_PORTS; u4Index++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u4Index);

        if (pPortEntry != NULL)
        {
            *pi4NextDot1dBasePort = u4Index;
            *pi4NextDot1dTrafficClassPriority = 0;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortOutboundAccessPriorityTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dPortOutboundRegenPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortOutboundAccessPriorityTable (INT4 *pi4Dot1dBasePort,
                                                      INT4
                                                      *pi4Dot1dPortOutboundRegenPriority)
{
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    i1Result
        = nmhGetFirstIndexDot1dTrafficClassTable (pi4Dot1dBasePort,
                                                  pi4Dot1dPortOutboundRegenPriority);

    return i1Result;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortOutboundAccessPriorityTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
                Dot1dPortOutboundRegenPriority
                nextDot1dPortOutboundRegenPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortOutboundAccessPriorityTable (INT4 i4Dot1dBasePort,
                                                     INT4 *pi4NextDot1dBasePort,
                                                     INT4
                                                     i4Dot1dPortOutboundRegenPriority,
                                                     INT4
                                                     *pi4NextDot1dPortOutboundRegenPriority)
{
    INT1                i1Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    i1Result
        = nmhGetNextIndexDot1dTrafficClassTable (i4Dot1dBasePort,
                                                 pi4NextDot1dBasePort,
                                                 i4Dot1dPortOutboundRegenPriority,
                                                 pi4NextDot1dPortOutboundRegenPriority);

    return i1Result;
}

/**************** Proprietary MIB Get First, Get Next Routines **************/

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanPortTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qFutureVlanPortTable (INT4 *pi4Dot1qFutureVlanPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qFutureVlanPortTable
            (0, pi4Dot1qFutureVlanPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanPortTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                nextDot1qFutureVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qFutureVlanPortTable (INT4 i4Dot1qFutureVlanPort,
                                         INT4 *pi4NextDot1qFutureVlanPort)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT4               u4Index;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1qFutureVlanPort < 0 || i4Dot1qFutureVlanPort > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = i4Dot1qFutureVlanPort + 1;
         u4Index <= VLAN_MAX_PORTS; u4Index++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u4Index);

        if (pPortEntry != NULL)
        {
            *pi4NextDot1qFutureVlanPort = u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1qFutureVlanPortMacMapTable (INT4 *pi4Dot1qFutureVlanPort,
                                                tMacAddr *
                                                pDot1qFutureVlanPortMacMapAddr)
{
    tMacAddr            MacAddr;
    INT4                i4PortNo;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    i4PortNo = 0;

    return (nmhGetNextIndexDot1qFutureVlanPortMacMapTable (i4PortNo,
                                                           pi4Dot1qFutureVlanPort,
                                                           MacAddr,
                                                           pDot1qFutureVlanPortMacMapAddr));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                nextDot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr
                nextDot1qFutureVlanPortMacMapAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1qFutureVlanPortMacMapTable (INT4 i4Dot1qFutureVlanPort,
                                               INT4 *pi4NextDot1qFutureVlanPort,
                                               tMacAddr
                                               Dot1qFutureVlanPortMacMapAddr,
                                               tMacAddr *
                                               pNextDot1qFutureVlanPortMacMapAddr)
{
    tMacMapIndex        CurrMacMapIndex;
    tMacMapIndex        NextMacMapIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    CurrMacMapIndex.u4Port = i4Dot1qFutureVlanPort;
    VLAN_CPY_MAC_ADDR (CurrMacMapIndex.MacAddr, Dot1qFutureVlanPortMacMapAddr);

    if (VlanMacMapTableGetNextEntry (&CurrMacMapIndex, &NextMacMapIndex)
        == SNMP_SUCCESS)
    {
        *pi4NextDot1qFutureVlanPort = NextMacMapIndex.u4Port;
        VLAN_CPY_MAC_ADDR (pNextDot1qFutureVlanPortMacMapAddr,
                           NextMacMapIndex.MacAddr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFdbTable
 Input       :  The Indices
                Dot1qFdbId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qFdbTable (UINT4 u4Dot1qFdbId)
{
    UINT4               u4FidIndex;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4Dot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qTpFdbTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qTpFdbTable (UINT4 u4Dot1qFdbId,
                                         tMacAddr Dot1qTpFdbAddress)
{
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    tVlanFdbInfo       *pVlanFdbInfo;
#else
    tHwUnicastMacEntry  HwUcastMacEntry;
#endif
#else
    tVlanFdbEntry      *pFdbEntry = NULL;

    UINT4               u4FidIndex;

#endif

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    pVlanFdbInfo = VlanGetFdbEntry (u4Dot1qFdbId, Dot1qTpFdbAddress);

    if (pVlanFdbInfo == NULL)
    {
        return SNMP_FAILURE;
    }
#else
    if (VlanHwGetFdbEntry
        (VLAN_CURR_CONTEXT_ID (), u4Dot1qFdbId, Dot1qTpFdbAddress,
         &HwUcastMacEntry) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
#else
    u4FidIndex = VlanGetFidEntryFromFdbId (u4Dot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        return SNMP_FAILURE;
    }

    pFdbEntry = VlanGetUcastEntry (u4FidIndex, Dot1qTpFdbAddress);

    if (pFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qTpGroupTable
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qTpGroupAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qTpGroupTable (UINT4 u4Dot1qVlanIndex,
                                           tMacAddr Dot1qTpGroupAddress)
{
    tVlanId             VlanId;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    VlanId = (tVlanId) u4Dot1qVlanIndex;

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pGroupEntry = VlanGetGroupEntry (Dot1qTpGroupAddress, pVlanEntry);

    if (pGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qForwardAllTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qForwardAllTable (UINT4 u4Dot1qVlanIndex)
{
#ifdef GARP_EXTENDED_FILTER
    tVlanId             VlanId;
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    VlanId = (tVlanId) u4Dot1qVlanIndex;

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4Dot1qVlanIndex);
    return VLAN_FAILURE;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qForwardUnregisteredTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qForwardUnregisteredTable (UINT4 u4Dot1qVlanIndex)
{
#ifdef GARP_EXTENDED_FILTER
    tVlanId             VlanId;
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    VlanId = (tVlanId) u4Dot1qVlanIndex;

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4Dot1qVlanIndex);

    return SNMP_SUCCESS;
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qStaticUnicastTable
 Input       :  The Indices
                Dot1qFdbId
                StaticAddress
                ReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qStaticUnicastTable (UINT4 u4Dot1qFdbId,
                                                 tMacAddr StaticAddress,
                                                 INT4 i4ReceivePort)
{
    UINT4               u4FidIndex;
    tVlanStUcastEntry  *pStUcastEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4Dot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        return SNMP_FAILURE;
    }

    pStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex,
                                                             StaticAddress,
                                                             (UINT2)
                                                             i4ReceivePort);

    if (pStUcastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qStaticMulticastTable
 Input       :  The Indices
                Dot1qVlanIndex
                Dot1qStaticMulticastAddress
                Dot1qStaticMulticastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qStaticMulticastTable (UINT4 u4Dot1qVlanIndex,
                                                   tMacAddr
                                                   Dot1qStaticMulticastAddress,
                                                   INT4
                                                   i4Dot1qStaticMulticastReceivePort)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qVlanIndex);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pStMcastEntry =
        VlanGetStMcastEntryWithExactRcvPort (Dot1qStaticMulticastAddress,
                                             (UINT2)
                                             i4Dot1qStaticMulticastReceivePort,
                                             pVlanEntry);

    if (pStMcastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qVlanCurrentTable
 Input       :  The Indices
                Dot1qVlanTimeMark
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qVlanCurrentTable (UINT4 u4Dot1qVlanTimeMark,
                                               UINT4 u4Dot1qVlanIndex)
{
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4Dot1qVlanTimeMark);

    pVlanEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qVlanIndex);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qVlanStaticTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qVlanStaticTable (UINT4 u4Dot1qVlanIndex)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4Dot1qVlanIndex);

    if (pStVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qPortVlanTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qPortVlanTable (INT4 i4Dot1dBasePort)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1dBasePort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dBasePort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qPortVlanStatisticsTable (INT4 i4Dot1dBasePort,
                                                      UINT4 u4Dot1qVlanIndex)
{
    INT1                i1RetVal;
    tVlanCurrEntry     *pVlanEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qPortVlanTable (i4Dot1dBasePort);

    if (i1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pVlanEntry = VlanGetVlanEntry ((tVlanId) u4Dot1qVlanIndex);

    if (pVlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable (INT4 i4Dot1dBasePort,
                                                        UINT4 u4Dot1qVlanIndex)
{
    INT1                i1RetVal;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1qPortVlanStatisticsTable (i4Dot1dBasePort,
                                                              u4Dot1qVlanIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qLearningConstraintsTable
 Input       :  The Indices
                Dot1qConstraintVlan
                Dot1qConstraintSet
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qLearningConstraintsTable (UINT4
                                                       u4Dot1qConstraintVlan,
                                                       INT4
                                                       i4Dot1qConstraintSet)
{
    tVlanConstraintEntry *pConstEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pConstEntry = VlanGetConstEntry ((tVlanId) u4Dot1qConstraintVlan,
                                     (UINT2) i4Dot1qConstraintSet);

    if (pConstEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/**************************** P-MIB Test Routines ****************************/

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortCapabilitiesTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dPortCapabilitiesTable (INT4 i4Dot1dBasePort)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortPriorityTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dPortPriorityTable (INT4 i4Dot1dBasePort)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dUserPriorityRegenTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dUserPriorityRegenTable (INT4 i4Dot1dBasePort,
                                                     INT4 i4Dot1dUserPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1dUserPriority < 0)
        || (i4Dot1dUserPriority > VLAN_HIGHEST_PRIORITY))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTrafficClassTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dTrafficClassPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dTrafficClassTable (INT4 i4Dot1dBasePort,
                                                INT4
                                                i4Dot1dTrafficClassPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1dTrafficClassPriority < 0) ||
        (i4Dot1dTrafficClassPriority > VLAN_HIGHEST_PRIORITY))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortOutboundAccessPriorityTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1dPortOutboundRegenPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dPortOutboundAccessPriorityTable (INT4
                                                              i4Dot1dBasePort,
                                                              INT4
                                                              i4Dot1dPortOutboundRegenPriority)
{
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4Dot1dBasePort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1dPortOutboundRegenPriority < 0) ||
        (i4Dot1dPortOutboundRegenPriority > VLAN_HIGHEST_PRIORITY))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortMacMapAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable (INT4
                                                        i4Dot1qFutureVlanPort,
                                                        tMacAddr
                                                        Dot1qFutureVlanPortMacMapAddr)
{
    tVlanMacMapEntry   *pMacMapEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pMacMapEntry = VlanGetMacMapEntry (Dot1qFutureVlanPortMacMapAddr,
                                       i4Dot1qFutureVlanPort);

    if (pMacMapEntry != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhValidateIndexInstanceDot1qFutureVlanPortTable
 *  Input       :  The Indices
 *                 Dot1dBasePort
 *  Output      :  The Routines Validates the Given Indices.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhValidateIndexInstanceDot1qFutureVlanPortTable (INT4 i4Dot1dBasePort)
{
    INT1                i1RetVal;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qPortVlanTable (i4Dot1dBasePort);

    return i1RetVal;
}

/****************************************************************************
 Function    :  VlanGetNextGroupEntry ()        
 Input       :  pGroupTable - Pointer to the Group address table.
             :  pMacAddr - Mac Address 
 Output      :  None 
 Returns     :  The Group Mac addr entry with the next least Mac address
****************************************************************************/
tVlanGroupEntry    *
VlanGetNextGroupEntry (tVlanGroupEntry * pGroupTable, tMacAddr MacAddr)
{
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanGroupEntry    *pNextGroupEntry = NULL;
    UINT1               u1MacFound = VLAN_FALSE;
    INT4                i4Result;

    pGroupEntry = pGroupTable;

    while (pGroupEntry != NULL)
    {
        i4Result = VlanCmpMacAddr (MacAddr, pGroupEntry->MacAddr);

        if (VLAN_LESSER == i4Result)
        {
            if (VLAN_FALSE == u1MacFound)
            {
                pNextGroupEntry = pGroupEntry;
                u1MacFound = VLAN_TRUE;
            }
            else
            {
                i4Result = VlanCmpMacAddr (pNextGroupEntry->MacAddr,
                                           pGroupEntry->MacAddr);

                if (i4Result == VLAN_GREATER)
                {
                    pNextGroupEntry = pGroupEntry;
                }
            }
        }

        pGroupEntry = pGroupEntry->pNextNode;
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pNextGroupEntry;
    }
    else
    {
        return NULL;
    }
}

/***************************************************************************
 Function    :  VlanGetFirstStUcastEntry ()        
 Input       :  pStUcastTable - Pointer to the Static Unicast Mac addr Table.
 Output      :  None 
 Returns     :  The Static Unicast mac entry with the least Mac address
****************************************************************************/
tVlanStUcastEntry  *
VlanGetFirstStUcastEntry (tVlanStUcastEntry * pStUcastTable)
{
    tVlanStUcastEntry  *pStUcastEntry;
    tVlanStUcastEntry  *pFirstStUcastEntry = NULL;
    INT4                i4Result;
    UINT1               u1MacFound = VLAN_FALSE;

    pStUcastEntry = pStUcastTable;

    while (pStUcastEntry != NULL)
    {
        if (u1MacFound == VLAN_FALSE)
        {
            pFirstStUcastEntry = pStUcastEntry;
            u1MacFound = VLAN_TRUE;
        }
        else
        {
            i4Result = VlanCmpMacAddr (pFirstStUcastEntry->MacAddr,
                                       pStUcastEntry->MacAddr);

            if ((i4Result == VLAN_GREATER)
                || ((i4Result == VLAN_EQUAL)
                    && (pFirstStUcastEntry->u2RcvPort >
                        pStUcastEntry->u2RcvPort)))
            {
                pFirstStUcastEntry = pStUcastEntry;
            }
        }

        pStUcastEntry = pStUcastEntry->pNextNode;
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pFirstStUcastEntry;
    }

    return NULL;
}

/****************************************************************************
 Function    :  VlanGetNextStUcastEntry ()        
 Input       :  pStUcastEntry - Pointer to the Static Unicast address table.
             :  pMacAddr - Mac Address 
             :  u2RcvPort - The receive Port
 Output      :  None 
 Returns     :  The Static Mac addr entry with the next least Mac address
****************************************************************************/
tVlanStUcastEntry  *
VlanGetNextStUcastEntry (tVlanStUcastEntry * pStUcastTable,
                         tMacAddr MacAddr, UINT2 u2RcvPort)
{
    tVlanStUcastEntry  *pStUcastEntry;
    tVlanStUcastEntry  *pNextStUcastEntry = NULL;
    UINT1               u1MacFound = VLAN_FALSE;
    INT4                i4Result;

    pStUcastEntry = pStUcastTable;

    while (pStUcastEntry != NULL)
    {
        i4Result = VlanCmpMacAddr (MacAddr, pStUcastEntry->MacAddr);

        if ((VLAN_LESSER == i4Result)
            || ((VLAN_EQUAL == i4Result)
                && (u2RcvPort < pStUcastEntry->u2RcvPort)))
        {
            if (VLAN_FALSE == u1MacFound)
            {
                pNextStUcastEntry = pStUcastEntry;
                u1MacFound = VLAN_TRUE;
            }
            else
            {
                i4Result = VlanCmpMacAddr (pNextStUcastEntry->MacAddr,
                                           pStUcastEntry->MacAddr);

                if ((VLAN_GREATER == i4Result)
                    || ((VLAN_EQUAL == i4Result)
                        && (pNextStUcastEntry->u2RcvPort >
                            pStUcastEntry->u2RcvPort)))
                {
                    pNextStUcastEntry = pStUcastEntry;
                }
            }
        }

        pStUcastEntry = pStUcastEntry->pNextNode;
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pNextStUcastEntry;
    }
    else
    {
        return NULL;
    }
}

/***************************************************************************
 Function    :  VlanGetFirstStMcastEntry ()        
 Input       :  pStMcastTable - Pointer to the Static Multicast Table.
 Output      :  None 
 Returns     :  The Static Multicast mac entry with the least Mac address
****************************************************************************/
tVlanStMcastEntry  *
VlanGetFirstStMcastEntry (tVlanStMcastEntry * pStMcastTable)
{
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanStMcastEntry  *pFirstStMcastEntry = NULL;
    INT4                i4Result;
    UINT1               u1MacFound = VLAN_FALSE;

    pStMcastEntry = pStMcastTable;

    while (pStMcastEntry != NULL)
    {
        if (u1MacFound == VLAN_FALSE)
        {
            pFirstStMcastEntry = pStMcastEntry;
            u1MacFound = VLAN_TRUE;
        }
        else
        {
            i4Result = VlanCmpMacAddr (pFirstStMcastEntry->MacAddr,
                                       pStMcastEntry->MacAddr);

            if ((i4Result == VLAN_GREATER)
                || ((i4Result == VLAN_EQUAL)
                    && (pFirstStMcastEntry->u2RcvPort >
                        pStMcastEntry->u2RcvPort)))
            {
                pFirstStMcastEntry = pStMcastEntry;
            }
        }

        pStMcastEntry = pStMcastEntry->pNextNode;
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pFirstStMcastEntry;
    }
    else
    {
        return NULL;
    }
}

/****************************************************************************
 Function    :  VlanGetNextStMcastEntry ()        
 Input       :  pStMcastEntry - Pointer to the Static Multicast table.
             :  pMacAddr - Mac Address 
                u2RcvPort - The receive Port
 Output      :  None 
 Returns     :  The Static Multicast entry with the next least Mac address
****************************************************************************/
tVlanStMcastEntry  *
VlanGetNextStMcastEntry (tVlanStMcastEntry * pStMcastTable,
                         tMacAddr MacAddr, UINT2 u2RcvPort)
{
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanStMcastEntry  *pNextStMcastEntry = NULL;
    UINT1               u1MacFound = VLAN_FALSE;
    INT4                i4Result;

    pStMcastEntry = pStMcastTable;

    while (pStMcastEntry != NULL)
    {
        i4Result = VlanCmpMacAddr (MacAddr, pStMcastEntry->MacAddr);

        if ((VLAN_LESSER == i4Result)
            || ((VLAN_EQUAL == i4Result)
                && (u2RcvPort < pStMcastEntry->u2RcvPort)))
        {
            if (VLAN_FALSE == u1MacFound)
            {
                pNextStMcastEntry = pStMcastEntry;
                u1MacFound = VLAN_TRUE;
            }
            else
            {
                i4Result = VlanCmpMacAddr (pNextStMcastEntry->MacAddr,
                                           pStMcastEntry->MacAddr);

                if ((VLAN_GREATER == i4Result)
                    || ((VLAN_EQUAL == i4Result)
                        && (pNextStMcastEntry->u2RcvPort >
                            pStMcastEntry->u2RcvPort)))
                {
                    pNextStMcastEntry = pStMcastEntry;
                }
            }
        }

        pStMcastEntry = pStMcastEntry->pNextNode;
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pNextStMcastEntry;
    }
    else
    {
        return NULL;
    }
}

/****************************************************************************
 Function    :  VlanGetNextCurrVlanEntry ()        
 Input       :  u4Dot1qVlanIndex - The Vlan Id
                u2Port - Port index             
 Output      :  None 
 Returns     :  The Curr Vlan entry with the next least Vlan Id.
****************************************************************************/
tVlanCurrEntry     *
VlanGetNextCurrVlanEntry (UINT4 u4VlanIndex, UINT2 u2Port)
{
    tVlanCurrEntry     *pCurrEntry;
    UINT4               u4NextVlanIndex;
    INT1                i1RetVal;
    UINT1               u1RetVal = VLAN_FALSE;

    i1RetVal = VlanGetNextVlanId (u4VlanIndex, &u4NextVlanIndex);

    while (i1RetVal == SNMP_SUCCESS)
    {
        pCurrEntry = VlanGetVlanEntry ((tVlanId) u4NextVlanIndex);
        if (pCurrEntry == NULL)
        {
            break;
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2Port, u1RetVal);

        if (u1RetVal == VLAN_TRUE)
        {
            return pCurrEntry;
        }

        /* For Showing Forbidden Ports */
        if (pCurrEntry->pStVlanEntry != NULL)
        {
            VLAN_IS_FORBIDDEN_PORT (pCurrEntry->pStVlanEntry, u2Port, u1RetVal);
        }

        if (u1RetVal == VLAN_TRUE)
        {
            return pCurrEntry;
        }

        u4VlanIndex = u4NextVlanIndex;

        i1RetVal = VlanGetNextVlanId (u4VlanIndex, &u4NextVlanIndex);

    }
    return NULL;
}

/****************************************************************************
 Function    :  VlanGetNextVlanId ()        
 Input       :  u4VlanIndex - The Vlan Id
                pu4NextVlanIndex - Next Vlan Id             
 Output      :  None 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
VlanGetNextVlanId (UINT4 u4VlanIndex, UINT4 *pu4NextVlanIndex)
{
    tVlanId             VlanId;
    tVlanId             ScanVlanId;
    tVlanId             NextVlanId;
    tVlanCurrEntry     *pCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    VlanId = (tVlanId) u4VlanIndex;

    if (VlanId >= VLAN_INVALID_VLAN_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (VlanId == 0)
    {
        NextVlanId = VLAN_START_VLAN_INDEX ();

        /* Default vlan may not be present in the system.
         * So check whether the vlan entry present. */
        pCurrEntry = VlanGetVlanEntry (NextVlanId);

        if (pCurrEntry == NULL)
        {
            return SNMP_FAILURE;
        }

    }
    else
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry != NULL)
        {
            NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId);

            if (NextVlanId == VLAN_INVALID_VLAN_INDEX)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* 
             * Since this function may be called with invalid/deleted VLAN 
             * in any case scanning will be done from the beginning   
             */
            VLAN_SCAN_VLAN_TABLE (ScanVlanId)
            {
                if (ScanVlanId > VlanId)
                {
                    break;
                }
            }

            /* If the next VLAN doesnot exist then return SNMP_FAILURE */
            if (ScanVlanId == VLAN_INVALID_VLAN_INDEX)
            {
                return SNMP_FAILURE;
            }

            NextVlanId = ScanVlanId;
        }
    }

    *pu4NextVlanIndex = NextVlanId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  VlanGetFirstFdbEntry ()        
 Input       :  FdbHashTable - Fdb hash table.
 Output      :  None 
 Returns     :  The Fdb entry with the least Mac address
****************************************************************************/
tVlanFdbEntry      *
VlanGetFirstFdbEntry (tVlanFdbHashTable FdbHashTable)
{
    tVlanFdbEntry      *pFdbEntry;
    tVlanFdbEntry      *pFirstFdbEntry = NULL;
    UINT4               u4HashIndex;
    INT4                i4Result;
    UINT1               u1MacFound = VLAN_FALSE;

    for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
    {
        pFdbEntry = FdbHashTable[u4HashIndex];

        while (pFdbEntry != NULL)
        {
            if (u1MacFound == VLAN_FALSE)
            {
                pFirstFdbEntry = pFdbEntry;
                u1MacFound = VLAN_TRUE;
            }
            else
            {
                i4Result = VlanCmpMacAddr (pFirstFdbEntry->MacAddr,
                                           pFdbEntry->MacAddr);

                if (i4Result == VLAN_GREATER)
                {
                    pFirstFdbEntry = pFdbEntry;
                }
            }

            pFdbEntry = pFdbEntry->pNextHashNode;
        }
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pFirstFdbEntry;
    }
    else
    {
        return NULL;
    }
}

/****************************************************************************
 Function    :  VlanGetNextFdbEntry ()        
 Input       :  FdbHashTable - Fdb hash table.
             :  pMacAddr 
 Output      :  None 
 Returns     :  The Fdb entry with the next least Mac address
****************************************************************************/
tVlanFdbEntry      *
VlanGetNextFdbEntry (tVlanFdbHashTable FdbHashTable, tMacAddr MacAddr)
{
    tVlanFdbEntry      *pFdbEntry;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    UINT4               u4HashIndex;
    UINT1               u1MacFound = VLAN_FALSE;
    INT4                i4Result;

    for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
    {
        pFdbEntry = FdbHashTable[u4HashIndex];

        while (pFdbEntry != NULL)
        {
            i4Result = VlanCmpMacAddr (MacAddr, pFdbEntry->MacAddr);

            if (VLAN_LESSER == i4Result)
            {
                if (u1MacFound == VLAN_FALSE)
                {
                    pNextFdbEntry = pFdbEntry;
                    u1MacFound = VLAN_TRUE;
                }
                else
                {
                    i4Result = VlanCmpMacAddr (pNextFdbEntry->MacAddr,
                                               pFdbEntry->MacAddr);

                    if (i4Result == VLAN_GREATER)
                    {
                        pNextFdbEntry = pFdbEntry;
                    }
                }
            }

            pFdbEntry = pFdbEntry->pNextHashNode;
        }
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pNextFdbEntry;
    }
    else
    {
        return NULL;
    }
}

/****************************************************************************
 Function    :  VlanGetFirstGroupEntry ()        
 Input       :  pGroupTable - Pointer to the Group Mac address Table.
 Output      :  None 
 Returns     :  The Group entry with the least Mac address
****************************************************************************/
tVlanGroupEntry    *
VlanGetFirstGroupEntry (tVlanGroupEntry * pGroupTable)
{
    tVlanGroupEntry    *pGroupEntry;
    tVlanGroupEntry    *pFirstGroupEntry = NULL;
    INT4                i4Result;
    UINT1               u1MacFound = VLAN_FALSE;

    pGroupEntry = pGroupTable;

    while (pGroupEntry != NULL)
    {
        if (u1MacFound == VLAN_FALSE)
        {
            pFirstGroupEntry = pGroupEntry;
            u1MacFound = VLAN_TRUE;
        }
        else
        {
            i4Result = VlanCmpMacAddr (pFirstGroupEntry->MacAddr,
                                       pGroupEntry->MacAddr);

            if (i4Result == VLAN_GREATER)
            {
                pFirstGroupEntry = pGroupEntry;
            }
        }

        pGroupEntry = pGroupEntry->pNextNode;
    }

    if (u1MacFound == VLAN_TRUE)
    {
        return pFirstGroupEntry;
    }
    else
    {
        return NULL;
    }
}

/* LOW LEVEL Routines for Table : Dot1vProtocolGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1vProtocolGroupTable
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1vProtocolGroupTable (INT4
                                                 i4Dot1vProtocolTemplateFrameType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pDot1vProtocolTemplateProtocolValue)
{
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;
    UINT1               u1Length = 0;
    UINT1               au1Value[VLAN_MAX_PROTO_SIZE];

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_PORT_PROTO_SNAPOTHER == (UINT1) i4Dot1vProtocolTemplateFrameType)
    {
        /*  Check whether the PID value for SNAP_OTHER type 
         *  is 5 byte
         */
        if (VLAN_5BYTE_PROTO_TEMP_SIZE
            != (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /*  Check whether the PID value for other type 
         *  is 2 byte
         */
        if (VLAN_2BYTE_PROTO_TEMP_SIZE
            != (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length)
        {
            return SNMP_FAILURE;
        }
    }

    VLAN_MEMSET (au1Value, 0, VLAN_MAX_PROTO_SIZE);
    u1Length = (UINT1) pDot1vProtocolTemplateProtocolValue->i4_Length;
    VLAN_MEMCPY (au1Value,
                 pDot1vProtocolTemplateProtocolValue->pu1_OctetList, u1Length);

    pVlanProtGrpEntry =
        VlanGetProtGrpEntry ((UINT1) i4Dot1vProtocolTemplateFrameType,
                             u1Length, au1Value);

    if (pVlanProtGrpEntry != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1vProtocolGroupTable
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1vProtocolGroupTable (INT4
                                         *pi4Dot1vProtocolTemplateFrameType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pDot1vProtocolTemplateProtocolValue)
{
    tSNMP_OCTET_STRING_TYPE FirstOctetStringType;
    UINT1               au1Value[VLAN_5BYTE_PROTO_TEMP_SIZE];

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    FirstOctetStringType.i4_Length = VLAN_2BYTE_PROTO_TEMP_SIZE;

    FirstOctetStringType.pu1_OctetList = au1Value;
    MEMSET (FirstOctetStringType.pu1_OctetList, 0, VLAN_5BYTE_PROTO_TEMP_SIZE);

    return (nmhGetNextIndexDot1vProtocolGroupTable (0,
                                                    pi4Dot1vProtocolTemplateFrameType,
                                                    &FirstOctetStringType,
                                                    pDot1vProtocolTemplateProtocolValue));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1vProtocolGroupTable
 Input       :  The Indices
                Dot1vProtocolTemplateFrameType
                nextDot1vProtocolTemplateFrameType
                Dot1vProtocolTemplateProtocolValue
                nextDot1vProtocolTemplateProtocolValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1vProtocolGroupTable (INT4 i4Dot1vProtocolTemplateFrameType,
                                        INT4
                                        *pi4NextDot1vProtocolTemplateFrameType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pDot1vProtocolTemplateProtocolValue,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextDot1vProtocolTemplateProtocolValue)
{
    UINT4               u4HashIndex = 0;
    UINT1               u1IsEntryFound = VLAN_FALSE;
    UINT1               u1FrameType = 0xff;
    UINT1               au1ProtoValue[VLAN_MAX_PROTO_SIZE];
    UINT1               u1ProtoLength = VLAN_MAX_PROTO_SIZE;
    UINT1               u1FirstLevel = VLAN_FALSE;
    tVlanProtGrpEntry  *pVlanProtGrpEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1vProtocolTemplateFrameType < 0)
    {
        return SNMP_FAILURE;
    }

    if ((pDot1vProtocolTemplateProtocolValue->i4_Length < 0) ||
        (pDot1vProtocolTemplateProtocolValue->i4_Length >
         VLAN_5BYTE_PROTO_TEMP_SIZE))
    {
        i4Dot1vProtocolTemplateFrameType++;

        pDot1vProtocolTemplateProtocolValue->i4_Length =
            VLAN_2BYTE_PROTO_TEMP_SIZE;
        pDot1vProtocolTemplateProtocolValue->pu1_OctetList[0] = 0;
        pDot1vProtocolTemplateProtocolValue->pu1_OctetList[1] = 0;
    }

    VLAN_MEMSET (au1ProtoValue, 0xff, VLAN_MAX_PROTO_SIZE);

    for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
    {
        VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                               u4HashIndex, pVlanProtGrpEntry,
                               tVlanProtGrpEntry *)
        {
            /* if the scanned first index is equal to given first index */
            if (pVlanProtGrpEntry->VlanProtoTemplate.u1TemplateProtoFrameType ==
                i4Dot1vProtocolTemplateFrameType)
            {
                /* check if the scanned second index is greater than given second
                 * index and also if the scanned second index is greater than
                 * the temporary second index. if so, then update temporary 
                 * second index with scanned second index.
                 *
                 * NOTE: Temporary seond index is the local variable which is 
                 * inialised to big value and subsequently updated in each scanning,
                 * till it gets final better value.
                 */
                if ((pDot1vProtocolTemplateProtocolValue->i4_Length <
                     VLAN_2BYTE_PROTO_TEMP_SIZE) ||
                    (VLAN_MEMCMP
                     (pVlanProtGrpEntry->VlanProtoTemplate.au1ProtoValue,
                      pDot1vProtocolTemplateProtocolValue->pu1_OctetList,
                      pDot1vProtocolTemplateProtocolValue->i4_Length) > 0))
                {
                    if ((pVlanProtGrpEntry->
                         VlanProtoTemplate.u1TemplateProtoFrameType <
                         u1FrameType)
                        ||
                        (VLAN_MEMCMP
                         (au1ProtoValue,
                          pVlanProtGrpEntry->VlanProtoTemplate.au1ProtoValue,
                          MEM_MAX_BYTES (pVlanProtGrpEntry->VlanProtoTemplate.
                                         u1Length, VLAN_MAX_PROTO_SIZE)) > 0))
                    {
                        u1IsEntryFound = VLAN_TRUE;
                        u1FirstLevel = VLAN_TRUE;
                        u1FrameType =
                            pVlanProtGrpEntry->
                            VlanProtoTemplate.u1TemplateProtoFrameType;
                        VLAN_MEMSET (au1ProtoValue, 0, VLAN_MAX_PROTO_SIZE);
                        VLAN_MEMCPY (au1ProtoValue,
                                     pVlanProtGrpEntry->
                                     VlanProtoTemplate.au1ProtoValue,
                                     MEM_MAX_BYTES
                                     (pVlanProtGrpEntry->VlanProtoTemplate.
                                      u1Length, VLAN_MAX_PROTO_SIZE));
                        u1ProtoLength =
                            pVlanProtGrpEntry->VlanProtoTemplate.u1Length;
                    }
                }
            }

            /* if scanned first index is greater than given first index and also
             * scanned first index is equal to temporary first index
             *
             * NOTE: Temporary first index is the local variable which is 
             * initialised to big value and subsequently updated in each scanning
             * till it gets final better value.
             */

            else if ((u1FirstLevel == VLAN_FALSE) &&
                     ((pVlanProtGrpEntry->
                       VlanProtoTemplate.u1TemplateProtoFrameType >
                       i4Dot1vProtocolTemplateFrameType)
                      && (pVlanProtGrpEntry->
                          VlanProtoTemplate.u1TemplateProtoFrameType ==
                          u1FrameType)))
            {

                /* if temporary second index is greater than scanned second index
                 * then update the temporary second index with scanned second index
                 * NOTE: here we are NOT comparing scanned second index to be 
                 * greater than given second index, since scanned first index is 
                 * already bigger than given first index.
                 */

                if (VLAN_MEMCMP (au1ProtoValue,
                                 pVlanProtGrpEntry->
                                 VlanProtoTemplate.au1ProtoValue,
                                 MEM_MAX_BYTES
                                 (pVlanProtGrpEntry->VlanProtoTemplate.u1Length,
                                  VLAN_MAX_PROTO_SIZE)) > 0)
                {
                    u1IsEntryFound = VLAN_TRUE;
                    u1FrameType =
                        pVlanProtGrpEntry->
                        VlanProtoTemplate.u1TemplateProtoFrameType;
                    VLAN_MEMSET (au1ProtoValue, 0, VLAN_MAX_PROTO_SIZE);
                    VLAN_MEMCPY (au1ProtoValue,
                                 pVlanProtGrpEntry->
                                 VlanProtoTemplate.au1ProtoValue,
                                 MEM_MAX_BYTES
                                 (pVlanProtGrpEntry->VlanProtoTemplate.u1Length,
                                  VLAN_MAX_PROTO_SIZE));
                    u1ProtoLength =
                        pVlanProtGrpEntry->VlanProtoTemplate.u1Length;
                }
            }
            /* if scanned first index is greater than both given first index
             * and temporary first index 
             */
            else if ((u1FirstLevel == VLAN_FALSE) &&
                     ((pVlanProtGrpEntry->
                       VlanProtoTemplate.u1TemplateProtoFrameType >
                       i4Dot1vProtocolTemplateFrameType)
                      && (pVlanProtGrpEntry->
                          VlanProtoTemplate.u1TemplateProtoFrameType <
                          u1FrameType)))
            {
                /* here no need to do any checks for second index, since the 
                 * first index is exact best. so update temporary second index
                 * with the scanned second index 
                 */
                u1IsEntryFound = VLAN_TRUE;
                u1FrameType =
                    pVlanProtGrpEntry->
                    VlanProtoTemplate.u1TemplateProtoFrameType;
                VLAN_MEMSET (au1ProtoValue, 0, VLAN_MAX_PROTO_SIZE);
                VLAN_MEMCPY (au1ProtoValue,
                             pVlanProtGrpEntry->VlanProtoTemplate.au1ProtoValue,
                             pVlanProtGrpEntry->VlanProtoTemplate.u1Length);
                u1ProtoLength = pVlanProtGrpEntry->VlanProtoTemplate.u1Length;
            }
        }
    }

    if (u1IsEntryFound == VLAN_TRUE)
    {
        pNextDot1vProtocolTemplateProtocolValue->i4_Length = u1ProtoLength;
        VLAN_MEMCPY (pNextDot1vProtocolTemplateProtocolValue->pu1_OctetList,
                     au1ProtoValue, u1ProtoLength);
        *pi4NextDot1vProtocolTemplateFrameType = u1FrameType;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : Dot1vProtocolPortTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1vProtocolPortTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1vProtocolPortTable (INT4 i4Dot1dBasePort,
                                                INT4 i4Dot1vProtocolPortGroupId)
{
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT2               u2BasePort = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2BasePort = (UINT2) i4Dot1dBasePort;

    pVlanPortVidSetEntry =
        VlanGetPortProtoVidSetEntry (u2BasePort,
                                     (UINT4) i4Dot1vProtocolPortGroupId);

    if (pVlanPortVidSetEntry != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1vProtocolPortTable
 Input       :  The Indices
                Dot1dBasePort
                Dot1vProtocolPortGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1vProtocolPortTable (INT4 *pi4Dot1dBasePort,
                                        INT4 *pi4Dot1vProtocolPortGroupId)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1vProtocolPortTable (0,
                                                   pi4Dot1dBasePort,
                                                   0,
                                                   pi4Dot1vProtocolPortGroupId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1vProtocolPortTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
                Dot1vProtocolPortGroupId
                nextDot1vProtocolPortGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1vProtocolPortTable (INT4 i4Dot1dBasePort,
                                       INT4 *pi4NextDot1dBasePort,
                                       INT4 i4Dot1vProtocolPortGroupId,
                                       INT4 *pi4NextDot1vProtocolPortGroupId)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT1               u1IsEntryFound = VLAN_FALSE;
    UINT2               u2Port = 0;
    UINT2               u2FirstPort = 0;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if ((i4Dot1dBasePort < 0) || (i4Dot1vProtocolPortGroupId < 0))
    {
        return SNMP_FAILURE;
    }

    u2FirstPort = (UINT2) i4Dot1dBasePort;
    for (u2Port = gpVlanContextInfo->u2VlanStartPortInd;
         u2Port != VLAN_INVALID_PORT_INDEX;
         u2Port = VLAN_GET_NEXT_PORT_INDEX (u2Port))
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSetEntry,
                       tVlanPortVidSet *)
        {
            if ((u2FirstPort < u2Port) || ((u2FirstPort == u2Port) &&
                                           (i4Dot1vProtocolPortGroupId <
                                            (INT4)
                                            pVlanPortVidSetEntry->u4ProtGrpId)))
            {
                if (u1IsEntryFound == VLAN_FALSE)
                {

                    *pi4NextDot1dBasePort = (INT4) u2Port;
                    *pi4NextDot1vProtocolPortGroupId
                        = pVlanPortVidSetEntry->u4ProtGrpId;
                    u1IsEntryFound = VLAN_TRUE;
                }
                else
                {

                    if ((*pi4NextDot1dBasePort > (INT4) u2Port) ||
                        ((*pi4NextDot1dBasePort == (INT4) u2Port) &&
                         (*pi4NextDot1vProtocolPortGroupId >
                          (INT4) pVlanPortVidSetEntry->u4ProtGrpId)))
                    {

                        *pi4NextDot1dBasePort = (INT4) u2Port;
                        *pi4NextDot1vProtocolPortGroupId
                            = pVlanPortVidSetEntry->u4ProtGrpId;

                    }
                }
            }

        }
    }

    if (u1IsEntryFound == VLAN_TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetNextLearningConstraintsTable              */
/*                                                                           */
/*    Description         : This function gets the indices of the            */
/*                          learning constraints table lexicographically     */
/*                          greater than the input indices.                  */
/*                                                                           */
/*    Input(s)            : u4Dot1qConstraintVlan - Constraint Vlan value    */
/*                            i4Dot1qConstraintSet  - Constraint Set Value   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : pu4NextDot1qConstraintVlan - Next Constraint     */
/*                                                       Vlan in the table.  */
/*                                                                           */
/*                          pi4NextDot1qConstraintSet  - Next Constraint     */
/*                                                       Set in the table.   */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->aVlanConstraintTable                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SNMP_SUCCESS / SNMP_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT1
VlanGetNextLearningConstraintsTable (UINT4 u4Dot1qConstraintVlan,
                                     UINT4 *pu4NextDot1qConstraintVlan,
                                     INT4 i4Dot1qConstraintSet,
                                     INT4 *pi4NextDot1qConstraintSet,
                                     UINT1 u1VlanSort)
{
    tVlanConstraintEntry *pConstraintEntry = NULL;
    UINT4               u4Index;
    UINT1               u1EntryFound = VLAN_FALSE;
    tVlanId             VlanId;
    UINT2               u2ConstraintSet;
    INT4                i4Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = 0; u4Index < VLAN_MAX_CONSTRAINTS; u4Index++)
    {
        pConstraintEntry = &gpVlanContextInfo->aVlanConstraintTable[u4Index];

        if (VLAN_DESTROY != pConstraintEntry->u1RowStatus)
        {
            VlanId = pConstraintEntry->VlanId;
            u2ConstraintSet = pConstraintEntry->u2ConstraintSet;

            i4Result
                = VlanConstraintSetCmp (u4Dot1qConstraintVlan,
                                        i4Dot1qConstraintSet,
                                        VlanId, u2ConstraintSet, u1VlanSort);

            if (i4Result < 0)
            {
                if (VLAN_FALSE == u1EntryFound)
                {
                    *pu4NextDot1qConstraintVlan = VlanId;
                    *pi4NextDot1qConstraintSet = u2ConstraintSet;

                    u1EntryFound = VLAN_TRUE;
                }
                else
                {
                    i4Result
                        = VlanConstraintSetCmp (*pu4NextDot1qConstraintVlan,
                                                *pi4NextDot1qConstraintSet,
                                                VlanId, u2ConstraintSet,
                                                u1VlanSort);

                    if (i4Result > 0)
                    {
                        *pu4NextDot1qConstraintVlan = VlanId;
                        *pi4NextDot1qConstraintSet = u2ConstraintSet;
                    }
                }
            }
        }
    }

    if (u1EntryFound == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

INT4
VlanConstraintSetCmp (UINT4 u4Vlan1, INT4 i4Set1,
                      UINT4 u4Vlan2, INT4 i4Set2, UINT1 u1VlanFlag)
{
    INT4                i4LhsIndex1;
    INT4                i4LhsIndex2;
    INT4                i4RhsIndex1;
    INT4                i4RhsIndex2;

    if (u1VlanFlag == VLAN_TRUE)
    {
        i4LhsIndex1 = (INT4) u4Vlan1;
        i4LhsIndex2 = i4Set1;
        i4RhsIndex1 = (INT4) u4Vlan2;
        i4RhsIndex2 = i4Set2;
    }
    else
    {
        i4LhsIndex1 = i4Set1;
        i4LhsIndex2 = (INT4) u4Vlan1;
        i4RhsIndex1 = i4Set2;
        i4RhsIndex2 = (INT4) u4Vlan2;
    }

    if ((i4LhsIndex1 > i4RhsIndex1)
        || ((i4LhsIndex1 == i4RhsIndex1) && (i4LhsIndex2 > i4RhsIndex2)))
    {
        return 1;
    }

    if ((i4LhsIndex1 < i4RhsIndex1)
        || ((i4LhsIndex1 == i4RhsIndex1) && (i4LhsIndex2 < i4RhsIndex2)))
    {
        return -1;
    }

    return 0;
}

/* LOW LEVEL Routines for Table : Dot1qFutureVlanTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanTunnelTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanTunnelTable (INT4 i4Dot1qFutureVlanPort)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanTunnelTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1qFutureVlanTunnelTable (INT4 *pi4Dot1qFutureVlanPort)
{
    UNUSED_PARAM (pi4Dot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanTunnelTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                nextDot1qFutureVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1qFutureVlanTunnelTable (INT4 i4Dot1qFutureVlanPort,
                                           INT4 *pi4NextDot1qFutureVlanPort)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (pi4NextDot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable (INT4
                                                            i4Dot1qFutureVlanPort)
{
    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                Dot1qFutureVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1qFutureVlanTunnelProtocolTable (INT4
                                                    *pi4Dot1qFutureVlanPort)
{
    UNUSED_PARAM (pi4Dot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                nextDot1qFutureVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1qFutureVlanTunnelProtocolTable (INT4 i4Dot1qFutureVlanPort,
                                                   INT4
                                                   *pi4NextDot1qFutureVlanPort)
{

    UNUSED_PARAM (i4Dot1qFutureVlanPort);
    UNUSED_PARAM (pi4NextDot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanFidMapTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1qFutureVlanFidMapTable (UINT4
                                                    u4Dot1qFutureVlanIndex)
{
    UINT1               u1VlanLearning;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    u1VlanLearning = VlanGetVlanLearningMode ();

    if (u1VlanLearning != VLAN_HYBRID_LEARNING)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanFidMapTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1qFutureVlanFidMapTable (UINT4 *pu4Dot1qFutureVlanIndex)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qFutureVlanFidMapTable
            (0, pu4Dot1qFutureVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanFidMapTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
                nextDot1qFutureVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1qFutureVlanFidMapTable (UINT4 u4Dot1qFutureVlanIndex,
                                           UINT4 *pu4NextDot1qFutureVlanIndex)
{
    UINT1               u1VlanLearning;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u1VlanLearning = VlanGetVlanLearningMode ();

    if (u1VlanLearning != VLAN_HYBRID_LEARNING)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qFutureVlanIndex >= VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }

    *pu4NextDot1qFutureVlanIndex = u4Dot1qFutureVlanIndex + 1;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qFutureVlanCounterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanCounterTable
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceDot1qFutureVlanCounterTable
    (UINT4 u4Dot1qFutureVlanIndex)
{
    tVlanCurrEntry     *pVlanCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4Dot1qFutureVlanIndex);

    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanCounterTable
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexDot1qFutureVlanCounterTable
    (UINT4 *pu4Dot1qFutureVlanIndex)
{
    return nmhGetNextIndexDot1qFutureVlanCounterTable
        (0, pu4Dot1qFutureVlanIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanCounterTable
 Input       :  The Indices
                Dot1qFutureVlanCounterVlanId
                nextDot1qFutureVlanCounterVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexDot1qFutureVlanCounterTable
    (UINT4 u4Dot1qFutureVlanIndex, UINT4 *pu4NextDot1qFutureVlanIndex)
{

    tVlanCurrEntry     *pVlanCurrEntry = NULL;
#ifndef NPAPI_WANTED
    tVlanStats         *pVlanCounters = NULL;
#endif
    UINT4               u4VlanId;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qFutureVlanIndex > VLAN_MAX_VLAN_ID)

    {
        return SNMP_FAILURE;
    }

    for (u4VlanId = u4Dot1qFutureVlanIndex + 1;
         u4VlanId <= VLAN_MAX_VLAN_ID; u4VlanId++)
    {
        pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4VlanId);

        if (pVlanCurrEntry != NULL)
        {
#ifndef NPAPI_WANTED
            if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
            {
                pVlanCounters = pVlanCurrEntry->pVlanStats;
                if (pVlanCounters != NULL)
                {
                    *pu4NextDot1qFutureVlanIndex =
                        (tVlanId) pVlanCurrEntry->VlanId;
                    return SNMP_SUCCESS;
                }
            }
            else
#endif
            {
                *pu4NextDot1qFutureVlanIndex = (tVlanId) pVlanCurrEntry->VlanId;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Dot1qFutureVlanUnicastMacControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
    (UINT4 u4Dot1qFutureVlanIndex)
{
    tVlanCurrEntry     *pVlanCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4Dot1qFutureVlanIndex);

    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexDot1qFutureVlanUnicastMacControlTable
    (UINT4 *pu4Dot1qFutureVlanIndex)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return nmhGetNextIndexDot1qFutureVlanUnicastMacControlTable
        (0, pu4Dot1qFutureVlanIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                Dot1qFutureVlanIndex
                nextDot1qFutureVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexDot1qFutureVlanUnicastMacControlTable
    (UINT4 u4Dot1qFutureVlanIndex, UINT4 *pu4NextDot1qFutureVlanIndex)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT4               u4VlanId;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qFutureVlanIndex > VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }

    for (u4VlanId = u4Dot1qFutureVlanIndex + 1;
         u4VlanId <= VLAN_MAX_VLAN_ID; u4VlanId++)
    {
        pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4VlanId);

        if (pVlanCurrEntry != NULL)
        {
            if (pVlanCurrEntry->pVlanMacControl != NULL)
            {
                *pu4NextDot1qFutureVlanIndex = (UINT4) (pVlanCurrEntry->VlanId);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanWildCardTable
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1qFutureVlanWildCardTable (tMacAddr
                                                      WildCardMacAddress)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanWildCardTable
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1qFutureVlanWildCardTable (tMacAddr * pWildCardMacAddress)
{
    tMacAddr            MacAddr;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexDot1qFutureVlanWildCardTable (MacAddr,
                                                         pWildCardMacAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanWildCardTable
 Input       :  The Indices
                Dot1qFutureVlanWildCardMacAddress
                nextDot1qFutureVlanWildCardMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1qFutureVlanWildCardTable (tMacAddr WildCardMacAddress,
                                             tMacAddr * pNextWildCardMacAddress)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tVlanWildCardEntry *pNextWildCardEntry = NULL;
    INT4                i4Result;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    VLAN_SLL_SCAN (&gpVlanContextInfo->WildCardTable, pWildCardEntry,
                   tVlanWildCardEntry *)
    {
        i4Result = VlanCmpMacAddr (WildCardMacAddress, pWildCardEntry->MacAddr);

        if (VLAN_LESSER == i4Result)
        {
            if (pNextWildCardEntry == NULL)
            {
                pNextWildCardEntry = pWildCardEntry;
            }
            else
            {
                i4Result = VlanCmpMacAddr (pNextWildCardEntry->MacAddr,
                                           pWildCardEntry->MacAddr);
                if (VLAN_GREATER == i4Result)
                {
                    pNextWildCardEntry = pWildCardEntry;
                }
            }
        }

    }

    if (pNextWildCardEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pNextWildCardMacAddress, pNextWildCardEntry->MacAddr,
            sizeof (tMacAddr));

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1qFutureStaticUnicastExtnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceDot1qFutureStaticUnicastExtnTable
    (UINT4 u4Dot1qFdbId,
     tMacAddr Dot1qStaticUnicastAddress, INT4 i4Dot1qStaticUnicastReceivePort)
{
    return (nmhValidateIndexInstanceDot1qStaticUnicastTable
            (u4Dot1qFdbId,
             Dot1qStaticUnicastAddress, i4Dot1qStaticUnicastReceivePort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                Dot1qFdbId
                Dot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexDot1qFutureStaticUnicastExtnTable
    (UINT4 *pu4Dot1qFdbId,
     tMacAddr * pDot1qStaticUnicastAddress,
     INT4 *pi4Dot1qStaticUnicastReceivePort)
{
    return (nmhGetFirstIndexDot1qStaticUnicastTable
            (pu4Dot1qFdbId,
             pDot1qStaticUnicastAddress, pi4Dot1qStaticUnicastReceivePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                Dot1qFdbId
                nextDot1qFdbId
                Dot1qStaticUnicastAddress
                nextDot1qStaticUnicastAddress
                Dot1qStaticUnicastReceivePort
                nextDot1qStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexDot1qFutureStaticUnicastExtnTable
    (UINT4 u4Dot1qFdbId,
     UINT4 *pu4NextDot1qFdbId,
     tMacAddr Dot1qStaticUnicastAddress,
     tMacAddr * pNextDot1qStaticUnicastAddress,
     INT4 i4Dot1qStaticUnicastReceivePort,
     INT4 *pi4NextDot1qStaticUnicastReceivePort)
{
    return (nmhGetNextIndexDot1qStaticUnicastTable
            (u4Dot1qFdbId,
             pu4NextDot1qFdbId,
             Dot1qStaticUnicastAddress,
             pNextDot1qStaticUnicastAddress,
             i4Dot1qStaticUnicastReceivePort,
             pi4NextDot1qStaticUnicastReceivePort));
}

/* LOW LEVEL Routines for Table : Dot1qFutureVlanPortSubnetMapTable. */
                                                                                   /****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable (INT4
                                                           i4Dot1qFutureVlanPort,
                                                           UINT4
                                                           u4Dot1qFutureVlanPortSubnetMapAddr)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);
    return (nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
            (i4Dot1qFutureVlanPort, u4Dot1qFutureVlanPortSubnetMapAddr,
             u4DefMask));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexDot1qFutureVlanPortSubnetMapTable
    (INT4 *pi4Dot1qFutureVlanPort, UINT4 *pu4Dot1qFutureVlanPortSubnetMapAddr)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;

    return (nmhGetFirstIndexDot1qFutureVlanPortSubnetMapExtTable
            (pi4Dot1qFutureVlanPort, pu4Dot1qFutureVlanPortSubnetMapAddr,
             &u4DefMask));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                nextDot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapAddr
                nextDot1qFutureVlanPortSubnetMapAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable
    (INT4 i4Dot1qFutureVlanPort, INT4 *pi4NextDot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapAddr,
     UINT4 *pu4NextDot1qFutureVlanPortSubnetMapAddr)
{
    UINT4               u4DefMask = VLAN_INIT_VAL;
    UINT4               u4NextSubnetMask = VLAN_INIT_VAL;

    u4DefMask =
        VlanGetDefMaskFromSrcIPAddr (u4Dot1qFutureVlanPortSubnetMapAddr);

    while (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
           (i4Dot1qFutureVlanPort, pi4NextDot1qFutureVlanPort,
            u4Dot1qFutureVlanPortSubnetMapAddr,
            pu4NextDot1qFutureVlanPortSubnetMapAddr, u4DefMask,
            &u4NextSubnetMask) == SNMP_SUCCESS)
    {
        if ((i4Dot1qFutureVlanPort == *pi4NextDot1qFutureVlanPort) &&
            (u4Dot1qFutureVlanPortSubnetMapAddr ==
             *pu4NextDot1qFutureVlanPortSubnetMapAddr))
        {
            i4Dot1qFutureVlanPort = *pi4NextDot1qFutureVlanPort;
            u4Dot1qFutureVlanPortSubnetMapAddr =
                *pu4NextDot1qFutureVlanPortSubnetMapAddr;
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
    (INT4 i4Dot1qFutureVlanPort, UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask)
{
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    pSubnetMapEntry =
        VlanGetSubnetMapEntry (u4Dot1qFutureVlanPortSubnetMapExtAddr,
                               i4Dot1qFutureVlanPort,
                               u4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pSubnetMapEntry != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexDot1qFutureVlanPortSubnetMapExtTable
    (INT4 *pi4Dot1qFutureVlanPort,
     UINT4 *pu4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 *pu4Dot1qFutureVlanPortSubnetMapExtMask)
{
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;
    UINT4               u4SubnetAddr = VLAN_INIT_VAL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSubnetMapTableGetFirstEntry ((UINT4 *) pi4Dot1qFutureVlanPort,
                                         &u4SubnetAddr,
                                         pu4Dot1qFutureVlanPortSubnetMapExtMask)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pSubnetMapEntry =
        VlanGetSubnetMapEntry (u4SubnetAddr, (UINT4) (*pi4Dot1qFutureVlanPort),
                               *pu4Dot1qFutureVlanPortSubnetMapExtMask);

    if (pSubnetMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4Dot1qFutureVlanPortSubnetMapExtAddr = pSubnetMapEntry->SrcIPAddr;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                Dot1qFutureVlanPort
                nextDot1qFutureVlanPort
                Dot1qFutureVlanPortSubnetMapExtAddr
                nextDot1qFutureVlanPortSubnetMapExtAddr
                Dot1qFutureVlanPortSubnetMapExtMask
                nextDot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
    (INT4 i4Dot1qFutureVlanPort, INT4 *pi4NextDot1qFutureVlanPort,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 *pu4NextDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4Dot1qFutureVlanPortSubnetMapExtMask,
     UINT4 *pu4NextDot1qFutureVlanPortSubnetMapExtMask)
{
    tVlanSubnetMapIndex CurrSubnetMapIndex;
    tVlanSubnetMapIndex NextSubnetMapIndex;
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;

    MEMSET (&CurrSubnetMapIndex, VLAN_INIT_VAL, sizeof (tVlanSubnetMapIndex));
    MEMSET (&NextSubnetMapIndex, VLAN_INIT_VAL, sizeof (tVlanSubnetMapIndex));

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    CurrSubnetMapIndex.u4Port = i4Dot1qFutureVlanPort;
    CurrSubnetMapIndex.SubnetAddr = u4Dot1qFutureVlanPortSubnetMapExtAddr;
    CurrSubnetMapIndex.SubnetMask = u4Dot1qFutureVlanPortSubnetMapExtMask;

    if (VlanSubnetMapTableGetNextEntry
        (&CurrSubnetMapIndex, &NextSubnetMapIndex) == VLAN_SUCCESS)
    {
        pSubnetMapEntry =
            VlanGetSubnetMapEntry (NextSubnetMapIndex.SubnetAddr,
                                   NextSubnetMapIndex.u4Port,
                                   NextSubnetMapIndex.SubnetMask);

        if (pSubnetMapEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4NextDot1qFutureVlanPort = NextSubnetMapIndex.u4Port;
        *pu4NextDot1qFutureVlanPortSubnetMapExtAddr =
            NextSubnetMapIndex.SubnetAddr;

        *pu4NextDot1qFutureVlanPortSubnetMapExtMask =
            NextSubnetMapIndex.SubnetMask;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureStVlanExtTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1qFutureStVlanExtTable (UINT4 *pu4Dot1qVlanIndex)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1qFutureStVlanExtTable (0, pu4Dot1qVlanIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureStVlanExtTable
 Input       :  The Indices
                Dot1qVlanIndex
                nextDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1qFutureStVlanExtTable (UINT4 u4Dot1qVlanIndex,
                                          UINT4 *pu4NextDot1qVlanIndex)
{
    return (nmhGetNextIndexDot1qVlanStaticTable (u4Dot1qVlanIndex,
                                                 pu4NextDot1qVlanIndex));
}

/* LOW LEVEL Routines for Table : Dot1qFutureStVlanExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureStVlanExtTable
 Input       :  The Indices
                Dot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1qFutureStVlanExtTable (UINT4 u4Dot1qVlanIndex)
{
    return (nmhValidateIndexInstanceDot1qVlanStaticTable (u4Dot1qVlanIndex));
}

/* LOW LEVEL Routines for Table : Dot1qFutureVlanLoopbackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                Dot1qFutureVlanLoopbackVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceDot1qFutureVlanLoopbackTable
    (UINT4 u4Dot1qFutureVlanIndex)
{
    tVlanCurrEntry     *pVlanCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_VLAN_ID_VALID (u4Dot1qFutureVlanIndex) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4Dot1qFutureVlanIndex);

    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                Dot1qFutureVlanLoopbackVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexDot1qFutureVlanLoopbackTable
    (UINT4 *pu4Dot1qFutureVlanIndex)
{
    return nmhGetNextIndexDot1qFutureVlanLoopbackTable
        (0, pu4Dot1qFutureVlanIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                Dot1qFutureVlanLoopbackVlanId
                nextDot1qFutureVlanLoopbackVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexDot1qFutureVlanLoopbackTable
    (UINT4 u4Dot1qFutureVlanIndex, UINT4 *pu4NextDot1qFutureVlanIndex)
{

    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT4               u4VlanId;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (u4Dot1qFutureVlanIndex > VLAN_MAX_VLAN_ID)

    {
        return SNMP_FAILURE;
    }

    for (u4VlanId = u4Dot1qFutureVlanIndex + 1;
         u4VlanId <= VLAN_MAX_VLAN_ID; u4VlanId++)
    {
        pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u4VlanId);

        if (pVlanCurrEntry != NULL)
        {
            *pu4NextDot1qFutureVlanIndex = (tVlanId) pVlanCurrEntry->VlanId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}
