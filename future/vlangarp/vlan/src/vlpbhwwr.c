/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vlpbhwwr.c,v 1.12 2016/05/06 10:39:18 siva Exp $
/ * Licensee Aricent Inc., 2005-2006                        */
/*                                                                           */
/*  FILE NAME             : vlpbnpwr.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 11 Aug 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN H/W Wrapper routines.    */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 11/08/2006                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/
#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanHwSetProviderBridgePortType.                     */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
VlanHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT4 u4PortType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetProviderBridgePortType (u4ContextId,
                                                     u4IfIndex,
                                                     u4PortType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PortType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPortSVlanTranslationStatus.                 */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On Success                            */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1Status)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortSVlanTranslationStatus (u4ContextId,
                                                         u4IfIndex,
                                                         u1Status) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwAddSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On Success                            */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAddSVlanTranslationEntry (u4ContextId,
                                                    u4IfIndex,
                                                    u2LocalSVlan,
                                                    u2RelaySVlan) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwDelSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      from an VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwDelSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelSVlanTranslationEntry (u4ContextId,
                                                    u4IfIndex,
                                                    u2LocalSVlan,
                                                    u2RelaySVlan) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPortEtherTypeSwapStatus.                */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT1 u1Status)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortEtherTypeSwapStatus (u4ContextId,
                                                      u4IfIndex,
                                                      u1Status) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwAddEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet  */
/*                      u2RelayEtherType- Ether type understand by the bridge*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                             UINT2 u2LocalEtherType, UINT2 u2RelayEtherType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAddEtherTypeSwapEntry (u4ContextId,
                                                 u4IfIndex,
                                                 u2LocalEtherType,
                                                 u2RelayEtherType) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwDelEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwDelEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                             UINT2 u2LocalEtherType, UINT2 u2RelayEtherType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelEtherTypeSwapEntry (u4ContextId,
                                                 u4IfIndex,
                                                 u2LocalEtherType,
                                                 u2RelayEtherType) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwAddSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                                                                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
#ifdef NPAPI_WANTED

    if (VlanSVlanMap.u1PepUntag == VLAN_SNMP_TRUE)
    {
        VlanSVlanMap.u1PepUntag = FNP_TRUE;
    }
    else
    {
        VlanSVlanMap.u1PepUntag = FNP_FALSE;
    }

    if (VlanSVlanMap.u1CepUntag == VLAN_SNMP_TRUE)
    {
        VlanSVlanMap.u1CepUntag = FNP_TRUE;
    }
    else
    {
        VlanSVlanMap.u1CepUntag = FNP_FALSE;
    }

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAddSVlanMap (u4ContextId, VlanSVlanMap) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
#endif
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanHwDelSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is deleted for */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwDeleteSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
#ifdef NPAPI_WANTED
    tHwVlanCVlanStat   VlanStat;
    if (VlanSVlanMap.u1PepUntag == VLAN_SNMP_TRUE)
    {
        VlanSVlanMap.u1PepUntag = FNP_TRUE;
    }
    else
    {
        VlanSVlanMap.u1PepUntag = FNP_FALSE;
    }

    if (VlanSVlanMap.u1CepUntag == VLAN_SNMP_TRUE)
    {
        VlanSVlanMap.u1CepUntag = FNP_TRUE;
    }
    else
    {
        VlanSVlanMap.u1CepUntag = FNP_FALSE;
    }

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        VlanStat.CVlanId =  VlanSVlanMap.CVlanId; 
        VlanStat.u2Port  =  VlanSVlanMap.u2Port;
        VlanStat.u1Status = VLAN_DISABLED;
        VlanFsMiVlanHwSetCVlanStat (u4ContextId,VlanStat);
	if (VlanPbIsSVlanActive (VlanSVlanMap.SVlanId) == VLAN_TRUE)
	{
		if (VlanFsMiVlanHwDeleteSVlanMap (u4ContextId, VlanSVlanMap) !=
				FNP_SUCCESS)
		{
			return VLAN_FAILURE;
		}
	}
    }

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPortSVlanClassifyMethod                 */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT1 u1TableType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortSVlanClassifyMethod (u4ContextId,
                                                      u4IfIndex,
                                                      u1TableType) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1TableType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwPortMacLearningLimit.                      */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                            UINT4 u4MacLimit)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwPortMacLearningLimit (u4ContextId,
                                                u4IfIndex,
                                                u4MacLimit) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MacLimit);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwStaticMulticastMacTableLimit .             */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - Mac Limit of the Multicast Table        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
#ifdef NPAPI_WANTED
    if (VlanFsMiVlanHwMulticastMacTableLimit (u4ContextId,
                                              u4MacLimit) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MacLimit);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPortCustomerVlan.                       */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u2Port - Port Index                                  */
/*                      CVlanId - Configured Customer VLAN ID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortCustomerVlan (UINT4 u4ContextId, UINT2 u2Port, tVlanId CVlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortCustomerVlan (u4ContextId,
                                               u2Port, CVlanId) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (CVlanId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwResetPortCustomerVlan.                     */
/*                                                                           */
/* Description        : This function is called for resetting the ports      */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u2Port - Port Index                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwResetPortCustomerVlan (UINT4 u4ContextId, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwResetPortCustomerVlan (u4ContextId,
                                                 u2Port) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwCreateProviderEdgePort                     */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id.                         */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwCreateProviderEdgePort (UINT4 u4ContextId, UINT2 u2IfIndex,
                              tVlanId SVlanId, tHwVlanPbPepInfo PepConfig)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwCreateProviderEdgePort (u4ContextId,
                                                  u2IfIndex,
                                                  SVlanId,
                                                  PepConfig) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (PepConfig);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwDelProviderEdgePort                          */
/*                                                                           */
/* Description        : This function deletes the Provider Edge Port from    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwDelProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelProviderEdgePort (u4ContextId,
                                               u4IfIndex,
                                               SVlanId) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPepPvid                                 */
/*                                                                           */
/* Description        : This function sets the PVID for the given Provider   */
/*                      Edge Port in hardware.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      Pvid       - PVID to be set.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPepPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                  tVlanId Pvid)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPepPvid (u4ContextId,
                                      u4IfIndex, SVlanId, Pvid) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (Pvid);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPepAccFrameType                           */
/*                                                                           */
/* Description        : This function sets the Acceptable frame type for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      u1AccepFrameType - Acceptable frame type to be set   */
/*                                         for this PEP.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPepAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tVlanId SVlanId, UINT1 u1AccepFrameType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPepAccFrameType (u4ContextId,
                                              u4IfIndex,
                                              SVlanId,
                                              u1AccepFrameType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1AccepFrameType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPepDefUserPriority                        */
/*                                                                           */
/* Description        : This function sets the Default User priority for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      i4DefUsrPri  - Default user priority for this PEP.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPepDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                             tVlanId SVlanId, INT4 i4DefUsrPri)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPepDefUserPriority (u4ContextId,
                                                 u4IfIndex,
                                                 SVlanId,
                                                 i4DefUsrPri) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4DefUsrPri);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPepIngFiltering                           */
/*                                                                           */
/* Description        : This function sets the Enable Ingress Filetering     */
/*                      parameter for the given Provider Edge Port in        */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      u1IngFilterEnable - Enable / Disable Ingress         */
/*                                          filtering.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPepIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tVlanId SVlanId, UINT1 u1IngFilterEnable)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPepIngFiltering (u4ContextId,
                                              u4IfIndex,
                                              SVlanId,
                                              u1IngFilterEnable) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1IngFilterEnable);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPcpEncodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.         */
/*                          u2Priority  - Priority value                     */
/*                          u1DropEligible - Drop Eligible value             */
/*                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                      tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPcpEncodTbl (u4ContextId,
                                          u4IfIndex,
                                          NpPbVlanPcpInfo) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPcpDecodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.         */
/*                          u2PcpVlaue  - PCP value                          */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).   */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp).            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                      tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPcpDecodTbl (u4ContextId,
                                          u4IfIndex,
                                          NpPbVlanPcpInfo) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
#endif
    return VLAN_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : VlanHwSetPortUseDei.                               */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei)
{
#ifdef NPAPI_WANTED
    if (u1UseDei == VLAN_SNMP_TRUE)
    {
        u1UseDei = FNP_TRUE;
    }
    else
    {
        u1UseDei = FNP_FALSE;
    }
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortUseDei (u4ContextId,
                                         u4IfIndex, u1UseDei) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
#endif
    return VLAN_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : VlanHwSetPortReqDropEncoding.                      */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT1 u1ReqDrpEncoding)
{
#ifdef NPAPI_WANTED
    if (u1ReqDrpEncoding == VLAN_SNMP_TRUE)
    {
        u1ReqDrpEncoding = FNP_TRUE;
    }
    else
    {
        u1ReqDrpEncoding = FNP_FALSE;
    }
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortReqDropEncoding (u4ContextId,
                                                  u4IfIndex,
                                                  u1ReqDrpEncoding) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
#endif
    return VLAN_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : VlanHwSetPortPcpSelRow.                            */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex       - Port Index.                        */
/*                      u2PcpSelection  - It can take the following values:  */
/*                                            8P0D, 7P1D, 6P2D, 5P3D         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                           UINT2 u2PcpSelection)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortPcpSelection (u4ContextId,
                                               u4IfIndex,
                                               u2PcpSelection) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PcpSelection);
#endif
    return VLAN_SUCCESS;
}

/* Internal CNP - Service Priority Regeneration Table NPAPIs */
/*****************************************************************************/
/* Function Name      : VlanHwSetServicePriRegenEntry                        */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      i4RecvPriority - Receive Priority                    */
/*                      i4RegenPriority - Regenerated Priority               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId SVlanId, INT4 i4RecvPriority,
                               INT4 i4RegenPriority)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetServicePriRegenEntry (u4ContextId,
                                                   u4IfIndex,
                                                   SVlanId,
                                                   i4RecvPriority,
                                                   i4RegenPriority) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4RecvPriority);
    UNUSED_PARAM (i4RegenPriority);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetTunnelMacAddress                            */
/*                                                                           */
/* Description        : This function creates filters for receiving the      */
/*                      packets destined to proprietary tunnel MAC           */
/*                      address configured for different L2 protocols.       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetTunnelMacAddress (UINT4 u4ContextId, tMacAddr MacAddr,
                           UINT2 u2Protocol)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetTunnelMacAddress (u4ContextId, MacAddr, u2Protocol)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Protocol);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetCvidUntagPep                                */
/*                                                                           */
/* Description        : This function sets the UntagPep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetCvidUntagPep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
#ifdef NPAPI_WANTED
    if (VlanSVlanMap.u1PepUntag == VLAN_SNMP_TRUE)
    {
        VlanSVlanMap.u1PepUntag = FNP_TRUE;
    }
    else
    {
        VlanSVlanMap.u1PepUntag = FNP_FALSE;
    }
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetCvidUntagPep (u4ContextId, VlanSVlanMap) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetCvidUntagCep                            */
/*                                                                           */
/* Description        : This function sets the UntagCep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetCvidUntagCep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
#ifdef NPAPI_WANTED
    if (VlanSVlanMap.u1CepUntag == VLAN_SNMP_TRUE)
    {
        VlanSVlanMap.u1CepUntag = FNP_TRUE;
    }
    else
    {
        VlanSVlanMap.u1CepUntag = FNP_FALSE;
    }

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetCvidUntagCep (u4ContextId, VlanSVlanMap) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetNextSVlanMap                            */
/*                                                                           */
/* Description        : This function is called to get Cvid Entry for CVlan  */
/*                      for Port in the SVLAN classification tables          */
/*                                                                           */
/* Input(s)           : VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                      CvlanId - CVlan Id                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On FNP_SUCCESS                        */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/

INT4
VlanHwGetNextSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                       tVlanSVlanMap * pRetVlanSVlanMap)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetNextSVlanMap
            (u4ContextId, VlanSVlanMap, pRetVlanSVlanMap) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    UNUSED_PARAM (pRetVlanSVlanMap);
#endif
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VlanHwGetNextSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called to get entry                 */
/*                      from a  VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                      pVidTransEntryInfo - Vid Translation Entry           */
/*                      u1IsLocalVid - OSIX_TRUE : Local Vid should be used  */
/*                                     OSIX_FALSE : Relay Vid should be used */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
VlanHwGetNextSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan,
                                    tVidTransEntryInfo * pVidTransEntryInfo)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetNextSVlanTranslationEntry (u4ContextId,
                                                        u4IfIndex,
                                                        u2LocalSVlan,
                                                        pVidTransEntryInfo)
            != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (pVidTransEntryInfo);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetEvcAttribute                                */
/*                                                                           */
/* Description        : This function is called to set Evc Attribute entry   */
/*                      to NPAPI                                             */
/*                                                                           */
/* Input(s)           : i4FsEvcContextId - Virtual Context Identifier.       */
/*                      u4Action - Attribute Action                          */
/*                      pIssEvcInfo - Evc Info Entry                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
VlanHwSetEvcAttribute (INT4 i4FsEvcContextId, UINT4 u4Action,
                       tEvcInfo * pIssEvcInfo)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetEvcAttribute (i4FsEvcContextId,
                                           u4Action,
                                           pIssEvcInfo) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4FsEvcContextId);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pIssEvcInfo);
#endif
    return VLAN_SUCCESS;
}



/*****************************************************************************/
/* Function Name      : VlanHwGetCVlanStats                                  */
/*                                                                           */
/* Description        : This function is called to get Customer Vlan         */
/*                      statistics information.                              */
/*                                                                           */
/* Input(s)           : i4ContextId - Virtual Context Identifier.            */
/*                      u2Port      - Port Number                            */
/*                      i1StatType  - Stat Type                              */
/*                                    1.Unicast packets                      */
/*                                    2.Frames                               */
/*                                    3.Bytes                                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
VlanHwGetCVlanStats (INT4 i4ContextId, INT4 u2Port, UINT4 u4CVlanId,
                     UINT1 i1StatType,UINT4 *pu4Value)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        switch (i1StatType)
        {
            case VLAN_STAT_UCAST_IN_FRAMES:
                i1StatType = NP_STAT_VLAN_UCAST_IN_FRAMES;
                break;
            case VLAN_STAT_VLAN_IN_FRAMES:
                i1StatType = NP_STAT_CUSTOMER_VLAN_IN_FRAMES;
                break;
            case VLAN_STAT_VLAN_IN_BYTES:
                i1StatType = NP_STAT_CUSTOMER_VLAN_IN_BYTES;
                break;

        }
        if (VlanFsMiVlanHwGetCVlanStats (i4ContextId,
                                        u2Port,
                                        u4CVlanId,
                                        i1StatType, pu4Value) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        } 
              
    }
#else
    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (u4CVlanId);
    UNUSED_PARAM (i1StatType);
    UNUSED_PARAM (pu4Value);
    UNUSED_PARAM (u2Port);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwClearCVlanStats                                */
/*                                                                           */
/* Description        : This function is called to clear Customer Vlan       */
/*                      statistics information.                              */
/*                                                                           */
/* Input(s)           : i4FsEvcContextId - Virtual Context Identifier.       */
/*                      u4Action - Attribute Action                          */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
VlanHwClearCVlanStats (INT4 i4ContextId, INT4 u2Port, UINT2 u2CVlanId)
{
#ifdef NPAPI_WANTED
    INT4                i4Result = VLAN_FAILURE;

    i4Result =  VlanFsMiVlanHwClearCVlanStats (i4ContextId,
                               u2Port,
                               u2CVlanId); 
    if (i4Result == FNP_SUCCESS)
    {
        return VLAN_SUCCESS;
    }
#else

    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (u2CVlanId);
    UNUSED_PARAM (u2Port);
#endif
    return VLAN_FAILURE;
}
